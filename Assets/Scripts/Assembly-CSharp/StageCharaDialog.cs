using System.Collections.Generic;
using UnityEngine;

public class StageCharaDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		PLAY = 0,
		CANCEL = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private int mSelectedPartnerNo = -1;

	private SMVerticalListInfo mIconListInfo;

	private SMVerticalList mIconList;

	private GameObject mListCenter;

	public int SelectedPartnerNo
	{
		get
		{
			return mSelectedPartnerNo;
		}
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("LevelInfo_SelectPartners_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		mIconListInfo = new SMVerticalListInfo(4, 490f, 455f, 4, 490f, 455f, 1, 130);
		UILabel uILabel = Util.CreateLabel("DescTop", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("LevelInfo_SelectPartners"), mBaseDepth + 3, new Vector3(0f, 226f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		CreateCancelButton("OnCancelPushed");
	}

	private void ListOpen()
	{
		List<SMVerticalListItem> items = PartnerListItem.CreatePartnerList();
		mListCenter = Util.CreateGameObject("GridRoot", base.gameObject);
		mListCenter.transform.localPosition = new Vector3(-8f, -30f, 0f);
		mIconList = SMVerticalList.Make(mListCenter, this, mIconListInfo, items, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mIconList.OnCreateItem = OnCreateIconListItem;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UISprite uISprite = Util.CreateSprite("ListBg", mListCenter.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(8f, 0f, 0f), Vector3.one, false);
		uISprite.SetDimensions(530, 475);
		uISprite.type = UIBasicSprite.Type.Sliced;
	}

	private void OnCreateIconListItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIButton uIButton = null;
		UISprite uISprite = null;
		uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)mIconList.mList.cellWidth;
		uISprite.height = (int)mIconList.mList.cellHeight;
		PartnerListItem partnerListItem = (PartnerListItem)item;
		if (partnerListItem.data == null)
		{
			if (partnerListItem.category != byte.MaxValue)
			{
				PartnerListItem.CreateCategoryRibbon(itemGO, mBaseDepth + 2, partnerListItem.category);
			}
			return;
		}
		string text = "icon_chara_question";
		string imageName = "null";
		int companionLevel = mGame.mPlayer.GetCompanionLevel(partnerListItem.data.index);
		if (partnerListItem.enable)
		{
			text = partnerListItem.data.iconName;
			imageName = ((partnerListItem.data.maxLevel <= companionLevel) ? Def.CharacterIconLevelMaxImageTbl[companionLevel] : Def.CharacterIconLevelImageTbl[companionLevel]);
		}
		uIButton = Util.CreateJellyImageButton("Icon_" + partnerListItem.data.index, itemGO, image2);
		Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 2, Vector3.one, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnPartnerPushedFromList", UIButtonMessage.Trigger.OnClick);
		uIButton.GetComponent<JellyImageButton>().SetBaseScale(0.9f);
		uIButton.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mIconList.mListScrollView;
		uISprite = Util.CreateSprite("Lvl_" + partnerListItem.data.index, uIButton.gameObject, image);
		Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 3, new Vector3(0f, -55f, 0f), Vector3.one, false);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
		ListOpen();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectedPartnerNo = mGame.mCompanionIndex;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void OnPartnerPushedFromList(GameObject a_go)
	{
		if (GetBusy() || mState.GetStatus() != 0 || !(a_go != null))
		{
			return;
		}
		string text = a_go.name;
		if (text.Contains("_"))
		{
			string[] array = text.Split('_');
			if (array.Length > 1)
			{
				int num = int.Parse(array[1]);
				mSelectedPartnerNo = num;
				mGame.PlaySe("SE_POSITIVE", -1);
				mState.Reset(STATE.WAIT, true);
				Close();
			}
		}
	}
}
