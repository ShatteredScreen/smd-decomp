using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using UnityEngine;

public class ResourceDownloadManager : MonoBehaviour
{
	public enum KIND
	{
		MAIN = 0,
		MAIN_STAGE = 1,
		ADV = 2,
		ADV_FIGURE = 3,
		ADV_STAGE = 4
	}

	public enum STATE
	{
		INIT = 0,
		WAIT = 1,
		DOWNLOAD_PREPARE = 2,
		DOWNLOAD_ENQUEUE = 3,
		DOWNLOAD_START = 4,
		DOWNLOAD_FILE = 5,
		DOWNLOAD_REENQUEUE = 6,
		ERROR = 7,
		ERROR_WAIT = 8,
		CANCEL = 9,
		COMPLETE = 10,
		COMPLETE_WAIT = 11
	}

	private class ResourceDLRequest
	{
		public HttpAsync _httpAsync;

		public HttpAsync.ResponseHandler _responseHandler;

		public Action<int> _errorHandler;

		public object _userdata;

		public DateTime _time;

		public int _serverResult;

		public int _threadIndex;
	}

	private class RequestState : IDisposable
	{
		public volatile RegisteredWaitHandle WaitHandle;

		public volatile ManualResetEvent TimeOutHandled;

		public volatile ManualResetEvent ThreadEndWaitHandled;

		private bool mDisposed;

		public int BufferSize { get; set; }

		public byte[] BufferRead { get; set; }

		public HttpWebRequest Request { get; set; }

		public HttpWebResponse Response { get; set; }

		public Stream ResponseStream { get; set; }

		public BIJDLData DownloadInfo { get; set; }

		public int threadIndex { get; set; }

		public string WritePath { get; set; }

		public Stream WriteStream { get; set; }

		public int WriteSize { get; set; }

		public RequestState()
		{
			Request = null;
			ResponseStream = null;
			DownloadInfo = null;
			WritePath = null;
			WriteStream = null;
			WriteSize = 0;
		}

		void IDisposable.Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		~RequestState()
		{
			Dispose(false);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				mDisposed = true;
			}
		}
	}

	private const int POST_MAX = 10;

	private static string[] DLFolder = new string[5] { "DL", "DL_Stg", "DL_Adv", "DL_Advf", "DL_AdvStg" };

	public static int DLmanagerCount = 0;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	public static bool StopOnReadThreadFlag = false;

	public int mTimerValue;

	public float mTimeoutTimer;

	public bool mTimeoutCheck;

	public bool mIsSuspend;

	public bool respawnReq;

	private Dictionary<KIND, BIJDLDataInfo> mLocalDLListDict = new Dictionary<KIND, BIJDLDataInfo>();

	public Dictionary<KIND, ResourceDownloadInfo> mServerListDict = new Dictionary<KIND, ResourceDownloadInfo>();

	private Dictionary<KIND, string> mSavePathDict = new Dictionary<KIND, string>();

	public List<KIND> mKindList = new List<KIND>();

	private KIND mKind;

	private List<BIJDLData> mDLList;

	private Queue<BIJDLData> mDLQueue = new Queue<BIJDLData>();

	private GameMain mGame;

	public int mDLFileNum;

	private int PreDLCount;

	private DateTime DLCountShortTime;

	private Queue<ResourceDLRequest> mRequests = new Queue<ResourceDLRequest>();

	private readonly object mRequestsLock = new object();

	private ManualResetEvent mRequestsNotify = new ManualResetEvent(false);

	private volatile bool mIsRunning;

	private int mThreadNum;

	private Thread[] mThreads;

	private ManualResetEvent[] mThreadLocks;

	private BIJDLData[] mNowProcessingFile;

	public bool[] mThreadProcessing;

	public int mRunningThreadNum;

	private object[] mWriteStreamLock;

	public List<byte[]> mBufferList = new List<byte[]>();

	private readonly object mBufferLock = new object();

	private Queue<ResourceDLRequest> mResponses = new Queue<ResourceDLRequest>();

	private readonly object mResponsesLock = new object();

	private Action<int> m_postHandler = delegate(int a_result)
	{
		if (ResourceDownloadManager.GetDownloadResourceEvent != null)
		{
			ResourceDownloadManager.GetDownloadResourceEvent(a_result, null);
		}
	};

	public int TotalDLCount
	{
		get
		{
			return mDLList.Count;
		}
	}

	public int DLCount { get; private set; }

	public bool IsDownloadFinished { get; private set; }

	public bool IsUnzipFinished { get; private set; }

	public bool IsDLCountShort { get; private set; }

	[method: MethodImpl(32)]
	public static event Action<int, BIJDLData> GetDownloadResourceEvent;

	public static string getResourceDLPath(KIND aKind)
	{
		string aFolderName = DLFolder[(int)aKind];
		return BIJUnity.getResourceDLPath(aFolderName);
	}

	public static KIND getKindWithFileName(string aFileName)
	{
		KIND result = KIND.MAIN;
		if (aFileName.IndexOf("adv_") == 0)
		{
			result = KIND.ADV;
		}
		else if (aFileName.IndexOf("advf_") == 0)
		{
			result = KIND.ADV_FIGURE;
		}
		else if (aFileName.IndexOf("lvl_ADV") == 0)
		{
			result = KIND.ADV_STAGE;
		}
		else if (aFileName.IndexOf("lvl_") == 0)
		{
			result = KIND.MAIN_STAGE;
		}
		return result;
	}

	private void Awake()
	{
		StopOnReadThreadFlag = false;
		foreach (int value in Enum.GetValues(typeof(KIND)))
		{
			string resourceDLPath = getResourceDLPath((KIND)value);
			if (!Directory.Exists(resourceDLPath))
			{
				Directory.CreateDirectory(resourceDLPath);
			}
			mSavePathDict.Add((KIND)value, resourceDLPath);
		}
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Start()
	{
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.DOWNLOAD_PREPARE:
			PrepareDownload(mGame.mOptions.DLThreadNum);
			mState.Change(STATE.DOWNLOAD_ENQUEUE);
			break;
		case STATE.DOWNLOAD_ENQUEUE:
			EnqueueDLList();
			mState.Change(STATE.DOWNLOAD_START);
			break;
		case STATE.DOWNLOAD_REENQUEUE:
			StopOnReadThreadFlag = false;
			ClearProcess();
			mState.Change(STATE.DOWNLOAD_ENQUEUE);
			break;
		case STATE.DOWNLOAD_START:
			StartDownloadThread(0);
			mState.Change(STATE.DOWNLOAD_FILE);
			break;
		case STATE.DOWNLOAD_FILE:
		{
			if (mIsSuspend)
			{
				IsDLCountShort = false;
				PreDLCount = 0;
				DLCountShortTime = DateTime.Now;
				break;
			}
			bool flag = ProcessDownload();
			if (mGame.DLErrorInfo != null)
			{
				mState.Change(STATE.ERROR);
				break;
			}
			ResourceDLRequest resourceDLRequest = null;
			ResourceDLErrorInfo.ERROR_CODE eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.NO_ERROR;
			try
			{
				eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.OTHER;
				lock (mResponsesLock)
				{
					if (mResponses.Count > 0)
					{
						resourceDLRequest = mResponses.Dequeue();
					}
				}
				if (ResourceDLUtils.IsDebugResponseHandlerError())
				{
					throw new Exception("[TEST] Response Callback ERROR!!");
				}
				if (resourceDLRequest != null)
				{
					ResourceDLUtils.log(string.Format("Response(#{0}) - {1}", (resourceDLRequest._httpAsync == null) ? (-1) : resourceDLRequest._httpAsync.SeqNo, ResourceDLUtils.ResultString(resourceDLRequest._serverResult)));
					if (resourceDLRequest._serverResult == 0)
					{
						if (resourceDLRequest._responseHandler != null)
						{
							resourceDLRequest._responseHandler(resourceDLRequest._httpAsync);
						}
						eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.NO_ERROR;
					}
					else if (resourceDLRequest._errorHandler != null)
					{
						resourceDLRequest._errorHandler(resourceDLRequest._serverResult);
					}
					if (resourceDLRequest._httpAsync != null)
					{
						try
						{
							resourceDLRequest._httpAsync.Dispose();
						}
						catch (Exception ex)
						{
							BIJLog.E("Failed : req._httpAsync.Dispose() : " + ex);
						}
					}
				}
				else if (mDLQueue.Count == 0)
				{
					if (PreDLCount == DLCount && !isThreadProcessing())
					{
						long num = DateTimeUtil.BetweenSeconds(DLCountShortTime, DateTime.Now);
						if (num >= 15)
						{
							IsDLCountShort = true;
							mState.Change(STATE.ERROR_WAIT);
						}
					}
					else
					{
						PreDLCount = DLCount;
						DLCountShortTime = DateTime.Now;
					}
				}
			}
			catch (Exception ex2)
			{
				ResourceDLUtils.log("Response failed." + ex2.Message);
				if (resourceDLRequest != null)
				{
					eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
					BIJDLData a_data = resourceDLRequest._userdata as BIJDLData;
					mGame.CreateDLError(a_data, eRROR_CODE, ResourceDLErrorInfo.PLACE.DLMANAGER_MAIN, string.Empty, ex2);
				}
				else
				{
					eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
					mGame.CreateDLError(null, eRROR_CODE, ResourceDLErrorInfo.PLACE.DLMANAGER_MAIN, string.Empty, ex2);
				}
			}
			if (mTimeoutCheck)
			{
				mTimeoutTimer -= Time.deltaTime;
				if (mTimeoutTimer <= 0f)
				{
					StopOnReadThreadFlag = true;
					mGame.CreateDLError(null, ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT_BACK, ResourceDLErrorInfo.PLACE.DLMANAGER_MAIN, string.Empty, string.Empty);
				}
			}
			if (mGame.DLErrorInfo == null)
			{
				if (flag)
				{
					mState.Change(STATE.COMPLETE);
				}
			}
			else
			{
				mState.Change(STATE.ERROR);
			}
			break;
		}
		case STATE.COMPLETE:
			IsDownloadFinished = true;
			mState.Change(STATE.COMPLETE_WAIT);
			break;
		case STATE.ERROR:
			mState.Change(STATE.ERROR_WAIT);
			break;
		}
		mState.Update();
	}

	private void OnDestroy()
	{
		ResourceDLUtils.dbg1("ResourceDLManager Destroy");
		if (ResourceDownloadManager.GetDownloadResourceEvent != null)
		{
			ResourceDownloadManager.GetDownloadResourceEvent = (Action<int, BIJDLData>)Delegate.Remove(ResourceDownloadManager.GetDownloadResourceEvent, new Action<int, BIJDLData>(ResourceDownloadManager_OnGetResourceDownload));
		}
		StopDLThread();
		StopOnReadThreadFlag = true;
	}

	public void ClearBuffer()
	{
		lock (mBufferLock)
		{
			while (mBufferList.Count > 0)
			{
				mBufferList.RemoveAt(0);
			}
		}
	}

	private void StopDLThread()
	{
		mIsRunning = false;
		mRequestsNotify.Set();
		if (mThreads != null)
		{
			for (int i = 0; i < mThreads.Length; i++)
			{
				int num = 0;
				Thread thread = mThreads[i];
				if (thread == null)
				{
					continue;
				}
				if (thread.IsAlive)
				{
					do
					{
						thread.Join(100);
					}
					while (thread.IsAlive && num++ < 3);
				}
				if (thread.IsAlive)
				{
					thread.Abort();
				}
			}
		}
		lock (mRequestsLock)
		{
			foreach (ResourceDLRequest mRequest in mRequests)
			{
				mRequest._httpAsync.Abort();
			}
			mRequests.Clear();
		}
		mThreads = null;
	}

	public void EndDLThread()
	{
		mIsRunning = false;
		mRequestsNotify.Set();
		mThreads = null;
	}

	public void ClearRequests()
	{
		lock (mRequestsLock)
		{
			foreach (ResourceDLRequest mRequest in mRequests)
			{
				mRequest._httpAsync.Abort();
			}
			mRequests.Clear();
		}
		lock (mResponsesLock)
		{
			mResponses.Clear();
		}
	}

	private void OnApplicationPause(bool a_pauseStatus)
	{
		if (!a_pauseStatus)
		{
		}
	}

	public void Init(KIND aKind)
	{
		mDLList = new List<BIJDLData>();
		DLCount = 0;
		IsDownloadFinished = false;
		DeleteNotListedFiles(aKind);
		ResourceDLUtils.dbg2("Init");
	}

	public void DeleteNotListedFiles(KIND aKind)
	{
		string[] files = Directory.GetFiles(mSavePathDict[aKind]);
		foreach (string text in files)
		{
			string text2 = text.Substring(text.LastIndexOf("/") + 1);
			if (!(text2 == Constants.RESOURCEDL_SERVERFILE) && !(text2 == Constants.RESOURCEDL_LOCALFILE_OLD) && !(text2 == Constants.RESOURCEDL_LOCALFILE))
			{
				bool flag = true;
				ResourceDownloadInfo resourceDownloadInfo = mServerListDict[aKind];
				if (resourceDownloadInfo.mDict.ContainsKey(text2))
				{
					flag = false;
				}
				if (flag && File.Exists(text))
				{
					File.Delete(text);
				}
			}
		}
	}

	public void LoadLocalDLFilelist(KIND aKind, string a_filename)
	{
		if (!File.Exists(a_filename))
		{
			return;
		}
		try
		{
			using (BIJEncryptDataReader a_reader = new BIJEncryptDataReader(a_filename))
			{
				if (mServerListDict.ContainsKey(aKind))
				{
					mServerListDict.Remove(aKind);
				}
				ResourceDownloadInfo resourceDownloadInfo = new ResourceDownloadInfo();
				resourceDownloadInfo.Deserialize(a_reader);
				mServerListDict.Add(aKind, resourceDownloadInfo);
			}
		}
		catch (Exception ex)
		{
			ResourceDLUtils.dbg1("!!! Exception !!! in ParseDLList _ex=" + ex, true);
		}
	}

	public void ClearResourceDLInfo(KIND aKind)
	{
		if (mServerListDict.ContainsKey(aKind))
		{
			mServerListDict.Remove(aKind);
		}
	}

	public void LoadLocalDataList(KIND aKind)
	{
		ResourceDLUtils.dbg2("Load Local DL Data List");
		if (mLocalDLListDict.ContainsKey(aKind))
		{
			mLocalDLListDict.Remove(aKind);
		}
		string text = mSavePathDict[aKind];
		string text2 = text + Constants.RESOURCEDL_LOCALFILE;
		string text3 = text2;
		int a_version = 1;
		bool flag = false;
		if (!File.Exists(text2))
		{
			text3 = text + Constants.RESOURCEDL_LOCALFILE_OLD;
			flag = true;
		}
		BIJDLDataInfo bIJDLDataInfo = new BIJDLDataInfo(text2, a_version);
		bool flag2 = false;
		if (File.Exists(text2))
		{
			flag2 = bIJDLDataInfo.Load(flag, text3);
		}
		if (!flag2)
		{
			ResourceDLUtils.dbg1("Local DLFile Read Failed!!", true);
			bIJDLDataInfo.Clear();
		}
		if (flag && bIJDLDataInfo.Save())
		{
			File.Delete(text3);
		}
		mLocalDLListDict.Add(aKind, bIJDLDataInfo);
	}

	public BIJBinaryReader GetDownloadFileReadStream(string filename)
	{
		if (!GameMain.USE_RESOURCEDL)
		{
			return null;
		}
		string text = filename;
		if (text.LastIndexOf(".") > 0 && !text.Contains(".bytes"))
		{
			text += ".bytes";
		}
		KIND kindWithFileName = getKindWithFileName(text);
		if (!mServerListDict.ContainsKey(kindWithFileName))
		{
			return null;
		}
		bool flag = false;
		ResourceDownloadInfo resourceDownloadInfo = mServerListDict[kindWithFileName];
		if (resourceDownloadInfo.mDict.ContainsKey(text))
		{
			flag = true;
		}
		if (!flag)
		{
			return null;
		}
		text = mSavePathDict[kindWithFileName] + text;
		if (!File.Exists(text))
		{
			return null;
		}
		return new BIJEncryptDataReader(text);
	}

	public byte[] GetBytesDownloadFile(string filename)
	{
		BIJBinaryReader downloadFileReadStream = GetDownloadFileReadStream(filename);
		if (downloadFileReadStream == null)
		{
			return null;
		}
		byte[] result = downloadFileReadStream.ReadAll();
		downloadFileReadStream.Close();
		return result;
	}

	public string GetStringDownloadFile(string filename)
	{
		byte[] bytesDownloadFile = GetBytesDownloadFile(filename);
		if (bytesDownloadFile == null)
		{
			return null;
		}
		return Encoding.UTF8.GetString(bytesDownloadFile);
	}

	public bool IsDownloadFileExist(string filename)
	{
		string text = filename;
		if (text.LastIndexOf(".") > 0 && !text.Contains(".bytes"))
		{
			text += ".bytes";
		}
		KIND kindWithFileName = getKindWithFileName(text);
		text = mSavePathDict[kindWithFileName] + text;
		if (!File.Exists(text))
		{
			return false;
		}
		return true;
	}

	public bool IsInDownloadFileList(string filename)
	{
		string text = filename;
		if (text.LastIndexOf(".") > 0 && !text.Contains(".bytes"))
		{
			text += ".bytes";
		}
		KIND kindWithFileName = getKindWithFileName(text);
		ResourceDownloadInfo value = null;
		if (mServerListDict.TryGetValue(kindWithFileName, out value) && value.mDict.ContainsKey(filename))
		{
			return true;
		}
		return false;
	}

	public string GetDownloadFilePath(KIND aKind, string filename)
	{
		string text = filename;
		if (text.LastIndexOf(".") > 0 && !text.Contains(".bytes"))
		{
			text += ".bytes";
		}
		return mSavePathDict[aKind] + text;
	}

	public string GetDownloadFileURL(KIND aKind, string filename)
	{
		string text = filename;
		if (text.LastIndexOf(".") > 0 && !text.Contains(".bytes"))
		{
			text += ".bytes";
		}
		string result = string.Empty;
		ResourceDownloadInfo value = null;
		if (mServerListDict.TryGetValue(aKind, out value))
		{
			BIJDLData value2 = null;
			if (value.mDict.TryGetValue(filename, out value2))
			{
				result = value.mBaseURL + value2.URL;
			}
		}
		return result;
	}

	public void LoadLocalDLData(bool exceptDLTarget = false)
	{
		foreach (int value in Enum.GetValues(typeof(KIND)))
		{
			if (!exceptDLTarget || !mKindList.Contains((KIND)value))
			{
				string resourceDLPath = getResourceDLPath((KIND)value);
				LoadLocalDLFilelist((KIND)value, resourceDLPath + Constants.RESOURCEDL_SERVERFILE);
				LoadLocalDataList((KIND)value);
			}
		}
	}

	public void LoadDownloadFiles()
	{
		ResourceManager.LoadAssetBundleList();
		LoadUpdateFiles();
	}

	public void LoadUpdateFiles()
	{
		Res.LoadDownloadResourceDictionaty();
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		instance.ReloadAccessoryData();
		instance.ReloadCompanionData();
		instance.ReloadSkillEffectData();
		instance.ReloadStoryDemoData();
		instance.ReloadVisualCollectionData();
		instance.LoadDownloadMapPageData();
		instance.LoadDownloadShopData();
		instance.LoadDownloadTrophyExchangeData();
		instance.LoadDownloadEventData();
		instance.LoadDownloadSpecialDailyData();
		instance.LoadDownloadAlternateRewardData();
		MissionManager.UpdateMissionData(instance.mDLManager);
	}

	public bool CheckDLFileIntegrity(bool exceptDLTarget = false, bool checkStage = false)
	{
		bool flag = true;
		foreach (int value in Enum.GetValues(typeof(KIND)))
		{
			if ((!exceptDLTarget || !mKindList.Contains((KIND)value)) && (value != 1 || checkStage))
			{
				bool flag2 = false;
				flag2 = ((value != 1) ? CheckDLFileIntegrity((KIND)value) : CheckDLFileNum((KIND)value));
				flag = flag && flag2;
				if (!flag2 && !mKindList.Contains((KIND)value))
				{
					mKindList.Add((KIND)value);
				}
			}
		}
		return flag;
	}

	private bool CheckDLFileIntegrity(KIND aKind)
	{
		bool result = true;
		ResourceDownloadInfo value = null;
		if (mServerListDict.TryGetValue(aKind, out value))
		{
			List<BIJDLData> list = new List<BIJDLData>(value.mDict.Values);
			for (int i = 0; i < list.Count; i++)
			{
				BIJDLData bIJDLData = list[i];
				string text = getResourceDLPath(aKind) + bIJDLData.BaseName;
				if (File.Exists(text))
				{
					FileInfo fileInfo = new FileInfo(text);
					if (fileInfo.Length != bIJDLData.DataSize)
					{
						result = false;
						File.Delete(text);
					}
				}
				else
				{
					result = false;
				}
			}
		}
		else
		{
			result = false;
		}
		return result;
	}

	public bool CheckDLFileIntegrity(KIND aKind, string aFileName, bool checkCRC = false)
	{
		bool result = true;
		ResourceDownloadInfo value = null;
		if (mServerListDict.TryGetValue(aKind, out value))
		{
			BIJDLData value2 = null;
			if (value.mDict.TryGetValue(aFileName, out value2))
			{
				string text = getResourceDLPath(aKind) + value2.BaseName;
				if (File.Exists(text))
				{
					FileInfo fileInfo = new FileInfo(text);
					if (fileInfo.Length != value2.DataSize)
					{
						result = false;
						File.Delete(text);
					}
					else if (checkCRC)
					{
						string fileChecksum = BIJIOUtils.GetFileChecksum(text);
						if (value2.CRC != fileChecksum)
						{
							result = false;
							File.Delete(text);
						}
					}
				}
				else
				{
					result = false;
				}
			}
			else
			{
				result = false;
			}
		}
		else
		{
			result = false;
		}
		return result;
	}

	private bool CheckDLFileNum(KIND aKind)
	{
		bool result = true;
		ResourceDownloadInfo value = null;
		if (mServerListDict.TryGetValue(aKind, out value))
		{
			int num = Directory.GetFiles(mSavePathDict[aKind]).Length;
			if (num - 2 < value.mDict.Count)
			{
				result = false;
			}
		}
		else
		{
			result = false;
		}
		return result;
	}

	public void RegisterKindListAll()
	{
		foreach (int value in Enum.GetValues(typeof(KIND)))
		{
			if (!mKindList.Contains((KIND)value))
			{
				mKindList.Add((KIND)value);
			}
		}
	}

	public IEnumerator RegisterFileDataToDownload(KIND aKind)
	{
		ResourceDownloadInfo serverList = mServerListDict[aKind];
		BIJDLDataInfo localDLList = mLocalDLListDict[aKind];
		string dlPath = getResourceDLPath(aKind);
		bool needSave = false;
		List<BIJDLData> valsList = new List<BIJDLData>(serverList.mDict.Values);
		DateTime startTime = DateTime.Now;
		for (int i = 0; i < valsList.Count; i++)
		{
			BIJDLData data = valsList[i];
			if (!localDLList.HasAlreadyDL(dlPath, data))
			{
				needSave = true;
				data.DLDataStats = BIJDLData.STATS.NON;
				if (!mDLList.Contains(data))
				{
					mDLList.Add(data);
				}
			}
			long span = DateTimeUtil.BetweenMilliseconds(startTime, DateTime.Now);
			if (span > 30)
			{
				yield return null;
				startTime = DateTime.Now;
			}
		}
		if (needSave)
		{
			localDLList.Save();
		}
	}

	public string GetAssetBundleDictionaryFileName(KIND aKind)
	{
		string empty = string.Empty;
		string[] array = new string[5]
		{
			"AssetBundleDictionary.bin.bytes",
			string.Empty,
			"adv_AssetBundleDictionary.bin.bytes",
			"advf_AssetBundleDictionary.bin.bytes",
			string.Empty
		};
		return array[(int)aKind];
	}

	public string GetResourceDictDownloadFileName(KIND aKind)
	{
		string result = string.Empty;
		string[] array = new string[5]
		{
			"ResourceDictDownload.bin.bytes",
			string.Empty,
			"adv_ResourceDictDownload.bin.bytes",
			"advf_ResourceDictDownload.bin.bytes",
			string.Empty
		};
		string text = array[(int)aKind];
		if (!string.IsNullOrEmpty(text))
		{
			ResourceDownloadInfo value = null;
			if (mServerListDict.TryGetValue(aKind, out value) && value.mDict.ContainsKey(text))
			{
				result = text;
			}
		}
		return result;
	}

	public bool isNeedDownload()
	{
		bool result = false;
		if (mDLList.Count > 0)
		{
			ResourceDLUtils.dbg2("Go to DOWNLOAD!!!");
			result = true;
		}
		return result;
	}

	public int getNeedDownloadNum()
	{
		return mDLList.Count;
	}

	public int getNeedDownloadSize()
	{
		int num = 0;
		for (int i = 0; i < mDLList.Count; i++)
		{
			num += mDLList[i].DataSize;
		}
		return num;
	}

	public void DoDownload(KIND aKind)
	{
		mKind = aKind;
		IsDLCountShort = false;
		PreDLCount = 0;
		DLCountShortTime = DateTime.Now;
		mState.Change(STATE.DOWNLOAD_PREPARE);
	}

	public bool ProcessDownload()
	{
		BIJDLData bIJDLData = null;
		int num = -1;
		if (mDLQueue.Count == 0)
		{
			if (DLCount >= TotalDLCount)
			{
				ResourceDLUtils.dbg2(string.Format("Finish DL ( {0} / {1} ) !!!!!", DLCount, TotalDLCount));
				return true;
			}
			return false;
		}
		int num2 = 0;
		lock (mRequestsLock)
		{
			num2 = mRequests.Count;
		}
		if (ResourceDLUtils.IsDebugPostCountToMAX())
		{
			ResourceDLUtils.err("Server Requests are overflow FOR TEST!");
			return false;
		}
		if (num2 > 10)
		{
			ResourceDLUtils.log("Server Requests are overflow! " + num2);
			return false;
		}
		int num3 = -1;
		for (int i = 0; i < mNowProcessingFile.Length; i++)
		{
			if (mNowProcessingFile[i] == null)
			{
				num3 = i;
				break;
			}
		}
		if (num3 == -1)
		{
			return false;
		}
		ResourceDLUtils.dbg2("empty slot is #" + num);
		BIJDLData[] array = mDLQueue.ToArray();
		if (array[0] != null)
		{
			double freeDiskSpace = BIJUnity.getFreeDiskSpace(BIJUnity.getDataPath());
			if ((long)array[0].DataSize * 2L > (long)freeDiskSpace)
			{
				mGame.CreateDLError(null, ResourceDLErrorInfo.ERROR_CODE.LOW_DISK_SPACE, ResourceDLErrorInfo.PLACE.DLMANAGER_MAIN, string.Empty, string.Empty);
				return false;
			}
		}
		bIJDLData = mDLQueue.Dequeue();
		if (bIJDLData == null)
		{
			ResourceDLUtils.err("null data in DL Queue.");
			return false;
		}
		if (mTimerValue > 0)
		{
			mTimeoutCheck = true;
			mTimeoutTimer = mTimerValue;
		}
		else
		{
			mTimeoutCheck = false;
			mTimeoutTimer = 0f;
		}
		bIJDLData.DLDataStats = BIJDLData.STATS.DL_WAIT;
		Dictionary<string, object> p = new Dictionary<string, object>();
		p["t"] = DateTime.Now.ToString("yyyyMMdd");
		p["q"] = ((!(bIJDLData.Query == string.Empty)) ? bIJDLData.Query : "1");
		HttpAsync a_httpAsync = Network.CreateHttpGet(mServerListDict[mKind].mBaseURL + bIJDLData.URL, ref p, Server.Timeout, false);
		if (a_httpAsync == null)
		{
			BIJLog.E("CreateHttpPostAsync ERROR");
			respawnReq = true;
			mGame.CreateDLError(bIJDLData, ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT_FATAL, ResourceDLErrorInfo.PLACE.DLMANAGER_MAIN, string.Empty, string.Empty);
			return false;
		}
		if (PostRequest(ref a_httpAsync, m_postHandler, OnResponseCallback, bIJDLData))
		{
			ResourceDLUtils.dbg2("set to processing file :" + bIJDLData.BaseName);
			mNowProcessingFile[num3] = bIJDLData;
		}
		else
		{
			ResourceDLUtils.dbg2("failed to post request.");
			mGame.CreateDLError(bIJDLData, ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS, ResourceDLErrorInfo.PLACE.DLMANAGER_MAIN, string.Empty, string.Empty);
		}
		return false;
	}

	public void InitMultiThreadDisable()
	{
		mRunningThreadNum = 0;
	}

	public void PrepareDownload(int a_thread_num)
	{
		ResourceDLUtils.dbg2("prepare download.   thread num=" + a_thread_num);
		lock (mBufferLock)
		{
			if (mBufferList.Count == 0)
			{
				for (int i = 0; i < 3; i++)
				{
					mBufferList.Add(new byte[4096]);
				}
			}
		}
		if (ResourceDownloadManager.GetDownloadResourceEvent == null)
		{
			ResourceDLUtils.dbg2("add event : ResourceDownloadManager_OnGetResourceDownload;");
			ResourceDownloadManager.GetDownloadResourceEvent = (Action<int, BIJDLData>)Delegate.Combine(ResourceDownloadManager.GetDownloadResourceEvent, new Action<int, BIJDLData>(ResourceDownloadManager_OnGetResourceDownload));
		}
		else
		{
			ResourceDLUtils.dbg2("event has already added.:" + ResourceDownloadManager.GetDownloadResourceEvent.ToString());
		}
		if (mIsRunning)
		{
			return;
		}
		if (mThreadNum < a_thread_num)
		{
			mThreads = null;
			mNowProcessingFile = null;
			mThreadLocks = null;
			mWriteStreamLock = null;
			mThreadProcessing = null;
		}
		mThreadNum = a_thread_num;
		mRunningThreadNum = 0;
		if (mThreads == null)
		{
			mThreads = new Thread[mThreadNum];
		}
		if (mNowProcessingFile == null)
		{
			mNowProcessingFile = new BIJDLData[mThreadNum];
		}
		if (mThreadLocks == null)
		{
			mThreadLocks = new ManualResetEvent[mThreadNum];
		}
		if (mWriteStreamLock == null)
		{
			mWriteStreamLock = new object[mThreadNum];
		}
		if (mThreadProcessing == null)
		{
			mThreadProcessing = new bool[mThreadNum];
		}
		for (int j = 0; j < mThreads.Length; j++)
		{
			if (mThreads[j] == null)
			{
				mThreads[j] = new Thread(_ThreadMain);
			}
			if (mWriteStreamLock[j] == null)
			{
				mWriteStreamLock[j] = new object();
			}
			mThreadLocks[j] = null;
			mNowProcessingFile[j] = null;
			mThreadProcessing[j] = false;
		}
		mIsRunning = true;
	}

	public void ClearProcess()
	{
		mNowProcessingFile = null;
	}

	public bool isThreadProcessing()
	{
		if (mThreadProcessing == null)
		{
			return false;
		}
		bool flag = false;
		for (int i = 0; i < mThreadProcessing.Length; i++)
		{
			flag |= mThreadProcessing[i];
		}
		return flag;
	}

	public void EnqueueDLList()
	{
		ResourceDLUtils.dbg2("EnqueueDLList");
		mDLQueue.Clear();
		for (int i = 0; i < mDLList.Count; i++)
		{
			BIJDLData bIJDLData = mDLList[i];
			if ((bIJDLData.DLDataStats & BIJDLData.STATS.DL) == 0)
			{
				mDLQueue.Enqueue(mDLList[i]);
			}
		}
	}

	public void StartDownloadThread(int i)
	{
		ResourceDLUtils.dbg2("StartDownloadThread");
		if (mThreads != null && i < mThreads.Length && !mThreads[i].IsAlive)
		{
			object[] parameter = new object[1] { i };
			mThreads[i].Start(parameter);
			mRunningThreadNum++;
		}
	}

	private int GetStreamingWaitTimeMillis()
	{
		return 60000;
	}

	private void OnResponseCallback(HttpAsync _httpAsync)
	{
		ResourceDLErrorInfo.ERROR_CODE a_code = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
		BIJDLData bIJDLData = null;
		try
		{
			bIJDLData = ((ResourceDLRequest)_httpAsync.UserData)._userdata as BIJDLData;
			if (_httpAsync.Result != HttpAsync.RESULT.SUCCESS)
			{
				if (_httpAsync != null && _httpAsync.Response != null)
				{
					try
					{
						_httpAsync.Response.Close();
					}
					catch (Exception ex)
					{
						BIJLog.E("Failed : _httpAsync.Response.Close() : " + ex);
					}
				}
				a_code = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
				if (_httpAsync.Result == HttpAsync.RESULT.TIMEOUT)
				{
					a_code = ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT;
				}
				mGame.CreateDLError(bIJDLData, a_code, ResourceDLErrorInfo.PLACE.DLMANAGER_ONRESPONSECALLBACK, string.Empty, string.Empty);
				return;
			}
			RequestState requestState = new RequestState();
			requestState.Request = _httpAsync.Request as HttpWebRequest;
			requestState.Response = _httpAsync.Response as HttpWebResponse;
			requestState.DownloadInfo = bIJDLData;
			requestState.threadIndex = ((ResourceDLRequest)_httpAsync.UserData)._threadIndex;
			Stream stream = (requestState.ResponseStream = requestState.Response.GetResponseStream());
			if (StopOnReadThreadFlag)
			{
				a_code = ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT_BACK;
				throw new Exception("CANCELED: " + bIJDLData.BaseName);
			}
			requestState.TimeOutHandled = new ManualResetEvent(false);
			requestState.TimeOutHandled.Reset();
			requestState.ThreadEndWaitHandled = new ManualResetEvent(true);
			requestState.ThreadEndWaitHandled.Set();
			lock (mBufferLock)
			{
				if (mBufferList.Count == 0)
				{
					mBufferList.Add(new byte[4096]);
				}
				requestState.BufferRead = mBufferList[0];
				requestState.BufferSize = requestState.BufferRead.Length;
				mBufferList.RemoveAt(0);
			}
			IAsyncResult asyncResult = stream.BeginRead(requestState.BufferRead, 0, requestState.BufferSize, OnReadCallback, requestState);
			if (asyncResult == null)
			{
				throw new Exception("Could not start an asynchronous reading for: " + bIJDLData.BaseName);
			}
			Stopwatch stopwatch = Stopwatch.StartNew();
			while (!requestState.TimeOutHandled.WaitOne(100))
			{
				stopwatch.Stop();
				if (stopwatch.ElapsedMilliseconds > GetStreamingWaitTimeMillis())
				{
					a_code = ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT;
					throw new Exception("Timeout is occured in response of: " + bIJDLData.BaseName);
				}
				stopwatch.Start();
			}
		}
		catch (Exception a_e)
		{
			if (_httpAsync != null && _httpAsync.Response != null)
			{
				try
				{
					_httpAsync.Response.Close();
				}
				catch (Exception ex2)
				{
					BIJLog.E("Failed : _httpAsync.Response.Close() : " + ex2);
				}
			}
			mGame.CreateDLError(bIJDLData, a_code, ResourceDLErrorInfo.PLACE.DLMANAGER_ONRESPONSECALLBACK, string.Empty, a_e);
		}
	}

	private void OnReadCallback(IAsyncResult result)
	{
		ResourceDLErrorInfo.ERROR_CODE a_code = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
		RequestState requestState = null;
		BIJDLData bIJDLData = null;
		int num = -1;
		try
		{
			requestState = (RequestState)result.AsyncState;
			num = requestState.threadIndex;
			requestState.TimeOutHandled.Set();
			while (!requestState.ThreadEndWaitHandled.WaitOne(100))
			{
				ResourceDLUtils.dbg2("TID = " + Thread.CurrentThread.ManagedThreadId + " : state.ThreadEndWaitHandled.WaitOne");
			}
			if (requestState.ResponseStream != null)
			{
				bIJDLData = requestState.DownloadInfo;
				int num2 = requestState.ResponseStream.EndRead(result);
				if (num2 > 0)
				{
					lock (mWriteStreamLock[num])
					{
						if (requestState.WriteStream == null)
						{
							string text = Path.Combine(mSavePathDict[mKind], bIJDLData.BaseName);
							if (!Directory.Exists(Path.GetDirectoryName(text)))
							{
								Directory.CreateDirectory(Path.GetDirectoryName(text));
							}
							requestState.WritePath = text;
							ResourceDLUtils.dbg2(string.Format("Write Path Create := " + requestState.WritePath));
							requestState.WriteStream = new FileStream(requestState.WritePath, FileMode.Create);
						}
						a_code = ResourceDLErrorInfo.ERROR_CODE.MEMORY_ACCESS;
						requestState.WriteStream.Write(requestState.BufferRead, 0, num2);
						lock (mBufferLock)
						{
							mBufferList.Add(requestState.BufferRead);
							requestState.BufferRead = null;
							requestState.BufferSize = 0;
						}
						a_code = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
						requestState.WriteSize += num2;
					}
					float num3 = (float)requestState.WriteSize / (float)bIJDLData.DataSize;
					if (StopOnReadThreadFlag)
					{
						a_code = ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT_BACK;
						throw new Exception("CANCELED: " + bIJDLData.BaseName);
					}
					requestState.TimeOutHandled.Reset();
					lock (mBufferLock)
					{
						if (mBufferList.Count == 0)
						{
							mBufferList.Add(new byte[4096]);
						}
						requestState.BufferRead = mBufferList[0];
						requestState.BufferSize = requestState.BufferRead.Length;
						mBufferList.RemoveAt(0);
					}
					IAsyncResult asyncResult = requestState.ResponseStream.BeginRead(requestState.BufferRead, 0, requestState.BufferSize, OnReadCallback, requestState);
					ResourceDLUtils.dbg2("TID = " + Thread.CurrentThread.ManagedThreadId + " : After BeginRead");
					requestState.ThreadEndWaitHandled.Reset();
					if (asyncResult == null)
					{
						throw new Exception("Could not start an asynchronous reading for: " + bIJDLData.BaseName);
					}
					Stopwatch stopwatch = Stopwatch.StartNew();
					while (!requestState.TimeOutHandled.WaitOne(100))
					{
						stopwatch.Stop();
						if (stopwatch.ElapsedMilliseconds > GetStreamingWaitTimeMillis())
						{
							a_code = ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT;
							throw new Exception("Timeout is occured in response of: " + bIJDLData.BaseName);
						}
						stopwatch.Start();
					}
				}
				else
				{
					if (requestState.BufferRead != null)
					{
						lock (mBufferLock)
						{
							mBufferList.Add(requestState.BufferRead);
							requestState.BufferRead = null;
							requestState.BufferSize = 0;
						}
					}
					lock (mWriteStreamLock[num])
					{
						if (requestState.WriteStream != null)
						{
							try
							{
								requestState.WriteStream.Flush();
								requestState.WriteStream.Close();
							}
							catch (Exception ex)
							{
								BIJLog.E("Failed : state.WriteStream.Flush() or Close() : " + ex);
							}
							requestState.WriteStream = null;
						}
					}
					FileInfo fileInfo = new FileInfo(requestState.WritePath);
					if (bIJDLData.DataSize > 0 && fileInfo.Length != bIJDLData.DataSize)
					{
						throw new Exception("File size miss match : " + ((bIJDLData != null) ? bIJDLData.BaseName : string.Empty));
					}
					if (!string.IsNullOrEmpty(bIJDLData.CRC))
					{
						string fileChecksum = BIJIOUtils.GetFileChecksum(requestState.WritePath, Constants.RESOURCEDL_CHECKSUM_BUFFSIZ * 1024);
						if (string.Compare(fileChecksum, bIJDLData.CRC, StringComparison.OrdinalIgnoreCase) != 0)
						{
							throw new Exception("File checksum miss match : " + ((bIJDLData != null) ? bIJDLData.BaseName : string.Empty));
						}
					}
					if (ResourceDownloadManager.GetDownloadResourceEvent != null)
					{
						ResourceDLUtils.dbg2("Go to callback !!");
						ResourceDownloadManager.GetDownloadResourceEvent(0, bIJDLData);
					}
					if (requestState.ResponseStream != null)
					{
						try
						{
							requestState.ResponseStream.Close();
						}
						catch (Exception ex2)
						{
							BIJLog.E("Failed : state.ResponseStream.Close() : " + ex2);
						}
						requestState.ResponseStream = null;
					}
					if (requestState.Response != null)
					{
						try
						{
							requestState.Response.Close();
						}
						catch (Exception ex3)
						{
							BIJLog.E("Failed : state.Response.Close() : " + ex3);
						}
						requestState.Response = null;
					}
				}
			}
		}
		catch (Exception ex4)
		{
			BIJLog.E("TID =" + Thread.CurrentThread.ManagedThreadId + " : OnReadCallback : " + ex4);
			if (requestState != null)
			{
				lock (mWriteStreamLock[num])
				{
					if (requestState.WriteStream != null)
					{
						try
						{
							requestState.WriteStream.Close();
						}
						catch (Exception ex5)
						{
							BIJLog.E("Failed : state.WriteStream.Close() : " + ex5);
						}
						requestState.WriteStream = null;
					}
				}
				if (!string.IsNullOrEmpty(requestState.WritePath) && File.Exists(requestState.WritePath))
				{
					try
					{
						BIJLog.E("Broken File! Delete : " + requestState.WritePath);
						File.Delete(requestState.WritePath);
						BIJLog.E("Broken File! Delete : SUCCESS");
					}
					catch (Exception ex6)
					{
						BIJLog.E("Failed : File.Delete(state.WritePath) Broken File! Delete : " + ex6);
					}
				}
				if (requestState.ResponseStream != null)
				{
					try
					{
						requestState.ResponseStream.Close();
					}
					catch (Exception ex7)
					{
						BIJLog.E("Failed : state.ResponseStream.Close() : " + ex7);
					}
					requestState.ResponseStream = null;
				}
				if (requestState.Response != null)
				{
					try
					{
						requestState.Response.Close();
					}
					catch (Exception ex8)
					{
						BIJLog.E("Failed : state.ResponseStream.Close() : " + ex8);
					}
					requestState.Response = null;
				}
			}
			if (mGame.DLErrorInfo == null)
			{
				mGame.CreateDLError(bIJDLData, a_code, ResourceDLErrorInfo.PLACE.DLMANAGER_ONREADCALLBACK, string.Empty, ex4);
			}
		}
		requestState.ThreadEndWaitHandled.Set();
	}

	private void ResourceDownloadManager_OnGetResourceDownload(int a_result, BIJDLData a_data)
	{
		if (a_data != null)
		{
		}
		bool flag = false;
		ResourceDLErrorInfo.ERROR_CODE eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.NO_ERROR;
		switch (a_result)
		{
		case 0:
			ResourceDLUtils.dbg2("Event SUCCESS !!");
			flag = true;
			break;
		case 1:
			ResourceDLUtils.dbg2("Event ERROR !!");
			eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
			goto IL_00a8;
		case 3:
			ResourceDLUtils.dbg2("Event TIMEOUT !!");
			eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.CONNECTION_TIMEOUT;
			goto IL_00a8;
		case 4:
			ResourceDLUtils.dbg2("Event CANCEL !!");
			eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.CONNECTION_CANCELED;
			goto IL_00a8;
		default:
			{
				ResourceDLUtils.dbg2("Event Result Other : " + a_result);
				eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.SERVER_ACCESS;
				goto IL_00a8;
			}
			IL_00a8:
			if (eRROR_CODE != 0)
			{
				if (a_data != null)
				{
					mGame.CreateDLError(a_data, eRROR_CODE, ResourceDLErrorInfo.PLACE.DLMANAGER_ONGETRESOURCEDOWNLOAD, string.Empty, string.Empty);
				}
				else
				{
					mGame.CreateDLError(null, eRROR_CODE, ResourceDLErrorInfo.PLACE.DLMANAGER_ONGETRESOURCEDOWNLOAD, string.Empty, string.Empty);
				}
			}
			break;
		}
		for (int i = 0; i < mNowProcessingFile.Length; i++)
		{
			BIJDLData bIJDLData = mNowProcessingFile[i];
			if (bIJDLData == a_data)
			{
				mNowProcessingFile[i] = null;
			}
		}
		if (flag)
		{
			ResourceDLUtils.dbg2("DOWNLOAD SUCCESS !!");
			a_data.DLDataStats = BIJDLData.STATS.DL;
			BIJDLDataInfo bIJDLDataInfo = mLocalDLListDict[mKind];
			bIJDLDataInfo.UpdateDLData(a_data);
			bIJDLDataInfo.Save();
			DLCount++;
			mDLFileNum++;
		}
		else
		{
			ResourceDLUtils.dbg2("DOWNLOAD FAILED !!");
			if (a_data != null)
			{
				BIJLog.E("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				a_data.DLDataStats = BIJDLData.STATS.ERROR;
			}
		}
	}

	private int GetRequestsWaitTimeMillis()
	{
		return -1;
	}

	private int GetResponsesWaitTimeMillis()
	{
		return 10;
	}

	private void _ThreadMain(object args)
	{
		object[] array = (object[])args;
		int _threadIndex = (int)array[0];
		int num = -1;
		if (_threadIndex < mThreads.Length - 1)
		{
			num = _threadIndex + 1;
		}
		while (mIsRunning)
		{
			ResourceDLErrorInfo.ERROR_CODE eRROR_CODE = ResourceDLErrorInfo.ERROR_CODE.NO_ERROR;
			ResourceDLErrorInfo.ERROR_CODE eRROR_CODE2 = ResourceDLErrorInfo.ERROR_CODE.NO_ERROR;
			try
			{
				while (!mRequestsNotify.WaitOne(GetRequestsWaitTimeMillis()))
				{
				}
				if (!mIsRunning)
				{
					break;
				}
				mRequestsNotify.Reset();
				ResourceDLUtils.dbg2(string.Format("_threadmain {0} =======================", 0));
				ResourceDLRequest resourceDLRequest = null;
				do
				{
					resourceDLRequest = null;
					lock (mRequestsLock)
					{
						if (mGame.DLErrorInfo == null && !mIsSuspend && mRequests.Count > 0)
						{
							resourceDLRequest = mRequests.Dequeue();
						}
					}
					if (resourceDLRequest != null)
					{
						mThreadProcessing[_threadIndex] = true;
						BIJDLData bIJDLData = (BIJDLData)resourceDLRequest._userdata;
						ResourceDLUtils.dbg2("req is exist!!!");
						try
						{
							mThreadLocks[_threadIndex] = new ManualResetEvent(false);
							resourceDLRequest._time = DateTime.Now;
							resourceDLRequest._threadIndex = _threadIndex;
							resourceDLRequest._httpAsync.GetResponse(delegate(HttpAsync httpAsync)
							{
								ResourceDLRequest resourceDLRequest2 = httpAsync.UserData as ResourceDLRequest;
								if (resourceDLRequest2 != null)
								{
									ResourceDLUtils.dbg2(string.Format("(#{0}): Server Result={1}    {2} ({3}sec)", resourceDLRequest2._httpAsync.SeqNo, resourceDLRequest2._httpAsync.Result, resourceDLRequest2._httpAsync.Request.RequestUri, (DateTime.Now - resourceDLRequest2._time).TotalMilliseconds / 1000.0));
								}
								mThreadLocks[_threadIndex].Set();
							}, resourceDLRequest);
							bool flag = false;
							while (!mThreadLocks[_threadIndex].WaitOne(GetResponsesWaitTimeMillis()))
							{
								if (!mIsRunning)
								{
									flag = true;
									break;
								}
							}
							if (ResourceDLUtils.IsDebugThreadCancelError())
							{
								flag = true;
							}
							if (flag)
							{
								ResourceDLUtils.dbg2("ThreadMain canceled");
								resourceDLRequest._serverResult = 4;
							}
							else
							{
								HttpAsync.RESULT rESULT = resourceDLRequest._httpAsync.Result;
								if (ResourceDLUtils.IsDebugThreadAsyncResult())
								{
									rESULT = (HttpAsync.RESULT)ResourceDLUtils.GetDebugThreadAsyncResult((int)rESULT);
								}
								switch (rESULT)
								{
								case HttpAsync.RESULT.SUCCESS:
								{
									ResourceDLUtils.dbg2("ThreadMain HttpAsync SUCCESS");
									HttpWebResponse httpWebResponse = resourceDLRequest._httpAsync.Response as HttpWebResponse;
									if (ResourceDLUtils.IsDebugThreadNoResponseError())
									{
										httpWebResponse = null;
									}
									if (httpWebResponse != null)
									{
										BIJDLData bIJDLData2 = resourceDLRequest._userdata as BIJDLData;
										ResourceDLUtils.log(string.Format("(#{0}): HTTP Result={1}   (len={2})", resourceDLRequest._httpAsync.SeqNo, httpWebResponse.StatusCode, httpWebResponse.ContentLength));
										HttpStatusCode httpStatusCode = httpWebResponse.StatusCode;
										if (ResourceDLUtils.IsDebugThreadHTTPError())
										{
											httpStatusCode = HttpStatusCode.NotFound;
										}
										if (httpStatusCode == HttpStatusCode.OK || httpStatusCode == HttpStatusCode.Accepted || httpStatusCode == HttpStatusCode.NoContent)
										{
											resourceDLRequest._serverResult = 0;
											if (num != -1)
											{
												StartDownloadThread(num);
												num = -1;
											}
										}
										else
										{
											resourceDLRequest._serverResult = 1;
										}
									}
									else
									{
										ResourceDLUtils.dbg2("ThreadMain HttpAsync NO Response");
										resourceDLRequest._serverResult = 1;
									}
									break;
								}
								case HttpAsync.RESULT.ERROR:
									ResourceDLUtils.dbg2("ThreadMain HttpAsync ERROR");
									resourceDLRequest._serverResult = 1;
									break;
								case HttpAsync.RESULT.TIMEOUT:
									ResourceDLUtils.dbg2("ThreadMain HttpAsync TIMEOUT");
									resourceDLRequest._serverResult = 3;
									break;
								default:
									ResourceDLUtils.dbg2("ThreadMain HttpAsync OTHER : " + rESULT);
									resourceDLRequest._serverResult = 1;
									break;
								}
							}
						}
						catch (Exception ex)
						{
							ResourceDLUtils.log("Unknown server comminucation error." + ex.Message);
							resourceDLRequest._serverResult = 1;
						}
						lock (mResponsesLock)
						{
							ResourceDLUtils.dbg2("ThreadMain Enqueue to Response Queue");
							mResponses.Enqueue(resourceDLRequest);
						}
					}
					else
					{
						mThreadProcessing[_threadIndex] = false;
					}
				}
				while (resourceDLRequest != null);
			}
			catch (Exception a_e)
			{
				ResourceDLUtils.err("Server comminucation something missing.", a_e);
				if (eRROR_CODE == ResourceDLErrorInfo.ERROR_CODE.NO_ERROR && eRROR_CODE2 != 0)
				{
					mGame.CreateDLError(null, eRROR_CODE2, ResourceDLErrorInfo.PLACE.DLMANAGER_THREADMAIN, string.Empty, a_e);
				}
			}
		}
	}

	private bool PostRequest(ref HttpAsync a_httpAsync, Action<int> a_handler, HttpAsync.ResponseHandler a_responseHandler, object a_userdata)
	{
		ResourceDLRequest resourceDLRequest = new ResourceDLRequest();
		resourceDLRequest._httpAsync = a_httpAsync;
		resourceDLRequest._errorHandler = a_handler;
		resourceDLRequest._responseHandler = a_responseHandler;
		resourceDLRequest._userdata = a_userdata;
		resourceDLRequest._serverResult = 0;
		lock (mRequestsLock)
		{
			mRequests.Enqueue(resourceDLRequest);
		}
		mRequestsNotify.Set();
		ResourceDLUtils.dbg2("Post Request!!");
		return true;
	}

	public IEnumerator UnzipAssets()
	{
		ResourceDLUtils.dbg2("UnzipAssets");
		IsUnzipFinished = true;
		yield return null;
	}

	public bool DLFilelistHashCheck()
	{
		bool flag = true;
		foreach (int value in Enum.GetValues(typeof(KIND)))
		{
			bool flag2 = DLFilelistHashCheck((KIND)value);
			flag = flag && flag2;
			if (!flag2 && !mKindList.Contains((KIND)value))
			{
				mKindList.Add((KIND)value);
			}
		}
		return flag;
	}

	public bool DLFilelistHashCheck(KIND aKind)
	{
		string resourceDLPath = getResourceDLPath(aKind);
		resourceDLPath += Constants.RESOURCEDL_SERVERFILE;
		string fileHash = BIJIOUtils.getFileHash(resourceDLPath);
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		string empty = string.Empty;
		switch (aKind)
		{
		case KIND.MAIN_STAGE:
			empty = instance.mGameProfile.DlListHashStage;
			break;
		case KIND.ADV:
			empty = instance.mGameProfile.DlListHashAdv;
			break;
		case KIND.ADV_FIGURE:
			empty = instance.mGameProfile.DlListHashAdvFigure;
			break;
		case KIND.ADV_STAGE:
			empty = instance.mGameProfile.DlListHashAdvStage;
			break;
		default:
			empty = instance.mGameProfile.DlListHash;
			break;
		}
		if (string.IsNullOrEmpty(empty))
		{
		}
		if (string.IsNullOrEmpty(fileHash))
		{
			return false;
		}
		bool flag = fileHash.Equals(empty);
		if (instance.mDebugDLFilelistHashError)
		{
			flag = false;
			instance.mDebugDLFilelistHashError = false;
		}
		if (flag)
		{
			return true;
		}
		return false;
	}
}
