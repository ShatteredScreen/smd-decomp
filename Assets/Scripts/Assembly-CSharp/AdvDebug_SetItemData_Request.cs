public class AdvDebug_SetItemData_Request : RequestBase
{
	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }
}
