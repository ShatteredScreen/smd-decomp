using System;
using System.Collections.Generic;

public class SNSFriendManager
{
	private enum STATE
	{
		NONE = 0,
		WAIT = 1,
		NETWORK_PROLOGUE = 2,
		NETWORK_01 = 3,
		NETWORK_02 = 4,
		NETWORK_EPILOGUE = 5,
		NETWORK_NONE = 6
	}

	public enum MODE
	{
		NONE = 0,
		FORCE = 1,
		LITE = 2
	}

	private class _GetFriendsData
	{
		public List<MyFaceBookFriend> results { get; set; }
	}

	public delegate void OnGetSNSFriendData(List<MyFaceBookFriend> list);

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.NONE);

	public GameMain mGame;

	private SNS mSNSKind;

	private MODE mMode;

	private OnGetSNSFriendData mCallback;

	private bool mNetworkDataModified;

	private Dictionary<string, MyFaceBookFriend> mSNSFriendsWork;

	private Dictionary<string, MyFaceBookFriend> mGameFriendsWork;

	private int mNewAddedGameFriendCount;

	private Queue<MyFaceBookFriend> mFriendIconsWork;

	private string mErrorMessage;

	public bool mSucceed;

	public bool mInNetwork;

	public bool mFBfriend;

	public bool IsDataModified
	{
		get
		{
			return mNetworkDataModified;
		}
	}

	public int NewAddedGameFriendCount
	{
		get
		{
			return mNewAddedGameFriendCount;
		}
	}

	public bool HasError
	{
		get
		{
			return !string.IsNullOrEmpty(mErrorMessage);
		}
	}

	public SNSFriendManager(GameMain game)
		: this(game, MODE.NONE, SNS.Facebook)
	{
	}

	public SNSFriendManager(GameMain game, MODE mode)
		: this(game, mode, SNS.Facebook)
	{
	}

	public SNSFriendManager(GameMain game, MODE mode, SNS kind)
	{
		mGame = game;
		mSNSKind = kind;
		mMode = mode;
		if (mGame != null && (mMode == MODE.FORCE || mMode == MODE.LITE))
		{
			mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
	}

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
	}

	private static void dbg(string msg)
	{
	}

	public bool Start()
	{
		if (mState.GetStatus() != 0)
		{
			return false;
		}
		if (mSNSKind != SNS.Facebook)
		{
			return false;
		}
		mGame.FaceBookUserList = new List<MyFaceBookFriend>();
		mState.Reset(STATE.NETWORK_PROLOGUE, true);
		return true;
	}

	public bool IsFinished()
	{
		return mState.GetStatus() == STATE.NETWORK_NONE;
	}

	public void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_PROLOGUE:
			Server.GetSNSFriendDataEvent += Server_OnSNSGetFriends;
			mErrorMessage = null;
			mNetworkDataModified = false;
			mNewAddedGameFriendCount = 0;
			mSNSFriendsWork = new Dictionary<string, MyFaceBookFriend>();
			mGameFriendsWork = new Dictionary<string, MyFaceBookFriend>();
			mFriendIconsWork = new Queue<MyFaceBookFriend>();
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			Network_OnSNSGetFriends();
			break;
		case STATE.NETWORK_EPILOGUE:
			Server.GetSNSFriendDataEvent -= Server_OnSNSGetFriends;
			mSNSFriendsWork.Clear();
			mGameFriendsWork.Clear();
			mFriendIconsWork.Clear();
			mState.Change(STATE.NETWORK_NONE);
			break;
		}
		mState.Update();
	}

	private string GetSNSID(MyFaceBookFriend mf)
	{
		return GetSNSID(mf, string.Empty);
	}

	private string GetSNSID(MyFriendData mf)
	{
		return GetSNSID(mf, string.Empty);
	}

	private string GetSNSID(MyFaceBookFriend mf, string prefix)
	{
		return GetSNSID(mf.Data, prefix);
	}

	private string GetSNSID(MyFriendData mf, string prefix)
	{
		if (string.IsNullOrEmpty(prefix))
		{
			prefix = string.Empty;
		}
		string text = null;
		switch (mSNSKind)
		{
		case SNS.Facebook:
			return prefix + mf.Fbid;
		case SNS.GameCenter:
			return prefix + mf.Gcid;
		case SNS.GooglePlus:
			return prefix + mf.Gpid;
		default:
			return null;
		}
	}

	private bool SetSNSID(ref MyFaceBookFriend mf, string id)
	{
		if (string.IsNullOrEmpty(id))
		{
			return false;
		}
		bool result = true;
		switch (mSNSKind)
		{
		case SNS.Facebook:
			mf.Data.Fbid = id;
			break;
		case SNS.GameCenter:
			mf.Data.Gcid = id;
			break;
		case SNS.GooglePlus:
			mf.Data.Gpid = id;
			break;
		default:
			result = false;
			break;
		}
		return result;
	}

	private bool SetSNSName(ref MyFaceBookFriend mf, string name)
	{
		if (string.IsNullOrEmpty(name))
		{
			return false;
		}
		bool result = true;
		switch (mSNSKind)
		{
		case SNS.Facebook:
			mf.Data.FbName = name;
			break;
		case SNS.GameCenter:
			mf.Data.GcName = name;
			break;
		case SNS.GooglePlus:
			mf.Data.GpName = name;
			break;
		default:
			result = false;
			break;
		}
		return result;
	}

	private void Network_OnSNSGetFriends()
	{
		STATE _NextState = STATE.NETWORK_EPILOGUE;
		BIJSNS bIJSNS = SocialManager.Instance[mSNSKind];
		if (!GameMain.IsSNSLogined(bIJSNS))
		{
			log("Network_OnSNSGetFriends: Not logged in. skip.");
			mState.Change(_NextState);
			return;
		}
		mState.Change(STATE.WAIT);
		int sNS_FRIEND_MAX = Constants.SNS_FRIEND_MAX;
		bIJSNS.GetFriends(null, sNS_FRIEND_MAX, delegate(BIJSNS.Response res, object userdata)
		{
			mSucceed = false;
			mInNetwork = false;
			if (res != null && res.detail != null)
			{
				try
				{
					List<IBIJSNSUser> list = res.detail as List<IBIJSNSUser>;
					log("Network_OnSNSGetFriends: friends=" + ((list != null) ? list.Count : (-1)));
					if (list != null && list.Count > 0)
					{
						mGame.FBNameData.Clear();
						foreach (IBIJSNSUser item in list)
						{
							dbg("friend = " + item);
							MyFaceBookFriend mf = new MyFaceBookFriend();
							SetSNSID(ref mf, item.ID);
							mf.Data.Name = item.Name;
							mf.Gender = item.Gender;
							mGame.FBNameData.Add(item.ID, item.Name);
							mSNSFriendsWork[item.ID] = mf;
						}
						List<string> a_list = new List<string>();
						if (mSNSFriendsWork.Count > 0)
						{
							a_list = new List<string>(mSNSFriendsWork.Keys);
						}
						mSucceed = Server.GetSNSFriendData(a_list, mSNSKind);
						mInNetwork = true;
					}
					else if (list != null && list.Count == 0)
					{
						mInNetwork = true;
					}
				}
				catch (Exception)
				{
				}
				mGame.mPlayer.Data.LastGetSocialTime = DateTime.Now;
			}
			if (!mSucceed)
			{
				mState.Change(_NextState);
			}
		}, null);
	}

	private void Server_OnSNSGetFriends(int result, int code, string json)
	{
		STATE aStatus = STATE.NETWORK_EPILOGUE;
		log("Server_OnGetFriends: " + GameMain.Server_Result(result));
		if (result == 0)
		{
			try
			{
				_GetFriendsData obj = new _GetFriendsData();
				new MiniJSONSerializer().Populate(ref obj, json);
				if (obj != null && obj.results != null && obj.results.Count > 0)
				{
					mFBfriend = true;
					mGame.FaceBookUserList = new List<MyFaceBookFriend>();
					List<MyFaceBookFriend> facebookUserList = mGame.mPlayer.GetFacebookUserList(obj.results);
					foreach (MyFaceBookFriend item in facebookUserList)
					{
						mGame.FaceBookUserList.Add(item.Clone());
					}
				}
				else
				{
					mFBfriend = false;
				}
				mGame.mPlayer.Data.LastGetSocialTime = DateTime.Now;
				if (mCallback != null)
				{
					mCallback(new List<MyFaceBookFriend>(mGameFriendsWork.Values));
				}
			}
			catch (Exception)
			{
			}
		}
		else
		{
			mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mGameFriendsWork.Clear();
		mState.Change(aStatus);
	}
}
