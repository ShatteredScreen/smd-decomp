using System;

public sealed class ChanceCommInfo
{
	[MiniJSONAlias("start")]
	public long StartTimeEpoch { get; set; }

	[MiniJSONAlias("end")]
	public long EndTimeEpoch { get; set; }

	[MiniJSONAlias("period")]
	public long Period { get; set; }

	[MiniJSONAlias("purchase")]
	public int Purchase { get; set; }

	public DateTime StartTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(StartTimeEpoch).ToLocalTime();
		}
	}

	public DateTime EndTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(EndTimeEpoch).ToLocalTime();
		}
	}

	public ChanceCommInfo()
	{
		StartTimeEpoch = 0L;
		EndTimeEpoch = 0L;
		Period = 0L;
		Purchase = 0;
	}

	public bool InTime()
	{
		DateTime dateTime = DateTimeUtil.Now();
		return StartTime <= dateTime && dateTime < EndTime;
	}
}
