using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;
using UnityEngine;

public class PurchaseFullDialog : PurchaseDialog
{
	public enum MODE
	{
		None = 0,
		Gem = 1,
		Campaign = 2,
		Resume = 3
	}

	public new enum STATE
	{
		INIT = 0,
		AUTH_CHECK = 1,
		AUTH_CHECK_WAIT = 2,
		IS_PURCHASE_POSSIBLE = 3,
		CAMPAIGN_CHECK = 4,
		CAMPAIGN_BANNER = 5,
		GET_TIER_INFO = 6,
		GET_BIRTHDAY = 7,
		INPUT_BIRTHDAY = 8,
		SET_BIRTHDAY = 9,
		RESTORE_TRANSACTIONS_CHECK = 10,
		RESTORE_TRANSACTIONS = 11,
		TIER_SELECT = 12,
		CREATE_LIST = 13,
		WAIT_CREATE_LIST = 14,
		MAINTENACE_CHECK = 15,
		PURCHASE_CHECK = 16,
		PURCHASE = 17,
		CONSUME_PRODUCT = 18,
		ADD_GEM = 19,
		CAMPAIGN_CHARGE = 20,
		PURCHASE_END = 21,
		UPDATE_GEM = 22,
		CLOSE = 23,
		ERROR = 24,
		FAIL = 25,
		WAIT = 26,
		EDITOR_WAIT = 27,
		AUTHERROR_WAIT = 28
	}

	private const float INNER_FRAME_FLUCT_HEIGHT = 192f;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private static float mAdjustListHeight = (int)(192f * (Aspect - 1f));

	private List<SMVerticalListItem> mCampaignTierList = new List<SMVerticalListItem>();

	private SMVerticalList mTierCampaignSelectList;

	private static SMVerticalListInfo TierListInfo = new SMVerticalListInfo(2, 1040f, 330f + mAdjustListHeight, 1, 510f, 780f, 1, 126);

	private static SMVerticalListInfo TierListInfoEn = new SMVerticalListInfo(2, 1040f, 280f + mAdjustListHeight, 1, 510f, 730f, 1, 126);

	private static SMVerticalListInfo CampaignTierListInfo = new SMVerticalListInfo(2, 1040f, 330f + mAdjustListHeight, 1, 510f, 780f, 1, 335);

	private static SMVerticalListInfo CampaignTierListInfoEn = new SMVerticalListInfo(2, 1040f, 330f + mAdjustListHeight, 1, 510f, 780f, 1, 335);

	private GameObject mDialogCenter;

	private List<UISprite> mLaceList = new List<UISprite>();

	private bool mLaceMove;

	private GameObject mAnchorTop;

	private GameObject mAnchorTopRight;

	private GameObject mAnchorBottomLeft;

	private GameObject mHeader;

	private UISprite mBackGround;

	private UISprite mTitleBarR;

	private UISprite mTitleBarL;

	private UISprite mTitleBarBG;

	private UILabel mTitle;

	private UIButton mBackButton;

	private UIButton mDisplay;

	private UIButton mTabGem;

	private UIButton mTabCampaign;

	private UILabel mCaution;

	private string mPCautionStr;

	private string mLCautionStr;

	private UILabel mSaleFinished;

	private MODE mCurrentMode;

	private MODE mPausedMode;

	private bool mListCreated;

	private CampaignData mSelectedCampaign;

	private bool mPurchasedBefore;

	private bool mRankingClosed;

	private bool mAcountCheck;

	private static bool isShow = false;

	private bool mResumed;

	protected static float Aspect
	{
		get
		{
			Vector2 vector = Util.LogScreenSize();
			Vector2 vector2 = ((!(vector.x > vector.y)) ? new Vector2(vector.y, vector.x) : vector);
			return vector2.y / (vector2.x / 16f * 9f);
		}
	}

	public static bool IsShow()
	{
		return isShow;
	}

	public override void Start()
	{
		base.Start();
		isShow = true;
		mPurchasedBefore = SingletonMonoBehaviour<GameMain>.Instance.mServerCramVariableData.mPurchased;
		if (GameMain.USE_DEBUG_DIALOG)
		{
			mAcountCheck = true;
		}
	}

	public new void Init(string message)
	{
		mMessage = message;
	}

	private IEnumerator LaceRotate(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		BIJImage resImageCollection = mAtlasMailBox;
		UISprite sprite = Util.CreateSprite("Lace", parent, resImageCollection);
		Util.SetSpriteInfo(sprite, "characolle_back_lace02", depth, pos, scale, false);
		mLaceList.Add(sprite);
		float angle = 0f;
		while (mLaceMove)
		{
			angle += Time.deltaTime * speed;
			sprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	public override IEnumerator BuildResource()
	{
		switch (mGame.mGameStateManager.CurrentState)
		{
		case GameStateManager.GAME_STATE.PUZZLE:
		{
			if (!(mGame.mGameStateManager.mGameState is GameStatePuzzle))
			{
				break;
			}
			GameStatePuzzle gsp = mGame.mGameStateManager.mGameState as GameStatePuzzle;
			if (gsp != null)
			{
				yield return StartCoroutine(gsp.CloseSocialRanking(delegate(bool _closed)
				{
					mRankingClosed = _closed;
				}));
			}
			break;
		}
		case GameStateManager.GAME_STATE.MAP:
		case GameStateManager.GAME_STATE.EVENT_RALLY:
		case GameStateManager.GAME_STATE.EVENT_STAR:
		case GameStateManager.GAME_STATE.EVENT_RALLY2:
		case GameStateManager.GAME_STATE.EVENT_BINGO:
		case GameStateManager.GAME_STATE.EVENT_LABYRINTH:
		{
			GameStateSMMapBase mapbase = mGame.mGameStateManager.mGameState as GameStateSMMapBase;
			if (mapbase != null)
			{
				yield return StartCoroutine(mapbase.CloseSocialRanking(delegate(bool _closed)
				{
					mRankingClosed = _closed;
				}));
			}
			break;
		}
		}
		yield return StartCoroutine(base.BuildResource());
	}

	public override void BuildDialog()
	{
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage atlas = mAtlasMailBox;
		UIFont atlasFont = GameMain.LoadFont();
		GameObject parent = base.gameObject;
		mAnchorTop = Util.CreateAnchorWithChild("AnchorTop", parent, UIAnchor.Side.Top, component).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("AnchorTopRight", parent, UIAnchor.Side.TopRight, component).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("AnchorBottomLeft", parent, UIAnchor.Side.BottomLeft, component).gameObject;
		mLaceMove = true;
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, mBaseDepth + 1, new Vector3(30f, -110f, 0f), Vector3.one, 15f));
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, mBaseDepth + 1, new Vector3(-140f, -10f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(190f, -50f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(-20f, 60f, 0f), Vector3.one, 15f));
		Vector2 vector = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, Util.ScreenOrientation);
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomLeft, Util.ScreenOrientation);
		Vector2 vector3 = Util.LogScreenSize();
		float num = vector3.y * 0.5f;
		float num2 = vector3.x * 0.5f;
		string empty = string.Empty;
		mHeader = Util.CreateGameObject("Header", parent);
		mHeader.transform.localPosition = vector;
		mBackGround = Util.CreateSprite("BackGround", parent, atlas);
		Util.SetSpriteInfo(mBackGround, "characolle_back00", mBaseDepth, new Vector3(0f, 0f, 200f), Vector3.one, false);
		mBackGround.type = UIBasicSprite.Type.Tiled;
		mTitleBarR = Util.CreateSprite("TitleBarR", mHeader, image);
		Util.SetSpriteInfo(mTitleBarR, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, 173f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBarL = Util.CreateSprite("TitleBarL", mHeader, image);
		Util.SetSpriteInfo(mTitleBarL, "menubar_frame", mBaseDepth + 2, new Vector3(2f, 173f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitle = Util.CreateLabel("Title", mHeader, atlasFont);
		Util.SetLabelInfo(mTitle, string.Empty, mBaseDepth + 3, Vector3.zero, 38, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(mTitle, Localization.Get("Purchase_Title"));
		mTitle.transform.localPosition = new Vector3(0f, -26f, 0f);
		empty = "button_back";
		mBackButton = Util.CreateJellyImageButton("BackButton", parent, image);
		Util.SetImageButtonInfo(mBackButton, empty, empty, empty, mBaseDepth + 3, new Vector3(vector2.x + 55f, vector2.y + 55f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mBackButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
		empty = "LC_jem_tab00";
		mTabCampaign = Util.CreateImageButton("TabCampaign", parent, mAtlasMailBox);
		Util.SetImageButtonInfo(mTabCampaign, empty, empty, empty, mBaseDepth + 10, new Vector3(-170f, 455f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabCampaign, this, "OnCampaignTabPushed", UIButtonMessage.Trigger.OnClick);
		empty = "LC_jem_tab01";
		mTabGem = Util.CreateImageButton("TabGem", parent, mAtlasMailBox);
		Util.SetImageButtonInfo(mTabGem, empty, empty, empty, mBaseDepth + 10, new Vector3(0f, 455f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabGem, this, "OnGemTabPushed", UIButtonMessage.Trigger.OnClick);
		ActivateTab(MODE.None);
		SetJellyTargetScale(1f);
		InitDialogFrame(DIALOG_SIZE.XLARGE, null, null, 610, 865);
		CreateGemWindow();
		mPCautionStr = Localization.Get("Purchase_Caution_Decs_En");
		mLCautionStr = Localization.Get("Purchase_Caution_L_Decs_En");
		mCaution = Util.CreateLabel("Caution", parent, atlasFont);
		Util.SetLabelInfo(mCaution, string.Empty, mBaseDepth + 5, new Vector3(-240f, -380f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		mCaution.color = new Color32(242, 55, 117, byte.MaxValue);
		mSaleFinished = Util.CreateLabel("SaleFinished", parent, atlasFont);
		Util.SetLabelInfo(mSaleFinished, string.Empty, mBaseDepth + 7, new Vector3(0f, 50f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
		mSaleFinished.spacingY = 10;
		mSaleFinished.color = Def.DEFAULT_MESSAGE_COLOR;
		SetLayout(Util.ScreenOrientation);
	}

	public override void CreateGemWindow(bool isEula = false)
	{
		if (!(mDialogFrame == null))
		{
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			UIFont atlasFont = GameMain.LoadFont();
			Vector3 vector = ((Util.ScreenOrientation != ScreenOrientation.LandscapeLeft && Util.ScreenOrientation != ScreenOrientation.LandscapeRight) ? mGemFramePositionOffsetP : mGemFramePositionOffsetL);
			mGemFrame = Util.CreateSprite("GemFrame", base.gameObject, image);
			Util.SetSpriteInfo(mGemFrame, "instruction_panel8", mBaseDepth, Vector3.zero + vector, Vector3.one, false);
			mGemFrame.SetDimensions(190, 70);
			UISprite sprite = Util.CreateSprite("GemIcon", mGemFrame.gameObject, image);
			Util.SetSpriteInfo(sprite, "jem_panel", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
			mGemDisplayNum = mGame.mPlayer.Dollar;
			mGemNumLabel = Util.CreateLabel("GemNum", mGemFrame.gameObject, atlasFont);
			Util.SetLabelInfo(mGemNumLabel, string.Empty + mGemDisplayNum, mBaseDepth + 2, new Vector3(20f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			mGemNumLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			SetLayoutGemDisp(Util.ScreenOrientation);
		}
	}

	protected override void SetLayoutGemDisp(ScreenOrientation o)
	{
		if (!(mGemFrame == null))
		{
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mGemFrame.transform.localPosition = new Vector3(150f, -470f);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mGemFrame.transform.localPosition = new Vector3(440f, -235f - mAdjustListHeight / 2f);
				break;
			}
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, Util.ScreenOrientation);
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomLeft, Util.ScreenOrientation);
		Vector2 vector3 = Util.LogScreenSize();
		float num = vector3.y * 0.5f;
		float num2 = vector3.x * 0.5f;
		string empty = string.Empty;
		mHeader.transform.localPosition = vector;
		mBackButton.transform.localPosition = new Vector3(vector2.x + 55f, vector2.y + 55f);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			float num4 = ((!Util.IsWideScreen()) ? (-5) : 0);
			mDialogFrame.SetLocalPositionY(num4);
			mDialogFrame.SetDimensions(610, 865);
			if (mDisplay != null)
			{
				mDisplay.transform.localPosition = new Vector3(-80f, -460f);
			}
			mTabGem.transform.localPosition = new Vector3(0f, 455f + num4, 0f);
			mTabCampaign.transform.localPosition = new Vector3(-170f, 455f + num4, 0f);
			SetLayoutCaution(o);
			SetLayoutVerticalList(mTierSelectList, true, mCurrentMode == MODE.Gem);
			SetLayoutVerticalList(mTierCampaignSelectList, true, mCurrentMode == MODE.Campaign);
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			float num3 = ((!Util.IsWideScreen()) ? (-10) : 0);
			mDialogFrame.SetLocalPositionY(num3);
			mDialogFrame.SetDimensions(1130, 370 + (int)mAdjustListHeight);
			if (mDisplay != null)
			{
				mDisplay.transform.localPosition = new Vector3(200f, -225f - mAdjustListHeight / 2f);
			}
			mTabGem.transform.localPosition = new Vector3(90f, 207f + mAdjustListHeight / 2f + num3, 0f);
			mTabCampaign.transform.localPosition = new Vector3(-90f, 207f + mAdjustListHeight / 2f + num3, 0f);
			SetLayoutCaution(o);
			SetLayoutVerticalList(mTierSelectList, false, mCurrentMode == MODE.Gem);
			SetLayoutVerticalList(mTierCampaignSelectList, false, mCurrentMode == MODE.Campaign);
			break;
		}
		}
		if (mBackGround != null)
		{
			Vector2 vector4 = Util.LogScreenSize();
			float num5 = vector4.y / 910f;
			mBackGround.transform.localScale = new Vector3(num5, num5, 1f);
			mBackGround.SetDimensions((int)(vector4.x / num5) + 2, (int)(vector4.y / num5) + 2);
		}
	}

	private void SetLayoutVerticalList(SMVerticalList aList, bool aPortrait, bool aIsActive)
	{
		if (aList == null)
		{
			return;
		}
		if (aIsActive)
		{
			aList.OnScreenChanged(aPortrait);
			aList.ScrollSet(0f);
			return;
		}
		aList.gameObject.SetActive(true);
		aList.OnScreenChanged(aPortrait);
		aList.ScrollSet(0f);
		aList.gameObject.SetActive(false);
		float y = 0f;
		if (!Util.IsWideScreen())
		{
			y = ((!aPortrait) ? (-10f) : (-5f));
		}
		aList.SetLocalPositionY(y);
	}

	protected void SetLayoutCaution(ScreenOrientation o)
	{
		if (mCaution == null)
		{
			return;
		}
		switch (mCurrentMode)
		{
		case MODE.None:
		case MODE.Resume:
			return;
		case MODE.Campaign:
			mCaution.text = string.Empty;
			break;
		case MODE.Gem:
			if (mGame.mMaintenanceProfile.GemSale == 0)
			{
				mCaution.text = string.Empty;
				break;
			}
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mCaution.text = mPCautionStr;
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mCaution.text = mLCautionStr;
				break;
			}
			break;
		}
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mCaution.transform.localPosition = new Vector3(-240f, -380f);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mCaution.transform.localPosition = new Vector3(-320f, -145f - mAdjustListHeight / 2f);
			break;
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		isShow = false;
		if (mCallback != null)
		{
			mCallback();
		}
		if (mRankingClosed)
		{
			switch (mGame.mGameStateManager.CurrentState)
			{
			case GameStateManager.GAME_STATE.PUZZLE:
				if (mGame.mGameStateManager.mGameState is GameStatePuzzle)
				{
					GameStatePuzzle gameStatePuzzle = mGame.mGameStateManager.mGameState as GameStatePuzzle;
					if (gameStatePuzzle != null)
					{
						gameStatePuzzle.RankingClosing = false;
						gameStatePuzzle.SocialRanking();
					}
				}
				break;
			case GameStateManager.GAME_STATE.MAP:
			case GameStateManager.GAME_STATE.EVENT_RALLY:
			case GameStateManager.GAME_STATE.EVENT_STAR:
			case GameStateManager.GAME_STATE.EVENT_RALLY2:
			case GameStateManager.GAME_STATE.EVENT_BINGO:
			case GameStateManager.GAME_STATE.EVENT_LABYRINTH:
			{
				GameStateSMMapBase gameStateSMMapBase = mGame.mGameStateManager.mGameState as GameStateSMMapBase;
				if (gameStateSMMapBase != null)
				{
					gameStateSMMapBase.ShowSocialRankingDialog();
				}
				break;
			}
			}
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	protected void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			mResumed = true;
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	private new void OnCancelPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.TIER_SELECT && !GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mGame.ClearCampaignBanner();
			Close();
		}
	}

	public new void OnBuyPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.TIER_SELECT && !mResumed)
		{
			string[] array = go.name.Split('@');
			PItem item = PurchaseManager.instance.GetItem(array[1]);
			mGame.PlaySe("SE_POSITIVE", -1);
			selectPItem = item;
			purchasing = true;
			mSelectedCampaign = null;
			mState.Change(STATE.PURCHASE_CHECK);
		}
	}

	public void OnCampaignBuyPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.TIER_SELECT && !mResumed)
		{
			string[] array = go.name.Split('@');
			int index = int.Parse(array[1]);
			TierCampaignListItem tierCampaignListItem = (TierCampaignListItem)mCampaignTierList[index];
			if (tierCampaignListItem.mCampaignData.limit_count != 0 && tierCampaignListItem.mCampaignData.InTime())
			{
				PItem pItem = tierCampaignListItem.pItem;
				mGame.PlaySe("SE_POSITIVE", -1);
				selectPItem = pItem;
				purchasing = true;
				mSelectedCampaign = tierCampaignListItem.mCampaignData;
				mState.Change(STATE.PURCHASE_CHECK);
			}
		}
	}

	private void ActivateTab(MODE aMode, bool aWaitCreate = true)
	{
		bool flag = false;
		switch (aMode)
		{
		case MODE.Gem:
			Util.SetImageButtonColor(mTabGem, Color.white);
			Util.SetImageButtonColor(mTabCampaign, Color.gray);
			mTabGem.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 + 1;
			mTabCampaign.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 - 1;
			if (mGame.mMaintenanceProfile.GemSale == 0)
			{
				if (string.IsNullOrEmpty(mSaleFinished.text))
				{
					mSaleFinished.text = Localization.Get("GemEnd");
				}
				if (mTierSelectList != null)
				{
					mTierSelectList.gameObject.SetActive(false);
				}
			}
			else if (mTierSelectList == null)
			{
				CreateList();
				flag = true;
			}
			else
			{
				mTierSelectList.gameObject.SetActive(true);
			}
			if (mTierCampaignSelectList != null)
			{
				mTierCampaignSelectList.gameObject.SetActive(false);
			}
			break;
		case MODE.Campaign:
			Util.SetImageButtonColor(mTabGem, Color.gray);
			Util.SetImageButtonColor(mTabCampaign, Color.white);
			mTabGem.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 - 1;
			mTabCampaign.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 + 1;
			if (mGame.mMaintenanceProfile.GemSale == 0)
			{
				if (string.IsNullOrEmpty(mSaleFinished.text))
				{
					mSaleFinished.text = Localization.Get("GemEnd");
				}
				if (mTierCampaignSelectList != null)
				{
					mTierCampaignSelectList.gameObject.SetActive(false);
				}
			}
			else if (mTierCampaignSelectList == null)
			{
				CreateCampaignList();
				flag = true;
			}
			else
			{
				mTierCampaignSelectList.gameObject.SetActive(true);
			}
			if (mTierSelectList != null)
			{
				mTierSelectList.gameObject.SetActive(false);
			}
			break;
		case MODE.Resume:
			Util.SetImageButtonColor(mTabGem, Color.gray);
			Util.SetImageButtonColor(mTabCampaign, Color.gray);
			mTabGem.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 - 1;
			mTabCampaign.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 - 1;
			if (mTierSelectList != null)
			{
				mTierSelectList.gameObject.SetActive(false);
			}
			if (mTierCampaignSelectList != null)
			{
				mTierCampaignSelectList.gameObject.SetActive(false);
			}
			break;
		default:
			Util.SetImageButtonColor(mTabGem, Color.gray);
			Util.SetImageButtonColor(mTabCampaign, Color.gray);
			mTabGem.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 - 1;
			mTabCampaign.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 10 - 1;
			break;
		}
		if (aWaitCreate && flag && mCurrentMode != 0)
		{
			mListCreated = false;
			mState.Reset(STATE.WAIT_CREATE_LIST, true);
		}
		mCurrentMode = aMode;
		SetLayoutCaution(Util.ScreenOrientation);
	}

	public void OnCampaignTabPushed(GameObject go)
	{
		if (mCurrentMode != MODE.Campaign && mState.GetStatus() == STATE.TIER_SELECT)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			ActivateTab(MODE.Campaign);
		}
	}

	public void OnGemTabPushed(GameObject go)
	{
		if (mCurrentMode != MODE.Gem && mState.GetStatus() == STATE.TIER_SELECT)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			ActivateTab(MODE.Gem);
		}
	}

	protected override void CreateList()
	{
		CreateList(TierListInfoEn, -10f, 25f, 3);
	}

	public override void OnBeforeCreate_Tier()
	{
		switch (Util.ScreenOrientation)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mTierSelectList.OnScreenChanged(true);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mTierSelectList.OnScreenChanged(false);
			break;
		}
	}

	public override void OnCreateFinish_Tier()
	{
		mListCreated = true;
	}

	public override void Update()
	{
		DialogBaseUpdate();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			selectPItem = null;
			purchasing = false;
			mState.Change(STATE.AUTH_CHECK);
			break;
		case STATE.AUTH_CHECK:
			mGame.mPlayer.Data.LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.Network_UserAuth();
			mState.Change(STATE.AUTH_CHECK_WAIT);
			break;
		case STATE.AUTH_CHECK_WAIT:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.IS_PURCHASE_POSSIBLE);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
				break;
			}
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			if (mUserAuthErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 1, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), STATE.CLOSE);
			}
			break;
		}
		case STATE.IS_PURCHASE_POSSIBLE:
			mPurchasePossible = PurchaseManager.instance.CanMakePayments();
			if (!mPurchasePossible)
			{
				CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("ParentalControl"), STATE.CLOSE);
			}
			else
			{
				mState.Change(STATE.GET_TIER_INFO);
			}
			break;
		case STATE.GET_TIER_INFO:
			if (mState.IsChanged())
			{
				PurchaseManager.instance.RequestProductData();
				mGame.StartConnecting();
				isSucess = false;
				isConnnection = true;
			}
			else
			{
				if (isConnnection)
				{
					break;
				}
				mGame.FinishConnecting();
				if (isSucess)
				{
					if (purchaseFailed)
					{
						mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
						purchaseFailed = false;
					}
					else
					{
						mState.Change(STATE.CAMPAIGN_CHECK);
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase fail products"), STATE.CLOSE);
				}
			}
			break;
		case STATE.CAMPAIGN_CHECK:
			if (mState.IsChanged())
			{
				mGame.Network_CampaignCheckForce(mGame.mTutorialManager.IsInitialTutorialCompleted());
			}
			else
			{
				if (!GameMain.mCampaignCheckDone)
				{
					break;
				}
				if (GameMain.mCampaignCheckSucceed)
				{
					mState.Change(STATE.CAMPAIGN_BANNER);
					break;
				}
				CreateErrorRetryMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkError_CampaignDesc"), delegate(ConfirmDialog.SELECT_ITEM i)
				{
					if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
					{
						mState.Change(STATE.CAMPAIGN_CHECK);
					}
					else
					{
						mState.Change(STATE.CAMPAIGN_BANNER);
					}
				});
			}
			break;
		case STATE.CAMPAIGN_BANNER:
			if (mState.IsChanged())
			{
				if (!mGame.GetCampaignBanner(false))
				{
					mState.Change(STATE.CREATE_LIST);
				}
			}
			else if (GameMain.mCampaignBannerComplete)
			{
				mState.Change(STATE.CREATE_LIST);
			}
			break;
		case STATE.CREATE_LIST:
			if (mGame.IsCampaignAvailable())
			{
				ActivateTab(MODE.Campaign, false);
			}
			else
			{
				ActivateTab(MODE.Gem, false);
			}
			mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
			break;
		case STATE.GET_BIRTHDAY:
			if (mState.IsChanged())
			{
				mGame.Network_GetBirthday();
				mGame.StartConnecting();
			}
			else
			{
				if (!GameMain.mGetBirthdayCheckDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mGetBirthdaySucceed)
				{
					if (string.IsNullOrEmpty(GameMain.mGetBirthdayValue))
					{
						mState.Change(STATE.INPUT_BIRTHDAY);
					}
					else
					{
						mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), STATE.CLOSE);
				}
			}
			break;
		case STATE.INPUT_BIRTHDAY:
			if (mState.IsChanged())
			{
				AgeConfirmationDialog ageConfirmationDialog = Util.CreateGameObject("InputBirthdayDialog", base.transform.parent.gameObject).AddComponent<AgeConfirmationDialog>();
				ageConfirmationDialog.SetClosedCallback(delegate(string birthday)
				{
					mBirthday = birthday;
					mState.Change(STATE.SET_BIRTHDAY);
				});
			}
			break;
		case STATE.SET_BIRTHDAY:
			if (mState.IsChanged())
			{
				mGame.Network_SetBirthday(mBirthday);
				mGame.StartConnecting();
			}
			else if (GameMain.mSetBirthdayCheckDone)
			{
				mGame.FinishConnecting();
				if (GameMain.mSetBirthdaySucceed)
				{
					mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_SetBirthdayFailedDesc"), STATE.CLOSE);
				}
			}
			break;
		case STATE.RESTORE_TRANSACTIONS_CHECK:
			if (PurchaseManager.LogData.HasPLog())
			{
				PurchaseLog.PLog pLog = PurchaseManager.LogData.GetLastOnePLog();
				if (pLog.purchaseState == 100)
				{
					for (int k = 0; k < mNotConsumedGPList.Count; k++)
					{
						if (mNotConsumedGPList[k].productId == pLog.itemID)
						{
							PurchaseManager.LogData.PurchaseComplete();
							PurchaseManager.SavePurchaseData();
							pLog = null;
						}
					}
					if (pLog != null)
					{
						PurchaseManager.LogData.SetLastOnePLogStatus(PurchaseLog.PLog.State.COMPLETED);
						CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverDesc"), STATE.RESTORE_TRANSACTIONS);
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverDesc"), STATE.RESTORE_TRANSACTIONS);
				}
			}
			if (PurchaseManager.LogData.HasPLog())
			{
				break;
			}
			if (mNotConsumedGPList.Count > 0)
			{
				successItemID = new string[1];
				successItemID[0] = mNotConsumedGPList[0].productId;
				PItem item = PurchaseManager.instance.GetItem(mNotConsumedGPList[0].productId);
				if (item != null)
				{
					item.signature = mNotConsumedGPList[0].signature;
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverDesc"), STATE.CONSUME_PRODUCT);
					CampaignLog.CLog lastCLog = PurchaseManager.CampaignLogData.GetLastCLog(item.ItemID);
					if (lastCLog == null)
					{
						break;
					}
					mSelectedCampaign = null;
					List<CampaignData> mCampaignList = mGame.mCampaignList;
					for (int l = 0; l < mCampaignList.Count; l++)
					{
						CampaignData campaignData = mCampaignList[l];
						if (lastCLog.iap_campaign_id == campaignData.iap_campaign_id)
						{
							mSelectedCampaign = campaignData;
							break;
						}
					}
					if (mSelectedCampaign == null)
					{
						mSelectedCampaign = new CampaignData();
						mSelectedCampaign.iap_campaign_id = lastCLog.iap_campaign_id;
						mSelectedCampaign.iapname = lastCLog.iapname;
						mSelectedCampaign.debug_iapname = lastCLog.iapname;
						mSelectedCampaign.endtime = lastCLog.endtime;
					}
				}
				else
				{
					mState.Change(STATE.TIER_SELECT);
					PurchaseManager.CampaignLogData.ClearCLog(true);
				}
			}
			else
			{
				mState.Change(STATE.TIER_SELECT);
				PurchaseManager.CampaignLogData.ClearCLog(true);
			}
			break;
		case STATE.RESTORE_TRANSACTIONS:
			if (mState.IsChanged())
			{
				PurchaseLog.PLog lastOnePLog = PurchaseManager.LogData.GetLastOnePLog();
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.TRANSACTION;
				mGame.Network_BuyGem(PurchaseManager.instance.GetItemIdx(lastOnePLog.itemID), lastOnePLog.receipt, lastOnePLog.signature);
				mGame.StartConnecting();
			}
			else
			{
				if (!GameMain.mBuyGemCheckDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mBuyGemSucceed)
				{
					PurchaseLog.PLog lastOnePLog2 = PurchaseManager.LogData.GetLastOnePLog();
					if (lastOnePLog2 != null)
					{
						CampaignLog.CLog lastCLog3 = PurchaseManager.CampaignLogData.GetLastCLog(lastOnePLog2.itemID);
						if (lastCLog3 != null)
						{
							bool flag = false;
							CampaignData campaignData2 = null;
							List<CampaignData> mCampaignList2 = mGame.mCampaignList;
							for (int m = 0; m < mCampaignList2.Count; m++)
							{
								CampaignData campaignData3 = mCampaignList2[m];
								if (lastCLog3.iap_campaign_id == campaignData3.iap_campaign_id)
								{
									campaignData2 = campaignData3;
									flag = true;
									break;
								}
							}
							if (campaignData2 == null)
							{
								campaignData2 = new CampaignData();
								campaignData2.iap_campaign_id = lastCLog3.iap_campaign_id;
								campaignData2.iapname = lastCLog3.iapname;
								campaignData2.debug_iapname = lastCLog3.iapname;
								campaignData2.endtime = lastCLog3.endtime;
							}
							mGame.Network_CampaignCharge(campaignData2.iap_campaign_id, lastCLog3.datatime);
							if (mSelectedCampaign != null)
							{
								if (mGame.mServerCramVariableData.PurchaseCampaign == null)
								{
									mGame.mServerCramVariableData.PurchaseCampaign = new Dictionary<int, ServerCramVariableData.PurchaseCampaignInfo>();
								}
								if (!mGame.mServerCramVariableData.PurchaseCampaign.ContainsKey(mSelectedCampaign.iap_campaign_id))
								{
									mGame.mServerCramVariableData.PurchaseCampaign.Add(mSelectedCampaign.iap_campaign_id, new ServerCramVariableData.PurchaseCampaignInfo
									{
										BuyCount = 0,
										ShowDialogCount = 0,
										StartOfferTime = 0L,
										Place = 0
									});
								}
								ServerCramVariableData.PurchaseCampaignInfo value2 = mGame.mServerCramVariableData.PurchaseCampaign[mSelectedCampaign.iap_campaign_id];
								value2.BuyCount++;
								mGame.mServerCramVariableData.PurchaseCampaign[mSelectedCampaign.iap_campaign_id] = value2;
								mGame.SaveServerCramVariableData();
								int iapId2 = (int)PurchaseManager.instance.GetItemKind(mGame.GetCampaignIapName(mSelectedCampaign.iap_campaign_id));
								ServerCram.GemPurchase(mSelectedCampaign.iap_campaign_group_id, iapId2, (int)mSelectedCampaign.RemainMinutes(), (int)mSelectedCampaign.endtime, (int)GameMain.mBuyGemPlace, (int)value2.StartOfferTime, value2.ShowDialogCount, value2.BuyCount);
							}
							PurchaseManager.CampaignLogData.DeleteCLog(campaignData2.iap_campaign_id, true);
							if (flag)
							{
								if (campaignData2.limit_count > 0)
								{
									campaignData2.limit_count--;
								}
								CreateCampaignList();
							}
						}
					}
					PurchaseManager.LogData.PurchaseComplete();
					PurchaseManager.SavePurchaseData();
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverSuccess"), STATE.RESTORE_TRANSACTIONS_CHECK);
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverFail"), STATE.RESTORE_TRANSACTIONS);
				}
			}
			break;
		case STATE.TIER_SELECT:
		case STATE.EDITOR_WAIT:
			if (CheckResume() || !mState.IsChanged() || !mAcountCheck)
			{
				break;
			}
			mAcountCheck = false;
			if (mGoogleSkuInfoDict.Count > 0)
			{
				string googleAccount = BIJUnity.getGoogleAccount();
				if (string.IsNullOrEmpty(googleAccount))
				{
					ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.gameObject).AddComponent<ConfirmDialog>();
					confirmDialog.Init(Localization.Get("Debug_Check_GoogleAcount_Title"), Localization.Get("Debug_Check_GoogleAcount_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
				}
			}
			break;
		case STATE.WAIT_CREATE_LIST:
			if (!mState.IsChanged() && mListCreated)
			{
				mState.Change(STATE.TIER_SELECT);
			}
			break;
		case STATE.MAINTENACE_CHECK:
			if (mState.IsChanged())
			{
				mResumed = false;
				mGame.mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
				mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			}
			else
			{
				if (!GameMain.mMaintenanceProfileCheckDone)
				{
					break;
				}
				if (GameMain.mMaintenanceProfileSucceed)
				{
					if (GameMain.MaintenanceMode)
					{
						mGame.CreateMaintenanceDialog(ConnectCommonErrorDialog.STYLE.CLOSE, OnMaintenancheDialogClosed);
						mState.Change(STATE.WAIT);
					}
					else
					{
						ActivateTab(mPausedMode);
						mPausedMode = MODE.Resume;
						mState.Change(STATE.TIER_SELECT);
					}
				}
				else
				{
					mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY_CLOSE, OnMaintenancheCheckErrorDialogClosed, 15);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.PURCHASE_CHECK:
			if (mState.IsChanged())
			{
				if (mSelectedCampaign == null)
				{
					CampaignSaleEntry iAPSale = mGame.SegmentProfile.GetIAPSale(selectPItem.ItemID);
					if (iAPSale == null)
					{
						CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail"), STATE.TIER_SELECT);
						break;
					}
					if (!iAPSale.InTime())
					{
						CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail time"), STATE.TIER_SELECT);
						break;
					}
				}
				else
				{
					if (!mGame.mCampaignList.Contains(mSelectedCampaign))
					{
						CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail"), STATE.TIER_SELECT);
						break;
					}
					if (!mSelectedCampaign.InTime())
					{
						CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail time"), STATE.TIER_SELECT);
						break;
					}
				}
				mGame.Network_ChargeCheck(selectPItem.ItemNo);
				mGame.StartConnecting();
			}
			else
			{
				if (!GameMain.mChargeCheckCheckDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mChargeCheckSucceed)
				{
					if (GameMain.mChargeCheckResult)
					{
						if (GameMain.mChargeCheckAgeType == 1 || GameMain.mChargeCheckAgeType == 2)
						{
							string desc = Localization.Get("Purchase_ParentConfirmDesc") + "\n[FF0000]" + Localization.Get("AgeConfimation_Desc03") + "[-]";
							ConfirmDialog confirmDialog2 = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
							confirmDialog2.Init(Localization.Get("Purchase_ParentConfirmTitle"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.IMAGE_CHOICE, DIALOG_SIZE.MEDIUM, "LC_button_purchase", "LC_button_disagree");
							confirmDialog2.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
							{
								if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
								{
									mState.Change(STATE.PURCHASE);
								}
								else
								{
									mState.Change(STATE.TIER_SELECT);
								}
							});
							mState.Change(STATE.ERROR);
						}
						else
						{
							mState.Change(STATE.PURCHASE);
						}
					}
					else if (!GameMain.mChargeCheckLimitResult)
					{
						CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Cant Purchase Max SD"), STATE.TIER_SELECT);
					}
					else if (!GameMain.mChargeCheckAgeResult)
					{
						if (GameMain.mChargeCheckAgeType == 1)
						{
							CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Purchase_ChargeCheckFailed_AGE1"), STATE.TIER_SELECT);
						}
						else if (GameMain.mChargeCheckAgeType == 2)
						{
							CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Purchase_ChargeCheckFailed_AGE2"), STATE.TIER_SELECT);
						}
						else
						{
							CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Purchase_ChargeCheckFailed_AGE3"), STATE.TIER_SELECT);
						}
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), STATE.TIER_SELECT);
				}
			}
			break;
		case STATE.PURCHASE:
			if (mState.IsChanged())
			{
				if (mSelectedCampaign != null)
				{
					CampaignLog.CLog cLog = new CampaignLog.CLog();
					cLog.iap_campaign_id = mSelectedCampaign.iap_campaign_id;
					cLog.iapname = mSelectedCampaign.GetIapName();
					cLog.endtime = mSelectedCampaign.endtime;
					cLog.datatime = mGame.mCampaignDataTime;
					cLog.recordtime = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
					PurchaseManager.CampaignLogData.SetCLog(cLog, true);
				}
				Purchase();
			}
			else
			{
				if (isConnnection)
				{
					break;
				}
				if (isSucess)
				{
					mState.Change(STATE.CONSUME_PRODUCT);
				}
				else if (failKind == FAILKIND.CANCEL)
				{
					if (mSelectedCampaign != null)
					{
						PurchaseManager.CampaignLogData.DeleteCLog(mSelectedCampaign.iap_campaign_id, true);
					}
					CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase cancel"), STATE.TIER_SELECT);
				}
				else
				{
					purchaseFailed = true;
					CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail"), STATE.GET_TIER_INFO);
				}
			}
			break;
		case STATE.CONSUME_PRODUCT:
			if (mState.IsChanged())
			{
				GoogleIAB.consumeProducts(successItemID);
				isConnnection = true;
				isSucess = false;
			}
			else
			{
				if (isConnnection)
				{
					break;
				}
				if (isSucess)
				{
					if (mNotConsumedGPList.Count > 0)
					{
						mNotConsumedGPList.RemoveAt(0);
					}
					mState.Change(STATE.ADD_GEM);
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail"), STATE.TIER_SELECT);
				}
			}
			break;
		case STATE.ADD_GEM:
			if (mState.IsChanged())
			{
				mGame.Network_BuyGem(purchaseInfo.IAPNo, purchaseInfo.ReceiptDataString, purchaseInfo.ReceiptSignature);
				mGame.StartConnecting();
				if (purchaseInfo != null)
				{
					mGame.mOptions.AddPurchaseHistory(DateTime.Now, (float)purchaseInfo.TierData.Cost, purchaseInfo.TierData.CurrencyCode);
					mGame.SaveOptions();
					mGame.mServerCramVariableData.mPurchased = true;
					mGame.SaveServerCramVariableData();
					short aCampaignId = -1;
					short aCampaignGid = -1;
					if (mSelectedCampaign != null)
					{
						aCampaignId = (short)mSelectedCampaign.iap_campaign_id;
						aCampaignGid = (short)mSelectedCampaign.iap_campaign_group_id;
					}
					UserBehavior.Instance.AddPurchase((short)purchaseInfo.IAPNo, aCampaignId, aCampaignGid);
					UserBehavior.Save();
				}
			}
			else
			{
				if (!GameMain.mBuyGemCheckDone)
				{
					break;
				}
				if (GameMain.mBuyGemSucceed)
				{
					if (mSelectedCampaign == null)
					{
						mGame.FinishConnecting();
						mState.Change(STATE.PURCHASE_END);
					}
					else
					{
						mState.Change(STATE.CAMPAIGN_CHARGE);
					}
				}
				else
				{
					mGame.FinishConnecting();
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), delegate
					{
						mState.Change(STATE.ADD_GEM);
					});
				}
			}
			break;
		case STATE.CAMPAIGN_CHARGE:
			if (mState.IsChanged())
			{
				CampaignLog.CLog lastCLog2 = PurchaseManager.CampaignLogData.GetLastCLog(mSelectedCampaign.iap_campaign_id);
				long aPurchaseTime = mGame.mCampaignDataTime;
				if (lastCLog2 != null)
				{
					aPurchaseTime = lastCLog2.datatime;
				}
				mGame.Network_CampaignCharge(mSelectedCampaign.iap_campaign_id, aPurchaseTime);
				if (mSelectedCampaign != null)
				{
					if (mGame.mServerCramVariableData.PurchaseCampaign == null)
					{
						mGame.mServerCramVariableData.PurchaseCampaign = new Dictionary<int, ServerCramVariableData.PurchaseCampaignInfo>();
					}
					if (!mGame.mServerCramVariableData.PurchaseCampaign.ContainsKey(mSelectedCampaign.iap_campaign_id))
					{
						mGame.mServerCramVariableData.PurchaseCampaign.Add(mSelectedCampaign.iap_campaign_id, new ServerCramVariableData.PurchaseCampaignInfo
						{
							BuyCount = 0,
							ShowDialogCount = 0,
							StartOfferTime = 0L,
							Place = 0
						});
					}
					ServerCramVariableData.PurchaseCampaignInfo value = mGame.mServerCramVariableData.PurchaseCampaign[mSelectedCampaign.iap_campaign_id];
					value.BuyCount++;
					value.Place = 0;
					mGame.mServerCramVariableData.PurchaseCampaign[mSelectedCampaign.iap_campaign_id] = value;
					mGame.SaveServerCramVariableData();
					int iapId = (int)PurchaseManager.instance.GetItemKind(mGame.GetCampaignIapName(mSelectedCampaign.iap_campaign_id));
					ServerCram.GemPurchase(mSelectedCampaign.iap_campaign_group_id, iapId, (int)mSelectedCampaign.RemainMinutes(), (int)mSelectedCampaign.endtime, (int)GameMain.mBuyGemPlace, (int)value.StartOfferTime, value.ShowDialogCount, value.BuyCount);
				}
			}
			else
			{
				if (!GameMain.mCampaignChargeDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mCampaignChargeSucceed)
				{
					mResumed = false;
					PurchaseManager.CampaignLogData.DeleteCLog(mSelectedCampaign.iap_campaign_id, true);
					if (mSelectedCampaign.limit_count > 0)
					{
						mSelectedCampaign.limit_count--;
					}
					mSelectedCampaign = null;
					CreateCampaignList();
					mState.Change(STATE.PURCHASE_END);
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), delegate
					{
						mState.Change(STATE.CAMPAIGN_CHARGE);
					});
				}
			}
			break;
		case STATE.PURCHASE_END:
			mState.Change(STATE.UPDATE_GEM);
			break;
		case STATE.UPDATE_GEM:
		{
			if (!mState.IsChanged())
			{
				break;
			}
			CreateErrorMessage(Localization.Get("Purchase success title"), Localization.Get("Purchase success"), STATE.RESTORE_TRANSACTIONS_CHECK);
			PurchaseManager.LogData.PurchaseComplete();
			PurchaseManager.SavePurchaseData();
			if (mGame.mGameStateManager.CurrentState != GameStateManager.GAME_STATE.PUZZLE)
			{
				DateTime now = DateTime.Now;
				int num = -1;
				for (int j = 0; j < mGame.SegmentProfile.ChanceList.Count; j++)
				{
					if (mGame.SegmentProfile.ChanceList != null && j < mGame.SegmentProfile.ChanceList.Count)
					{
						ChanceCommInfo common = mGame.SegmentProfile.ChanceList[j].Common;
						if (common != null && !(now < common.StartTime) && !(common.EndTime < now) && mGame.SegmentProfile.ChanceList[j].Data != null)
						{
							num = j;
							break;
						}
					}
				}
				if (num != -1)
				{
					mGame.Network_ChargePriceCheck(mGame.SegmentProfile.ChanceList[num].Common.Period);
				}
			}
			ITierData tierData = null;
			if (purchaseInfo != null)
			{
				tierData = purchaseInfo.TierData;
			}
			if (tierData == null)
			{
				return;
			}
			string currencyCode = tierData.CurrencyCode;
			int itemNo = tierData.ItemNo;
			string itemID = tierData.ItemID;
			double cost = tierData.Cost;
			int item_num = 1;
			double num2 = cost;
			SingletonMonoBehaviour<Marketing>.Instance.PartytrackSendPayment(itemID, item_num, currencyCode, num2);
			SingletonMonoBehaviour<Marketing>.Instance.AdjustSendPayment(itemNo, currencyCode, num2);
			break;
		}
		case STATE.CLOSE:
			Close();
			break;
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		CampaignListItemUpdate();
		mState.Update();
	}

	private void CreateErrorMessage(string title, string text, STATE nextState)
	{
		ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(title, text, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(delegate
		{
			mState.Change(nextState);
		});
		mState.Change(STATE.ERROR);
	}

	private void CreateErrorMessage(string title, string text, ConfirmDialog.OnDialogClosed onCloseFunc)
	{
		ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(title, text, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(onCloseFunc);
		mState.Change(STATE.ERROR);
	}

	private void CreateErrorRetryMessage(string title, string text, ConfirmDialog.OnDialogClosed onCloseFunc)
	{
		ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(title, text, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
		confirmDialog.SetClosedCallback(onCloseFunc);
		mState.Change(STATE.ERROR);
	}

	private bool CheckResume()
	{
		if (mResumed)
		{
			if (mCurrentMode != MODE.Resume)
			{
				mPausedMode = mCurrentMode;
				ActivateTab(MODE.Resume);
			}
			mState.Reset(STATE.MAINTENACE_CHECK, true);
			return true;
		}
		return false;
	}

	private void CreateCampaignList()
	{
		mCampaignTierList.Clear();
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		List<CampaignData> mCampaignList = mGame.mCampaignList;
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (campaignData.limit_count == 0 && campaignData.is_force_display == 0)
			{
				continue;
			}
			for (int j = 0; j < PurchaseManager.instance.mPItems.Count; j++)
			{
				PItem pItem = PurchaseManager.instance.mPItems[j];
				if ((!GameMain.USE_DEBUG_TIER || !(campaignData.debug_iapname == pItem.ItemID)) && (GameMain.USE_DEBUG_TIER || !(campaignData.iapname.ToLower() == pItem.ItemID.ToLower())))
				{
					continue;
				}
				bool flag = true;
				if (!mGoogleSkuInfoDict.ContainsKey(pItem.itemID))
				{
					flag = false;
				}
				if (flag)
				{
					TierCampaignListItem tierCampaignListItem = new TierCampaignListItem();
					tierCampaignListItem.enable = true;
					tierCampaignListItem.pItem = pItem;
					tierCampaignListItem.googleSkuInfo = mGoogleSkuInfoDict[pItem.itemID];
					tierCampaignListItem.price = tierCampaignListItem.googleSkuInfo.price;
					tierCampaignListItem.name = tierCampaignListItem.googleSkuInfo.title;
					tierCampaignListItem.amount = pItem.amount.ToString();
					tierCampaignListItem.mCampaignData = campaignData;
					if (campaignData.limit_count == 0)
					{
						list.Add(tierCampaignListItem);
					}
					else
					{
						mCampaignTierList.Add(tierCampaignListItem);
					}
				}
				break;
			}
		}
		for (int k = 0; k < list.Count; k++)
		{
			mCampaignTierList.Add(list[k]);
		}
		if (mCampaignTierList.Count == 1)
		{
			mCampaignTierList.Add(new TierCampaignListItem());
		}
		if (mTierCampaignSelectList != null)
		{
			UnityEngine.Object.Destroy(mTierCampaignSelectList.gameObject);
			mTierCampaignSelectList = null;
		}
		mTierCampaignSelectList = SMVerticalList.Make(base.gameObject, this, CampaignTierListInfoEn, mCampaignTierList, -10f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mTierCampaignSelectList.OnCreateItem = OnCreateCampaignItem_Tier;
		mTierCampaignSelectList.OnBeforeCreate = OnBeforeCreateCampaign_Tier;
		mTierCampaignSelectList.OnCreateFinish = OnCreateFinishCampaign_Tier;
	}

	private void OnCreateCampaignItem_Tier(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		UIFont atlasFont = GameMain.LoadFont();
		float cellWidth = mTierCampaignSelectList.mList.cellWidth;
		float num = mTierCampaignSelectList.mList.cellHeight - 17f;
		TierCampaignListItem tierCampaignListItem = (TierCampaignListItem)item;
		if (tierCampaignListItem.mCampaignData == null)
		{
			return;
		}
		UISprite uISprite = Util.CreateSprite("Back", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)num;
		Texture2D texture2D = null;
		for (int i = 0; i < mGame.mCampaignBannerList.Count; i++)
		{
			LinkBannerInfo linkBannerInfo = mGame.mCampaignBannerList[i];
			if (tierCampaignListItem.mCampaignData.iap_campaign_id == linkBannerInfo.param)
			{
				texture2D = linkBannerInfo.banner;
				break;
			}
		}
		if (texture2D != null)
		{
			UITexture uITexture = Util.CreateGameObject("Banner", itemGO).AddComponent<UITexture>();
			uITexture.depth = mBaseDepth + 2;
			uITexture.transform.localPosition = new Vector3(0f, 47f, 0f);
			uITexture.transform.localScale = new Vector3(0.98f, 0.98f);
			uITexture.mainTexture = texture2D;
			uITexture.SetDimensions(texture2D.width, texture2D.height);
		}
		uISprite = Util.CreateSprite("PriceFrame", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 5, new Vector3(0f, -75f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(490, 40);
		UILabel uILabel = Util.CreateLabel("Price", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, tierCampaignListItem.price, mBaseDepth + 6, new Vector3(0f, -75f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(490, 24);
		uISprite = Util.CreateSprite("RemainTimeFrame", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel4", mBaseDepth + 3, new Vector3(-70f, -126f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(350, 40);
		string text = tierCampaignListItem.mCampaignData.RemainTime();
		uILabel = Util.CreateLabel("RemainTime", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 4, new Vector3(-70f, -126f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color32(195, 22, 47, byte.MaxValue);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(320, 24);
		tierCampaignListItem.mRemainTime = uILabel;
		text = "LC_button_buy3";
		if (tierCampaignListItem.mCampaignData.limit_count == 0)
		{
			text = "LC_button_soldout2";
		}
		UIButton uIButton = Util.CreateJellyImageButton("BuyButton@" + index, itemGO, resImage.Image);
		Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 7, new Vector3(180f, -124f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnCampaignBuyPushed", UIButtonMessage.Trigger.OnClick);
		if (tierCampaignListItem.mCampaignData.limit_count == 0)
		{
			uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		tierCampaignListItem.mBuyButton = uIButton;
	}

	public void OnBeforeCreateCampaign_Tier()
	{
		switch (Util.ScreenOrientation)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mTierCampaignSelectList.OnScreenChanged(true);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mTierCampaignSelectList.OnScreenChanged(false);
			break;
		}
	}

	public void OnCreateFinishCampaign_Tier()
	{
		mListCreated = true;
	}

	private void CampaignListItemUpdate()
	{
		if (mTierCampaignSelectList == null || !mTierCampaignSelectList.IsContentsCreated || mResumed)
		{
			return;
		}
		for (int i = 0; i < mCampaignTierList.Count; i++)
		{
			TierCampaignListItem tierCampaignListItem = (TierCampaignListItem)mCampaignTierList[i];
			if (tierCampaignListItem.mCampaignData == null || tierCampaignListItem.mTimeOut)
			{
				continue;
			}
			tierCampaignListItem.mRemainTime.text = tierCampaignListItem.mCampaignData.RemainTime();
			if (!tierCampaignListItem.mCampaignData.InTime())
			{
				tierCampaignListItem.mRemainTime.text = tierCampaignListItem.mCampaignData.RemainTime();
				if (tierCampaignListItem.mCampaignData.limit_count != 0)
				{
					tierCampaignListItem.mBuyButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
				}
				tierCampaignListItem.mTimeOut = true;
			}
		}
	}

	public override void OnPushedDisplay()
	{
		if (mState.GetStatus() == STATE.TIER_SELECT)
		{
			mEULADialog = Util.CreateGameObject("EULADialog", base.gameObject).AddComponent<EULADialog>();
			mEULADialog.Init(EULADialog.MODE.AS);
			mEULADialog.SetCallback(OnEULADialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
	}

	public override void OnEULADialogClosed(EULADialog.SELECT_ITEM item)
	{
		mEULADialog = null;
		mState.Reset(STATE.TIER_SELECT, true);
	}

	private void OnConnectErrorCampaignChargeRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorCampaignChargeAbandon()
	{
		mState.Change(STATE.PURCHASE_END);
	}

	private void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorAuthAbandon()
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	private void OnMaintenancheDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		mState.Reset(STATE.TIER_SELECT, true);
		OnCancelPushed(null);
	}

	private void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		if (i == ConnectCommonErrorDialog.SELECT_ITEM.RETRY)
		{
			mState.Change((STATE)state);
			return;
		}
		mState.Reset(STATE.TIER_SELECT, true);
		OnCancelPushed(null);
	}
}
