using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BIJSNSGooglePlus : BIJSNSAppSwitcher
{
	public class BIJSNSGooglePlusCallback : BIJSNSEventCallback
	{
		public BIJSNSGooglePlusCallback(BIJSNS _sns, int seqno)
			: base(_sns, seqno)
		{
		}

		public override void Register()
		{
			GooglePlusIF.authenticationSucceededEvent = (Action<string>)Delegate.Combine(GooglePlusIF.authenticationSucceededEvent, new Action<string>(OnLoginSucceed));
			GooglePlusIF.authenticationFailedEvent = (Action<string>)Delegate.Combine(GooglePlusIF.authenticationFailedEvent, new Action<string>(OnLoginFailed));
			GooglePlusIF.loadPersonSucceededEvent = (Action<object>)Delegate.Combine(GooglePlusIF.loadPersonSucceededEvent, new Action<object>(OnLoadPersonSucceed));
			GooglePlusIF.loadPersonFailedEvent = (Action<string>)Delegate.Combine(GooglePlusIF.loadPersonFailedEvent, new Action<string>(OnLoadPersonFailed));
			GooglePlusIF.loadVisiblePeopleSucceededEvent = (Action<object>)Delegate.Combine(GooglePlusIF.loadVisiblePeopleSucceededEvent, new Action<object>(OnLoadVisiblePeopleSucceed));
			GooglePlusIF.loadVisiblePeopleFailedEvent = (Action<string>)Delegate.Combine(GooglePlusIF.loadVisiblePeopleFailedEvent, new Action<string>(OnLoadVisiblePeopleFailed));
			GooglePlusIF.interactivePostSucceededEvent = (Action<string>)Delegate.Combine(GooglePlusIF.interactivePostSucceededEvent, new Action<string>(OnInteractivePostSucceeded));
			GooglePlusIF.interactivePostFailedEvent = (Action<string>)Delegate.Combine(GooglePlusIF.interactivePostFailedEvent, new Action<string>(OnInteractivePostFailed));
			base.Register();
		}

		public override void Unregister()
		{
			GooglePlusIF.authenticationSucceededEvent = (Action<string>)Delegate.Remove(GooglePlusIF.authenticationSucceededEvent, new Action<string>(OnLoginSucceed));
			GooglePlusIF.authenticationFailedEvent = (Action<string>)Delegate.Remove(GooglePlusIF.authenticationFailedEvent, new Action<string>(OnLoginFailed));
			GooglePlusIF.loadPersonSucceededEvent = (Action<object>)Delegate.Remove(GooglePlusIF.loadPersonSucceededEvent, new Action<object>(OnLoadPersonSucceed));
			GooglePlusIF.loadPersonFailedEvent = (Action<string>)Delegate.Remove(GooglePlusIF.loadPersonFailedEvent, new Action<string>(OnLoadPersonFailed));
			GooglePlusIF.loadVisiblePeopleSucceededEvent = (Action<object>)Delegate.Remove(GooglePlusIF.loadVisiblePeopleSucceededEvent, new Action<object>(OnLoadVisiblePeopleSucceed));
			GooglePlusIF.loadVisiblePeopleFailedEvent = (Action<string>)Delegate.Remove(GooglePlusIF.loadVisiblePeopleFailedEvent, new Action<string>(OnLoadVisiblePeopleFailed));
			GooglePlusIF.interactivePostSucceededEvent = (Action<string>)Delegate.Remove(GooglePlusIF.interactivePostSucceededEvent, new Action<string>(OnInteractivePostSucceeded));
			GooglePlusIF.interactivePostFailedEvent = (Action<string>)Delegate.Remove(GooglePlusIF.interactivePostFailedEvent, new Action<string>(OnInteractivePostFailed));
			base.Unregister();
		}

		private static string ToJsonStr(object o)
		{
			if (o == null)
			{
				return "(null)";
			}
			string empty = string.Empty;
			try
			{
				return MiniJSON.jsonEncode(o).ToString();
			}
			catch (Exception)
			{
				return o.ToString();
			}
		}

		protected virtual void OnLoginSucceed(string result)
		{
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, result));
		}

		protected virtual void OnLoginFailed(string error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}

		protected virtual void OnLoadPersonSucceed(object result)
		{
			IBIJSNSUser detail;
			try
			{
				Hashtable hashtable = result as Hashtable;
				BIJSNSUser bIJSNSUser = new BIJSNSUser();
				if (hashtable.ContainsKey("id"))
				{
					bIJSNSUser.ID = BIJSNSEventCallback.ToSS(hashtable["id"]);
				}
				if (hashtable.ContainsKey("name"))
				{
					bIJSNSUser.Name = BIJSNSEventCallback.ToSS(hashtable["name"]);
				}
				if (hashtable.ContainsKey("gender"))
				{
					bIJSNSUser.Gender = BIJSNSEventCallback.ToSS(hashtable["gender"]);
				}
				if (string.IsNullOrEmpty(bIJSNSUser.ID))
				{
					throw new Exception("no id");
				}
				detail = bIJSNSUser;
			}
			catch (Exception ex)
			{
				Parent.ReplyEvent(SeqNo, new Response(1, "Google+ get user info response error: " + ex.ToString(), result));
				return;
			}
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, detail));
		}

		protected virtual void OnLoadPersonFailed(string error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}

		protected virtual void OnLoadVisiblePeopleSucceed(object result)
		{
			List<IBIJSNSUser> list = new List<IBIJSNSUser>();
			try
			{
				Hashtable hashtable = result as Hashtable;
				if (hashtable.ContainsKey("data"))
				{
					ArrayList arrayList = hashtable["data"] as ArrayList;
					if (arrayList != null && arrayList.Count > 0)
					{
						for (int i = 0; i < arrayList.Count; i++)
						{
							Hashtable hashtable2 = arrayList[i] as Hashtable;
							BIJSNSUser bIJSNSUser = new BIJSNSUser();
							if (hashtable2.ContainsKey("id"))
							{
								bIJSNSUser.ID = BIJSNSEventCallback.ToSS(hashtable2["id"]);
							}
							if (hashtable2.ContainsKey("name"))
							{
								bIJSNSUser.Name = BIJSNSEventCallback.ToSS(hashtable2["name"]);
							}
							if (hashtable2.ContainsKey("gender"))
							{
								bIJSNSUser.Gender = BIJSNSEventCallback.ToSS(hashtable2["gender"]);
							}
							if (!string.IsNullOrEmpty(bIJSNSUser.ID))
							{
								list.Add(bIJSNSUser);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Parent.ReplyEvent(SeqNo, new Response(1, "Google+ get friends response error: " + ex.ToString(), result));
				return;
			}
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, list));
		}

		protected virtual void OnLoadVisiblePeopleFailed(string error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}

		protected virtual void OnInteractivePostSucceeded(object result)
		{
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, result));
		}

		protected virtual void OnInteractivePostFailed(object error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}
	}

	private static readonly Dictionary<string, string> USER_IMAGE_URL = new Dictionary<string, string>();

	private static object LOCK_URL = new object();

	public override SNS Kind
	{
		get
		{
			return SNS.GooglePlus;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool Init(GameObject go, MonoBehaviour mb)
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		bool result = base.Init(go, mb);
		GooglePlusIF.getUserImageURLEvent = (Action<string, string>)Delegate.Combine(GooglePlusIF.getUserImageURLEvent, new Action<string, string>(OnGetUserImageURL));
		float num = Time.realtimeSinceStartup - realtimeSinceStartup;
		return result;
	}

	public override void Term()
	{
		GooglePlusIF.getUserImageURLEvent = (Action<string, string>)Delegate.Remove(GooglePlusIF.getUserImageURLEvent, new Action<string, string>(OnGetUserImageURL));
		base.Term();
	}

	protected bool GetUserImageURL(string userid, out string url)
	{
		return GetUserImageURL(userid, out url, false);
	}

	protected bool GetUserImageURL(string userid, out string url, bool afterRemove)
	{
		url = null;
		if (userid == null)
		{
			return false;
		}
		lock (LOCK_URL)
		{
			if (USER_IMAGE_URL.TryGetValue(userid, out url))
			{
				if (afterRemove)
				{
					return USER_IMAGE_URL.Remove(userid);
				}
				return true;
			}
		}
		return false;
	}

	protected virtual void OnGetUserImageURL(string userid, string url)
	{
		lock (LOCK_URL)
		{
			USER_IMAGE_URL[userid] = url;
		}
	}

	protected override Response OnBackToGameResult(Request req)
	{
		Response response = null;
		if (req.type == 2)
		{
			if (IsLogined())
			{
				return new Response(0, "Back to game", null);
			}
			return new Response(1, "Back to game", null);
		}
		return new Response(0, "Back to game", null);
	}

	public override IEventCallback CreateEventCallback(int seqno)
	{
		return new BIJSNSGooglePlusCallback(this, seqno);
	}
}
