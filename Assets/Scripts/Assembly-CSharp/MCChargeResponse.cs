public class MCChargeResponse : ParameterObject<MCChargeResponse>
{
	public int mc { get; set; }

	public int sc { get; set; }

	public int code { get; set; }

	public int code_ex { get; set; }

	public long server_time { get; set; }
}
