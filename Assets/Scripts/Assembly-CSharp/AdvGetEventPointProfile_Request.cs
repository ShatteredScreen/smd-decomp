public class AdvGetEventPointProfile_Request : RequestBase
{
	[MiniJSONAlias("event_ids")]
	public short[] EventIDs { get; set; }
}
