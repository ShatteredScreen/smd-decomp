using System;
using System.Reflection;
using System.Text;

public class AdInfo : ICloneable
{
	public bool Enabled { get; set; }

	public string Id { get; set; }

	public AdInfo()
		: this(string.Empty, false)
	{
	}

	public AdInfo(string id, bool enabled)
	{
		Id = id;
		Enabled = enabled;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdInfo Clone()
	{
		return MemberwiseClone() as AdInfo;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[{0}: {1}]", type.Name, stringBuilder.ToString());
	}
}
