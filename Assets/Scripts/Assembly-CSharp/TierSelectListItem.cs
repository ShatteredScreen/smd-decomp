using System;
using Prime31;

public class TierSelectListItem : SMVerticalListItem, IComparable
{
	public bool enable;

	public string amount = "0";

	public string price = "0";

	public string name = "none";

	public CampaignSaleEntry mCampaignSaleEntry;

	public PItem pItem;

	public GoogleSkuInfo googleSkuInfo;

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as TierSelectListItem);
	}

	public int CompareTo(TierSelectListItem _obj)
	{
		if (mCampaignSaleEntry == null)
		{
			return int.MaxValue;
		}
		if (_obj != null && _obj.mCampaignSaleEntry != null)
		{
			return mCampaignSaleEntry.No - _obj.mCampaignSaleEntry.No;
		}
		return int.MaxValue;
	}

	public static int CompareByShowId(TierSelectListItem a, TierSelectListItem b)
	{
		return a.mCampaignSaleEntry.ShowId - b.mCampaignSaleEntry.ShowId;
	}
}
