public class StoryCommandData
{
	public string command;

	public string param0;

	public string param1;

	public string param2;

	public string param3;

	public string param4;

	public string param5;

	public StoryCommandData()
	{
		command = "NOP";
		param0 = "0";
		param1 = "0";
		param2 = "0";
		param3 = "0";
		param4 = "0";
		param5 = "0";
	}

	public void deserialize(BIJBinaryReader stream)
	{
		command = stream.ReadUTF();
		param0 = stream.ReadUTF();
		param1 = stream.ReadUTF();
		param2 = stream.ReadUTF();
		param3 = stream.ReadUTF();
		param4 = stream.ReadUTF();
		param5 = stream.ReadUTF();
	}

	public StoryCommandData Clone()
	{
		return (StoryCommandData)MemberwiseClone();
	}
}
