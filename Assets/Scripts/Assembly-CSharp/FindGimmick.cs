using System;
using System.Collections.Generic;
using UnityEngine;

public class FindGimmick : MonoBehaviour
{
	public enum STATE
	{
		STAY = 0,
		COLLECT = 1,
		COLLECT_END = 2
	}

	public delegate void AllCollectDelegate(FindGimmick gimmick, Vector3 position, string overWriteImage);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.STAY);

	private FixedSprite mSprite;

	private IntVector2 mSize;

	private int mRotateDeg;

	private List<IntVector2> mRemainTiles = new List<IntVector2>();

	private float mAngle;

	private float mKillTimer;

	public Def.TILE_KIND mKind;

	private string mImageName;

	public AllCollectDelegate OnAllCollect = delegate
	{
	};

	private void Start()
	{
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.COLLECT:
		{
			Vector3 localPosition = mSprite.transform.localPosition;
			if (mState.IsChanged())
			{
				mSprite.transform.localPosition = new Vector3(localPosition.x, localPosition.y, -5f);
				mAngle = 0f;
				break;
			}
			mAngle += Time.deltaTime * 360f;
			if (mAngle >= 90f)
			{
				mAngle = 90f;
				mState.Change(STATE.COLLECT_END);
			}
			float num = Mathf.Sin(mAngle * ((float)Math.PI / 180f));
			mSprite.transform.localRotation = Quaternion.Euler(0f, 0f, (float)mRotateDeg * (1f - num));
			mSprite.transform.localPosition = new Vector3(localPosition.x * num, localPosition.y * num, -5f);
			num = Mathf.Sin(mAngle * 2f * ((float)Math.PI / 180f));
			mSprite.transform.localScale = new Vector3(1f + num * 0.3f, 1f + num * 0.3f, 1f);
			break;
		}
		case STATE.COLLECT_END:
			if (mState.IsChanged())
			{
				mKillTimer = 0.2f;
				break;
			}
			mKillTimer -= Time.deltaTime;
			if (mKillTimer <= 0f)
			{
				OnAllCollect(this, base.transform.position, mImageName);
				UnityEngine.Object.Destroy(base.gameObject);
			}
			break;
		}
		mState.Update();
	}

	public void Init(ref StencilData data)
	{
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_TILE");
		mImageName = data.ImageName;
		mSprite = Util.CreateGameObject("Image", base.gameObject).AddComponent<FixedSprite>();
		mSprite.Init("PUZZLETILE_FIXED_SPRITE", data.ImageName, Vector3.zero, resImage.Image);
		mRotateDeg = data.Degree;
		mSprite.transform.localScale = new Vector3(Def.TILE_SCALE, Def.TILE_SCALE, 1f);
		mSprite.transform.localRotation = Quaternion.Euler(0f, 0f, mRotateDeg);
		mKind = data.StencilType;
		mSize = data.Size;
		float x = (float)mSize.x * Def.TILE_PITCH_H / 2f - Def.TILE_PITCH_H / 2f;
		float y = (float)mSize.y * Def.TILE_PITCH_V / 2f - Def.TILE_PITCH_V / 2f;
		mSprite.transform.localPosition = new Vector3(x, y, 0f);
		for (int i = 0; i < mSize.x; i++)
		{
			for (int j = 0; j < mSize.y; j++)
			{
				mRemainTiles.Add(new IntVector2(data.Position.x + i, data.Position.y + j));
			}
		}
	}

	public bool OnAttributeCrush(IntVector2 tilePos)
	{
		int count = mRemainTiles.Count;
		bool result = false;
		for (int num = count - 1; num >= 0; num--)
		{
			if (mRemainTiles[num].x == tilePos.x && mRemainTiles[num].y == tilePos.y)
			{
				mRemainTiles.RemoveAt(num);
				result = true;
				if (mRemainTiles.Count == 0)
				{
					SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_FIND_COMPLETE", -1);
					mState.Change(STATE.COLLECT);
				}
			}
		}
		return result;
	}

	public bool CollectStartCheck()
	{
		return mRemainTiles.Count == 0;
	}
}
