using UnityEngine;

public class GemUseConfirmDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i, int aValue);

	private SELECT_ITEM mSelectItem;

	private int mPrice;

	private bool mButtonSeEnable = true;

	private int mValue;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("GemConfirmation_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		float y = 50f;
		float y2 = -90f;
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		titleDescKey = string.Format(Localization.Get("GemConfirmation_Desc"), mPrice);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, y, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, y2, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(130f, y2, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(int aPrice, int aCallbackValue, bool openSeEnable = true)
	{
		mPrice = aPrice;
		mValue = aCallbackValue;
		SetOpenSeEnable(openSeEnable);
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem, mValue);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (mButtonSeEnable)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
			}
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (mButtonSeEnable)
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetButtonSeEnable(bool enable)
	{
		mButtonSeEnable = enable;
	}
}
