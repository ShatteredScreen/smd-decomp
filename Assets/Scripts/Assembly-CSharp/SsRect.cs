using System;
using UnityEngine;

[Serializable]
public class SsRect : SsInterpolatable
{
	public int Left;

	public int Top;

	public int Right;

	public int Bottom;

	public int Width
	{
		get
		{
			return Right - Left;
		}
	}

	public int Height
	{
		get
		{
			return Bottom - Top;
		}
	}

	public SsRect()
	{
	}

	public SsRect(SsRect r)
	{
		Left = r.Left;
		Top = r.Top;
		Right = r.Right;
		Bottom = r.Bottom;
	}

	public static SsRect[] CreateArray(int num)
	{
		SsRect[] array = new SsRect[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = new SsRect();
		}
		return array;
	}

	public SsRect Clone()
	{
		return new SsRect(this);
	}

	public Vector2 WH()
	{
		return new Vector2(Right - Left, Bottom - Top);
	}

	public override string ToString()
	{
		return "Left: " + Left + ", Top: " + Top + ", Right: " + Right + ", Bottom: " + Bottom;
	}

	public SsInterpolatable GetInterpolated(SsCurveParams curve, float time, SsInterpolatable start, SsInterpolatable end, int startTime, int endTime)
	{
		SsRect ssRect = new SsRect();
		return ssRect.Interpolate(curve, time, start, end, startTime, endTime);
	}

	public SsInterpolatable Interpolate(SsCurveParams curve, float time, SsInterpolatable start_, SsInterpolatable end_, int startTime, int endTime)
	{
		SsRect ssRect = (SsRect)start_;
		SsRect ssRect2 = (SsRect)end_;
		Left = SsInterpolation.Interpolate(curve, time, ssRect.Left, ssRect2.Left, startTime, endTime);
		Top = SsInterpolation.Interpolate(curve, time, ssRect.Top, ssRect2.Top, startTime, endTime);
		Right = SsInterpolation.Interpolate(curve, time, ssRect.Right, ssRect2.Right, startTime, endTime);
		Bottom = SsInterpolation.Interpolate(curve, time, ssRect.Bottom, ssRect2.Bottom, startTime, endTime);
		return this;
	}

	public static explicit operator Rect(SsRect s)
	{
		Rect result = default(Rect);
		result.xMin = s.Left;
		result.xMax = s.Right;
		result.yMin = s.Top;
		result.yMax = s.Bottom;
		return result;
	}
}
