using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameStateEventBingo : GameStateSMMapBase
{
	public enum STATE
	{
		NONE = 0,
		LOAD_WAIT = 1,
		UNLOAD_WAIT = 2,
		INIT = 3,
		UNLOCK_ACTING = 4,
		MAIN = 5,
		DLLISTHASH_WAIT = 6,
		COURSECLEAR_WAIT = 7,
		COURSECOMPLETE_WAIT = 8,
		REACH_LINE_EFFECT = 9,
		REACH_LINE_EFFECT_WAIT = 10,
		REACH_EFFECT = 11,
		REACH_EFFECT_WAIT = 12,
		BINGO_LINE_EFFECT = 13,
		BINGO_LINE_EFFECT_WAIT = 14,
		BINGO_EFFECT = 15,
		BINGO_EFFECT_WAIT = 16,
		ALLBINGO_EFFECT = 17,
		ALLBINGO_EFFECT_WAIT = 18,
		STARCOMP_EFFECT = 19,
		STARCOMP_EFFECT_WAIT = 20,
		REWARD_INIT = 21,
		REWARD_EFFECT = 22,
		NEWCOURSE_OPEN = 23,
		NEWCOURSE_OPEN_WAIT = 24,
		LOTTERY_EFFECT = 25,
		LOTTERY_EFFECT_WAIT = 26,
		GETREWARD_INIT = 27,
		MOVEWAIT = 28,
		WAIT = 29,
		PLAY = 30,
		PAUSE = 31,
		WEBVIEW = 32,
		WEBVIEW_WAIT = 33,
		NETWORK_CONNECT_WAIT = 34,
		NETWORK_PROLOGUE = 35,
		NETWORK_MAINTENANCE = 36,
		NETWORK_MAINTENANCE_WAIT = 37,
		NETWORK_AUTH = 38,
		NETWORK_AUTH_WAIT = 39,
		NETWORK_GAMEINFO = 40,
		NETWORK_GAMEINFO_WAIT = 41,
		NETWORK_SEGMENTINFO = 42,
		NETWORK_SEGMENTINFO_WAIT = 43,
		NETWORK_LOGINBONUS = 44,
		NETWORK_LOGINBONUS_WAIT = 45,
		NETWORK_MAILINIT = 46,
		NETWORK_ADV_GET_MAIL = 47,
		NETWORK_ADV_GET_MAIL_WAIT = 48,
		NETWORK_SUPPORTGIFT = 49,
		NETWORK_SUPPORTGIFT_WAIT = 50,
		NETWORK_GIFT = 51,
		NETWORK_GIFT_WAIT = 52,
		NETWORK_FRIEND = 53,
		NETWORK_FRIEND_WAIT = 54,
		NETWORK_FRIEND_REPEAT = 55,
		NETWORK_SUPPURCHASE = 56,
		NETWORK_SUPPURCHASE_WAIT = 57,
		NETWORK_MAILEND = 58,
		NETWORK_MYFRIENDLIST = 59,
		NETWORK_MYFRIENDLIST_WAIT = 60,
		NETWORK_CAMPAIGN = 61,
		NETWORK_CAMPAIGN_WAIT = 62,
		NETWORK_EPILOGUE = 63,
		NETWORK_REWARD_MAINTENACE = 64,
		NETWORK_REWARD_MAINTENACE_WAIT = 65,
		NETWORK_REWARD_SET = 66,
		NETWORK_REWARD_SET_WAIT = 67,
		NETWORK_ADV_REWARD_SET = 68,
		NETWORK_ADV_REWARD_WAIT = 69,
		NETWORK_REWARD_MAINTENACE2 = 70,
		NETWORK_REWARD_MAINTENACE2_00 = 71,
		NETWORK_REWARD_SET2 = 72,
		NETWORK_REWARD_SET2_00 = 73,
		NETWORK_RESUME_MAINTENACE = 74,
		NETWORK_RESUME_MAINTENACE_00 = 75,
		NETWORK_RESUME_GAMEINFO = 76,
		NETWORK_RESUME_GAMEINFO_00 = 77,
		NETWORK_RESUME_CAMPAIGN = 78,
		NETWORK_RESUME_CAMPAIGN_00 = 79,
		NETWORK_RESUME_LOGINBONUS = 80,
		NETWORK_RESUME_LOGINBONUS_00 = 81,
		NETWORK_STAGE = 82,
		NETWORK_STAGE_WAIT = 83,
		NETWORK_NOTICE_WAIT = 84,
		AUTHERROR_RETURN_TITLE = 85,
		AUTHERROR_RETURN_WAIT = 86,
		MAINTENANCE_RETURN_TITLE = 87,
		MAINTENANCE_RETURN_WAIT = 88,
		GOTO_REENTER = 89
	}

	private class BingoRewardListData : SMVerticalListItem
	{
		public string mImgName;

		public int mNum;

		public float mScaleX;

		public float mScaleY;

		public bool mIsPartner;

		public bool mIsGot;

		public BingoRewardListData()
		{
			mImgName = string.Empty;
			mNum = 0;
			mScaleX = 0.6f;
			mScaleY = 0.6f;
			mIsPartner = false;
			mIsGot = false;
		}
	}

	private const int BINGO_NUM_IN_SHEET = 8;

	private const float LOTTERY_SKIP_FRAME = 94f;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private STATE mRewardErrorPrevState = STATE.MAIN;

	private string mGuID;

	private Dictionary<int, GameObject> mRewardPanelGoDict = new Dictionary<int, GameObject>();

	private List<BoxCollider> mRewardPanelBoxColliderList = new List<BoxCollider>();

	private GameObject mBGRoot;

	protected OneShotAnime mSignAnimation;

	protected OneShotAnime mClearAnimation;

	protected OneShotAnime mCompleteAnimation;

	protected bool mIsNextCourseDialogFlg;

	private EventMiniDialog mEventMiniDialog;

	protected bool mIsFinishedCourseMove;

	private EventImageDialog mEventImageDialog;

	private string DEFAULT_EVENT_BGM = "BGM_MAP_F";

	private bool mGetRewardInfoConnectComplete;

	private SlideBingoPanel mSlideBingoPanel;

	private List<SMEventRallyPage> mCourseList = new List<SMEventRallyPage>();

	private SMEventPageData mEventPageData;

	protected short CurrentCourseID = -1;

	private short NewOpenCourseId = -1;

	private int mPrevBingoNum;

	private int mTotalBingoNum;

	private Dictionary<int, AccessoryData> mGetAccessoryDict = new Dictionary<int, AccessoryData>();

	private Dictionary<int, AccessoryData> mGetAlterAccessoryDict;

	private Dictionary<int, bool> mGetAccessoryEffectFinishedDict = new Dictionary<int, bool>();

	private List<AccessoryData> mGemAccessoryList = new List<AccessoryData>();

	private List<AccessoryData> mAdvAccessoryList = new List<AccessoryData>();

	private List<AccessoryData> mIsGotAccessoryList = new List<AccessoryData>();

	private List<int> mGotRewardStampList = new List<int>();

	private AccessoryData mGetPartner;

	private UIPanel mLotteryEffectPanel;

	private bool mLotteryEffectFinished;

	private PartsChangedSprite mLotteryAnime;

	private GameObject mCameraRoot;

	private GameObject mLotteryCameraBR;

	private UIButton mLotterySkipButton;

	protected Vector3 mSkipButtonPos = Vector3.zero;

	protected float mSkipButtonScale = 1f;

	private ReLotteryDialog.MODE mReLotteryMode = ReLotteryDialog.MODE.HEART;

	private UISprite mRewardPanel;

	private UISprite mNextRewardSprite;

	private bool mGetGemItem;

	private bool mGetAdvItem;

	private UISsSprite mSheeCompSsAnime;

	private short[] mStageClearNumArray;

	private STATE mStateAfterConnect = STATE.NETWORK_EPILOGUE;

	private STATE mBootConnectErrorState;

	public STATE StateStatus = STATE.WAIT;

	public STATE OverwriteState = STATE.WAIT;

	private bool mMainStateChangeNow;

	protected bool mStartWebview;

	public float Event_DeviceOffset;

	private SMVerticalListInfo mRewardListInfo;

	private SMVerticalList mRewardList;

	private List<SMVerticalListItem> mList = new List<SMVerticalListItem>();

	private int GetRewardInfoConnectMode;

	private short lotteryStageNum;

	private short[] mBingoLine = new short[24]
	{
		1, 2, 3, 4, 5, 6, 7, 8, 9, 1,
		4, 7, 2, 5, 8, 3, 6, 9, 1, 5,
		9, 3, 5, 7
	};

	private List<short> mReachList = new List<short>();

	private bool mReachEffectFinished;

	private UISsSprite mReachSsAnime;

	private List<short> mBingoList = new List<short>();

	private bool mBingoEffectFinished;

	private UISsSprite mBingoSsAnime;

	private List<short> mBingoList_Cram = new List<short>();

	private bool mAllBingoEffectFinished;

	private UISsSprite mAllBingoSsAnime;

	private bool mStarCompEffectFinished;

	private UISsSprite mStarCompSsAnime;

	private bool mIsStarCompEffect;

	private bool mFirstGuideDialogStart;

	private bool mNewCourseGuideDialogStart;

	protected override void RegisterServerCallback()
	{
		Server.GetEventStageRankingDataEvent += Server_OnGetStageRankingData;
	}

	protected override void UnregisterServerCallback()
	{
		Server.GetEventStageRankingDataEvent -= Server_OnGetStageRankingData;
	}

	protected override void Network_OnStage()
	{
		STATE aStatus = STATE.WAIT;
		int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		int currentLevel = playerMapData.CurrentLevel;
		base.RankingData = null;
		mState.Change(STATE.NETWORK_STAGE_WAIT);
		bool flag = false;
		if (!Server.GetEventStageRankingData(currentSeries, currentLevel, CurrentEventID, CurrentCourseID))
		{
			mState.Change(aStatus);
		}
	}

	protected override void Network_OnStageWait()
	{
		_GetRankingData rankingData = base.RankingData;
		if (rankingData != null && rankingData.results.Count > 0)
		{
			mState.Change(STATE.WAIT);
			if (EnableSocialRankingDialog())
			{
				ShowSocialRankingDialog(rankingData.series, rankingData.stage, rankingData.results);
			}
		}
	}

	protected virtual void Server_OnGetStageRankingData(int result, int code, string json)
	{
		if (mState.GetStatus() != STATE.NETWORK_STAGE_WAIT || result != 0)
		{
			return;
		}
		try
		{
			_GetRankingData obj = new _GetRankingData();
			new MiniJSONSerializer().Populate(ref obj, json);
			if (obj != null && obj.results != null)
			{
				for (int num = obj.results.Count - 1; num >= 0; num--)
				{
					if (obj.results[num].uuid == mGame.mPlayer.UUID)
					{
						int num2 = 0;
						PlayerEventData data;
						mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
						PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
						PlayerClearData _psd;
						if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, CurrentCourseID, playerMapData.CurrentLevel, out _psd))
						{
							num2 = _psd.HightScore;
						}
						if (num2 <= 0)
						{
							obj.results.RemoveAt(num);
						}
					}
					else if (!mGame.mPlayer.Friends.ContainsKey(obj.results[num].uuid))
					{
						obj.results.RemoveAt(num);
					}
				}
				obj.results.Sort();
			}
			base.RankingData = obj;
		}
		catch (Exception)
		{
		}
	}

	public override void Start()
	{
		base.Start();
		RegisterServerCallback();
		CurrentEventID = mGame.mPlayer.Data.CurrentEventID;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		CurrentCourseID = data.CourseID;
	}

	public void OnDestroy()
	{
		UnregisterServerCallback();
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
	}

	private void StatusUpdate(STATE a_current, STATE a_status)
	{
		if (a_status == STATE.MAIN)
		{
			return;
		}
		if (mCockpit != null)
		{
			mCockpit.SetEnableButtonAction(false);
		}
		if (mSlideBingoPanel != null)
		{
			mSlideBingoPanel.SetEnableButtonAction(false);
		}
		if (mRewardPanelBoxColliderList == null)
		{
			return;
		}
		for (int i = 0; i < mRewardPanelBoxColliderList.Count; i++)
		{
			BoxCollider boxCollider = mRewardPanelBoxColliderList[i];
			if (boxCollider != null)
			{
				boxCollider.enabled = false;
			}
		}
	}

	public override void Update()
	{
		DragPower = Vector2.zero;
		base.Update();
		if (OverwriteState != STATE.WAIT)
		{
			mState.Change(OverwriteState);
			OverwriteState = STATE.WAIT;
		}
		StateStatus = mState.GetStatus();
		if (mState.IsChanged())
		{
			StatusUpdate(StateStatus, StateStatus);
		}
		switch (StateStatus)
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mState.Change(STATE.NETWORK_PROLOGUE);
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_STAGE:
			Network_OnStage();
			break;
		case STATE.NETWORK_STAGE_WAIT:
			Network_OnStageWait();
			break;
		case STATE.NETWORK_CONNECT_WAIT:
			if (!mIsConnecting)
			{
				if (!mHasBootConnectingError)
				{
					mConnectionTime = Time.realtimeSinceStartup;
					mState.Change(STATE.NETWORK_PROLOGUE);
				}
				else if (mBootConnectErrorState != 0)
				{
					mState.Reset(mBootConnectErrorState, true);
				}
			}
			break;
		case STATE.GOTO_REENTER:
			if (GameMain.USE_DEBUG_DIALOG)
			{
				mGameState.SetNextGameState(mGame.GetNextGameState());
				mState.Change(STATE.UNLOAD_WAIT);
			}
			else
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
				mState.Change(STATE.UNLOAD_WAIT);
			}
			break;
		case STATE.INIT:
			if (mState.IsChanged())
			{
				if (MaintenanceModeStartCheck())
				{
					mState.Reset(STATE.WAIT, true);
					break;
				}
				UpdateReLotteryBottonStatus();
			}
			IsAllBingoSheet();
			if (!ReachEffectStartCheck() && !BingoEffectStartCheck())
			{
				if (AccessoryConnectCheck())
				{
					mState.Reset(STATE.NETWORK_REWARD_MAINTENACE2, true);
				}
				else if (!RewardEffectStartCheck() && !AlternateRewardCheck() && !AllBingoEffectStartCheck() && !StarCompEffectStartCheck() && !GuideDialogStartCheck())
				{
					GiftAttentionCheck();
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			break;
		case STATE.UNLOCK_ACTING:
			if (mState.IsChanged() && MaintenanceModeStartCheck())
			{
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				Update_UNLOCK_ACTING();
			}
			break;
		case STATE.MAIN:
		{
			if (mState.IsChanged())
			{
				mMainStateChangeNow = true;
				UpdateReLotteryBottonStatus();
				if (MaintenanceModeStartCheck())
				{
					mState.Reset(STATE.WAIT, true);
					break;
				}
				switch (DLFilelistHashCheck())
				{
				case DLFILE_CHECK_RESULT.FILE_DESTROY:
					mState.Reset(STATE.WAIT, true);
					break;
				case DLFILE_CHECK_RESULT.HASH_NG:
					mState.Reset(STATE.WAIT, true);
					break;
				default:
					if (!CheaterStartCheck() && !WebviewStartCheck() && !GiftStartCheck() && !RankingRankUpStarStartCheck() && !OpenNewCourseStartCheck())
					{
						if (mFirstGuideDialogStart)
						{
							mFirstGuideDialogStart = false;
							StartCoroutine(GrayOut());
							BingoGuideDialog bingoGuideDialog = Util.CreateGameObject("BingoGuideDialog", mRoot).AddComponent<BingoGuideDialog>();
							bingoGuideDialog.Init(0);
							bingoGuideDialog.SetClosedCallback(OnBingoGuideDialogClosed);
							mState.Change(STATE.WAIT);
						}
						else if (mNewCourseGuideDialogStart)
						{
							mNewCourseGuideDialogStart = false;
							StartCoroutine(GrayOut());
							BingoGuideDialog bingoGuideDialog2 = Util.CreateGameObject("BingoGuideDialog", mRoot).AddComponent<BingoGuideDialog>();
							bingoGuideDialog2.Init(4);
							bingoGuideDialog2.SetClosedCallback(OnBingoGuideDialogClosed);
							mState.Change(STATE.WAIT);
						}
						else if (LotteryStartCheck())
						{
							EventBingoCockpit eventBingoCockpit2 = mCockpit as EventBingoCockpit;
							eventBingoCockpit2.MoveReLotteryButton(true);
						}
					}
					break;
				}
				break;
			}
			if (mSocialRanking != null)
			{
				mSocialRanking.Close();
				mSocialRanking = null;
			}
			Update_MAIN();
			UpdateMugenHeartStatus();
			mGame.SetMusicVolume(1f, 0);
			if (!mMainStateChangeNow)
			{
				break;
			}
			mMainStateChangeNow = false;
			if (mCockpit != null)
			{
				mCockpit.SetEnableButtonAction(true);
			}
			if (mSlideBingoPanel != null)
			{
				mSlideBingoPanel.SetEnableButtonAction(true);
			}
			if (mRewardPanelBoxColliderList == null)
			{
				break;
			}
			for (int i = 0; i < mRewardPanelBoxColliderList.Count; i++)
			{
				BoxCollider boxCollider = mRewardPanelBoxColliderList[i];
				if (boxCollider != null)
				{
					boxCollider.enabled = true;
				}
			}
			break;
		}
		case STATE.REACH_LINE_EFFECT:
			mGame.SetMusicVolume(0.4f, 0);
			mSlideBingoPanel.CreateReachLine(mReachList, CurrentCourseID);
			mState.Change(STATE.REACH_LINE_EFFECT_WAIT);
			break;
		case STATE.REACH_LINE_EFFECT_WAIT:
			if (mSlideBingoPanel.ReachEffectFinished())
			{
				StartCoroutine(GrayOut());
				mState.Reset(STATE.REACH_EFFECT, true);
			}
			break;
		case STATE.REACH_EFFECT:
			mCameraRoot = CreateDemoCamera();
			mReachSsAnime = Util.CreateUISsSprite("ReachEffect", mCameraRoot, "EFFECT_BINGO_REACH", new Vector3(0f, 0f, 0f), new Vector3(1f, 1f, 1f), 1000);
			mReachSsAnime.AnimationFinished = delegate
			{
				mReachEffectFinished = true;
			};
			mReachSsAnime.PlayCount = 1;
			mReachSsAnime.Play();
			mGame.PlaySe("SE_MAP_GET_RBKEY", -1);
			mState.Change(STATE.REACH_EFFECT_WAIT);
			break;
		case STATE.REACH_EFFECT_WAIT:
			if (mReachEffectFinished)
			{
				mReachEffectFinished = false;
				if (mReachSsAnime != null)
				{
					UnityEngine.Object.Destroy(mReachSsAnime.gameObject);
					mReachSsAnime = null;
				}
				if (mCameraRoot != null)
				{
					UnityEngine.Object.Destroy(mCameraRoot);
					mCameraRoot = null;
				}
				StartCoroutine(GrayIn());
				mState.Change(STATE.INIT);
			}
			break;
		case STATE.BINGO_LINE_EFFECT:
			mGame.SetMusicVolume(0.4f, 0);
			mSlideBingoPanel.CreateBingoLine(mBingoList, CurrentCourseID);
			mState.Change(STATE.BINGO_LINE_EFFECT_WAIT);
			break;
		case STATE.BINGO_LINE_EFFECT_WAIT:
			if (mSlideBingoPanel.BingoEffectFinished())
			{
				StartCoroutine(GrayOut());
				mState.Reset(STATE.BINGO_EFFECT, true);
			}
			break;
		case STATE.BINGO_EFFECT:
			mCameraRoot = CreateDemoCamera();
			mBingoSsAnime = Util.CreateUISsSprite("BingoEffect", mCameraRoot, "EFFECT_BINGO_BINGO", new Vector3(0f, 0f, 0f), new Vector3(1f, 1f, 1f), 1000);
			mBingoSsAnime.AnimationFinished = delegate
			{
				mBingoEffectFinished = true;
			};
			mBingoSsAnime.PlayCount = 1;
			mBingoSsAnime.Play();
			mGame.PlaySe("SE_BINGO_BINGO", -1);
			mState.Change(STATE.BINGO_EFFECT_WAIT);
			break;
		case STATE.BINGO_EFFECT_WAIT:
			if (mBingoEffectFinished)
			{
				mBingoEffectFinished = false;
				if (mBingoSsAnime != null)
				{
					UnityEngine.Object.Destroy(mBingoSsAnime.gameObject);
					mBingoSsAnime = null;
				}
				if (mCameraRoot != null)
				{
					UnityEngine.Object.Destroy(mCameraRoot);
					mCameraRoot = null;
				}
				StartCoroutine(GrayIn());
				mState.Change(STATE.INIT);
			}
			break;
		case STATE.ALLBINGO_EFFECT:
			mGame.SetMusicVolume(0.4f, 0);
			mCameraRoot = CreateDemoCamera();
			mAllBingoSsAnime = Util.CreateUISsSprite("AllBingoEffect", mCameraRoot, "EFFECT_BINGO_ALLBINGO", new Vector3(0f, 0f, 0f), new Vector3(1f, 1f, 1f), 1000);
			mAllBingoSsAnime.AnimationFinished = delegate
			{
				mAllBingoEffectFinished = true;
			};
			mAllBingoSsAnime.PlayCount = 1;
			mAllBingoSsAnime.Play();
			mGame.PlaySe("SE_BINGO_FULLCOMP", -1);
			mState.Change(STATE.ALLBINGO_EFFECT_WAIT);
			break;
		case STATE.ALLBINGO_EFFECT_WAIT:
			if (mAllBingoEffectFinished)
			{
				mAllBingoEffectFinished = false;
				if (mAllBingoSsAnime != null)
				{
					UnityEngine.Object.Destroy(mAllBingoSsAnime.gameObject);
					mAllBingoSsAnime = null;
				}
				if (mCameraRoot != null)
				{
					UnityEngine.Object.Destroy(mCameraRoot);
					mCameraRoot = null;
				}
				mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mAllBingoEffectList[CurrentCourseID] = true;
				mGame.Save();
				TryNextSheetDialog tryNextSheetDialog = Util.CreateGameObject("TryNextSheetDialog", mRoot).AddComponent<TryNextSheetDialog>();
				SMEventPageData sMEventPageData2 = mMapPageData as SMEventPageData;
				bool a_nextsheet = false;
				if (sMEventPageData2 != null)
				{
					a_nextsheet = mTotalBingoNum < sMEventPageData2.EventCourseList.Count * 8;
				}
				tryNextSheetDialog.Init(a_nextsheet);
				tryNextSheetDialog.SetClosedCallback(OnTryNextSheetDialogClosed);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.STARCOMP_EFFECT:
			mGame.SetMusicVolume(0.4f, 0);
			mCameraRoot = CreateDemoCamera();
			mStarCompSsAnime = Util.CreateUISsSprite("StarCompEffect", mCameraRoot, "EFFECT_BINGO_STARCOMPLETE", new Vector3(0f, 0f, 0f), new Vector3(1f, 1f, 1f), 1000);
			mStarCompSsAnime.AnimationFinished = delegate
			{
				mStarCompEffectFinished = true;
			};
			mStarCompSsAnime.PlayCount = 1;
			mStarCompSsAnime.Play();
			mGame.PlaySe("SE_DIALY_COMPLETE_STAMP", -1);
			mState.Change(STATE.STARCOMP_EFFECT_WAIT);
			break;
		case STATE.STARCOMP_EFFECT_WAIT:
			if (mStarCompEffectFinished)
			{
				mStarCompEffectFinished = false;
				if (mStarCompSsAnime != null)
				{
					UnityEngine.Object.Destroy(mStarCompSsAnime.gameObject);
					mStarCompSsAnime = null;
				}
				if (mCameraRoot != null)
				{
					UnityEngine.Object.Destroy(mCameraRoot);
					mCameraRoot = null;
				}
				mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mAllStarCompEffectList[CurrentCourseID] = true;
				mGetAccessoryDict.Clear();
				mIsGotAccessoryList.Clear();
				mIsStarCompEffect = true;
				SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
				AccessoryData accessoryData2 = mGame.GetAccessoryData(sMEventPageData.EventCourseList[CurrentCourseID].CompleteAccessoryID);
				if (accessoryData2 != null)
				{
					mGetAccessoryDict[0] = accessoryData2;
					mState.Change(STATE.REWARD_INIT);
				}
				else
				{
					mGame.Save();
					mState.Change(STATE.INIT);
				}
			}
			break;
		case STATE.REWARD_INIT:
			mGemAccessoryList.Clear();
			mAdvAccessoryList.Clear();
			foreach (AccessoryData value2 in mGetAccessoryDict.Values)
			{
				if (value2.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
				{
					mGemAccessoryList.Add(value2);
				}
				else if (value2.AccessoryType == AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
				{
					mAdvAccessoryList.Add(value2);
				}
			}
			if (mGemAccessoryList.Count > 0 || mAdvAccessoryList.Count > 0)
			{
				mState.Change(STATE.NETWORK_REWARD_MAINTENACE);
				break;
			}
			if (!mIsStarCompEffect)
			{
				mState.Change(STATE.REWARD_EFFECT);
				break;
			}
			mIsStarCompEffect = false;
			CreateRewardDialog();
			mState.Change(STATE.WAIT);
			break;
		case STATE.REWARD_EFFECT:
			mGame.SetMusicVolume(0.4f, 0);
			StartCoroutine(StampEffect());
			mState.Change(STATE.WAIT);
			break;
		case STATE.NEWCOURSE_OPEN:
			if (NewOpenCourseId == CurrentCourseID)
			{
				NewOpenCourseId = -1;
				mSlideBingoPanel.OpenSheetLock(CurrentCourseID);
				mState.Change(STATE.NEWCOURSE_OPEN_WAIT);
			}
			break;
		case STATE.NEWCOURSE_OPEN_WAIT:
			if (mSlideBingoPanel.OpenSheetEffectFinished())
			{
				EventBingoCockpit eventBingoCockpit3 = mCockpit as EventBingoCockpit;
				eventBingoCockpit3.MoveReLotteryButton(true);
				LotteryStartCheck(false, true);
			}
			break;
		case STATE.LOTTERY_EFFECT:
		{
			mGame.SetMusicVolume(0.4f, 0);
			mLotteryEffectFinished = false;
			mCameraRoot = CreateDemoCamera();
			string spriteName = "bng_ball" + lotteryStageNum;
			CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[1] { default(CHANGE_PART_INFO) };
			array[0].partName = "change_000";
			array[0].spriteName = spriteName;
			mLotteryAnime = Util.CreateGameObject("LotteryAnime", mCameraRoot).AddComponent<PartsChangedSprite>();
			mLotteryAnime.Atlas = ResourceManager.LoadImageAsync("EVENTBINGO").Image;
			mLotteryAnime.transform.localPosition = new Vector3(0f, 0f, 0f);
			SMEventPageData eventPageData = GameMain.GetEventPageData(CurrentEventID);
			string fileName = "EFFECT_BINGO_LOTTERY_" + eventPageData.BingoMapSetting.ReLotteryLive2d;
			mLotteryAnime.ChangeAnime(fileName, array, true, 1, delegate
			{
				mLotteryEffectFinished = true;
			});
			mLotteryAnime.enabled = true;
			mGame.PlaySe("SE_BINGO_LOT_02", 1);
			mLotteryCameraBR = Util.CreateAnchorWithChild("LotteryCameraAnchorBR", mCameraRoot, UIAnchor.Side.BottomRight, mCameraRoot.GetComponent<Camera>()).gameObject;
			bool layoutLotterySkipButton = SetDeviceScreen(Util.ScreenOrientation);
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			mLotterySkipButton = Util.CreateJellyImageButton("SkipButton", mLotteryCameraBR.gameObject, image);
			Util.SetImageButtonInfo(mLotterySkipButton, "LC_button_skip2", "LC_button_skip2", "LC_button_skip2", 50, mSkipButtonPos, new Vector3(mSkipButtonScale, mSkipButtonScale, 1f));
			Util.AddImageButtonMessage(mLotterySkipButton, this, "OnLotterySkipButtonPushed", UIButtonMessage.Trigger.OnClick);
			SetLayoutLotterySkipButton(layoutLotterySkipButton);
			mState.Change(STATE.LOTTERY_EFFECT_WAIT);
			break;
		}
		case STATE.LOTTERY_EFFECT_WAIT:
			if (mLotteryAnime != null && mLotteryAnime._sprite.AnimFrame >= 94f && mLotterySkipButton != null)
			{
				UnityEngine.Object.Destroy(mLotterySkipButton.gameObject);
				mLotterySkipButton = null;
			}
			if (mLotteryEffectFinished)
			{
				StartCoroutine(GrayIn(0.5f));
				if (mLotteryAnime != null)
				{
					UnityEngine.Object.Destroy(mLotteryAnime.gameObject);
					mLotteryAnime = null;
				}
				if (mLotteryCameraBR != null)
				{
					UnityEngine.Object.Destroy(mLotteryCameraBR.gameObject);
					mLotteryCameraBR = null;
				}
				if (mCameraRoot != null)
				{
					UnityEngine.Object.Destroy(mCameraRoot);
					mCameraRoot = null;
				}
				mSlideBingoPanel.UpdateStageButton(CurrentCourseID, lotteryStageNum);
				mSlideBingoPanel.SetCourseEnable(CurrentCourseID, true);
				UpdateReLotteryBottonStatus();
				mGame.PlaySe("SE_DEMO_IMAGE01", -1);
				EventBingoCockpit eventBingoCockpit = mCockpit as EventBingoCockpit;
				eventBingoCockpit.MoveReLotteryButton(false);
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case STATE.GETREWARD_INIT:
		{
			bool flag = false;
			if (mGetGemItem)
			{
				mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
				flag = true;
			}
			if (mGetAdvItem)
			{
				mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
				flag = true;
			}
			mGetAccessoryDict.Clear();
			mGemAccessoryList.Clear();
			mAdvAccessoryList.Clear();
			mIsGotAccessoryList.Clear();
			mGetPartner = null;
			mGetGemItem = false;
			if (flag)
			{
				mState.Change(STATE.NETWORK_MAILINIT);
			}
			else
			{
				mState.Change(STATE.INIT);
			}
			break;
		}
		case STATE.PLAY:
		{
			mGame.mReplay = false;
			mGame.ClearPresentBox();
			if (mGame.mEventMode)
			{
				mGame.mSkinIndex = mGame.mEventSkinIndex;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mPlayer.Data.SelectedCompanion;
				}
			}
			else
			{
				mGame.mSkinIndex = mGame.mPlayer.Data.SelectedSkin;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mPlayer.Data.SelectedCompanion;
				}
			}
			int playerIcon = mGame.mPlayer.Data.PlayerIcon;
			if (mGame.mPlayer.IsCompanionUnlock(mGame.mPlayer.Data.SelectedCompanion) && mGame.mLastCompanionSetting)
			{
				mGame.mPlayer.Data.LastUsedIcon = mGame.mPlayer.Data.SelectedCompanion;
				if (!mGame.mPlayer.Data.LastUsedIconCancelFlg)
				{
					if (mGame.mPlayer.Data.PlayerIcon >= 10000)
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion + 10000;
					}
					else
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion;
					}
				}
				if (playerIcon != mGame.mPlayer.Data.PlayerIcon)
				{
					mGame.Network_NicknameEdit(string.Empty);
				}
			}
			if (playerIcon != mGame.mPlayer.Data.PlayerIcon)
			{
				mGame.Network_NicknameEdit(string.Empty);
			}
			mGame.AlreadyConnectAccessoryList.Clear();
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.PUZZLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		}
		case STATE.WEBVIEW:
			OnHowToDialog();
			break;
		case STATE.NETWORK_PROLOGUE:
			if (!GameMain.CanCommunication())
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
				break;
			}
			if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				Server.ManualConnecting = true;
			}
			else if (mTipsScreenDisplayed)
			{
				mGameState.SetLoadingUISsAnimation(2);
			}
			mState.Change(STATE.NETWORK_MAINTENANCE);
			break;
		case STATE.NETWORK_MAINTENANCE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_MAINTENANCE_WAIT);
			break;
		case STATE.NETWORK_MAINTENANCE_WAIT:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_AUTH);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 36);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_AUTH:
			mGame.Network_UserAuth();
			mState.Change(STATE.NETWORK_AUTH_WAIT);
			break;
		case STATE.NETWORK_AUTH_WAIT:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_GAMEINFO);
					break;
				}
				mGame.FinishConnecting();
				mState.Change(STATE.AUTHERROR_RETURN_TITLE);
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = mUserAuthErrorCode;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 38, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 38, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_GAMEINFO:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_GAMEINFO_WAIT);
			break;
		case STATE.NETWORK_GAMEINFO_WAIT:
			if (GameMain.mGameProfileCheckDone)
			{
				if (GameMain.mGameProfileSucceed)
				{
					mState.Change(STATE.NETWORK_SEGMENTINFO);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 40, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_SEGMENTINFO:
			mGame.Network_GetSegmentProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_SEGMENTINFO_WAIT);
			break;
		case STATE.NETWORK_SEGMENTINFO_WAIT:
		{
			if (!GameMain.mSegmentProfileCheckDone)
			{
				break;
			}
			if (GameMain.mSegmentProfileSucceed)
			{
				mState.Change(STATE.NETWORK_LOGINBONUS);
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode2 = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = mUserAuthErrorCode2;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 42, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 42, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_LOGINBONUS:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_LOGINBONUS_WAIT);
			break;
		case STATE.NETWORK_LOGINBONUS_WAIT:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				mGame.DoLoginBonusServerCheck = false;
				mState.Change(STATE.NETWORK_MAILINIT);
			}
			else
			{
				Server.ErrorCode mLoginBonusErrorCode = GameMain.mLoginBonusErrorCode;
				Server.ErrorCode errorCode = mLoginBonusErrorCode;
				if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 44, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 44, OnConnectErrorAuthRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.NETWORK_MAILINIT:
			mGame.mGotGiftIds = new List<int>();
			mGame.mGotApplyIds = new List<int>();
			mGame.mGotSupportIds = new List<int>();
			mGame.mGotSupportPurchaseIds = new List<int>();
			mState.Change(STATE.NETWORK_SUPPORTGIFT);
			break;
		case STATE.NETWORK_ADV_GET_MAIL:
		{
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			mGame.Network_AdvGetMail(mGuID, is_retry);
			mState.Change(STATE.NETWORK_ADV_GET_MAIL_WAIT);
			break;
		}
		case STATE.NETWORK_ADV_GET_MAIL_WAIT:
		{
			if (!GameMain.mAdvGetMailCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetMailSucceed)
			{
				mGuID = null;
				mState.Change(STATE.NETWORK_SUPPORTGIFT);
				break;
			}
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = GameMain.mAdvGetMailErrorCode;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 47, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 47, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_SUPPORTGIFT:
		{
			STATE aStatus3 = STATE.NETWORK_SUPPORTGIFT_WAIT;
			if (mGame.MapNetwork_SupportCheck())
			{
				aStatus3 = STATE.NETWORK_GIFT;
			}
			mState.Change(aStatus3);
			break;
		}
		case STATE.NETWORK_SUPPORTGIFT_WAIT:
			if (GameMain.mMapSupportCheckDone)
			{
				mGame.MapNetwork_SupportGot();
				mState.Change(STATE.NETWORK_GIFT);
			}
			break;
		case STATE.NETWORK_GIFT:
		{
			STATE aStatus2 = STATE.NETWORK_GIFT_WAIT;
			if (mGame.MapNetwork_GiftCheck())
			{
				aStatus2 = STATE.NETWORK_FRIEND;
			}
			mState.Change(aStatus2);
			break;
		}
		case STATE.NETWORK_GIFT_WAIT:
			if (GameMain.mMapGiftCheckDone)
			{
				mGame.MapNetwork_GiftGot();
				mState.Change(STATE.NETWORK_FRIEND);
			}
			break;
		case STATE.NETWORK_FRIEND:
		{
			STATE aStatus = STATE.NETWORK_FRIEND_WAIT;
			if (mGame.MapNetwork_ApplyCheck())
			{
				aStatus = STATE.NETWORK_SUPPURCHASE;
			}
			mState.Change(aStatus);
			break;
		}
		case STATE.NETWORK_FRIEND_WAIT:
			if (GameMain.mMapApplyCheckDone)
			{
				mGame.MapNetwork_ApplyGot();
				if (GameMain.mMapApplyRepeat)
				{
					mState.Change(STATE.NETWORK_FRIEND_REPEAT);
				}
				else
				{
					mState.Change(STATE.NETWORK_SUPPURCHASE);
				}
			}
			break;
		case STATE.NETWORK_FRIEND_REPEAT:
		{
			long num = DateTimeUtil.BetweenMilliseconds(mGame.mPlayer.Data.LastGetApplyTime, DateTime.Now);
			if (num > GameMain.mMapApplyRepeatWait)
			{
				mGame.mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
				mState.Change(STATE.NETWORK_FRIEND);
			}
			break;
		}
		case STATE.NETWORK_SUPPURCHASE:
			if (GameMain.NO_NETWORK)
			{
				mState.Change(STATE.NETWORK_MAILEND);
			}
			else if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				STATE aStatus4 = STATE.NETWORK_SUPPURCHASE_WAIT;
				if (mGame.MapNetwork_SupportPurchaseCheck())
				{
					aStatus4 = STATE.NETWORK_MAILEND;
				}
				mState.Change(aStatus4);
			}
			else
			{
				mState.Change(STATE.NETWORK_MAILEND);
			}
			break;
		case STATE.NETWORK_SUPPURCHASE_WAIT:
			if (GameMain.mMapSupportPurchaseCheckDone)
			{
				mState.Change(STATE.NETWORK_MAILEND);
			}
			break;
		case STATE.NETWORK_MAILEND:
		{
			if (mGame.mFbUserMngGift == null)
			{
				mGame.mFbUserMngGift = FBUserManager.CreateInstance();
			}
			List<GiftItemData> list3 = new List<GiftItemData>();
			if (mGame.mPlayer.Gifts.Count > 0)
			{
				foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
				{
					GiftItem value = gift.Value;
					list3.Add(value.Data);
				}
				mGame.mFbUserMngGift.SetFBUserList(list3);
			}
			mState.Change(STATE.NETWORK_MYFRIENDLIST);
			break;
		}
		case STATE.NETWORK_MYFRIENDLIST:
			mGame.mFriendManager = new GameFriendManager(mGame);
			if (!mGame.mFriendManager.Start())
			{
				mState.Change(STATE.NETWORK_CAMPAIGN);
			}
			else
			{
				mState.Change(STATE.NETWORK_MYFRIENDLIST_WAIT);
			}
			break;
		case STATE.NETWORK_MYFRIENDLIST_WAIT:
			if (mGame.mFriendManager == null || mGame.mFriendManager.IsFinished())
			{
				mState.Change(STATE.NETWORK_CAMPAIGN);
			}
			else
			{
				mGame.mFriendManager.Update();
			}
			break;
		case STATE.NETWORK_CAMPAIGN:
			if (mGame.Network_CampaignCheck())
			{
				mState.Change(STATE.NETWORK_CAMPAIGN_WAIT);
			}
			else
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_CAMPAIGN_WAIT:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_EPILOGUE:
			if (mGame.mNetworkDataModified)
			{
				mGame.Save();
			}
			if (mGame.mGotGiftIds != null)
			{
				mGame.mGotGiftIds.Clear();
			}
			if (mGame.mGotSupportIds != null)
			{
				mGame.mGotSupportIds.Clear();
			}
			if (mGame.mGotApplyIds != null)
			{
				mGame.mGotApplyIds.Clear();
			}
			if (mGame.mGotSupportPurchaseIds != null)
			{
				mGame.mGotSupportPurchaseIds.Clear();
			}
			if (mTipsScreenDisplayed)
			{
				mGameState.TipsScreenOff();
			}
			else
			{
				Server.ManualConnecting = false;
			}
			mState.Change(STATE.INIT);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_REWARD_MAINTENACE_WAIT);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE_WAIT:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else if (mGemAccessoryList.Count > 0)
				{
					mState.Change(STATE.NETWORK_REWARD_SET);
				}
				else if (mAdvAccessoryList.Count > 0)
				{
					mState.Change(STATE.NETWORK_ADV_REWARD_SET);
				}
				else
				{
					mState.Change(STATE.INIT);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 64);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_REWARD_SET:
			mGame.Network_RewardSet(mGemAccessoryList[0].Index);
			mState.Change(STATE.NETWORK_REWARD_SET_WAIT);
			break;
		case STATE.NETWORK_REWARD_SET_WAIT:
			if (!GameMain.mRewardSetCheckDone)
			{
				break;
			}
			if (GameMain.mRewardSetSucceed)
			{
				mGame.mPlayer.AddAccessory_AllInGift(mGemAccessoryList[0], CurrentEventID);
				mGame.Save();
				if (GameMain.mRewardSetStatus != 0)
				{
					mIsGotAccessoryList.Add(mGemAccessoryList[0]);
				}
				else
				{
					mGetGemItem = true;
				}
				mGemAccessoryList.RemoveAt(0);
				if (mGemAccessoryList.Count > 0)
				{
					mState.Change(STATE.NETWORK_REWARD_SET);
					break;
				}
				if (mAdvAccessoryList.Count > 0)
				{
					mState.Change(STATE.NETWORK_ADV_REWARD_SET);
					break;
				}
				if (!mIsStarCompEffect)
				{
					mState.Change(STATE.REWARD_EFFECT);
					break;
				}
				mIsStarCompEffect = false;
				CreateRewardDialog();
				mState.Change(STATE.WAIT);
			}
			else if (GameMain.USE_DEBUG_DIALOG && GameMain.mRewardSetErrorCode == Server.ErrorCode.ACCESSORY_NOTFOUND)
			{
				StartCoroutine(GrayOut());
				mConfirmDialog = Util.CreateGameObject("AccessoryNotFoundDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("Network_Error_Title"), string.Format(Localization.Get("Debug_Bingo_NotFoundAccessory"), mGemAccessoryList[0].Index), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				mConfirmDialog.SetClosedCallback(OnAcNotFoundDialogClosed);
				mState.Change(STATE.WAIT);
			}
			else
			{
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 66, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_ADV_REWARD_SET:
		{
			int is_retry2 = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry2 = 1;
			}
			List<int> list2 = new List<int>();
			for (int k = 0; k < mAdvAccessoryList.Count; k++)
			{
				if (mGame.mAdvRewardListDict.ContainsKey(int.Parse(mAdvAccessoryList[k].GotID)))
				{
					AdvRewardListData advRewardListData = mGame.mAdvRewardListDict[int.Parse(mAdvAccessoryList[k].GotID)];
					if (advRewardListData.CanReceive)
					{
						list2.Add(int.Parse(mAdvAccessoryList[k].GotID));
					}
					else
					{
						mIsGotAccessoryList.Add(mAdvAccessoryList[k]);
					}
				}
			}
			if (list2.Count > 0)
			{
				mGame.Network_AdvSetReward(null, list2.ToArray(), mGuID, is_retry2);
				mState.Change(STATE.NETWORK_ADV_REWARD_WAIT);
				break;
			}
			for (int l = 0; l < mAdvAccessoryList.Count; l++)
			{
				mGame.mPlayer.AddAccessory_AllInGift(mAdvAccessoryList[l], CurrentEventID);
			}
			if (!mIsStarCompEffect)
			{
				mState.Change(STATE.REWARD_EFFECT);
				break;
			}
			mIsStarCompEffect = false;
			CreateRewardDialog();
			mState.Change(STATE.WAIT);
			break;
		}
		case STATE.NETWORK_ADV_REWARD_WAIT:
			if (!GameMain.mAdvSetRewardCheckDone)
			{
				break;
			}
			if (GameMain.mAdvSetRewardSucceed)
			{
				mGuID = null;
				mGetAdvItem = true;
				for (int j = 0; j < mAdvAccessoryList.Count; j++)
				{
					mGame.mPlayer.AddAccessory_AllInGift(mAdvAccessoryList[j], CurrentEventID);
				}
				mGame.Save();
				if (!mIsStarCompEffect)
				{
					mState.Change(STATE.REWARD_EFFECT);
					break;
				}
				mIsStarCompEffect = false;
				CreateRewardDialog();
				mState.Change(STATE.WAIT);
			}
			else
			{
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 68, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_REWARD_MAINTENACE2:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_REWARD_MAINTENACE2_00);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE2_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_REWARD_SET2);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 70);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_REWARD_SET2:
			Server.ManualConnecting = true;
			mGame.Network_RewardSet(AccessoryConnectIndex);
			mState.Reset(STATE.NETWORK_REWARD_SET2_00, true);
			break;
		case STATE.NETWORK_REWARD_SET2_00:
		{
			if (!GameMain.mRewardSetCheckDone)
			{
				break;
			}
			Server.ManualConnecting = false;
			string empty = string.Empty;
			string empty2 = string.Empty;
			if (GameMain.mRewardSetSucceed)
			{
				AccessoryData accessoryData = mGame.GetAccessoryData(AccessoryConnectIndex);
				bool flag2 = mGame.mPlayer.SubAccessoryConnect(accessoryData);
				AccessoryConnectIndex = 0;
				mGame.IsPlayerSaveRequest = true;
				if (GameMain.mRewardSetStatus == 0)
				{
					int index = accessoryData.Index;
					if (index != 0)
					{
						int cur_series = (int)mGame.mPlayer.Data.CurrentSeries;
						int get_type = 4;
						if (mGame.IsDailyChallengePuzzle)
						{
							cur_series = 200;
						}
						ServerCram.GetAccessory(index, cur_series, get_type);
					}
					if (flag2)
					{
						mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
						mState.Reset(STATE.NETWORK_SUPPURCHASE, true);
					}
					else
					{
						mState.Reset(STATE.INIT, true);
					}
					break;
				}
				empty = Localization.Get("GotError_Title");
				empty2 = Localization.Get("GotError_Desc");
				if (GameStateSMMapBase.mIsShowErrorDialog)
				{
					StartCoroutine(GrayOut());
					GetRewardDialog getRewardDialog = Util.CreateGameObject("NoGetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
					List<AccessoryData> list = new List<AccessoryData>();
					list.Add(accessoryData);
					getRewardDialog.Init(list, true);
					getRewardDialog.SetClosedCallback(delegate
					{
						StartCoroutine(GrayIn());
						mState.Change(STATE.INIT);
					});
					mState.Reset(STATE.WAIT);
				}
				else
				{
					mState.Reset(STATE.INIT, true);
				}
			}
			else
			{
				empty = Localization.Get("Network_Error_Title");
				empty2 = Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b");
				if (GameStateSMMapBase.mIsShowErrorDialog)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 72, OnConnectErrorAccessoryRetry, OnConnectErrorAccessoryAbandon);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					OnConnectErrorAccessoryAbandon();
				}
			}
			break;
		}
		case STATE.NETWORK_RESUME_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_MAINTENACE_00);
			break;
		case STATE.NETWORK_RESUME_MAINTENACE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_RESUME_GAMEINFO);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 74);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_GAMEINFO:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_GAMEINFO_00);
			break;
		case STATE.NETWORK_RESUME_GAMEINFO_00:
			if (GameMain.mGameProfileCheckDone)
			{
				if (GameMain.mGameProfileSucceed)
				{
					mState.Change(STATE.NETWORK_RESUME_CAMPAIGN);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 76, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN:
			if (PurchaseFullDialog.IsShow())
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			else if (mGame.Network_CampaignCheck())
			{
				mState.Change(STATE.NETWORK_RESUME_CAMPAIGN_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_RESUME_LOGINBONUS_00);
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				mGame.DoLoginBonusServerCheck = false;
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
					mState.Change(STATE.NETWORK_MAILINIT);
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 80, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.AUTHERROR_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Transfer_CertError_Title"), Localization.Get("Transfered_TitleBack_Desc2"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.AUTHERROR_RETURN_WAIT);
			break;
		case STATE.MAINTENANCE_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("MaintenanceMode_ReturnTitle_Title"), Localization.Get("MaintenanceMode_ReturnTitle_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.MAINTENANCE_RETURN_WAIT);
			mGame.IsTitleReturnMaintenance = true;
			break;
		}
		UpdateSlideBG();
		mState.Update();
	}

	public override IEnumerator LoadGameState()
	{
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		mTipsScreenDisplayed = false;
		if (mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			mTipsScreenDisplayed = true;
			yield return StartCoroutine(mGameState.CoTipsScreenOn(1, false));
			float temp_start = Time.realtimeSinceStartup;
			float span_time = Time.realtimeSinceStartup - _start;
			while (span_time < 0.5f)
			{
				span_time = Time.realtimeSinceStartup - _start;
				yield return 0;
			}
		}
		yield return StartCoroutine(LoadCommonResources());
		mState.SetResetCallback(StatusUpdate);
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		GameObject parent = mRoot;
		mAnchorBottom = Util.CreateAnchorWithChild("MapAnchorBottom", parent, UIAnchor.Side.Bottom, camera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("MapAnchorBottomLeft", parent, UIAnchor.Side.BottomLeft, camera).gameObject;
		mAnchorRight = Util.CreateAnchorWithChild("MapAnchorRight", parent, UIAnchor.Side.Right, camera).gameObject;
		mAnchorCenter = Util.CreateAnchorWithChild("MapAnchorCenter", parent, UIAnchor.Side.Center, camera).gameObject;
		mMapPageRoot = Util.CreateGameObject("MapPageRoot", mAnchorCenter.gameObject);
		ResourceManager.LoadImage("FADE");
		mMapPageData = GameMain.GetCurrentEventPageData();
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		DEFAULT_EVENT_BGM = eventPageData.EventSetting.MapEventBGM;
		if (DEFAULT_EVENT_BGM == string.Empty)
		{
			DEFAULT_EVENT_BGM = "BGM_MAP";
		}
		ResSound resSound = ResourceManager.LoadSoundAsync(DEFAULT_EVENT_BGM);
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		if (!mGame.mPlayer.Data.mBingoEventDataDict.ContainsKey(CurrentEventID))
		{
			mGame.mPlayer.Data.MakeBingoEventData(CurrentEventID);
			mGame.Save();
		}
		string url = string.Format(SingletonMonoBehaviour<Network>.Instance.Store_URL, BIJUnity.getCountryCode());
		ResImage resImage = ResourceManager.LoadImageAsync(eventPageData.EventSetting.MapAtlasKey);
		while (resImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		int eventID = mGame.mPlayer.Data.CurrentEventID;
		SMSeasonEventSetting eventsetting = mGame.mEventData.InSessionEventList[eventID];
		ResImage bannerAtlas = ResourceManager.LoadImageAsync(eventsetting.BannerAtlas);
		while (bannerAtlas.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		mBGRoot = Util.CreateGameObject("EventBGRoot", mMapPageRoot);
		BIJImage atlasMap = ResourceManager.LoadImage(eventPageData.EventSetting.MapAtlasKey).Image;
		SMEventBG mBGImage2 = Util.CreateGameObject("EventBG_Left", mBGRoot).AddComponent<SMEventBG>();
		string backname = eventPageData.EventSetting.MapBackName;
		mBGImage2.Active(atlasMap, backname, new Vector3(-242f, 0f, Def.MAP_BASE_Z), new Vector2(1.4f, 1.4f));
		mBGImage2 = Util.CreateGameObject("EventBG_Right", mBGRoot).AddComponent<SMEventBG>();
		mBGImage2.Active(atlasMap, backname, new Vector3(241f, 0f, Def.MAP_BASE_Z), new Vector2(1.4f, 1.4f));
		ResImage resBingoImage = ResourceManager.LoadImageAsync("EVENTBINGO");
		while (resBingoImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		List<ResSsAnimation> asyncAnimeList = new List<ResSsAnimation>();
		SMEventPageData mapdata = GameMain.GetEventPageData(CurrentEventID);
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_LOTTERY_" + mapdata.BingoMapSetting.ReLotteryLive2d));
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_SELECT"));
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_ALLBINGO"));
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_BINGO"));
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_REACH"));
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_STARCOMPLETE"));
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_REACH_LINE"));
		asyncAnimeList.Add(ResourceManager.LoadSsAnimationAsync("EFFECT_BINGO_SHEETCOMP"));
		for (int j = 0; j < asyncAnimeList.Count; j++)
		{
			while (asyncAnimeList[j].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		List<ResSound> asyncSoundList = new List<ResSound>
		{
			ResourceManager.LoadSoundAsync("SE_BINGO_FULLCOMP"),
			ResourceManager.LoadSoundAsync("SE_BINGO_BINGO"),
			ResourceManager.LoadSoundAsync("SE_BINGO_SWIPE"),
			ResourceManager.LoadSoundAsync("SE_BINGO_LOT_02"),
			ResourceManager.LoadSoundAsync("SE_BINGO_BINGO_STAGE_EFFECT")
		};
		for (int i = 0; i < asyncSoundList.Count; i++)
		{
			while (asyncSoundList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		ResImage image = ResourceManager.LoadImageAsync("HUD2", true);
		ResSsAnimation anim = ResourceManager.LoadSsAnimationAsync("EFFECT_SHINE", true);
		UIFont font = GameMain.LoadFont();
		mGame.mUseBooster0 = false;
		mGame.mUseBooster1 = false;
		mGame.mUseBooster2 = false;
		mGame.mUseBooster3 = false;
		mGame.mUseBooster4 = false;
		mGame.mUseBooster5 = false;
		UpdateTotalBingoCount();
		CreateBingoBoard();
		yield return null;
		mSlideBingoPanel.InitMove = true;
		mSlideBingoPanel.SetMoveSlide(CurrentCourseID, true);
		mSlideBingoPanel.SetEnableButtonAction(false);
		mRewardPanel = Util.CreateSprite("RewardPanel", mAnchorBottom.gameObject, atlasMap);
		Util.SetSpriteInfo(mRewardPanel, "event_panel00", 20, new Vector3(0f, 180f, 0f), Vector3.one, false);
		mRewardPanel.SetDimensions(639, 89);
		mRewardPanel.type = UIBasicSprite.Type.Sliced;
		StartCoroutine(GetRewardInfoConnect());
		while (!mGetRewardInfoConnectComplete)
		{
			yield return null;
		}
		CreateRewardList();
		mNextRewardSprite = Util.CreateSprite("NextReward", mRewardPanel.gameObject, atlasMap);
		Util.SetSpriteInfo(imageName: GetNextRewardSpriteName(), sprite: mNextRewardSprite, depth: 21, position: new Vector3(0f, 43f, 0f), scale: Vector3.one, flip: false);
		SetDeviceScreen(Util.ScreenOrientation);
		SetLayout(Util.ScreenOrientation);
		if (!mIsPlayingBGM && mShowStoryDemoList.Count == 0 && resSound.mAudioClip != null)
		{
			mGame.PlayMusic(DEFAULT_EVENT_BGM, 0, true, false, true);
			mGame.SetMusicVolume(1f, 0);
			mIsPlayingBGM = true;
		}
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		yield return Resources.UnloadUnusedAssets();
		ResourceManager.EnableLoadAllResourceFromAssetBundle();
		mSlideBingoPanel.InitMove = false;
		LoadGameStateFinished();
	}

	public override IEnumerator UnloadGameState()
	{
		if (mTipsScreenDisplayed)
		{
			mGameState.TipsScreenOff();
		}
		else
		{
			Server.ManualConnecting = false;
		}
		ResourceManager.UnloadSound(DEFAULT_EVENT_BGM);
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		if (mSheeCompSsAnime != null)
		{
			UnityEngine.Object.Destroy(mSheeCompSsAnime.gameObject);
			mSheeCompSsAnime = null;
		}
		if (mCockpit != null)
		{
			GameMain.SafeDestroy(mCockpit.gameObject);
			mCockpit = null;
		}
		UnityEngine.Object.Destroy(mAnchorBottom.gameObject);
		mAnchorBottom = null;
		UnityEngine.Object.Destroy(mAnchorRight.gameObject);
		mAnchorRight = null;
		UnityEngine.Object.Destroy(mAnchorBottomLeft.gameObject);
		mAnchorBottomLeft = null;
		UnityEngine.Object.Destroy(mAnchorCenter.gameObject);
		mAnchorCenter = null;
		UnityEngine.Object.Destroy(mRoot);
		ResourceManager.UnloadSsAnimationAll();
		ResourceManager.UnloadImageAll();
		mGame.UnloadNotPreloadSoundResources(true);
		UnloadGameStateFinished();
		yield return 0;
	}

	protected override void Update_UNLOCK_ACTING()
	{
		if (!isLoadFinished() || !mGameState.IsFadeFinished)
		{
			return;
		}
		if (mLoginBonusScreen == null && mGame.mLoginBonusData != null && mGame.mLoginBonusData.HasLoginData)
		{
			GetLoginBonusResponse loginBonusByIndex = mGame.mLoginBonusData.GetLoginBonusByIndex(0);
			if (loginBonusByIndex.IsDislpay)
			{
				mLoginBonusScreen = Util.CreateGameObject("LoginBonusScreen", mRoot).AddComponent<LoginBonusScreen>();
				mLoginBonusScreen.init(loginBonusByIndex, delegate
				{
					mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
					mState.Reset(STATE.UNLOCK_ACTING, true);
					mLoginBonusScreen = null;
				});
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	protected override void Update_MAIN()
	{
		UpdateReLotteryBottonStatus();
		if (mGame.mBackKeyTrg)
		{
			if (GameMain.USE_DEBUG_DIALOG && GameMain.HIDDEN_DEBUG_BUTTON)
			{
				OnDebugPushed();
			}
			else
			{
				OnBackKeyPushed();
			}
		}
	}

	protected bool CheaterStartCheck()
	{
		if (!mGame.CheckTimeCheater())
		{
			return false;
		}
		mState.Change(STATE.WAIT);
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("Cheater_TimeCheat_Desc"));
		mConfirmDialog.Init(Localization.Get("Cheater_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnTimeCheatErrorDialogClosed);
		return true;
	}

	protected void OnTimeCheatErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		OnExitToTitle(true);
	}

	protected override bool IsEventExpired(bool a_return, ConfirmDialog.OnDialogClosed a_callback, Action a_cancel_callback)
	{
		EventCockpit eventCockpit = mCockpit as EventCockpit;
		if (!mGame.IsSeasonEventExpired(CurrentEventID) && (eventCockpit == null || !eventCockpit.IsOldEventFinished))
		{
			return false;
		}
		if (a_return)
		{
			Action action = delegate
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("ReturnMap_Desc02"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(a_callback);
			};
			if (eventCockpit == null || !eventCockpit.TryOpenOldEventExtensionDialog(a_cancel_callback, action))
			{
				action();
			}
		}
		else
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("EventExpired_Desc02"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(a_callback);
		}
		return true;
	}

	protected void OnEventExit(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			if (!IsEventExpired(true, OnMoveToMap, delegate
			{
				mState.Change(STATE.MAIN);
			}))
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("ReturnMap_Desc"));
				mConfirmDialog.Init(Localization.Get("ReturnMap_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnMoveToMap);
			}
		}
	}

	protected void OnMoveToMap(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			MoveToMap();
			return;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void MoveToMap()
	{
		mGame.StopMusic(0, 0.5f);
		mGame.mPlayer.SetNextSeries(mGame.mPlayer.Data.PreviousSeries);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override bool WebviewStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return false;
		}
		if (mGame.mEventProfile == null)
		{
			return false;
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		if (seasonEvent == null)
		{
			return false;
		}
		int helpID = seasonEvent.HelpID;
		string helpURL = seasonEvent.HelpURL;
		if (helpID == 0 || string.IsNullOrEmpty(helpURL))
		{
			return false;
		}
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		if (data.FirstHowToPlayFlg >= seasonEvent.HelpID)
		{
			return false;
		}
		mStartWebview = true;
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		OnEventHelp_Sub(null, true);
		return true;
	}

	protected void OnDebugEventHowToClosed(ConfirmDialog.SELECT_ITEM i)
	{
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		bool flag = false;
		if (data.FirstHowToPlayFlg < seasonEvent.HelpID || mStartWebview)
		{
			mStartWebview = false;
			flag = true;
		}
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
		if (mConfirmDialog != null)
		{
			UnityEngine.Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		if (flag)
		{
			int num = -1;
			int getnum = 0;
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			List<int> list = new List<int>(sMEventPageData.EventRewardList.Keys);
			for (int j = 0; j < list.Count; j++)
			{
				BingoRewardListData bingoRewardListData = new BingoRewardListData();
				SMEventRewardSetting sMEventRewardSetting = sMEventPageData.EventRewardList[list[j]];
				AccessoryData accessoryData = mGame.GetAccessoryData(sMEventRewardSetting.RewardID);
				if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
				{
					num = accessoryData.Index;
					getnum = j + 1;
					break;
				}
			}
			if (num != -1)
			{
				mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
				mEventIntroCharacterDialog.Init(num, Def.EVENT_TYPE.SM_BINGO, getnum);
				mEventIntroCharacterDialog.SetClosedCallback(OnEventIntroCharactertDialogClosed);
			}
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	private void OnEventIntroCharactertDialogClosed()
	{
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		if (!mGame.mPlayer.IsAccessoryUnlock(sMEventPageData.BingoMapSetting.FreeTicket))
		{
			List<AccessoryData> list = new List<AccessoryData>();
			list.Add(mGame.GetAccessoryData(sMEventPageData.BingoMapSetting.FreeTicket));
			mGame.mPlayer.UnlockAccessory(sMEventPageData.BingoMapSetting.FreeTicket);
			mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReLotteryTicket += int.Parse(list[0].GotID);
			mGame.Save();
			GetRewardDialog getRewardDialog = Util.CreateGameObject("GetFreeTicketDialog", mRoot).AddComponent<GetRewardDialog>();
			getRewardDialog.Init(list, false, true);
			getRewardDialog.SetClosedCallback(OnGetFreeTicketDialogClosed);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	private void OnGetFreeTicketDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnEventHelp(GameObject go)
	{
		OnEventHelp_Sub(go, false);
	}

	private void OnEventHelp_Sub(GameObject go, bool is_init_open)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
			mHelpDialog = null;
		}
		if (!is_init_open && (mSlideBingoPanel == null || !mSlideBingoPanel.IsPanelNoMove))
		{
			return;
		}
		if (IsEventHelpExpired(delegate
		{
			mConfirmDialog = null;
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}))
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			return;
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		if (seasonEvent != null)
		{
			int helpID = seasonEvent.HelpID;
			string helpURL = seasonEvent.HelpURL;
			if (helpID != 0 && !string.IsNullOrEmpty(helpURL))
			{
				StartCoroutine(GrayOut());
				mGame.PlaySe("SE_POSITIVE", -1);
				mHelpDialog = Util.CreateGameObject("HowToDialog", mRoot).AddComponent<WebViewDialog>();
				mHelpDialog.Title = Localization.Get("EventPage");
				mHelpDialog.SetClosedCallback(OnHowToDialogClosed);
				mHelpDialog.SetPageMovedCallback(OnHowToDialogPageMoved);
				mState.Reset(STATE.WEBVIEW, true);
			}
		}
	}

	protected void OnHowToDialogClosed()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		bool flag = false;
		if (data.FirstHowToPlayFlg < seasonEvent.HelpID || mStartWebview)
		{
			mStartWebview = false;
			flag = true;
		}
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
		}
		mHelpDialog = null;
		if (flag)
		{
			int num = -1;
			int getnum = 0;
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			List<int> list = new List<int>(sMEventPageData.EventRewardList.Keys);
			for (int i = 0; i < list.Count; i++)
			{
				BingoRewardListData bingoRewardListData = new BingoRewardListData();
				SMEventRewardSetting sMEventRewardSetting = sMEventPageData.EventRewardList[list[i]];
				AccessoryData accessoryData = mGame.GetAccessoryData(sMEventRewardSetting.RewardID);
				if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
				{
					num = accessoryData.Index;
					getnum = i + 1;
					break;
				}
			}
			if (num != -1)
			{
				mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
				mEventIntroCharacterDialog.Init(num, Def.EVENT_TYPE.SM_BINGO, getnum);
				mEventIntroCharacterDialog.SetClosedCallback(OnEventIntroCharactertDialogClosed);
			}
		}
		else
		{
			mGame.Save();
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	private void OnHowToErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (mHelpDialog != null)
		{
			switch (item)
			{
			case ConfirmDialog.SELECT_ITEM.POSITIVE:
				mHelpDialog.Reload();
				break;
			case ConfirmDialog.SELECT_ITEM.NEGATIVE:
				mHelpDialog.Close();
				break;
			}
		}
		mConfirmDialog = null;
	}

	private void OnHowToErrorDialogClosed2(ConfirmDialog.SELECT_ITEM item)
	{
		if (mHelpDialog != null)
		{
			mHelpDialog.Close();
		}
		mConfirmDialog = null;
	}

	private void OnHowToDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mHelpDialog.ShowWebview(true);
		}
		else
		{
			mHelpDialog.ShowWebview(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
			mConfirmDialog.SetClosedCallback(OnHowToErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	protected void OnHowToDialog()
	{
		if (mHelpDialog.IsOpend)
		{
			mState.Reset(STATE.WEBVIEW_WAIT, true);
			mHelpDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_DOMAIN);
			StringBuilder stringBuilder = new StringBuilder();
			SeasonEventSettings seasonEvent = mGame.EventProfile.GetSeasonEvent(CurrentEventID);
			if (seasonEvent != null)
			{
				stringBuilder.Append(seasonEvent.HelpURL);
				mHelpDialog.LoadURL(stringBuilder.ToString());
				return;
			}
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnHowToErrorDialogClosed2);
		}
	}

	protected bool RankingRankUpStarStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsStar(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.STAR);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected void OnRankUpDialogClosed()
	{
		StartCoroutine(GrayIn());
		mRankUpDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnStageButtonPushed(int index)
	{
		int num = index * 100;
		bool flag = false;
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL2_2 && num == 200)
		{
			mGame.mTutorialManager.DeleteTutorialArrow();
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL2_2);
			flag = true;
		}
		if ((!flag && ((mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.UNLOCK_ACTING) || base.IsShowingMarketingScreen)) || mSlideBingoPanel == null || !mSlideBingoPanel.IsPanelNoMove)
		{
			return;
		}
		if (IsEventExpired(false, delegate
		{
			mConfirmDialog = null;
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}, delegate
		{
			mState.Change(STATE.MAIN);
		}))
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			return;
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(num, out a_main, out a_sub);
		SMMapStageSetting mapStage = mMapPageData.GetMapStage(a_main, a_sub);
		int num2 = -1;
		if (mapStage != null)
		{
			int storyDemoAtStageSelect = mapStage.GetStoryDemoAtStageSelect();
			if (storyDemoAtStageSelect != -1 && !mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, storyDemoAtStageSelect))
			{
				num2 = storyDemoAtStageSelect;
			}
		}
		if (num2 == -1 || mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, num2))
		{
			StartCoroutine(GrayOut());
			StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, num, string.Empty, 0));
			return;
		}
		StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
		storyDemoTemp.DemoIndex = num2;
		storyDemoTemp.SelectStage = num;
		storyDemoTemp.ClearStage = -1;
		mShowStoryDemoList.Add(storyDemoTemp);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected virtual IEnumerator StartStage(Def.SERIES a_series, int stage, string _DisplayStageNumber, int _LimitTimeOrMoves, int _NoticeStageNo = 0)
	{
		mState.Reset(STATE.NETWORK_STAGE, true);
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(a_series, CurrentEventID, out eventData);
		PlayerMapData data = eventData.CourseData[CurrentCourseID];
		data.CurrentLevel = stage;
		mGame.mPlayer.Data.SetMapData(a_series, data);
		int main;
		int sub;
		Def.SplitStageNo(stage, out main, out sub);
		stage = Def.GetStageNo(CurrentCourseID, main, sub);
		bool _stage_load_finished = false;
		bool _stage_load_success = false;
		float _start_time = Time.realtimeSinceStartup;
		GameStatePuzzle.STAGE_DISPLAY_NUMBER = _DisplayStageNumber;
		GameStatePuzzle.STAGE_LIMIT_TIMES_OR_MOVES = _LimitTimeOrMoves;
		string _DisplayStageNumber2 = default(string);
		int _LimitTimeOrMoves2 = default(int);
		StartCoroutine(SMStageData.CreateAsync(userSegment: mGame.GetUserSegment(), a_series: a_series, eventID: CurrentEventID, stage: stage, fn: delegate(int _stage, SMStageData.EventArgs _e)
		{
			if (_e != null)
			{
				_stage_load_success = true;
				GameStatePuzzle.STAGE_DATA_REQUIRE = false;
				mGame.mCurrentStage = _e.StageData;
				if (!string.IsNullOrEmpty(_DisplayStageNumber2))
				{
					mGame.mCurrentStage.DisplayStageNumber = _DisplayStageNumber2;
				}
				if (_LimitTimeOrMoves2 > 0)
				{
					Def.STAGE_LOSE_CONDITION loseType = mGame.mCurrentStage.LoseType;
					if (loseType != 0 && loseType == Def.STAGE_LOSE_CONDITION.TIME)
					{
						mGame.mCurrentStage.OverwriteLimitTimeSec = _LimitTimeOrMoves2;
					}
					else
					{
						mGame.mCurrentStage.OverwriteLimitMoves = _LimitTimeOrMoves2;
					}
				}
				short a_course;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(_stage, out a_course, out a_main, out a_sub);
				SMEventPageData eventPageData = GameMain.GetEventPageData(CurrentEventID);
				mGame.mCurrentMapPageData = eventPageData;
				mGame.mCurrentStageInfo = eventPageData.GetMapStage(a_main, a_sub, a_course);
				if (mGame.mCurrentStageInfo == null)
				{
					mGame.mCurrentStageInfo = new SMMapStageSetting();
				}
				else
				{
					GameStatePuzzle.STAGE_DISPLAY_NUMBER = mGame.mCurrentStageInfo.DisplayStageNo;
					mGame.mCurrentStage.DisplayStageNumber = mGame.mCurrentStageInfo.DisplayStageNo;
				}
			}
			_stage_load_finished = true;
		}));
		while (!_stage_load_finished)
		{
			yield return 0;
		}
		float _end_span = Time.realtimeSinceStartup - _start_time;
		if (!_stage_load_success || mGame.mCurrentStage == null)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string message = string.Format(Localization.Get("StageData_Error_Desc_02"));
			mConfirmDialog.Init(Localization.Get("StageData_Error_Title_02"), message, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnStageDataErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
			yield break;
		}
		if (mStageInfo == null)
		{
			mStageInfo = Util.CreateGameObject("StageInfoDialog", mRoot).AddComponent<StageInfoDialog>();
			mStageInfo.Init();
			mStageInfo.SetClosedCallback(OnStageInfoDialogClosed);
			mStageInfo.SetPartnerSelectCallback(OnStageCharaDialogOpen);
			mStageInfo.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			mStageInfo.SetBaseDepth(65);
		}
		SetScrollPower(0f);
	}

	private void OnStageDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		mConfirmDialog = null;
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnStageCharaDialogOpen()
	{
		mStageChara = Util.CreateGameObject("StageCharaDialog", mRoot).AddComponent<StageCharaDialog>();
		mStageChara.SetClosedCallback(OnStageCharaDialogClosed);
	}

	public void OnStageCharaDialogClosed(StageCharaDialog.SELECT_ITEM selectItem)
	{
		int selectedPartnerNo = mStageChara.SelectedPartnerNo;
		if (mStageInfo != null && selectedPartnerNo != -1)
		{
			mStageInfo.SetCompanion(selectedPartnerNo);
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
	}

	public void OnStageInfoDialogClosed(StageInfoDialog.SELECT_ITEM selectItem)
	{
		switch (selectItem)
		{
		case StageInfoDialog.SELECT_ITEM.PLAY:
		{
			List<StageRankingData> rankingData = null;
			if (base.RankingData != null)
			{
				rankingData = base.RankingData.results;
			}
			mGame.InitFriendsRanking(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, ref rankingData, mGame.mPlayer);
			mState.Change(STATE.PLAY);
			break;
		}
		case StageInfoDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		}
		if (mStageInfo != null)
		{
			UnityEngine.Object.Destroy(mStageInfo.gameObject);
			mStageInfo = null;
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
		if (GameMain.MaintenanceMode)
		{
			mGame.IsTitleReturnMaintenance = true;
			OnExitToTitle(true);
		}
	}

	public override void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void OnBackKeyPushed(bool ignore = false)
	{
		if (ignore || (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen))
		{
			OnEventExit(null);
		}
	}

	public void OnDebugPushed()
	{
		mState.Reset(STATE.WAIT, true);
		SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
		sMDDebugDialog.SetGameState(this);
		sMDDebugDialog.SetClosedCallback(delegate(SMDDebugDialog.SELECT_ITEM i)
		{
			if (i == SMDDebugDialog.SELECT_ITEM.PUSH_BACKKEY)
			{
				OnBackKeyPushed(true);
			}
			else
			{
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
		});
	}

	public void OnSDPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.EVENTMAP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(mRoot);
			mState.Reset(STATE.WAIT, true);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected new void OnPurchaseDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnHeartPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_EVENTMAP;
			BuyHeartDialog buyHeartDialog = Util.CreateGameObject("HeartDialog", mRoot).AddComponent<BuyHeartDialog>();
			mState.Reset(STATE.WAIT, true);
			buyHeartDialog.SetClosedCallback(OnHeartDialogDialogClosed);
			buyHeartDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected void OnHeartDialogDialogClosed(BuyHeartDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnMugenHeartButtonPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && mCockpit.mMugenflg && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
		{
			mState.Reset(STATE.WAIT, true);
			base.OnMugenHeartButtonPushed(go);
		}
	}

	public override void OnMugenHeartDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnMugenHeartDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void CreateMugenHeartDialog(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START_WAIT;
		}
		else
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.END_WAIT;
		}
		mState.Reset(STATE.WAIT, true);
		base.CreateMugenHeartDialog(i);
	}

	public override void OnMugenDialogDialogClosed(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			base.OnMugenDialogDialogClosed(i);
			return;
		}
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.NONE;
		mGame.mPlayer.Data.mUseMugenHeartSetTime = 0;
		mGame.Save();
		base.OnMugenDialogDialogClosed(i);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void StartMugenHeart()
	{
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START;
		mGame.mPlayer.Data.MugenHeartUseTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		Def.ITEMGET_TYPE a_type;
		mGame.mPlayer.SubBoosterNum(Def.MugenHeartTimeToKindData[mGame.mPlayer.Data.mUseMugenHeartSetTime], 1, out a_type);
		if (mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) != mGame.mPlayer.Data.MaxNormalLifeCount)
		{
			mGame.mPlayer.SetItemCount(Def.ITEM_CATEGORY.CONSUME, 1, mGame.mPlayer.Data.MaxNormalLifeCount);
		}
		mGame.Save();
		base.StartMugenHeart();
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnStoreBagPushed(GameObject go, bool linkBanner = false)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && mCockpit.mShopflg && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
		{
			mState.Reset(STATE.WAIT, true);
			base.OnStoreBagPushed(go);
		}
	}

	public override void OnStoreBagDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnStoreBagDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		mState.Change((STATE)state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	private void OnAcNotFoundDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mGame.mPlayer.AddAccessory_AllInGift(mGemAccessoryList[0], CurrentEventID);
		mGame.Save();
		mIsGotAccessoryList.Add(mGemAccessoryList[0]);
		mGemAccessoryList.RemoveAt(0);
		if (mGemAccessoryList.Count > 0)
		{
			mState.Change(STATE.NETWORK_REWARD_SET);
			return;
		}
		if (mAdvAccessoryList.Count > 0)
		{
			mState.Change(STATE.NETWORK_ADV_REWARD_SET);
			return;
		}
		if (!mIsStarCompEffect)
		{
			mState.Change(STATE.REWARD_EFFECT);
			return;
		}
		mIsStarCompEffect = false;
		CreateRewardDialog();
		mState.Change(STATE.WAIT);
	}

	protected void OnConnectErrorAuthAbandon()
	{
		mGame.IsTitleReturnMaintenance = true;
		mGame.PlaySe("SE_NEGATIVE", -1);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void OnConnectErrorSegmentRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAccessoryRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected virtual void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugLoginBonusTimeReset)
			{
				mGame.mPlayer.Data.LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
				mGame.mDebugLoginBonusTimeReset = false;
			}
			if (mState.GetStatus() == STATE.MAIN)
			{
				mState.Reset(STATE.NETWORK_RESUME_MAINTENACE, true);
			}
			else
			{
				mGame.DoLoginBonusServerCheck = true;
			}
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	protected override bool SetDeviceScreen(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		Map_DeviceScreenSizeY = vector.y;
		Event_DeviceOffset = 213f;
		bool result = false;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			MAP_BOUNDS_RANGE = 67f;
			Map_DeviceOffset = Event_DeviceOffset;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			MAP_BOUNDS_RANGE = 67f * mGame.mDeviceRatioScale;
			Map_DeviceOffset = Event_DeviceOffset + (1136f - vector.y) / 2f * mGame.mDeviceRatioScale;
			result = true;
			break;
		}
		return result;
	}

	protected override void SetLayout(ScreenOrientation o)
	{
		if (mSocialRanking != null)
		{
			UnityEngine.Object.Destroy(mSocialRanking.gameObject);
			mSocialRanking = null;
			Network_OnStageWait();
		}
		mMapPageRoot.transform.parent = mAnchorCenter.gameObject.transform;
		mMapPageRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
		mMapPageRoot.transform.localScale = new Vector3(mGame.mDeviceRatioScale, mGame.mDeviceRatioScale, 1f);
		bool flag = SetDeviceScreen(o);
		if (mCockpit == null)
		{
			mCockpit = Util.CreateGameObject("EventBingoCockpit", base.gameObject).AddComponent<EventBingoCockpit>();
			if (mState.GetStatus() == STATE.MAIN)
			{
				mCockpit.Init(this);
			}
			else
			{
				mCockpit.Init(this, false);
			}
			mCockpit.SetCallback(OnCockpitOpend, OnCockpitClosed);
			EventBingoCockpit eventBingoCockpit = mCockpit as EventBingoCockpit;
			eventBingoCockpit.SetTotalBingoNum(mTotalBingoNum);
		}
		GiftAttentionCheck();
		if (mState.GetStatus() == STATE.MAIN)
		{
			mCockpit.SetEnableButtonAction(true);
		}
		else
		{
			mCockpit.SetEnableButtonAction(false);
		}
		EventBingoCockpit eventBingoCockpit2 = mCockpit as EventBingoCockpit;
		if (eventBingoCockpit2 != null)
		{
			eventBingoCockpit2.CreateEventTitleBoard();
			eventBingoCockpit2.SetLayout(o);
		}
		if (!flag)
		{
			if (mBGRoot != null)
			{
				mBGRoot.transform.localPosition = new Vector3(mBGRoot.transform.localPosition.x, 0f, Def.MAP_BASE_Z);
			}
			if (mSlideBingoPanel != null)
			{
				mSlideBingoPanel.transform.SetParent(mMapPageRoot.transform);
				mSlideBingoPanel.transform.localPosition = new Vector3(0f, 25f, 0f);
				mSlideBingoPanel.transform.localScale = new Vector3(1f, 1f);
				mSlideBingoPanel.ForceEndFlick();
			}
			if (mRewardPanel != null)
			{
				mRewardPanel.transform.SetParent(mAnchorBottom.transform);
				mRewardPanel.transform.localPosition = new Vector3(0f, 180f, 0f);
				mRewardPanel.transform.localScale = new Vector3(1f, 1f);
				mRewardPanel.gameObject.SetActive(false);
				mRewardPanel.gameObject.SetActive(true);
			}
		}
		else
		{
			if (mBGRoot != null)
			{
				mBGRoot.transform.localPosition = new Vector3(mBGRoot.transform.localPosition.x, -280f, Def.MAP_BASE_Z);
			}
			if (mSlideBingoPanel != null)
			{
				mSlideBingoPanel.transform.SetParent(mMapPageRoot.transform);
				if (Util.IsWideScreen())
				{
					mSlideBingoPanel.transform.localPosition = new Vector3(225f, -15f, 0f);
				}
				else
				{
					mSlideBingoPanel.transform.localPosition = new Vector3(240f, -15f, 0f);
				}
				mSlideBingoPanel.transform.localScale = new Vector3(1f / mMapPageRoot.transform.localScale.x * 0.9f, 1f / mMapPageRoot.transform.localScale.y * 0.9f);
				mSlideBingoPanel.ForceEndFlick();
			}
			if (mRewardPanel != null)
			{
				mRewardPanel.transform.SetParent(mAnchorBottomLeft.transform);
				mRewardPanel.transform.localPosition = new Vector3(288f, 180f, 0f);
				mRewardPanel.transform.localScale = new Vector3(0.9f, 0.9f);
				mRewardPanel.gameObject.SetActive(false);
				mRewardPanel.gameObject.SetActive(true);
			}
		}
		SetLayoutLotterySkipButton(flag);
	}

	public void SetLayoutLotterySkipButton(bool a_islandscape)
	{
		if (!a_islandscape)
		{
			mSkipButtonPos = new Vector3(-80f, 67f, 0f);
			mSkipButtonScale = 1f;
		}
		else
		{
			mSkipButtonPos = new Vector3(-80f, 67f, 0f);
			mSkipButtonScale = 1f;
		}
		if (mLotterySkipButton != null)
		{
			mLotterySkipButton.transform.localPosition = mSkipButtonPos;
			mLotterySkipButton.transform.localScale = new Vector3(mSkipButtonScale, mSkipButtonScale, 1f);
		}
	}

	public override bool OnDrawCockpit()
	{
		return true;
	}

	protected void OnCockpitOpend(bool opening)
	{
	}

	protected void OnCockpitClosed(bool closing)
	{
	}

	public override bool IsEnableButton()
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return false;
		}
		return true;
	}

	protected void OnGotErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void OnMapDestroyUpDateClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override void OnMapDestroyClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void OnClearAnimationFinished(SsSprite sprite)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnCompleteAnimationFinished(SsSprite sprite)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnCourseNotifyDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void OnExitToTitle(bool force)
	{
		if ((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || force)
		{
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mState.Change(STATE.UNLOAD_WAIT);
		}
	}

	public void OnGiftPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			JellyImageButton component = go.GetComponent<JellyImageButton>();
			if (component.IsEnable() && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
			{
				StartCoroutine("GrayOut");
				mState.Reset(STATE.WAIT, true);
				mGiftFullDialog = Util.CreateGameObject("GiftDialog", mRoot).AddComponent<GiftFullDialog>();
				mGiftFullDialog.SetClosedCallback(OnGiftDialogClosed);
			}
		}
	}

	public void OnGiftDialogClosed(bool stageOpen, bool friendHelp, bool friendhelpSuccess, bool advChara, bool advTicket)
	{
		if (stageOpen)
		{
			mState.Reset(STATE.WAIT);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Gift_AferReturnTitle"), Localization.Get("Gift_AferReturnDesc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			return;
		}
		if (friendHelp && mGame.PlayingFriendHelp != null)
		{
			StartCoroutine(GrayOut());
			StartCoroutine(StartStage((Def.SERIES)mGame.PlayingFriendHelp.Gift.Data.ItemKind, mGame.PlayingFriendHelp.Gift.Data.ItemID, string.Empty, 0));
			return;
		}
		if (mCockpit != null)
		{
			mCockpit.SetEnableMailAttention(mGame.mPlayer.HasModifiedGifts());
		}
		StartCoroutine("GrayIn");
		if (friendhelpSuccess)
		{
			int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
			int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
			mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, -1);
			mState.Change(STATE.UNLOCK_ACTING);
		}
		else if (mGame.AddedNewFriend)
		{
			mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
			mState.Change(STATE.NETWORK_PROLOGUE);
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	protected override bool GiftStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return false;
		}
		return base.GiftStartCheck();
	}

	public void OnGameCenterPushed(GameObject go)
	{
		JellyImageButton component = go.GetComponent<JellyImageButton>();
		if (component == null || mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return;
		}
		if (!component.IsEnable() || !mGame.EnableAdvEnter())
		{
			OnGameCenterNoEnterPushed(go);
		}
		else
		{
			if (mSlideBingoPanel == null || !mSlideBingoPanel.IsPanelNoMove)
			{
				return;
			}
			EventCockpit cockpit = mCockpit as EventCockpit;
			Action action = delegate
			{
				if (!mGame.IsSeasonEventExpired(CurrentEventID) && cockpit != null && cockpit.IsOldEventFinished)
				{
					mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("EvMap_GameCenter_Conf_Expired_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
					mConfirmDialog.SetClosedCallback(OnMoveToMap);
				}
				else
				{
					mEventAdvConfirmDialog = Util.CreateGameObject("EventAdvConfirmDialog", mRoot).AddComponent<EventAdvConfirmDialog>();
					mEventAdvConfirmDialog.Init(mGame.IsSeasonEventExpired(CurrentEventID), true);
					mEventAdvConfirmDialog.SetClosedCallback(delegate(EventAdvConfirmDialog.SELECT_ITEM i)
					{
						if (i == EventAdvConfirmDialog.SELECT_ITEM.POSITIVE)
						{
							if (!mGame.EnableAdvEnter())
							{
								UnityEngine.Object.Destroy(mEventAdvConfirmDialog.gameObject);
								mEventAdvConfirmDialog = null;
								OnGameCenterNoEnterPushed(go);
							}
							else
							{
								bool[] wonHistory = mGame.GetWonHistory(0, 10);
								int a_main;
								int a_sub;
								Def.SplitStageNo(mGame.mPlayer.PlayableMaxLevel, out a_main, out a_sub);
								PlayerEventData data;
								mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
								ServerCram.TapIconArcade(CurrentEventID, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, CurrentEventID * 1000000 + data.CourseID * 100000 + mGame.mPlayer.PlayableMaxLevel, mGame.LastWon, (wonHistory != null) ? wonHistory.Length : 0, mGame.GetWonCount(0, 10, false), mGame.GetWonMaxCountContinue(0, 10, false), GetPossessCompanionCramValue(), mGame.mPlayer.LifeCount, mGame.LastStar);
								mGame.EnterADV();
							}
						}
						else
						{
							StartCoroutine(GrayIn());
							mState.Change(STATE.UNLOCK_ACTING);
						}
						mEventAdvConfirmDialog = null;
					});
				}
			};
			if (cockpit == null || !cockpit.TryOpenOldEventExtensionDialog(delegate
			{
				mState.Change(STATE.MAIN);
			}, action))
			{
				action();
			}
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnGameCenterNoEnterPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mAdvNotEnterDialog = Util.CreateGameObject("mAdvNotEnterDialog", mRoot).AddComponent<AdvNotEnterDialog>();
			if (!mGame.EnableAdvSession())
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.MAINTENANCE_ADV);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
			else
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.STAGE_NO_CLEAR);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
		}
	}

	public void AdvNotEnterDialogClosed()
	{
		if (mAdvNotEnterDialog != null)
		{
			UnityEngine.Object.Destroy(mAdvNotEnterDialog.gameObject);
			mAdvNotEnterDialog = null;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnTitleReturnMessageClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGame.StopMusic(0, 0.5f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public override void DEBUG_ChangeStateToInit()
	{
		mState.Change(STATE.INIT);
	}

	public override void ReEnterMap()
	{
		mState.Reset(STATE.GOTO_REENTER);
	}

	private void CreateBingoBoard()
	{
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		BIJImage image = ResourceManager.LoadImage(sMEventPageData.EventSetting.MapAtlasKey).Image;
		List<short> list = new List<short>();
		for (short num = 1; num < 5; num++)
		{
			int index = sMEventPageData.BingoMapSetting.CourseOpenKeyIdList[num];
			if (!mGame.mPlayer.IsAccessoryUnlock(index))
			{
				list.Add(num);
			}
		}
		mSlideBingoPanel = Util.CreateGameObject("SlideBingoRoot", mMapPageRoot).AddComponent<SlideBingoPanel>();
		mSlideBingoPanel.Init(CurrentEventID, image, sMEventPageData.BingoStageList, mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData, list);
		mSlideBingoPanel.setCallBack(OnStageButtonPushed, OnStageButtonDisabled, OnSetStateWait, OnSlideStart);
		mSlideBingoPanel.setSlideMovedCallBack(OnSlideMoved);
		mSlideBingoPanel.transform.localPosition = new Vector3(0f, 25f, 0f);
		List<int> list2 = new List<int>(sMEventPageData.EventCourseList.Keys);
		mStageClearNumArray = new short[list2.Count];
		for (int i = 0; i < list2.Count; i++)
		{
			int num2 = list2[i];
			List<SMMapStageSetting> mapStages = sMEventPageData.GetMapStages(num2);
			mSlideBingoPanel.AddSlideBingoPanel((short)num2, mapStages);
			short num3 = 0;
			for (short num4 = 1; num4 < 10; num4++)
			{
				PlayerClearData _psd = null;
				SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, (short)i, Def.GetStageNo(num4, 0), out _psd);
				if (_psd != null)
				{
					num3++;
				}
			}
			mStageClearNumArray[i] = num3;
		}
	}

	private void UpdateSlideBG()
	{
		if (mBGRoot != null && mSlideBingoPanel != null)
		{
			float num = mSlideBingoPanel.mPosX / 10f;
			mBGRoot.transform.localPosition = new Vector3(num + 128f, mBGRoot.transform.localPosition.y, Def.MAP_BASE_Z);
		}
	}

	private void OnSlideStart(short courseNum)
	{
	}

	private void OnSlideMoved(short courseNum)
	{
		if (mState.GetStatus() == STATE.MOVEWAIT)
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
		CurrentCourseID = courseNum;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
		data.CourseID = CurrentCourseID;
		UpdateReLotteryBottonStatus();
		IsAllBingoSheet();
	}

	private void UpdateReLotteryBottonStatus()
	{
		if (mCockpit != null)
		{
			bool setEnable = true;
			short num = mStageClearNumArray[CurrentCourseID];
			if (num >= 9)
			{
				setEnable = false;
			}
			if (num >= 8)
			{
				setEnable = false;
			}
			if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Count < CurrentCourseID + 1)
			{
				setEnable = false;
			}
			if (mGame.IsSeasonEventExpired(CurrentEventID))
			{
				setEnable = false;
			}
			EventBingoCockpit eventBingoCockpit = mCockpit as EventBingoCockpit;
			eventBingoCockpit.UpdateReLotteryButton(setEnable);
		}
	}

	private void CreateRewardList()
	{
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		List<int> list = new List<int>(sMEventPageData.EventRewardList.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			BingoRewardListData bingoRewardListData = new BingoRewardListData();
			SMEventRewardSetting sMEventRewardSetting = sMEventPageData.EventRewardList[list[i]];
			AccessoryData accessoryData = mGame.GetAccessoryData(sMEventRewardSetting.RewardID);
			if (accessoryData != null)
			{
				switch (accessoryData.AccessoryType)
				{
				case AccessoryData.ACCESSORY_TYPE.BOOSTER:
				{
					Def.BOOSTER_KIND a_kind;
					accessoryData.GetBooster(out a_kind, out bingoRewardListData.mNum);
					bingoRewardListData.mImgName = mGame.mBoosterData[(int)a_kind].iconBuy;
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				}
				case AccessoryData.ACCESSORY_TYPE.ADV_ITEM:
					if (mGame.mAdvRewardListDict.ContainsKey(int.Parse(accessoryData.GotID)))
					{
						AdvRewardListData advRewardListData = mGame.mAdvRewardListDict[int.Parse(accessoryData.GotID)];
						bingoRewardListData.mImgName = advRewardListData.IconKey;
						bingoRewardListData.mNum = (int)advRewardListData.Value;
						bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					}
					break;
				case AccessoryData.ACCESSORY_TYPE.GEM:
					bingoRewardListData.mImgName = "icon_84jem";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				case AccessoryData.ACCESSORY_TYPE.GROWUP:
					bingoRewardListData.mImgName = "icon_level";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
					bingoRewardListData.mImgName = "icon_dailyEvent_stamp02";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				case AccessoryData.ACCESSORY_TYPE.HEART:
					bingoRewardListData.mImgName = "icon_heart";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
					bingoRewardListData.mImgName = "icon_dailyEvent_stamp01";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				case AccessoryData.ACCESSORY_TYPE.BINGO_KEY:
					bingoRewardListData.mImgName = "icon_eventroadblock_key01";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				case AccessoryData.ACCESSORY_TYPE.BINGO_TICKET:
					bingoRewardListData.mImgName = "icon_event_ticket00";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				case AccessoryData.ACCESSORY_TYPE.PARTNER:
				{
					CompanionData companionData = mGame.mCompanionData[int.Parse(accessoryData.GotID)];
					bingoRewardListData.mImgName = companionData.iconName;
					bingoRewardListData.mIsPartner = true;
					bingoRewardListData.mScaleX = 0.45f;
					bingoRewardListData.mScaleY = 0.45f;
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				}
				case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
					bingoRewardListData.mImgName = "icon_old_event_key";
					bingoRewardListData.mNum = int.Parse(accessoryData.GotID);
					bingoRewardListData.mIsGot = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
					break;
				}
			}
			mList.Add(bingoRewardListData);
		}
		mRewardListInfo = new SMVerticalListInfo(1, 585f, 70f, 1, 585f, 70f, 0, 70);
		mRewardList = SMVerticalList.Make(mRewardPanel.gameObject, this, mRewardListInfo, mList, 0f, -5f, 30, false, UIScrollView.Movement.Horizontal);
		mRewardList.OnCreateItem = OnCreateItem;
		mRewardList.OnCreateFinish = OnCreateFinish;
		mRewardList.HideScrollBar();
	}

	private void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		BingoRewardListData bingoRewardListData = item as BingoRewardListData;
		float cellWidth = mRewardList.mList.cellWidth;
		float cellHeight = mRewardList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", 31, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		BIJImage image2 = ResourceManager.LoadImage(sMEventPageData.EventSetting.MapAtlasKey).Image;
		UISprite uISprite2 = Util.CreateSprite("RewardItem", itemGO, image2);
		Util.SetSpriteInfo(uISprite2, "event_panel01", 32, new Vector3(0f, 0f, 0f), Vector3.one, false);
		BIJImage atlas = image;
		if (bingoRewardListData.mIsPartner)
		{
			atlas = ResourceManager.LoadImage("ICON").Image;
		}
		UISprite uISprite3 = Util.CreateSprite("RewardIcon", uISprite2.gameObject, atlas);
		Util.SetSpriteInfo(uISprite3, bingoRewardListData.mImgName, 33, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite3.gameObject.transform.localScale = new Vector3(bingoRewardListData.mScaleX, bingoRewardListData.mScaleY);
		if (bingoRewardListData.mNum > 0)
		{
			UILabel uILabel = Util.CreateLabel("RewardNum", uISprite2.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Format(Localization.Get("Gift_Number"), bingoRewardListData.mNum), 34, new Vector3(27f, -20f, 0f), 16, 0, 0, UIWidget.Pivot.Right);
			uILabel.color = new Color(0.4f, 0.2627451f, 0.14901961f);
			uILabel.effectStyle = UILabel.Effect.Outline8;
			uILabel.effectColor = Color.white;
			uILabel.effectDistance = new Vector2(2f, 2f);
		}
		if (bingoRewardListData.mIsGot)
		{
			UISprite sprite = Util.CreateSprite("GotStamp", uISprite2.gameObject, image2);
			Util.SetSpriteInfo(sprite, "clearstamp", 35, new Vector3(0f, 0f, 0f), Vector3.one, false);
			AddStampReward(index);
		}
		else
		{
			mRewardPanelGoDict[index] = uISprite2.gameObject;
		}
	}

	private void OnCreateFinish()
	{
		float num = 0.031f;
		if (mRewardPanelGoDict.Count > 0)
		{
			List<int> list = new List<int>(mRewardPanelGoDict.Keys);
			list.Sort();
			float num2 = (float)list[0] - 3f;
			if (num2 > 0f)
			{
				mRewardList.ScrollSet(num * num2);
			}
		}
		else
		{
			mRewardList.ScrollSet(1f);
		}
		for (int i = 0; i < mRewardList.ListItems.Count; i++)
		{
			BoxCollider component = mRewardList.ListItems[i].GetComponent<BoxCollider>();
			if (component != null)
			{
				mRewardPanelBoxColliderList.Add(component);
			}
		}
		UpdateNextRewardImage();
	}

	private IEnumerator GetRewardInfoConnect()
	{
		List<int> BingoAdvRewardList = new List<int>();
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		List<int> keys = new List<int>(eventPageData.EventRewardList.Keys);
		for (int i = 0; i < keys.Count; i++)
		{
			BingoRewardListData data = new BingoRewardListData();
			SMEventRewardSetting eventRewardData = eventPageData.EventRewardList[keys[i]];
			AccessoryData accessoryData = mGame.GetAccessoryData(eventRewardData.RewardID);
			if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
			{
				BingoAdvRewardList.Add(int.Parse(accessoryData.GotID));
			}
		}
		int[] rewardArray2 = null;
		if (BingoAdvRewardList.Count > 0)
		{
			rewardArray2 = mGame.CheckRewardListData(BingoAdvRewardList.ToArray());
			if (rewardArray2 == null)
			{
				mGetRewardInfoConnectComplete = true;
				yield break;
			}
			GetRewardInfoConnectMode = 0;
			while (true)
			{
				switch (GetRewardInfoConnectMode)
				{
				case 0:
					mGame.Network_AdvGetRewardInfo(rewardArray2);
					GetRewardInfoConnectMode = 1;
					break;
				case 1:
					if (GameMain.mAdvGetRewardInfoCheckDone)
					{
						if (GameMain.mAdvGetRewardInfoSucceed)
						{
							mGetRewardInfoConnectComplete = true;
							GetRewardInfoConnectMode = -1;
						}
						else
						{
							mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 0, OnGerRewardInfoRetry, null);
							GetRewardInfoConnectMode = 99;
						}
					}
					break;
				}
				if (GetRewardInfoConnectMode == -1)
				{
					break;
				}
				yield return null;
			}
		}
		else
		{
			mGetRewardInfoConnectComplete = true;
		}
	}

	protected void OnGerRewardInfoRetry(int a_state)
	{
		GetRewardInfoConnectMode = a_state;
		StartCoroutine(GrayIn());
	}

	private bool LotteryStartCheck(bool IsReLottery = false, bool IsNewOpen = false)
	{
		bool flag = false;
		bool flag2 = false;
		List<short> list = new List<short>();
		bool flag3 = false;
		for (short num = 1; num < 10; num++)
		{
			PlayerClearData _psd = null;
			SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, CurrentCourseID, Def.GetStageNo(num, 0), out _psd);
			if (_psd != null)
			{
				list.Add(num);
			}
		}
		if (list.Count >= 9)
		{
			return flag;
		}
		if (IsReLottery)
		{
			if (list.Count >= 8)
			{
				return flag;
			}
			if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Count > CurrentCourseID)
			{
				flag = true;
			}
		}
		else if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Count <= 0)
		{
			flag = true;
			flag3 = true;
			lotteryStageNum = 5;
			mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Add(lotteryStageNum);
			flag2 = true;
		}
		else if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Count > CurrentCourseID)
		{
			short num2 = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData[CurrentCourseID];
			for (int i = 0; i < list.Count; i++)
			{
				if (num2 == list[i])
				{
					flag = true;
					break;
				}
			}
		}
		else if (IsNewOpen)
		{
			flag = true;
			mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Add(0);
		}
		if (flag)
		{
			short num3 = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData[CurrentCourseID];
			while (!flag3)
			{
				int num4 = UnityEngine.Random.Range(1, 10);
				bool flag4 = true;
				for (int j = 0; j < list.Count; j++)
				{
					if (num4 == list[j])
					{
						flag4 = false;
						break;
					}
				}
				if (flag4 && num4 != num3)
				{
					lotteryStageNum = (short)num4;
					flag3 = true;
					mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData[CurrentCourseID] = lotteryStageNum;
				}
			}
			if (IsReLottery)
			{
				if (mReLotteryMode == ReLotteryDialog.MODE.HEART)
				{
					mGame.mPlayer.SubLifeCount(1);
				}
				else if (mReLotteryMode == ReLotteryDialog.MODE.TICKET && mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReLotteryTicket > 0)
				{
					mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReLotteryTicket--;
				}
			}
			mGame.Save();
			int currentEventID = CurrentEventID;
			int currentCourseID = CurrentCourseID;
			int evt_bng = mTotalBingoNum;
			int totalStar = mSlideBingoPanel.GetTotalStar();
			int old_stg = ((!flag2 && !IsNewOpen) ? (CurrentEventID * 1000000 + CurrentCourseID * 100000 + num3 * 100) : 0);
			int new_stg = CurrentEventID * 1000000 + CurrentCourseID * 100000 + lotteryStageNum * 100;
			int num5 = 0;
			int use_amount = 0;
			if (IsReLottery)
			{
				num5 = (int)mReLotteryMode;
				if (mReLotteryMode == ReLotteryDialog.MODE.HEART || mReLotteryMode == ReLotteryDialog.MODE.TICKET)
				{
					use_amount = 1;
				}
			}
			int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
			int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
			int itemCount = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1);
			int mReLotteryTicket = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReLotteryTicket;
			if (num5 > 0)
			{
				ServerCram.EventBingoLottery(currentEventID, currentCourseID, old_stg, new_stg, num5, use_amount, evt_bng, totalStar, lastPlayLevel, lastPlaySeries, itemCount, mReLotteryTicket);
			}
			StartCoroutine(GrayOut());
			mState.Change(STATE.LOTTERY_EFFECT);
		}
		return flag;
	}

	public void OnReLotteryButtonPushed(GameObject go)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		JellyImageButton component = go.GetComponent<JellyImageButton>();
		if (component.IsEnable() && !(mSlideBingoPanel == null) && mSlideBingoPanel.IsPanelNoMove)
		{
			int itemCount = 0;
			ReLotteryDialog.MODE mODE = ReLotteryDialog.MODE.MUGEN_HEART;
			if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
			{
				mReLotteryMode = ReLotteryDialog.MODE.MUGEN_HEART;
				mODE = ReLotteryDialog.MODE.MUGEN_HEART;
			}
			else if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReLotteryTicket > 0)
			{
				mReLotteryMode = ReLotteryDialog.MODE.TICKET;
				mODE = ReLotteryDialog.MODE.TICKET;
				itemCount = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReLotteryTicket;
			}
			else if (mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) > 0)
			{
				mReLotteryMode = ReLotteryDialog.MODE.HEART;
				mODE = ReLotteryDialog.MODE.HEART;
				itemCount = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1);
				itemCount += mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 2);
			}
			else
			{
				mReLotteryMode = ReLotteryDialog.MODE.HEART;
				mODE = ReLotteryDialog.MODE.HEART_LESS;
				itemCount = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1);
				itemCount += mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 2);
			}
			EventBingoCockpit eventBingoCockpit = mCockpit as EventBingoCockpit;
			eventBingoCockpit.MoveReLotteryButton(true);
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			string nekoImg = "chara_bng01";
			if (sMEventPageData.BingoMapSetting.ReLotteryLive2d == "LUNA")
			{
				nekoImg = "chara_bng01";
			}
			StartCoroutine(GrayOut());
			ReLotteryDialog reLotteryDialog = Util.CreateGameObject("ReLotteryDialog", mRoot).AddComponent<ReLotteryDialog>();
			reLotteryDialog.Init(mReLotteryMode, itemCount, nekoImg);
			reLotteryDialog.SetDialogMode(mODE);
			reLotteryDialog.SetClosedCallback(OnRelotteryDialogClosed);
			reLotteryDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			mState.Reset(STATE.WAIT, true);
		}
	}

	private IEnumerator JumpUpLive2dChara()
	{
		EventCockpit evCockpit = mCockpit as EventCockpit;
		if (evCockpit != null)
		{
			yield return StartCoroutine(evCockpit.JumpUpLive2dCharacter(CompanionData.MOTION.JUMP));
		}
		StartCoroutine(GrayOut());
		mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].GotBingoReward(mGetPartner.Index);
		mGetPartnerDialog = Util.CreateGameObject("GetPartnerDialog", mRoot).AddComponent<GetPartnerDialog>();
		mGetPartnerDialog.Init(mGetPartner, GetPartnerDialog.PLACE.MAIN, true, true);
		mGetPartnerDialog.SetClosedCallback(OnGetPartnerDialogClosed);
	}

	private IEnumerator JumpDownLive2dChara()
	{
		EventCockpit evCockpit = mCockpit as EventCockpit;
		if (evCockpit != null)
		{
			yield return StartCoroutine(evCockpit.JumpDownLive2dCharacter(CompanionData.MOTION.LANDING));
		}
		CommonRewardDialogCheck();
	}

	private string GetLive2DMotionName()
	{
		string empty = string.Empty;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		if (mTotalBingoNum < sMEventPageData.BingoMapSetting.FirstGetBingoNum)
		{
			return sMEventPageData.BingoMapSetting.EventLive2dMotionLv2;
		}
		if (mTotalBingoNum < sMEventPageData.BingoMapSetting.SecondGetBingoNum)
		{
			return sMEventPageData.BingoMapSetting.EventLive2dMotionLv5;
		}
		return sMEventPageData.BingoMapSetting.EventLive2dMotionComp;
	}

	private void OnRelotteryDialogClosed(ReLotteryDialog.SELECT_ITEM i)
	{
		if (mState.GetStatus() != STATE.WAIT)
		{
			return;
		}
		StartCoroutine(GrayIn());
		if (i == ReLotteryDialog.SELECT_ITEM.OK)
		{
			if (!IsEventExpired(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerExpired_Event_Desc"), delegate
			{
				mConfirmDialog = null;
				StartCoroutine(GrayIn());
				EventBingoCockpit eventBingoCockpit3 = mCockpit as EventBingoCockpit;
				eventBingoCockpit3.MoveReLotteryButton(false);
				mState.Change(STATE.MAIN);
			}) && !LotteryStartCheck(true))
			{
				EventBingoCockpit eventBingoCockpit = mCockpit as EventBingoCockpit;
				eventBingoCockpit.MoveReLotteryButton(false);
				mState.Reset(STATE.MAIN, true);
			}
		}
		else
		{
			EventBingoCockpit eventBingoCockpit2 = mCockpit as EventBingoCockpit;
			eventBingoCockpit2.MoveReLotteryButton(false);
			mState.Reset(STATE.MAIN, true);
		}
	}

	private void OnStageButtonDisabled()
	{
	}

	private void OnSetStateWait()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.MOVEWAIT, true);
		}
	}

	private void OnDisableDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mConfirmDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private void OnTestMove()
	{
	}

	private bool OpenNewCourseStartCheck()
	{
		bool result = false;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		int count = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Count;
		if (count == sMEventPageData.BingoMapSetting.CourseOpenKeyIdList.Count)
		{
			return result;
		}
		int num = sMEventPageData.BingoMapSetting.CourseOpenKeyIdList[count];
		if (mGame.mPlayer.IsAccessoryUnlock(num))
		{
			result = true;
			NewOpenCourseId = (short)count;
			List<AccessoryData> list = new List<AccessoryData>();
			list.Add(mGame.GetAccessoryData(num));
			StartCoroutine(GrayOut());
			GetRewardDialog getRewardDialog = Util.CreateGameObject("GetKeyDialog", mRoot).AddComponent<GetRewardDialog>();
			getRewardDialog.Init(list);
			getRewardDialog.SetClosedCallback(OnGetKeyDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		return result;
	}

	private void OnGetKeyDialogClosed()
	{
		StartCoroutine(GrayIn());
		mSlideBingoPanel.SetMoveSlide(NewOpenCourseId, true);
		mGame.SetMusicVolume(0.4f, 0);
		mState.Change(STATE.NEWCOURSE_OPEN);
	}

	private bool ReachEffectStartCheck()
	{
		bool result = false;
		List<short> list = new List<short>();
		for (short num = 1; num < 10; num++)
		{
			PlayerClearData _psd = null;
			SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, CurrentCourseID, Def.GetStageNo(num, 0), out _psd);
			if (_psd != null)
			{
				list.Add(num);
			}
		}
		if (list.Count <= 1)
		{
			return result;
		}
		List<bool> list2 = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReachEffectList[CurrentCourseID];
		mReachList.Clear();
		GetLineEffectList(list2, list, mReachList, 2);
		if (mReachList.Count > 0)
		{
			result = true;
			for (int i = 0; i < mReachList.Count; i++)
			{
				list2[mReachList[i]] = true;
			}
			mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mReachEffectList[CurrentCourseID] = list2;
			mState.Reset(STATE.REACH_LINE_EFFECT, true);
		}
		return result;
	}

	private void OnReachDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mConfirmDialog = null;
		mState.Change(STATE.INIT);
	}

	private bool BingoEffectStartCheck()
	{
		bool result = false;
		List<short> list = new List<short>();
		for (short num = 1; num < 10; num++)
		{
			PlayerClearData _psd = null;
			SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, CurrentCourseID, Def.GetStageNo(num, 0), out _psd);
			if (_psd != null)
			{
				list.Add(num);
			}
		}
		if (list.Count <= 2)
		{
			return result;
		}
		List<bool> list2 = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mBingoEffectList[CurrentCourseID];
		mBingoList.Clear();
		GetLineEffectList(list2, list, mBingoList, 3);
		if (mBingoList.Count > 0)
		{
			result = true;
			for (int i = 0; i < mBingoList.Count; i++)
			{
				list2[mBingoList[i]] = true;
				mBingoList_Cram.Add(mBingoList[i]);
			}
			mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mBingoEffectList[CurrentCourseID] = list2;
			mState.Change(STATE.BINGO_LINE_EFFECT);
		}
		return result;
	}

	private void GetLineEffectList(List<bool> _EffectDataList, List<short> _ClearStageList, List<short> _AddList, int _FoundCount)
	{
		for (short num = 0; num < 8; num++)
		{
			if (!_EffectDataList[num])
			{
				int num2 = num * 3;
				int num3 = 0;
				for (int i = 0; i < _ClearStageList.Count; i++)
				{
					for (int j = 0; j < 3; j++)
					{
						if (_ClearStageList[i] == mBingoLine[num2 + j])
						{
							num3++;
							break;
						}
					}
					if (num3 == _FoundCount)
					{
						_AddList.Add(num);
						break;
					}
				}
			}
		}
	}

	private void UpdateTotalBingoCount()
	{
		mTotalBingoNum = 0;
		List<short> list = new List<short>();
		for (short num = 0; num < 5; num++)
		{
			list.Clear();
			for (short num2 = 1; num2 < 10; num2++)
			{
				PlayerClearData _psd = null;
				SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, num, Def.GetStageNo(num2, 0), out _psd);
				if (_psd != null)
				{
					list.Add(num2);
				}
			}
			for (short num3 = 0; num3 < 8; num3++)
			{
				int num4 = num3 * 3;
				int num5 = 0;
				for (int i = 0; i < list.Count; i++)
				{
					for (int j = 0; j < 3; j++)
					{
						if (mBingoLine[num4 + j] == list[i])
						{
							num5++;
							break;
						}
					}
					if (num5 == 3)
					{
						mTotalBingoNum++;
						break;
					}
				}
			}
		}
	}

	private IEnumerator StampEffect()
	{
		mGetAccessoryEffectFinishedDict.Clear();
		List<int> keyList = new List<int>(mGetAccessoryDict.Keys);
		if (mGetAlterAccessoryDict != null && mGetAlterAccessoryDict.Count > 0)
		{
			List<int> tempList = new List<int>(mGetAlterAccessoryDict.Keys);
			for (int j = 0; j < tempList.Count; j++)
			{
				if (!keyList.Contains(tempList[j]))
				{
					keyList.Add(tempList[j]);
				}
			}
			mGetAlterAccessoryDict.Clear();
			mGetAlterAccessoryDict = null;
		}
		keyList.Sort();
		int show_i = 0;
		float timeSpan = 1f;
		while (true)
		{
			timeSpan += Time.deltaTime;
			if (timeSpan > 0.15f)
			{
				if (mRewardPanelGoDict.ContainsKey(keyList[show_i]))
				{
					StartCoroutine(StampEffectSingle(keyList[show_i]));
					show_i++;
					timeSpan = 0f;
				}
				else
				{
					mGetAccessoryEffectFinishedDict[keyList[show_i]] = true;
					show_i++;
				}
			}
			if (keyList.Count == mGetAccessoryEffectFinishedDict.Count)
			{
				break;
			}
			yield return null;
		}
		while (true)
		{
			bool isFinished = true;
			if (keyList.Count == mGetAccessoryEffectFinishedDict.Count)
			{
				List<int> keyFinishList = new List<int>(mGetAccessoryEffectFinishedDict.Keys);
				for (int i = 0; i < keyFinishList.Count; i++)
				{
					if (!mGetAccessoryEffectFinishedDict[keyFinishList[i]])
					{
						isFinished = false;
						break;
					}
				}
			}
			if (isFinished)
			{
				break;
			}
			yield return null;
		}
		CreateRewardDialog();
	}

	private IEnumerator StampEffectSingle(int bingo_i)
	{
		mGetAccessoryEffectFinishedDict[bingo_i] = false;
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		BIJImage atlasMap = ResourceManager.LoadImage(eventPageData.EventSetting.MapAtlasKey).Image;
		GameObject baseGo = mRewardPanelGoDict[bingo_i];
		UISprite sprite2 = Util.CreateSprite("GotStamp1", baseGo, atlasMap);
		Util.SetSpriteInfo(sprite2, "clearstamp", 35, Vector3.zero, new Vector3(1f, 1f, 1f), false);
		bool skip2 = false;
		float angle2 = 0f;
		while (angle2 < 90f)
		{
			angle2 += Time.deltaTime * 90f;
			if (angle2 > 90f)
			{
				angle2 = 90f;
			}
			float scale2 = 1f + (1f - Mathf.Sin(angle2 * ((float)Math.PI / 180f))) * 2f;
			float alpha2 = Mathf.Sin(angle2 * ((float)Math.PI / 180f));
			sprite2.transform.localScale = new Vector3(scale2, scale2, 1f);
			sprite2.transform.localRotation = Quaternion.Euler(0f, 0f, 90f - angle2);
			sprite2.color = new Color(1f, 1f, 1f, alpha2);
			if (mGame.mTouchDownTrg)
			{
				sprite2.transform.localScale = Vector3.one;
				sprite2.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
				sprite2.color = Color.white;
				skip2 = true;
				break;
			}
			yield return 0;
		}
		mGame.PlaySe("SE_RANKUP1", 2);
		sprite2 = Util.CreateSprite("GotStamp2", baseGo, atlasMap);
		Util.SetSpriteInfo(sprite2, "clearstamp", 34, Vector3.zero, new Vector3(1f, 1f, 1f), false);
		angle2 = 0f;
		while (angle2 < 90f)
		{
			angle2 += Time.deltaTime * 180f;
			if (angle2 > 90f)
			{
				angle2 = 90f;
			}
			float scale = 1f + Mathf.Sin(angle2 * ((float)Math.PI / 180f));
			float alpha = 1f - Mathf.Sin(angle2 * ((float)Math.PI / 180f));
			sprite2.transform.localScale = new Vector3(scale, scale, 1f);
			sprite2.color = new Color(1f, 1f, 1f, alpha);
			if (mGame.mTouchDownTrg || skip2)
			{
				sprite2.transform.localScale = Vector3.one;
				sprite2.color = Color.clear;
				skip2 = true;
				break;
			}
			yield return 0;
		}
		UnityEngine.Object.Destroy(sprite2.gameObject);
		mGetAccessoryEffectFinishedDict[bingo_i] = true;
		AddStampReward(bingo_i);
	}

	private void CreateRewardDialog()
	{
		List<AccessoryData> list = new List<AccessoryData>();
		List<AccessoryData> list2 = new List<AccessoryData>();
		foreach (AccessoryData value in mGetAccessoryDict.Values)
		{
			mGame.mPlayer.AddAccessory_AllInGift(value, CurrentEventID);
			if (value.AccessoryType != AccessoryData.ACCESSORY_TYPE.PARTNER)
			{
				mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].GotBingoReward(value.Index);
			}
			bool flag = false;
			for (int i = 0; i < mIsGotAccessoryList.Count; i++)
			{
				if (value.Index == mIsGotAccessoryList[i].Index)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				continue;
			}
			if (value.AccessoryType != AccessoryData.ACCESSORY_TYPE.BINGO_KEY)
			{
				if (value.AccessoryType != AccessoryData.ACCESSORY_TYPE.PARTNER)
				{
					list.Add(value);
				}
				else
				{
					mGetPartner = value;
				}
			}
			else
			{
				list2.Add(value);
			}
		}
		mGame.Save();
		UpdateNextRewardImage();
		int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
		int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
		int currentEventID = CurrentEventID;
		int currentCourseID = CurrentCourseID;
		int evt_bng = mTotalBingoNum;
		int totalStar = mSlideBingoPanel.GetTotalStar();
		for (int j = 0; j < mBingoList_Cram.Count; j++)
		{
			int bng_no = mBingoList_Cram[j];
			ServerCram.EventBingo(lastPlayLevel, lastPlaySeries, currentEventID, currentCourseID, bng_no, evt_bng, totalStar);
		}
		mBingoList_Cram.Clear();
		if (list.Count > 0)
		{
			StartCoroutine(GrayOut());
			List<AccessoryData> list3 = new List<AccessoryData>();
			for (int k = 0; k < list.Count; k++)
			{
				list3.Add(list[k]);
				if (list3.Count >= 4)
				{
					break;
				}
			}
			GetRewardDialog getRewardDialog = Util.CreateGameObject("GetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
			getRewardDialog.Init(list3);
			getRewardDialog.SetClosedCallback(OnGetRewardDialogClosed);
		}
		else if (mGetPartner != null)
		{
			StartCoroutine(JumpUpLive2dChara());
		}
		else if (mIsGotAccessoryList.Count > 0 && mIsGotAccessoryList.Count <= 4)
		{
			List<AccessoryData> list4 = new List<AccessoryData>();
			for (int l = 0; l < mIsGotAccessoryList.Count; l++)
			{
				list4.Add(mIsGotAccessoryList[l]);
			}
			StartCoroutine(GrayOut());
			GetRewardDialog getRewardDialog2 = Util.CreateGameObject("NoGetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
			getRewardDialog2.Init(list4, true);
			getRewardDialog2.SetClosedCallback(OnNoGetRewardDialogClosed);
		}
		else
		{
			mState.Change(STATE.GETREWARD_INIT);
		}
	}

	private void OnGetRewardDialogClosed()
	{
		StartCoroutine(GrayIn());
		if (mGetPartner != null)
		{
			StartCoroutine(JumpUpLive2dChara());
		}
		else
		{
			CommonRewardDialogCheck();
		}
	}

	private void OnNoGetRewardDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.GETREWARD_INIT);
	}

	private void OnGetPartnerDialogClosed(GetPartnerDialog.SELECT_ITEM a_selectItem)
	{
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mGetPartnerDialog.gameObject);
		mGetPartnerDialog = null;
		mGetPartner = null;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		int num = -1;
		int getnum = 0;
		List<int> list = new List<int>(sMEventPageData.EventRewardList.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			BingoRewardListData bingoRewardListData = new BingoRewardListData();
			SMEventRewardSetting sMEventRewardSetting = sMEventPageData.EventRewardList[list[i]];
			AccessoryData accessoryData = mGame.GetAccessoryData(sMEventRewardSetting.RewardID);
			if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER && !mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index))
			{
				num = accessoryData.Index;
				getnum = i + 1;
				break;
			}
		}
		if (num != -1)
		{
			StartCoroutine(GrayOut());
			mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
			mEventIntroCharacterDialog.Init(num, Def.EVENT_TYPE.SM_BINGO, getnum, true);
			mEventIntroCharacterDialog.SetClosedCallback(PartnerCommonDialogClosed);
		}
		else if (mSlideBingoPanel.GetTotalStar() < 135)
		{
			StartCoroutine(GrayOut());
			BingoGuideDialog bingoGuideDialog = Util.CreateGameObject("BingoGuideDialog", mRoot).AddComponent<BingoGuideDialog>();
			bingoGuideDialog.Init(5);
			bingoGuideDialog.SetClosedCallback(OnBingoGuideDialogClosed);
		}
		else
		{
			StartCoroutine(JumpDownLive2dChara());
		}
	}

	private void PartnerCommonDialogClosed()
	{
		StartCoroutine(GrayIn());
		StartCoroutine(JumpDownLive2dChara());
	}

	private void CommonRewardDialogCheck()
	{
		if (mIsGotAccessoryList.Count <= 0 || mIsGotAccessoryList.Count >= 5)
		{
			mState.Change(STATE.GETREWARD_INIT);
			return;
		}
		List<AccessoryData> list = new List<AccessoryData>();
		for (int i = 0; i < mIsGotAccessoryList.Count; i++)
		{
			list.Add(mIsGotAccessoryList[i]);
		}
		StartCoroutine(GrayOut());
		GetRewardDialog getRewardDialog = Util.CreateGameObject("NoGetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
		getRewardDialog.Init(list, true);
		getRewardDialog.SetClosedCallback(OnNoGetRewardDialogClosed);
	}

	private bool RewardEffectStartCheck()
	{
		if (mTotalBingoNum == 0)
		{
			return false;
		}
		if (mPrevBingoNum == mTotalBingoNum)
		{
			return false;
		}
		mPrevBingoNum = mTotalBingoNum;
		mGetAccessoryDict.Clear();
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		for (int i = 0; i < mTotalBingoNum; i++)
		{
			SMEventRewardSetting sMEventRewardSetting = sMEventPageData.EventRewardList[i + 1];
			if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].GotAccessoryList.Contains(sMEventRewardSetting.RewardID))
			{
				continue;
			}
			AccessoryData accessoryData = mGame.GetAccessoryData(sMEventRewardSetting.RewardID);
			if (accessoryData == null)
			{
				continue;
			}
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
			{
				bool flag = false;
				if (!mGame.mPlayer.IsAccessoryUnlock(sMEventRewardSetting.RewardID))
				{
					if (mGame.mCompanionData[accessoryData.GetGotIDByNum()].IsExpandBase())
					{
					}
					if (mGame.mPlayer.IsCompanionUnlock(accessoryData.GetCompanionID()))
					{
						flag = true;
						mGame.mPlayer.UnlockAccessory(accessoryData.Index);
					}
				}
				else
				{
					flag = true;
				}
				if (flag)
				{
					bool flag2 = true;
					List<AccessoryData> alternateReward = mGame.GetAlternateReward(sMEventRewardSetting.RewardID);
					foreach (AccessoryData item in alternateReward)
					{
						if (!mGame.mPlayer.IsAccessoryUnlock(item.Index))
						{
							flag2 = false;
							break;
						}
					}
					if (flag2)
					{
						flag = false;
						continue;
					}
				}
				if (flag)
				{
					SetAlternateReward(sMEventRewardSetting.RewardID);
					if (mGetAlterAccessoryDict == null)
					{
						mGetAlterAccessoryDict = new Dictionary<int, AccessoryData>();
					}
					mGetAlterAccessoryDict[i] = accessoryData;
				}
				else
				{
					mGetAccessoryDict[i] = accessoryData;
				}
			}
			else if (!mGame.mPlayer.IsAccessoryUnlock(sMEventRewardSetting.RewardID))
			{
				mGetAccessoryDict[i] = accessoryData;
			}
		}
		if (mGetAccessoryDict.Count <= 0)
		{
			return false;
		}
		mState.Reset(STATE.REWARD_INIT, true);
		return true;
	}

	private bool AlternateRewardCheck()
	{
		if (mAlterUnlockAccessory.Count == 0)
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mAlternateRewardDialog = Util.CreateGameObject("AlterRewardDialog", mRoot).AddComponent<AlternateRewardDialog>();
		mAlternateRewardDialog.Init(mAlterUnlockAccessory);
		mAlternateRewardDialog.SetClosedCallback(delegate
		{
			UnityEngine.Object.Destroy(mAlternateRewardDialog.gameObject);
			mAlternateRewardDialog = null;
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			int num = -1;
			int getnum = 0;
			List<int> list = new List<int>(sMEventPageData.EventRewardList.Keys);
			for (int j = mTotalBingoNum; j < list.Count; j++)
			{
				BingoRewardListData bingoRewardListData = new BingoRewardListData();
				SMEventRewardSetting sMEventRewardSetting = sMEventPageData.EventRewardList[list[j]];
				AccessoryData accessoryData = mGame.GetAccessoryData(sMEventRewardSetting.RewardID);
				if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
				{
					num = accessoryData.Index;
					getnum = j + 1;
					break;
				}
			}
			if (num != -1)
			{
				mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
				mEventIntroCharacterDialog.Init(num, Def.EVENT_TYPE.SM_BINGO, getnum, true);
				mEventIntroCharacterDialog.SetClosedCallback(delegate
				{
					StartCoroutine(GrayIn());
					mState.Change(STATE.INIT);
				});
			}
			else
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.INIT);
			}
		});
		mAlterUnlockAccessory.Clear();
		mState.Reset(STATE.WAIT, true);
		return true;
	}

	private bool AccessoryConnectCheck()
	{
		int count = mGame.mPlayer.Data.AccessoryConnect.Count;
		if (count > 0 && AccessoryConnectIndex == 0)
		{
			for (int i = 0; i < mGame.mPlayer.Data.AccessoryConnect.Count; i++)
			{
				int num = mGame.mPlayer.Data.AccessoryConnect[i];
				if (!mGame.AlreadyConnectAccessoryList.Contains(num))
				{
					AccessoryConnectIndex = num;
					return true;
				}
			}
		}
		return false;
	}

	private bool AllBingoEffectStartCheck()
	{
		if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mAllBingoEffectList[CurrentCourseID])
		{
			return false;
		}
		for (short num = 1; num < 10; num++)
		{
			PlayerClearData _psd = null;
			SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, CurrentCourseID, Def.GetStageNo(num, 0), out _psd);
			if (_psd == null)
			{
				return false;
			}
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.ALLBINGO_EFFECT);
		return true;
	}

	private void OnTryNextSheetDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.INIT);
	}

	private bool StarCompEffectStartCheck()
	{
		if (mSlideBingoPanel.GetSheetTotalStar(CurrentCourseID) < 27)
		{
			return false;
		}
		if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mAllStarCompEffectList[CurrentCourseID])
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.STARCOMP_EFFECT);
		return true;
	}

	private bool GuideDialogStartCheck()
	{
		int num = -1;
		for (int i = 0; i < mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mGuideDialogList.Count; i++)
		{
			if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mGuideDialogList[i])
			{
				continue;
			}
			switch (i)
			{
			case 0:
				mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mGuideDialogList[i] = true;
				num = i;
				break;
			case 1:
			{
				for (short num2 = 1; num2 < 10; num2++)
				{
					PlayerClearData _psd = null;
					SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, 0, Def.GetStageNo(num2, 0), out _psd);
					if (_psd != null)
					{
						mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mGuideDialogList[i] = true;
						num = i;
						break;
					}
				}
				break;
			}
			case 2:
				if (mTotalBingoNum > 0)
				{
					mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mGuideDialogList[i] = true;
					num = i;
				}
				break;
			case 3:
			{
				if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData.Count <= 0)
				{
					break;
				}
				int a_main = mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mLotteryData[0];
				PlayerClearData _psd2 = null;
				SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, CurrentEventID, 0, Def.GetStageNo(a_main, 0), out _psd2);
				if (_psd2 == null)
				{
					PlayerStageData _psd3;
					mGame.mPlayer.GetStageChallengeData(Def.SERIES.SM_EV, CurrentEventID, 0, Def.GetStageNo(a_main, 0), out _psd3);
					if (_psd3 != null)
					{
						mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mGuideDialogList[i] = true;
						num = i;
					}
				}
				break;
			}
			case 4:
			{
				SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
				int index = sMEventPageData.BingoMapSetting.CourseOpenKeyIdList[1];
				if (mGame.mPlayer.IsAccessoryUnlock(index))
				{
					mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mGuideDialogList[i] = true;
					num = i;
				}
				break;
			}
			}
			if (num != -1)
			{
				break;
			}
		}
		if (num == -1)
		{
			return false;
		}
		mGame.Save();
		switch (num)
		{
		case 0:
			mFirstGuideDialogStart = true;
			break;
		case 4:
			mNewCourseGuideDialogStart = true;
			break;
		default:
		{
			StartCoroutine(GrayOut());
			BingoGuideDialog bingoGuideDialog = Util.CreateGameObject("BingoGuideDialog", mRoot).AddComponent<BingoGuideDialog>();
			bingoGuideDialog.Init(num);
			bingoGuideDialog.SetClosedCallback(OnBingoGuideDialogClosed);
			mState.Change(STATE.WAIT);
			break;
		}
		}
		return true;
	}

	private void OnBingoGuideDialogClosed(int i)
	{
		StartCoroutine(GrayIn());
		switch (i)
		{
		case 0:
		case 4:
			mState.Change(STATE.MAIN);
			break;
		case 5:
			StartCoroutine(JumpDownLive2dChara());
			break;
		default:
			mState.Change(STATE.INIT);
			break;
		}
	}

	private void IsAllBingoSheet()
	{
		if (mGame.mPlayer.Data.mBingoEventDataDict[CurrentEventID].mAllBingoEffectList[CurrentCourseID])
		{
			if (mSheeCompSsAnime == null)
			{
				mSheeCompSsAnime = Util.CreateUISsSprite("SheetCompEffect", mSlideBingoPanel.gameObject, "EFFECT_BINGO_SHEETCOMP", new Vector3(0f, -30f, 0f), new Vector3(1f, 1f, 1f), 5);
				mSheeCompSsAnime.Play();
			}
		}
		else if (mSheeCompSsAnime != null)
		{
			UnityEngine.Object.Destroy(mSheeCompSsAnime.gameObject);
			mSheeCompSsAnime = null;
		}
	}

	private GameObject CreateDemoCamera()
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.DEMO_CAMERA));
		gameObject.transform.parent = mGame.mCamera.transform.parent;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		UIPanel uIPanel = gameObject.AddComponent<UIPanel>();
		uIPanel.depth = 1000;
		uIPanel.sortingOrder = 1000;
		return gameObject;
	}

	private void OnLotterySkipButtonPushed(GameObject a_go)
	{
		if (mLotteryAnime != null && mLotteryAnime._sprite.AnimFrame < 94f)
		{
			mLotteryAnime._sprite.AnimFrame = 94f;
			mGame.StopSe(1);
		}
	}

	private void AddStampReward(int a_index)
	{
		if (!mGotRewardStampList.Contains(a_index))
		{
			mGotRewardStampList.Add(a_index);
		}
	}

	private string GetNextRewardSpriteName()
	{
		string result = "event_panel02";
		int count = mGotRewardStampList.Count;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		int count2 = sMEventPageData.EventRewardList.Count;
		if (count >= count2)
		{
			result = "event_panel03";
		}
		return result;
	}

	private void UpdateNextRewardImage()
	{
		if (mNextRewardSprite != null)
		{
			Util.SetSpriteImageName(mNextRewardSprite, GetNextRewardSpriteName(), Vector3.one, false);
		}
	}

	private void StartConnectFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_MYFRIENDLIST;
		}
	}

	private void StartConnectInLoadGameState()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1044);
		list.Add(1001);
		list.Add(1031);
		list.Add(1005);
		list.Add(1045);
		if (mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			list.Add(2012);
			list.Add(1017);
			list.Add(1015);
			list.Add(1016);
			list.Add(3);
		}
		else
		{
			list.Add(1016);
		}
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, false, false, StartConnectFinishCallback);
	}

	private void StartConnectToGetRewardMailFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_RESUME_CAMPAIGN;
		}
	}

	private void StartConnectToGetRewardMail()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1044);
		list.Add(1001);
		list.Add(2012);
		list.Add(1017);
		list.Add(3);
		mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
		mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, true, false, StartConnectToGetRewardMailFinishCallback);
	}

	private void StartConnectAfterResumeFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_RESUME_CAMPAIGN;
		}
	}

	private void StartConnectAfterResume()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1044);
		list.Add(1001);
		list.Add(1031);
		list.Add(1005);
		list.Add(1045);
		list.Add(2012);
		list.Add(1017);
		list.Add(3);
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, true, false, StartConnectAfterResumeFinishCallback);
	}

	protected void OnConnectErrorAccessoryAbandon()
	{
		GameStateSMMapBase.mIsShowErrorDialog = false;
		AccessoryData accessoryData = mGame.GetAccessoryData(AccessoryConnectIndex);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			mGame.mPlayer.AddAccessoryConnect(AccessoryConnectIndex);
		}
		mGame.AlreadyConnectAccessoryList.Add(AccessoryConnectIndex);
		AccessoryConnectIndex = 0;
		mState.Reset(STATE.INIT, true);
		StartCoroutine(GrayIn());
	}
}
