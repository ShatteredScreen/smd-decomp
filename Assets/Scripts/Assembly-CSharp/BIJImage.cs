using UnityEngine;

public abstract class BIJImage : MonoBehaviour
{
	public abstract Texture Texture { get; }

	public abstract int Count { get; }

	public abstract bool Contains(int index, string name);

	public abstract string GetName(int index, string name);

	public abstract Vector2 GetOffset(int index, string name);

	public abstract Vector2 GetSize(int index, string name);

	public abstract Vector2 GetPixelOffset(int index, string name);

	public abstract Vector2 GetPixelSize(int index, string name);

	public abstract Vector4 GetPixelPadding(int index, string name);

	public abstract Vector4 GetPadding(int index, string name);

	public abstract Vector4 GetPixelBorder(int index, string name);

	public abstract Vector4 GetBorder(int index, string name);

	public bool Contains(int index)
	{
		return Contains(index, null);
	}

	public bool Contains(string name)
	{
		return Contains(0, name);
	}

	public string GetName(int index)
	{
		return GetName(index, null);
	}

	public Vector2 GetOffset(int index)
	{
		return GetOffset(index, null);
	}

	public Vector2 GetOffset(string name)
	{
		return GetOffset(0, name);
	}

	public Vector2 GetSize(int index)
	{
		return GetSize(index, null);
	}

	public Vector2 GetSize(string name)
	{
		return GetSize(0, name);
	}

	public Vector2 GetPixelOffset(int index)
	{
		return GetPixelOffset(index, null);
	}

	public Vector2 GetPixelOffset(string name)
	{
		return GetPixelOffset(0, name);
	}

	public Vector2 GetPixelSize(int index)
	{
		return GetPixelSize(index, null);
	}

	public Vector2 GetPixelSize(string name)
	{
		return GetPixelSize(0, name);
	}

	public Vector4 GetPixelPadding(int index)
	{
		return GetPixelPadding(index, null);
	}

	public Vector4 GetPixelPadding(string name)
	{
		return GetPixelPadding(0, name);
	}

	public Vector4 GetPadding(int index)
	{
		return GetPadding(index, null);
	}

	public Vector4 GetPadding(string name)
	{
		return GetPadding(0, name);
	}

	public Vector4 GetPixelBorder(int index)
	{
		return GetPixelBorder(index, null);
	}

	public Vector4 GetPixelBorder(string name)
	{
		return GetPixelBorder(0, name);
	}

	public Vector4 GetBorder(int index)
	{
		return GetBorder(index, null);
	}

	public Vector4 GetBorder(string name)
	{
		return GetBorder(0, name);
	}
}
