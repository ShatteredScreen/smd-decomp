using UnityEngine;
using UnityEngine.Rendering;

public class SimpleTextureAtlas : SimpleTexture
{
	protected Shader mShader;

	protected override void Start()
	{
		if (mMeshFilter == null)
		{
			mMeshFilter = base.gameObject.AddComponent<MeshFilter>();
		}
		if (mMeshRenderer == null)
		{
			mMeshRenderer = base.gameObject.AddComponent<MeshRenderer>();
		}
	}

	protected override void SetTexture()
	{
		if (mMesh != null)
		{
			Object.Destroy(mMesh);
			mMesh = null;
		}
		if (mMaterial != null)
		{
			Object.Destroy(mMaterial);
			mMaterial = null;
		}
		if (mMeshFilter == null)
		{
			mMeshFilter = base.gameObject.AddComponent<MeshFilter>();
		}
		if (mMeshRenderer == null)
		{
			mMeshRenderer = base.gameObject.AddComponent<MeshRenderer>();
		}
		mMesh = new Mesh();
		mMaterial = new Material(mShader);
		mMeshFilter.sharedMesh = mMesh;
		mMeshRenderer.sharedMaterial = mMaterial;
		mMeshRenderer.sharedMaterial.mainTexture = mTexture;
		mMeshRenderer.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRenderer.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		mMeshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		mMeshRenderer.shadowCastingMode = ShadowCastingMode.Off;
		UpdateVertices();
	}

	public void Init(Texture tex, Vector2 size, Rect uv, PIVOT pivot)
	{
		mTexture = tex;
		mSize = size;
		mUv = uv;
		mPivot = pivot;
	}

	public void SetShader(Shader a_shader)
	{
		mShader = a_shader;
		SetTexture();
	}

	public void SetAlpha(float a_alpha)
	{
		if (!(mMesh == null))
		{
			if (mMesh.uv2 == null)
			{
				mMesh.uv2 = new Vector2[4];
			}
			for (int i = 0; i < mMesh.uv2.Length; i++)
			{
				mMesh.uv2[i] = new Vector2(0f, a_alpha);
			}
		}
	}
}
