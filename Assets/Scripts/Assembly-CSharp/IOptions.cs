public interface IOptions
{
	string Fbid { get; }

	string Gcid { get; }

	string Gpid { get; }

	string Gender { get; }
}
