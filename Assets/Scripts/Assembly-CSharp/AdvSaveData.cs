using System;
using System.Collections.Generic;

public class AdvSaveData
{
	public short mAdvSaveVersion = 1190;

	public List<long> AdvGotRewardList = new List<long>();

	public List<short> LastEventIdList = new List<short>();

	public short mAdvLoaclMTVersion;

	public List<EventEffectDispInfo> mEventEffectDispInfoList = new List<EventEffectDispInfo>();

	public int mGachaTicket_Normal;

	public int mGachaTicket_Premium;

	public int mGachaTicket_PremiumAuxiliary;

	public int mChargeExp;

	public int mSkillLvUpItemNum;

	public Dictionary<int, AdvCharacter> CharacterDictionary = new Dictionary<int, AdvCharacter>();

	public int LotteryRandomArrIdx { get; set; }

	public int[] LotteryRandomArr { get; set; }

	public int mRestStamina { get; set; }

	public DateTime mOldestAdvStaminaUseTime { get; set; }

	public int mLastStamina { get; set; }

	public int LastEventInfoID { get; set; }

	public DateTime LastEventInfoDisplayTime { get; set; }

	public DateTime LastBootTime { get; set; }

	public DateTime LastPlayTime { get; set; }

	public long BootCount { get; set; }

	public long IntervalCount { get; set; }

	public long ContinuousCount { get; set; }

	public long PlayCount { get; set; }

	public long TotalWin { get; set; }

	public long TotalLose { get; set; }

	public double TotalPlayTime { get; set; }

	public Def.SERIES LastPlaySeries { get; set; }

	public int LastPlayLevel { get; set; }

	public DateTime LastPlayLevelTime { get; set; }

	public int LastPlayLevelCount { get; set; }

	public DateTime LastPlayLevelStartTime { get; set; }

	public AdvSaveData()
	{
		PlayCount = 0L;
		TotalWin = 0L;
		TotalLose = 0L;
		LastPlaySeries = Def.SERIES.SM_ADV;
		LastPlayLevel = 0;
		LastPlayLevelTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastPlayLevelCount = 0;
		LastPlayLevelStartTime = DateTimeUtil.UnitLocalTimeEpoch;
	}

	public static AdvSaveData InitAdvData()
	{
		return new AdvSaveData();
	}

	public static AdvSaveData DeserializeFromBinary(BIJBinaryReader aReader, Dictionary<int, AdvDungeonData> _aDungeonDataList, Dictionary<int, AdvStageData> _aDataList)
	{
		return new AdvSaveData();
	}

	public void UpdateMyCharaDict()
	{
	}
}
