public class ConnectAdvGetAllItem : ConnectBase
{
	protected override void InitServerFlg()
	{
		GameMain.mAdvGetAllItemCheckDone = false;
		GameMain.mAdvGetAllItemSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mAdvGetAllItemCheckDone = true;
		GameMain.mAdvGetAllItemSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mAdvGetAllItemCheckDone = true;
		GameMain.mAdvGetAllItemSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			ServerFlgSucceeded();
			base.StateStart();
		}
		else if (!Server.AdvGetAllItem())
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetAdvGetAllItemFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
