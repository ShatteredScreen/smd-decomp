using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using UnityEngine;

public class Server : MonoBehaviour
{
	private class ServerRequest
	{
		public API _api;

		public HttpAsync _httpAsync;

		public HttpAsync.ResponseHandler _responseHandler;

		public Action<int, int> _handler;

		public object _userdata;

		public DateTime _time;

		public int _serverResult;
	}

	public enum API
	{
		ConnectionEstablish = 1000,
		UserAuth = 1001,
		NicknameEdit = 1002,
		NicknameCheck = 1003,
		GetEntry = 1004,
		GetSegmentInfo = 1005,
		SendDeviceToken = 1006,
		SearchUserID = 1007,
		SearchUserRandom = 1008,
		RegisterFriend = 1009,
		GetFriends = 1010,
		RemoveFriend = 1011,
		GetSNSFriendData = 1012,
		GiveGift = 1013,
		GiveApply = 1014,
		GetGift = 1015,
		GetApply = 1016,
		GetSupport = 1017,
		GotGift = 1018,
		GotApply = 1019,
		GotSupport = 1020,
		GiveGiftAll = 1021,
		SaveGameData = 1022,
		GetGameData = 1023,
		GetSaveDataOfOthers = 1024,
		SetStageClearData = 1025,
		GetStageRankingData = 1026,
		GetStageMetaData = 1027,
		SetEventStageClearData = 1028,
		GetEventStageRankingData = 1029,
		GetEventStageMetaData = 1030,
		GetGameInfo = 1031,
		GetEventInfo = 1032,
		GetDebugTerminal = 1033,
		DLFilelistCheck = 1034,
		DLFilelistHashCheck = 1035,
		GetTransferParam = 1036,
		LogTransfer = 1037,
		ClearTransfer = 1038,
		GetMetadata = 1039,
		AccomplishResult = 1040,
		SetPurchase = 1041,
		SaveJournal = 1042,
		GetWallPaper = 1043,
		GetMaintenanceInfo = 1044,
		LoginBonus = 1045,
		LoginBonusDebugClear = 1046,
		LoginBonusDebugGetNextData = 1047,
		ChargePriceCheck = 1048,
		CheckSave = 1049,
		AdvGetAllItem = 2000,
		AdvQuestStart = 2001,
		AdvQuestEnd = 2002,
		AdvGetGashaInfo = 2003,
		AdvGashaPlay = 2004,
		AdvGrowUpFigure = 2005,
		AdvCheckMasterTableVer = 2006,
		AdvGetMasterTableData = 2007,
		AdvGetRewardInfo = 2008,
		AdvSetReward = 2009,
		AdvSendSaveData = 2010,
		AdvGetSaveData = 2011,
		AdvGetMail = 2012,
		AdvSendGotMail = 2013,
		AdvGetEventPoint = 2014,
		AdvSetEventPoint = 2015,
		AdvGetTransferParam = 2016,
		AdvSaveTransferGameState = 2017,
		AdvGetTransferGameState = 2018,
		AdvDebug_FigureAllDelete = 2019,
		AdvDebug_RewardAllDelete = 2020,
		AdvDebug_SetItemData = 2021,
		AdvDebug_GetAllItem = 2022,
		AdvDebug_GashaInfoAllDelete = 2023,
		AdvDebug_FigureAllGrowUp = 2024,
		AdvDebug_AddFigure = 2025,
		AdvDebug_RemoveFigure = 2026,
		AdvDebug_GetAllFigure = 2027,
		AdvDebug_SetFigureStatus = 2028,
		AdvCheckSave = 2029,
		MAX = 2030
	}

	public enum ErrorCode
	{
		OTHER = -1,
		SUCCESS = 0,
		NO_JSON_MIME = 1,
		NO_JSON_FORMAT = 2,
		INVALID_DATA_FORMAT = 3,
		INVALID_REQUEST_FORMAT = 4,
		NO_REGISTER_UUID = 5,
		NO_REGISTER_SEARCHID = 6,
		NICKNAME_IN_NGWORD = 7,
		SQL_ERROR = 8,
		NO_EXISTS_FILE = 9,
		DUPLICATE_DATA = 17,
		ACCESSORY_NOTFOUND = 18,
		AUTHKEY_REGISTERED = 90,
		AUTHKEY_DIFFERENT = 91,
		MAINTENANCE_MODE = 98,
		ADVERR_1000 = 1000,
		ADVERR_1001 = 1001,
		ADVERR_1002 = 1002,
		ADVERR_1003 = 1003,
		ADVERR_1004 = 1004,
		ADVERR_1005 = 1005,
		ADVERR_1006 = 1006,
		ADVERR_9997 = 9997,
		ADVERR_9998 = 9998,
		ADVERR_9999 = 9999,
		TIMEOUT = 10000,
		NOTFOUND = 11000,
		JSON_PARSE = 12000,
		JSON_STREAM = 13000,
		ABORT = 14000,
		NO_MORE_FRIEND = 15000
	}

	public class RegisterResponse
	{
		public int status { get; set; }
	}

	public class _GameSettingInfo
	{
		public GameProfile gameinfo { get; set; }

		public EventProfile eventinfo { get; set; }

		public string dir { get; set; }
	}

	public enum MailType
	{
		GIFT = 0,
		FRIEND = 1,
		SUPPORT = 2,
		REWARD = 3,
		ADV = 4,
		FROM_ADV = 5
	}

	public class _MaintenanceSettingInfo
	{
		public long time { get; set; }

		public MaintenanceProfile MaintenanceInfo { get; set; }

		public _MaintenanceSettingInfo()
		{
			time = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		}
	}

	public class _SegmentSettingInfo
	{
		public SegmentProfile segmentinfo { get; set; }

		public string dir { get; set; }
	}

	public enum TransferLogStateKind
	{
		START = 0,
		INPUT_PASSWORD = 1,
		GET_HIVEID = 10,
		GET_SAVEDATA = 20,
		GET_SAVEDATA_NODATA = 21,
		CONFORM_MIGRATION_OK = 30,
		CONFORM_MIGRATION_NO = 31,
		SEND_GOT_OK = 40,
		SEND_GOT_NG = 41,
		COMPLETE = 100
	}

	public const int SUCCESS = 0;

	public const int ERROR = 1;

	public const int TIMEOUT = 3;

	public const int CANCELED = 4;

	public const int NOTFOUND = 5;

	public const int ABORT = 6;

	private const int POST_MAX = 10;

	private static object mInstanceLock = new object();

	private static Server mInstance = null;

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private Queue<ServerRequest> mRequests = new Queue<ServerRequest>();

	private readonly object mRequestsLock = new object();

	private ManualResetEvent mRequestsNotify = new ManualResetEvent(false);

	private Thread mThread;

	private ManualResetEvent mThreadLock;

	private volatile bool mIsRunning;

	private Queue<ServerRequest> mResponses = new Queue<ServerRequest>();

	private readonly object mResponsesLock = new object();

	private static ThreadSafeDictionary<API, bool> mConnectFlags = new ThreadSafeDictionary<API, bool>();

	public static Server Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					mInstance = (Server)gameObject.GetComponent(typeof(Server));
				}
				return mInstance;
			}
		}
	}

	public static string BaseURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppBase_URL);
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppName);
			return stringBuilder.ToString();
		}
	}

	public static string GameInfoURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.GameInfo_URL);
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppName);
			return stringBuilder.ToString();
		}
	}

	public static string EventURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppBase_URL);
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppName);
			stringBuilder.Append("/event/");
			return stringBuilder.ToString();
		}
	}

	public static string SocialURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.Social_URL);
			return stringBuilder.ToString();
		}
	}

	public static string VipURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.VIP_URL);
			return stringBuilder.ToString();
		}
	}

	public static string CampaignURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.Campaign_URL);
			return stringBuilder.ToString();
		}
	}

	public static string RewardURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.Reward_URL);
			return stringBuilder.ToString();
		}
	}

	public static string TransferBaseURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.TransferBase_URL);
			stringBuilder.Append("app/");
			return stringBuilder.ToString();
		}
	}

	public static string DLFilelistBaseURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.DLFILELIST_URL);
			return stringBuilder.ToString();
		}
	}

	public static int Timeout { get; set; }

	public static bool ManualConnecting { get; set; }

	private static bool IsSendable
	{
		get
		{
			if (InternetReachability.Status == NetworkReachability.NotReachable)
			{
				return false;
			}
			return true;
		}
	}

	public static bool IsNetworking
	{
		get
		{
			return ManualConnecting || IsConnecting(API.GetTransferParam);
		}
	}

	public static string AdvBaseURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AdvAppBase_URL);
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppName);
			return stringBuilder.ToString();
		}
	}

	[method: MethodImpl(32)]
	public static event Action<int, Stream> DLFilelistCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AccomplishResultEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvGetAllItemEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvQuestStartEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvQuestEndEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvGetGashaInfoEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvGashaPlayEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvGrowUpFigureEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvCheckMasterTableVerEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvGetMasterTableDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvGetRewardInfoEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvSetRewardEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvSendSaveDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> AdvGetSaveDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvCheckSaveEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> AdvGetMailEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> AdvSendGotMailEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvGetEventPointEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvSetEventPointEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvSaveTransferGameStateEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> AdvGetTransferGameStateEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> AdvGetTransferParamEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_FigureAllDeleteEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_RewardAllDeleteEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_SetItemDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_GetAllItemEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_GashaInfoAllDeleteEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_FigureAllGrowUpEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_AddFigureEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_RemoveFigureEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_GetAllFigureEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> AdvDebug_SetFigureStatusEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> ConnectionEstablishEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> UserAuthEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> DebugTerminalRegistration;

	[method: MethodImpl(32)]
	public static event Action<int, int> SendDeviceTokenEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> RegisterFriendEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, string> GetFriendsEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> RemoveFriendEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, string> GetSNSFriendDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, string> GetRandomUserEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, string> GetIDUserEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> SaveGameStateEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream, int> GetServerGameDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> GetMetadataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> CheckSaveEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> GetGameInfoEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> GiveGiftEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> GiveGiftAllEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, MailType, string> GetGiftEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, MailType> GotGiftEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> GiveApplyEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, MailType, Stream> GetApplyEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, MailType> GotApplyEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, MailType, Stream> GetSupportEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, MailType> GotSupportEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> LoginBonusEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> LoginBonusDebugClearEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> LoginBonusDebugNextDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> GetMaintenanceInfoEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> NicknameCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> NicknameEditEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> GetSegmentInfoEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> SetPurchaseEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> SaveJournalEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> ChargePriceCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream, int> GetServerSaveDataOfOthersEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream, int> GetServerSaveDataOfOthersEventCX;

	[method: MethodImpl(32)]
	public static event Action<int, int> SetStageClearDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, string> GetStageRankingDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> GetStageMetaDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> SetEventStageClearDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, string> GetEventStageRankingDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> GetEventStageMetaDataEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int> SaveTransferGameStateEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream, int> GetTransferGameStateEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> GetTransferParamEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, Stream> TransferAuthEvent;

	[method: MethodImpl(32)]
	public static event Action<int, int, string> GetWallPaperEvent;

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
		try
		{
			long num = time();
		}
		catch (Exception)
		{
		}
	}

	private static void dbg(string msg)
	{
	}

	public static string BaseEventURL(string a_prefix)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppEventBase_URL);
		stringBuilder.Append(a_prefix + "/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppName);
		return stringBuilder.ToString();
	}

	public static string ToSS(object o)
	{
		if (o == null)
		{
			return string.Empty;
		}
		return o.ToString();
	}

	public static string ResultString(int result)
	{
		switch (result)
		{
		case 0:
			return "SUCCESS";
		case 1:
			return "ERROR";
		case 4:
			return "CANCELED";
		case 3:
			return "TIMEOUT";
		default:
			return "UNKNOWN!!";
		}
	}

	private void Awake()
	{
		mThread = new Thread(_ThreadMain);
	}

	private void Start()
	{
		mIsRunning = true;
		mThread.Start();
	}

	private void Update()
	{
		ServerRequest serverRequest = null;
		try
		{
			lock (mResponsesLock)
			{
				if (mResponses.Count > 0)
				{
					serverRequest = mResponses.Dequeue();
				}
			}
			if (serverRequest == null)
			{
				return;
			}
			log(string.Format("Response(#{0}) - {1} {2} {3} {4}", (serverRequest._httpAsync == null) ? (-1) : serverRequest._httpAsync.SeqNo, serverRequest._serverResult, ResultString(serverRequest._serverResult), serverRequest._api, (serverRequest._httpAsync == null) ? "HTTPASYNC NULL" : serverRequest._httpAsync.Result.ToString()));
			if (serverRequest._serverResult == 0)
			{
				if (serverRequest._responseHandler != null)
				{
					serverRequest._responseHandler(serverRequest._httpAsync);
				}
			}
			else if (serverRequest._serverResult == 3)
			{
				if (serverRequest._handler != null)
				{
					serverRequest._handler(serverRequest._serverResult, 10000);
				}
			}
			else if (serverRequest._serverResult == 5)
			{
				if (serverRequest._handler != null)
				{
					serverRequest._handler(serverRequest._serverResult, 11000);
				}
			}
			else if (serverRequest._serverResult == 6)
			{
				if (serverRequest._handler != null)
				{
					serverRequest._handler(serverRequest._serverResult, 14000);
				}
			}
			else if (serverRequest._handler != null && serverRequest._httpAsync != null)
			{
				log(string.Concat(serverRequest._api, " Error Result: ", serverRequest._serverResult));
				int arg = -1;
				HttpWebResponse httpWebResponse = serverRequest._httpAsync.Response as HttpWebResponse;
				try
				{
					using (Stream stream = httpWebResponse.GetResponseStream())
					{
						string text = string.Empty;
						if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
						{
							byte[] responseByte = Instance.GetResponseByte(stream);
							text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
						}
						else
						{
							using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
							{
								text = streamReader.ReadToEnd();
							}
						}
						log(string.Concat(serverRequest._api, " ErrorCode: ", text));
						try
						{
							SMErrorResponse obj = new SMErrorResponse();
							new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, text);
							arg = obj.code;
						}
						catch (Exception ex)
						{
							arg = 12000;
							BIJLog.E(string.Concat(serverRequest._api, " : Error Load From Json:", ex.Message, "\njson:", text));
						}
					}
				}
				catch (Exception e)
				{
					arg = 13000;
					BIJLog.E(string.Concat(serverRequest._api, " : Server response error."), e);
				}
				finally
				{
					serverRequest._handler(serverRequest._serverResult, arg);
					if (httpWebResponse != null)
					{
						httpWebResponse.Close();
					}
				}
			}
			if (serverRequest._httpAsync == null)
			{
				return;
			}
			try
			{
				serverRequest._httpAsync.Dispose();
			}
			catch (Exception)
			{
			}
		}
		catch (Exception)
		{
		}
	}

	private void OnApplicationQuit()
	{
		int num = 0;
		do
		{
			mIsRunning = false;
			mRequestsNotify.Set();
			mThread.Join(100);
		}
		while (mThread.IsAlive && num++ < 3);
		if (mThread.IsAlive)
		{
			mThread.Abort();
		}
		lock (mRequestsLock)
		{
			foreach (ServerRequest mRequest in mRequests)
			{
				mRequest._httpAsync.Abort();
			}
			mRequests.Clear();
		}
	}

	private int GetRequestsWaitTimeMillis()
	{
		return -1;
	}

	private int GetResponsesWaitTimeMillis()
	{
		return 10;
	}

	private void _ThreadMain()
	{
		while (mIsRunning)
		{
			try
			{
				mRequestsNotify.WaitOne(GetRequestsWaitTimeMillis());
				mRequestsNotify.Reset();
				ServerRequest serverRequest = null;
				do
				{
					serverRequest = null;
					lock (mRequestsLock)
					{
						if (mRequests.Count > 0)
						{
							serverRequest = mRequests.Dequeue();
						}
					}
					if (serverRequest == null)
					{
						continue;
					}
					dbg(string.Format("Request API:{0} ", serverRequest._api));
					try
					{
						mThreadLock = new ManualResetEvent(false);
						serverRequest._time = DateTime.Now;
						serverRequest._httpAsync.GetResponse(delegate(HttpAsync httpAsync)
						{
							ServerRequest serverRequest2 = httpAsync.UserData as ServerRequest;
							if (serverRequest2 != null)
							{
								dbg(string.Format("(#{0}): Server HttpAsync Result={1}    {2} ({3}sec)", serverRequest2._httpAsync.SeqNo, serverRequest2._httpAsync.Result, serverRequest2._httpAsync.Request.RequestUri, ((DateTime.Now - serverRequest2._time).TotalMilliseconds / 1000.0).ToString("0.000")));
							}
							mThreadLock.Set();
						}, serverRequest);
						bool flag = false;
						while (!mThreadLock.WaitOne(GetResponsesWaitTimeMillis()))
						{
							if (!mIsRunning)
							{
								flag = true;
								break;
							}
						}
						if (flag)
						{
							serverRequest._serverResult = 4;
						}
						else
						{
							switch (serverRequest._httpAsync.Result)
							{
							case HttpAsync.RESULT.SUCCESS:
							{
								HttpWebResponse httpWebResponse = serverRequest._httpAsync.Response as HttpWebResponse;
								if (httpWebResponse != null)
								{
									log(string.Format("(#{0}): HTTP Result={1}   (len={2})", (serverRequest._httpAsync == null) ? (-1) : serverRequest._httpAsync.SeqNo, httpWebResponse.StatusCode, httpWebResponse.ContentLength));
									if (httpWebResponse.StatusCode == HttpStatusCode.OK)
									{
										serverRequest._serverResult = 0;
									}
									else if (httpWebResponse.StatusCode == HttpStatusCode.NotFound)
									{
										serverRequest._serverResult = 5;
									}
									else
									{
										serverRequest._serverResult = 1;
									}
								}
								else
								{
									log(string.Format("(#{0}): HTTP Result=NO RESPONSE", serverRequest._httpAsync.SeqNo));
									serverRequest._serverResult = 1;
								}
								break;
							}
							case HttpAsync.RESULT.NOTFOUND:
								log(string.Format("(#{0}): HTTP Result=NOTFOUND", serverRequest._httpAsync.SeqNo));
								serverRequest._serverResult = 5;
								break;
							case HttpAsync.RESULT.ERROR:
								log(string.Format("(#{0}): HTTP Result=ERROR", serverRequest._httpAsync.SeqNo));
								serverRequest._serverResult = 1;
								break;
							case HttpAsync.RESULT.TIMEOUT:
								log(string.Format("(#{0}): HTTP Result=TIMEOUT", serverRequest._httpAsync.SeqNo));
								serverRequest._serverResult = 3;
								break;
							case HttpAsync.RESULT.ABORT:
								log(string.Format("(#{0}): HTTP Result=ABORT", serverRequest._httpAsync.SeqNo));
								serverRequest._serverResult = 6;
								break;
							default:
								log(string.Format("(#{0}): HTTP Result=UNKNOWN ERROR", serverRequest._httpAsync.SeqNo));
								serverRequest._serverResult = 1;
								break;
							}
						}
					}
					catch (Exception)
					{
						serverRequest._serverResult = 1;
					}
					lock (mResponsesLock)
					{
						mResponses.Enqueue(serverRequest);
					}
					SetConnecting(serverRequest._api, false);
				}
				while (serverRequest != null);
			}
			catch (Exception e)
			{
				BIJLog.E("Server comminucation something missing.", e);
			}
		}
	}

	private bool PostRequest(API api, ref HttpAsync httpAsync, Action<int, int> handler, HttpAsync.ResponseHandler responseHandler, object userdata)
	{
		int num = 0;
		lock (mRequestsLock)
		{
			num = mRequests.Count;
		}
		if (num >= 10)
		{
			return false;
		}
		ServerRequest serverRequest = new ServerRequest();
		serverRequest._api = api;
		serverRequest._httpAsync = httpAsync;
		serverRequest._handler = handler;
		serverRequest._responseHandler = responseHandler;
		serverRequest._userdata = userdata;
		serverRequest._serverResult = 0;
		SetConnecting(serverRequest._api, true);
		lock (mRequestsLock)
		{
			mRequests.Enqueue(serverRequest);
		}
		mRequestsNotify.Set();
		return true;
	}

	public static bool DLFilelistCheck(ResourceDownloadManager.KIND aKind)
	{
		API api = API.DLFilelistCheck;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.DLFilelistCheckEvent != null)
			{
				Server.DLFilelistCheckEvent(result, null);
			}
		};
		if (!GameMain.USE_RESOURCEDL && !IsSendable)
		{
			return false;
		}
		if (IsConnecting(api))
		{
			return false;
		}
		string value = SingletonMonoBehaviour<GameMain>.Instance.mGameProfile.DlListURL;
		string value2 = SingletonMonoBehaviour<GameMain>.Instance.mGameProfile.DlDataVer;
		switch (aKind)
		{
		case ResourceDownloadManager.KIND.MAIN_STAGE:
		case ResourceDownloadManager.KIND.ADV_STAGE:
			value = SingletonMonoBehaviour<GameMain>.Instance.mGameProfile.DlStageURL;
			value2 = SingletonMonoBehaviour<GameMain>.Instance.mGameProfile.DlStageVer;
			break;
		}
		if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value2))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(value);
		switch (aKind)
		{
		case ResourceDownloadManager.KIND.MAIN_STAGE:
			stringBuilder.Append("getFileList_Stg.php");
			break;
		case ResourceDownloadManager.KIND.ADV:
			stringBuilder.Append("getFileList_Adv.php");
			break;
		case ResourceDownloadManager.KIND.ADV_FIGURE:
			stringBuilder.Append("getFileList_AdvFigure.php");
			break;
		case ResourceDownloadManager.KIND.ADV_STAGE:
			stringBuilder.Append("getFileList_AdvStg.php");
			break;
		default:
			stringBuilder.Append("getFileList.php");
			break;
		}
		Dictionary<string, object> p = new Dictionary<string, object>();
		p["version"] = value2;
		HttpAsync httpAsync = null;
		bool flag = false;
		try
		{
			httpAsync = Network.CreateHttpGet(stringBuilder.ToString(), ref p, Timeout, false);
			return Instance.PostRequest(api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
			{
				int num = 0;
				HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
				try
				{
					using (Stream arg = httpWebResponse.GetResponseStream())
					{
						try
						{
							if (Server.DLFilelistCheckEvent != null)
							{
								Server.DLFilelistCheckEvent(num, arg);
							}
						}
						catch (Exception e2)
						{
							num = 1;
							BIJLog.E("DLFilelistCheck response handling error.", e2);
						}
					}
				}
				catch (Exception e3)
				{
					BIJLog.E("DLFilelistCheck response error.", e3);
					num = 1;
				}
				finally
				{
					if (num == 1 && handler != null)
					{
						handler(num, 0);
					}
					if (httpWebResponse != null)
					{
						httpWebResponse.Close();
					}
				}
			}, null);
		}
		catch (Exception e)
		{
			BIJLog.E("DLFilelistCheck request error.", e);
			return false;
		}
	}

	public static bool AccomplishResult(string campaign, int command, string code)
	{
		API _api = API.AccomplishResult;
		Action<int, int> handler = Server.AccomplishResultEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("accomplishResult.php");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		p["campaign"] = campaign;
		p["cmd"] = string.Empty + command;
		p["code"] = code;
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsync(stringBuilder.ToString(), ref p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("AccomplishResult: " + text);
							arg = (("1".CompareTo(text) != 0) ? 1 : 0);
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AccomplishResult response error.", e2);
						arg = 1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, 0);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AccomplishResult post error.");
					if (handler != null)
					{
						handler(1, 0);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AccomplishResult request error.", e);
			return false;
		}
	}

	private static bool IsConnecting(API api)
	{
		bool value = false;
		if (!mConnectFlags.TryGetValue(api, out value))
		{
			return false;
		}
		return value;
	}

	private static void SetConnecting(API api, bool connecting)
	{
		mConnectFlags[api] = connecting;
	}

	[Conditional("BIJ_DEBUG")]
	public static void ShowAPILog(int a_api, string a_pref = "")
	{
		if (a_api < 1000)
		{
		}
	}

	public static bool AdvGetAllItem()
	{
		API _api = API.AdvGetAllItem;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetAllItemEvent != null)
			{
				Server.AdvGetAllItemEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/all-item");
		AdvGetAllItemProfile_Request obj = new AdvGetAllItemProfile_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGetAllItem={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGetAllItemProfile_Response obj2 = new AdvGetAllItemProfile_Response();
								new MiniJSONSerializer().Populate(ref obj2, text);
								if (obj2 != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGetAllItemProfile = obj2;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGetAllItem response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetAllItem post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetAllItem request error.", e);
			return false;
		}
	}

	public static bool AdvQuestStart(int quest_id, int[] first_reward_ids, int[] enemy_reward_ids)
	{
		API _api = API.AdvQuestStart;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvQuestStartEvent != null)
			{
				Server.AdvQuestStartEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/quest/");
		stringBuilder.Append(quest_id.ToString());
		stringBuilder.Append("/start");
		AdvQuestStartProfile_Request advQuestStartProfile_Request = new AdvQuestStartProfile_Request();
		advQuestStartProfile_Request.FirstRewardIDs = first_reward_ids;
		advQuestStartProfile_Request.EnemyRewardIDs = enemy_reward_ids;
		string p = new MiniJSONSerializer().Serialize(advQuestStartProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvQuestStart={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvQuestStartProfile_Response obj = new AdvQuestStartProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvQuestStartProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvQuestStart response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvQuestStart post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvQuestStart request error.", e);
			return false;
		}
	}

	public static bool AdvQuestEnd(int quest_id, int[] first_reward_ids, int[] enemy_reward_ids, int is_win, string requestId, int is_retry)
	{
		API _api = API.AdvQuestEnd;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvQuestEndEvent != null)
			{
				Server.AdvQuestEndEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/quest/");
		stringBuilder.Append(quest_id.ToString());
		stringBuilder.Append("/end");
		AdvQuestEndProfile_Request advQuestEndProfile_Request = new AdvQuestEndProfile_Request();
		advQuestEndProfile_Request.IsWin = is_win;
		advQuestEndProfile_Request.FirstRewardIDs = first_reward_ids;
		advQuestEndProfile_Request.EnemyRewardIDs = enemy_reward_ids;
		advQuestEndProfile_Request.RequestID = requestId;
		advQuestEndProfile_Request.IsRetry = is_retry;
		string p = new MiniJSONSerializer().Serialize(advQuestEndProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvQuestEnd={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvQuestEndProfile_Response obj = new AdvQuestEndProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvQuestEndProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvQuestEnd response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvQuestEnd post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvQuestEnd request error.", e);
			return false;
		}
	}

	public static bool AdvGetGashaInfo()
	{
		API _api = API.AdvGetGashaInfo;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetGashaInfoEvent != null)
			{
				Server.AdvGetGashaInfoEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/gasha/list");
		AdvGetGashaInfoProfile_Request advGetGashaInfoProfile_Request = new AdvGetGashaInfoProfile_Request();
		if (!SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsGashaAdvTutorialCompleted())
		{
			advGetGashaInfoProfile_Request.IsTutorial = 1;
		}
		else
		{
			advGetGashaInfoProfile_Request.IsTutorial = 0;
		}
		string p = new MiniJSONSerializer().Serialize(advGetGashaInfoProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGetGashaInfo={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGetGashaInfoProfile_Response obj = new AdvGetGashaInfoProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGetGashaInfoProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGetGashaInfo response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetGashaInfo post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetGashaInfo request error.", e);
			return false;
		}
	}

	public static bool AdvGashaPlay(int group_id, int gasha_id, int price_id, string request_id, int is_retry)
	{
		API _api = API.AdvGashaPlay;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGashaPlayEvent != null)
			{
				Server.AdvGashaPlayEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/gasha/play");
		AdvGashaPlayProfile_Request advGashaPlayProfile_Request = new AdvGashaPlayProfile_Request();
		advGashaPlayProfile_Request.GroupID = group_id;
		advGashaPlayProfile_Request.GashaID = gasha_id;
		advGashaPlayProfile_Request.PriceID = price_id;
		advGashaPlayProfile_Request.RequestID = request_id;
		advGashaPlayProfile_Request.IsRetry = is_retry;
		advGashaPlayProfile_Request.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		advGashaPlayProfile_Request.series = SingletonMonoBehaviour<Network>.Instance.Series;
		advGashaPlayProfile_Request.lvl = SingletonMonoBehaviour<Network>.Instance.Level;
		advGashaPlayProfile_Request.mc = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		advGashaPlayProfile_Request.sc = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		advGashaPlayProfile_Request.tgt_series = SingletonMonoBehaviour<Network>.Instance.LastSeries;
		advGashaPlayProfile_Request.tgt_stage = SingletonMonoBehaviour<Network>.Instance.LastStageNo;
		advGashaPlayProfile_Request.uniq_id = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(advGashaPlayProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGashaPlay={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGashaPlayProfile_Response obj = new AdvGashaPlayProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaPlayProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGashaPlay response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGashaPlay post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGashaPlay request error.", e);
			return false;
		}
	}

	public static bool AdvGrowUpFigure(int figure_id, int lv_quantity, int skill_quantity, string request_id, int is_retry)
	{
		API _api = API.AdvGrowUpFigure;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGrowUpFigureEvent != null)
			{
				Server.AdvGrowUpFigureEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/figure/");
		stringBuilder.Append(figure_id.ToString());
		stringBuilder.Append("/growup");
		AdvGrowUpFigureProfile_Request advGrowUpFigureProfile_Request = new AdvGrowUpFigureProfile_Request();
		advGrowUpFigureProfile_Request.LvQuantity = lv_quantity;
		advGrowUpFigureProfile_Request.SkillLvQuantity = skill_quantity;
		advGrowUpFigureProfile_Request.RequestID = request_id;
		advGrowUpFigureProfile_Request.IsRetry = is_retry;
		string p = new MiniJSONSerializer().Serialize(advGrowUpFigureProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGrowUpFigure={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGrowUpFigureProfile_Response obj = new AdvGrowUpFigureProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGrowUpFigureProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGrowUpFigure response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGrowUpFigure post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGrowUpFigure request error.", e);
			return false;
		}
	}

	public static bool AdvCheckMasterTableVer()
	{
		API _api = API.AdvCheckMasterTableVer;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvCheckMasterTableVerEvent != null)
			{
				Server.AdvCheckMasterTableVerEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/master/revision/latest");
		AdvCheckMasterTableVerProfile_Request obj = new AdvCheckMasterTableVerProfile_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvCheckMasterTableVer={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvCheckMasterTableVerProfile_Response obj2 = new AdvCheckMasterTableVerProfile_Response();
								new MiniJSONSerializer().Populate(ref obj2, text);
								if (obj2 != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvCheckMasterTableVerProfile = obj2;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvCheckMasterTableVer response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvCheckMasterTableVer post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvCheckMasterTableVer request error.", e);
			return false;
		}
	}

	public static bool AdvGetMasterTableData(List<AdvMasterDataInfo> master_table_list)
	{
		API _api = API.AdvGetMasterTableData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetMasterTableDataEvent != null)
			{
				Server.AdvGetMasterTableDataEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/master");
		AdvGetMasterTableDataProfile_Request advGetMasterTableDataProfile_Request = new AdvGetMasterTableDataProfile_Request();
		advGetMasterTableDataProfile_Request.MasterTableInfoDataList = master_table_list;
		string p = new MiniJSONSerializer().Serialize(advGetMasterTableDataProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGetMasterTableData={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGetMasterTableDataProfile_Response obj = new AdvGetMasterTableDataProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGetMasterTableDataProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGetMasterTableData response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetMasterTableData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetMasterTableData request error.", e);
			return false;
		}
	}

	public static bool AdvGetRewardInfo(int[] reward_ids, string category)
	{
		API _api = API.AdvGetRewardInfo;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetRewardInfoEvent != null)
			{
				Server.AdvGetRewardInfoEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/");
		stringBuilder.Append(category);
		stringBuilder.Append("/reward");
		AdvGetRewardInfoProfile_Request advGetRewardInfoProfile_Request = new AdvGetRewardInfoProfile_Request();
		advGetRewardInfoProfile_Request.RewardIDs = reward_ids;
		string p = new MiniJSONSerializer().Serialize(advGetRewardInfoProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGetRewardInfo={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGetRewardInfoProfile_Response obj = new AdvGetRewardInfoProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGetRewardInfoProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGetRewardInfo response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetRewardInfo post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetRewardInfo request error.", e);
			return false;
		}
	}

	public static bool AdvSetReward(int[] reward_ids, int[] mail_rewards_ids, string requestId, int is_retry, string category)
	{
		API _api = API.AdvSetReward;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvSetRewardEvent != null)
			{
				Server.AdvSetRewardEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/");
		stringBuilder.Append(category);
		stringBuilder.Append("/reward/set");
		AdvSetRewardProfile_Request advSetRewardProfile_Request = new AdvSetRewardProfile_Request();
		if (reward_ids != null)
		{
			advSetRewardProfile_Request.RewardIDs = reward_ids;
		}
		else
		{
			advSetRewardProfile_Request.RewardIDs = new int[0];
		}
		if (mail_rewards_ids != null)
		{
			advSetRewardProfile_Request.MailRewardIDs = mail_rewards_ids;
		}
		else
		{
			advSetRewardProfile_Request.MailRewardIDs = new int[0];
		}
		advSetRewardProfile_Request.RequestID = requestId;
		advSetRewardProfile_Request.IsRetry = is_retry;
		string p = new MiniJSONSerializer().Serialize(advSetRewardProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvSetReward={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvSetRewardProfile_Response obj = new AdvSetRewardProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvSetRewardProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvSetReward response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvSetReward post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvSetReward request error.", e);
			return false;
		}
	}

	public static bool AdvSendSaveData(BIJBinaryReader dataReader, BIJBinaryReader metaReader, bool force, bool want)
	{
		API _api = API.AdvSendSaveData;
		Action<int, int> handler = Server.AdvSendSaveDataEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/gamedata/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		if (force)
		{
			stringBuilder.Append("?FORCE=1");
		}
		else if (want)
		{
			stringBuilder.Append("?WANT=1");
		}
		Dictionary<string, BIJBinaryReader> octets = new Dictionary<string, BIJBinaryReader>();
		if (dataReader != null)
		{
			octets["data"] = dataReader;
		}
		if (metaReader != null)
		{
			octets["metadata"] = metaReader;
		}
		AdvSendSaveDataProfile_Request advSendSaveDataProfile_Request = new AdvSendSaveDataProfile_Request();
		advSendSaveDataProfile_Request.FaceBookID = SingletonMonoBehaviour<Network>.Instance.Fbid;
		advSendSaveDataProfile_Request.MaxQuestID = SingletonMonoBehaviour<Network>.Instance.AdvLastStage;
		advSendSaveDataProfile_Request.Experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		advSendSaveDataProfile_Request.MainCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		advSendSaveDataProfile_Request.SubCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		string p = new MiniJSONSerializer().Serialize(advSendSaveDataProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpMultiPartAsyncJson(stringBuilder.ToString(), p, ref octets, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					int arg = 0;
					int arg2 = 0;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("AdvSendSaveData: " + text);
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvSendSaveData response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvSendSaveData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AdvSendSaveData request error.", e);
			return false;
		}
	}

	public static bool AdvGetSaveData()
	{
		API _api = API.AdvGetSaveData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetSaveDataEvent != null)
			{
				Server.AdvGetSaveDataEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/gamedata/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/get");
		AdvGetSaveDataProfile_Request obj = new AdvGetSaveDataProfile_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.AdvGetSaveDataEvent != null)
								{
									Server.AdvGetSaveDataEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("AdvGetSaveData response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("AdvGetSaveData response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetSaveData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetSaveData request error.", e);
			return false;
		}
	}

	public static bool AdvCheckSave()
	{
		API _api = API.AdvCheckSave;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvCheckSaveEvent != null)
			{
				Server.AdvCheckSaveEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/gamedata/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/check");
		AdvSendSaveDataProfile_Request advSendSaveDataProfile_Request = new AdvSendSaveDataProfile_Request();
		advSendSaveDataProfile_Request.FaceBookID = SingletonMonoBehaviour<Network>.Instance.Fbid;
		advSendSaveDataProfile_Request.MaxQuestID = SingletonMonoBehaviour<Network>.Instance.AdvLastStage;
		advSendSaveDataProfile_Request.Experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		advSendSaveDataProfile_Request.MainCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		advSendSaveDataProfile_Request.SubCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		string p = new MiniJSONSerializer().Serialize(advSendSaveDataProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvCheckSave={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								CheckSaveResponseProfile obj = new CheckSaveResponseProfile();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null && obj.is_save == 1)
								{
									arg2 = 0;
								}
								arg = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvCheckSave response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvCheckSave post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AdvCheckSave request error.", e);
			return false;
		}
	}

	public static bool AdvGetMail(string requestId, int is_retry)
	{
		API _api = API.AdvGetMail;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetMailEvent != null)
			{
				Server.AdvGetMailEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/get/arcade");
		AdvGetMailProfile_Request advGetMailProfile_Request = new AdvGetMailProfile_Request();
		advGetMailProfile_Request.RequestID = requestId;
		advGetMailProfile_Request.IsRetry = is_retry;
		string p = new MiniJSONSerializer().Serialize(advGetMailProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGetMail={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGetMailProfile_Response obj = new AdvGetMailProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGetMailProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGetMail response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetMail post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetMail request error.", e);
			return false;
		}
	}

	public static bool AdvSendGotMail(int[] id, string requestId, int is_retry)
	{
		API _api = API.AdvSendGotMail;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvSendGotMailEvent != null)
			{
				Server.AdvSendGotMailEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/got/arcade");
		AdvSendGotMailProfile_Request advSendGotMailProfile_Request = new AdvSendGotMailProfile_Request();
		advSendGotMailProfile_Request.MailTargets = new List<AdvMailTarget>();
		for (int i = 0; i < id.Length; i++)
		{
			AdvMailTarget advMailTarget = new AdvMailTarget();
			advMailTarget.id = id[i];
			advSendGotMailProfile_Request.MailTargets.Add(advMailTarget);
		}
		advSendGotMailProfile_Request.RequestID = requestId;
		advSendGotMailProfile_Request.IsRetry = is_retry;
		string p = new MiniJSONSerializer().Serialize(advSendGotMailProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvSendGotMail={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvSendGotMailProfile_Response obj = new AdvSendGotMailProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvSendGotMailProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvSendGotMail response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvSendGotMail post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvSendGotMail request error.", e);
			return false;
		}
	}

	public static bool AdvGetEventPoint(short[] event_ids)
	{
		API _api = API.AdvGetEventPoint;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetEventPointEvent != null)
			{
				Server.AdvGetEventPointEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/event/point/get");
		AdvGetEventPointProfile_Request advGetEventPointProfile_Request = new AdvGetEventPointProfile_Request();
		advGetEventPointProfile_Request.EventIDs = event_ids;
		string p = new MiniJSONSerializer().Serialize(advGetEventPointProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvGetEventPoint={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvGetEventPointProfile_Response obj = new AdvGetEventPointProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvGetEventPointProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvGetEventPoint response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetEventPoint post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetEventPoint request error.", e);
			return false;
		}
	}

	public static bool AdvSetEventPoint(short event_id, int quantity, string requestId, int is_retry)
	{
		API _api = API.AdvSetEventPoint;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvSetEventPointEvent != null)
			{
				Server.AdvSetEventPointEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/event/point/add");
		AdvSetEventPointProfile_Request advSetEventPointProfile_Request = new AdvSetEventPointProfile_Request();
		advSetEventPointProfile_Request.EventID = event_id;
		advSetEventPointProfile_Request.Quantity = quantity;
		advSetEventPointProfile_Request.RequestID = requestId;
		advSetEventPointProfile_Request.IsRetry = is_retry;
		string p = new MiniJSONSerializer().Serialize(advSetEventPointProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): AdvSetEventPoint={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								AdvSetEventPointProfile_Response obj = new AdvSetEventPointProfile_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvSetEventPointProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvSetEventPoint response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvSetEventPoint post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("AdvSetEventPoint request error.", e);
			return false;
		}
	}

	public static bool AdvSaveTransferGameState(BIJBinaryReader dataReader, BIJBinaryReader metaReader)
	{
		API _api = API.AdvSaveTransferGameState;
		Action<int, int> handler = Server.AdvSaveTransferGameStateEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/gamedata/transfer/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		Dictionary<string, BIJBinaryReader> octets = new Dictionary<string, BIJBinaryReader>();
		if (dataReader != null)
		{
			octets["data"] = dataReader;
		}
		if (metaReader != null)
		{
			octets["metadata"] = metaReader;
		}
		AdvSaveTransferGameStateProfile_Request advSaveTransferGameStateProfile_Request = new AdvSaveTransferGameStateProfile_Request();
		advSaveTransferGameStateProfile_Request.FaceBookID = SingletonMonoBehaviour<Network>.Instance.Fbid;
		advSaveTransferGameStateProfile_Request.MaxQuestID = SingletonMonoBehaviour<Network>.Instance.AdvLastStage;
		advSaveTransferGameStateProfile_Request.Experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		advSaveTransferGameStateProfile_Request.MainCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		advSaveTransferGameStateProfile_Request.SubCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		string p = new MiniJSONSerializer().Serialize(advSaveTransferGameStateProfile_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpMultiPartAsyncJson(stringBuilder.ToString(), p, ref octets, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					int arg = 0;
					int arg2 = 0;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("AdvSaveTransferGameState: " + text);
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("AdvSaveTransferGameState response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvSaveTransferGameState post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AdvSaveTransferGameState request error.", e);
			return false;
		}
	}

	public static bool AdvGetTransferGameState()
	{
		API _api = API.AdvGetTransferGameState;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetTransferGameStateEvent != null)
			{
				Server.AdvGetTransferGameStateEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/gamedata/transfer/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/get");
		AdvGetTransferGameStateProfile_Request obj = new AdvGetTransferGameStateProfile_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.AdvGetTransferGameStateEvent != null)
								{
									Server.AdvGetTransferGameStateEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("AdvGetTransferGameState response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("AdvGetTransferGameState response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetTransferGameState post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetTransferGameState request error.", e);
			return false;
		}
	}

	public static bool AdvGetTransferParam()
	{
		API _api = API.AdvGetTransferParam;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvGetTransferParamEvent != null)
			{
				Server.AdvGetTransferParamEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/gamedata/transfer/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/param/get");
		AdvGetTransferParamProfile_Request obj = new AdvGetTransferParamProfile_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.AdvGetTransferParamEvent != null)
								{
									Server.AdvGetTransferParamEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("AdvGetTransferParam response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("AdvGetTransferParam response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AdvGetTransferParam post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AdvGetTransferParam request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_FigureAllDelete()
	{
		API _api = API.AdvDebug_FigureAllDelete;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_FigureAllDeleteEvent != null)
			{
				Server.AdvDebug_FigureAllDeleteEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/all-figure/unset");
		AdvDebug_FigureAllDelete_Request obj = new AdvDebug_FigureAllDelete_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.AdvDebug_FigureAllDeleteEvent != null)
								{
									Server.AdvDebug_FigureAllDeleteEvent(0, 0);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("response error.", e3);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_RewardAllDelete()
	{
		API _api = API.AdvDebug_RewardAllDelete;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_RewardAllDeleteEvent != null)
			{
				Server.AdvDebug_RewardAllDeleteEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/all-reward/unset");
		AdvDebug_RewardAllDelete_Request obj = new AdvDebug_RewardAllDelete_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.AdvDebug_RewardAllDeleteEvent != null)
								{
									Server.AdvDebug_RewardAllDeleteEvent(0, 0);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("response error.", e3);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_SetItemData(int itemId, int Quantity)
	{
		API _api = API.AdvDebug_SetItemData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_SetItemDataEvent != null)
			{
				Server.AdvDebug_SetItemDataEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/item/");
		stringBuilder.Append(itemId.ToString());
		stringBuilder.Append("/update");
		AdvDebug_SetItemData_Request advDebug_SetItemData_Request = new AdvDebug_SetItemData_Request();
		advDebug_SetItemData_Request.quantity = Quantity;
		string p = new MiniJSONSerializer().Serialize(advDebug_SetItemData_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								AdvDebug_SetItemData_Response obj = new AdvDebug_SetItemData_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvDebug_SetItemDataProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_GetAllItem()
	{
		API _api = API.AdvDebug_GetAllItem;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_GetAllItemEvent != null)
			{
				Server.AdvDebug_GetAllItemEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/all-item/set");
		AdvDebug_GetAllItem_Request obj = new AdvDebug_GetAllItem_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								AdvDebug_GetAllItem_Response obj2 = new AdvDebug_GetAllItem_Response();
								new MiniJSONSerializer().Populate(ref obj2, text);
								if (obj2 != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvDebug_GetAllItemProfile = obj2;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_GashaInfoAllDelete()
	{
		API _api = API.AdvDebug_GashaInfoAllDelete;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_GashaInfoAllDeleteEvent != null)
			{
				Server.AdvDebug_GashaInfoAllDeleteEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/all-gasha/unset");
		AdvDebug_GashaInfoAllDelete_Request obj = new AdvDebug_GashaInfoAllDelete_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.AdvDebug_GashaInfoAllDeleteEvent != null)
								{
									Server.AdvDebug_GashaInfoAllDeleteEvent(0, 0);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("response error.", e3);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_FigureAllGrowUp()
	{
		API _api = API.AdvDebug_FigureAllGrowUp;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_FigureAllGrowUpEvent != null)
			{
				Server.AdvDebug_FigureAllGrowUpEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/all-figure/maxlvl");
		AdvDebug_FigureAllGrowUp_Request obj = new AdvDebug_FigureAllGrowUp_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								AdvDebug_FigureAllGrowUp_Response obj2 = new AdvDebug_FigureAllGrowUp_Response();
								new MiniJSONSerializer().Populate(ref obj2, text);
								if (obj2 != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvDebug_FigureAllGrowUpProfile = obj2;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_AddFigure(int figureId)
	{
		API _api = API.AdvDebug_AddFigure;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_AddFigureEvent != null)
			{
				Server.AdvDebug_AddFigureEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/figure/");
		stringBuilder.Append(figureId.ToString());
		stringBuilder.Append("/set");
		AdvDebug_AddFigure_Request obj = new AdvDebug_AddFigure_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								AdvDebug_AddFigure_Response obj2 = new AdvDebug_AddFigure_Response();
								new MiniJSONSerializer().Populate(ref obj2, text);
								if (obj2 != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvDebug_AddFigureProfile = obj2;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_RemoveFigure(int figureId)
	{
		API _api = API.AdvDebug_RemoveFigure;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_RemoveFigureEvent != null)
			{
				Server.AdvDebug_RemoveFigureEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/figure/");
		stringBuilder.Append(figureId.ToString());
		stringBuilder.Append("/unset");
		AdvDebug_RemoveFigure_Request obj = new AdvDebug_RemoveFigure_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								AdvDebug_RemoveFigure_Response obj2 = new AdvDebug_RemoveFigure_Response();
								new MiniJSONSerializer().Populate(ref obj2, text);
								if (obj2 != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvDebug_RemoveFigureProfile = obj2;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_GetAllFigure()
	{
		API _api = API.AdvDebug_GetAllFigure;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_GetAllFigureEvent != null)
			{
				Server.AdvDebug_GetAllFigureEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/all-figure/set");
		AdvDebug_GetAllFigure_Request obj = new AdvDebug_GetAllFigure_Request();
		string p = new MiniJSONSerializer().Serialize(obj);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								AdvDebug_GetAllFigure_Response obj2 = new AdvDebug_GetAllFigure_Response();
								new MiniJSONSerializer().Populate(ref obj2, text);
								if (obj2 != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvDebug_GetAllFigureProfile = obj2;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public static bool AdvDebug_SetFigureStatus(int figureId, int level, int skilllevel, int skil_exp)
	{
		API _api = API.AdvDebug_SetFigureStatus;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.AdvDebug_SetFigureStatusEvent != null)
			{
				Server.AdvDebug_SetFigureStatusEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID.ToString());
		stringBuilder.Append("/figure/");
		stringBuilder.Append(figureId.ToString());
		stringBuilder.Append("/update");
		AdvDebug_SetFigureStatus_Request advDebug_SetFigureStatus_Request = new AdvDebug_SetFigureStatus_Request();
		advDebug_SetFigureStatus_Request.lvl = level;
		advDebug_SetFigureStatus_Request.skill_lvl = skilllevel;
		advDebug_SetFigureStatus_Request.skill_xp = skil_exp;
		string p = new MiniJSONSerializer().Serialize(advDebug_SetFigureStatus_Request);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								AdvDebug_SetFigureStatus_Response obj = new AdvDebug_SetFigureStatus_Response();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mAdvDebug_SetFigureStatusProfile = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}

	public byte[] GetResponseByte(Stream _Stream)
	{
		byte[] array = new byte[1024];
		MemoryStream memoryStream = new MemoryStream();
		while (true)
		{
			int num = _Stream.Read(array, 0, array.Length);
			if (num > 0)
			{
				memoryStream.Write(array, 0, num);
				continue;
			}
			break;
		}
		byte[] result = memoryStream.ToArray();
		memoryStream.Close();
		return result;
	}

	public bool CheckResponseGZIPData(WebHeaderCollection _headers)
	{
		bool result = false;
		for (int i = 0; i < _headers.Count; i++)
		{
			string key = _headers.GetKey(i);
			if (key == "X-Bij-Content-Encoding")
			{
				result = true;
				break;
			}
		}
		return result;
	}

	public static bool ConnectionEstablish()
	{
		API _api = API.ConnectionEstablish;
		Action<int, int> handler = Server.ConnectionEstablishEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.Connection_URL);
		string empty = string.Empty;
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), empty, Timeout, delegate(HttpAsync httpAsync)
			{
				bool flag2 = false;
				if (!httpAsync.IsRequestTimeout)
				{
					flag2 = Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
					{
						int arg = 1;
						int arg2 = -1;
						HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
						string empty2 = string.Empty;
						try
						{
							arg = 0;
							arg2 = 0;
						}
						catch (Exception e)
						{
							BIJLog.E("ConectionEstablish response error. : " + empty2, e);
							arg = 1;
							arg2 = -1;
						}
						finally
						{
							if (handler != null)
							{
								handler(arg, arg2);
							}
							if (httpWebResponse != null)
							{
								httpWebResponse.Close();
							}
						}
					}, null);
				}
				if (!flag2)
				{
					BIJLog.E("ConectionEstablish post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception)
		{
			return false;
		}
	}

	public static string MakeUserAuthParameter()
	{
		UserAuth userAuth = new UserAuth();
		userAuth.UDID = Network.BNIDPublishID;
		userAuth.UnityID = Network.UnityID;
		userAuth.UniqueID = Network.BNIDAuthID;
		userAuth.UUID = SingletonMonoBehaviour<Network>.Instance.UUID;
		userAuth.MacAddress = Network.MacAddress;
		userAuth.HiveId = SingletonMonoBehaviour<Network>.Instance.HiveId;
		userAuth.OpenUDID = Network.OpenUDID;
		userAuth.AdvertisingID = Network.AdvertisingID;
		return new MiniJSONSerializer().Serialize(userAuth);
	}

	public static bool UserAuthCheck()
	{
		API _api = API.UserAuth;
		Action<int, int> handler = Server.UserAuthEvent;
		if (GameMain.USE_DEBUG_DIALOG)
		{
		}
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/user/auth");
		string p = MakeUserAuthParameter();
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					string text = string.Empty;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							text = streamReader.ReadToEnd();
							UserAuthResponse obj = Network.NetworkInterface.UserUniqueID;
							dbg(string.Format("(#{0}): UserAuthCheck={1}", _httpAsync.SeqNo, text));
							new MiniJSONSerializer().Populate(ref obj, text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("UserAuthCheck response error. : " + text, e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("UserAuthCheck post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("UserAuthCheck request error.", e);
			return false;
		}
	}

	public static bool DebugTerminal()
	{
		API _api = API.GetDebugTerminal;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.DebugTerminalRegistration != null)
			{
				Server.DebugTerminalRegistration(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/debuguser/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/register");
		NWDebugTerminal nWDebugTerminal = new NWDebugTerminal();
		nWDebugTerminal.UDID = Network.BNIDPublishID;
		nWDebugTerminal.OpenUDID = Network.OpenUDID;
		nWDebugTerminal.UniqueID = Network.BNIDAuthID;
		nWDebugTerminal.HiveID = SingletonMonoBehaviour<Network>.Instance.HiveId;
		nWDebugTerminal.BundleID = new BIJMD5(BIJUnity.getBundleName()).ToString();
		string p = new MiniJSONSerializer().Serialize(nWDebugTerminal);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.DebugTerminalRegistration != null)
								{
									Server.DebugTerminalRegistration(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("DebugTerminalRegistration response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("DebugTerminalRegistration response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("DebugTerminalRegistration post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("DebugTerminalRegistration request error.", e);
			return false;
		}
	}

	public static bool SendDeviceToken(string deviceToken)
	{
		API _api = API.SendDeviceToken;
		Action<int, int> handler = Server.SendDeviceTokenEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/notification/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/token");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		SendToken sendToken = new SendToken();
		sendToken.UDID = p["udid"] as string;
		sendToken.OpenUDID = Network.OpenUDID;
		sendToken.UniqueID = Network.BNIDAuthID;
		sendToken.Token = deviceToken;
		string p2 = new MiniJSONSerializer().Serialize(sendToken);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p2, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("SendDeviceToken: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("SendDeviceToken response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("SendDeviceToken post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("SendDeviceToken request error.", e);
			return false;
		}
	}

	public static string ErrorText(int code)
	{
		string empty = string.Empty;
		switch ((ErrorCode)code)
		{
		case ErrorCode.OTHER:
			return "internal application\nerror";
		case ErrorCode.SUCCESS:
			return "no error";
		case ErrorCode.TIMEOUT:
			return "Time out";
		case ErrorCode.NOTFOUND:
			return "Server Page Not Found";
		case ErrorCode.JSON_PARSE:
			return "Json Parse\nerror";
		case ErrorCode.JSON_STREAM:
			return "Server Stream\nerror";
		case ErrorCode.ABORT:
			return "System.Net.WebException:\nAborted.";
		default:
			return Localization.Get("Debug_ShowServerError_" + code);
		}
	}

	public static bool RegisterFriend(int a_toID)
	{
		API _api = API.RegisterFriend;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.RegisterFriendEvent != null)
			{
				Server.RegisterFriendEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/friend/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/register/");
		stringBuilder.Append(a_toID);
		RegisterFriend registerFriend = new RegisterFriend();
		registerFriend.UDID = Network.BNIDPublishID;
		registerFriend.OpenUDID = Network.OpenUDID;
		registerFriend.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(registerFriend);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					int arg = 1;
					int arg2 = -1;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							try
							{
								arg = 0;
								arg2 = 0;
								string text = streamReader.ReadToEnd();
								RegisterResponse obj = new RegisterResponse();
								dbg(string.Format("(#{0}): RegisterFriend={1}", _httpAsync.SeqNo, text));
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj.status == 1)
								{
									arg = 1;
									arg2 = 15000;
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetFriends response handling error.", e2);
								arg = 1;
								arg2 = -1;
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetFriends response error.", e3);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetFriends post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetFriends request error.", e);
			return false;
		}
	}

	public static bool GetFriends()
	{
		API _api = API.GetFriends;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetFriendsEvent != null)
			{
				Server.GetFriendsEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/friend/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/list");
		GetFriendList getFriendList = new GetFriendList();
		getFriendList.UDID = Network.BNIDPublishID;
		getFriendList.OpenUDID = Network.OpenUDID;
		getFriendList.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getFriendList);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							try
							{
								string arg = string.Empty;
								if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
								{
									byte[] responseByte = Instance.GetResponseByte(stream);
									arg = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
								}
								else
								{
									using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
									{
										arg = streamReader.ReadToEnd();
									}
								}
								if (Server.GetFriendsEvent != null)
								{
									Server.GetFriendsEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetFriends response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetFriends response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetFriends post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetFriends request error.", e);
			return false;
		}
	}

	public static bool RemoveFriend(int a_toID = -1)
	{
		API _api = API.RemoveFriend;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.RemoveFriendEvent != null)
			{
				Server.RemoveFriendEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/friend/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		if (a_toID == -1)
		{
			stringBuilder.Append("/remove");
		}
		else
		{
			stringBuilder.Append("/remove/");
			stringBuilder.Append(a_toID);
		}
		RemoveFriend removeFriend = new RemoveFriend();
		removeFriend.UDID = Network.BNIDPublishID;
		removeFriend.OpenUDID = Network.OpenUDID;
		removeFriend.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(removeFriend);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					int arg = 1;
					int arg2 = -1;
					try
					{
						using (httpWebResponse.GetResponseStream())
						{
							try
							{
								arg = 0;
								arg2 = 0;
							}
							catch (Exception e2)
							{
								BIJLog.E("GetFriends response handling error.", e2);
								arg = 1;
								arg2 = -1;
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetFriends response error.", e3);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetFriends post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetFriends request error.", e);
			return false;
		}
	}

	public static bool GetSNSFriendData(List<string> a_list, SNS a_kind)
	{
		API _api = API.GetSNSFriendData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetSNSFriendDataEvent != null)
			{
				Server.GetSNSFriendDataEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/search/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/facebook");
		GetSNSFriendList getSNSFriendList = new GetSNSFriendList();
		getSNSFriendList.UDID = Network.BNIDPublishID;
		getSNSFriendList.OpenUDID = Network.OpenUDID;
		getSNSFriendList.UniqueID = Network.BNIDAuthID;
		getSNSFriendList.SetTarget(a_list, a_kind);
		string p = new MiniJSONSerializer().Serialize(getSNSFriendList);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							try
							{
								string arg = string.Empty;
								if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
								{
									byte[] responseByte = Instance.GetResponseByte(stream);
									arg = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
								}
								else
								{
									using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
									{
										arg = streamReader.ReadToEnd();
									}
								}
								if (Server.GetSNSFriendDataEvent != null)
								{
									Server.GetSNSFriendDataEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetSNSFriendData response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetSNSFriendData response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetSNSFriendData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetSNSFriendData request error.", e);
			return false;
		}
	}

	public static bool GetRandomUser(int a_series, int a_stage, int a_range, int a_cnt)
	{
		API _api = API.SearchUserRandom;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetRandomUserEvent != null)
			{
				Server.GetRandomUserEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/search/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		SearchRandomUser searchRandomUser = new SearchRandomUser();
		searchRandomUser.UDID = Network.BNIDPublishID;
		searchRandomUser.OpenUDID = Network.OpenUDID;
		searchRandomUser.UniqueID = Network.BNIDAuthID;
		searchRandomUser.Series = a_series;
		searchRandomUser.Stage = a_stage;
		searchRandomUser.Range = a_range;
		searchRandomUser.Count = a_cnt;
		string p = new MiniJSONSerializer().Serialize(searchRandomUser);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							try
							{
								string arg = string.Empty;
								if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
								{
									byte[] responseByte = Instance.GetResponseByte(stream);
									arg = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
								}
								else
								{
									using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
									{
										arg = streamReader.ReadToEnd();
									}
								}
								if (Server.GetRandomUserEvent != null)
								{
									Server.GetRandomUserEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetRandomUser response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetRandomUser response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetRandomUser post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetRandomUser request error.", e);
			return false;
		}
	}

	public static bool GetIDUser(string a_searchid)
	{
		API _api = API.SearchUserID;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetIDUserEvent != null)
			{
				Server.GetIDUserEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/search/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/id/");
		stringBuilder.Append(a_searchid);
		SearchIDUser searchIDUser = new SearchIDUser();
		searchIDUser.UDID = Network.BNIDPublishID;
		searchIDUser.OpenUDID = Network.OpenUDID;
		searchIDUser.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(searchIDUser);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							try
							{
								string arg = string.Empty;
								if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
								{
									byte[] responseByte = Instance.GetResponseByte(stream);
									arg = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
								}
								else
								{
									using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
									{
										arg = streamReader.ReadToEnd();
									}
								}
								if (Server.GetIDUserEvent != null)
								{
									Server.GetIDUserEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetIDUserEvent response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetIDUserEvent response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetIDUserEvent post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetIDUserEvent request error.", e);
			return false;
		}
	}

	public static bool SaveGameState(BIJBinaryReader dataReader, BIJBinaryReader metaReader, Dictionary<string, object> overwrites = null)
	{
		return SaveGameState(dataReader, metaReader, false, false, overwrites);
	}

	public static bool SaveForceGameState(BIJBinaryReader dataReader, BIJBinaryReader metaReader, Dictionary<string, object> overwrites = null)
	{
		return SaveGameState(dataReader, metaReader, true, false, overwrites);
	}

	public static bool SaveGameState(BIJBinaryReader dataReader, BIJBinaryReader metaReader, bool force, bool want, Dictionary<string, object> overwrites)
	{
		API _api = API.SaveGameData;
		Action<int, int> handler = Server.SaveGameStateEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/gamedata/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		if (force)
		{
			stringBuilder.Append("?FORCE=1");
		}
		else if (want)
		{
			stringBuilder.Append("?WANT=1");
		}
		Dictionary<string, BIJBinaryReader> octets = new Dictionary<string, BIJBinaryReader>();
		if (dataReader != null)
		{
			octets["data"] = dataReader;
		}
		if (metaReader != null)
		{
			octets["metadata"] = metaReader;
		}
		string p = MakeSaveDataParameter();
		bool flag = false;
		try
		{
			return Network.CreateHttpMultiPartAsyncJson(stringBuilder.ToString(), p, ref octets, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					int arg = 0;
					int arg2 = 0;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("SaveGameState: " + text);
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("SaveGameState response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("SaveGameState post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("SaveGameState request error.", e);
			return false;
		}
	}

	public static string MakeSaveDataParameter()
	{
		int series = SingletonMonoBehaviour<Network>.Instance.Series;
		int level = SingletonMonoBehaviour<Network>.Instance.Level;
		int masterCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		int secondaryCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		int experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		string fbid = SingletonMonoBehaviour<Network>.Instance.Fbid;
		int clearSeries = SingletonMonoBehaviour<Network>.Instance.ClearSeries;
		int clearStageNo = SingletonMonoBehaviour<Network>.Instance.ClearStageNo;
		long totalScore = SingletonMonoBehaviour<Network>.Instance.TotalScore;
		long totalStars = SingletonMonoBehaviour<Network>.Instance.TotalStars;
		long totalTrophy = SingletonMonoBehaviour<Network>.Instance.TotalTrophy;
		SendSaveData sendSaveData = new SendSaveData();
		sendSaveData.UDID = Network.BNIDPublishID;
		sendSaveData.OpenUDID = Network.OpenUDID;
		sendSaveData.UniqueID = Network.BNIDAuthID;
		sendSaveData.FBID = fbid;
		sendSaveData.Series = series;
		sendSaveData.Level = level;
		sendSaveData.PlayTime = experience;
		sendSaveData.MainCurrency = masterCurrency;
		sendSaveData.SubCurrency = secondaryCurrency;
		sendSaveData.TotalScore = totalScore;
		sendSaveData.TotalStar = totalStars;
		sendSaveData.TotalTrophy = totalTrophy;
		sendSaveData.PlayStage = SingletonMonoBehaviour<Network>.Instance.PlayStage;
		return new MiniJSONSerializer().Serialize(sendSaveData);
	}

	public static bool FirstGetGameState()
	{
		int hiveId = SingletonMonoBehaviour<Network>.Instance.HiveId;
		int uUID = SingletonMonoBehaviour<Network>.Instance.UUID;
		return GetServerGameData(false, hiveId, uUID);
	}

	public static bool GetGameState()
	{
		int hiveId = SingletonMonoBehaviour<Network>.Instance.HiveId;
		int uUID = SingletonMonoBehaviour<Network>.Instance.UUID;
		return GetServerGameData(true, hiveId, uUID);
	}

	public static bool GetServerGameData(bool usehive, int hiveId, int uuid)
	{
		API _api = API.GetGameData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetServerGameDataEvent != null)
			{
				Server.GetServerGameDataEvent(result, code, null, 0);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/gamedata/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		if (!usehive)
		{
			stringBuilder.Append("/get");
		}
		else
		{
			stringBuilder.Append("/get");
		}
		GetSaveData getSaveData = new GetSaveData();
		getSaveData.UDID = Network.BNIDPublishID;
		getSaveData.OpenUDID = Network.OpenUDID;
		getSaveData.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getSaveData);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					string responseHeader = httpWebResponse.GetResponseHeader("X-BIJ-DATVER");
					int arg = 0;
					if (!string.IsNullOrEmpty(responseHeader))
					{
						arg = int.Parse(responseHeader);
					}
					try
					{
						using (Stream arg2 = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetServerGameDataEvent != null)
								{
									Server.GetServerGameDataEvent(0, 0, arg2, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetServerGameData response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetServerGameData response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetServerGameData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetServerGameData request error.", e);
			return false;
		}
	}

	public static bool GetMetadata()
	{
		int uUID = SingletonMonoBehaviour<Network>.Instance.UUID;
		return GetMetadata(uUID);
	}

	public static bool GetMetadata(int uuid)
	{
		API _api = API.GetMetadata;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetMetadataEvent != null)
			{
				Server.GetMetadataEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("getMetadata.php");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		p["tgt_uuid"] = uuid;
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsync(stringBuilder.ToString(), ref p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetMetadataEvent != null)
								{
									Server.GetMetadataEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetMetadata response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetMetadata response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetMetadata post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetMetadata request error.", e);
			return false;
		}
	}

	public static bool CheckSave()
	{
		API _api = API.CheckSave;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.CheckSaveEvent != null)
			{
				Server.CheckSaveEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/gamedata/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/check");
		string p = MakeSaveDataParameter();
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text))
							{
								CheckSaveResponseProfile obj = new CheckSaveResponseProfile();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null && obj.is_save == 1)
								{
									arg2 = 0;
								}
								arg = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("CheckSave response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("CheckSave post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("CheckSave request error.", e);
			return false;
		}
	}

	public static bool GetGameInfo(string a_datekey = "", string a_debugkey = "")
	{
		API _api = API.GetGameInfo;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetGameInfoEvent != null)
			{
				Server.GetGameInfoEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/conf/game");
		GetGameProfile getGameProfile = new GetGameProfile();
		getGameProfile.UDID = Network.BNIDPublishID;
		getGameProfile.OpenUDID = Network.OpenUDID;
		getGameProfile.UniqueID = Network.BNIDAuthID;
		if (!string.IsNullOrEmpty(a_datekey))
		{
			getGameProfile.DateKey = long.Parse(a_datekey);
		}
		if (!string.IsNullOrEmpty(a_debugkey))
		{
			getGameProfile.DebugKey = a_debugkey;
		}
		else
		{
			getGameProfile.DebugKey = string.Empty;
		}
		string p = new MiniJSONSerializer().Serialize(getGameProfile);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): GetGameInfo={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								_GameSettingInfo obj = new _GameSettingInfo();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj.gameinfo != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mGameProfile = obj.gameinfo;
								}
								if (obj.eventinfo != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mEventProfile = obj.eventinfo;
								}
								string mServerGameProfileDirectory = string.Empty;
								if (!string.IsNullOrEmpty(obj.dir))
								{
									mServerGameProfileDirectory = obj.dir;
								}
								GameMain.mServerGameProfileDirectory = mServerGameProfileDirectory;
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GetGameInfo response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetGameInfo post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetGameInfo request error.", e);
			return false;
		}
	}

	public static bool GiveGift(IGiftItem gift, int a_toID)
	{
		API _api = API.GiveGift;
		Action<int, int> handler = Server.GiveGiftEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/send/gift/");
		stringBuilder.Append(a_toID);
		GiveGift giveGift = new GiveGift();
		giveGift.UDID = Network.BNIDPublishID;
		giveGift.OpenUDID = Network.OpenUDID;
		giveGift.UniqueID = Network.BNIDAuthID;
		giveGift.Category = gift.ItemCategory;
		giveGift.CategorySub = gift.ItemKind;
		giveGift.Quantity = gift.Quantity;
		giveGift.ItemID = gift.ItemID;
		string p = new MiniJSONSerializer().Serialize(giveGift);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("GiveGift: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GiveGift response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GiveGift post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GiveGift request error.", e);
			return false;
		}
	}

	public static bool GiveGiftAll(IGiftItem gift, List<int> a_toID)
	{
		API _api = API.GiveGiftAll;
		Action<int, int> handler = Server.GiveGiftAllEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/send/all/gift");
		GiveGiftALL giveGiftALL = new GiveGiftALL();
		giveGiftALL.UDID = Network.BNIDPublishID;
		giveGiftALL.OpenUDID = Network.OpenUDID;
		giveGiftALL.UniqueID = Network.BNIDAuthID;
		giveGiftALL.Category = gift.ItemCategory;
		giveGiftALL.CategorySub = gift.ItemKind;
		giveGiftALL.Quantity = gift.Quantity;
		giveGiftALL.ItemID = gift.ItemID;
		for (int i = 0; i < a_toID.Count; i++)
		{
			giveGiftALL.SetTarget(a_toID[i]);
		}
		string p = new MiniJSONSerializer().Serialize(giveGiftALL);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("GiveGift: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GiveGift response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GiveGift post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GiveGift request error.", e);
			return false;
		}
	}

	public static bool GetGift(int amount)
	{
		return GetGift(MailType.GIFT, amount);
	}

	public static bool GetGift(MailType _kind, int amount)
	{
		API _api = API.GetGift;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetGiftEvent != null)
			{
				Server.GetGiftEvent(result, code, _kind, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		switch (_kind)
		{
		case MailType.GIFT:
			stringBuilder.Append("/get/gift");
			break;
		case MailType.FRIEND:
			stringBuilder.Append("/get/friend");
			break;
		case MailType.SUPPORT:
			stringBuilder.Append("/get/support");
			break;
		}
		GetMail getMail = new GetMail();
		getMail.UDID = Network.BNIDPublishID;
		getMail.OpenUDID = Network.OpenUDID;
		getMail.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getMail);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							try
							{
								string arg = string.Empty;
								if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
								{
									byte[] responseByte = Instance.GetResponseByte(stream);
									arg = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
								}
								else
								{
									using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
									{
										arg = streamReader.ReadToEnd();
									}
								}
								if (Server.GetGiftEvent != null)
								{
									Server.GetGiftEvent(0, 0, _kind, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetGift response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetGift response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetGift post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetGift request error.", e);
			return false;
		}
	}

	public static bool GotGift(MailType _kind, List<int> _ids)
	{
		API _api = API.GotGift;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GotGiftEvent != null)
			{
				Server.GotGiftEvent(result, code, _kind);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		switch (_kind)
		{
		case MailType.GIFT:
			stringBuilder.Append("/got/gift");
			break;
		case MailType.FRIEND:
			stringBuilder.Append("/got/friend");
			break;
		case MailType.SUPPORT:
			stringBuilder.Append("/got/support");
			break;
		}
		GotMail gotMail = new GotMail();
		gotMail.UDID = Network.BNIDPublishID;
		gotMail.OpenUDID = Network.OpenUDID;
		gotMail.UniqueID = Network.BNIDAuthID;
		for (int i = 0; i < _ids.Count; i++)
		{
			gotMail.AddTargets(_ids[i]);
		}
		string p = new MiniJSONSerializer().Serialize(gotMail);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("GotGift: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GotGift response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GotGift post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GotGift request error.", e);
			return false;
		}
	}

	public static bool GiveApply(IGiftItem gift, int a_toID)
	{
		API _api = API.GiveApply;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GiveApplyEvent != null)
			{
				Server.GiveApplyEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/send/friend/");
		stringBuilder.Append(a_toID);
		GiveApply giveApply = new GiveApply();
		giveApply.UDID = Network.BNIDPublishID;
		giveApply.OpenUDID = Network.OpenUDID;
		giveApply.UniqueID = Network.BNIDAuthID;
		giveApply.Category = gift.ItemCategory;
		giveApply.CategorySub = gift.ItemKind;
		giveApply.Quantity = gift.Quantity;
		giveApply.ItemID = gift.ItemID;
		string p = new MiniJSONSerializer().Serialize(giveApply);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("GiveApply: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GiveApply response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GiveApply post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GiveApply request error.", e);
			return false;
		}
	}

	public static bool GetFriendApply(int amount)
	{
		return GetApply(MailType.FRIEND, amount);
	}

	public static bool GetApply(MailType _kind, int amount)
	{
		API _api = API.GetApply;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetApplyEvent != null)
			{
				Server.GetApplyEvent(result, code, _kind, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		MailType mailType = _kind;
		if (mailType == MailType.FRIEND)
		{
			stringBuilder.Append("/get/friend");
		}
		GetMail getMail = new GetMail();
		getMail.UDID = Network.BNIDPublishID;
		getMail.OpenUDID = Network.OpenUDID;
		getMail.UniqueID = Network.BNIDAuthID;
		getMail.MailCount = amount;
		string p = new MiniJSONSerializer().Serialize(getMail);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetApplyEvent != null)
								{
									Server.GetApplyEvent(0, 0, _kind, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetApply response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetApply response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetApply post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetApply request error.", e);
			return false;
		}
	}

	public static bool GotApply(MailType _kind, List<int> _ids)
	{
		API _api = API.GotApply;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GotApplyEvent != null)
			{
				Server.GotApplyEvent(result, code, _kind);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		MailType mailType = _kind;
		if (mailType == MailType.FRIEND)
		{
			stringBuilder.Append("/got/friend");
		}
		GotMail gotMail = new GotMail();
		gotMail.UDID = Network.BNIDPublishID;
		gotMail.OpenUDID = Network.OpenUDID;
		gotMail.UniqueID = Network.BNIDAuthID;
		for (int i = 0; i < _ids.Count; i++)
		{
			gotMail.AddTargets(_ids[i]);
		}
		string p = new MiniJSONSerializer().Serialize(gotMail);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("GotApply: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GotApply response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GotApply post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GotApply request error.", e);
			return false;
		}
	}

	public static bool GetSupportGift(int amount)
	{
		return GetSupport(MailType.SUPPORT, amount);
	}

	public static bool GetSupport(MailType _kind, int amount)
	{
		API _api = API.GetGift;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetSupportEvent != null)
			{
				Server.GetSupportEvent(result, code, _kind, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		MailType mailType = _kind;
		if (mailType == MailType.SUPPORT)
		{
			stringBuilder.Append("/get/support");
		}
		GetMail getMail = new GetMail();
		getMail.UDID = Network.BNIDPublishID;
		getMail.OpenUDID = Network.OpenUDID;
		getMail.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getMail);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetSupportEvent != null)
								{
									Server.GetSupportEvent(0, 0, _kind, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetSupport response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetSupport response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetSupport post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetSupport request error.", e);
			return false;
		}
	}

	public static bool GotSupport(MailType _kind, List<int> _ids)
	{
		API _api = API.GotGift;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GotSupportEvent != null)
			{
				Server.GotSupportEvent(result, code, _kind);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/mail/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		MailType mailType = _kind;
		if (mailType == MailType.SUPPORT)
		{
			stringBuilder.Append("/got/support");
		}
		GotMail gotMail = new GotMail();
		gotMail.UDID = Network.BNIDPublishID;
		gotMail.OpenUDID = Network.OpenUDID;
		gotMail.UniqueID = Network.BNIDAuthID;
		for (int i = 0; i < _ids.Count; i++)
		{
			gotMail.AddTargets(_ids[i]);
		}
		string p = new MiniJSONSerializer().Serialize(gotMail);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("GotSupport: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GotSupport response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GotSupport post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GotSupport request error.", e);
			return false;
		}
	}

	public static bool LoginBonus(string a_request, out string a_guid)
	{
		a_guid = string.Empty;
		API _api = API.LoginBonus;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.LoginBonusEvent != null)
			{
				Server.LoginBonusEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseEventURL("login"));
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/bonus/receive");
		string text = a_request;
		if (string.IsNullOrEmpty(text))
		{
			text = Network.RequestID;
		}
		GetLoginBonus getLoginBonus = new GetLoginBonus();
		getLoginBonus.UDID = Network.BNIDPublishID;
		getLoginBonus.OpenUDID = Network.OpenUDID;
		getLoginBonus.RequestID = text;
		getLoginBonus.UniqueID = Network.BNIDAuthID;
		a_guid = text;
		string p = new MiniJSONSerializer().Serialize(getLoginBonus);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text2 = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(text2))
							{
								SMDLoginBonusData obj = new SMDLoginBonusData();
								new MiniJSONSerializer().Populate(ref obj, text2);
								if (obj != null)
								{
									if (obj.HasLoginData)
									{
										SingletonMonoBehaviour<GameMain>.Instance.mLoginBonusData = obj;
										using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(Constants.LOGINBONUS_BACKUP_FILE, Encoding.UTF8))
										{
											bIJEncryptDataWriter.WriteUTF(text2);
										}
									}
									else
									{
										SingletonMonoBehaviour<GameMain>.Instance.LoadLoginBonusLocalFile();
									}
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("LoginBonus response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("LoginBonus post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("LoginBonus request error.", e);
			return false;
		}
	}

	public static bool LoginBonusDebugClear()
	{
		API _api = API.LoginBonusDebugClear;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.LoginBonusDebugClearEvent != null)
			{
				Server.LoginBonusDebugClearEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseEventURL("login"));
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/bonus/unset");
		GetLoginBonus getLoginBonus = new GetLoginBonus();
		getLoginBonus.UDID = Network.BNIDPublishID;
		getLoginBonus.OpenUDID = Network.OpenUDID;
		getLoginBonus.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getLoginBonus);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string value = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(value))
							{
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("LoginBonusDebugClear response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("LoginBonusDebugClear post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("LoginBonusDebugClear request error.", e);
			return false;
		}
	}

	public static bool LoginBonusDebugNextData()
	{
		API _api = API.LoginBonusDebugGetNextData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.LoginBonusDebugNextDataEvent != null)
			{
				Server.LoginBonusDebugNextDataEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseEventURL("login"));
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/bonus/receive/set");
		GetLoginBonus getLoginBonus = new GetLoginBonus();
		getLoginBonus.UDID = Network.BNIDPublishID;
		getLoginBonus.OpenUDID = Network.OpenUDID;
		getLoginBonus.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getLoginBonus);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string value = streamReader.ReadToEnd();
							if (!string.IsNullOrEmpty(value))
							{
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("LoginBonusDebugClear response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("LoginBonusDebugClear post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("LoginBonusDebugClear request error.", e);
			return false;
		}
	}

	public static bool GetMaintenanceInfo(string a_datekey = "", string a_debugkey = "")
	{
		API _api = API.GetMaintenanceInfo;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetMaintenanceInfoEvent != null)
			{
				Server.GetMaintenanceInfoEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/conf/MaintenanceInfo");
		GetMaintenanceProfile getMaintenanceProfile = new GetMaintenanceProfile();
		getMaintenanceProfile.UDID = Network.BNIDPublishID;
		getMaintenanceProfile.OpenUDID = Network.OpenUDID;
		getMaintenanceProfile.UniqueID = Network.BNIDAuthID;
		if (!string.IsNullOrEmpty(a_datekey))
		{
			getMaintenanceProfile.DateKey = long.Parse(a_datekey);
		}
		if (!string.IsNullOrEmpty(a_debugkey))
		{
			getMaintenanceProfile.DebugKey = a_debugkey;
		}
		else
		{
			getMaintenanceProfile.DebugKey = string.Empty;
		}
		string p = new MiniJSONSerializer().Serialize(getMaintenanceProfile);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): GetMaintenanceInfo={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								_MaintenanceSettingInfo obj = new _MaintenanceSettingInfo();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj.MaintenanceInfo != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mMaintenanceProfile = obj.MaintenanceInfo;
								}
								SingletonMonoBehaviour<GameMain>.Instance.mMaintenanceProfile.ServerTime = obj.time;
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GetMaintenanceInfo response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetMaintenanceInfo post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetMaintenanceInfo request error.", e);
			return false;
		}
	}

	public static bool NicknameCheck()
	{
		API _api = API.NicknameCheck;
		Action<int, int> handler = Server.NicknameCheckEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/get");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		UserGet userGet = new UserGet();
		userGet.UDID = Network.BNIDPublishID;
		userGet.OpenUDID = Network.OpenUDID;
		userGet.UniqueID = Network.BNIDAuthID;
		string p2 = new MiniJSONSerializer().Serialize(userGet);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p2, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					string text = string.Empty;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							text = streamReader.ReadToEnd();
							dbg(string.Format("(#{0}): NicknameCheck={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								UserGetResponse obj = Network.NetworkInterface.UserProfile;
								new MiniJSONSerializer().Populate(ref obj, text);
								dbg("@@@ NicknameCheck: " + obj.ToString());
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("NicknameCheck response error. : " + text, e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("NicknameCheck post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("NicknameCheck request error.", e);
			return false;
		}
	}

	public static bool NicknameEdit(string a_nickname, int a_iconid, int a_iconlvl, int a_series, int a_stage, int a_emoid = 0)
	{
		API _api = API.NicknameEdit;
		Action<int, int> handler = Server.NicknameEditEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/edit");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		UserEdit userEdit = new UserEdit();
		userEdit.UDID = Network.BNIDPublishID;
		userEdit.OpenUDID = Network.OpenUDID;
		userEdit.UniqueID = Network.BNIDAuthID;
		if (!string.IsNullOrEmpty(a_nickname))
		{
			userEdit.NickName = a_nickname;
		}
		userEdit.IconID = a_iconid;
		userEdit.IconLevel = a_iconlvl;
		userEdit.EmoticonID = a_emoid;
		string p2 = new MiniJSONSerializer().Serialize(userEdit);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p2, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate
				{
					if (handler != null)
					{
						handler(0, 0);
					}
				}, null))
				{
					BIJLog.E("GetEntry post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetEntry request error.", e);
			return false;
		}
	}

	public static bool GetSegmentInfo(string a_datekey = "", string a_debugkey = "")
	{
		API _api = API.GetSegmentInfo;
		Action<int, int> handler = Server.GetSegmentInfoEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/segment");
		UserGet userGet = new UserGet();
		userGet.UDID = Network.BNIDPublishID;
		userGet.OpenUDID = Network.OpenUDID;
		userGet.UniqueID = Network.BNIDAuthID;
		if (!string.IsNullOrEmpty(a_datekey))
		{
			userGet.DateKey = long.Parse(a_datekey);
		}
		if (!string.IsNullOrEmpty(a_debugkey))
		{
			userGet.DebugKey = a_debugkey;
		}
		else
		{
			userGet.DebugKey = string.Empty;
		}
		string p = new MiniJSONSerializer().Serialize(userGet);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							string text = string.Empty;
							if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
							{
								byte[] responseByte = Instance.GetResponseByte(stream);
								text = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
							}
							else
							{
								using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
								{
									text = streamReader.ReadToEnd();
								}
							}
							dbg(string.Format("(#{0}): GetSegmentInfo={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								_SegmentSettingInfo obj = new _SegmentSettingInfo();
								new MiniJSONSerializer().Populate(ref obj, text);
								SingletonMonoBehaviour<GameMain>.Instance.mSegmentProfile = obj.segmentinfo.Clone();
								if (SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile == null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mSegmentProfile = new SegmentProfile();
								}
								string mServerSegmentProfileDirectory = string.Empty;
								if (!string.IsNullOrEmpty(obj.dir))
								{
									mServerSegmentProfileDirectory = obj.dir;
								}
								GameMain.mServerSegmentProfileDirectory = mServerSegmentProfileDirectory;
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GetSegmentInfo response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetSegmentInfo post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetSegmentInfo request error.", e);
			return false;
		}
	}

	public static bool SetPurchase(IPurchaseInfo info)
	{
		return SetPurchase(info, 0L, false);
	}

	public static bool SetPurchase(IPurchaseInfo info, long time)
	{
		return SetPurchase(info, time, false);
	}

	public static bool SetPurchase(IPurchaseInfo info, long time, bool restore)
	{
		API _api = API.SetPurchase;
		Action<int, int> handler = Server.SetPurchaseEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(VipURL);
		stringBuilder.Append("setiap_index.php");
		Dictionary<string, object> p = new Dictionary<string, object>();
		string deviceModel = Network.DeviceModel;
		string operatingSystem = SystemInfo.operatingSystem;
		int systemMemorySize = SystemInfo.systemMemorySize;
		int internetConnectionMode = Network.InternetConnectionMode;
		string graphicsDeviceSpec = Network.GraphicsDeviceSpec;
		int level = SingletonMonoBehaviour<Network>.Instance.Level;
		int masterCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		int secondaryCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		int experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		p["udid"] = Network.UniqueDeviceIdentifier;
		p["appname"] = SingletonMonoBehaviour<Network>.Instance.AppName;
		p["appver"] = SingletonMonoBehaviour<Network>.Instance.AppVer;
		p["cn"] = SingletonMonoBehaviour<Network>.Instance.COUNTRY_CODE;
		p["hiveid"] = SingletonMonoBehaviour<Network>.Instance.HiveId;
		p["lvl"] = string.Empty + level;
		p["mc"] = string.Empty + masterCurrency;
		p["sc"] = string.Empty + secondaryCurrency;
		p["xp"] = string.Empty + experience;
		p["fbid"] = SingletonMonoBehaviour<Network>.Instance.Fbid;
		p["gcid"] = SingletonMonoBehaviour<Network>.Instance.Gcid;
		p["gpid"] = SingletonMonoBehaviour<Network>.Instance.Gpid;
		p["iapid"] = info.IAPNo;
		p["po"] = info.OrderID;
		p["time"] = string.Empty + info.PurchasedTimeStamp;
		p["pt"] = string.Empty + time;
		p["rt"] = string.Empty + (restore ? 1 : 99);
		p["device"] = deviceModel;
		p["osver"] = operatingSystem;
		p["mem"] = string.Empty + systemMemorySize;
		p["gdv"] = graphicsDeviceSpec;
		p["ic"] = internetConnectionMode;
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsync(stringBuilder.ToString(), ref p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("SetPurchase: " + text);
							arg = (("1".CompareTo(text) != 0) ? 1 : 0);
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("SetPurchase response error.", e2);
						arg = 1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, 0);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("SetPurchase post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("SetPurchase request error.", e);
			return false;
		}
	}

	public static bool SaveJournal(IReceiptData receipt)
	{
		return SaveJournal(receipt, 0L);
	}

	public static bool SaveJournal(IReceiptData receipt, long purchaseCount)
	{
		API _api = API.SaveJournal;
		Action<int, int> handler = Server.SaveJournalEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("setJournal.php");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		string deviceModel = Network.DeviceModel;
		string operatingSystem = SystemInfo.operatingSystem;
		int systemMemorySize = SystemInfo.systemMemorySize;
		int internetConnectionMode = Network.InternetConnectionMode;
		string graphicsDeviceSpec = Network.GraphicsDeviceSpec;
		int level = SingletonMonoBehaviour<Network>.Instance.Level;
		int masterCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		int secondaryCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		int experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		p["lvl"] = string.Empty + level;
		p["mc"] = string.Empty + masterCurrency;
		p["sc"] = string.Empty + secondaryCurrency;
		p["xp"] = string.Empty + experience;
		p["pcnt"] = string.Empty + purchaseCount;
		p["sig"] = receipt.ReceiptSignature;
		p["iapid"] = receipt.IAPNo;
		bool result = false;
		try
		{
			using (BIJBinaryReader value = new BIJMemoryBinaryReader(receipt.GetPurchase(), "purchase.dat"))
			{
				using (BIJBinaryReader value2 = new BIJMemoryBinaryReader(receipt.GetReceipt(), "receipt.dat"))
				{
					p["dat"] = value;
					p["mdat"] = value2;
					result = Network.CreateHttpMultiPartAsync(stringBuilder.ToString(), ref p, Timeout, delegate(HttpAsync httpAsync)
					{
						if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
						{
							int arg = 1;
							HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
							try
							{
								using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
								{
									string text = streamReader.ReadToEnd();
									log("SaveJournal: " + text);
									arg = (("1".CompareTo(text) != 0) ? 1 : 0);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("SaveJournal response error.", e2);
								arg = 1;
							}
							finally
							{
								if (handler != null)
								{
									handler(arg, 0);
								}
								if (httpWebResponse != null)
								{
									httpWebResponse.Close();
								}
							}
						}, null))
						{
							BIJLog.E("SaveJournal post error.");
							if (handler != null)
							{
								handler(1, -1);
							}
						}
					});
				}
			}
		}
		catch (Exception e)
		{
			BIJLog.E("SaveJournal request error.", e);
			result = false;
		}
		return result;
	}

	public static bool ChargePriceCheck(long _past_seconds)
	{
		API _api = API.ChargePriceCheck;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.ChargePriceCheckEvent != null)
			{
				Server.ChargePriceCheckEvent(result, code);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(ServerIAP.BaseURL);
		stringBuilder.Append("/currency/charge/price/check");
		ChargePriceCheckProfile chargePriceCheckProfile = new ChargePriceCheckProfile();
		chargePriceCheckProfile.HiveID = SingletonMonoBehaviour<Network>.Instance.HiveId;
		chargePriceCheckProfile.UUID = SingletonMonoBehaviour<Network>.Instance.UUID;
		chargePriceCheckProfile.UDID = Network.BNIDPublishID;
		chargePriceCheckProfile.UniqueID = Network.BNIDAuthID;
		chargePriceCheckProfile.PastSeconds = _past_seconds;
		string p = new MiniJSONSerializer().Serialize(chargePriceCheckProfile);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							dbg(string.Format("(#{0}): GetGameInfo={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								ChargePriceCheckResponseProfile obj = new ChargePriceCheckResponseProfile();
								new MiniJSONSerializer().Populate(ref obj, text);
								if (obj != null)
								{
									SingletonMonoBehaviour<GameMain>.Instance.mChargePriceCheckData = obj;
								}
								arg = 0;
								arg2 = 0;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("ChargePriceCheck response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("ChargePriceCheck post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, true);
		}
		catch (Exception e)
		{
			BIJLog.E("ChargePriceCheck request error.", e);
			return false;
		}
	}

	public static bool GetServerSaveDataOfOthers(string search_id)
	{
		API _api = API.GetSaveDataOfOthers;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetServerSaveDataOfOthersEvent != null)
			{
				Server.GetServerSaveDataOfOthersEvent(result, code, null, 0);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/debuguser/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/getgamedata");
		GetSaveDataOfOthersProfile getSaveDataOfOthersProfile = new GetSaveDataOfOthersProfile();
		getSaveDataOfOthersProfile.UDID = Network.BNIDPublishID;
		getSaveDataOfOthersProfile.OpenUDID = Network.OpenUDID;
		getSaveDataOfOthersProfile.SearchID = search_id;
		string p = new MiniJSONSerializer().Serialize(getSaveDataOfOthersProfile);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					string responseHeader = httpWebResponse.GetResponseHeader("X-BIJ-DATVER");
					int arg = 0;
					if (!string.IsNullOrEmpty(responseHeader))
					{
						arg = int.Parse(responseHeader);
					}
					try
					{
						using (Stream arg2 = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetServerSaveDataOfOthersEvent != null)
								{
									Server.GetServerSaveDataOfOthersEvent(0, 0, arg2, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetServerSaveDataOfOthers response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetServerSaveDataOfOthers response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetServerSaveDataOfOthers post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetServerSaveDataOfOthers request error.", e);
			return false;
		}
	}

	public static bool GetServerSaveDataOfOthersCX(string search_id)
	{
		API _api = API.GetSaveDataOfOthers;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetServerSaveDataOfOthersEventCX != null)
			{
				Server.GetServerSaveDataOfOthersEventCX(result, code, null, 0);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(AdvBaseURL);
		stringBuilder.Append("/debug/user/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/gamedata/get");
		GetSaveDataOfOthersProfileCX getSaveDataOfOthersProfileCX = new GetSaveDataOfOthersProfileCX();
		getSaveDataOfOthersProfileCX.UDID = Network.BNIDPublishID;
		getSaveDataOfOthersProfileCX.OpenUDID = Network.OpenUDID;
		getSaveDataOfOthersProfileCX.SearchID = search_id;
		string p = new MiniJSONSerializer().Serialize(getSaveDataOfOthersProfileCX);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					string responseHeader = httpWebResponse.GetResponseHeader("X-BIJ-DATVER");
					int arg = 0;
					if (!string.IsNullOrEmpty(responseHeader))
					{
						arg = int.Parse(responseHeader);
					}
					try
					{
						using (Stream arg2 = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetServerSaveDataOfOthersEventCX != null)
								{
									Server.GetServerSaveDataOfOthersEventCX(0, 0, arg2, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetServerSaveDataOfOthersCX response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetServerSaveDataOfOthersCX response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetServerSaveDataOfOthersCX post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetServerSaveDataOfOthersCX request error.", e);
			return false;
		}
	}

	public static bool SetStageClearData(int series, int _stage, int hiveId, int uuid, string dispname, IStageInfo _info, bool ForceOverwrite = false)
	{
		API _api = API.SetStageClearData;
		Action<int, int> handler = Server.SetStageClearDataEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/stage/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/");
		stringBuilder.Append(series);
		stringBuilder.Append("/");
		stringBuilder.Append(_stage);
		stringBuilder.Append("/score");
		if (ForceOverwrite)
		{
			stringBuilder.Append("?FORCE=1");
		}
		SetStageClearData setStageClearData = new SetStageClearData();
		setStageClearData.UDID = Network.BNIDPublishID;
		setStageClearData.OpenUDID = Network.OpenUDID;
		setStageClearData.UniqueID = Network.BNIDAuthID;
		setStageClearData.ClearScore = _info.ClearScore;
		setStageClearData.ClearTime = _info.ClearTime;
		string p = new MiniJSONSerializer().Serialize(setStageClearData);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("SetStageClearData: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("SetStageClearData response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("SetStageClearData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("SetStageClearData request error.", e);
			return false;
		}
	}

	public static bool GetStageRankingData(int a_series, int _stage)
	{
		API _api = API.GetStageRankingData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetStageRankingDataEvent != null)
			{
				Server.GetStageRankingDataEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/stage/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/");
		stringBuilder.Append(a_series);
		stringBuilder.Append("/");
		stringBuilder.Append(_stage);
		stringBuilder.Append("/ranking/get");
		GetStageRanking getStageRanking = new GetStageRanking();
		getStageRanking.UDID = Network.BNIDPublishID;
		getStageRanking.OpenUDID = Network.OpenUDID;
		getStageRanking.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getStageRanking);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							try
							{
								string arg = string.Empty;
								if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
								{
									byte[] responseByte = Instance.GetResponseByte(stream);
									arg = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
								}
								else
								{
									using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
									{
										arg = streamReader.ReadToEnd();
									}
								}
								if (Server.GetStageRankingDataEvent != null)
								{
									Server.GetStageRankingDataEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetStageRankingData response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetStageRankingData response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetStageRankingData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetStageRankingData request error.", e);
			return false;
		}
	}

	public static bool GetStageMetaData(int _stage, string hiveId, string uuid)
	{
		API _api = API.GetStageMetaData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetStageMetaDataEvent != null)
			{
				Server.GetStageMetaDataEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("getStgMetadata.php");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		p["fhiveid"] = hiveId;
		p["fuuid"] = uuid;
		p["stg"] = _stage;
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsync(stringBuilder.ToString(), ref p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetStageMetaDataEvent != null)
								{
									Server.GetStageMetaDataEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetStageMetaData response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetStageMetaData response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetStageMetaData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetStageMetaData request error.", e);
			return false;
		}
	}

	public static bool SetEventStageClearData(int _stage, int hiveId, int uuid, string dispname, IStageInfo _info, int eventID, short courseID)
	{
		API _api = API.SetEventStageClearData;
		Action<int, int> handler = Server.SetEventStageClearDataEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(_stage, out a_main, out a_sub);
		int stageNo = Def.GetStageNo(courseID, a_main, a_sub);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/event/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/");
		stringBuilder.Append(eventID);
		stringBuilder.Append("/");
		stringBuilder.Append(stageNo);
		stringBuilder.Append("/score");
		SetStageClearData setStageClearData = new SetStageClearData();
		setStageClearData.UDID = Network.BNIDPublishID;
		setStageClearData.OpenUDID = Network.OpenUDID;
		setStageClearData.UniqueID = Network.BNIDAuthID;
		setStageClearData.ClearScore = _info.ClearScore;
		setStageClearData.ClearTime = _info.ClearTime;
		string p = new MiniJSONSerializer().Serialize(setStageClearData);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					int arg = 1;
					int arg2 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("SetStageClearData: " + text);
							arg = 0;
							arg2 = 0;
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("SetStageClearData response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("SetStageClearData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("SetStageClearData request error.", e);
			return false;
		}
	}

	public static bool GetEventStageRankingData(int _series, int _stage, int _eventID, int _courseID)
	{
		API _api = API.GetEventStageRankingData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetEventStageRankingDataEvent != null)
			{
				Server.GetEventStageRankingDataEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(_stage, out a_main, out a_sub);
		int stageNo = Def.GetStageNo(_courseID, a_main, a_sub);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/event/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/");
		stringBuilder.Append(_eventID);
		stringBuilder.Append("/");
		stringBuilder.Append(stageNo);
		stringBuilder.Append("/ranking/get");
		GetStageRanking getStageRanking = new GetStageRanking();
		getStageRanking.UDID = Network.BNIDPublishID;
		getStageRanking.OpenUDID = Network.OpenUDID;
		getStageRanking.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getStageRanking);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream stream = httpWebResponse.GetResponseStream())
						{
							try
							{
								string arg = string.Empty;
								if (Instance.CheckResponseGZIPData(httpWebResponse.Headers))
								{
									byte[] responseByte = Instance.GetResponseByte(stream);
									arg = Encoding.UTF8.GetString(GZipUtil.UncompressBuffer(responseByte));
								}
								else
								{
									using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
									{
										arg = streamReader.ReadToEnd();
									}
								}
								if (Server.GetEventStageRankingDataEvent != null)
								{
									Server.GetEventStageRankingDataEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetStageRankingData response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetStageRankingData response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetStageRankingData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			}, false, false, true);
		}
		catch (Exception e)
		{
			BIJLog.E("GetStageRankingData request error.", e);
			return false;
		}
	}

	public static bool GetEventStageMetaData(int _stage, string hiveId, string uuid)
	{
		API _api = API.GetEventStageMetaData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetEventStageMetaDataEvent != null)
			{
				Server.GetEventStageMetaDataEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("getEvtStgMetadata.php");
		Dictionary<string, object> p = new Dictionary<string, object>();
		Network.GetCommonParams(ref p);
		p["fhiveid"] = hiveId;
		p["fuuid"] = uuid;
		p["estg"] = _stage;
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsync(stringBuilder.ToString(), ref p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetEventStageMetaDataEvent != null)
								{
									Server.GetEventStageMetaDataEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetEventStageMetaData response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetEventStageMetaData response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetEventStageMetaData post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetEventStageMetaData request error.", e);
			return false;
		}
	}

	public static bool SaveTransferGameState(BIJBinaryReader dataReader, BIJBinaryReader metaReader, Dictionary<string, object> overwrites, int transMode)
	{
		API _api = API.SaveGameData;
		Action<int, int> handler = Server.SaveTransferGameStateEvent;
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/gamedata/transfer/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		Dictionary<string, BIJBinaryReader> octets = new Dictionary<string, BIJBinaryReader>();
		if (dataReader != null)
		{
			octets["data"] = dataReader;
		}
		if (metaReader != null)
		{
			octets["metadata"] = metaReader;
		}
		string p = MakeSaveDataParameter();
		bool flag = false;
		try
		{
			return Network.CreateHttpMultiPartAsyncJson(stringBuilder.ToString(), p, ref octets, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					int arg = 0;
					int arg2 = 0;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							log("SaveTransferGameState: " + text);
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("SaveTransferGameState response error.", e2);
						arg = 1;
						arg2 = -1;
					}
					finally
					{
						if (handler != null)
						{
							handler(arg, arg2);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("SaveTransferGameState post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("SaveTransferGameState request error.", e);
			return false;
		}
	}

	public static bool GetTransferGameState()
	{
		API _api = API.GetGameData;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetTransferGameStateEvent != null)
			{
				Server.GetTransferGameStateEvent(result, code, null, 0);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/gamedata/transfer/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/get");
		GetSaveData getSaveData = new GetSaveData();
		getSaveData.UDID = Network.BNIDPublishID;
		getSaveData.OpenUDID = Network.OpenUDID;
		getSaveData.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getSaveData);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					string responseHeader = httpWebResponse.GetResponseHeader("X-BIJ-DATVER");
					int arg = 0;
					if (!string.IsNullOrEmpty(responseHeader))
					{
						arg = int.Parse(responseHeader);
					}
					try
					{
						using (Stream arg2 = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetTransferGameStateEvent != null)
								{
									Server.GetTransferGameStateEvent(0, 0, arg2, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetTransferGameState response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetTransferGameState response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetTransferGameState post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetTransferGameState request error.", e);
			return false;
		}
	}

	public static bool GetTransferParam()
	{
		API _api = API.GetTransferParam;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetTransferParamEvent != null)
			{
				Server.GetTransferParamEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/gamedata/transfer/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/param/get");
		GetSaveDataParam getSaveDataParam = new GetSaveDataParam();
		getSaveDataParam.UDID = Network.BNIDPublishID;
		getSaveDataParam.OpenUDID = Network.OpenUDID;
		getSaveDataParam.UniqueID = Network.BNIDAuthID;
		string p = new MiniJSONSerializer().Serialize(getSaveDataParam);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.GetTransferParamEvent != null)
								{
									Server.GetTransferParamEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("GetTransferParam response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("GetTransferParam response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetTransferParam post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetTransferParam request error.", e);
			return false;
		}
	}

	public static bool TransferAuth()
	{
		API _api = API.UserAuth;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.TransferAuthEvent != null)
			{
				Server.TransferAuthEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/user/auth");
		string p = MakeUserAuthParameter();
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (Stream arg = httpWebResponse.GetResponseStream())
						{
							try
							{
								if (Server.TransferAuthEvent != null)
								{
									Server.TransferAuthEvent(0, 0, arg);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("AuthTransferEvent response handling error.", e2);
							}
						}
					}
					catch (Exception e3)
					{
						BIJLog.E("AuthTransferEvent response error.", e3);
						if (handler != null)
						{
							handler(1, -1);
						}
					}
					finally
					{
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("AuthTransferEvent post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("AuthTransferEvent request error.", e);
			return false;
		}
	}

	public static bool LogTransfer(TransferLogStateKind state, int transMode)
	{
		return true;
	}

	public static bool GetWallPaper(string imageNum)
	{
		API _api = API.GetWallPaper;
		Action<int, int> handler = delegate(int result, int code)
		{
			if (Server.GetWallPaperEvent != null)
			{
				Server.GetWallPaperEvent(result, code, null);
			}
		};
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append("/wallpaper/");
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.UUID);
		stringBuilder.Append("/");
		stringBuilder.Append(imageNum);
		stringBuilder.Append("/url");
		GetWallPaper getWallPaper = new GetWallPaper();
		getWallPaper.UDID = Network.BNIDPublishID;
		getWallPaper.OpenUDID = Network.OpenUDID;
		getWallPaper.UniqueID = Network.BNIDAuthID;
		int num = Screen.width;
		int num2 = Screen.height;
		ScreenOrientation screenOrientation = Util.ScreenOrientation;
		if (screenOrientation == ScreenOrientation.LandscapeLeft || screenOrientation == ScreenOrientation.LandscapeLeft || screenOrientation == ScreenOrientation.LandscapeRight)
		{
			int num3 = num;
			num = num2;
			num2 = num3;
		}
		getWallPaper.Width = num;
		getWallPaper.Height = num2;
		getWallPaper.Model = SystemInfo.deviceModel;
		string p = new MiniJSONSerializer().Serialize(getWallPaper);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, handler, delegate(HttpAsync _httpAsync)
				{
					string arg = null;
					int arg2 = 1;
					int arg3 = -1;
					HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
					try
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string text = streamReader.ReadToEnd();
							dbg(string.Format("(#{0}): GetWallPaper={1}", _httpAsync.SeqNo, text));
							if (!string.IsNullOrEmpty(text))
							{
								GetWallPaperResponse obj = new GetWallPaperResponse();
								new MiniJSONSerializer().Populate(ref obj, text);
								arg2 = 0;
								arg3 = 0;
								arg = obj.URL;
							}
						}
					}
					catch (Exception e2)
					{
						BIJLog.E("GetWallPaper response error.", e2);
						arg2 = 1;
						arg3 = -1;
						arg = null;
					}
					finally
					{
						if (Server.GetWallPaperEvent != null)
						{
							Server.GetWallPaperEvent(arg2, arg3, arg);
						}
						if (httpWebResponse != null)
						{
							httpWebResponse.Close();
						}
					}
				}, null))
				{
					BIJLog.E("GetWallPaper post error.");
					if (handler != null)
					{
						handler(1, -1);
					}
				}
			});
		}
		catch (Exception e)
		{
			BIJLog.E("GetWallPaper request error.", e);
			return false;
		}
	}
}
