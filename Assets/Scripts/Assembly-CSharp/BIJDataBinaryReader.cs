using System;
using System.IO;
using System.Text;

public class BIJDataBinaryReader : BIJBinaryReader
{
	private FileStream mStream;

	public BIJDataBinaryReader(string name)
		: this(name, Encoding.UTF8)
	{
	}

	public BIJDataBinaryReader(string name, Encoding encoding)
	{
		mStream = null;
		Reader = null;
		try
		{
			mPath = System.IO.Path.Combine(GetBasePath(), name);
			mStream = new FileStream(mPath, FileMode.Open);
			Reader = new BinaryReader(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	internal static string GetBasePath()
	{
		return BIJUnity.getDataPath();
	}

	public override void Close()
	{
		base.Close();
		try
		{
			if (mStream != null)
			{
				mStream.Close();
			}
		}
		catch (Exception)
		{
		}
		mStream = null;
	}
}
