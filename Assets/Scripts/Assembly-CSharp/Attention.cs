using System;
using System.Collections;
using UnityEngine;

public class Attention : MonoBehaviour
{
	private UISprite mSprite;

	private GameObject mSpriteGO;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void Init(float scale, int depth, Vector3 pos, BIJImage atlas, string spriteKey)
	{
		mSprite = Util.CreateSprite("Attention", base.gameObject, atlas);
		Util.SetSpriteInfo(mSprite, spriteKey, depth, Vector3.zero, new Vector3(scale, scale, 1f), false, UIWidget.Pivot.Bottom);
		StartCoroutine(UpdateAttention());
		base.transform.localPosition = pos;
		mSpriteGO = mSprite.gameObject;
	}

	protected IEnumerator UpdateAttention()
	{
		float angle3 = 0f;
		while (true)
		{
			angle3 = 0f;
			while (angle3 < 180f)
			{
				angle3 += Time.deltaTime * 450f;
				if (angle3 > 180f)
				{
					angle3 = 180f;
				}
				float y = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * 30f;
				mSprite.transform.localPosition = new Vector3(0f, y, 0f);
				yield return 0;
			}
			angle3 = 0f;
			while (angle3 < 180f)
			{
				angle3 += Time.deltaTime * 810f;
				if (angle3 > 180f)
				{
					angle3 = 180f;
				}
				float y2 = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * 10f;
				mSprite.transform.localPosition = new Vector3(0f, y2, 0f);
				yield return 0;
			}
			yield return new WaitForSeconds(1.5f);
		}
	}

	public void SetAttentionEnable(bool enable)
	{
		if (mSprite != null)
		{
			NGUITools.SetActive(mSpriteGO, enable);
		}
	}

	public bool GetAttentionActiveSelf()
	{
		bool result = false;
		if (mSprite != null)
		{
			result = mSpriteGO.activeSelf;
		}
		return result;
	}
}
