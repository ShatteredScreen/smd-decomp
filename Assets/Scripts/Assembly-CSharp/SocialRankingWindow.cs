using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SocialRankingWindow : MonoBehaviour
{
	public enum STATE
	{
		START = 0,
		PREOPEN = 1,
		OPEN = 2,
		INIT = 3,
		MAIN = 4,
		WAIT = 5,
		CLOSE = 6,
		END = 7
	}

	public enum CALLBACK
	{
		OnClosed = 0
	}

	private class _StageWonInfo : IStageInfo
	{
		public int ClearScore { get; set; }

		public int ClearTime { get; set; }

		public byte[] Metadata { get; set; }

		public byte[] GetMetaData()
		{
			return Metadata;
		}
	}

	public delegate void OnLoginPushed();

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.START);

	private GameMain mGame;

	private UIPanel mSelfPanel;

	private int mBaseDepth = 50;

	private float mProgression;

	private bool mDestroyFlg;

	private UISprite mTitleSprite;

	private UISprite mWindowSprite;

	private int mSeries;

	private int mStageNo;

	private bool mScreen = true;

	private bool mWinLose = true;

	private OnLoginPushed mLoginCallback;

	private SMVerticalListInfo mStageRankingListInfo;

	private SMVerticalList mStageRankingList;

	private List<SMVerticalListItem> list = new List<SMVerticalListItem>();

	private int mMyItemIndex = -1;

	private GameObject mMyObject;

	private GameObject mFriendObject;

	private RandomFriendDialog mRandomDialog;

	private UIButton mAddButton;

	private float mStateTime;

	private Dictionary<CALLBACK, UnityEvent> mCallback;

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mSelfPanel = base.gameObject.AddComponent<UIPanel>();
	}

	private void Start()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		if (mScreen)
		{
			base.gameObject.transform.localPosition = new Vector3(0f, -240f, 0f);
		}
		else if (!mScreen)
		{
			base.gameObject.transform.localPosition = new Vector3(240f, 0f, 0f);
		}
		mProgression = 0f;
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.START:
			if (mStageRankingList != null && mStageRankingList.IsContentsCreated)
			{
				if (mScreen)
				{
					mStageRankingList.SetScrollBarPosition(new Vector3(0f, -80f, 0f));
				}
				else
				{
					mStageRankingList.SetScrollBarPosition(new Vector3(81f, 0f, 0f));
				}
				mStateTime = 0f;
				mState.Change(STATE.PREOPEN);
			}
			break;
		case STATE.PREOPEN:
			mStateTime += Time.deltaTime;
			if (mStateTime > 0.3f)
			{
				mStateTime = 0.3f;
				mState.Change(STATE.OPEN);
			}
			break;
		case STATE.OPEN:
			if (mMyObject != null)
			{
				if (list.Count > 4)
				{
					mStageRankingList.CenterOn(mMyObject);
				}
			}
			else if (list.Count > 4)
			{
				mStageRankingList.CenterOn(mFriendObject);
			}
			mProgression += Time.deltaTime * 360f;
			if (mProgression >= 90f)
			{
				mProgression = 90f;
				mState.Change(STATE.INIT);
			}
			if (mScreen)
			{
				base.gameObject.transform.localPosition = new Vector3(0f, -240f * (1f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))), 0f);
			}
			else
			{
				base.gameObject.transform.localPosition = new Vector3(240f * (1f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))), 0f, 0f);
			}
			break;
		case STATE.INIT:
			mState.Change(STATE.WAIT);
			break;
		case STATE.WAIT:
			if (mDestroyFlg)
			{
				mState.Change(STATE.CLOSE);
			}
			break;
		case STATE.CLOSE:
			mProgression -= Time.deltaTime * 360f;
			if (mProgression <= 0f)
			{
				mProgression = 0f;
				mState.Change(STATE.END);
				InvokeCallback(CALLBACK.OnClosed);
			}
			if (mScreen)
			{
				base.gameObject.transform.localPosition = new Vector3(0f, -240f * (1f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))), 0f);
			}
			else
			{
				base.gameObject.transform.localPosition = new Vector3(240f * (1f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))), 0f, 0f);
			}
			break;
		case STATE.END:
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		mState.Update();
	}

	public void Init(int a_series, int a_stage, List<StageRankingData> ranking, bool a_add, bool screen = true, bool winlose = true)
	{
		mScreen = screen;
		mSeries = a_series;
		mStageNo = a_stage;
		mWinLose = winlose;
		Vector2 vector = Util.LogScreenSize();
		float num = Mathf.Min(vector.x, 950f);
		float y = vector.y;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int num2 = 0;
		int num3 = 0;
		PlayerClearData _psd;
		if (mGame.mEventMode)
		{
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, out data);
			short a_course = data.CourseID;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(mStageNo, out a_course, out a_main, out a_sub);
			int stageNo = Def.GetStageNo(a_main, a_sub);
			if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, a_course, stageNo, out _psd))
			{
				num2 = _psd.HightScore;
				num3 = _psd.GotStars;
			}
		}
		else if (mGame.mPlayer.GetStageClearData((Def.SERIES)mSeries, mStageNo, out _psd))
		{
			num2 = _psd.HightScore;
			num3 = _psd.GotStars;
		}
		if (screen)
		{
			FinishDialog finishDialog = UnityEngine.Object.FindObjectOfType<FinishDialog>();
			list = new List<SMVerticalListItem>();
			for (int i = 0; i < ranking.Count; i++)
			{
				list.Add(ranking[i]);
			}
			if (list.Count != 0)
			{
				mWindowSprite = Util.CreateSprite("Window", base.gameObject, image);
				Util.SetSpriteInfo(mWindowSprite, "bg00", mBaseDepth, Vector3.zero, Vector3.one, false);
				mWindowSprite.pivot = UIWidget.Pivot.Bottom;
				mWindowSprite.type = UIBasicSprite.Type.Tiled;
				mWindowSprite.gameObject.transform.localPosition = Vector3.zero;
				mWindowSprite.SetDimensions((int)num + 1, 180);
				mTitleSprite = Util.CreateSprite("SociaplTitle", mWindowSprite.gameObject, image);
				Util.SetSpriteInfo(mTitleSprite, "ranking_line", mBaseDepth, Vector3.zero, Vector3.one, false);
				mTitleSprite.pivot = UIWidget.Pivot.Center;
				mTitleSprite.type = UIBasicSprite.Type.Sliced;
				mTitleSprite.gameObject.transform.localPosition = new Vector3(0f, 200f, 0f);
				mTitleSprite.SetDimensions((int)num + 1, 40);
				float posX = 0f;
				if (a_add)
				{
				}
				UILabel uILabel = Util.CreateLabel("RankingTitle", mTitleSprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, Localization.Get("Ranking_Title"), mBaseDepth + 3, new Vector3(-240f, -1f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = new Color(21f / 85f, 0.18039216f, 0.4f, 1f);
				if (mMyItemIndex < 3)
				{
					UISprite sprite = Util.CreateSprite("Crown", mTitleSprite.gameObject, image);
					Util.SetSpriteInfo(sprite, string.Format("ranking{0}_00", mMyItemIndex + 1), mBaseDepth + 2, new Vector3(-85f, 0f, 0f), Vector3.one, false);
				}
				else
				{
					UISprite sprite = Util.CreateSprite("Crown", mTitleSprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "ranking_crown", mBaseDepth + 2, new Vector3(-85f, 0f, 0f), Vector3.one, false);
					uILabel = Util.CreateLabel("RankingNum", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, string.Empty + (mMyItemIndex + 1), mBaseDepth + 4, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
					uILabel.color = new Color(21f / 85f, 0.18039216f, 0.4f, 1f);
				}
				uILabel = Util.CreateLabel("Hiscore", mTitleSprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, Localization.Get("MyHiScore"), mBaseDepth + 4, new Vector3(50f, -1f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = new Color(21f / 85f, 0.18039216f, 0.4f, 1f);
				uILabel = Util.CreateLabel("HiscoreNum", mTitleSprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, string.Empty + num2, mBaseDepth + 4, new Vector3(300f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Right);
				uILabel.color = new Color(21f / 85f, 0.18039216f, 0.4f, 1f);
				mStageRankingListInfo = new SMVerticalListInfo(1, num, 170f, 1, num, 170f, 1, 170);
				mStageRankingList = SMVerticalList.Make(mWindowSprite.gameObject, this, mStageRankingListInfo, list, posX, 90f, mSelfPanel.sortingOrder + 1, false, UIScrollView.Movement.Horizontal);
				mStageRankingList.OnCreateItem = OnCreateItem;
			}
			return;
		}
		list = new List<SMVerticalListItem>();
		FinishDialog finishDialog2 = UnityEngine.Object.FindObjectOfType<FinishDialog>();
		for (int j = 0; j < ranking.Count; j++)
		{
			list.Add(ranking[j]);
		}
		if (list.Count != 0)
		{
			mWindowSprite = Util.CreateSprite("Window", base.gameObject, image);
			Util.SetSpriteInfo(mWindowSprite, "bg00", mBaseDepth, Vector3.zero, Vector3.one, false);
			mWindowSprite.pivot = UIWidget.Pivot.Right;
			mWindowSprite.type = UIBasicSprite.Type.Tiled;
			mWindowSprite.SetDimensions(205, (int)y + 1);
			mWindowSprite.gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
			mTitleSprite = Util.CreateSprite("SociaplTitle", mWindowSprite.gameObject, image);
			Util.SetSpriteInfo(mTitleSprite, "ranking_line", mBaseDepth, Vector3.zero, Vector3.one, false);
			mTitleSprite.pivot = UIWidget.Pivot.Right;
			mTitleSprite.type = UIBasicSprite.Type.Tiled;
			mTitleSprite.SetDimensions(40, (int)y + 1);
			mTitleSprite.gameObject.transform.localPosition = new Vector3(-166f, 0f, 0f);
			float posY = 0f;
			if (a_add)
			{
			}
			UILabel uILabel2 = Util.CreateLabel("RankingTitle", mTitleSprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel2, Localization.Get("Ranking_Title"), mBaseDepth + 3, new Vector3(-20f, 223f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel2.color = new Color(21f / 85f, 0.18039216f, 0.4f, 1f);
			uILabel2.transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
			if (mMyItemIndex < 3)
			{
				UISprite sprite = Util.CreateSprite("Crown", mTitleSprite.gameObject, image);
				Util.SetSpriteInfo(sprite, string.Format("ranking{0}_00", mMyItemIndex + 1), mBaseDepth + 2, new Vector3(-17f, 104f, 0f), Vector3.one, false);
				sprite.transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
			}
			else
			{
				UISprite sprite = Util.CreateSprite("Crown", mTitleSprite.gameObject, image);
				Util.SetSpriteInfo(sprite, "ranking_crown", mBaseDepth + 2, new Vector3(-17f, 104f, 0f), Vector3.one, false);
				sprite.transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
				uILabel2 = Util.CreateLabel("RankingNum", sprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel2, string.Empty + (mMyItemIndex + 1), mBaseDepth + 4, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
				uILabel2.color = Color.white;
				uILabel2.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			}
			uILabel2 = Util.CreateLabel("Hiscore", mTitleSprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel2, Localization.Get("MyHiScore"), mBaseDepth + 4, new Vector3(-20f, -6f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
			uILabel2.color = new Color(21f / 85f, 0.18039216f, 0.4f, 1f);
			uILabel2.transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
			uILabel2 = Util.CreateLabel("HiscoreNum", mTitleSprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel2, string.Empty + num2, mBaseDepth + 4, new Vector3(-9.5f, -199f, 0f), 26, 0, 0, UIWidget.Pivot.Top);
			uILabel2.color = new Color(21f / 85f, 0.18039216f, 0.4f, 1f);
			uILabel2.transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
			mStageRankingListInfo = new SMVerticalListInfo(1, 170f, y, 1, 170f, y, 1, 170);
			mStageRankingList = SMVerticalList.Make(mWindowSprite.gameObject, this, mStageRankingListInfo, list, -89f, posY, mSelfPanel.sortingOrder + 1, true, UIScrollView.Movement.Vertical);
			mStageRankingList.OnCreateItem = OnCreateItem;
		}
	}

	public void Close()
	{
		mProgression = 90f;
		mDestroyFlg = true;
		mState.Change(STATE.CLOSE);
	}

	public void OnAddFriend(GameObject go)
	{
		if (mState.GetStatus() == STATE.WAIT && mGame.mPlayer.Data.ApplyRandomUserNum >= GameMain.APPLY_RANDOM_USER_MAX)
		{
			ConfirmDialog confirmDialog = Util.CreateGameObject("LimitApplyDialog", mGame.mAnchor.gameObject).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("Friend_Request_Close_Title"), Localization.Get("Friend_Request_Close_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			confirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Change(STATE.WAIT);
		}
	}

	private void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
	}

	public void SetSortingOder(int order)
	{
		mSelfPanel.sortingOrder = order;
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		if (index == mMyItemIndex)
		{
			mMyObject = itemGO;
		}
		if (index == 0)
		{
			mFriendObject = itemGO;
		}
		List<MyFriend> list = null;
		list = mGame.AllFriends();
		StageRankingData stageRankingData = item as StageRankingData;
		float cellWidth = mStageRankingList.mList.cellWidth;
		float cellHeight = mStageRankingList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, image2);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		if (stageRankingData.iconid < 0)
		{
			if (list != null && list.Count > 0)
			{
				foreach (MyFriend item2 in list)
				{
					if (item2.Data.UUID != stageRankingData.uuid)
					{
					}
				}
			}
		}
		else
		{
			int num2 = 0;
			if (stageRankingData.uuid == mGame.mPlayer.UUID)
			{
				stageRankingData.iconid = mGame.mPlayer.Data.PlayerIcon;
			}
			num2 = stageRankingData.iconid;
			if (!GameMain.IsFacebookIconEnable() && stageRankingData.iconid >= 10000)
			{
				num2 -= 10000;
			}
			if (num2 < 10000)
			{
				if (num2 >= mGame.mCompanionData.Count)
				{
					num2 = 0;
				}
				CompanionData companionData = mGame.mCompanionData[num2];
				UISprite uISprite2 = Util.CreateSprite("Icon", itemGO, image);
				Util.SetSpriteInfo(uISprite2, companionData.iconName, mBaseDepth + 1, new Vector3(0f, 17f, 0f), Vector3.one, false);
				uISprite2.SetDimensions(80, 80);
				if (GameMain.IsFacebookMiniIconEnable())
				{
					if (stageRankingData.uuid == mGame.mPlayer.UUID)
					{
						if (IsFacebookLogined())
						{
							string imageName = "icon_sns00_mini";
							UISprite sprite = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
							Util.SetSpriteInfo(sprite, imageName, mBaseDepth + 2, new Vector3(25f, 25f, 0f), Vector3.one, false);
						}
					}
					else
					{
						foreach (KeyValuePair<int, MyFriend> friend in mGame.mPlayer.Friends)
						{
							if (!string.IsNullOrEmpty(friend.Value.Data.Fbid) && friend.Key == stageRankingData.uuid)
							{
								string imageName2 = "icon_sns00_mini";
								UISprite sprite2 = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
								Util.SetSpriteInfo(sprite2, imageName2, mBaseDepth + 2, new Vector3(25f, 25f, 0f), Vector3.one, false);
							}
						}
					}
				}
			}
			else if (stageRankingData.uuid == mGame.mPlayer.UUID)
			{
				if (mGame.mPlayer.Icon != null)
				{
					UITexture uITexture = Util.CreateGameObject("Icon", itemGO).AddComponent<UITexture>();
					uITexture.depth = mBaseDepth + 1;
					uITexture.transform.localPosition = new Vector3(0f, 17f, 0f);
					uITexture.mainTexture = mGame.mPlayer.Icon;
					uITexture.SetDimensions(65, 65);
				}
				else
				{
					UISprite uISprite3 = Util.CreateSprite("FriendIcon", itemGO, image);
					Util.SetSpriteInfo(uISprite3, "icon_chara_facebook", mBaseDepth + 1, new Vector3(0f, 17f, 0f), Vector3.one, false);
					uISprite3.SetDimensions(65, 65);
				}
				if (num2 >= mGame.mCompanionData.Count)
				{
					num2 = 0;
				}
				CompanionData companionData2 = mGame.mCompanionData[num2];
				UISprite uISprite4 = Util.CreateSprite("Icon", itemGO, image);
				Util.SetSpriteInfo(uISprite4, companionData2.iconName, mBaseDepth + 2, new Vector3(30f, 50f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(40, 40);
			}
			else
			{
				foreach (KeyValuePair<int, MyFriend> friend2 in mGame.mPlayer.Friends)
				{
					if (friend2.Key == stageRankingData.uuid)
					{
						if (friend2.Value.Icon != null)
						{
							UITexture uITexture2 = Util.CreateGameObject("Icon", itemGO).AddComponent<UITexture>();
							uITexture2.depth = mBaseDepth + 1;
							uITexture2.transform.localPosition = new Vector3(0f, 17f, 0f);
							uITexture2.mainTexture = friend2.Value.Icon;
							uITexture2.SetDimensions(65, 65);
						}
						else
						{
							UISprite uISprite5 = Util.CreateSprite("FriendIcon", itemGO, image);
							Util.SetSpriteInfo(uISprite5, "icon_chara_facebook", mBaseDepth + 1, new Vector3(0f, 17f, 0f), Vector3.one, false);
							uISprite5.SetDimensions(65, 65);
						}
						num2 = stageRankingData.iconid - 10000;
						if (num2 >= mGame.mCompanionData.Count)
						{
							num2 = 0;
						}
						CompanionData companionData3 = mGame.mCompanionData[num2];
						UISprite uISprite6 = Util.CreateSprite("Icon", itemGO, image);
						Util.SetSpriteInfo(uISprite6, companionData3.iconName, mBaseDepth + 2, new Vector3(30f, 50f, 0f), Vector3.one, false);
						uISprite6.SetDimensions(40, 40);
					}
				}
			}
		}
		UILabel uILabel;
		if (index < 3)
		{
			UISprite sprite3 = Util.CreateSprite("Crown", itemGO.gameObject, image2);
			Util.SetSpriteInfo(sprite3, string.Format("ranking{0}_00", index + 1), mBaseDepth + 2, new Vector3(-37f, 56f, 0f), Vector3.one, false);
			sprite3 = Util.CreateSprite("IconFrame", itemGO.gameObject, image2);
			Util.SetSpriteInfo(sprite3, string.Format("ranking{0}_01", index + 1), mBaseDepth + 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
		}
		else
		{
			UISprite uISprite7 = Util.CreateSprite("Crown", itemGO.gameObject, image2);
			Util.SetSpriteInfo(uISprite7, "ranking_crown", mBaseDepth + 2, new Vector3(-37f, 56f, 0f), Vector3.one, false);
			uILabel = Util.CreateLabel("RankingNum", uISprite7.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty + (index + 1), mBaseDepth + 3, new Vector3(0f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.white;
		}
		uILabel = Util.CreateLabel("Nickname", itemGO.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, stageRankingData.nickname, mBaseDepth + 4, new Vector3(0f, -40f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(155, 20);
		uILabel = Util.CreateLabel("HiscoreNum", itemGO.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty + stageRankingData.clr_score, mBaseDepth + 4, new Vector3(0f, -65f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
	}

	private void ExecCallbackMethod(UnityAction<UnityAction> method, params UnityAction[] acts)
	{
		if (method == null || acts == null)
		{
			return;
		}
		foreach (UnityAction unityAction in acts)
		{
			if (unityAction != null)
			{
				method(unityAction);
			}
		}
	}

	private UnityEvent GetCallback(CALLBACK type, bool force_create)
	{
		if (force_create && mCallback == null)
		{
			mCallback = new Dictionary<CALLBACK, UnityEvent>();
		}
		if (force_create && !mCallback.ContainsKey(type))
		{
			mCallback.Add(type, new UnityEvent());
		}
		return (mCallback == null || !mCallback.ContainsKey(type)) ? null : mCallback[type];
	}

	private void InvokeCallback(CALLBACK type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.Invoke();
		}
	}

	public void AddCallback(CALLBACK type, params UnityAction[] acts)
	{
		if (acts != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.AddListener, acts);
			}
		}
	}

	public void RemoveCallback(CALLBACK type, params UnityAction[] acts)
	{
		if (acts != null)
		{
			UnityEvent callback = GetCallback(type, false);
			if (callback != null)
			{
				ExecCallbackMethod(callback.RemoveListener, acts);
			}
		}
	}

	public void ClearCallback(params CALLBACK[] types)
	{
		if (types == null)
		{
			return;
		}
		foreach (CALLBACK type in types)
		{
			UnityEvent callback = GetCallback(type, false);
			if (callback != null)
			{
				callback.RemoveAllListeners();
			}
		}
	}

	public void ClearCallback()
	{
		foreach (int value in Enum.GetValues(typeof(CALLBACK)))
		{
			ClearCallback((CALLBACK)value);
		}
	}

	public void SetCallback(CALLBACK type, params UnityAction[] acts)
	{
		ClearCallback(type);
		AddCallback(type, acts);
	}
}
