using System.Collections.Generic;
using System.IO;

public class LinkBannerManager : SingletonMonoBehaviour<LinkBannerManager>
{
	public delegate void OnGetCompleteCallback(List<LinkBannerInfo> aBannerList);

	private static LinkBannerManager mInstance;

	private List<LinkBannerRequest> mRequestList;

	private bool mCacheDelete;

	public bool mPause;

	public new static LinkBannerManager Instance
	{
		get
		{
			return mInstance;
		}
	}

	public static void CreateLinkBannerManager()
	{
		mInstance = Util.CreateGameObject("LinkBannerManager", null).AddComponent<LinkBannerManager>();
	}

	protected override void Awake()
	{
		base.Awake();
		mRequestList = new List<LinkBannerRequest>();
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		mRequestList.Clear();
	}

	public void Update()
	{
		if (mCacheDelete)
		{
			string path = Path.Combine(BIJUnity.getTempPath(), "linkbanner");
			if (Directory.Exists(path))
			{
				Directory.Delete(path, true);
				Directory.CreateDirectory(path);
			}
			mCacheDelete = false;
		}
		for (int i = 0; i < mRequestList.Count; i++)
		{
			LinkBannerRequest linkBannerRequest = mRequestList[i];
			linkBannerRequest.Update();
		}
	}

	public bool GetLinkBannerRequest(LinkBannerRequest aRequest, LinkBannerSetting aSetting, OnGetCompleteCallback aCallback, bool aDLOnly = false)
	{
		if (aSetting == null)
		{
			return false;
		}
		if (aSetting.BannerList.Count <= 0)
		{
			return false;
		}
		aRequest.mNewSetting = aSetting;
		aRequest.mNewDLOnly = aDLOnly;
		aRequest.mCompleteCallback = aCallback;
		if (!mRequestList.Contains(aRequest))
		{
			mRequestList.Add(aRequest);
		}
		return true;
	}

	public void ClearLinkBanner(LinkBannerRequest aRequest)
	{
		aRequest.ClearLinkBanner();
	}

	public void DeleteCache()
	{
		mCacheDelete = true;
	}

	private void OnApplicationPause(bool pause_state)
	{
		mPause = pause_state;
		if (mRequestList == null)
		{
			return;
		}
		if (pause_state)
		{
			foreach (LinkBannerRequest mRequest in mRequestList)
			{
				mRequest.Pause();
			}
			return;
		}
		foreach (LinkBannerRequest mRequest2 in mRequestList)
		{
			mRequest2.Resume();
		}
	}

	public bool EnableDownloadBanner()
	{
		if (mPause)
		{
			return false;
		}
		if (!InternetReachability.IsEnable)
		{
			return false;
		}
		return true;
	}
}
