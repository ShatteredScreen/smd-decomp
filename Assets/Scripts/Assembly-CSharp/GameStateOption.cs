using System;
using System.Collections;
using UnityEngine;

public class GameStateOption : GameState
{
	public enum STATE
	{
		MAIN = 0,
		LOAD_WAIT = 1,
		UNLOAD_WAIT = 2,
		WAIT = 3,
		OPTION = 4,
		NETWORK_LOGIN00 = 5,
		NETWORK_LOGIN01 = 6,
		NETWORK_LOGOUT = 7,
		AUTHERROR_RETURN_TITLE = 8,
		AUTHERROR_RETURN_WAIT = 9
	}

	public enum OPTION
	{
		INVALID = -1,
		BGM = 0,
		SE = 1,
		COMBO = 2,
		NOTIFICATION = 3,
		FACEBOOK = 4,
		GOOGLE_PLUS = 5,
		TWITTER = 6,
		GPGS = 7,
		INVITE = 8,
		GEM = 9,
		REACQUIRE_DATA = 10,
		GPGS_ACHIEVEMENT = 11,
		TITLE = 12
	}

	public enum OPTION_DIALOG_STYLE
	{
		OPTION_MENU = 0
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private static readonly Color32 TEXT_COLOR = new Color32(0, 0, 0, byte.MaxValue);

	private static readonly Color32 TEXT_MESSAGE = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

	private static readonly int TEXT_SIZE = 24;

	private GameObject mAnchorRoot;

	private UIButton mExitButton;

	private UILabel mExitBack;

	private UISprite mBackground;

	private GameObject mAnchorTop;

	private GameObject mAnchorTopRight;

	private GameObject mAnchorTopLeft;

	private GameObject mAnchorBottomRight;

	private GameObject mAnchorBottomLeft;

	private UISprite mBack;

	private UILabel mTitle;

	private UILabel mVersion;

	private UILabel mBGM;

	private UILabel mSE;

	private UILabel mNotification;

	private UILabel mFacebook;

	private UILabel mGoogle;

	private UILabel mTwitter;

	private UILabel mGPGS;

	private GameFriendManager mFriendManager;

	private UIButton mButtonBGM;

	private UIButton mButtonSE;

	private UIButton mButtonCombo;

	private UIButton mButtonNotification;

	private UIButton mButtonFacebook;

	private UIButton mButtonGoogle;

	private UIButton mButtonTwitter;

	private UIButton mButtonGPGS;

	private UIButton mButtonFriend;

	private UIButton mButtonGem;

	private UIButton mButtonResource;

	private UIButton mButtonGPGSAchievement;

	private int mSNSCallCount;

	private ConfirmDialog mConfirmdialog;

	private SDConfirmDialog mSDConfirmDialog;

	private InviteDialog mInviteDialog;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private bool mEULADialogDisplayReq;

	public int mBaseDepth = 50;

	public int mButton_depth;

	private OPTION mSelectItem = OPTION.INVALID;

	private OPTION_DIALOG_STYLE mOptionDialog;

	private GameStateManager mGameState;

	public bool Modified { get; private set; }

	public bool mBgm_state { get; private set; }

	public bool mSe_state { get; private set; }

	public bool mNotification_state { get; private set; }

	public override void Start()
	{
		base.Start();
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}

	public override void Unload()
	{
		base.Unload();
	}

	private void OnDestroy()
	{
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mGame.PlayMusic("BGM_PUZZLE", 0, true);
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns3 = mSNS;
			_sns3.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns3, string.Empty, string.Empty))
					{
						UpdateButtonText();
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns3);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns2, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							UpdateButtonText();
							Modified = false;
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns2))
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						UpdateButtonText();
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
				else
				{
					mState.Reset(STATE.MAIN, true);
					if (mSNS == SocialManager.Instance[SNS.GooglePlayGameServices] && GameMain.IsSNSLogined(_sns2) && mSelectItem == OPTION.GPGS_ACHIEVEMENT)
					{
						OnGPGSAchievementButtonPushed();
					}
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGOUT:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.Logout(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					string id = null;
					string text = null;
					GameMain.TryGetSNSUserInfo(_sns, out id, out text);
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						UpdateButtonText();
						Modified = false;
						mGame.SaveOptions();
						mGame.mPlayer.ResetFriends();
						mGame.Save();
					}
					mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				mState.Change(STATE.MAIN);
			}, null);
			break;
		}
		case STATE.AUTHERROR_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Transfer_CertError_Title"), Localization.Get("Transfered_TitleBack_Desc2"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.AUTHERROR_RETURN_WAIT);
			break;
		}
		if (mGame.mBackKeyTrg)
		{
			OnBackKeyPress();
		}
		mState.Update();
	}

	public override IEnumerator UnloadGameState()
	{
		ResourceManager.UnloadSound("BGM_PUZZLE");
		UnloadGameStateFinished();
		yield return 0;
	}

	public override IEnumerator LoadGameState()
	{
		ResSound resSound = ResourceManager.LoadSoundAsync("BGM_PUZZLE");
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		UIFont font = GameMain.LoadFont();
		UISprite sprite15 = null;
		UILabel label14 = null;
		Vector2 logScreen = Util.LogScreenSize();
		mButton_depth = mBaseDepth + 1;
		mAnchorTop = Util.CreateAnchorWithChild("OptionAnchorTop", mRoot, UIAnchor.Side.Top, mGame.mCamera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("OptionAnchorTopRight", mRoot, UIAnchor.Side.TopRight, mGame.mCamera).gameObject;
		mAnchorTopLeft = Util.CreateAnchorWithChild("OptionAnchorTopLeft", mRoot, UIAnchor.Side.TopLeft, mGame.mCamera).gameObject;
		mAnchorBottomRight = Util.CreateAnchorWithChild("OptionAnchorBottomRight", mRoot, UIAnchor.Side.BottomRight, mGame.mCamera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("OptionAnchorBottomLeft", mRoot, UIAnchor.Side.BottomLeft, mGame.mCamera).gameObject;
		mBack = Util.CreateSprite("BackGround", mRoot, atlas);
		Util.SetSpriteInfo(mBack, "bg00", mBaseDepth, Vector3.zero, Vector3.one, false);
		mBack.type = UIBasicSprite.Type.Tiled;
		mBack.SetDimensions(1138, 1138);
		sprite15 = Util.CreateSprite("TitleBarR", mAnchorTop.gameObject, atlas);
		Util.SetSpriteInfo(sprite15, "menubar_frame", mBaseDepth + 1, new Vector3(-2f, 173f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite15 = Util.CreateSprite("TitleBarL", mAnchorTop.gameObject, atlas);
		Util.SetSpriteInfo(sprite15, "menubar_frame", mBaseDepth + 1, new Vector3(2f, 173f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		label14 = Util.CreateLabel("TitleBarLabel", mAnchorTop.gameObject, font);
		Util.SetLabelInfo(label14, Localization.Get("Option_Title"), mBaseDepth + 4, new Vector3(0f, -26f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
		mVersion = Util.CreateLabel("VersionLabel", mAnchorBottomRight.gameObject, font);
		Util.SetLabelInfo(mVersion, "Version : " + Application.version, mBaseDepth + 4, new Vector3(-20f, 32f, 0f), 24, 0, 0, UIWidget.Pivot.Right);
		Color32 text_message = TEXT_MESSAGE;
		float iconX = -169f;
		float y_button4 = 258f;
		float pos_y = 101f;
		float whitebar = 128f;
		string imageName25;
		if (mGame.mOptions.BGMEnable)
		{
			mBgm_state = false;
			imageName25 = "button_music_ON";
		}
		else
		{
			mBgm_state = true;
			imageName25 = "button_music_OFF";
		}
		mButtonBGM = Util.CreateJellyImageButton("BGMButton", mRoot, atlas);
		Util.SetImageButtonInfo(mButtonBGM, imageName25, imageName25, imageName25, mButton_depth, new Vector3(-200f, 404f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonBGM, this, "OnBgmPushed", UIButtonMessage.Trigger.OnClick);
		if (mGame.mOptions.SEEnable)
		{
			mBgm_state = false;
			imageName25 = "button_sound_ON";
		}
		else
		{
			mBgm_state = true;
			imageName25 = "button_sound_OFF";
		}
		mButtonSE = Util.CreateJellyImageButton("SEButton", mRoot, atlas);
		Util.SetImageButtonInfo(mButtonSE, imageName25, imageName25, imageName25, mButton_depth, new Vector3(-67f, 404f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonSE, this, "OnSePushed", UIButtonMessage.Trigger.OnClick);
		if (mGame.mOptions.ComboCutIn)
		{
			mNotification_state = false;
			imageName25 = "button_combo_ON";
		}
		else
		{
			mNotification_state = true;
			imageName25 = "button_combo_OFF";
		}
		mButtonCombo = Util.CreateJellyImageButton("ComboButton", mRoot, atlas);
		Util.SetImageButtonInfo(mButtonCombo, imageName25, imageName25, imageName25, mButton_depth, new Vector3(67f, 404f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonCombo, this, "OnComboPushed", UIButtonMessage.Trigger.OnClick);
		if (mGame.mOptions.UseNotification)
		{
			mNotification_state = false;
			imageName25 = "button_notice_ON";
		}
		else
		{
			mNotification_state = true;
			imageName25 = "button_notice_OFF";
		}
		mButtonNotification = Util.CreateJellyImageButton("NotificationButton", mRoot, atlas);
		Util.SetImageButtonInfo(mButtonNotification, imageName25, imageName25, imageName25, mButton_depth, new Vector3(200f, 404f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonNotification, this, "OnNotificationPushed", UIButtonMessage.Trigger.OnClick);
		float iconscaleXY = 0.5f;
		float iconscaleZ = 1f;
		imageName25 = "button_back";
		mExitButton = Util.CreateJellyImageButton("ExitButton", mAnchorBottomLeft.gameObject, atlas);
		Util.SetImageButtonInfo(mExitButton, imageName25, imageName25, imageName25, mButton_depth, new Vector3(60f, 60f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mExitButton, this, "OnExitPushed", UIButtonMessage.Trigger.OnClick);
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option, SNS.Facebook))
		{
			imageName25 = "button00";
			mButtonFacebook = Util.CreateJellyImageButton("FacebookButton", mRoot, atlas);
			Util.SetImageButtonInfo(mButtonFacebook, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButtonFacebook, this, "OnFacebookButtonPushed", UIButtonMessage.Trigger.OnClick);
			imageName25 = "icon_sns00";
			sprite15 = Util.CreateSprite("FacebookIcon", mButtonFacebook.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), new Vector3(iconscaleXY, iconscaleXY, iconscaleZ), false);
			sprite15.type = UIBasicSprite.Type.Sliced;
			sprite15.SetDimensions(50, 50);
			imageName25 = "button01";
			sprite15 = Util.CreateSprite("FacebookBar", mButtonFacebook.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(whitebar, 0f, 0f), Vector3.one, false);
			label14 = Util.CreateLabel("message", mButtonFacebook.gameObject, font);
			Util.SetLabelInfo(label14, Localization.Get("SNS_Facebook"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(label14);
			label14.fontSize = 28;
			Util.SetLabelColor(label14, text_message);
			label14.overflowMethod = UILabel.Overflow.ResizeHeight;
			label14.SetDimensions(250, 40);
			label14 = Util.CreateLabel("Status", mButtonFacebook.gameObject, font);
			Util.SetLabelInfo(label14, string.Empty, mButton_depth + 3, Vector3.zero, 20, 0, 0, UIWidget.Pivot.Center);
			label14.color = Def.DEFAULT_MESSAGE_COLOR;
			label14.gameObject.transform.localPosition = new Vector3(130f, -5f, 0f);
			mFacebook = label14;
			y_button4 -= pos_y;
		}
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option, SNS.GooglePlus))
		{
			imageName25 = "button00";
			mButtonGoogle = Util.CreateJellyImageButton("GoogleButton", mRoot, atlas);
			Util.SetImageButtonInfo(mButtonGoogle, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButtonGoogle, this, "OnGooglePlusButtonPushed", UIButtonMessage.Trigger.OnClick);
			imageName25 = "icon_sns05";
			sprite15 = Util.CreateSprite("GoogleIcon", mButtonGoogle.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), new Vector3(iconscaleXY, iconscaleXY, iconscaleZ), false);
			sprite15.type = UIBasicSprite.Type.Sliced;
			sprite15.SetDimensions(50, 50);
			imageName25 = "button01";
			sprite15 = Util.CreateSprite("GoogleBar", mButtonGoogle.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(whitebar, 0f, 0f), Vector3.one, false);
			label14 = Util.CreateLabel("message", mButtonGoogle.gameObject, font);
			Util.SetLabelInfo(label14, Localization.Get("SNS_GooglePlus"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(label14);
			label14.fontSize = 28;
			Util.SetLabelColor(label14, text_message);
			label14.overflowMethod = UILabel.Overflow.ResizeHeight;
			label14.SetDimensions(250, 40);
			label14 = Util.CreateLabel("Status", mButtonGoogle.gameObject, font);
			Util.SetLabelInfo(label14, string.Empty, mButton_depth + 3, Vector3.zero, 20, 0, 0, UIWidget.Pivot.Center);
			label14.color = Def.DEFAULT_MESSAGE_COLOR;
			label14.gameObject.transform.localPosition = new Vector3(130f, -5f, 0f);
			mGoogle = label14;
			y_button4 -= pos_y;
		}
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option, SNS.Twitter))
		{
			imageName25 = "button00";
			mButtonTwitter = Util.CreateJellyImageButton("TwitterButton", mRoot, atlas);
			Util.SetImageButtonInfo(mButtonTwitter, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButtonTwitter, this, "OnTwitterButtonPushed", UIButtonMessage.Trigger.OnClick);
			imageName25 = "icon_sns03";
			sprite15 = Util.CreateSprite("twitterIcon", mButtonTwitter.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), new Vector3(iconscaleXY, iconscaleXY, iconscaleZ), false);
			sprite15.type = UIBasicSprite.Type.Sliced;
			sprite15.SetDimensions(50, 50);
			imageName25 = "button01";
			sprite15 = Util.CreateSprite("TwitterBar", mButtonTwitter.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(whitebar, 0f, 0f), Vector3.one, false);
			label14 = Util.CreateLabel("message", mButtonTwitter.gameObject, font);
			Util.SetLabelInfo(label14, Localization.Get("SNS_Twitter"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(label14);
			label14.fontSize = 28;
			Util.SetLabelColor(label14, text_message);
			label14.overflowMethod = UILabel.Overflow.ResizeHeight;
			label14.SetDimensions(250, 40);
			label14 = Util.CreateLabel("Status", mButtonTwitter.gameObject, font);
			Util.SetLabelInfo(label14, string.Empty, mButton_depth + 3, Vector3.zero, 20, 0, 0, UIWidget.Pivot.Center);
			label14.color = Def.DEFAULT_MESSAGE_COLOR;
			label14.gameObject.transform.localPosition = new Vector3(130f, -5f, 0f);
			mTwitter = label14;
			y_button4 -= pos_y;
		}
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Option, SNS.GooglePlayGameServices))
		{
			imageName25 = "button00";
			mButtonGPGS = Util.CreateJellyImageButton("GPGSButton", mRoot, atlas);
			Util.SetImageButtonInfo(mButtonGPGS, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButtonGPGS, this, "OnGPGSButtonPushed", UIButtonMessage.Trigger.OnClick);
			imageName25 = "icon_sns06";
			sprite15 = Util.CreateSprite("GPGSIcon", mButtonGPGS.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), new Vector3(iconscaleXY, iconscaleXY, iconscaleZ), false);
			sprite15.type = UIBasicSprite.Type.Sliced;
			sprite15.SetDimensions(50, 50);
			imageName25 = "button01";
			sprite15 = Util.CreateSprite("GPGSBar", mButtonGPGS.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(whitebar, 0f, 0f), Vector3.one, false);
			label14 = Util.CreateLabel("message", mButtonGPGS.gameObject, font);
			Util.SetLabelInfo(label14, Localization.Get("SNS_GooglePlayGameServices"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(label14);
			label14.fontSize = 28;
			Util.SetLabelColor(label14, text_message);
			label14.overflowMethod = UILabel.Overflow.ResizeHeight;
			label14.SetDimensions(250, 40);
			label14 = Util.CreateLabel("Status", mButtonGPGS.gameObject, font);
			Util.SetLabelInfo(label14, string.Empty, mButton_depth + 3, Vector3.zero, 20, 0, 0, UIWidget.Pivot.Center);
			label14.color = Def.DEFAULT_MESSAGE_COLOR;
			label14.gameObject.transform.localPosition = new Vector3(130f, -5f, 0f);
			mGPGS = label14;
			y_button4 -= pos_y;
		}
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite))
		{
			imageName25 = "button00";
			mButtonFriend = Util.CreateJellyImageButton("OnInviteButton", mRoot, atlas);
			Util.SetImageButtonInfo(mButtonFriend, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButtonFriend, this, "OnInviteButtonPushed", UIButtonMessage.Trigger.OnClick);
			imageName25 = "icon_invitation";
			sprite15 = Util.CreateSprite("invitationIcon", mButtonFriend.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), Vector3.one, false);
			label14 = Util.CreateLabel("message", mButtonFriend.gameObject, font);
			Util.SetLabelInfo(label14, Localization.Get("SNS_Invite"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(label14);
			label14.fontSize = 28;
			Util.SetLabelColor(label14, text_message);
			label14.overflowMethod = UILabel.Overflow.ResizeHeight;
			label14.SetDimensions(250, 40);
			y_button4 -= pos_y;
		}
		imageName25 = "button00";
		mButtonGem = Util.CreateJellyImageButton("SDConfirmButton", mRoot, atlas);
		Util.SetImageButtonInfo(mButtonGem, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonGem, this, "OnSDConfirmPushed", UIButtonMessage.Trigger.OnClick);
		imageName25 = "icon_currency_jem";
		sprite15 = Util.CreateSprite("friendIcon", mButtonGem.gameObject, atlas);
		Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), Vector3.one, false);
		label14 = Util.CreateLabel("message", mButtonGem.gameObject, font);
		Util.SetLabelInfo(label14, Localization.Get("SNS_Gem"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		DialogBase.SetDialogLabelEffect(label14);
		label14.fontSize = 28;
		Util.SetLabelColor(label14, text_message);
		label14.overflowMethod = UILabel.Overflow.ResizeHeight;
		label14.SetDimensions(250, 40);
		y_button4 -= pos_y;
		imageName25 = "button00";
		mButtonResource = Util.CreateJellyImageButton("OnResourceClearButton", mRoot, atlas);
		Util.SetImageButtonInfo(mButtonResource, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonResource, this, "OnResourceClearButtonPushed", UIButtonMessage.Trigger.OnClick);
		imageName25 = "icon_resourceClear";
		sprite15 = Util.CreateSprite("resourceClear", mButtonResource.gameObject, atlas);
		Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), Vector3.one, false);
		label14 = Util.CreateLabel("message", mButtonResource.gameObject, font);
		Util.SetLabelInfo(label14, Localization.Get("SNS_Resources_Clear"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		DialogBase.SetDialogLabelEffect(label14);
		label14.fontSize = 28;
		Util.SetLabelColor(label14, text_message);
		label14.overflowMethod = UILabel.Overflow.ResizeHeight;
		label14.SetDimensions(250, 40);
		y_button4 -= pos_y;
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement, SNS.GooglePlayGameServices))
		{
			imageName25 = "button00";
			mButtonGPGSAchievement = Util.CreateJellyImageButton("GPGSAchievementButton", mRoot, atlas);
			Util.SetImageButtonInfo(mButtonGPGSAchievement, imageName25, imageName25, imageName25, mButton_depth, new Vector3(0f, y_button4, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButtonGPGSAchievement, this, "OnGPGSAchievementButtonPushed", UIButtonMessage.Trigger.OnClick);
			imageName25 = "icon_sns07";
			sprite15 = Util.CreateSprite("GPGSIcon", mButtonGPGSAchievement.gameObject, atlas);
			Util.SetSpriteInfo(sprite15, imageName25, mButton_depth + 1, new Vector3(iconX, 0f, 0f), new Vector3(iconscaleXY, iconscaleXY, iconscaleZ), false);
			sprite15.type = UIBasicSprite.Type.Sliced;
			sprite15.SetDimensions(50, 50);
			label14 = Util.CreateLabel("message", mButtonGPGSAchievement.gameObject, font);
			Util.SetLabelInfo(label14, Localization.Get("GooglePlus_Achievement"), mButton_depth + 1, new Vector3(-130f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(label14);
			label14.fontSize = 34;
			Util.SetLabelColor(label14, text_message);
			label14.overflowMethod = UILabel.Overflow.ShrinkContent;
			label14.SetDimensions(330, 34);
			y_button4 -= pos_y;
		}
		if (GameMain.USE_DEBUG_DIALOG && !GameMain.HIDDEN_DEBUG_BUTTON)
		{
			UIButton button = Util.CreateJellyImageButton(atlas: ResourceManager.LoadImage("HUD").Image, name: "DebugButton", parent: mAnchorTopLeft.gameObject, a_type: Util.BUTTON_TYPE.NORMAL2);
			Util.SetImageButtonInfo(button, "button_debug", "button_debug", "button_debug", mButton_depth + 1, new Vector3(50f, -340f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnDebugPushed", UIButtonMessage.Trigger.OnClick);
		}
		UpdateButtonText();
		BoxCollider collider = mRoot.AddComponent<BoxCollider>();
		collider.center = new Vector3(0f, 0f, 0f);
		collider.size = new Vector3(logScreen.x, logScreen.y, 1f);
		mState.Update();
		SetLayout(Util.ScreenOrientation);
		LoadGameStateFinished();
		yield return 0;
	}

	public void OnDebugPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.StopMusic(0, 0.2f);
			SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
			sMDDebugDialog.SetGameState(this);
			sMDDebugDialog.SetClosedCallback(delegate
			{
				mGame.PlayMusic("BGM_PUZZLE_CHANCE", 0, true);
			});
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
		{
			float num = 258f;
			float num2 = 101f;
			mButtonBGM.transform.localPosition = new Vector3(-200f, 404f, 0f);
			mButtonSE.transform.localPosition = new Vector3(-67f, 404f, 0f);
			mButtonCombo.transform.localPosition = new Vector3(67f, 404f, 0f);
			mButtonNotification.transform.localPosition = new Vector3(200f, 404f, 0f);
			if (mButtonFacebook != null)
			{
				mButtonFacebook.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mButtonGoogle != null)
			{
				mButtonGoogle.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mButtonTwitter != null)
			{
				mButtonTwitter.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mButtonGPGS != null)
			{
				mButtonGPGS.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mButtonFriend != null)
			{
				mButtonFriend.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mButtonGem != null)
			{
				mButtonGem.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mButtonResource != null)
			{
				mButtonResource.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mButtonGPGSAchievement != null)
			{
				mButtonGPGSAchievement.transform.localPosition = new Vector3(0f, num, 0f);
				num -= num2;
			}
			if (mVersion != null)
			{
				mVersion.transform.parent = mAnchorBottomRight.gameObject.transform;
				mVersion.transform.localPosition = new Vector3(-20f, 32f);
			}
		}
		else
		{
			float num3 = 70f;
			float num4 = 101f;
			float num5 = -232f;
			mButtonBGM.transform.localPosition = new Vector3(-200f, 185f, 0f);
			mButtonSE.transform.localPosition = new Vector3(-67f, 185f, 0f);
			mButtonCombo.transform.localPosition = new Vector3(67f, 185f, 0f);
			mButtonNotification.transform.localPosition = new Vector3(200f, 185f, 0f);
			UIButton[] array = new UIButton[8] { mButtonFacebook, mButtonTwitter, mButtonGoogle, mButtonGPGS, mButtonGPGSAchievement, mButtonFriend, mButtonGem, mButtonResource };
			int num6 = 0;
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] != null)
				{
					array[i].transform.localPosition = new Vector3(num5, num3, 0f);
					num5 *= -1f;
					num6++;
					if ((num6 & 1) == 0)
					{
						num3 -= num4;
					}
				}
			}
			if (mVersion != null)
			{
				mVersion.transform.parent = mAnchorTopRight.gameObject.transform;
				mVersion.transform.localPosition = new Vector3(-20f, -120f);
			}
		}
		if (mBack != null)
		{
			Vector2 vector = Util.LogScreenSize();
			float num7 = vector.y / 909f;
			mBack.transform.localScale = new Vector3(num7, num7, 1f);
			mBack.SetDimensions((int)(vector.x / num7), 909);
		}
	}

	private void UpdateButtonText()
	{
		string text = Localization.Get("Option_On");
		string text2 = Localization.Get("Option_Off");
		string text3 = Localization.Get("Option_Login");
		string text4 = Localization.Get("Option_Logout");
		if (mBGM != null)
		{
			Util.SetLabelText(mBGM, (!mGame.mOptions.BGMEnable) ? text2 : text);
		}
		if (mSE != null)
		{
			Util.SetLabelText(mSE, (!mGame.mOptions.SEEnable) ? text2 : text);
		}
		if (mNotification != null)
		{
			Util.SetLabelText(mNotification, (!mGame.mOptions.UseNotification) ? text2 : text);
		}
		if (mFacebook != null)
		{
			if (IsFacebookLogined())
			{
				string text5 = text4;
				Util.SetLabelText(mFacebook, text5);
			}
			else
			{
				Util.SetLabelText(mFacebook, text3);
			}
		}
		if (mTwitter != null)
		{
			if (IsTwitterLogined())
			{
				string text5 = text4;
				Util.SetLabelText(mTwitter, text5);
			}
			else
			{
				Util.SetLabelText(mTwitter, text3);
			}
		}
		if (mGoogle != null)
		{
			if (IsGoogleLogined())
			{
				string text5 = text4;
				Util.SetLabelText(mGoogle, text5);
			}
			else
			{
				Util.SetLabelText(mGoogle, text3);
			}
		}
		if (mGPGS != null)
		{
			if (IsGPGSLogined())
			{
				string text5 = text4;
				Util.SetLabelText(mGPGS, text5);
			}
			else
			{
				Util.SetLabelText(mGPGS, text3);
			}
		}
	}

	public void Close()
	{
		CloseSubDialogs();
		mGame.SaveOptions();
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	private void CloseSubDialogs()
	{
		if (mInviteDialog != null)
		{
			mInviteDialog.Close();
		}
		if (mSDConfirmDialog != null)
		{
			mSDConfirmDialog.Close();
		}
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
	}

	public void OnBackKeyPress()
	{
		OnExitPushed();
	}

	public void OnBgmPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.ToggleBGM();
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = OPTION.BGM;
			string empty = string.Empty;
			if (mGame.mOptions.BGMEnable)
			{
				mBgm_state = false;
				empty = "button_music_ON";
			}
			else
			{
				mBgm_state = true;
				empty = "button_music_OFF";
			}
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	public void OnSePushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.ToggleSE();
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = OPTION.SE;
			string empty = string.Empty;
			if (mGame.mOptions.SEEnable)
			{
				mSe_state = false;
				empty = "button_sound_ON";
			}
			else
			{
				mSe_state = true;
				empty = "button_sound_OFF";
			}
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	public void OnComboPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.ToggleComboCutIn();
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = OPTION.COMBO;
			string empty = string.Empty;
			empty = ((!mGame.mOptions.ComboCutIn) ? "button_combo_OFF" : "button_combo_ON");
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	public void OnNotificationPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.ToggleNOTIFICATION();
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = OPTION.NOTIFICATION;
			string empty = string.Empty;
			if (mGame.mOptions.UseNotification)
			{
				mNotification_state = false;
				empty = "button_notice_ON";
			}
			else
			{
				mNotification_state = true;
				empty = "button_notice_OFF";
			}
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	private void OnExitPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.TITLE;
			mGame.SaveOptions();
			mGame.StopMusic(0, 0.2f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mGame.PlaySe("SE_NEGATIVE", -1);
			mState.Change(STATE.UNLOAD_WAIT);
		}
	}

	private bool SNSSuccess(BIJSNS _sns)
	{
		return false;
	}

	private void OnSNSSuccessDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		Close();
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = mRoot.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void OnLogoutConfirmDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		bool flag = false;
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			flag = true;
		}
		mConfirmDialog = null;
		if (flag)
		{
			mState.Reset(STATE.NETWORK_LOGOUT, true);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnFacebookButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.FACEBOOK;
			mSNS = SocialManager.Instance[SNS.Facebook];
			if (IsFacebookLogined())
			{
				mState.Reset(STATE.WAIT, true);
				StartCoroutine(GrayOut());
				GameObject parent = mRoot.transform.parent.gameObject;
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("LogoutConfirm_Desc"));
				mConfirmDialog.Init(Localization.Get("LogoutConfirm_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnLogoutFacebookConfirmDialogClosed);
				mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private void OnLogoutFacebookConfirmDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		bool flag = false;
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			flag = true;
		}
		mConfirmDialog = null;
		if (flag)
		{
			if (mGame.mPlayer.Data.PlayerIcon >= 10000)
			{
				mGame.mPlayer.Data.PlayerIcon -= 10000;
			}
			mState.Reset(STATE.NETWORK_LOGOUT, true);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	private bool IsGoogleLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.GooglePlus];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnGooglePlusButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.GOOGLE_PLUS;
			mSNS = SocialManager.Instance[SNS.GooglePlus];
			if (IsGoogleLogined())
			{
				mState.Reset(STATE.WAIT, true);
				StartCoroutine(GrayOut());
				GameObject parent = mRoot.transform.parent.gameObject;
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("LogoutConfirm_Desc"));
				mConfirmDialog.Init(Localization.Get("LogoutConfirm_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnLogoutConfirmDialogClosed);
				mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private bool IsTwitterLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Twitter];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnTwitterButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.TWITTER;
			mSNS = SocialManager.Instance[SNS.Twitter];
			if (IsTwitterLogined())
			{
				mState.Reset(STATE.WAIT, true);
				StartCoroutine(GrayOut());
				GameObject parent = mRoot.transform.parent.gameObject;
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("LogoutConfirm_Desc"));
				mConfirmDialog.Init(Localization.Get("LogoutConfirm_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnLogoutConfirmDialogClosed);
				mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private bool IsGPGSLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.GooglePlayGameServices];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnGPGSButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.GPGS;
			mSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
			if (IsGPGSLogined())
			{
				mState.Reset(STATE.WAIT, true);
				StartCoroutine(GrayOut());
				GameObject parent = mRoot.transform.parent.gameObject;
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("LogoutConfirm_Desc"));
				mConfirmDialog.Init(Localization.Get("LogoutConfirm_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnLogoutConfirmDialogClosed);
				mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private void OnInviteButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.INVITE;
			mState.Reset(STATE.WAIT, true);
			StartCoroutine(GrayOut());
			mInviteDialog = Util.CreateGameObject("InviteDialog", mRoot).AddComponent<InviteDialog>();
			mInviteDialog.SetClosedCallback(OnInviteDialogClosed);
			mInviteDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	private void OnInviteDialogClosed()
	{
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mInviteDialog.gameObject);
		mInviteDialog = null;
		UpdateButtonText();
		mState.Change(STATE.MAIN);
	}

	public void OnResourceClearButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.REACQUIRE_DATA;
			StartCoroutine(GrayOut());
			mConfirmdialog = Util.CreateGameObject("QuitDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmdialog.SetClosedCallback(OnQuitDialogClosed);
			mConfirmdialog.Init(Localization.Get("SNS_Resources_Clear"), Localization.Get("Resource_destroy_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			mState.Reset(STATE.WAIT);
		}
	}

	public void OnQuitDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			LinkBannerManager.Instance.DeleteCache();
			if (GameMain.USE_RESOURCEDL)
			{
				mGame.SaveOptions();
				mGame.StopMusic(0, 0.2f);
				ResourceDLUtils.DeleteDLFile(ResourceDLUtils.DEL_TYPE.AllDLFile);
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	private void OnGPGSAchievementButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN && 0 == 0 && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement, SNS.GooglePlayGameServices))
		{
			mSelectItem = OPTION.GPGS_ACHIEVEMENT;
			BIJSNS sns = SocialManager.Instance[SNS.GooglePlayGameServices];
			mSNSCallCount = 0;
			DoSNS(sns);
		}
	}

	private void DoSNS(BIJSNS _sns)
	{
		mSNSCallCount++;
		try
		{
			mState.Change(STATE.WAIT);
			mSNS = _sns;
			if (_sns.IsOpEnabled(2) && !_sns.IsLogined())
			{
				if (mSNSCallCount > 1)
				{
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.NETWORK_LOGIN00);
				}
			}
			else if (!GameMain.ShowAchievements(_sns, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSLogout iBIJSNSLogout = res.detail as IBIJSNSLogout;
						if (iBIJSNSLogout != null && iBIJSNSLogout.Logout)
						{
							mState.Change(STATE.NETWORK_LOGOUT);
							return;
						}
					}
				}
				catch (Exception)
				{
				}
				mState.Change(STATE.MAIN);
			}, null))
			{
				mState.Change(STATE.MAIN);
			}
		}
		catch (Exception)
		{
			mState.Change(STATE.MAIN);
		}
	}

	private void OnSDConfirmPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = OPTION.GEM;
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mSDConfirmDialog = Util.CreateGameObject("SDConfirmDialog", mRoot).AddComponent<SDConfirmDialog>();
			mSDConfirmDialog.SetClosedCallback(OnSDConfirmDialogClosed);
			mSDConfirmDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			mSDConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	private void OnSDConfirmDialogClosed()
	{
		if (mSDConfirmDialog != null)
		{
			UnityEngine.Object.Destroy(mSDConfirmDialog.gameObject);
			mSDConfirmDialog = null;
		}
		if (!GameMain.MaintenanceMode)
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.MAIN);
			return;
		}
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("MaintenanceMode_ReturnTitle_Desc"));
		mConfirmDialog.Init(Localization.Get("MaintenanceMode_ReturnTitle_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mConfirmDialog.SetClosedCallback(OnMaintenanceModeDialogClosed);
		mConfirmDialog.SetBaseDepth(70);
	}

	public void OnMaintenanceModeDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		mConfirmDialog = null;
		mGame.IsTitleReturnMaintenance = true;
		mGame.StopMusic(0, 0.5f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void OnTitleReturnMessageClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mGame.StopMusic(0, 0.5f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}
}
