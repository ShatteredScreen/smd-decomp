public class MCCheckRequest : ParameterObject<MCCheckRequest>
{
	public int bhiveid { get; set; }

	public string udid { get; set; }

	public string uniq_id { get; set; }

	public int uuid { get; set; }
}
