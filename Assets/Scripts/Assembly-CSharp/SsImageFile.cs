using System;
using UnityEngine;

[Serializable]
public class SsImageFile
{
	public string path;

	public Texture2D texture;

	public Material[] materials;

	public int width;

	public int height;

	public int bpp;

	public static SsImageFile invalidInstance;

	static SsImageFile()
	{
		invalidInstance = new SsImageFile();
		invalidInstance.path = "INVALID";
		invalidInstance.width = 8;
		invalidInstance.height = 8;
		invalidInstance.bpp = 8;
	}

	public Material GetMaterial(SsShaderType t)
	{
		if (materials == null)
		{
			return null;
		}
		int num = SsShaderManager.ToSerial(t);
		if (num >= materials.Length)
		{
			return null;
		}
		return materials[num];
	}
}
