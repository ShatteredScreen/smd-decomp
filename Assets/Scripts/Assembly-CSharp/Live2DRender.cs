using System.Collections.Generic;
using UnityEngine;

public class Live2DRender : MonoBehaviour
{
	private Camera mCamera;

	public RenderTexture RenderTexture;

	public int TextureWidth = 512;

	public int TextureHeight = 512;

	public int Depth = 24;

	public Color BackgroundColor = new Color(0.5f, 0.5f, 0.5f, 0f);

	public List<Live2DInstance> InstanceList = new List<Live2DInstance>();

	protected ScreenOrientation mOrientation;

	private int SortByZ(Live2DInstance a, Live2DInstance b)
	{
		return (int)(b.Position.z - a.Position.z);
	}

	public void OnDestroy()
	{
		InstanceList.Clear();
		mCamera.targetTexture = null;
		if (RenderTexture != null)
		{
			if (RenderTexture.IsCreated())
			{
				RenderTexture.Release();
			}
			RenderTexture = null;
		}
	}

	public void Start()
	{
	}

	public void Update()
	{
		if (mOrientation != Util.ScreenOrientation)
		{
			SetLayout(Util.ScreenOrientation);
			mOrientation = Util.ScreenOrientation;
		}
	}

	public void Init(int textureWidth, int textureHeight)
	{
		mCamera = base.gameObject.GetComponent<Camera>();
		TextureWidth = textureWidth;
		TextureHeight = textureHeight;
		ChangeTextureSize(TextureWidth, TextureHeight);
		mCamera.backgroundColor = BackgroundColor;
		mOrientation = Util.ScreenOrientation;
		SetLayout(mOrientation);
	}

	private void ChangeTextureSize(int width, int height)
	{
		RenderTexture = new RenderTexture(width, height, Depth);
		RenderTexture.generateMips = false;
		RenderTexture.useMipMap = false;
		RenderTexture.enableRandomWrite = false;
		RenderTexture.format = RenderTextureFormat.ARGB32;
		mCamera.targetTexture = RenderTexture;
	}

	public void ResisterInstance(Live2DInstance instance)
	{
		InstanceList.Add(instance);
	}

	public void UnresisterInstance(Live2DInstance instance)
	{
		InstanceList.Remove(instance);
	}

	public List<Live2DInstance> GetResisterInstances()
	{
		return InstanceList;
	}

	public bool IsResisterInstance(string _key)
	{
		int i = 0;
		for (int count = InstanceList.Count; i < count; i++)
		{
			if (InstanceList[i].mL2DKey == _key)
			{
				return true;
			}
		}
		return false;
	}

	public bool IsResisterInstanceOne()
	{
		return InstanceList.Count > 0;
	}

	public void OnPostRender()
	{
		InstanceList.Sort(SortByZ);
		foreach (Live2DInstance instance in InstanceList)
		{
			instance.ModelDraw();
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		float num = 1f;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			num = vector.y / 1136f;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			num = vector.y / 1136f;
			break;
		}
		base.transform.localScale = new Vector3(num, num, 1f);
	}
}
