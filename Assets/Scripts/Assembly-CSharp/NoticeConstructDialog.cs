using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

public class NoticeConstructDialog : DialogBase
{
	private enum ROTATE_TARGET
	{
		HEADER = 0,
		FOOTER = 1
	}

	private enum CHECK_STATE : byte
	{
		ON = 0,
		OFF = 1
	}

	private enum PANEL_TYPE
	{
		Backword = 0,
		Forward = 1
	}

	private enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2
	}

	[StructLayout(LayoutKind.Auto)]
	private struct NewMainMapData
	{
		public int MaxLevels { get; set; }

		public long StartTime { get; set; }

		public string Banner { get; set; }
	}

	[StructLayout(LayoutKind.Auto)]
	private struct ShopData
	{
		public int ShopID { get; set; }

		public long Start { get; set; }

		public long End { get; set; }
	}

	private readonly Dictionary<CHECK_STATE, string> CHECK_IMAGE_NAME = new Dictionary<CHECK_STATE, string>
	{
		{
			CHECK_STATE.ON,
			"notice_check"
		},
		{
			CHECK_STATE.OFF,
			"null"
		}
	};

	private readonly Vector4 CHECK_POSITION = new Vector4(-143f, 158f, 330f, 62f);

	private static readonly TimeSpan PREVIOUS_NOTICE_TIME = new TimeSpan(5, 0, 0, 0, 0);

	private static readonly TimeSpan VISIBLE_NOTICE_TIME = new TimeSpan(7, 0, 0, 0, 0);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private SMVerticalVariableList mScrollList;

	private GameObject mGameObject;

	private Color mBackGroundColor = Color.white;

	private BoxCollider mButtonCollider;

	private int mCachedDepth;

	private Dictionary<PANEL_TYPE, UIPanel> mControlPanel = new Dictionary<PANEL_TYPE, UIPanel>();

	private Dictionary<ROTATE_TARGET, Transform> mRotateTarget = new Dictionary<ROTATE_TARGET, Transform>();

	private Dictionary<ROTATE_TARGET, Transform> mRefTarget = new Dictionary<ROTATE_TARGET, Transform>();

	private float mTitleBarHeight;

	private int mPage;

	private GameObject mRoot;

	public bool IsBusy
	{
		get
		{
			return GetBusy() || mState.GetStatus() != STATE.MAIN;
		}
	}

	public bool IsCheckState { get; private set; }

	public int Page
	{
		get
		{
			return mPage;
		}
	}

	public GameObject GameObject
	{
		get
		{
			if (mGameObject == null)
			{
				mGameObject = base.gameObject;
			}
			return mGameObject;
		}
	}

	private void LateUpdate()
	{
		if (mPanel != null && mPanel.depth != mCachedDepth)
		{
			bool flag = true;
			if (mControlPanel.ContainsKey(PANEL_TYPE.Backword) && (bool)mControlPanel[PANEL_TYPE.Backword])
			{
				mControlPanel[PANEL_TYPE.Backword].depth = mPanel.depth;
			}
			else
			{
				flag = false;
			}
			if (mControlPanel.ContainsKey(PANEL_TYPE.Forward) && (bool)mControlPanel[PANEL_TYPE.Forward])
			{
				mControlPanel[PANEL_TYPE.Forward].depth = mPanel.depth + 2;
			}
			else
			{
				flag = false;
			}
			if (mScrollList != null)
			{
				mScrollList.SetDepth(mPanel.depth + 1);
			}
			else
			{
				flag = false;
			}
			if (flag)
			{
				mCachedDepth = mPanel.depth;
			}
		}
		mState.Update();
	}

	public void Init(Color back_ground_color, GameObject root, int start_page = 0)
	{
		mBackGroundColor = back_ground_color;
		mPage = start_page;
		mRoot = root;
		if (mPage != 0)
		{
			return;
		}
		List<InformationManager.AutoNoticeQueueItem> list = new List<InformationManager.AutoNoticeQueueItem>();
		int[] eventID = null;
		Def.SERIES[] series = null;
		long start = 0L;
		long end = 0L;
		KeyValuePair<Def.SERIES, NewMainMapData>[] maxlvls = null;
		ShopData[] id5 = null;
		Action<InformationManager.Info.NoticeConstruct.BannerInfo, int, long, Func<string>> action = delegate(InformationManager.Info.NoticeConstruct.BannerInfo _banner, int _parent, long _start_time, Func<string> _get_inner)
		{
			if (_banner != null && _banner.Header != null && !string.IsNullOrEmpty(_banner.Header.FileName))
			{
				string text2 = string.Empty;
				if (_get_inner != null)
				{
					text2 = _get_inner();
				}
				list.Add(new InformationManager.AutoNoticeQueueItem
				{
					ImageName = ((!string.IsNullOrEmpty(_banner.Header.Atlas)) ? _banner.Header.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
					FileName = _banner.Header.FileName,
					Inner = new SMVerticalVariableList.ItemBase.DescInfo(text2, 20, Color.white, UIWidget.Pivot.Left),
					ID = _parent,
					IgnoreNextSpacing = (_banner.Body != null || _banner.Footer != null),
					StartTime = _start_time,
					Group = _banner.Group,
					Priority = _banner.Priority,
					Location = InformationManager.AutoNoticeQueueItem.LocationType.Header,
					LoadMode = ((!string.IsNullOrEmpty(_banner.Header.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
					ParentID = _parent
				});
			}
		};
		Action<InformationManager.Info.NoticeConstruct.BannerInfo, int, long, Func<string>> action2 = delegate(InformationManager.Info.NoticeConstruct.BannerInfo _banner, int _parent, long _start_time, Func<string> _get_inner)
		{
			if (_banner != null && _banner.Footer != null && !string.IsNullOrEmpty(_banner.Footer.FileName))
			{
				string text = string.Empty;
				if (_get_inner != null)
				{
					text = _get_inner();
				}
				list.Add(new InformationManager.AutoNoticeQueueItem
				{
					ImageName = ((!string.IsNullOrEmpty(_banner.Footer.Atlas)) ? _banner.Footer.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
					FileName = _banner.Footer.FileName,
					Inner = new SMVerticalVariableList.ItemBase.DescInfo(text, 20, Color.white, UIWidget.Pivot.Left),
					ID = _parent,
					ParentID = _parent,
					Group = _banner.Group,
					Priority = _banner.Priority,
					StartTime = _start_time,
					Location = InformationManager.AutoNoticeQueueItem.LocationType.Footer,
					LoadMode = ((!string.IsNullOrEmpty(_banner.Footer.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner)
				});
			}
		};
		Func<ShopLimitedTime, string, string> func = delegate(ShopLimitedTime _shop, string _base_name)
		{
			string empty = string.Empty;
			if (_shop == null)
			{
				return empty;
			}
			empty = _shop.SaleBannerURL;
			if (string.IsNullOrEmpty(empty))
			{
				StringBuilder stringBuilder = new StringBuilder(_base_name);
				if (_shop.ResaleCount != 0)
				{
					stringBuilder.Append("_").Append(_shop.ResaleCount);
				}
				stringBuilder.Append(".png");
				empty = Util.ConvertInfoBannerFileName(string.Format(stringBuilder.ToString(), _shop.ShopItemID.ToString("00")));
			}
			return empty;
		};
		if (CanUpdateApplication())
		{
			int count = list.Count;
			InformationManager.Info.NoticeConstruct.BannerInfo info16 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.Update);
			list.Add(new InformationManager.AutoNoticeQueueItem
			{
				ImageName = ((!string.IsNullOrEmpty(info16.Body.Atlas)) ? info16.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
				FileName = info16.Body.FileName,
				Inner = ((!string.IsNullOrEmpty(info16.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info16.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
				ID = count,
				IgnoreNextSpacing = (info16.Footer != null && !string.IsNullOrEmpty(info16.Footer.FileName)),
				StartTime = 0L,
				Group = info16.Group,
				Priority = info16.Priority,
				Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
				LoadMode = ((!string.IsNullOrEmpty(info16.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
				ClickEvent = delegate(object[] _args)
				{
					if (_args != null)
					{
						MightOpenURL((string)_args[0], (bool)_args[1]);
					}
				},
				ClickEventParam = new object[2]
				{
					string.Format(SingletonMonoBehaviour<Network>.Instance.Store_URL, BIJUnity.getCountryCode()),
					info16.IsExternalSpawnURL
				}
			});
			action(info16, count, 0L, () => string.IsNullOrEmpty(info16.Header.TextKey) ? string.Empty : Localization.Get(info16.Header.TextKey));
			action2(info16, count, 0L, () => string.IsNullOrEmpty(info16.Footer.TextKey) ? string.Empty : Localization.Get(info16.Footer.TextKey));
		}
		if (CanMainEvent(ref eventID) && eventID != null && eventID.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info15 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenMainEvent);
			int[] array = eventID;
			foreach (int id4 in array)
			{
				SeasonEventSettings seasonEventSettings = mGame.EventProfile.SeasonEventList.FirstOrDefault((SeasonEventSettings n) => n.EventID == id4);
				if (seasonEventSettings == null)
				{
					continue;
				}
				DateTime s4 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.StartTime);
				DateTime e3 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.EndTime);
				int count2 = list.Count;
				list.Add(new InformationManager.AutoNoticeQueueItem
				{
					ImageName = ((!string.IsNullOrEmpty(info15.Body.Atlas)) ? info15.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
					FileName = ((!string.IsNullOrEmpty(seasonEventSettings.InfoBannerFileName)) ? seasonEventSettings.InfoBannerFileName : Util.ConvertInfoBannerFileName(new StringBuilder().AppendFormat(info15.Body.FileName, id4).ToString())),
					Inner = ((!string.IsNullOrEmpty(info15.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info15.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
					ID = count2,
					IgnoreNextSpacing = (info15.Footer != null && !string.IsNullOrEmpty(info15.Footer.FileName)),
					StartTime = seasonEventSettings.StartTime,
					Group = info15.Group,
					Priority = info15.Priority,
					Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
					LoadMode = ((!string.IsNullOrEmpty(info15.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
					ClickEvent = delegate(object[] _args)
					{
						if (_args != null)
						{
							MightOpenURL((string)_args[0], (bool)_args[1]);
						}
					},
					ClickEventParam = new object[2] { info15.WebViewURL, info15.IsExternalSpawnURL }
				});
				action(info15, count2, seasonEventSettings.StartTime, () => string.IsNullOrEmpty(info15.Header.TextKey) ? string.Empty : Localization.Get(info15.Header.TextKey));
				action2(info15, count2, seasonEventSettings.StartTime, () => string.IsNullOrEmpty(info15.Footer.TextKey) ? string.Empty : string.Format(Localization.Get(info15.Footer.TextKey), s4.Month, s4.Day, e3.Month, e3.Day, e3.Hour.ToString("00"), e3.Minute.ToString("00")));
			}
		}
		if (CanSaleSpecialGasha(ref eventID) && eventID != null && eventID.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info14 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.SaleSpecialGasha);
			int[] array2 = eventID;
			foreach (int num in array2)
			{
				if (!SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict.ContainsKey(num))
				{
					continue;
				}
				int count3 = list.Count;
				string result_atlas = ((!string.IsNullOrEmpty(info14.Body.Atlas)) ? info14.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL);
				string result_filename = Util.ConvertInfoBannerFileName(string.Format(info14.Body.FileName, num));
				InformationManager.Instance.GetGashaLinkBannerOrElse(num, ref result_atlas, ref result_filename);
				list.Add(new InformationManager.AutoNoticeQueueItem
				{
					ImageName = result_atlas,
					FileName = result_filename,
					Inner = ((!string.IsNullOrEmpty(info14.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info14.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
					ID = count3,
					IgnoreNextSpacing = (info14.Footer != null && !string.IsNullOrEmpty(info14.Footer.FileName)),
					StartTime = SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[num].starttime,
					Group = info14.Group,
					Priority = info14.Priority,
					Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
					LoadMode = ((!string.IsNullOrEmpty(info14.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
					ClickEvent = delegate(object[] _args)
					{
						if (_args != null)
						{
							MightOpenURL((string)_args[0], (bool)_args[1]);
						}
					},
					ClickEventParam = new object[2] { info14.WebViewURL, info14.IsExternalSpawnURL }
				});
				action(info14, count3, SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[num].starttime, () => string.IsNullOrEmpty(info14.Header.TextKey) ? string.Empty : Localization.Get(info14.Header.TextKey));
				DateTime s3 = DateTimeUtil.UnixTimeStampToDateTime(SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[num].starttime);
				DateTime e2 = DateTimeUtil.UnixTimeStampToDateTime(SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[num].endtime);
				action2(info14, count3, SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[num].starttime, () => string.IsNullOrEmpty(info14.Footer.TextKey) ? string.Empty : string.Format(Localization.Get(info14.Footer.TextKey), s3.Month, s3.Day, e2.Month, e2.Day, e2.Hour.ToString("00"), e2.Minute.ToString("00")));
			}
		}
		if (CanAdvPoint(ref eventID) && eventID != null && eventID.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info13 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenAdvPoint);
			DateTime dateTime = DateTimeUtil.Now();
			int[] array3 = eventID;
			foreach (int id3 in array3)
			{
				PointGetEventSettings pointGetEventSettings = mGame.EventProfile.PGEvent.FirstOrDefault((PointGetEventSettings n) => n.EventID == id3);
				if (pointGetEventSettings == null)
				{
					continue;
				}
				int count4 = list.Count;
				DateTime s5 = DateTimeUtil.UnixTimeStampToDateTime(pointGetEventSettings.StartTime);
				DateTime e4 = DateTimeUtil.UnixTimeStampToDateTime(pointGetEventSettings.EndTime);
				LinkBannerDetail linkBannerDetail = pointGetEventSettings.LinkBanner.BannerList.FirstOrDefault((LinkBannerDetail _) => _ != null && !string.IsNullOrEmpty(_.FileName) && _.Param == 0 && _.Action == string.Empty);
				string fileName = Util.ConvertInfoBannerFileName(new StringBuilder().AppendFormat(info13.Body.FileName, id3).ToString());
				if (linkBannerDetail != null)
				{
					fileName = linkBannerDetail.FileName;
				}
				list.Add(new InformationManager.AutoNoticeQueueItem
				{
					ImageName = ((!string.IsNullOrEmpty(info13.Body.Atlas)) ? info13.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
					FileName = fileName,
					Inner = ((!string.IsNullOrEmpty(info13.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info13.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
					ID = count4,
					IgnoreNextSpacing = (info13.Footer != null && !string.IsNullOrEmpty(info13.Footer.FileName)),
					StartTime = pointGetEventSettings.StartTime,
					Group = info13.Group,
					Priority = info13.Priority,
					Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
					LoadMode = ((!string.IsNullOrEmpty(info13.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
					ClickEvent = delegate(object[] _args)
					{
						if (_args != null)
						{
							MightOpenURL((string)_args[0], (bool)_args[1]);
						}
					},
					ClickEventParam = new object[2] { info13.WebViewURL, info13.IsExternalSpawnURL }
				});
				if (pointGetEventSettings.CpTimeSettingsList != null)
				{
					foreach (CpTimeSettings cpTimeSettings in pointGetEventSettings.CpTimeSettingsList)
					{
						if (cpTimeSettings != null)
						{
							DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(cpTimeSettings.StartTime, true);
							DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(cpTimeSettings.EndTime, true);
							if (!(dateTime2 > dateTime) && !(dateTime > dateTime3))
							{
								list.Add(new InformationManager.AutoNoticeQueueItem
								{
									ImageName = ((!string.IsNullOrEmpty(info13.Header.Atlas)) ? info13.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
									FileName = info13.Header.FileName,
									ID = count4,
									IgnoreNextSpacing = true,
									ParentID = count4,
									StartTime = pointGetEventSettings.StartTime,
									Group = info13.Group,
									Priority = info13.Priority,
									Location = InformationManager.AutoNoticeQueueItem.LocationType.Header,
									LoadMode = ((!string.IsNullOrEmpty(info13.Header.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner)
								});
							}
						}
					}
				}
				action2(info13, count4, pointGetEventSettings.StartTime, () => string.IsNullOrEmpty(info13.Footer.TextKey) ? string.Empty : string.Format(Localization.Get(info13.Footer.TextKey), s5.Month, s5.Day, e4.Month, e4.Day, e4.Hour.ToString("00"), e4.Minute.ToString("00")));
			}
		}
		if (CanMainEventNotice(ref eventID) && eventID != null && eventID.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info12 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.NoticeMainEvent);
			if (mGame != null && mGame.EventProfile != null && mGame.EventProfile.SeasonEventList != null)
			{
				int[] array4 = eventID;
				foreach (int id2 in array4)
				{
					SeasonEventSettings seasonEventSettings2 = mGame.EventProfile.SeasonEventList.FirstOrDefault((SeasonEventSettings n) => n != null && n.EventID == id2);
					if (seasonEventSettings2 == null)
					{
						continue;
					}
					int count5 = list.Count;
					list.Add(new InformationManager.AutoNoticeQueueItem
					{
						ImageName = ((!string.IsNullOrEmpty(info12.Body.Atlas)) ? info12.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
						FileName = ((!string.IsNullOrEmpty(seasonEventSettings2.InfoBannerFileName)) ? seasonEventSettings2.InfoBannerFileName : Util.ConvertInfoBannerFileName(string.Format(info12.Body.FileName, id2))),
						Inner = ((!string.IsNullOrEmpty(info12.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info12.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
						ID = count5,
						IgnoreNextSpacing = (info12.Footer != null && !string.IsNullOrEmpty(info12.Footer.FileName)),
						StartTime = seasonEventSettings2.StartTime - (long)PREVIOUS_NOTICE_TIME.TotalSeconds,
						Group = info12.Group,
						Priority = info12.Priority,
						Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
						LoadMode = ((!string.IsNullOrEmpty(info12.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
						ClickEvent = delegate(object[] _args)
						{
							if (_args != null)
							{
								MightOpenURL((string)_args[0], (bool)_args[1]);
							}
						},
						ClickEventParam = new object[2] { info12.WebViewURL, info12.IsExternalSpawnURL }
					});
					action(info12, count5, seasonEventSettings2.StartTime - (long)PREVIOUS_NOTICE_TIME.TotalSeconds, () => string.IsNullOrEmpty(info12.Header.TextKey) ? string.Empty : Localization.Get(info12.Header.TextKey));
					action2(info12, count5, seasonEventSettings2.StartTime - (long)PREVIOUS_NOTICE_TIME.TotalSeconds, () => string.IsNullOrEmpty(info12.Footer.TextKey) ? string.Empty : Localization.Get(info12.Footer.TextKey));
				}
			}
		}
		if (CanAdvPointNotice(ref eventID) && eventID != null && eventID.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info11 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.NoticeAdvPoint);
			if (mGame != null && mGame.EventProfile != null && mGame.EventProfile.PGEvent != null)
			{
				int[] array5 = eventID;
				foreach (int id in array5)
				{
					PointGetEventSettings pointGetEventSettings2 = mGame.EventProfile.PGEvent.FirstOrDefault((PointGetEventSettings n) => n != null && n.EventID == id);
					if (pointGetEventSettings2 == null)
					{
						continue;
					}
					LinkBannerDetail linkBannerDetail2 = pointGetEventSettings2.LinkBanner.BannerList.FirstOrDefault((LinkBannerDetail _) => _ != null && !string.IsNullOrEmpty(_.FileName) && _.Param == 0 && _.Action == string.Empty);
					string fileName2 = Util.ConvertInfoBannerFileName(new StringBuilder().AppendFormat(info11.Body.FileName, id).ToString());
					if (linkBannerDetail2 != null)
					{
						fileName2 = linkBannerDetail2.FileName;
					}
					int count6 = list.Count;
					list.Add(new InformationManager.AutoNoticeQueueItem
					{
						ImageName = ((!string.IsNullOrEmpty(info11.Body.Atlas)) ? info11.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
						FileName = fileName2,
						Inner = ((!string.IsNullOrEmpty(info11.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info11.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
						ID = count6,
						IgnoreNextSpacing = (info11.Footer != null && !string.IsNullOrEmpty(info11.Footer.FileName)),
						StartTime = pointGetEventSettings2.StartTime - (long)PREVIOUS_NOTICE_TIME.TotalSeconds,
						Group = info11.Group,
						Priority = info11.Priority,
						Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
						LoadMode = ((!string.IsNullOrEmpty(info11.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
						ClickEvent = delegate(object[] _args)
						{
							if (_args != null)
							{
								MightOpenURL((string)_args[0], (bool)_args[1]);
							}
						},
						ClickEventParam = new object[2] { info11.WebViewURL, info11.IsExternalSpawnURL }
					});
					action(info11, count6, pointGetEventSettings2.StartTime - (long)PREVIOUS_NOTICE_TIME.TotalSeconds, () => string.IsNullOrEmpty(info11.Header.TextKey) ? string.Empty : Localization.Get(info11.Header.TextKey));
					action2(info11, count6, pointGetEventSettings2.StartTime - (long)PREVIOUS_NOTICE_TIME.TotalSeconds, () => string.IsNullOrEmpty(info11.Footer.TextKey) ? string.Empty : Localization.Get(info11.Footer.TextKey));
				}
			}
		}
		if (CanNewMainMap(ref maxlvls) && maxlvls != null && maxlvls.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info10 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenNewMainMap);
			KeyValuePair<Def.SERIES, NewMainMapData>[] array6 = maxlvls;
			for (int num2 = 0; num2 < array6.Length; num2++)
			{
				KeyValuePair<Def.SERIES, NewMainMapData> keyValuePair = array6[num2];
				string arg = string.Empty;
				switch (keyValuePair.Key)
				{
				case Def.SERIES.SM_R:
					arg = "BM";
					break;
				case Def.SERIES.SM_S:
					arg = "DB";
					break;
				case Def.SERIES.SM_SS:
					arg = "DM";
					break;
				case Def.SERIES.SM_STARS:
					arg = "SS";
					break;
				case Def.SERIES.SM_GF00:
					arg = "GF";
					break;
				}
				int a_main;
				int a_sub;
				Def.SplitStageNo(keyValuePair.Value.MaxLevels, out a_main, out a_sub);
				string fileName3 = string.Format(info10.Body.FileName, arg, a_main);
				if (!string.IsNullOrEmpty(keyValuePair.Value.Banner))
				{
					fileName3 = keyValuePair.Value.Banner;
				}
				int count7 = list.Count;
				list.Add(new InformationManager.AutoNoticeQueueItem
				{
					ImageName = ((!string.IsNullOrEmpty(info10.Body.Atlas)) ? info10.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
					FileName = fileName3,
					Inner = ((!string.IsNullOrEmpty(info10.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info10.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
					ID = count7,
					IgnoreNextSpacing = (info10.Footer != null && !string.IsNullOrEmpty(info10.Footer.FileName)),
					StartTime = keyValuePair.Value.StartTime,
					Group = info10.Group,
					Priority = info10.Priority,
					Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
					LoadMode = ((!string.IsNullOrEmpty(info10.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
					ClickEvent = delegate(object[] _args)
					{
						if (_args != null)
						{
							MightOpenURL((string)_args[0], (bool)_args[1]);
						}
					},
					ClickEventParam = new object[2] { info10.WebViewURL, info10.IsExternalSpawnURL }
				});
				action(info10, count7, keyValuePair.Value.StartTime, () => string.IsNullOrEmpty(info10.Header.TextKey) ? string.Empty : Localization.Get(info10.Header.TextKey));
				action2(info10, count7, keyValuePair.Value.StartTime, () => string.IsNullOrEmpty(info10.Footer.TextKey) ? string.Empty : Localization.Get(info10.Footer.TextKey));
			}
		}
		if (CanSaleRecommend(ref id5) && id5 != null && id5.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info9 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.SaleRecommendSet);
			if (mGame != null && mGame.SegmentProfile != null && mGame.SegmentProfile.ShopSetItemList != null)
			{
				ShopData[] array7 = id5;
				for (int num3 = 0; num3 < array7.Length; num3++)
				{
					ShopData sd2 = array7[num3];
					ShopLimitedTime shop2 = mGame.SegmentProfile.ShopSetItemList.FirstOrDefault((ShopLimitedTime n) => n != null && n.ShopItemID == sd2.ShopID && n.StartTime == sd2.Start && n.EndTime == sd2.End);
					if (shop2 == null)
					{
						continue;
					}
					int count8 = list.Count;
					list.Add(new InformationManager.AutoNoticeQueueItem
					{
						ImageName = ((!string.IsNullOrEmpty(info9.Body.Atlas)) ? info9.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
						FileName = func(shop2, info9.Body.FileName),
						Inner = ((!string.IsNullOrEmpty(info9.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info9.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
						ID = count8,
						IgnoreNextSpacing = (info9.Footer != null && !string.IsNullOrEmpty(info9.Footer.FileName)),
						StartTime = shop2.StartTime,
						Group = info9.Group,
						Priority = info9.Priority,
						Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
						LoadMode = ((!string.IsNullOrEmpty(info9.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
						ClickEvent = delegate(object[] _args)
						{
							if (_args != null)
							{
								MightOpenURL((string)_args[0], (bool)_args[1]);
							}
						},
						ClickEventParam = new object[2] { info9.WebViewURL, info9.IsExternalSpawnURL }
					});
					action(info9, count8, shop2.StartTime, () => string.IsNullOrEmpty(info9.Header.TextKey) ? string.Empty : Localization.Get(info9.Header.TextKey));
					action2(info9, count8, shop2.StartTime, delegate
					{
						if (string.IsNullOrEmpty(info9.Footer.TextKey))
						{
							return string.Empty;
						}
						DateTime dateTime10 = DateTimeUtil.UnixTimeStampToDateTime(shop2.StartTime);
						DateTime dateTime11 = DateTimeUtil.UnixTimeStampToDateTime(shop2.EndTime);
						return string.Format(Localization.Get(info9.Footer.TextKey), dateTime10.Month, dateTime10.Day, dateTime11.Month, dateTime11.Day, dateTime11.Hour.ToString("00"), dateTime11.Minute.ToString("00"));
					});
				}
			}
		}
		if (CanSaleNonRecommend(ref id5) && id5 != null && id5.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info8 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.SaleNonRecommendSet);
			if (mGame != null && mGame.SegmentProfile != null && mGame.SegmentProfile.ShopSetItemList != null)
			{
				ShopData[] array8 = id5;
				for (int num4 = 0; num4 < array8.Length; num4++)
				{
					ShopData sd = array8[num4];
					ShopLimitedTime shop = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => n != null && n.ShopItemID == sd.ShopID && n.StartTime == sd.Start && n.EndTime == sd.End);
					if (shop == null)
					{
						continue;
					}
					int count9 = list.Count;
					list.Add(new InformationManager.AutoNoticeQueueItem
					{
						ImageName = ((!string.IsNullOrEmpty(info8.Body.Atlas)) ? info8.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
						FileName = func(shop, info8.Body.FileName),
						Inner = ((!string.IsNullOrEmpty(info8.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info8.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
						ID = count9,
						IgnoreNextSpacing = (info8.Footer != null && !string.IsNullOrEmpty(info8.Footer.FileName)),
						StartTime = shop.StartTime,
						Group = info8.Group,
						Priority = info8.Priority,
						Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
						LoadMode = ((!string.IsNullOrEmpty(info8.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
						ClickEvent = delegate(object[] _args)
						{
							if (_args != null)
							{
								MightOpenURL((string)_args[0], (bool)_args[1]);
							}
						},
						ClickEventParam = new object[2] { info8.WebViewURL, info8.IsExternalSpawnURL }
					});
					action(info8, count9, shop.StartTime, () => string.IsNullOrEmpty(info8.Header.TextKey) ? string.Empty : Localization.Get(info8.Header.TextKey));
					action2(info8, count9, shop.StartTime, delegate
					{
						if (string.IsNullOrEmpty(info8.Footer.TextKey))
						{
							return string.Empty;
						}
						DateTime dateTime8 = DateTimeUtil.UnixTimeStampToDateTime(shop.StartTime);
						DateTime dateTime9 = DateTimeUtil.UnixTimeStampToDateTime(shop.EndTime);
						return string.Format(Localization.Get(info8.Footer.TextKey), dateTime8.Month, dateTime8.Day, dateTime9.Month, dateTime9.Day, dateTime9.Hour.ToString("00"), dateTime9.Minute.ToString("00"));
					});
				}
			}
		}
		if (CanOpenSeriesCampaign(ref series) && series != null && series.Length > 0)
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info7 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenSeriesCampaign);
			Def.SERIES[] array9 = series;
			foreach (Def.SERIES s2 in array9)
			{
				SeriesCampaignSetting setting = mGame.EventProfile.SeriesCampaignList.FirstOrDefault((SeriesCampaignSetting n) => n.Series == s2);
				if (setting == null)
				{
					continue;
				}
				string arg2 = "sc";
				switch (s2)
				{
				case Def.SERIES.SM_FIRST:
					arg2 = "DK";
					break;
				case Def.SERIES.SM_R:
					arg2 = "BM";
					break;
				case Def.SERIES.SM_S:
					arg2 = "DB";
					break;
				case Def.SERIES.SM_SS:
					arg2 = "DM";
					break;
				case Def.SERIES.SM_STARS:
					arg2 = "SS";
					break;
				case Def.SERIES.SM_GF00:
					arg2 = "GF";
					break;
				}
				string imageName = ((!string.IsNullOrEmpty(info7.Body.Atlas)) ? info7.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL);
				string fileName4 = info7.Body.FileName;
				if (!string.IsNullOrEmpty(setting.BannerFileName))
				{
					fileName4 = setting.BannerFileName;
					imageName = InformationManager.Instance.NoticeConstruct_BaseURL;
				}
				int count10 = list.Count;
				list.Add(new InformationManager.AutoNoticeQueueItem
				{
					ImageName = imageName,
					FileName = fileName4,
					Inner = ((!string.IsNullOrEmpty(info7.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info7.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
					ID = count10,
					IgnoreNextSpacing = (info7.Footer != null && !string.IsNullOrEmpty(info7.Footer.FileName)),
					StartTime = setting.StartTime,
					Group = info7.Group,
					Priority = info7.Priority,
					Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
					LoadMode = ((!string.IsNullOrEmpty(info7.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
					ClickEvent = delegate(object[] _args)
					{
						if (_args != null)
						{
							MightOpenURL((string)_args[0], (bool)_args[1]);
						}
					},
					ClickEventParam = new object[2]
					{
						string.Format(info7.WebViewURL, arg2),
						info7.IsExternalSpawnURL
					}
				});
				action(info7, count10, setting.StartTime, () => string.IsNullOrEmpty(info7.Header.TextKey) ? string.Empty : Localization.Get(info7.Header.TextKey));
				action2(info7, count10, setting.StartTime, delegate
				{
					if (string.IsNullOrEmpty(info7.Footer.TextKey))
					{
						return string.Empty;
					}
					DateTime dateTime6 = DateTimeUtil.UnixTimeStampToDateTime(setting.StartTime);
					DateTime dateTime7 = DateTimeUtil.UnixTimeStampToDateTime(setting.EndTime);
					return string.Format(Localization.Get(info7.Footer.TextKey), dateTime6.Month, dateTime6.Day, dateTime7.Month, dateTime7.Day, dateTime7.Hour.ToString("00"), dateTime7.Minute.ToString("00"));
				});
			}
		}
		if (CanOpenDailyChallenge())
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info6 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenDailyChallenge);
			int count11 = list.Count;
			list.Add(new InformationManager.AutoNoticeQueueItem
			{
				ImageName = ((!string.IsNullOrEmpty(info6.Body.Atlas)) ? info6.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
				FileName = info6.Body.FileName,
				Inner = ((!string.IsNullOrEmpty(info6.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info6.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
				ID = count11,
				IgnoreNextSpacing = (info6.Footer != null && !string.IsNullOrEmpty(info6.Footer.FileName)),
				StartTime = GlobalVariables.Instance.SelectedDailyChallengeSetting.StartTime,
				Group = info6.Group,
				Priority = info6.Priority,
				Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
				LoadMode = ((!string.IsNullOrEmpty(info6.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
				ClickEvent = delegate(object[] _args)
				{
					if (_args != null)
					{
						MightOpenURL((string)_args[0], (bool)_args[1]);
					}
				},
				ClickEventParam = new object[2] { info6.WebViewURL, info6.IsExternalSpawnURL }
			});
			action(info6, count11, GlobalVariables.Instance.SelectedDailyChallengeSetting.StartTime, () => string.IsNullOrEmpty(info6.Header.TextKey) ? string.Empty : Localization.Get(info6.Header.TextKey));
			action2(info6, count11, GlobalVariables.Instance.SelectedDailyChallengeSetting.StartTime, delegate
			{
				if (string.IsNullOrEmpty(info6.Footer.TextKey))
				{
					return string.Empty;
				}
				DateTime dateTime4 = DateTimeUtil.UnixTimeStampToDateTime(GlobalVariables.Instance.SelectedDailyChallengeSetting.StartTime);
				DateTime dateTime5 = DateTimeUtil.UnixTimeStampToDateTime(GlobalVariables.Instance.SelectedDailyChallengeSetting.EndTime);
				return string.Format(Localization.Get(info6.Footer.TextKey), dateTime4.Month, dateTime4.Day, dateTime5.Month, dateTime5.Day, dateTime5.Hour.ToString("00"), dateTime5.Minute.ToString("00"));
			});
		}
		if (CanOpenGuerrilla())
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info5 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.Guerrilla);
			int count12 = list.Count;
			list.Add(new InformationManager.AutoNoticeQueueItem
			{
				ImageName = ((!string.IsNullOrEmpty(info5.Body.Atlas)) ? info5.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
				FileName = info5.Body.FileName,
				Inner = ((!string.IsNullOrEmpty(info5.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info5.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
				ID = count12,
				IgnoreNextSpacing = (info5.Footer != null && !string.IsNullOrEmpty(info5.Footer.FileName)),
				StartTime = 0L,
				Group = info5.Group,
				Priority = info5.Priority,
				Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
				LoadMode = ((!string.IsNullOrEmpty(info5.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
				ClickEvent = delegate(object[] _args)
				{
					if (_args != null)
					{
						MightOpenURL((string)_args[0], (bool)_args[1]);
					}
				},
				ClickEventParam = new object[2] { info5.WebViewURL, info5.IsExternalSpawnURL }
			});
			action(info5, count12, 0L, () => string.IsNullOrEmpty(info5.Header.TextKey) ? string.Empty : Localization.Get(info5.Header.TextKey));
			action2(info5, count12, 0L, () => string.IsNullOrEmpty(info5.Footer.TextKey) ? string.Empty : Localization.Get(info5.Footer.TextKey));
		}
		if (CanReplacementTrophy())
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info4 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.TrophyExchange);
			int count13 = list.Count;
			start = (long)(SingletonMonoBehaviour<GameMain>.Instance.EventProfile.GetTrophyItemReplenishTime() - DateTimeUtil.UnitLocalTimeEpoch).TotalSeconds;
			list.Add(new InformationManager.AutoNoticeQueueItem
			{
				ImageName = ((!string.IsNullOrEmpty(info4.Body.Atlas)) ? info4.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
				FileName = info4.Body.FileName,
				Inner = ((!string.IsNullOrEmpty(info4.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info4.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
				ID = count13,
				IgnoreNextSpacing = (info4.Footer != null && !string.IsNullOrEmpty(info4.Footer.FileName)),
				StartTime = start,
				Group = info4.Group,
				Priority = info4.Priority,
				Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
				LoadMode = ((!string.IsNullOrEmpty(info4.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
				ClickEvent = delegate(object[] _args)
				{
					if (_args != null)
					{
						MightOpenURL((string)_args[0], (bool)_args[1]);
					}
				},
				ClickEventParam = new object[2] { info4.WebViewURL, info4.IsExternalSpawnURL }
			});
			action(info4, count13, start, () => string.IsNullOrEmpty(info4.Header.TextKey) ? string.Empty : Localization.Get(info4.Header.TextKey));
			action2(info4, count13, start, () => string.IsNullOrEmpty(info4.Footer.TextKey) ? string.Empty : Localization.Get(info4.Footer.TextKey));
		}
		if (CanSaleMugenHeart(ref start, ref end))
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info3 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.MugenHeart);
			bool toLocal = false;
			DateTime s = DateTimeUtil.UnixTimeStampToDateTime(start, toLocal);
			DateTime e = DateTimeUtil.UnixTimeStampToDateTime(end, toLocal);
			int count14 = list.Count;
			list.Add(new InformationManager.AutoNoticeQueueItem
			{
				ImageName = ((!string.IsNullOrEmpty(info3.Body.Atlas)) ? info3.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
				FileName = info3.Body.FileName,
				Inner = ((!string.IsNullOrEmpty(info3.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info3.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
				ID = count14,
				IgnoreNextSpacing = (info3.Footer != null && !string.IsNullOrEmpty(info3.Footer.FileName)),
				StartTime = start,
				Group = info3.Group,
				Priority = info3.Priority,
				Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
				LoadMode = ((!string.IsNullOrEmpty(info3.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
				ClickEvent = delegate(object[] _args)
				{
					if (_args != null)
					{
						MightOpenURL((string)_args[0], (bool)_args[1]);
					}
				},
				ClickEventParam = new object[2] { info3.WebViewURL, info3.IsExternalSpawnURL }
			});
			action(info3, count14, start, () => string.IsNullOrEmpty(info3.Header.TextKey) ? string.Empty : Localization.Get(info3.Header.TextKey));
			action2(info3, count14, start, () => string.IsNullOrEmpty(info3.Footer.TextKey) ? string.Empty : string.Format(Localization.Get(info3.Footer.TextKey), s.Month, s.Day, e.Month, e.Day, e.Hour.ToString("00"), e.Minute.ToString("00")));
		}
		if (CanAddNewOldEvent(ref start))
		{
			InformationManager.Info.NoticeConstruct.BannerInfo info2 = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OldEvent);
			int count15 = list.Count;
			list.Add(new InformationManager.AutoNoticeQueueItem
			{
				ImageName = ((!string.IsNullOrEmpty(info2.Body.Atlas)) ? info2.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
				FileName = info2.Body.FileName,
				Inner = ((!string.IsNullOrEmpty(info2.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info2.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
				ID = count15,
				IgnoreNextSpacing = (info2.Footer != null && !string.IsNullOrEmpty(info2.Footer.FileName)),
				StartTime = start,
				Group = info2.Group,
				Priority = info2.Priority,
				Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
				LoadMode = ((!string.IsNullOrEmpty(info2.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
				ClickEvent = delegate(object[] _args)
				{
					if (_args != null)
					{
						MightOpenURL((string)_args[0], (bool)_args[1]);
					}
				},
				ClickEventParam = new object[2] { info2.WebViewURL, info2.IsExternalSpawnURL }
			});
			action(info2, count15, start, () => string.IsNullOrEmpty(info2.Header.TextKey) ? string.Empty : Localization.Get(info2.Header.TextKey));
			action2(info2, count15, start, () => string.IsNullOrEmpty(info2.Footer.TextKey) ? string.Empty : Localization.Get(info2.Footer.TextKey));
		}
		if (CanUnique())
		{
			DateTime now = DateTimeUtil.Now();
			InformationManager.Info.NoticeConstruct.UniqueBannerInfo[] array10 = (from _ in InformationManager.Instance.GetNoticeConstructUniqueInfo()
				where _ != null && _.StartTime <= now && now <= _.EndTime && _.Force == null
				select _).ToArray();
			if (array10 != null)
			{
				InformationManager.Info.NoticeConstruct.UniqueBannerInfo[] array11 = array10;
				InformationManager.Info.NoticeConstruct.UniqueBannerInfo info;
				for (int num6 = 0; num6 < array11.Length; num6++)
				{
					info = array11[num6];
					if (info.IsForceHide)
					{
						continue;
					}
					int count16 = list.Count;
					list.Add(new InformationManager.AutoNoticeQueueItem
					{
						ImageName = ((!string.IsNullOrEmpty(info.Body.Atlas)) ? info.Body.Atlas : InformationManager.Instance.NoticeConstruct_BaseURL),
						FileName = info.Body.FileName,
						Inner = ((!string.IsNullOrEmpty(info.Body.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(info.Body.TextKey), 20, Color.white, UIWidget.Pivot.Center) : null),
						ID = count16,
						IgnoreNextSpacing = (info.Footer != null && !string.IsNullOrEmpty(info.Footer.FileName)),
						StartTime = info.StartUnixTime,
						Group = info.Group,
						Priority = info.Priority,
						Location = InformationManager.AutoNoticeQueueItem.LocationType.Body,
						LoadMode = ((!string.IsNullOrEmpty(info.Body.Atlas)) ? InformationManager.AutoNoticeQueueItem.LoadModeType.Atlas : InformationManager.AutoNoticeQueueItem.LoadModeType.Banner),
						ClickEvent = delegate(object[] _args)
						{
							if (_args != null)
							{
								MightOpenURL((string)_args[0], (bool)_args[1]);
							}
						},
						ClickEventParam = new object[2] { info.WebViewURL, info.IsExternalSpawnURL }
					});
					action(info, count16, info.StartUnixTime, () => string.IsNullOrEmpty(info.Header.TextKey) ? string.Empty : Localization.Get(info.Header.TextKey));
					action2(info, count16, info.StartUnixTime, () => string.IsNullOrEmpty(info.Footer.TextKey) ? string.Empty : Localization.Get(info.Footer.TextKey));
				}
			}
		}
		InformationManager.Instance.EnqueueAutoNoticeWithoutForce(list.ToArray());
	}

	public override void BuildDialog()
	{
		UISprite uISprite = null;
		UIPanel uIPanel = null;
		UIWidget uIWidget = null;
		GameObject gameObject = null;
		GameObject gameObject2 = null;
		string text = null;
		DateTime now = DateTimeUtil.Now();
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		Vector2 vector = Util.LogScreenSize();
		InformationManager.AutoNoticeQueueItem[] array = InformationManager.Instance.DequeueAutoNoticeWithoutForce(mPage + 1 < InformationManager.Instance.NoticeConstruct_MaxPage);
		bool isLastpage = mPage + 1 >= InformationManager.Instance.NoticeConstruct_MaxPage || InformationManager.Instance.AutoNoticeQueueCount == 0;
		SetJellyTargetScale(1f);
		mRefTarget.Add(ROTATE_TARGET.HEADER, Util.CreateAnchorWithChild("AnchorT", GameObject, UIAnchor.Side.Top, component).transform);
		mRefTarget.Add(ROTATE_TARGET.FOOTER, Util.CreateAnchorWithChild("AnchorB", GameObject, UIAnchor.Side.Bottom, component).transform);
		Queue<SMVerticalVariableList.ItemBase> queue = new Queue<SMVerticalVariableList.ItemBase>();
		uIPanel = Util.CreatePanel("Backward", GameObject, mBaseDepth, mPanel.sortingOrder);
		gameObject = uIPanel.gameObject;
		mControlPanel.Add(PANEL_TYPE.Backword, uIPanel);
		uISprite = Util.CreateSprite("BackGround", gameObject, image);
		Util.SetSpriteInfo(uISprite, "bg00w", mBaseDepth, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetAnchor(gameObject, 0, 0, 0, 0);
		uISprite.color = mBackGroundColor;
		Action<InformationManager.Info.NoticeConstruct.UniqueBannerInfo[]> action = delegate(InformationManager.Info.NoticeConstruct.UniqueBannerInfo[] _info)
		{
			foreach (InformationManager.Info.NoticeConstruct.UniqueBannerInfo uniqueBannerInfo in _info)
			{
				if (uniqueBannerInfo != null && !uniqueBannerInfo.IsForceHide)
				{
					InformationManager.Info.NoticeConstruct.BannerInfo.ImageInfo[] array3 = new InformationManager.Info.NoticeConstruct.BannerInfo.ImageInfo[3] { uniqueBannerInfo.Header, uniqueBannerInfo.Body, uniqueBannerInfo.Footer };
					int count = queue.Count;
					InformationManager.Info.NoticeConstruct.BannerInfo.ImageInfo[] array4 = array3;
					foreach (InformationManager.Info.NoticeConstruct.BannerInfo.ImageInfo imageInfo in array4)
					{
						if (imageInfo != null)
						{
							SMVerticalVariableList.ItemBase itemBase = null;
							itemBase = ((!string.IsNullOrEmpty(imageInfo.Atlas)) ? ((SMVerticalVariableList.ItemBase)new SMVerticalVariableList.AtlasItem
							{
								AtlasName = imageInfo.Atlas
							}) : ((SMVerticalVariableList.ItemBase)new SMVerticalVariableList.BannerItem
							{
								BannerURL = InformationManager.Instance.NoticeConstruct_BaseURL
							}));
							itemBase.FileName = imageInfo.FileName;
							itemBase.Inner = ((!string.IsNullOrEmpty(imageInfo.TextKey)) ? new SMVerticalVariableList.ItemBase.DescInfo(Localization.Get(imageInfo.TextKey), 20, Color.white, UIWidget.Pivot.Left) : null);
							itemBase.ID = count;
							if (imageInfo == uniqueBannerInfo.Header)
							{
								if (uniqueBannerInfo.Body != null || uniqueBannerInfo.Footer != null)
								{
									itemBase.IgnoreNextSpacing = true;
								}
							}
							else if (imageInfo == uniqueBannerInfo.Body)
							{
								itemBase.ClickEvent = delegate(object[] _args)
								{
									if (_args != null)
									{
										MightOpenURL((string)_args[0], (bool)_args[1]);
									}
								};
								itemBase.ClickEventParam = new object[2] { uniqueBannerInfo.WebViewURL, uniqueBannerInfo.IsExternalSpawnURL };
								if (uniqueBannerInfo.Footer != null)
								{
									itemBase.IgnoreNextSpacing = true;
								}
							}
							queue.Enqueue(itemBase);
						}
					}
				}
			}
		};
		InformationManager.Info.NoticeConstruct.UniqueBannerInfo[] obj = (from _ in InformationManager.Instance.GetNoticeConstructUniqueInfo()
			where _ != null && ((_.StartTime <= now && now <= _.EndTime) || _.IsForceShow) && _.Force != null && _.Force.LocationValue == InformationManager.Info.NoticeConstruct.UniqueBannerInfo.ForceInfo.LocationType.Header && ((_.Force.IsFirstPage && mPage == 0) || (_.Force.IsLastPage && isLastpage) || _.Force.Page == mPage)
			select _).ToArray();
		action(obj);
		if (array != null)
		{
			InformationManager.AutoNoticeQueueItem[] array2 = array;
			foreach (InformationManager.AutoNoticeQueueItem autoNoticeQueueItem in array2)
			{
				if (autoNoticeQueueItem != null)
				{
					if (autoNoticeQueueItem.LoadMode == InformationManager.AutoNoticeQueueItem.LoadModeType.Banner)
					{
						queue.Enqueue(new SMVerticalVariableList.BannerItem
						{
							BannerURL = autoNoticeQueueItem.ImageName,
							FileName = autoNoticeQueueItem.FileName,
							Inner = autoNoticeQueueItem.Inner,
							ID = queue.Count,
							ClickEvent = autoNoticeQueueItem.ClickEvent,
							ClickEventParam = autoNoticeQueueItem.ClickEventParam,
							IgnoreNextSpacing = autoNoticeQueueItem.IgnoreNextSpacing
						});
					}
					else
					{
						queue.Enqueue(new SMVerticalVariableList.AtlasItem
						{
							AtlasName = autoNoticeQueueItem.ImageName,
							FileName = autoNoticeQueueItem.FileName,
							Inner = autoNoticeQueueItem.Inner,
							ID = queue.Count,
							ClickEvent = autoNoticeQueueItem.ClickEvent,
							ClickEventParam = autoNoticeQueueItem.ClickEventParam,
							IgnoreNextSpacing = autoNoticeQueueItem.IgnoreNextSpacing
						});
					}
				}
			}
		}
		InformationManager.Info.NoticeConstruct.UniqueBannerInfo[] obj2 = (from _ in InformationManager.Instance.GetNoticeConstructUniqueInfo()
			where _ != null && ((_.StartTime <= now && now <= _.EndTime) || _.IsForceShow) && _.Force != null && _.Force.LocationValue == InformationManager.Info.NoticeConstruct.UniqueBannerInfo.ForceInfo.LocationType.Footer && ((_.Force.IsFirstPage && mPage == 0) || (_.Force.IsLastPage && isLastpage) || _.Force.Page == mPage)
			select _).ToArray();
		action(obj2);
		bool flag = InformationManager.Instance.AutoNoticeQueueCount == 0;
		Vector4 scalingXYWH_Portrait = Vector4.zero;
		Vector4 scalingXYWH_Landscape = Vector4.zero;
		float num = (flag ? 25 : 0);
		float num2 = ((!flag) ? 75 : 0);
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopLeft, mOrientation);
		Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomRight, mOrientation);
		float num3 = 1f;
		if (Util.IsWideScreen())
		{
			float num4 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
			float num5 = 0.5625f;
			float num6 = 0.4617052f;
			num3 = 1f - (num5 - num4) / (num5 - num6) + 1f;
		}
		float num7 = 1.7044334f;
		switch (mOrientation)
		{
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (Util.IsWideScreen())
			{
				float z5 = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y) - 11f * num7;
				float num12 = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x) - -50f * num7;
				float z6 = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x);
				float num13 = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y);
				float y5 = ((!flag) ? (10f * num3) : 35f);
				float y6 = ((!flag) ? (36f * num3 + -52f * (num3 - 1f)) : 61f);
				float num14 = (float)((!flag) ? (-80) : (-130)) * num3 + -155f * (num3 - 1f);
				float num15 = (float)((!flag) ? (-178) : (-230)) * num3 + 127f * (num3 - 1f);
				scalingXYWH_Portrait = new Vector4(0f, y5, z5, num12 + num14);
				scalingXYWH_Landscape = new Vector4(0f, y6, z6, num13 + num15);
			}
			else
			{
				float z7 = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y);
				float w3 = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x) - (float)((!flag) ? 211 : 261);
				float z8 = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x);
				float w4 = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y) - (float)((!flag) ? 211 : 261);
				float y7 = ((!flag) ? 20 : 45);
				float y8 = ((!flag) ? 20 : 45);
				scalingXYWH_Portrait = new Vector4(0f, y7, z7, w3);
				scalingXYWH_Landscape = new Vector4(0f, y8, z8, w4);
			}
			break;
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (Util.IsWideScreen())
			{
				float z = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x);
				float num8 = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y);
				float z2 = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y) - 72f * num7;
				float num9 = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x) - -11f * num7;
				float y = (float)((!flag) ? 10 : 35) * num3;
				float y2 = (float)((!flag) ? 36 : 61) * num3 + -52f * (num3 - 1f);
				float num10 = (float)((!flag) ? (-80) : (-130)) * num3 + -65f * (num3 - 1f);
				float num11 = (float)((!flag) ? (-178) : (-230)) * num3 + 117f * (num3 - 1f);
				scalingXYWH_Portrait = new Vector4(0f, y, z, num8 + num10);
				scalingXYWH_Landscape = new Vector4(0f, y2, z2, num9 + num11);
			}
			else
			{
				float z3 = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x);
				float w = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y) - (float)((!flag) ? 211 : 261);
				float z4 = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y);
				float w2 = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x) - (float)((!flag) ? 211 : 261);
				float y3 = ((!flag) ? 20 : 45);
				float y4 = ((!flag) ? 20 : 45);
				scalingXYWH_Portrait = new Vector4(0f, y3, z3, w);
				scalingXYWH_Landscape = new Vector4(0f, y4, z4, w2);
			}
			break;
		}
		mScrollList = SMVerticalVariableList.Make(new SMVerticalVariableList.MakeConfig
		{
			Parent = GameObject,
			Depth = mBaseDepth + 1,
			SortingOrder = mPanel.sortingOrder,
			Spacing = 10f,
			Item = queue.ToArray(),
			Anchor = SMVerticalVariableList.MakeAnchor.Scaling,
			ScalingXYWH_Portrait = scalingXYWH_Portrait,
			ScalingXYWH_Landscape = scalingXYWH_Landscape,
			HideScrollBar = true,
			InitOrientation = mOrientation,
			FuncGetRealtimeScaleOffset = () => new Vector2(0f - mRefTarget.Sum((KeyValuePair<ROTATE_TARGET, Transform> n) => Mathf.Abs(n.Value.localPosition.x)), 0f - mRefTarget.Sum((KeyValuePair<ROTATE_TARGET, Transform> n) => Mathf.Abs(n.Value.localPosition.y))),
			TextureMaxWidth = InformationManager.Instance.NoticeConstruct_TextureMaxWidth,
			IsClip = false
		});
		uIPanel = Util.CreatePanel("Forward", GameObject, mBaseDepth + 2, mPanel.sortingOrder + 2);
		mControlPanel.Add(PANEL_TYPE.Forward, uIPanel);
		gameObject = uIPanel.gameObject;
		uIWidget = Util.CreateGameObject("Header", gameObject).AddComponent<UIWidget>();
		uIWidget.depth = mBaseDepth + 2;
		uIWidget.pivot = UIWidget.Pivot.Top;
		uIWidget.SetLocalPosition(0f, 618f);
		gameObject2 = uIWidget.gameObject;
		if (!mRotateTarget.ContainsKey(ROTATE_TARGET.HEADER))
		{
			mRotateTarget.Add(ROTATE_TARGET.HEADER, gameObject2.transform);
		}
		else
		{
			mRotateTarget[ROTATE_TARGET.HEADER] = gameObject2.transform;
		}
		uISprite = Util.CreateSprite("Right", gameObject2, image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, 123f), Vector3.one, false, UIWidget.Pivot.BottomLeft);
		uISprite.SetLocalScaleY(-1f);
		mTitleBarHeight = uISprite.height;
		uISprite = Util.CreateSprite("Left", gameObject2, image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(2f, 123f), Vector3.one, false, UIWidget.Pivot.BottomLeft);
		uISprite.SetLocalScale(-1f, -1f, 1f);
		int fontSize = 38;
		UILabel uILabel = Util.CreateLabel("Title", gameObject2, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Webview_Title"), mBaseDepth + 3, new Vector3(0f, -50f), fontSize, 0, 0, UIWidget.Pivot.Top);
		uILabel.transform.SubLocalPositionY(9f);
		uIWidget = Util.CreateGameObject("Footer", gameObject).AddComponent<UIWidget>();
		uIWidget.depth = mBaseDepth + 2;
		uIWidget.pivot = UIWidget.Pivot.Bottom;
		uIWidget.SetLocalPosition(0f, -581f);
		gameObject2 = uIWidget.gameObject;
		if (!mRotateTarget.ContainsKey(ROTATE_TARGET.FOOTER))
		{
			mRotateTarget.Add(ROTATE_TARGET.FOOTER, gameObject2.transform);
		}
		else
		{
			mRotateTarget[ROTATE_TARGET.FOOTER] = gameObject2.transform;
		}
		uISprite = Util.CreateSprite("Right", gameObject2, image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, -123f), Vector3.one, false, UIWidget.Pivot.BottomLeft);
		uISprite = Util.CreateSprite("Left", gameObject2, image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(2f, -123f), Vector3.one, false, UIWidget.Pivot.BottomLeft);
		uISprite.SetLocalScaleX(-1f);
		UIButton uIButton = null;
		if (flag)
		{
			Vector4 cHECK_POSITION = CHECK_POSITION;
			float y9 = cHECK_POSITION.y;
			Vector4 cHECK_POSITION2 = CHECK_POSITION;
			float num16 = (y9 + cHECK_POSITION2.w * 0.5f) * 0.5f;
			uISprite = Util.CreateSprite("BackGround", gameObject2, image);
			Util.SetSpriteInfo(uISprite, "bg00w", mBaseDepth + 1, new Vector3(0f, num16), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Tiled;
			uISprite.color = mBackGroundColor;
			uISprite.SetDimensions(1384, (int)(num16 * 2f));
			uISprite.autoResizeBoxCollider = true;
			BoxCollider boxCollider = uISprite.GetComponent<BoxCollider>();
			if (boxCollider == null)
			{
				boxCollider = uISprite.gameObject.AddComponent<BoxCollider>();
			}
			boxCollider.center = Vector3.zero;
			BoxCollider boxCollider2 = boxCollider;
			Vector4 cHECK_POSITION3 = CHECK_POSITION;
			boxCollider2.size = new Vector3(1384f, cHECK_POSITION3.w);
			uISprite.ResizeCollider();
			IsCheckState = true;
			text = CHECK_IMAGE_NAME[CHECK_STATE.ON];
			uIButton = Util.CreateJellyImageButton("ButtonPositive", gameObject2, image);
			UIButton button = uIButton;
			string normal = text;
			string hover = text;
			string push = text;
			int depth = mBaseDepth + 4;
			Vector4 cHECK_POSITION4 = CHECK_POSITION;
			float x = cHECK_POSITION4.x;
			Vector4 cHECK_POSITION5 = CHECK_POSITION;
			Util.SetImageButtonInfo(button, normal, hover, push, depth, new Vector3(x, cHECK_POSITION5.y), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnTogglePushed", UIButtonMessage.Trigger.OnClick);
			mButtonCollider = uIButton.GetComponent<BoxCollider>();
			if ((bool)mButtonCollider)
			{
				mButtonCollider.center = new Vector3(115f, 0f);
				BoxCollider boxCollider3 = mButtonCollider;
				Vector4 cHECK_POSITION6 = CHECK_POSITION;
				float z9 = cHECK_POSITION6.z;
				Vector4 cHECK_POSITION7 = CHECK_POSITION;
				boxCollider3.size = new Vector3(z9, cHECK_POSITION7.w, 1f);
			}
			else
			{
				uIButton.gameObject.AddComponent<BoxCollider>();
			}
			text = string.Format(Localization.Get("Webview_Checkbox_Desc"));
			UILabel uILabel2 = Util.CreateLabel("Checkbox_Desc", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel2, text, mBaseDepth + 3, new Vector3(40f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
			uILabel2.color = Def.DEFAULT_MESSAGE_COLOR;
			uISprite = Util.CreateSprite("Check", gameObject2, image);
			UISprite sprite = uISprite;
			int depth2 = mBaseDepth + 3;
			Vector4 cHECK_POSITION8 = CHECK_POSITION;
			float x2 = cHECK_POSITION8.x;
			Vector4 cHECK_POSITION9 = CHECK_POSITION;
			Util.SetSpriteInfo(sprite, "notice_check_panel", depth2, new Vector3(x2, cHECK_POSITION9.y), Vector3.one, false);
			text = "LC_button_close2";
		}
		else
		{
			text = "LC_button_next";
		}
		uIButton = Util.CreateJellyImageButton("CloseButton", gameObject2, image);
		Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 3, new Vector3(0f, 60f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		BoxCollider boxCollider4 = GameObject.AddComponent<BoxCollider>();
		boxCollider4.center = Vector3.zero;
		boxCollider4.size = new Vector3(vector.x, vector.y, 1f);
		StartCoroutine(SetLayout(mOrientation));
	}

	private void MightOpenURL(string url, bool is_external_spawn)
	{
		if (string.IsNullOrEmpty(url))
		{
			return;
		}
		if (is_external_spawn)
		{
			Application.OpenURL(url);
			return;
		}
		GameObject go = Util.CreateGameObject("WebViewDialog", mRoot);
		WebViewDialog dialog = go.AddComponent<WebViewDialog>();
		dialog.Title = Localization.Get("Webview_Title");
		dialog.Init(new Color32(byte.MaxValue, 230, 235, byte.MaxValue));
		dialog.SetClosedCallback(delegate
		{
			UnityEngine.Object.Destroy(go);
			go = null;
		});
		dialog.SetPageMovedCallback(delegate
		{
		});
		dialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
		{
			dialog.LoadURL(url);
			dialog.ShowWebview(true);
		});
	}

	private IEnumerator SetLayout(ScreenOrientation o)
	{
		yield return null;
		float screenTop = Util.LogScreenSize().y / 2f;
		if (mScrollList != null)
		{
			mScrollList.MightUpdateCollider = true;
			mScrollList.SetLayout();
		}
		if (mRotateTarget == null)
		{
			yield break;
		}
		foreach (KeyValuePair<ROTATE_TARGET, Transform> pair in mRotateTarget)
		{
			if (!(pair.Value == null))
			{
				Vector3? pos = null;
				switch (pair.Key)
				{
				case ROTATE_TARGET.HEADER:
					pos = new Vector3(0f, screenTop + 50f + mRefTarget[pair.Key].localPosition.y);
					break;
				case ROTATE_TARGET.FOOTER:
					pos = new Vector3(0f, 0f - screenTop - 13f + mRefTarget[pair.Key].localPosition.y);
					break;
				}
				if (pos.HasValue)
				{
					pair.Value.SetLocalPosition(pos.Value);
				}
			}
		}
	}

	public static bool CanMake()
	{
		long start = 0L;
		long end = 0L;
		int[] eventID = null;
		Def.SERIES[] series = null;
		KeyValuePair<Def.SERIES, NewMainMapData>[] maxlvls = null;
		ShopData[] id = null;
		return CanUpdateApplication() || CanMainEvent(ref eventID) || CanSaleSpecialGasha(ref eventID) || CanAdvPoint(ref eventID) || CanMainEventNotice(ref eventID) || CanAdvPointNotice(ref eventID) || CanNewMainMap(ref maxlvls) || CanSaleRecommend(ref id) || CanSaleNonRecommend(ref id) || CanOpenSeriesCampaign(ref series) || CanOpenDailyChallenge() || CanOpenGuerrilla() || CanReplacementTrophy() || CanSaleMugenHeart(ref start, ref end) || CanAddNewOldEvent(ref start) || CanUnique();
	}

	private static bool CanUpdateApplication()
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.Update);
		if (noticeConstructInfo == null)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		return SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AppVer > 1290 && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.UpdateVersionFlag != GameProfile.UpdateKind.NONE;
	}

	private static bool CanMainEvent(ref int[] eventID)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenMainEvent);
		eventID = null;
		if (noticeConstructInfo == null)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		if ((SingletonMonoBehaviour<GameMain>.Instance.IsSeasonEventHolding() & GameMain.EventState.Season) != GameMain.EventState.Season || !SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsInitialTutorialCompleted() || SingletonMonoBehaviour<GameMain>.Instance.IsTipsScreenVisibled)
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		if (SingletonMonoBehaviour<GameMain>.Instance != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile.SeasonEventList != null)
		{
			List<int> list = new List<int>();
			foreach (SeasonEventSettings seasonEvent in SingletonMonoBehaviour<GameMain>.Instance.EventProfile.SeasonEventList)
			{
				if (seasonEvent != null && seasonEvent.EventID < 200 && !noticeConstructInfo.ContainsExclude(seasonEvent.EventID))
				{
					DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEvent.StartTime, true);
					DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(seasonEvent.EndTime, true);
					if (dateTime2 <= dateTime && dateTime <= dateTime3)
					{
						list.Add(seasonEvent.EventID);
					}
				}
			}
			eventID = list.ToArray();
		}
		return eventID != null && eventID.Length > 0;
	}

	private static bool CanSaleSpecialGasha(ref int[] gasha_group_id)
	{
		gasha_group_id = null;
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.SaleSpecialGasha);
		if (noticeConstructInfo == null || true)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.EnableAdvEnter() || !SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL22_0))
		{
			return false;
		}
		bool result = false;
		if (SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict != null)
		{
			List<int> list = new List<int>();
			DateTime dateTime = DateTimeUtil.Now();
			foreach (int key in SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict.Keys)
			{
				if (SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict.ContainsKey(key) && SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[key] != null && SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[key].manifest != null && SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[key].manifest.sort > 10 && !noticeConstructInfo.ContainsExclude(SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[key].id))
				{
					DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[key].starttime, true);
					DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[key].endtime, true);
					AdvGashaInfo advGashaInfo = SingletonMonoBehaviour<GameMain>.Instance.mAdvGashaInfoDict[key];
					if (advGashaInfo != null && !(dateTime < dateTime2) && !(dateTime3 < dateTime))
					{
						result = true;
						list.Add(key);
					}
				}
			}
			if (list.Count > 0)
			{
				gasha_group_id = list.ToArray();
			}
		}
		return result;
	}

	private static bool CanAdvPoint(ref int[] eventID)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenAdvPoint);
		eventID = null;
		if (noticeConstructInfo == null || true)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.EnableAdvEnter() || !SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL22_0) || !SingletonMonoBehaviour<GameMain>.Instance.IsPlayEventDungeon())
		{
			return false;
		}
		if (SingletonMonoBehaviour<GameMain>.Instance != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile.PGEvent != null)
		{
			DateTime dateTime = DateTimeUtil.Now();
			List<int> list = new List<int>();
			foreach (PointGetEventSettings item in SingletonMonoBehaviour<GameMain>.Instance.EventProfile.PGEvent)
			{
				if (item != null && !noticeConstructInfo.ContainsExclude(item.EventID))
				{
					DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(item.StartTime, true);
					DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(item.EndTime, true);
					if (dateTime2 <= dateTime && dateTime <= dateTime3)
					{
						list.Add(item.EventID);
					}
				}
			}
			eventID = list.ToArray();
		}
		return eventID != null && eventID.Length > 0;
	}

	private static bool CanMainEventNotice(ref int[] eventID)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.NoticeMainEvent);
		eventID = null;
		if (noticeConstructInfo == null)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		if ((SingletonMonoBehaviour<GameMain>.Instance.IsSeasonEventHolding() & GameMain.EventState.Season) != GameMain.EventState.Season || !SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsInitialTutorialCompleted() || SingletonMonoBehaviour<GameMain>.Instance.IsTipsScreenVisibled)
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		if (SingletonMonoBehaviour<GameMain>.Instance != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile.SeasonEventList != null)
		{
			List<int> list = new List<int>();
			foreach (SeasonEventSettings seasonEvent in SingletonMonoBehaviour<GameMain>.Instance.EventProfile.SeasonEventList)
			{
				if (seasonEvent != null && seasonEvent.EventID < 200 && !noticeConstructInfo.ContainsExclude(seasonEvent.EventID))
				{
					DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEvent.StartTime, true);
					if (dateTime2 > dateTime && dateTime2 - dateTime < PREVIOUS_NOTICE_TIME)
					{
						list.Add(seasonEvent.EventID);
					}
				}
			}
			eventID = list.ToArray();
		}
		return eventID != null && eventID.Length > 0;
	}

	private static bool CanAdvPointNotice(ref int[] eventID)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.NoticeAdvPoint);
		eventID = null;
		if (noticeConstructInfo == null || true)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.EnableAdvEnter() || !SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL22_0) || !SingletonMonoBehaviour<GameMain>.Instance.IsPlayEventDungeon())
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		if (SingletonMonoBehaviour<GameMain>.Instance != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile != null && SingletonMonoBehaviour<GameMain>.Instance.EventProfile.PGEvent != null)
		{
			List<int> list = new List<int>();
			foreach (PointGetEventSettings item in SingletonMonoBehaviour<GameMain>.Instance.EventProfile.PGEvent)
			{
				if (item != null && !noticeConstructInfo.ContainsExclude(item.EventID))
				{
					DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(item.StartTime, true);
					if (dateTime2 > dateTime && dateTime2 - dateTime < PREVIOUS_NOTICE_TIME)
					{
						list.Add(item.EventID);
					}
				}
			}
			eventID = list.ToArray();
		}
		return eventID != null && eventID.Length > 0;
	}

	private static bool CanNewMainMap(ref KeyValuePair<Def.SERIES, NewMainMapData>[] maxlvls)
	{
		maxlvls = null;
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenNewMainMap);
		if (noticeConstructInfo == null)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		List<KeyValuePair<Def.SERIES, SeriesMaxLevelSchedule>> list = new List<KeyValuePair<Def.SERIES, SeriesMaxLevelSchedule>>();
		DateTime dateTime = DateTimeUtil.Now();
		foreach (SeriesMaxLevel maxLevel in SingletonMonoBehaviour<GameMain>.Instance.GameProfile.MaxLevels)
		{
			if (maxLevel == null || maxLevel.Schedule == null || noticeConstructInfo.ContainsExclude(maxLevel.SeriesInt))
			{
				continue;
			}
			foreach (SeriesMaxLevelSchedule item in maxLevel.Schedule)
			{
				if (item != null && !(item.StartTime > dateTime) && !(dateTime - item.StartTime > VISIBLE_NOTICE_TIME))
				{
					list.Add(new KeyValuePair<Def.SERIES, SeriesMaxLevelSchedule>((Def.SERIES)maxLevel.SeriesInt, item));
				}
			}
		}
		if (list != null)
		{
			list.OrderByDescending((KeyValuePair<Def.SERIES, SeriesMaxLevelSchedule> _) => _.Value.StartTime);
			maxlvls = list.Select((KeyValuePair<Def.SERIES, SeriesMaxLevelSchedule> _) => new KeyValuePair<Def.SERIES, NewMainMapData>(_.Key, new NewMainMapData
			{
				MaxLevels = _.Value.MaxLevel,
				StartTime = _.Value.StartUnixTime,
				Banner = _.Value.BannerFileName
			})).ToArray();
		}
		return list != null && list.Count > 0;
	}

	private static bool CanSaleRecommend(ref ShopData[] id)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.SaleRecommendSet);
		id = null;
		if (noticeConstructInfo != null)
		{
			if (noticeConstructInfo.IsForceShow)
			{
				return true;
			}
			if (noticeConstructInfo.IsForceHide)
			{
				return false;
			}
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsInitialTutorialCompleted())
		{
			return false;
		}
		if (SingletonMonoBehaviour<GameMain>.Instance != null && SingletonMonoBehaviour<GameMain>.Instance.mShopItemData != null)
		{
			IEnumerable<ShopLimitedTime> enumerable = SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.ShopSetItemList.Where((ShopLimitedTime n) => n.SaleBannerID > 0);
			if (enumerable != null)
			{
				DateTime dateTime = DateTimeUtil.Now();
				List<ShopData> list = new List<ShopData>();
				ShopLimitedTime s;
				foreach (ShopLimitedTime item in enumerable)
				{
					s = item;
					if (s != null && !noticeConstructInfo.ContainsExclude(s.ShopItemID))
					{
						DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(s.StartTime, true);
						DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(s.EndTime, true);
						if (SingletonMonoBehaviour<GameMain>.Instance.mShopItemData.Any((KeyValuePair<int, ShopItemData> n) => n.Key == s.ShopItemID) && dateTime2 <= dateTime && dateTime <= dateTime3)
						{
							list.Add(new ShopData
							{
								ShopID = s.ShopItemID,
								Start = s.StartTime,
								End = s.EndTime
							});
						}
					}
				}
				id = list.ToArray();
			}
		}
		return id != null && id.Length > 0;
	}

	private static bool CanSaleNonRecommend(ref ShopData[] id)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.SaleNonRecommendSet);
		id = null;
		if (noticeConstructInfo != null)
		{
			if (noticeConstructInfo.IsForceShow)
			{
				return true;
			}
			if (noticeConstructInfo.IsForceHide)
			{
				return false;
			}
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsInitialTutorialCompleted())
		{
			return false;
		}
		if (SingletonMonoBehaviour<GameMain>.Instance != null && SingletonMonoBehaviour<GameMain>.Instance.mShopItemData != null)
		{
			List<ShopLimitedTime> list = new List<ShopLimitedTime>();
			IEnumerable<ShopLimitedTime> enumerable = SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.ShopSetItemList.Where((ShopLimitedTime n) => n.SaleBannerID == 0);
			IEnumerable<ShopLimitedTime> enumerable2 = SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.ShopMugenHeartItemList.Where((ShopLimitedTime n) => n.SaleBannerID == 0);
			IEnumerable<ShopLimitedTime> enumerable3 = SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.ShopAllCrushItemList.Where((ShopLimitedTime n) => n.SaleBannerID == 0);
			if (enumerable != null)
			{
				list.AddRange(enumerable.ToArray());
			}
			if (enumerable2 != null)
			{
				list.AddRange(enumerable2.ToArray());
			}
			if (enumerable3 != null)
			{
				list.AddRange(enumerable3.ToArray());
			}
			if (list != null)
			{
				DateTime dateTime = DateTimeUtil.Now();
				List<ShopData> list2 = new List<ShopData>();
				ShopLimitedTime s;
				foreach (ShopLimitedTime item in list)
				{
					s = item;
					if (s != null && !noticeConstructInfo.ContainsExclude(s.ShopItemID))
					{
						DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(s.StartTime, true);
						DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(s.EndTime, true);
						if (SingletonMonoBehaviour<GameMain>.Instance.mShopItemData.Any((KeyValuePair<int, ShopItemData> n) => n.Key == s.ShopItemID) && dateTime2 <= dateTime && dateTime <= dateTime3)
						{
							list2.Add(new ShopData
							{
								ShopID = s.ShopItemID,
								Start = s.StartTime,
								End = s.EndTime
							});
						}
					}
				}
				id = list2.Distinct().ToArray();
			}
		}
		return id != null && id.Length > 0;
	}

	private static bool CanOpenSeriesCampaign(ref Def.SERIES[] series)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenSeriesCampaign);
		series = null;
		if (noticeConstructInfo == null)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0))
		{
			return false;
		}
		List<Def.SERIES> list = new List<Def.SERIES>();
		DateTime dateTime = DateTimeUtil.Now();
		foreach (SeriesCampaignSetting seriesCampaign in SingletonMonoBehaviour<GameMain>.Instance.EventProfile.SeriesCampaignList)
		{
			if (seriesCampaign != null && !noticeConstructInfo.ContainsExclude(seriesCampaign.SeriesOriginal))
			{
				DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seriesCampaign.StartTime, true);
				DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(seriesCampaign.EndTime, true);
				if (dateTime2 <= dateTime && dateTime <= dateTime3 && !list.Contains(seriesCampaign.Series))
				{
					list.Add(seriesCampaign.Series);
				}
			}
		}
		series = list.ToArray();
		return series != null && series.Length > 0;
	}

	private static bool CanOpenDailyChallenge()
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OpenDailyChallenge);
		if (noticeConstructInfo == null || GlobalVariables.Instance.SelectedDailyChallengeSetting == null)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsInitialTutorialCompleted())
		{
			return false;
		}
		if (noticeConstructInfo.ContainsExclude(GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID))
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(GlobalVariables.Instance.SelectedDailyChallengeSetting.StartTime, true);
		DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(GlobalVariables.Instance.SelectedDailyChallengeSetting.EndTime, true);
		return dateTime2 <= dateTime && dateTime <= dateTime3;
	}

	private static bool CanOpenGuerrilla()
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.Guerrilla);
		if (noticeConstructInfo == null || true)
		{
			return false;
		}
		if (!SingletonMonoBehaviour<GameMain>.Instance.EnableAdvEnter() || !SingletonMonoBehaviour<GameMain>.Instance.IsPlayEventDungeon())
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		double nowTime = DateTimeUtil.LoacalDateTimeToUnixTimeStamp(dateTime);
		DateTime dateTime2 = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0);
		DateTime dateTime3 = dateTime2.AddDays(1.0);
		double todayTime = DateTimeUtil.LoacalDateTimeToUnixTimeStamp(dateTime2);
		double tomorrowTime = DateTimeUtil.LoacalDateTimeToUnixTimeStamp(dateTime3);
		if (!SingletonMonoBehaviour<GameMain>.Instance.EventProfile.GLAEvent.Any((GLAEventSettings n) => (todayTime <= (double)n.StartTime && (double)n.StartTime < tomorrowTime) || (todayTime <= (double)n.EndTime && (double)n.EndTime < tomorrowTime)))
		{
			return false;
		}
		if (SingletonMonoBehaviour<GameMain>.Instance.EventProfile.GLAEvent.Any((GLAEventSettings n) => (double)n.StartTime <= nowTime && nowTime <= (double)n.EndTime))
		{
			return true;
		}
		if (SingletonMonoBehaviour<GameMain>.Instance.EventProfile.GLAEvent.Any((GLAEventSettings n) => nowTime <= (double)n.StartTime && (double)n.StartTime < tomorrowTime))
		{
			DateTime dateTime4 = dateTime2.AddDays(-1.0);
			double yesterdayTime = DateTimeUtil.LoacalDateTimeToUnixTimeStamp(dateTime4);
			if (!SingletonMonoBehaviour<GameMain>.Instance.EventProfile.GLAEvent.Any((GLAEventSettings n) => yesterdayTime <= (double)n.StartTime && (double)n.StartTime < nowTime))
			{
				return false;
			}
			return true;
		}
		DateTime dateTime5 = dateTime3.AddDays(1.0);
		double twoDayTime = DateTimeUtil.LoacalDateTimeToUnixTimeStamp(dateTime5);
		if (SingletonMonoBehaviour<GameMain>.Instance.EventProfile.GLAEvent.Any((GLAEventSettings n) => tomorrowTime <= (double)n.StartTime && (double)n.StartTime < twoDayTime))
		{
			return true;
		}
		return false;
	}

	private static bool CanReplacementTrophy()
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.TrophyExchange);
		if (noticeConstructInfo == null)
		{
			return false;
		}
		if (noticeConstructInfo.IsForceShow)
		{
			return true;
		}
		if (noticeConstructInfo.IsForceHide)
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now().ToUniversalTime();
		return dateTime - SingletonMonoBehaviour<GameMain>.Instance.EventProfile.GetTrophyItemReplenishTime() <= VISIBLE_NOTICE_TIME;
	}

	private static bool CanSaleMugenHeart(ref long start, ref long end)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.MugenHeart);
		if (noticeConstructInfo == null)
		{
			return false;
		}
		return false;
	}

	private static bool CanAddNewOldEvent(ref long start)
	{
		InformationManager.Info.NoticeConstruct.BannerInfo noticeConstructInfo = InformationManager.Instance.GetNoticeConstructInfo(InformationManager.Info.NoticeConstruct.InfoType.OldEvent);
		if (noticeConstructInfo == null)
		{
			return false;
		}
		return false;
	}

	private static bool CanUnique()
	{
		InformationManager.Info.NoticeConstruct.UniqueBannerInfo[] noticeConstructUniqueInfo = InformationManager.Instance.GetNoticeConstructUniqueInfo();
		if (noticeConstructUniqueInfo == null)
		{
			return false;
		}
		InformationManager.Info.NoticeConstruct.UniqueBannerInfo[] array = noticeConstructUniqueInfo;
		foreach (InformationManager.Info.NoticeConstruct.UniqueBannerInfo uniqueBannerInfo in array)
		{
			if (uniqueBannerInfo.IsForceShow)
			{
				return true;
			}
			if (!uniqueBannerInfo.IsForceHide)
			{
				DateTime dateTime = DateTimeUtil.Now();
				if (uniqueBannerInfo.StartTime <= dateTime && dateTime <= uniqueBannerInfo.EndTime)
				{
					return true;
				}
			}
		}
		return false;
	}

	public override void OnOpenFinished()
	{
		if (mScrollList != null)
		{
			mScrollList.MightUpdateCollider = true;
		}
		mState.Change(STATE.MAIN);
	}

	public override void OnCloseFinished()
	{
		UnityEngine.Object.Destroy(GameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed();
	}

	public void OnTogglePushed(GameObject go)
	{
		if (!IsBusy)
		{
			IsCheckState = !IsCheckState;
			string text = ((!IsCheckState) ? CHECK_IMAGE_NAME[CHECK_STATE.OFF] : CHECK_IMAGE_NAME[CHECK_STATE.ON]);
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), text, text, text);
			if (mButtonCollider != null)
			{
				mButtonCollider.center = new Vector3(115f, 0f, 0f);
				BoxCollider boxCollider = mButtonCollider;
				Vector4 cHECK_POSITION = CHECK_POSITION;
				float z = cHECK_POSITION.z;
				Vector4 cHECK_POSITION2 = CHECK_POSITION;
				boxCollider.size = new Vector3(z, cHECK_POSITION2.w, 1f);
			}
		}
	}

	public void OnPositivePushed()
	{
		if (!IsBusy)
		{
			mState.Reset(STATE.WAIT);
			mGame.PlaySe("SE_POSITIVE", -1);
			if (mScrollList != null)
			{
				UnityEngine.Object.Destroy(mScrollList.gameObject);
				mScrollList = null;
			}
			Close();
		}
	}

	public void OnNegativePushed()
	{
		if (!IsBusy)
		{
			mState.Reset(STATE.WAIT);
			mGame.PlaySe("SE_NEGATIVE", -1);
			if (mScrollList != null)
			{
				UnityEngine.Object.Destroy(mScrollList.gameObject);
				mScrollList = null;
			}
			Close();
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		StartCoroutine(SetLayout(o));
	}
}
