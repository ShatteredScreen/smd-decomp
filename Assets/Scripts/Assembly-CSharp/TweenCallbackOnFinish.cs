using UnityEngine;

public class TweenCallbackOnFinish : MonoBehaviour
{
	public delegate void OnFinish(GameObject sender);

	public OnFinish onTweenFinishCallback;

	public void OnTweenFinish()
	{
		if (onTweenFinishCallback != null)
		{
			onTweenFinishCallback(base.gameObject);
		}
	}
}
