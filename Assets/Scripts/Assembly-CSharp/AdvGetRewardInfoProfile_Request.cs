public class AdvGetRewardInfoProfile_Request : RequestBase
{
	[MiniJSONAlias("reward_ids")]
	public int[] RewardIDs { get; set; }
}
