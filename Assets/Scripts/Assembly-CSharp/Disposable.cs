using System;
using UnityEngine.Events;

public abstract class Disposable : IDisposable
{
	private bool mDisposed;

	protected UnityAction UnManagedDisposeEvent { private get; set; }

	protected UnityAction ManagedDisposeEvent { private get; set; }

	~Disposable()
	{
		Dispose(false);
	}

	public void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
	}

	private void Dispose(bool disposing)
	{
		if (!mDisposed)
		{
			if (disposing && ManagedDisposeEvent != null)
			{
				ManagedDisposeEvent();
			}
			if (UnManagedDisposeEvent != null)
			{
				UnManagedDisposeEvent();
			}
			mDisposed = true;
		}
	}
}
