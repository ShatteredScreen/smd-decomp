using System.Text;
using UnityEngine;

public class BIJDetectLeak : MonoBehaviour
{
	private Rect mDrawRect;

	private GUIStyle mStyle;

	private void Awake()
	{
		mDrawRect = new Rect(0f, 0f, Screen.width - 10, Screen.height);
		GUIStyleState gUIStyleState = new GUIStyleState();
		gUIStyleState.textColor = Color.black;
		mStyle = new GUIStyle();
		mStyle.normal = gUIStyleState;
		mStyle.contentOffset = new Vector2(2f, 2f);
		mStyle.fontSize = 10;
		mStyle.alignment = TextAnchor.LowerRight;
	}

	private void OnGUI()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine(string.Format("All {0} =", Object.FindObjectsOfType(typeof(Object)).Length));
		stringBuilder.AppendLine(string.Format("Textures {0} =", Object.FindObjectsOfType(typeof(Texture)).Length));
		stringBuilder.AppendLine(string.Format("AudioClips {0} =", Object.FindObjectsOfType(typeof(AudioClip)).Length));
		stringBuilder.AppendLine(string.Format("Meshes {0} =", Object.FindObjectsOfType(typeof(Mesh)).Length));
		stringBuilder.AppendLine(string.Format("Materials {0} =", Object.FindObjectsOfType(typeof(Material)).Length));
		stringBuilder.AppendLine(string.Format("GameObjects {0} =", Object.FindObjectsOfType(typeof(GameObject)).Length));
		stringBuilder.AppendLine(string.Format("Components {0} =", Object.FindObjectsOfType(typeof(Component)).Length));
		GUI.Label(mDrawRect, new GUIContent(stringBuilder.ToString()), mStyle);
	}
}
