using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
	public GameMain mGame;

	public string mParentName = "Anchor";

	public string mGameStateKey = string.Empty;

	public string mNextGameStateKey = string.Empty;

	public GameObject mRoot;

	public GameObject mBackGroundRoot;

	private UIPanel mGrayScreenPanel;

	private UISprite mGrayScreen;

	private UISprite mWhiteScreen;

	private bool mLoadFinished;

	private bool mUnloadFinished;

	protected bool mGrayOutFuncEnable;

	protected bool mGrayInFuncEnable;

	private float mGrayScreenAlpha;

	private int mGrayScreenDepth;

	public GameObject ParticleRoot { get; private set; }

	public int GrayScreenDepth
	{
		get
		{
			return mGrayScreenDepth;
		}
	}

	public int GrayScreenSortOrder
	{
		get
		{
			return mGrayScreenPanel.sortingOrder;
		}
	}

	public virtual void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		GameObject parent = GameObject.Find(mParentName);
		mRoot = Util.CreateGameObject(GetType().Name + "Root", parent);
		ParticleRoot = Util.CreateGameObject("ParticleRoot", base.gameObject);
		ParticleRoot.layer = mRoot.layer;
		mLoadFinished = false;
		StartCoroutine(OnLoad());
	}

	public virtual void Unload()
	{
		mUnloadFinished = false;
		StartCoroutine(OnUnload());
	}

	public virtual void Update()
	{
		if (mGrayScreen != null && mGame.mDialogManager.GrayScreenDepth != mGrayScreenDepth)
		{
			mGrayScreenDepth = mGame.mDialogManager.GrayScreenDepth;
			if (mGrayScreenDepth < 1)
			{
				mGrayScreenDepth = 1;
			}
			mGrayScreenPanel.depth = 1;
			mGrayScreenPanel.sortingOrder = mGrayScreenDepth;
			Vector3 localPosition = mGrayScreen.transform.localPosition;
			mGrayScreen.transform.localPosition = new Vector3(localPosition.x, localPosition.y, mGame.mDialogManager.GrayScreenZ);
		}
	}

	public virtual void OnScreenChanged(ScreenOrientation o)
	{
	}

	public IEnumerator GrayOut()
	{
		yield return StartCoroutine(GrayOut(0.5f, Color.black, 1f));
	}

	public IEnumerator GrayOut(float targetAlpha, Color col, float sec)
	{
		if (mGrayInFuncEnable)
		{
			mGrayInFuncEnable = false;
		}
		mGrayOutFuncEnable = true;
		if (mGrayScreen == null)
		{
			BIJImage atlas = ResourceManager.LoadImage("FADE").Image;
			mGrayScreenPanel = Util.CreateGameObject("GrayScreenPanel", mRoot).AddComponent<UIPanel>();
			mGrayScreenPanel.depth = 1;
			mGrayScreenPanel.sortingOrder = mGrayScreenDepth;
			mGrayScreen = Util.CreateSprite("GrayScreen", mGrayScreenPanel.gameObject, atlas);
			Util.SetSpriteInfo(mGrayScreen, "bg_white", 0, new Vector3(0f, 0f, Def.FADE_BASE_Z), Vector3.one, false);
			mGrayScreen.type = UIBasicSprite.Type.Sliced;
			mGrayScreen.SetDimensions(1704, 1704);
			mGrayScreen.color = new Color(col.r, col.g, col.b, 0f);
			mGrayScreenAlpha = 0f;
			yield return 0;
		}
		float startAlpha = mGrayScreenAlpha;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * (90f / sec);
			if (angle > 90f)
			{
				angle = 90f;
			}
			mGrayScreenAlpha = startAlpha + Mathf.Sin(angle * ((float)Math.PI / 180f)) * (targetAlpha - startAlpha);
			mGrayScreen.color = new Color(col.r, col.g, col.b, mGrayScreenAlpha);
			yield return 0;
			if (!mGrayOutFuncEnable)
			{
				break;
			}
		}
		mGrayOutFuncEnable = false;
	}

	public void GrayOutForce(float targetAlpha)
	{
		Color black = Color.black;
		if (mGrayScreen == null)
		{
			BIJImage image = ResourceManager.LoadImage("FADE").Image;
			mGrayScreenPanel = Util.CreateGameObject("GrayScreenPanel", mRoot).AddComponent<UIPanel>();
			mGrayScreenPanel.depth = 0;
			mGrayScreenPanel.sortingOrder = mGrayScreenDepth;
			mGrayScreen = Util.CreateSprite("GrayScreen", mGrayScreenPanel.gameObject, image);
			Util.SetSpriteInfo(mGrayScreen, "bg_white", 0, new Vector3(0f, 0f, Def.FADE_BASE_Z), Vector3.one, false);
			mGrayScreen.type = UIBasicSprite.Type.Sliced;
			mGrayScreen.SetDimensions(1704, 1704);
			mGrayScreen.color = new Color(black.r, black.g, black.b, 0f);
			mGrayScreenAlpha = 0f;
		}
		mGrayScreenAlpha = targetAlpha;
		mGrayScreen.color = new Color(black.r, black.g, black.b, mGrayScreenAlpha);
		mGrayOutFuncEnable = false;
	}

	public IEnumerator GrayIn()
	{
		yield return StartCoroutine(GrayIn(1f));
	}

	public IEnumerator GrayIn(float sec, float target = 0f)
	{
		if (mGrayScreen == null)
		{
			yield break;
		}
		if (mGrayOutFuncEnable)
		{
			mGrayOutFuncEnable = false;
		}
		mGrayInFuncEnable = true;
		Color col = mGrayScreen.color;
		float startAlpha = mGrayScreenAlpha;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * (90f / sec);
			if (angle > 90f)
			{
				angle = 90f;
			}
			mGrayScreenAlpha = startAlpha + Mathf.Sin(angle * ((float)Math.PI / 180f)) * (target - startAlpha);
			mGrayScreen.color = new Color(col.r, col.g, col.b, mGrayScreenAlpha);
			yield return 0;
			if (!mGrayInFuncEnable)
			{
				yield break;
			}
		}
		if (!(target > 0f))
		{
			UnityEngine.Object.Destroy(mGrayScreenPanel.gameObject);
			mGrayScreenPanel = null;
			mGrayScreen = null;
			mGrayInFuncEnable = false;
		}
	}

	public void SetParentGrayScreen(Transform parent, int layer_no = -1, bool recursive = false)
	{
		if ((bool)mGrayScreenPanel)
		{
			mGrayScreenPanel.transform.SetParent(parent);
			if (layer_no >= 0)
			{
				SetLayer(mGrayScreenPanel.gameObject, layer_no, recursive);
			}
		}
	}

	public void RestoreGrayScreenParent()
	{
		if ((bool)mGrayScreenPanel)
		{
			mGrayScreenPanel.transform.SetParent(mRoot.transform);
			SetLayer(mGrayScreenPanel.gameObject, 9, true);
		}
	}

	private void SetLayer(GameObject go, int layer_no, bool recursive = false)
	{
		if (go == null)
		{
			return;
		}
		go.layer = layer_no;
		if (!recursive)
		{
			return;
		}
		foreach (Transform item in go.transform)
		{
			SetLayer(item.gameObject, layer_no, recursive);
		}
	}

	public IEnumerator CameraFlash()
	{
		if (mWhiteScreen == null)
		{
			mWhiteScreen = Util.CreateSprite(atlas: ResourceManager.LoadImage("FADE").Image, name: "WhiteScreen", parent: mRoot);
			Util.SetSpriteInfo(mWhiteScreen, "bg_white", 100, new Vector3(0f, 0f, Def.FADE_BASE_Z), Vector3.one, false);
			mWhiteScreen.type = UIBasicSprite.Type.Sliced;
			mWhiteScreen.SetDimensions(1704, 1704);
			mWhiteScreen.color = new Color(1f, 1f, 1f, 1f);
			yield return 0;
		}
		float alpha = 1f;
		while (alpha > 0f)
		{
			alpha -= Time.deltaTime * 1f;
			if (alpha < 0f)
			{
				alpha = 0f;
			}
			mWhiteScreen.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		UnityEngine.Object.Destroy(mWhiteScreen.gameObject);
		mWhiteScreen = null;
	}

	private IEnumerator OnLoad()
	{
		float _start3 = Time.realtimeSinceStartup;
		if (GameMain.USE_RESOURCEDL)
		{
			_start3 = Time.realtimeSinceStartup;
			yield return StartCoroutine(SingletonMonoBehaviour<ResourceManager>.Instance.LoadSceneResources(mGameStateKey));
		}
		_start3 = Time.realtimeSinceStartup;
		yield return StartCoroutine(LoadGameState());
		mLoadFinished = true;
	}

	public virtual IEnumerator LoadGameState()
	{
		yield return 0;
	}

	private IEnumerator OnUnload()
	{
		yield return StartCoroutine(UnloadGameState());
		if (GameMain.USE_RESOURCEDL)
		{
			yield return StartCoroutine(SingletonMonoBehaviour<ResourceManager>.Instance.UnloadSceneResources(mGameStateKey, mNextGameStateKey));
		}
		UnityEngine.Object.Destroy(mRoot);
		mRoot = null;
		mUnloadFinished = true;
	}

	public virtual IEnumerator UnloadGameState()
	{
		yield return 0;
	}

	public void LoadGameStateFinished()
	{
	}

	public void UnloadGameStateFinished()
	{
	}

	public bool isLoadFinished()
	{
		return mLoadFinished;
	}

	public bool isUnloadFinished()
	{
		return mUnloadFinished;
	}

	protected virtual IEnumerator LoadCommonResources()
	{
		List<ResourceInstance> iconResourceList = new List<ResourceInstance>
		{
			ResourceManager.LoadImageAsync("ICON"),
			ResourceManager.LoadSsAnimationAsync("EFFECT_ICON_AVATER"),
			ResourceManager.LoadSsAnimationAsync("EFFECT_ICON_FIXEDSPRITE"),
			ResourceManager.LoadSsAnimationAsync("MAP_SNS_ICON"),
			ResourceManager.LoadSsAnimationAsync("MAP_STRAY_ICON"),
			ResourceManager.LoadSsAnimationAsync("ICON_CURSOR")
		};
		for (int i = 0; i < iconResourceList.Count; i++)
		{
			while (iconResourceList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return null;
			}
		}
	}
}
