using System;
using System.Collections.Generic;

public class AdvStageData : IComparable<AdvStageData>
{
	public class SPEnemyDrop
	{
		public byte EnemyIndex;

		public int RewardID;

		public short RewardRatio;

		public void CopyFrom(SPEnemyDrop a_from)
		{
			EnemyIndex = a_from.EnemyIndex;
			RewardID = a_from.RewardID;
			RewardRatio = a_from.RewardRatio;
		}
	}

	public int index;

	public Dictionary<int, SPEnemyDrop> mEnemyDropConstList = new Dictionary<int, SPEnemyDrop>();

	public int CompareTo(AdvStageData _stageData)
	{
		return 1;
	}

	public bool IsUnlock()
	{
		return false;
	}
}
