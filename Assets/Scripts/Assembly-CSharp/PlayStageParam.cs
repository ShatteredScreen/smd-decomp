using System;

public class PlayStageParam : ICloneable
{
	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int Level { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public PlayStageParam Clone()
	{
		return MemberwiseClone() as PlayStageParam;
	}
}
