public class ConnectAdvCheckMasterTableVersion : ConnectBase
{
	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (!Server.AdvCheckMasterTableVer())
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetAdvCheckMasterTableVersionFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
