using System;
using System.Collections.Generic;

public class AdvGetMasterTableDataProfile_Response : ICloneable
{
	[MiniJSONAlias("masters")]
	public List<AdvMasterTableData> MasterTableDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGetMasterTableDataProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGetMasterTableDataProfile_Response;
	}
}
