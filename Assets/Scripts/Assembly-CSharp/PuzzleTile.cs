using System;
using UnityEngine;

public class PuzzleTile
{
	public enum TILE_STATE
	{
		STAY = 0,
		FLOAT = 1,
		SWAP = 2,
		SWAPEND = 3,
		RETURN = 4,
		RETURNEND = 5,
		CRUSHWAIT = 6,
		CRUSH = 7,
		CRUSHEND = 8,
		BOMB = 9,
		CHANGEFORM = 10,
		TIMEREFFECT = 11,
		TIMEREFFECTEND = 12
	}

	public enum TILE_FALL_STATE
	{
		STAY = 0,
		FALL = 1,
		FALLEND = 2
	}

	protected StatusManager<TILE_STATE> mState = new StatusManager<TILE_STATE>(TILE_STATE.STAY);

	protected StatusManager<TILE_FALL_STATE> mFallState = new StatusManager<TILE_FALL_STATE>(TILE_FALL_STATE.STAY);

	protected TileData mData;

	protected TileSprite mSprite;

	protected Transform mSpriteTransform;

	protected TileData.DROP_ANIME_TYPE mPrevAnimeType = TileData.DROP_ANIME_TYPE.MAXNUM;

	public Vector3 LocalPosition;

	public Vector3 LocalPositionOffset;

	protected Def.TILE_KIND mKind;

	public int mScore = 30;

	protected int mChangeFormScore;

	private Vector2 mDragOriginPos;

	protected float mFloatTime;

	protected bool mFallRock;

	protected Vector2 mMoveOriginPos;

	protected float mMoveProgression;

	protected Def.DIR mMoveDirection;

	protected float mFallResistTime;

	protected Vector2 mFallVelocity;

	protected IntVector2 mFallTarget;

	protected Vector2 mFallStartCoords;

	protected Vector2 mFallTargetCoords;

	protected Vector2 mFallUnderCoords;

	protected Def.TILE_KIND mTargetKind;

	protected Def.TILE_FORM mTargetForm;

	protected float mCrushWaitTimer;

	protected float mBombWaitTimer;

	protected float mChangeFormWaitTimer;

	protected Def.EFFECT_OVERRIDE mEffectOverride;

	protected Def.TILE_KIND mRainbowTargetKind;

	protected int mBombSize;

	public Def.ITEM_CATEGORY ItemCategory;

	public int ItemIndex;

	public float mGatherCheckWaitTimer;

	protected float mBoundProgression;

	protected float mAppealProgression;

	protected IPuzzleManager mManager;

	protected GameMain mGame;

	protected bool mEnter;

	public OneShotAnime mEnterEffect;

	private float mStayTimer;

	protected LoopAnime mEffectOverTile;

	public bool mAnimeFinished;

	private bool mStopGrayOutFunc;

	public bool mOrbCrush;

	public bool mTalismanFaceCrush;

	private bool mCreatedCount;

	public TileData Data
	{
		get
		{
			return mData;
		}
		set
		{
			mData = value;
		}
	}

	public Vector3 Position
	{
		get
		{
			if (mSpriteTransform != null)
			{
				return mSpriteTransform.position;
			}
			return Vector3.zero;
		}
		set
		{
			if (mSpriteTransform != null)
			{
				mSpriteTransform.position = value;
			}
		}
	}

	public IntVector2 TilePos { get; set; }

	public Vector3 Scale
	{
		get
		{
			if (mSpriteTransform != null)
			{
				return mSpriteTransform.localScale;
			}
			return Vector3.one;
		}
		set
		{
			if (mSpriteTransform != null)
			{
				mSpriteTransform.localScale = value;
			}
		}
	}

	public TileSprite Sprite
	{
		get
		{
			return mSprite;
		}
		set
		{
			mSprite = value;
			mSpriteTransform = value.transform;
		}
	}

	public Def.TILE_KIND PrevKind { get; protected set; }

	public Def.TILE_KIND Kind
	{
		get
		{
			return mKind;
		}
		protected set
		{
			PrevKind = mKind;
			mKind = value;
		}
	}

	public Def.TILE_FORM Form { get; protected set; }

	public Def.TILE_FORM NextForm { get; set; }

	public Def.TILE_KIND NextKind { get; set; }

	public PuzzleTile(Def.TILE_KIND kind, Def.TILE_FORM form, IntVector2 tilePos, Vector3 localPos, TileSprite sprite, IPuzzleManager manager, Def.ITEM_CATEGORY itemCategory, int itemIndex)
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mState.Reset(TILE_STATE.STAY, true);
		mFallState.Reset(TILE_FALL_STATE.STAY, true);
		mManager = manager;
		mData = mGame.mTileData[(int)kind];
		Kind = kind;
		Form = form;
		TilePos = tilePos;
		mBombSize = 1;
		LocalPosition = localPos;
		LocalPositionOffset = Vector3.zero;
		mOrbCrush = false;
		mCreatedCount = false;
		ItemCategory = itemCategory;
		ItemIndex = itemIndex;
		if (sprite != null)
		{
			Sprite = sprite;
			mSpriteTransform.localPosition = LocalPosition;
			BIJImage image = ResourceManager.LoadImage("PUZZLE_TILE").Image;
			sprite.Atlas = image;
			changeAnime(TileData.DROP_ANIME_TYPE.NORMAL, 0, true);
		}
		VisibleCheck();
		SpecialCreate(form);
	}

	public virtual void UpdateP(float deltaTime)
	{
		if (Sprite != null)
		{
			Sprite._sprite.Speed = deltaTime / 0.03333333f;
		}
		switch (mState.GetStatus())
		{
		case TILE_STATE.STAY:
			stateStay(deltaTime);
			break;
		case TILE_STATE.FLOAT:
			stateFloat(deltaTime);
			break;
		case TILE_STATE.SWAP:
			stateSwap(deltaTime);
			break;
		case TILE_STATE.RETURN:
			stateReturn(deltaTime);
			break;
		case TILE_STATE.CRUSHWAIT:
			stateCrushWait(deltaTime);
			break;
		case TILE_STATE.CRUSH:
			stateCrush(deltaTime);
			break;
		case TILE_STATE.BOMB:
			stateBomb(deltaTime);
			break;
		case TILE_STATE.CHANGEFORM:
			stateChangeForm(deltaTime);
			break;
		}
		mState.Update();
		switch (mFallState.GetStatus())
		{
		case TILE_FALL_STATE.STAY:
			fallStateStay(deltaTime);
			break;
		case TILE_FALL_STATE.FALL:
			fallStateFall(deltaTime);
			break;
		case TILE_FALL_STATE.FALLEND:
			fallStateFallEnd(deltaTime);
			break;
		}
		mFallState.Update();
		if (mSpriteTransform != null)
		{
			mSpriteTransform.localPosition = LocalPosition + LocalPositionOffset;
		}
	}

	protected virtual void VisibleCheck()
	{
		if (Sprite == null)
		{
			return;
		}
		bool flag = Sprite.enabled;
		PuzzleTile attribute = mManager.GetAttribute(TilePos);
		Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
		if (attribute != null)
		{
			tILE_KIND = attribute.Kind;
		}
		bool flag2 = mManager.IsInField(TilePos);
		if (flag)
		{
			bool flag3 = attribute != null && (tILE_KIND == Def.TILE_KIND.SPAWN || tILE_KIND == Def.TILE_KIND.EXIT);
			if ((!flag2 || flag3) && mFallState.GetStatus() != TILE_FALL_STATE.FALL)
			{
				flag = false;
			}
		}
		else
		{
			bool flag4 = tILE_KIND != Def.TILE_KIND.SPAWN;
			if ((flag2 && flag4) || mFallState.GetStatus() == TILE_FALL_STATE.FALL)
			{
				flag = true;
			}
		}
		if (Sprite.enabled != flag)
		{
			Sprite.enabled = flag;
		}
		if (!mEnter && flag)
		{
			OnEnter();
		}
		mEnter = flag;
	}

	public void OnAnimationFinished(SsSprite sprite)
	{
		sprite.Pause();
		mAnimeFinished = true;
	}

	public void OnTimerAnimationFinished(SsSprite sprite)
	{
	}

	protected virtual void fallStateStay(float deltaTime)
	{
		if (isFallable())
		{
			IntVector2 intVector = fallTargetCheck();
			if (intVector != null)
			{
				fall(intVector);
			}
		}
	}

	private void fallStateFall(float deltaTime)
	{
		mFallVelocity.y -= Def.GRAVITY * deltaTime;
		if (mFallVelocity.y < Def.FALL_SPEED_MAX)
		{
			mFallVelocity.y = Def.FALL_SPEED_MAX;
		}
		Vector3 localPosition = LocalPosition;
		float z = localPosition.z;
		localPosition.x += mFallVelocity.x * deltaTime;
		localPosition.y += mFallVelocity.y * deltaTime;
		if (TilePos.x != mFallTarget.x)
		{
			float num = Mathf.Abs(localPosition.y - mFallStartCoords.y) / Mathf.Abs(mFallUnderCoords.y - mFallStartCoords.y);
			if (num > 1f)
			{
				num = 1f;
			}
			localPosition.x = mFallStartCoords.x + (mFallTargetCoords.x - mFallStartCoords.x) * num;
		}
		if (localPosition.y <= mFallTargetCoords.y)
		{
			TilePos = mFallTarget;
			PuzzleTile attribute = mManager.GetAttribute(TilePos);
			if (attribute != null && attribute.Kind == Def.TILE_KIND.WARP_IN)
			{
				ISMTileInfo tileInfo = mGame.mCurrentStage.GetTileInfo(TilePos.x, TilePos.y);
				IntVector2 intVector = new IntVector2(tileInfo.WarpOutPosX, tileInfo.WarpOutPosY);
				mManager.MoveTempCandy(this, intVector);
				TilePos = new IntVector2(tileInfo.WarpOutPosX, tileInfo.WarpOutPosY);
				mFallTargetCoords = mManager.GetDefaultCoords(intVector);
				mFallTargetCoords += new Vector2(0f, (0f - Def.TILE_PITCH_V) * 0.6f);
			}
			localPosition = mFallTargetCoords;
			localPosition.z = z;
			IntVector2 intVector2 = fallTargetCheck();
			if (intVector2 == null)
			{
				localPosition.z = z;
				fallEnd();
				if (mBombWaitTimer <= 0f)
				{
					mManager.RemoveTile(TilePos, false);
					mManager.PlaceTile(TilePos, this);
					if (mManager.IsCollectable(this))
					{
						mManager.BeginCollect(this);
					}
				}
			}
			else
			{
				mManager.MoveTempCandy(this, intVector2);
				mFallTarget = intVector2;
				mFallStartCoords = mManager.GetDefaultCoords(TilePos);
				mFallTargetCoords = mManager.GetFallTargetCoords(mFallTarget);
				mFallUnderCoords = mManager.GetDefaultCoords(new IntVector2(TilePos.x, TilePos.y - 1));
			}
		}
		LocalPosition = localPosition;
		VisibleCheck();
	}

	private void fallStateFallEnd(float deltaTime)
	{
		if (mFallState.IsChanged())
		{
			mBoundProgression = 0f;
		}
		else
		{
			mBoundProgression += deltaTime * 2520f;
			if (mBoundProgression > 180f)
			{
				mBoundProgression = 180f;
			}
			float y = Mathf.Sin(mBoundProgression * ((float)Math.PI / 180f)) * Def.TILE_PITCH_V / 10f;
			LocalPositionOffset = new Vector3(0f, y, 0f);
		}
		if (mState.GetStatus() == TILE_STATE.CHANGEFORM)
		{
			return;
		}
		if (mGatherCheckWaitTimer > 0f)
		{
			mGatherCheckWaitTimer -= deltaTime;
			if (mGatherCheckWaitTimer <= 0f && mManager.IsThreeOrMoreGather(TilePos))
			{
				mManager.AddCombo();
				mManager.MakeHitNum(LocalPosition);
				if (UnityEngine.Random.Range(0, 100) < 50)
				{
					mManager.CrushTile(this, Def.DIR.DOWN);
				}
				else
				{
					mManager.CrushTile(this, Def.DIR.RIGHT);
				}
				mFallState.Change(TILE_FALL_STATE.STAY);
				LocalPositionOffset = Vector3.zero;
			}
		}
		if (mBoundProgression >= 180f && mGatherCheckWaitTimer <= 0f)
		{
			mFallState.Change(TILE_FALL_STATE.STAY);
			LocalPositionOffset = Vector3.zero;
		}
	}

	protected virtual void stateStay(float deltaTime)
	{
		if (mState.IsChanged())
		{
			mStayTimer = 0f;
		}
		mStayTimer += deltaTime;
		if (mFallState.GetStatus() != 0)
		{
			return;
		}
		if (mChangeFormWaitTimer > 0f)
		{
			mChangeFormWaitTimer -= deltaTime;
			if (mChangeFormWaitTimer <= 0f)
			{
				ChangeForm(mTargetKind, mTargetForm, mCrushWaitTimer, 0f, false, true, mChangeFormScore, false);
			}
		}
		else if (mCrushWaitTimer > 0f)
		{
			mCrushWaitTimer -= deltaTime;
			if (mCrushWaitTimer <= 0f && mManager.BeginCrush(this, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, mScore, mRainbowTargetKind, mBombSize))
			{
				mManager.BeginAttributeCrush(TilePos, 0f, false);
			}
		}
		else if (mBombWaitTimer > 0f)
		{
			mBombWaitTimer -= deltaTime;
			if (mBombWaitTimer <= 0f && mManager.BeginCrush(this, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, mScore, mRainbowTargetKind, mBombSize))
			{
				mManager.BeginAttributeCrush(TilePos, 0f, false);
			}
		}
		else if (isColor() && Form == Def.TILE_FORM.NORMAL && deltaTime > 0f)
		{
			if (UnityEngine.Random.Range(0, 500) == 1)
			{
				Shine();
			}
		}
		else if (isTapTriggerTile() && mStayTimer >= 4f)
		{
			mStayTimer = 0f;
			CreateTapAppealEffect();
		}
	}

	private void stateFloat(float deltaTime)
	{
		mFloatTime += deltaTime;
	}

	private void stateSwap(float deltaTime)
	{
		mMoveProgression += deltaTime * Def.SWAP_SPEED;
		if (mMoveProgression > 90f)
		{
			mMoveProgression = 90f;
		}
		Vector2 vector = default(Vector2);
		vector.x = mMoveOriginPos.x + (1f - Mathf.Cos(mMoveProgression * ((float)Math.PI / 180f))) * Def.TILE_PITCH_H * (float)Def.DIR_OFS[(int)mMoveDirection].x;
		vector.y = mMoveOriginPos.y + (1f - Mathf.Cos(mMoveProgression * ((float)Math.PI / 180f))) * Def.TILE_PITCH_V * (float)Def.DIR_OFS[(int)mMoveDirection].y;
		float z = LocalPosition.z;
		LocalPosition = new Vector3(vector.x, vector.y, z);
		if (mMoveProgression >= 90f)
		{
			mState.Reset(TILE_STATE.SWAPEND, true);
		}
	}

	private void stateReturn(float deltaTime)
	{
		mMoveProgression += deltaTime * Def.SWAPRET_SPEED;
		if (mMoveProgression > 90f)
		{
			mMoveProgression = 90f;
		}
		Vector2 vector = default(Vector2);
		vector.x = mMoveOriginPos.x + (1f - Mathf.Cos(mMoveProgression * ((float)Math.PI / 180f))) * Def.TILE_PITCH_H * (float)Def.DIR_OFS[(int)mMoveDirection].x;
		vector.y = mMoveOriginPos.y + (1f - Mathf.Cos(mMoveProgression * ((float)Math.PI / 180f))) * Def.TILE_PITCH_V * (float)Def.DIR_OFS[(int)mMoveDirection].y;
		float z = LocalPosition.z;
		LocalPosition = new Vector3(vector.x, vector.y, z);
		if (mMoveProgression >= 90f)
		{
			mState.Reset(TILE_STATE.RETURNEND, true);
		}
	}

	protected virtual void stateCrushWait(float deltaTime)
	{
		mCrushWaitTimer -= deltaTime;
		if (!(mCrushWaitTimer <= 0f))
		{
			return;
		}
		mState.Reset(TILE_STATE.CRUSH, true);
		switch (mEffectOverride)
		{
		case Def.EFFECT_OVERRIDE.NORMAL:
			switch (Form)
			{
			case Def.TILE_FORM.WAVE_H:
				mManager.SpecialWaveH(TilePos);
				changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
				SpawnOneShotAnime("Effect", "EFFECT_STRIPE_EXPLOSION_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 0f, 0f);
				mGame.PlaySe("SE_WAVE", 2);
				break;
			case Def.TILE_FORM.WAVE_V:
				mManager.SpecialWaveV(TilePos);
				changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
				SpawnOneShotAnime("Effect", "EFFECT_STRIPE_EXPLOSION_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 90f, 0f);
				mGame.PlaySe("SE_WAVE", 2);
				break;
			case Def.TILE_FORM.BOMB:
				mManager.SpecialBomb(TilePos, mBombSize);
				bomb();
				mGame.PlaySe("SE_BOMB", 2);
				break;
			case Def.TILE_FORM.BOMB_B:
				if (isColor())
				{
					mManager.SpecialBomb(TilePos, mBombSize);
					changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
					SpawnOneShotAnime("Effect", "EFFECT_BOMB_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 0f, 0f);
					mGame.PlaySe("SE_BOMB", 2);
					break;
				}
				switch (Kind)
				{
				case Def.TILE_KIND.ORB_WAVE_V2:
					mManager.SpecialWaveV(TilePos);
					SpawnOneShotAnime("Effect", "EFFECT_STRIPE_EXPLOSION_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 90f, 0f);
					mGame.PlaySe("SE_WAVE", 2);
					break;
				case Def.TILE_KIND.ORB_WAVE_H2:
					mManager.SpecialWaveH(TilePos);
					SpawnOneShotAnime("Effect", "EFFECT_STRIPE_EXPLOSION_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 0f, 0f);
					mGame.PlaySe("SE_WAVE", 2);
					break;
				case Def.TILE_KIND.ORB_BOMB2:
					mManager.SpecialBomb(TilePos, mBombSize);
					SpawnOneShotAnime("Effect", "EFFECT_BOMB_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 0f, 0f);
					mGame.PlaySe("SE_BOMB", 2);
					break;
				case Def.TILE_KIND.ORB_BOSSDAMAGE2:
					mManager.SpecialBossDamage(this);
					mGame.PlaySe("SE_BOMB", 2);
					break;
				}
				changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
				break;
			case Def.TILE_FORM.RAINBOW:
				mManager.SpecialRainbow(TilePos, mRainbowTargetKind);
				crush(0.5f, Def.EFFECT_OVERRIDE.DISABLE, 0, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, mBombSize, true);
				SpawnOneShotAnime("Rainbow", "EFFECT_RAINBOW_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -5f), Vector3.one, 0f, 0f);
				mGame.PlaySe("SE_RAINBOW", 2);
				break;
			case Def.TILE_FORM.PAINT:
			{
				mManager.SpecialPaint(TilePos, mRainbowTargetKind, Kind);
				crush(0.5f, Def.EFFECT_OVERRIDE.DISABLE, Def.PAINT_CLASH_SCORE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, mBombSize, true);
				string key = "EFFECT_RAINBOW_EXPLOSION";
				switch (Kind)
				{
				case Def.TILE_KIND.COLOR0:
					key = "EFFECT_PAINT_COLOR0";
					break;
				case Def.TILE_KIND.COLOR1:
					key = "EFFECT_PAINT_COLOR1";
					break;
				case Def.TILE_KIND.COLOR2:
					key = "EFFECT_PAINT_COLOR2";
					break;
				case Def.TILE_KIND.COLOR3:
					key = "EFFECT_PAINT_COLOR3";
					break;
				case Def.TILE_KIND.COLOR4:
					key = "EFFECT_PAINT_COLOR4";
					break;
				case Def.TILE_KIND.COLOR5:
					key = "EFFECT_PAINT_COLOR5";
					break;
				}
				SpawnOneShotAnime("Paint", key, mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -5f), Vector3.one, 0f, 0f);
				Util.CreateOneShotParticle("PaintParticle", "Particles/PaintParticle", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -5f), Vector3.one, 3f, 0f);
				mGame.PlaySe("SE_RAINBOW", 2);
				break;
			}
			case Def.TILE_FORM.TAP_BOMB:
				mManager.SpecialBomb(TilePos, mBombSize);
				changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
				SpawnOneShotAnime("Effect", "EFFECT_TAP_BOMB_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 0f, 0f);
				mGame.PlaySe("SE_BOMB", 2);
				break;
			case Def.TILE_FORM.ADDTIME:
				stateCrushWaitSub_NormalExplosion();
				CreateTimeEffect(UnityEngine.Random.Range(0f, 0.4f), Def.ADDTIME_TILE_SEC);
				mGame.PlaySe("SE_ADDTIME_TILE", 2);
				break;
			case Def.TILE_FORM.ADDPLUS:
				stateCrushWaitSub_NormalExplosion();
				CreateMovesEffect(UnityEngine.Random.Range(0f, 0.4f), Def.ADDMOVE_TILE_MOVES);
				mGame.PlaySe("SE_RANKUP2", 2);
				break;
			case Def.TILE_FORM.JEWEL:
			case Def.TILE_FORM.JEWEL3:
			case Def.TILE_FORM.JEWEL5:
			case Def.TILE_FORM.JEWEL9:
				changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
				CreateJewelEffectAnime();
				break;
			case Def.TILE_FORM.PEARL:
				changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
				CreatePearlEffect(UnityEngine.Random.Range(0f, 0.4f));
				CreatePearlEffectAnime();
				break;
			default:
				stateCrushWaitSub_NormalExplosion();
				break;
			}
			break;
		case Def.EFFECT_OVERRIDE.WAVE_WAVE:
			mManager.SpecialCombo_WaveWave(TilePos);
			changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
			SpawnOneShotAnime("Effect", "EFFECT_STRIPE_EXPLOSION_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 0f, 0f);
			SpawnOneShotAnime("Effect", "EFFECT_STRIPE_EXPLOSION_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 90f, 0f);
			mGame.PlaySe("SE_WAVE", 2);
			break;
		case Def.EFFECT_OVERRIDE.WAVE_BOMB:
			mManager.SpecialCombo_WaveBomb(TilePos);
			changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
			SpawnOneShotAnime("Effect", "EFFECT_BOMB_STRIPE_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 0f, Def.WAVE_BOMB_FIRST_CROSS_TIME);
			SpawnOneShotAnime("Effect", "EFFECT_BOMB_STRIPE_H", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, -3f), Vector3.one, 90f, Def.WAVE_BOMB_SECOND_CROSS_TIME);
			mGame.PlaySe("SE_WAVE_BOMB", 2);
			break;
		case Def.EFFECT_OVERRIDE.BOMB_BOMB:
			mManager.SpecialCombo_BombBomb(TilePos);
			bomb();
			mGame.PlaySe("SE_BOMB", 2);
			break;
		case Def.EFFECT_OVERRIDE.WAVE_RAINBOW:
			mManager.SpecialCombo_WaveRainbow(TilePos, mRainbowTargetKind);
			stateCrushWaitSub_NormalExplosion();
			mGame.PlaySe("SE_WAVE_RAINBOW", 2);
			break;
		case Def.EFFECT_OVERRIDE.BOMB_RAINBOW:
			mManager.SpecialCombo_BombRainbow(TilePos, mRainbowTargetKind);
			stateCrushWaitSub_NormalExplosion();
			mGame.PlaySe("SE_BOMB_RAINBOW", 2);
			break;
		case Def.EFFECT_OVERRIDE.RAINBOW_RAINBOW:
			mManager.SpecialCombo_RainbowRainbow(TilePos);
			stateCrushWaitSub_NormalExplosion();
			mGame.PlaySe("SE_RAINBOW_RAINBOW", 2);
			break;
		case Def.EFFECT_OVERRIDE.BOMB_PAINT:
			mManager.SpecialCombo_BombPaint(TilePos, Kind, mRainbowTargetKind);
			stateCrushWaitSub_NormalExplosion();
			mGame.PlaySe("SE_BOMB_RAINBOW", 2);
			break;
		case Def.EFFECT_OVERRIDE.WAVE_PAINT:
			mManager.SpecialCombo_WavePaint(TilePos, Kind, mRainbowTargetKind);
			stateCrushWaitSub_NormalExplosion();
			mGame.PlaySe("SE_WAVE_RAINBOW", 2);
			break;
		case Def.EFFECT_OVERRIDE.PAINT_RAINBOW:
			mManager.SpecialCombo_PaintRainbow(TilePos, Kind);
			stateCrushWaitSub_NormalExplosion();
			mGame.PlaySe("SE_RAINBOW_RAINBOW", 2);
			break;
		case Def.EFFECT_OVERRIDE.PAINT_PAINT:
			mManager.SpecialCombo_PaintPaint(TilePos, Kind);
			stateCrushWaitSub_NormalExplosion();
			mGame.PlaySe("SE_RAINBOW_RAINBOW", 2);
			break;
		case Def.EFFECT_OVERRIDE.DISABLE:
			stateCrushWaitSub_NormalExplosion();
			break;
		default:
			stateCrushWaitSub_NormalExplosion();
			break;
		}
	}

	protected virtual void stateCrushWaitSub_NormalExplosion()
	{
		switch (Kind)
		{
		case Def.TILE_KIND.THROUGH_WALL0:
			mAnimeFinished = true;
			mGame.PlaySe("SE_THROUGH_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_THROUGH_WALL0_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0.5f, mScore);
			break;
		case Def.TILE_KIND.THROUGH_WALL1:
			mAnimeFinished = true;
			mGame.PlaySe("SE_THROUGH_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_THROUGH_WALL1_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0.5f, mScore);
			break;
		case Def.TILE_KIND.THROUGH_WALL2:
			mAnimeFinished = true;
			mGame.PlaySe("SE_THROUGH_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_THROUGH_WALL2_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0.5f, mScore);
			break;
		case Def.TILE_KIND.BROKEN_WALL0:
			mAnimeFinished = true;
			mGame.PlaySe("SE_BROKEN_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_BROKEN_WALL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.BROKEN_WALL1:
			mAnimeFinished = true;
			mGame.PlaySe("SE_BROKEN_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_BROKEN_WALL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.BROKEN_WALL2:
			mAnimeFinished = true;
			mGame.PlaySe("SE_BROKEN_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_BROKEN_WALL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.MOVABLE_WALL0:
			mAnimeFinished = true;
			mGame.PlaySe("SE_BROKEN_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_MOVABLE_WALL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.MOVABLE_WALL1:
			mAnimeFinished = true;
			mGame.PlaySe("SE_BROKEN_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_MOVABLE_WALL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.MOVABLE_WALL2:
			mAnimeFinished = true;
			mGame.PlaySe("SE_BROKEN_WALL", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_MOVABLE_WALL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.STAR:
			changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
			mGame.PlaySe("SE_RESULT_STAR_GOLD", 0);
			break;
		case Def.TILE_KIND.CAPTURE0:
			mAnimeFinished = true;
			mGame.PlaySe("SE_CAPTURE", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_CAPTURE_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0.5f, mScore);
			break;
		case Def.TILE_KIND.CAPTURE1:
			mAnimeFinished = true;
			mGame.PlaySe("SE_CAPTURE", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_CAPTURE_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.CAPTURE2:
			mAnimeFinished = true;
			mGame.PlaySe("SE_CAPTURE", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_CAPTURE_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.INCREASE:
			changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
			mGame.PlaySe("SE_BOSS_DAMAGE", -1);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.GROWN_UP0:
			changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
			mGame.PlaySe("SE_GROWN_UP", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_CRISTAL2_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.GROWN_UP1:
			mAnimeFinished = true;
			mGame.PlaySe("SE_GROWN_UP", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_CRISTAL1_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.GROWN_UP2:
			mAnimeFinished = true;
			mGame.PlaySe("SE_GROWN_UP", -1);
			Util.CreateOneShotAnime("Effect", "EFFECT_CRISTAL1_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.PAIR0_PARTS:
			mAnimeFinished = true;
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.PAIR1_PARTS:
			mAnimeFinished = true;
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.SKILL_BALL:
			changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
			break;
		case Def.TILE_KIND.ORB_WAVE_V0:
		case Def.TILE_KIND.ORB_WAVE_V1:
		case Def.TILE_KIND.ORB_WAVE_H0:
		case Def.TILE_KIND.ORB_WAVE_H1:
		case Def.TILE_KIND.ORB_BOMB0:
		case Def.TILE_KIND.ORB_BOMB1:
		case Def.TILE_KIND.ORB_BOSSDAMAGE0:
		case Def.TILE_KIND.ORB_BOSSDAMAGE1:
			NextKind = Kind + 1;
			setOrbCrushFlg(true);
			mAnimeFinished = true;
			CreateOrbUpAnime();
			CreateScoreEffect(0f, mScore);
			break;
		case Def.TILE_KIND.ORB_WAVE_V2:
		case Def.TILE_KIND.ORB_WAVE_H2:
		case Def.TILE_KIND.ORB_BOMB2:
		case Def.TILE_KIND.ORB_BOSSDAMAGE2:
			mAnimeFinished = true;
			bomb_orb();
			mGame.PlaySe("SE_ORB_BREAK", -1);
			break;
		case Def.TILE_KIND.FIXED_FLOWER0:
		case Def.TILE_KIND.MOVABLE_FLOWER0:
			NextKind = Kind + 1;
			mAnimeFinished = true;
			mGame.PlaySe("SE_FLOWER_GLOW", -1);
			break;
		case Def.TILE_KIND.FIXED_FLOWER1:
		case Def.TILE_KIND.MOVABLE_FLOWER1:
			NextKind = mManager.RandomColorTile(-1);
			NextForm = Def.TILE_FORM.PEARL;
			mAnimeFinished = true;
			mGame.PlaySe("SE_FLOWER_GLOW", -1);
			break;
		case Def.TILE_KIND.TALISMAN0_3:
			NextKind = Def.TILE_KIND.TALISMAN0_0;
			mAnimeFinished = true;
			CreateTalismanFaceCrushEffectAnime();
			mTalismanFaceCrush = true;
			((PuzzleManager)mManager).OnTalismanPlaceMovableWall(this, 2, 0, 0);
			break;
		case Def.TILE_KIND.TALISMAN1_3:
			NextKind = Def.TILE_KIND.TALISMAN1_0;
			mAnimeFinished = true;
			CreateTalismanFaceCrushEffectAnime();
			mTalismanFaceCrush = true;
			((PuzzleManager)mManager).OnTalismanPlaceMovableWall(this, 2, 0, 0);
			break;
		case Def.TILE_KIND.TALISMAN2_3:
			NextKind = Def.TILE_KIND.TALISMAN2_0;
			mAnimeFinished = true;
			CreateTalismanFaceCrushEffectAnime();
			mTalismanFaceCrush = true;
			((PuzzleManager)mManager).OnTalismanPlaceMovableWall(this, 2, 0, 0);
			break;
		default:
		{
			changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
			float num = UnityEngine.Random.Range(0f, Def.EXPLOSION_SCALE_RANDOM) + 1f;
			float delay = UnityEngine.Random.Range(0f, Def.EXPLOSION_DELAY_RANDOM);
			SpawnOneShotAnime("Effect_2", "EFFECT_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), new Vector3(num, num, 1f), 0f, delay);
			CreateScoreEffect(0.1f, mScore);
			break;
		}
		}
	}

	protected void stateCrush(float deltaTime)
	{
		if (mAnimeFinished)
		{
			mState.Reset(TILE_STATE.CRUSHEND, true);
		}
	}

	private void stateBomb(float deltaTime)
	{
		if (mAnimeFinished)
		{
			stayBombB();
		}
	}

	protected void stateChangeForm(float deltaTime)
	{
		if (!mAnimeFinished)
		{
			return;
		}
		stay();
		if (isAttributeTile() || (mFallState.GetStatus() != 0 && mFallState.GetStatus() != TILE_FALL_STATE.FALLEND))
		{
			return;
		}
		mManager.RemoveTile(this, true);
		mManager.PlaceTile(TilePos, this);
		if (mManager.IsThreeOrMoreGather(TilePos))
		{
			mManager.AddCombo();
			mManager.MakeHitNum(LocalPosition);
			if (UnityEngine.Random.Range(0, 100) < 50)
			{
				mManager.CrushTile(this, Def.DIR.DOWN);
			}
			else
			{
				mManager.CrushTile(this, Def.DIR.RIGHT);
			}
		}
	}

	public TILE_STATE GetState()
	{
		return mState.GetStatus();
	}

	public TILE_FALL_STATE GetFallState()
	{
		return mFallState.GetStatus();
	}

	public Vector2 GetFallVelocity()
	{
		return mFallVelocity;
	}

	public bool isWave()
	{
		if (Form == Def.TILE_FORM.WAVE_H || Form == Def.TILE_FORM.WAVE_V)
		{
			return true;
		}
		return false;
	}

	public bool isColor()
	{
		return mData.isColor;
	}

	public bool isNormalForm()
	{
		if (Form == Def.TILE_FORM.NORMAL)
		{
			return true;
		}
		return false;
	}

	public bool isSpecialForm()
	{
		if (Form == Def.TILE_FORM.BOMB || Form == Def.TILE_FORM.BOMB_B || Form == Def.TILE_FORM.PAINT || Form == Def.TILE_FORM.RAINBOW || Form == Def.TILE_FORM.WAVE_H || Form == Def.TILE_FORM.WAVE_V)
		{
			return true;
		}
		return false;
	}

	public bool isJewel()
	{
		if (Form == Def.TILE_FORM.JEWEL || Form == Def.TILE_FORM.JEWEL3 || Form == Def.TILE_FORM.JEWEL5 || Form == Def.TILE_FORM.JEWEL9)
		{
			return true;
		}
		return false;
	}

	public bool isExitableTile()
	{
		return mData.isExit;
	}

	public bool isIngredient()
	{
		return mData.isIngredient;
	}

	public bool isPresentBox()
	{
		return mData.isPresentBox;
	}

	public bool isKey()
	{
		return mData.isKey;
	}

	public bool isFind()
	{
		return mData.isFind;
	}

	public bool isMovableWall()
	{
		return mData.isMovableWall;
	}

	public bool isAttributeTile()
	{
		if (mData.tileType == TileData.TILE_TYPE.ATTRIBUTE)
		{
			return true;
		}
		return false;
	}

	public bool isThrough()
	{
		return mData.isThrough;
	}

	public bool isThroughWall()
	{
		return mData.isThroughWall;
	}

	public bool isBrokenWall()
	{
		return mData.isBrokenWall;
	}

	public bool isGrownUp()
	{
		return mData.isGrownUp;
	}

	public bool isCapture()
	{
		return mData.isCapture;
	}

	public bool isOrb()
	{
		return mData.isOrb;
	}

	public bool isFlower()
	{
		return mData.isFlower;
	}

	public bool isTalisman()
	{
		return mData.isTalisman;
	}

	public bool isTalismanFace()
	{
		return mData.isTalismanFace;
	}

	public bool isCandyCaptor()
	{
		return mData.isCaptor;
	}

	public bool isTapTriggerTile()
	{
		if (Kind == Def.TILE_KIND.SKILL_BALL || Form == Def.TILE_FORM.TAP_BOMB)
		{
			return true;
		}
		return false;
	}

	public bool isTimer()
	{
		return false;
	}

	public bool isPair()
	{
		if (Kind == Def.TILE_KIND.PAIR0_PARTS || Kind == Def.TILE_KIND.PAIR0_PARTS)
		{
			return true;
		}
		return false;
	}

	public bool isPairComplete()
	{
		if (Kind == Def.TILE_KIND.PAIR0_COMPLETE || Kind == Def.TILE_KIND.PAIR0_COMPLETE)
		{
			return true;
		}
		return false;
	}

	public bool isFootstamp()
	{
		if (Form == Def.TILE_FORM.FOOT_STAMP)
		{
			return true;
		}
		return false;
	}

	public bool isDiana()
	{
		if (Kind == Def.TILE_KIND.DIANA)
		{
			return true;
		}
		return false;
	}

	public bool isMove()
	{
		if (mBombWaitTimer > 0f || mCrushWaitTimer > 0f || mChangeFormWaitTimer > 0f)
		{
			return true;
		}
		if (mFallState.GetStatus() != 0)
		{
			return true;
		}
		if (mState.GetStatus() == TILE_STATE.CHANGEFORM)
		{
			return true;
		}
		if (isTimer() && mState.GetStatus() == TILE_STATE.TIMEREFFECT)
		{
			return true;
		}
		if (mState.GetStatus() == TILE_STATE.STAY || mState.GetStatus() == TILE_STATE.FLOAT)
		{
			return false;
		}
		return true;
	}

	public bool isMovable()
	{
		return mData.isMove;
	}

	public bool isSwapMove()
	{
		if (mState.GetStatus() == TILE_STATE.SWAP || mState.GetStatus() == TILE_STATE.SWAPEND || mState.GetStatus() == TILE_STATE.RETURN || mState.GetStatus() == TILE_STATE.RETURNEND)
		{
			return true;
		}
		return false;
	}

	public bool isFallable()
	{
		if (!mData.isFall)
		{
			return false;
		}
		if (mFallRock)
		{
			return false;
		}
		if (mState.GetStatus() == TILE_STATE.STAY || mState.GetStatus() == TILE_STATE.FLOAT || mState.GetStatus() == TILE_STATE.CHANGEFORM)
		{
			return true;
		}
		return false;
	}

	private IntVector2 fallTargetCheck()
	{
		bool exitable = isExitableTile();
		return mManager.SearchFallTarget(TilePos, exitable);
	}

	public void stay()
	{
		mFallResistTime = 0f;
		mState.Reset(TILE_STATE.STAY, true);
		if (mBombWaitTimer > 0f)
		{
			changeAnime(TileData.DROP_ANIME_TYPE.GLOW_BOMB_WAIT, 0);
		}
		else
		{
			changeAnime(TileData.DROP_ANIME_TYPE.NORMAL, 0);
		}
	}

	public void stayBombB()
	{
		setBombTimer(Def.BOMB_B_STAY_TIME);
		Form = Def.TILE_FORM.BOMB_B;
		mFallResistTime = 0f;
		mState.Reset(TILE_STATE.STAY, true);
		changeAnime(TileData.DROP_ANIME_TYPE.GLOW_BOMB_WAIT, 0);
	}

	public bool selectFloat(Vector2 orignWorldPos)
	{
		if (mState.GetStatus() != 0)
		{
			return false;
		}
		if (mFallState.GetStatus() != 0)
		{
			return false;
		}
		if (mBombWaitTimer > 0f)
		{
			return false;
		}
		mDragOriginPos = orignWorldPos;
		mFloatTime = 0f;
		mState.Reset(TILE_STATE.FLOAT, true);
		return true;
	}

	public virtual void selectFloatEnd()
	{
		if (mState.GetStatus() != TILE_STATE.FLOAT)
		{
			return;
		}
		stay();
		if (isTapTriggerTile() && mFloatTime < 0.5f)
		{
			if (Form == Def.TILE_FORM.TAP_BOMB)
			{
				if (mManager.IsItemUsableTapItem())
				{
					if (Kind == Def.TILE_KIND.SKILL_BALL)
					{
						mManager.SkillPushed(this);
					}
					else
					{
						mManager.BeginTapCrush(this);
					}
				}
			}
			else if (mManager.IsItemUsable())
			{
				if (Kind == Def.TILE_KIND.SKILL_BALL)
				{
					mManager.SkillPushed(this);
				}
				else
				{
					mManager.BeginTapCrush(this);
				}
			}
		}
		if (!GameMain.USE_DEBUG_DIALOG || !(mFloatTime >= 0.5f))
		{
			return;
		}
		if (mGame.mDebugLongPressMode)
		{
			if (Kind == Def.TILE_KIND.COLOR0)
			{
				mManager.RemoveTile(this, true);
				ChangeForm(Def.TILE_KIND.COLOR1, Form, 0f, 0f, false, true, 0, false);
			}
			else if (Kind == Def.TILE_KIND.COLOR1)
			{
				mManager.RemoveTile(this, true);
				ChangeForm(Def.TILE_KIND.COLOR2, Form, 0f, 0f, false, true, 0, false);
			}
			else if (Kind == Def.TILE_KIND.COLOR2)
			{
				mManager.RemoveTile(this, true);
				ChangeForm(Def.TILE_KIND.COLOR3, Form, 0f, 0f, false, true, 0, false);
			}
			else if (Kind == Def.TILE_KIND.COLOR3)
			{
				mManager.RemoveTile(this, true);
				ChangeForm(Def.TILE_KIND.COLOR4, Form, 0f, 0f, false, true, 0, false);
			}
			else if (Kind == Def.TILE_KIND.COLOR4)
			{
				mManager.RemoveTile(this, true);
				ChangeForm(Def.TILE_KIND.COLOR5, Form, 0f, 0f, false, true, 0, false);
			}
			else if (Kind == Def.TILE_KIND.COLOR5)
			{
				mManager.RemoveTile(this, true);
				ChangeForm(Def.TILE_KIND.COLOR0, Form, 0f, 0f, false, true, 0, false);
			}
		}
		else if (Form == Def.TILE_FORM.NORMAL)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Kind, Def.TILE_FORM.WAVE_H, 0f, 0f, false, true, 0, false);
		}
		else if (Form == Def.TILE_FORM.WAVE_H)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Kind, Def.TILE_FORM.WAVE_V, 0f, 0f, false, true, 0, false);
		}
		else if (Form == Def.TILE_FORM.WAVE_V)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Kind, Def.TILE_FORM.BOMB, 0f, 0f, false, true, 0, false);
		}
		else if (Form == Def.TILE_FORM.BOMB)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Kind, Def.TILE_FORM.PAINT, 0f, 0f, false, true, 0, false);
		}
		else if (Form == Def.TILE_FORM.PAINT)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Kind, Def.TILE_FORM.FOOT_STAMP, 0f, 0f, false, true, 0, false);
		}
		else if (Form == Def.TILE_FORM.FOOT_STAMP)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Def.TILE_KIND.SPECIAL, Def.TILE_FORM.RAINBOW, 0f, 0f, false, true, 0, false);
		}
		else if (Form == Def.TILE_FORM.RAINBOW)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Def.TILE_KIND.SPECIAL, Def.TILE_FORM.TAP_BOMB, 0f, 0f, false, true, 0, false);
		}
		else if (Form == Def.TILE_FORM.TAP_BOMB)
		{
			mManager.RemoveTile(this, true);
			ChangeForm(Def.TILE_KIND.SKILL_BALL, Def.TILE_FORM.NORMAL, 0f, 0f, false, true, 0, false);
		}
	}

	public void move(TILE_STATE newState, Def.DIR dir)
	{
		mMoveOriginPos = new Vector2(LocalPosition.x, LocalPosition.y);
		mMoveDirection = dir;
		mMoveProgression = 0f;
		mState.Reset(newState, true);
		SpawnOneShotAnime("Effect_mv", "EFFECT_CHANGE_CURSOR", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
	}

	public bool crush(float waitTime, Def.EFFECT_OVERRIDE effectOverride, int score, Def.TILE_KIND nextKind, Def.TILE_FORM nextForm, Def.TILE_KIND rainbowTargetKind, int bombSize, bool crushCountSkip = false)
	{
		if (mState.GetStatus() != TILE_STATE.CRUSHWAIT)
		{
			mState.Reset(TILE_STATE.CRUSHWAIT, true);
			mCrushWaitTimer = waitTime;
			mEffectOverride = effectOverride;
			NextKind = nextKind;
			NextForm = nextForm;
			mScore = score;
			mRainbowTargetKind = rainbowTargetKind;
			mBombSize = bombSize;
			if (!crushCountSkip)
			{
				SpecialCrush(Form);
			}
			return true;
		}
		return false;
	}

	public bool AttributeCrush(float timer, bool side)
	{
		bool flag = false;
		if (Kind == Def.TILE_KIND.CAPTURE0 || Kind == Def.TILE_KIND.CAPTURE1)
		{
			flag = true;
		}
		if (mState.GetStatus() != 0 && !flag)
		{
			return false;
		}
		switch (Kind)
		{
		case Def.TILE_KIND.THROUGH_WALL0:
			if (!side)
			{
				crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 250, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
				return true;
			}
			break;
		case Def.TILE_KIND.THROUGH_WALL1:
			if (!side)
			{
				crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 250, Def.TILE_KIND.THROUGH_WALL0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
				return true;
			}
			break;
		case Def.TILE_KIND.THROUGH_WALL2:
			if (!side)
			{
				crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 250, Def.TILE_KIND.THROUGH_WALL1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
				return true;
			}
			break;
		case Def.TILE_KIND.BROKEN_WALL0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.BROKEN_WALL1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.BROKEN_WALL0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.BROKEN_WALL2:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.BROKEN_WALL1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.MOVABLE_WALL0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.MOVABLE_WALL1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.MOVABLE_WALL0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.MOVABLE_WALL2:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.MOVABLE_WALL1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.STAR:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.CAPTURE0:
			if (!side)
			{
				crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
				return true;
			}
			break;
		case Def.TILE_KIND.CAPTURE1:
			if (!side)
			{
				crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.CAPTURE0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
				return true;
			}
			break;
		case Def.TILE_KIND.CAPTURE2:
			if (!side)
			{
				crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.CAPTURE1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
				return true;
			}
			break;
		case Def.TILE_KIND.INCREASE:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 30, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.GROWN_UP0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 500, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.GROWN_UP1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.GROWN_UP0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.GROWN_UP2:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.GROWN_UP1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_WAVE_V0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_WAVE_V1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_WAVE_V1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_WAVE_V2, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_WAVE_V2:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 500, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_WAVE_H0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_WAVE_H1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_WAVE_H1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_WAVE_H2, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_WAVE_H2:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 500, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_BOMB0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_BOMB1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 1);
			return true;
		case Def.TILE_KIND.ORB_BOMB1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_BOMB2, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 1);
			return true;
		case Def.TILE_KIND.ORB_BOMB2:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 500, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 1);
			return true;
		case Def.TILE_KIND.ORB_BOSSDAMAGE0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_BOSSDAMAGE1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_BOSSDAMAGE1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.ORB_BOSSDAMAGE2, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.ORB_BOSSDAMAGE2:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 500, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.MOVABLE_FLOWER0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.MOVABLE_FLOWER1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.MOVABLE_FLOWER1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.COLOR0, Def.TILE_FORM.PEARL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.FIXED_FLOWER0:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.FIXED_FLOWER1, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.FIXED_FLOWER1:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.COLOR0, Def.TILE_FORM.PEARL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.TALISMAN0_3:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.TALISMAN0_0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.TALISMAN1_3:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.TALISMAN1_0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		case Def.TILE_KIND.TALISMAN2_3:
			crush(timer, Def.EFFECT_OVERRIDE.NORMAL, 0, Def.TILE_KIND.TALISMAN2_0, Def.TILE_FORM.NORMAL, Def.TILE_KIND.NONE, 0);
			return true;
		}
		return false;
	}

	public void bomb()
	{
		mState.Reset(TILE_STATE.BOMB, true);
		changeAnime(TileData.DROP_ANIME_TYPE.EXPLOSION, 1);
		SpawnOneShotAnime("Effect_bomb", "EFFECT_BOMB_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f);
	}

	public void bomb_orb()
	{
		mState.Reset(TILE_STATE.BOMB, true);
	}

	public void fall(IntVector2 targetPos)
	{
		mManager.RemoveTile(this, true);
		mManager.MoveTempCandy(this, targetPos);
		mFallTarget = targetPos;
		mFallVelocity = new Vector2(0f, (0f - Def.GRAVITY) * Def.FALL_INIT_SPEED_RATIO);
		if (TilePos.x == targetPos.x)
		{
			PuzzleTile tile = mManager.GetTile(new IntVector2(targetPos.x, targetPos.y - 1), true);
			if (tile != null && tile.GetFallState() == TILE_FALL_STATE.FALL)
			{
				mFallVelocity = tile.GetFallVelocity();
			}
		}
		mFallStartCoords = mManager.GetDefaultCoords(TilePos);
		mFallTargetCoords = mManager.GetFallTargetCoords(mFallTarget);
		mFallUnderCoords = mManager.GetDefaultCoords(new IntVector2(TilePos.x, TilePos.y - 1));
		mFallState.Change(TILE_FALL_STATE.FALL);
	}

	public void fallEnd()
	{
		mFallState.Change(TILE_FALL_STATE.FALLEND);
		mGatherCheckWaitTimer = Def.GATHER_CHECK_WAIT_TIMER;
		mGame.PlaySe("SE_BOUND", -1);
	}

	public void ChangeForm(Def.TILE_KIND kind, Def.TILE_FORM form, float afterCrushTimer, float delayTime, bool immediate, bool useAfterChangeAnime, int addScore, bool createCountUp)
	{
		mTargetKind = Def.TILE_KIND.NONE;
		mTargetForm = Def.TILE_FORM.NONE;
		if (kind != Def.TILE_KIND.NONE)
		{
			mTargetKind = kind;
		}
		if (form != Def.TILE_FORM.NONE)
		{
			mTargetForm = form;
		}
		else
		{
			mTargetForm = Form;
		}
		mChangeFormScore = addScore;
		mCrushWaitTimer = afterCrushTimer;
		mChangeFormWaitTimer = delayTime;
		if (immediate)
		{
			Kind = mTargetKind;
			Form = mTargetForm;
			mData = mGame.mTileData[(int)mTargetKind];
			changeAnime(mPrevAnimeType, 0, true);
		}
		else if (mChangeFormWaitTimer > 0f)
		{
			mState.Reset(TILE_STATE.STAY, true);
		}
		else
		{
			Form = mTargetForm;
			if (mTargetKind != Def.TILE_KIND.NONE)
			{
				if (!isAttributeTile())
				{
					mManager.RemoveTile(this, true);
				}
				Kind = mTargetKind;
				mData = mGame.mTileData[(int)mTargetKind];
			}
			if (useAfterChangeAnime)
			{
				changeAnime(TileData.DROP_ANIME_TYPE.AFTER_CHANGE, 1);
			}
			else
			{
				changeAnime(mPrevAnimeType, 1, true);
			}
			if (Kind != Def.TILE_KIND.INCREASE && !mData.isTalisman && !mData.isTalismanFace)
			{
				CreateEnterEffect();
			}
			CreateScoreEffect(0f, mChangeFormScore);
			mState.Reset(TILE_STATE.CHANGEFORM, true);
		}
		if (createCountUp)
		{
			SpecialCreate(mTargetForm);
		}
	}

	public void ChangeColorForShuffle(Def.TILE_KIND kind, float waitTime)
	{
		mTargetKind = kind;
		mTargetForm = Form;
		mChangeFormWaitTimer = waitTime;
		mState.Reset(TILE_STATE.STAY, true);
		Kind = kind;
	}

	public void appeal()
	{
		if (mEffectOverTile != null)
		{
			UnityEngine.Object.Destroy(mEffectOverTile.gameObject);
		}
		mEffectOverTile = Util.CreateLoopAnime("AppealEffect", "EFFECT_APPEAL", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, false, null);
	}

	public void appealEnd()
	{
		if (mEffectOverTile != null)
		{
			UnityEngine.Object.Destroy(mEffectOverTile.gameObject);
			mEffectOverTile = null;
		}
	}

	private void Shine()
	{
		changeAnime(TileData.DROP_ANIME_TYPE.NORMAL_SHINE, 1, true);
	}

	public void Shake()
	{
		if (Form == Def.TILE_FORM.NORMAL && isColor())
		{
			changeAnime(TileData.DROP_ANIME_TYPE.NORMAL_SHAKE, 1, true);
		}
	}

	protected void changeAnime(TileData.DROP_ANIME_TYPE animeType, int playCount, bool force = false)
	{
		if (Sprite == null)
		{
			return;
		}
		string text = string.Empty;
		mData.GetAnimeType(ref animeType, Form, PrevKind);
		if (animeType != mPrevAnimeType || force)
		{
			text = mData.DropAnimeNames[(int)animeType];
			if (text == "null")
			{
				return;
			}
		}
		CHANGE_PART_INFO[] a_ret;
		mData.GetChangePartsInfo(animeType, Form, out a_ret, PrevKind);
		mPrevAnimeType = animeType;
		if (Sprite.ChangeAnime(text, a_ret, force, playCount, OnAnimationFinished))
		{
			Sprite._scl = new Vector3(Def.TILE_SCALE, Def.TILE_SCALE, 1f);
			mAnimeFinished = false;
		}
	}

	public void setBombTimer(float timer)
	{
		mBombWaitTimer = timer;
	}

	protected virtual void OnEnter()
	{
		if (isExitableTile())
		{
			CreateEnterEffect();
		}
	}

	public void setOrbCrushFlg(bool flg)
	{
		mOrbCrush = flg;
	}

	private void CreateEnterEffect()
	{
		if (!isOrb())
		{
			mEnterEffect = Util.CreateOneShotAnime("Enter", "EFFECT_AFTER_CHANGE", mSprite.gameObject, new Vector3(0f, 0f, -2f), Vector3.one, 0f, 0f);
		}
	}

	private void CreateTapAppealEffect()
	{
		mEnterEffect = Util.CreateOneShotAnime("Enter", "EFFECT_TAP_BALLOON", mSprite.gameObject, new Vector3(0f, 0f, -6f), Vector3.one, 0f, 0f);
	}

	public bool Contains(Vector2 worldPos)
	{
		if (mSpriteTransform == null)
		{
			return false;
		}
		Vector2 vector = Util.LogScreenRatio();
		Vector3 vector2 = mSpriteTransform.position * vector.y;
		float tILE_HITCHECK_V = Def.TILE_HITCHECK_V;
		float tILE_HITCHECK_H = Def.TILE_HITCHECK_H;
		if (vector2.x - tILE_HITCHECK_H > worldPos.x || vector2.x + tILE_HITCHECK_H < worldPos.x)
		{
			return false;
		}
		if (vector2.y - tILE_HITCHECK_V > worldPos.y || vector2.y + tILE_HITCHECK_V < worldPos.y)
		{
			return false;
		}
		return true;
	}

	public Def.DIR OnDrag(Vector2 worldPos)
	{
		Def.DIR result = Def.DIR.NONE;
		float num = Def.TILE_HEIGHT / (float)Screen.height;
		float num2 = Def.TILE_WIDTH / (float)Screen.width;
		if (mState.GetStatus() == TILE_STATE.FLOAT && isMovable() && !isMove())
		{
			float num3 = Mathf.Abs(worldPos.x - mDragOriginPos.x);
			float num4 = Mathf.Abs(worldPos.y - mDragOriginPos.y);
			if (num3 >= num4)
			{
				if (worldPos.x - mDragOriginPos.x < 0f - num2)
				{
					result = Def.DIR.LEFT;
				}
				else if (worldPos.x - mDragOriginPos.x > num2)
				{
					result = Def.DIR.RIGHT;
				}
			}
			else if (worldPos.y - mDragOriginPos.y < 0f - num)
			{
				result = Def.DIR.DOWN;
			}
			else if (worldPos.y - mDragOriginPos.y > num)
			{
				result = Def.DIR.UP;
			}
		}
		return result;
	}

	protected void SpawnOneShotAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, float rotateDeg, float delay)
	{
		OneShotAnime a_anime = Util.CreateOneShotAnime(name, key, parent, pos, scale, rotateDeg, delay, false, mManager.OneShotAnime_OnFinished);
		mManager.AddOneShotAnime(a_anime);
	}

	protected void CreateOrbUpAnime()
	{
		CHANGE_PART_INFO[] a_ret;
		mData.GetChangePartsInfo(TileData.DROP_ANIME_TYPE.NORMAL, Form, out a_ret);
		Util.CreateOneShotAnime("Effect", "EFFECT_ORB_PLUS", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, a_ret, ResourceManager.LoadImage("PUZZLE_TILE").Image);
		mGame.PlaySe("SE_ORB_GROWTH_PLUS", -1);
	}

	public void CreateOrbDownAnime()
	{
		CHANGE_PART_INFO[] a_ret;
		mData.GetChangePartsInfo(TileData.DROP_ANIME_TYPE.NORMAL, Form, out a_ret);
		Util.CreateOneShotAnime("Effect", "EFFECT_ORB_MINUS", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, a_ret, ResourceManager.LoadImage("PUZZLE_TILE").Image);
		mGame.PlaySe("SE_ORB_GROWTH_MINUS", 1);
	}

	protected void CreatePearlEffectAnime()
	{
		CHANGE_PART_INFO[] a_ret;
		mData.GetChangePartsInfo(TileData.DROP_ANIME_TYPE.EXPLOSION, Form, out a_ret);
		Util.CreateOneShotAnime("Effect", "EFFECT_PEARL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, a_ret, ResourceManager.LoadImage("PUZZLE_TILE").Image);
		mGame.PlaySe("SE_GC_GACHA_TAP_LIGHT", -1);
	}

	protected void CreateJewelEffectAnime()
	{
		CHANGE_PART_INFO[] a_ret;
		mData.GetChangePartsInfo(TileData.DROP_ANIME_TYPE.EXPLOSION, Form, out a_ret);
		Util.CreateOneShotAnime("Effect", "EFFECT_JEWEL_EXPLOSION", mManager.Root, new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_SCORE_Z - 1f), Vector3.one, 0f, 0f, a_ret, ResourceManager.LoadImage("PUZZLE_TILE").Image);
		mGame.PlaySe("SE_JEWEL_PIECE", 2);
	}

	protected void CreateTalismanFaceCrushEffectAnime()
	{
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[4]
		{
			new CHANGE_PART_INFO
			{
				partName = "change_000",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_001",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_002",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_003",
				spriteName = "null"
			}
		};
		switch (Kind)
		{
		case Def.TILE_KIND.TALISMAN0_3:
			array[0].spriteName = "talisman_00";
			array[1].spriteName = "talisman_00";
			array[2].spriteName = "gimmick_RT_orb";
			break;
		case Def.TILE_KIND.TALISMAN1_3:
			array[0].spriteName = "talisman_01";
			array[1].spriteName = "talisman_01";
			array[2].spriteName = "gimmick_RT_mirror";
			break;
		case Def.TILE_KIND.TALISMAN2_3:
			array[0].spriteName = "talisman_02";
			array[1].spriteName = "talisman_02";
			array[2].spriteName = "gimmick_RT_sword";
			break;
		}
		Util.CreateOneShotAnime("Effect", "DROP_TALISMAN_FACE_DAMAGE", Sprite.gameObject, new Vector3(0f, 0f, -1f), Vector3.one, 0f, 0f, array, ResourceManager.LoadImage("PUZZLE_TILE").Image);
		mGame.PlaySe("SE_BOSS_DAMAGE", 2);
	}

	public virtual void CreateScoreEffect(float delay, int score)
	{
		if (score > 0)
		{
			score += (int)((float)score * (mManager.ExpandedScoreRatio + mManager.SkillExpandedScoreRatio));
			GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.GETSCOREEFFECT));
			Util.SetGameObject(gameObject, "Score", mManager.Root);
			GetScoreEffect component = gameObject.GetComponent<GetScoreEffect>();
			component.transform.localPosition = new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_SCORE_Z);
			component.Init(score, Kind, delay);
			mManager.AddScore(score);
		}
	}

	protected void CreateTimeEffect(float delay, float addTime)
	{
		if (addTime > 0f)
		{
			GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.GETTIMEEFFECT));
			Util.SetGameObject(gameObject, "Time", mManager.Root);
			GetTimeEffect component = gameObject.GetComponent<GetTimeEffect>();
			component.transform.localPosition = new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_SCORE_Z - 1f);
			component.Init(addTime, Form, delay);
			mManager.AddTime(addTime);
		}
	}

	protected void CreateMovesEffect(float delay, int addMoves)
	{
		if (addMoves > 0)
		{
			GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.GETTIMEEFFECT));
			Util.SetGameObject(gameObject, "Moves", mManager.Root);
			GetTimeEffect component = gameObject.GetComponent<GetTimeEffect>();
			component.transform.localPosition = new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_SCORE_Z - 1f);
			component.Init(addMoves, Form, delay);
			mManager.AddMoves(addMoves);
		}
	}

	protected void CreatePearlEffect(float delay)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.GETTIMEEFFECT));
		Util.SetGameObject(gameObject, "Pearl", mManager.Root);
		GetTimeEffect component = gameObject.GetComponent<GetTimeEffect>();
		component.transform.localPosition = new Vector3(LocalPosition.x, LocalPosition.y, Def.PUZZLE_TILE_SCORE_Z - 1f);
		component.Init(0f, Form, delay);
		mManager.AddScore(1000);
	}

	public void GrayOut(bool gray)
	{
		if (!(Sprite != null))
		{
			return;
		}
		if (gray)
		{
			if (isIngredient() || isPair() || isSpecialForm() || isGrownUp() || Kind == Def.TILE_KIND.SKILL_BALL || Kind == Def.TILE_KIND.INCREASE || isOrb() || isKey() || isTalisman() || isFootstamp())
			{
				TileData.DROP_ANIME_TYPE defaultAnimeType = TileData.DROP_ANIME_TYPE.NORMAL;
				mData.GetAnimeType(ref defaultAnimeType, Form);
				CHANGE_PART_INFO[] a_ret;
				mData.GetChangePartsInfo(defaultAnimeType, Form, out a_ret);
				Sprite.ChangeAnime("DROP_NORMAL", a_ret, true, 1, OnAnimationFinished);
				Sprite._scl = new Vector3(Def.TILE_SCALE, Def.TILE_SCALE, 1f);
				mAnimeFinished = false;
			}
			Sprite._sprite.Speed = 0f;
			Sprite.Grayout(true);
		}
		else
		{
			Sprite.Grayout(false);
			if (isIngredient() || isPair() || isSpecialForm() || isGrownUp() || Kind == Def.TILE_KIND.SKILL_BALL || Kind == Def.TILE_KIND.INCREASE || isOrb() || isKey() || isTalisman() || isFootstamp())
			{
				changeAnime(TileData.DROP_ANIME_TYPE.NORMAL, 0, true);
			}
			Sprite._sprite.Speed = 1f;
		}
	}

	public bool isGrayOut()
	{
		if (mSprite == null)
		{
			return false;
		}
		return mSprite.IsGrayOut;
	}

	public void SetFallRock(bool flg)
	{
		mFallRock = flg;
	}

	private void SpecialCreate(Def.TILE_FORM form)
	{
		if (form == Def.TILE_FORM.BOMB || form == Def.TILE_FORM.PAINT || form == Def.TILE_FORM.WAVE_H || form == Def.TILE_FORM.WAVE_V || form == Def.TILE_FORM.RAINBOW || form == Def.TILE_FORM.TAP_BOMB)
		{
			mManager.AddSpecialCreateCount(form);
			mCreatedCount = true;
		}
	}

	private void SpecialCrush(Def.TILE_FORM form)
	{
		if ((form == Def.TILE_FORM.BOMB || form == Def.TILE_FORM.PAINT || form == Def.TILE_FORM.WAVE_H || form == Def.TILE_FORM.WAVE_V || form == Def.TILE_FORM.RAINBOW || form == Def.TILE_FORM.TAP_BOMB) && mCreatedCount)
		{
			mManager.AddSpecialCrushCount(form);
		}
	}
}
