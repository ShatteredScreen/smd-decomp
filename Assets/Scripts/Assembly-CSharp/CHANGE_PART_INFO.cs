using UnityEngine;

public struct CHANGE_PART_INFO
{
	public string partName;

	public string spriteName;

	public Vector2 centerOffset;

	public bool enableOffset;
}
