public class BirthdayGetResponse : ParameterObject<BirthdayGetResponse>
{
	public string birthday { get; set; }

	public int code { get; set; }

	public int code_ex { get; set; }

	public long server_time { get; set; }
}
