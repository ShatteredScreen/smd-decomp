using System.Collections.Generic;

public class MissionManager
{
	public enum MISSION_ID
	{
		MISSION_ID_NONE = 0,
		MISSION_ID_PIECE_HEART = 1,
		MISSION_ID_PIECE_RED = 2,
		MISSION_ID_PIECE_BLUE = 3,
		MISSION_ID_PIECE_GREEN = 4,
		MISSION_ID_PIECE_STAR = 5,
		MISSION_ID_PIECE_MOON = 6,
		MISSION_ID_BARRIER = 7,
		MISSION_ID_ACCESSORY = 8,
		MISSION_ID_ENEMY = 9,
		MISSION_ID_CRYSTAL = 10,
		MISSION_ID_LUNA = 11,
		MISSION_ID_INCREASE_ENEMY = 12,
		MISSION_ID_USE_SKILL = 13,
		MISSION_ID_STAR_FIRST = 14,
		MISSION_ID_STAR_R = 15,
		MISSION_ID_STAR_S = 16,
		MISSION_ID_STAR_SS = 17,
		MISSION_ID_STAR_STARS = 18,
		MISSION_ID_CLEAR_FIRST = 19,
		MISSION_ID_CLEAR_R = 20,
		MISSION_ID_CLEAR_S = 21,
		MISSION_ID_CLEAR_SS = 22,
		MISSION_ID_CLEAR_STARS = 23,
		MISSION_ID_SUBROUTE = 24,
		MISSION_ID_LEVEL_MAX = 25,
		MISSION_ID_TRY_STAGE = 26,
		MISSION_ID_GET_PRESENT = 27,
		MISSION_ID_CLEAR_EVENT = 28,
		MISSION_ID_WHOLE_CAKE = 29,
		MISSION_ID_STAR_GF00 = 30,
		MISSION_ID_CLEAR_GF00 = 31,
		MISSION_ID_ADV_AREA = 1001,
		MISSION_ID_ADV_FIGURE_GET = 1002,
		MISSION_ID_ADV_ENEMY = 1003,
		MISSION_ID_ADV_SKILL = 1004,
		MISSION_ID_ADV_FEVER = 1005,
		MISSION_ID_ADV_FIGURE_LV1 = 1006,
		MISSION_ID_ADV_FIGURE_LV2 = 1007,
		MISSION_ID_ADV_FIGURE_LV3 = 1008,
		MISSION_ID_ADV_FIGURE_LV4 = 1009,
		MISSION_ID_ADV_FIGURE_LV5 = 1010,
		MISSION_ID_ADV_FIGURE_LV6 = 1011,
		MISSION_ID_ADV_FIGURE_LV7 = 1012,
		MISSION_ID_ADV_FIGURE_LV8 = 1013,
		MISSION_ID_ADV_FIGURE_SKILL_LV1 = 1014,
		MISSION_ID_ADV_FIGURE_SKILL_LV2 = 1015,
		MISSION_ID_ADV_FIGURE_SKILL_LV3 = 1016
	}

	public const int MISSION_ID_ADV_MIN = 1000;

	private static MissionCountFigure mCountFigure = new MissionCountFigure();

	private static int[] mTargetLevel = new int[8] { 10, 20, 30, 40, 50, 60, 70, 80 };

	private static int[] mTargetSkillLevel = new int[3] { 2, 4, 7 };

	private static MissionCountPuzzle mCountPuzzle = new MissionCountPuzzle();

	private static PlayerClearData mLastClearData = null;

	private static MissionCountPuzzleAdv mCountPuzzleAdv = new MissionCountPuzzleAdv();

	private static GameMain game = null;

	private static Dictionary<int, MissionData> mMissionDict = new Dictionary<int, MissionData>();

	private static Dictionary<int, MissionProgress> mProgressDict = null;

	private static int mReflectPlayerData = 0;

	private static GameMain mGame
	{
		get
		{
			if (game == null)
			{
				game = SingletonMonoBehaviour<GameMain>.Instance;
			}
			return game;
		}
	}

	public static void InitCountFigure()
	{
		mCountFigure.Init();
	}

	public static void CountFigure(AdvCharacter aFigure)
	{
		mCountFigure.mNum++;
		for (int i = 0; i < mTargetLevel.Length && aFigure.level >= mTargetLevel[i]; i++)
		{
			mCountFigure.mLevel[i]++;
		}
		for (int j = 0; j < mTargetSkillLevel.Length && aFigure.skillLevel >= mTargetSkillLevel[j]; j++)
		{
			mCountFigure.mSkill[j]++;
		}
	}

	public static void ReflectCountFigure()
	{
		UpdateMissionCurrentValue(1002, mCountFigure.mNum);
		UpdateMissionCurrentValue(1006, mCountFigure.mLevel[0]);
		UpdateMissionCurrentValue(1007, mCountFigure.mLevel[1]);
		UpdateMissionCurrentValue(1008, mCountFigure.mLevel[2]);
		UpdateMissionCurrentValue(1009, mCountFigure.mLevel[3]);
		UpdateMissionCurrentValue(1010, mCountFigure.mLevel[4]);
		UpdateMissionCurrentValue(1011, mCountFigure.mLevel[5]);
		UpdateMissionCurrentValue(1012, mCountFigure.mLevel[6]);
		UpdateMissionCurrentValue(1013, mCountFigure.mLevel[7]);
		UpdateMissionCurrentValue(1014, mCountFigure.mSkill[0]);
		UpdateMissionCurrentValue(1015, mCountFigure.mSkill[1]);
		UpdateMissionCurrentValue(1016, mCountFigure.mSkill[2]);
	}

	public static void UpdateMission_FigureLevel(int aOldLevel, int aNewLevel)
	{
		if (aOldLevel >= aNewLevel)
		{
			return;
		}
		for (int i = 0; i < mTargetLevel.Length; i++)
		{
			if (aOldLevel < mTargetLevel[i] && aNewLevel >= mTargetLevel[i])
			{
				UpdateMission_FigureLevel(mTargetLevel[i]);
			}
		}
	}

	public static void UpdateMission_FigureLevel(int aLevel)
	{
		switch (aLevel)
		{
		case 10:
			UpdateMission(1006, 1);
			break;
		case 20:
			UpdateMission(1007, 1);
			break;
		case 30:
			UpdateMission(1008, 1);
			break;
		case 40:
			UpdateMission(1009, 1);
			break;
		case 50:
			UpdateMission(1010, 1);
			break;
		case 60:
			UpdateMission(1011, 1);
			break;
		case 70:
			UpdateMission(1012, 1);
			break;
		case 80:
			UpdateMission(1013, 1);
			break;
		}
	}

	public static void UpdateMission_FigureSkillLevel(int aOldSkillLevel, int aNewSkillLevel)
	{
		if (aOldSkillLevel >= aNewSkillLevel)
		{
			return;
		}
		for (int i = 0; i < mTargetSkillLevel.Length; i++)
		{
			if (aOldSkillLevel < mTargetSkillLevel[i] && aNewSkillLevel >= mTargetSkillLevel[i])
			{
				UpdateMission_FigureSkillLevel(mTargetSkillLevel[i]);
			}
		}
	}

	public static void UpdateMission_FigureSkillLevel(int aSkillLevel)
	{
		switch (aSkillLevel)
		{
		case 2:
			UpdateMission(1014, 1);
			break;
		case 4:
			UpdateMission(1015, 1);
			break;
		case 7:
			UpdateMission(1016, 1);
			break;
		case 3:
		case 5:
		case 6:
			break;
		}
	}

	public static void InitCountPuzzle()
	{
		mLastClearData = null;
		mCountPuzzle.Init();
		Def.SERIES series = mGame.mCurrentStage.Series;
		int stageNumber = mGame.mCurrentStage.StageNumber;
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			return;
		}
		PlayerClearData _psd;
		if (mGame.mEventMode)
		{
			short a_course;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(stageNumber, out a_course, out a_main, out a_sub);
			int stageNo = Def.GetStageNo(a_main, a_sub);
			if (mGame.mPlayer.GetStageClearData(series, mGame.mCurrentStage.EventID, a_course, stageNo, out _psd))
			{
				mLastClearData = _psd.Clone();
			}
		}
		else if (mGame.mPlayer.GetStageClearData(series, stageNumber, out _psd))
		{
			mLastClearData = _psd.Clone();
		}
	}

	public static PlayerClearData PlayerCrearData()
	{
		return mLastClearData;
	}

	public static void ResultCountPuzzle(bool isWin, int aSkillCount)
	{
		float num = 1f;
		float num2 = 1f;
		float num3 = 1f;
		if (!isWin)
		{
			num3 = 1f;
		}
		else if (mLastClearData == null)
		{
			num2 = 1f;
		}
		num *= num2 * num3;
		if (mGame.mCurrentStage.LoseType != Def.STAGE_LOSE_CONDITION.TIME)
		{
			UpdateMission(1, (int)((float)mCountPuzzle.mHeart * num));
			UpdateMission(2, (int)((float)mCountPuzzle.mRed * num));
			UpdateMission(3, (int)((float)mCountPuzzle.mBlue * num));
			UpdateMission(4, (int)((float)mCountPuzzle.mGreen * num));
			UpdateMission(5, (int)((float)mCountPuzzle.mStar * num));
			UpdateMission(6, (int)((float)mCountPuzzle.mMoon * num));
			UpdateMission(13, aSkillCount);
		}
		UpdateMission(7, (int)((float)mCountPuzzle.mBarrier * num));
		UpdateMission(8, mCountPuzzle.mAccessory);
		UpdateMission(10, mCountPuzzle.mCrystal);
		UpdateMission(11, mCountPuzzle.mLuna);
		UpdateMission(12, mCountPuzzle.mIncrease);
		UpdateMission(29, mCountPuzzle.mCake);
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.DEFEAT && SingletonMonoBehaviour<PuzzleManager>.Instance.IsBossGimmickDead())
		{
			UpdateMission(9, 1);
		}
		if (mLastClearData == null && !mGame.IsDailyChallengePuzzle && mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
		{
			UpdateMission(26, 1);
		}
		if (!isWin)
		{
			return;
		}
		UpdateMission(27, mCountPuzzle.mPresent);
		Def.SERIES series = mGame.mCurrentStage.Series;
		int stageNumber = mGame.mCurrentStage.StageNumber;
		if (mGame.mEventMode)
		{
			short a_course;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(stageNumber, out a_course, out a_main, out a_sub);
			int stageNo = Def.GetStageNo(a_main, a_sub);
		}
		else
		{
			if (mGame.IsDailyChallengePuzzle || mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
			{
				return;
			}
			PlayerClearData _psd;
			if (mGame.mPlayer.GetStageClearData(series, stageNumber, out _psd))
			{
				int num4 = _psd.GotStars;
				if (mLastClearData != null)
				{
					num4 -= mLastClearData.GotStars;
				}
				switch (series)
				{
				case Def.SERIES.SM_FIRST:
					UpdateMission(14, num4);
					break;
				case Def.SERIES.SM_R:
					UpdateMission(15, num4);
					break;
				case Def.SERIES.SM_S:
					UpdateMission(16, num4);
					break;
				case Def.SERIES.SM_SS:
					UpdateMission(17, num4);
					break;
				case Def.SERIES.SM_STARS:
					UpdateMission(18, num4);
					break;
				case Def.SERIES.SM_GF00:
					UpdateMission(30, num4);
					break;
				}
			}
			switch (series)
			{
			case Def.SERIES.SM_FIRST:
				UpdateMissionCurrentValue(19, stageNumber / 100);
				break;
			case Def.SERIES.SM_R:
				UpdateMissionCurrentValue(20, stageNumber / 100);
				break;
			case Def.SERIES.SM_S:
				UpdateMissionCurrentValue(21, stageNumber / 100);
				break;
			case Def.SERIES.SM_SS:
				UpdateMissionCurrentValue(22, stageNumber / 100);
				break;
			case Def.SERIES.SM_STARS:
				UpdateMissionCurrentValue(23, stageNumber / 100);
				break;
			case Def.SERIES.SM_GF00:
				UpdateMissionCurrentValue(31, stageNumber / 100);
				break;
			}
			if (mLastClearData == null && stageNumber % 100 != 0)
			{
				UpdateMission(24, 1);
			}
		}
	}

	public static void CountPuzzleTile(Def.TILE_KIND aKind)
	{
		switch (aKind)
		{
		case Def.TILE_KIND.COLOR0:
			mCountPuzzle.mRed++;
			break;
		case Def.TILE_KIND.COLOR1:
			mCountPuzzle.mBlue++;
			break;
		case Def.TILE_KIND.COLOR2:
			mCountPuzzle.mGreen++;
			break;
		case Def.TILE_KIND.COLOR3:
			mCountPuzzle.mMoon++;
			break;
		case Def.TILE_KIND.COLOR4:
			mCountPuzzle.mStar++;
			break;
		case Def.TILE_KIND.COLOR5:
			mCountPuzzle.mHeart++;
			break;
		case Def.TILE_KIND.THROUGH_WALL0:
			mCountPuzzle.mBarrier++;
			break;
		case Def.TILE_KIND.GROWN_UP0:
			mCountPuzzle.mCrystal++;
			break;
		case Def.TILE_KIND.INGREDIENT0:
		case Def.TILE_KIND.INGREDIENT1:
		case Def.TILE_KIND.INGREDIENT2:
		case Def.TILE_KIND.INGREDIENT3:
			mCountPuzzle.mAccessory++;
			break;
		case Def.TILE_KIND.FIND0:
			mCountPuzzle.mLuna++;
			break;
		case Def.TILE_KIND.INCREASE:
			mCountPuzzle.mIncrease++;
			break;
		case Def.TILE_KIND.PRESENTBOX4:
		case Def.TILE_KIND.PRESENTBOX5:
		case Def.TILE_KIND.PRESENTBOX6:
			mCountPuzzle.mPresent++;
			break;
		case Def.TILE_KIND.PAIR0_COMPLETE:
			mCountPuzzle.mCake++;
			break;
		}
	}

	public static void InitCountPuzzleAdv()
	{
		mCountPuzzleAdv.Init();
		if (!mGame.IsAdvEventMode())
		{
		}
	}

	public static void ResultCountPuzzleAdv(bool isWin, AdvStageSaveData aClearData)
	{
		UpdateMission(1003, mCountPuzzleAdv.mEnemy);
		UpdateMission(1004, mCountPuzzleAdv.mSkill);
		UpdateMission(1005, mCountPuzzleAdv.mFever);
		if (isWin && !mGame.IsAdvEventMode() && aClearData.mWinCount == 1)
		{
			UpdateMission(1001, 1);
		}
	}

	public static void CountPuzzleAdv_Enemy()
	{
		mCountPuzzleAdv.mEnemy++;
	}

	public static void CountPuzzleAdv_Skill()
	{
		mCountPuzzleAdv.mSkill++;
	}

	public static void CountPuzzleAdv_Fever()
	{
		mCountPuzzleAdv.mFever++;
	}

	public static void ClearMissionProgress(PlayerData aPlayerData)
	{
		aPlayerData.MissionProgressDict.Clear();
		CreateProgressBuffer(aPlayerData.MissionProgressDict);
		aPlayerData.ReflectMissionProgress = 0;
	}

	public static void SaveMissionProgress(PlayerData aPlayerData, BIJBinaryWriter aWriter)
	{
		aWriter.WriteInt(aPlayerData.MissionProgressDict.Count);
		foreach (MissionProgress value in aPlayerData.MissionProgressDict.Values)
		{
			value.SerializeToBinary(aWriter);
		}
	}

	public static void LoadMissionProgress(PlayerData aPlayerData, BIJBinaryReader aReader, int aVersion)
	{
		aPlayerData.MissionProgressDict.Clear();
		aPlayerData.ReflectMissionProgress = 0;
		if (aVersion < 1050)
		{
			CreateProgressBuffer(aPlayerData.MissionProgressDict);
			aPlayerData.ReflectMissionProgress = 1050;
			return;
		}
		if (aVersion < 1230)
		{
			aPlayerData.ReflectMissionProgress = 1230;
		}
		int num = aReader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			MissionProgress missionProgress = new MissionProgress();
			missionProgress.DeserializeFromBinary(aReader);
			AddMissionProgress(aPlayerData.MissionProgressDict, missionProgress);
		}
	}

	public static void SetMissionProgress(PlayerData aPlayerData)
	{
		mProgressDict = aPlayerData.MissionProgressDict;
		CreateProgressBuffer(mProgressDict);
		mReflectPlayerData = aPlayerData.ReflectMissionProgress;
		ReflectCountFigure();
	}

	public static void ReflectPlayerData()
	{
		switch (mReflectPlayerData)
		{
		case 1050:
			ReflectPlayerData_v1050();
			break;
		case 1230:
			ReflectPlayerData_v1230();
			break;
		}
		mReflectPlayerData = 0;
	}

	public static void ReflectPlayerData_v1050()
	{
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		Player mPlayer = mGame.mPlayer;
		PlayerData data = mPlayer.Data;
		PlayerMapData player1stSeriesData = data.Player1stSeriesData;
		foreach (PlayerClearData stageClearDatum in player1stSeriesData.StageClearData)
		{
			num += stageClearDatum.GotStars;
			if (stageClearDatum.StageNo % 100 != 0)
			{
				num3++;
			}
		}
		num2 = player1stSeriesData.StageClearData.Count - num3;
		foreach (int key in data.CompanionXPs.Keys)
		{
			if (mPlayer.GetCompanionLevel(key) >= 5)
			{
				num4++;
			}
		}
		UpdateMissionCurrentValue(14, num);
		UpdateMissionCurrentValue(19, num2);
		UpdateMissionCurrentValue(24, num3);
		UpdateMissionCurrentValue(25, num4);
	}

	public static void ReflectPlayerData_v1230()
	{
		PlayerData data = mGame.mPlayer.Data;
		PlayerMapData playerMapData = data.Player1stSeriesData;
		for (Def.SERIES sERIES = Def.SERIES.SM_FIRST; sERIES <= Def.SERIES.SM_STARS; sERIES++)
		{
			switch (sERIES)
			{
			case Def.SERIES.SM_R:
				playerMapData = data.PlayerRSeriesData;
				break;
			case Def.SERIES.SM_S:
				playerMapData = data.PlayerSSeriesData;
				break;
			case Def.SERIES.SM_SS:
				playerMapData = data.PlayerSSSeriesData;
				break;
			case Def.SERIES.SM_STARS:
				playerMapData = data.PlayerStarSSeriesData;
				break;
			}
			int num = 0;
			foreach (PlayerClearData stageClearDatum in playerMapData.StageClearData)
			{
				int num2 = stageClearDatum.StageNo / 100;
				if (num < num2)
				{
					num = num2;
				}
			}
			switch (sERIES)
			{
			case Def.SERIES.SM_FIRST:
				_SetMissionValue(19, num);
				break;
			case Def.SERIES.SM_R:
				_SetMissionValue(20, num);
				break;
			case Def.SERIES.SM_S:
				_SetMissionValue(21, num);
				break;
			case Def.SERIES.SM_SS:
				_SetMissionValue(22, num);
				break;
			case Def.SERIES.SM_STARS:
				_SetMissionValue(23, num);
				break;
			}
		}
	}

	private static void CreateProgressBuffer(Dictionary<int, MissionProgress> aProgressDict)
	{
		foreach (MissionData value in mMissionDict.Values)
		{
			if (!aProgressDict.ContainsKey(value.mID))
			{
				MissionProgress missionProgress = new MissionProgress();
				missionProgress.mID = value.mID;
				aProgressDict.Add(value.mID, missionProgress);
			}
		}
	}

	private static void AddMissionProgress(Dictionary<int, MissionProgress> aProgressDict, MissionProgress aProgress)
	{
		int mID = aProgress.mID;
		if (aProgressDict.ContainsKey(mID))
		{
			aProgressDict[mID] = aProgress;
		}
		else
		{
			aProgressDict.Add(mID, aProgress);
		}
	}

	public static void DeserializeMissionData(BIJBinaryReader aReader, Dictionary<int, MissionData> aMissionDict)
	{
		int num = aReader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			MissionData missionData = new MissionData();
			missionData.Deserialize(aReader);
			aMissionDict.Add(missionData.mID, missionData);
		}
	}

	public static void LoadMissionData()
	{
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader("Data/TrophyMission.bin"))
		{
			DeserializeMissionData(bIJResourceBinaryReader, mMissionDict);
			bIJResourceBinaryReader.Close();
		}
	}

	public static void UpdateMissionData(ResourceDownloadManager aDLManager)
	{
		if (aDLManager == null)
		{
			return;
		}
		BIJBinaryReader downloadFileReadStream = aDLManager.GetDownloadFileReadStream("TrophyMission_Ex.bin.bytes");
		if (downloadFileReadStream == null)
		{
			return;
		}
		mMissionDict.Clear();
		LoadMissionData();
		Dictionary<int, MissionData> dictionary = new Dictionary<int, MissionData>();
		DeserializeMissionData(downloadFileReadStream, dictionary);
		downloadFileReadStream.Close();
		foreach (MissionData value in dictionary.Values)
		{
			int mID = value.mID;
			if (mMissionDict.ContainsKey(mID))
			{
				mMissionDict[mID] = value;
			}
			else
			{
				mMissionDict.Add(mID, value);
			}
		}
	}

	private static void SendCram(MissionData aMission, int aCurrentStep, int aCurrentValue, int aNewValue)
	{
		int num = -1;
		int i;
		for (i = aCurrentStep; i <= aMission.mOpenStep; i++)
		{
			num = i;
			if (aCurrentValue < aMission.mSteps[i - 1].mTarget)
			{
				break;
			}
		}
		if (num != -1 && i <= aMission.mOpenStep)
		{
			for (i = num; i <= aMission.mOpenStep && aNewValue >= aMission.mSteps[i - 1].mTarget; i++)
			{
				ServerCram.ReachTrophyMission(aMission.mID, i, mGame.mOptions.PurchaseCount);
			}
		}
	}

	public static bool IsMissionOpened(int aID)
	{
		if (aID == 0 || !mMissionDict.ContainsKey(aID))
		{
			return false;
		}
		MissionData missionData = mMissionDict[aID];
		if (missionData.mOpenStep <= 0)
		{
			return false;
		}
		if (missionData.mOpenID > 0)
		{
			MissionData missionData2 = mMissionDict[missionData.mOpenID];
			if (missionData2.mOpenStep <= 0)
			{
				return false;
			}
			MissionProgress missionProgress = mProgressDict[missionData.mOpenID];
			if (missionProgress.mStep < missionData2.mMaxStep)
			{
				return false;
			}
		}
		return true;
	}

	public static void UpdateMission(int aID, int aChange)
	{
		if (!IsMissionOpened(aID))
		{
			return;
		}
		MissionData missionData = mMissionDict[aID];
		MissionProgress missionProgress = mProgressDict[aID];
		int mValue = missionProgress.mValue;
		int mTarget = missionData.mSteps[missionData.mOpenStep - 1].mTarget;
		if (missionProgress.mValue + missionProgress.mChangeValue + aChange <= mTarget)
		{
			if (missionData.mShowGuage == MissionData.SHOWGUAGE.NONE)
			{
				missionProgress.mValue += aChange;
			}
			else
			{
				missionProgress.mChangeValue += aChange;
			}
		}
		else if (missionData.mShowGuage == MissionData.SHOWGUAGE.NONE)
		{
			missionProgress.mValue = mTarget;
		}
		else
		{
			missionProgress.mChangeValue = mTarget - missionProgress.mValue;
		}
		SendCram(missionData, missionProgress.mStep, mValue, missionProgress.mValue + missionProgress.mChangeValue);
	}

	public static void UpdateMissionCurrentValue(int aID, int aValue)
	{
		if (!IsMissionOpened(aID))
		{
			return;
		}
		MissionData missionData = mMissionDict[aID];
		MissionProgress missionProgress = mProgressDict[aID];
		if (aValue > missionProgress.mValue)
		{
			int mValue = missionProgress.mValue;
			int mTarget = missionData.mSteps[missionData.mOpenStep - 1].mTarget;
			int num = aValue - missionProgress.mValue;
			if (aValue <= mTarget)
			{
				missionProgress.mValue = aValue;
			}
			else
			{
				missionProgress.mValue = mTarget;
			}
			missionProgress.mChangeValue = 0;
			SendCram(missionData, missionProgress.mStep, mValue, missionProgress.mValue + missionProgress.mChangeValue);
		}
	}

	public static void _SetMissionValue(int aID, int aValue)
	{
		if (IsMissionOpened(aID))
		{
			MissionData missionData = mMissionDict[aID];
			MissionProgress missionProgress = mProgressDict[aID];
			int mTarget = missionData.mSteps[missionData.mOpenStep - 1].mTarget;
			int num = aValue - missionProgress.mValue;
			if (aValue <= mTarget)
			{
				missionProgress.mValue = aValue;
			}
			else
			{
				missionProgress.mValue = mTarget;
			}
			missionProgress.mChangeValue = 0;
		}
	}

	public static int ClearMission(int aID)
	{
		if (!IsMissionOpened(aID))
		{
			return -1;
		}
		MissionData missionData = mMissionDict[aID];
		MissionProgress missionProgress = mProgressDict[aID];
		if (missionProgress.mStep > missionData.mOpenStep)
		{
			return -1;
		}
		missionProgress.mStep++;
		if (missionProgress.mStep > missionData.mOpenStep)
		{
			return -1;
		}
		return missionProgress.mStep;
	}

	public static List<BatchClearInfo> BatchClearMission(List<int> aIDList)
	{
		List<BatchClearInfo> list = new List<BatchClearInfo>();
		bool flag = false;
		for (int i = 0; i < aIDList.Count; i++)
		{
			int num = aIDList[i];
			MissionData missionData = GetMissionData(num);
			MissionProgress missionProgress = GetMissionProgress(num);
			BatchClearInfo batchClearInfo = new BatchClearInfo();
			batchClearInfo.mID = num;
			batchClearInfo.mCleared = false;
			batchClearInfo.mNextStep = missionProgress.mStep;
			batchClearInfo.mReward = 0;
			if (missionProgress.mStep > missionData.mOpenStep)
			{
				batchClearInfo.mNextStep = -1;
			}
			else
			{
				while (missionProgress.mStep <= missionData.mOpenStep)
				{
					MissionStep missionStep = missionData.mSteps[missionProgress.mStep - 1];
					if (missionProgress.mValue < missionStep.mTarget)
					{
						break;
					}
					mGame.mPlayer.AddTrophy(missionStep.mReward);
					flag = true;
					batchClearInfo.mCleared = true;
					batchClearInfo.mReward += missionStep.mReward;
					batchClearInfo.mNextStep = ClearMission(num);
					int sub_type = num * 1000 + missionProgress.mStep;
					ServerCram.GetTrophy(missionStep.mReward, mGame.mPlayer.GetTrophy(), 1, sub_type, mGame.mOptions.PurchaseCount);
					missionProgress.mStep++;
				}
			}
			list.Add(batchClearInfo);
		}
		if (flag)
		{
			mGame.Save();
		}
		return list;
	}

	public static bool IsOpenStepCleared(int aID)
	{
		if (!IsMissionOpened(aID))
		{
			return false;
		}
		MissionData missionData = mMissionDict[aID];
		MissionProgress missionProgress = mProgressDict[aID];
		if (missionProgress.mStep > missionData.mOpenStep)
		{
			return true;
		}
		return false;
	}

	public static MissionData GetMissionData(int aID)
	{
		if (aID == 0 || !mMissionDict.ContainsKey(aID))
		{
			return null;
		}
		return mMissionDict[aID];
	}

	public static MissionStep GetMissionStep(int aID, int aStep)
	{
		if (aID == 0 || !mMissionDict.ContainsKey(aID))
		{
			return null;
		}
		if (aStep > mMissionDict[aID].mSteps.Count)
		{
			return null;
		}
		return mMissionDict[aID].mSteps[aStep - 1];
	}

	public static MissionProgress GetMissionProgress(int aID)
	{
		if (aID == 0 || !mMissionDict.ContainsKey(aID))
		{
			return null;
		}
		MissionProgress missionProgress = new MissionProgress();
		missionProgress.mID = aID;
		missionProgress.mStep = mProgressDict[aID].mStep;
		missionProgress.mValue = mProgressDict[aID].mValue;
		missionProgress.mChangeValue = mProgressDict[aID].mChangeValue;
		mProgressDict[aID].mValue += mProgressDict[aID].mChangeValue;
		mProgressDict[aID].mChangeValue = 0;
		return missionProgress;
	}

	public static MissionInfo GetMissionInfo(int aID)
	{
		if (aID == 0 || !mMissionDict.ContainsKey(aID))
		{
			return null;
		}
		MissionInfo missionInfo = new MissionInfo();
		missionInfo.mData = GetMissionData(aID);
		missionInfo.mProgress = GetMissionProgress(aID);
		return missionInfo;
	}

	public static List<ShowGuageInfo> GetShowGuageInfo(MissionInfo aMissionInfo)
	{
		List<ShowGuageInfo> list = new List<ShowGuageInfo>();
		MissionData mData = aMissionInfo.mData;
		MissionProgress mProgress = aMissionInfo.mProgress;
		int num = mProgress.mStep;
		int mValue = mProgress.mValue;
		int num2 = mProgress.mChangeValue;
		bool flag = false;
		while (num2 > 0 || flag)
		{
			flag = false;
			MissionStep missionStep = mData.mSteps[num - 1];
			if (mValue >= missionStep.mTarget)
			{
				num++;
				continue;
			}
			ShowGuageInfo showGuageInfo = new ShowGuageInfo();
			showGuageInfo.mStep = num;
			showGuageInfo.mReward = missionStep.mReward;
			if (num == 1)
			{
				showGuageInfo.mGuageSize = missionStep.mTarget;
				showGuageInfo.mCurrentValue = mValue;
			}
			else
			{
				MissionStep missionStep2 = mData.mSteps[num - 2];
				showGuageInfo.mGuageSize = missionStep.mTarget - missionStep2.mTarget;
				showGuageInfo.mCurrentValue = mValue - missionStep2.mTarget;
				if (showGuageInfo.mCurrentValue < 0)
				{
					showGuageInfo.mCurrentValue = 0;
				}
			}
			if (showGuageInfo.mCurrentValue + num2 >= showGuageInfo.mGuageSize)
			{
				showGuageInfo.mChangeValue = showGuageInfo.mGuageSize - showGuageInfo.mCurrentValue;
				if (num < mData.mOpenStep)
				{
					flag = true;
				}
			}
			else
			{
				showGuageInfo.mChangeValue = num2;
			}
			num2 -= showGuageInfo.mChangeValue;
			list.Add(showGuageInfo);
			num++;
		}
		return list;
	}

	public static bool EnableGetReward()
	{
		bool result = false;
		bool flag = mGame.EnableAdvEnter();
		List<int> notClearedArray = GetNotClearedArray();
		for (int i = 0; i < notClearedArray.Count; i++)
		{
			int num = notClearedArray[i];
			if (num < 1000 || flag)
			{
				int mStep = mProgressDict[num].mStep;
				MissionStep missionStep = GetMissionStep(num, mStep);
				if (mProgressDict[num].mValue + mProgressDict[num].mChangeValue >= missionStep.mTarget)
				{
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public static List<int> GetNotClearedArray()
	{
		bool flag = mGame.EnableAdvEnter();
		List<int> list = new List<int>();
		foreach (MissionData value in mMissionDict.Values)
		{
			int mID = value.mID;
			if ((mID < 1000 || flag) && IsMissionOpened(mID) && mProgressDict[mID].mStep <= value.mOpenStep)
			{
				list.Add(mID);
			}
		}
		return list;
	}

	public static List<int> GetClearedArray()
	{
		bool flag = mGame.EnableAdvEnter();
		List<int> list = new List<int>();
		foreach (MissionProgress value in mProgressDict.Values)
		{
			if ((value.mID < 1000 || flag) && value.mStep >= 2)
			{
				list.Add(value.mID);
			}
		}
		return list;
	}

	public static List<int> GetShowGuageArray(MissionData.SHOWGUAGE aShowGuage)
	{
		bool flag = mGame.EnableAdvEnter();
		List<int> list = new List<int>();
		foreach (MissionData value in mMissionDict.Values)
		{
			if (value.mShowGuage == aShowGuage)
			{
				int mID = value.mID;
				if ((mID < 1000 || flag) && mProgressDict[mID].mChangeValue > 0)
				{
					list.Add(mID);
				}
			}
		}
		return list;
	}
}
