using System.Text;

public class SMEventCharacterMapSetting : SMMapPageSettingBase
{
	public int CharacterGetNum_1 = -1;

	public int CharacterGetNum_2 = -1;

	public int CharacterGetNum_3 = -1;

	public int CharacterGetNum_4 = -1;

	public string EventLive2DName_1 = string.Empty;

	public string EventLive2DName_2 = string.Empty;

	public string EventLive2DName_3 = string.Empty;

	public string EventLive2DName_4 = string.Empty;

	public string EventLive2DNameComp = string.Empty;

	public int EventCockpitCharaID_1 = -1;

	public int EventCockpitCharaID_2 = -1;

	public int EventCockpitCharaID_3 = -1;

	public int EventCockpitCharaID_4 = -1;

	public int EventCockpitCharaID_Comp = -1;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteInt(CharacterGetNum_1);
		a_writer.WriteInt(CharacterGetNum_2);
		a_writer.WriteInt(CharacterGetNum_3);
		a_writer.WriteInt(CharacterGetNum_4);
		a_writer.WriteUTF(EventLive2DName_1);
		a_writer.WriteUTF(EventLive2DName_2);
		a_writer.WriteUTF(EventLive2DName_3);
		a_writer.WriteUTF(EventLive2DName_4);
		a_writer.WriteUTF(EventLive2DNameComp);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		CharacterGetNum_1 = a_reader.ReadInt();
		CharacterGetNum_2 = a_reader.ReadInt();
		CharacterGetNum_3 = a_reader.ReadInt();
		CharacterGetNum_4 = a_reader.ReadInt();
		EventLive2DName_1 = a_reader.ReadUTF();
		EventLive2DName_2 = a_reader.ReadUTF();
		EventLive2DName_3 = a_reader.ReadUTF();
		EventLive2DName_4 = a_reader.ReadUTF();
		EventLive2DNameComp = a_reader.ReadUTF();
		if (string.IsNullOrEmpty(EventLive2DName_2) || EventLive2DName_2.CompareTo("NULL") == 0)
		{
			EventLive2DName_2 = EventLive2DName_1;
		}
		if (string.IsNullOrEmpty(EventLive2DName_3) || EventLive2DName_3.CompareTo("NULL") == 0)
		{
			EventLive2DName_3 = EventLive2DName_1;
		}
		if (string.IsNullOrEmpty(EventLive2DName_4) || EventLive2DName_4.CompareTo("NULL") == 0)
		{
			EventLive2DName_4 = EventLive2DName_1;
		}
		if (string.IsNullOrEmpty(EventLive2DNameComp) || EventLive2DNameComp.CompareTo("NULL") == 0)
		{
			EventLive2DNameComp = EventLive2DName_1;
		}
		ConvertToCompanionID();
	}

	private void ConvertToCompanionID()
	{
		try
		{
			if (!string.IsNullOrEmpty(EventLive2DName_1))
			{
				EventCockpitCharaID_1 = int.Parse(EventLive2DName_1);
			}
		}
		catch
		{
			StringBuilder stringBuilder = new StringBuilder("Parse Error 1:");
			stringBuilder = stringBuilder.Append(EventLive2DName_1);
			BIJLog.E(stringBuilder.ToString());
			EventCockpitCharaID_1 = -1;
		}
		try
		{
			if (!string.IsNullOrEmpty(EventLive2DName_2))
			{
				EventCockpitCharaID_2 = int.Parse(EventLive2DName_2);
			}
		}
		catch
		{
			StringBuilder stringBuilder2 = new StringBuilder("Parse Error 2:");
			stringBuilder2 = stringBuilder2.Append(EventLive2DName_2);
			BIJLog.E(stringBuilder2.ToString());
			EventCockpitCharaID_2 = -1;
		}
		try
		{
			if (!string.IsNullOrEmpty(EventLive2DName_3))
			{
				EventCockpitCharaID_3 = int.Parse(EventLive2DName_3);
			}
		}
		catch
		{
			StringBuilder stringBuilder3 = new StringBuilder("Parse Error 3:");
			stringBuilder3 = stringBuilder3.Append(EventLive2DName_3);
			BIJLog.E(stringBuilder3.ToString());
			EventCockpitCharaID_3 = -1;
		}
		try
		{
			if (!string.IsNullOrEmpty(EventLive2DName_4))
			{
				EventCockpitCharaID_4 = int.Parse(EventLive2DName_4);
			}
		}
		catch
		{
			StringBuilder stringBuilder4 = new StringBuilder("Parse Error 4:");
			stringBuilder4 = stringBuilder4.Append(EventLive2DName_4);
			BIJLog.E(stringBuilder4.ToString());
			EventCockpitCharaID_4 = -1;
		}
		try
		{
			if (!string.IsNullOrEmpty(EventLive2DNameComp))
			{
				EventCockpitCharaID_Comp = int.Parse(EventLive2DNameComp);
			}
		}
		catch
		{
			StringBuilder stringBuilder5 = new StringBuilder("Parse Error Comp:");
			stringBuilder5 = stringBuilder5.Append(EventLive2DNameComp);
			BIJLog.E(stringBuilder5.ToString());
			EventCockpitCharaID_Comp = -1;
		}
	}
}
