using System;
using UnityEngine;

public class WebViewObject : MonoBehaviour
{
	private Action<string> callback;

	private Color mBackGroundColor = Color.clear;

	private AndroidJavaObject webView;

	private bool mIsKeyboardVisible;

	public bool IsKeyboardVisible
	{
		get
		{
			return mIsKeyboardVisible;
		}
	}

	public void SetKeyboardVisible(string pIsVisible)
	{
		mIsKeyboardVisible = pIsVisible == "true";
	}

	public void SetBackGroundColor(Color aColor)
	{
		mBackGroundColor = aColor;
	}

	public void Init(Action<string> cb = null)
	{
		callback = cb;
		webView = new AndroidJavaObject("net.gree.unitywebview.WebViewPlugin");
		webView.Call("Init", base.name);
	}

	private void OnDestroy()
	{
		if (webView != null)
		{
			webView.Call("Destroy");
		}
	}

	public void SetCenterPositionWithScale(Vector2 center, Vector2 scale)
	{
		if (webView != null)
		{
			webView.Call("SetFrame", (int)center.x, (int)center.y, (int)scale.x, (int)scale.y);
		}
	}

	public void SetMargins(int left, int top, int right, int bottom)
	{
		if (webView != null)
		{
			webView.Call("SetMargins", left, top, right, bottom);
		}
	}

	public void SetVisibility(bool v)
	{
		if (webView != null)
		{
			webView.Call("SetVisibility", v);
		}
	}

	public void LoadURL(string url)
	{
		if (webView != null)
		{
			webView.Call("LoadURL", url);
		}
	}

	public void Reload()
	{
		if (webView != null)
		{
			webView.Call("Reload");
		}
	}

	public void Stop()
	{
		if (webView != null)
		{
			webView.Call("Stop");
		}
	}

	public void EvaluateJS(string js)
	{
		if (webView != null)
		{
			webView.Call("LoadURL", "javascript:" + js);
		}
	}

	public void CallFromJS(string message)
	{
		if (callback != null)
		{
			callback(message);
		}
	}

	public void SetExternalSpawnMode(int mode)
	{
		if (webView != null)
		{
			webView.Call("SetExternalSpawnMode", mode);
		}
	}

	public void ShowProgressDialog(bool show)
	{
		if (webView != null)
		{
			webView.Call("ShowProgressDialog", show);
		}
	}

	public void ClearCacheAll()
	{
		if (webView != null)
		{
			webView.Call("ClearCacheAll");
		}
	}
}
