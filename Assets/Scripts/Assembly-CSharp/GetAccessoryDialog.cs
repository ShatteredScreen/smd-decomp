using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetAccessoryDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	private enum ITEMTYPE
	{
		DIALOG = 0,
		KEY = 1,
		EKEY = 2
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private ITEMTYPE mItemtype;

	private OnDialogClosed mCallback;

	private AccessoryData mData;

	private string mIconName = "null";

	private string mTitle = string.Empty;

	private string mText = string.Empty;

	private string mLastStage = string.Empty;

	private string mImagename = string.Empty;

	private int KeyCount;

	private bool mIsGotGem;

	private int mCount;

	private bool mOKEnable = true;

	private bool mWallpaperComp;

	private UISsSprite mUISsAnime;

	private List<UISprite> mTrophyProduction = new List<UISprite>();

	private UISprite mTrophySprite;

	private UIButton mSkipbutton;

	private GameObject mTrophyFlameObject;

	private BIJImage atlas;

	private bool mIsRBUnlockByOther;

	private int mRBStage = -1;

	private NumberImageString mNumberString;

	private int mMaxRBKeyNum;

	public Def.SERIES mSeries = Def.SERIES.SM_ALL;

	private float mCounter;

	private Vector2 mLogScreen = Util.LogScreenSize();

	private bool mShrinkFlg;

	private int mDescSize = 28;

	private bool waitFlg;

	private bool mBigFlg;

	private UISprite mTrophyLightSprite1;

	private UISprite mTrophyLightSprite2;

	public AccessoryData Data
	{
		get
		{
			return mData;
		}
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		mLogScreen = Util.LogScreenSize();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mCounter += Time.deltaTime;
		if (mOKEnable && mCounter > 0.8f)
		{
			string text = "LC_button_ok2";
			UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
			Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 3, new Vector3(0f, -97f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
			mOKEnable = false;
		}
		mState.Update();
	}

	public void Init(AccessoryData a_data, int count, bool a_addAccessory = true)
	{
		mData = a_data;
		mCount = count;
		int a_num = mData.GetNum();
		mGame.mPlayer.AddAccessory(mData, a_addAccessory);
		mSeries = mData.Series;
		switch (a_data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		{
			Def.BOOSTER_KIND a_kind;
			mData.GetBooster(out a_kind, out a_num);
			BoosterData boosterData = mGame.mBoosterData[(int)a_kind];
			mIconName = boosterData.iconNameOff;
			mTitle = Localization.Get("GetAccessory_Item_Title");
			string arg3 = Localization.Get(boosterData.name);
			mText = string.Format(Localization.Get("GetAccessory_Item_DescA"), arg3, a_num);
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.GEM:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = string.Format(Localization.Get("GetAccessory_GEM_Desc"), a_num);
			mImagename = "icon_currency_jem";
			mIsGotGem = true;
			mItemtype = ITEMTYPE.DIALOG;
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = string.Format(Localization.Get("GetAccessory_HEART_Desc"), a_num);
			mImagename = "icon_heart";
			mItemtype = ITEMTYPE.DIALOG;
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = string.Format(Localization.Get("GetAccessory_Item_DescA"), Localization.Get("Stamp_Heart_Title"), a_num);
			mImagename = "icon_dailyEvent_stamp01";
			mItemtype = ITEMTYPE.DIALOG;
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = string.Format(Localization.Get("GetAccessory_Item_DescA"), Localization.Get("Stamp_Level_Title"), a_num);
			mImagename = "icon_dailyEvent_stamp02";
			mItemtype = ITEMTYPE.DIALOG;
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("GetAccessory_RBKEY_Desc");
			mImagename = "icon_loadblock_key";
			mItemtype = ITEMTYPE.KEY;
			mRBStage = mData.GetRBKeyStageNo();
			mMaxRBKeyNum = 3;
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_R:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("GetAccessory_RBKEY_Desc");
			mImagename = "icon_loadblock_key_R";
			mItemtype = ITEMTYPE.KEY;
			mRBStage = mData.GetRBKeyStageNo();
			mMaxRBKeyNum = 3;
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_S:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("GetAccessory_RBKEY_Desc");
			mImagename = "icon_loadblock_key_S";
			mItemtype = ITEMTYPE.KEY;
			mRBStage = mData.GetRBKeyStageNo();
			mMaxRBKeyNum = 3;
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_SS:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("GetAccessory_RBKEY_Desc");
			mImagename = "icon_loadblock_key_SS";
			mItemtype = ITEMTYPE.KEY;
			mRBStage = mData.GetRBKeyStageNo();
			mMaxRBKeyNum = 3;
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_STARS:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("GetAccessory_RBKEY_Desc");
			mImagename = "icon_loadblock_key_STARS";
			mItemtype = ITEMTYPE.KEY;
			mRBStage = mData.GetRBKeyStageNo();
			mMaxRBKeyNum = 3;
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_GF00:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("GetAccessory_RBKEY_Desc");
			mImagename = "icon_loadblock_key_GF00";
			mItemtype = ITEMTYPE.KEY;
			mRBStage = mData.GetRBKeyStageNo();
			mMaxRBKeyNum = 3;
			break;
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = string.Format(Localization.Get("Ranking_TrophyNum"));
			mText += " ";
			mText = string.Format(Localization.Get("GetAccessory_Item_DescA"), mText, a_num);
			mImagename = "icon_ranking_trophy";
			mItemtype = ITEMTYPE.DIALOG;
			break;
		case AccessoryData.ACCESSORY_TYPE.ERB_KEY:
			mTitle = Localization.Get("SeasonEvent_KeyGet_Title");
			mText = Localization.Get("SeasonEvent_KeyGet_Desc");
			mImagename = "icon_eventroadblock_key";
			mItemtype = ITEMTYPE.EKEY;
			mRBStage = mData.Index;
			mMaxRBKeyNum = 1;
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("WallPaper_Complete02_Title") + "\n" + Localization.Get("WallPaper_Complete02_Desc");
			mImagename = "ac_icon_wallpaper_complete";
			mItemtype = ITEMTYPE.DIALOG;
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = Localization.Get("GetAccessory_WallPapper_Piece_Desc");
			mImagename = "icon_wallpaper";
			mItemtype = ITEMTYPE.DIALOG;
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
		{
			mTitle = Localization.Get("GetAccessory_Item_Title");
			string arg2 = Localization.Get("ConsumeItemDesc_Growup");
			mText = string.Format(Localization.Get("GetAccessory_Item_DescA"), arg2, a_num);
			mImagename = "icon_level";
			mItemtype = ITEMTYPE.DIALOG;
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
		{
			mTitle = Localization.Get("GetAccessory_Item_Title");
			string arg = Localization.Get("ConsumeItemDesc_OldEventKey");
			mText = string.Format(Localization.Get("GetAccessory_Item_DescA"), arg, a_num);
			mImagename = SMDImageUtil.GetHUDIconKey(1, 20, a_num);
			mItemtype = ITEMTYPE.DIALOG;
			break;
		}
		}
		mGame.PlaySe("VOICE_005", -1);
		mGame.Save();
	}

	public void AdvGetInit(AccessoryData a_data, AdvRewardListData adv_data)
	{
		mData = a_data;
		mTitle = Localization.Get("GetAccessory_Item_Title");
		string arg = Localization.Get(Def.AdvKindImageTbl[adv_data.Kind]);
		mText = string.Format(Localization.Get("GetAccessory_BOOSTER_Desc_03"), arg, adv_data.num);
		mImagename = SMDImageUtil.GetHUDIconKey(adv_data.Category, adv_data.Kind, (int)adv_data.num);
		mShrinkFlg = true;
		mGame.PlaySe("VOICE_005", -1);
		mGame.Save();
	}

	public void AdvGotInit(AccessoryData a_data, AdvRewardData adv_data)
	{
		mData = a_data;
		mTitle = Localization.Get("GotError_Title");
		mText = string.Format(Localization.Get("GotError_Desc"));
		mImagename = SMDImageUtil.GetHUDIconKey(adv_data.category, adv_data.sub_category, adv_data.quantity);
		mGame.Save();
	}

	public void Init(bool WallPaperComp)
	{
		mWallpaperComp = true;
		mTitle = Localization.Get("ConsumeItemTitle_WallPaper_Complete");
		mText = Localization.Get("ConsumeItemDesc_WallPaper_Complete");
		mImagename = "ac_icon_wallpaper_complete";
		mItemtype = ITEMTYPE.DIALOG;
		mGame.PlaySe("VOICE_005", -1);
	}

	public void EventReward(int a_data)
	{
		mTitle = Localization.Get("GetAccessory_Item_Title");
		mText = string.Format(Localization.Get("DemoEvStarRemuneration_Desc01"), a_data);
		mImagename = "icon_currency_jem";
		mIsGotGem = true;
		mItemtype = ITEMTYPE.DIALOG;
		mGame.PlaySe("VOICE_005", -1);
		mDescSize = 26;
	}

	public void Init(string a_name, string a_title, string a_text, int a_last = -1)
	{
		mData = null;
		mIconName = a_name;
		mTitle = a_title;
		mText = a_text;
		mItemtype = ITEMTYPE.DIALOG;
		if (a_last != -1)
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(a_last, out a_main, out a_sub);
			if (a_sub == 0)
			{
				mLastStage = string.Empty + a_main;
			}
			else
			{
				mLastStage = string.Format("{0}_{1}", a_main, a_sub);
			}
		}
		mGame.PlaySe("VOICE_005", -1);
	}

	public void Init(AccessoryData a_data, bool addAccessory = false)
	{
		mData = a_data;
		mCount = 0;
		int num = mData.GetNum();
		mGame.mPlayer.AddAccessory(mData);
		mSeries = mData.Series;
		switch (a_data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		{
			Def.BOOSTER_KIND a_kind = Def.BOOSTER_KIND.NONE;
			int a_num;
			a_data.GetBooster(out a_kind, out a_num);
			BoosterData boosterData = mGame.mBoosterData[(int)a_kind];
			mIconName = boosterData.iconNameOff;
			mTitle = Localization.Get("GetAccessory_Item_Title");
			string arg = Localization.Get(boosterData.name);
			mText = string.Format(Localization.Get("GetAccessory_BOOSTER_Desc"), arg, a_num);
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.GEM:
			mTitle = Localization.Get("GetAccessory_Item_Title");
			mText = string.Format(Localization.Get("GetAccessory_GEM_Desc"), mData.GetGotIDByNum());
			mImagename = "icon_currency_jem";
			mIsGotGem = true;
			mItemtype = ITEMTYPE.DIALOG;
			break;
		}
	}

	public void InitRBKeyUnlockByOther(int a_last, RoadBlockDialog.SELECT_ITEM i, Def.SERIES a_series)
	{
		mData = null;
		mTitle = Localization.Get("Unlock_RB_Key_Get_Title");
		switch (i)
		{
		case RoadBlockDialog.SELECT_ITEM.UNLOCK_SD:
			mText = Localization.Get("Unlock_RB_Key_Get_Text00");
			break;
		case RoadBlockDialog.SELECT_ITEM.UNLOCK_TICKET:
			mText = Localization.Get("Unlock_RB_Key_Get_Text02");
			break;
		default:
			mText = Localization.Get("Unlock_RB_Key_Get_Text01");
			break;
		}
		mMaxRBKeyNum = 3;
		mSeries = a_series;
		mImagename = mGame.GetRBKeyImageName(mSeries);
		mItemtype = ITEMTYPE.KEY;
		mIsRBUnlockByOther = true;
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_last, out a_main, out a_sub);
		if (a_sub == 0)
		{
			mLastStage = string.Empty + a_main;
		}
		else
		{
			mLastStage = string.Format("{0}_{1}", a_main, a_sub);
		}
		mRBStage = a_main;
		mGame.PlaySe("VOICE_007", -1);
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		if (mItemtype == ITEMTYPE.KEY)
		{
			InitDialogFrame(DIALOG_SIZE.MEDIUM);
			InitDialogTitle(mTitle);
			UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, atlas);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 67f, 0f), Vector3.one, false);
			uISprite.SetDimensions(510, 104);
			uISprite.type = UIBasicSprite.Type.Sliced;
			float num = -141f;
			int num2 = mMaxRBKeyNum;
			int stageNo = Def.GetStageNo(mRBStage, 0);
			List<int> roadBlockAccessoryIndex = mGame.GetRoadBlockAccessoryIndex(stageNo, mSeries);
			for (int i = 0; i < roadBlockAccessoryIndex.Count; i++)
			{
				if (mGame.mPlayer.IsAccessoryUnlock(roadBlockAccessoryIndex[i]))
				{
					KeyCount++;
				}
			}
			if (KeyCount > mMaxRBKeyNum)
			{
				KeyCount = mMaxRBKeyNum;
			}
			if (mIsRBUnlockByOther)
			{
				mCount = mMaxRBKeyNum - (KeyCount + 1);
			}
			else
			{
				KeyCount -= mCount + 1;
			}
			while (KeyCount > 0)
			{
				UISprite sprite = Util.CreateSprite("backkey", uISprite.gameObject, atlas);
				Util.SetSpriteInfo(sprite, "itemGet_bg", mBaseDepth + 3, new Vector3(num, -1f, 0f), Vector3.one, false);
				sprite = Util.CreateSprite("oldimage", uISprite.gameObject, atlas);
				Util.SetSpriteInfo(sprite, mImagename, mBaseDepth + 4, new Vector3(num, -1f, 0f), Vector3.one, false);
				num2--;
				num += 141f;
				KeyCount--;
			}
			while (mCount > -1)
			{
				UISprite sprite = Util.CreateSprite("newimage", uISprite.gameObject, atlas);
				Util.SetSpriteInfo(sprite, mImagename, mBaseDepth + 5, new Vector3(num, -1f, 0f), Vector3.one, false);
				num2--;
				num += 141f;
				mUISsAnime = Util.CreateGameObject("KeyHalo", sprite.gameObject).AddComponent<UISsSprite>();
				mUISsAnime.Animation = ResourceManager.LoadSsAnimation("MAP_KEY_HALO").SsAnime;
				mUISsAnime.depth = mBaseDepth + 4;
				mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
				mUISsAnime.Play();
				mCount--;
			}
			while (num2 > 0)
			{
				UISprite sprite = Util.CreateSprite("backkey", uISprite.gameObject, atlas);
				Util.SetSpriteInfo(sprite, "itemGet_bg", mBaseDepth + 3, new Vector3(num, -1f, 0f), Vector3.one, false);
				sprite = Util.CreateSprite("Remainingimage", uISprite.gameObject, atlas);
				Util.SetSpriteInfo(sprite, mImagename, mBaseDepth + 4, new Vector3(num, -1f, 0f), Vector3.one, false);
				sprite.color = Color.gray;
				num += 141f;
				num2--;
			}
			mGame.PlaySe("SE_MAP_GET_RBKEY", -1);
			UILabel label = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(label, mText, mBaseDepth + 2, new Vector3(0f, -18f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(label, Def.DEFAULT_MESSAGE_COLOR);
			return;
		}
		if (mItemtype == ITEMTYPE.EKEY)
		{
			InitDialogFrame(DIALOG_SIZE.MEDIUM);
			InitDialogTitle(mTitle);
			UISprite uISprite2 = Util.CreateSprite("BoosterBoard", base.gameObject, atlas);
			Util.SetSpriteInfo(uISprite2, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 67f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(510, 104);
			uISprite2.type = UIBasicSprite.Type.Sliced;
			float num3 = 0f;
			int num4 = mMaxRBKeyNum;
			KeyCount = 0;
			for (mCount = num4; mCount > 0; mCount--)
			{
				UISprite uISprite3 = Util.CreateSprite("newimage", uISprite2.gameObject, atlas);
				Util.SetSpriteInfo(uISprite3, mImagename, mBaseDepth + 5, new Vector3(num3, -1f, 0f), Vector3.one, false);
				num4--;
				num3 += 141f;
				mUISsAnime = Util.CreateGameObject("KeyHalo", uISprite3.gameObject).AddComponent<UISsSprite>();
				mUISsAnime.Animation = ResourceManager.LoadSsAnimation("MAP_KEY_HALO").SsAnime;
				mUISsAnime.depth = mBaseDepth + 4;
				mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
				mUISsAnime.Play();
			}
			mGame.PlaySe("SE_MAP_GET_RBKEY", -1);
			UILabel label2 = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(label2, mText, mBaseDepth + 2, new Vector3(0f, -18f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(label2, Def.DEFAULT_MESSAGE_COLOR);
			return;
		}
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(mTitle);
		UISprite uISprite4 = Util.CreateSprite("Frame", base.gameObject, atlas);
		Util.SetSpriteInfo(uISprite4, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 68f, 0f), Vector3.one, false);
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(510, 104);
		UISprite uISprite5 = null;
		string text = "null";
		text = ((mIconName.CompareTo("null") == 0) ? mImagename : mIconName);
		if (mData != null && mData.AccessoryType == AccessoryData.ACCESSORY_TYPE.TROPHY)
		{
			mTrophySprite = Util.CreateSprite("TrophyMainIconImage", uISprite4.gameObject, atlas);
			Util.SetSpriteInfo(mTrophySprite, text, mBaseDepth + 2, new Vector3(-93f, 0f, 0f), Vector3.one, false);
			mTrophyFlameObject = uISprite4.gameObject;
			int gotIDByNum = mData.GetGotIDByNum();
			for (int j = 0; j < gotIDByNum; j++)
			{
				uISprite5 = Util.CreateSprite("IconImage", uISprite4.gameObject, atlas);
				Util.SetSpriteInfo(uISprite5, "icon_ranking_trophy", mBaseDepth + 100 - j, new Vector3(-93f, 0f, 0f), Vector3.one, false);
				NGUITools.SetActive(uISprite5.gameObject, false);
				mTrophyProduction.Add(uISprite5);
			}
			UILabel uILabel = Util.CreateLabel("Num", uISprite4.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Xtext"), mBaseDepth + 3, new Vector3(0f, -2f, 0f), 50, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = new Color(41f / 85f, 37f / 85f, 11f / 85f, 1f);
			mNumberString = Util.CreateGameObject("Number", uISprite4.gameObject).AddComponent<NumberImageString>();
			mNumberString.Init(3, gotIDByNum, mBaseDepth + 5, 1.3f, UIWidget.Pivot.Center, false, atlas);
			mNumberString.transform.localPosition = new Vector3(93f, 0f, 0f);
			StartCoroutine(TrophyMove());
		}
		else
		{
			uISprite5 = Util.CreateSprite("IconImage", uISprite4.gameObject, atlas);
			Util.SetSpriteInfo(uISprite5, text, mBaseDepth + 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
		}
		UILabel uILabel2 = Util.CreateLabel("Text", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel2, mText, mBaseDepth + 2, new Vector3(0f, -23f, 0f), mDescSize, 0, 0, UIWidget.Pivot.Center);
		uILabel2.spacingY = 5;
		Util.SetLabelColor(uILabel2, Def.DEFAULT_MESSAGE_COLOR);
		if (mShrinkFlg)
		{
			uILabel2.maxLineCount = 1;
			uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel2.SetDimensions(500, 80);
		}
		mGame.PlaySe("SE_ACCESSORY_UNLOCK", -1);
	}

	private IEnumerator TrophyMove()
	{
		yield return new WaitForSeconds(1.3f);
		if (!waitFlg)
		{
			float angle = 0f;
			while (angle < 90f)
			{
				yield return new WaitForSeconds(0.001f);
			}
		}
	}

	private IEnumerator TrophyMoveStart()
	{
		for (int i = 0; i < mTrophyProduction.Count; i++)
		{
			StartCoroutine(TrophyMoving(i));
			yield return new WaitForSeconds(0.15f);
			StartCoroutine(TrophyBigSmall());
		}
		if (mTrophyLightSprite1 != null)
		{
			NGUITools.SetActive(mTrophyLightSprite1.gameObject, false);
		}
	}

	private IEnumerator TrophyBigSmall()
	{
		if (mTrophyLightSprite1 == null)
		{
			mTrophyLightSprite1 = Util.CreateSprite("mainTrophyLight1", mTrophySprite.gameObject, atlas);
			Util.SetSpriteInfo(mTrophyLightSprite1, "efc_shine_ring00", mBaseDepth + 102, new Vector3(0f, 0f, 0f), Vector3.one, false);
		}
		if (!mBigFlg)
		{
			mBigFlg = true;
			for (float i = 0f; i < 45f; i += 6f)
			{
				mTrophyLightSprite1.SetDimensions(100 + (int)i * 3, 100 + (int)i * 3);
				mTrophySprite.transform.localScale = new Vector3(1f + i * 0.01f, 1f + i * 0.01f);
				yield return 0;
			}
			mTrophySprite.transform.localScale = new Vector3(1f, 1f);
			mTrophyLightSprite1.SetDimensions(100, 100);
			mBigFlg = false;
		}
	}

	private IEnumerator TrophyMoving(int a)
	{
		UISprite lightSprite1 = null;
		UISprite lightSprite4 = null;
		UISprite lightSprite6 = null;
		UISprite lightSprite8 = null;
		int match = 20;
		lightSprite1 = Util.CreateSprite("lightSprite1", mTrophyFlameObject, atlas);
		Util.SetSpriteInfo(lightSprite1, "icon_ranking_trophy2", mBaseDepth + 101, new Vector3(-match, -match, 0f), Vector3.one, false);
		lightSprite4 = Util.CreateSprite("lightSprite2", mTrophyFlameObject, atlas);
		Util.SetSpriteInfo(lightSprite4, "icon_ranking_trophy3", mBaseDepth + 101, new Vector3(-match, match, 0f), Vector3.one, false);
		lightSprite6 = Util.CreateSprite("lightSprite3", mTrophyFlameObject, atlas);
		Util.SetSpriteInfo(lightSprite6, "icon_ranking_trophy2", mBaseDepth + 101, new Vector3(match, -match, 0f), Vector3.one, false);
		lightSprite8 = Util.CreateSprite("lightSprite4", mTrophyFlameObject, atlas);
		Util.SetSpriteInfo(lightSprite8, "icon_ranking_trophy3", mBaseDepth + 101, new Vector3(match, match, 0f), Vector3.one, false);
		lightSprite8.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
		NGUITools.SetActive(lightSprite1.gameObject, false);
		NGUITools.SetActive(lightSprite4.gameObject, false);
		NGUITools.SetActive(lightSprite6.gameObject, false);
		NGUITools.SetActive(lightSprite8.gameObject, false);
		NGUITools.SetActive(mTrophyProduction[a].gameObject, true);
		mNumberString.SetNum(mTrophyProduction.Count - 1 - a);
		UISprite rightEffect = null;
		float targetX = 154f;
		float targetY = mLogScreen.y / 2f - 106f;
		float motoX = mTrophyProduction[a].transform.localPosition.x;
		float count = 1000f;
		if (a % 3 != 0)
		{
			count += 100f;
		}
		BezierCurve bezierCurve1 = new BezierCurve(new Vector2(-93f, 0f), new Vector2(targetX, targetY), new Vector2(targetX - 200f, targetY - 100f), new Vector2(targetX - 150f, targetY - 50f));
		BezierCurve bezierCurve2 = new BezierCurve(new Vector2(-93f, 0f), new Vector2(targetX, targetY), new Vector2(0f, 0f), new Vector2(0f, 0f));
		BezierCurve bezierCurve3 = new BezierCurve(new Vector2(-93f, 0f), new Vector2(targetX, targetY), new Vector2(targetX, targetY - 300f), new Vector2(targetX + 100f, targetY - 100f));
		float x = 0f;
		float y = 0f;
		float scale = 0.36f / count;
		float scaleCount = 1f;
		float lastScaleCount = 1f;
		float speed = 7f;
		float speedUp = 1.15f;
		mGame.PlaySe("SE_DAILY_TROPHY_MOVE", -1);
		List<float> trollLightX = new List<float>();
		List<float> trollLightY = new List<float>();
		float deltaCount = 0f;
		bool lightFlg = false;
		if (rightEffect == null)
		{
			while (true)
			{
				deltaCount += Time.deltaTime;
				Vector2 pos = new Vector2(0f, 0f);
				if (deltaCount > 1f)
				{
					deltaCount = 1f;
				}
				switch (a % 3)
				{
				case 0:
					pos = bezierCurve1.GetPosition(deltaCount);
					mTrophyProduction[a].transform.localPosition = new Vector3(pos.x, pos.y, 0f);
					break;
				case 1:
					pos.x = targetX * deltaCount;
					pos.y = targetY * deltaCount;
					mTrophyProduction[a].transform.localPosition = new Vector3(pos.x, pos.y, 0f);
					break;
				case 2:
					pos = bezierCurve3.GetPosition(deltaCount);
					mTrophyProduction[a].transform.localPosition = new Vector3(pos.x, pos.y, 0f);
					break;
				}
				trollLightX.Add(pos.x);
				trollLightY.Add(pos.y);
				lightFlg = !lightFlg;
				if (lightFlg)
				{
					NGUITools.SetActive(lightSprite1.gameObject, true);
					NGUITools.SetActive(lightSprite4.gameObject, false);
					NGUITools.SetActive(lightSprite6.gameObject, true);
					NGUITools.SetActive(lightSprite8.gameObject, false);
				}
				else
				{
					NGUITools.SetActive(lightSprite1.gameObject, false);
					NGUITools.SetActive(lightSprite4.gameObject, true);
					NGUITools.SetActive(lightSprite6.gameObject, false);
					NGUITools.SetActive(lightSprite8.gameObject, true);
				}
				if (deltaCount > 0.2f)
				{
					lightSprite1.transform.localPosition = new Vector3(trollLightX[0] - 20f, trollLightY[0] - 20f);
					lightSprite4.transform.localPosition = new Vector3(trollLightX[0] - 20f, trollLightY[0] + 20f);
					lightSprite6.transform.localPosition = new Vector3(trollLightX[0] + 20f, trollLightY[0] - 20f);
					lightSprite8.transform.localPosition = new Vector3(trollLightX[0] + 20f, trollLightY[0] + 20f);
					trollLightX.RemoveAt(0);
					trollLightY.RemoveAt(0);
				}
				else
				{
					NGUITools.SetActive(lightSprite1.gameObject, false);
					NGUITools.SetActive(lightSprite4.gameObject, false);
					NGUITools.SetActive(lightSprite6.gameObject, false);
					NGUITools.SetActive(lightSprite8.gameObject, false);
				}
				float lightSprite1Scale = 1f - deltaCount * 0.2f;
				lightSprite1.transform.localScale = new Vector3(lightSprite1Scale, lightSprite1Scale);
				float lightSprite2Scale = 0.8f - deltaCount * 0.4f;
				lightSprite4.transform.localScale = new Vector3(lightSprite2Scale, lightSprite2Scale);
				float lightSprite3Scale = 0.6f - deltaCount * 0.4f;
				lightSprite6.transform.localScale = new Vector3(lightSprite3Scale, lightSprite3Scale);
				float lightSprite4Scale = 0.4f - deltaCount * 0.3f;
				lightSprite8.transform.localScale = new Vector3(lightSprite4Scale, lightSprite4Scale);
				if (deltaCount >= 1f)
				{
					break;
				}
				yield return 0;
			}
			StartCoroutine(Effect(a));
		}
		float lightX = 20f;
		float lightY = 20f;
		while (trollLightX.Count != 0)
		{
			lightSprite1.transform.localPosition = new Vector3(trollLightX[0] - lightX, trollLightY[0] - lightY);
			lightSprite4.transform.localPosition = new Vector3(trollLightX[0] - lightX, trollLightY[0] + lightY);
			lightSprite6.transform.localPosition = new Vector3(trollLightX[0] + lightX, trollLightY[0] - lightY);
			lightSprite8.transform.localPosition = new Vector3(trollLightX[0] + lightX, trollLightY[0] + lightY);
			trollLightX.RemoveAt(0);
			trollLightY.RemoveAt(0);
			lightY -= 1f;
			lightX -= 1f;
			yield return 0;
		}
		yield return 0;
		NGUITools.SetActive(lightSprite1.gameObject, false);
		NGUITools.SetActive(lightSprite4.gameObject, false);
		NGUITools.SetActive(lightSprite6.gameObject, false);
		NGUITools.SetActive(lightSprite8.gameObject, false);
		if (a != 0)
		{
			yield break;
		}
		while (true)
		{
			bool moveFlg = true;
			for (int moving = 0; moving < mTrophyProduction.Count; moving++)
			{
				if (mTrophyProduction[moving] != null)
				{
					moveFlg = false;
				}
			}
			if (moveFlg)
			{
				break;
			}
			yield return 0;
		}
		yield return new WaitForSeconds(0.2f);
		if (mState.GetStatus() == STATE.MAIN)
		{
			Close();
		}
	}

	private IEnumerator Effect(int a)
	{
		Util.SetSpriteImageName(mTrophyProduction[a], "box_open", new Vector3(0.93f, 0.93f, 1f), false);
		mGame.mTrophyProductionFlg = GameMain.TROPHY_PRODUCTION.OPEN0;
		float angle = 0f;
		while (true)
		{
			if (angle < 90f)
			{
				angle += Time.deltaTime * 180f;
				float ratio2 = Mathf.Sin(angle * ((float)Math.PI / 180f));
				mTrophyProduction[a].transform.localScale = new Vector3(0.01f + ratio2 * 2f, 0.1f + ratio2 * 2f, 1f);
				mTrophyProduction[a].transform.localRotation = Quaternion.Euler(0f, 0f, angle * 1f);
				yield return 0;
				continue;
			}
			if (!(angle < 150f))
			{
				break;
			}
			angle += Time.deltaTime * 180f;
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mTrophyProduction[a].transform.localScale = new Vector3(0.01f + ratio * 2f, 0.1f + ratio * 2f, 1f);
			mTrophyProduction[a].transform.localRotation = Quaternion.Euler(0f, 0f, angle * 1f);
			yield return 0;
		}
		UnityEngine.Object.Destroy(mTrophyProduction[a].gameObject);
		mTrophyProduction[a] = null;
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			if (mData != null && mData.AccessoryType == AccessoryData.ACCESSORY_TYPE.TROPHY)
			{
				StartCoroutine(TrophyMoveStart());
				mSkipbutton = Util.CreateJellyImageButton("NullSkip", base.gameObject, atlas);
				Util.SetImageButtonInfo(mSkipbutton, "null", "null", "null", mBaseDepth + 101, new Vector3(0f, 0f, 0f), Vector3.one);
				Util.AddImageButtonMessage(mSkipbutton, this, "ClosePushed", UIButtonMessage.Trigger.OnClick);
				Vector2 vector = Util.LogScreenSize();
				BoxCollider component = mSkipbutton.GetComponent<BoxCollider>();
				component.size = new Vector3(vector.x, vector.y, 1f);
			}
			else
			{
				Close();
			}
		}
	}

	public void ClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			Close();
		}
	}

	public override void Close()
	{
		mState.Reset(STATE.WAIT, true);
		for (int i = 0; i < mTrophyProduction.Count; i++)
		{
			if (mTrophyProduction[i] != null)
			{
				NGUITools.SetActive(mTrophyProduction[i].gameObject, false);
			}
		}
		base.Close();
	}

	public override void OnOpenFinished()
	{
		if (mWallpaperComp)
		{
			mGame.PlaySe("SE_DIALY_COMPLETE_STAMP", 2);
			mGame.PlaySe("VOICE_029", -1);
			mUISsAnime = Util.CreateGameObject("GrowHalo", base.gameObject).AddComponent<UISsSprite>();
			mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_STAMP_COMP").SsAnime;
			mUISsAnime.depth = mBaseDepth + 8;
			mUISsAnime.transform.localPosition = new Vector3(0f, 8f, 0f);
			mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
			mUISsAnime.PlayCount = 1;
			mUISsAnime.Play();
		}
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		if (mSkipbutton != null)
		{
			ClosePushed();
		}
		else if (!mOKEnable)
		{
			OnClosePushed();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
