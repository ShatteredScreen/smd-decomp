public class GetWallPaper : ParameterObject<GetWallPaper>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("width")]
	public int Width { get; set; }

	[MiniJSONAlias("height")]
	public int Height { get; set; }

	[MiniJSONAlias("model")]
	public string Model { get; set; }
}
