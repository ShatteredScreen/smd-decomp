using UnityEngine;

public class SelectOptionDialog : DialogBase
{
	public enum SELECT_OPTION
	{
		BGM = 0,
		SE = 1,
		COMBO = 2,
		NOTIFICATION = 3,
		HELP = 4,
		MAP = 5,
		EULA = 6,
		PRIVACY = 7,
		PRIVACY_OPTION = 8
	}

	public enum OPTION_DIALOG_STYLE
	{
		OPTION_ICON = 0
	}

	public delegate void OnDialogClosed(SELECT_OPTION i);

	private SELECT_OPTION mSelectItem;

	private OPTION_DIALOG_STYLE mOptionDialog;

	private DIALOG_SIZE mDialogSize;

	private string mTitle;

	private string mDescription;

	private float mCloseTimer = 1f;

	private string imageName = string.Empty;

	private bool mSe_state;

	private bool mNotification_state;

	private bool mOptionChanged;

	private OnDialogClosed mCallback;

	private WebViewDialog mHelpDialog;

	private EULADialog mEULADialog;

	public bool mBgm_state { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		InitDialogFrame(mDialogSize);
		InitDialogTitle(mTitle);
		float num = -110f;
		float num2 = -280f;
		float x = -166f;
		float x2 = 166f;
		float y = 69f;
		float y2 = -72f;
		float num3 = -44f;
		float num4 = -44f;
		switch (mDialogSize)
		{
		case DIALOG_SIZE.SMALL:
			num = -10f;
			num2 = -130f;
			break;
		case DIALOG_SIZE.MEDIUM:
			num = 60f;
			num2 = -80f;
			break;
		case DIALOG_SIZE.LARGE:
			num = -180f;
			num2 = -490f;
			break;
		case DIALOG_SIZE.XLARGE:
			num = -310f;
			num2 = -690f;
			break;
		}
		if (mOptionDialog == OPTION_DIALOG_STYLE.OPTION_ICON)
		{
			if (mGame.mOptions.BGMEnable)
			{
				mBgm_state = false;
				imageName = "button_music_ON";
			}
			else
			{
				mBgm_state = true;
				imageName = "button_music_OFF";
			}
			UIButton uIButton = Util.CreateJellyImageButton("ButtonBgm", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, imageName, imageName, imageName, mBaseDepth + 4, new Vector3(x, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnBgmPushed", UIButtonMessage.Trigger.OnClick);
			UIButton uIButton2 = uIButton;
			if (mGame.mOptions.SEEnable)
			{
				mSe_state = false;
				imageName = "button_sound_ON";
			}
			else
			{
				mSe_state = true;
				imageName = "button_sound_OFF";
			}
			uIButton = Util.CreateJellyImageButton("ButtonSe", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, imageName, imageName, imageName, mBaseDepth + 4, new Vector3(0f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnSePushed", UIButtonMessage.Trigger.OnClick);
			UIButton uIButton3 = uIButton;
			if (mGame.mOptions.ComboCutIn)
			{
				imageName = "button_combo_ON";
			}
			else
			{
				imageName = "button_combo_OFF";
			}
			uIButton = Util.CreateJellyImageButton("ButtonCombo", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, imageName, imageName, imageName, mBaseDepth + 4, new Vector3(x2, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnComboPushed", UIButtonMessage.Trigger.OnClick);
			UIButton uIButton4 = uIButton;
			if (mGame.mOptions.UseNotification)
			{
				mNotification_state = false;
				imageName = "button_notice_ON";
			}
			else
			{
				mNotification_state = true;
				imageName = "button_notice_OFF";
			}
			uIButton = Util.CreateJellyImageButton("ButtonNotification", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, imageName, imageName, imageName, mBaseDepth + 4, new Vector3(x, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnNotificationPushed", UIButtonMessage.Trigger.OnClick);
			UIButton uIButton5 = uIButton;
			uIButton = Util.CreateJellyImageButton("ButtonHelp", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button_help", "button_help", "button_help", mBaseDepth + 4, new Vector3(0f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnHelpPushed", UIButtonMessage.Trigger.OnClick);
			UIButton uIButton6 = uIButton;
			uIButton = Util.CreateJellyImageButton("ButtonTerms", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button_agreement", "button_agreement", "button_agreement", mBaseDepth + 4, new Vector3(x2, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnTermsPushed", UIButtonMessage.Trigger.OnClick);
			UIButton uIButton7 = uIButton;
			float num5 = 200f;
			float num6 = 0f - num5;
			uIButton2.transform.localPosition = new Vector3(num6, y, 0f);
			uIButton3.transform.localPosition = new Vector3(num6 - (num6 - num5) / 3f, y, 0f);
			uIButton4.transform.localPosition = new Vector3(num5 - (num6 - num5) / -3f, y, 0f);
			uIButton5.transform.localPosition = new Vector3(num5, y, 0f);
			uIButton6.transform.localPosition = new Vector3(num6, y2, 0f);
			uIButton7.transform.localPosition = new Vector3(num6 - (num6 - num5) / 3f, y2, 0f);
			uIButton = Util.CreateJellyImageButton("ButtonPrivacy", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button_privacypolicy", "button_privacypolicy", "button_privacypolicy", mBaseDepth + 4, new Vector3(num5 - (num6 - num5) / -3f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnPrivacyPushed", UIButtonMessage.Trigger.OnClick);
			uIButton = Util.CreateJellyImageButton("ButtonPrivacyOption", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button_privacy_options", "button_privacy_options", "button_privacy_options", mBaseDepth + 4, new Vector3(num5, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnPrivacyOptionPushed", UIButtonMessage.Trigger.OnClick);
			if (string.IsNullOrEmpty(mGame.GameProfile.mGDPRSetting.mOptOutURL))
			{
				uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
			CreateCancelButton("OnCancelPushed");
		}
	}

	public void Init(string title, string desc, OPTION_DIALOG_STYLE style, bool openSeEnable = true)
	{
		Init(title, desc, style, DIALOG_SIZE.MEDIUM);
		SetOpenSeEnable(openSeEnable);
	}

	public void Init(string title, string desc, OPTION_DIALOG_STYLE style, DIALOG_SIZE size)
	{
		mTitle = title;
		mDescription = desc;
		mOptionDialog = style;
		mDialogSize = size;
	}

	public void OnBgmPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.ToggleBGM();
			mOptionChanged = true;
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.BGM;
			string empty = string.Empty;
			if (mGame.mOptions.BGMEnable)
			{
				mBgm_state = false;
				empty = "button_music_ON";
			}
			else
			{
				mBgm_state = true;
				empty = "button_music_OFF";
			}
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	public void OnSePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.ToggleSE();
			mOptionChanged = true;
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.SE;
			string empty = string.Empty;
			if (mGame.mOptions.SEEnable)
			{
				mSe_state = false;
				empty = "button_sound_ON";
			}
			else
			{
				mSe_state = true;
				empty = "button_sound_OFF";
			}
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	public void OnComboPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.ToggleComboCutIn();
			mOptionChanged = true;
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.COMBO;
			string empty = string.Empty;
			empty = ((!mGame.mOptions.ComboCutIn) ? "button_combo_OFF" : "button_combo_ON");
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	public void OnNotificationPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.ToggleNOTIFICATION();
			mOptionChanged = true;
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.NOTIFICATION;
			string empty = string.Empty;
			if (mGame.mOptions.UseNotification)
			{
				mNotification_state = false;
				empty = "button_notice_ON";
			}
			else
			{
				mNotification_state = true;
				empty = "button_notice_OFF";
			}
			Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), empty, empty, empty);
		}
	}

	public void OnHelpPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.HELP;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnTermsPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.EULA;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnPrivacyPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.PRIVACY;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnPrivacyOptionPushed(GameObject go)
	{
		if (go.GetComponent<JellyImageButton>().IsEnable() && !GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_OPTION.PRIVACY_OPTION;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mSelectItem = SELECT_OPTION.MAP;
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
		mOptionChanged = false;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
