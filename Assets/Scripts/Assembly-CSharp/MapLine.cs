using System.Collections.Generic;
using UnityEngine;

public class MapLine : MapEntity
{
	public STATE mStateStatus;

	protected new float mStateTime;

	protected int mMapIndex;

	protected bool mFirstInitialized;

	protected bool mBookedCallback;

	protected Def.SERIES mSeries;

	public bool mIsUnlockAnimeFinished;

	public bool mIsLockAnimeFinished;

	protected OneShotAnime mOneShotAnime;

	public int StageNo { get; protected set; }

	public bool IsUnlockAnimeFinished
	{
		get
		{
			return mIsUnlockAnimeFinished;
		}
	}

	public bool IsLockAnimeFinished
	{
		get
		{
			return mIsLockAnimeFinished;
		}
	}

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		mStateStatus = mState.GetStatus();
		switch (mStateStatus)
		{
		case STATE.ON:
			if (mIsUnlockAnimeFinished)
			{
				PlayAnime(DefaultAnimeKey + "_UNLOCK_IDLE", true);
				if (mOneShotAnime != null)
				{
					Object.Destroy(mOneShotAnime.gameObject);
					mOneShotAnime = null;
				}
				mState.Change(STATE.STAY);
			}
			break;
		case STATE.OFF:
			if (mIsLockAnimeFinished)
			{
				PlayAnime(DefaultAnimeKey + "_LOCK_IDLE", true);
				if (mOneShotAnime != null)
				{
					Object.Destroy(mOneShotAnime.gameObject);
					mOneShotAnime = null;
				}
				mState.Change(STATE.STAY);
			}
			break;
		case STATE.WAIT:
			if (_sprite.IsAnimationFinished() || mForceAnimeFinished)
			{
				mForceAnimeFinished = false;
				mState.Change(STATE.STAY);
			}
			break;
		}
		mState.Update();
	}

	public virtual void Init(int _counter, string _name, BIJImage atlas, int a_mapIndex, float x, float y, Vector3 scale, float a_rad, string a_animeKey)
	{
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mAtlas = atlas;
		mScale = scale;
		base._zrot = a_rad;
		float order = float.Parse(_name);
		float mAP_LINE_Z = Def.MAP_LINE_Z;
		base._pos = new Vector3(x, y, mAP_LINE_Z);
		Player mPlayer = mGame.mPlayer;
		mSeries = mPlayer.Data.CurrentSeries;
		int num = (StageNo = Def.GetStageNoByRouteOrder(order));
		base.gameObject.name = "line_" + num;
		Player.STAGE_STATUS sTAGE_STATUS = Player.STAGE_STATUS.NOOPEN;
		if (mGame.mEventMode)
		{
			PlayerEventData data;
			mPlayer.Data.GetMapData(mSeries, mPlayer.Data.CurrentEventID, out data);
			if (data != null)
			{
				sTAGE_STATUS = mPlayer.GetLineStatus(mPlayer.Data.CurrentEventID, data.CourseID, num);
			}
		}
		else
		{
			sTAGE_STATUS = mPlayer.GetLineStatus(mSeries, num);
		}
		switch (sTAGE_STATUS)
		{
		case Player.STAGE_STATUS.NOOPEN:
			PlayAnime(AnimeKey + "_NOOPEN", true);
			break;
		case Player.STAGE_STATUS.LOCK:
			PlayAnime(AnimeKey + "_LOCK_IDLE", true);
			break;
		default:
			PlayAnime(AnimeKey + "_UNLOCK_IDLE", true);
			break;
		}
		IsLockTapAnime = false;
		HasTapAnime = false;
		mFirstInitialized = true;
	}

	public void ChangeAnime(Player.STAGE_STATUS a_status)
	{
		switch (a_status)
		{
		case Player.STAGE_STATUS.NOOPEN:
			PlayAnime("MAP_F_FIXED_1", true);
			break;
		case Player.STAGE_STATUS.LOCK:
			PlayAnime(AnimeKey + "_LOCK_IDLE", true);
			break;
		default:
			PlayAnime(AnimeKey + "_UNLOCK_IDLE", true);
			break;
		}
	}

	public void LockAnime()
	{
		AnimeKey = DefaultAnimeKey;
		mChangeParts = new Dictionary<string, string>();
		mState.Change(STATE.OFF);
		mIsLockAnimeFinished = false;
		mOneShotAnime = Util.CreateOneShotAnime("LineLock", AnimeKey + "_LOCK", base.gameObject, Vector3.zero, Vector3.one, 0f, 0f, false, OnLockAnimationFinished);
		mGame.PlaySe("SE_MAP_OPEN_AREA", -1, 0.3f, 0.1f);
	}

	public void UnlockAnime()
	{
		AnimeKey = DefaultAnimeKey;
		mChangeParts = new Dictionary<string, string>();
		mState.Change(STATE.ON);
		mIsUnlockAnimeFinished = false;
		mOneShotAnime = Util.CreateOneShotAnime("LineUnlock", AnimeKey + "_UNLOCK", base.gameObject, Vector3.zero, Vector3.one, 0f, 0f, false, OnUnlockAnimationFinished);
		mGame.PlaySe("SE_MAP_OPEN_AREA", -1, 1f, 0.1f);
	}

	public void SetEnable(bool a_flg)
	{
	}

	public void OnLockAnimationFinished(SsSprite sprite)
	{
		mIsLockAnimeFinished = true;
	}

	public void OnUnlockAnimationFinished(SsSprite sprite)
	{
		mIsUnlockAnimeFinished = true;
	}
}
