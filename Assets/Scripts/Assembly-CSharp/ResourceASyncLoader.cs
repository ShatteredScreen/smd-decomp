using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using live2d.framework;

public class ResourceASyncLoader : SingletonMonoBehaviour<ResourceASyncLoader>
{
	public enum REQSTATE
	{
		IDLE = 0,
		WAIT_LOAD_INAPP = 1,
		LOAD_AB = 2,
		WAIT_LOAD_AB = 3,
		LOAD_FROM_AB = 4,
		WAIT_LOAD_FROM_AB = 5,
		LOADALL_FROM_AB = 6,
		WAIT_LOADALL_FROM_AB = 7,
		WAIT_ENABLE_LOADALL = 8,
		LOAD_LIVE2D = 9,
		WAIT_LOAD_LIVE2D = 10,
		COMPLETE = 11,
		DYING = 12,
		ERROR_LOAD_AB = 13
	}

	public class ASyncLoadRequest
	{
		public REQSTATE mReqState;

		public int mSubState;

		public int mCnt;

		public string mKey;

		public Res.ResCommonAttr mCommonAttr;

		public ResourceInstance mResource;

		public bool mInApp;

		public bool mABLoaded;

		public bool mWaitAllLoad;

		public bool mUpdated;

		public ResourceInstance mAllLoadResource;

		public List<string> mAseetList;

		public ASyncLoadRequest()
		{
			mReqState = REQSTATE.IDLE;
			mSubState = 0;
			mCnt = 0;
			mKey = string.Empty;
			mCommonAttr = null;
			mResource = null;
			mInApp = true;
			mABLoaded = false;
			mWaitAllLoad = false;
			mUpdated = false;
			mAllLoadResource = null;
			mAseetList = null;
		}
	}

	private enum STATE
	{
		IDLE = 0
	}

	public enum WAIT_REQUEST
	{
		WAIT_REQ_NON = 0,
		WAIT_REQ_FADE = 1
	}

	private const int PROCESS_MAX = 10;

	private const int LoadABRetryNum = 5;

	private STATE mState;

	private static List<ASyncLoadRequest> mRequestList = new List<ASyncLoadRequest>();

	private static List<ASyncLoadRequest> mProcessingList = new List<ASyncLoadRequest>();

	private static List<ASyncLoadRequest> mLoadAllList = new List<ASyncLoadRequest>();

	private static bool mEnableAllLoad = false;

	private static int mWaitAsyncLoadRequest = 0;

	public static int mASyncLoadCount = 0;

	public static void setAllLoadEnable(bool enable)
	{
		mEnableAllLoad = enable;
	}

	public static void DisableAsyncLoad(WAIT_REQUEST aReq)
	{
		mWaitAsyncLoadRequest |= (int)aReq;
	}

	public static void EnableAsyncLoad(WAIT_REQUEST aReq)
	{
		mWaitAsyncLoadRequest &= (int)(~aReq);
	}

	public static void ResMngLog(string str)
	{
	}

	public static void RequestAsyncLoad(string _key, Res.ResCommonAttr _commonAttr, ResourceInstance _resource, bool _waitAllLoad = false)
	{
		ASyncLoadRequest aSyncLoadRequest = new ASyncLoadRequest();
		aSyncLoadRequest.mCommonAttr = _commonAttr;
		aSyncLoadRequest.mResource = _resource;
		aSyncLoadRequest.mKey = _key;
		aSyncLoadRequest.mWaitAllLoad = _waitAllLoad;
		mRequestList.Add(aSyncLoadRequest);
	}

	private new void Awake()
	{
	}

	private void Start()
	{
	}

	public void Update()
	{
		int num = 0;
		ASyncLoadRequest aSyncLoadRequest = null;
		for (int i = 0; i < mProcessingList.Count; i++)
		{
			aSyncLoadRequest = mProcessingList[i];
			if (aSyncLoadRequest.mReqState != REQSTATE.COMPLETE)
			{
				num++;
			}
			UpdateRequest(aSyncLoadRequest);
		}
		for (int j = 0; j < mLoadAllList.Count; j++)
		{
			aSyncLoadRequest = mLoadAllList[j];
			if (aSyncLoadRequest.mAllLoadResource != null)
			{
				num++;
				aSyncLoadRequest.mUpdated = true;
				UpdateRequest(aSyncLoadRequest);
			}
		}
		if (mWaitAsyncLoadRequest == 0)
		{
			while (mRequestList.Count > 0 && num < 10)
			{
				aSyncLoadRequest = mRequestList[0];
				mRequestList.RemoveAt(0);
				UpdateRequest(aSyncLoadRequest);
				mProcessingList.Add(aSyncLoadRequest);
				num++;
			}
			for (int k = 0; k < mLoadAllList.Count; k++)
			{
				aSyncLoadRequest = mLoadAllList[k];
				if (!aSyncLoadRequest.mUpdated)
				{
					if (aSyncLoadRequest.mReqState != REQSTATE.COMPLETE)
					{
						num++;
					}
					UpdateRequest(aSyncLoadRequest);
					if (num >= 10)
					{
						break;
					}
				}
			}
		}
		mASyncLoadCount = num;
		for (int num2 = mLoadAllList.Count - 1; num2 >= 0; num2--)
		{
			aSyncLoadRequest = mLoadAllList[num2];
			aSyncLoadRequest.mUpdated = false;
			if (aSyncLoadRequest.mReqState == REQSTATE.DYING)
			{
				mLoadAllList.RemoveAt(num2);
			}
		}
		for (int num3 = mProcessingList.Count - 1; num3 >= 0; num3--)
		{
			aSyncLoadRequest = mProcessingList[num3];
			if (aSyncLoadRequest.mReqState == REQSTATE.LOADALL_FROM_AB || aSyncLoadRequest.mReqState == REQSTATE.WAIT_ENABLE_LOADALL)
			{
				mLoadAllList.Add(aSyncLoadRequest);
				mProcessingList.RemoveAt(num3);
			}
			else if (aSyncLoadRequest.mReqState == REQSTATE.DYING)
			{
				mProcessingList.RemoveAt(num3);
			}
		}
	}

	public void UpdateRequest(ASyncLoadRequest aRequest)
	{
		switch (aRequest.mReqState)
		{
		case REQSTATE.IDLE:
			if (aRequest.mResource == null)
			{
				break;
			}
			if (aRequest.mCommonAttr.abName.Length > 0 && GameMain.USE_RESOURCEDL)
			{
				if (!ResourceManager.AlreadyLoaded(aRequest.mCommonAttr.abName))
				{
					ResMngLog("ResourceASyncLoader::StartLoad AssetBundle " + aRequest.mResource.Name);
					aRequest.mInApp = false;
					aRequest.mReqState = REQSTATE.WAIT_LOAD_AB;
					StartCoroutine(LoadAssetBundle(aRequest));
					break;
				}
				ResMngLog("ResourceASyncLoader::StartLoad from AssetBundle " + aRequest.mResource.Name);
				aRequest.mInApp = false;
				switch (aRequest.mResource.ResourceType)
				{
				case ResourceInstance.TYPE.IMAGE:
				case ResourceInstance.TYPE.SS_ANIMATION:
				case ResourceInstance.TYPE.SOUND:
				case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
					aRequest.mReqState = REQSTATE.WAIT_LOAD_FROM_AB;
					StartCoroutine(LoadFromAssetBundle(aRequest));
					break;
				case ResourceInstance.TYPE.L2D_ANIMATION:
					aRequest.mReqState = REQSTATE.WAIT_LOAD_LIVE2D;
					StartCoroutine(LoadLive2DAnimation(aRequest));
					break;
				}
			}
			else
			{
				ResMngLog("ResourceASyncLoader::StartLoad in App " + aRequest.mResource.Name);
				switch (aRequest.mResource.ResourceType)
				{
				case ResourceInstance.TYPE.IMAGE:
				case ResourceInstance.TYPE.SS_ANIMATION:
				case ResourceInstance.TYPE.SOUND:
				case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
					aRequest.mReqState = REQSTATE.WAIT_LOAD_INAPP;
					StartCoroutine(LoadInApp(aRequest));
					break;
				case ResourceInstance.TYPE.L2D_ANIMATION:
					aRequest.mReqState = REQSTATE.WAIT_LOAD_LIVE2D;
					StartCoroutine(LoadLive2DAnimation(aRequest));
					break;
				}
			}
			break;
		case REQSTATE.WAIT_LOAD_INAPP:
			break;
		case REQSTATE.WAIT_LOAD_AB:
			break;
		case REQSTATE.LOAD_FROM_AB:
			aRequest.mReqState = REQSTATE.WAIT_LOAD_FROM_AB;
			StartCoroutine(LoadFromAssetBundle(aRequest));
			break;
		case REQSTATE.WAIT_LOAD_FROM_AB:
			break;
		case REQSTATE.LOADALL_FROM_AB:
			aRequest.mReqState = REQSTATE.WAIT_LOADALL_FROM_AB;
			LoadAllFromAssetBundle(aRequest);
			break;
		case REQSTATE.WAIT_LOADALL_FROM_AB:
			LoadAllFromAssetBundle(aRequest);
			break;
		case REQSTATE.WAIT_ENABLE_LOADALL:
			if (mEnableAllLoad)
			{
				aRequest.mReqState = REQSTATE.WAIT_LOADALL_FROM_AB;
				LoadAllFromAssetBundle(aRequest);
			}
			break;
		case REQSTATE.LOAD_LIVE2D:
			aRequest.mReqState = REQSTATE.WAIT_LOAD_LIVE2D;
			StartCoroutine(LoadLive2DAnimation(aRequest));
			break;
		case REQSTATE.WAIT_LOAD_LIVE2D:
			break;
		case REQSTATE.COMPLETE:
			if (aRequest.mABLoaded)
			{
				int loadCount = ResourceManager.GetLoadCount(aRequest.mCommonAttr.abName);
				if (loadCount > 1 || IsAssetBundleAccessing(aRequest.mCommonAttr.abName, aRequest))
				{
					break;
				}
				ResourceManager.AddLoadingCounter(aRequest.mCommonAttr.abName, -1);
				ResourceManager.UnloadAssetBundle(aRequest.mCommonAttr.abName);
			}
			aRequest.mReqState = REQSTATE.DYING;
			break;
		case REQSTATE.DYING:
			break;
		case REQSTATE.ERROR_LOAD_AB:
			LoadAssetBundleError(aRequest);
			break;
		case REQSTATE.LOAD_AB:
			break;
		}
	}

	private IEnumerator LoadInApp(ASyncLoadRequest aRequest)
	{
		if (aRequest.mResource.LoadState == ResourceInstance.LOADSTATE.INIT)
		{
			aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.LOADING;
			ResourceRequest request;
			switch (aRequest.mResource.ResourceType)
			{
			default:
				yield break;
			case ResourceInstance.TYPE.IMAGE:
			case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
				request = Resources.LoadAsync(aRequest.mResource.Name, typeof(GameObject));
				break;
			case ResourceInstance.TYPE.SS_ANIMATION:
				request = Resources.LoadAsync(aRequest.mResource.Name, typeof(SsAnimation));
				break;
			case ResourceInstance.TYPE.SOUND:
				request = Resources.LoadAsync(aRequest.mResource.Name, typeof(AudioClip));
				break;
			}
			yield return 0;
			while (!request.isDone)
			{
				ResMngLog("ResourceASyncLoader::Loading " + aRequest.mResource.Name);
				yield return 0;
			}
			ResMngLog("ResourceASyncLoader::CompleteLoad " + aRequest.mResource.Name);
			aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
			Object obj = request.asset;
			switch (aRequest.mResource.ResourceType)
			{
			case ResourceInstance.TYPE.IMAGE:
			{
				GameObject prefab = Object.Instantiate(obj) as GameObject;
				ResImage resImage = aRequest.mResource as ResImage;
				resImage.Atlas = prefab.GetComponent<UIAtlas>();
				resImage.mPrefab = prefab;
				break;
			}
			case ResourceInstance.TYPE.SS_ANIMATION:
			{
				ResSsAnimation resSsAnimation = aRequest.mResource as ResSsAnimation;
				resSsAnimation.SsAnime = Object.Instantiate(obj) as SsAnimation;
				break;
			}
			case ResourceInstance.TYPE.SOUND:
			{
				ResSound resSound = aRequest.mResource as ResSound;
				resSound.mAudioClip = Object.Instantiate(obj) as AudioClip;
				break;
			}
			case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
			{
				GameObject prefab2 = Object.Instantiate(obj) as GameObject;
				ResScriptableObject resSO = aRequest.mResource as ResScriptableObject;
				resSO.Container = prefab2.GetComponent<ScriptableObjectContainer>();
				resSO.mPrefab = prefab2;
				break;
			}
			}
		}
		aRequest.mResource = null;
		aRequest.mReqState = REQSTATE.COMPLETE;
	}

	private IEnumerator LoadFromAssetBundleProc(ResourceInstance resource, string bundleName)
	{
		ResourceManager.AddLoadingCounter(bundleName, 1);
		AssetBundleRequest abRequest;
		switch (resource.ResourceType)
		{
		default:
			yield break;
		case ResourceInstance.TYPE.IMAGE:
		case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
			abRequest = ResourceManager.LoadResourceAsyncFromAssetBundle(resource.Name + ".prefab", bundleName);
			break;
		case ResourceInstance.TYPE.SS_ANIMATION:
			abRequest = ResourceManager.LoadResourceAsyncFromAssetBundle(resource.Name + ".asset", bundleName);
			break;
		case ResourceInstance.TYPE.SOUND:
			abRequest = ResourceManager.LoadResourceAsyncFromAssetBundle(resource.Name + ".mp3", bundleName);
			break;
		}
		while (!abRequest.isDone)
		{
			ResMngLog("ResourceASyncLoader::Loading from AssetBundle " + resource.Name);
			yield return 0;
		}
		ResourceManager.AddLoadingCounter(bundleName, -1);
		ResMngLog("ResourceASyncLoader::CompleteLoad from AssetBundle " + resource.Name);
		Object obj = abRequest.asset;
		switch (resource.ResourceType)
		{
		case ResourceInstance.TYPE.IMAGE:
		{
			GameObject prefab = Object.Instantiate(obj) as GameObject;
			ResImage resImage = resource as ResImage;
			resImage.Atlas = prefab.GetComponent<UIAtlas>();
			resImage.mPrefab = prefab;
			break;
		}
		case ResourceInstance.TYPE.SS_ANIMATION:
		{
			ResSsAnimation resSsAnimation = resource as ResSsAnimation;
			resSsAnimation.SsAnime = Object.Instantiate(obj) as SsAnimation;
			break;
		}
		case ResourceInstance.TYPE.SOUND:
		{
			ResSound resSound = resource as ResSound;
			resSound.mAudioClip = Object.Instantiate(obj) as AudioClip;
			break;
		}
		case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
		{
			GameObject prefab2 = Object.Instantiate(obj) as GameObject;
			ResScriptableObject resSO = resource as ResScriptableObject;
			resSO.Container = prefab2.GetComponent<ScriptableObjectContainer>();
			resSO.mPrefab = prefab2;
			break;
		}
		}
	}

	private IEnumerator LoadFromAssetBundle(ASyncLoadRequest aRequest)
	{
		if (aRequest.mResource.LoadState == ResourceInstance.LOADSTATE.INIT)
		{
			aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.LOADING;
			yield return StartCoroutine(LoadFromAssetBundleProc(aRequest.mResource, aRequest.mCommonAttr.abName));
			switch (aRequest.mResource.ResourceType)
			{
			case ResourceInstance.TYPE.IMAGE:
			{
				ResImage resImage = aRequest.mResource as ResImage;
				while (resImage.Atlas == null)
				{
					yield return 0;
				}
				aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
				break;
			}
			case ResourceInstance.TYPE.SS_ANIMATION:
			{
				ResSsAnimation resSsAnimation = aRequest.mResource as ResSsAnimation;
				while (resSsAnimation.SsAnime == null)
				{
					yield return 0;
				}
				ResourceManager.CheckSsAnimeMatShaderLink(resSsAnimation);
				aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
				break;
			}
			case ResourceInstance.TYPE.L2D_ANIMATION:
				aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
				break;
			case ResourceInstance.TYPE.SOUND:
			{
				ResSound resSound = aRequest.mResource as ResSound;
				while (resSound.mAudioClip == null)
				{
					yield return 0;
				}
				aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
				break;
			}
			case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
			{
				ResScriptableObject resSO = aRequest.mResource as ResScriptableObject;
				aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
				break;
			}
			default:
				aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
				break;
			}
		}
		if (aRequest.mABLoaded)
		{
			if (aRequest.mResource.ResourceType == ResourceInstance.TYPE.SCRIPTABLE_OBJECT)
			{
				aRequest.mResource = null;
				aRequest.mReqState = REQSTATE.COMPLETE;
				yield break;
			}
			if (!aRequest.mWaitAllLoad)
			{
				aRequest.mReqState = REQSTATE.LOADALL_FROM_AB;
			}
			else
			{
				aRequest.mReqState = REQSTATE.WAIT_ENABLE_LOADALL;
			}
			aRequest.mSubState = 0;
		}
		else
		{
			aRequest.mResource = null;
			aRequest.mReqState = REQSTATE.COMPLETE;
		}
	}

	private void LoadAllFromAssetBundle(ASyncLoadRequest aRequest)
	{
		switch (aRequest.mSubState)
		{
		case 0:
			aRequest.mAseetList = ResourceManager.ABInfoList.GetResouecesFromAssetBundle(aRequest.mCommonAttr.abName);
			if (aRequest.mAseetList != null)
			{
				aRequest.mCnt = 0;
				aRequest.mSubState = 1;
			}
			else
			{
				aRequest.mResource = null;
				aRequest.mReqState = REQSTATE.COMPLETE;
			}
			break;
		case 1:
		{
			string empty = string.Empty;
			aRequest.mAllLoadResource = null;
			for (int i = aRequest.mCnt; i < aRequest.mAseetList.Count; i++)
			{
				empty = aRequest.mAseetList[i];
				if (empty.Contains(".prefab"))
				{
					string fileName = empty.Substring(0, empty.LastIndexOf(".prefab"));
					ResImage resImage2 = ResourceManager.GetResImage(fileName, true);
					if (resImage2.Atlas != null || resImage2.LoadState != 0)
					{
						continue;
					}
					aRequest.mAllLoadResource = resImage2;
				}
				else if (empty.Contains("ssa.asset"))
				{
					string fileName2 = empty.Substring(0, empty.LastIndexOf(".asset"));
					ResSsAnimation resSsAnimation2 = ResourceManager.GetResSsAnimation(fileName2, true);
					if (resSsAnimation2.SsAnime != null || resSsAnimation2.LoadState != 0)
					{
						continue;
					}
					aRequest.mAllLoadResource = resSsAnimation2;
				}
				else
				{
					if (!empty.Contains(".mp3"))
					{
						continue;
					}
					string fileName3 = empty.Substring(0, empty.LastIndexOf(".mp3"));
					ResSound resSound2 = ResourceManager.GetResSound(fileName3, true);
					if (resSound2.mAudioClip != null || resSound2.LoadState != 0)
					{
						continue;
					}
					aRequest.mAllLoadResource = resSound2;
				}
				aRequest.mCnt = i + 1;
				break;
			}
			if (aRequest.mAllLoadResource == null)
			{
				aRequest.mResource = null;
				aRequest.mReqState = REQSTATE.COMPLETE;
			}
			else
			{
				aRequest.mAllLoadResource.LoadState = ResourceInstance.LOADSTATE.LOADING;
				StartCoroutine(LoadFromAssetBundleProc(aRequest.mAllLoadResource, aRequest.mCommonAttr.abName));
				aRequest.mSubState = 2;
			}
			break;
		}
		case 2:
		{
			bool flag = false;
			switch (aRequest.mAllLoadResource.ResourceType)
			{
			case ResourceInstance.TYPE.IMAGE:
			{
				ResImage resImage = aRequest.mAllLoadResource as ResImage;
				if (resImage.Atlas == null)
				{
					flag = true;
				}
				break;
			}
			case ResourceInstance.TYPE.SS_ANIMATION:
			{
				ResSsAnimation resSsAnimation = aRequest.mAllLoadResource as ResSsAnimation;
				if (resSsAnimation.SsAnime == null)
				{
					flag = true;
				}
				break;
			}
			case ResourceInstance.TYPE.SOUND:
			{
				ResSound resSound = aRequest.mAllLoadResource as ResSound;
				if (resSound.mAudioClip == null)
				{
					flag = true;
				}
				break;
			}
			case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
			{
				ResScriptableObject resScriptableObject = aRequest.mAllLoadResource as ResScriptableObject;
				if (resScriptableObject.Container == null)
				{
					flag = true;
				}
				break;
			}
			}
			if (!flag)
			{
				if (aRequest.mAllLoadResource.ResourceType == ResourceInstance.TYPE.SS_ANIMATION)
				{
					ResSsAnimation resSsAnime = aRequest.mAllLoadResource as ResSsAnimation;
					ResourceManager.CheckSsAnimeMatShaderLink(resSsAnime);
				}
				aRequest.mAllLoadResource.LoadState = ResourceInstance.LOADSTATE.DONE;
				aRequest.mAllLoadResource = null;
				aRequest.mSubState = 1;
			}
			break;
		}
		}
	}

	private IEnumerator LoadAssetBundle(ASyncLoadRequest aRequest)
	{
		if (aRequest.mResource == null)
		{
			yield break;
		}
		float _start = Time.realtimeSinceStartup;
		string error = string.Empty;
		int retryCount = 0;
		bool loaded = false;
		bool success = false;
		while (!success)
		{
			yield return StartCoroutine(SingletonMonoBehaviour<ResourceManager>.Instance.LoadAssetBundle(aRequest.mCommonAttr.abName, delegate(AssetBundle ab, string arg)
			{
				loaded = true;
				error = arg;
			}));
			if (error == string.Empty)
			{
				success = true;
				if (loaded)
				{
					aRequest.mABLoaded = true;
					ResourceManager.AddLoadingCounter(aRequest.mCommonAttr.abName, 1);
				}
				if (retryCount > 0)
				{
					ResourceManager.mDebugButtonColor = Color.yellow;
				}
				continue;
			}
			ResourceDLUtils.err("ERROR(" + retryCount + ") = " + error, true);
			ResMngLog("***** ResourceASyncLoader::LoadAssetBundle() Error(" + retryCount + ") = " + aRequest.mCommonAttr.abName);
			ResourceManager.mABLoadError = true;
			retryCount++;
			if (retryCount <= 5)
			{
				error = string.Empty;
				continue;
			}
			aRequest.mReqState = REQSTATE.ERROR_LOAD_AB;
			ResourceManager.mDebugButtonColor = Color.black;
			yield break;
		}
		switch (aRequest.mResource.ResourceType)
		{
		case ResourceInstance.TYPE.IMAGE:
		case ResourceInstance.TYPE.SS_ANIMATION:
		case ResourceInstance.TYPE.SOUND:
		case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
			aRequest.mReqState = REQSTATE.LOAD_FROM_AB;
			break;
		case ResourceInstance.TYPE.L2D_ANIMATION:
			aRequest.mReqState = REQSTATE.LOAD_LIVE2D;
			break;
		}
	}

	private void LoadAssetBundleError(ASyncLoadRequest aRequest)
	{
		switch (aRequest.mResource.ResourceType)
		{
		case ResourceInstance.TYPE.IMAGE:
		{
			ResImage resImage = aRequest.mResource as ResImage;
			resImage.Atlas = null;
			break;
		}
		case ResourceInstance.TYPE.SS_ANIMATION:
		{
			ResSsAnimation resSsAnimation = aRequest.mResource as ResSsAnimation;
			resSsAnimation.SsAnime = null;
			break;
		}
		case ResourceInstance.TYPE.L2D_ANIMATION:
		{
			ResLive2DAnimation resLive2DAnimation = aRequest.mResource as ResLive2DAnimation;
			resLive2DAnimation.ModelSetting = null;
			break;
		}
		case ResourceInstance.TYPE.SOUND:
		{
			ResSound resSound = aRequest.mResource as ResSound;
			resSound.mAudioClip = null;
			break;
		}
		case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
		{
			ResScriptableObject resScriptableObject = aRequest.mResource as ResScriptableObject;
			resScriptableObject.Container = null;
			break;
		}
		}
		aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
		aRequest.mResource = null;
		aRequest.mReqState = REQSTATE.COMPLETE;
	}

	private IEnumerator LoadLive2DAnimation(ASyncLoadRequest aRequest)
	{
		ResMngLog("ResourceASyncLoader::LoadLive2DAnimation() Start = " + aRequest.mResource.Name);
		if (aRequest.mResource.LoadState == ResourceInstance.LOADSTATE.INIT)
		{
			aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.LOADING;
			ResLive2DAnimation resLive2DAnimation2 = aRequest.mResource as ResLive2DAnimation;
			string dictKey2 = aRequest.mResource.Name + ".bytes";
			string bundleName = aRequest.mCommonAttr.abName;
			ModelSettingJson modelSetting = null;
			Dictionary<string, byte[]> packData = new Dictionary<string, byte[]>();
			BIJMemoryBinaryReader reader2 = null;
			ResMngLog("  Load proc 1 :: mResource.Name = " + aRequest.mResource.Name);
			TextAsset text;
			if (aRequest.mInApp)
			{
				ResMngLog("  Load inApp");
				ResourceRequest request2 = Resources.LoadAsync(aRequest.mResource.Name, typeof(TextAsset));
				yield return 0;
				while (!request2.isDone)
				{
					yield return 0;
				}
				Object obj4 = request2.asset;
				text = Object.Instantiate(obj4) as TextAsset;
				reader2 = new BIJMemoryBinaryReader(text.bytes);
			}
			else
			{
				ResMngLog("  Load from AssetBundle");
				ResMngLog("   :: dictKey = " + dictKey2);
				ResMngLog("   :: bundleName = " + bundleName);
				AssetBundleRequest abRequest2 = ResourceManager.LoadResourceAsyncFromAssetBundle(dictKey2, bundleName);
				yield return 0;
				while (!abRequest2.isDone)
				{
					ResMngLog("  Waiting ...");
					yield return 0;
				}
				ResMngLog("  Loaded !!!");
				Object obj3 = abRequest2.asset;
				text = Object.Instantiate(obj3) as TextAsset;
				reader2 = new BIJMemoryBinaryReader(text.bytes);
			}
			int num = reader2.ReadInt();
			for (int l = 0; l < num; l++)
			{
				string file = reader2.ReadUTF();
				int size = reader2.ReadInt();
				byte[] buff = new byte[size];
				reader2.ReadRaw(buff, 0, size);
				if (!packData.ContainsKey(file))
				{
					packData.Add(file, buff);
				}
			}
			reader2.Close();
			if (text != null)
			{
				GameMain.SafeDestroy(text);
			}
			byte[] parts;
			if (packData.TryGetValue("model.json", out parts))
			{
				modelSetting = new ModelSettingJson(parts);
			}
			if (modelSetting == null)
			{
				resLive2DAnimation2 = null;
				ResMngLog("  Load NG");
			}
			else
			{
				resLive2DAnimation2.BasePath = Path.GetDirectoryName(aRequest.mResource.Name);
				string subFileName6 = modelSetting.GetModelFile();
				if (packData.TryGetValue(subFileName6, out parts))
				{
					resLive2DAnimation2.ModelData = parts;
				}
				subFileName6 = modelSetting.GetPhysicsFile();
				if (subFileName6 != null && packData.TryGetValue(subFileName6, out parts))
				{
					resLive2DAnimation2.PhysicsData = parts;
				}
				string[] motionGroupNames = modelSetting.GetMotionGroupNames();
				if (motionGroupNames != null)
				{
					resLive2DAnimation2.MotionDataDict = new Dictionary<string, byte[]>();
					for (int k = 0; k < modelSetting.GetMotionNum(motionGroupNames[0]); k++)
					{
						subFileName6 = modelSetting.GetMotionFile(motionGroupNames[0], k);
						if (packData.TryGetValue(subFileName6, out parts))
						{
							resLive2DAnimation2.MotionDataDict.Add(modelSetting.GetMotionFile(motionGroupNames[0], k), parts);
						}
					}
				}
				if (modelSetting.GetExpressionNum() > 0)
				{
					resLive2DAnimation2.ExpressionDataDict = new Dictionary<string, byte[]>();
					for (int j = 0; j < modelSetting.GetExpressionNum(); j++)
					{
						subFileName6 = modelSetting.GetExpressionFile(j);
						if (packData.TryGetValue(subFileName6, out parts))
						{
							resLive2DAnimation2.ExpressionDataDict.Add(modelSetting.GetExpressionFile(j), parts);
						}
					}
				}
				if (modelSetting.GetTextureNum() > 0)
				{
					resLive2DAnimation2.Textures = new Texture2D[modelSetting.GetTextureNum()];
					for (int i = 0; i < resLive2DAnimation2.Textures.Length; i++)
					{
						subFileName6 = resLive2DAnimation2.BasePath + "/" + modelSetting.GetTextureFile(i);
						if (aRequest.mInApp)
						{
							ResourceRequest request2 = Resources.LoadAsync(subFileName6, typeof(Texture2D));
							yield return 0;
							while (!request2.isDone)
							{
								yield return 0;
							}
							Object obj2 = request2.asset;
							resLive2DAnimation2.Textures[i] = Object.Instantiate(obj2) as Texture2D;
							continue;
						}
						dictKey2 = subFileName6 + ".png";
						AssetBundleRequest abRequest2 = ResourceManager.LoadResourceAsyncFromAssetBundle(dictKey2, bundleName);
						yield return 0;
						while (!abRequest2.isDone)
						{
							yield return 0;
						}
						Object obj = abRequest2.asset;
						resLive2DAnimation2.Textures[i] = Object.Instantiate(obj) as Texture2D;
					}
				}
				subFileName6 = modelSetting.GetPoseFile();
				if (subFileName6 != null && packData.TryGetValue(subFileName6, out parts))
				{
					resLive2DAnimation2.Pose = L2DPose.load(parts);
				}
				resLive2DAnimation2.ModelSetting = modelSetting;
			}
			aRequest.mResource.LoadState = ResourceInstance.LOADSTATE.DONE;
		}
		aRequest.mResource = null;
		aRequest.mReqState = REQSTATE.COMPLETE;
	}

	public static bool IsAssetBundleAccessing(string abName, ASyncLoadRequest aRequest = null)
	{
		for (int i = 0; i < mRequestList.Count; i++)
		{
			ASyncLoadRequest aSyncLoadRequest = mRequestList[i];
			if (aSyncLoadRequest != aRequest && abName == aSyncLoadRequest.mCommonAttr.abName)
			{
				switch (aSyncLoadRequest.mReqState)
				{
				case REQSTATE.IDLE:
				case REQSTATE.LOAD_AB:
				case REQSTATE.WAIT_LOAD_AB:
				case REQSTATE.LOAD_FROM_AB:
				case REQSTATE.WAIT_LOAD_FROM_AB:
				case REQSTATE.LOADALL_FROM_AB:
				case REQSTATE.WAIT_LOADALL_FROM_AB:
				case REQSTATE.WAIT_ENABLE_LOADALL:
				case REQSTATE.LOAD_LIVE2D:
				case REQSTATE.WAIT_LOAD_LIVE2D:
					return true;
				}
			}
		}
		for (int j = 0; j < mProcessingList.Count; j++)
		{
			ASyncLoadRequest aSyncLoadRequest = mProcessingList[j];
			if (aSyncLoadRequest != aRequest && abName == aSyncLoadRequest.mCommonAttr.abName)
			{
				switch (aSyncLoadRequest.mReqState)
				{
				case REQSTATE.IDLE:
				case REQSTATE.LOAD_AB:
				case REQSTATE.WAIT_LOAD_AB:
				case REQSTATE.LOAD_FROM_AB:
				case REQSTATE.WAIT_LOAD_FROM_AB:
				case REQSTATE.LOADALL_FROM_AB:
				case REQSTATE.WAIT_LOADALL_FROM_AB:
				case REQSTATE.WAIT_ENABLE_LOADALL:
				case REQSTATE.LOAD_LIVE2D:
				case REQSTATE.WAIT_LOAD_LIVE2D:
					return true;
				}
			}
		}
		for (int k = 0; k < mLoadAllList.Count; k++)
		{
			ASyncLoadRequest aSyncLoadRequest = mLoadAllList[k];
			if (aSyncLoadRequest != aRequest && abName == aSyncLoadRequest.mCommonAttr.abName)
			{
				switch (aSyncLoadRequest.mReqState)
				{
				case REQSTATE.IDLE:
				case REQSTATE.LOAD_AB:
				case REQSTATE.WAIT_LOAD_AB:
				case REQSTATE.LOAD_FROM_AB:
				case REQSTATE.WAIT_LOAD_FROM_AB:
				case REQSTATE.LOADALL_FROM_AB:
				case REQSTATE.WAIT_LOADALL_FROM_AB:
				case REQSTATE.WAIT_ENABLE_LOADALL:
				case REQSTATE.LOAD_LIVE2D:
				case REQSTATE.WAIT_LOAD_LIVE2D:
					return true;
				}
			}
		}
		return false;
	}
}
