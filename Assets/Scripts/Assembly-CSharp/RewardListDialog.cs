using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RewardListDialog : DialogBase
{
	private enum LOAD_ATLAS
	{
		HUD = 0,
		EVENTLABYRINTH = 1,
		ICON = 2,
		FULL_SCREEN_BG = 3
	}

	private enum LayoutTarget
	{
		TitleBarL = 0,
		TitleBarR = 1,
		Title = 2,
		Back = 3,
		Possess = 4,
		DialogFrame = 5,
		BackGround = 6
	}

	private enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2
	}

	public sealed class AtlasInfo : IDisposable
	{
		private bool mInit;

		private bool mForceNotUnload;

		private bool mDisposed;

		public ResImage Resource { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Resource == null) ? null : Resource.Image;
			}
		}

		public string Key { get; private set; }

		public AtlasInfo(string key)
		{
			Key = key;
		}

		~AtlasInfo()
		{
			Dispose(false);
		}

		public IEnumerator Load(bool not_unload = false)
		{
			if (Resource != null)
			{
				UnLoad();
			}
			if (Res.ResImageDict.ContainsKey(Key) && ResourceManager.IsLoaded(Key))
			{
				Resource = ResourceManager.LoadImage(Key);
				if (Resource != null)
				{
					yield break;
				}
			}
			Resource = ResourceManager.LoadImageAsync(Key);
			if (Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.INIT)
			{
				mInit = true;
				mForceNotUnload = not_unload;
			}
		}

		private void UnLoad()
		{
			if (mInit && !mForceNotUnload)
			{
				ResourceManager.UnloadImage(Key);
			}
			mInit = false;
		}

		public bool IsDone()
		{
			return Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.DONE;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	private const int WIDE_SCREEN_LIST_HEIGHT_OFFSET = 20;

	private GameObject mGameObject;

	private Dictionary<LOAD_ATLAS, AtlasInfo> mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();

	private Dictionary<LayoutTarget, Transform> mLayoutTarget = new Dictionary<LayoutTarget, Transform>();

	private UISsSprite mLoadingAnime;

	private SMVerticalList mScrollRect;

	private float mAspectRatio = 1f;

	private int mDialogListHeightOffset;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private int DialogListHeightOffset
	{
		get
		{
			if (mDialogListHeightOffset == 0 && Util.IsWideScreen())
			{
				float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num2 = 0.5625f;
				float num3 = 0.4617052f;
				mDialogListHeightOffset = (int)((num2 - num) / (num2 - num3) * 60f);
			}
			return mDialogListHeightOffset;
		}
	}

	private GameObject GameObject
	{
		get
		{
			if (mGameObject == null)
			{
				mGameObject = base.gameObject;
			}
			return mGameObject;
		}
	}

	public bool IsBusy
	{
		get
		{
			return GetBusy() || mState.GetStatus() != STATE.MAIN;
		}
	}

	public override void Update()
	{
		base.Update();
		mState.Update();
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		float num = vector.y * 0.5f;
		float num2 = (0f - vector.x) * 0.5f;
		float num3 = 0f - num;
		if (Util.IsWideScreen())
		{
			num = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, o).y;
			num2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Left, o).x;
			num3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, o).y;
		}
		float num4 = 70f;
		foreach (int value in Enum.GetValues(typeof(LayoutTarget)))
		{
			if (!mLayoutTarget.ContainsKey((LayoutTarget)value) || !mLayoutTarget[(LayoutTarget)value])
			{
				continue;
			}
			Transform transform = mLayoutTarget[(LayoutTarget)value];
			Vector3? vector2 = null;
			Vector3? vector3 = null;
			IntVector2 intVector = null;
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				switch ((LayoutTarget)value)
				{
				case LayoutTarget.TitleBarL:
					vector2 = new Vector3(2f, num + 50f + 123f);
					break;
				case LayoutTarget.TitleBarR:
					vector2 = new Vector3(-2f, num + 50f + 123f);
					break;
				case LayoutTarget.Title:
					vector2 = new Vector3(0f, num - 28f);
					break;
				case LayoutTarget.Back:
					vector2 = new Vector3(num2 + num4, num3 + 80f);
					break;
				case LayoutTarget.Possess:
					vector2 = new Vector3(0f, num - 128f);
					break;
				case LayoutTarget.DialogFrame:
					vector2 = new Vector3(0f, -15f);
					intVector = new IntVector2(600, 830 + DialogListHeightOffset);
					break;
				case LayoutTarget.BackGround:
					vector3 = new Vector3(1f, (Util.LogScreenSize().y + 2f) / 910f, 1f);
					break;
				}
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				switch ((LayoutTarget)value)
				{
				case LayoutTarget.TitleBarL:
					vector2 = new Vector3(2f, num + 50f + 123f);
					break;
				case LayoutTarget.TitleBarR:
					vector2 = new Vector3(-2f, num + 50f + 123f);
					break;
				case LayoutTarget.Title:
					vector2 = new Vector3(0f, num - 28f);
					break;
				case LayoutTarget.Back:
					vector2 = new Vector3(num2 + num4, num3 + 80f);
					break;
				case LayoutTarget.Possess:
					vector2 = new Vector3(0f, num - 128f);
					break;
				case LayoutTarget.DialogFrame:
					vector2 = new Vector3(0f, (!Util.IsWideScreen()) ? (-15) : 0);
					intVector = new IntVector2(1090, (int)(335f + (335f * mAspectRatio - 335f) * 2f) + DialogListHeightOffset + (Util.IsWideScreen() ? 20 : 0));
					break;
				case LayoutTarget.BackGround:
					vector3 = new Vector3(1f, (Util.LogScreenSize().y + 2f) / 910f, 1f);
					break;
				}
				break;
			}
			if (vector2.HasValue)
			{
				transform.SetLocalPosition(vector2.Value);
			}
			if (vector3.HasValue)
			{
				transform.SetLocalScale(vector3.Value);
			}
			if (intVector != null)
			{
				UIWidget component = transform.GetComponent<UIWidget>();
				if ((bool)component)
				{
					component.SetDimensions(intVector.x, intVector.y);
				}
			}
		}
		if (mScrollRect != null)
		{
			mScrollRect.SetLocalPositionY((!Util.IsWideScreen() || (o != ScreenOrientation.LandscapeLeft && o != ScreenOrientation.LandscapeRight)) ? (-15) : 0);
			mScrollRect.OnScreenChanged(o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown);
		}
	}

	private UILabel CreateLabel(string disp_message, UIFont font, int offset_depth, int font_size, Vector3 pos, UIWidget.Pivot pivot, Color color, GameObject parent = null)
	{
		UILabel uILabel = Util.CreateLabel("Label", (!(parent != null)) ? GameObject : parent, font);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + offset_depth, pos, font_size, 0, 0, pivot);
		Util.SetLabelText(uILabel, disp_message);
		uILabel.color = color;
		return uILabel;
	}

	public override void BuildDialog()
	{
		UISprite uISprite = null;
		UILabel uILabel = null;
		UIButton uIButton = null;
		GameObject gameObject = null;
		string text = null;
		float num = 0f;
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		Vector2 vector = Util.LogScreenSize();
		float num2 = vector.y * 0.5f;
		float num3 = vector.x * 0.5f;
		UIFont font = GameMain.LoadFont();
		if (vector.x > vector.y)
		{
			mAspectRatio = vector.y / (vector.x / 16f * 9f);
		}
		else
		{
			mAspectRatio = vector.x / (vector.y / 16f * 9f);
		}
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out data);
		SetJellyTargetScale(1f);
		uISprite = Util.CreateSprite("BackGround", GameObject, mAtlas[LOAD_ATLAS.FULL_SCREEN_BG].Image);
		Util.SetSpriteInfo(uISprite, "characolle_back00", mBaseDepth, new Vector3(0f, 0f, 200f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions(1386, 910);
		uISprite.SetLocalScale(1f, 1.5230769f, 1f);
		BoxCollider boxCollider = uISprite.gameObject.AddComponent<BoxCollider>();
		boxCollider.size = new Vector3(1384f, 1384f);
		mLayoutTarget.Add(LayoutTarget.BackGround, uISprite.transform);
		gameObject = Util.CreateAnchor("CollectionAnchorBottomLeft", GameObject, UIAnchor.Side.BottomLeft, component).gameObject;
		StartCoroutine(LaceRotateAsync(gameObject, mBaseDepth + 1, new Vector3(-20f, 60f, 0f), Vector3.one, 15f));
		StartCoroutine(LaceRotateAsync(gameObject, mBaseDepth + 1, new Vector3(190f, -50f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		gameObject = Util.CreateAnchor("CollectionAnchorTopRight", GameObject, UIAnchor.Side.TopRight, component).gameObject;
		StartCoroutine(LaceRotateAsync(gameObject, mBaseDepth + 1, new Vector3(-140f, -10f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotateAsync(gameObject, mBaseDepth + 1, new Vector3(30f, -110f, 0f), Vector3.one, 15f));
		uISprite = Util.CreateSprite("TitleBarR", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, num2 + 50f + 123f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mLayoutTarget.Add(LayoutTarget.TitleBarR, uISprite.transform);
		uISprite = Util.CreateSprite("TitleBarL", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(2f, num2 + 50f + 123f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mLayoutTarget.Add(LayoutTarget.TitleBarL, uISprite.transform);
		uILabel = CreateLabel(Localization.Get("AdvEvPointRewardList00"), font, 3, 38, new Vector3(0f, num2 - 28f), UIWidget.Pivot.Center, Color.white);
		mLayoutTarget.Add(LayoutTarget.Title, uILabel.transform);
		text = "button_back";
		num = 70f;
		uIButton = Util.CreateJellyImageButton("BackButton", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 3, new Vector3(0f - num3 + num, 0f - num2 + 80f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
		mLayoutTarget.Add(LayoutTarget.Back, uIButton.transform);
		if (data != null && data.LabyrinthData != null)
		{
			int num4 = 300;
			float num5 = -15f;
			float num6 = 80f;
			num4 = 370;
			num5 = -35f;
			num6 = 100f;
			uISprite = Util.CreateSprite("PossessFrame", GameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH].Image);
			Util.SetSpriteInfo(uISprite, "list_instruction_panel00", mBaseDepth + 3, new Vector3(0f, 430f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(num4, 64);
			gameObject = uISprite.gameObject;
			mLayoutTarget.Add(LayoutTarget.Possess, gameObject.transform);
			CreateLabel(Localization.Get("Labyrinth_Result_02"), font, 4, 28, new Vector3(num5, -1f), UIWidget.Pivot.Center, Def.DEFAULT_MESSAGE_COLOR, gameObject);
			CreateLabel(data.LabyrinthData.JewelAmount.ToString(), font, 4, 28, new Vector3(num6, -1f), UIWidget.Pivot.Center, Def.DEFAULT_MESSAGE_COLOR, gameObject);
		}
		InitDialogFrame(DIALOG_SIZE.LARGE, null, null, 580, 830);
		mDialogFrame.SetLocalPosition(0f, -15f);
		mLayoutTarget.Add(LayoutTarget.DialogFrame, mDialogFrame.transform);
		mLoadingAnime = Util.CreateUISsSprite("LoadingAnime", mDialogFrame.gameObject, "ADV_FIGURE_LOADING", Vector3.zero, Vector3.one, mBaseDepth + 3);
		SetLayout(Util.ScreenOrientation);
	}

	public override IEnumerator BuildResource()
	{
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();
		}
		foreach (AtlasInfo info3 in mAtlas.Values)
		{
			info3.Dispose();
		}
		mAtlas.Clear();
		foreach (int type in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			AtlasInfo info2 = new AtlasInfo(((LOAD_ATLAS)type).ToString());
			mAtlas.Add((LOAD_ATLAS)type, info2);
			StartCoroutine(info2.Load());
		}
		foreach (AtlasInfo info in mAtlas.Values)
		{
			while (!info.IsDone())
			{
				yield return null;
			}
		}
	}

	private IEnumerator CreateListAsync()
	{
		SMEventPageData evPageData = GameMain.GetCurrentEventPageData();
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out eventData);
		if (evPageData == null || evPageData.EventRewardList == null)
		{
			mState.Change(STATE.MAIN);
			yield break;
		}
		int lineCountL = 10;
		int lineCountP = 5;
		int labelScale = 20;
		float iconSize = 98f;
		Vector2 sizeOffset = new Vector2(0f, 10f);
		Vector2 itemSize = new Vector2(iconSize, iconSize + (float)labelScale) + sizeOffset;
		List<int> pointList = new List<int>(evPageData.EventRewardList.Keys);
		UIFont font = GameMain.LoadFont();
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		for (int i = 0; i < pointList.Count; i++)
		{
			list.Add(new SMVerticalListItem());
		}
		mScrollRect = SMVerticalList.Make(listInfo: new SMVerticalListInfo(lHeight: 285f + (285f * mAspectRatio - 285f) * 2f + (float)DialogListHeightOffset + (float)(Util.IsWideScreen() ? 20 : 0), lMaxPerLine: lineCountL, lWidth: (int)(itemSize.x * (float)lineCountL) + 10, pMaxPerLine: lineCountP, pWidth: (int)(itemSize.x * (float)lineCountP) + 10, pHeight: 780 + DialogListHeightOffset, _scrollSpeed: 1, _itemHeight: (int)itemSize.y + 10), _parent: GameObject, _messageTaget: this, _items: list, _posX: 0f, _posY: 0f, _baseDepth: mBaseDepth + 3, _portrait: true, _movement: UIScrollView.Movement.Vertical, _listupdate: true);
		mScrollRect.ScrollBarPositionOffsetPortrait = new Vector3(10f, 0f);
		mScrollRect.ScrollBarPositionOffsetLandscape = new Vector3(10f, 0f);
		mScrollRect.SetLocalPosition(-15f, -100005f);
		mScrollRect.OnCreateItem = delegate(GameObject fn_item_go, int fn_index, SMVerticalListItem fn_item, SMVerticalListItem fn_last_item)
		{
			if (fn_index >= 0 && fn_index < pointList.Count && evPageData.EventRewardList.ContainsKey(pointList[fn_index]))
			{
				int rewardID = evPageData.EventRewardList[pointList[fn_index]].RewardID;
				bool flag = evPageData.EventRewardList[pointList[fn_index]].IsGCItem != 0;
				UISprite uISprite = Util.CreateSprite("Jewel", fn_item_go, mAtlas[LOAD_ATLAS.HUD].Image);
				Util.SetSpriteInfo(uISprite, "icon_currency_jewel", mBaseDepth + 1, new Vector3(-32f, 49f), Vector3.one, false);
				uISprite.SetDimensions(32, 32);
				UILabel uILabel = Util.CreateLabel("Label", fn_item_go, font);
				Util.SetLabelInfo(uILabel, string.Format("{0:#,0}", pointList[fn_index]), mBaseDepth + 1, Vector3.zero, 17, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = Def.DEFAULT_ADV_MESSAGE_COLOR;
				uILabel.SetLocalPosition(13f - sizeOffset.x, (itemSize.y - (float)labelScale - sizeOffset.y) * 0.5f);
				UISprite uISprite2 = Util.CreateSprite("Frame", fn_item_go, mAtlas[LOAD_ATLAS.EVENTLABYRINTH].Image);
				Util.SetSpriteInfo(uISprite2, "panel_day", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
				Vector3 localPosition = uISprite2.transform.localPosition;
				localPosition.y = (itemSize.y - iconSize - sizeOffset.y) * -0.5f;
				uISprite2.transform.localPosition = localPosition;
				uISprite2.width = (int)iconSize;
				uISprite2.height = (int)iconSize;
				if (flag)
				{
					AdvRewardListData advRewardListData = null;
					if (mGame.mAdvRewardListDict.ContainsKey(rewardID))
					{
						advRewardListData = mGame.mAdvRewardListDict[rewardID];
					}
					if (advRewardListData != null)
					{
						if (advRewardListData.Category != 24)
						{
							UISprite uISprite3 = Util.CreateSprite("Icon", fn_item_go, mAtlas[LOAD_ATLAS.HUD].Image);
							Util.SetSpriteInfo(uISprite3, advRewardListData.IconKey, mBaseDepth + 2, Vector3.zero, Vector3.one, false);
							localPosition = uISprite3.transform.localPosition;
							localPosition.y = (itemSize.y - iconSize - sizeOffset.y) * -0.5f;
							uISprite3.transform.localPosition = localPosition;
							uISprite3.width = 64;
							uISprite3.height = 64;
						}
						else
						{
							float num2 = 64f / 123f;
							AdvCharacterData character = mGame.mAdvCharactersDataDict[advRewardListData.Kind];
							AdvIconInstance advIconInstance = AdvIconInstance.Make(fn_item_go, "Icon", character, new Vector3(0f, -10f), mBaseDepth + 3, true);
							advIconInstance.transform.localScale = new Vector3(num2, num2);
						}
						if (!advRewardListData.CanReceive)
						{
							UISprite uISprite4 = Util.CreateSprite("Icon", fn_item_go, mAtlas[LOAD_ATLAS.EVENTLABYRINTH].Image);
							Util.SetSpriteInfo(uISprite4, "login_clearstamp", mBaseDepth + 7, Vector3.zero, Vector3.one, false);
							localPosition = uISprite4.transform.localPosition;
							localPosition.y = (itemSize.y - iconSize - sizeOffset.y) * -0.5f;
							uISprite4.transform.localPosition = localPosition;
						}
					}
				}
				else
				{
					AccessoryData accessoryData = mGame.GetAccessoryData(rewardID);
					if (accessoryData != null)
					{
						string imageName = "null";
						int result = 0;
						bool flag2 = false;
						LOAD_ATLAS key = LOAD_ATLAS.HUD;
						switch (accessoryData.AccessoryType)
						{
						case AccessoryData.ACCESSORY_TYPE.BOOSTER:
						{
							Def.BOOSTER_KIND a_kind;
							accessoryData.GetBooster(out a_kind, out result);
							imageName = mGame.mBoosterData[(int)a_kind].iconBuy;
							flag2 = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
							break;
						}
						case AccessoryData.ACCESSORY_TYPE.ADV_ITEM:
							if (mGame.mAdvRewardListDict.ContainsKey(int.Parse(accessoryData.GotID)))
							{
								AdvRewardListData advRewardListData2 = mGame.mAdvRewardListDict[int.Parse(accessoryData.GotID)];
								imageName = advRewardListData2.IconKey;
								result = (int)advRewardListData2.Value;
								flag2 = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
							}
							break;
						case AccessoryData.ACCESSORY_TYPE.PARTNER:
						{
							CompanionData companionData = mGame.mCompanionData[int.Parse(accessoryData.GotID)];
							imageName = companionData.iconName;
							key = LOAD_ATLAS.ICON;
							result = -1;
							flag2 = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
							break;
						}
						case AccessoryData.ACCESSORY_TYPE.GEM:
							imageName = "icon_84jem";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.GROWUP:
							imageName = "icon_level";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
							imageName = "icon_dailyEvent_stamp02";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.HEART:
							imageName = "icon_heart";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
							imageName = "icon_dailyEvent_stamp01";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.BINGO_KEY:
							imageName = "icon_eventroadblock_key01";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.BINGO_TICKET:
							imageName = "icon_event_ticket00";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
							imageName = "icon_old_event_key";
							goto default;
						case AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY:
							imageName = "icon_eventroadblock_key02";
							goto default;
						default:
							int.TryParse(accessoryData.GotID, out result);
							flag2 = mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index);
							break;
						}
						UISprite uISprite5 = Util.CreateSprite("Icon", fn_item_go, mAtlas[key].Image);
						Util.SetSpriteInfo(uISprite5, imageName, mBaseDepth + 2, Vector3.zero, Vector3.one, false);
						uISprite5.SetLocalPositionY((itemSize.y - iconSize - sizeOffset.y) * -0.5f);
						uISprite5.width = 64;
						uISprite5.height = 64;
						if (result != -1)
						{
							uILabel = Util.CreateLabel("Num", fn_item_go, font);
							Util.SetLabelInfo(uILabel, Localization.Get("Xtext") + result, mBaseDepth + 3, new Vector3(35f, -42f), 17, 75, 0, UIWidget.Pivot.Right);
							Util.SetLabeShrinkContent(uILabel, 75, 17);
							Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
							uILabel.effectStyle = UILabel.Effect.Outline8;
							uILabel.effectColor = Color.white;
							uILabel.effectDistance = new Vector2(2f, 2f);
						}
						if (flag2)
						{
							UISprite uISprite6 = Util.CreateSprite("Icon", fn_item_go, mAtlas[LOAD_ATLAS.EVENTLABYRINTH].Image);
							Util.SetSpriteInfo(uISprite6, "login_clearstamp", mBaseDepth + 7, Vector3.zero, Vector3.one, false);
							localPosition = uISprite6.transform.localPosition;
							localPosition.y = (itemSize.y - iconSize - sizeOffset.y) * -0.5f;
							uISprite6.transform.localPosition = localPosition;
						}
					}
				}
			}
		};
		bool isCreated = false;
		mScrollRect.OnCreateFinish = delegate
		{
			isCreated = true;
			SetLayout(Util.ScreenOrientation);
			int num = evPageData.EventRewardList.Keys.Count((int n) => eventData.LabyrinthData.JewelAmount >= n);
			if (num >= evPageData.EventRewardList.Keys.Count / 2)
			{
				mScrollRect.ScrollSet(1f);
			}
			else
			{
				mScrollRect.ScrollSet(0f);
			}
		};
		while (!isCreated)
		{
			mState.Change(STATE.WAIT);
			yield return null;
		}
		yield return null;
		float posY = -15f;
		ScreenOrientation screenOrientation = mOrientation;
		if ((screenOrientation == ScreenOrientation.LandscapeLeft || screenOrientation == ScreenOrientation.LandscapeRight) && Util.IsWideScreen())
		{
			posY = 0f;
		}
		mScrollRect.SetLocalPosition(-15f, posY);
		GameMain.SafeDestroy(mLoadingAnime.gameObject);
		mLoadingAnime = null;
		mState.Change(STATE.MAIN);
	}

	private IEnumerator LaceRotateAsync(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		UISprite sprite = Util.CreateSprite("Lace", parent, mAtlas[LOAD_ATLAS.FULL_SCREEN_BG].Image);
		Util.SetSpriteInfo(sprite, "characolle_back_lace02", depth, pos, scale, false);
		float angle = 0f;
		Transform trans = sprite.transform;
		while (true)
		{
			angle += Time.deltaTime * speed;
			trans.SetLocalRotation(Quaternion.Euler(0f, 0f, angle));
			yield return null;
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	private void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public override void OnCloseFinished()
	{
		base.OnCloseFinished();
		if (mAtlas != null)
		{
			foreach (AtlasInfo value in mAtlas.Values)
			{
				value.Dispose();
			}
			mAtlas.Clear();
		}
		mAtlas = null;
		GameMain.SafeDestroy(GameObject);
		mGameObject = null;
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
		mState.Change(STATE.WAIT);
		StartCoroutine(CreateListAsync());
	}
}
