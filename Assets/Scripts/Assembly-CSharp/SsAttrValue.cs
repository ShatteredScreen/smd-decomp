public interface SsAttrValue
{
	bool HasValue { get; }

	int RefKeyIndex { get; set; }

	void SetValue(object v);
}
