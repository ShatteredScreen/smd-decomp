public enum SsColorBlendOperation
{
	Non = 0,
	Mix = 1,
	Mul = 2,
	Add = 3,
	Sub = 4,
	Num = 5
}
