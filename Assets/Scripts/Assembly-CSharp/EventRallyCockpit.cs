using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class EventRallyCockpit : EventCockpit
{
	protected struct CoursePanelInfo
	{
		public UISprite Sprite;

		public string ImageName;
	}

	protected const float SaleIconRallyBasePosY_P = 178f;

	protected const float SaleIconRallyBasePosY_P_Next = 290f;

	protected const float SaleIconRallyBasePosY_L = 165f;

	protected const float SaleIconRallyBasePosY_L_Next = 290f;

	protected const float SaleIconRallyPlaceInterval = 112f;

	protected UIButton mCourseNext;

	protected string mNextImage = "null";

	protected bool mPreserveImageSetNext;

	protected UIButton mCourseBack;

	protected string mBackImage = "null";

	protected bool mPreserveImageSetBack;

	protected bool mButtonSE;

	protected int mStarNumMax;

	protected int mStarNumCurrent;

	protected bool mStarNumUpdate;

	protected UISprite mNum1;

	protected UISprite mNum2;

	protected UISprite mNum3;

	protected UISprite mNum4;

	protected string mStarNumImage1 = "event_num0";

	protected string mStarNumImage2 = "event_num0";

	protected string mStarNumImage3 = "event_num0";

	protected string mStarNumImage4 = "event_num0";

	protected bool mCourseUpdate;

	protected UISprite mCourseNumPanel1;

	protected string mCourseNum = "event_num1";

	protected List<CoursePanelInfo> mCoursePanelList;

	protected UISprite mCoursePanel1;

	protected UISprite mCoursePanel2;

	protected UISprite mCoursePanel3;

	protected UISprite mCoursePanel4;

	protected UISprite mCoursePanel5;

	protected UISprite mCoursePanel6;

	protected string mCourseImage1 = "course01";

	protected string mCourseImage2 = "course03";

	protected string mCourseImage3 = "course03";

	protected string mCourseImage4 = "course03";

	protected string mCourseImage5 = "course03";

	protected string mCourseImage6 = "course03";

	protected GameObject mCoursePanelParent;

	protected ParticleSystem mPS;

	protected UISprite mLine;

	protected GameObject mFooterRoot;

	protected override void Update()
	{
		if (mGSM == null)
		{
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			CreateCockpitStatus(40);
			CreateCockpitItems(40);
			CreateCockpitDebug(40);
			CreateSaleItem(40);
			CreateMugenHeart(40);
			if (!mButtonEnableFlg)
			{
				SetEnableButtonAction(false);
				mButtonEnableFlg = true;
			}
			mState.Change(STATE.MAIN);
			break;
		case STATE.OPEN:
			if (mState.IsChanged() && mOpenCallback != null)
			{
				mOpenCallback(false);
			}
			break;
		case STATE.MAIN:
			UpdateSaleFlg();
			if (base.mShopflg)
			{
				base.ShopBagEnable = true;
			}
			else
			{
				base.ShopBagEnable = false;
			}
			UpdateMugenHeartIconFlg();
			if (base.mMugenflg)
			{
				base.ShopMugenHeartEnable = true;
			}
			else
			{
				base.ShopMugenHeartEnable = false;
			}
			break;
		case STATE.MOVE:
			if (!mState.IsChanged())
			{
				break;
			}
			switch (mState.GetPreStatus())
			{
			case STATE.OPEN:
				if (mCloseCallback != null)
				{
					mCloseCallback(true);
				}
				break;
			case STATE.CLOSE:
				if (mOpenCallback != null)
				{
					mOpenCallback(true);
				}
				break;
			}
			break;
		}
		mState.Update();
		UpdateUIPanel();
		UpdateHUD();
		UpdateDrawFlag();
		UpdateDrawOptionFlag();
		UpdateStatus();
	}

	protected override Vector3 ShopBugLocation()
	{
		return (Util.ScreenOrientation != ScreenOrientation.Portrait && Util.ScreenOrientation != ScreenOrientation.PortraitUpsideDown) ? ((string.IsNullOrEmpty(mNextImage) || mNextImage == "null") ? ((!base.mMugenflg) ? new Vector3(-70f, 165f) : new Vector3(-70f, 287f)) : ((!base.mMugenflg) ? new Vector3(-70f, 287f) : new Vector3(-70f, 400f))) : ((!string.IsNullOrEmpty(mNextImage) && !(mNextImage == "null")) ? ((!base.mMugenflg) ? new Vector3(-70f, 290f) : new Vector3(-70f, 400f)) : ((!base.mMugenflg) ? new Vector3(-70f, 178f) : new Vector3(-70f, 290f)));
	}

	protected override Vector3 MugenHeartLocation()
	{
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			if (string.IsNullOrEmpty(mNextImage) || mNextImage == "null")
			{
				return new Vector3(-70f, 178f);
			}
			return new Vector3(-70f, 290f);
		}
		if (string.IsNullOrEmpty(mNextImage) || mNextImage == "null")
		{
			return new Vector3(-70f, 165f);
		}
		return new Vector3(-70f, 287f);
	}

	protected override Vector3 AllCrushIconLocation()
	{
		int num = 0;
		if (base.mShopflg)
		{
			num++;
		}
		if (base.mMugenflg)
		{
			num++;
		}
		Vector3 result;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			result = ((!string.IsNullOrEmpty(mNextImage) && !(mNextImage == "null")) ? new Vector3(-70f, 290f + 112f * (float)num) : new Vector3(-70f, 178f + 112f * (float)num));
		}
		else
		{
			float num2 = 0f;
			if (num > 1)
			{
				num = 0;
				num2 = -112f;
			}
			result = ((!string.IsNullOrEmpty(mNextImage) && !(mNextImage == "null")) ? new Vector3(-70f + num2, 290f + 112f * (float)num) : new Vector3(-70f + num2, 165f + 112f * (float)num));
		}
		return result;
	}

	public void LineCreate()
	{
		Vector2 vector = Util.LogScreenSize();
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		if (mLine != null)
		{
			Object.Destroy(mLine.gameObject);
			mLine = null;
		}
		mLine = Util.CreateSprite("Line", mAnchorT.gameObject, image);
		Util.SetSpriteInfo(mLine, "line", 42, new Vector3(0f, -156f, 0f), Vector3.one, false);
		mLine.type = UIBasicSprite.Type.Sliced;
		mLine.SetDimensions((int)vector.x - 20, 24);
	}

	protected override void CreateCockpitStatus(int baseDepth)
	{
		base.CreateCockpitStatus(baseDepth);
		Vector2 vector = Util.LogScreenSize();
		SMEventPageData eventPageData = GameMain.GetEventPageData(mGame.mPlayer.Data.CurrentEventID);
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		BIJImage image2 = ResourceManager.LoadImage(eventPageData.EventSetting.MapAtlasKey).Image;
		int currentEventID = mGame.mPlayer.Data.CurrentEventID;
		SMSeasonEventSetting sMSeasonEventSetting = mGame.mEventData.InSessionEventList[currentEventID];
		BIJImage image3 = ResourceManager.LoadImage(sMSeasonEventSetting.BannerAtlas).Image;
		UISprite sprite = Util.CreateSprite("EventTitle", mAnchorT.gameObject, image3);
		Util.SetSpriteInfo(sprite, eventPageData.EventSetting.MapEventTitle, baseDepth + 2, new Vector3(0f, -120f, 0f), Vector3.one, false);
		LineCreate();
		mCoursePanelParent = Util.CreateGameObject("CoursePanelParent", mAnchorTR.gameObject);
		SMMapPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		float num = 45f;
		float y = -187f;
		if (currentEventPageData != null)
		{
			if (mCoursePanelList == null)
			{
				mCoursePanelList = new List<CoursePanelInfo>();
			}
			int validCourseCount = currentEventPageData.GetValidCourseCount();
			float num2 = -35f;
			UISprite sprite2 = Util.CreateSprite("CoursePanel", mCoursePanelParent, image);
			Util.SetSpriteInfo(sprite2, "LC_course00", baseDepth + 2, new Vector3(num2 - (float)(validCourseCount - 1) * num - 95f, y), Vector3.one, false);
			mCourseNumPanel1 = Util.CreateSprite("num", mCoursePanelParent, image);
			Util.SetSpriteInfo(mCourseNumPanel1, mCourseNum, baseDepth + 2, new Vector3(num2 - (float)(validCourseCount - 1) * num - 40f, y), Vector3.one, false);
			for (int i = 0; i < validCourseCount; i++)
			{
				CoursePanelInfo item = default(CoursePanelInfo);
				item.ImageName = ((i != 0) ? "course03" : "course01");
				item.Sprite = Util.CreateSprite(new StringBuilder("Course").Append(i + 1).ToString(), mCoursePanelParent, image);
				Util.SetSpriteInfo(item.Sprite, "course01", baseDepth + 2, new Vector3(num2 - (float)(validCourseCount - 1 - i) * num, y), Vector3.one, false);
				mCoursePanelList.Add(item);
			}
		}
		else
		{
			UISprite sprite3 = Util.CreateSprite("CoursePanel", mCoursePanelParent, image);
			Util.SetSpriteInfo(sprite3, "LC_course00", baseDepth + 2, new Vector3(-310f, y, 0f), Vector3.one, false);
			mCourseNumPanel1 = Util.CreateSprite("num", mCoursePanelParent, image);
			Util.SetSpriteInfo(mCourseNumPanel1, mCourseNum, baseDepth + 2, new Vector3(-255f, y, 0f), Vector3.one, false);
			float num3 = 0f;
			mCoursePanel1 = Util.CreateSprite("Course1", mCoursePanelParent, image);
			Util.SetSpriteInfo(mCoursePanel1, mCourseImage1, baseDepth + 2, new Vector3(-215f + num3, y, 0f), Vector3.one, false);
			num3 += num;
			mCoursePanel2 = Util.CreateSprite("Course2", mCoursePanelParent, image);
			Util.SetSpriteInfo(mCoursePanel2, mCourseImage2, baseDepth + 2, new Vector3(-215f + num3, y, 0f), Vector3.one, false);
			num3 += num;
			mCoursePanel3 = Util.CreateSprite("Course3", mCoursePanelParent, image);
			Util.SetSpriteInfo(mCoursePanel3, mCourseImage3, baseDepth + 2, new Vector3(-215f + num3, y, 0f), Vector3.one, false);
			num3 += num;
			mCoursePanel4 = Util.CreateSprite("Course4", mCoursePanelParent, image);
			Util.SetSpriteInfo(mCoursePanel4, mCourseImage4, baseDepth + 2, new Vector3(-215f + num3, y, 0f), Vector3.one, false);
			num3 += num;
			mCoursePanel5 = Util.CreateSprite("Course5", mCoursePanelParent, image);
			Util.SetSpriteInfo(mCoursePanel5, mCourseImage5, baseDepth + 2, new Vector3(-215f + num3, y, 0f), Vector3.one, false);
			num3 += num;
		}
		float y2 = -187f;
		UISprite uISprite = Util.CreateSprite("Star", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(uISprite, "star", baseDepth + 2, new Vector3(-255f, -160f, 0f), Vector3.one, false);
		uISprite.transform.localPosition = new Vector3(40f, y2);
		num = 30f;
		mNum1 = Util.CreateSprite("num1", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum1, mStarNumImage1, baseDepth + 2, Vector3.one, Vector3.one, false);
		mNum1.transform.localPosition = new Vector3(40f + num, y2);
		num += 20f;
		mNum2 = Util.CreateSprite("num2", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum2, mStarNumImage2, baseDepth + 2, Vector3.one, Vector3.one, false);
		mNum2.transform.localPosition = new Vector3(40f + num, y2);
		num += 20f;
		UISprite uISprite2 = Util.CreateSprite("Slash", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "event_num_slash", baseDepth + 2, Vector3.one, Vector3.one, false);
		uISprite2.transform.localPosition = new Vector3(40f + num, y2);
		num += 20f;
		mNum3 = Util.CreateSprite("num3", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum3, mStarNumImage3, baseDepth + 2, Vector3.one, Vector3.one, false);
		mNum3.transform.localPosition = new Vector3(40f + num, y2);
		num += 20f;
		mNum4 = Util.CreateSprite("num4", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum4, mStarNumImage4, baseDepth + 2, Vector3.one, Vector3.one, false);
		mNum4.transform.localPosition = new Vector3(40f + num, y2);
		num += 20f;
	}

	protected override void CreateCockpitItems(int baseDepth)
	{
		base.CreateCockpitItems(baseDepth);
		GameObject parent = mAnchorB.gameObject;
		float num = 0f;
		float num2 = 170f;
		mFooterRoot = Util.CreateGameObject("FooterParent", parent);
		UISprite sprite = Util.CreateSprite("StatusR", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(-2f, num - num2, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite = Util.CreateSprite("StatusL", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(2f, num - num2, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		GameObject parent2 = mAnchorBL.gameObject;
		GameObject gameObject = mAnchorBR.gameObject;
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		mCourseNext = Util.CreateJellyImageButton("coursenext", gameObject.gameObject, image);
		Util.SetImageButtonInfo(mCourseNext, "null", "null", "null", baseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mCourseNext, mGSM, "OnCourseNext", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mCourseNext);
		mCourseNext.transform.localPosition = new Vector3(-80f, 166f);
		mCourseBack = Util.CreateJellyImageButton("courseback", parent2, image);
		Util.SetImageButtonInfo(mCourseBack, "null", "null", "null", baseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mCourseBack, mGSM, "OnCourseBack", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mCourseBack);
		mCourseBack.transform.localPosition = new Vector3(60f, 174f);
		SetLayout(Util.ScreenOrientation);
	}

	public override void SetLayout(ScreenOrientation o)
	{
		base.SetLayout(o);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = Vector3.zero;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = new Vector3(0f, -200f, 0f);
			}
			break;
		}
		if (mCoursePanelParent != null)
		{
			float x = 0f;
			if (o == ScreenOrientation.LandscapeLeft || o == ScreenOrientation.LandscapeRight)
			{
				x = -140f;
			}
			mCoursePanelParent.transform.SetLocalPositionX(x);
		}
		if (mOldEventRemainFrame != null)
		{
			float x2 = -140f;
			if (o == ScreenOrientation.LandscapeLeft || o == ScreenOrientation.LandscapeRight)
			{
				x2 = -280f;
			}
			mOldEventRemainFrame.transform.SetLocalPositionX(x2);
		}
	}

	public void StarCurrent(int MaxStar = 0, int NumCurrentStar = 0)
	{
		mStarNumMax = MaxStar;
		mStarNumCurrent = NumCurrentStar;
		mStarNumImage1 = "event_num" + mStarNumCurrent / 10;
		mStarNumImage2 = "event_num" + mStarNumCurrent % 10;
		mStarNumImage3 = "event_num" + mStarNumMax / 10;
		mStarNumImage4 = "event_num" + mStarNumMax % 10;
		mStarNumUpdate = true;
	}

	public void DisplayCourseNextButton(bool flg = false, bool a_se = false)
	{
		string text = mNextImage;
		if (flg)
		{
			mNextImage = "LC_course_next";
			mButtonSE = a_se;
		}
		else
		{
			if (mPS != null)
			{
				Object.Destroy(mPS.gameObject);
				mPS = null;
			}
			mNextImage = "null";
		}
		if (text.CompareTo(mNextImage) != 0)
		{
			mPreserveImageSetNext = true;
		}
	}

	public void DisplayCourseBackButton(bool flg = false, bool a_se = false)
	{
		string text = mBackImage;
		if (flg)
		{
			mBackImage = "LC_course_back";
			mButtonSE = a_se;
		}
		else
		{
			mBackImage = "null";
		}
		if (text.CompareTo(mBackImage) != 0)
		{
			mPreserveImageSetBack = true;
		}
	}

	public void SetCourse(int a_current, int a_playable)
	{
		mCourseUpdate = true;
		mCourseImage1 = "course03";
		mCourseImage2 = "course03";
		mCourseImage3 = "course03";
		mCourseImage4 = "course03";
		mCourseImage5 = "course01";
		switch (a_playable)
		{
		case 0:
			mCourseImage1 = "course02";
			mCourseImage2 = "course03";
			mCourseImage3 = "course03";
			mCourseImage4 = "course03";
			mCourseImage5 = "course03";
			break;
		case 1:
			mCourseImage1 = "course02";
			mCourseImage2 = "course02";
			mCourseImage3 = "course03";
			mCourseImage4 = "course03";
			mCourseImage5 = "course03";
			break;
		case 2:
			mCourseImage1 = "course02";
			mCourseImage2 = "course02";
			mCourseImage3 = "course02";
			mCourseImage4 = "course03";
			mCourseImage5 = "course03";
			break;
		case 3:
			mCourseImage1 = "course02";
			mCourseImage2 = "course02";
			mCourseImage3 = "course02";
			mCourseImage4 = "course02";
			mCourseImage5 = "course03";
			break;
		case 4:
			mCourseImage1 = "course02";
			mCourseImage2 = "course02";
			mCourseImage3 = "course02";
			mCourseImage4 = "course02";
			mCourseImage5 = "course02";
			break;
		case 5:
			mCourseImage1 = "course02";
			mCourseImage2 = "course02";
			mCourseImage3 = "course02";
			mCourseImage4 = "course02";
			mCourseImage5 = "course02";
			break;
		}
		switch (a_current)
		{
		case 0:
			mCourseImage1 = "course01";
			break;
		case 1:
			mCourseImage2 = "course01";
			break;
		case 2:
			mCourseImage3 = "course01";
			break;
		case 3:
			mCourseImage4 = "course01";
			break;
		case 4:
			mCourseImage5 = "course01";
			break;
		}
		if (mCoursePanelList != null)
		{
			for (int i = 0; i < mCoursePanelList.Count; i++)
			{
				CoursePanelInfo value = mCoursePanelList[i];
				if (i == a_current)
				{
					value.ImageName = "course01";
				}
				else if (i <= a_playable)
				{
					value.ImageName = "course02";
				}
				else
				{
					value.ImageName = "course03";
				}
				mCoursePanelList[i] = value;
			}
		}
		mCourseNum = "event_num" + (a_current + 1);
	}

	private IEnumerator UIButtonIdleAnimation(UIButton a_button)
	{
		yield return 0;
	}

	private IEnumerator ShowShopButton(UIButton a_button, string flg)
	{
		if (a_button == null || a_button.gameObject == null)
		{
			yield break;
		}
		if (flg == "null")
		{
			NGUITools.SetActive(a_button.gameObject, false);
			yield break;
		}
		NGUITools.SetActive(a_button.gameObject, true);
		bool buttonEnable = true;
		BoxCollider collider2 = a_button.GetComponent<BoxCollider>();
		if (collider2 != null && !collider2.enabled)
		{
			buttonEnable = collider2.enabled;
			collider2.enabled = true;
		}
		yield return 0;
		collider2 = a_button.GetComponent<BoxCollider>();
		if (collider2 != null && !buttonEnable)
		{
			collider2.enabled = buttonEnable;
		}
		float scale2 = 3f;
		float interval = 0.2f;
		float t = 0f;
		while (true)
		{
			t += Time.deltaTime / interval;
			scale2 = Mathf.Lerp(3f, 1f, t);
			a_button.gameObject.transform.localScale = new Vector3(scale2, scale2, 1f);
			if (scale2 <= 1f)
			{
				break;
			}
			yield return 0;
		}
	}

	private IEnumerator ShowButtonSprite(UIButton a_button, string a_image)
	{
		bool buttonEnable = true;
		BoxCollider collider2 = a_button.GetComponent<BoxCollider>();
		if (collider2 != null && !collider2.enabled)
		{
			buttonEnable = collider2.enabled;
			collider2.enabled = true;
		}
		Util.SetImageButtonGraphic(a_button, a_image, a_image, a_image);
		yield return 0;
		collider2 = a_button.GetComponent<BoxCollider>();
		if (collider2 != null && !buttonEnable)
		{
			collider2.enabled = buttonEnable;
		}
		float scale2 = 3f;
		float interval = 0.2f;
		float t = 0f;
		while (true)
		{
			t += Time.deltaTime / interval;
			scale2 = Mathf.Lerp(3f, 1f, t);
			a_button.gameObject.transform.localScale = new Vector3(scale2, scale2, 1f);
			if (scale2 <= 1f)
			{
				break;
			}
			yield return 0;
		}
		if (mButtonSE)
		{
			mGame.PlaySe("SE_OPENOPTION", -1);
			mButtonSE = false;
		}
	}

	private void UpdateHUD()
	{
		if (mCourseNext != null && mPreserveImageSetNext)
		{
			mPreserveImageSetNext = false;
			StartCoroutine(ShowButtonSprite(mCourseNext, mNextImage));
			StartCoroutine(ShowShopButton(mAllCrushButton, mNextImage));
			StartCoroutine(ShowShopButton(mugenHeartbutton, mNextImage));
			StartCoroutine(ShowShopButton(shopBagbutton, mNextImage));
		}
		if (mCourseBack != null && mPreserveImageSetBack)
		{
			mPreserveImageSetBack = false;
			StartCoroutine(ShowButtonSprite(mCourseBack, mBackImage));
			StartCoroutine(ShowShopButton(mAllCrushButton, mBackImage));
			StartCoroutine(ShowShopButton(mugenHeartbutton, mBackImage));
			StartCoroutine(ShowShopButton(shopBagbutton, mBackImage));
		}
		if (mStarNumUpdate && mNum1 != null && mNum2 != null && mNum3 != null && mNum4 != null)
		{
			mStarNumUpdate = false;
			Util.SetSpriteImageName(mNum1, mStarNumImage1, Vector3.one, false);
			Util.SetSpriteImageName(mNum2, mStarNumImage2, Vector3.one, false);
			Util.SetSpriteImageName(mNum3, mStarNumImage3, Vector3.one, false);
			Util.SetSpriteImageName(mNum4, mStarNumImage4, Vector3.one, false);
		}
		if (!mCourseUpdate)
		{
			return;
		}
		if (mCoursePanelList != null && mCourseNumPanel1 != null)
		{
			mCourseUpdate = false;
			foreach (CoursePanelInfo mCoursePanel in mCoursePanelList)
			{
				Util.SetSpriteImageName(mCoursePanel.Sprite, mCoursePanel.ImageName, Vector3.one, false);
			}
			Util.SetSpriteImageName(mCourseNumPanel1, mCourseNum, Vector3.one, false);
		}
		else if (mCoursePanel1 != null && mCoursePanel2 != null && mCoursePanel3 != null && mCoursePanel4 != null && mCoursePanel4 != null && mCourseNumPanel1 != null)
		{
			mCourseUpdate = false;
			Util.SetSpriteImageName(mCoursePanel1, mCourseImage1, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel2, mCourseImage2, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel3, mCourseImage3, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel4, mCourseImage4, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel5, mCourseImage5, Vector3.one, false);
			Util.SetSpriteImageName(mCourseNumPanel1, mCourseNum, Vector3.one, false);
		}
	}
}
