using System;

public class PlayerEventStageData : PlayerStageData, ICloneable
{
	public int EventID { get; set; }

	public short CourseID { get; set; }

	public int EventStageNo
	{
		get
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(base.StageNo, out a_main, out a_sub);
			return Def.GetStageNo(CourseID, a_main, a_sub);
		}
	}

	public PlayerEventStageData()
	{
		EventID = -1;
		CourseID = -1;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override bool Equals(object obj)
	{
		PlayerEventStageData playerEventStageData = obj as PlayerEventStageData;
		if (playerEventStageData == null)
		{
			return false;
		}
		if (playerEventStageData.Series != base.Series)
		{
			return false;
		}
		if (playerEventStageData.EventID != EventID)
		{
			return false;
		}
		if (playerEventStageData.CourseID != CourseID)
		{
			return false;
		}
		if (playerEventStageData.StageNo != base.StageNo)
		{
			return false;
		}
		if (playerEventStageData.WinCount == base.WinCount && playerEventStageData.LoseCount == base.LoseCount)
		{
			return true;
		}
		return false;
	}

	public new PlayerEventStageData Clone()
	{
		return MemberwiseClone() as PlayerEventStageData;
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		base.SerializeToBinary(data);
		data.WriteInt(EventID);
		data.WriteShort(CourseID);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		base.DeserializeFromBinary(data, reqVersion);
		EventID = data.ReadInt();
		CourseID = data.ReadShort();
	}

	public override string SerializeToJson()
	{
		return string.Empty;
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
	}
}
