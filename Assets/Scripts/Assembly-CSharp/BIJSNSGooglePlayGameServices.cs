using System;
using System.Collections;
using UnityEngine;

public class BIJSNSGooglePlayGameServices : BIJSNSAppSwitcher
{
	public class BIJSNSGooglePlayGameServicesCallback : BIJSNSEventCallback
	{
		public BIJSNSGooglePlayGameServicesCallback(BIJSNS _sns, int seqno)
			: base(_sns, seqno)
		{
		}

		public override void Register()
		{
			GooglePlayGameServiceIF.authenticationSucceededEvent = (Action<string>)Delegate.Combine(GooglePlayGameServiceIF.authenticationSucceededEvent, new Action<string>(OnLoginSucceed));
			GooglePlayGameServiceIF.authenticationFailedEvent = (Action<string>)Delegate.Combine(GooglePlayGameServiceIF.authenticationFailedEvent, new Action<string>(OnLoginFailed));
			GooglePlayGameServiceIF.loadPlayerSucceededEvent = (Action<object>)Delegate.Combine(GooglePlayGameServiceIF.loadPlayerSucceededEvent, new Action<object>(OnLoadPlayerSucceed));
			GooglePlayGameServiceIF.loadPlayerFailedEvent = (Action<string>)Delegate.Combine(GooglePlayGameServiceIF.loadPlayerFailedEvent, new Action<string>(OnLoadPlayerFailed));
			GooglePlayGameServiceIF.showAchievementsSucceededEvent = (Action<object>)Delegate.Combine(GooglePlayGameServiceIF.showAchievementsSucceededEvent, new Action<object>(OnShowAchievementsSucceeded));
			GooglePlayGameServiceIF.showAchievementsFailedEvent = (Action<string>)Delegate.Combine(GooglePlayGameServiceIF.showAchievementsFailedEvent, new Action<string>(OnShowArchivementsFailed));
			base.Register();
		}

		public override void Unregister()
		{
			GooglePlayGameServiceIF.authenticationSucceededEvent = (Action<string>)Delegate.Remove(GooglePlayGameServiceIF.authenticationSucceededEvent, new Action<string>(OnLoginSucceed));
			GooglePlayGameServiceIF.authenticationFailedEvent = (Action<string>)Delegate.Remove(GooglePlayGameServiceIF.authenticationFailedEvent, new Action<string>(OnLoginFailed));
			GooglePlayGameServiceIF.loadPlayerSucceededEvent = (Action<object>)Delegate.Remove(GooglePlayGameServiceIF.loadPlayerSucceededEvent, new Action<object>(OnLoadPlayerSucceed));
			GooglePlayGameServiceIF.loadPlayerFailedEvent = (Action<string>)Delegate.Remove(GooglePlayGameServiceIF.loadPlayerFailedEvent, new Action<string>(OnLoadPlayerFailed));
			GooglePlayGameServiceIF.showAchievementsSucceededEvent = (Action<object>)Delegate.Remove(GooglePlayGameServiceIF.showAchievementsSucceededEvent, new Action<object>(OnShowAchievementsSucceeded));
			GooglePlayGameServiceIF.showAchievementsFailedEvent = (Action<string>)Delegate.Remove(GooglePlayGameServiceIF.showAchievementsFailedEvent, new Action<string>(OnShowArchivementsFailed));
			base.Unregister();
		}

		private static string ToJsonStr(object o)
		{
			if (o == null)
			{
				return "(null)";
			}
			string empty = string.Empty;
			try
			{
				return MiniJSON.jsonEncode(o).ToString();
			}
			catch (Exception)
			{
				return o.ToString();
			}
		}

		protected virtual void OnLoginSucceed(string result)
		{
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, result));
		}

		protected virtual void OnLoginFailed(string error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}

		protected virtual void OnLoadPlayerSucceed(object result)
		{
			IBIJSNSUser detail;
			try
			{
				Hashtable hashtable = result as Hashtable;
				BIJSNSUser bIJSNSUser = new BIJSNSUser();
				if (hashtable.ContainsKey("id"))
				{
					bIJSNSUser.ID = BIJSNSEventCallback.ToSS(hashtable["id"]);
				}
				if (hashtable.ContainsKey("name"))
				{
					bIJSNSUser.Name = BIJSNSEventCallback.ToSS(hashtable["name"]);
				}
				if (hashtable.ContainsKey("gender"))
				{
					bIJSNSUser.Gender = BIJSNSEventCallback.ToSS(hashtable["gender"]);
				}
				if (string.IsNullOrEmpty(bIJSNSUser.ID))
				{
					throw new Exception("no id");
				}
				detail = bIJSNSUser;
			}
			catch (Exception ex)
			{
				Parent.ReplyEvent(SeqNo, new Response(1, "Google Play Game Services get user info response error: " + ex.ToString(), result));
				return;
			}
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, detail));
		}

		protected virtual void OnLoadPlayerFailed(string error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}

		protected virtual void OnShowAchievementsSucceeded(object result)
		{
			IBIJSNSLogout detail;
			try
			{
				Hashtable hashtable = result as Hashtable;
				BIJSNSLogout bIJSNSLogout = new BIJSNSLogout(false);
				if (hashtable.ContainsKey("logout"))
				{
					bIJSNSLogout.Logout = bool.Parse(BIJSNSEventCallback.ToSS(hashtable["logout"]));
				}
				detail = bIJSNSLogout;
			}
			catch (Exception)
			{
				detail = new BIJSNSLogout(false);
			}
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, detail));
		}

		protected virtual void OnShowArchivementsFailed(object error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}
	}

	public override SNS Kind
	{
		get
		{
			return SNS.GooglePlayGameServices;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool Init(GameObject go, MonoBehaviour mb)
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		bool result = base.Init(go, mb);
		float num = Time.realtimeSinceStartup - realtimeSinceStartup;
		return result;
	}

	public override void Term()
	{
		base.Term();
	}

	protected override Response OnBackToGameResult(Request req)
	{
		Response response = null;
		if (req.type == 2)
		{
			if (IsLogined())
			{
				return new Response(0, "Back to game", null);
			}
			return new Response(1, "Back to game", null);
		}
		return new Response(0, "Back to game", null);
	}

	public override IEventCallback CreateEventCallback(int seqno)
	{
		return new BIJSNSGooglePlayGameServicesCallback(this, seqno);
	}
}
