using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
	private const int DEPTH_INTERVAL = 20;

	private List<DialogBase> mDialogList = new List<DialogBase>();

	private Stack<DialogBase> mFocusStack = new Stack<DialogBase>();

	public int mDepth;

	private float mZ;

	private ParticleSystem mPS;

	public int GrayScreenDepth { get; private set; }

	public int SetGrayScreenDepth
	{
		get
		{
			return GrayScreenDepth;
		}
		set
		{
			GrayScreenDepth = value;
		}
	}

	public float GrayScreenZ { get; private set; }

	private void Start()
	{
		mDepth = Def.DIALOG_BASE_DEPTH;
		mZ = Def.DIALOG_BASE_Z;
		GrayScreenDepth = mDepth - 1;
		GrayScreenZ = mZ + 1f;
		GameObject gameObject = GameObject.Find("Anchor").gameObject;
		GameObject particle = GameMain.GetParticle("Particles/DialogParticle");
		particle.transform.parent = gameObject.transform;
		particle.layer = gameObject.gameObject.layer;
		particle.transform.localScale = Vector3.one;
		mPS = particle.GetComponent<ParticleSystem>();
		mPS.Stop();
	}

	private void Update()
	{
	}

	public void SetBaseDialogDepth(int _depth, float _z)
	{
		if (mDialogList.Count > 0)
		{
			BIJLog.E("Cant SetBaseDialogDepth base Depth!!");
			return;
		}
		mDepth = _depth;
		mZ = _z;
	}

	public void GetNewDialogDepth(out int depth, out float z)
	{
		depth = mDepth;
		z = mZ;
	}

	public void Resister(DialogBase dlg)
	{
		if (mFocusStack.Count > 0)
		{
			mFocusStack.Peek().OnFocusLost();
		}
		mDialogList.Add(dlg);
		mFocusStack.Push(dlg);
		dlg.OnFocus();
		GrayScreenDepth = mDepth - 1;
		GrayScreenZ = mZ + 1f;
		mDepth += 20;
		mZ -= 1f;
	}

	public void Unresister(DialogBase dlg)
	{
		if (mFocusStack.Count > 0 && mFocusStack.Peek() == dlg)
		{
			mFocusStack.Pop();
			dlg.OnFocusLost();
			mDepth -= 20;
			mZ += 1f;
			while (mFocusStack.Count > 0 && !mDialogList.Contains(mFocusStack.Peek()))
			{
				mFocusStack.Pop();
				mDepth -= 20;
				mZ += 1f;
			}
			if (mFocusStack.Count > 0)
			{
				mFocusStack.Peek().OnFocus();
			}
			GrayScreenDepth = mDepth - 20 - 1;
			GrayScreenZ = mZ + 1f + 1f;
		}
		mDialogList.Remove(dlg);
	}

	public void OpenParticle(Vector3 pos)
	{
		mPS.transform.localPosition = new Vector3(pos.x, pos.y, GrayScreenZ - 0.05f);
		mPS.Emit(32);
	}
}
