public enum SsSceneFormatVersion
{
	V300 = 196608,
	V315 = 201984,
	V320 = 204800,
	V332 = 209408
}
