using System;

public class SMAttributeTile : ISMAttributeTile, ISMTile, ICloneable
{
	protected Def.TILE_KIND mKind;

	protected Def.TILE_FORM mForm;

	protected SMAttributeTile mNext;

	public Def.TILE_KIND Kind
	{
		get
		{
			return mKind;
		}
	}

	public Def.TILE_FORM Form
	{
		get
		{
			return mForm;
		}
	}

	public ISMAttributeTile Next
	{
		get
		{
			return mNext;
		}
		set
		{
			mNext = value as SMAttributeTile;
		}
	}

	public SMAttributeTile()
	{
		mKind = Def.TILE_KIND.NONE;
		mForm = Def.TILE_FORM.NORMAL;
		mNext = null;
	}

	public SMAttributeTile(Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		mKind = kind;
		mForm = form;
		mNext = null;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public object Clone()
	{
		return MemberwiseClone() as SMAttributeTile;
	}

	public object DeepClone()
	{
		SMAttributeTile sMAttributeTile = Clone() as SMAttributeTile;
		sMAttributeTile.mForm = mForm;
		sMAttributeTile.mKind = mKind;
		if (mNext != null)
		{
			sMAttributeTile.mNext = mNext.DeepClone() as SMAttributeTile;
		}
		else
		{
			sMAttributeTile.mNext = null;
		}
		return sMAttributeTile;
	}

	public override bool Equals(object obj)
	{
		SMAttributeTile sMAttributeTile = obj as SMAttributeTile;
		if (sMAttributeTile != null)
		{
			return sMAttributeTile.Kind == Kind && sMAttributeTile.Form == Form;
		}
		return false;
	}

	public override string ToString()
	{
		return string.Format("Kind={0} Form={1}", Kind, Form);
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}

	internal void Serialize(ref BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt((int)mKind);
		data.WriteInt((int)mForm);
		if (mNext == null)
		{
			data.WriteBool(false);
			return;
		}
		data.WriteBool(true);
		mNext.Serialize(ref data, reqVersion);
	}

	internal void Deserialize(ref BIJBinaryReader data, int savedVersion)
	{
		mKind = (Def.TILE_KIND)data.ReadInt();
		mForm = (Def.TILE_FORM)data.ReadInt();
		if (data.ReadBool())
		{
			mNext = new SMAttributeTile();
			mNext.Deserialize(ref data, savedVersion);
		}
	}

	public bool IsKind(Def.TILE_KIND a_kind)
	{
		return a_kind == Kind;
	}
}
