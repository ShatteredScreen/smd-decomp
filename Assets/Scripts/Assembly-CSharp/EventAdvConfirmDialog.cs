using UnityEngine;

public class EventAdvConfirmDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private string mTitle = string.Empty;

	private string mDesc0 = string.Empty;

	private string mDesc1 = string.Empty;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public void Init(bool a_expired, bool a_isNoItem = false)
	{
		if (a_expired)
		{
			mTitle = "EvMap_GameCenter_Conf_Expired_Title";
			if (a_isNoItem)
			{
				mDesc0 = string.Empty;
				mDesc1 = "EvMap_GameCenter_Conf_Expired_Desc02";
			}
			else
			{
				mDesc0 = string.Empty;
				mDesc1 = "EvMap_GameCenter_Conf_Expired_Desc";
			}
		}
		else
		{
			mTitle = "EvMap_GameCenter_Conf_Title";
			mDesc0 = "EvMap_GameCenter_Conf_Desc00";
			mDesc1 = "EvMap_GameCenter_Conf_Desc01";
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = null;
		text = Localization.Get(mTitle);
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(text);
		Vector3 zero = Vector3.zero;
		Color color = Def.DEFAULT_MESSAGE_COLOR;
		UILabel uILabel;
		if (!string.IsNullOrEmpty(mDesc0))
		{
			text = Localization.Get(mDesc0);
			uILabel = Util.CreateLabel("Desc00", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(0f, 77f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			uILabel.spacingY = 6;
			color = Color.red;
			zero = new Vector3(0f, -16f, 0f);
		}
		else
		{
			zero = new Vector3(0f, 30f, 0f);
		}
		text = Localization.Get(mDesc1);
		uILabel = Util.CreateLabel("Desc01", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, zero, 26, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel, color);
		uILabel.spacingY = 6;
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, -110f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(130f, -110f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		mSelectItem = SELECT_ITEM.NEGATIVE;
		OnClosePushed();
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
