using System;

public class CampaignData
{
	public int iap_campaign_id { get; set; }

	public int iap_campaign_group_id { get; set; }

	public string debug_iapname { get; set; }

	public string iapname { get; set; }

	public string image { get; set; }

	public int pattern_flag { get; set; }

	public int is_force_display { get; set; }

	public int limit_count { get; set; }

	public long endtime { get; set; }

	public CampaignData()
	{
	}

	public CampaignData(int aId, int aGroupId, string aDebugIapname, string aIapname, string aImage, int aPattern, int aLimit)
	{
		iap_campaign_id = aId;
		iap_campaign_group_id = aGroupId;
		debug_iapname = aDebugIapname;
		iapname = aIapname;
		image = aImage;
		pattern_flag = aPattern;
		is_force_display = 1;
		limit_count = aLimit;
		endtime = 0L;
	}

	public string GetIapName()
	{
		if (GameMain.USE_DEBUG_TIER)
		{
			return debug_iapname;
		}
		return iapname;
	}

	public bool InTime()
	{
		DateTime dateTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(endtime, true);
		if (dateTime < dateTime2)
		{
			return true;
		}
		return false;
	}

	public string RemainTime()
	{
		string text = Localization.Get("Purchase_RemainDesc");
		string empty = string.Empty;
		string text2 = " ";
		empty = " ";
		text2 = "\u3000";
		DateTime tdt = DateTimeUtil.UnixTimeStampToDateTime(endtime + 1, true);
		DateTime fdt = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		long num = DateTimeUtil.BetweenDays(fdt, tdt);
		if (pattern_flag == 1 || num <= 0)
		{
			long num2 = DateTimeUtil.BetweenHours(fdt, tdt);
			if (num2 <= 0)
			{
				long num3 = DateTimeUtil.BetweenMinutes(fdt, tdt);
				if (num3 <= 0)
				{
					long num4 = DateTimeUtil.BetweenSeconds(fdt, tdt);
					if (num4 < 10)
					{
						empty = text2;
					}
					if (num4 < 0)
					{
						num4 = 0L;
					}
					return text + empty + string.Format(Localization.Get("Purchase_RemainSecond"), num4);
				}
				if (num3 < 10)
				{
					empty = text2;
				}
				return text + empty + string.Format(Localization.Get("Purchase_RemainMinute"), num3);
			}
			if (num2 < 10)
			{
				empty = text2;
			}
			return text + empty + string.Format(Localization.Get("Purchase_RemainHour"), num2);
		}
		if (num < 10)
		{
			empty = text2;
		}
		return text + empty + string.Format(Localization.Get("Purchase_RemainDay"), num);
	}

	public long RemainMinutes(long aSelectTime = 0)
	{
		DateTime tdt = DateTimeUtil.UnixTimeStampToDateTime(endtime, true);
		DateTime fdt = ((aSelectTime != 0L) ? DateTimeUtil.UnixTimeStampToDateTime(aSelectTime, true).AddSeconds(GameMain.mServerTimeDiff * -1.0) : DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0));
		return DateTimeUtil.BetweenMinutes(fdt, tdt);
	}
}
