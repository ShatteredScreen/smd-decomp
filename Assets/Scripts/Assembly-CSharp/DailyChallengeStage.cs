using System;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/DailyChallenge/Stage")]
public sealed class DailyChallengeStage : ScriptableObject
{
	[Serializable]
	public sealed class StageItem
	{
		[Tooltip("マスNo")]
		[SerializeField]
		private int mID;

		[Tooltip("使用ステージ - シリーズ")]
		[SerializeField]
		private Def.SERIES mSeries;

		[Tooltip("使用ステージ - 面番号")]
		[SerializeField]
		private int mMainNo;

		[Tooltip("報酬")]
		[SerializeField]
		private int mAccessoryID;

		public int ID
		{
			get
			{
				return mID;
			}
			set
			{
				mID = value;
			}
		}

		public Def.SERIES Series
		{
			get
			{
				return mSeries;
			}
			set
			{
				mSeries = value;
			}
		}

		public int MainNo
		{
			get
			{
				return mMainNo;
			}
			set
			{
				mMainNo = value;
			}
		}

		public int AccessoryID
		{
			get
			{
				return mAccessoryID;
			}
			set
			{
				mAccessoryID = value;
			}
		}
	}

	public const int COMPLETE_SHEET_INDEX = 999;

	[SerializeField]
	private StageItem[] mData;

	[SerializeField]
	private int mAccessoryID;

	[SerializeField]
	private DailyChallengeRewardListSetting mListSetting;

	public StageItem this[int index]
	{
		get
		{
			return (mData == null || index < 0 || index >= Count) ? null : mData[index];
		}
	}

	public int Count
	{
		get
		{
			return (mData != null) ? mData.Length : 0;
		}
	}

	public DailyChallengeRewardListSetting ListSetting
	{
		set
		{
			mListSetting = value;
		}
	}

	public int AccessoryID
	{
		get
		{
			return mAccessoryID;
		}
	}

	public void Set(int idx, DailyChallengeStageData data)
	{
		if (idx < 0)
		{
			return;
		}
		if (idx == 999)
		{
			if (mListSetting != null)
			{
				mAccessoryID = mListSetting.GetAccessoryID(data.RewardID);
			}
			return;
		}
		if (mData == null)
		{
			mData = new StageItem[0];
		}
		if (idx >= mData.Length)
		{
			Array.Resize(ref mData, idx);
		}
		mData[idx - 1] = new StageItem
		{
			ID = data.No,
			Series = ConvertSeriesByString(data.Series),
			MainNo = data.MainNo,
			AccessoryID = ((mListSetting != null) ? mListSetting.GetAccessoryID(data.RewardID) : 0)
		};
	}

	private Def.SERIES ConvertSeriesByString(string str)
	{
		foreach (int value in Enum.GetValues(typeof(Def.SERIES)))
		{
			if (((Def.SERIES)value).ToString() == "SM_" + str || ((Def.SERIES)value).ToString() == str)
			{
				return (Def.SERIES)value;
			}
		}
		return Def.SERIES.SM_FIRST;
	}

	public int GetNextStageID(int value)
	{
		int result = 999;
		int next = value + 1;
		if (next < 0)
		{
			return result;
		}
		StageItem stageItem = mData.FirstOrDefault((StageItem n) => n.ID == next);
		if (stageItem != null)
		{
			result = stageItem.ID;
		}
		return result;
	}
}
