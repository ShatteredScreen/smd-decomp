using System;
using System.Collections.Generic;
using UnityEngine;

public class MapStageButton : ScSpriteObject
{
	public enum STATE
	{
		STAY = 0,
		ARRIVE = 1,
		ON = 2,
		OFF = 3,
		LOCK_ON = 4,
		LOCK_OFF = 5,
		WAIT = 6
	}

	public enum FUKIDASHI_STATE
	{
		ON = 0,
		STAY = 1,
		OFF = 2,
		OFF_FINISH = 3,
		WAIT = 4
	}

	public delegate void OnPushed(GameObject go);

	public STATE mStateStatus;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.STAY);

	public FUKIDASHI_STATE mFukidashiStateStatus = FUKIDASHI_STATE.WAIT;

	protected StatusManager<FUKIDASHI_STATE> mFukidashiState = new StatusManager<FUKIDASHI_STATE>(FUKIDASHI_STATE.WAIT);

	protected Dictionary<string, string> mChangeImageDict = new Dictionary<string, string>();

	protected HashSet<string> mIgnorePartsName = new HashSet<string>();

	protected static readonly string[] numberImageName = new string[10] { "btn_stage_num00", "btn_stage_num01", "btn_stage_num02", "btn_stage_num03", "btn_stage_num04", "btn_stage_num05", "btn_stage_num06", "btn_stage_num07", "btn_stage_num08", "btn_stage_num09" };

	protected static readonly string[] numberSubImageName = new string[10] { "sb_stage_num00", "sb_stage_num01", "sb_stage_num02", "sb_stage_num03", "sb_stage_num04", "sb_stage_num05", "sb_stage_num06", "sb_stage_num07", "sb_stage_num08", "sb_stage_num09" };

	protected GameMain mGame;

	protected Camera mCamera;

	protected GameStateSMMapBase mGameState;

	public BIJImage mAtlas;

	public BIJImage mHudAtlas;

	public Player.STAGE_STATUS mMode;

	public int mStageNo;

	protected byte mStarNum;

	protected float mBaseScale;

	protected float mSizeAngle;

	protected float mMaxSizeAngle = 90f;

	protected float mButtonReactionSpeed = 480f;

	protected float mButtonReactionScaleX = 1f;

	protected float mButtonReactionScaleY = -1f;

	protected string DefaultAnimeKey = string.Empty;

	protected int DisplayNumber = -1;

	protected bool mBookedCallback;

	protected bool mCallCallback;

	protected OnPushed mCallback;

	protected bool mHasItem;

	protected ParticleOnSSAnime mOpenParticle;

	protected ParticleOnSSAnime mUnlockParticle;

	protected bool mFirstInitialized;

	protected BoxCollider mBoxCollider;

	protected UIEventTrigger mEventTrigger;

	protected Vector3 mColliderCenter = new Vector3(0f, 0f, 0f);

	protected int mColliderSize = 100;

	protected bool mUseColliderReciprocal;

	public string PlayAnimationName = string.Empty;

	private LoopAnime mLaceRotateAnime;

	protected bool mFukidashiDisplayFlg;

	protected string mFukidashiImageLayer = "image";

	protected string mFukidashiImageName = string.Empty;

	protected PartsChangedSprite mFukidashiAnime;

	protected float mFukidashiSizeAngle;

	protected string mFukidashiAnimeName = string.Empty;

	public List<CHANGE_PART_INFO[]> mChangePartsList = new List<CHANGE_PART_INFO[]>();

	public int mCurrentPartsIndex;

	private bool mIsFukidashiReflesh;

	private float mFukidashiSuspendTime;

	private bool mIsFukidashiSuspend;

	private bool mIsFukidashiLoop;

	private List<string> mReservedFukidashiList = new List<string>();

	public int[] Debug_Accessories;

	public bool[] Debug_HasAccessories;

	public string Debug_StageNo = string.Empty;

	public int StageNo
	{
		get
		{
			return mStageNo;
		}
	}

	public float DEFAULT_SCALE { get; protected set; }

	public bool UseCurrentAnime { get; protected set; }

	public void SetUseCurrentAnime(bool a_flg)
	{
		UseCurrentAnime = a_flg;
	}

	public void SetFukidashi(bool a_flg, string a_layername, string a_pngname)
	{
		string a_anime = "EFFECT_FUKIDASHI_R";
		CHANGE_PART_INFO[] a_info = null;
		if (a_pngname.CompareTo(string.Empty) != 0)
		{
			a_info = new CHANGE_PART_INFO[1]
			{
				new CHANGE_PART_INFO
				{
					partName = a_layername,
					spriteName = a_pngname
				}
			};
			Vector3 localPosition = base.gameObject.transform.localPosition;
			Vector2 vector = Util.LogScreenSize();
			if (localPosition.x > vector.x / 2f * 0.7f)
			{
				a_anime = "EFFECT_FUKIDASHI_L";
			}
		}
		SetFukidashi(a_flg, a_anime, new Vector3(0f, 0f, -1.3f), a_info, mHudAtlas);
	}

	private void OnFukidashiAnimationFinished(SsSprite a_sprite)
	{
		if (a_sprite != null)
		{
			if (mIsFukidashiLoop)
			{
				mFukidashiAnime._sprite.AnimFrame = 0f;
				mFukidashiAnime._sprite.Play();
			}
			else
			{
				mIsFukidashiSuspend = true;
				mFukidashiSuspendTime = 0f;
			}
		}
	}

	public void SetFukidashiDisplayFlg(bool a_flg)
	{
		mFukidashiDisplayFlg = a_flg;
	}

	private int FindParts(CHANGE_PART_INFO[] a_info)
	{
		int result = -1;
		if (a_info == null)
		{
			return result;
		}
		if (mChangePartsList.Count > 0)
		{
			for (int i = 0; i < mChangePartsList.Count; i++)
			{
				CHANGE_PART_INFO[] array = mChangePartsList[i];
				if (a_info.Length != array.Length)
				{
					continue;
				}
				bool[] array2 = new bool[array.Length];
				for (int j = 0; j < array2.Length; j++)
				{
					array2[j] = false;
				}
				for (int k = 0; k < array.Length; k++)
				{
					CHANGE_PART_INFO cHANGE_PART_INFO = array[k];
					for (int l = 0; l < a_info.Length; l++)
					{
						CHANGE_PART_INFO cHANGE_PART_INFO2 = a_info[l];
						if (cHANGE_PART_INFO.partName.CompareTo(cHANGE_PART_INFO2.partName) == 0 && cHANGE_PART_INFO.spriteName.CompareTo(cHANGE_PART_INFO2.spriteName) == 0)
						{
							array2[k] = true;
						}
					}
				}
				bool flag = true;
				for (int m = 0; m < array2.Length; m++)
				{
					if (!array2[m])
					{
						flag = false;
					}
				}
				if (flag)
				{
					result = i;
					break;
				}
			}
		}
		return result;
	}

	public void FukidashiPartsAdd(CHANGE_PART_INFO[] a_info)
	{
		int num = FindParts(a_info);
		if (num == -1)
		{
			mChangePartsList.Add(a_info);
		}
	}

	public void SetFukidashi(bool a_flg, string a_anime, Vector3 a_pos, CHANGE_PART_INFO[] a_info, BIJImage a_atlas = null, bool a_loop = false)
	{
		if (a_flg)
		{
			FukidashiPartsAdd(a_info);
			if (mFukidashiAnime == null)
			{
				BIJImage bIJImage = a_atlas;
				if (bIJImage == null)
				{
					bIJImage = mHudAtlas;
				}
				mCurrentPartsIndex = 0;
				mFukidashiAnimeName = a_anime;
				mFukidashiAnime = Util.CreateOneShotAnimeNoInit("Fukidashi" + mStageNo, base.gameObject);
				mFukidashiAnime.Atlas = bIJImage;
				mFukidashiAnime.ChangeAnime(mFukidashiAnimeName, mChangePartsList[mCurrentPartsIndex], true, 1, OnFukidashiAnimationFinished);
				mFukidashiAnime.transform.localPosition = a_pos;
				mFukidashiAnime.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
				mFukidashiDisplayFlg = true;
				mIsFukidashiLoop = a_loop;
			}
		}
		else if (a_info != null)
		{
			int num = FindParts(a_info);
			if (num > -1 && num < mChangePartsList.Count)
			{
				mChangePartsList.RemoveAt(num);
				mIsFukidashiReflesh = true;
			}
			if (mChangePartsList.Count == 0)
			{
				mIsFukidashiReflesh = false;
				mFukidashiState.Change(FUKIDASHI_STATE.OFF);
				mFukidashiDisplayFlg = false;
			}
		}
		else
		{
			mFukidashiState.Change(FUKIDASHI_STATE.OFF);
			mFukidashiDisplayFlg = false;
		}
	}

	public void SetReservedFukidashiFlg(string a_layername, string a_pngname)
	{
		mFukidashiDisplayFlg = true;
		mFukidashiImageName = a_pngname;
		if (mMode == Player.STAGE_STATUS.LOCK || mMode == Player.STAGE_STATUS.UNLOCK)
		{
			SetFukidashi(mFukidashiDisplayFlg, mFukidashiImageLayer, mFukidashiImageName);
		}
		else
		{
			mReservedFukidashiList.Add(a_pngname);
		}
	}

	public override void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		DEFAULT_SCALE = 0.9f;
		mCamera = mGame.mCamera;
		base.Awake();
		mBoxCollider = base.gameObject.GetComponent<BoxCollider>();
		if (mBoxCollider == null)
		{
			mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
		}
		mEventTrigger = base.gameObject.GetComponent<UIEventTrigger>();
		if (mEventTrigger == null)
		{
			mEventTrigger = base.gameObject.AddComponent<UIEventTrigger>();
		}
	}

	public override void Start()
	{
		base.Start();
		_sprite.DoUpdateBoundingBox2 = true;
		_sprite.HitIgnoreParts = mIgnorePartsName;
	}

	public override void Update()
	{
		UpdateButton();
		UpdateFukidashi();
	}

	protected virtual void UpdateButton_StateOn()
	{
		if (mState.IsChanged())
		{
			mSizeAngle = 0f;
			return;
		}
		mSizeAngle += Time.deltaTime * mButtonReactionSpeed;
		if (mSizeAngle > mMaxSizeAngle)
		{
			mSizeAngle = mMaxSizeAngle;
			if (!mGame.mTouchCnd)
			{
				mState.Change(STATE.OFF);
			}
		}
		float num = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleX) * mBaseScale;
		float num2 = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleY) * mBaseScale;
		_sprite.transform.localScale = new Vector3(num, num2, 1f);
		if (mUseColliderReciprocal)
		{
			mBoxCollider.size = new Vector3((float)mColliderSize * mBaseScale / num, (float)mColliderSize * mBaseScale / num2, mBoxCollider.size.z);
		}
	}

	protected virtual void UpdateButton_StateOff()
	{
		if (mState.IsChanged())
		{
			if (mCallCallback && mMode != Player.STAGE_STATUS.LOCK && mCallback != null)
			{
				mGame.PlaySe("SE_LEVEL_BUTTON", -1);
			}
			else
			{
				mGame.PlaySe("SE_LEVEL_LOCK", -1);
			}
			return;
		}
		mSizeAngle -= Time.deltaTime * 640f;
		if (mSizeAngle < 0f)
		{
			mSizeAngle = 0f;
			if (mMode == Player.STAGE_STATUS.UNLOCK || mMode == Player.STAGE_STATUS.CLEAR)
			{
				Vector3 localPosition = base.transform.localPosition;
				Util.CreateOneShotAnime(pos: new Vector3(localPosition.x, localPosition.y, Def.MAP_BUTTON_Z - 0.1f), name: "MapSelectAnime", key: "MAP_SELECT_STAGE", parent: base.transform.parent.gameObject, scale: Vector3.one, rotateDeg: 0f, delay: 0f);
			}
			mState.Change(STATE.WAIT);
		}
		float num = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleX) * mBaseScale;
		float num2 = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleY) * mBaseScale;
		_sprite.transform.localScale = new Vector3(num, num2, 1f);
		if (mUseColliderReciprocal)
		{
			mBoxCollider.size = new Vector3((float)mColliderSize * mBaseScale / num, (float)mColliderSize * mBaseScale / num2, mBoxCollider.size.z);
		}
	}

	protected virtual void UpdateButton_StateLockOn()
	{
		if (!mState.IsChanged())
		{
			mSizeAngle += Time.deltaTime * mButtonReactionSpeed;
			if (mSizeAngle > mMaxSizeAngle)
			{
				mSizeAngle = mMaxSizeAngle;
			}
			float num = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleX) * mBaseScale;
			float num2 = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleY) * mBaseScale;
			_sprite.transform.localScale = new Vector3(num, num2, 1f);
			if (mUseColliderReciprocal)
			{
				mBoxCollider.size = new Vector3((float)mColliderSize * mBaseScale / num, (float)mColliderSize * mBaseScale / num2, mBoxCollider.size.z);
			}
			if (!mGame.mTouchCnd)
			{
				mState.Change(STATE.LOCK_OFF);
			}
		}
	}

	protected virtual void UpdateButton_StateLockOff()
	{
		if (mState.IsChanged())
		{
			mGame.PlaySe("SE_LEVEL_LOCK", -1);
			return;
		}
		mSizeAngle -= Time.deltaTime * 640f;
		if (mSizeAngle < 0f)
		{
			mSizeAngle = 0f;
			mState.Change(STATE.STAY);
		}
		float num = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleX) * mBaseScale;
		float num2 = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.2f * mButtonReactionScaleY) * mBaseScale;
		_sprite.transform.localScale = new Vector3(num, num2, 1f);
		if (mUseColliderReciprocal)
		{
			mBoxCollider.size = new Vector3((float)mColliderSize * mBaseScale / num, (float)mColliderSize * mBaseScale / num2, mBoxCollider.size.z);
		}
	}

	protected virtual void UpdateButton()
	{
		mStateStatus = mState.GetStatus();
		switch (mStateStatus)
		{
		case STATE.STAY:
			if (mState.IsChanged())
			{
				mSizeAngle = 0f;
				mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
				_sprite.transform.localScale = new Vector3(mBaseScale, mBaseScale, 1f);
			}
			break;
		case STATE.ARRIVE:
		{
			if (mState.IsChanged())
			{
				mSizeAngle = 0f;
			}
			mSizeAngle += Time.deltaTime * 540f;
			if (mSizeAngle > 360f)
			{
				mSizeAngle = 360f;
				mState.Change(STATE.STAY);
			}
			float num = (1f + Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.1f) * mBaseScale;
			float num2 = (1f - Mathf.Sin(mSizeAngle * ((float)Math.PI / 180f)) * 0.1f) * mBaseScale;
			_sprite.transform.localScale = new Vector3(num, num2, 1f);
			if (mUseColliderReciprocal)
			{
				mBoxCollider.size = new Vector3((float)mColliderSize * mBaseScale / num, (float)mColliderSize * mBaseScale / num2, mBoxCollider.size.z);
			}
			break;
		}
		case STATE.ON:
			UpdateButton_StateOn();
			break;
		case STATE.OFF:
			UpdateButton_StateOff();
			break;
		case STATE.LOCK_ON:
			UpdateButton_StateLockOn();
			break;
		case STATE.LOCK_OFF:
			UpdateButton_StateLockOff();
			break;
		case STATE.WAIT:
			if (mState.IsChanged())
			{
				mSizeAngle = 0f;
				break;
			}
			mSizeAngle += Time.deltaTime * 480f;
			if (mSizeAngle > 90f)
			{
				mSizeAngle = 90f;
				if (mCallCallback && mMode != Player.STAGE_STATUS.LOCK && mCallback != null)
				{
					mCallCallback = false;
					mCallback(base.gameObject);
				}
				mBoxCollider = base.gameObject.GetComponent<BoxCollider>();
				if (mBoxCollider == null)
				{
					mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
				}
				mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
				mState.Change(STATE.STAY);
			}
			break;
		}
		mState.Update();
	}

	protected virtual void UpdateFukidashi()
	{
		mFukidashiStateStatus = mFukidashiState.GetStatus();
		switch (mFukidashiStateStatus)
		{
		case FUKIDASHI_STATE.OFF:
		{
			if (mFukidashiAnime == null)
			{
				mFukidashiState.Change(FUKIDASHI_STATE.OFF_FINISH);
				break;
			}
			if (mFukidashiState.IsChanged())
			{
				mFukidashiSizeAngle = 0f;
				break;
			}
			mFukidashiSizeAngle += Time.deltaTime * 480f;
			if (mFukidashiSizeAngle > 90f)
			{
				mFukidashiSizeAngle = 90f;
				mFukidashiState.Change(FUKIDASHI_STATE.OFF_FINISH);
			}
			float num = Mathf.Sin(mFukidashiSizeAngle * ((float)Math.PI / 180f));
			mFukidashiAnime.gameObject.transform.localScale = new Vector3(1f - num, 1f - num, 1f);
			break;
		}
		case FUKIDASHI_STATE.OFF_FINISH:
			if (mFukidashiAnime != null)
			{
				UnityEngine.Object.Destroy(mFukidashiAnime.gameObject);
				mFukidashiAnime = null;
				mIsFukidashiSuspend = false;
			}
			mFukidashiState.Reset(FUKIDASHI_STATE.WAIT, true);
			break;
		}
		if (mIsFukidashiSuspend && mFukidashiAnime != null)
		{
			mFukidashiSuspendTime += Time.deltaTime;
			if (mFukidashiSuspendTime > 0.5f)
			{
				if (mChangePartsList.Count > 1 || mIsFukidashiReflesh)
				{
					bool flag = false;
					int num2 = mCurrentPartsIndex;
					mCurrentPartsIndex++;
					if (mCurrentPartsIndex >= mChangePartsList.Count)
					{
						mCurrentPartsIndex = 0;
					}
					if (num2 != mCurrentPartsIndex)
					{
						flag = true;
					}
					if (mIsFukidashiReflesh)
					{
						mIsFukidashiReflesh = false;
						flag = true;
					}
					if (flag)
					{
						mFukidashiAnime.ChangeAnime(mFukidashiAnimeName, mChangePartsList[mCurrentPartsIndex], true, 1, OnFukidashiAnimationFinished);
					}
				}
				mFukidashiAnime._sprite.AnimFrame = 0f;
				mFukidashiAnime._sprite.Play();
				mIsFukidashiSuspend = false;
			}
		}
		mFukidashiState.Update();
	}

	public void ChangeMode()
	{
		Init(base.transform.localPosition, mBaseScale, mStageNo, mStarNum, mMode, mAtlas, DefaultAnimeKey, mHasItem, DisplayNumber);
	}

	public void ChangeMode(Player.STAGE_STATUS mode)
	{
		if (mode == Player.STAGE_STATUS.UNLOCK || mode == Player.STAGE_STATUS.CLEAR)
		{
			mBaseScale = DEFAULT_SCALE;
		}
		Init(base.transform.localPosition, mBaseScale, mStageNo, mStarNum, mode, mAtlas, DefaultAnimeKey, mHasItem, DisplayNumber);
	}

	public void ChangeMode(Player.STAGE_STATUS mode, byte starnum)
	{
		if (mode == Player.STAGE_STATUS.UNLOCK || mMode == Player.STAGE_STATUS.CLEAR)
		{
			mBaseScale = DEFAULT_SCALE;
		}
		Init(base.transform.localPosition, mBaseScale, mStageNo, starnum, mode, mAtlas, DefaultAnimeKey, mHasItem, DisplayNumber);
	}

	public void ChangeMode(Player.STAGE_STATUS mode, byte starnum, bool hasItem)
	{
		if (mode == Player.STAGE_STATUS.UNLOCK || mMode == Player.STAGE_STATUS.CLEAR)
		{
			mBaseScale = DEFAULT_SCALE;
		}
		mHasItem = hasItem;
		Init(base.transform.localPosition, mBaseScale, mStageNo, starnum, mode, mAtlas, DefaultAnimeKey, mHasItem, DisplayNumber);
	}

	public void ChangeMode(bool a_hasItem)
	{
		if (mHasItem != a_hasItem)
		{
			Init(base.transform.localPosition, mBaseScale, mStageNo, mStarNum, mMode, mAtlas, DefaultAnimeKey, a_hasItem, DisplayNumber);
		}
	}

	public virtual void Init(Vector3 pos, float scale, int stageNo, byte starNum, Player.STAGE_STATUS mode, BIJImage atlas, string a_animeKey, bool a_hasItem = false, int displayNo = -1)
	{
		mAtlas = atlas;
		mHudAtlas = ResourceManager.LoadImage("HUD").Image;
		mBaseScale = scale;
		mStarNum = starNum;
		mStageNo = stageNo;
		mHasItem = a_hasItem;
		DefaultAnimeKey = a_animeKey;
		DisplayNumber = displayNo;
		mChangeImageDict["btn_stage"] = "null";
		mChangeImageDict["one_01"] = "null";
		mChangeImageDict["ten_01"] = "null";
		mChangeImageDict["ten_02"] = "null";
		mChangeImageDict["hundred_01"] = "null";
		mChangeImageDict["hundred_02"] = "null";
		mChangeImageDict["hundred_03"] = "null";
		mChangeImageDict["star_01"] = "null";
		mChangeImageDict["star_02"] = "null";
		mChangeImageDict["star_03"] = "null";
		mChangeImageDict["label"] = "null";
		mChangeImageDict["sb_one_01"] = "null";
		mChangeImageDict["base_race"] = "null";
		if (mHasItem)
		{
			mChangeImageDict["base_race"] = "null";
			if (mLaceRotateAnime == null)
			{
				mLaceRotateAnime = Util.CreateLoopAnime("Lace" + stageNo, "MAP_LACE_ROTATE", base.gameObject, new Vector3(0f, -6f, 0.2f), new Vector3(1.75f, 1.75f, 1f), 0f, 0f, false, null);
			}
		}
		else
		{
			mChangeImageDict["base_race"] = "null";
			if (mLaceRotateAnime != null)
			{
				UnityEngine.Object.Destroy(mLaceRotateAnime.gameObject);
				mLaceRotateAnime = null;
			}
		}
		bool force = false;
		if (mMode != mode)
		{
			force = true;
		}
		mMode = mode;
		string key = DefaultAnimeKey;
		PlayerMapData currentMapData = mGame.mPlayer.CurrentMapData;
		if (UseCurrentAnime && mStageNo == currentMapData.OpenNoticeLevel)
		{
			force = true;
			if (mode == Player.STAGE_STATUS.UNLOCK || mode == Player.STAGE_STATUS.CLEAR)
			{
				key = DefaultAnimeKey + "_CURRENT";
			}
			switch (mode)
			{
			case Player.STAGE_STATUS.LOCK:
				mChangeImageDict["btn_stage"] = "btn_stage_untrodden";
				break;
			case Player.STAGE_STATUS.UNLOCK:
				mBaseScale = DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_traverse";
				break;
			case Player.STAGE_STATUS.CLEAR:
				mBaseScale = DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_cleared";
				key = DefaultAnimeKey + "_CLEAR";
				break;
			default:
				mChangeImageDict["base_race"] = "null";
				break;
			}
		}
		else
		{
			switch (mode)
			{
			case Player.STAGE_STATUS.LOCK:
				mChangeImageDict["btn_stage"] = "btn_stage_untrodden";
				break;
			case Player.STAGE_STATUS.UNLOCK:
				mBaseScale = DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_traverse";
				break;
			case Player.STAGE_STATUS.CLEAR:
				mBaseScale = DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_cleared";
				key = DefaultAnimeKey + "_CLEAR";
				break;
			default:
				mChangeImageDict["base_race"] = "null";
				break;
			}
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(mStageNo, out a_main, out a_sub);
		if (mode != 0)
		{
			if (DisplayNumber != -1)
			{
				a_main = DisplayNumber;
			}
			if (a_main < 10)
			{
				mChangeImageDict["one_01"] = numberImageName[a_main];
			}
			else if (a_main < 100)
			{
				mChangeImageDict["ten_01"] = numberImageName[a_main % 10];
				mChangeImageDict["ten_02"] = numberImageName[a_main / 10];
			}
			else if (a_main < 1000)
			{
				int num = a_main;
				mChangeImageDict["hundred_01"] = numberImageName[num % 10];
				num /= 10;
				mChangeImageDict["hundred_02"] = numberImageName[num % 10];
				mChangeImageDict["hundred_03"] = numberImageName[num / 10];
			}
			if (a_sub == 0 || DisplayNumber != -1)
			{
				mChangeImageDict["label"] = "null";
				mChangeImageDict["sb_one_01"] = "null";
			}
			else
			{
				mChangeImageDict["label"] = "sb_stage_label";
				mChangeImageDict["sb_one_01"] = numberSubImageName[a_sub % 10];
			}
			mChangeImageDict["star_01"] = "btn_stage_star00";
			mChangeImageDict["star_02"] = "btn_stage_star00";
			mChangeImageDict["star_03"] = "btn_stage_star00";
			if (starNum >= 1)
			{
				mChangeImageDict["star_01"] = "btn_stage_star01";
			}
			if (starNum >= 2)
			{
				mChangeImageDict["star_02"] = "btn_stage_star01";
			}
			if (starNum >= 3)
			{
				mChangeImageDict["star_03"] = "btn_stage_star01";
			}
			if (mLaceRotateAnime != null)
			{
				mLaceRotateAnime.gameObject.SetActive(true);
			}
		}
		else if (mLaceRotateAnime != null)
		{
			mLaceRotateAnime.gameObject.SetActive(false);
		}
		ChangeAnime(key, mChangeImageDict, force, 0);
		_sprite.transform.localScale = new Vector3(mBaseScale, mBaseScale, 1f);
		_sprite.transform.localPosition = pos;
		if (mode == Player.STAGE_STATUS.CLEAR || mode == Player.STAGE_STATUS.UNLOCK)
		{
			_sprite.Play();
		}
		else
		{
			_sprite.Pause();
		}
		mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
		if (!mFirstInitialized)
		{
			EventDelegate item = new EventDelegate(this, "OnButtonPushed");
			mEventTrigger.onPress.Add(item);
			EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
			mEventTrigger.onRelease.Add(item2);
			EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
			mEventTrigger.onClick.Add(item3);
		}
		if ((mode == Player.STAGE_STATUS.LOCK || mMode == Player.STAGE_STATUS.UNLOCK) && mReservedFukidashiList.Count > 0)
		{
			for (int i = 0; i < mReservedFukidashiList.Count; i++)
			{
				string a_pngname = mReservedFukidashiList[i];
				SetFukidashi(mFukidashiDisplayFlg, mFukidashiImageLayer, a_pngname);
			}
			mReservedFukidashiList.Clear();
		}
		mFirstInitialized = true;
	}

	public void SetPushedCallback(OnPushed callback)
	{
		mCallback = callback;
	}

	public void SetGameState(GameStateSMMapBase a_gamestate)
	{
		mGameState = a_gamestate;
	}

	public virtual void SetColor(Color col)
	{
		if (!(_sprite == null))
		{
			for (int i = 0; i < _sprite._colors.Length; i++)
			{
				_sprite._colors[i] = col;
			}
			_sprite._mesh.colors = _sprite._colors;
		}
	}

	public void SetEnable(bool a_flg)
	{
		if (mBoxCollider != null)
		{
			mBoxCollider.enabled = a_flg;
			mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
		}
	}

	public bool ContainsPoint(Vector3 worldPos)
	{
		STATE sTATE = STATE.STAY;
		switch (mMode)
		{
		case Player.STAGE_STATUS.UNLOCK:
		case Player.STAGE_STATUS.CLEAR:
			sTATE = STATE.ON;
			break;
		case Player.STAGE_STATUS.LOCK:
			sTATE = STATE.LOCK_ON;
			break;
		default:
			return false;
		}
		bool flag = _sprite.ContainsPoint2(worldPos, true);
		if (flag)
		{
			mState.Change(sTATE);
		}
		return flag;
	}

	public void OnButtonPushed()
	{
		if (mStateStatus == STATE.STAY)
		{
			STATE sTATE = STATE.STAY;
			switch (mMode)
			{
			case Player.STAGE_STATUS.UNLOCK:
			case Player.STAGE_STATUS.CLEAR:
				sTATE = STATE.ON;
				break;
			case Player.STAGE_STATUS.LOCK:
				sTATE = STATE.LOCK_ON;
				break;
			}
			if (sTATE != 0)
			{
				mState.Change(sTATE);
			}
		}
	}

	public void OnButtonClicked()
	{
		if (!mGame.IsTipsScreenVisibled)
		{
			if (mMode == Player.STAGE_STATUS.UNLOCK || mMode == Player.STAGE_STATUS.CLEAR)
			{
				mCallCallback = true;
			}
			else if (mMode != Player.STAGE_STATUS.LOCK)
			{
			}
		}
	}

	public void OnButtonReleased()
	{
		if (mStateStatus == STATE.ON || mStateStatus == STATE.LOCK_ON)
		{
			STATE sTATE = STATE.STAY;
			switch (mMode)
			{
			case Player.STAGE_STATUS.UNLOCK:
			case Player.STAGE_STATUS.CLEAR:
				sTATE = STATE.OFF;
				break;
			case Player.STAGE_STATUS.LOCK:
				sTATE = STATE.LOCK_OFF;
				break;
			}
			if (sTATE != 0)
			{
				mState.Change(sTATE);
			}
		}
	}

	public bool ChangeAnime(string key, Dictionary<string, string> changeParts, bool force, int playCount)
	{
		if (mAtlas == null)
		{
			return false;
		}
		PlayAnimationName = key;
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(key);
		if (resSsAnimation == null)
		{
			_sprite.Animation = null;
			return true;
		}
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		if (changeParts != null)
		{
			SsPartRes[] partList = ssAnime.PartList;
			Vector2 vector = default(Vector2);
			for (int i = 0; i < partList.Length; i++)
			{
				string value;
				if (!partList[i].IsRoot && changeParts.TryGetValue(partList[i].Name, out value))
				{
					Vector2 offset = mAtlas.GetOffset(value);
					Vector2 size = mAtlas.GetSize(value);
					float x = offset.x;
					float y = offset.y;
					float x2 = offset.x + size.x;
					float y2 = offset.y + size.y;
					partList[i].UVs = new Vector2[4]
					{
						new Vector2(x, y2),
						new Vector2(x2, y2),
						new Vector2(x2, y),
						new Vector2(x, y)
					};
					Vector4 pixelPadding = mAtlas.GetPixelPadding(value);
					vector.x = partList[i].ScaleX(0);
					vector.y = partList[i].ScaleY(0);
					size = mAtlas.GetPixelSize(value);
					size.x *= vector.x;
					size.y *= vector.y;
					offset = new Vector2(partList[i].OriginX, partList[i].OriginY);
					offset.x += pixelPadding.x;
					offset.y += pixelPadding.y;
					size.x -= pixelPadding.x + pixelPadding.z;
					size.y -= pixelPadding.y + pixelPadding.w;
					x = (0f - size.x) / 2f;
					y = (0f - size.y) / 2f;
					x2 = size.x / 2f;
					y2 = size.y / 2f;
					float z = 0f;
					partList[i].OrgVertices = new Vector3[4]
					{
						new Vector3(x, y2, z),
						new Vector3(x2, y2, z),
						new Vector3(x2, y, z),
						new Vector3(x, y, z)
					};
				}
			}
		}
		CleanupAnimation();
		_sprite.Animation = ssAnime;
		_sprite.Play();
		_sprite.PlayCount = playCount;
		return true;
	}

	public void Celebrate()
	{
	}

	public void Open()
	{
		ChangeMode(Player.STAGE_STATUS.LOCK);
		if (!(mOpenParticle != null))
		{
			mOpenParticle = Util.CreateGameObject("OpenPartilce", base.gameObject).AddComponent<ParticleOnSSAnime>();
			mOpenParticle.Init(_sprite, "Particles/StageOpenParticle", -10f, "one_01");
			mOpenParticle.SetEmitterDestroyCallback(OnOpenParticleFinished);
		}
	}

	public void Unlock(bool a_particle = true)
	{
		ChangeMode(Player.STAGE_STATUS.UNLOCK);
		if (!(mUnlockParticle != null) && a_particle)
		{
			mUnlockParticle = Util.CreateGameObject("UnlockPartilce", base.gameObject).AddComponent<ParticleOnSSAnime>();
			mUnlockParticle.Init(_sprite, "Particles/StageUnlockParticle", -10f, "one_01");
			mUnlockParticle.SetEmitterDestroyCallback(OnUnlockParticleFinished);
		}
	}

	public void Cleared()
	{
		ChangeMode(Player.STAGE_STATUS.CLEAR);
		if (!(mUnlockParticle != null))
		{
			mUnlockParticle = Util.CreateGameObject("UnlockPartilce", base.gameObject).AddComponent<ParticleOnSSAnime>();
			mUnlockParticle.Init(_sprite, "Particles/StageUnlockParticle", -10f, "one_01");
			mUnlockParticle.SetEmitterDestroyCallback(OnUnlockParticleFinished);
		}
	}

	public void OnOpenParticleFinished()
	{
		if (mOpenParticle != null)
		{
			GameMain.SafeDestroy(mOpenParticle.gameObject);
			mOpenParticle = null;
		}
	}

	public void OnUnlockParticleFinished()
	{
		if (mUnlockParticle != null)
		{
			GameMain.SafeDestroy(mUnlockParticle.gameObject);
			mUnlockParticle = null;
		}
	}

	public void Arrival()
	{
		mState.Change(STATE.ARRIVE);
	}

	public virtual void SetDebugInfo(SMMapStageSetting a_setting)
	{
		if (a_setting != null)
		{
			Debug_Accessories = new int[a_setting.BoxID.Count];
			Debug_HasAccessories = new bool[a_setting.BoxID.Count];
			for (int i = 0; i < Debug_Accessories.Length; i++)
			{
				int num = a_setting.BoxID[i];
				Debug_Accessories[i] = num;
				Debug_HasAccessories[i] = mGame.mPlayer.IsAccessoryUnlock(num);
			}
			Debug_StageNo = a_setting.mStageNo + "_" + a_setting.mSubNo;
		}
	}
}
