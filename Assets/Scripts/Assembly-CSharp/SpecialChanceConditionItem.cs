using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObject/Condition Item")]
public sealed class SpecialChanceConditionItem : ScriptableObject
{
	public enum BeforeBoosterKind
	{
		WaveBomb = 0,
		Tap2Bomb = 1,
		Rainbow = 2,
		AddScore = 3,
		SkillCharge = 4
	}

	public enum PlaySituation
	{
		Result = 0,
		LittleBitMore = 1,
		GetStar = 2
	}

	[Flags]
	public enum ContinueType
	{
		None = 0,
		MinOnly = 1,
		MaxOnly = 2
	}

	[Flags]
	public enum EffectPoint
	{
		None = 0,
		First = 1,
		R = 2,
		S = 4,
		SS = 8,
		StarS = 0x10,
		GF00 = 0x20,
		Reserve1 = 0x40,
		Reserve2 = 0x80,
		Reserve3 = 0x100,
		Reserve4 = 0x200,
		Rally = 0x400,
		Star = 0x800,
		Rally2 = 0x1000,
		Bingo = 0x2000,
		DailyChallenge = 0x4000,
		Labyrinth = 0x8000
	}

	public struct Option
	{
		public int? ContinueCount;

		public bool? IsLose;

		public bool? IsLittleBitMore;

		public int? GetStarCount;
	}

	public const int GET_STAR_PARAM_DIGIT = 1000;

	public const int GET_STAR_PARAM_OFFSET = 1;

	public static readonly Dictionary<SpecialChanceConditionSet.CHANCE_MODE, string> STR_CHANCE_NAME = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, string>
	{
		{
			SpecialChanceConditionSet.CHANCE_MODE.SPECIAL,
			"スペシャルチャンス"
		},
		{
			SpecialChanceConditionSet.CHANCE_MODE.PLUS,
			"プラスチャンス"
		},
		{
			SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP,
			"スコアアップチャンス"
		},
		{
			SpecialChanceConditionSet.CHANCE_MODE.CONTINUE,
			"コンティニューチャンス"
		},
		{
			SpecialChanceConditionSet.CHANCE_MODE.JEWEL_DOUBLE_UP,
			"ダブルアップチャンス"
		},
		{
			SpecialChanceConditionSet.CHANCE_MODE.STAR_GET,
			"スターゲットチャレンジ"
		}
	};

	[Tooltip("チャンスモード")]
	[SerializeField]
	private SpecialChanceConditionSet.CHANCE_MODE mMode;

	[Tooltip("発生箇所")]
	[SerializeField]
	private EffectPoint mEffectPoint;

	[SerializeField]
	[Tooltip("取得星数判定を使用する判定")]
	private bool mIsAcquisitionStarRange;

	[Tooltip("取得星数")]
	[SerializeField]
	private Vector2 mAcquisitionStarRange;

	[SerializeField]
	[Tooltip("所持ブースター判定を使用する判定")]
	private bool mIsPossessBooster;

	[Tooltip("所持ブースター判定")]
	[SerializeField]
	private Def.BOOSTER_KIND[] mPossessBooster;

	[Tooltip("未所持ブースター判定を使用する判定")]
	[SerializeField]
	private bool mIsNoPossessBooster;

	[Tooltip("未所持ブースター判定")]
	[SerializeField]
	private Def.BOOSTER_KIND[] mNoPossessBooster;

	[Tooltip("事前ブースター使用数判定を使用する判定")]
	[SerializeField]
	private bool mIsUseBeforeBoostersCount;

	[SerializeField]
	[Tooltip("事前ブースター使用数判定")]
	private Vector2 mUseBeforeBoostersRange;

	[SerializeField]
	[Tooltip("特定の事前ブースター使用判定を使用する判定")]
	private bool mIsUseBeforeBooster;

	[Tooltip("特定の事前ブースター使用判定")]
	[SerializeField]
	private BeforeBoosterKind[] mUseBeforeBooster;

	[SerializeField]
	[Tooltip("特定の事前ブースター未使用判定を使用する判定")]
	private bool mIsNoUseBeforeBooster;

	[Tooltip("特定の事前ブースター未使用判定")]
	[SerializeField]
	private BeforeBoosterKind[] mNoUseBeforeBooster;

	[Tooltip("コンティニュー数を使用する判定")]
	[SerializeField]
	private ContinueType mContinueType;

	[SerializeField]
	[Tooltip("コンティニュー数")]
	private Vector2 mContinueRange;

	[SerializeField]
	[Tooltip("プレイ状況を使用する判定")]
	private bool[] mIsPlaySituation;

	[Tooltip("プレイ状況")]
	[SerializeField]
	private int[] mPlaySituation;

	public SpecialChanceConditionSet.CHANCE_MODE Mode
	{
		get
		{
			return mMode;
		}
	}

	public void Set(SpecialChanceData data)
	{
		if (data == null)
		{
			return;
		}
		if (STR_CHANCE_NAME.Any((KeyValuePair<SpecialChanceConditionSet.CHANCE_MODE, string> n) => n.Value == data.ChanceType))
		{
			mMode = STR_CHANCE_NAME.FirstOrDefault((KeyValuePair<SpecialChanceConditionSet.CHANCE_MODE, string> n) => n.Value == data.ChanceType).Key;
		}
		mEffectPoint = EffectPoint.None;
		if (data.EffectPointFirst.Equals("○"))
		{
			mEffectPoint |= EffectPoint.First;
		}
		if (data.EffectPointR.Equals("○"))
		{
			mEffectPoint |= EffectPoint.R;
		}
		if (data.EffectPointS.Equals("○"))
		{
			mEffectPoint |= EffectPoint.S;
		}
		if (data.EffectPointSS.Equals("○"))
		{
			mEffectPoint |= EffectPoint.SS;
		}
		if (data.EffectPointStarS.Equals("○"))
		{
			mEffectPoint |= EffectPoint.StarS;
		}
		if (data.EffectPointSideStory.Equals("○"))
		{
			mEffectPoint |= EffectPoint.GF00;
		}
		if (data.EffectPointReserve1.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Reserve1;
		}
		if (data.EffectPointReserve2.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Reserve2;
		}
		if (data.EffectPointReserve3.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Reserve3;
		}
		if (data.EffectPointReserve4.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Reserve4;
		}
		if (data.EffectPointRally.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Rally;
		}
		if (data.EffectPointStar.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Star;
		}
		if (data.EffectPointRally2.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Rally2;
		}
		if (data.EffectPointBingo.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Bingo;
		}
		if (data.EffectPointDailyChallenge.Equals("○"))
		{
			mEffectPoint |= EffectPoint.DailyChallenge;
		}
		if (data.EffectPointLabyrinth.Equals("○"))
		{
			mEffectPoint |= EffectPoint.Labyrinth;
		}
		mIsAcquisitionStarRange = data.AcquisitionStarMin.HasValue && data.AcquisitionStarMax.HasValue;
		if (mIsAcquisitionStarRange)
		{
			mAcquisitionStarRange = new Vector2(data.AcquisitionStarMin.Value, data.AcquisitionStarMax.Value);
		}
		bool flag = data.PossessWaveBomb != null || data.PossessTapBomb2 != null || data.PossessRainbow != null || data.PossessAddScore != null || data.PossessSkillCharge != null || data.PossessSelectOne != null || data.PossessSelectCollect != null || data.PossessColorCrush != null || data.PossessBlockCrush != null || data.PossessPairMaker != null || data.PossessAllCrush != null;
		UnityAction<Def.BOOSTER_KIND, string> unityAction = delegate(Def.BOOSTER_KIND fn_kind, string fn_flag)
		{
			switch (fn_flag)
			{
			case "所持":
				mIsPossessBooster = true;
				if (mPossessBooster == null)
				{
					mPossessBooster = new Def.BOOSTER_KIND[0];
				}
				mPossessBooster = mPossessBooster.Concat(new Def.BOOSTER_KIND[1] { fn_kind }).ToArray();
				break;
			case "未所持":
				mIsNoPossessBooster = true;
				if (mNoPossessBooster == null)
				{
					mNoPossessBooster = new Def.BOOSTER_KIND[0];
				}
				mNoPossessBooster = mNoPossessBooster.Concat(new Def.BOOSTER_KIND[1] { fn_kind }).ToArray();
				break;
			}
		};
		if (flag)
		{
			unityAction(Def.BOOSTER_KIND.WAVE_BOMB, data.PossessWaveBomb);
			unityAction(Def.BOOSTER_KIND.TAP_BOMB2, data.PossessTapBomb2);
			unityAction(Def.BOOSTER_KIND.RAINBOW, data.PossessRainbow);
			unityAction(Def.BOOSTER_KIND.ADD_SCORE, data.PossessAddScore);
			unityAction(Def.BOOSTER_KIND.SKILL_CHARGE, data.PossessSkillCharge);
			unityAction(Def.BOOSTER_KIND.SELECT_ONE, data.PossessSelectOne);
			unityAction(Def.BOOSTER_KIND.SELECT_COLLECT, data.PossessSelectCollect);
			unityAction(Def.BOOSTER_KIND.COLOR_CRUSH, data.PossessColorCrush);
			unityAction(Def.BOOSTER_KIND.BLOCK_CRUSH, data.PossessBlockCrush);
			unityAction(Def.BOOSTER_KIND.PAIR_MAKER, data.PossessPairMaker);
			unityAction(Def.BOOSTER_KIND.ALL_CRUSH, data.PossessAllCrush);
		}
		else
		{
			mIsPossessBooster = false;
			mIsNoPossessBooster = false;
		}
		mIsUseBeforeBoostersCount = data.BeforeUseMin.HasValue && data.BeforeUseMax.HasValue;
		if (mIsUseBeforeBoostersCount)
		{
			mUseBeforeBoostersRange = new Vector2(data.BeforeUseMin.Value, data.BeforeUseMax.Value);
		}
		bool flag2 = data.UseBeforeWaveBomb != null || data.UseBeforeTapBomb2 != null || data.UseBeforeRainbow != null || data.UseBeforeAddScore != null || data.UseBeforeSkillCharge != null;
		UnityAction<BeforeBoosterKind, string> unityAction2 = delegate(BeforeBoosterKind fn_kind, string fn_flag)
		{
			switch (fn_flag)
			{
			case "使用":
				mIsUseBeforeBooster = true;
				if (mUseBeforeBooster == null)
				{
					mUseBeforeBooster = new BeforeBoosterKind[0];
				}
				mUseBeforeBooster = mUseBeforeBooster.Concat(new BeforeBoosterKind[1] { fn_kind }).ToArray();
				break;
			case "未使用":
				mIsNoUseBeforeBooster = true;
				if (mNoUseBeforeBooster == null)
				{
					mNoUseBeforeBooster = new BeforeBoosterKind[0];
				}
				mNoUseBeforeBooster = mNoUseBeforeBooster.Concat(new BeforeBoosterKind[1] { fn_kind }).ToArray();
				break;
			}
		};
		if (flag2)
		{
			unityAction2(BeforeBoosterKind.WaveBomb, data.UseBeforeWaveBomb);
			unityAction2(BeforeBoosterKind.Tap2Bomb, data.UseBeforeTapBomb2);
			unityAction2(BeforeBoosterKind.Rainbow, data.UseBeforeRainbow);
			unityAction2(BeforeBoosterKind.AddScore, data.UseBeforeAddScore);
			unityAction2(BeforeBoosterKind.SkillCharge, data.UseBeforeSkillCharge);
		}
		else
		{
			mIsUseBeforeBooster = false;
			mIsNoUseBeforeBooster = false;
		}
		mContinueType = ContinueType.None;
		if (data.ContinueMin.HasValue)
		{
			mContinueType |= ContinueType.MinOnly;
		}
		if (data.ContinueMax.HasValue)
		{
			mContinueType |= ContinueType.MaxOnly;
		}
		mContinueRange = default(Vector2);
		mContinueRange.x = (data.ContinueMin.HasValue ? ((!data.ContinueMax.HasValue || data.ContinueMin.Value <= data.ContinueMax.Value) ? data.ContinueMin.Value : data.ContinueMax.Value) : 0);
		mContinueRange.y = (data.ContinueMax.HasValue ? ((!data.ContinueMin.HasValue || data.ContinueMin.Value <= data.ContinueMax.Value) ? data.ContinueMax.Value : data.ContinueMin.Value) : 0);
		mIsPlaySituation = new bool[Enum.GetNames(typeof(PlaySituation)).Length];
		mPlaySituation = new int[Enum.GetNames(typeof(PlaySituation)).Length];
		foreach (int value in Enum.GetValues(typeof(PlaySituation)))
		{
			int num = -1;
			bool flag3 = true;
			switch ((PlaySituation)value)
			{
			case PlaySituation.Result:
				switch (data.PlayResult)
				{
				case "勝利":
					num = 0;
					break;
				case "敗北":
					num = 1;
					break;
				default:
					flag3 = false;
					break;
				}
				break;
			case PlaySituation.LittleBitMore:
				switch (data.PlayLittleBitMore)
				{
				case "発生":
					num = 0;
					break;
				case "未発生":
					num = 1;
					break;
				default:
					flag3 = false;
					break;
				}
				break;
			case PlaySituation.GetStar:
			{
				if (!data.PlayGetStarMin.HasValue && !data.PlayGetStarMax.HasValue)
				{
					flag3 = false;
					break;
				}
				int num2 = 0;
				if (data.PlayGetStarMin.HasValue)
				{
					num2 += data.PlayGetStarMin.Value + 1;
				}
				if (data.PlayGetStarMax.HasValue)
				{
					num2 += (data.PlayGetStarMax.Value + 1) * 1000;
				}
				if (data.PlayGetStarMin.HasValue || data.PlayGetStarMax.HasValue)
				{
					num = num2;
				}
				break;
			}
			}
			mIsPlaySituation[value] = flag3;
			mPlaySituation[value] = num;
		}
	}

	public bool IsAchieve(Option opt)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (instance == null)
		{
			return false;
		}
		SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		if (currentEventPageData == null)
		{
			if (instance != null && instance.mCurrentStage != null)
			{
				switch (instance.mCurrentStage.Series)
				{
				case Def.SERIES.SM_FIRST:
					if ((mEffectPoint & EffectPoint.First) == 0)
					{
						return false;
					}
					break;
				case Def.SERIES.SM_R:
					if ((mEffectPoint & EffectPoint.R) == 0)
					{
						return false;
					}
					break;
				case Def.SERIES.SM_S:
					if ((mEffectPoint & EffectPoint.S) == 0)
					{
						return false;
					}
					break;
				case Def.SERIES.SM_SS:
					if ((mEffectPoint & EffectPoint.SS) == 0)
					{
						return false;
					}
					break;
				case Def.SERIES.SM_STARS:
					if ((mEffectPoint & EffectPoint.StarS) == 0)
					{
						return false;
					}
					break;
				case Def.SERIES.SM_GF00:
					if ((mEffectPoint & EffectPoint.GF00) == 0)
					{
						return false;
					}
					break;
				}
			}
		}
		else if (currentEventPageData.EventSetting != null)
		{
			switch (currentEventPageData.EventSetting.EventType)
			{
			case Def.EVENT_TYPE.SM_RALLY:
				if ((mEffectPoint & EffectPoint.Rally) == 0)
				{
					return false;
				}
				break;
			case Def.EVENT_TYPE.SM_STAR:
				if ((mEffectPoint & EffectPoint.Star) == 0)
				{
					return false;
				}
				break;
			case Def.EVENT_TYPE.SM_RALLY2:
				if ((mEffectPoint & EffectPoint.Rally2) == 0)
				{
					return false;
				}
				break;
			case Def.EVENT_TYPE.SM_BINGO:
				if ((mEffectPoint & EffectPoint.Bingo) == 0)
				{
					return false;
				}
				break;
			case Def.EVENT_TYPE.SM_LABYRINTH:
				if ((mEffectPoint & EffectPoint.Labyrinth) == 0)
				{
					return false;
				}
				break;
			}
		}
		if (SingletonMonoBehaviour<GameMain>.Instance.IsDailyChallengePuzzle && (mEffectPoint & EffectPoint.DailyChallenge) == 0)
		{
			return false;
		}
		if (mIsAcquisitionStarRange)
		{
			int num = 0;
			PlayerClearData _psd;
			if (instance.mEventMode)
			{
				short a_course;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(instance.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
				int stageNo = Def.GetStageNo(a_main, a_sub);
				if (instance.mPlayer.GetStageClearData(instance.mPlayer.Data.CurrentSeries, instance.mCurrentStage.EventID, a_course, stageNo, out _psd))
				{
					num = _psd.GotStars;
				}
			}
			else if (instance.mPlayer.GetStageClearData(instance.mPlayer.Data.CurrentSeries, instance.mCurrentStage.StageNumber, out _psd))
			{
				num = _psd.GotStars;
			}
			if ((float)num < mAcquisitionStarRange.x || (float)num > mAcquisitionStarRange.y)
			{
				return false;
			}
		}
		if (mIsPossessBooster)
		{
			if (instance.mPlayer == null || instance.mPlayer.Data == null || instance.mPlayer.Data.Boosters == null || instance.mPlayer.Data.FreeBoosters == null)
			{
				return false;
			}
			Def.BOOSTER_KIND[] array = mPossessBooster;
			Def.BOOSTER_KIND kind2;
			for (int k = 0; k < array.Length; k++)
			{
				kind2 = array[k];
				if (!instance.mPlayer.Data.Boosters.Any((KeyValuePair<Def.BOOSTER_KIND, int> n) => n.Key == kind2 && n.Value > 0) && !instance.mPlayer.Data.FreeBoosters.Any((KeyValuePair<Def.BOOSTER_KIND, int> n) => n.Key == kind2 && n.Value > 0))
				{
					return false;
				}
			}
		}
		if (mIsNoPossessBooster)
		{
			if (instance.mPlayer == null || instance.mPlayer.Data == null || instance.mPlayer.Data.Boosters == null || instance.mPlayer.Data.FreeBoosters == null)
			{
				return false;
			}
			Def.BOOSTER_KIND[] array2 = mNoPossessBooster;
			Def.BOOSTER_KIND kind;
			for (int l = 0; l < array2.Length; l++)
			{
				kind = array2[l];
				if (instance.mPlayer.Data.Boosters.Any((KeyValuePair<Def.BOOSTER_KIND, int> n) => n.Key == kind && n.Value > 0))
				{
					return false;
				}
				if (instance.mPlayer.Data.FreeBoosters.Any((KeyValuePair<Def.BOOSTER_KIND, int> n) => n.Key == kind && n.Value > 0))
				{
					return false;
				}
			}
		}
		if (mIsUseBeforeBoostersCount)
		{
			if (instance.mPlayer == null || instance.mPlayer.Data == null)
			{
				return false;
			}
			if ((float)instance.mPlayer.Data.UsePlusChanceBoosterCount < mUseBeforeBoostersRange.x || (float)instance.mPlayer.Data.UsePlusChanceBoosterCount > mUseBeforeBoostersRange.y)
			{
				return false;
			}
		}
		if (mIsUseBeforeBooster)
		{
			BeforeBoosterKind[] array3 = mUseBeforeBooster;
			foreach (BeforeBoosterKind beforeBoosterKind in array3)
			{
				Def.BOOSTER_KIND j = Def.BOOSTER_KIND.NONE;
				switch (beforeBoosterKind)
				{
				case BeforeBoosterKind.WaveBomb:
					j = Def.BOOSTER_KIND.WAVE_BOMB;
					break;
				case BeforeBoosterKind.Tap2Bomb:
					j = Def.BOOSTER_KIND.TAP_BOMB2;
					break;
				case BeforeBoosterKind.Rainbow:
					j = Def.BOOSTER_KIND.RAINBOW;
					break;
				case BeforeBoosterKind.AddScore:
					j = Def.BOOSTER_KIND.ADD_SCORE;
					break;
				case BeforeBoosterKind.SkillCharge:
					j = Def.BOOSTER_KIND.SKILL_CHARGE;
					break;
				}
				if (j != 0 && !instance.mPlayer.Data.UseBeforeBooster.Any((Def.BOOSTER_KIND n) => n == j))
				{
					return false;
				}
			}
		}
		if (mIsNoUseBeforeBooster)
		{
			BeforeBoosterKind[] array4 = mNoUseBeforeBooster;
			foreach (BeforeBoosterKind beforeBoosterKind2 in array4)
			{
				Def.BOOSTER_KIND i = Def.BOOSTER_KIND.NONE;
				switch (beforeBoosterKind2)
				{
				case BeforeBoosterKind.WaveBomb:
					i = Def.BOOSTER_KIND.WAVE_BOMB;
					break;
				case BeforeBoosterKind.Tap2Bomb:
					i = Def.BOOSTER_KIND.TAP_BOMB2;
					break;
				case BeforeBoosterKind.Rainbow:
					i = Def.BOOSTER_KIND.RAINBOW;
					break;
				case BeforeBoosterKind.AddScore:
					i = Def.BOOSTER_KIND.ADD_SCORE;
					break;
				case BeforeBoosterKind.SkillCharge:
					i = Def.BOOSTER_KIND.SKILL_CHARGE;
					break;
				}
				if (i != 0 && instance.mPlayer.Data.UseBeforeBooster.Any((Def.BOOSTER_KIND n) => n == i))
				{
					return false;
				}
			}
		}
		if (mContinueType != 0)
		{
			int? continueCount = opt.ContinueCount;
			if (!continueCount.HasValue)
			{
				return false;
			}
			if ((mContinueType & ContinueType.MinOnly) != 0 && opt.ContinueCount.Value < (int)mContinueRange.x)
			{
				return false;
			}
			if ((mContinueType & ContinueType.MaxOnly) != 0 && opt.ContinueCount.Value > (int)mContinueRange.y)
			{
				return false;
			}
		}
		if (mIsPlaySituation != null)
		{
			foreach (int value in Enum.GetValues(typeof(PlaySituation)))
			{
				switch ((PlaySituation)value)
				{
				case PlaySituation.Result:
					if (mIsPlaySituation[value])
					{
						bool? isLose = opt.IsLose;
						if (!isLose.HasValue)
						{
							return false;
						}
					}
					break;
				case PlaySituation.LittleBitMore:
					if (mIsPlaySituation[value])
					{
						bool? isLittleBitMore = opt.IsLittleBitMore;
						if (!isLittleBitMore.HasValue)
						{
							return false;
						}
					}
					break;
				case PlaySituation.GetStar:
					if (mIsPlaySituation[value])
					{
						int? getStarCount = opt.GetStarCount;
						if (!getStarCount.HasValue)
						{
							return false;
						}
					}
					break;
				}
				if (value > mPlaySituation.Length)
				{
					return false;
				}
				int num3 = mPlaySituation[value];
				switch ((PlaySituation)value)
				{
				case PlaySituation.Result:
					if (num3 == 0 && opt.IsLose.Value)
					{
						return false;
					}
					if (num3 == 1 && !opt.IsLose.Value)
					{
						return false;
					}
					break;
				case PlaySituation.LittleBitMore:
					if (num3 == 0 && !opt.IsLittleBitMore.Value)
					{
						return false;
					}
					if (num3 == 1 && opt.IsLittleBitMore.Value)
					{
						return false;
					}
					break;
				case PlaySituation.GetStar:
				{
					int num4 = num3;
					if (num4 != -1)
					{
						int num5 = num3 % 1000;
						int num6 = num3 / 1000;
						if (num5 > 0 && opt.GetStarCount.Value < num5 - 1)
						{
							return false;
						}
						if (num6 > 0 && opt.GetStarCount.Value > num6 - 1)
						{
							return false;
						}
					}
					break;
				}
				}
			}
		}
		return true;
	}
}
