using System;
using System.IO;
using System.Text;

public class BIJFileBinaryWriter : BIJBinaryWriter
{
	private FileStream mStream;

	public BIJFileBinaryWriter(string path)
		: this(path, Encoding.UTF8, false, true)
	{
	}

	public BIJFileBinaryWriter(string path, Encoding encoding, bool append)
		: this(path, encoding, append, true)
	{
	}

	public BIJFileBinaryWriter(string path, Encoding encoding, bool append, bool mkdir)
	{
		mStream = null;
		Writer = null;
		try
		{
			FileMode mode = ((!append) ? FileMode.Create : FileMode.Append);
			mPath = path;
			if (mkdir)
			{
				string directoryName = System.IO.Path.GetDirectoryName(mPath);
				if (!string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
				{
					Directory.CreateDirectory(directoryName);
				}
			}
			mStream = new FileStream(mPath, mode);
			Writer = new BinaryWriter(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	public override void Close()
	{
		base.Close();
		if (mStream != null)
		{
			try
			{
				mStream.Close();
			}
			catch (Exception)
			{
			}
			mStream = null;
		}
	}
}
