using System;

[Serializable]
public class SsInheritanceParam
{
	public bool Use;

	public float Rate;

	public override string ToString()
	{
		return "Use: " + Use + ", Rate: " + Rate;
	}
}
