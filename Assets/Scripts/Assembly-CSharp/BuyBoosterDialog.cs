using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BuyBoosterDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		BUY_COMPLETE = 0,
		CANCEL = 1,
		BUY_DOLLAR = 2
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		MAINTENANCE_CHECK_START = 2,
		MAINTENANCE_CHECK_WAIT = 3,
		NETWORK_00 = 4,
		NETWORK_00_1 = 5,
		NETWORK_00_2 = 6,
		NETWORK_01 = 7,
		NETWORK_01_1 = 8,
		NETWORK_01_2 = 9,
		MAINTENANCE_CHECK_START02 = 10,
		MAINTENANCE_CHECK_WAIT02 = 11,
		NETWORK_02 = 12,
		NETWORK_02_1 = 13,
		NETWORK_02_2 = 14,
		NETWORK_02_3 = 15,
		NETWORK_03 = 16,
		AUTHERROR_WAIT = 17,
		NETWORK_02_2_1 = 18,
		NETWORK_02_2_2 = 19,
		NETWORK_02_2_3 = 20,
		NETWORK_02_2_4 = 21,
		NETWORK_02_2_5 = 22,
		NETWORK_02_2_6 = 23,
		WATCH_DIALOG_CLOSED = 24
	}

	public enum PLACE
	{
		IN_SHOP = 1,
		PRE_STAGE = 8,
		IN_STAGE = 9,
		MAP_SALE = 10,
		EVENT_SALE = 11,
		LINBANNER_MAP_SALE = 14,
		LINBANNER_EVENT_SALE = 15
	}

	public delegate void OnDialogClosed(ShopItemData ItemData, SELECT_ITEM item);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAINTENANCE_CHECK_START);

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private Def.BOOSTER_KIND mBoosterKind;

	private ShopItemData mItemData;

	private ConfirmDialog mErrorDialog;

	private TuxedoMaskDilog mTuxedMaskDialog;

	private ResLive2DAnimation mTuxedLive2D;

	private STATE mStateAfterNetworkError;

	private int mPrice;

	private int mSetNum;

	private bool mIsSale;

	private UISsSprite mUISsAnime;

	private double mStartTime;

	private double mEndTime;

	private UISprite MainBoard;

	private UILabel mLabel;

	private int mSlot;

	private ResImage mResShopImage;

	private bool mInvisibleThrough;

	private bool mInvisibleCalled;

	private bool mCheckEndSaleTime = true;

	private int? mTargetShopCount;

	private LinkBannerRequest mLinkBannerRequest;

	private LinkBannerInfo[] mLinkBanner;

	private DialogBase mWatchClosedDialog;

	private string mGuID;

	private string[] mSaleBannerList = new string[4] { "LC_sale00", "LC_sale01", "LC_sale02", "LC_sale03" };

	private bool IsInvisibleCalled
	{
		get
		{
			return mInvisibleThrough && mInvisibleCalled;
		}
	}

	public PLACE mPlace { get; set; }

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (mInvisibleThrough && !mInvisibleCalled)
			{
				mInvisibleCalled = true;
				OnBuyPushed(null);
			}
			break;
		case STATE.WATCH_DIALOG_CLOSED:
			if ((bool)mWatchClosedDialog && mWatchClosedDialog.IsClosed())
			{
				mWatchClosedDialog = null;
				mJelly.ResetReduce();
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.MAINTENANCE_CHECK_START:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT:
			if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
			{
				mState.Change(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00:
			if (!mGame.Network_UserAuth())
			{
				if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_00);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		case STATE.NETWORK_00_1:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00_2);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
				break;
			}
			Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mUserAuthErrorCode;
			if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 4, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 4, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_00_2:
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			if (!mGame.Network_GetGemCount())
			{
				if (GameMain.mGetGemCountSucceed)
				{
					if (!IsInvisibleCalled)
					{
						mState.Change(STATE.NETWORK_03);
						break;
					}
					mSelectItem = SELECT_ITEM.CANCEL;
					Close();
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_01_1);
			}
			break;
		case STATE.NETWORK_01_1:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_01_2);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			break;
		case STATE.NETWORK_01_2:
			if (!IsInvisibleCalled)
			{
				mState.Change(STATE.NETWORK_03);
				break;
			}
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
			break;
		case STATE.MAINTENANCE_CHECK_START02:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.CLOSE);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT02);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT02:
			switch (MaintenanceCheckResult())
			{
			case MAINTENANCE_RESULT.OK:
				mState.Change(STATE.NETWORK_02);
				break;
			case MAINTENANCE_RESULT.ERROR:
			case MAINTENANCE_RESULT.IN_MAINTENANCE:
				if (!GetBusy())
				{
					if (!IsInvisibleCalled)
					{
						mState.Change(STATE.MAIN);
						break;
					}
					mSelectItem = SELECT_ITEM.CANCEL;
					Close();
				}
				break;
			}
			break;
		case STATE.NETWORK_02:
		{
			bool flag3 = mGame.Network_BuyItem(mItemData.Index);
			mGame.ConnectRetryFlg = false;
			if (!flag3)
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_1);
			}
			break;
		}
		case STATE.NETWORK_02_1:
			if (!GameMain.mBuyItemCheckDone)
			{
				break;
			}
			if (GameMain.mBuyItemSucceed)
			{
				mState.Change(STATE.NETWORK_02_2);
				break;
			}
			switch (GameMain.mBuyItemErrorCode)
			{
			case Server.ErrorCode.MAINTENANCE_MODE:
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 12, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				mState.Change(STATE.WAIT);
				break;
			case Server.ErrorCode.DUPLICATE_DATA:
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mState.Change(STATE.WAIT);
				break;
			}
			default:
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_CLOSE, 12, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				mState.Change(STATE.WAIT);
				break;
			}
			break;
		case STATE.NETWORK_02_2:
		{
			int num = 0;
			int countID = 0;
			for (int i = 0; i < mGame.SegmentProfile.ShopItemList.Count; i++)
			{
				int shopItemID = mGame.SegmentProfile.ShopItemList[i].ShopItemID;
				int? num2 = mTargetShopCount;
				bool flag = !num2.HasValue || mGame.SegmentProfile.ShopItemList[i].ShopCount == mTargetShopCount.Value;
				if (shopItemID.CompareTo(mItemData.Index) == 0 && flag)
				{
					num = mGame.SegmentProfile.ShopItemList[i].ShopItemID;
					countID = mGame.SegmentProfile.ShopItemList[i].ShopCount;
				}
			}
			mGame.mPlayer.BoughtItem(mItemData.ItemDetail);
			mGame.mPlayer.AddPurchaseItem(num, countID);
			mGame.Save();
			mGame.mOptions.BuyBoostCount++;
			mGame.SaveOptions();
			for (Def.BOOSTER_KIND bOOSTER_KIND = Def.BOOSTER_KIND.WAVE_BOMB; bOOSTER_KIND < Def.BOOSTER_KIND.MAX; bOOSTER_KIND++)
			{
				if (bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART15 || bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART30 || bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART60)
				{
					if (mItemData.ItemDetail.IsMugenHeart(bOOSTER_KIND))
					{
						UserBehavior.Instance.BuyMugenHeart((short)mItemData.GemAmount);
						UserBehavior.Save();
						break;
					}
				}
				else if (mItemData.ItemDetail.IsBoosterInclude(bOOSTER_KIND))
				{
					UserBehavior.Instance.BuyBooster((short)num, (short)mItemData.GemAmount);
					UserBehavior.Save();
					break;
				}
			}
			mGame.SendGetBoostInShop(mItemData, (int)mPlace);
			bool flag2 = false;
			foreach (KeyValuePair<ShopItemDetail.ItemKind, int> bundleItem in mItemData.ItemDetail.BundleItems)
			{
				if (bundleItem.Key == ShopItemDetail.ItemKind.GROWUP)
				{
					int value = bundleItem.Value;
					int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
					int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
					int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
					int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
					int advLastStage = mGame.mPlayer.AdvLastStage;
					int advMaxStamina = mGame.mPlayer.AdvMaxStamina;
					ServerCram.ArcadeGetItem(value, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 10, 1, (int)mPlace, advLastStage, advMaxStamina);
				}
				if ((bundleItem.Key == ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET && bundleItem.Value > 0) || (bundleItem.Key == ShopItemDetail.ItemKind.PREMIUM_TICKET && bundleItem.Value > 0))
				{
					flag2 = true;
				}
			}
			if (flag2)
			{
				mState.Change(STATE.NETWORK_02_2_1);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_3);
			}
			break;
		}
		case STATE.NETWORK_02_2_1:
		{
			if (!mItemData.ItemDetail.BundleItems.ContainsKey(ShopItemDetail.ItemKind.PREMIUM_TICKET))
			{
				mState.Reset(STATE.NETWORK_02_2_3, true);
				goto case STATE.NETWORK_02_2_3;
			}
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			mGame.Network_AdvSetReward(new int[1] { mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_TICKET] }, null, mGuID, is_retry);
			mState.Change(STATE.NETWORK_02_2_2);
			break;
		}
		case STATE.NETWORK_02_2_2:
			if (!GameMain.mAdvSetRewardCheckDone)
			{
				break;
			}
			if (GameMain.mAdvSetRewardSucceed)
			{
				mGame.mPlayer.AdvSaveData.AdvGotRewardList.Add(mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_TICKET]);
				mGame.SaveAdvData();
				mGuID = null;
				mState.Change(STATE.NETWORK_02_2_3);
				break;
			}
			switch (GameMain.mBuyItemErrorCode)
			{
			case Server.ErrorCode.MAINTENANCE_MODE:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 12, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			case Server.ErrorCode.DUPLICATE_DATA:
			{
				ConfirmDialog confirmDialog2 = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog2.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog2.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mWatchClosedDialog = confirmDialog2;
				break;
			}
			default:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 18, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			}
			mJelly.Reduce(0f);
			mState.Change(STATE.WATCH_DIALOG_CLOSED);
			break;
		case STATE.NETWORK_02_2_3:
		{
			if (!mItemData.ItemDetail.BundleItems.ContainsKey(ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET))
			{
				mState.Reset(STATE.NETWORK_02_2_5, true);
				goto case STATE.NETWORK_02_2_5;
			}
			int is_retry2 = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry2 = 1;
			}
			mGame.Network_AdvSetReward(new int[1] { mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET] }, null, mGuID, is_retry2);
			mState.Change(STATE.NETWORK_02_2_4);
			break;
		}
		case STATE.NETWORK_02_2_4:
			if (!GameMain.mAdvSetRewardCheckDone)
			{
				break;
			}
			if (GameMain.mAdvSetRewardSucceed)
			{
				mGame.mPlayer.AdvSaveData.AdvGotRewardList.Add(mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET]);
				mGame.SaveAdvData();
				mGuID = null;
				mState.Change(STATE.NETWORK_02_2_5);
				break;
			}
			switch (GameMain.mBuyItemErrorCode)
			{
			case Server.ErrorCode.MAINTENANCE_MODE:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 12, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			case Server.ErrorCode.DUPLICATE_DATA:
			{
				ConfirmDialog confirmDialog3 = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog3.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog3.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mWatchClosedDialog = confirmDialog3;
				break;
			}
			default:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 20, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			}
			mJelly.Reduce(0f);
			mState.Change(STATE.WATCH_DIALOG_CLOSED);
			break;
		case STATE.NETWORK_02_2_5:
			mGame.Network_AdvGetAllItem();
			mState.Change(STATE.NETWORK_02_2_6);
			break;
		case STATE.NETWORK_02_2_6:
		{
			if (!GameMain.mAdvGetAllItemCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetAllItemSucceed)
			{
				mState.Change(STATE.NETWORK_02_3);
				break;
			}
			Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mAdvGetAllItemErrorCode;
			if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 12, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
			}
			else
			{
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 20, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
			}
			break;
		}
		case STATE.NETWORK_02_3:
			mTuxedMaskDialog = Util.CreateGameObject("TuxedDialog", base.ParentGameObject).AddComponent<TuxedoMaskDilog>();
			mTuxedMaskDialog.Init(mGame.mShopItemData[mItemData.Index]);
			mTuxedMaskDialog.SetClosedCallback(OnTuxedMaskDialogClosed);
			mState.Change(STATE.WAIT);
			break;
		case STATE.NETWORK_03:
			if (mLinkBanner == null && mGame != null && mGame.SegmentProfile != null && mGame.SegmentProfile.ShopItemList != null)
			{
				ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => n.ShopItemID == mItemData.Index);
				if (mItemData.DialogShowBanner || (shopLimitedTime != null && !string.IsNullOrEmpty(shopLimitedTime.SaleDialogBannerURL)))
				{
					mLinkBannerRequest = new LinkBannerRequest();
					LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerRequest);
					mLinkBannerRequest.SetHighPriority(true);
					StringBuilder stringBuilder = new StringBuilder();
					if (shopLimitedTime != null && !string.IsNullOrEmpty(shopLimitedTime.SaleDialogBannerURL))
					{
						stringBuilder.Append(shopLimitedTime.SaleDialogBannerURL);
					}
					else
					{
						stringBuilder.Append(string.Format("saledialog_{0}.png", mItemData.Index));
					}
					LinkBannerSetting linkBannerSetting = new LinkBannerSetting();
					linkBannerSetting.BaseURL = mGame.SegmentProfile.ShopDialogBannerBaseURL;
					linkBannerSetting.BannerList = new List<LinkBannerDetail>();
					linkBannerSetting.BannerList.Add(new LinkBannerDetail
					{
						Action = LinkBannerInfo.ACTION.NOP.ToString(),
						Param = mItemData.Index,
						FileName = stringBuilder.ToString()
					});
					LinkBannerManager.Instance.GetLinkBannerRequest(mLinkBannerRequest, linkBannerSetting, delegate(List<LinkBannerInfo> fn_banner_info)
					{
						mLinkBanner = fn_banner_info.ToArray();
					});
				}
			}
			mState.Change(STATE.MAIN);
			break;
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	private void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorAuthAbandon()
	{
		mSelectItem = SELECT_ITEM.CANCEL;
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	private void OnConnectErrorSegmentRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorSegmentAbandon()
	{
		mSelectItem = SELECT_ITEM.CANCEL;
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	public override IEnumerator BuildResource()
	{
		mResShopImage = ResourceManager.LoadImageAsync("SHOP");
		while (mResShopImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		if (mInvisibleThrough)
		{
			return;
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = mResShopImage.Image;
		BIJImage image3 = ResourceManager.LoadImage("DIALOG_BASE").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int num = (int)mBoosterKind;
		bool flag = Def.MugenHeartIdToKindData.ContainsKey(mItemData.Index);
		string text = string.Format(Localization.Get(mItemData.Name));
		if (mItemData.SaleNum == 1 || flag)
		{
			InitDialogFrame(DIALOG_SIZE.LARGE, "DIALOG_BASE", mItemData.DialogSpriteBack);
			mTitleBoard = Util.CreateSprite("TitleBoard", base.gameObject, image3);
			Util.SetSpriteInfo(mTitleBoard, mItemData.DialogSpriteTitle, mBaseDepth + 7, new Vector3(0f, 280f, 0f), Vector3.one, false);
			mTitleBoard.type = UIBasicSprite.Type.Sliced;
			mTitleBoard.SetDimensions(442, 85);
			UILabel uILabel = Util.CreateLabel("Title", mTitleBoard.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 8, new Vector3(0f, -2f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelText(uILabel, text);
			uILabel.color = Color.white;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(370, 38);
			uILabel.effectStyle = UILabel.Effect.Shadow;
			MainBoard = Util.CreateSprite("MainBoard", base.gameObject, image);
			Util.SetSpriteInfo(MainBoard, "instruction_panel13", mBaseDepth + 2, new Vector3(0f, 105f, 0f), Vector3.one, false);
			if (mItemData.ItemDetail.BundleItems.Count == 2)
			{
				MainBoard.SetDimensions(436, 210);
			}
			else
			{
				MainBoard.SetDimensions(510, 210);
			}
			MainBoard.type = UIBasicSprite.Type.Sliced;
		}
		else
		{
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle(text);
			MainBoard = Util.CreateSprite("MainBoard", base.gameObject, image);
			Util.SetSpriteInfo(MainBoard, "instruction_panel12", mBaseDepth + 2, new Vector3(0f, 105f, 0f), Vector3.one, false);
			MainBoard.SetDimensions(510, 210);
			MainBoard.type = UIBasicSprite.Type.Sliced;
		}
		UISprite sprite = Util.CreateSprite("Accessory9Top", MainBoard.gameObject, image);
		Util.SetSpriteInfo(sprite, "instruction_accessory09", mBaseDepth + 3, new Vector3(0f, 113f, 0f), Vector3.one, false);
		sprite = Util.CreateSprite("Accessory9Down", MainBoard.gameObject, image);
		Util.SetSpriteInfo(sprite, "instruction_accessory09", mBaseDepth + 3, new Vector3(0f, -113f, 0f), Vector3.one, false);
		sprite.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);
		UISprite uISprite = Util.CreateSprite("BoosterBoardDown", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, -101f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 143);
		uISprite.type = UIBasicSprite.Type.Sliced;
		if (!mItemData.IsSetBundle)
		{
			int num2 = 0;
			for (int i = 0; i < mGame.SegmentProfile.ShopItemList.Count; i++)
			{
				int shopItemID = mGame.SegmentProfile.ShopItemList[i].ShopItemID;
				int? num3 = mTargetShopCount;
				bool flag2 = !num3.HasValue || mGame.SegmentProfile.ShopItemList[i].ShopCount == mTargetShopCount.Value;
				if (shopItemID.CompareTo(mItemData.Index) == 0 && flag2)
				{
					num2 = mGame.SegmentProfile.ShopItemList[i].ShopItemLimit;
					break;
				}
			}
			if (mItemData.SaleNum == 0)
			{
				string text2 = Localization.Get(mItemData.Desc);
				if (flag)
				{
					int num4 = Def.MugenHeartLimitTimeData[Def.MugenHeartIdToKindData[mItemData.Index]];
					text2 = string.Format(text2, num4 / 60);
				}
				if (num2 > 0)
				{
					UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, 9f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 24;
					uILabel.spacingY = 5;
					string text3 = string.Format(Localization.Get("ShopItemDesc_Store_SET_Rem"), num2);
					uILabel = Util.CreateLabel("LimitDesc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text3, mBaseDepth + 3, new Vector3(0f, -50f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					uILabel.fontSize = 20;
					uILabel.color = Color.red;
				}
				else
				{
					UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, -2f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 24;
					uILabel.spacingY = 10;
					uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
					uILabel.SetDimensions(500, 130);
				}
			}
			UISprite uISprite2 = Util.CreateSprite("BoosterBoardIn", MainBoard.gameObject, image);
			Util.SetSpriteInfo(uISprite2, "instruction_panel14", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(495, 195);
			uISprite2.type = UIBasicSprite.Type.Sliced;
			text = mItemData.IconName;
			if (flag)
			{
				UISprite uISprite3 = Util.CreateSprite("BoosterIcon", MainBoard.gameObject, image2);
				Util.SetSpriteInfo(uISprite3, text, mBaseDepth + 3, new Vector3(0f, -3f, 0f), Vector3.one, false);
				uISprite3.SetDimensions(145, 145);
				if (mItemData.SaleNum == 0)
				{
					mUISsAnime = Util.CreateGameObject("SHINE", MainBoard.gameObject).AddComponent<UISsSprite>();
					mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_SHINE_BIG_01").SsAnime;
					mUISsAnime.depth = mBaseDepth + 10;
					mUISsAnime.transform.localPosition = new Vector3(180f, 108f, 0f);
					mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
					mUISsAnime.Play();
				}
			}
			else
			{
				UISprite uISprite3 = Util.CreateSprite("BoosterIcon", MainBoard.gameObject, image2);
				Util.SetSpriteInfo(uISprite3, text, mBaseDepth + 3, new Vector3(-80f, -3f, 0f), Vector3.one, false);
				uISprite3.SetDimensions(145, 145);
				text = Localization.Get("Xtext");
				UILabel uILabel = Util.CreateLabel("NumDesc", MainBoard.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(25f, -6f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel);
				uILabel.fontSize = 56;
				uILabel.color = new Color(0.7372549f, 61f / 85f, 7f / 85f, 1f);
				NumberImageString numberImageString = Util.CreateGameObject("NumDefault", MainBoard.gameObject).AddComponent<NumberImageString>();
				numberImageString.Init(2, mItemData.DefaultNum, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, false, image, false, Res.BuyNumImageString);
				numberImageString.SetPitch(80f);
				if (mItemData.DefaultNum < 10)
				{
					numberImageString.transform.localPosition = new Vector3(98f, 0f, 0f);
				}
				else
				{
					numberImageString.transform.localPosition = new Vector3(124f, 0f, 0f);
				}
			}
			if (mItemData.SaleNum == 0)
			{
				for (int j = 0; j < mGame.SegmentProfile.ShopItemList.Count; j++)
				{
					int shopItemID2 = mGame.SegmentProfile.ShopItemList[j].ShopItemID;
					int? num5 = mTargetShopCount;
					bool flag3 = !num5.HasValue || mGame.SegmentProfile.ShopItemList[j].ShopCount == mTargetShopCount.Value;
					if (shopItemID2.CompareTo(mItemData.Index) == 0 && flag3)
					{
						mStartTime = mGame.SegmentProfile.ShopItemList[j].StartTime;
						mEndTime = mGame.SegmentProfile.ShopItemList[j].EndTime;
						num2 = mGame.SegmentProfile.ShopItemList[j].ShopItemLimit;
					}
				}
				if (mEndTime != 0.0)
				{
					bool flag4 = true;
					flag4 = false;
					DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(mEndTime, flag4);
					string text4 = "00";
					if (dateTime.Minute != 0)
					{
						text4 = string.Empty + dateTime.Minute;
					}
					string text2 = string.Format(Localization.Get("ShopItemDesc_Store_SET_Head"), dateTime.Month, dateTime.Day, dateTime.Hour, text4);
					sprite = Util.CreateSprite("MessageFrameTitle", uISprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "instruction_accessory11", mBaseDepth + 3, new Vector3(0f, 67f, 0f), Vector3.one, false);
					sprite.type = UIBasicSprite.Type.Sliced;
					sprite.SetDimensions(452, 40);
					UILabel uILabel = Util.CreateLabel("RedDesc", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.red;
				}
				if (!string.IsNullOrEmpty(mItemData.SupplementKey))
				{
					sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -71f, 0f), Vector3.one, false);
					sprite.type = UIBasicSprite.Type.Sliced;
					sprite.SetDimensions(368, 48);
					UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, Localization.Get(mItemData.SupplementKey), mBaseDepth + 4, new Vector3(0f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.white;
					Util.SetLabeShrinkContent(uILabel, 350, 28);
				}
				else if (mItemData.GemDefaultAmount > mItemData.GemAmount)
				{
					sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -71f, 0f), Vector3.one, false);
					sprite.type = UIBasicSprite.Type.Sliced;
					sprite.SetDimensions(368, 48);
					string text5 = string.Format(Localization.Get("ShopItemDesc_Normal_Price"));
					string text6 = string.Format(Localization.Get("ShopItemDesc_Normal_Price_Gem"), mItemData.GemDefaultAmount);
					UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text5, mBaseDepth + 4, new Vector3(-84f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.white;
					UISprite sprite2 = Util.CreateSprite("Gem", sprite.gameObject, image);
					Util.SetSpriteInfo(sprite2, "icon_currency_jemMini", mBaseDepth + 4, new Vector3(18f, 6.5f, 0f), Vector3.one, false);
					uILabel = Util.CreateLabel("WhiteDescRight", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text6, mBaseDepth + 4, new Vector3(82f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.white;
					UISprite sprite3 = Util.CreateSprite("MessageFrameTitle", uILabel.gameObject, image);
					Util.SetSpriteInfo(sprite3, "line_cancellation2", mBaseDepth + 5, new Vector3(0f, 2f, 0f), Vector3.one, false);
					UISprite sprite4 = Util.CreateSprite("MessageFrameTitle", sprite.gameObject, image);
					Util.SetSpriteInfo(sprite4, "instruction_accessory13", mBaseDepth + 4, new Vector3(153f, 6f, 0f), Vector3.one, false);
				}
			}
			if (mItemData.SaleNum == 1)
			{
				num2 = 0;
				for (int k = 0; k < mGame.SegmentProfile.ShopItemList.Count; k++)
				{
					int shopItemID3 = mGame.SegmentProfile.ShopItemList[k].ShopItemID;
					int? num6 = mTargetShopCount;
					bool flag5 = !num6.HasValue || mGame.SegmentProfile.ShopItemList[k].ShopCount == mTargetShopCount.Value;
					if (shopItemID3.CompareTo(mItemData.Index) == 0 && flag5)
					{
						mStartTime = mGame.SegmentProfile.ShopItemList[k].StartTime;
						mEndTime = mGame.SegmentProfile.ShopItemList[k].EndTime;
						num2 = mGame.SegmentProfile.ShopItemList[k].ShopItemLimit;
					}
				}
				bool flag6 = true;
				flag6 = false;
				DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(mEndTime, flag6);
				string text2 = Localization.Get(mItemData.Desc);
				if (flag)
				{
					int num7 = Def.MugenHeartLimitTimeData[Def.MugenHeartIdToKindData[mItemData.Index]];
					text2 = string.Format(text2, num7 / 60);
				}
				if (num2 > 0)
				{
					UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, 9f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 24;
					uILabel.spacingY = 5;
					uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
					uILabel.SetDimensions(382, 84);
					string text7 = string.Format(Localization.Get("ShopItemDesc_Store_SET_Rem"), num2);
					uILabel = Util.CreateLabel("LimitDesc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text7, mBaseDepth + 3, new Vector3(0f, -38f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					uILabel.fontSize = 20;
					uILabel.color = Color.red;
				}
				else
				{
					UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, -5f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 24;
					uILabel.spacingY = 10;
				}
				if (mEndTime != 0.0)
				{
					string text8 = "00";
					if (dateTime2.Minute != 0)
					{
						text8 = string.Empty + dateTime2.Minute;
					}
					text2 = string.Format(Localization.Get("ShopItemDesc_Store_SET_Head"), dateTime2.Month, dateTime2.Day, dateTime2.Hour, text8);
					sprite = Util.CreateSprite("MessageFrameTitle", uISprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "instruction_accessory11", mBaseDepth + 3, new Vector3(0f, 67f, 0f), Vector3.one, false);
					sprite.type = UIBasicSprite.Type.Sliced;
					sprite.SetDimensions(452, 40);
					UILabel uILabel = Util.CreateLabel("RedDesc", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.red;
				}
				CreateSaleBanner(MainBoard.gameObject);
				if (!string.IsNullOrEmpty(mItemData.SupplementKey))
				{
					sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -71f, 0f), Vector3.one, false);
					sprite.type = UIBasicSprite.Type.Sliced;
					sprite.SetDimensions(368, 48);
					UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, Localization.Get(mItemData.SupplementKey), mBaseDepth + 4, new Vector3(0f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.white;
					Util.SetLabeShrinkContent(uILabel, 350, 28);
				}
				else if (mItemData.GemDefaultAmount > mItemData.GemAmount)
				{
					if (flag)
					{
						sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
						Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -76f, 0f), Vector3.one, false);
						sprite.type = UIBasicSprite.Type.Sliced;
						sprite.SetDimensions(368, 48);
						UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, Localization.Get("ShopItemDesc_Normal_Price02"), mBaseDepth + 4, new Vector3(-136f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
						uILabel.SetDimensions(76, 28);
						UISprite sprite5 = Util.CreateSprite("Gem", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite5, "icon_currency_jemMini", mBaseDepth + 4, new Vector3(-85f, 6.5f, 0f), Vector3.one, false);
						uILabel = Util.CreateLabel("WhiteDescGem", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, string.Empty + mItemData.GemDefaultAmount, mBaseDepth + 4, new Vector3(-40f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						UISprite uISprite4 = Util.CreateSprite("Correctionline", uILabel.gameObject, image);
						Util.SetSpriteInfo(uISprite4, "line_cancellation2", mBaseDepth + 5, new Vector3(2f, 2f, 0f), Vector3.one, false);
						uISprite4.width = 60;
						int num8 = 100 - (int)((float)mItemData.GemAmount / (float)mItemData.GemDefaultAmount * 100f);
						string text9 = string.Format(Localization.Get("ShopItemDesc_Normal_Price_OFF"), num8);
						uILabel = Util.CreateLabel("WhiteDescRight", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, text9, mBaseDepth + 4, new Vector3(65f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 16;
						uILabel.color = Color.yellow;
						UISprite sprite6 = Util.CreateSprite("MessageFrameTitle", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite6, "instruction_accessory13", mBaseDepth + 4, new Vector3(153f, 6f, 0f), Vector3.one, false);
					}
					else
					{
						sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
						Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -71f, 0f), Vector3.one, false);
						sprite.type = UIBasicSprite.Type.Sliced;
						sprite.SetDimensions(368, 48);
						string text10 = string.Format(Localization.Get("ShopItemDesc_Normal_Price"));
						string text11 = string.Format(Localization.Get("ShopItemDesc_Normal_Price_Gem"), mItemData.GemDefaultAmount);
						UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, text10, mBaseDepth + 4, new Vector3(-84f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						UISprite sprite7 = Util.CreateSprite("Gem", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite7, "icon_currency_jemMini", mBaseDepth + 4, new Vector3(18f, 6.5f, 0f), Vector3.one, false);
						uILabel = Util.CreateLabel("WhiteDescRight", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, text11, mBaseDepth + 4, new Vector3(82f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						UISprite sprite8 = Util.CreateSprite("MessageFrameTitle", uILabel.gameObject, image);
						Util.SetSpriteInfo(sprite8, "line_cancellation2", mBaseDepth + 5, new Vector3(0f, 2f, 0f), Vector3.one, false);
						UISprite sprite9 = Util.CreateSprite("MessageFrameTitle", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite9, "instruction_accessory13", mBaseDepth + 4, new Vector3(153f, 6f, 0f), Vector3.one, false);
					}
				}
			}
		}
		else
		{
			string text2 = Localization.Get(mItemData.Desc);
			if (flag)
			{
				int num9 = Def.MugenHeartLimitTimeData[Def.MugenHeartIdToKindData[mItemData.Index]];
				text2 = string.Format(text2, num9 / 60);
			}
			if (mItemData.SaleNum == 1)
			{
				for (int l = 0; l < mGame.SegmentProfile.ShopItemList.Count; l++)
				{
					int shopItemID4 = mGame.SegmentProfile.ShopItemList[l].ShopItemID;
					int? num10 = mTargetShopCount;
					bool flag7 = !num10.HasValue || mGame.SegmentProfile.ShopItemList[l].ShopCount == mTargetShopCount.Value;
					if (shopItemID4.CompareTo(mItemData.Index) == 0 && flag7)
					{
						mStartTime = mGame.SegmentProfile.ShopItemList[l].StartTime;
						mEndTime = mGame.SegmentProfile.ShopItemList[l].EndTime;
					}
				}
				bool flag8 = true;
				flag8 = false;
				DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(mEndTime, flag8);
				if (mEndTime != 0.0)
				{
					string text12 = "00";
					if (dateTime3.Minute != 0)
					{
						text12 = string.Empty + dateTime3.Minute;
					}
					string text13 = string.Format(Localization.Get("ShopItemDesc_Store_SET_Head"), dateTime3.Month, dateTime3.Day, dateTime3.Hour, text12);
					sprite = Util.CreateSprite("MessageFrameTitle", uISprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "instruction_accessory11", mBaseDepth + 3, new Vector3(0f, 67f, 0f), Vector3.one, false);
					sprite.type = UIBasicSprite.Type.Sliced;
					sprite.SetDimensions(452, 40);
					UILabel uILabel = Util.CreateLabel("RedDesc", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text13, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.red;
				}
				int num11 = 0;
				for (int m = 0; m < mGame.SegmentProfile.ShopItemList.Count; m++)
				{
					int shopItemID5 = mGame.SegmentProfile.ShopItemList[m].ShopItemID;
					int? num12 = mTargetShopCount;
					bool flag9 = !num12.HasValue || mGame.SegmentProfile.ShopItemList[m].ShopCount == mTargetShopCount.Value;
					if (shopItemID5.CompareTo(mItemData.Index) == 0 && flag9)
					{
						num11 = mGame.SegmentProfile.ShopItemList[m].ShopItemLimit;
					}
				}
				if (num11 > 0)
				{
					UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, 9f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 24;
					uILabel.spacingY = 5;
					string text14 = string.Format(Localization.Get("ShopItemDesc_Store_SET_Rem"), num11);
					uILabel = Util.CreateLabel("LimitDesc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text14, mBaseDepth + 3, new Vector3(0f, -34f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					uILabel.fontSize = 20;
					uILabel.color = Color.red;
				}
				else
				{
					UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, -5f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 24;
					uILabel.spacingY = 10;
				}
				CreateSaleBanner(MainBoard.gameObject);
				if (!string.IsNullOrEmpty(mItemData.SupplementKey))
				{
					sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
					Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -71f, 0f), Vector3.one, false);
					sprite.type = UIBasicSprite.Type.Sliced;
					sprite.SetDimensions(368, 48);
					UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, Localization.Get(mItemData.SupplementKey), mBaseDepth + 4, new Vector3(0f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 22;
					uILabel.color = Color.white;
					Util.SetLabeShrinkContent(uILabel, 350, 28);
				}
				else if (mItemData.GemDefaultAmount > mItemData.GemAmount)
				{
					if (flag)
					{
						sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
						Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -71f, 0f), Vector3.one, false);
						sprite.type = UIBasicSprite.Type.Sliced;
						sprite.SetDimensions(368, 48);
						UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, Localization.Get("ShopItemDesc_Normal_Price02"), mBaseDepth + 4, new Vector3(-136f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
						uILabel.SetDimensions(76, 28);
						UISprite sprite10 = Util.CreateSprite("Gem", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite10, "icon_currency_jemMini", mBaseDepth + 4, new Vector3(-85f, 6.5f, 0f), Vector3.one, false);
						uILabel = Util.CreateLabel("WhiteDescGem", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, string.Empty + mItemData.GemDefaultAmount, mBaseDepth + 4, new Vector3(-40f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						UISprite uISprite5 = Util.CreateSprite("Correctionline", uILabel.gameObject, image);
						Util.SetSpriteInfo(uISprite5, "line_cancellation2", mBaseDepth + 5, new Vector3(2f, 2f, 0f), Vector3.one, false);
						uISprite5.width = 60;
						int num13 = 100 - (int)((float)mItemData.GemAmount / (float)mItemData.GemDefaultAmount * 100f);
						string text15 = string.Format(Localization.Get("ShopItemDesc_Normal_Price_OFF"), num13);
						uILabel = Util.CreateLabel("WhiteDescRight", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, text15, mBaseDepth + 4, new Vector3(65f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 16;
						uILabel.color = Color.yellow;
						UISprite sprite11 = Util.CreateSprite("MessageFrameTitle", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite11, "instruction_accessory13", mBaseDepth + 4, new Vector3(153f, 6f, 0f), Vector3.one, false);
					}
					else if (mItemData.ItemDetail.GROWUP == 0)
					{
						sprite = Util.CreateSprite("BotFrameGem", uISprite.gameObject, image);
						Util.SetSpriteInfo(sprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(0f, -71f, 0f), Vector3.one, false);
						sprite.type = UIBasicSprite.Type.Sliced;
						sprite.SetDimensions(368, 48);
						string text16 = string.Format(Localization.Get("ShopItemDesc_Normal_Price"));
						string text17 = string.Format(Localization.Get("ShopItemDesc_Normal_Price_Gem"), mItemData.GemDefaultAmount);
						UILabel uILabel = Util.CreateLabel("WhiteDescLeft", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, text16, mBaseDepth + 4, new Vector3(-84f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						UISprite sprite12 = Util.CreateSprite("Gem", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite12, "icon_currency_jemMini", mBaseDepth + 4, new Vector3(18f, 6.5f, 0f), Vector3.one, false);
						uILabel = Util.CreateLabel("WhiteDescRight", sprite.gameObject, atlasFont);
						Util.SetLabelInfo(uILabel, text17, mBaseDepth + 4, new Vector3(82f, 4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
						DialogBase.SetDialogLabelEffect2(uILabel);
						uILabel.fontSize = 22;
						uILabel.color = Color.white;
						UISprite sprite13 = Util.CreateSprite("MessageFrameTitle", uILabel.gameObject, image);
						Util.SetSpriteInfo(sprite13, "line_cancellation2", mBaseDepth + 5, new Vector3(0f, 2f, 0f), Vector3.one, false);
						UISprite sprite14 = Util.CreateSprite("MessageFrameTitle", sprite.gameObject, image);
						Util.SetSpriteInfo(sprite14, "instruction_accessory13", mBaseDepth + 4, new Vector3(153f, 6f, 0f), Vector3.one, false);
					}
				}
			}
			else
			{
				UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, -2f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel);
				uILabel.fontSize = 26;
				uILabel.spacingY = 10;
			}
			ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => n.ShopItemID == mItemData.Index);
			if (!mItemData.DialogShowBanner && (shopLimitedTime == null || string.IsNullOrEmpty(shopLimitedTime.SaleDialogBannerURL)))
			{
				int num14 = 0;
				foreach (KeyValuePair<ShopItemDetail.ItemKind, int> bundleItem in mItemData.ItemDetail.BundleItems)
				{
					Vector3 vector = new Vector3(0f, 0f, 0f);
					Vector3 vector2 = new Vector3(0f, 0f, 0f);
					int num15 = num14 / 2;
					float num16 = num14 % 2;
					if (mItemData.ItemDetail.BundleItems.Count == 2)
					{
						num16 = 0.5f;
						if (num14 == 1)
						{
							num15 = 1;
						}
					}
					if (mItemData.ItemDetail.BundleItems.Count == 3 && num14 == 2)
					{
						num16 = 0.5f;
					}
					UISprite uISprite6 = Util.CreateSprite("BoosterBoardIn", MainBoard.gameObject, image);
					Util.SetSpriteInfo(uISprite6, "instruction_panel14", mBaseDepth + 3, new Vector3(-126f + 252f * num16, 51 - 102 * num15, 0f), Vector3.one, false);
					uISprite6.SetDimensions(250, 100);
					uISprite6.type = UIBasicSprite.Type.Sliced;
					string empty = string.Empty;
					BoosterData boosterData = mGame.mBoosterData[(int)bundleItem.Key];
					empty = boosterData.iconBuy;
					UISprite uISprite7 = Util.CreateSprite("ItemIcon", uISprite6.gameObject, image);
					Util.SetSpriteInfo(uISprite7, empty, mBaseDepth + 3, new Vector3(-63f, 0f, 0f), Vector3.one, false);
					uISprite7.SetDimensions(92, 92);
					text = Localization.Get("Xtext");
					UILabel uILabel = Util.CreateLabel("NumDesc", uISprite6.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(5f, -5.4f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 45;
					uILabel.color = new Color(0.7372549f, 61f / 85f, 7f / 85f, 1f);
					NumberImageString numberImageString2 = Util.CreateGameObject("NumDefault", uISprite6.gameObject).AddComponent<NumberImageString>();
					numberImageString2.Init(2, bundleItem.Value, mBaseDepth + 3, 0.75f, UIWidget.Pivot.Center, false, image, false, Res.BuyNumImageString);
					numberImageString2.SetKerning(-5f);
					numberImageString2.transform.localPosition = new Vector3(67f, 0f, 0f);
					numberImageString2.transform.SetLocalScale(0.8f, 0.8f, 1f);
					num14++;
				}
			}
		}
		GameObject gameObject = null;
		if (flag && mGame.mPlayer.Data.mMugenHeart != 0)
		{
			UISprite uISprite8 = Util.CreateSprite("NoBuyButton", base.gameObject, image);
			Util.SetSpriteInfo(uISprite8, "LC_button_buy", mBaseDepth + 3, new Vector3(0f, -233f, 0f), Vector3.one, false);
			uISprite8.color = Color.gray;
			gameObject = uISprite8.gameObject;
		}
		else
		{
			UIButton uIButton = Util.CreateJellyImageButton("BuyButton", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "LC_button_buy", "LC_button_buy", "LC_button_buy", mBaseDepth + 3, new Vector3(0f, -233f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnBuyPushed", UIButtonMessage.Trigger.OnClick);
			gameObject = uIButton.gameObject;
		}
		if (gameObject != null)
		{
			mLabel = Util.CreateLabel("Price", gameObject, atlasFont);
			Util.SetLabelInfo(mLabel, string.Empty + mItemData.GemAmount, mBaseDepth + 4, new Vector3(71f, -5f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(mLabel);
			if (mItemData.GemAmount > mGame.mPlayer.Dollar)
			{
				mLabel.color = new Color(1f, 0.1f, 0.1f);
			}
		}
		CreateGemWindow();
		CreateCancelButton("OnCancelPushed");
	}

	private void OnTuxedMaskDialogClosed(TuxedoMaskDilog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
		mTuxedMaskDialog = null;
		Close();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	private void OnDuplicateDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mGame.GetGemCountFreqFlag = false;
		mState.Change(STATE.NETWORK_00);
	}

	private void OnConnectErrorBuyItemRetry(int a_state)
	{
		mGame.ConnectRetryFlg = true;
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorBuyItemAbandon()
	{
		mGame.ConnectRetryFlg = false;
		mSelectItem = SELECT_ITEM.CANCEL;
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	private void ShowNetworkErrorDialog(STATE aNextState)
	{
		mStateAfterNetworkError = aNextState;
		mState.Reset(STATE.WAIT, true);
		mErrorDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mErrorDialog.SetClosedCallback(OnNetworkErrorDialogClosed);
	}

	private void OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		if (!IsInvisibleCalled || mStateAfterNetworkError != 0)
		{
			mState.Change(mStateAfterNetworkError);
			return;
		}
		mSelectItem = SELECT_ITEM.CANCEL;
		Close();
	}

	public void Init(ShopItemData a_item, bool invisible_through = false, bool is_check_end_sale_time = true, int? target_shop_count = null)
	{
		mItemData = a_item;
		mPrice = a_item.GemAmount;
		mInvisibleThrough = invisible_through;
		mCheckEndSaleTime = is_check_end_sale_time;
		mTargetShopCount = target_shop_count;
	}

	public override void OnOpenFinished()
	{
		ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => n.ShopItemID == mItemData.Index);
		if (mItemData.DialogShowBanner || (shopLimitedTime != null && !string.IsNullOrEmpty(shopLimitedTime.SaleDialogBannerURL)))
		{
			GameObject go = Util.CreateGameObject("Banner", MainBoard.gameObject);
			StartCoroutine(Routine_OnAttachBanner(go, mItemData.Index));
		}
	}

	public override void OnCloseFinished()
	{
		if (mLinkBannerRequest != null)
		{
			LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerRequest);
		}
		mLinkBannerRequest = null;
		mLinkBanner = null;
		if (mCallback != null)
		{
			mCallback(mItemData, mSelectItem);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnBuyPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		if (mCheckEndSaleTime)
		{
			DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(mEndTime, true);
			DateTime dateTime2 = DateTimeUtil.Now();
			if (mEndTime != 0.0 && dateTime < dateTime2)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("ShopItemTitle_BuySale_Timeout"), Localization.Get("ShopItemDesc_BuySale_Timeout"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
				confirmDialog.SetClosedCallback(OnSaleTimeOutClosed);
				mState.Change(STATE.WAIT);
				return;
			}
		}
		if (mPrice <= mGame.mPlayer.Dollar)
		{
			GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
			gemUseConfirmDialog.Init(mPrice, 10);
			gemUseConfirmDialog.SetClosedCallback(OnGemUseConfirmClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			ConfirmDialog confirmDialog2 = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			confirmDialog2.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			confirmDialog2.SetClosedCallback(OnConfirmClosed);
			mState.Change(STATE.WAIT);
		}
	}

	public void OnSaleTimeOutClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mState.Change(STATE.MAIN);
		Close();
	}

	public void OnGemUseConfirmClosed(GemUseConfirmDialog.SELECT_ITEM item, int aValue)
	{
		if (item == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Change((STATE)aValue);
			return;
		}
		if (!IsInvisibleCalled)
		{
			mState.Change(STATE.MAIN);
			return;
		}
		mSelectItem = SELECT_ITEM.CANCEL;
		Close();
	}

	public void OnConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
			purchaseDialog.SetBaseDepth(mBaseDepth + 10);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(PurchaseAuthError);
		}
		else if (!IsInvisibleCalled)
		{
			mState.Change(STATE.MAIN);
		}
		else
		{
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	private void PurchaseAuthError()
	{
		mState.Change(STATE.AUTHERROR_WAIT);
	}

	public void OnPurchaseDialogClosed()
	{
		mGemDisplayNum = mGame.mPlayer.Dollar;
		if (mGemNumLabel != null)
		{
			mGemNumLabel.text = string.Empty + mGemDisplayNum;
		}
		if (mLabel != null)
		{
			if (mItemData.GemAmount > mGame.mPlayer.Dollar)
			{
				mLabel.color = new Color(1f, 0.1f, 0.1f);
			}
			else
			{
				DialogBase.SetDialogLabelEffect2(mLabel);
			}
		}
		if (!IsInvisibleCalled)
		{
			mState.Change(STATE.MAIN);
		}
		else
		{
			Close();
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAINTENANCE_CHECK_WAIT))
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}

	private void CreateSaleBanner(GameObject a_root)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		string imageName = mSaleBannerList[0];
		if (mItemData != null && mGame.mSegmentProfile != null)
		{
			ShopLimitedTime shopLimitedTime = mGame.mSegmentProfile.GetShopLimitedTime(mItemData.Index);
			if (shopLimitedTime != null && shopLimitedTime.SaleBannerID < mSaleBannerList.Length)
			{
				imageName = mSaleBannerList[shopLimitedTime.SaleBannerID];
			}
		}
		UISprite uISprite = Util.CreateSprite("Banner", a_root, image);
		Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 5, new Vector3(180f, 108f, 0f), Vector3.one, false);
		mUISsAnime = Util.CreateGameObject("SHINE", uISprite.gameObject).AddComponent<UISsSprite>();
		mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_SHINE_ON_LABEL").SsAnime;
		mUISsAnime.depth = mBaseDepth + 10;
		mUISsAnime.Play();
	}

	private IEnumerator Routine_OnAttachBanner(GameObject go, int param)
	{
		if (!go)
		{
			yield break;
		}
		UISprite bg2 = Util.CreateSprite("BackGround", go, ResourceManager.LoadImage("HUD").Image);
		Util.SetSpriteInfo(bg2, "bg00w", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		Util.SetSpriteSize(bg2, 484, 180);
		bg2.color = Color.gray;
		UISsSprite loading2 = Util.CreateUISsSprite("LoadingAnime", go, "ADV_FIGURE_LOADING", Vector3.zero, Vector3.one, mBaseDepth + 4);
		while (mLinkBanner == null)
		{
			yield return null;
		}
		int param2 = default(int);
		LinkBannerInfo info = mLinkBanner.FirstOrDefault((LinkBannerInfo n) => n.param == param2);
		while (info == null)
		{
			yield return null;
		}
		if (info.banner != null)
		{
			UITexture texture = go.AddComponent<UITexture>();
			texture.depth = mBaseDepth + 4;
			texture.mainTexture = info.banner;
			texture.SetDimensions(info.banner.width, info.banner.height);
			if ((bool)loading2)
			{
				GameMain.SafeDestroy(loading2.gameObject);
			}
			if ((bool)bg2)
			{
				GameMain.SafeDestroy(bg2.gameObject);
			}
			loading2 = null;
			bg2 = null;
		}
		go.transform.SubLocalPositionY(5f);
	}
}
