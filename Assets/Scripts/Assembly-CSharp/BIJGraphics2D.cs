using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public sealed class BIJGraphics2D
{
	private static Texture2D mFastTextureDst;

	private static Texture2D mFastTextureSrc;

	private static Color32[] mFastTextureDstPix;

	private static Color32[] mFastTextureSrcPix;

	private static GCHandle mFastTextureDstHandle;

	private static GCHandle mFastTextureSrcHandle;

	private BIJGraphics2D()
	{
	}

	public static int ColorToARGB(Color color)
	{
		byte b = (byte)(255f * color.r);
		byte b2 = (byte)(255f * color.g);
		byte b3 = (byte)(255f * color.b);
		byte b4 = (byte)(255f * color.a);
		return (b << 24) | (b2 << 16) | (b3 << 8) | b4;
	}

	public static Color ARGBToColor(int argb)
	{
		float a = (float)((byte)(argb >> 24) & 0xFF) / 255f;
		float r = (float)((byte)(argb >> 16) & 0xFF) / 255f;
		float g = (float)((byte)(argb >> 8) & 0xFF) / 255f;
		float b = (float)((byte)argb & 0xFF) / 255f;
		return new Color(r, g, b, a);
	}

	public static Texture2D LoadTextureFromResource(string name)
	{
		return LoadTextureFromResource(name, TextureFormat.ARGB32, false);
	}

	public static Texture2D LoadTextureFromResource(string name, TextureFormat format, bool mipmap)
	{
		TextAsset textAsset = null;
		Texture2D texture2D = null;
		if (!string.IsNullOrEmpty(name))
		{
			try
			{
				texture2D = new Texture2D(0, 0, format, mipmap);
				textAsset = Resources.Load(name) as TextAsset;
				texture2D.LoadImage(textAsset.bytes);
			}
			catch (Exception)
			{
				texture2D = null;
			}
			finally
			{
				if (textAsset != null)
				{
					Resources.UnloadAsset(textAsset);
				}
				textAsset = null;
			}
		}
		return texture2D;
	}

	public static Texture2D LoadTextureFromStream(string name)
	{
		return LoadTextureFromStream(name, TextureFormat.ARGB32, false, 0f);
	}

	public static Texture2D LoadTextureFromStream(string name, TextureFormat format, bool mipmap, float timeout)
	{
		Texture2D texture2D = null;
		if (!string.IsNullOrEmpty(name))
		{
			string url = "jar:file://" + Application.dataPath + "!/assets/" + name;
			try
			{
				using (WWW wWW = new WWW(url))
				{
					float realtimeSinceStartup = Time.realtimeSinceStartup;
					while (!wWW.isDone)
					{
						if (timeout > 0f && Time.realtimeSinceStartup - realtimeSinceStartup > timeout)
						{
							return null;
						}
					}
					texture2D = new Texture2D(0, 0, format, mipmap);
					texture2D.LoadImage(wWW.bytes);
				}
			}
			catch (Exception)
			{
				texture2D = null;
			}
		}
		return texture2D;
	}

	public static void SaveTextureToFile(ref Texture2D texture, string path)
	{
		FileStream fileStream = null;
		BinaryWriter binaryWriter = null;
		try
		{
			fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
			binaryWriter = new BinaryWriter(fileStream);
			binaryWriter.Write(texture.EncodeToPNG());
		}
		catch (Exception)
		{
		}
		finally
		{
			if (binaryWriter != null)
			{
				binaryWriter.Flush();
				binaryWriter.Close();
			}
			if (fileStream != null)
			{
				fileStream.Close();
			}
		}
	}

	public static void ClearTexture(ref Texture2D texture)
	{
		int num = texture.width * texture.height;
		Color32[] array = new Color32[num];
		for (int i = 0; i < num; i++)
		{
			array[i] = Color.clear;
		}
		texture.SetPixels32(array);
	}

	public static void DrawTexture(ref Texture2D destination, Vector2 offset, ref Texture2D source, Rect rect)
	{
		DrawTexture(ref destination, (int)offset.x, (int)offset.y, ref source, (int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, false);
	}

	public static void DrawTexture(ref Texture2D destination, Vector2 offset, ref Texture2D source, Rect rect, bool blend)
	{
		DrawTexture(ref destination, (int)offset.x, (int)offset.y, ref source, (int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, blend);
	}

	public static void DrawTexture(ref Texture2D destination, int dx, int dy, ref Texture2D source, int x, int y, int width, int height, bool blend)
	{
		Color[] pixels = source.GetPixels(x, y, width, height);
		if (blend)
		{
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					int num = i * width + j;
					Color pixel = destination.GetPixel(dx + j, dy + i);
					pixels[num] = Color.Lerp(pixel, pixels[num], pixels[num].a);
				}
			}
		}
		destination.SetPixels(dx, dy, width, height, pixels);
	}

	[DllImport("bijunityplugin")]
	private static extern void NativeClearTexture([In][Out] IntPtr destination, int w, int h);

	[DllImport("bijunityplugin")]
	private static extern void NativeDrawTexture([In][Out] IntPtr destination, int dx, int dy, int dw, int dh, [In][Out] IntPtr source, int sx, int sy, int sw, int sh, int smx, int smy, int blend);

	[DllImport("bijunityplugin")]
	private static extern void NativeFillTexture([In][Out] IntPtr destination, int dx, int dy, int dw, int dh, int color, int sw, int sh, int blend);

	public static void FastBeginTexture(ref Texture2D destination, ref Texture2D source)
	{
		mFastTextureDst = destination;
		mFastTextureSrc = source;
		mFastTextureDstPix = mFastTextureDst.GetPixels32();
		mFastTextureSrcPix = mFastTextureSrc.GetPixels32();
		mFastTextureDstHandle = GCHandle.Alloc(mFastTextureDstPix, GCHandleType.Pinned);
		mFastTextureSrcHandle = GCHandle.Alloc(mFastTextureSrcPix, GCHandleType.Pinned);
	}

	public static void FastEndTexture(bool apply)
	{
		if (apply)
		{
			mFastTextureDst.SetPixels32(mFastTextureDstPix);
			mFastTextureDst.Apply();
		}
		mFastTextureDstHandle.Free();
		mFastTextureSrcHandle.Free();
	}

	private static void FastClearTexture()
	{
		try
		{
			NativeClearTexture(mFastTextureDstHandle.AddrOfPinnedObject(), mFastTextureDst.width, mFastTextureDst.height);
		}
		catch (Exception e)
		{
			BIJLog.E("NativeClearTexture call error,", e);
		}
	}

	public static void FastFillTexture(int dx, int dy, Color color, int sw, int sh, bool blend)
	{
		try
		{
			Color32 color2 = color;
			int color3 = BitConverter.ToInt32(new byte[4] { color2.r, color2.g, color2.b, color2.a }, 0);
			NativeFillTexture(mFastTextureDstHandle.AddrOfPinnedObject(), dx, dy, mFastTextureDst.width, mFastTextureDst.height, color3, sw, sh, blend ? 1 : 0);
		}
		catch (Exception e)
		{
			BIJLog.E("NativeFillTexture call error,", e);
		}
	}

	public static void FastDrawTexture(int dx, int dy, int sx, int sy, int sw, int sh, bool blend)
	{
		try
		{
			NativeDrawTexture(mFastTextureDstHandle.AddrOfPinnedObject(), dx, dy, mFastTextureDst.width, mFastTextureDst.height, mFastTextureSrcHandle.AddrOfPinnedObject(), sx, sy, sw, sh, mFastTextureSrc.width, mFastTextureSrc.height, blend ? 1 : 0);
		}
		catch (Exception e)
		{
			BIJLog.E("NativeDrawTexture call error,", e);
		}
	}

	public static void DrawLine(ref Texture2D texture, Vector2 s, Vector2 e, Color color)
	{
		DrawLine(ref texture, (int)s.x, (int)s.y, (int)e.x, (int)e.y, color);
	}

	public static void DrawLine(ref Texture2D texture, int x0, int y0, int x1, int y1, Color color)
	{
		int num = y1 - y0;
		int num2 = x1 - x0;
		int num3 = 1;
		int num4 = 1;
		if (num < 0)
		{
			num = -num;
			num3 = -1;
		}
		if (num2 < 0)
		{
			num2 = -num2;
			num4 = -1;
		}
		num <<= 1;
		num2 <<= 1;
		texture.SetPixel(x0, y0, color);
		float num5;
		if (num2 > num)
		{
			num5 = num - (num2 >> 1);
			while (x0 != x1)
			{
				if (num5 >= 0f)
				{
					y0 += num3;
					num5 -= (float)num2;
				}
				x0 += num4;
				num5 += (float)num;
				texture.SetPixel(x0, y0, color);
			}
			return;
		}
		num5 = num2 - (num >> 1);
		while (y0 != y1)
		{
			if (num5 >= 0f)
			{
				x0 += num4;
				num5 -= (float)num;
			}
			y0 += num3;
			num5 += (float)num2;
			texture.SetPixel(x0, y0, color);
		}
	}

	public static void DrawRect(ref Texture2D texture, Rect rect, Color color)
	{
		DrawRect(ref texture, (int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, color);
	}

	public static void DrawRect(ref Texture2D texture, int x, int y, int width, int height, Color color)
	{
		FillRect(ref texture, x, y, width, height, Color.clear, color, true);
	}

	public static void FillRect(ref Texture2D texture, Rect rect, Color fillColor, Color borderColor)
	{
		FillRect(ref texture, (int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, fillColor, borderColor, true);
	}

	public static void FillRect(ref Texture2D texture, Rect rect, Color fillColor, Color borderColor, bool blend)
	{
		FillRect(ref texture, (int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, fillColor, borderColor, blend);
	}

	public static void FillRect(ref Texture2D texture, int x, int y, int width, int height, Color fillColor, Color borderColor, bool blend)
	{
		Color[] array = new Color[width * height];
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				Color pixel = texture.GetPixel(x + j, y + i);
				int num = i * width + j;
				if (j == 0 || j == width - 1 || i == 0 || i == height - 1)
				{
					if (blend)
					{
						array[num] = Color.Lerp(pixel, borderColor, borderColor.a);
					}
					else
					{
						array[num] = borderColor;
					}
				}
				else if (blend)
				{
					array[num] = Color.Lerp(pixel, fillColor, fillColor.a);
				}
				else
				{
					array[num] = fillColor;
				}
			}
		}
		texture.SetPixels(x, y, width, height, array);
	}

	public static void DrawImage(BIJImage image, int imageIndex, Rect rect)
	{
		Material material = new Material(Shader.Find("BIJ/BIJ2DAlpha"));
		DrawImage(image, imageIndex, rect, ref material, GUI.depth, Color.white);
		UnityEngine.Object.Destroy(material);
	}

	public static void DrawImage(BIJImage image, int imageIndex, Rect rect, ref Material material)
	{
		DrawImage(image, imageIndex, rect, ref material, GUI.depth, Color.white);
	}

	public static void DrawImage(BIJImage image, int imageIndex, Rect rect, ref Material material, float depth, Color color)
	{
		if (!(material == null))
		{
			if (image != null)
			{
				material.mainTexture = image.Texture;
				material.mainTextureOffset = image.GetOffset(imageIndex);
				material.mainTextureScale = image.GetSize(imageIndex);
			}
			else
			{
				material.mainTexture = null;
				material.mainTextureOffset = new Vector2(0f, 0f);
				material.mainTextureScale = new Vector2(0f, 0f);
			}
			GL.PushMatrix();
			material.SetPass(0);
			GL.LoadOrtho();
			GL.Color(color);
			GL.Begin(7);
			float x = rect.xMin / (float)Screen.width;
			float y = ((float)Screen.height - rect.yMax) / (float)Screen.height;
			float x2 = rect.xMax / (float)Screen.width;
			float y2 = ((float)Screen.height - rect.yMin) / (float)Screen.height;
			GL.MultiTexCoord2(0, 0f, 0f);
			GL.Vertex3(x, y, depth);
			GL.MultiTexCoord2(0, 0f, 1f);
			GL.Vertex3(x, y2, depth);
			GL.MultiTexCoord2(0, 1f, 1f);
			GL.Vertex3(x2, y2, depth);
			GL.MultiTexCoord2(0, 1f, 0f);
			GL.Vertex3(x2, y, depth);
			GL.End();
			GL.PopMatrix();
		}
	}

	public static int NextPowerOfTwo(float n)
	{
		return NextPowerOfTwo((int)n);
	}

	public static int NextPowerOfTwo(int n)
	{
		int num;
		for (num = 2; num < n; num <<= 1)
		{
		}
		return num;
	}
}
