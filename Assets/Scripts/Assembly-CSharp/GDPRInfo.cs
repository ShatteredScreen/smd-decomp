public class GDPRInfo
{
	[MiniJSONAlias("kind")]
	public int mKind { get; set; }

	[MiniJSONAlias("name")]
	public string mName { get; set; }

	[MiniJSONAlias("url")]
	public string mURL { get; set; }

	[MiniJSONAlias("adjust")]
	public string mAdjustKey { get; set; }

	[MiniJSONAlias("date")]
	public long mDate { get; set; }

	[MiniJSONAlias("update")]
	public long mUpdateDate { get; set; }

	public GDPRInfo()
	{
		mKind = -1;
		mName = string.Empty;
		mURL = string.Empty;
		mAdjustKey = string.Empty;
		mDate = 0L;
		mUpdateDate = 0L;
	}
}
