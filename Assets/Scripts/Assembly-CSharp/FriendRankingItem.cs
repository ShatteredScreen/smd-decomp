using System.Collections.Generic;
using UnityEngine;

public class FriendRankingItem : ListItem
{
	private GameMain mGame = SingletonMonoBehaviour<GameMain>.Instance;

	public MyFriend Info { get; private set; }

	public string Name
	{
		get
		{
			if (Info == null)
			{
				if (IsLoggedIn())
				{
					return mGame.mOptions.FbName;
				}
				return Localization.Get("You");
			}
			return Info.Data.Name;
		}
	}

	public Texture2D Icon
	{
		get
		{
			if (Info == null)
			{
				if (IsLoggedIn())
				{
					return mGame.mPlayer.Icon;
				}
				return null;
			}
			return Info.Icon;
		}
	}

	public string Gender
	{
		get
		{
			if (Info == null)
			{
				if (IsLoggedIn())
				{
					return mGame.mOptions.Gender;
				}
				return "male";
			}
			return Info.Gender;
		}
	}

	public int Level
	{
		get
		{
			if (Info == null)
			{
				int result = 1;
				List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
				for (int i = 0; i < playStage.Count; i++)
				{
					if (playStage[i].Series == (int)mGame.mPlayer.Data.CurrentSeries)
					{
						result = playStage[i].Level;
						result /= 100;
						break;
					}
				}
				return result;
			}
			int result2 = 1;
			List<PlayStageParam> playStage2 = Info.Data.PlayStage;
			if (playStage2.Count == 0)
			{
				if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST)
				{
					result2 = mGame.NoHavePlayStageLevel(Info.Data.Level);
				}
			}
			else
			{
				for (int j = 0; j < playStage2.Count; j++)
				{
					if (playStage2[j].Series == (int)mGame.mPlayer.Data.CurrentSeries)
					{
						result2 = playStage2[j].Level;
						result2 /= 100;
						break;
					}
				}
			}
			return result2;
		}
	}

	public int UUID
	{
		get
		{
			if (Info == null)
			{
				return mGame.mPlayer.UUID;
			}
			return Info.Data.UUID;
		}
	}

	public long TotalScore
	{
		get
		{
			if (Info == null)
			{
				return mGame.mPlayer.GetRankingTotalScore();
			}
			return Info.Data.TotalScore;
		}
	}

	public long TotalStar
	{
		get
		{
			if (Info == null)
			{
				return mGame.mPlayer.GetRankingTotalStar();
			}
			return Info.Data.TotalStars;
		}
	}

	public long TotalTrophy
	{
		get
		{
			if (Info == null)
			{
				return mGame.mPlayer.GetRankingTotalTrophy();
			}
			return Info.Data.TotalTrophy;
		}
	}

	public FriendRankingItem()
		: this(null)
	{
	}

	public FriendRankingItem(MyFriend friend)
	{
		Info = friend;
	}

	private bool IsLoggedIn()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}
}
