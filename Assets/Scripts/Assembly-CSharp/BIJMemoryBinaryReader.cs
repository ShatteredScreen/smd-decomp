using System;
using System.IO;
using System.Text;

public class BIJMemoryBinaryReader : BIJBinaryReader
{
	private MemoryStream mStream;

	public BIJMemoryBinaryReader(byte[] buffer)
		: this(buffer, Encoding.UTF8, string.Empty)
	{
	}

	public BIJMemoryBinaryReader(byte[] buffer, string name)
		: this(buffer, Encoding.UTF8, name)
	{
	}

	public BIJMemoryBinaryReader(byte[] buffer, Encoding encoding, string name)
	{
		mStream = null;
		Reader = null;
		try
		{
			mPath = name;
			mStream = new MemoryStream(buffer);
			Reader = new BinaryReader(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	public override void Close()
	{
		base.Close();
		if (mStream != null)
		{
			try
			{
				mStream.Close();
			}
			catch (Exception)
			{
			}
			mStream = null;
		}
	}
}
