public class ConnectGetProfileParam : ConnectParameterBase
{
	public string DebugKey { get; private set; }

	public string DebugDate { get; private set; }

	public ConnectGetProfileParam()
	{
		DebugKey = string.Empty;
		DebugDate = string.Empty;
	}

	public void SetParameter(string a_datekey, string a_debugkey)
	{
		DebugKey = a_debugkey;
		DebugDate = a_datekey;
	}
}
