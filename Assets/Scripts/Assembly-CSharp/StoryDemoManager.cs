using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using live2d;

public class StoryDemoManager : MonoBehaviour
{
	public enum STATE
	{
		INIT = 0,
		DO = 1,
		END = 2,
		CANCEL = 3,
		WAIT = 4
	}

	public delegate void OnDemoFinished(int index);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	private GameMain mGame;

	private GameStateManager mGameState;

	private Camera mMainCamera;

	private Camera mSubCamera;

	private UIPanel mStoryDemoPanel;

	private UIPanel mStoryDemoUIPanel;

	private GameObject mAnchorBottom;

	private GameObject mAnchorBottomLeft;

	private GameObject mAnchorTopRight;

	private GameObject mAnchorCenter;

	private UIPanel mStoryDemoUIPanel2;

	private GameObject mAnchorCenter2;

	private GameObject mDemoRoot;

	private GameObject mMotionRoot;

	private int mStoryIndex;

	private bool mSkipEnable;

	private bool mDisposeLive2D;

	private bool mUnloadUnuseAssetsRequest;

	private ResImage mAtlas;

	private UIFont mFont;

	private int mBaseDepth = 60;

	private Live2DRender mL2DRender;

	private SimpleTexture mL2DScreen;

	private UISprite mBG;

	private UIButton mSkipButton;

	private UIButton mNextButton;

	private UIButton mScreenShotButton;

	private UILabel mText;

	private UILabel mNameText;

	private UISprite mChapterBoard;

	private UISprite mChapterBoardL;

	private UISprite mChapterBoardR;

	private UILabel mChapterNum;

	private UILabel mChapterTitle;

	private UISprite mImage;

	private UISprite mFlashScreen;

	private OneShotAnime mTransformScreen;

	private ParticleSystem mTransformParticle;

	private bool mTextArrowFinish;

	private bool mNextPushed;

	private bool mCommandBusy;

	private float mCancelTimer;

	private int mSeChannel;

	private static float CHAR_WAIT_TIME = 0.03f;

	private static float DEFAULT_CHARACTER_SCALE = 0.4f;

	private static float DEMO_CENTER_OFFSET = 340f;

	private static float BASE_Z = -1f;

	private static float BG_IMAGE_Z = BASE_Z + 1.1f;

	private static float IMAGE_Z = BASE_Z - 1f;

	private static float CHARACTER_LOWER_Z = BASE_Z - 1f;

	private static float TEXT_WINDOW_Z = BASE_Z - 10f;

	private static float FLASH_SCREEN_Z = BASE_Z - 20f;

	private Dictionary<string, Live2DInstance> mCharacterDict = new Dictionary<string, Live2DInstance>();

	private Dictionary<string, UISprite> mImageDict = new Dictionary<string, UISprite>();

	private Dictionary<string, SimpleTexture> mBgImageDict = new Dictionary<string, SimpleTexture>();

	private Dictionary<string, ResImage> mAdditionalLoadedAtlasDict = new Dictionary<string, ResImage>();

	private List<StoryCommandData> mCommandList;

	private int mCurrentIndex;

	private List<string> mPreLoadAnimationKeys = new List<string>();

	private List<ResLive2DAnimation> mPreLoadAnimations = new List<ResLive2DAnimation>();

	private Dictionary<string, bool> mRenderPlanDict = new Dictionary<string, bool>();

	private List<ResSound> mPreLoadSounds = new List<ResSound>();

	private List<string> mUnloadSounds = new List<string>();

	private OnDemoFinished OnFinishCallback;

	private ConfirmDialog mConfirmDialog;

	private ScreenShotDialog mScreenShotDialog;

	private int mGrayScreenDepthBuf;

	protected ScreenOrientation mOrientation;

	private string[] BgImageNames = new string[3] { "story_window00", "story_window01", "story_window02" };

	private Color[] TextColors = new Color[3]
	{
		new Color(7f / 15f, 39f / 85f, 0.2f),
		new Color(1f, 1f, 1f),
		new Color(7f / 15f, 39f / 85f, 0.2f)
	};

	private Color[] NameColors = new Color[3]
	{
		new Color(31f / 51f, 0.6f, 31f / 85f),
		new Color(0.30980393f, 14f / 51f, 0.36862746f),
		new Color(31f / 51f, 0.6f, 31f / 85f)
	};

	private bool firstDrawed;

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private IEnumerator Start()
	{
		Resources.UnloadUnusedAssets();
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
		mL2DRender = mGame.CreateLive2DRender(1136, 1136, new Color(0f, 0f, 0f, 0f));
		mL2DRender.BackgroundColor = new Color(0f, 0f, 0f, 0f);
		GameObject go = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.DEMO_CAMERA));
		go.transform.parent = mGame.mCamera.transform.parent;
		go.transform.localPosition = Vector3.zero;
		go.transform.localScale = Vector3.one;
		mMainCamera = mGame.mCamera;
		mSubCamera = go.GetComponent<Camera>();
		mSubCamera.eventMask = mSubCamera.gameObject.layer;
		mStoryDemoPanel = Util.CreateGameObject("StoryDemoPanel", mSubCamera.gameObject).AddComponent<UIPanel>();
		mStoryDemoPanel.depth = mGame.mDialogManager.GrayScreenDepth + 1;
		mStoryDemoPanel.sortingOrder = mGame.mDialogManager.GrayScreenDepth + 1;
		mStoryDemoUIPanel = Util.CreateGameObject("StoryDemoUIPanel", mSubCamera.gameObject).AddComponent<UIPanel>();
		mStoryDemoUIPanel.depth = mGame.mDialogManager.GrayScreenDepth + 2;
		mStoryDemoUIPanel.sortingOrder = mGame.mDialogManager.GrayScreenDepth + 2;
		mStoryDemoUIPanel2 = Util.CreateGameObject("StoryDemoUIPanel2", mMainCamera.gameObject).AddComponent<UIPanel>();
		mStoryDemoUIPanel2.depth = mGame.mDialogManager.GrayScreenDepth + 11;
		mStoryDemoUIPanel2.sortingOrder = mGame.mDialogManager.GrayScreenDepth + 11;
		mAnchorBottom = Util.CreateAnchorWithChild("StoryAnchorBottom", mStoryDemoPanel.gameObject, UIAnchor.Side.Bottom, mSubCamera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("StoryAnchorBottomLeft", mStoryDemoUIPanel.gameObject, UIAnchor.Side.BottomLeft, mSubCamera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("StoryAnchorTopRight", mStoryDemoUIPanel.gameObject, UIAnchor.Side.TopRight, mSubCamera).gameObject;
		mAnchorCenter = Util.CreateAnchorWithChild("StoryAnchorCenter", mStoryDemoPanel.gameObject, UIAnchor.Side.Center, mSubCamera).gameObject;
		mAnchorCenter2 = Util.CreateAnchorWithChild("StoryAnchorCenter", mStoryDemoUIPanel2.gameObject, UIAnchor.Side.Center, mMainCamera).gameObject;
		mDemoRoot = Util.CreateGameObject("StoryDemoRoot", mAnchorBottom.gameObject);
		mMotionRoot = Util.CreateGameObject("MotionRoot", mL2DRender.gameObject);
		mAtlas = ResourceManager.LoadImageAsync("STORY");
		while (mAtlas.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mFont = GameMain.LoadFont();
		yield return 0;
		mL2DScreen = Util.CreateGameObject("Texture", mDemoRoot).AddComponent<SimpleTexture>();
		mL2DScreen.Init(mL2DRender.RenderTexture, new Vector2(1136f, 1136f), new Rect(0f, 0f, 1f, 1f), SimpleTexture.PIVOT.CENTER);
		mL2DScreen.transform.localPosition = new Vector3(0f, DEMO_CENTER_OFFSET, 0f);
		mOrientation = Util.ScreenOrientation;
		SetLayout(mOrientation);
		mState.Change(STATE.INIT);
	}

	private IEnumerator ReleaseResurces()
	{
		if (mSkipButton != null)
		{
			UnityEngine.Object.Destroy(mSkipButton.gameObject);
		}
		if (mScreenShotButton != null)
		{
			UnityEngine.Object.Destroy(mScreenShotButton.gameObject);
		}
		mSkipButton = null;
		mScreenShotButton = null;
		if (mL2DScreen != null)
		{
			UnityEngine.Object.Destroy(mL2DScreen.gameObject);
			mL2DScreen = null;
		}
		if (mL2DRender != null)
		{
			UnityEngine.Object.Destroy(mL2DRender.gameObject);
			mL2DRender = null;
		}
		if (mSubCamera != null)
		{
			UnityEngine.Object.Destroy(mSubCamera.gameObject);
			mSubCamera = null;
		}
		if (mStoryDemoUIPanel != null)
		{
			UnityEngine.Object.Destroy(mStoryDemoUIPanel.gameObject);
			mStoryDemoUIPanel = null;
		}
		if (mStoryDemoUIPanel2 != null)
		{
			UnityEngine.Object.Destroy(mStoryDemoUIPanel2.gameObject);
			mStoryDemoUIPanel2 = null;
		}
		CloseSubDialogs();
		for (int i = 0; i < mPreLoadAnimationKeys.Count; i++)
		{
			ResourceManager.UnloadLive2DAnimation(mPreLoadAnimationKeys[i]);
		}
		mCharacterDict.Clear();
		mImageDict.Clear();
		mBgImageDict.Clear();
		mPreLoadAnimationKeys.Clear();
		mPreLoadAnimations.Clear();
		yield return 0;
		foreach (string s in mUnloadSounds)
		{
			ResourceManager.UnloadSound(s);
		}
		mUnloadSounds.Clear();
		if (mDisposeLive2D)
		{
			Live2D.dispose();
			Live2D.init();
		}
		foreach (KeyValuePair<string, ResImage> item in mAdditionalLoadedAtlasDict)
		{
			ResourceManager.UnloadImage(item.Key);
		}
		mAdditionalLoadedAtlasDict.Clear();
		ResourceManager.UnloadImage("STORY");
		yield return Resources.UnloadUnusedAssets();
		if (OnFinishCallback != null)
		{
			OnFinishCallback(mStoryIndex);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private void OnDestroy()
	{
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
		{
			bool flag2 = true;
			for (int i = 0; i < mPreLoadAnimations.Count; i++)
			{
				if (mPreLoadAnimations[i].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					flag2 = false;
				}
			}
			for (int j = 0; j < mPreLoadSounds.Count; j++)
			{
				if (mPreLoadSounds[j].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					flag2 = false;
				}
			}
			if (!flag2)
			{
				break;
			}
			if (mSkipEnable)
			{
				mSkipButton = Util.CreateJellyImageButton("SkipButton", mAnchorTopRight.gameObject, mAtlas.Image);
				Util.SetImageButtonInfo(mSkipButton, "LC_button_skip", "LC_button_skip", "LC_button_skip", mBaseDepth + 11, Vector3.zero, Vector3.one);
				Util.AddImageButtonMessage(mSkipButton, this, "OnSkipPushed", UIButtonMessage.Trigger.OnClick);
				mSkipButton.transform.localPosition = new Vector3(-100f, -140f, TEXT_WINDOW_Z - 20f);
				if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Screenshot))
				{
					mScreenShotButton = Util.CreateJellyImageButton("ScreenShotButton", mAnchorBottomLeft.gameObject, mAtlas.Image);
					Util.SetImageButtonInfo(mScreenShotButton, "button_camera", "button_camera", "button_camera", mBaseDepth + 11, Vector3.zero, Vector3.one);
					Util.AddImageButtonMessage(mScreenShotButton, this, "OnScreenShotPushed", UIButtonMessage.Trigger.OnClick);
					mScreenShotButton.transform.localPosition = new Vector3(80f, 60f, TEXT_WINDOW_Z);
				}
			}
			else
			{
				mSkipButton = null;
				mScreenShotButton = null;
			}
			mState.Change(STATE.DO);
			mGame.FinishLoading();
			break;
		}
		case STATE.DO:
			if (!mCommandBusy)
			{
				bool flag;
				do
				{
					flag = Do();
					CheckFeatureLoadChara();
					mCurrentIndex++;
				}
				while (flag);
			}
			break;
		case STATE.END:
			if (mState.IsChanged())
			{
				StartCoroutine(ReleaseResurces());
			}
			return;
		case STATE.CANCEL:
			if (mState.IsChanged())
			{
				mCancelTimer = 0.5f;
			}
			if (mCancelTimer > 0f)
			{
				mCancelTimer -= Time.deltaTime;
				if (mCancelTimer <= 0f)
				{
					mState.Change(STATE.END);
					mGame.StopMusic(1, 0f);
				}
			}
			break;
		}
		mState.Update();
		if (mOrientation != Util.ScreenOrientation)
		{
			SetLayout(Util.ScreenOrientation);
			mOrientation = Util.ScreenOrientation;
		}
	}

	private bool Do()
	{
		if (mCommandList.Count <= mCurrentIndex)
		{
			mState.Change(STATE.END);
			return false;
		}
		StoryCommandData storyCommandData = mCommandList[mCurrentIndex];
		switch (storyCommandData.command)
		{
		case "BG_IN":
			StartCoroutine(CmdBgIn(storyCommandData));
			break;
		case "BG_COLOR":
			CmdBgColor(storyCommandData);
			break;
		case "BG_OUT":
			StartCoroutine(CmdBgOut(storyCommandData));
			break;
		case "BG_TRANSFORM_IN":
			CmdBgTransformIn(storyCommandData);
			break;
		case "BG_TRANSFORM_OUT":
			StartCoroutine(CmdBgTransformOut(storyCommandData));
			break;
		case "TEXT":
			StartCoroutine(CmdText(storyCommandData));
			break;
		case "TEXT_FLUSH":
			StartCoroutine(CmdTextFlush(storyCommandData));
			break;
		case "CHAPTER_TITLE_IN":
			StartCoroutine(CmdChapterTitleIn(storyCommandData));
			break;
		case "CHAPTER_TITLE_OUT":
			StartCoroutine(CmdChapterTitleOut(storyCommandData));
			break;
		case "CHARACTER_IN":
			StartCoroutine(CmdCharacterIn(storyCommandData));
			break;
		case "CHARACTER_OUT":
			StartCoroutine(CmdCharacterOut(storyCommandData));
			break;
		case "CHARACTER_CHANGE":
			StartCoroutine(CmdCharacterChange(storyCommandData));
			break;
		case "CHARACTER_MOVE":
			StartCoroutine(CmdCharacterMove(storyCommandData));
			break;
		case "CHANGE_EXPRESSION":
			StartCoroutine(CmdChangeExpression(storyCommandData));
			break;
		case "CHANGE_MOTION":
			StartCoroutine(CmdChangeMotion(storyCommandData));
			break;
		case "IMAGE_IN":
			StartCoroutine(CmdImageIn(storyCommandData));
			break;
		case "IMAGE_OUT":
			StartCoroutine(CmdImageOut(storyCommandData));
			break;
		case "IMAGE_IN_ID":
			StartCoroutine(CmdImageInId(storyCommandData));
			break;
		case "IMAGE_OUT_ID":
			StartCoroutine(CmdImageOutId(storyCommandData));
			break;
		case "BG_IMAGE_IN_ID":
			StartCoroutine(CmdBgImageInId(storyCommandData));
			break;
		case "BG_IMAGE_OUT_ID":
			StartCoroutine(CmdBgImageOutId(storyCommandData));
			break;
		case "FLASH_SCREEN_IN":
			StartCoroutine(CmdFlashScreenIn(storyCommandData));
			break;
		case "FLASH_SCREEN_OUT":
			StartCoroutine(CmdFlashScreenOut(storyCommandData));
			break;
		case "SHAKE":
			StartCoroutine(CmdShake(storyCommandData));
			break;
		case "EFFECT_FILE":
			CmdEffectFile(storyCommandData);
			break;
		case "PLAY_SE":
			CmdPlaySe(storyCommandData);
			break;
		case "PLAY_BGM":
			CmdPlayBgm(storyCommandData);
			break;
		case "STOP_BGM":
			CmdStopBgm(storyCommandData);
			break;
		case "TAP_WAIT":
			StartCoroutine(CmdTapWait(storyCommandData));
			break;
		case "WAIT":
			StartCoroutine(CmdWait(storyCommandData));
			break;
		case "END":
			mState.Change(STATE.END);
			break;
		}
		if (mCommandBusy)
		{
			return false;
		}
		return true;
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		float num = 1f;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mDemoRoot != null)
			{
				mDemoRoot.transform.localPosition = new Vector3(0f, 160f, 0f);
				num = 1f;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mDemoRoot != null)
			{
				mDemoRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
				num = 1f / mGame.mDeviceRatioScale;
			}
			break;
		}
		if (mDemoRoot != null)
		{
			mDemoRoot.transform.localScale = new Vector3(num, num, 1f);
		}
		if (mFlashScreen != null)
		{
			mFlashScreen.SetDimensions((int)vector.x + 2, (int)vector.y + 2);
		}
	}

	public void ChangeStoryDemo(int storyIndex, OnDemoFinished callback, bool skipEnable = true, bool disposeLive2D = true)
	{
		mStoryIndex = storyIndex;
		StoryDemoPlay(mGame.mStoryDemoData[storyIndex].path, callback, skipEnable, disposeLive2D);
	}

	public void ChangeStoryDemo(string storyPath, OnDemoFinished callback, bool skipEnable = true, bool disposeLive2D = true)
	{
		mStoryIndex = -1;
		StoryDemoPlay(storyPath, callback, skipEnable, disposeLive2D);
	}

	public void StoryDemoPlay(string storyPath, OnDemoFinished callback, bool skipEnable, bool disposeLive2D)
	{
		mGame.StartLoading();
		mSkipEnable = skipEnable;
		mDisposeLive2D = disposeLive2D;
		OnFinishCallback = callback;
		mCommandList = new List<StoryCommandData>();
		mCurrentIndex = 0;
		string filename = storyPath.Substring(storyPath.LastIndexOf("/") + 1);
		BIJBinaryReader downloadFileReadStream = SingletonMonoBehaviour<GameMain>.Instance.mDLManager.GetDownloadFileReadStream(filename);
		if (downloadFileReadStream == null)
		{
			using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(storyPath))
			{
				short num = bIJResourceBinaryReader.ReadShort();
				for (int i = 0; i < num; i++)
				{
					StoryCommandData storyCommandData = new StoryCommandData();
					storyCommandData.deserialize(bIJResourceBinaryReader);
					mCommandList.Add(storyCommandData);
				}
				bIJResourceBinaryReader.Close();
			}
		}
		else
		{
			short num2 = downloadFileReadStream.ReadShort();
			for (int j = 0; j < num2; j++)
			{
				StoryCommandData storyCommandData2 = new StoryCommandData();
				storyCommandData2.deserialize(downloadFileReadStream);
				mCommandList.Add(storyCommandData2);
			}
			downloadFileReadStream.Close();
		}
		for (int k = 0; k < mCommandList.Count; k++)
		{
			string empty = string.Empty;
			if (string.Compare(mCommandList[k].command, "CHARACTER_IN") == 0)
			{
				empty = mCommandList[k].param1;
			}
			else if (string.Compare(mCommandList[k].command, "CHARACTER_CHANGE") == 0)
			{
				empty = mCommandList[k].param1;
			}
			if (string.Compare(mCommandList[k].command, "PLAY_BGM") == 0)
			{
				ResSound resSound = ResourceManager.LoadSoundAsync(mCommandList[k].param0);
				mPreLoadSounds.Add(resSound);
				if (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					mUnloadSounds.Add(mCommandList[k].param0);
				}
			}
		}
		CheckFeatureLoadChara();
	}

	public void CheckFeatureLoadChara()
	{
		Dictionary<string, bool> dictionary = new Dictionary<string, bool>();
		if (mL2DRender != null)
		{
			List<Live2DInstance> resisterInstances = mL2DRender.GetResisterInstances();
			int i = 0;
			for (int count = resisterInstances.Count; i < count; i++)
			{
				dictionary[resisterInstances[i].mL2DKey] = true;
			}
		}
		for (int j = mCurrentIndex - 1; j < mCommandList.Count; j++)
		{
			if (j >= 0)
			{
				if (dictionary.Keys.Count >= 5)
				{
					break;
				}
				string text = string.Empty;
				if (string.Compare(mCommandList[j].command, "CHARACTER_IN") == 0)
				{
					text = mCommandList[j].param1;
				}
				else if (string.Compare(mCommandList[j].command, "CHARACTER_CHANGE") == 0)
				{
					text = mCommandList[j].param1;
				}
				if (!string.IsNullOrEmpty(text))
				{
					dictionary[text] = true;
				}
			}
		}
		bool flag = false;
		int num = mPreLoadAnimationKeys.Count - 1;
		int num2 = 0;
		while (num2 <= num)
		{
			string key = mPreLoadAnimationKeys[num];
			if (dictionary.ContainsKey(key))
			{
				dictionary.Remove(key);
			}
			else if (!mRenderPlanDict.ContainsKey(key))
			{
				ResourceManager.UnloadLive2DAnimation(key);
				mPreLoadAnimationKeys.RemoveAt(num);
				mPreLoadAnimations.RemoveAt(num);
				flag = true;
			}
			num--;
		}
		if (flag)
		{
			Resources.UnloadUnusedAssets();
		}
		foreach (string key2 in dictionary.Keys)
		{
			mPreLoadAnimationKeys.Add(key2);
			mPreLoadAnimations.Add(ResourceManager.LoadLive2DAnimationAsync(key2));
		}
	}

	public IEnumerator CmdBgIn(StoryCommandData command)
	{
		int colorIndex = int.Parse(command.param0);
		mBG = Util.CreateSprite("StoryBg", mDemoRoot, mAtlas.Image);
		Util.SetSpriteInfo(mBG, BgImageNames[colorIndex], mBaseDepth + 10, Vector3.zero, Vector3.one, false);
		mBG.type = UIBasicSprite.Type.Advanced;
		mBG.leftType = UIBasicSprite.AdvancedType.Sliced;
		mBG.rightType = UIBasicSprite.AdvancedType.Sliced;
		mBG.topType = UIBasicSprite.AdvancedType.Tiled;
		mBG.bottomType = UIBasicSprite.AdvancedType.Tiled;
		mBG.centerType = UIBasicSprite.AdvancedType.Tiled;
		mBG.SetDimensions(640, 250);
		mBG.pivot = UIWidget.Pivot.Bottom;
		mBG.transform.localPosition = new Vector3(0f, 0f, TEXT_WINDOW_Z);
		mText = Util.CreateLabel("MessageText", mBG.gameObject, mFont);
		Util.SetLabelInfo(mText, string.Empty, mBaseDepth + 12, Vector3.zero, 26, 0, 0, UIWidget.Pivot.TopLeft);
		mText.color = TextColors[colorIndex];
		mText.overflowMethod = UILabel.Overflow.ResizeHeight;
		mText.spacingY = 10;
		mText.SetDimensions(592, 240);
		mText.transform.localPosition = new Vector3(-296f, 150f, 0f);
		UISprite sprite = Util.CreateSprite("NameFrame", mBG.gameObject, mAtlas.Image);
		Util.SetSpriteInfo(sprite, "story_name", mBaseDepth + 11, Vector3.zero, Vector3.one, false);
		sprite.pivot = UIWidget.Pivot.Left;
		sprite.transform.localPosition = new Vector3(-300f, 200f, 0f);
		mNameText = Util.CreateLabel("NameText", mBG.gameObject, mFont);
		Util.SetLabelInfo(mNameText, string.Empty, mBaseDepth + 12, Vector3.zero, 26, 0, 0, UIWidget.Pivot.Left);
		mNameText.color = NameColors[colorIndex];
		mNameText.overflowMethod = UILabel.Overflow.ShrinkContent;
		mNameText.SetDimensions(310, 32);
		mNameText.transform.localPosition = new Vector3(-282f, 198f, 0f);
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 180f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mBG.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
	}

	public void CmdBgColor(StoryCommandData command)
	{
		if (mBG != null)
		{
			int num = int.Parse(command.param0);
			mBG.spriteName = BgImageNames[num];
			mText.color = TextColors[num];
			mNameText.color = NameColors[num];
		}
	}

	public IEnumerator CmdBgOut(StoryCommandData command)
	{
		if (!(mBG != null))
		{
			yield break;
		}
		float angle = 90f;
		while (angle > 0f)
		{
			angle -= Time.deltaTime * 180f;
			if (angle < 0f)
			{
				angle = 0f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mBG.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		UnityEngine.Object.Destroy(mBG.gameObject);
		if (mNextButton != null)
		{
			UnityEngine.Object.Destroy(mNextButton.gameObject);
		}
		mBG = null;
	}

	public void CmdBgTransformIn(StoryCommandData command)
	{
		float num = (float)mGame.mRoot.manualHeight / 1136f;
		mTransformScreen = Util.CreateOneShotAnime(scale: new Vector3(num, num, 1f), name: "TransformBg", key: "EFFECT_DEMO_TRANSFORMATION", parent: mAnchorCenter.gameObject, pos: new Vector3(0f, 0f, 10f), rotateDeg: 0f, delay: 0f, autoDestroy: false);
		mTransformScreen._sprite.PlayCount = 0;
		mTransformParticle = GameMain.GetParticle("Particles/TransformParticle").GetComponent<ParticleSystem>();
		mTransformParticle.gameObject.transform.parent = mTransformScreen.transform;
		mTransformParticle.gameObject.layer = mTransformScreen.gameObject.layer;
		mTransformParticle.gameObject.transform.localScale = Vector3.one;
		mTransformParticle.gameObject.transform.localPosition = new Vector3(0f, -284f, 0f);
		mTransformParticle.Play();
	}

	public IEnumerator CmdBgTransformOut(StoryCommandData command)
	{
		if (!(mTransformScreen != null))
		{
			yield break;
		}
		mTransformParticle.Stop();
		float angle = 90f;
		while (angle > 0f)
		{
			angle -= Time.deltaTime * 180f;
			if (angle < 0f)
			{
				angle = 0f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mTransformScreen.SetColor(new Color(1f, 1f, 1f, alpha));
			yield return 0;
		}
		UnityEngine.Object.Destroy(mTransformScreen.gameObject);
		mTransformScreen = null;
	}

	public IEnumerator CmdText(StoryCommandData command)
	{
		mCommandBusy = true;
		Live2DInstance inst = null;
		while (true)
		{
			mCharacterDict.TryGetValue(command.param0, out inst);
			if (inst != null)
			{
				break;
			}
			yield return 0;
		}
		while (!inst.IsLoadFinished())
		{
			yield return 0;
		}
		mNameText.text = Localization.Get(command.param1);
		if ((string.IsNullOrEmpty(command.param3) || string.Compare(command.param3, "LIPON") == 0) && inst != null)
		{
			float speedScale = 1f;
			if (!string.IsNullOrEmpty(command.param4))
			{
				speedScale = float.Parse(command.param4);
			}
			inst.StartLipSync(1f, speedScale);
		}
		float lipTime = -1f;
		if (!string.IsNullOrEmpty(command.param5))
		{
			lipTime = float.Parse(command.param5);
		}
		string str = Localization.Get(command.param2);
		string strBuf = string.Empty;
		int charCount = 0;
		int charNum = 1;
		while (charCount < str.Length)
		{
			int sub = str.Length - (charCount + charNum);
			if (sub < 0)
			{
				charNum += sub;
			}
			string subStr = str.Substring(charCount, charNum);
			charCount += charNum;
			strBuf += subStr;
			mText.text = strBuf;
			charNum = 0;
			if (subStr == "\u3000" || subStr == " ")
			{
				continue;
			}
			float wait = CHAR_WAIT_TIME;
			if (mGame.mTouchDownTrg)
			{
				mText.text = str;
				break;
			}
			while (wait > 0f)
			{
				yield return 0;
				if (lipTime > 0f)
				{
					lipTime -= Time.deltaTime;
					if (lipTime < 0f && inst != null)
					{
						inst.EndLipSync();
					}
				}
				wait -= Time.deltaTime;
				if (wait < 0f)
				{
					for (; wait < 0f; wait += CHAR_WAIT_TIME)
					{
						charNum++;
					}
					break;
				}
			}
		}
		mNextPushed = false;
		mTextArrowFinish = false;
		StartCoroutine(TextArrow());
		while (true)
		{
			bool tapped = false;
			if (mGame.mTouchDownTrg)
			{
				Vector3 pos = mMainCamera.ScreenToWorldPoint(mGame.mTouchPos);
				tapped = IsValidTap(pos);
			}
			if (mNextPushed || tapped)
			{
				break;
			}
			yield return 0;
			if (lipTime > 0f)
			{
				lipTime -= Time.deltaTime;
				if (lipTime < 0f && inst != null)
				{
					inst.EndLipSync();
				}
			}
		}
		mTextArrowFinish = true;
		if (inst != null)
		{
			inst.EndLipSync();
		}
		mCommandBusy = false;
	}

	private IEnumerator TextArrow()
	{
		Vector3 basePos = new Vector3(290f, 36f, 0f);
		UISprite arrow = Util.CreateSprite("TextArrow", mDemoRoot, mAtlas.Image);
		Util.SetSpriteInfo(arrow, "story_arrow", mBaseDepth + 11, basePos, Vector3.one, false);
		float rotateAngle = 0f;
		float boundAngle = 0f;
		int bounds = 0;
		while (!mTextArrowFinish && mState.GetStatus() == STATE.DO)
		{
			rotateAngle += Time.deltaTime * 360f;
			float scaleX = Mathf.Sin(rotateAngle * ((float)Math.PI / 180f));
			arrow.transform.localScale = new Vector3(scaleX, 1f, 1f);
			boundAngle += Time.deltaTime * 720f;
			if (boundAngle > 180f)
			{
				boundAngle -= 180f;
				bounds++;
				if (bounds > 8)
				{
					bounds = 0;
				}
			}
			float ratio2 = 1f;
			switch (bounds)
			{
			case 0:
				ratio2 = 1f;
				break;
			case 1:
				ratio2 = 0.2f;
				break;
			case 2:
				ratio2 = 0.05f;
				break;
			default:
				ratio2 = 0f;
				break;
			}
			float posY = Mathf.Sin(boundAngle * ((float)Math.PI / 180f)) * 40f * ratio2;
			arrow.transform.localPosition = basePos + new Vector3(0f, posY, 0f);
			yield return 0;
		}
		UnityEngine.Object.Destroy(arrow.gameObject);
	}

	public IEnumerator CmdTextFlush(StoryCommandData command)
	{
		mNameText.text = string.Empty;
		mText.text = string.Empty;
		yield return 0;
	}

	public IEnumerator CmdChapterTitleIn(StoryCommandData command)
	{
		mCommandBusy = true;
		mChapterBoard = Util.CreateSprite("ChapterBoard", mAnchorCenter.gameObject, mAtlas.Image);
		Util.SetSpriteInfo(mChapterBoard, "demo_title_panel", mBaseDepth + 10, Vector3.zero, Vector3.one, false);
		mChapterBoard.color = new Color(1f, 1f, 1f, 0f);
		mChapterBoardL = Util.CreateSprite("ChapterBoard", mAnchorCenter.gameObject, mAtlas.Image);
		Util.SetSpriteInfo(mChapterBoardL, "demo_title_panel2", mBaseDepth + 9, new Vector3(-130f, -70f, 0f), Vector3.one, false);
		mChapterBoardL.color = new Color(1f, 1f, 1f, 0f);
		mChapterBoardR = Util.CreateSprite("ChapterBoard", mAnchorCenter.gameObject, mAtlas.Image);
		Util.SetSpriteInfo(mChapterBoardR, "demo_title_panel2", mBaseDepth + 9, new Vector3(130f, 70f, 0f), Vector3.one, false);
		mChapterBoardR.color = new Color(1f, 1f, 1f, 0f);
		mChapterNum = Util.CreateLabel("ChapterNum", mChapterBoard.gameObject, mFont);
		Util.SetLabelInfo(mChapterNum, Localization.Get(command.param0), mBaseDepth + 11, new Vector3(0f, 45f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
		mChapterNum.color = new Color(Def.DEFAULT_MESSAGE_COLOR.r, Def.DEFAULT_MESSAGE_COLOR.g, Def.DEFAULT_MESSAGE_COLOR.b, 0f);
		mChapterTitle = Util.CreateLabel("ChapterTitle", mChapterBoard.gameObject, mFont);
		Util.SetLabelInfo(mChapterTitle, Localization.Get(command.param1), mBaseDepth + 11, new Vector3(0f, -34f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		mChapterTitle.color = new Color(Def.DEFAULT_MESSAGE_COLOR.r, Def.DEFAULT_MESSAGE_COLOR.g, Def.DEFAULT_MESSAGE_COLOR.b, 0f);
		mChapterTitle.spacingY = 8;
		StartCoroutine(ChapterTitleUpdate());
		StartCoroutine(ChapterTitleFadeIn(mChapterBoard, 90f, Color.white));
		yield return new WaitForSeconds(0.7f);
		StartCoroutine(ChapterTitleFadeIn(mChapterBoardL, 45f, Color.white));
		StartCoroutine(ChapterTitleFadeIn(mChapterBoardR, 45f, Color.white));
		yield return new WaitForSeconds(0.5f);
		StartCoroutine(ChapterTitleFadeIn(mChapterNum, 90f, Def.DEFAULT_MESSAGE_COLOR));
		yield return new WaitForSeconds(0.5f);
		StartCoroutine(ChapterTitleFadeIn(mChapterTitle, 90f, Def.DEFAULT_MESSAGE_COLOR));
		mCommandBusy = false;
	}

	private IEnumerator ChapterTitleFadeIn(UIWidget widget, float speed, Color baseColor)
	{
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * speed;
			if (angle > 90f)
			{
				angle = 90f;
			}
			widget.color = new Color(a: Mathf.Sin(angle * ((float)Math.PI / 180f)), r: baseColor.r, g: baseColor.g, b: baseColor.b);
			yield return 0;
		}
	}

	private IEnumerator ChapterTitleUpdate()
	{
		float angle = 0f;
		while (mChapterBoard != null)
		{
			angle += Time.deltaTime * 15f;
			mChapterBoardL.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			mChapterBoardR.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	public IEnumerator CmdChapterTitleOut(StoryCommandData command)
	{
		mCommandBusy = true;
		float angle2 = 90f;
		while (angle2 > 0f)
		{
			angle2 -= Time.deltaTime * 90f;
			if (angle2 < 0f)
			{
				angle2 = 0f;
			}
			float alpha = Mathf.Sin(angle2 * ((float)Math.PI / 180f));
			mChapterBoardL.color = new Color(1f, 1f, 1f, alpha);
			mChapterBoardR.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		angle2 = 90f;
		while (angle2 > 0f)
		{
			angle2 -= Time.deltaTime * 90f;
			if (angle2 < 0f)
			{
				angle2 = 0f;
			}
			float alpha2 = Mathf.Sin(angle2 * ((float)Math.PI / 180f));
			mChapterBoard.color = new Color(1f, 1f, 1f, alpha2);
			yield return 0;
		}
		UnityEngine.Object.Destroy(mChapterBoard.gameObject);
		UnityEngine.Object.Destroy(mChapterBoardL.gameObject);
		UnityEngine.Object.Destroy(mChapterBoardR.gameObject);
		mChapterBoard = null;
		mCommandBusy = false;
	}

	public IEnumerator CmdCharacterIn(StoryCommandData command)
	{
		mRenderPlanDict[command.param1] = true;
		float scale = float.Parse(command.param4) / 100f;
		if (scale <= 0f)
		{
			scale = DEFAULT_CHARACTER_SCALE;
		}
		while (true)
		{
			bool exist = false;
			int i = 0;
			for (int imax = mPreLoadAnimationKeys.Count; i < imax; i++)
			{
				if (mPreLoadAnimationKeys[i] == command.param1 && mPreLoadAnimations[i].LoadState == ResourceInstance.LOADSTATE.DONE)
				{
					exist = true;
				}
			}
			if (exist)
			{
				break;
			}
			yield return 0;
		}
		string[] xy = command.param2.Split(',');
		Vector3 pos = new Vector3(float.Parse(xy[0]), float.Parse(xy[1]), float.Parse(xy[2]));
		Live2DInstance inst = Util.CreateGameObject("motion", mMotionRoot).AddComponent<Live2DInstance>();
		inst.Init(command.param1, pos, scale, Live2DInstance.PIVOT.CENTER, new Color(0f, 0f, 0f, 0f), false);
		mCharacterDict.Add(command.param0, inst);
		float targetPosX = 0f;
		if (string.Compare(command.param3, "L") == 0)
		{
			targetPosX = -100f;
		}
		else if (string.Compare(command.param3, "R") == 0)
		{
			targetPosX = 100f;
		}
		inst.SetTargetPoint(new Vector2(targetPosX, 0f), true);
		inst.SetBaseColor(new Color(0f, 0f, 0f, 0f));
		mL2DRender.ResisterInstance(inst);
		yield return 0;
		while (!inst.IsLoadFinished())
		{
			yield return 0;
		}
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 360f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float a = Mathf.Sin(angle * ((float)Math.PI / 180f));
			inst.SetBaseColor(new Color(a, a, a, 1f));
			yield return 0;
		}
	}

	public IEnumerator CmdCharacterOut(StoryCommandData command)
	{
		Live2DInstance inst = null;
		while (true)
		{
			mCharacterDict.TryGetValue(command.param0, out inst);
			if (inst != null)
			{
				break;
			}
			yield return 0;
		}
		while (!inst.IsLoadFinished())
		{
			yield return 0;
		}
		float angle = 90f;
		while (angle > 0f)
		{
			angle -= Time.deltaTime * 360f;
			if (angle < 0f)
			{
				angle = 0f;
			}
			float a = Mathf.Sin(angle * ((float)Math.PI / 180f));
			inst.SetBaseColor(new Color(a, a, a, 1f));
			yield return 0;
		}
		if (mRenderPlanDict.ContainsKey(inst.mL2DKey))
		{
			mRenderPlanDict.Remove(inst.mL2DKey);
		}
		mCharacterDict.Remove(command.param0);
		mL2DRender.UnresisterInstance(inst);
		UnityEngine.Object.Destroy(inst.gameObject);
		yield return 0;
	}

	public IEnumerator CmdCharacterChange(StoryCommandData command)
	{
		Live2DInstance inst2 = null;
		while (true)
		{
			mCharacterDict.TryGetValue(command.param0, out inst2);
			if (inst2 != null)
			{
				break;
			}
			yield return 0;
		}
		while (!inst2.IsLoadFinished())
		{
			yield return 0;
		}
		Vector3 pos = inst2.Position;
		if (mRenderPlanDict.ContainsKey(inst2.mL2DKey))
		{
			mRenderPlanDict.Remove(inst2.mL2DKey);
		}
		mCharacterDict.Remove(command.param0);
		mL2DRender.UnresisterInstance(inst2);
		UnityEngine.Object.Destroy(inst2.gameObject);
		mRenderPlanDict[command.param1] = true;
		float scale = float.Parse(command.param4) / 100f;
		if (scale <= 0f)
		{
			scale = DEFAULT_CHARACTER_SCALE;
		}
		while (true)
		{
			bool exist = false;
			int i = 0;
			for (int imax = mPreLoadAnimations.Count; i < imax; i++)
			{
				if (mPreLoadAnimationKeys[i] == command.param1 && mPreLoadAnimations[i].LoadState == ResourceInstance.LOADSTATE.DONE)
				{
					exist = true;
				}
			}
			if (exist)
			{
				break;
			}
			yield return 0;
		}
		inst2 = Util.CreateGameObject("motion", mMotionRoot).AddComponent<Live2DInstance>();
		inst2.Init(command.param1, Vector3.zero, scale, Live2DInstance.PIVOT.CENTER);
		inst2.Position = pos;
		mL2DRender.ResisterInstance(inst2);
		float targetPosX = 0f;
		if (string.Compare(command.param3, "L") == 0)
		{
			targetPosX = -100f;
		}
		else if (string.Compare(command.param3, "R") == 0)
		{
			targetPosX = 100f;
		}
		inst2.SetTargetPoint(new Vector2(targetPosX, 0f), true);
		mCharacterDict.Add(command.param0, inst2);
	}

	public IEnumerator CmdCharacterMove(StoryCommandData command)
	{
		Live2DInstance inst = null;
		while (true)
		{
			mCharacterDict.TryGetValue(command.param0, out inst);
			if (inst != null)
			{
				break;
			}
			yield return 0;
		}
		while (!inst.IsLoadFinished())
		{
			yield return 0;
		}
		Vector3 pos = inst.Position;
		string[] xy = command.param1.Split(',');
		Vector2 targetPos = new Vector2(float.Parse(xy[0]), float.Parse(xy[1]));
		float targetPosX = 0f;
		if (string.Compare(command.param2, "L") == 0)
		{
			targetPosX = -100f;
		}
		else if (string.Compare(command.param2, "R") == 0)
		{
			targetPosX = 100f;
		}
		inst.SetTargetPoint(new Vector2(targetPosX, 0f));
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 360f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float x = (targetPos.x - pos.x) * Mathf.Sin(angle * ((float)Math.PI / 180f));
			float y = (targetPos.y - pos.y) * Mathf.Sin(angle * ((float)Math.PI / 180f));
			inst.Position = new Vector3(pos.x + x, pos.y + y, pos.z);
			yield return 0;
		}
		yield return 0;
	}

	public IEnumerator CmdChangeExpression(StoryCommandData command)
	{
		Live2DInstance inst = null;
		while (true)
		{
			mCharacterDict.TryGetValue(command.param0, out inst);
			if (inst != null)
			{
				break;
			}
			yield return 0;
		}
		while (!inst.IsLoadFinished())
		{
			yield return 0;
		}
		int fadeTime = -1;
		if (!string.IsNullOrEmpty(command.param2))
		{
			fadeTime = (int)(float.Parse(command.param2) * 1000f);
		}
		inst.SetExpression("expressions/" + command.param1 + ".exp.json", fadeTime);
	}

	public IEnumerator CmdChangeMotion(StoryCommandData command)
	{
		Live2DInstance inst = null;
		while (true)
		{
			mCharacterDict.TryGetValue(command.param0, out inst);
			if (inst != null)
			{
				break;
			}
			yield return 0;
		}
		while (!inst.IsLoadFinished())
		{
			yield return 0;
		}
		bool loop2 = false;
		if (string.Compare(command.param2, "LOOP") == 0)
		{
			loop2 = true;
		}
		int fadeTime = -1;
		if (!string.IsNullOrEmpty(command.param5))
		{
			fadeTime = (int)(float.Parse(command.param5) * 1000f);
		}
		inst.StartMotion("motions/" + command.param1 + ".mtn", loop2, fadeTime);
		if (string.Compare(command.param2, "NEXT") == 0)
		{
			while (!inst.IsAnimationFinished())
			{
				yield return 0;
			}
			loop2 = false;
			if (string.Compare(command.param4, "LOOP") == 0)
			{
				loop2 = true;
			}
			inst.StartMotion("motions/" + command.param3 + ".mtn", loop2);
		}
	}

	public IEnumerator CmdImageIn(StoryCommandData command)
	{
		mCommandBusy = true;
		string[] xy = command.param1.Split(',');
		Vector3 pos = new Vector3(float.Parse(xy[0]), float.Parse(xy[1]), IMAGE_Z);
		ParticleSystem particle = GameMain.GetParticle("Particles/StoryDemoImageParticle").GetComponent<ParticleSystem>();
		particle.gameObject.transform.parent = mDemoRoot.transform;
		particle.gameObject.layer = mDemoRoot.layer;
		particle.gameObject.transform.localScale = Vector3.one;
		particle.gameObject.transform.localPosition = new Vector3(pos.x, pos.y, IMAGE_Z - 5f);
		particle.Play();
		float timer2 = 0.5f;
		mImage = Util.CreateSprite("Image", mDemoRoot, mAtlas.Image);
		Util.SetSpriteInfo(mImage, command.param0, mBaseDepth + 10, pos, Vector3.one, false);
		mImage.color = new Color(1f, 1f, 1f, 0f);
		yield return 0;
		float angle = 0f;
		float fadeTime = ((!string.IsNullOrEmpty(command.param2)) ? float.Parse(command.param2) : 0.5f);
		while (angle < 90f)
		{
			angle += Time.deltaTime * (90f / fadeTime);
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mImage.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		particle.Stop();
		timer2 = 1.5f;
		while (timer2 > 0f)
		{
			timer2 -= Time.deltaTime;
			yield return 0;
		}
		UnityEngine.Object.Destroy(particle.gameObject);
		mCommandBusy = false;
	}

	public IEnumerator CmdImageOut(StoryCommandData command)
	{
		mCommandBusy = true;
		if (mImage != null)
		{
			float angle = 90f;
			float fadeTime = ((!string.IsNullOrEmpty(command.param0)) ? float.Parse(command.param0) : 0.5f);
			while (angle > 0f)
			{
				angle -= Time.deltaTime * (90f / fadeTime);
				if (angle < 0f)
				{
					angle = 0f;
				}
				float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
				mImage.color = new Color(1f, 1f, 1f, alpha);
				yield return 0;
			}
			UnityEngine.Object.Destroy(mImage.gameObject);
			mImage = null;
		}
		mCommandBusy = false;
	}

	public IEnumerator CmdImageInId(StoryCommandData command)
	{
		string[] xy = command.param2.Split(',');
		Vector3 pos = new Vector3(float.Parse(xy[0]), float.Parse(xy[1]), IMAGE_Z);
		bool particleSpawn = true;
		if (string.Compare(command.param4, "OFF") == 0)
		{
			particleSpawn = false;
		}
		ParticleSystem particle = null;
		if (particleSpawn)
		{
			particle = GameMain.GetParticle("Particles/StoryDemoImageParticle").GetComponent<ParticleSystem>();
			particle.gameObject.transform.parent = mDemoRoot.transform;
			particle.gameObject.layer = mDemoRoot.layer;
			particle.gameObject.transform.localScale = Vector3.one;
			particle.gameObject.transform.localPosition = new Vector3(pos.x, pos.y, IMAGE_Z - 5f);
			particle.Play();
		}
		float timer2 = 0.5f;
		UISprite image = Util.CreateSprite("Image", mDemoRoot, mAtlas.Image);
		Util.SetSpriteInfo(image, command.param1, mBaseDepth + 10, pos, Vector3.one, false);
		image.color = new Color(1f, 1f, 1f, 0f);
		yield return 0;
		float angle = 0f;
		float fadeTime = ((!string.IsNullOrEmpty(command.param3)) ? float.Parse(command.param3) : 0.5f);
		while (angle < 90f)
		{
			angle += Time.deltaTime * (90f / fadeTime);
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			image.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		if (particle != null)
		{
			particle.Stop();
		}
		timer2 = 1.5f;
		while (timer2 > 0f)
		{
			timer2 -= Time.deltaTime;
			yield return 0;
		}
		if (particle != null)
		{
			UnityEngine.Object.Destroy(particle.gameObject);
		}
		mImageDict.Add(command.param0, image);
	}

	public IEnumerator CmdImageOutId(StoryCommandData command)
	{
		UISprite image = null;
		mImageDict.TryGetValue(command.param0, out image);
		if (!(image != null))
		{
			yield break;
		}
		float angle = 90f;
		float fadeTime = ((!string.IsNullOrEmpty(command.param1)) ? float.Parse(command.param1) : 0.5f);
		while (angle > 0f)
		{
			angle -= Time.deltaTime * (90f / fadeTime);
			if (angle < 0f)
			{
				angle = 0f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			image.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		UnityEngine.Object.Destroy(image.gameObject);
		mImageDict.Remove(command.param0);
	}

	public IEnumerator CmdBgImageInId(StoryCommandData command)
	{
		string[] xy = command.param2.Split(',');
		Vector3 pos = new Vector3(float.Parse(xy[0]), float.Parse(xy[1]), BG_IMAGE_Z);
		xy = command.param3.Split(',');
		Vector3 scale = new Vector3(float.Parse(xy[0]), float.Parse(xy[1]), 1f);
		ResImage atlas = mAtlas;
		if (!string.IsNullOrEmpty(command.param5))
		{
			if (mAdditionalLoadedAtlasDict.ContainsKey(command.param5))
			{
				atlas = mAdditionalLoadedAtlasDict[command.param5];
			}
			else
			{
				atlas = ResourceManager.LoadImageAsync(command.param5);
				while (atlas.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
				mAdditionalLoadedAtlasDict[command.param5] = atlas;
			}
		}
		SimpleTexture image = Util.CreateGameObject("Bg", mAnchorCenter.gameObject).AddComponent<SimpleTexture>();
		image.Init(atlas.Image, command.param1, SimpleTexture.PIVOT.CENTER);
		image.gameObject.transform.localPosition = pos;
		image.SetInitialColor(new Color(1f, 1f, 1f, 0f));
		image.transform.localScale = scale;
		yield return 0;
		float angle = 0f;
		float fadeTime = ((!string.IsNullOrEmpty(command.param4)) ? float.Parse(command.param4) : 0.5f);
		while (angle < 90f)
		{
			angle += Time.deltaTime * (90f / fadeTime);
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			image.SetColor(new Color(1f, 1f, 1f, alpha));
			yield return 0;
		}
		mBgImageDict.Add(command.param0, image);
	}

	public IEnumerator CmdBgImageOutId(StoryCommandData command)
	{
		SimpleTexture image = null;
		mBgImageDict.TryGetValue(command.param0, out image);
		if (!(image != null))
		{
			yield break;
		}
		float angle = 90f;
		float fadeTime = ((!string.IsNullOrEmpty(command.param1)) ? float.Parse(command.param1) : 0.5f);
		while (angle > 0f)
		{
			angle -= Time.deltaTime * (90f / fadeTime);
			if (angle < 0f)
			{
				angle = 0f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			image.SetColor(new Color(1f, 1f, 1f, alpha));
			yield return 0;
		}
		UnityEngine.Object.Destroy(image.gameObject);
		mBgImageDict.Remove(command.param0);
	}

	public IEnumerator CmdFlashScreenIn(StoryCommandData command)
	{
		mFlashScreen = Util.CreateSprite(atlas: ResourceManager.LoadImage("FADE").Image, name: "FlashScreen", parent: mAnchorCenter.gameObject);
		Util.SetSpriteInfo(mFlashScreen, "bg_white", mBaseDepth + 20, new Vector3(0f, 0f, FLASH_SCREEN_Z), Vector3.one, false);
		mFlashScreen.type = UIBasicSprite.Type.Sliced;
		Vector2 screenSize = Util.LogScreenSize();
		mFlashScreen.SetDimensions((int)screenSize.x + 2, (int)screenSize.y + 2);
		float angle = 0f;
		float fadeTime = float.Parse(command.param0);
		while (angle < 90f)
		{
			angle += Time.deltaTime * (90f / fadeTime);
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mFlashScreen.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
	}

	public IEnumerator CmdFlashScreenOut(StoryCommandData command)
	{
		float angle = 0f;
		float fadeTime = float.Parse(command.param0);
		while (angle < 90f)
		{
			angle += Time.deltaTime * (90f / fadeTime);
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = 1f - Mathf.Sin(angle * ((float)Math.PI / 180f));
			mFlashScreen.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		UnityEngine.Object.Destroy(mFlashScreen.gameObject);
		mFlashScreen = null;
	}

	public IEnumerator CmdShake(StoryCommandData command)
	{
		mCommandBusy = true;
		float yOfs = 0f;
		float shakeTime = float.Parse(command.param0);
		while (shakeTime > 0f)
		{
			shakeTime -= Time.deltaTime;
			yOfs = 0f;
			if (mOrientation == ScreenOrientation.Portrait || mOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				yOfs = 160f;
			}
			if (mSubCamera != null && mDemoRoot != null)
			{
				mDemoRoot.transform.localPosition = new Vector3(UnityEngine.Random.Range(0f, 10f), UnityEngine.Random.Range(0f, 10f) + yOfs, 0f);
				yield return 0;
			}
		}
		if (mSubCamera != null && mDemoRoot != null)
		{
			mDemoRoot.transform.localPosition = new Vector3(0f, yOfs, 0f);
		}
		mCommandBusy = false;
	}

	public void CmdEffectFile(StoryCommandData command)
	{
		string[] array = command.param1.Split(',');
		OneShotAnime oneShotAnime = Util.CreateOneShotAnime(pos: new Vector3(float.Parse(array[0]), float.Parse(array[1]), float.Parse(array[2])), name: "Effect", key: command.param0, parent: mDemoRoot.gameObject, scale: Vector3.one, rotateDeg: 0f, delay: 0f);
	}

	public void CmdPlaySe(StoryCommandData command)
	{
		mGame.PlaySe(command.param0, mSeChannel);
		mSeChannel++;
		if (mSeChannel > 3)
		{
			mSeChannel = 0;
		}
	}

	public void CmdPlayBgm(StoryCommandData command)
	{
		mGame.PlayMusic(command.param0, 1, true);
	}

	public void CmdStopBgm(StoryCommandData command)
	{
		mGame.StopMusic(1, 0.2f);
	}

	public IEnumerator CmdTapWait(StoryCommandData command)
	{
		mCommandBusy = true;
		bool tapped = false;
		do
		{
			if (mGame.mTouchDownTrg)
			{
				Vector3 pos = mMainCamera.ScreenToWorldPoint(mGame.mTouchPos);
				tapped = IsValidTap(pos);
			}
			yield return 0;
		}
		while (!tapped);
		mCommandBusy = false;
	}

	public bool IsValidTap(Vector3 worldPos)
	{
		if (mState.GetStatus() != STATE.DO)
		{
			return false;
		}
		bool flag = false;
		if (mScreenShotButton != null)
		{
			Bounds bounds = mScreenShotButton.GetComponent<BoxCollider>().bounds;
			worldPos.z = bounds.center.z;
			if (bounds.Contains(worldPos))
			{
				return false;
			}
		}
		return true;
	}

	public IEnumerator CmdWait(StoryCommandData command)
	{
		mCommandBusy = true;
		float time = float.Parse(command.param0);
		while (time > 0f)
		{
			time -= Time.deltaTime;
			yield return 0;
		}
		mCommandBusy = false;
	}

	private void CloseSubDialogs()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		if (mScreenShotDialog != null)
		{
			mScreenShotDialog.Close();
		}
	}

	public void OnSkipPushed()
	{
		if (mState.GetStatus() == STATE.DO)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mState.Reset(STATE.CANCEL, true);
		}
	}

	public void OnNextPushed()
	{
		if (mState.GetStatus() == STATE.DO)
		{
			mNextPushed = true;
		}
	}

	public void OnScreenShotPushed()
	{
		if (mState.GetStatus() == STATE.DO)
		{
			mState.Reset(STATE.WAIT, true);
			StartCoroutine(TakeScreenShot());
		}
	}

	private void EnableDemoPanel(bool enabled)
	{
		if (mStoryDemoPanel != null)
		{
			NGUITools.SetActive(mStoryDemoPanel.gameObject, enabled);
		}
		EnableDemoButtons(enabled);
	}

	private void EnableDemoButtons(bool enabled)
	{
		if (mSkipButton != null)
		{
			NGUITools.SetActive(mSkipButton.gameObject, enabled);
		}
		if (mScreenShotButton != null)
		{
			NGUITools.SetActive(mScreenShotButton.gameObject, enabled);
		}
	}

	public IEnumerator TakeScreenShot()
	{
		GameObject parent = mAnchorCenter2.gameObject;
		int seqNo = 0;
		string tmpPath2 = null;
		do
		{
			tmpPath2 = Path.Combine(path2: string.Format("ScreenShot_{0}_{1}.png", DateTime.Now.ToString("yyyyMMddHHmmss"), seqNo.ToString("00")), path1: Path.Combine(BIJUnity.getTempPath(), "ss"));
			seqNo++;
			yield return 0;
		}
		while (File.Exists(tmpPath2) && seqNo < 100);
		mGame.PlaySe("SE_CAMERA_SHUTTER", -1);
		yield return StartCoroutine(mGameState.mGameState.CameraFlash());
		mGrayScreenDepthBuf = mGame.mDialogManager.GrayScreenDepth;
		if (string.IsNullOrEmpty(tmpPath2) || File.Exists(tmpPath2))
		{
			EnableDemoPanel(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
			string message = string.Format(Localization.Get("ScreenShot_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("ScreenShot_Error_Title"), message, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnScreenShotErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(mBaseDepth + 100);
			yield break;
		}
		EnableDemoButtons(false);
		yield return StartCoroutine(CaptureScreenShot(tmpPath2));
		if (!File.Exists(tmpPath2))
		{
			EnableDemoPanel(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
			string message2 = string.Format(Localization.Get("ScreenShot_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("ScreenShot_Error_Title"), message2, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnScreenShotErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(mBaseDepth + 100);
		}
		else
		{
			EnableDemoPanel(false);
			mScreenShotDialog = Util.CreateGameObject("ScreenShotDialog", parent).AddComponent<ScreenShotDialog>();
			mScreenShotDialog.Path = tmpPath2;
			mScreenShotDialog.SetClosedCallback(OnScreenShotDialogClosed);
			mScreenShotDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	private IEnumerator CaptureScreenShot(string path)
	{
		yield return new WaitForEndOfFrame();
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		tex.ReadPixels(new Rect(0f, 0f, width, height), 0, 0);
		tex.Apply();
		yield return 0;
		byte[] screenshot = tex.EncodeToPNG();
		using (BIJBinaryWriter _data = new BIJFileBinaryWriter(path, Encoding.UTF8, false, true))
		{
			_data.WriteRaw(screenshot, 0, screenshot.Length);
			_data.Flush();
		}
	}

	private void OnScreenShotDialogClosed()
	{
		UnityEngine.Object.Destroy(mScreenShotDialog.gameObject);
		mScreenShotDialog = null;
		EnableDemoPanel(true);
		StartCoroutine(ReplaceGrayScreen());
		if (mState.GetStatus() == STATE.WAIT)
		{
			mState.Change(STATE.DO);
		}
	}

	private void OnScreenShotErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
		}
		mConfirmDialog = null;
		EnableDemoPanel(true);
		StartCoroutine(ReplaceGrayScreen());
		if (mState.GetStatus() == STATE.WAIT)
		{
			mState.Change(STATE.DO);
		}
	}

	private IEnumerator ReplaceGrayScreen()
	{
		yield return 0;
		mGame.mDialogManager.SetGrayScreenDepth = mGrayScreenDepthBuf;
	}
}
