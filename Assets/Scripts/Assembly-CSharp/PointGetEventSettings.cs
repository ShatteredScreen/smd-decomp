using System;
using System.Collections.Generic;

public class PointGetEventSettings : ICloneable
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("event_id")]
	public short EventID { get; set; }

	[MiniJSONAlias("help_url")]
	public string HelpURL { get; set; }

	[MiniJSONAlias("sp_url")]
	public string SpBonusListURL { get; set; }

	[MiniJSONAlias("linkbanner")]
	public LinkBannerSetting LinkBanner { get; set; }

	[MiniJSONAlias("spchara_settings")]
	public List<SpCharaSettings> SpCharaSetList { get; set; }

	[MiniJSONAlias("cptime_settings")]
	public List<CpTimeSettings> CpTimeSettingsList { get; set; }

	public PointGetEventSettings()
	{
		SpCharaSetList = new List<SpCharaSettings>();
		CpTimeSettingsList = new List<CpTimeSettings>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public PointGetEventSettings Clone()
	{
		return MemberwiseClone() as PointGetEventSettings;
	}
}
