using System;
using System.Collections;
using System.Collections.Generic;

public class GridDataConverter : IMiniJSONConverter
{
	private static string ToSS(object obj)
	{
		return (obj != null) ? obj.ToString() : string.Empty;
	}

	private static T GetEnumValue<T>(object obj)
	{
		return (T)Enum.Parse(typeof(T), ToSS(obj));
	}

	public object ConvertTo(object obj)
	{
		SMGridData sMGridData = obj as SMGridData;
		if (sMGridData == null)
		{
			return null;
		}
		IDictionary<string, object> dictionary = new SortedDictionary<string, object>();
		int width = sMGridData.Width;
		int height = sMGridData.Height;
		dictionary["Width"] = width;
		dictionary["Height"] = height;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				string key = string.Empty + (i * width + j);
				IDictionary<string, object> dictionary2 = new Dictionary<string, object>();
				dictionary2["X"] = j;
				dictionary2["Y"] = i;
				SMTile tile = sMGridData.GetTile(j, i);
				if (tile == null)
				{
					dictionary2["Tile"] = null;
				}
				else
				{
					IDictionary<string, object> dictionary3 = new Dictionary<string, object>();
					dictionary3["Kind"] = tile.Kind.ToString();
					dictionary3["Form"] = tile.Form.ToString();
					dictionary2["Tile"] = dictionary3;
				}
				SMAttributeTile attributeTile = sMGridData.GetAttributeTile(j, i);
				if (attributeTile == null)
				{
					dictionary2["AttributeTile"] = null;
				}
				else
				{
					IDictionary<string, object> dictionary4 = new Dictionary<string, object>();
					IDictionary<string, object> dictionary5 = dictionary4;
					ISMAttributeTile iSMAttributeTile = attributeTile;
					while (iSMAttributeTile != null)
					{
						dictionary5["Kind"] = iSMAttributeTile.Kind.ToString();
						dictionary5["Form"] = iSMAttributeTile.Form.ToString();
						iSMAttributeTile = iSMAttributeTile.Next;
						if (iSMAttributeTile != null)
						{
							IDictionary<string, object> dictionary6 = dictionary5;
							dictionary5 = (IDictionary<string, object>)(dictionary6["Next"] = new Dictionary<string, object>());
						}
					}
					dictionary2["AttributeTile"] = dictionary4;
				}
				SMTileInfo tileInfo = sMGridData.GetTileInfo(j, i);
				if (tileInfo == null)
				{
					dictionary2["SMTileInfo"] = null;
				}
				else
				{
					string json = new MiniJSONSerializer().Serialize(tileInfo);
					dictionary2["SMTileInfo"] = MiniJSON.jsonDecode(json);
				}
				dictionary[key] = dictionary2;
			}
		}
		return dictionary;
	}

	public object ConvertFrom(object json)
	{
		IDictionary dictionary = json as IDictionary;
		if (dictionary == null)
		{
			return null;
		}
		int num = 0;
		int num2 = 0;
		if (dictionary.Contains("Width"))
		{
			num = Convert.ToInt32(dictionary["Width"]);
		}
		if (dictionary.Contains("Height"))
		{
			num2 = Convert.ToInt32(dictionary["Height"]);
		}
		SMGridData sMGridData = new SMGridData(num, num2);
		for (int i = 0; i < num2; i++)
		{
			for (int j = 0; j < num; j++)
			{
				string key = string.Empty + (i * num + j);
				object obj;
				if (!dictionary.Contains(key) || !((obj = dictionary[key]) is IDictionary))
				{
					continue;
				}
				IDictionary dictionary2 = obj as IDictionary;
				int num3 = 0;
				int num4 = 0;
				if (dictionary2.Contains("X"))
				{
					num3 = Convert.ToInt32(dictionary2["X"]);
				}
				if (dictionary2.Contains("Y"))
				{
					num4 = Convert.ToInt32(dictionary2["Y"]);
				}
				if (j != num3 || i != num4)
				{
				}
				if (dictionary2.Contains("Tile") && (obj = dictionary2["Tile"]) is IDictionary)
				{
					IDictionary dictionary3 = obj as IDictionary;
					Def.TILE_KIND enumValue = GetEnumValue<Def.TILE_KIND>(dictionary3["Kind"]);
					Def.TILE_FORM enumValue2 = GetEnumValue<Def.TILE_FORM>(dictionary3["Form"]);
					sMGridData.SetTile(j, i, new SMTile(enumValue, enumValue2));
				}
				if (dictionary2.Contains("AttributeTile") && (obj = dictionary2["AttributeTile"]) is IDictionary)
				{
					IDictionary dictionary4 = obj as IDictionary;
					IDictionary dictionary5 = dictionary4;
					SMAttributeTile sMAttributeTile = null;
					SMAttributeTile sMAttributeTile2 = null;
					while (dictionary5 != null)
					{
						Def.TILE_KIND enumValue3 = GetEnumValue<Def.TILE_KIND>(dictionary5["Kind"]);
						Def.TILE_FORM enumValue4 = GetEnumValue<Def.TILE_FORM>(dictionary5["Form"]);
						SMAttributeTile sMAttributeTile3 = new SMAttributeTile(enumValue3, enumValue4);
						if (sMAttributeTile == null)
						{
							sMAttributeTile = sMAttributeTile3;
						}
						if (sMAttributeTile2 != null)
						{
							sMAttributeTile2.Next = sMAttributeTile3;
						}
						if (dictionary5.Contains("Next"))
						{
							sMAttributeTile2 = sMAttributeTile3;
							dictionary5 = dictionary5["Next"] as IDictionary;
							continue;
						}
						break;
					}
					sMGridData.SetAttributeTile(j, i, sMAttributeTile);
				}
				if (!dictionary2.Contains("SMTileInfo") || !((obj = dictionary2["SMTileInfo"]) is IDictionary))
				{
					continue;
				}
				IDictionary dictionary6 = obj as IDictionary;
				SMTileInfo sMTileInfo = new SMTileInfo();
				List<Def.TILE_KIND> list = new List<Def.TILE_KIND>();
				if (dictionary6.Contains("Stocks") && (obj = dictionary6["Stocks"]) is IList)
				{
					IList list2 = obj as IList;
					if (list2 != null && list2.Count > 0)
					{
						foreach (string item in list2)
						{
							list.Add((Def.TILE_KIND)(int)Enum.Parse(typeof(Def.TILE_KIND), item));
						}
					}
				}
				sMTileInfo.Stocks = list;
				if (dictionary6.Contains("TimerRemain") && (obj = dictionary6["TimerRemain"]) is double)
				{
					double num5 = (double)obj;
					int num6 = (int)num5;
					if (num6 != -1)
					{
						sMTileInfo.TimerRemain = num6;
					}
				}
				if (dictionary6.Contains("FallIngredient") && (obj = dictionary6["FallIngredient"]) is bool)
				{
					bool fallIngredient = (bool)obj;
					sMTileInfo.FallIngredient = fallIngredient;
				}
				if (dictionary6.Contains("FallPresentBox") && (obj = dictionary6["FallPresentBox"]) is bool)
				{
					bool fallPresentBox = (bool)obj;
					sMTileInfo.FallPresentBox = fallPresentBox;
				}
				if (dictionary6.Contains("FallMovableWall") && (obj = dictionary6["FallMovableWall"]) is bool)
				{
					bool fallMovableWall = (bool)obj;
					sMTileInfo.FallMovableWall = fallMovableWall;
				}
				if (dictionary6.Contains("FallPair") && (obj = dictionary6["FallPair"]) is bool)
				{
					bool fallPair = (bool)obj;
					sMTileInfo.FallPair = fallPair;
				}
				if (dictionary6.Contains("FallTimerBomb") && (obj = dictionary6["FallTimerBomb"]) is bool)
				{
					bool fallTimerBomb = (bool)obj;
					sMTileInfo.FallTimerBomb = fallTimerBomb;
				}
				if (dictionary6.Contains("TileAttribute_BLANK") && (obj = dictionary6["TileAttribute_BLANK"]) is bool)
				{
					bool tileAttribute_BLANK = (bool)obj;
					sMTileInfo.TileAttribute_BLANK = tileAttribute_BLANK;
				}
				if (dictionary6.Contains("TileAttribute_FIND") && (obj = dictionary6["TileAttribute_FIND"]) is bool)
				{
					bool tileAttribute_FIND = (bool)obj;
					sMTileInfo.TileAttribute_FIND = tileAttribute_FIND;
				}
				if (dictionary6.Contains("FindType") && (obj = dictionary6["FindType"]) is double)
				{
					int findType = (int)obj;
					sMTileInfo.FindType = (Def.TILE_KIND)findType;
				}
				if (dictionary6.Contains("FindImageName") && (obj = dictionary6["FindImageName"]) is string)
				{
					string findImageName = (string)obj;
					sMTileInfo.FindImageName = findImageName;
				}
				if (dictionary6.Contains("FindSizeX") && (obj = dictionary6["FindSizeX"]) is double)
				{
					int findSizeX = (int)obj;
					sMTileInfo.FindSizeX = findSizeX;
				}
				if (dictionary6.Contains("FindSizeY") && (obj = dictionary6["FindSizeY"]) is double)
				{
					int findSizeY = (int)obj;
					sMTileInfo.FindSizeY = findSizeY;
				}
				if (dictionary6.Contains("FindRotate") && (obj = dictionary6["FindRotate"]) is double)
				{
					int findRotate = (int)obj;
					sMTileInfo.FindRotate = findRotate;
				}
				if (dictionary6.Contains("WarpIndex") && (obj = dictionary6["WarpIndex"]) is double)
				{
					int warpIndex = (int)obj;
					sMTileInfo.WarpIndex = warpIndex;
				}
				if (dictionary6.Contains("WarpOutPosX") && (obj = dictionary6["WarpOutPosX"]) is double)
				{
					int warpOutPosX = (int)obj;
					sMTileInfo.WarpOutPosX = warpOutPosX;
				}
				if (dictionary6.Contains("WarpOutPosY") && (obj = dictionary6["WarpOutPosY"]) is double)
				{
					int warpOutPosY = (int)obj;
					sMTileInfo.WarpOutPosY = warpOutPosY;
				}
				if (dictionary6.Contains("MakeObstacleArea") && (obj = dictionary6["MakeObstacleArea"]) is bool)
				{
					bool makeObstacleArea = (bool)obj;
					sMTileInfo.MakeObstacleArea = makeObstacleArea;
				}
				if (dictionary6.Contains("TileAttribute2_FAMILIAR_SPIRIT") && (obj = dictionary6["TileAttribute2_FAMILIAR_SPIRIT"]) is bool)
				{
					bool tileAttribute2_FAMILIAR_SPIRIT = (bool)obj;
					sMTileInfo.TileAttribute2_FAMILIAR_SPIRIT = tileAttribute2_FAMILIAR_SPIRIT;
				}
				if (dictionary6.Contains("FamiliarSpiritID") && (obj = dictionary6["FamiliarSpiritID"]) is byte)
				{
					byte familiarSpiritID = (byte)obj;
					sMTileInfo.FamiliarSpiritID = familiarSpiritID;
				}
				if (dictionary6.Contains("DianaWayKindNum") && (obj = dictionary6["DianaWayKindNum"]) is byte)
				{
					byte dianaWayKindNum = (byte)obj;
					sMTileInfo.DianaWayKindNum = dianaWayKindNum;
				}
				sMGridData.SetTileInfo(j, i, sMTileInfo);
			}
		}
		return sMGridData;
	}
}
