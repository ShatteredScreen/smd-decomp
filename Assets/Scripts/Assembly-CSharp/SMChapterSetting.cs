public class SMChapterSetting : SMMapPageSettingBase
{
	public short ChapterID;

	public string ChapterTitle = string.Empty;

	public int FirstStage
	{
		get
		{
			return mStageNo;
		}
	}

	public int LastStage
	{
		get
		{
			return mSubNo;
		}
	}

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteShort((short)mCourseNo);
		a_writer.WriteShort(ChapterID);
		a_writer.WriteInt(mStageNo);
		a_writer.WriteInt(mSubNo);
		a_writer.WriteUTF(OpenDemo);
		a_writer.WriteUTF(ClearDemo);
		a_writer.WriteUTF(ChapterTitle);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		mCourseNo = a_reader.ReadShort();
		ChapterID = a_reader.ReadShort();
		mStageNo = a_reader.ReadInt();
		mSubNo = a_reader.ReadInt();
		OpenDemo = a_reader.ReadUTF();
		ClearDemo = a_reader.ReadUTF();
		ChapterTitle = a_reader.ReadUTF();
	}
}
