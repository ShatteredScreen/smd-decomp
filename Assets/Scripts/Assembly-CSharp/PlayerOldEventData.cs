using System;

public class PlayerOldEventData : SMJsonData, ICloneable
{
	private short mExtendCount;

	public short Version { get; set; }

	public short EventID { get; set; }

	public DateTime CloseTime { get; set; }

	public bool ExtendedOfferDone { get; set; }

	public short ExtendCount
	{
		get
		{
			return mExtendCount;
		}
		set
		{
			if (value > 9999)
			{
				mExtendCount = 9999;
			}
			else
			{
				mExtendCount = value;
			}
		}
	}

	public bool HasOpened { get; set; }

	public PlayerOldEventData()
	{
		Version = 1290;
		CloseTime = new DateTime(0L);
		ExtendedOfferDone = false;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		data.WriteShort(EventID);
		data.WriteDateTime(CloseTime);
		data.WriteBool(ExtendedOfferDone);
		data.WriteShort(ExtendCount);
		data.WriteBool(HasOpened);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		Version = data.ReadShort();
		EventID = data.ReadShort();
		CloseTime = data.ReadDateTime();
		ExtendedOfferDone = data.ReadBool();
		ExtendCount = data.ReadShort();
		if (reqVersion >= 1180)
		{
			HasOpened = data.ReadBool();
		}
	}

	public override string SerializeToJson()
	{
		return string.Empty;
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerOldEventData obj = new PlayerOldEventData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public PlayerOldEventData Clone()
	{
		return MemberwiseClone() as PlayerOldEventData;
	}
}
