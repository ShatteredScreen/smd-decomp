using System;
using System.Collections;
using System.Collections.Generic;

public class DateTimeDictConverter : IMiniJSONConverter
{
	private static string ToSS(object obj)
	{
		return (obj != null) ? obj.ToString() : string.Empty;
	}

	private static T GetEnumValue<T>(object obj)
	{
		return (T)Enum.Parse(typeof(T), ToSS(obj));
	}

	public object ConvertTo(object a_obj)
	{
		Dictionary<string, DateTime> dictionary = a_obj as Dictionary<string, DateTime>;
		if (dictionary == null)
		{
			return null;
		}
		IDictionary<string, object> dictionary2 = new SortedDictionary<string, object>();
		foreach (KeyValuePair<string, DateTime> item in dictionary)
		{
			string key = item.Key;
			DateTime value = item.Value;
			string value2 = new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(value);
			dictionary2.Add(key, value2);
		}
		return dictionary2;
	}

	public object ConvertFrom(object a_json)
	{
		IDictionary dictionary = a_json as IDictionary;
		if (dictionary == null)
		{
			return null;
		}
		Dictionary<string, DateTime> result = new Dictionary<string, DateTime>();
		foreach (DictionaryEntry item in dictionary)
		{
			string key = item.Key as string;
			string s = item.Value as string;
			try
			{
				DateTime dateTime = DateTime.Parse(s).ToLocalTime();
				dictionary.Add(key, dateTime);
			}
			catch (Exception)
			{
			}
		}
		return result;
	}
}
