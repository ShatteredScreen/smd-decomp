using System.Collections;
using UnityEngine;

public class EventLabyrinthCourseLine : MapLine
{
	public enum StatusChangeType
	{
		NONE = 0,
		TO_UNLOCK = 1
	}

	public const int FRAME_LOCK = 0;

	public const int FRAME_UNLOCK = 0;

	public const int FRAME_UNLOCK_LOOP = 60;

	protected short mCourseID;

	private StatusChangeType mLineChangeType;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public void SetStatusTypeReserved(StatusChangeType a_flg)
	{
		mLineChangeType = a_flg;
	}

	public override void Init(int _counter, string _name, BIJImage atlas, int a_mapIndex, float x, float y, Vector3 scale, float a_rad, string a_animeKey)
	{
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mAtlas = atlas;
		mScale = scale;
		base._zrot = a_rad;
		float order = float.Parse(_name);
		float mAP_LINE_Z = Def.MAP_LINE_Z;
		base._pos = new Vector3(x, y, mAP_LINE_Z);
		Player mPlayer = mGame.mPlayer;
		mSeries = mPlayer.Data.CurrentSeries;
		int num = (base.StageNo = Def.GetStageNoByRouteOrder(order));
		base.gameObject.name = "line_" + num;
		Player.STAGE_STATUS sTAGE_STATUS = Player.STAGE_STATUS.NOOPEN;
		mChangeParts = null;
		if (mGame.mEventMode)
		{
			PlayerEventData data;
			mPlayer.Data.GetMapData(mSeries, mPlayer.Data.CurrentEventID, out data);
			if (data != null)
			{
				sTAGE_STATUS = data.LabyrinthData.CourseLineStatus[(short)num];
			}
		}
		else
		{
			sTAGE_STATUS = mPlayer.GetLineStatus(mSeries, num);
		}
		int num2 = -1;
		bool flag = false;
		Player.STAGE_STATUS sTAGE_STATUS2 = sTAGE_STATUS;
		if (sTAGE_STATUS2 == Player.STAGE_STATUS.NOOPEN || sTAGE_STATUS2 == Player.STAGE_STATUS.LOCK)
		{
			num2 = 0;
			flag = true;
		}
		else if (mLineChangeType == StatusChangeType.TO_UNLOCK)
		{
			num2 = 0;
			flag = true;
		}
		else
		{
			num2 = 60;
		}
		PlayAnime(AnimeKey, false);
		_sprite.AnimFrame = num2;
		_sprite.PlayAtStart = false;
		if (flag)
		{
			_sprite.AnimationFinished = null;
			_sprite.Pause();
		}
		else
		{
			_sprite.AnimationFinished = OnAnimationFinished;
			_sprite.Play();
		}
		IsLockTapAnime = false;
		HasTapAnime = false;
		mFirstInitialized = true;
	}

	public IEnumerator UnlockCourse()
	{
		SetStatusTypeReserved(StatusChangeType.NONE);
		_sprite.AnimFrame = 0f;
		_sprite.AnimationFinished = OnAnimationFinished;
		_sprite.Play();
		mGame.PlaySe("SE_BINGO_BINGO_STAGE_EFFECT", -1);
		SsPart part = _sprite.GetPart("over_add");
		if (part == null)
		{
			yield break;
		}
		bool animeFinished = false;
		SsPart.KeyframeCallback handler2 = null;
		handler2 = delegate(SsPart a_part, SsAttrValueInterface a_val)
		{
			SsUserDataKeyValue ssUserDataKeyValue = a_val as SsUserDataKeyValue;
			if (ssUserDataKeyValue.String.CompareTo("CLEAR_FINISH") == 0)
			{
				a_part.OnUserDataKey -= handler2;
				animeFinished = true;
			}
		};
		part.OnUserDataKey += handler2;
		float _start = Time.realtimeSinceStartup;
		while (!animeFinished)
		{
			float timeSpan = Time.realtimeSinceStartup - _start;
			if (timeSpan > 10f)
			{
				animeFinished = true;
			}
			if (animeFinished)
			{
				break;
			}
			yield return null;
		}
	}

	public override void OnAnimationFinished(SsSprite sprite)
	{
		sprite.AnimFrame = 60f;
		sprite.Play();
	}
}
