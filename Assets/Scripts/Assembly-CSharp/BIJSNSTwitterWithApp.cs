using System.Collections;
using UnityEngine;

public class BIJSNSTwitterWithApp : BIJSNSAppSwitcher
{
	public override SNS Kind
	{
		get
		{
			return SNS.Twitter;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		if (type == 7 || type == 8)
		{
			return true;
		}
		return false;
	}

	private IEnumerator BIJSNSTwitterWithApp_PluginFailed(int seqno, string message, object result)
	{
		yield return new WaitForSeconds(0.5f);
		SetSpawn(0);
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitterWithApp.PluginFailed failed: seqno=" + seqno + " current=" + base.CurrentSeqNo + " message=" + message);
		if (base.CurrentSeqNo == seqno)
		{
			base.CurrentRes = new Response(1, message, result);
			ChangeStep(STEP.COMM_RESULT);
		}
	}

	public override void PostMessage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitterWithApp.PostMessage");
		if (spawn)
		{
			CallCallback(handler, new Response(1, "Twitter already invoked.", null), userdata);
			return;
		}
		int type = 7;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string text = BIJSNS.ToSS(postdata.Message);
			string imageUrl = BIJSNS.ToSS(postdata.ImageURL);
			BIJSNS.dbg("@@@ SNS @@@ " + text);
			bool flag = BIJUnity.invokeTwitterWithMessage(title, text, imageUrl);
			BIJSNS.log("SNSInvokeTwitterWithMessage: " + flag);
			if (!flag)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSTwitterWithApp_PluginFailed(num, "Twitter invoke failed at posting", null));
			}
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "Twitter not requested", null), userdata);
		}
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitterWithApp.UploadImage");
		if (spawn)
		{
			CallCallback(handler, new Response(1, "Twitter already invoked.", null), userdata);
			return;
		}
		int type = 9;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string text = BIJSNS.ToSS(postdata.Message);
			string imageUrl = BIJSNS.ToSS(postdata.ImageURL);
			BIJSNS.dbg("@@@ SNS @@@ " + text);
			bool flag = BIJUnity.invokeTwitterWithMessage(title, text, imageUrl);
			BIJSNS.log("SNSInvokeTwitterWithMessage: " + flag);
			if (!flag)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSTwitterWithApp_PluginFailed(num, "Twitter invoke failed at posting", null));
			}
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "Twitter not requested", null), userdata);
		}
	}

	public override void Invite(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitterWithApp.Invite");
		if (spawn)
		{
			CallCallback(handler, new Response(1, "Twitter already invoked.", null), userdata);
			return;
		}
		int type = 8;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string text = BIJSNS.ToSS(postdata.Message);
			string imageUrl = BIJSNS.ToSS(postdata.ImageURL);
			BIJSNS.dbg("@@@ SNS @@@ " + text);
			bool flag = BIJUnity.invokeTwitterWithMessage(title, text, imageUrl);
			BIJSNS.log("SNSInvokeTwitterWithMessage: " + flag);
			if (!flag)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSTwitterWithApp_PluginFailed(num, "Twitter invoke failed at inviting", null));
			}
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "Twitter not requested", null), userdata);
		}
	}
}
