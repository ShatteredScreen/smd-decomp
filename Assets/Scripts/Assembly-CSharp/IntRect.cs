public class IntRect
{
	public int xMin;

	public int yMin;

	public int xMax;

	public int yMax;

	public int x
	{
		get
		{
			return xMin;
		}
		set
		{
			xMin = value;
		}
	}

	public int y
	{
		get
		{
			return yMin;
		}
		set
		{
			yMin = value;
		}
	}

	public int width
	{
		get
		{
			return xMax - xMin;
		}
		set
		{
			xMax = xMin + value;
		}
	}

	public int height
	{
		get
		{
			return yMax - yMin;
		}
		set
		{
			yMax = yMin + value;
		}
	}

	public IntRect(int xMin, int yMin, int xMax, int yMax)
	{
		this.xMin = xMin;
		this.yMin = yMin;
		this.xMax = xMax;
		this.yMax = yMax;
	}

	public override string ToString()
	{
		return string.Format("MinX={{X={0} Y={1}}} Max={{X={2} Y={3}}}", xMin, yMin, xMax, yMax);
	}
}
