public class BIJSNSAppSwitcher : BIJSNSNone
{
	protected bool spawn;

	protected bool backtogame;

	protected int spawnSeqNo;

	protected virtual bool IsEnableSwitching()
	{
		return true;
	}

	public override void Update()
	{
		base.Update();
		if (IsEnableSwitching() && spawn && backtogame)
		{
			if (base.CurrentSeqNo == spawnSeqNo)
			{
				base.CurrentRes = OnBackToGameResult(base.CurrentReq);
				ChangeStep(STEP.COMM_RESULT);
				spawnSeqNo = 0;
			}
			spawn = false;
		}
	}

	protected virtual void SetSpawn(int seqno)
	{
		if (seqno == 0)
		{
			spawn = false;
			backtogame = false;
			spawnSeqNo = 0;
		}
		else
		{
			spawn = true;
			backtogame = false;
			spawnSeqNo = seqno;
		}
	}

	public override void OnResume()
	{
		backtogame = true;
	}

	public override void OnSuspned()
	{
		backtogame = false;
	}

	protected virtual Response OnBackToGameResult(Request req)
	{
		return new Response(0, "Back to game", null);
	}
}
