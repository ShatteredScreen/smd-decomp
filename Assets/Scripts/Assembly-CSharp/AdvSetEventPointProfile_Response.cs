using System;

public class AdvSetEventPointProfile_Response : ICloneable
{
	[MiniJSONAlias("event_id")]
	public short event_id { get; set; }

	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvSetEventPointProfile_Response Clone()
	{
		return MemberwiseClone() as AdvSetEventPointProfile_Response;
	}
}
