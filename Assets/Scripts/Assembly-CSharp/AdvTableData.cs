using System;

public class AdvTableData : ICloneable
{
	[MiniJSONAlias("id")]
	public int id { get; set; }

	[MiniJSONAlias("chara_name")]
	public string chara_name { get; set; }

	[MiniJSONAlias("posing_name")]
	public string posing_name { get; set; }

	[MiniJSONAlias("rarity")]
	public byte rarity { get; set; }

	[MiniJSONAlias("attr")]
	public byte attr { get; set; }

	[MiniJSONAlias("skill")]
	public short skill { get; set; }

	[MiniJSONAlias("max_lvl")]
	public int max_lvl { get; set; }

	[MiniJSONAlias("max_skill_lvl")]
	public int max_skill_lvl { get; set; }

	[MiniJSONAlias("default_hp")]
	public short default_hp { get; set; }

	[MiniJSONAlias("default_atk")]
	public short default_atk { get; set; }

	[MiniJSONAlias("default_def")]
	public short default_def { get; set; }

	[MiniJSONAlias("max_hp")]
	public short max_hp { get; set; }

	[MiniJSONAlias("max_atk")]
	public short max_atk { get; set; }

	[MiniJSONAlias("max_def")]
	public short max_def { get; set; }

	[MiniJSONAlias("read_chara_name")]
	public string read_chara_name { get; set; }

	[MiniJSONAlias("read_posing_name")]
	public string read_posing_name { get; set; }

	[MiniJSONAlias("chara")]
	public int chara { get; set; }

	[MiniJSONAlias("series")]
	public byte series { get; set; }

	[MiniJSONAlias("atlas")]
	public short atlas { get; set; }

	[MiniJSONAlias("image")]
	public short image { get; set; }

	[MiniJSONAlias("live2d")]
	public int live2d { get; set; }

	[MiniJSONAlias("reward_id")]
	public int reward_id { get; set; }

	[MiniJSONAlias("revision")]
	public int revision { get; set; }

	[MiniJSONAlias("lvl")]
	public int lvl { get; set; }

	[MiniJSONAlias("lower_xp")]
	public int lower_xp { get; set; }

	[MiniJSONAlias("upper_xp")]
	public int upper_xp { get; set; }

	[MiniJSONAlias("name")]
	public string name { get; set; }

	[MiniJSONAlias("description")]
	public string description { get; set; }

	[MiniJSONAlias("detail")]
	public string detail { get; set; }

	[MiniJSONAlias("skill_type")]
	public byte skill_type { get; set; }

	[MiniJSONAlias("default_gauge")]
	public short default_gauge { get; set; }

	[MiniJSONAlias("default_param1")]
	public short default_param1 { get; set; }

	[MiniJSONAlias("default_param2")]
	public short default_param2 { get; set; }

	[MiniJSONAlias("default_param3")]
	public short default_param3 { get; set; }

	[MiniJSONAlias("default_param4")]
	public short default_param4 { get; set; }

	[MiniJSONAlias("default_param5")]
	public short default_param5 { get; set; }

	[MiniJSONAlias("default_param6")]
	public short default_param6 { get; set; }

	[MiniJSONAlias("default_param7")]
	public short default_param7 { get; set; }

	[MiniJSONAlias("default_param8")]
	public short default_param8 { get; set; }

	[MiniJSONAlias("max_gauge")]
	public short max_gauge { get; set; }

	[MiniJSONAlias("max_param1")]
	public short max_param1 { get; set; }

	[MiniJSONAlias("max_param2")]
	public short max_param2 { get; set; }

	[MiniJSONAlias("max_param3")]
	public short max_param3 { get; set; }

	[MiniJSONAlias("max_param4")]
	public short max_param4 { get; set; }

	[MiniJSONAlias("max_param5")]
	public short max_param5 { get; set; }

	[MiniJSONAlias("max_param6")]
	public short max_param6 { get; set; }

	[MiniJSONAlias("max_param7")]
	public short max_param7 { get; set; }

	[MiniJSONAlias("max_param8")]
	public short max_param8 { get; set; }

	[MiniJSONAlias("weight")]
	public short weight { get; set; }

	[MiniJSONAlias("skill_effect_id")]
	public short skill_effect_id { get; set; }

	[MiniJSONAlias("read_name")]
	public string read_name { get; set; }

	[MiniJSONAlias("kind")]
	public byte kind { get; set; }

	[MiniJSONAlias("atlas_name")]
	public string atlas_name { get; set; }

	[MiniJSONAlias("anime_red")]
	public string anime_red { get; set; }

	[MiniJSONAlias("sound_red")]
	public string sound_red { get; set; }

	[MiniJSONAlias("anime_green")]
	public string anime_green { get; set; }

	[MiniJSONAlias("sound_green")]
	public string sound_green { get; set; }

	[MiniJSONAlias("anime_blue")]
	public string anime_blue { get; set; }

	[MiniJSONAlias("sound_blue")]
	public string sound_blue { get; set; }

	[MiniJSONAlias("anime_yellow")]
	public string anime_yellow { get; set; }

	[MiniJSONAlias("sound_yellow")]
	public string sound_yellow { get; set; }

	[MiniJSONAlias("anime_purple")]
	public string anime_purple { get; set; }

	[MiniJSONAlias("sound_purple")]
	public string sound_purple { get; set; }

	[MiniJSONAlias("scale")]
	public float scale { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvTableData Clone()
	{
		return MemberwiseClone() as AdvTableData;
	}
}
