using UnityEngine;

public class BingoGuideDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	private class RewardSpriteData
	{
		public string mImgName;

		public int mNum;

		public RewardSpriteData()
		{
			mImgName = string.Empty;
			mNum = 0;
		}
	}

	public delegate void OnDialogClosed(int i);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private Vector2 mLogScreen = Util.LogScreenSize();

	private int mGuideNum;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		mLogScreen = Util.LogScreenSize();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public void Init(int _guideNum)
	{
		mGuideNum = _guideNum;
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		BIJImage image2 = ResourceManager.LoadImage(currentEventPageData.EventSetting.MapAtlasKey).Image;
		string titleDescKey = string.Empty;
		string text = string.Empty;
		string text2 = string.Empty;
		switch (mGuideNum)
		{
		case 0:
			titleDescKey = Localization.Get("BINGO_Guide00_Title");
			text = Localization.Get("BINGO_Guide00_Desc");
			text2 = "LC_label_howto04";
			break;
		case 1:
			titleDescKey = Localization.Get("BINGO_Guide01_Title");
			text = Localization.Get("BINGO_Guide01_Desc");
			text2 = "LC_label_howto05";
			break;
		case 2:
			titleDescKey = Localization.Get("BINGO_Guide02_Title");
			text = Localization.Get("BINGO_Guide02_Desc");
			text2 = "LC_label_howto03";
			break;
		case 3:
			titleDescKey = Localization.Get("BINGO_Guide03_Title");
			text = Localization.Get("BINGO_Guide03_Desc");
			text2 = "LC_label_howto00";
			break;
		case 4:
			titleDescKey = Localization.Get("BINGO_Guide04_Title");
			text = Localization.Get("BINGO_Guide04_Desc");
			text2 = "LC_label_howto01";
			break;
		case 5:
			titleDescKey = Localization.Get("BINGO_Guide05_Title");
			text = Localization.Get("BINGO_Guide05_Desc");
			text2 = "LC_label_howto03";
			break;
		}
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		if (text2 != string.Empty)
		{
			UISprite sprite = Util.CreateSprite("Img", base.gameObject, image2);
			Util.SetSpriteInfo(sprite, text2, mBaseDepth + 2, new Vector3(0f, 67f, 0f), Vector3.one, false);
			UILabel uILabel = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(0f, -44f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(500, 100);
			uILabel.spacingY = 8;
		}
		else
		{
			UILabel label = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(label, text, mBaseDepth + 2, new Vector3(0f, 20f, 0f), 27, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(label, Def.DEFAULT_MESSAGE_COLOR);
		}
		UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -131f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		mState.Reset(STATE.WAIT, true);
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mGuideNum);
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
