using System;
using System.Collections.Generic;

public class PlayerMapData : SMJsonData, ICloneable
{
	public int Version { get; set; }

	public int CurrentLevel { get; set; }

	public int OpenNoticeLevel { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(Player.STAGE_STATUS))]
	public Dictionary<int, Player.STAGE_STATUS> StageStatus { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(Player.STAGE_STATUS))]
	public Dictionary<int, Player.STAGE_STATUS> LineStatus { get; set; }

	[MiniJSONArray(typeof(PlayerClearData))]
	public List<PlayerClearData> StageClearData { get; set; }

	[MiniJSONArray(typeof(PlayerStageData))]
	public List<PlayerStageData> StageChallengeData { get; set; }

	public int PlayableMaxLevel { get; set; }

	public int LastClearedLevel { get; set; }

	public int AvatarStagePosition { get; set; }

	public float AvatarPosition { get; set; }

	public string AvatarNodeName { get; set; }

	public int NextRoadBlockLevel { get; set; }

	public int RoadBlockReachLevel { get; set; }

	public DateTime RoadBlockReachTime { get; set; }

	public List<short> UnlockedRoadBlock { get; set; }

	[MiniJSONArray(typeof(PlayerClearData))]
	public List<PlayerClearData> TempStageClearData { get; set; }

	[MiniJSONArray(typeof(PlayerStageData))]
	public List<PlayerStageData> TempStageChallengeData { get; set; }

	public int MaxLevel { get; set; }

	public int MaxMainStageNum { get; set; }

	public PlayerMapData()
	{
		Version = 1290;
		StageStatus = new Dictionary<int, Player.STAGE_STATUS>();
		LineStatus = new Dictionary<int, Player.STAGE_STATUS>();
		StageClearData = new List<PlayerClearData>();
		StageChallengeData = new List<PlayerStageData>();
		CurrentLevel = Def.GetStageNo(Constants.FIRST_LEVEL, 0);
		StageStatus.Clear();
		LineStatus.Clear();
		OpenNoticeLevel = 0;
		AvatarPosition = 0f;
		AvatarNodeName = string.Empty;
		AvatarStagePosition = OpenNoticeLevel;
		StageClearData.Clear();
		StageChallengeData.Clear();
		PlayableMaxLevel = CurrentLevel;
		LastClearedLevel = 0;
		NextRoadBlockLevel = 0;
		RoadBlockReachLevel = 0;
		RoadBlockReachTime = DateTimeUtil.UnitLocalTimeEpoch;
		MaxLevel = 0;
		UnlockedRoadBlock = new List<short>();
		TempStageChallengeData = new List<PlayerStageData>();
		TempStageClearData = new List<PlayerClearData>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public bool HasClearInfo()
	{
		return StageClearData.Count > 0;
	}

	public bool UpdateUnlockedRoadBlock(int a_rb)
	{
		bool result = false;
		short item = (short)a_rb;
		if (!UnlockedRoadBlock.Contains(item))
		{
			UnlockedRoadBlock.Add(item);
			UnlockedRoadBlock.Sort();
			result = true;
		}
		return result;
	}

	public bool IsUnlockedRoadBlock(int a_rb)
	{
		short item = (short)a_rb;
		return UnlockedRoadBlock.Contains(item);
	}

	public int LatestUnlockedRoadBlock()
	{
		List<short> list = new List<short>(UnlockedRoadBlock);
		if (list.Count > 0)
		{
			list.Sort((short a, short b) => a - b);
			return list[list.Count - 1];
		}
		return 0;
	}

	public int GetClearedMainStage()
	{
		int num = 0;
		for (int i = 0; i < StageClearData.Count; i++)
		{
			PlayerClearData playerClearData = StageClearData[i];
			int a_main;
			int a_sub;
			Def.SplitStageNo(playerClearData.StageNo, out a_main, out a_sub);
			if (a_sub == 0)
			{
				num++;
			}
		}
		return num;
	}

	public int GetClearedStarCount()
	{
		int num = 0;
		for (int i = 0; i < StageClearData.Count; i++)
		{
			PlayerClearData playerClearData = StageClearData[i];
			num += playerClearData.GotStars;
		}
		return num;
	}

	public PlayerClearData GetClearData(int a_stageNo)
	{
		for (int i = 0; i < StageClearData.Count; i++)
		{
			PlayerClearData playerClearData = StageClearData[i];
			if (playerClearData.StageNo == a_stageNo)
			{
				return playerClearData;
			}
		}
		return null;
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		data.WriteInt(CurrentLevel);
		data.WriteInt(OpenNoticeLevel);
		data.WriteInt(AvatarStagePosition);
		data.WriteFloat(AvatarPosition);
		data.WriteUTF(AvatarNodeName);
		data.WriteInt(StageStatus.Count);
		foreach (KeyValuePair<int, Player.STAGE_STATUS> item in StageStatus)
		{
			int key = item.Key;
			Player.STAGE_STATUS value = item.Value;
			data.WriteInt(key);
			data.WriteInt((int)value);
		}
		data.WriteInt(LineStatus.Count);
		foreach (KeyValuePair<int, Player.STAGE_STATUS> item2 in LineStatus)
		{
			int key2 = item2.Key;
			Player.STAGE_STATUS value2 = item2.Value;
			data.WriteInt(key2);
			data.WriteInt((int)value2);
		}
		data.WriteInt(StageClearData.Count);
		foreach (PlayerClearData stageClearDatum in StageClearData)
		{
			PlayerClearData playerClearData = stageClearDatum;
			playerClearData.SerializeToBinary(data);
		}
		data.WriteInt(StageChallengeData.Count);
		foreach (PlayerStageData stageChallengeDatum in StageChallengeData)
		{
			PlayerStageData playerStageData = stageChallengeDatum;
			playerStageData.SerializeToBinary(data);
		}
		data.WriteInt(PlayableMaxLevel);
		data.WriteInt(LastClearedLevel);
		data.WriteInt(NextRoadBlockLevel);
		data.WriteInt(RoadBlockReachLevel);
		data.WriteDateTime(RoadBlockReachTime);
		data.WriteInt(MaxLevel);
		data.WriteInt(UnlockedRoadBlock.Count);
		for (int i = 0; i < UnlockedRoadBlock.Count; i++)
		{
			data.WriteShort(UnlockedRoadBlock[i]);
		}
		data.WriteInt(TempStageClearData.Count);
		foreach (PlayerClearData tempStageClearDatum in TempStageClearData)
		{
			PlayerClearData playerClearData2 = tempStageClearDatum;
			playerClearData2.SerializeToBinary(data);
		}
		data.WriteInt(TempStageChallengeData.Count);
		foreach (PlayerStageData tempStageChallengeDatum in TempStageChallengeData)
		{
			PlayerStageData playerStageData2 = tempStageChallengeDatum;
			playerStageData2.SerializeToBinary(data);
		}
	}

	public void DeserializeFromBinary<ClearT, ChallengeT>(BIJBinaryReader data, int reqVersion) where ClearT : PlayerClearData, new() where ChallengeT : PlayerStageData, new()
	{
		short num = data.ReadShort();
		CurrentLevel = data.ReadInt();
		OpenNoticeLevel = data.ReadInt();
		AvatarStagePosition = data.ReadInt();
		AvatarPosition = data.ReadFloat();
		AvatarNodeName = data.ReadUTF();
		StageStatus.Clear();
		int num2 = data.ReadInt();
		for (int i = 0; i < num2; i++)
		{
			int key = data.ReadInt();
			int num3 = data.ReadInt();
			Player.STAGE_STATUS value = (Player.STAGE_STATUS)num3;
			StageStatus[key] = value;
		}
		LineStatus.Clear();
		num2 = data.ReadInt();
		for (int j = 0; j < num2; j++)
		{
			int key2 = data.ReadInt();
			int num4 = data.ReadInt();
			Player.STAGE_STATUS value2 = (Player.STAGE_STATUS)num4;
			LineStatus[key2] = value2;
		}
		StageClearData.Clear();
		num2 = data.ReadInt();
		for (int k = 0; k < num2; k++)
		{
			ClearT item = new ClearT();
			item.DeserializeFromBinary(data, reqVersion);
			StageClearData.Add(item);
		}
		StageChallengeData.Clear();
		num2 = data.ReadInt();
		for (int l = 0; l < num2; l++)
		{
			ChallengeT item2 = new ChallengeT();
			item2.DeserializeFromBinary(data, reqVersion);
			StageChallengeData.Add(item2);
		}
		PlayableMaxLevel = data.ReadInt();
		LastClearedLevel = data.ReadInt();
		NextRoadBlockLevel = data.ReadInt();
		RoadBlockReachLevel = data.ReadInt();
		RoadBlockReachTime = data.ReadDateTime();
		MaxLevel = data.ReadInt();
		num2 = data.ReadInt();
		for (int m = 0; m < num2; m++)
		{
			UnlockedRoadBlock.Add(data.ReadShort());
		}
		if (num >= 1051)
		{
			TempStageClearData.Clear();
			num2 = data.ReadInt();
			for (int n = 0; n < num2; n++)
			{
				ClearT item3 = new ClearT();
				item3.DeserializeFromBinary(data, reqVersion);
				TempStageClearData.Add(item3);
			}
			TempStageChallengeData.Clear();
			num2 = data.ReadInt();
			for (int num5 = 0; num5 < num2; num5++)
			{
				ChallengeT item4 = new ChallengeT();
				item4.DeserializeFromBinary(data, reqVersion);
				TempStageChallengeData.Add(item4);
			}
		}
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerMapData obj = new PlayerMapData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
	}

	public PlayerMapData Clone()
	{
		return MemberwiseClone() as PlayerMapData;
	}
}
