using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public sealed class InformationManager : MonoBehaviour
{
	public sealed class Info
	{
		public sealed class DataInfo : ICloneable
		{
			public sealed class Os : ICloneable
			{
				public class MainInfo : ICloneable
				{
					[MiniJSONAlias("info_cnt")]
					public int Count { get; set; }

					[MiniJSONAlias("info_url")]
					public List<string> URL { get; set; }

					public MainInfo()
					{
						URL = new List<string>();
					}

					object ICloneable.Clone()
					{
						return Clone();
					}

					public MainInfo Clone()
					{
						MainInfo mainInfo = MemberwiseClone() as MainInfo;
						string[] array = new string[URL.Count];
						URL.CopyTo(array);
						mainInfo.URL = array.ToList();
						return mainInfo;
					}
				}

				public sealed class AdvInfo : MainInfo, ICloneable
				{
					[MiniJSONAlias("info_hide")]
					public int Hide { get; set; }

					public bool IsHide
					{
						get
						{
							return Hide == 1;
						}
					}

					object ICloneable.Clone()
					{
						return Clone();
					}

					public new AdvInfo Clone()
					{
						AdvInfo advInfo = MemberwiseClone() as AdvInfo;
						string[] array = new string[base.URL.Count];
						base.URL.CopyTo(array);
						advInfo.URL = array.ToList();
						return advInfo;
					}
				}

				[MiniJSONAlias("main_info")]
				public MainInfo Main { get; set; }

				[MiniJSONAlias("adv_info")]
				public AdvInfo Adv { get; set; }

				public Os()
				{
					Main = new MainInfo();
					Adv = new AdvInfo();
				}

				object ICloneable.Clone()
				{
					return Clone();
				}

				public Os Clone()
				{
					Os os = MemberwiseClone() as Os;
					os.Main = Main.Clone();
					os.Adv = Adv.Clone();
					return os;
				}
			}

			[MiniJSONAlias("start")]
			public long StartTime { get; set; }

			[MiniJSONAlias("ad")]
			public Os Android { get; set; }

			[MiniJSONAlias("ios")]
			public Os iOS { get; set; }

			private Os OS
			{
				get
				{
					return Android;
				}
			}

			public Os.MainInfo Main
			{
				get
				{
					return (OS == null) ? null : OS.Main;
				}
			}

			public Os.AdvInfo Adv
			{
				get
				{
					return (OS == null) ? null : OS.Adv;
				}
			}

			public bool IsOptimized { get; private set; }

			public DataInfo()
			{
				Android = (iOS = new Os());
			}

			object ICloneable.Clone()
			{
				return Clone();
			}

			public DataInfo Clone()
			{
				DataInfo dataInfo = MemberwiseClone() as DataInfo;
				dataInfo.Android = Android.Clone();
				dataInfo.iOS = iOS.Clone();
				return dataInfo;
			}

			public void Optimize()
			{
				if (!IsOptimized && Main != null)
				{
					DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(StartTime, true);
					string s = new StringBuilder(dateTime.Year.ToString("0000")).Append(dateTime.Month.ToString("00")).Append(dateTime.Day.ToString("00")).Append(Main.Count)
						.ToString();
					int result;
					Main.Count = (int.TryParse(s, out result) ? result : 0);
					IsOptimized = true;
				}
			}
		}

		public sealed class NoticeConstruct
		{
			public enum InfoType
			{
				Update = 0,
				OpenMainEvent = 1,
				SaleSpecialGasha = 2,
				OpenAdvPoint = 3,
				NoticeMainEvent = 4,
				NoticeAdvPoint = 5,
				OpenNewMainMap = 6,
				SaleRecommendSet = 7,
				SaleNonRecommendSet = 8,
				OpenSeriesCampaign = 9,
				OpenDailyChallenge = 10,
				TakeMessageCard = 11,
				Guerrilla = 12,
				TrophyExchange = 13,
				MugenHeart = 14,
				OldEvent = 15
			}

			public class BannerInfo
			{
				public sealed class ImageInfo
				{
					[MiniJSONAlias("atlas")]
					public string Atlas { get; set; }

					[MiniJSONAlias("filename")]
					public string FileName { get; set; }

					[MiniJSONAlias("text_key")]
					public string TextKey { get; set; }
				}

				[MiniJSONAlias("body")]
				public ImageInfo Body { get; set; }

				[MiniJSONAlias("header")]
				public ImageInfo Header { get; set; }

				[MiniJSONAlias("footer")]
				public ImageInfo Footer { get; set; }

				[MiniJSONAlias("show")]
				public int ShowFlag { get; set; }

				[MiniJSONAlias("group")]
				public int Group { get; set; }

				[MiniJSONAlias("priority")]
				public int Priority { get; set; }

				[MiniJSONAlias("spawn")]
				public int WebViewSpawn { get; set; }

				[MiniJSONAlias("ex_url")]
				public string WebViewURL { get; set; }

				[MiniJSONAlias("exclude")]
				public List<int> Exclude { get; set; }

				public bool IsForceShow
				{
					get
					{
						return ShowFlag >= 2;
					}
				}

				public bool IsForceHide
				{
					get
					{
						return ShowFlag <= 0;
					}
				}

				public bool IsExternalSpawnURL
				{
					get
					{
						return WebViewSpawn != 0;
					}
				}

				public bool ContainsExclude(int value)
				{
					return Exclude != null && Exclude.Contains(value);
				}
			}

			public sealed class UniqueBannerInfo : BannerInfo
			{
				public sealed class ForceInfo
				{
					public enum LocationType
					{
						Header = 0,
						Footer = 1
					}

					[MiniJSONAlias("page")]
					public int Page { get; set; }

					[MiniJSONAlias("location")]
					public int Location { get; set; }

					public bool IsFirstPage
					{
						get
						{
							return Page == -1;
						}
					}

					public bool IsLastPage
					{
						get
						{
							return Page == -2;
						}
					}

					public LocationType LocationValue
					{
						get
						{
							return (Location != 0) ? LocationType.Footer : LocationType.Header;
						}
					}
				}

				[MiniJSONAlias("start")]
				public long StartUnixTime { get; set; }

				[MiniJSONAlias("end")]
				public long EndUnixTime { get; set; }

				[MiniJSONAlias("force")]
				public ForceInfo Force { get; set; }

				public bool UseTime
				{
					get
					{
						return StartUnixTime == 0L && EndUnixTime == 0;
					}
				}

				public DateTime StartTime
				{
					get
					{
						return DateTimeUtil.UnixTimeStampToDateTime(StartUnixTime).ToLocalTime();
					}
				}

				public DateTime EndTime
				{
					get
					{
						return DateTimeUtil.UnixTimeStampToDateTime(EndUnixTime).ToLocalTime();
					}
				}

				public bool InTime
				{
					get
					{
						DateTime dateTime = DateTimeUtil.Now();
						return StartTime <= dateTime && dateTime < EndTime;
					}
				}
			}

			public sealed class ForceGashaLBInfo
			{
				[MiniJSONAlias("group")]
				public int GroupID { get; set; }

				[MiniJSONAlias("atlas")]
				public string Atlas { get; set; }

				[MiniJSONAlias("filename")]
				public string FileName { get; set; }
			}

			[MiniJSONAlias("base_url")]
			public string BaseURL { get; set; }

			[MiniJSONAlias("tex_max_width")]
			public int TextureMaxWidth { get; set; }

			[MiniJSONAlias("page_separate")]
			public int PageSeparate { get; set; }

			[MiniJSONAlias("max_page")]
			public int MaxPage { get; set; }

			[MiniJSONAlias("update")]
			public BannerInfo Update { get; set; }

			[MiniJSONAlias("open_ev")]
			public BannerInfo OpenMainEvent { get; set; }

			[MiniJSONAlias("gasha")]
			public BannerInfo SaleSpecialGasha { get; set; }

			[MiniJSONAlias("open_point")]
			public BannerInfo OpenAdvPoint { get; set; }

			[MiniJSONAlias("event_notice")]
			public BannerInfo NoticeMainEvent { get; set; }

			[MiniJSONAlias("point_notice")]
			public BannerInfo NoticeAdvPoint { get; set; }

			[MiniJSONAlias("new_map")]
			public BannerInfo OpenNewMainMap { get; set; }

			[MiniJSONAlias("sale_rcmd")]
			public BannerInfo SaleRecommendSet { get; set; }

			[MiniJSONAlias("sale_no_rcmd")]
			public BannerInfo SaleNonRecommendSet { get; set; }

			[MiniJSONAlias("open_sc")]
			public BannerInfo OpenSeriesCampaign { get; set; }

			[MiniJSONAlias("open_dc")]
			public BannerInfo OpenDailyChallenge { get; set; }

			[MiniJSONAlias("message_card")]
			public BannerInfo TakeMessageCard { get; set; }

			[MiniJSONAlias("guerrilla")]
			public BannerInfo Guerrilla { get; set; }

			[MiniJSONAlias("trophyexchange")]
			public BannerInfo TrophyExchange { get; set; }

			[MiniJSONAlias("mugen_heart")]
			public BannerInfo MugenHeart { get; set; }

			[MiniJSONAlias("old_event")]
			public BannerInfo OldEvent { get; set; }

			[MiniJSONAlias("Unique")]
			public List<UniqueBannerInfo> Unique { get; set; }

			[MiniJSONAlias("gacha_lb")]
			public List<ForceGashaLBInfo> GashaLinkBanner { get; set; }

			public int EnableInfoCount
			{
				get
				{
					List<BannerInfo> list = new List<BannerInfo>();
					foreach (int value in Enum.GetValues(typeof(InfoType)))
					{
						list.Add(GetBannerInfo((InfoType)value));
					}
					return (list != null && list.Count != 0) ? list.Count((BannerInfo n) => n != null) : 0;
				}
			}

			public BannerInfo GetBannerInfo(InfoType type)
			{
				switch (type)
				{
				case InfoType.Update:
					return Update;
				case InfoType.OpenMainEvent:
					return OpenMainEvent;
				case InfoType.SaleSpecialGasha:
					return SaleSpecialGasha;
				case InfoType.OpenAdvPoint:
					return OpenAdvPoint;
				case InfoType.NoticeMainEvent:
					return NoticeMainEvent;
				case InfoType.NoticeAdvPoint:
					return NoticeAdvPoint;
				case InfoType.OpenNewMainMap:
					return OpenNewMainMap;
				case InfoType.SaleRecommendSet:
					return SaleRecommendSet;
				case InfoType.SaleNonRecommendSet:
					return SaleNonRecommendSet;
				case InfoType.OpenSeriesCampaign:
					return OpenSeriesCampaign;
				case InfoType.OpenDailyChallenge:
					return OpenDailyChallenge;
				case InfoType.TakeMessageCard:
					return TakeMessageCard;
				case InfoType.Guerrilla:
					return Guerrilla;
				case InfoType.TrophyExchange:
					return TrophyExchange;
				case InfoType.MugenHeart:
					return MugenHeart;
				case InfoType.OldEvent:
					return OldEvent;
				default:
					return null;
				}
			}
		}

		public bool IsAdjusted { get; private set; }

		[MiniJSONAlias("data")]
		public List<DataInfo> Data { get; set; }

		[MiniJSONAlias("auto_notice")]
		public NoticeConstruct NoticeConstructParam { get; set; }

		public Info()
		{
			Data = new List<DataInfo>();
		}

		public void AdjustData()
		{
			if (IsAdjusted || Data == null)
			{
				return;
			}
			long unixTime = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTimeUtil.Now());
			long latestTime = 0L;
			if (Data.Any((DataInfo n) => n != null && unixTime >= n.StartTime))
			{
				latestTime = Data.Where((DataInfo n) => n != null && unixTime >= n.StartTime).Max((DataInfo n) => n.StartTime);
			}
			if (latestTime != 0L)
			{
				DataInfo data = Data.FirstOrDefault((DataInfo n) => n.StartTime == latestTime);
				if (data != null)
				{
					DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0);
					DateTime dateTime2 = dateTime.AddSeconds(data.StartTime).ToLocalTime();
					Queue<DataInfo> queue = new Queue<DataInfo>();
					DataInfo old = (from n in Data
						where n.StartTime < latestTime
						orderby n.StartTime
						select n).FirstOrDefault();
					if (old != null)
					{
						while (old != data)
						{
							queue.Enqueue(old.Clone());
							old = (from n in Data
								where n.StartTime > old.StartTime
								orderby n.StartTime
								select n).FirstOrDefault();
						}
					}
					queue.Enqueue(data.Clone());
					DateTime dateTime3 = DateTimeUtil.Now();
					dateTime3 = new DateTime(dateTime3.Year, dateTime3.Month, dateTime3.Day, 0, 0, 0);
					double num = DateTimeUtil.LoacalDateTimeToUnixTimeStamp(dateTime3);
					if ((double)latestTime < num)
					{
						DataInfo dataInfo = data.Clone();
						dateTime2 = dateTime.AddSeconds(num).ToLocalTime();
						dataInfo.StartTime = (long)num;
						dataInfo.Android.Main.Count = 0;
						dataInfo.iOS.Main.Count = 0;
						queue.Enqueue(dataInfo.Clone());
						data = dataInfo;
					}
					DataInfo dataInfo2 = Data.OrderBy((DataInfo n) => n.StartTime).LastOrDefault();
					while (data.StartTime < dataInfo2.StartTime)
					{
						DataInfo dataInfo3 = (from n in Data
							where n.StartTime > data.StartTime
							orderby n.StartTime
							select n).FirstOrDefault();
						DateTime dateTime4 = dateTime.AddSeconds(dataInfo3.StartTime).ToLocalTime();
						if (dateTime4.Day == dateTime2.Day || (dateTime4.Hour == 0 && dateTime4.Minute == 0 && dateTime4.Second == 0))
						{
							dateTime2 = dateTime4;
							queue.Enqueue(dataInfo3.Clone());
							data = dataInfo3;
							continue;
						}
						DataInfo dataInfo4 = data.Clone();
						dateTime2 = dateTime.AddSeconds(data.StartTime).ToLocalTime().AddDays(1.0);
						dateTime2 = new DateTime(dateTime2.Year, dateTime2.Month, dateTime2.Day, 0, 0, 0);
						dataInfo4.StartTime = (long)(dateTime2.ToUniversalTime() - dateTime).TotalSeconds;
						dataInfo4.Android.Main.Count = 0;
						dataInfo4.iOS.Main.Count = 0;
						queue.Enqueue(dataInfo4.Clone());
						data = dataInfo4;
					}
					Data = queue.ToList();
				}
			}
			IsAdjusted = true;
		}
	}

	public sealed class AutoNoticeQueueItem : SMVerticalVariableList.ItemBase
	{
		public enum LoadModeType
		{
			Banner = 0,
			Atlas = 1
		}

		public enum LocationType : byte
		{
			Header = 0,
			Body = 1,
			Footer = 2
		}

		public string ImageName { get; set; }

		public long StartTime { get; set; }

		public int Group { get; set; }

		public int Priority { get; set; }

		public int ParentID { get; set; }

		public LocationType Location { get; set; }

		public LoadModeType LoadMode { get; set; }
	}

	public const int INFO_DEFAULT_PAGE = 0;

	private static InformationManager mInstance;

	private Info mInfo;

	private DateTime mLastUpdateTime = new DateTime(0L);

	private Coroutine mConnectRoutine;

	private Info.DataInfo mCurrentData;

	private List<AutoNoticeQueueItem> mAutoNoticeQueue = new List<AutoNoticeQueueItem>();

	private Info.DataInfo LatestData
	{
		get
		{
			if (mCurrentData != null)
			{
				return mCurrentData;
			}
			Info.DataInfo dataInfo = null;
			long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTimeUtil.Now());
			if (mInfo != null && mInfo.Data != null)
			{
				for (int i = 0; i < mInfo.Data.Count; i++)
				{
					if (num >= mInfo.Data[i].StartTime && (dataInfo == null || mInfo.Data[i].StartTime >= dataInfo.StartTime))
					{
						dataInfo = mInfo.Data[i];
					}
				}
			}
			if (dataInfo == null)
			{
				Info.DataInfo dataInfo2 = new Info.DataInfo();
				dataInfo2.StartTime = 0L;
				dataInfo2.Android = new Info.DataInfo.Os
				{
					Main = new Info.DataInfo.Os.MainInfo
					{
						Count = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoCount : 0),
						URL = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile == null) ? new List<string>() : SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoURL)
					},
					Adv = new Info.DataInfo.Os.AdvInfo
					{
						Count = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoCount : 0),
						URL = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile == null || SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings == null) ? new List<string>() : SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoURL),
						Hide = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoHide : 0)
					}
				};
				dataInfo = dataInfo2;
			}
			mCurrentData = dataInfo;
			return dataInfo;
		}
	}

	public int Count
	{
		get
		{
			Info.DataInfo latestData = LatestData;
			return (latestData != null && latestData.Main != null) ? latestData.Main.Count : 0;
		}
	}

	public string[] URL
	{
		get
		{
			Info.DataInfo latestData = LatestData;
			return (latestData == null || latestData.Main == null) ? new string[0] : latestData.Main.URL.ToArray();
		}
	}

	public int Adv_Count
	{
		get
		{
			Info.DataInfo latestData = LatestData;
			return (latestData != null && latestData.Adv != null) ? latestData.Adv.Count : 0;
		}
	}

	public string[] Adv_URL
	{
		get
		{
			Info.DataInfo latestData = LatestData;
			return (latestData == null || latestData.Adv == null) ? new string[0] : latestData.Adv.URL.ToArray();
		}
	}

	public bool Adv_Hide
	{
		get
		{
			Info.DataInfo latestData = LatestData;
			return latestData != null && latestData.Adv != null && latestData.Adv.IsHide;
		}
	}

	public string NoticeConstruct_BaseURL
	{
		get
		{
			return (mInfo == null || mInfo.NoticeConstructParam == null) ? string.Empty : mInfo.NoticeConstructParam.BaseURL;
		}
	}

	public int NoticeConstruct_TextureMaxWidth
	{
		get
		{
			return (mInfo == null || mInfo.NoticeConstructParam == null || mInfo.NoticeConstructParam.TextureMaxWidth <= 0) ? 500 : mInfo.NoticeConstructParam.TextureMaxWidth;
		}
	}

	public int NoticeConstruct_PageSeparate
	{
		get
		{
			return (mInfo == null || mInfo.NoticeConstructParam == null || mInfo.NoticeConstructParam.PageSeparate <= 0) ? 4 : mInfo.NoticeConstructParam.PageSeparate;
		}
	}

	public int NoticeConstruct_MaxPage
	{
		get
		{
			return (mInfo == null || mInfo.NoticeConstructParam == null || mInfo.NoticeConstructParam.MaxPage <= 0) ? 4 : mInfo.NoticeConstructParam.MaxPage;
		}
	}

	public bool IsConnectSuccessed { get; private set; }

	public bool IsConnectDone { get; private set; }

	public static InformationManager Instance
	{
		get
		{
			return mInstance;
		}
	}

	public int EnabledInfoCount
	{
		get
		{
			return (mInfo != null && mInfo.NoticeConstructParam != null) ? mInfo.NoticeConstructParam.EnableInfoCount : 0;
		}
	}

	public int AutoNoticeQueueCount
	{
		get
		{
			return (mAutoNoticeQueue != null) ? mAutoNoticeQueue.Count : 0;
		}
	}

	public void EnqueueAutoNoticeWithoutForce(AutoNoticeQueueItem[] array)
	{
		mAutoNoticeQueue.Clear();
		AutoNoticeQueueItem[] array2 = (from _ in array
			where _ != null && _.Group < 0
			orderby _.ID, _.Priority, _.Group, (byte)_.Location
			select _).ToArray();
		if (array2 != null)
		{
			AutoNoticeQueueItem[] array3 = array2;
			foreach (AutoNoticeQueueItem item in array3)
			{
				mAutoNoticeQueue.Add(item);
			}
		}
		AutoNoticeQueueItem[] array4 = (from _ in array
			where _ != null && _.Group >= 0
			orderby _.StartTime descending, _.ID, _.Priority, _.Group, (byte)_.Location
			select _).ToArray();
		if (array4 != null)
		{
			AutoNoticeQueueItem[] array5 = array4;
			foreach (AutoNoticeQueueItem item2 in array5)
			{
				mAutoNoticeQueue.Add(item2);
			}
		}
	}

	public Info.NoticeConstruct.BannerInfo GetNoticeConstructInfo(Info.NoticeConstruct.InfoType type)
	{
		return (mInfo == null || mInfo.NoticeConstructParam == null) ? null : mInfo.NoticeConstructParam.GetBannerInfo(type);
	}

	public Info.NoticeConstruct.UniqueBannerInfo[] GetNoticeConstructUniqueInfo()
	{
		return (mInfo == null || mInfo.NoticeConstructParam == null || mInfo.NoticeConstructParam.Unique == null) ? null : mInfo.NoticeConstructParam.Unique.ToArray();
	}

	public AutoNoticeQueueItem[] DequeueAutoNoticeWithoutForce(bool influence_page_separate = true)
	{
		List<AutoNoticeQueueItem> list = new List<AutoNoticeQueueItem>();
		if (mAutoNoticeQueue != null)
		{
			int num = 0;
			while (mAutoNoticeQueue.Count != 0)
			{
				bool flag = false;
				AutoNoticeQueueItem autoNoticeQueueItem = mAutoNoticeQueue[0];
				if (autoNoticeQueueItem != null)
				{
					if (autoNoticeQueueItem.Location == AutoNoticeQueueItem.LocationType.Body)
					{
						num++;
					}
					if (influence_page_separate && num >= NoticeConstruct_PageSeparate)
					{
						switch (autoNoticeQueueItem.Location)
						{
						case AutoNoticeQueueItem.LocationType.Body:
							if (mAutoNoticeQueue.Count > 1 && mAutoNoticeQueue[1].Location != AutoNoticeQueueItem.LocationType.Footer)
							{
								flag = true;
							}
							break;
						case AutoNoticeQueueItem.LocationType.Footer:
							flag = true;
							break;
						}
					}
					list.Add(autoNoticeQueueItem);
				}
				mAutoNoticeQueue.RemoveAt(0);
				if (flag)
				{
					break;
				}
			}
		}
		return list.ToArray();
	}

	public void GetGashaLinkBannerOrElse(int group_id, ref string result_atlas, ref string result_filename)
	{
		if (mInfo != null && mInfo.NoticeConstructParam != null && mInfo.NoticeConstructParam.GashaLinkBanner != null && mInfo.NoticeConstructParam.GashaLinkBanner.Any((Info.NoticeConstruct.ForceGashaLBInfo _) => _ != null && _.GroupID == group_id))
		{
			Info.NoticeConstruct.ForceGashaLBInfo forceGashaLBInfo = mInfo.NoticeConstructParam.GashaLinkBanner.FirstOrDefault((Info.NoticeConstruct.ForceGashaLBInfo _) => _ != null && _.GroupID == group_id);
			if (forceGashaLBInfo != null)
			{
				result_atlas = forceGashaLBInfo.Atlas;
				result_filename = forceGashaLBInfo.FileName;
			}
		}
	}

	public static void CreateInstance()
	{
		if (mInstance == null)
		{
			mInstance = Util.CreateGameObject("InformationManager", null).AddComponent<InformationManager>();
		}
	}

	private void LateUpdate()
	{
		mCurrentData = null;
	}

	public void MightUpdateInfo(bool compare_info_time = true)
	{
		if (SingletonMonoBehaviour<GameMain>.Instance.GameProfile == null)
		{
			IsConnectDone = true;
			IsConnectSuccessed = false;
			return;
		}
		if (mInfo != null && mInfo.Data != null && mLastUpdateTime >= SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastGetGameInfoTime && compare_info_time)
		{
			IsConnectDone = true;
			IsConnectSuccessed = true;
			return;
		}
		if (!InternetReachability.IsEnable)
		{
			IsConnectDone = true;
			IsConnectSuccessed = false;
			return;
		}
		if (mConnectRoutine != null)
		{
			StopCoroutine(mConnectRoutine);
		}
		mInfo = null;
		IsConnectDone = false;
		IsConnectSuccessed = false;
		mConnectRoutine = StartCoroutine(ConnectAsync());
	}

	private IEnumerator ConnectAsync()
	{
		if (!string.IsNullOrEmpty(SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoDataURL))
		{
			if (InternetReachability.IsEnable)
			{
				WWW www2 = new WWW(SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoDataURL);
				while (www2 == null)
				{
					yield return null;
				}
				while (!www2.isDone && InternetReachability.IsEnable)
				{
					yield return null;
				}
				if (!InternetReachability.IsEnable)
				{
					www2.Dispose();
					www2 = null;
				}
				else if (!string.IsNullOrEmpty(www2.error))
				{
					if (SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null)
					{
						mInfo = new Info
						{
							Data = new List<Info.DataInfo>
							{
								new Info.DataInfo
								{
									StartTime = 0L,
									Android = new Info.DataInfo.Os
									{
										Main = new Info.DataInfo.Os.MainInfo
										{
											Count = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoCount : 0),
											URL = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile == null) ? new List<string>() : SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoURL)
										},
										Adv = new Info.DataInfo.Os.AdvInfo
										{
											Count = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoCount : 0),
											URL = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile == null || SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings == null) ? new List<string>() : SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoURL),
											Hide = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoHide : 0)
										}
									}
								}
							}
						};
						mLastUpdateTime = SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastGetGameInfoTime;
						IsConnectSuccessed = true;
					}
					www2.Dispose();
					www2 = null;
				}
				else
				{
					mInfo = new Info();
					new MiniJSONSerializer().Populate(ref mInfo, www2.text);
					if (mInfo != null && mInfo.Data != null)
					{
						mInfo.AdjustData();
						for (int i = 0; i < mInfo.Data.Count; i++)
						{
							if (mInfo.Data[i] != null)
							{
								mInfo.Data[i].Optimize();
							}
						}
					}
					mLastUpdateTime = SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastGetGameInfoTime;
					IsConnectSuccessed = true;
					www2.Dispose();
					www2 = null;
				}
			}
		}
		else if (SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings != null)
		{
			mInfo = new Info
			{
				Data = new List<Info.DataInfo>
				{
					new Info.DataInfo
					{
						StartTime = 0L,
						Android = new Info.DataInfo.Os
						{
							Main = new Info.DataInfo.Os.MainInfo
							{
								Count = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoCount : 0),
								URL = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile == null) ? new List<string>() : SingletonMonoBehaviour<GameMain>.Instance.GameProfile.InfoURL)
							},
							Adv = new Info.DataInfo.Os.AdvInfo
							{
								Count = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoCount : 0),
								URL = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile == null || SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings == null) ? new List<string>() : SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoURL),
								Hide = ((SingletonMonoBehaviour<GameMain>.Instance.GameProfile != null && SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings != null) ? SingletonMonoBehaviour<GameMain>.Instance.GameProfile.AdvSettings.InfoHide : 0)
							}
						}
					}
				}
			};
			mLastUpdateTime = SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastGetGameInfoTime;
			IsConnectSuccessed = true;
		}
		IsConnectDone = true;
	}
}
