using UnityEngine;

public class MusicBoxLazer : LazerInstanceBase
{
	public enum STATE
	{
		WAIT = 0,
		MOVE = 1,
		HIT = 2,
		FINISH = 3
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	private Vector2 mPos;

	private Vector2 mTargetPos;

	private Vector2 mDistance;

	private Util.TriFunc mTriFunc;

	private float mDelayTimer;

	public int IntParam0;

	public int IntParam1;

	public MusicBoxLazer(Vector2 beginPoint, Vector2 endPoint, float width, string spriteName, float delayTime)
		: base(2, width, spriteName, STYLE.SQUARE)
	{
		mPos = beginPoint;
		mTargetPos = endPoint;
		mDistance = endPoint - beginPoint;
		mTriFunc = new Util.TriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
		mDelayTimer = delayTime;
	}

	public override void Update(float deltaTime)
	{
		switch (mState.GetStatus())
		{
		case STATE.WAIT:
			mDelayTimer -= deltaTime;
			if (mDelayTimer <= 0f)
			{
				mState.Change(STATE.MOVE);
			}
			break;
		case STATE.MOVE:
		{
			mTriFunc.AddDegree(deltaTime * 360f);
			Vector2 vector = mPos + mDistance * mTriFunc.Sin();
			mTrail[0] = new Vector2(vector.x, vector.y + mWidth);
			mTrail[1] = new Vector2(vector.x, vector.y - mWidth);
			if (mTriFunc.degree >= 90f)
			{
				mState.Change(STATE.FINISH);
			}
			break;
		}
		case STATE.FINISH:
			Finish();
			break;
		}
		mState.Update();
	}
}
