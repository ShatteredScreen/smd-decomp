public class MCReceipt : ParameterObject<MCChargeRequest>
{
	public string data { get; set; }

	public string signature { get; set; }
}
