public class AdvDebug_SetFigureStatus_Request : RequestBase
{
	[MiniJSONAlias("lvl")]
	public int lvl { get; set; }

	[MiniJSONAlias("skill_xp")]
	public int skill_xp { get; set; }

	[MiniJSONAlias("skill_lvl")]
	public int skill_lvl { get; set; }
}
