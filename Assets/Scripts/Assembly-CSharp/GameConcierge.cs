using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameConcierge
{
	public enum LOSE_COUNT_SEGMENT
	{
		NONE = 0,
		CONTINUOUS = 1
	}

	public enum PLAY_TIME_SEGMENT
	{
		NONE = 0,
		ROOKIE = 1
	}

	public enum PLAY_SKILL_SEGMENT
	{
		NONE = 0,
		NOVICE = 1
	}

	public enum LOGIN_SEGMENT
	{
		NONE = 0,
		BETWEEN_SHORT = 1
	}

	private GameMain mGame;

	private PLAY_TIME_SEGMENT mPlayTimeSegment;

	private PLAY_SKILL_SEGMENT mPlaySkillSegment;

	private LOGIN_SEGMENT mLoginSegment;

	private int mColorModificationTime;

	private List<RatioChange1Info> mColorModifiedList = new List<RatioChange1Info>();

	private GameMain Game
	{
		get
		{
			if (mGame == null)
			{
				mGame = SingletonMonoBehaviour<GameMain>.Instance;
			}
			return mGame;
		}
	}

	private List<int> mStripeCountHistory
	{
		get
		{
			return UserBehavior.Instance.mStripeCountHistory;
		}
		set
		{
			UserBehavior.Instance.mStripeCountHistory = value;
		}
	}

	private Dictionary<Def.SERIES, object> mMode7Id
	{
		get
		{
			return UserBehavior.Instance.mMode7Id;
		}
		set
		{
			UserBehavior.Instance.mMode7Id = value;
		}
	}

	private Dictionary<Def.SERIES, object> mMode7RemainCount
	{
		get
		{
			return UserBehavior.Instance.mMode7RemainCount;
		}
		set
		{
			UserBehavior.Instance.mMode7RemainCount = value;
		}
	}

	private Dictionary<Def.SERIES, object> mMode7LastTime
	{
		get
		{
			return UserBehavior.Instance.mMode7LastTime;
		}
		set
		{
			UserBehavior.Instance.mMode7LastTime = value;
		}
	}

	private void Serialize()
	{
		UserBehavior.Save();
	}

	public void DeserializeFromPlayerPrefs()
	{
		mStripeCountHistory.Clear();
		if (PlayerPrefs.HasKey("CONCIERGE_STRIPECOUNT"))
		{
			string @string = PlayerPrefs.GetString("CONCIERGE_STRIPECOUNT");
			string[] array = @string.Split(',');
			string[] array2 = array;
			foreach (string s in array2)
			{
				int num = 0;
				try
				{
					num = int.Parse(s);
				}
				catch
				{
					num = -1;
				}
				if (num > 0)
				{
					mStripeCountHistory.Add(num);
				}
			}
		}
		if (PlayerPrefs.HasKey("CONCIERGE_M7_TYPE"))
		{
			mMode7Id.Clear();
			mMode7Id = StringToDict<int>(PlayerPrefs.GetString("CONCIERGE_M7_TYPE"));
			if (mMode7Id == null)
			{
				mMode7Id = new Dictionary<Def.SERIES, object>();
				mMode7RemainCount = new Dictionary<Def.SERIES, object>();
				mMode7LastTime = new Dictionary<Def.SERIES, object>();
				return;
			}
		}
		if (PlayerPrefs.HasKey("CONCIERGE_M7_COUNT"))
		{
			mMode7RemainCount.Clear();
			mMode7RemainCount = StringToDict<int>(PlayerPrefs.GetString("CONCIERGE_M7_COUNT"));
			if (mMode7RemainCount == null)
			{
				mMode7Id = new Dictionary<Def.SERIES, object>();
				mMode7RemainCount = new Dictionary<Def.SERIES, object>();
				mMode7LastTime = new Dictionary<Def.SERIES, object>();
				return;
			}
		}
		if (!PlayerPrefs.HasKey("CONCIERGE_M7_COUNT"))
		{
			return;
		}
		mMode7LastTime.Clear();
		Dictionary<Def.SERIES, object> dictionary = new Dictionary<Def.SERIES, object>();
		dictionary = StringToDict<int>(PlayerPrefs.GetString("CONCIERGE_M7_LASTTIME"));
		if (dictionary == null)
		{
			mMode7Id = new Dictionary<Def.SERIES, object>();
			mMode7RemainCount = new Dictionary<Def.SERIES, object>();
			mMode7LastTime = new Dictionary<Def.SERIES, object>();
			return;
		}
		foreach (KeyValuePair<Def.SERIES, object> item in dictionary)
		{
			DateTime dateTime = default(DateTime);
			try
			{
				dateTime = DateTimeUtil.UnixTimeStampToDateTime((int)item.Value).ToLocalTime();
			}
			catch
			{
				dateTime = default(DateTime);
			}
			mMode7LastTime.Add(item.Key, dateTime);
		}
	}

	private string DictToString<T>(Dictionary<Def.SERIES, object> dict)
	{
		Type typeFromHandle = typeof(T);
		bool flag = true;
		StringBuilder stringBuilder = new StringBuilder();
		foreach (KeyValuePair<Def.SERIES, object> item in dict)
		{
			if (!flag)
			{
				stringBuilder.Append(",");
			}
			else
			{
				flag = false;
			}
			stringBuilder.Append((int)item.Key);
			stringBuilder.Append(",");
			if (typeFromHandle == typeof(int))
			{
				stringBuilder.Append(((int)item.Value).ToString());
			}
			else if (typeFromHandle == typeof(DateTime))
			{
				stringBuilder.Append(((DateTime)item.Value).ToString());
			}
		}
		return stringBuilder.ToString();
	}

	private Dictionary<Def.SERIES, object> StringToDict<T>(string str)
	{
		Type typeFromHandle = typeof(T);
		Dictionary<Def.SERIES, object> dictionary = new Dictionary<Def.SERIES, object>();
		string[] array = str.Split(',');
		if (array.Length % 2 != 0)
		{
			return null;
		}
		for (int i = 0; i < array.Length; i += 2)
		{
			if (typeFromHandle == typeof(int))
			{
				int num = -1;
				try
				{
					num = int.Parse(array[i]);
				}
				catch
				{
					num = -1;
				}
				int num2 = -1;
				try
				{
					num2 = int.Parse(array[i + 1]);
				}
				catch
				{
					num2 = -1;
				}
				if (num != -1 && num2 != -1)
				{
					dictionary.Add((Def.SERIES)num, num2);
				}
			}
			else if (typeFromHandle == typeof(DateTime))
			{
				int num3 = -1;
				try
				{
					num3 = int.Parse(array[i]);
				}
				catch
				{
					num3 = -1;
				}
				DateTime dateTime = default(DateTime);
				try
				{
					dateTime = DateTime.Parse(array[i + 1]);
				}
				catch
				{
					dateTime = default(DateTime);
				}
				if (num3 != -1)
				{
					dictionary.Add((Def.SERIES)num3, dateTime);
				}
			}
		}
		return dictionary;
	}

	public void UpdateUserSegment()
	{
		if (Game.SegmentProfile == null)
		{
			return;
		}
		UpdatePlayTimeSegment();
		if (Game.mPlayer.IntervalDays >= Game.SegmentProfile.DropoutLoginIntervalDays)
		{
			mLoginSegment = LOGIN_SEGMENT.BETWEEN_SHORT;
		}
		else
		{
			mLoginSegment = LOGIN_SEGMENT.NONE;
		}
		if (mStripeCountHistory.Count < 20)
		{
			mPlaySkillSegment = PLAY_SKILL_SEGMENT.NONE;
			return;
		}
		int num = 0;
		foreach (int item in mStripeCountHistory)
		{
			num += item;
		}
		int num2 = num / mStripeCountHistory.Count;
		if (num2 < Game.SegmentProfile.NoviceStripeCount)
		{
			mPlaySkillSegment = PLAY_SKILL_SEGMENT.NOVICE;
		}
		else
		{
			mPlaySkillSegment = PLAY_SKILL_SEGMENT.NONE;
		}
	}

	private void UpdatePlayTimeSegment()
	{
		if (new TimeSpan(0, 0, (int)Game.mPlayer.Data.TotalPlayTime).TotalHours < (double)Game.SegmentProfile.RookiePlayHour)
		{
			mPlayTimeSegment = PLAY_TIME_SEGMENT.ROOKIE;
		}
		else
		{
			mPlayTimeSegment = PLAY_TIME_SEGMENT.NONE;
		}
	}

	public int GetUserSegmentNumber()
	{
		return ((int)mPlayTimeSegment << 2) | ((int)mLoginSegment << 4) | ((int)mPlaySkillSegment << 6);
	}

	public void ResetGameConcierge()
	{
		mPlayTimeSegment = PLAY_TIME_SEGMENT.NONE;
		mPlaySkillSegment = PLAY_SKILL_SEGMENT.NONE;
		mLoginSegment = LOGIN_SEGMENT.NONE;
		UserBehavior.Instance.Reset();
		Serialize();
	}

	public void SetPuzzleEndStripeCount(int count, Def.STAGE_LOSE_REASON loseReason)
	{
		ISMStageData mCurrentStage = Game.mCurrentStage;
		if (loseReason != 0 && loseReason != Def.STAGE_LOSE_REASON.NO_MORE_MOVES && mCurrentStage.LoseType != Def.STAGE_LOSE_CONDITION.TIME)
		{
			mStripeCountHistory.Add(count);
			while (mStripeCountHistory.Count > 20)
			{
				mStripeCountHistory.RemoveAt(0);
			}
			Serialize();
		}
	}

	public Def.DIFFICULTY_MODE GetDifficultyMode()
	{
		Def.DIFFICULTY_MODE result = Def.DIFFICULTY_MODE.NORMAL;
		if (Game.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
		{
			return result;
		}
		if (GameMain.USE_DEBUG_DIALOG)
		{
			if (Game.mDebugDifficultyModeDisable)
			{
				return result;
			}
			if (Game.mDebugDifficltyMode != 0)
			{
				return Game.mDebugDifficltyMode;
			}
		}
		if (Game.mSegmentProfile == null)
		{
			return result;
		}
		if (!Game.mTutorialManager.IsInitialTutorialCompleted())
		{
			return result;
		}
		if (CheckDifficultyMode7())
		{
			return Def.DIFFICULTY_MODE.FILL_MATCH;
		}
		if (CheckDifficultyMode1())
		{
			return Def.DIFFICULTY_MODE.COLOR_MODIFY_POSI;
		}
		return result;
	}

	public object[] GetDifficultyModeParam(Def.DIFFICULTY_MODE mode)
	{
		ISMStageData mCurrentStage = Game.mCurrentStage;
		Def.SERIES series = mCurrentStage.Series;
		int num = -1;
		int stageNumber = mCurrentStage.StageNumber;
		if (series == Def.SERIES.SM_EV)
		{
			num = mCurrentStage.EventID;
		}
		object[] result = null;
		if (mode != Def.DIFFICULTY_MODE.COLOR_MODIFY_POSI && mode == Def.DIFFICULTY_MODE.FILL_MATCH)
		{
			if (GameMain.USE_DEBUG_DIALOG && Game.mDebugDifficltyMode != 0)
			{
				result = new object[2] { 10, 40 };
			}
			else if (GlobalVariables.Instance.AddMovesCampaignAmount > 0)
			{
				result = new object[2] { 10, 20 };
			}
			else
			{
				RatioChange7Info difficultyMode7InfoById = GetDifficultyMode7InfoById((int)mMode7Id[series]);
				if (difficultyMode7InfoById != null)
				{
					result = new object[2] { difficultyMode7InfoById.MinRatio, difficultyMode7InfoById.MaxRatio };
				}
			}
		}
		return result;
	}

	public void PuzzleStart(Def.DIFFICULTY_MODE mode)
	{
		switch (mode)
		{
		case Def.DIFFICULTY_MODE.COLOR_MODIFY_POSI:
			Game.mPlayerMetaData.PlayCount = 0;
			Game.Save();
			break;
		}
	}

	public void PuzzleEnd(Def.DIFFICULTY_MODE mode, bool win)
	{
		if ((GameMain.USE_DEBUG_DIALOG && Game.mDebugDifficltyMode != 0) || GlobalVariables.Instance.AddMovesCampaignAmount > 0)
		{
			return;
		}
		ISMStageData mCurrentStage = Game.mCurrentStage;
		Def.SERIES series = mCurrentStage.Series;
		if (mode == Def.DIFFICULTY_MODE.COLOR_MODIFY_POSI || mode != Def.DIFFICULTY_MODE.FILL_MATCH)
		{
			return;
		}
		bool flag = false;
		if (win)
		{
			mMode7RemainCount[series] = (int)mMode7RemainCount[series] - 1;
			if ((int)mMode7RemainCount[series] == 0)
			{
				flag = true;
			}
		}
		RatioChange7Info difficultyMode7InfoById = GetDifficultyMode7InfoById((int)mMode7Id[series]);
		if (difficultyMode7InfoById.SegmentType == 0)
		{
			UpdatePlayTimeSegment();
			if (mPlayTimeSegment != PLAY_TIME_SEGMENT.ROOKIE)
			{
				flag = true;
			}
		}
		if (flag)
		{
			mMode7Id[series] = -1;
			mMode7RemainCount[series] = -1;
		}
	}

	public bool CheckDifficultyMode1()
	{
		if (mColorModifiedList.Count > 0)
		{
			mColorModificationTime++;
			for (int num = mColorModifiedList.Count - 1; num >= 0; num--)
			{
				if (mColorModifiedList[num].Addition < mColorModificationTime)
				{
					mColorModifiedList.RemoveAt(num);
				}
			}
			if (mColorModifiedList.Count > 0)
			{
				return true;
			}
		}
		ISMStageData mCurrentStage = Game.mCurrentStage;
		Def.SERIES series = mCurrentStage.Series;
		int num2 = -1;
		int stageNumber = mCurrentStage.StageNumber;
		if (series == Def.SERIES.SM_EV)
		{
			num2 = mCurrentStage.EventID;
		}
		List<RatioChange1Info> list = new List<RatioChange1Info>();
		bool flag = false;
		DateTime now = DateTime.Now;
		for (int i = 0; i < Game.mSegmentProfile.RatioChange1List.Count; i++)
		{
			RatioChange1Info ratioChange1Info = Game.mSegmentProfile.RatioChange1List[i];
			if (series != (Def.SERIES)ratioChange1Info.Series || (series == Def.SERIES.SM_EV && ratioChange1Info.EventID != -1 && num2 != ratioChange1Info.EventID) || (ratioChange1Info.StageMin != -1 && (stageNumber < ratioChange1Info.StageMin || stageNumber > ratioChange1Info.StageMax)))
			{
				continue;
			}
			int trigger = ratioChange1Info.Trigger;
			long num3 = ratioChange1Info.Interval;
			if (trigger > 0 && Game.mPlayerMetaData.PlayCount <= trigger)
			{
				continue;
			}
			long num4 = DateTimeUtil.BetweenMinutes(Game.mPlayer.Data.LastColorModificationTime, now);
			if (num3 > 0)
			{
				if (num4 < num3)
				{
					continue;
				}
				flag = true;
			}
			list.Add(ratioChange1Info);
		}
		if (list.Count > 0)
		{
			mColorModificationTime = 0;
			if (flag)
			{
				Game.mPlayer.Data.LastColorModificationTime = now;
			}
			mColorModifiedList = list;
			int cur_stage = stageNumber;
			if (series == Def.SERIES.SM_EV)
			{
				cur_stage = Def.GetStageNoForServer(num2, stageNumber);
			}
			ServerCram.DifficultyChangeStart(100, (int)series, cur_stage, 1);
			return true;
		}
		return false;
	}

	private bool CheckDifficultyMode7()
	{
		if (GlobalVariables.Instance.AddMovesCampaignAmount > 0)
		{
			return true;
		}
		ISMStageData mCurrentStage = Game.mCurrentStage;
		Def.SERIES series = mCurrentStage.Series;
		int num = -1;
		int stageNumber = mCurrentStage.StageNumber;
		if (series == Def.SERIES.SM_EV)
		{
			num = mCurrentStage.EventID;
		}
		if (mMode7Id.ContainsKey(series) && (int)mMode7Id[series] != -1)
		{
			RatioChange7Info difficultyMode7InfoById = GetDifficultyMode7InfoById((int)mMode7Id[series]);
			if (difficultyMode7InfoById != null && (int)mMode7RemainCount[series] > 0)
			{
				if (difficultyMode7InfoById.StageMin == -1)
				{
					if (UnityEngine.Random.Range(0, 100) < difficultyMode7InfoById.ActiveRatio)
					{
						return true;
					}
					return false;
				}
				if (stageNumber >= difficultyMode7InfoById.StageMin && stageNumber <= difficultyMode7InfoById.StageMax)
				{
					if (UnityEngine.Random.Range(0, 100) < difficultyMode7InfoById.ActiveRatio)
					{
						return true;
					}
					return false;
				}
			}
			else
			{
				mMode7Id[series] = -1;
				mMode7RemainCount[series] = -1;
			}
		}
		DateTime now = DateTime.Now;
		for (int i = 0; i < Game.mSegmentProfile.RatioChange7List.Count; i++)
		{
			RatioChange7Info ratioChange7Info = Game.mSegmentProfile.RatioChange7List[i];
			if (series != (Def.SERIES)ratioChange7Info.Series || (series == Def.SERIES.SM_EV && ratioChange7Info.EventID != -1 && num != ratioChange7Info.EventID) || (ratioChange7Info.StageMin != -1 && (stageNumber < ratioChange7Info.StageMin || stageNumber > ratioChange7Info.StageMax)) || (ratioChange7Info.ABParam != 0 && ratioChange7Info.ABParam % 2 != Game.mPlayer.HiveId % 2))
			{
				continue;
			}
			bool flag = false;
			int segment_type = 0;
			switch (ratioChange7Info.SegmentType)
			{
			case 0:
				if (mPlayTimeSegment == PLAY_TIME_SEGMENT.ROOKIE)
				{
					flag = true;
					segment_type = 4;
				}
				break;
			case 1:
				if (mLoginSegment == LOGIN_SEGMENT.BETWEEN_SHORT)
				{
					flag = true;
					segment_type = 16;
				}
				break;
			}
			if (!flag)
			{
				continue;
			}
			int key = ((int)series << 4) | ratioChange7Info.SegmentType;
			DateTime fdt = default(DateTime);
			if (mMode7LastTime.ContainsKey((Def.SERIES)key))
			{
				fdt = (DateTime)mMode7LastTime[(Def.SERIES)key];
			}
			long num2 = DateTimeUtil.BetweenHours(fdt, now);
			if (num2 >= ratioChange7Info.IntervalHour)
			{
				mMode7Id[series] = ratioChange7Info.Id;
				mMode7RemainCount[series] = ratioChange7Info.ContinueCount;
				mMode7LastTime[(Def.SERIES)key] = now;
				Serialize();
				int cur_stage = stageNumber;
				if (series == Def.SERIES.SM_EV)
				{
					cur_stage = Def.GetStageNoForServer(num, stageNumber);
				}
				ServerCram.DifficultyChangeStart(700, (int)series, cur_stage, segment_type);
				return true;
			}
		}
		return false;
	}

	private RatioChange7Info GetDifficultyMode7InfoById(int id)
	{
		RatioChange7Info ratioChange7Info = null;
		for (int i = 0; i < Game.mSegmentProfile.RatioChange7List.Count; i++)
		{
			ratioChange7Info = Game.mSegmentProfile.RatioChange7List[i];
			if (ratioChange7Info.Id == id)
			{
				break;
			}
			ratioChange7Info = null;
		}
		return ratioChange7Info;
	}
}
