using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class SMVerticalVariableList : MonoBehaviour
{
	private enum STATE
	{
		INIT = 0,
		MAIN = 1,
		NETWORK_00 = 2
	}

	public enum MakeAnchor
	{
		Scaling = 0,
		Stretch = 1
	}

	private enum SimpleOrientation
	{
		Portrait = 0,
		Landscape = 1
	}

	public abstract class ItemBase
	{
		public enum ScaleMode
		{
			SameSize = 0,
			HorizontalRatio = 1,
			DirectScaling = 2
		}

		public class DescInfo
		{
			private const int DEFAULT_SIZE = 16;

			private static readonly Color DEFAULT_COLOR = Color.black;

			private static readonly UIWidget.Pivot DEFAULT_PIVOT = UIWidget.Pivot.Center;

			private static readonly Vector3 DEFAULT_POSITION = Vector3.zero;

			public string Text { get; set; }

			public int Size { get; set; }

			public Color Color { get; set; }

			public UIWidget.Pivot Pivot { get; set; }

			public Vector3 Position { get; set; }

			public DescInfo(string text)
				: this(text, 16, DEFAULT_COLOR, DEFAULT_PIVOT, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, int size)
				: this(text, size, DEFAULT_COLOR, DEFAULT_PIVOT, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, Color color)
				: this(text, 16, color, DEFAULT_PIVOT, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, UIWidget.Pivot pivot)
				: this(text, 16, DEFAULT_COLOR, pivot, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, Vector3 position)
				: this(text, 16, DEFAULT_COLOR, DEFAULT_PIVOT, position)
			{
			}

			public DescInfo(string text, int size, Color color)
				: this(text, size, color, DEFAULT_PIVOT, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, int size, UIWidget.Pivot pivot)
				: this(text, size, DEFAULT_COLOR, pivot, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, int size, Vector3 position)
				: this(text, size, DEFAULT_COLOR, DEFAULT_PIVOT, position)
			{
			}

			public DescInfo(string text, Color color, UIWidget.Pivot pivot)
				: this(text, 16, color, pivot, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, Color color, Vector3 position)
				: this(text, 16, color, DEFAULT_PIVOT, position)
			{
			}

			public DescInfo(string text, UIWidget.Pivot pivot, Vector3 position)
				: this(text, 16, DEFAULT_COLOR, pivot, position)
			{
			}

			public DescInfo(string text, int size, Color color, UIWidget.Pivot pivot)
				: this(text, size, color, pivot, DEFAULT_POSITION)
			{
			}

			public DescInfo(string text, int size, Color color, Vector3 position)
				: this(text, size, color, DEFAULT_PIVOT, position)
			{
			}

			public DescInfo(string text, int size, UIWidget.Pivot pivot, Vector3 position)
				: this(text, size, DEFAULT_COLOR, pivot, position)
			{
			}

			public DescInfo(string text, Color color, UIWidget.Pivot pivot, Vector3 position)
				: this(text, 16, color, pivot, position)
			{
			}

			public DescInfo(string text, int size, Color color, UIWidget.Pivot pivot, Vector3 position)
			{
				Text = text;
				Size = size;
				Color = color;
				Pivot = pivot;
				Position = position;
			}
		}

		public DescInfo Header { get; set; }

		public DescInfo Footer { get; set; }

		public DescInfo Inner { get; set; }

		public int ID { get; set; }

		public IntVector2 ImageSize { get; set; }

		public float DescSpacing { get; set; }

		public string FileName { get; set; }

		public bool IgnoreNextSpacing { get; set; }

		public ScaleMode ScalingMode { get; set; }

		public UnityAction<object[]> ClickEvent { get; set; }

		public object[] ClickEventParam { get; set; }
	}

	public sealed class AtlasItem : ItemBase
	{
		public string AtlasName { get; set; }

		public bool AutoUnload { get; set; }
	}

	public class BannerItem : ItemBase
	{
		public string BannerURL { get; set; }
	}

	private class ItemInfo
	{
		public string BannerURL { get; set; }

		public string BannerFileName { get; set; }

		public bool IsLoaded { get; set; }

		public bool IsChanged { get; set; }

		public Vector2 CachedHeight { get; set; }

		public ItemBase Config { get; set; }

		public GameObject Item { get; set; }

		public Transform Image { get; set; }

		public IntVector2 CachedImageSize { get; set; }

		public bool IgnoreNextSpacing { get; set; }

		public int HeaderDefaultFontSize { get; set; }

		public int FooterDefaultFontSize { get; set; }

		public bool FlexibleImageSizeByInnerText { get; set; }

		public ItemInfo()
		{
			IsChanged = true;
		}

		public float GetCachedHeight(ScreenOrientation o)
		{
			if (o == ScreenOrientation.LandscapeLeft || o == ScreenOrientation.LandscapeRight)
			{
				return CachedHeight.y;
			}
			return CachedHeight.x;
		}
	}

	private sealed class ClickEvent : UnityEvent<object[]>
	{
	}

	private sealed class ClickEventArgs
	{
		public ClickEvent Event { get; set; }

		public object[] Param { get; set; }
	}

	public struct MakeConfig
	{
		public GameObject Parent;

		public int Depth;

		public int SortingOrder;

		public float Spacing;

		public ItemBase[] Item;

		public MakeAnchor Anchor;

		public Vector4 StretchLRTB;

		public Vector4 ScalingXYWH_Portrait;

		public Vector4 ScalingXYWH_Landscape;

		public bool HideScrollBar;

		public ScreenOrientation InitOrientation;

		public float InitScrollValue;

		public Func<Vector2> FuncGetRealtimeScaleOffset;

		public int TextureMaxWidth;

		public bool IsClip;
	}

	private struct DepthInfo
	{
		public UIWidget Widget;

		public UIPanel Panel;

		public int Offset;
	}

	private const string GO_NAME_HEADER = "Header";

	private const string GO_NAME_FOOTER = "Footer";

	private const string GO_NAME_INNER = "Inner";

	private const string GO_NAME_IMAGE = "Image";

	private const int ATLAS_HEIGHT_OFFSET = 20;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.NETWORK_00);

	private int mLoadCount;

	private List<ItemInfo> mInfo = new List<ItemInfo>();

	private List<DepthInfo> mDepthOffset = new List<DepthInfo>();

	private Dictionary<string, LinkBannerRequest> mLinkBannerReq = new Dictionary<string, LinkBannerRequest>();

	private Dictionary<string, List<LinkBannerInfo>> mLinkBanner = new Dictionary<string, List<LinkBannerInfo>>();

	private List<string> mReserveUnloadAtlasName = new List<string>();

	private Dictionary<GameObject, ClickEventArgs> mClickEventTarget = new Dictionary<GameObject, ClickEventArgs>();

	private UISprite mDragger;

	private UIScrollView mScrollView;

	private UIPanel mScrollViewPanel;

	private UIScrollBar mScrollBar;

	private float mItemSpacing;

	private int mLoadingBannerCount;

	private List<string> mConnectedBannerBaseURL = new List<string>();

	private Vector2 mCachedScrollViewPanelSize = Vector2.zero;

	private MakeAnchor mAnchorMode;

	private ScreenOrientation mOrientation;

	private bool mIsUpdateOrientation;

	private Dictionary<SimpleOrientation, float> mOrientationScrollValue = new Dictionary<SimpleOrientation, float>
	{
		{
			SimpleOrientation.Portrait,
			0f
		},
		{
			SimpleOrientation.Landscape,
			0f
		}
	};

	private Vector4 mScalingXYWH_Portrait = Vector4.zero;

	private Vector4 mScalingXYWH_Landscape = Vector4.zero;

	private Func<Vector2> mFuncGetRealtimeScaleOffset;

	private bool mIsLayoutChanged;

	private Dictionary<ScreenOrientation, float> mViewWidth = new Dictionary<ScreenOrientation, float>();

	private Dictionary<ScreenOrientation, float> mItemScaleRatio = new Dictionary<ScreenOrientation, float>();

	private int mTextureMaxWidth = 500;

	public bool IsBusy
	{
		get
		{
			return mState.GetStatus() != STATE.MAIN;
		}
	}

	public bool MightUpdateCollider { private get; set; }

	public float ScrollValue
	{
		get
		{
			return (!(mScrollBar != null)) ? 0f : mScrollBar.value;
		}
		set
		{
			if (mScrollBar != null)
			{
				mScrollBar.value = value;
			}
		}
	}

	private void Update()
	{
		if (mState.IsChanged())
		{
			STATE status = mState.GetStatus();
			if (status == STATE.MAIN)
			{
				if ((bool)mDragger)
				{
					int height = mDragger.height;
					mDragger.UpdateAnchors();
					mDragger.SetDimensions(mDragger.width, height);
					if (!mDragger.GetComponent<BoxCollider>())
					{
						mDragger.gameObject.AddComponent<BoxCollider>();
					}
					mDragger.ResizeCollider();
				}
				BoxCollider[] array = (from n in mInfo
					where n.Item
					select n.Item.GetComponent<BoxCollider>() into n
					where n
					select n).ToArray();
				if (array != null)
				{
					BoxCollider[] array2 = array;
					foreach (BoxCollider boxCollider in array2)
					{
						boxCollider.enabled = true;
					}
				}
			}
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (mLoadCount == mInfo.Count && mInfo.All((ItemInfo n) => n.IsLoaded))
			{
				mState.Change(STATE.MAIN);
				break;
			}
			if (mLoadingBannerCount <= 0 && mInfo.Any((ItemInfo n) => !mConnectedBannerBaseURL.Contains(n.BannerURL)))
			{
				mState.Change(STATE.NETWORK_00);
				goto case STATE.NETWORK_00;
			}
			break;
		case STATE.NETWORK_00:
			if (mInfo != null)
			{
				ItemInfo baseInfo = mInfo.FirstOrDefault((ItemInfo n) => !string.IsNullOrEmpty(n.BannerFileName) && !mConnectedBannerBaseURL.Contains(n.BannerURL));
				if (baseInfo != null)
				{
					if (mLinkBannerReq == null)
					{
						mLinkBannerReq = new Dictionary<string, LinkBannerRequest>();
					}
					if (!mLinkBannerReq.ContainsKey(baseInfo.BannerURL))
					{
						mLinkBannerReq.Add(baseInfo.BannerURL, new LinkBannerRequest());
					}
					mLinkBanner = null;
					LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerReq[baseInfo.BannerURL]);
					mLinkBannerReq[baseInfo.BannerURL].SetHighPriority(true);
					ItemInfo[] array3 = mInfo.Where((ItemInfo n) => !string.IsNullOrEmpty(n.BannerFileName) && n.BannerURL == baseInfo.BannerURL).ToArray();
					if (array3 != null)
					{
						LinkBannerSetting setting = new LinkBannerSetting();
						setting.BannerList = new List<LinkBannerDetail>();
						array3.Where((ItemInfo n) => n.Config != null).ForEach(delegate(ItemInfo n)
						{
							setting.BannerList.Add(new LinkBannerDetail
							{
								Action = LinkBannerInfo.ACTION.NOP.ToString(),
								Param = n.Config.ID,
								FileName = n.BannerFileName
							});
						});
						if (setting.BannerList.Count > 0)
						{
							mLoadingBannerCount = setting.BannerList.Count;
							if (mLinkBannerReq[baseInfo.BannerURL].mReceiveEveryTimeCallback == null)
							{
								mLinkBannerReq[baseInfo.BannerURL].mReceiveEveryTimeCallback = new LinkBannerRequest.OnReceiveEveryTimeCallback();
								mLinkBannerReq[baseInfo.BannerURL].mReceiveEveryTimeCallback.AddListener(delegate(LinkBannerInfo _info)
								{
									if (mLinkBanner == null)
									{
										mLinkBanner = new Dictionary<string, List<LinkBannerInfo>>();
									}
									if (!mLinkBanner.ContainsKey(setting.BaseURL))
									{
										mLinkBanner.Add(baseInfo.BannerURL, new List<LinkBannerInfo>());
									}
									mLinkBanner[baseInfo.BannerURL].Add(_info);
								});
							}
							setting.BaseURL = baseInfo.BannerURL;
							LinkBannerManager.Instance.GetLinkBannerRequest(mLinkBannerReq[baseInfo.BannerURL], setting, delegate
							{
							});
							mConnectedBannerBaseURL.Add(baseInfo.BannerURL);
						}
					}
				}
			}
			mState.Change(STATE.INIT);
			break;
		}
		mState.Update();
		if (Util.ScreenOrientation != mOrientation)
		{
			mOrientation = Util.ScreenOrientation;
			mIsUpdateOrientation = true;
			if (mScrollBar != null)
			{
				mScrollBar.value = 0f;
			}
			if (mInfo != null)
			{
				foreach (ItemInfo item in mInfo)
				{
					if (item != null)
					{
						item.IsChanged = true;
					}
				}
			}
		}
		switch (mOrientation)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mOrientationScrollValue[SimpleOrientation.Portrait] = ScrollValue;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mOrientationScrollValue[SimpleOrientation.Landscape] = ScrollValue;
			break;
		}
	}

	private void LateUpdate()
	{
		if (mScrollViewPanel == null)
		{
			mScrollViewPanel = mScrollView.GetComponent<UIPanel>();
		}
		if (mScrollViewPanel != null && mState.GetStatus() == STATE.MAIN)
		{
			switch (mAnchorMode)
			{
			case MakeAnchor.Stretch:
				if (mCachedScrollViewPanelSize != mScrollViewPanel.GetViewSize())
				{
					MightUpdateCollider = true;
					mCachedScrollViewPanelSize = mScrollViewPanel.GetViewSize();
					switch (mOrientation)
					{
					case ScreenOrientation.Portrait:
					case ScreenOrientation.PortraitUpsideDown:
						ScrollValue = mOrientationScrollValue[SimpleOrientation.Landscape];
						break;
					case ScreenOrientation.LandscapeLeft:
					case ScreenOrientation.LandscapeRight:
						ScrollValue = mOrientationScrollValue[SimpleOrientation.Portrait];
						break;
					}
				}
				break;
			case MakeAnchor.Scaling:
				if (mIsUpdateOrientation && mIsLayoutChanged)
				{
					mIsUpdateOrientation = false;
					mIsLayoutChanged = false;
					MightUpdateCollider = true;
					switch (mOrientation)
					{
					case ScreenOrientation.Portrait:
					case ScreenOrientation.PortraitUpsideDown:
					{
						Vector4 baseClipRegion2 = mScalingXYWH_Portrait;
						Vector2 vector2 = ((mFuncGetRealtimeScaleOffset == null) ? Vector2.zero : mFuncGetRealtimeScaleOffset());
						baseClipRegion2.z += vector2.x;
						baseClipRegion2.w += vector2.y;
						mScrollViewPanel.baseClipRegion = baseClipRegion2;
						break;
					}
					case ScreenOrientation.LandscapeLeft:
					case ScreenOrientation.LandscapeRight:
					{
						Vector4 baseClipRegion = mScalingXYWH_Landscape;
						Vector2 vector = ((mFuncGetRealtimeScaleOffset == null) ? Vector2.zero : mFuncGetRealtimeScaleOffset());
						baseClipRegion.z += vector.x;
						baseClipRegion.w += vector.y;
						mScrollViewPanel.baseClipRegion = baseClipRegion;
						break;
					}
					}
				}
				break;
			}
		}
		if (!MightUpdateCollider)
		{
			return;
		}
		MightUpdateCollider = false;
		float num = 0f;
		if (mInfo != null)
		{
			for (int i = 0; i < mInfo.Count; i++)
			{
				if (mInfo[i] == null || !mInfo[i].Item)
				{
					continue;
				}
				Transform transform = mInfo[i].Item.transform;
				if (transform == null)
				{
					continue;
				}
				transform.SetLocalPositionY(0f - num);
				if (!mInfo[i].IsChanged)
				{
					num += mInfo[i].GetCachedHeight(mOrientation);
				}
				else
				{
					float num2 = 0f;
					float x = 0f;
					float num3 = 0f;
					Vector2 zero = Vector2.zero;
					Transform transform2 = transform.Find("Header");
					if ((bool)transform2)
					{
						UILabel component = transform2.GetComponent<UILabel>();
						if ((bool)component)
						{
							component.fontSize = (int)((float)mInfo[i].HeaderDefaultFontSize * GetItemScaleRatio(mOrientation));
							num2 += (float)component.fontSize;
							zero.x += (int)((float)mInfo[i].HeaderDefaultFontSize * GetItemScaleRatio(ScreenOrientation.Portrait));
							zero.y += (int)((float)mInfo[i].HeaderDefaultFontSize * GetItemScaleRatio(ScreenOrientation.LandscapeLeft));
							if (mInfo[i].Config != null)
							{
								num2 += mInfo[i].Config.DescSpacing * GetItemScaleRatio(mOrientation);
								zero.x += (int)(mInfo[i].Config.DescSpacing * GetItemScaleRatio(ScreenOrientation.Portrait));
								zero.y += (int)(mInfo[i].Config.DescSpacing * GetItemScaleRatio(ScreenOrientation.LandscapeLeft));
							}
							num3 -= num2;
						}
					}
					if ((bool)mInfo[i].Image)
					{
						mInfo[i].Image.SetLocalPositionY(0f - num2);
						UIBasicSprite component2 = mInfo[i].Image.GetComponent<UIBasicSprite>();
						if ((bool)component2)
						{
							int num4 = ((component2 is UISprite) ? 20 : 0);
							component2.SetDimensions((int)((float)mInfo[i].CachedImageSize.x * GetItemScaleRatio(mOrientation)), (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(mOrientation) + (float)num4));
							num2 += (float)component2.height;
							x = component2.width;
							num3 -= (float)component2.height * 0.5f;
							zero.x += (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(ScreenOrientation.Portrait) + (float)num4);
							zero.y += (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(ScreenOrientation.LandscapeLeft) + (float)num4);
						}
						transform2 = mInfo[i].Image.Find("Inner");
						if (mInfo[i].Config.Inner != null && (bool)transform2 && (bool)component2)
						{
							ItemBase.DescInfo inner = mInfo[i].Config.Inner;
							UILabel component3 = transform2.GetComponent<UILabel>();
							if (!component3)
							{
								int fontSize = (int)((float)inner.Size * GetItemScaleRatio(mOrientation));
								Vector3 position = inner.Position;
								component3 = transform2.gameObject.AddComponent<UILabel>();
								component3.bitmapFont = GameMain.LoadFont();
								Util.SetLabelInfo(component3, inner.Text, component2.depth + 1, inner.Position, fontSize, 0, 0, inner.Pivot);
								Util.SetLabelColor(component3, inner.Color);
								int height = component3.height;
								component3.SetAnchor(mInfo[i].Image);
								component3.leftAnchor.relative = 0f;
								component3.rightAnchor.relative = 1f;
								component3.bottomAnchor.relative = 0f;
								component3.topAnchor.relative = 1f;
								component3.leftAnchor.absolute = 20;
								component3.rightAnchor.absolute = -20;
								component3.bottomAnchor.absolute = 10;
								component3.topAnchor.absolute = -10;
								component3.UpdateAnchors();
								if (component2 is UISprite)
								{
									position -= new Vector3(0f, 10f + (float)component3.height * 0.5f);
								}
								component3.SetLocalPositionY(position.y);
								component3.fontSize = (int)((float)inner.Size * GetItemScaleRatio(mOrientation));
								if (mInfo[i].FlexibleImageSizeByInnerText)
								{
									int num5 = ((component2 is UISprite) ? 20 : 0);
									num2 -= (float)component2.height;
									num3 += (float)component2.height * 0.5f;
									zero.x -= (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(ScreenOrientation.Portrait) + (float)num5);
									zero.y -= (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(ScreenOrientation.LandscapeLeft) + (float)num5);
									mInfo[i].CachedImageSize = new IntVector2(mTextureMaxWidth, height);
									component2.SetDimensions((int)((float)mInfo[i].CachedImageSize.x * GetItemScaleRatio(mOrientation)), (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(mOrientation) + (float)num5));
									num2 += (float)component2.height;
									num3 -= (float)component2.height * 0.5f;
									zero.x += (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(ScreenOrientation.Portrait) + (float)num5);
									zero.y += (int)((float)mInfo[i].CachedImageSize.y * GetItemScaleRatio(ScreenOrientation.LandscapeLeft) + (float)num5);
								}
							}
							else
							{
								component3.fontSize = (int)((float)inner.Size * GetItemScaleRatio(mOrientation));
								Vector3 position2 = inner.Position;
								if (component2 is UISprite)
								{
									position2 -= new Vector3(0f, 10f + (float)component3.height * 0.5f);
								}
								component3.SetLocalPositionY(position2.y);
							}
						}
					}
					transform2 = transform.Find("Footer");
					if ((bool)transform2)
					{
						UILabel component4 = transform2.GetComponent<UILabel>();
						if ((bool)component4)
						{
							if (mInfo[i].Config != null)
							{
								num2 += mInfo[i].Config.DescSpacing * GetItemScaleRatio(mOrientation);
								zero.x += (int)(mInfo[i].Config.DescSpacing * GetItemScaleRatio(ScreenOrientation.Portrait));
								zero.y += (int)(mInfo[i].Config.DescSpacing * GetItemScaleRatio(ScreenOrientation.LandscapeLeft));
							}
							component4.SetLocalPositionY(0f - num2);
							component4.fontSize = (int)((float)mInfo[i].FooterDefaultFontSize * GetItemScaleRatio(mOrientation));
							num2 += (float)component4.fontSize;
							zero.x += (int)((float)mInfo[i].FooterDefaultFontSize * GetItemScaleRatio(ScreenOrientation.Portrait));
							zero.y += (int)((float)mInfo[i].FooterDefaultFontSize * GetItemScaleRatio(ScreenOrientation.LandscapeLeft));
						}
					}
					if (mInfo != null && mInfo[i] != null && mInfo[i].Item != null)
					{
						BoxCollider component5 = mInfo[i].Item.GetComponent<BoxCollider>();
						if ((bool)component5)
						{
							component5.center = new Vector3(0f, num3, 0f);
							component5.size = new Vector3(x, num2, 1f);
						}
					}
					mInfo[i].CachedHeight = zero;
					mInfo[i].IsChanged = false;
					num += num2;
				}
				if (i + 1 < mInfo.Count && !mInfo[i].IgnoreNextSpacing)
				{
					num += mItemSpacing * GetItemScaleRatio(mOrientation);
				}
			}
		}
		Vector2 zero2 = Vector2.zero;
		if ((bool)mScrollViewPanel)
		{
			mScrollViewPanel.ResetAndUpdateAnchors();
			zero2.x = mScrollViewPanel.width;
			zero2.y = mScrollViewPanel.height;
		}
		if ((bool)mDragger)
		{
			if (zero2.y > num)
			{
				mDragger.SetDimensions((int)zero2.x, (int)zero2.y);
			}
			else
			{
				mDragger.SetDimensions((int)zero2.x, (int)num);
			}
			mDragger.ResizeCollider();
		}
	}

	public void SetDepth(int depth)
	{
		if (mDepthOffset == null)
		{
			return;
		}
		foreach (DepthInfo item in mDepthOffset)
		{
			if (item.Widget != null)
			{
				item.Widget.depth = depth + item.Offset;
			}
			if (item.Panel != null)
			{
				item.Panel.depth = depth + item.Offset;
			}
		}
	}

	public void SetLayout()
	{
		mIsLayoutChanged = true;
	}

	public static SMVerticalVariableList Make(MakeConfig config)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		UILabel uILabel = null;
		GameObject gameObject = Util.CreateGameObject("SmVerticalVariableList", config.Parent);
		SMVerticalVariableList result = gameObject.AddComponent<SMVerticalVariableList>();
		result.mItemSpacing = config.Spacing;
		result.mAnchorMode = config.Anchor;
		result.mOrientation = config.InitOrientation;
		result.mScalingXYWH_Portrait = config.ScalingXYWH_Portrait;
		result.mScalingXYWH_Landscape = config.ScalingXYWH_Landscape;
		result.mFuncGetRealtimeScaleOffset = config.FuncGetRealtimeScaleOffset;
		result.mTextureMaxWidth = config.TextureMaxWidth;
		UIPanel uIPanel = gameObject.AddComponent<UIPanel>();
		uIPanel.depth = config.Depth;
		uIPanel.sortingOrder = config.SortingOrder;
		if (config.IsClip)
		{
			uIPanel.clipping = UIDrawCall.Clipping.SoftClip;
			uIPanel.clipSoftness = Vector2.zero;
		}
		else
		{
			uIPanel.clipping = UIDrawCall.Clipping.ConstrainButDontClip;
		}
		MakeAnchor anchor = config.Anchor;
		if (anchor != 0 && anchor == MakeAnchor.Stretch)
		{
			uIPanel.SetAnchor(gameObject.GetComponentInParent<UIRoot>().transform);
			uIPanel.leftAnchor.relative = 0f;
			uIPanel.rightAnchor.relative = 1f;
			uIPanel.bottomAnchor.relative = 0f;
			uIPanel.topAnchor.relative = 1f;
			uIPanel.leftAnchor.absolute = (int)config.StretchLRTB.x;
			uIPanel.rightAnchor.absolute = (int)config.StretchLRTB.y;
			uIPanel.bottomAnchor.absolute = (int)config.StretchLRTB.w;
			uIPanel.topAnchor.absolute = (int)config.StretchLRTB.z;
		}
		else
		{
			Transform anchor2 = null;
			uIPanel.SetAnchor(anchor2);
			Vector4 zero = Vector4.zero;
			Vector2 vector = ((config.FuncGetRealtimeScaleOffset == null) ? Vector2.zero : config.FuncGetRealtimeScaleOffset());
			ScreenOrientation initOrientation = config.InitOrientation;
			zero = ((initOrientation != ScreenOrientation.Portrait && initOrientation != ScreenOrientation.PortraitUpsideDown) ? config.ScalingXYWH_Landscape : config.ScalingXYWH_Portrait);
			zero.z += vector.x;
			zero.w += vector.y;
			uIPanel.baseClipRegion = zero;
		}
		uIPanel.ResetAndUpdateAnchors();
		result.mDepthOffset.Add(new DepthInfo
		{
			Panel = uIPanel,
			Offset = 0
		});
		result.mScrollView = gameObject.AddComponent<UIScrollView>();
		result.mScrollView.contentPivot = UIWidget.Pivot.Top;
		result.mScrollView.movement = UIScrollView.Movement.Vertical;
		uISprite = Util.CreateSprite("Dragger", gameObject, image);
		Util.SetSpriteInfo(uISprite, "null", config.Depth + 1, Vector3.zero, Vector3.one, false, UIWidget.Pivot.Top);
		uISprite.SetDimensions(0, 0);
		uISprite.SetAnchor(gameObject);
		uISprite.leftAnchor.relative = 0f;
		uISprite.rightAnchor.relative = 1f;
		uISprite.bottomAnchor.relative = 0f;
		uISprite.topAnchor.relative = 1f;
		uISprite.leftAnchor.absolute = 0;
		uISprite.rightAnchor.absolute = 0;
		uISprite.bottomAnchor.absolute = 0;
		uISprite.topAnchor.absolute = 0;
		uISprite.ResetAndUpdateAnchors();
		uISprite.updateAnchors = UIRect.AnchorUpdate.OnEnable;
		gameObject = uISprite.gameObject;
		result.mDragger = uISprite;
		result.mDepthOffset.Add(new DepthInfo
		{
			Widget = uISprite,
			Offset = 1
		});
		UIDragScrollView uIDragScrollView = gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = result.mScrollView;
		if (config.Item != null)
		{
			result.mLoadCount = config.Item.Length;
			float num = 0f;
			ItemBase[] item = config.Item;
			foreach (ItemBase itemBase in item)
			{
				if (itemBase == null)
				{
					continue;
				}
				GameObject gameObject2 = null;
				ClickEvent clickEvent = new ClickEvent();
				if (itemBase.ClickEvent == null)
				{
					uISprite = Util.CreateSprite(itemBase.ID.ToString(), gameObject, image);
					Util.SetSpriteInfo(uISprite, "null", config.Depth + 2, new Vector3(0f, 0f - num), Vector3.one, false, UIWidget.Pivot.Top);
					gameObject2 = uISprite.gameObject;
					result.mDepthOffset.Add(new DepthInfo
					{
						Widget = uISprite,
						Offset = 2
					});
				}
				else
				{
					UIButton uIButton = Util.CreateImageButton(itemBase.ID.ToString(), gameObject, image);
					Util.SetImageButtonInfo(uIButton, "null", "null", "null", config.Depth + 2, new Vector3(0f, 0f - num), Vector3.one);
					Util.SetImageButtonMessage(uIButton, result, "OnItemClick", UIButtonMessage.Trigger.OnClick);
					gameObject2 = uIButton.gameObject;
					clickEvent.AddListener(itemBase.ClickEvent);
					result.mDepthOffset.Add(new DepthInfo
					{
						Widget = uIButton.GetComponent<UIWidget>(),
						Offset = 2
					});
					BoxCollider component = gameObject2.GetComponent<BoxCollider>();
					component.enabled = false;
					UIDragScrollView uIDragScrollView2 = gameObject2.AddComponent<UIDragScrollView>();
					uIDragScrollView2.scrollView = result.mScrollView;
				}
				Vector2 size = Vector2.zero;
				Vector2 cachedHeight = Vector2.zero;
				ItemInfo info = null;
				if (itemBase.Header != null)
				{
					int num2 = (int)((float)itemBase.Header.Size * result.GetItemScaleRatio(config.InitOrientation));
					float num3 = itemBase.DescSpacing * result.GetItemScaleRatio(config.InitOrientation);
					uILabel = Util.CreateLabel("Header", gameObject2, atlasFont);
					Util.SetLabelInfo(uILabel, itemBase.Header.Text, config.Depth, Vector3.zero, num2, 0, 0, itemBase.Header.Pivot);
					Util.SetLabelColor(uILabel, itemBase.Header.Color);
					size.y += (float)num2 + num3;
					result.mDepthOffset.Add(new DepthInfo
					{
						Widget = uILabel,
						Offset = 0
					});
					if (info == null)
					{
						info = new ItemInfo();
					}
					info.HeaderDefaultFontSize = itemBase.Header.Size;
					cachedHeight.x += (float)(int)((float)itemBase.Header.Size * result.GetItemScaleRatio(ScreenOrientation.Portrait)) + itemBase.DescSpacing * result.GetItemScaleRatio(ScreenOrientation.Portrait);
					cachedHeight.y += (float)(int)((float)itemBase.Header.Size * result.GetItemScaleRatio(ScreenOrientation.LandscapeLeft)) + itemBase.DescSpacing * result.GetItemScaleRatio(ScreenOrientation.LandscapeLeft);
				}
				if (itemBase is AtlasItem)
				{
					AtlasItem atlasItem = itemBase as AtlasItem;
					if (atlasItem != null)
					{
						result.StartCoroutine(result.LoadImageAsync(gameObject2, atlasItem, info, 0f - size.y, config.Depth, 3, delegate(Vector2 _size, ItemInfo _info)
						{
							size += _size;
							info = _info;
							cachedHeight.x += _size.x * result.GetItemScaleRatio(ScreenOrientation.Portrait);
							cachedHeight.y += _size.y * result.GetItemScaleRatio(ScreenOrientation.LandscapeLeft);
						}));
					}
				}
				else if (itemBase is BannerItem)
				{
					BannerItem bannerItem = itemBase as BannerItem;
					if (bannerItem != null)
					{
						result.StartCoroutine(result.LoadBannerAsync(gameObject2, bannerItem, info, 0f - size.y, config.Depth, 3, delegate(Vector2 _size, ItemInfo _info)
						{
							size += _size;
							info = _info;
							cachedHeight.x += _size.x * result.GetItemScaleRatio(ScreenOrientation.Portrait);
							cachedHeight.y += _size.y * result.GetItemScaleRatio(ScreenOrientation.LandscapeLeft);
						}));
					}
				}
				if (itemBase.Footer != null)
				{
					size.y += itemBase.DescSpacing * result.GetItemScaleRatio(config.InitOrientation);
					int num4 = (int)((float)itemBase.Footer.Size * result.GetItemScaleRatio(config.InitOrientation));
					uILabel = Util.CreateLabel("Footer", gameObject2, atlasFont);
					Util.SetLabelInfo(uILabel, itemBase.Footer.Text, config.Depth, new Vector3(0f, 0f - size.y), num4, 0, 0, itemBase.Footer.Pivot);
					Util.SetLabelColor(uILabel, itemBase.Footer.Color);
					size.y += num4;
					result.mDepthOffset.Add(new DepthInfo
					{
						Widget = uILabel,
						Offset = 0
					});
					if (info == null)
					{
						info = new ItemInfo();
					}
					info.FooterDefaultFontSize = itemBase.Footer.Size;
					cachedHeight.x += (float)(int)((float)itemBase.Footer.Size * result.GetItemScaleRatio(ScreenOrientation.Portrait)) + itemBase.DescSpacing * result.GetItemScaleRatio(ScreenOrientation.Portrait);
					cachedHeight.y += (float)(int)((float)itemBase.Footer.Size * result.GetItemScaleRatio(ScreenOrientation.LandscapeLeft)) + itemBase.DescSpacing * result.GetItemScaleRatio(ScreenOrientation.LandscapeLeft);
				}
				num += size.y;
				if (info != null && result.mInfo != null)
				{
					int num5 = result.mInfo.IndexOf(info);
					if (0 <= num5 && num5 < result.mInfo.Count)
					{
						result.mInfo[num5].CachedHeight = cachedHeight;
						result.mInfo[num5].IgnoreNextSpacing = itemBase.IgnoreNextSpacing;
					}
				}
				BoxCollider component2 = gameObject2.GetComponent<BoxCollider>();
				if ((bool)component2)
				{
					component2.size = size;
				}
				result.mClickEventTarget.Add(gameObject2, new ClickEventArgs
				{
					Event = clickEvent,
					Param = itemBase.ClickEventParam
				});
				if (!itemBase.IgnoreNextSpacing)
				{
					num += config.Spacing;
				}
			}
		}
		result.mScrollBar = Util.CreateGameObject("Scroll", config.Parent).AddComponent<UIScrollBar>();
		result.ScrollValue = config.InitScrollValue;
		result.mScrollView.verticalScrollBar = result.mScrollBar;
		if (!config.HideScrollBar)
		{
		}
		return result;
	}

	private float GetViewWidth(ScreenOrientation o)
	{
		if (mViewWidth == null)
		{
			mViewWidth = new Dictionary<ScreenOrientation, float>();
		}
		if (!mViewWidth.ContainsKey(o))
		{
			Vector2 vector = Util.LogScreenSize();
			float num = vector.x;
			if (!Util.IsWideScreen())
			{
				switch (o)
				{
				case ScreenOrientation.Portrait:
				case ScreenOrientation.PortraitUpsideDown:
					switch (mOrientation)
					{
					case ScreenOrientation.Portrait:
					case ScreenOrientation.PortraitUpsideDown:
						num = vector.x;
						break;
					case ScreenOrientation.LandscapeLeft:
					case ScreenOrientation.LandscapeRight:
						num = vector.y;
						break;
					}
					break;
				case ScreenOrientation.LandscapeLeft:
				case ScreenOrientation.LandscapeRight:
					switch (mOrientation)
					{
					case ScreenOrientation.Portrait:
					case ScreenOrientation.PortraitUpsideDown:
						num = vector.y;
						break;
					case ScreenOrientation.LandscapeLeft:
					case ScreenOrientation.LandscapeRight:
						num = vector.x;
						break;
					}
					break;
				}
			}
			else
			{
				Vector2 vector2 = UIChildAnchor.CalculateLocalPos(SingletonMonoBehaviour<GameMain>.Instance.mRoot.manualHeight, SingletonMonoBehaviour<GameMain>.Instance.mRoot.manualWidth, UIAnchor.Side.TopLeft, mOrientation);
				Vector2 vector3 = UIChildAnchor.CalculateLocalPos(SingletonMonoBehaviour<GameMain>.Instance.mRoot.manualHeight, SingletonMonoBehaviour<GameMain>.Instance.mRoot.manualWidth, UIAnchor.Side.BottomRight, mOrientation);
				switch (o)
				{
				case ScreenOrientation.Portrait:
				case ScreenOrientation.PortraitUpsideDown:
					switch (mOrientation)
					{
					case ScreenOrientation.Portrait:
					case ScreenOrientation.PortraitUpsideDown:
						num = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x);
						break;
					case ScreenOrientation.LandscapeLeft:
					case ScreenOrientation.LandscapeRight:
						num = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y);
						break;
					}
					break;
				case ScreenOrientation.LandscapeLeft:
				case ScreenOrientation.LandscapeRight:
					switch (mOrientation)
					{
					case ScreenOrientation.Portrait:
					case ScreenOrientation.PortraitUpsideDown:
						num = Mathf.Abs(vector2.y) + Mathf.Abs(vector3.y);
						break;
					case ScreenOrientation.LandscapeLeft:
					case ScreenOrientation.LandscapeRight:
						num = Mathf.Abs(vector2.x) + Mathf.Abs(vector3.x);
						break;
					}
					break;
				}
			}
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mViewWidth.Add(o, num * 0.95f);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mViewWidth.Add(o, (num * 0.5f + num * 0.25f) * 0.95f);
				break;
			}
		}
		return mViewWidth[o];
	}

	private float GetItemScaleRatio(ScreenOrientation o)
	{
		if (mItemScaleRatio == null)
		{
			mItemScaleRatio = new Dictionary<ScreenOrientation, float>();
		}
		if (!mItemScaleRatio.ContainsKey(o))
		{
			float viewWidth = GetViewWidth(o);
			mItemScaleRatio.Add(o, viewWidth / (float)mTextureMaxWidth);
		}
		return mItemScaleRatio[o];
	}

	private IEnumerator LoadBannerAsync(GameObject parent, BannerItem info, ItemInfo item, float pos_y, int depth, int depth_offset, UnityAction<Vector2, ItemInfo> on_finished)
	{
		if (!parent || info == null)
		{
			if (on_finished != null)
			{
				on_finished((info.ImageSize == null) ? Vector2.zero : new Vector2(info.ImageSize.x, info.ImageSize.y), null);
			}
			yield break;
		}
		if (item == null)
		{
			item = new ItemInfo();
		}
		item.IsLoaded = false;
		item.Config = info;
		item.BannerURL = info.BannerURL;
		item.BannerFileName = info.FileName;
		item.Item = parent;
		item.CachedImageSize = ((info.ImageSize == null) ? new IntVector2(0, 0) : info.ImageSize);
		mInfo.Add(item);
		IntVector2 size = ((info.ImageSize == null) ? new IntVector2(0, 0) : info.ImageSize);
		bool isFirstLoop = true;
		UnityAction<Vector2, ItemInfo> on_finished2 = default(UnityAction<Vector2, ItemInfo>);
		ItemInfo item2 = default(ItemInfo);
		UnityAction fnFirstLoopEvent = delegate
		{
			if (isFirstLoop)
			{
				isFirstLoop = false;
				if (on_finished2 != null)
				{
					on_finished2(new Vector2(size.x, size.y), item2);
				}
			}
		};
		BannerItem info2 = default(BannerItem);
		while (true)
		{
			if (mLinkBanner == null || !mLinkBanner.ContainsKey(info.BannerURL))
			{
				fnFirstLoopEvent();
				yield return null;
				continue;
			}
			LinkBannerInfo bnrInfo = mLinkBanner[info.BannerURL].FirstOrDefault((LinkBannerInfo n) => n.param == info2.ID);
			if (bnrInfo != null && bnrInfo.banner != null)
			{
				float scale = GetItemScaleRatio(mOrientation);
				IntVector2 bannerSize = new IntVector2(bnrInfo.banner.width, bnrInfo.banner.height);
				UITexture texture = Util.CreateGameObject("Image", parent).AddComponent<UITexture>();
				texture.depth = depth + depth_offset;
				texture.mainTexture = bnrInfo.banner;
				texture.SetDimensions((int)((float)bannerSize.x * scale), (int)((float)bannerSize.y * scale));
				texture.pivot = UIWidget.Pivot.Top;
				texture.SetLocalPositionY(pos_y);
				mDepthOffset.Add(new DepthInfo
				{
					Widget = texture,
					Offset = depth_offset
				});
				int idx = mInfo.IndexOf(item);
				if (idx >= 0 && idx < mInfo.Count)
				{
					float pScale = GetItemScaleRatio(ScreenOrientation.Portrait);
					float lScale = GetItemScaleRatio(ScreenOrientation.LandscapeRight);
					mInfo[idx].IsLoaded = true;
					mInfo[idx].IsChanged = true;
					mInfo[idx].CachedHeight = new Vector2((float)bnrInfo.banner.height * pScale, (float)bnrInfo.banner.height * lScale);
					mInfo[idx].CachedImageSize = new IntVector2(bannerSize.x, bannerSize.y);
					mInfo[idx].Image = texture.transform;
					if (mInfo[idx].Config != null && mInfo[idx].Config.Inner != null)
					{
						Util.CreateGameObject("Inner", texture.gameObject);
					}
				}
				MightUpdateCollider = true;
				mLoadingBannerCount--;
				fnFirstLoopEvent();
				break;
			}
			if (bnrInfo != null && bnrInfo.banner == null)
			{
				break;
			}
			fnFirstLoopEvent();
			yield return null;
		}
	}

	private IEnumerator LoadImageAsync(GameObject parent, AtlasItem info, ItemInfo item, float pos_y, int depth, int depth_offset, UnityAction<Vector2, ItemInfo> on_finished)
	{
		bool isAsync = false;
		int height = 0;
		int width = 0;
		ResImage img = null;
		if ((bool)parent && info != null)
		{
			if (ResourceManager.IsLoaded(info.AtlasName))
			{
				img = ResourceManager.LoadImage(info.AtlasName);
				if (img != null && img.Image != null)
				{
					UISprite sprite2 = Util.CreateSprite("Image", parent, img.Image);
					Util.SetSpriteInfo(sprite2, info.FileName, depth + depth_offset, new Vector3(0f, pos_y), Vector3.one, false, UIWidget.Pivot.Top);
					sprite2.type = UIBasicSprite.Type.Sliced;
					mDepthOffset.Add(new DepthInfo
					{
						Widget = sprite2,
						Offset = depth_offset
					});
					if (item == null)
					{
						item = new ItemInfo();
					}
					item.IsLoaded = true;
					item.Item = parent;
					item.Config = info;
					item.CachedImageSize = new IntVector2(sprite2.width, sprite2.height);
					item.Image = sprite2.transform;
					item.FlexibleImageSizeByInnerText = true;
					Transform child = sprite2.transform.Find("Inner");
					if (child == null)
					{
						child = Util.CreateGameObject("Inner", sprite2.transform.gameObject).transform;
					}
					if (info.Inner != null && (bool)child && (bool)sprite2)
					{
						ItemBase.DescInfo inner = info.Inner;
						UILabel label2 = child.GetComponent<UILabel>();
						if (!label2)
						{
							label2 = child.gameObject.AddComponent<UILabel>();
							label2.bitmapFont = GameMain.LoadFont();
							Util.SetLabelInfo(label2, inner.Text, sprite2.depth + 1, inner.Position - new Vector3(0f, 10f + (float)inner.Size * 0.5f), inner.Size, 0, 0, inner.Pivot);
							Util.SetLabelColor(label2, inner.Color);
							int hh = label2.height;
							label2.SetAnchor(item.Image);
							label2.leftAnchor.relative = 0f;
							label2.rightAnchor.relative = 1f;
							label2.bottomAnchor.relative = 0f;
							label2.topAnchor.relative = 1f;
							label2.leftAnchor.absolute = 20;
							label2.rightAnchor.absolute = -20;
							label2.bottomAnchor.absolute = 10;
							label2.topAnchor.absolute = -10;
							label2.UpdateAnchors();
							mDepthOffset.Add(new DepthInfo
							{
								Widget = label2,
								Offset = depth_offset + 1
							});
							item.CachedImageSize = new IntVector2(mTextureMaxWidth, hh);
						}
					}
					MightUpdateCollider = true;
					height = sprite2.height;
					width = sprite2.width;
					goto IL_0569;
				}
			}
			img = ResourceManager.LoadImageAsync(info.AtlasName);
			height = ((info.ImageSize != null) ? info.ImageSize.y : 0);
			width = ((info.ImageSize != null) ? info.ImageSize.x : 0);
			isAsync = true;
			if (item == null)
			{
				item = new ItemInfo();
			}
			item.IsLoaded = false;
			item.Item = parent;
			item.Config = info;
			item.CachedImageSize = new IntVector2(width, height);
			item.FlexibleImageSizeByInnerText = true;
		}
		goto IL_0569;
		IL_0569:
		if (item != null)
		{
			mInfo.Add(item);
		}
		if (on_finished != null)
		{
			on_finished(new Vector2(width, height), item);
		}
		if (!isAsync)
		{
			yield break;
		}
		while (img.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		UISprite sprite = Util.CreateSprite("Image", parent, img.Image);
		Util.SetSpriteInfo(sprite, info.FileName, depth, new Vector3(0f, pos_y), Vector3.one, false, UIWidget.Pivot.Top);
		sprite.type = UIBasicSprite.Type.Sliced;
		mDepthOffset.Add(new DepthInfo
		{
			Widget = sprite,
			Offset = depth_offset
		});
		int idx = mInfo.IndexOf(item);
		if (idx >= 0 && idx < mInfo.Count)
		{
			mInfo[idx].IsLoaded = true;
			mInfo[idx].IsChanged = true;
			mInfo[idx].CachedImageSize = new IntVector2(sprite.width, sprite.height);
			mInfo[idx].Image = sprite.transform;
			if (mInfo[idx].Config != null && mInfo[idx].Config.Inner != null)
			{
				Util.CreateGameObject("Inner", sprite.gameObject);
			}
		}
		MightUpdateCollider = true;
	}

	private void OnDestroy()
	{
		if (mReserveUnloadAtlasName != null)
		{
			foreach (string item in mReserveUnloadAtlasName)
			{
				ResourceManager.UnloadImage(item);
			}
			mReserveUnloadAtlasName.Clear();
			mReserveUnloadAtlasName = null;
		}
		mInfo.Clear();
		mDepthOffset.Clear();
		mClickEventTarget.Clear();
		mConnectedBannerBaseURL.Clear();
		mInfo = null;
		mDepthOffset = null;
		mReserveUnloadAtlasName = null;
		mClickEventTarget = null;
		mConnectedBannerBaseURL = null;
		mDragger = null;
		mScrollView = null;
		if (mLinkBanner != null)
		{
			string[] array = mLinkBanner.Keys.ToArray();
			string[] array2 = array;
			foreach (string key in array2)
			{
				mLinkBanner[key] = null;
			}
			mLinkBanner.Clear();
		}
		mLinkBanner = null;
		if (mLinkBannerReq != null)
		{
			string[] array3 = mLinkBannerReq.Keys.ToArray();
			string[] array4 = array3;
			foreach (string key2 in array4)
			{
				LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerReq[key2]);
				mLinkBannerReq[key2] = null;
			}
			mLinkBannerReq.Clear();
		}
		mLinkBannerReq = null;
	}

	private void OnItemClick(GameObject go)
	{
		if (!IsBusy && mClickEventTarget != null && mClickEventTarget.ContainsKey(go) && mClickEventTarget.ContainsKey(go) && mClickEventTarget[go] != null && mClickEventTarget[go].Event != null)
		{
			mClickEventTarget[go].Event.Invoke(mClickEventTarget[go].Param);
		}
	}
}
