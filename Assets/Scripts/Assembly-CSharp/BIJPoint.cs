using UnityEngine;

public class BIJPoint
{
	public static readonly BIJPoint EMPTY = new BIJPoint(0, 0);

	public int X { get; set; }

	public int Y { get; set; }

	public BIJPoint()
		: this(0, 0)
	{
	}

	public BIJPoint(int x, int y)
	{
		X = x;
		Y = y;
	}

	public override string ToString()
	{
		return string.Format("({0,0}, {1,0})", X, Y);
	}

	public static int Dot(BIJPoint a, BIJPoint b)
	{
		return a.X * b.X + a.Y * b.Y;
	}

	public static int Cross(BIJPoint a, BIJPoint b)
	{
		return a.X * b.Y - b.X * a.Y;
	}

	public static bool IsInsideTriangle(BIJPoint p, BIJPoint a, BIJPoint b, BIJPoint c)
	{
		if (Cross(p - a, b - a) < 0)
		{
			return false;
		}
		if (Cross(p - b, c - b) < 0)
		{
			return false;
		}
		if (Cross(p - c, a - c) < 0)
		{
			return false;
		}
		return true;
	}

	public static explicit operator Vector2(BIJPoint rhs)
	{
		return new Vector2(rhs.X, rhs.Y);
	}

	public static BIJPoint operator +(BIJPoint a, BIJPoint b)
	{
		return new BIJPoint(a.X + b.X, a.Y + b.Y);
	}

	public static BIJPoint operator -(BIJPoint a, BIJPoint b)
	{
		return new BIJPoint(a.X - b.X, a.Y - b.Y);
	}
}
