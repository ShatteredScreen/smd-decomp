using System.Collections;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
	private struct LOOPInfo
	{
		public float mLoopEd;

		public float mLoopSt;
	}

	private struct FADEINFO
	{
		public AudioSource source;

		public int ch;

		public float volume;

		public float time;
	}

	public static SoundManager instance;

	public int channel_num_music = 3;

	public int channel_num_se = 3;

	public int channel_num_autose = 3;

	private int m_ChannelNum;

	private int m_ChannelMusicBottom;

	private int m_ChannelSeBottom;

	private int m_ChannelAutoSeBottom;

	private int mSize;

	private bool mLoopBGM;

	private AudioSource[] m_ChannelObject;

	private bool[] m_UnloadAtPlayEnd;

	private string[] m_ChannelObjectKey;

	private float m_MusicVolume;

	private float m_SEVolume;

	private string[] nowMusicKeys;

	private static LOOPInfo mIntroBGM;

	private AudioSource mAudiosrc;

	public float MusicVolume
	{
		get
		{
			return m_MusicVolume;
		}
		set
		{
			m_MusicVolume = value;
			for (int i = 0; i < channel_num_music; i++)
			{
				setMusicVolume(i, m_MusicVolume);
			}
		}
	}

	public float SEVolume
	{
		get
		{
			return m_SEVolume;
		}
		set
		{
			m_SEVolume = value;
			for (int i = 0; i < channel_num_se; i++)
			{
				setSeVolume(i, m_SEVolume);
			}
			for (int j = 0; j < channel_num_autose; j++)
			{
				setAutoSeVolume(j, m_SEVolume);
			}
		}
	}

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		Object.DontDestroyOnLoad(base.gameObject);
	}

	private void Start()
	{
		if (m_ChannelObject == null)
		{
			m_ChannelNum = channel_num_music + channel_num_se + channel_num_autose;
			nowMusicKeys = new string[channel_num_music];
			for (int i = 0; i < nowMusicKeys.Length; i++)
			{
				nowMusicKeys[i] = string.Empty;
			}
			m_ChannelMusicBottom = 0;
			m_ChannelSeBottom = m_ChannelMusicBottom + channel_num_music;
			m_ChannelAutoSeBottom = m_ChannelSeBottom + channel_num_se;
			m_ChannelObject = new AudioSource[m_ChannelNum];
			m_UnloadAtPlayEnd = new bool[m_ChannelNum];
			m_ChannelObjectKey = new string[m_ChannelNum];
			int num = 0;
			for (int j = 0; j < channel_num_music; j++)
			{
				m_ChannelObject[num] = new GameObject("GoSMMusic" + j).AddComponent<AudioSource>();
				m_ChannelObject[num].gameObject.transform.parent = base.gameObject.transform;
				num++;
			}
			for (int k = 0; k < channel_num_se; k++)
			{
				m_ChannelObject[num] = new GameObject("GoSMSE" + k).AddComponent<AudioSource>();
				m_ChannelObject[num].gameObject.transform.parent = base.gameObject.transform;
				num++;
			}
			for (int l = 0; l < channel_num_autose; l++)
			{
				m_ChannelObject[num] = new GameObject("GoSMAutoSE" + l).AddComponent<AudioSource>();
				m_ChannelObject[num].gameObject.transform.parent = base.gameObject.transform;
				num++;
			}
			for (int m = 0; m < m_ChannelNum; m++)
			{
				Object.DontDestroyOnLoad(m_ChannelObject[m]);
			}
		}
	}

	private void OnDestroy()
	{
		for (int i = 0; i < m_ChannelNum; i++)
		{
			Object.Destroy(m_ChannelObject[i].gameObject);
			m_ChannelObject[i] = null;
		}
		m_ChannelObject = null;
		m_UnloadAtPlayEnd = null;
		m_ChannelObjectKey = null;
	}

	private void Update()
	{
		if (mLoopBGM && mIntroBGM.mLoopEd != 0f && (double)mAudiosrc.time > (double)mIntroBGM.mLoopEd - 0.04)
		{
			mAudiosrc.time = mIntroBGM.mLoopSt;
		}
		for (int i = 0; i < m_ChannelNum; i++)
		{
			AudioSource audioSource = m_ChannelObject[i];
			if (audioSource.clip != null && !audioSource.isPlaying && m_UnloadAtPlayEnd[i])
			{
				removeSound(m_ChannelObjectKey[i]);
				m_ChannelObjectKey[i] = null;
				audioSource.clip = null;
			}
		}
	}

	public bool hasSound(string _keyname)
	{
		return ResourceManager.HasSound(_keyname);
	}

	public AudioClip addSound(string _folderpath, string _filename)
	{
		AudioClip audioClip = null;
		return ResourceManager.LoadSound(_folderpath + _filename, false).mAudioClip;
	}

	public IEnumerator addSoundFromStream(string _filename)
	{
		string url = "jar:file://" + Application.dataPath + "!/assets/" + _filename;
		ResourceManager.LoadSound(_filename, true);
		yield return 0;
	}

	public bool removeSound(string _keyname)
	{
		ResourceManager.RemoveSound(_keyname);
		return true;
	}

	public void ReleaseAllChannelClip()
	{
		for (int i = 0; i < channel_num_music; i++)
		{
			AudioSource audioSource = m_ChannelObject[m_ChannelMusicBottom + i];
			audioSource.Stop();
			audioSource.clip = null;
		}
		for (int j = 0; j < channel_num_se; j++)
		{
			AudioSource audioSource2 = m_ChannelObject[m_ChannelSeBottom + j];
			audioSource2.Stop();
			audioSource2.clip = null;
		}
		for (int k = 0; k < channel_num_autose; k++)
		{
			AudioSource audioSource3 = m_ChannelObject[m_ChannelAutoSeBottom + k];
			audioSource3.Stop();
			audioSource3.clip = null;
		}
	}

	public bool UnloadMusic(string _keyname, int _ch)
	{
		removeSound(_keyname);
		AudioSource audioSource = m_ChannelObject[m_ChannelMusicBottom + _ch];
		audioSource.Stop();
		audioSource.clip = null;
		return true;
	}

	public bool UnloadNowMusic(int _ch)
	{
		string nowMusicKey = GetNowMusicKey(_ch);
		if (!string.IsNullOrEmpty(nowMusicKey))
		{
			removeSound(nowMusicKey);
			AudioSource audioSource = m_ChannelObject[m_ChannelMusicBottom + _ch];
			audioSource.Stop();
			audioSource.clip = null;
			return true;
		}
		return false;
	}

	public bool playMusic(int _ch, string _keyname, bool _loop, bool _intro)
	{
		if (channel_num_music <= _ch)
		{
			return false;
		}
		AudioClip audioClip = null;
		audioClip = addSound(string.Empty, _keyname);
		if (audioClip == null)
		{
			return false;
		}
		nowMusicKeys[_ch] = _keyname;
		mAudiosrc = m_ChannelObject[m_ChannelMusicBottom + _ch];
		mAudiosrc.clip = audioClip;
		mAudiosrc.loop = _loop;
		mAudiosrc.Stop();
		mAudiosrc.time = 0f;
		mAudiosrc.Play();
		if (_intro)
		{
			mLoopBGM = true;
			mSize = mAudiosrc.clip.samples;
			mIntroBGM.mLoopSt = ResourceManager.LoadSound(_keyname, false).mLoopStart;
			mIntroBGM.mLoopEd = ResourceManager.LoadSound(_keyname, false).mLoopEnd;
		}
		else
		{
			mLoopBGM = false;
			mAudiosrc.time = 0f;
		}
		m_UnloadAtPlayEnd[m_ChannelMusicBottom + _ch] = false;
		m_ChannelObjectKey[m_ChannelMusicBottom + _ch] = _keyname;
		return true;
	}

	public IEnumerator playMusicFromStream(int _ch, string _keyname, bool _loop)
	{
		if (channel_num_music > _ch)
		{
			AudioClip ac2 = null;
			if (!hasSound(_keyname))
			{
				yield return StartCoroutine(addSoundFromStream(_keyname));
			}
			ac2 = ResourceManager.LoadSound(_keyname, false).mAudioClip;
			if (!(ac2 == null))
			{
				nowMusicKeys[_ch] = _keyname;
				AudioSource audiosrc = m_ChannelObject[m_ChannelMusicBottom + _ch];
				audiosrc.clip = ac2;
				audiosrc.loop = _loop;
				audiosrc.Stop();
				audiosrc.Play();
				m_UnloadAtPlayEnd[m_ChannelMusicBottom + _ch] = false;
				m_ChannelObjectKey[m_ChannelMusicBottom + _ch] = _keyname;
			}
		}
	}

	public bool stopMusic(int _ch, float fadeTime)
	{
		if (channel_num_music <= _ch)
		{
			return false;
		}
		AudioSource audioSource = m_ChannelObject[m_ChannelMusicBottom + _ch];
		if (fadeTime > 0f)
		{
			FADEINFO fADEINFO = default(FADEINFO);
			fADEINFO.source = audioSource;
			fADEINFO.volume = audioSource.volume;
			fADEINFO.time = fadeTime;
			fADEINFO.ch = _ch;
			StartCoroutine("StopMusicTask", fADEINFO);
		}
		else
		{
			nowMusicKeys[_ch] = string.Empty;
			audioSource.Stop();
		}
		return true;
	}

	private IEnumerator StopMusicTask(FADEINFO info)
	{
		float step = ((info.time != 0f) ? (info.volume / info.time) : 1f);
		float vol = info.volume;
		while (vol > 0f)
		{
			vol -= Time.deltaTime * step;
			info.source.volume = vol;
			yield return 0;
		}
		nowMusicKeys[info.ch] = string.Empty;
		info.source.Stop();
	}

	public string GetNowMusicKey(int _ch)
	{
		return nowMusicKeys[_ch];
	}

	public bool stopAllMusicNow()
	{
		return stopAllMusic(0f);
	}

	public bool stopAllMusic(float fadeTime)
	{
		for (int i = 0; i < channel_num_music; i++)
		{
			stopMusic(i, fadeTime);
		}
		return true;
	}

	public bool setMusicVolume(int _ch, float _volume)
	{
		if (channel_num_music <= _ch)
		{
			return false;
		}
		AudioSource audioSource = m_ChannelObject[m_ChannelMusicBottom + _ch];
		audioSource.volume = _volume;
		return true;
	}

	public float getMusicVolume(int _ch)
	{
		if (channel_num_music <= _ch)
		{
			return 0f;
		}
		AudioSource audioSource = m_ChannelObject[m_ChannelMusicBottom + _ch];
		return audioSource.volume;
	}

	public bool setMusicPosition(int _ch, Vector3 _pos)
	{
		if (channel_num_music <= _ch)
		{
			return false;
		}
		m_ChannelObject[m_ChannelMusicBottom + _ch].gameObject.transform.localPosition = _pos;
		return true;
	}

	public bool playSe(int _ch, string _keyname, float _volume, bool unloadAtPlayEnd, float delay_secondtime)
	{
		if (channel_num_se <= _ch)
		{
			return false;
		}
		AudioSource audioSource = m_ChannelObject[m_ChannelSeBottom + _ch];
		if (audioSource.isPlaying)
		{
			audioSource.Stop();
			if (m_UnloadAtPlayEnd[m_ChannelSeBottom + _ch])
			{
				removeSound(m_ChannelObjectKey[m_ChannelSeBottom + _ch]);
				m_ChannelObjectKey[m_ChannelSeBottom + _ch] = null;
				audioSource.clip = null;
			}
		}
		AudioClip audioClip = null;
		audioClip = addSound(string.Empty, _keyname);
		if (audioClip == null)
		{
			return false;
		}
		audioSource.clip = audioClip;
		audioSource.volume = _volume;
		audioSource.Stop();
		if (delay_secondtime == 0f)
		{
			audioSource.Play();
		}
		else
		{
			audioSource.PlayDelayed(delay_secondtime);
		}
		m_UnloadAtPlayEnd[m_ChannelSeBottom + _ch] = unloadAtPlayEnd;
		m_ChannelObjectKey[m_ChannelSeBottom + _ch] = _keyname;
		return true;
	}

	public bool stopSe(int _ch)
	{
		if (channel_num_se <= _ch)
		{
			return false;
		}
		AudioSource audioSource = m_ChannelObject[m_ChannelSeBottom + _ch];
		audioSource.Stop();
		return true;
	}

	public bool stopSeAll()
	{
		for (int i = 0; i < channel_num_se; i++)
		{
			stopSe(i);
		}
		return true;
	}

	public bool setSeVolume(int _ch, float _volume)
	{
		if (channel_num_se <= _ch)
		{
			return false;
		}
		AudioSource audioSource = m_ChannelObject[m_ChannelSeBottom + _ch];
		audioSource.volume = _volume;
		return true;
	}

	public bool setAutoSeVolume(int _ch, float _volume)
	{
		if (channel_num_autose <= _ch)
		{
			return false;
		}
		AudioSource audioSource = m_ChannelObject[m_ChannelAutoSeBottom + _ch];
		audioSource.volume = _volume;
		return true;
	}

	public bool setSePosition(int _ch, Vector3 _pos)
	{
		if (channel_num_se <= _ch)
		{
			return false;
		}
		m_ChannelObject[m_ChannelSeBottom + _ch].gameObject.transform.localPosition = _pos;
		return true;
	}

	public bool stopAutoSeAll()
	{
		for (int i = 0; i < channel_num_autose; i++)
		{
			AudioSource audioSource = m_ChannelObject[m_ChannelAutoSeBottom + i];
			audioSource.Stop();
		}
		return true;
	}

	public bool playAutoSe(string _keyname, float _volume, bool unloadAtPlayEnd, float delay_secondtime)
	{
		AudioClip audioClip = null;
		audioClip = addSound(string.Empty, _keyname);
		if (audioClip == null)
		{
			return false;
		}
		AudioSource audioSource = null;
		int num = -1;
		for (int i = 0; i < channel_num_autose; i++)
		{
			audioSource = m_ChannelObject[m_ChannelAutoSeBottom + i];
			if (!audioSource.isPlaying)
			{
				num = i;
				break;
			}
		}
		if (num == -1)
		{
			return false;
		}
		audioSource.clip = audioClip;
		audioSource.volume = _volume;
		audioSource.Stop();
		if (delay_secondtime == 0f)
		{
			audioSource.Play();
		}
		else
		{
			audioSource.PlayDelayed(delay_secondtime);
		}
		m_UnloadAtPlayEnd[m_ChannelAutoSeBottom + num] = unloadAtPlayEnd;
		m_ChannelObjectKey[m_ChannelAutoSeBottom + num] = _keyname;
		return true;
	}

	public bool setAutoSePosition(Vector3 _pos)
	{
		for (int i = 0; i < channel_num_autose; i++)
		{
			m_ChannelObject[m_ChannelAutoSeBottom + i].gameObject.transform.localPosition = _pos;
		}
		return true;
	}

	public bool isSePlaying(int _ch)
	{
		if (_ch != -1)
		{
			if (_ch >= channel_num_se)
			{
				return false;
			}
			AudioSource audioSource = m_ChannelObject[m_ChannelSeBottom + _ch];
			if (!audioSource.isPlaying)
			{
				return false;
			}
		}
		else
		{
			for (int i = 0; i < channel_num_autose; i++)
			{
				AudioSource audioSource2 = m_ChannelObject[m_ChannelAutoSeBottom + i];
				if (!audioSource2.isPlaying)
				{
					return false;
				}
			}
		}
		return true;
	}

	public void unloadSoundAll()
	{
		ResourceManager.UnloadSoundAll();
		Resources.UnloadUnusedAssets();
	}

	public static bool isDynamicSound()
	{
		return true;
	}
}
