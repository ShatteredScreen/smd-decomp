using System;

public class StatusManager<Type>
{
	public delegate void StatusResetCallback(Type a_current, Type a_next);

	private Type nowStatus;

	private Type nextStatus;

	private Type prevStatus;

	private int stay_count;

	private bool bChange;

	private bool resetChanged;

	private string mLogTitle = string.Empty;

	private bool mIsLogInChange;

	private StatusResetCallback mResetCallback;

	public StatusManager(Type aStatus)
	{
		nowStatus = (nextStatus = (prevStatus = aStatus));
		stay_count = 0;
		bChange = false;
	}

	public void SetLogInChange(bool a_flg, string a_title)
	{
		mLogTitle = a_title;
		mIsLogInChange = a_flg;
	}

	public void SetResetCallback(StatusResetCallback a_callback)
	{
		mResetCallback = a_callback;
	}

	~StatusManager()
	{
	}

	public void Update()
	{
		if (Convert.ToInt32(nowStatus) != Convert.ToInt32(nextStatus) || resetChanged)
		{
			resetChanged = false;
			bChange = true;
			prevStatus = nowStatus;
			nowStatus = nextStatus;
			stay_count = 0;
			if (!mIsLogInChange)
			{
			}
		}
		else
		{
			bChange = false;
			stay_count++;
		}
	}

	public void Change(Type aStatus)
	{
		nextStatus = aStatus;
	}

	public void Reset(Type aStatus, bool aReset = false)
	{
		Type a_current = nowStatus;
		prevStatus = nowStatus;
		nextStatus = (nowStatus = aStatus);
		stay_count = 0;
		bChange = aReset;
		resetChanged = aReset;
		if (mResetCallback != null)
		{
			mResetCallback(a_current, aStatus);
		}
	}

	public bool IsChanged()
	{
		return bChange;
	}

	public Type GetStatus()
	{
		return nowStatus;
	}

	public Type GetStatusAndMarkUsed()
	{
		resetChanged = false;
		return nowStatus;
	}

	public Type GetPreStatus()
	{
		return prevStatus;
	}

	public Type GetNextStatus()
	{
		return nextStatus;
	}

	public int GetCount()
	{
		return stay_count;
	}
}
