using UnityEngine;

public class EventCharaDescDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private DIALOG_SIZE mDialogSize;

	private string mTitle;

	private string mDescription;

	private string mDescription2;

	private int mCompanionID;

	private int mCompanionLevel;

	private float mCloseTimer = 1f;

	private string mImagePOSIData = string.Empty;

	private string mImageNEGAData = string.Empty;

	private int ReservedSortingOrder;

	protected bool mButtonSeEnable = true;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy())
		{
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(mTitle);
		float y = 63f;
		float y2 = -16f;
		float y3 = -110f;
		if (mCompanionID != 0)
		{
			float num = 210f;
			num = 105f;
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("DemoEV_TriCharaLv5_Desc_Top"), mBaseDepth + 4, new Vector3(0f, num, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.spacingY = 6;
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			CompanionData companionData = mGame.mCompanionData[mCompanionID];
			GameObject gameObject = null;
			float num2 = 100f;
			num2 = 0f;
			UISprite uISprite = Util.CreateSprite("MessageFrame", base.gameObject, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel15", mBaseDepth + 2, new Vector3(0f, num2, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(534, 112);
			gameObject = uISprite.gameObject;
			uISprite = Util.CreateSprite("MessageFrameTitle", gameObject, image);
			Util.SetSpriteInfo(uISprite, "instruction_accessory10", mBaseDepth + 3, new Vector3(0f, 52f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(518, 42);
			string empty = string.Empty;
			string empty2 = string.Empty;
			if (mCompanionLevel >= CompanionData.SKILL01_LEVEL)
			{
				empty = companionData.GetSkillName(2);
				empty2 = "[403512]" + companionData.GetSkillDesc(2) + companionData.GetSkillSubDesc(2) + "[-]";
			}
			else if (mCompanionLevel >= CompanionData.SKILL00_LEVEL)
			{
				empty = companionData.GetSkillName(1);
				empty2 = ((companionData.maxLevel < CompanionData.SKILL01_LEVEL) ? ("[403512]" + companionData.GetSkillDesc(1) + "[-]") : ("[403512]" + companionData.GetSkillDesc(1) + companionData.GetSkillSubDesc(1) + "[-]"));
			}
			else
			{
				empty = companionData.GetSkillName(1);
				empty2 = "[403512]" + companionData.GetSkillDesc(1) + "[-]\n[FF0000]" + Localization.Get("SkillInfoEmpty") + "[-]";
			}
			empty = empty.Replace("\n", string.Empty);
			uILabel = Util.CreateLabel("SkillName", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, empty, mBaseDepth + 4, new Vector3(0f, 53f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.white;
			uILabel.effectStyle = UILabel.Effect.Shadow;
			uILabel.effectColor = Color.black;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(500, 24);
			uILabel = Util.CreateLabel("SkillDesc", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, empty2, mBaseDepth + 4, new Vector3(0f, -4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(510, 65);
			y = -30f;
			y2 = -130f;
			y3 = -120f;
		}
		if (mCompanionLevel != 5)
		{
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, mDescription, mBaseDepth + 4, new Vector3(0f, y, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.spacingY = 6;
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel = Util.CreateLabel("Desc2", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, mDescription2, mBaseDepth + 4, new Vector3(0f, y2, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.spacingY = 6;
			uILabel.color = Color.red;
		}
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		string text = "LC_button_close";
		Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(0f, y3, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(string title, string desc, string desc2, int a_companionID = 0, int a_level = 0)
	{
		mTitle = title;
		mDescription = desc;
		mDescription2 = desc2;
		mCompanionID = a_companionID;
		mCompanionLevel = a_level;
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnPositivePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (mButtonSeEnable)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
			}
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetCloseTimer(float timer)
	{
		mCloseTimer = timer;
	}

	public void SetButtonSeEnable(bool enable)
	{
		mButtonSeEnable = enable;
	}

	public void SetSortOrder(int a_order)
	{
		ReservedSortingOrder = a_order;
	}
}
