using System;
using System.Collections.Generic;

public class AdvQuestEndProfile_Response : ICloneable
{
	[MiniJSONAlias("figure_exchange")]
	public List<AdvFigureExchange> FigureExchangeList { get; set; }

	[MiniJSONAlias("figures")]
	public List<AdvFigureData> FigureDataList { get; set; }

	[MiniJSONAlias("items")]
	public List<AdvItemData> ItemDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvQuestEndProfile_Response Clone()
	{
		return MemberwiseClone() as AdvQuestEndProfile_Response;
	}
}
