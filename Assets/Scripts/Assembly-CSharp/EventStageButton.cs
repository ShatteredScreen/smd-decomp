using UnityEngine;

public class EventStageButton : MapStageButton
{
	protected short mCourseID;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public override void Init(Vector3 pos, float scale, int stageNo, byte starNum, Player.STAGE_STATUS mode, BIJImage atlas, string a_animeKey, bool a_hasItem = false, int displayNo = -1)
	{
		mAtlas = atlas;
		mBaseScale = scale;
		mStarNum = starNum;
		mStageNo = stageNo;
		mHasItem = a_hasItem;
		DefaultAnimeKey = a_animeKey;
		DisplayNumber = displayNo;
		mChangeImageDict["btn_stage"] = "null";
		mChangeImageDict["one_01"] = "null";
		mChangeImageDict["ten_01"] = "null";
		mChangeImageDict["ten_02"] = "null";
		mChangeImageDict["hundred_01"] = "null";
		mChangeImageDict["hundred_02"] = "null";
		mChangeImageDict["hundred_03"] = "null";
		mChangeImageDict["star_01"] = "null";
		mChangeImageDict["star_02"] = "null";
		mChangeImageDict["star_03"] = "null";
		mChangeImageDict["label"] = "null";
		mChangeImageDict["sb_one_01"] = "null";
		mChangeImageDict["base_race"] = "null";
		if (mHasItem)
		{
			mChangeImageDict["base_race"] = "stage_remove";
		}
		else
		{
			mChangeImageDict["base_race"] = "null";
		}
		bool force = false;
		if (mMode != mode)
		{
			force = true;
		}
		mMode = mode;
		string key = DefaultAnimeKey;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[mCourseID];
		int num = playerMapData.OpenNoticeLevel;
		SMMapPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		SMEventPageData sMEventPageData = currentEventPageData as SMEventPageData;
		if (sMEventPageData.EventSetting.EventType == Def.EVENT_TYPE.SM_RALLY2 && num > playerMapData.NextRoadBlockLevel)
		{
			num -= 100;
		}
		if (base.UseCurrentAnime && mStageNo == num)
		{
			force = true;
			key = DefaultAnimeKey + "_CURRENT";
			switch (mode)
			{
			case Player.STAGE_STATUS.LOCK:
				mChangeImageDict["btn_stage"] = "btn_stage_untrodden";
				break;
			case Player.STAGE_STATUS.UNLOCK:
				mBaseScale = base.DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_traverse";
				break;
			case Player.STAGE_STATUS.CLEAR:
				mBaseScale = base.DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_cleared";
				key = DefaultAnimeKey + "_CLEAR";
				break;
			default:
				mChangeImageDict["base_race"] = "null";
				break;
			}
		}
		else
		{
			switch (mode)
			{
			case Player.STAGE_STATUS.LOCK:
				mChangeImageDict["btn_stage"] = "btn_stage_untrodden";
				break;
			case Player.STAGE_STATUS.UNLOCK:
				mBaseScale = base.DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_traverse";
				break;
			case Player.STAGE_STATUS.CLEAR:
				mBaseScale = base.DEFAULT_SCALE;
				mChangeImageDict["btn_stage"] = "btn_stage_cleared";
				key = DefaultAnimeKey + "_CLEAR";
				break;
			default:
				mChangeImageDict["base_race"] = "null";
				break;
			}
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(mStageNo, out a_main, out a_sub);
		if (mode != 0)
		{
			if (DisplayNumber != -1)
			{
				a_main = DisplayNumber;
			}
			if (a_main < 10)
			{
				mChangeImageDict["one_01"] = MapStageButton.numberImageName[a_main];
			}
			else if (a_main < 100)
			{
				mChangeImageDict["ten_01"] = MapStageButton.numberImageName[a_main % 10];
				mChangeImageDict["ten_02"] = MapStageButton.numberImageName[a_main / 10];
			}
			else if (a_main < 1000)
			{
				int num2 = a_main;
				mChangeImageDict["hundred_01"] = MapStageButton.numberImageName[num2 % 10];
				num2 /= 10;
				mChangeImageDict["hundred_02"] = MapStageButton.numberImageName[num2 % 10];
				mChangeImageDict["hundred_03"] = MapStageButton.numberImageName[num2 / 10];
			}
			if (a_sub == 0 || DisplayNumber != -1)
			{
				mChangeImageDict["label"] = "null";
				mChangeImageDict["sb_one_01"] = "null";
			}
			else
			{
				mChangeImageDict["label"] = "sb_stage_label";
				mChangeImageDict["sb_one_01"] = MapStageButton.numberSubImageName[a_sub % 10];
			}
			mChangeImageDict["star_01"] = "btn_stage_star00";
			mChangeImageDict["star_02"] = "btn_stage_star00";
			mChangeImageDict["star_03"] = "btn_stage_star00";
			if (starNum >= 1)
			{
				mChangeImageDict["star_01"] = "btn_stage_star01";
			}
			if (starNum >= 2)
			{
				mChangeImageDict["star_02"] = "btn_stage_star01";
			}
			if (starNum >= 3)
			{
				mChangeImageDict["star_03"] = "btn_stage_star01";
			}
		}
		ChangeAnime(key, mChangeImageDict, force, 0);
		_sprite.transform.localScale = new Vector3(mBaseScale, mBaseScale, 1f);
		_sprite.transform.localPosition = pos;
		if (mode == Player.STAGE_STATUS.CLEAR || mode == Player.STAGE_STATUS.UNLOCK)
		{
			_sprite.Play();
		}
		else
		{
			_sprite.Pause();
		}
		mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
		if (!mFirstInitialized)
		{
			EventDelegate item = new EventDelegate(this, "OnButtonPushed");
			mEventTrigger.onPress.Add(item);
			EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
			mEventTrigger.onRelease.Add(item2);
			EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
			mEventTrigger.onClick.Add(item3);
		}
		mFirstInitialized = true;
	}

	public override void SetDebugInfo(SMMapStageSetting a_setting)
	{
		if (a_setting != null)
		{
			SMEventStageSetting sMEventStageSetting = a_setting as SMEventStageSetting;
			Debug_Accessories = new int[sMEventStageSetting.BoxID.Count];
			Debug_HasAccessories = new bool[sMEventStageSetting.BoxID.Count];
			for (int i = 0; i < Debug_Accessories.Length; i++)
			{
				int num = sMEventStageSetting.BoxID[i];
				Debug_Accessories[i] = num;
				Debug_HasAccessories[i] = mGame.mPlayer.IsAccessoryUnlock(num);
			}
			int num2 = Def.GetStageNo(sMEventStageSetting.mCourseNo, sMEventStageSetting.mStageNo, sMEventStageSetting.mSubNo) / 100;
			Debug_StageNo = num2 + "_" + sMEventStageSetting.mSubNo;
		}
	}
}
