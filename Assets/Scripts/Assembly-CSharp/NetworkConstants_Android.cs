using System;
using System.Reflection;
using System.Text;

public sealed class NetworkConstants_Android : INetworkConstants
{
	private static object mInstanceLock = new object();

	private static NetworkConstants_Android mInstance = null;

	public string COUNTRY_CODE { get; private set; }

	public string NETWORK_SALT { get; private set; }

	public string APPNAME { get; private set; }

	public string AppBase_URL { get; private set; }

	public string AppEventBase_URL { get; private set; }

	public string ClientData_URL { get; private set; }

	public string Social_URL { get; private set; }

	public string NICKNAME_URL { get; private set; }

	public string VIP_URL { get; private set; }

	public string IAP_URL { get; private set; }

	public string CRAM_URL { get; private set; }

	public string CRAM_KPI_URL { get; private set; }

	public string MoreGames_URL { get; private set; }

	public string InfoBarApp_URL { get; private set; }

	public string InfoBarPage_URL { get; private set; }

	public string Campaign_URL { get; private set; }

	public string HELP_URL { get; private set; }

	public string STORE_URL { get; private set; }

	public string Reward_URL { get; private set; }

	public string TransferBase_URL { get; private set; }

	public string Facebook_APPID { get; private set; }

	public string Facebook_SECRET { get; private set; }

	public string GooglePlus_APPID { get; private set; }

	public string GooglePlus_CLIENTID { get; private set; }

	public string Twitter_KEY { get; private set; }

	public string Twitter_SECRET { get; private set; }

	public string Instagram_KEY { get; private set; }

	public string Instagram_SECRET { get; private set; }

	public int Partytrack_APPID { get; private set; }

	public string Partytrack_KEY { get; private set; }

	public string GrowthPush_APPID { get; private set; }

	public string GrowthPush_SECRET { get; private set; }

	public string GrowthPush_GCM_SENDER_ID { get; private set; }

	public string EULA_URL { get; private set; }

	public string DLFILELIST_URL { get; private set; }

	public string HEADER_PREFIX { get; private set; }

	public string GameInfo_URL { get; private set; }

	public string[] SSL_ACCEPT_CERTS { get; private set; }

	public string Connection_URL { get; private set; }

	public string NETWORK_ADV_SALT { get; private set; }

	public string AdvAppBase_URL { get; private set; }

	public static NetworkConstants_Android Instance
	{
		get
		{
			return mInstance;
		}
	}

	private NetworkConstants_Android()
	{
	}

	public static INetworkConstants Load()
	{
		using (BIJResourceBinaryReader reader = new BIJResourceBinaryReader("Data/Network/android.bin"))
		{
			return Load(reader);
		}
	}

	public static INetworkConstants Load(BIJBinaryReader reader)
	{
		if (mInstance == null)
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					mInstance = new NetworkConstants_Android();
					mInstance.COUNTRY_CODE = reader.ReadUTF();
					mInstance.NETWORK_SALT = reader.ReadUTF();
					mInstance.APPNAME = reader.ReadUTF();
					mInstance.AppBase_URL = reader.ReadUTF();
					mInstance.AppEventBase_URL = reader.ReadUTF();
					mInstance.ClientData_URL = reader.ReadUTF();
					mInstance.Social_URL = reader.ReadUTF();
					mInstance.NICKNAME_URL = reader.ReadUTF();
					mInstance.VIP_URL = reader.ReadUTF();
					mInstance.IAP_URL = reader.ReadUTF();
					mInstance.CRAM_URL = reader.ReadUTF();
					mInstance.CRAM_KPI_URL = reader.ReadUTF();
					mInstance.MoreGames_URL = reader.ReadUTF();
					mInstance.InfoBarApp_URL = reader.ReadUTF();
					mInstance.InfoBarPage_URL = reader.ReadUTF();
					mInstance.Campaign_URL = reader.ReadUTF();
					mInstance.HELP_URL = reader.ReadUTF();
					mInstance.STORE_URL = reader.ReadUTF();
					mInstance.Reward_URL = reader.ReadUTF();
					mInstance.TransferBase_URL = reader.ReadUTF();
					mInstance.Facebook_APPID = reader.ReadUTF();
					mInstance.Facebook_SECRET = reader.ReadUTF();
					mInstance.GooglePlus_APPID = reader.ReadUTF();
					mInstance.GooglePlus_CLIENTID = reader.ReadUTF();
					mInstance.Twitter_KEY = reader.ReadUTF();
					mInstance.Twitter_SECRET = reader.ReadUTF();
					mInstance.Instagram_KEY = reader.ReadUTF();
					mInstance.Instagram_SECRET = reader.ReadUTF();
					mInstance.Partytrack_APPID = reader.ReadInt();
					mInstance.Partytrack_KEY = reader.ReadUTF();
					mInstance.GrowthPush_APPID = reader.ReadUTF();
					mInstance.GrowthPush_SECRET = reader.ReadUTF();
					mInstance.GrowthPush_GCM_SENDER_ID = reader.ReadUTF();
					mInstance.EULA_URL = reader.ReadUTF();
					mInstance.DLFILELIST_URL = reader.ReadUTF();
					mInstance.HEADER_PREFIX = reader.ReadUTF();
					mInstance.GameInfo_URL = reader.ReadUTF();
					mInstance.SSL_ACCEPT_CERTS = reader.ReadUTFArray();
					mInstance.Connection_URL = reader.ReadUTF();
					mInstance.NETWORK_ADV_SALT = reader.ReadUTF();
					mInstance.AdvAppBase_URL = reader.ReadUTF();
				}
			}
		}
		return mInstance;
	}

	public static void Unload()
	{
		if (mInstance == null)
		{
			return;
		}
		lock (mInstanceLock)
		{
			if (mInstance != null)
			{
				mInstance.SSL_ACCEPT_CERTS = null;
				mInstance = null;
			}
		}
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
		PropertyInfo[] array = properties;
		foreach (PropertyInfo propertyInfo in array)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[{0}: {1}]", type.Name, stringBuilder.ToString());
	}
}
