using System;

public class StageConditionAttribute : Attribute
{
	public Def.STAGE_WIN_CONDITION Win;

	public Def.STAGE_LOSE_CONDITION Lose;

	public StageConditionAttribute(Def.STAGE_WIN_CONDITION win, Def.STAGE_LOSE_CONDITION lose)
	{
		Win = win;
		Lose = lose;
	}
}
