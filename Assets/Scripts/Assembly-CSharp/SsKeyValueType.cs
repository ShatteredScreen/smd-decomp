public enum SsKeyValueType
{
	Data = 0,
	Param = 1,
	Point = 2,
	Palette = 3,
	Color = 4,
	Vertex = 5,
	User = 6,
	Sound = 7,
	Num = 8
}
