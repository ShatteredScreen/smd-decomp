using System;

public class ConnectCurrencySpend : ConnectBase
{
	private MCSpendResponse mResponse;

	private bool mIsSetResponseData;

	public void SetResponseData(MCSpendResponse a_response)
	{
		mResponse = a_response;
		mIsSetResponseData = true;
	}

	protected override void InitServerFlg()
	{
		GameMain.mBuyItemCheckDone = false;
		GameMain.mBuyItemSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mBuyItemCheckDone = true;
		GameMain.mBuyItemSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mBuyItemCheckDone = true;
		GameMain.mBuyItemSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (mParam == null)
		{
			BIJLog.E("Must Parameter:" + GetType().Name);
			SetServerResponse(1, -1);
			return;
		}
		ConnectCurrencySpendParam connectCurrencySpendParam = mParam as ConnectCurrencySpendParam;
		if (connectCurrencySpendParam == null)
		{
			BIJLog.E("Type Error Parameter:" + GetType().Name + "<=" + mParam.GetType().Name);
			SetServerResponse(1, -1);
			return;
		}
		InitServerFlg();
		if (!ServerIAP.CurrencySpend(connectCurrencySpendParam.BuyItemID, base.RetryCount >= 1))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		if (mResponse != null)
		{
			int mc = mResponse.mc;
			int sc = mResponse.sc;
			long server_time = mResponse.server_time;
			mPlayer.SetDollar(mc, true);
			mPlayer.SetDollar(sc, false);
			mPlayer.ServerTime = server_time;
			mGame.GetGemCountGetTime = DateTime.Now;
		}
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
