using UnityEngine;

public class TransferPasswordDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public void Init(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public override void BuildDialog()
	{
		base.BuildDialog();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(Localization.Get("Transfer_ShowPassword_Title"));
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		UILabel uILabel = null;
		uISprite = Util.CreateSprite("BackGround", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "bg00", mBaseDepth + 1, new Vector3(0f, 20f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions(520, 430);
		uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Transfer_ShowPassword_Desc01"), mBaseDepth + 2, new Vector3(0f, 130f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.yellow;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.spacingY = 4;
		uILabel.SetDimensions(490, 130);
		uILabel = Util.CreateLabel("UserID_Title", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Transfer_ShowPassword_Id"), mBaseDepth + 3, new Vector3(0f, 20f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.66015625f, 67f / 128f, 0.98046875f);
		uISprite2 = Util.CreateSprite("UserID_Frame", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel2", mBaseDepth + 4, new Vector3(0f, -20f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(420, 50);
		uILabel = Util.CreateLabel("UserID", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, mGame.mUserUniqueID.TransferId, mBaseDepth + 5, new Vector3(0f, -3f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("UserID_Desc", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Transfer_ShowPassword_Desc02"), mBaseDepth + 6, new Vector3(0f, -60f, 0f), 18, 0, 0, UIWidget.Pivot.Center);
		uILabel = Util.CreateLabel("Password_Title", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Transfer_ShowPassword_Pass"), mBaseDepth + 7, new Vector3(0f, -130f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.66015625f, 67f / 128f, 0.98046875f);
		uISprite2 = Util.CreateSprite("Password_Frame", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel2", mBaseDepth + 8, new Vector3(0f, -170f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(420, 50);
		string text = mGame.mUserUniqueID.TransferPass.Substring(0, 4);
		string text2 = mGame.mUserUniqueID.TransferPass.Substring(4, 4);
		string text3 = mGame.mUserUniqueID.TransferPass.Substring(8, 4);
		string text4 = mGame.mUserUniqueID.TransferPass.Substring(12, 4);
		string text5 = string.Format(Localization.Get("Transfer_ShowPassword_PassFormat"), text, text2, text3, text4);
		uILabel = Util.CreateLabel("Password", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text5, mBaseDepth + 9, new Vector3(0f, -3f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 10, new Vector3(0f, -240f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}
}
