using System;
using System.IO;
using System.Text;
using UnityEngine;

public class BIJResourceBinaryReader : BIJBinaryReader
{
	private string mName;

	private MemoryStream mStream;

	public BIJResourceBinaryReader(string name)
		: this(name, Encoding.UTF8)
	{
	}

	public BIJResourceBinaryReader(string name, Encoding encoding)
	{
		mName = name;
		mStream = null;
		Reader = null;
		TextAsset textAsset = null;
		try
		{
			textAsset = Resources.Load(mName) as TextAsset;
			if (textAsset == null)
			{
				throw new IOException("Could not found resource: " + mName);
			}
			mStream = new MemoryStream(textAsset.bytes);
			Reader = new BinaryReader(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
		finally
		{
			if (textAsset != null)
			{
				Resources.UnloadAsset(textAsset);
			}
			textAsset = null;
		}
	}

	public override void Close()
	{
		base.Close();
		try
		{
			if (mStream != null)
			{
				mStream.Close();
			}
		}
		catch (Exception)
		{
		}
		mStream = null;
	}
}
