using System;
using System.IO;

public class ResourceDLUtils
{
	public enum DEL_TYPE
	{
		DL_BGM = 0,
		DL_ANIMATION = 1,
		DL_IMAGE = 2,
		DL_COMPANION_DATA = 3,
		RecordDLFilelist = 10,
		RecordFilelist = 11,
		AllDLFile = 12
	}

	public static string DLDebugButtonIndex = "button_return";

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private static bool sDebugDLFileListToNull = false;

	private static bool sDebugLocalDLFileListToNull = false;

	private static bool sDebugPostCountToMAX = false;

	private static bool sDebugPostRequestError1 = false;

	private static bool sDebugPostEventFileOpenError = false;

	private static bool sDebugPostEventFileWriteError = false;

	private static bool sDebugThreadCancelError = false;

	private static int sDebugThreadAsyncResult = -1;

	private static bool sDebugThreadNoResponseError = false;

	private static bool sDebugThreadHTTPError = false;

	private static bool sDebugResponseHandlerError = false;

	private static bool sDebugLoadAssetBundleWWWError = false;

	private static bool sDebugLoadResourceNoBundleError = false;

	private static bool sDebugLoadResourceNoGameObjectError = false;

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	public static void log(string msg)
	{
		try
		{
			long num = time();
		}
		catch (Exception)
		{
		}
	}

	public static string ResultString(int result)
	{
		switch (result)
		{
		case 0:
			return "SUCCESS";
		case 1:
			return "ERROR";
		case 4:
			return "CANCELED";
		default:
			return "UNKNOWN!!";
		}
	}

	public static void dbg1(string a_text, bool a_isDisplay = false)
	{
	}

	public static void dbg2(string a_text)
	{
	}

	public static void err(string a_text, Exception a_e = null)
	{
	}

	public static void err(string a_text, bool a_isDisplay)
	{
	}

	public static bool IsDelRecordDLFile()
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			return true;
		}
		return false;
	}

	public static void DeleteDLFile(string a_path)
	{
		dbg1(a_path);
		try
		{
			if (File.Exists(a_path))
			{
				File.Delete(a_path);
			}
		}
		catch (Exception)
		{
		}
	}

	public static void DeleteDLFile(DEL_TYPE a_type)
	{
		foreach (int value in Enum.GetValues(typeof(ResourceDownloadManager.KIND)))
		{
			DeleteDLFile(a_type, (ResourceDownloadManager.KIND)value);
		}
	}

	public static void DeleteDLFile(DEL_TYPE a_type, ResourceDownloadManager.KIND aKind)
	{
		switch (a_type)
		{
		case DEL_TYPE.DL_COMPANION_DATA:
			break;
		case DEL_TYPE.DL_ANIMATION:
			break;
		case DEL_TYPE.DL_BGM:
			break;
		case DEL_TYPE.DL_IMAGE:
			break;
		case DEL_TYPE.RecordDLFilelist:
		{
			string resourceDLPath2 = ResourceDownloadManager.getResourceDLPath(aKind);
			resourceDLPath2 += Constants.RESOURCEDL_SERVERFILE;
			DeleteDLFile(resourceDLPath2);
			break;
		}
		case DEL_TYPE.RecordFilelist:
		{
			string resourceDLPath3 = ResourceDownloadManager.getResourceDLPath(aKind);
			resourceDLPath3 += Constants.RESOURCEDL_LOCALFILE;
			DeleteDLFile(resourceDLPath3);
			break;
		}
		case DEL_TYPE.AllDLFile:
		{
			string resourceDLPath = ResourceDownloadManager.getResourceDLPath(aKind);
			string[] files = Directory.GetFiles(resourceDLPath);
			foreach (string a_path in files)
			{
				DeleteDLFile(a_path);
			}
			break;
		}
		case (DEL_TYPE)4:
		case (DEL_TYPE)5:
		case (DEL_TYPE)6:
		case (DEL_TYPE)7:
		case (DEL_TYPE)8:
		case (DEL_TYPE)9:
			break;
		}
	}

	public static void SetDebugDLFileListToNull(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugDLFileListToNull = a_flg;
		}
	}

	public static bool IsDebugDLFileListToNull()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugDLFileListToNull)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugLocalDLFileListToNull(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugLocalDLFileListToNull = a_flg;
		}
	}

	public static bool IsDebugLocalDLFileListToNull()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugLocalDLFileListToNull)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugPostCountToMAX(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugPostCountToMAX = a_flg;
		}
	}

	public static bool IsDebugPostCountToMAX()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugPostCountToMAX)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugPostRequestError1(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugPostRequestError1 = a_flg;
		}
	}

	public static bool IsDebugPostRequestError1()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugPostRequestError1)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugPostEventFileOpenError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugPostEventFileOpenError = a_flg;
		}
	}

	public static bool IsDebugPostEventFileOpenError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugPostEventFileOpenError)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugPostEventFileWriteError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugPostEventFileWriteError = a_flg;
		}
	}

	public static bool IsDebugPostEventFileWriteError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugPostEventFileWriteError)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugThreadCancelError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugThreadCancelError = a_flg;
		}
	}

	public static bool IsDebugThreadCancelError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugThreadCancelError)
		{
			return true;
		}
		return false;
	}

	public static bool IsDebugThreadAsyncResult()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugThreadAsyncResult != -1)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugThreadAsyncResult(int a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugThreadAsyncResult = a_flg;
		}
	}

	public static int GetDebugThreadAsyncResult(int a_result)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			return sDebugThreadAsyncResult;
		}
		return a_result;
	}

	public static void SetDebugThreadNoResponseError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugThreadNoResponseError = a_flg;
		}
	}

	public static bool IsDebugThreadNoResponseError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugThreadNoResponseError)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugThreadHTTPError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugThreadHTTPError = a_flg;
		}
	}

	public static bool IsDebugThreadHTTPError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugThreadHTTPError)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugResponseHandlerError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugResponseHandlerError = a_flg;
		}
	}

	public static bool IsDebugResponseHandlerError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugResponseHandlerError)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugLoadAssetBundleWWWError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugLoadAssetBundleWWWError = a_flg;
		}
	}

	public static bool IsDebugLoadAssetBundleWWWError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugLoadAssetBundleWWWError)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugLoadResourceNoBundleError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugLoadResourceNoBundleError = a_flg;
		}
	}

	public static bool IsDebugLoadResourceNoBundleError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugLoadResourceNoBundleError)
		{
			return true;
		}
		return false;
	}

	public static void SetDebugLoadResourceNoGameObjectError(bool a_flg)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			sDebugLoadResourceNoGameObjectError = a_flg;
		}
	}

	public static bool IsDebugLoadResourceNoGameObjectError()
	{
		if (GameMain.USE_DEBUG_DIALOG && sDebugLoadResourceNoGameObjectError)
		{
			return true;
		}
		return false;
	}
}
