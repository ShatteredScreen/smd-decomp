public class GiveGift : ParameterObject<GiveGift>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("category")]
	public int Category { get; set; }

	[MiniJSONAlias("sub_category")]
	public int CategorySub { get; set; }

	[MiniJSONAlias("itemid")]
	public int ItemID { get; set; }

	[MiniJSONAlias("quantity")]
	public int Quantity { get; set; }

	[MiniJSONAlias("iconid")]
	public int IconID { get; set; }

	[MiniJSONAlias("iconlvl")]
	public int IconLvl { get; set; }
}
