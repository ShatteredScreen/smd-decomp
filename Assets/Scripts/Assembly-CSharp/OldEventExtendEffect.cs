using System;
using System.Collections;
using UnityEngine;

public class OldEventExtendEffect : MonoBehaviour
{
	public delegate void OnEffectFinished();

	private OnEffectFinished mCallback;

	private GameMain mGame;

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Start()
	{
		StartCoroutine(CoEffect());
	}

	private void Update()
	{
	}

	private IEnumerator CoEffect()
	{
		ResLive2DAnimation resLive2D = ResourceManager.LoadLive2DAnimationAsync("SAILOR_PLUTO");
		ResSsAnimation resSsAnime = ResourceManager.LoadSsAnimationAsync("OLDEVENT_EXTEND");
		ResSound resSound = ResourceManager.LoadSoundAsync("SE_OLD_EVENT_EXTENSION_00");
		while (resLive2D.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (resSsAnime.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		Live2DRender l2dRender = mGame.CreateLive2DRender(568, 568);
		GameObject go = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.DEMO_CAMERA));
		go.transform.parent = mGame.mCamera.transform.parent;
		go.transform.localPosition = Vector3.zero;
		go.transform.localScale = Vector3.one;
		Live2DInstance l2dInst = Util.CreateGameObject("Pluto", l2dRender.gameObject).AddComponent<Live2DInstance>();
		l2dInst.Init("SAILOR_PLUTO", Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER, Color.white, true);
		while (!l2dInst.IsLoadFinished())
		{
			yield return 0;
		}
		l2dInst.Position = Vector3.zero;
		l2dRender.ResisterInstance(l2dInst);
		yield return 0;
		mGame.PlaySe("SE_OLD_EVENT_EXTENSION_00", 2);
		yield return new WaitForSeconds(2f / 3f);
		l2dInst.StartMotion("motions/extendedtime00.mtn", false);
		SimpleTexture charScreen = Util.CreateGameObject("Live2DScreen", go).AddComponent<SimpleTexture>();
		charScreen.Init(l2dRender.RenderTexture, new Vector2(568f, 568f), new Rect(0f, 0f, 1f, 1f), SimpleTexture.PIVOT.CENTER);
		charScreen.transform.localPosition = new Vector3(0f, 1024f, -1f);
		Util.CreateOneShotAnime("Effect", "OLDEVENT_EXTEND", go, new Vector3(0f, 0f, -2f), Vector3.one, 0f, 0f);
		float angle2 = 0f;
		while (angle2 < 90f)
		{
			angle2 += Time.deltaTime * 90f / (7f / 30f);
			if (angle2 >= 90f)
			{
				angle2 = 90f;
			}
			float ratio = Mathf.Cos(angle2 * ((float)Math.PI / 180f));
			charScreen.transform.localPosition = new Vector3(0f, 1024f * ratio, -1f);
			yield return 0;
		}
		yield return new WaitForSeconds(2.0666668f);
		angle2 = 0f;
		while (angle2 < 90f)
		{
			angle2 += Time.deltaTime * 90f / (7f / 30f);
			if (angle2 >= 90f)
			{
				angle2 = 90f;
			}
			float ratio2 = Mathf.Sin(angle2 * ((float)Math.PI / 180f));
			charScreen.transform.localPosition = new Vector3(0f, 1024f * ratio2, -1f);
			yield return 0;
		}
		yield return new WaitForSeconds(0.5f);
		UnityEngine.Object.Destroy(l2dRender.gameObject);
		UnityEngine.Object.Destroy(go);
		yield return new WaitForSeconds(0.3f);
		ResourceManager.UnloadLive2DAnimation("SAILOR_PLUTO");
		ResourceManager.UnloadSsAnimation("OLDEVENT_EXTEND");
		ResourceManager.UnloadSound("SE_OLD_EVENT_EXTENSION_00");
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void SetFinishedCallback(OnEffectFinished callback)
	{
		mCallback = callback;
	}
}
