using UnityEngine;

public static class InternetReachability
{
	public static bool IsEnable
	{
		get
		{
			return Status != NetworkReachability.NotReachable;
		}
	}

	public static NetworkReachability Status
	{
		get
		{
			return Application.internetReachability;
		}
	}

	public static void Init()
	{
	}

	public static void Dispose()
	{
	}
}
