using System;

public class ShopItemData : ICloneable
{
	private bool mIsSetBundle;

	public int Index { get; set; }

	public int GemDefaultAmount { get; set; }

	public int GemAmount { get; set; }

	public int SaleNum { get; set; }

	public int DefaultNum { get; set; }

	public string Detail { get; set; }

	public string ShopIconName { get; set; }

	public string IconName { get; set; }

	public string SaleIconName { get; set; }

	public string Name { get; set; }

	public string Desc { get; set; }

	public int BoosterID { get; set; }

	public string SupplementKey { get; set; }

	public string DialogSpriteTitle { get; set; }

	public string DialogSpriteBack { get; set; }

	public bool DialogShowBanner { get; set; }

	public string str { get; set; }

	public bool IsSetBundle
	{
		get
		{
			return mIsSetBundle;
		}
	}

	public ShopItemDetail ItemDetail { get; set; }

	public bool IsSale
	{
		get
		{
			return SaleNum == 1;
		}
	}

	public ShopItemData()
	{
		Index = 0;
		GemDefaultAmount = 0;
		GemAmount = 0;
		SaleNum = 0;
		DefaultNum = 0;
		Detail = string.Empty;
		ShopIconName = string.Empty;
		IconName = string.Empty;
		SaleIconName = string.Empty;
		Name = string.Empty;
		Desc = string.Empty;
		BoosterID = 0;
		SupplementKey = string.Empty;
		DialogSpriteTitle = string.Empty;
		DialogSpriteBack = string.Empty;
		DialogShowBanner = false;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void deserialize(BIJBinaryReader stream)
	{
		Index = stream.ReadShort();
		GemDefaultAmount = stream.ReadShort();
		GemAmount = stream.ReadShort();
		SaleNum = stream.ReadShort();
		DefaultNum = stream.ReadShort();
		Detail = stream.ReadUTF();
		ShopIconName = stream.ReadUTF();
		IconName = stream.ReadUTF();
		SaleIconName = stream.ReadUTF();
		Name = stream.ReadUTF();
		Desc = stream.ReadUTF();
		BoosterID = stream.ReadShort();
		SupplementKey = stream.ReadUTF();
		DialogSpriteTitle = stream.ReadUTF();
		DialogSpriteBack = stream.ReadUTF();
		DialogShowBanner = stream.ReadShort() != 0;
		ShopItemDetail obj = new ShopItemDetail();
		new MiniJSONSerializer().Populate(ref obj, Detail);
		ItemDetail = obj;
		ItemDetail.SetItem();
		mIsSetBundle = ItemDetail.IsSetBundle();
		if (!IsSale && !IsSetBundle)
		{
			ItemDetail.SetDefault(DefaultNum);
		}
	}

	public ShopItemData Clone()
	{
		return MemberwiseClone() as ShopItemData;
	}
}
