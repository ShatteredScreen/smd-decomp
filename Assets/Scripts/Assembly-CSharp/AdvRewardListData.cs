public class AdvRewardListData
{
	public int ID;

	public string UsePlace;

	public short Type;

	public int Category;

	public int Kind;

	public int ItemID;

	public long num;

	public bool CanReceive = true;

	public string IconKey;

	public long Value
	{
		get
		{
			return num;
		}
	}
}
