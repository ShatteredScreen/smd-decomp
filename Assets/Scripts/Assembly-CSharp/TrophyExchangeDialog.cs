using System.Collections.Generic;
using UnityEngine;

public class TrophyExchangeDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private OnDialogClosed mCallback;

	private float mCounter;

	private bool mOKEnable = true;

	private UISsSprite mUISsAnime;

	private int mTrophyGetnum;

	private TrophyItemListItem mItemData;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy())
		{
		}
		mCounter += Time.deltaTime;
		if (mOKEnable && mCounter > 1f)
		{
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 4, new Vector3(0f, -113f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			mOKEnable = false;
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("GetAccessory_Item_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 63f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 104);
		uISprite.type = UIBasicSprite.Type.Sliced;
		string empty = string.Empty;
		int num = 0;
		foreach (KeyValuePair<TrophyItemDetail.ItemKind, int> bundleItem in mItemData.data.ItemDetail.BundleItems)
		{
			num = bundleItem.Value;
		}
		if (mItemData.data.Category == 1)
		{
			BoosterData boosterData = mGame.mBoosterData[mItemData.data.ItemId];
			empty = boosterData.iconBuy;
			titleDescKey = string.Format(Localization.Get(boosterData.name));
			titleDescKey = string.Format(Localization.Get("GetAccessory_Item_DescA"), titleDescKey, num);
		}
		else
		{
			empty = mItemData.data.IconName;
			titleDescKey = string.Format(Localization.Get(mItemData.data.Name));
			titleDescKey = string.Format(Localization.Get("GetAccessory_Item_DescA"), titleDescKey, num);
		}
		UISprite sprite = Util.CreateSprite("IconImage", uISprite.gameObject, image);
		Util.SetSpriteInfo(sprite, empty, mBaseDepth + 2, new Vector3(0f, 1f, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, -36f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
	}

	public void Init(int trophyGetNum)
	{
		mTrophyGetnum = trophyGetNum;
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void Init(TrophyItemListItem a_item)
	{
		mItemData = a_item;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		if (!GetBusy() && !mOKEnable)
		{
			OnCancelPushed(null);
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
