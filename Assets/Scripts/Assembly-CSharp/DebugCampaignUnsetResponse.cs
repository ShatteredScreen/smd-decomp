public class DebugCampaignUnsetResponse : ParameterObject<DebugCampaignUnsetResponse>
{
	public long server_time { get; set; }

	public int code { get; set; }
}
