using System;

public class SeasonEventSettings : ICloneable
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("id")]
	public int EventID { get; set; }

	[MiniJSONAlias("help_id")]
	public int HelpID { get; set; }

	[MiniJSONAlias("help_url")]
	public string HelpURL { get; set; }

	[MiniJSONAlias("info_banner")]
	public string InfoBannerFileName { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public SeasonEventSettings Clone()
	{
		return MemberwiseClone() as SeasonEventSettings;
	}
}
