using UnityEngine;

public class GPGSPlugin : MonoBehaviour
{
	private static GPGSPlugin mInstance;

	private GooglePlayGameServiceIF mPluginIF;

	public static GPGSPlugin Instance
	{
		get
		{
			if (mInstance == null)
			{
				GameObject gameObject = new GameObject("GooglePlayGameServices");
				mInstance = gameObject.GetComponent<GPGSPlugin>();
				if (mInstance == null)
				{
					mInstance = gameObject.AddComponent<GPGSPlugin>();
				}
			}
			return mInstance;
		}
	}

	public static GooglePlayGameServiceIF PluginIF
	{
		get
		{
			return Instance.mPluginIF;
		}
	}

	private void Awake()
	{
		if (mInstance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		mInstance = this;
		Object.DontDestroyOnLoad(base.gameObject);
		mPluginIF = base.gameObject.AddComponent<GPGSAndroid>();
	}

	private void OnDestroy()
	{
	}

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnApplicationPause(bool pause)
	{
	}
}
