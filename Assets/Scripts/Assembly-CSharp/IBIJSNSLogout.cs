using System;

public interface IBIJSNSLogout : ICloneable, ICopyable
{
	bool Logout { get; set; }
}
