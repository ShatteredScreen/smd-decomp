using System.Collections.Generic;
using System.Linq;

public class SMEventPageData : SMMapPageData
{
	public int EventID;

	public SMEventSetting EventSetting;

	public SortedDictionary<int, SMEventCourseSetting> EventCourseList = new SortedDictionary<int, SMEventCourseSetting>();

	public SMEventBingoMapSetting BingoMapSetting;

	public SMEventCharacterMapSetting CharacterMapSetting;

	public SortedDictionary<int, SMEventLabyrinthStageSetting> LabyrinthStageList = new SortedDictionary<int, SMEventLabyrinthStageSetting>();

	public SMEventPageData()
	{
	}

	public SMEventPageData(string a_filename)
		: base(a_filename)
	{
	}

	public List<SMMapStageSetting> GetMapStages(int a_course)
	{
		List<SMMapStageSetting> list = new List<SMMapStageSetting>();
		List<SMMapStageSetting> list2 = new List<SMMapStageSetting>(MapStageDataList.Values);
		for (int i = 0; i < list2.Count; i++)
		{
			if (list2[i].mCourseNo == a_course)
			{
				list.Add(list2[i]);
			}
		}
		return list;
	}

	public List<SMRoadBlockSetting> GetRoadBlocks(int a_course)
	{
		List<SMRoadBlockSetting> list = new List<SMRoadBlockSetting>();
		List<SMRoadBlockSetting> list2 = new List<SMRoadBlockSetting>(RoadBlockDataList.Values);
		list2.Sort((SMRoadBlockSetting a, SMRoadBlockSetting b) => a.RoadBlockID - b.RoadBlockID);
		for (int i = 0; i < list2.Count; i++)
		{
			if (list2[i].mCourseNo == a_course)
			{
				list.Add(list2[i]);
			}
		}
		return list;
	}

	public int GetCourseTotalStars(short a_course, int a_star)
	{
		SMEventCourseSetting sMEventCourseSetting = EventCourseList[a_course];
		return a_star * sMEventCourseSetting.StageNum;
	}

	public List<int> GetMapStageNo(int a_course)
	{
		List<int> list = new List<int>();
		List<SMMapStageSetting> mapStages = GetMapStages(a_course);
		for (int i = 0; i < mapStages.Count; i++)
		{
			list.Add(Def.GetStageNo(mapStages[i].mStageNo, mapStages[i].mSubNo));
		}
		return list;
	}

	public override SMMapStageSetting GetMapStage(int a_main, int a_sub, int a_course = 0)
	{
		string key = SMMapPageSettingBase.MakeKey(a_main, a_sub, a_course);
		SMMapStageSetting value;
		if (MapStageDataList.TryGetValue(key, out value))
		{
			return value;
		}
		return null;
	}

	public override SMChapterSetting GetChapter(int a_main, int a_sub, int a_course = 0)
	{
		string key = SMMapPageSettingBase.MakeKey(a_main, a_sub, a_course);
		SMChapterSetting value;
		if (ChapterDataList.TryGetValue(key, out value))
		{
			return value;
		}
		return null;
	}

	public override SMChapterSetting GetChapterSearch(int a_main, int a_course = 0)
	{
		foreach (KeyValuePair<string, SMChapterSetting> chapterData in ChapterDataList)
		{
			if (chapterData.Value.mCourseNo != a_course || chapterData.Value.mStageNo > a_main || a_main > chapterData.Value.mSubNo)
			{
				continue;
			}
			return chapterData.Value;
		}
		return null;
	}

	public override SMRoadBlockSetting GetRoadBlock(int a_main, int a_sub, int a_course = 0)
	{
		foreach (KeyValuePair<string, SMRoadBlockSetting> roadBlockData in RoadBlockDataList)
		{
			if (roadBlockData.Value.mCourseNo != a_course || roadBlockData.Value.mStageNo != a_main || a_sub != roadBlockData.Value.mSubNo)
			{
				continue;
			}
			return roadBlockData.Value;
		}
		return null;
	}

	public SMEventLabyrinthStageSetting GetLabyrinthStageSetting(int a_main, int a_sub, int a_course)
	{
		foreach (KeyValuePair<int, SMEventLabyrinthStageSetting> labyrinthStage in LabyrinthStageList)
		{
			if (labyrinthStage.Value.mCourseNo != a_course || labyrinthStage.Value.mStageNo != a_main || a_sub != labyrinthStage.Value.mSubNo)
			{
				continue;
			}
			return labyrinthStage.Value;
		}
		return null;
	}

	public List<int> GetDemoList()
	{
		List<int> list = new List<int>();
		List<SMMapStageSetting> list2 = new List<SMMapStageSetting>(MapStageDataList.Values);
		for (int i = 0; i < list2.Count; i++)
		{
			SMMapStageSetting sMMapStageSetting = list2[i];
			if (!string.IsNullOrEmpty(sMMapStageSetting.StageSelectDemo) && sMMapStageSetting.StageSelectDemo != "NULL")
			{
				int item = int.Parse(sMMapStageSetting.StageSelectDemo);
				list.Add(item);
			}
			if (!string.IsNullOrEmpty(sMMapStageSetting.ClearDemo) && sMMapStageSetting.ClearDemo != "NULL")
			{
				int item2 = int.Parse(sMMapStageSetting.ClearDemo);
				list.Add(item2);
			}
		}
		return list;
	}

	public override void Serialize(BIJFileBinaryWriter sw)
	{
		base.Serialize(sw);
		sw.WriteInt(MapStarRewardList.Count);
		List<int> list = MapStarRewardList.Keys.ToList();
		list.Sort();
		foreach (int item in list)
		{
			SMEventStarRewardSetting sMEventStarRewardSetting = MapStarRewardList[item];
			sMEventStarRewardSetting.Serialize(sw);
		}
		if (BingoMapSetting == null)
		{
			BingoMapSetting = new SMEventBingoMapSetting();
		}
		BingoMapSetting.Serialize(sw);
		sw.WriteInt(EventRewardList.Count);
		List<int> list2 = EventRewardList.Keys.ToList();
		list2.Sort();
		foreach (int item2 in list2)
		{
			SMEventRewardSetting sMEventRewardSetting = EventRewardList[item2];
			sMEventRewardSetting.Serialize(sw);
		}
		sw.WriteInt(BingoStageList.Count);
		List<string> list3 = BingoStageList.Keys.ToList();
		list3.Sort();
		foreach (string item3 in list3)
		{
			SMEventBingoStageSetting sMEventBingoStageSetting = BingoStageList[item3];
			sMEventBingoStageSetting.Serialize(sw);
		}
		if (CharacterMapSetting == null)
		{
			CharacterMapSetting = new SMEventCharacterMapSetting();
		}
		CharacterMapSetting.Serialize(sw);
		sw.WriteInt(LabyrinthStageList.Count);
		List<int> list4 = LabyrinthStageList.Keys.ToList();
		list4.Sort();
		foreach (int item4 in list4)
		{
			SMEventLabyrinthStageSetting sMEventLabyrinthStageSetting = LabyrinthStageList[item4];
			sMEventLabyrinthStageSetting.Serialize(sw);
		}
		EventSetting.Serialize(sw);
		sw.WriteInt(EventCourseList.Count);
		list = EventCourseList.Keys.ToList();
		list.Sort();
		foreach (int item5 in list)
		{
			SMEventCourseSetting sMEventCourseSetting = EventCourseList[item5];
			sMEventCourseSetting.Serialize(sw);
		}
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		Series = (Def.SERIES)a_reader.ReadInt();
		MaxMainStageNum = a_reader.ReadInt();
		MaxSubStageNum = a_reader.ReadInt();
		int num = a_reader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			SMEventMapSetting sMEventMapSetting = new SMEventMapSetting();
			sMEventMapSetting.SetSeries(Series);
			sMEventMapSetting.Deserialize(a_reader);
			AddMapData(sMEventMapSetting);
		}
		num = a_reader.ReadInt();
		for (int j = 0; j < num; j++)
		{
			SMEventRoadBlockSetting sMEventRoadBlockSetting = new SMEventRoadBlockSetting();
			sMEventRoadBlockSetting.SetSeries(Series);
			sMEventRoadBlockSetting.Deserialize(a_reader);
			AddRoadBlockData(sMEventRoadBlockSetting);
		}
		num = a_reader.ReadInt();
		for (int k = 0; k < num; k++)
		{
			SMEventChapterSetting sMEventChapterSetting = new SMEventChapterSetting();
			sMEventChapterSetting.SetSeries(Series);
			sMEventChapterSetting.Deserialize(a_reader);
			AddChapterData(sMEventChapterSetting);
		}
		num = a_reader.ReadInt();
		for (int l = 0; l < num; l++)
		{
			SMEventStageSetting sMEventStageSetting = new SMEventStageSetting();
			sMEventStageSetting.SetSeries(Series);
			sMEventStageSetting.Deserialize(a_reader);
			AddMapStageData(sMEventStageSetting);
		}
		num = a_reader.ReadInt();
		for (int m = 0; m < num; m++)
		{
			SMDailyEventSetting sMDailyEventSetting = new SMDailyEventSetting();
			sMDailyEventSetting.SetSeries(Series);
			sMDailyEventSetting.Deserialize(a_reader);
			AddDailyEventData(sMDailyEventSetting);
		}
		num = a_reader.ReadInt();
		for (int n = 0; n < num; n++)
		{
			SMEventStarRewardSetting sMEventStarRewardSetting = new SMEventStarRewardSetting();
			sMEventStarRewardSetting.SetSeries(Series);
			sMEventStarRewardSetting.Deserialize(a_reader);
			AddStarRewardData(sMEventStarRewardSetting);
		}
		BingoMapSetting = new SMEventBingoMapSetting();
		BingoMapSetting.Deserialize(a_reader);
		num = a_reader.ReadInt();
		for (int num2 = 0; num2 < num; num2++)
		{
			SMEventRewardSetting sMEventRewardSetting = new SMEventRewardSetting();
			sMEventRewardSetting.SetSeries(Series);
			sMEventRewardSetting.Deserialize(a_reader);
			AddEventRewardData(sMEventRewardSetting);
		}
		num = a_reader.ReadInt();
		for (int num3 = 0; num3 < num; num3++)
		{
			SMEventBingoStageSetting sMEventBingoStageSetting = new SMEventBingoStageSetting();
			sMEventBingoStageSetting.SetSeries(Series);
			sMEventBingoStageSetting.Deserialize(a_reader);
			AddBingoStageData(sMEventBingoStageSetting);
		}
		CharacterMapSetting = new SMEventCharacterMapSetting();
		CharacterMapSetting.Deserialize(a_reader);
		num = a_reader.ReadInt();
		for (int num4 = 0; num4 < num; num4++)
		{
			SMEventLabyrinthStageSetting sMEventLabyrinthStageSetting = new SMEventLabyrinthStageSetting();
			sMEventLabyrinthStageSetting.SetSeries(Series);
			sMEventLabyrinthStageSetting.Deserialize(a_reader);
			AddLabyrinthStageData(sMEventLabyrinthStageSetting);
		}
		EventSetting = new SMEventSetting(0);
		EventSetting.Deserialize(a_reader);
		EventID = EventSetting.EventID;
		num = a_reader.ReadInt();
		for (int num5 = 0; num5 < num; num5++)
		{
			SMEventCourseSetting sMEventCourseSetting = new SMEventCourseSetting();
			sMEventCourseSetting.SetSeries(Series);
			sMEventCourseSetting.Deserialize(a_reader);
			sMEventCourseSetting.ConvertToEachEvent(EventSetting.EventType);
			AddCourseData(sMEventCourseSetting);
		}
	}

	public void AddCourseData(SMEventCourseSetting a_data)
	{
		if (!EventCourseList.ContainsKey(a_data.mCourseNo))
		{
			EventCourseList.Add(a_data.mCourseNo, a_data);
		}
		else
		{
			EventCourseList[a_data.mCourseNo] = a_data;
		}
	}

	public static void UpdateEventPageData(SMEventPageData aCurrent, SMEventPageData aUpdate)
	{
		SMMapPageData.UpdateMapPageData(aCurrent, aUpdate);
		aCurrent.EventSetting = aUpdate.EventSetting;
		foreach (SMEventCourseSetting value in aUpdate.EventCourseList.Values)
		{
			aCurrent.AddCourseData(value);
		}
	}

	public void AddLabyrinthStageData(SMEventLabyrinthStageSetting a_data)
	{
		if (!LabyrinthStageList.ContainsKey(a_data.StageNo))
		{
			LabyrinthStageList.Add(a_data.StageNo, a_data);
		}
		else
		{
			LabyrinthStageList[a_data.StageNo] = a_data;
		}
	}
}
