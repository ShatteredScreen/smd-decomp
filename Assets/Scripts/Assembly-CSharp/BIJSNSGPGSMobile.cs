using System;
using System.Collections;
using UnityEngine;

public class BIJSNSGPGSMobile : BIJSNSGooglePlayGameServices
{
	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		OP_TYPE oP_TYPE = OP_TYPE.NONE;
		try
		{
			oP_TYPE = (OP_TYPE)(int)Enum.ToObject(typeof(OP_TYPE), type);
		}
		catch (Exception)
		{
			return false;
		}
		switch (oP_TYPE)
		{
		case OP_TYPE.CLEARDATA:
		case OP_TYPE.LOGIN:
		case OP_TYPE.LOGOUT:
		case OP_TYPE.GETUSERINFO:
		case OP_TYPE.UNLOCKACHIEVEMENT:
		case OP_TYPE.INCREMENTACHIEVEMENT:
		case OP_TYPE.SHOWACHIEVEMENTS:
			return true;
		default:
			return false;
		}
	}

	public override bool IsLogined()
	{
		return GPGSPlugin.PluginIF.isSignedIn();
	}

	public override void ClearData(Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSGPGSMobile.ClearData");
		if (GPGSPlugin.PluginIF.resetAchievementAll())
		{
			CallCallback(handler, new Response(0, string.Empty, null), userdata);
		}
		else
		{
			CallCallback(handler, new Response(1, "Google Play Game Services failed to clear data", null), userdata);
		}
	}

	public override void Login(Handler handler, object userdata)
	{
		if (IsLogined())
		{
			CallCallback(handler, new Response(0, "Google Play Game Services already logged in", null), userdata);
			return;
		}
		int type = 2;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			GPGSPlugin.PluginIF.authenticate();
		}
		else
		{
			CallCallback(handler, new Response(1, "Google Play Game Services not requested", null), userdata);
		}
	}

	public override void Logout(Handler handler, object userdata)
	{
		if (!IsLogined())
		{
			CallCallback(handler, new Response(0, "Google Play Game Services already logged out", null), userdata);
			return;
		}
		GPGSPlugin.PluginIF.signOut();
		CallCallback(handler, new Response(0, string.Empty, null), userdata);
	}

	private bool CheckNoLogined()
	{
		return !IsLogined();
	}

	private IEnumerator BIJSNSGPGSMobile_PluginFailed(int seqno, string message, object result)
	{
		yield return new WaitForSeconds(0.5f);
		if (base.CurrentSeqNo == seqno)
		{
			base.CurrentRes = new Response(1, message, result);
			ChangeStep(STEP.COMM_RESULT);
		}
	}

	public override void GetUserInfo(string userId, Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google Play Game Services must logged in", null), userdata);
			return;
		}
		int type = 4;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (!GPGSPlugin.PluginIF.loadPlayer(userId))
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGPGSMobile_PluginFailed(num, "Google+ load person failed: " + userId, null));
			}
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}

	public override void UnlockAchievement(string id, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSGPGSMobile.UnlockArchivement");
		bool flag = false;
		if (GPGSPlugin.PluginIF.unlockAchievement(id))
		{
			CallCallback(handler, new Response(0, string.Empty, id), userdata);
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ failed unlock achievement: " + id, null), userdata);
		}
	}

	public override void IncrementAchievement(string id, int step, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSGPGSMobile.IncrementAchievement");
		bool flag = false;
		if (GPGSPlugin.PluginIF.incrementAchievement(id, step))
		{
			CallCallback(handler, new Response(0, string.Empty, id), userdata);
		}
		else
		{
			CallCallback(handler, new Response(1, "Google Play Game Services failed increment achievement: " + id, null), userdata);
		}
	}

	public override void ShowAchievements(Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google Play Game Services must logged in", null), userdata);
			return;
		}
		int type = 12;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			if (!GPGSPlugin.PluginIF.showAchievements() && base.CurrentSeqNo == num)
			{
				base.CurrentRes = new Response(0, "Google Play Game Services failed to invoke achievements.", null);
				ChangeStep(STEP.COMM_RESULT);
			}
		}
		else
		{
			CallCallback(handler, new Response(1, "Google Play Game Services not requested", null), userdata);
		}
	}
}
