using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Prime31;
using UnityEngine;
using UnityEngine.Events;
using live2d;

public class GameMain : SingletonMonoBehaviour<GameMain>, NetworkInterface
{
	public enum ADV_LINK
	{
		HOME = 0,
		PLAY = 1,
		GASHAMENU = 2,
		GASHA = 3,
		EVENT = 4
	}

	public class AdvNewCharaData
	{
		public int mCharaID { get; set; }

		public int mOldSkillLevel { get; set; }

		public int mNewSkillLevel { get; set; }

		public int mExpPoint { get; set; }

		public int mSkillExpPoint { get; set; }

		public AdvNewCharaData()
		{
			mCharaID = 0;
			mOldSkillLevel = 0;
			mNewSkillLevel = 0;
			mExpPoint = 0;
			mSkillExpPoint = 0;
		}
	}

	public enum LOCALIZE
	{
		JP = 0,
		EN = 1
	}

	protected class GetGiftTemp
	{
		public List<GetMailResponseGift> results { get; set; }
	}

	protected class GetApplyTemp
	{
		public List<GetMailResponseApply> results { get; set; }
	}

	protected class GetSupportTemp
	{
		public List<GetMailResponseSupport> results { get; set; }
	}

	protected class GetSupportPurchaseTemp
	{
		protected List<GetMailResponseSupport> results { get; set; }
	}

	public enum EVENT_TYPE
	{
		OnTouchDown = 0,
		OnTouchUp = 1,
		OnBackKey = 2
	}

	private struct BGMInfo
	{
		public string name;

		public int channel;

		public bool loop;

		public bool stream;

		public bool intro;
	}

	public class PurchaseInfo : IPurchaseInfo, IReceiptData
	{
		public int IAPNo { get; set; }

		public string OrderName { get; set; }

		public string OrderID { get; set; }

		public ITierData TierData { get; set; }

		public int ItemKind { get; set; }

		internal DateTime PurchasedTime { get; set; }

		public int PurchasedTimeStamp
		{
			get
			{
				return (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(PurchasedTime);
			}
		}

		public string Payload { get; set; }

		public string ReceiptSignature { get; set; }

		public string ReceiptDataString { get; set; }

		internal byte[] PurchaseBytes { get; set; }

		internal byte[] ReceiptBytes { get; set; }

		public byte[] GetPurchase()
		{
			byte[] array = PurchaseBytes;
			if (array == null || array.Length < 1)
			{
				array = new byte[1];
			}
			return array;
		}

		public byte[] GetReceipt()
		{
			byte[] array = ReceiptBytes;
			if (array == null || array.Length < 1)
			{
				array = new byte[1];
			}
			return array;
		}

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			Type type = GetType();
			PropertyInfo[] properties = type.GetProperties();
			foreach (PropertyInfo propertyInfo in properties)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(',');
				}
				stringBuilder.Append(propertyInfo.Name);
				stringBuilder.Append('=');
				stringBuilder.Append(propertyInfo.GetValue(this, null));
			}
			return string.Format("[PurchaseInfo: {0}]", stringBuilder.ToString());
		}
	}

	public enum EventState
	{
		None = 0,
		Season = 1,
		Revival = 2
	}

	public enum TROPHY_PRODUCTION
	{
		NONE = 0,
		STOCK = 1,
		OPEN0 = 2,
		OPEN1 = 3
	}

	public enum GiftUIMode
	{
		NONE = -1,
		RECEIVE = 0,
		SEND = 1,
		RESCUE = 2
	}

	public enum GiftListMode
	{
		NONE = -1,
		ALL = 0,
		HEART = 1,
		MEDAL = 2,
		OTHER = 3
	}

	public enum RANKINGMode
	{
		NONE = -1,
		STAR = 0,
		STAGE = 1,
		TROPHY = 2
	}

	public delegate void OnConnectRetry(int a_state);

	public delegate void OnConnectAbandon();

	public const float DebugAdvFeverBoostRatio = 25f;

	public const LOCALIZE Localize = LOCALIZE.EN;

	public const bool SUPERVISION_MODE = false;

	public const bool PERFORMANCE_LOG = false;

	public const bool SKIP_LOGO = false;

	public const bool ALWAYS_WEBVIEW = false;

	public const bool SDCHEAT = false;

	public const bool ALWAYS_GIFT_SENDABLE = false;

	public const bool ALWAYS_FB_INTRO = false;

	public const bool ALWAYS_INVITABLE = false;

	public const bool USE_RANKING_INTRO = false;

	public const bool USE_DEBUG_PATH = false;

	public const bool USE_HELP = true;

	public const bool USE_OPTION = true;

	public const bool USE_MOREGAME = true;

	public const bool USE_ANNOUNCE = true;

	public const bool USE_NICKNAME_CHANGE = true;

	public const bool USE_FACEBOOK = true;

	public const bool USE_GOOGLE_PLUS = true;

	public const bool USE_TWITTER = true;

	public const bool USE_INSTAGRAM = true;

	public const bool USE_GPGS = true;

	public const bool USE_OPTION_GPGS_LOGIN = true;

	public const bool USE_INVITE = true;

	public const bool USE_POST = true;

	public const bool USE_SCREENSHOT = true;

	public const bool USE_ACHIEVEMENT = true;

	public const bool USE_RESOURCEDL_ERROR_DETAIL = false;

	public const bool DEBUG_ADV_ATTACKUP = false;

	public const bool DEBUG_ADV_NOGEM = false;

	public const bool DEBUG_SHOW_SERVER_ERROR = false;

	public const bool MAP_CONNECT_ASYNC = true;

	public const bool GIFT_FRIEND_REQ = true;

	public const bool DEBUG_NO_CURRENCY_CHECK = false;

	public const bool DEBUG_ON_TROPHY_CHECK = true;

	public const bool DISABLE_STAGESKIP = false;

	public const bool ALWAYS_SERIESSELECT = true;

	public const bool USE_ADV_SAVEDATA = false;

	public const bool USE_LOCAL_MASTER_TABLE = false;

	public const bool DISABLE_INIT_MASTER_TABLE = false;

	public const bool USE_LOCAL_SKILLEFFECTDATA_MASTER_TABLE = true;

	private const int SLEEP_TIME_OUT_NONE = -3;

	private const float CONNECTING_DELAY = 0.5f;

	public const int REWARD_LEVEL_NONE = 999;

	public AdvGetAllItemProfile_Response mAdvGetAllItemProfile;

	public AdvQuestStartProfile_Response mAdvQuestStartProfile;

	public AdvQuestEndProfile_Response mAdvQuestEndProfile;

	public AdvGetGashaInfoProfile_Response mAdvGetGashaInfoProfile;

	public AdvGashaPlayProfile_Response mAdvGashaPlayProfile;

	public AdvGrowUpFigureProfile_Response mAdvGrowUpFigureProfile;

	public AdvCheckMasterTableVerProfile_Response mAdvCheckMasterTableVerProfile;

	public AdvGetMasterTableDataProfile_Response mAdvGetMasterTableDataProfile;

	public AdvGetRewardInfoProfile_Response mAdvGetRewardInfoProfile;

	public AdvSetRewardProfile_Response mAdvSetRewardProfile;

	public AdvGetMailProfile_Response mAdvGetMailProfile;

	public AdvSendGotMailProfile_Response mAdvSendGotMailProfile;

	public AdvGetEventPointProfile_Response mAdvGetEventPointProfile;

	public AdvSetEventPointProfile_Response mAdvSetEventPointProfile;

	public DateTime LastAdvGetAllItemTime = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvQuestStart = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvQuestEnd = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvGetGashaInfo = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvGashaPlay = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvGrowUpFigure = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvCheckMasterTableVer = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvGetMasterTableData = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvGetRewardInfo = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvSetReward = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvSendGotMail = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvGetEventPointTime = DateTimeUtil.UnitLocalTimeEpoch;

	public DateTime LastAdvSetEventPointTime = DateTimeUtil.UnitLocalTimeEpoch;

	public AdvDebug_SetItemData_Response mAdvDebug_SetItemDataProfile;

	public AdvDebug_GetAllItem_Response mAdvDebug_GetAllItemProfile;

	public AdvDebug_FigureAllGrowUp_Response mAdvDebug_FigureAllGrowUpProfile;

	public AdvDebug_AddFigure_Response mAdvDebug_AddFigureProfile;

	public AdvDebug_RemoveFigure_Response mAdvDebug_RemoveFigureProfile;

	public AdvDebug_GetAllFigure_Response mAdvDebug_GetAllFigureProfile;

	public AdvDebug_SetFigureStatus_Response mAdvDebug_SetFigureStatusProfile;

	public Dictionary<int, AdvDungeonData> mAdvDungeonData = new Dictionary<int, AdvDungeonData>();

	public Dictionary<int, AdvStageData> mAdvStageData = new Dictionary<int, AdvStageData>();

	public Dictionary<int, AdvCharacterData> mAdvCharactersDataDict = new Dictionary<int, AdvCharacterData>();

	public Dictionary<int, AdvRewardListData> mAdvRewardListDict = new Dictionary<int, AdvRewardListData>();

	public Dictionary<int, AdvGashaInfo> mAdvGashaInfoDict = new Dictionary<int, AdvGashaInfo>();

	public ADV_LINK mADVLink;

	public int mADVLinkParam;

	public List<AdvNewCharaData> mAdvNewCharacterList = new List<AdvNewCharaData>();

	public static bool IsMedalRecover = true;

	public DateTime mGashaPopUpTime = DateTimeUtil.UnitLocalTimeEpoch;

	public static bool mAdvGetAllItemSucceed = false;

	public static bool mAdvGetAllItemCheckDone = false;

	public static Server.ErrorCode mAdvGetAllItemErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvQuestStartSucceed = false;

	public static bool mAdvQuestStartCheckDone = false;

	public static Server.ErrorCode mAdvQuestStartErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvQuestEndSucceed = false;

	public static bool mAdvQuestEndCheckDone = false;

	public static Server.ErrorCode mAdvQuestEndErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvGetGashaInfoSucceed = false;

	public static bool mAdvGetGashaInfoCheckDone = false;

	public static Server.ErrorCode mAdvGetGashaInfoErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvGashaPlaySucceed = false;

	public static bool mAdvGashaPlayCheckDone = false;

	public static Server.ErrorCode mAdvGashaPlayErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvGrowUpFigureSucceed = false;

	public static bool mAdvGrowUpFigureCheckDone = false;

	public static Server.ErrorCode mAdvGrowUpFigureErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvCheckMasterTableVerSucceed = false;

	public static bool mAdvCheckMasterTableVerCheckDone = false;

	public static Server.ErrorCode mAdvCheckMasterTableVerErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvGetMasterTableDataSucceed = false;

	public static bool mAdvGetMasterTableDataCheckDone = false;

	public static Server.ErrorCode mAdvGetMasterTableDataErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvGetRewardInfoSucceed = false;

	public static bool mAdvGetRewardInfoCheckDone = false;

	public static Server.ErrorCode mAdvGetRewardInfoErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvSetRewardSucceed = false;

	public static bool mAdvSetRewardCheckDone = false;

	public static Server.ErrorCode mAdvSetRewardErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvSendSaveDataSucceed = false;

	public static bool mAdvSendSaveDataCheckDone = false;

	public static Server.ErrorCode mAdvSendSaveDataErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvGetSaveDataSucceed = false;

	public static bool mAdvGetSaveDataCheckDone = false;

	public static Server.ErrorCode mAdvGetSaveDataErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvGetMailSucceed = false;

	public static bool mAdvGetMailCheckDone = false;

	public static Server.ErrorCode mAdvGetMailErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvSendGotMailSucceed = false;

	public static bool mAdvSendGotMailCheckDone = false;

	public static Server.ErrorCode mAdvSendGotMailErrorCode = Server.ErrorCode.SUCCESS;

	public static List<KeyValuePair<int, GiftMailType>> mAdvSendGotMailFigureDict = new List<KeyValuePair<int, GiftMailType>>();

	public AdvNewCharaData mAdvGiftNewCharaData;

	public static bool mAdvGetEventPointSucceed = false;

	public static bool mAdvGetEventPointCheckDone = false;

	public static Server.ErrorCode mAdvGetEventPointErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvSetEventPointSucceed = false;

	public static bool mAdvSetEventPointCheckDone = false;

	public static Server.ErrorCode mAdvSetEventPointErrorCode = Server.ErrorCode.SUCCESS;

	public static bool mAdvDebug_FigureAllDeleteSucceed = false;

	public static bool mAdvDebug_FigureAllDeleteCheckDone = false;

	public static bool mAdvDebug_RewardAllDeleteSucceed = false;

	public static bool mAdvDebug_RewardAllDeleteCheckDone = false;

	public static bool mAdvDebug_SetItemDataSucceed = false;

	public static bool mAdvDebug_SetItemDataCheckDone = false;

	public static bool mAdvDebug_GetAllItemSucceed = false;

	public static bool mAdvDebug_GetAllItemCheckDone = false;

	public static bool mAdvDebug_GashaInfoAllDeleteSucceed = false;

	public static bool mAdvDebug_GashaInfoAllDeleteCheckDone = false;

	public static bool mAdvDebug_FigureAllGrowUpSucceed = false;

	public static bool mAdvDebug_FigureAllGrowUpCheckDone = false;

	public static bool mAdvDebug_AddFigureSucceed = false;

	public static bool mAdvDebug_AddFigureCheckDone = false;

	public static bool mAdvDebug_RemoveFigureSucceed = false;

	public static bool mAdvDebug_RemoveFigureCheckDone = false;

	public static bool mAdvDebug_GetAllFigureSucceed = false;

	public static bool mAdvDebug_GetAllFigureCheckDone = false;

	public static bool mAdvDebug_SetFigureStatusSucceed = false;

	public static bool mAdvDebug_SetFigureStatusCheckDone = false;

	private List<AccessoryData> _accessoryData;

	private List<AccessoryData> _mapAccessoryData;

	private List<AccessoryData> _wallPaperAccessoryData;

	private List<AccessoryData> _partnerAccessoryData;

	private List<AccessoryData> _layerNameAccessoryData;

	private Dictionary<Def.SERIES, List<AccessoryData>> _seriesAccessoryData;

	private Dictionary<Def.SERIES, SMMapPageData> _mapPageData;

	private Dictionary<int, SMEventPageData> _eventPageData;

	public Dictionary<int, List<int>> mDemoRallyDemoListCache;

	private List<CompanionData> _companionData;

	private List<SkillEffectData> _skillEffectData;

	private List<StoryDemoData> _storyDemoData;

	private List<VisualCollectionData> _visualCollectionData;

	public int mDebugForceScore = -1;

	public bool mDebugForceWin;

	public bool mDebugForceLose;

	public bool mDebugForceNoMoreMoves;

	public bool mDebugSkillCharge;

	public bool mDebugLongPressMode;

	public bool mDebug2MatchNaviMode = true;

	public Def.DIFFICULTY_MODE mDebugDifficltyMode;

	public bool mDebugDifficultyModeDisable;

	public int mDebugSkillBallMakeX = -1;

	public int mDebugSkillBallMakeY = -1;

	public bool mDebugAutoPlayMode;

	public bool mDebugRepeatPlayMode;

	public int mDebugLifeNotificationSec;

	public int mDebugLogin00NotificationSec;

	public int mDebugLogin01NotificationSec;

	public bool mDebugCheatClearIncludeSub;

	public bool mDebugCheatClearGetItem;

	public int mDebugForceStarNum;

	public int mDebugForceStarNumEventID = -1;

	public bool mDebugPurchaseStoreInfoOff;

	public bool mDebugDLFilelistHashError;

	public bool mDebugSkipCheckStageProcess;

	public bool mDebugAdvAllDrops;

	public bool mDebugAdvDropNoShuffle;

	public bool mDebugAdvNoDrops;

	public float mDebugAdvActionTime = 5f;

	public float mDebugAdvFeverTime = 10f;

	public bool mDebugAdvFeverBoost;

	public bool mDebugAdvSkillAlwaysUse;

	public bool mDebugAdvShowTurnChargeCount;

	public bool mDebugAdvShowStageInfo;

	public bool mDebugAdvEnemyAttackBecomePlayerHP;

	public bool mDebugFlagAdvMTDataRevision;

	public bool mDebugUseDevelopmentServer;

	private readonly string mDevelopmentServerURL = "http://sailormoon.games.bij-games01.com/";

	private readonly string mDevelopmentServerURLWW = "http://ww-sailormoon.games.bij-games01.com/";

	public bool mDebugLoginBonusTimeReset;

	public bool mDebugUseFakeServerTime;

	public DateTime? mDebugFakeServerTime;

	public bool mDebugShowLeadToMapDialog;

	public bool mDebugNoticeSeries;

	public bool mDebugShowLeadToADVDialog;

	public EventAppealSettings mDebugShowEventAppealSetting;

	public int mDebugForceSpecialChance = -1;

	public long mDebugForceSpecialChanceSegmentPrice = -1L;

	public bool mDebugOldEventShowGuideDialog;

	public int mDebugDailyChallengeID;

	public string mDebugDailyChallengeSheetSetName = string.Empty;

	public long mDebugDailyChallengeStartTime = -1L;

	public long mDebugDailyChallengeEndTime = -1L;

	public bool mDebugDailyChallengeVisibleBaseStage;

	public string mDebugInfoName = string.Empty;

	public bool mIsForceJewelGetChance;

	public int mDebugAdvCreateGimmickTilesNum = 21;

	public static bool mDebugNotUseServerTimeDiff;

	public int mDebugHighResoShowMode;

	public int mDebugHighResoLandscapeMode;

	public GameObject mDebugHighResoCanvas;

	public Coroutine mDebugHighResoRoutine;

	public bool mDebugStopGemSale;

	public bool mDebugServiceFinish;

	public string mDebugServiceFinishURL = string.Empty;

	public static bool mDLFileIntegrityCheckDone;

	public static bool mDLFilelistCheckDone = true;

	public static ResourceDownloadManager.KIND mDLFilelistKind;

	private static ScreenOrientation mPrevOrientation;

	private static bool mAutoRotateLL;

	private static bool mAutoRotateLR;

	private static bool mAutoRotateP;

	private static bool mAutoRotatePU;

	public bool NicknameEditSuccess;

	public bool NicknameEditCheckDone;

	public Server.ErrorCode NicknameEditErrorCode;

	public DateTime GetGemCountGetTime = DateTimeUtil.UnitLocalTimeEpoch;

	public bool GetGemCountFreqFlag;

	private bool mMaintenanceRetryFlg;

	private ConnectCommonErrorDialog mConnectErrorDialog;

	private int mConnectErrorState = -1;

	private OnConnectRetry mConnectRetry;

	private OnConnectAbandon mConnectAbandon;

	public int RetryCount;

	public static bool mUserAuthSucceed;

	public static bool mUserAuthCheckDone;

	public static bool mUserAuthChanged;

	public static bool mUserAuthTransferError;

	public static bool mUserAuthTransfered;

	public static Server.ErrorCode mUserAuthErrorCode;

	public static string mServerGameProfileDirectory = string.Empty;

	public static string mServerEventProfileDirectory = string.Empty;

	public static bool mGameProfileSucceed;

	public static bool mGameProfileCheckDone;

	public static string mServerSegmentProfileDirectory = string.Empty;

	public static bool mSegmentProfileSucceed;

	public static bool mSegmentProfileCheckDone = true;

	public static Server.ErrorCode mSegmentProfileErrorCode;

	public static bool mNickNameSucceed;

	public static bool mNickNameCheckDone = true;

	public static Server.ErrorCode NickNameErrorCode;

	public static bool mMaintenanceProfileSucceed;

	public static bool mMaintenanceProfileCheckDone;

	private static bool mSaveGameStateDone = true;

	public static bool mSaveGameDataSucceed;

	public static bool mGetGameDataCheckDone;

	public static bool mGetGameDataSucceed;

	public static bool mLoginBonusSucceed;

	public static bool mLoginBonusCheckDone;

	public static Server.ErrorCode mLoginBonusErrorCode;

	public static string mLoginBonusGUID = string.Empty;

	public static bool mBuyGemSucceed;

	public static bool mBuyGemCheckDone = true;

	public static ServerIAP.BuyGemPlace mBuyGemPlace = ServerIAP.BuyGemPlace.NONE;

	public static bool mGetBirthdaySucceed;

	public static bool mGetBirthdayCheckDone = true;

	public static string mGetBirthdayValue = string.Empty;

	public static bool mSetBirthdaySucceed;

	public static bool mSetBirthdayCheckDone = true;

	public static bool mChargeCheckSucceed;

	public static bool mChargeCheckCheckDone = true;

	public static bool mChargeCheckResult = true;

	public static bool mChargeCheckLimitResult;

	public static bool mChargeCheckAgeResult;

	public static int mChargeCheckAgeType = -1;

	public static bool mGetGemCountSucceed;

	public static bool mGetGemCountCheckDone = true;

	public static Server.ErrorCode mGetGemCountErrorCode;

	public static bool mBuyItemSucceed;

	public static bool mBuyItemCheckDone = true;

	public static Server.ErrorCode mBuyItemErrorCode;

	public static bool mRewardCheckSucceed;

	public static bool mRewardCheckCheckDone = true;

	public static Server.ErrorCode mRewardCheckErrorCode;

	public static int mRewardCheckStatus = -1;

	public static bool mRewardSetSucceed;

	public static bool mRewardSetCheckDone = true;

	public static Server.ErrorCode mRewardSetErrorCode;

	public static int mRewardSetStatus = -1;

	public List<CampaignData> mCampaignList = new List<CampaignData>();

	public LinkBannerRequest mCampaignBannerReq = new LinkBannerRequest();

	public List<LinkBannerInfo> mCampaignBannerList;

	public static bool mCampaignBannerComplete;

	private static bool mCampaignBannerDLOnly;

	public static bool mCampaignCheckDone;

	public static bool mCampaignCheckSucceed;

	public static Server.ErrorCode mCampaignCheckErrorCode;

	public DateTime mLastCampaignCheckTime = DateTimeUtil.UnitLocalTimeEpoch;

	public static bool mCampaignChargeDone;

	public static bool mCampaignChargeSucceed;

	public static Server.ErrorCode mCampaignChargeErrorCode;

	public static bool mDebugCampaignUnsetDone;

	public static bool mDebugCampaignUnsetSucceed;

	public static Server.ErrorCode mDebugCampaignUnsetErrorCode;

	public static bool mGetSaveDataOfOthersCheckDone;

	public static bool mGetSaveDataOfOthersSucceed;

	private string mSearchIdCx = string.Empty;

	public static bool mGetSaveDataOfOthersCXSucceed;

	public List<int> mGotSupportIds = new List<int>();

	public List<int> mGotGiftIds = new List<int>();

	public List<int> mGotApplyIds = new List<int>();

	public List<int> mGotSupportPurchaseIds = new List<int>();

	public GameFriendManager mFriendManager;

	public static bool mMapSupportSucceed;

	public static bool mMapSupportCheckDone;

	public static bool mMapGiftSucceed;

	public static bool mMapGiftCheckDone;

	public static bool mMapApplySucceed;

	public static bool mMapApplyCheckDone;

	public static bool mMapApplyRepeat;

	public static int mMapApplyRepeatCnt;

	public static long mMapApplyRepeatWait = 500L;

	public static int mMapApplyMailMax;

	public static bool mMapSupportPurchaseSucceed;

	public static bool mMapSupportPurchaseCheckDone;

	public bool mNetworkDataModified;

	public static bool mChargePriceCheckSucceed;

	public static bool mChargePriceCheckDone;

	public static Server.ErrorCode mChargePriceCheckErrorCode;

	public static bool USE_DEBUG_DIALOG = false;

	public static bool HIDDEN_DEBUG_BUTTON = false;

	public static bool USE_DEBUG_STATE = false;

	public static bool USE_DEBUG_TIER = false;

	public static bool NO_NETWORK = false;

	public static bool DEBUG_DEBUGSERVERINFO = false;

	public static bool CONNECT_TESTSERVER = false;

	public static bool OPEN_DEBUGDIALOG_BOOT = false;

	public static bool DISABLE_TIMECHECK = false;

	public static bool ALWAYS_LOG = false;

	public static bool DEBUG_WINRATE = false;

	public static bool DEBUG_SHOW_FIGURE_ID = false;

	public static readonly bool USE_RESOURCEDL = true;

	public static readonly bool DEBUG_RESOURCE_DL_CHECK = false;

	public static bool DEBUG_TERMINAL_ENTRY = false;

	public static bool USE_NEW_SERIES_SELECT = true;

	public int mTouchCount;

	public bool mTouchCnd;

	public bool mTouchDownTrg;

	public bool mTouchUpTrg;

	public bool mTouchWasDrag;

	public int mTouchTime;

	public float mTouchTimeSec;

	public Vector2 mTouchPos;

	public Vector2 mTouchPosOld;

	public bool mBackKeyCnd;

	public bool mBackKeyTrg;

	private GameObject mEventSystemGO;

	public GameStateManager mGameStateManager;

	private GameObject mDLManagerObject;

	public ResourceDownloadManager mDLManager;

	public DialogManager mDialogManager;

	public TutorialManager mTutorialManager;

	public FBUserManager mFbUserMngRandom;

	public FBUserManager mFbUserMngGift;

	public FBUserManager mFbUserMngFbFriend;

	public FBUserManager mFbUserMngFriend;

	public List<BoosterData> mBoosterData = new List<BoosterData>();

	public List<TileData> mTileData;

	public List<DailyChallengeSheet> mDailyChallengeData = new List<DailyChallengeSheet>();

	private List<TileData> mTileData_Normal = new List<TileData>();

	private List<TileData> mTileData_Adv = new List<TileData>();

	public Dictionary<int, ShopItemData> mShopItemData = new Dictionary<int, ShopItemData>();

	public Dictionary<int, TrophyExchangeItemData> mTrophyExchangeItemData = new Dictionary<int, TrophyExchangeItemData>();

	public SMEventData mEventData;

	public Dictionary<int, SMSpecialDailyEventData> mSpecialDailyEventData = new Dictionary<int, SMSpecialDailyEventData>();

	public Dictionary<int, SMAlternateRewardData> mAlternateRewardData = new Dictionary<int, SMAlternateRewardData>();

	public Options mOptions;

	public Player mPlayer;

	public PlayerMetaData mPlayerMetaData;

	public NetworkProfile mNetworkProfile;

	public GameProfile mGameProfile;

	public EventProfile mEventProfile;

	public SegmentProfile mSegmentProfile;

	public MaintenanceProfile mMaintenanceProfile;

	public UserAuthResponse mUserUniqueID;

	public UserGetResponse mUserProfile;

	public bool mNeedBackupCheck;

	public string mPlayerFilePath = string.Empty;

	public long mPlayerFileSize;

	public SMDLoginBonusData mLoginBonusData;

	public ServerCramVariableData mServerCramVariableData;

	public ChargePriceCheckResponseProfile mChargePriceCheckData;

	public GameConcierge mConcierge;

	public string mAdvFilePath = string.Empty;

	public long mAdvFileSize;

	public bool IsPlayerSaveRequest;

	public ISMStageData mCurrentStage;

	public SMMapPageData mCurrentMapPageData;

	public SMMapStageSetting mCurrentStageInfo;

	private List<ISMStageData> mStageStuck = new List<ISMStageData>();

	private List<SMMapStageSetting> mStageInfoStuck = new List<SMMapStageSetting>();

	public bool mInputEnable;

	private ScreenOrientation mScreenOrientation;

	private bool mAvailableRotation;

	public Camera mCamera;

	public UICamera mUICamera;

	public ScClippingCamera mClippingCamera;

	public UIAnchor mAnchor;

	public static bool load_step_wait = false;

	public float mDeviceRatioScale = 1f;

	public ScreenOrientation mDeviceOrientaion = ScreenOrientation.Portrait;

	public static int DISPLAY_RANDOM_USER_NUM = 0;

	public static int RANDOM_USER_MAX = 0;

	public static int APPLY_RANDOM_USER_MAX = 0;

	public LinkBannerRequest mLBRequest = new LinkBannerRequest();

	public List<LinkBannerInfo> linkbanner_InfoList;

	public bool adrenoParticled;

	public bool AllStray;

	private BIJLogManager mLogManager;

	private Dictionary<EVENT_TYPE, UnityEvent> mCallbackEvent = new Dictionary<EVENT_TYPE, UnityEvent>();

	private int mSleepTimeout = -3;

	private static UIFont mFont;

	public GameObject mSignRoot;

	public UIRoot mRoot;

	private bool mIsStarted;

	private static bool mIsPlayable = false;

	public bool InitGameFinished;

	private static GameObject mLoading = null;

	private static GameObject mLaceAnimation = null;

	private static GameObject mConnectingAnimation = null;

	private static float mConnectingStart = 0f;

	private static GameObject mPause = null;

	public static float frameDeltaMul = 1f;

	private Dictionary<Def.BOOSTER_KIND, List<int>> BoosterIncludeSaleList = new Dictionary<Def.BOOSTER_KIND, List<int>>();

	private static object syncImageRes = new object();

	private static Shader mDefaultShader;

	private static Shader mDefaultAddShader;

	private static Material mMixColorMixAlphaMarerial_PuzzleTile;

	private static Dictionary<string, GameObject> mParticlePrefabs = new Dictionary<string, GameObject>();

	private static UnityEngine.Object[] mOriginalPrefabs = new UnityEngine.Object[Res.LOAD_PREFABSSALL.Length];

	private static Dictionary<string, GameObject> mTweenPrefabs = new Dictionary<string, GameObject>();

	private static BGMInfo mLastBGM;

	private PurchaseManager mPurchaseManager;

	public static DateTime PurchaseSequenceLastTime = new DateTime(0L);

	public static int PurchaseSequenceCount = 0;

	private static readonly int[,] TUTORIAL_MATRIX = new int[145, 6]
	{
		{ -1, -1, -1, -1, 0, 0 },
		{ 274, 7000, 0, 100, 0, 0 },
		{ 275, 8000, 0, 100, 0, 0 },
		{ 276, 8010, 0, 100, 0, 0 },
		{ 277, 8020, 0, 100, 0, 0 },
		{ 278, 8030, 0, 100, 0, 0 },
		{ 279, 8040, 0, 100, 0, 0 },
		{ 280, 8050, 0, 100, 0, 0 },
		{ 281, 9000, 0, 100, 0, 0 },
		{ 317, 9001, 0, 100, 0, 0 },
		{ 318, 9002, 0, 100, 0, 0 },
		{ 319, 9003, 0, 100, 0, 0 },
		{ 282, 9010, 0, 100, 0, 0 },
		{ 320, 9011, 0, 100, 0, 0 },
		{ 321, 9012, 0, 100, 0, 0 },
		{ 283, 9020, 0, 100, 0, 0 },
		{ 322, 9021, 0, 100, 0, 0 },
		{ 323, 9022, 0, 100, 0, 0 },
		{ 324, 9023, 0, 100, 0, 0 },
		{ 0, 10000, 0, 100, 0, 0 },
		{ 1, 11000, 0, 100, 0, 0 },
		{ 2, 11010, 0, 100, 0, 0 },
		{ 3, 11020, 0, 100, 0, 0 },
		{ 4, 11030, 0, 100, 0, 0 },
		{ 5, 11040, 0, 100, 0, 0 },
		{ 6, 11050, 0, 100, 0, 0 },
		{ 7, 11060, 0, 100, 0, 0 },
		{ 8, 11070, 0, 100, 0, 0 },
		{ 9, 11500, 0, 100, 0, 0 },
		{ 10, 11510, 0, 100, 0, 0 },
		{ 11, 11520, 0, 100, 0, 0 },
		{ 12, 11530, 0, 100, 0, 0 },
		{ 13, 20000, 0, 200, 0, 0 },
		{ 14, 20010, 0, 200, 0, 0 },
		{ 15, 21000, 0, 200, 0, 0 },
		{ 16, 21010, 0, 200, 0, 0 },
		{ 17, 21020, 0, 200, 0, 0 },
		{ 18, 21030, 0, 200, 0, 0 },
		{ 19, 21040, 0, 200, 0, 0 },
		{ 20, 21050, 0, 200, 0, 0 },
		{ 21, 30000, 0, 300, 0, 0 },
		{ 22, 30010, 0, 300, 0, 0 },
		{ 23, 31000, 0, 300, 0, 0 },
		{ 24, 31010, 0, 300, 0, 0 },
		{ 25, 31020, 0, 300, 0, 0 },
		{ 26, 31030, 0, 300, 0, 0 },
		{ 27, 40000, 0, 400, 0, 0 },
		{ 28, 40010, 0, 400, 0, 0 },
		{ 29, 40020, 0, 400, 0, 0 },
		{ 30, 41000, 0, 400, 0, 0 },
		{ 31, 41010, 0, 400, 0, 0 },
		{ 32, 41020, 0, 400, 0, 0 },
		{ 33, 41030, 0, 400, 0, 0 },
		{ 34, 41040, 0, 400, 0, 0 },
		{ 35, 51000, 0, 500, 0, 0 },
		{ 36, 51010, 0, 500, 0, 0 },
		{ 37, 51020, 0, 500, 0, 0 },
		{ 38, 51030, 0, 500, 0, 0 },
		{ 39, 51040, 0, 500, 0, 0 },
		{ 40, 61000, 0, 600, 0, 0 },
		{ 41, 61010, 0, 600, 0, 0 },
		{ 42, 61020, 0, 600, 0, 0 },
		{ 43, 71000, 0, 700, 0, 0 },
		{ 44, 71010, 0, 700, 0, 0 },
		{ 45, 71020, 0, 700, 0, 0 },
		{ 46, 71030, 0, 700, 0, 0 },
		{ 47, 80000, 0, 800, 0, 0 },
		{ 48, 80010, 0, 800, 0, 0 },
		{ 49, 80020, 0, 800, 0, 0 },
		{ 50, 80030, 0, 800, 0, 0 },
		{ 51, 81000, 0, 800, 0, 0 },
		{ 52, 81010, 0, 800, 0, 0 },
		{ 53, 81020, 0, 800, 0, 0 },
		{ 54, 81030, 0, 800, 0, 0 },
		{ 55, 90000, 0, 900, 0, 0 },
		{ 56, 90010, 0, 900, 0, 0 },
		{ 57, 90020, 0, 900, 0, 0 },
		{ 58, 90030, 0, 900, 0, 0 },
		{ 59, 90040, 0, 900, 0, 0 },
		{ 60, 90050, 0, 900, 0, 0 },
		{ 61, 90060, 0, 900, 0, 0 },
		{ 62, 90070, 0, 900, 0, 0 },
		{ 63, 90080, 0, 900, 0, 0 },
		{ 64, 91000, 0, 900, 0, 0 },
		{ 65, 91010, 0, 900, 0, 0 },
		{ 66, 91020, 0, 900, 0, 0 },
		{ 67, 91030, 0, 900, 0, 0 },
		{ 68, 91040, 0, 900, 0, 0 },
		{ 69, 91050, 0, 900, 0, 0 },
		{ 70, 91060, 0, 900, 0, 0 },
		{ 71, 91070, 0, 900, 0, 0 },
		{ 72, 100000, 0, 1000, 0, 0 },
		{ 73, 100010, 0, 1000, 0, 0 },
		{ 74, 100020, 0, 1000, 0, 0 },
		{ 75, 100030, 0, 1000, 0, 0 },
		{ 76, 101000, 0, 1000, 0, 0 },
		{ 77, 101010, 0, 1000, 0, 0 },
		{ 78, 101020, 0, 1000, 0, 0 },
		{ 79, 101030, 0, 1000, 0, 0 },
		{ 80, 101040, 0, 1000, 0, 0 },
		{ 81, 111000, 0, 1100, 0, 0 },
		{ 82, 111010, 0, 1100, 0, 0 },
		{ 83, 111020, 0, 1100, 0, 0 },
		{ 84, 110000, 0, 1100, 0, 0 },
		{ 85, 110010, 0, 1100, 0, 0 },
		{ 86, 110020, 0, 1100, 0, 0 },
		{ 87, 110100, 0, 1100, 0, 0 },
		{ 88, 120000, 0, 1200, 0, 0 },
		{ 89, 120010, 0, 1200, 0, 0 },
		{ 92, 120020, 0, 1200, 0, 0 },
		{ 93, 120030, 0, 1200, 0, 0 },
		{ 90, 131000, 0, 1300, 0, 0 },
		{ 91, 131010, 0, 1300, 0, 0 },
		{ 94, 140000, 0, 1400, 0, 0 },
		{ 95, 140010, 0, 1400, 0, 0 },
		{ 96, 140020, 0, 1400, 0, 0 },
		{ 97, 151000, 0, 1500, 0, 0 },
		{ 98, 151010, 0, 1500, 0, 0 },
		{ 99, 160000, 0, 1600, 0, 0 },
		{ 100, 160010, 0, 1600, 0, 0 },
		{ 101, 161000, 0, 1600, 0, 0 },
		{ 102, 161010, 0, 1600, 0, 0 },
		{ 103, 170000, 0, 1700, 0, 0 },
		{ 104, 170010, 0, 1700, 0, 0 },
		{ 105, 170020, 0, 1700, 0, 0 },
		{ 106, 181000, 0, 1800, 0, 0 },
		{ 107, 181010, 0, 1800, 0, 0 },
		{ 108, 211000, 0, 2100, 0, 0 },
		{ 109, 211010, 0, 2100, 0, 0 },
		{ 190, 220000, 0, 2200, 0, 0 },
		{ 191, 220010, 0, 2200, 0, 0 },
		{ 192, 220020, 0, 2200, 0, 0 },
		{ 284, 1510000, 2, 151, 0, 0 },
		{ 285, 1510010, 2, 151, 0, 0 },
		{ 286, 1510020, 2, 151, 0, 0 },
		{ 287, 1660000, 2, 166, 0, 0 },
		{ 288, 1660010, 2, 166, 0, 0 },
		{ 289, 1660020, 2, 166, 0, 0 },
		{ 290, 1660030, 2, 166, 0, 0 },
		{ 167, 9990000, 0, 99900, 0, 0 },
		{ 168, 9990001, 0, 99900, 0, 0 },
		{ 169, 9990002, 0, 99900, 0, 0 },
		{ 170, 9990003, 0, 99900, 0, 0 },
		{ 181, 9990010, 0, 99900, 0, 0 },
		{ 182, 9990011, 0, 99900, 0, 0 }
	};

	private static readonly int[,] ADV_TUTORIAL_MATRIX = new int[67, 4]
	{
		{ -1, -1, -1, -1 },
		{ 193, 10000, 100, 0 },
		{ 194, 10010, 100, 0 },
		{ 195, 10020, 100, 0 },
		{ 196, 10030, 100, 0 },
		{ 197, 10040, 100, 0 },
		{ 198, 10050, 100, 0 },
		{ 199, 11000, 100, 0 },
		{ 200, 11010, 100, 0 },
		{ 201, 11020, 100, 0 },
		{ 202, 11030, 100, 0 },
		{ 203, 11040, 100, 0 },
		{ 204, 11050, 100, 0 },
		{ 205, 11060, 100, 0 },
		{ 206, 11070, 100, 0 },
		{ 207, 11080, 100, 0 },
		{ 208, 11090, 100, 0 },
		{ 209, 12000, 100, 0 },
		{ 210, 12010, 100, 0 },
		{ 211, 12020, 100, 0 },
		{ 212, 12030, 100, 0 },
		{ 213, 13000, 100, 0 },
		{ 214, 14000, 100, 0 },
		{ 215, 14010, 100, 0 },
		{ 216, 15000, 100, 0 },
		{ 242, 20000, 100, 0 },
		{ 243, 20010, 100, 0 },
		{ 244, 20020, 100, 0 },
		{ 245, 20030, 100, 0 },
		{ 246, 20040, 100, 0 },
		{ 247, 21000, 100, 0 },
		{ 248, 21010, 100, 0 },
		{ 249, 21020, 100, 0 },
		{ 250, 21030, 100, 0 },
		{ 251, 21040, 100, 0 },
		{ 252, 21050, 100, 0 },
		{ 253, 21060, 100, 0 },
		{ 254, 21070, 100, 0 },
		{ 255, 21080, 100, 0 },
		{ 256, 21090, 100, 0 },
		{ 257, 21100, 100, 0 },
		{ 258, 22000, 100, 0 },
		{ 231, 30000, 100, 0 },
		{ 232, 30010, 100, 0 },
		{ 233, 31000, 100, 0 },
		{ 234, 31010, 100, 0 },
		{ 235, 31020, 100, 0 },
		{ 236, 31030, 100, 0 },
		{ 237, 32000, 100, 0 },
		{ 238, 32010, 100, 0 },
		{ 239, 32020, 100, 0 },
		{ 240, 32030, 100, 0 },
		{ 241, 33000, 100, 0 },
		{ 217, 40000, 100, 0 },
		{ 218, 40010, 100, 0 },
		{ 219, 40020, 100, 0 },
		{ 220, 40030, 100, 0 },
		{ 221, 50000, 100, 60001 },
		{ 222, 50010, 100, 60001 },
		{ 223, 50020, 100, 60001 },
		{ 224, 50030, 100, 60001 },
		{ 225, 50040, 100, 60001 },
		{ 226, 60000, 100, 60003 },
		{ 227, 60010, 100, 60003 },
		{ 228, 60020, 100, 60003 },
		{ 229, 60030, 100, 60003 },
		{ 230, 60040, 100, 60003 }
	};

	private List<bool> mWonHistory = new List<bool>();

	private static bool IsWaitForQuit = false;

	private static ScreenOrientation prevOrientation = ScreenOrientation.Unknown;

	private static bool autorotateToLandscapeLeft;

	private static bool autorotateToLandscapeRight;

	private static bool autorotateToPortrait;

	private static bool autorotateToPortraitUpsideDown;

	private static DateTime? mServerTime;

	public bool Debug_AdvMaintenanceMode;

	public bool Debug_MaintenanceModeFlg;

	public int Debug_MaintenanceModeCount;

	public int mTrophyNumProduction;

	public int mTrophyNumSetLayout;

	public TROPHY_PRODUCTION mTrophyProductionFlg;

	private List<MyFriend> mLastCheckHeartSendableFriends;

	public static bool IsSNSFacebookInductionChecked = false;

	private static readonly Dictionary<int, string> ACHIEVEMENT_LEVELS = new Dictionary<int, string>();

	private static readonly Dictionary<int, string> ACHIEVEMENT_LEVELS_EN = new Dictionary<int, string>();

	private static readonly Dictionary<int, string> ACHIEVEMENT_STARS = new Dictionary<int, string>();

	private static readonly Dictionary<int, string> ACHIEVEMENT_STARS_EN = new Dictionary<int, string>();

	private static readonly Dictionary<int, string> ACHIEVEMENT_COLLECTIONS = new Dictionary<int, string>();

	private static readonly Dictionary<int, string> ACHIEVEMENT_COLLECTIONS_EN = new Dictionary<int, string>();

	private int[] mFighter5_IDList = new int[5] { 0, 1, 2, 3, 4 };

	private Def.SERIES mLastCheckRankingSeries;

	private int mLastCheckRankingStageNo;

	private List<StageRankingData> mLastCheckRankingStageData;

	private int mLastCheckPlayerStatStar = -1;

	private int mLastCheckPlayerStatScore = -1;

	private int mLastCheckPlayerStatLevel = -1;

	private int mLastCheckPlayerStatTrophy = -1;

	private bool mLastCheckRankUpStageScore;

	private bool mLastCheckRankUpFriendsStar;

	private bool mLastCheckRankUpFriendsLevel;

	private bool mLastCheckRankUpFriendsTrophy;

	private bool mMyCheckAdd;

	public bool dbgEffectPlaying;

	public bool dbgScoreEffectPlaying;

	public string dbgEffectKey;

	public float dbgInterval;

	public Vector3 dbgPos = Vector3.zero;

	public GameObject dbgParent;

	public int dbgMakeCount;

	public static int AdvGetMailCount { get; private set; }

	public DeepLink TappedDeepLink { get; private set; }

	public List<AccessoryData> mAccessoryData
	{
		get
		{
			if (_accessoryData == null)
			{
				LoadDownloadAccessoryData();
			}
			return _accessoryData;
		}
	}

	public List<AccessoryData> mMapAccessoryData
	{
		get
		{
			if (_mapAccessoryData == null)
			{
				LoadDownloadAccessoryData();
			}
			return _mapAccessoryData;
		}
	}

	public List<AccessoryData> mWallPaperAccessoryData
	{
		get
		{
			if (_wallPaperAccessoryData == null)
			{
				LoadDownloadAccessoryData();
			}
			return _wallPaperAccessoryData;
		}
	}

	public List<AccessoryData> mPartnerAccessoryData
	{
		get
		{
			if (_partnerAccessoryData == null)
			{
				LoadDownloadAccessoryData();
			}
			return _partnerAccessoryData;
		}
	}

	public List<AccessoryData> mLayerNameAccessoryData
	{
		get
		{
			if (_layerNameAccessoryData == null)
			{
				LoadDownloadAccessoryData();
			}
			return _layerNameAccessoryData;
		}
	}

	public Dictionary<Def.SERIES, List<AccessoryData>> mSeriesAccessoryData
	{
		get
		{
			if (_seriesAccessoryData == null)
			{
				LoadDownloadAccessoryData();
			}
			return _seriesAccessoryData;
		}
	}

	public Dictionary<Def.SERIES, SMMapPageData> mMapPageData
	{
		get
		{
			if (_mapPageData == null)
			{
				LoadDownloadMainMapPageData();
			}
			return _mapPageData;
		}
	}

	public Dictionary<int, SMEventPageData> mEventPageData
	{
		get
		{
			if (_eventPageData == null)
			{
				LoadDownloadEventMapPageData();
			}
			return _eventPageData;
		}
	}

	public List<CompanionData> mCompanionData
	{
		get
		{
			if (_companionData == null)
			{
				LoadDownloadCompanionData();
			}
			return _companionData;
		}
	}

	public List<SkillEffectData> mSkillEffectData
	{
		get
		{
			if (_skillEffectData == null)
			{
				LoadDownloadSkillEffectData();
			}
			return _skillEffectData;
		}
	}

	public List<StoryDemoData> mStoryDemoData
	{
		get
		{
			if (_storyDemoData == null)
			{
				LoadDownloadStoryDemoData();
			}
			return _storyDemoData;
		}
	}

	public List<VisualCollectionData> mVisualCollectionData
	{
		get
		{
			if (_visualCollectionData == null)
			{
				LoadDownloadVisualCollectionData();
			}
			return _visualCollectionData;
		}
	}

	public int DEBUG_MAP_LEVEL
	{
		get
		{
			return GlobalVariables.Instance.DEBUG_MAP_LEVEL;
		}
		set
		{
			GlobalVariables.Instance.DEBUG_MAP_LEVEL = value;
		}
	}

	public int mSkinIndex
	{
		get
		{
			return GlobalVariables.Instance.mSkinIndex;
		}
		set
		{
			GlobalVariables.Instance.mSkinIndex = value;
		}
	}

	public int mCompanionIndex
	{
		get
		{
			return GlobalVariables.Instance.mCompanionIndex;
		}
		set
		{
			GlobalVariables.Instance.mCompanionIndex = value;
		}
	}

	public bool mLastCompanionSetting
	{
		get
		{
			return GlobalVariables.Instance.mLastCompanionSetting;
		}
		set
		{
			GlobalVariables.Instance.mLastCompanionSetting = value;
		}
	}

	public bool mEventMode
	{
		get
		{
			return GlobalVariables.Instance.mEventMode;
		}
		set
		{
			GlobalVariables.Instance.mEventMode = value;
		}
	}

	public bool mOldEventMode
	{
		get
		{
			return GlobalVariables.Instance.mOldEventMode;
		}
		set
		{
			GlobalVariables.Instance.mOldEventMode = value;
		}
	}

	public bool mOldEventOpen
	{
		get
		{
			return GlobalVariables.Instance.mOldEventOpen;
		}
		set
		{
			GlobalVariables.Instance.mOldEventOpen = value;
		}
	}

	public bool mOldEventExtend
	{
		get
		{
			return GlobalVariables.Instance.mOldEventExtend;
		}
		set
		{
			GlobalVariables.Instance.mOldEventExtend = value;
		}
	}

	public bool mAdvMode
	{
		get
		{
			return GlobalVariables.Instance.mAdvMode;
		}
		set
		{
			GlobalVariables.Instance.mAdvMode = value;
		}
	}

	public bool mAdvEventMode
	{
		get
		{
			return GlobalVariables.Instance.mAdvEventMode;
		}
		set
		{
			GlobalVariables.Instance.mAdvEventMode = value;
		}
	}

	public int mAdvEventId
	{
		get
		{
			return GlobalVariables.Instance.mAdvEventId;
		}
		set
		{
			GlobalVariables.Instance.mAdvEventId = value;
		}
	}

	public int mEventId
	{
		get
		{
			return GlobalVariables.Instance.mEventId;
		}
		set
		{
			GlobalVariables.Instance.mEventId = value;
		}
	}

	public string mEventName
	{
		get
		{
			return GlobalVariables.Instance.mEventName;
		}
		set
		{
			GlobalVariables.Instance.mEventName = value;
		}
	}

	public int mEventSkinIndex
	{
		get
		{
			return GlobalVariables.Instance.mEventSkinIndex;
		}
		set
		{
			GlobalVariables.Instance.mEventSkinIndex = value;
		}
	}

	public int mEventCompanionIndex
	{
		get
		{
			return GlobalVariables.Instance.mEventCompanionIndex;
		}
		set
		{
			GlobalVariables.Instance.mEventCompanionIndex = value;
		}
	}

	public bool mReplay
	{
		get
		{
			return GlobalVariables.Instance.mReplay;
		}
		set
		{
			GlobalVariables.Instance.mReplay = value;
		}
	}

	public bool mUseBooster0
	{
		get
		{
			return GlobalVariables.Instance.mUseBooster0;
		}
		set
		{
			GlobalVariables.Instance.mUseBooster0 = value;
		}
	}

	public bool mUseBooster1
	{
		get
		{
			return GlobalVariables.Instance.mUseBooster1;
		}
		set
		{
			GlobalVariables.Instance.mUseBooster1 = value;
		}
	}

	public bool mUseBooster2
	{
		get
		{
			return GlobalVariables.Instance.mUseBooster2;
		}
		set
		{
			GlobalVariables.Instance.mUseBooster2 = value;
		}
	}

	public bool mUseBooster3
	{
		get
		{
			return GlobalVariables.Instance.mUseBooster3;
		}
		set
		{
			GlobalVariables.Instance.mUseBooster3 = value;
		}
	}

	public bool mUseBooster4
	{
		get
		{
			return GlobalVariables.Instance.mUseBooster4;
		}
		set
		{
			GlobalVariables.Instance.mUseBooster4 = value;
		}
	}

	public bool mUseBooster5
	{
		get
		{
			return GlobalVariables.Instance.mUseBooster5;
		}
		set
		{
			GlobalVariables.Instance.mUseBooster5 = value;
		}
	}

	public List<Def.BOOSTER_KIND> mUseBoosters
	{
		get
		{
			return GlobalVariables.Instance.mUseBoosters;
		}
		set
		{
			GlobalVariables.Instance.mUseBoosters = value;
		}
	}

	public List<Def.BOOSTER_KIND> mNoSendUseBoosters
	{
		get
		{
			return GlobalVariables.Instance.mNoSendUseBoosters;
		}
		set
		{
			GlobalVariables.Instance.mNoSendUseBoosters = value;
		}
	}

	public bool DoLoginBonusServerCheck
	{
		get
		{
			return GlobalVariables.Instance.DoLoginBonusServerCheck;
		}
		set
		{
			GlobalVariables.Instance.DoLoginBonusServerCheck = value;
		}
	}

	public DateTime AchievementCheckTime
	{
		get
		{
			return GlobalVariables.Instance.AchievementCheckTime;
		}
		set
		{
			GlobalVariables.Instance.AchievementCheckTime = value;
		}
	}

	public FriendHelpPlay PlayingFriendHelp
	{
		get
		{
			return GlobalVariables.Instance.PlayingFriendHelp;
		}
		set
		{
			GlobalVariables.Instance.PlayingFriendHelp = value;
		}
	}

	public bool AddedNewFriend
	{
		get
		{
			return GlobalVariables.Instance.AddedNewFriend;
		}
		set
		{
			GlobalVariables.Instance.AddedNewFriend = value;
		}
	}

	public bool ConnectRetryFlg
	{
		get
		{
			return GlobalVariables.Instance.ConnectRetryFlg;
		}
		set
		{
			GlobalVariables.Instance.ConnectRetryFlg = value;
		}
	}

	public bool FirstBootFlg
	{
		get
		{
			return GlobalVariables.Instance.FirstBootFlg;
		}
		set
		{
			GlobalVariables.Instance.FirstBootFlg = value;
		}
	}

	public bool NoticeFirstBootFlg
	{
		get
		{
			return GlobalVariables.Instance.NoticeFirstBootFlg;
		}
		set
		{
			GlobalVariables.Instance.NoticeFirstBootFlg = value;
		}
	}

	public bool AdvNoticeFirstBootFlg
	{
		get
		{
			return GlobalVariables.Instance.AdvNoticeFirstBootFlg;
		}
		set
		{
			GlobalVariables.Instance.AdvNoticeFirstBootFlg = value;
		}
	}

	public int AutoPopupStageNo
	{
		get
		{
			return GlobalVariables.Instance.AutoPopupStageNo;
		}
		set
		{
			GlobalVariables.Instance.AutoPopupStageNo = value;
		}
	}

	public bool TrophyGuideLoseFlg
	{
		get
		{
			return GlobalVariables.Instance.TrophyGuideLoseFlg;
		}
		set
		{
			GlobalVariables.Instance.TrophyGuideLoseFlg = value;
		}
	}

	public bool TrophyGuideWinCompFlg
	{
		get
		{
			return GlobalVariables.Instance.TrophyGuideWinCompFlg;
		}
		set
		{
			GlobalVariables.Instance.TrophyGuideWinCompFlg = value;
		}
	}

	public GiftUIMode FirstGiftTab
	{
		get
		{
			return GlobalVariables.Instance.FirstGiftTab;
		}
		set
		{
			GlobalVariables.Instance.FirstGiftTab = value;
		}
	}

	private List<AccessoryData> mPresentBoxList
	{
		get
		{
			return GlobalVariables.Instance.mPresentBoxList;
		}
		set
		{
			GlobalVariables.Instance.mPresentBoxList = value;
		}
	}

	public bool mIsStageClearStar3
	{
		get
		{
			return GlobalVariables.Instance.mIsStageClearStar3;
		}
		set
		{
			GlobalVariables.Instance.mIsStageClearStar3 = value;
		}
	}

	public List<SearchUserData> RandomUserList
	{
		get
		{
			return GlobalVariables.Instance.RandomUserList;
		}
		set
		{
			GlobalVariables.Instance.RandomUserList = value;
		}
	}

	public DateTime LastRandomUserGetTime
	{
		get
		{
			return GlobalVariables.Instance.LastRandomUserGetTime;
		}
		set
		{
			GlobalVariables.Instance.LastRandomUserGetTime = value;
		}
	}

	public List<MyFaceBookFriend> FaceBookUserList
	{
		get
		{
			return GlobalVariables.Instance.FaceBookUserList;
		}
		set
		{
			GlobalVariables.Instance.FaceBookUserList = value;
		}
	}

	public Dictionary<string, string> FBNameData
	{
		get
		{
			return GlobalVariables.Instance.FBNameData;
		}
		set
		{
			GlobalVariables.Instance.FBNameData = value;
		}
	}

	public List<int> AlreadyConnectAccessoryList
	{
		get
		{
			return GlobalVariables.Instance.AlreadyConnectAccessoryList;
		}
		set
		{
			GlobalVariables.Instance.AlreadyConnectAccessoryList = value;
		}
	}

	public List<int> AlreadyConnectAdvAccessoryList
	{
		get
		{
			return GlobalVariables.Instance.AlreadyConnectAdvAccessoryList;
		}
		set
		{
			GlobalVariables.Instance.AlreadyConnectAdvAccessoryList = value;
		}
	}

	public bool IsTitleReturnMaintenance
	{
		get
		{
			return GlobalVariables.Instance.IsTitleReturnMaintenance;
		}
		set
		{
			GlobalVariables.Instance.IsTitleReturnMaintenance = value;
		}
	}

	public List<int> CanAcquireAccessoryIdListForWin
	{
		get
		{
			return GlobalVariables.Instance.CanAcquireAccessoryIdListForWin;
		}
		set
		{
			GlobalVariables.Instance.CanAcquireAccessoryIdListForWin = value;
		}
	}

	public bool IsDailyChallengePuzzle
	{
		get
		{
			return GlobalVariables.Instance.IsDailyChallengePuzzle;
		}
		set
		{
			GlobalVariables.Instance.IsDailyChallengePuzzle = value;
		}
	}

	public int SelectedDailyChallengeSheetNo
	{
		get
		{
			return GlobalVariables.Instance.SelectedDailyChallengeSheetNo;
		}
		set
		{
			GlobalVariables.Instance.SelectedDailyChallengeSheetNo = value;
		}
	}

	public int SelectedDailyChallengeStageNo
	{
		get
		{
			return GlobalVariables.Instance.SelectedDailyChallengeStageNo;
		}
		set
		{
			GlobalVariables.Instance.SelectedDailyChallengeStageNo = value;
		}
	}

	private string DevelopmentServerURL
	{
		get
		{
			return mDevelopmentServerURLWW;
		}
	}

	public DateTime? DebugFakeServerTime
	{
		get
		{
			return mDebugFakeServerTime;
		}
		set
		{
			mDebugFakeServerTime = value;
		}
	}

	public ResourceDLErrorInfo DLErrorInfo { get; private set; }

	public bool IsFirstConnection { get; set; }

	public static bool IsServerSaving
	{
		get
		{
			return !mSaveGameStateDone;
		}
	}

	public long mNextCampaignTime { get; set; }

	public long mCampaignDataTime { get; set; }

	public static int MapSupportReceiveCount { get; set; }

	public static int MapSupportRequestCount { get; set; }

	public bool IsTipsScreenVisibled
	{
		get
		{
			if (mGameStateManager != null)
			{
				return mGameStateManager.IsTipsScreenVisibled;
			}
			return false;
		}
	}

	public static bool IsLogoFinished { get; set; }

	public static bool IsLoaded { get; set; }

	public static bool IsPlayable
	{
		get
		{
			return mIsPlayable && mUserAuthCheckDone;
		}
		set
		{
			mIsPlayable = value;
		}
	}

	public static bool IsFirstPlayFromStartup { get; set; }

	public IOptions Options
	{
		get
		{
			return mOptions;
		}
	}

	public IPlayer Player
	{
		get
		{
			return mPlayer;
		}
	}

	public NetworkProfile NetworkProfile
	{
		get
		{
			return mNetworkProfile;
		}
	}

	public GameProfile GameProfile
	{
		get
		{
			return mGameProfile;
		}
	}

	public EventProfile EventProfile
	{
		get
		{
			return mEventProfile;
		}
	}

	public SegmentProfile SegmentProfile
	{
		get
		{
			return mSegmentProfile;
		}
	}

	public MaintenanceProfile MaintenanceProfile
	{
		get
		{
			return mMaintenanceProfile;
		}
	}

	public UserAuthResponse UserUniqueID
	{
		get
		{
			return mUserUniqueID;
		}
	}

	public UserGetResponse UserProfile
	{
		get
		{
			return mUserProfile;
		}
	}

	public GameConcierge Concierge
	{
		get
		{
			return mConcierge;
		}
	}

	public static Shader DefaultShader
	{
		get
		{
			lock (syncImageRes)
			{
				if (mDefaultShader == null)
				{
					mDefaultShader = Shader.Find("BIJ/BIJ2DAlpha");
				}
			}
			return mDefaultShader;
		}
	}

	public static Shader DefaultAddShader
	{
		get
		{
			lock (syncImageRes)
			{
				if (mDefaultAddShader == null)
				{
					mDefaultAddShader = Shader.Find("Ss/AddColorAddAlpha");
				}
			}
			return mDefaultAddShader;
		}
	}

	public int LastWon { get; private set; }

	public int LastStar { get; private set; }

	public static bool IsQuitting { get; private set; }

	public static bool IsTerminated { get; private set; }

	public static bool LockRotationOnApplicationPause { get; set; }

	public static long GET_AUTHUUID_FREQ { get; set; }

	public static long GET_GAME_INFO_FREQ { get; set; }

	public static long GET_SEGMENT_INFO_FREQ { get; set; }

	public static long GET_SUPPORT_FREQ { get; set; }

	public static long GET_SUPPORTPURCHASE_FREQ { get; set; }

	public static long GET_GIFT_FREQ { get; set; }

	public static long GET_APPLY_FREQ { get; set; }

	public static long GET_FRIEND_FREQ { get; set; }

	public static long GET_SOCIAL_FREQ { get; set; }

	public static long GET_FRIEND_ICON_FREQ { get; set; }

	public static long GET_ONE_ICON_FREQ { get; set; }

	public static long GET_RANDOMUSER_FREQ { get; set; }

	public static long GET_GEMCOUNT_FREQ { get; set; }

	public static long GET_MAINTENANCE_INFO_FREQ { get; set; }

	public static long GET_LOGINBONUS_FREQ { get; set; }

	public static double mServerTimeDiff { get; set; }

	public static DateTime? ServerTime
	{
		get
		{
			return mServerTime;
		}
		private set
		{
			mServerTime = value;
			if (value.HasValue)
			{
				DateTime? dateTime = mServerTime;
				mServerTimeDiff = ((!dateTime.HasValue) ? null : new TimeSpan?(DateTime.Now - dateTime.Value)).Value.TotalSeconds;
			}
		}
	}

	public static bool IsRemoteTimeCheater
	{
		get
		{
			if (DISABLE_TIMECHECK)
			{
				return false;
			}
			if (!ServerTime.HasValue)
			{
				return false;
			}
			DateTime now = DateTime.Now;
			DateTime dateTime = ServerTime.Value.AddHours(-Constants.REMOTE_TIMECHEAT_CHECK);
			DateTime dateTime2 = ServerTime.Value.AddHours(Constants.REMOTE_TIMECHEAT_CHECK);
			if (dateTime <= now && now <= dateTime2)
			{
				return false;
			}
			return true;
		}
	}

	public static GameProfile.UpdateKind UpdateVersionFlag { get; set; }

	public static bool MaintenanceMode { get; private set; }

	public static string MaintenanceMessage { get; private set; }

	public static string MaintenanceURL { get; private set; }

	public static bool Adv_MaintenanceMode { get; private set; }

	public static bool IsBlackListUser { get; private set; }

	public static bool IsEnableTransfer { get; private set; }

	public static int TransferMode { get; private set; }

	public static string TransferText { get; private set; }

	public static string TransferURL { get; private set; }

	public static bool CheckReceipt { get; private set; }

	public static long PurchaseLimit { get; private set; }

	public static string SaveDataOfOthersPath { get; private set; }

	public static string BackupPath { get; private set; }

	public List<StageRankingData> LastCheckRankingStageData
	{
		get
		{
			return mLastCheckRankingStageData;
		}
	}

	public GameMain()
	{
		SingletonMonoBehaviour<GameMain>.SetSingletonType(typeof(GameMain));
	}

	private void InitAdv()
	{
	}

	public void CheckAdvAtlasRef()
	{
	}

	public void UnloadAdvFigureAtlas()
	{
	}

	public bool EnableAdvEnter()
	{
		return false;
	}

	public bool EnableAdvSession()
	{
		return false;
	}

	public bool IsAdvEventMode()
	{
		return false;
	}

	public int GetAdvEventID()
	{
		return mAdvEventId;
	}

	public bool GetAdvEventSchedule(int a_eventID, out DateTime a_start, out DateTime a_end, out bool a_ishold)
	{
		a_start = DateTime.Now;
		a_end = DateTime.Now;
		a_ishold = false;
		return false;
	}

	public bool CheckAdvEventExpired(int _eventId)
	{
		return false;
	}

	public int GetMaxPlayableStageID(AdvSaveData aSaveData, int aDungeonClass)
	{
		return 0;
	}

	public void LoadAdvDungeonData(string name)
	{
	}

	public void LoadDownloadAdvDungeonData()
	{
	}

	public List<AdvDungeonData> GetDungeonAllList()
	{
		return new List<AdvDungeonData>();
	}

	public bool IsPlayEventDungeon()
	{
		return false;
	}

	public short[] GetNowAdvEventIdList()
	{
		return null;
	}

	public bool GLAEventOpenNow()
	{
		return false;
	}

	public bool EventOpenNow_NotGLA()
	{
		return false;
	}

	public bool IsGasyaIconPopUp()
	{
		return false;
	}

	public List<AdvStageData> GetAdvStageDataListFromDungeon(int _dungeon_index)
	{
		return null;
	}

	public AdvRewardListData GetRewardListData(int a_id)
	{
		return null;
	}

	public int[] CheckRewardListData(int[] a_id, int forceId = 0)
	{
		return null;
	}

	public bool Network_AdvGetAllItem()
	{
		return true;
	}

	private void Server_OnAdvGetAllItem(int result, int code)
	{
	}

	public void SetAdvGetAllItemFromResponse()
	{
	}

	public bool Network_AdvQuestStart(int quest_id, int[] first_reward_ids, int[] enemy_reward_ids)
	{
		return true;
	}

	private void Server_OnAdvQuestStart(int result, int code)
	{
	}

	public bool Network_AdvQuestEnd(int quest_id, int[] first_reward_ids, int[] enemy_reward_ids, int is_win, string requestId, int is_retry)
	{
		return true;
	}

	private void Server_OnAdvQuestEnd(int result, int code)
	{
	}

	public bool Network_AdvGetGashaInfo()
	{
		return true;
	}

	private void Server_OnAdvGetGashaInfo(int result, int code)
	{
	}

	public bool Network_AdvGashaPlay(int group_id, int gasha_id, int price_id, string request_id, int is_retry)
	{
		return true;
	}

	private void Server_OnAdvGashaPlay(int result, int code)
	{
	}

	public bool Network_AdvGrowUpFigure(int figure_id, int lv_quantity, int skill_quantity, string request_id, int is_retry)
	{
		return true;
	}

	private void Server_OnAdvGrowUpFigure(int result, int code)
	{
	}

	public bool Network_AdvCheckMasterTableVer()
	{
		return true;
	}

	private void Server_OnAdvCheckMasterTableVer(int result, int code)
	{
	}

	public void SetAdvCheckMasterTableVersionFromResponse()
	{
	}

	public bool Network_AdvGetMasterTableData(List<AdvMasterDataInfo> master_table_list)
	{
		return true;
	}

	private void Server_OnAdvGetMasterTableData(int result, int code)
	{
	}

	public void SetAdvGetMasterTableDataFromResponse()
	{
	}

	public bool Network_AdvGetRewardInfo(int[] reward_ids, string category = "quest")
	{
		return true;
	}

	private void Server_OnAdvGetRewardInfo(int result, int code)
	{
	}

	public void SetAdvGetRewardInfoFromResponse()
	{
	}

	public bool Network_AdvSetReward(int[] reward_ids, int[] mail_rewards_ids, string requestId, int is_retry, string category = "quest")
	{
		return true;
	}

	private void Server_OnAdvSetReward(int result, int code)
	{
	}

	public void SetAdvSetRewardFromResponse()
	{
	}

	private bool Network_AdvSendSaveData(BIJBinaryReader dataReader, BIJBinaryReader metaReader, bool force, bool want)
	{
		return true;
	}

	private void Server_OnAdvSendSaveData(int result, int code)
	{
	}

	public bool Network_AdvGetSaveData()
	{
		return true;
	}

	private void Server_OnAdvGetSaveData(int result, int code, Stream resStream)
	{
	}

	public void SetAdvGetGameDataFromResponse(Stream resStream)
	{
	}

	public bool Network_AdvCheckSave()
	{
		return true;
	}

	private void Server_OnAdvCheckSave(int result, int code)
	{
	}

	public bool Network_AdvGetMail(string requestId, int is_retry, bool force = false)
	{
		return true;
	}

	private void Server_OnAdvGetMail(int result, int code, Stream resStream)
	{
	}

	public void SetAdvGetMailFromResponse()
	{
	}

	public bool Network_AdvSendGotMail(int[] id, string request_id, KeyValuePair<int, GiftMailType>[] character_info, int is_retry)
	{
		return Network_AdvSendGotMail_Sub(id, request_id, is_retry);
	}

	public bool Network_AdvSendGotMail(int[] id, string requestId, int characterId, GiftMailType giftMailType, int is_retry)
	{
		return Network_AdvSendGotMail_Sub(id, requestId, is_retry);
	}

	private bool Network_AdvSendGotMail_Sub(int[] id, string request_id, int is_retry)
	{
		return true;
	}

	private void Server_OnAdvSendGotMail(int result, int code, Stream resStream)
	{
	}

	public bool Network_AdvGetEventPoint(short[] event_ids)
	{
		return true;
	}

	private void Server_OnAdvGetEventPoint(int result, int code)
	{
	}

	public bool Network_AdvSetEventPoint(short event_id, int quantity, string requestId, int is_retry)
	{
		return true;
	}

	private void Server_OnAdvSetEventPoint(int result, int code)
	{
	}

	public bool Network_AdvDebug_FigureAllDelete()
	{
		return true;
	}

	private void Server_OnAdvDebug_FigureAllDelete(int result, int code)
	{
	}

	public bool Network_AdvDebug_RewardAllDelete()
	{
		return true;
	}

	private void Server_OnAdvDebug_RewardAllDelete(int result, int code)
	{
	}

	public bool Network_AdvDebug_SetItemData(int itemId, int Quantity)
	{
		return true;
	}

	private void Server_OnAdvDebug_SetItemData(int result, int code)
	{
	}

	public bool Network_AdvDebug_GetAllItem()
	{
		return true;
	}

	private void Server_OnAdvDebug_GetAllItem(int result, int code)
	{
	}

	public bool Network_AdvDebug_GashaInfoAllDelete()
	{
		return true;
	}

	private void Server_OnAdvDebug_GashaInfoAllDelete(int result, int code)
	{
	}

	public bool Network_AdvDebug_FigureAllGrowUp()
	{
		return true;
	}

	private void Server_OnAdvDebug_FigureAllGrowUp(int result, int code)
	{
	}

	public bool Network_AdvDebug_AddFigure(int figureId)
	{
		return true;
	}

	private void Server_OnAdvDebug_AddFigure(int result, int code)
	{
	}

	public bool Network_AdvDebug_RemoveFigure(int figureId)
	{
		return true;
	}

	private void Server_OnAdvDebug_RemoveFigure(int result, int code)
	{
	}

	public bool Network_AdvDebug_GetAllFigure()
	{
		return true;
	}

	private void Server_OnAdvDebug_GetAllFigure(int result, int code)
	{
	}

	public bool Network_AdvDebug_SetFigureStatus(int figureId, int level, int skilllevel, int skil_exp)
	{
		return true;
	}

	private void Server_OnAdvDebug_SetFigureStatus(int result, int code)
	{
	}

	public bool IsPlayAdvMode()
	{
		return false;
	}

	public AdvSaveData GetBackupAdvData()
	{
		return null;
	}

	public bool CountAdvPlayerBoot(out int playspan)
	{
		playspan = 0;
		return true;
	}

	public void OnDeeplinkAction(string a_json)
	{
		DeepLink deepLink = null;
		if (!string.IsNullOrEmpty(a_json))
		{
			try
			{
				deepLink = new MiniJSONSerializer().Deserialize<DeepLink>(a_json);
			}
			catch (Exception)
			{
				deepLink = null;
			}
		}
		if (deepLink != null)
		{
			TappedDeepLink = deepLink;
		}
	}

	public void ClearTappedDeepLink()
	{
		if (TappedDeepLink != null)
		{
			TappedDeepLink = null;
		}
	}

	public void LoadDownloadAccessoryData()
	{
		if (_accessoryData == null)
		{
			_accessoryData = new List<AccessoryData>();
			_mapAccessoryData = new List<AccessoryData>();
			_partnerAccessoryData = new List<AccessoryData>();
			_layerNameAccessoryData = new List<AccessoryData>();
			_wallPaperAccessoryData = new List<AccessoryData>();
			_seriesAccessoryData = new Dictionary<Def.SERIES, List<AccessoryData>>();
		}
		else
		{
			ClearAccessoryList();
		}
		BIJBinaryReader bIJBinaryReader = null;
		if (mDLManager != null)
		{
			bIJBinaryReader = mDLManager.GetDownloadFileReadStream("Accessories_Ex.bin.bytes");
		}
		if (bIJBinaryReader == null)
		{
			bIJBinaryReader = new BIJResourceBinaryReader("Data/Accessories.bin");
		}
		if (bIJBinaryReader != null)
		{
			int num = bIJBinaryReader.ReadInt();
			for (int i = 0; i < num; i++)
			{
				AccessoryData accessoryData = new AccessoryData();
				accessoryData.Deserialize(bIJBinaryReader);
				int index = accessoryData.Index;
				if (index < _accessoryData.Count)
				{
					_accessoryData[index] = accessoryData;
					continue;
				}
				while (_accessoryData.Count < index)
				{
					AccessoryData accessoryData2 = _accessoryData[0].Clone();
					accessoryData2.Index = _accessoryData.Count;
					_accessoryData.Add(accessoryData2);
				}
				_accessoryData.Add(accessoryData);
			}
			bIJBinaryReader.Close();
		}
		SeparateAccessoryData();
	}

	public void ReloadAccessoryData(bool aForce = false)
	{
		if (_accessoryData != null || aForce)
		{
			LoadDownloadAccessoryData();
		}
	}

	private void ClearAccessoryList()
	{
		_accessoryData.Clear();
	}

	private void SeparateAccessoryData()
	{
		_mapAccessoryData.Clear();
		_partnerAccessoryData.Clear();
		_seriesAccessoryData.Clear();
		_layerNameAccessoryData.Clear();
		_wallPaperAccessoryData.Clear();
		for (int i = 0; i < _accessoryData.Count; i++)
		{
			AccessoryData accessoryData = _accessoryData[i];
			if (accessoryData.AccessoryUnlockScene.CompareTo("MAP") == 0)
			{
				_mapAccessoryData.Add(accessoryData);
			}
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER || accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE)
			{
				_wallPaperAccessoryData.Add(accessoryData);
			}
			SortOutAccessoryData(accessoryData);
		}
	}

	private void SortOutAccessoryData(AccessoryData a_accessory)
	{
		AccessoryData.ACCESSORY_TYPE accessoryType = a_accessory.AccessoryType;
		if (accessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
		{
			_partnerAccessoryData.Add(a_accessory);
		}
		List<Def.SERIES> list = new List<Def.SERIES>(Def.SeriesPrefix.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			Def.SERIES key = list[i];
			if (!_seriesAccessoryData.ContainsKey(key))
			{
				_seriesAccessoryData.Add(key, new List<AccessoryData>());
			}
		}
		if (a_accessory.Series == Def.SERIES.SM_ALL)
		{
			for (int j = 0; j < list.Count; j++)
			{
				Def.SERIES key2 = list[j];
				_seriesAccessoryData[key2].Add(a_accessory);
			}
		}
		else
		{
			_seriesAccessoryData[a_accessory.Series].Add(a_accessory);
		}
		if (a_accessory.AnimeLayerName.CompareTo("null") != 0)
		{
			_layerNameAccessoryData.Add(a_accessory);
		}
	}

	public AccessoryData GetAccessoryData(string a_layerName)
	{
		return GetAccessoryData(mPlayer.Data.CurrentSeries, a_layerName);
	}

	public AccessoryData GetAccessoryData(Def.SERIES a_series, string a_layerName)
	{
		for (int i = 0; i < mLayerNameAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mLayerNameAccessoryData[i];
			if (accessoryData.Series == a_series && string.Compare(accessoryData.AnimeLayerName, a_layerName) == 0)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public AccessoryData GetAccessoryData(Def.SERIES a_series, int a_eventID, short a_courseID, string a_layerName)
	{
		for (int i = 0; i < mLayerNameAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mLayerNameAccessoryData[i];
			if (accessoryData.Series == a_series && accessoryData.EventID == a_eventID && accessoryData.CourseID == a_courseID && string.Compare(accessoryData.AnimeLayerName, a_layerName) == 0)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public AccessoryData GetAccessoryData(Def.SERIES a_series, int a_main, int a_sub, string a_location)
	{
		for (int i = 0; i < mAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mAccessoryData[i];
			if (accessoryData.Series == a_series && accessoryData.MainStage == a_main && accessoryData.SubStage == a_sub && accessoryData.AccessoryUnlockScene.CompareTo(a_location) == 0)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public AccessoryData GetAccessoryData(Def.SERIES a_series, int a_eventID, short a_course, int a_main, int a_sub, string a_location)
	{
		for (int i = 0; i < mAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mAccessoryData[i];
			if (accessoryData.Series == a_series && accessoryData.CourseID == a_course && accessoryData.MainStage == a_main && accessoryData.SubStage == a_sub && accessoryData.AccessoryUnlockScene.CompareTo(a_location) == 0)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public AccessoryData GetAccessoryData(int a_index)
	{
		return mAccessoryData[a_index];
	}

	public AccessoryData GetAccessoryData(Def.SERIES a_series, int a_index)
	{
		AccessoryData accessoryData = GetAccessoryData(a_index);
		if (accessoryData.Series == a_series)
		{
			return accessoryData;
		}
		return null;
	}

	public AccessoryData GetAccessoryWallPaperData(int a_index)
	{
		for (int i = 0; i < mWallPaperAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mWallPaperAccessoryData[i];
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER && accessoryData.IsFlip == a_index)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public AccessoryData GetAccessoryWallPaperPieceData(int a_index, int lastID)
	{
		for (int i = 0; i < mWallPaperAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mWallPaperAccessoryData[i];
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE && accessoryData.IsFlip == a_index && accessoryData.GetWallPaperPieceIndex() == lastID)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public AccessoryData GetAccessoryDataOnMap(int a_main, int a_sub)
	{
		for (int i = 0; i < mMapAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mMapAccessoryData[i];
			if (accessoryData.Series == mPlayer.Data.CurrentSeries && accessoryData.MainStage == a_main && accessoryData.SubStage == a_sub && accessoryData.AccessoryUnlockScene.CompareTo("MAP") == 0)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public AccessoryData GetAccessoryDataOnMap(int a_eventID, short a_courseID, int a_main, int a_sub)
	{
		for (int i = 0; i < mMapAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mMapAccessoryData[i];
			if (accessoryData.Series == mPlayer.Data.CurrentSeries && accessoryData.EventID == a_eventID && accessoryData.CourseID == a_courseID && accessoryData.MainStage == a_main && accessoryData.SubStage == a_sub && accessoryData.AccessoryUnlockScene.CompareTo("MAP") == 0)
			{
				return accessoryData;
			}
		}
		return null;
	}

	public List<AccessoryData> GetAccessoryByType(AccessoryData.ACCESSORY_TYPE a_type)
	{
		List<AccessoryData> list = new List<AccessoryData>();
		if (a_type == AccessoryData.ACCESSORY_TYPE.PARTNER)
		{
			list = new List<AccessoryData>(mPartnerAccessoryData);
		}
		if (list.Count == 0)
		{
			for (int i = 0; i < mAccessoryData.Count; i++)
			{
				AccessoryData accessoryData = mAccessoryData[i];
				if (accessoryData.AccessoryType == a_type)
				{
					list.Add(accessoryData);
				}
			}
		}
		return list;
	}

	public List<AccessoryData> GetAccessoryBySeries(Def.SERIES a_series, int a_event = -1)
	{
		List<AccessoryData> list = new List<AccessoryData>();
		List<AccessoryData> value;
		if (!mSeriesAccessoryData.TryGetValue(a_series, out value))
		{
			value = new List<AccessoryData>();
		}
		for (int i = 0; i < value.Count; i++)
		{
			AccessoryData accessoryData = value[i];
			if (accessoryData.Series == a_series && (a_event == -1 || a_event == accessoryData.EventID))
			{
				list.Add(accessoryData);
			}
		}
		return list;
	}

	public AccessoryData.ACCESSORY_TYPE GetRBKeyType(Def.SERIES a_series)
	{
		AccessoryData.ACCESSORY_TYPE result = AccessoryData.ACCESSORY_TYPE.RB_KEY;
		switch (a_series)
		{
		case Def.SERIES.SM_R:
			result = AccessoryData.ACCESSORY_TYPE.RB_KEY_R;
			break;
		case Def.SERIES.SM_S:
			result = AccessoryData.ACCESSORY_TYPE.RB_KEY_S;
			break;
		case Def.SERIES.SM_SS:
			result = AccessoryData.ACCESSORY_TYPE.RB_KEY_SS;
			break;
		case Def.SERIES.SM_STARS:
			result = AccessoryData.ACCESSORY_TYPE.RB_KEY_STARS;
			break;
		case Def.SERIES.SM_GF00:
			result = AccessoryData.ACCESSORY_TYPE.RB_KEY_GF00;
			break;
		}
		return result;
	}

	public List<AccessoryData> GetRoadBlockAccessory(int a_stage, Def.SERIES a_series)
	{
		List<AccessoryData> list = new List<AccessoryData>();
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stage, out a_main, out a_sub);
		AccessoryData.ACCESSORY_TYPE rBKeyType = GetRBKeyType(a_series);
		List<AccessoryData> value;
		if (!mSeriesAccessoryData.TryGetValue(a_series, out value))
		{
			value = new List<AccessoryData>();
		}
		for (int i = 0; i < value.Count; i++)
		{
			AccessoryData accessoryData = value[i];
			if (accessoryData.AccessoryType == rBKeyType && accessoryData.GetRBKeyStageNo() == a_main)
			{
				list.Add(accessoryData);
			}
		}
		return list;
	}

	public List<int> GetRoadBlockAccessoryIndex(int a_stage, Def.SERIES a_series)
	{
		List<AccessoryData> roadBlockAccessory = GetRoadBlockAccessory(a_stage, a_series);
		List<int> list = new List<int>();
		for (int i = 0; i < roadBlockAccessory.Count; i++)
		{
			AccessoryData accessoryData = roadBlockAccessory[i];
			list.Add(accessoryData.Index);
		}
		return list;
	}

	public List<AccessoryData> GetWallPaperPieces(int a_wallpaper)
	{
		List<AccessoryData> list = new List<AccessoryData>();
		for (int i = 0; i < mAccessoryData.Count; i++)
		{
			AccessoryData accessoryData = mAccessoryData[i];
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE && accessoryData.GetWallPaperIndex() == a_wallpaper)
			{
				list.Add(accessoryData);
			}
		}
		return list;
	}

	public int HasWallPaperPieces(int a_wallpaperIndex)
	{
		if (mPlayer.Data.WallPaper_Pieces.ContainsKey(a_wallpaperIndex))
		{
			return mPlayer.Data.WallPaper_Pieces[a_wallpaperIndex];
		}
		return 0;
	}

	public bool AddAccessoryFromPresentBox(int a_index)
	{
		if (mAccessoryData.Count > a_index)
		{
			AccessoryData accessoryData = mAccessoryData[a_index];
			bool flag = mPlayer.AddAccessory(accessoryData, true, false, 2);
			if (flag)
			{
				if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GROWUP_F)
				{
					mPlayer.AddCompanionLevel(mCompanionIndex);
				}
				if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.SP_DAILY && !SPDailyEventAdd(accessoryData))
				{
					return false;
				}
				mPresentBoxList.Add(accessoryData);
			}
			return flag;
		}
		return false;
	}

	public bool AddAccessoryFromTutorial(int a_index)
	{
		if (mAccessoryData.Count > a_index)
		{
			AccessoryData a_data = mAccessoryData[a_index];
			return mPlayer.AddAccessory(a_data);
		}
		return false;
	}

	public bool AddAccessoryConnect(int a_index)
	{
		if (mAccessoryData.Count > a_index)
		{
			AccessoryData accessoryData = mAccessoryData[a_index];
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
			{
				mPlayer.AddAccessoryConnect(a_index);
				GameStateSMMapBase.mIsShowErrorDialog = true;
				Save();
				return true;
			}
		}
		return false;
	}

	public bool AddAdvAccessoryConnect(int a_index)
	{
		if (mAccessoryData.Count > a_index)
		{
			AccessoryData accessoryData = mAccessoryData[a_index];
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
			{
				mPlayer.AddAdvAccessoryConnect(a_index);
				GameStateSMMapBase.mAdvIsShowErrorDialog = true;
				Save();
				return true;
			}
		}
		return false;
	}

	public bool IsEnableReceiveAccessoryType(AccessoryData a_accessory)
	{
		bool result = true;
		switch (a_accessory.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.NONE:
		case AccessoryData.ACCESSORY_TYPE.GROWUP_F:
		case AccessoryData.ACCESSORY_TYPE.HEART:
		case AccessoryData.ACCESSORY_TYPE.SP_DAILY:
		case AccessoryData.ACCESSORY_TYPE.GEM:
		case AccessoryData.ACCESSORY_TYPE.REVIVAL_EVENT:
		case AccessoryData.ACCESSORY_TYPE.MAX:
			result = false;
			break;
		}
		return result;
	}

	public void LoadEventMapPageData()
	{
		string path = "Data/MapPage/";
		TextAsset[] array = Resources.LoadAll<TextAsset>(path);
		if (array == null || array.Length <= 0)
		{
			return;
		}
		Regex regex = new Regex("[^0-9]");
		Array.Sort(array, (TextAsset _x, TextAsset _y) => _x.name.CompareTo(_y.name));
		for (int i = 0; i < array.Length; i++)
		{
			string text = array[i].name;
			if (text.Contains(".meta"))
			{
				continue;
			}
			bool flag = false;
			for (int j = 0; j < Res.LOAD_MAPPAGEDATA.Length; j++)
			{
				if (Res.LOAD_MAPPAGEDATA[j].Length != 0 && text.Contains(Res.LOAD_MAPPAGEDATA[j]))
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				continue;
			}
			int key = int.Parse(regex.Replace(text, string.Empty));
			if (!_eventPageData.ContainsKey(key))
			{
				using (BIJBinaryReader a_reader = new BIJMemoryBinaryReader(array[i].bytes))
				{
					SMEventPageData sMEventPageData = new SMEventPageData();
					sMEventPageData.Deserialize(a_reader);
					_eventPageData.Add(sMEventPageData.EventID, sMEventPageData);
				}
			}
		}
		if (mDemoRallyDemoListCache != null)
		{
			mDemoRallyDemoListCache.Clear();
			mDemoRallyDemoListCache = null;
		}
	}

	public void LoadDownloadMapPageData()
	{
		LoadDownloadMainMapPageData();
		ReloadEventMapPageData();
	}

	public void LoadDownloadMainMapPageData()
	{
		if (_mapPageData == null)
		{
			_mapPageData = new Dictionary<Def.SERIES, SMMapPageData>();
		}
		else
		{
			_mapPageData.Clear();
		}
		string text = "Data/MapPage/";
		for (int i = 0; i < Res.LOAD_MAPPAGEDATA.Length; i++)
		{
			if (Res.LOAD_MAPPAGEDATA[i].Length == 0)
			{
				continue;
			}
			BIJBinaryReader bIJBinaryReader = null;
			if (mDLManager != null)
			{
				string text2 = Res.LOAD_MAPPAGEDATA[i];
				text2 = text2.Substring(0, text2.LastIndexOf(".bin")) + "_Ex.bin.bytes";
				bIJBinaryReader = mDLManager.GetDownloadFileReadStream(text2);
			}
			if (bIJBinaryReader == null)
			{
				string text3 = text + Res.LOAD_MAPPAGEDATA[i];
				bIJBinaryReader = new BIJResourceBinaryReader(text3);
			}
			if (bIJBinaryReader != null)
			{
				SMMapPageData sMMapPageData = new SMMapPageData();
				sMMapPageData.Deserialize(bIJBinaryReader);
				bIJBinaryReader.Close();
				if (_mapPageData.ContainsKey((Def.SERIES)i))
				{
					_mapPageData[(Def.SERIES)i] = sMMapPageData;
				}
				else
				{
					_mapPageData.Add((Def.SERIES)i, sMMapPageData);
				}
			}
		}
	}

	public void LoadDownloadEventMapPageData()
	{
		if (_eventPageData == null)
		{
			_eventPageData = new Dictionary<int, SMEventPageData>();
		}
		else
		{
			_eventPageData.Clear();
		}
		string resourceDLPath = ResourceDownloadManager.getResourceDLPath(ResourceDownloadManager.KIND.MAIN);
		string[] files = Directory.GetFiles(resourceDLPath, "NewEventPage_*.bytes");
		for (int i = 0; i < files.Length; i++)
		{
			string filename = files[i].Substring(resourceDLPath.Length);
			BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream(filename);
			if (downloadFileReadStream != null)
			{
				SMEventPageData sMEventPageData = new SMEventPageData();
				sMEventPageData.Deserialize(downloadFileReadStream);
				downloadFileReadStream.Close();
				if (_eventPageData.ContainsKey(sMEventPageData.EventID))
				{
					_eventPageData[sMEventPageData.EventID] = sMEventPageData;
				}
				else
				{
					_eventPageData.Add(sMEventPageData.EventID, sMEventPageData);
				}
			}
		}
		LoadEventMapPageData();
		if (mDemoRallyDemoListCache != null)
		{
			mDemoRallyDemoListCache.Clear();
			mDemoRallyDemoListCache = null;
		}
	}

	public void ReloadEventMapPageData(bool aForce = false)
	{
		if (_eventPageData != null || aForce)
		{
			LoadDownloadEventMapPageData();
		}
	}

	public SMMapSsData GetMapRouteData(Def.SERIES a_series, int a_eventID, short a_courseID, float a_mapSize)
	{
		SMMapSsData sMMapSsData = null;
		string empty = string.Empty;
		string empty2 = string.Empty;
		if (Def.SeriesMapRouteData.ContainsKey(a_series))
		{
			empty = "Data/Route/" + Def.SeriesMapRouteData[a_series] + ".bin";
			empty2 = Def.SeriesMapRouteData[a_series] + "_Ex.bin.bytes";
		}
		else
		{
			if (a_series != Def.SERIES.SM_EV)
			{
				return null;
			}
			empty = "Data/Route/Event_" + a_eventID + "_" + a_courseID + ".bin";
			empty2 = "Event_" + a_eventID + "_" + a_courseID + "_Ex.bin.bytes";
		}
		if (false)
		{
			try
			{
				BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream(empty2);
				if (downloadFileReadStream == null)
				{
					throw new Exception(string.Empty);
				}
				sMMapSsData = new SMMapSsData();
				sMMapSsData.MapSize(a_mapSize);
				sMMapSsData.Deserialize(downloadFileReadStream);
			}
			catch
			{
				sMMapSsData = null;
			}
		}
		if (sMMapSsData == null)
		{
			try
			{
				BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(empty);
				if (bIJResourceBinaryReader == null)
				{
					throw new Exception(string.Empty);
				}
				sMMapSsData = new SMMapSsData();
				sMMapSsData.MapSize(a_mapSize);
				sMMapSsData.Deserialize(bIJResourceBinaryReader);
			}
			catch
			{
				sMMapSsData = null;
			}
		}
		return sMMapSsData;
	}

	public static SMMapPageData GetSeriesMapPageData(Def.SERIES a_series)
	{
		SMMapPageData value;
		if (!SingletonMonoBehaviour<GameMain>.Instance.mMapPageData.TryGetValue(a_series, out value))
		{
			return null;
		}
		return value;
	}

	public static SMMapPageData GetCurrentMapPageData()
	{
		SMMapPageData value;
		if (!SingletonMonoBehaviour<GameMain>.Instance.mMapPageData.TryGetValue(SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.CurrentSeries, out value))
		{
			return null;
		}
		return value;
	}

	public static SMEventPageData GetEventPageData(int a_eventID)
	{
		SMEventPageData value;
		if (!SingletonMonoBehaviour<GameMain>.Instance.mEventPageData.TryGetValue(a_eventID, out value))
		{
			return null;
		}
		return value;
	}

	public static SMEventPageData GetCurrentEventPageData()
	{
		SMEventPageData value;
		if (!SingletonMonoBehaviour<GameMain>.Instance.mEventPageData.TryGetValue(SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.CurrentEventID, out value))
		{
			return null;
		}
		return value;
	}

	public Dictionary<int, List<int>> GetDemoRallyDemoList()
	{
		if (mDemoRallyDemoListCache == null)
		{
			int count = mEventPageData.Count;
			mDemoRallyDemoListCache = new Dictionary<int, List<int>>();
			List<int> list = new List<int>(mEventPageData.Keys);
			list.Sort();
			for (int i = 0; i < list.Count; i++)
			{
				int key = list[i];
				SMEventPageData sMEventPageData = mEventPageData[key];
				if (sMEventPageData.EventSetting.EventType != Def.EVENT_TYPE.SM_RALLY2 || mDemoRallyDemoListCache.ContainsKey(sMEventPageData.EventID))
				{
					continue;
				}
				List<int> demoList = sMEventPageData.GetDemoList();
				bool flag = false;
				foreach (KeyValuePair<int, List<int>> item in mDemoRallyDemoListCache)
				{
					if (item.Value.Contains(demoList[0]))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					mDemoRallyDemoListCache.Add(sMEventPageData.EventID, demoList);
				}
			}
		}
		return mDemoRallyDemoListCache;
	}

	public int[] GetCompanionRewardIdArray(int a_eventID)
	{
		List<int> list = new List<int>();
		List<int> list2 = new List<int>();
		SMEventPageData eventPageData = GetEventPageData(a_eventID);
		if (eventPageData == null)
		{
			return new int[0];
		}
		switch (eventPageData.EventSetting.EventType)
		{
		case Def.EVENT_TYPE.SM_RALLY:
			foreach (KeyValuePair<int, SMEventCourseSetting> eventCourse in eventPageData.EventCourseList)
			{
				list.Add(eventCourse.Value.ClearAccessoryID);
				int clearAccessoryID = eventCourse.Value.ClearAccessoryID;
				AccessoryData accessoryData3 = GetAccessoryData(clearAccessoryID);
				if (accessoryData3.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
				{
					list2.Add(clearAccessoryID);
				}
			}
			break;
		case Def.EVENT_TYPE.SM_STAR:
		case Def.EVENT_TYPE.SM_RALLY2:
			foreach (KeyValuePair<string, SMRoadBlockSetting> roadBlockData in eventPageData.RoadBlockDataList)
			{
				SMEventRoadBlockSetting sMEventRoadBlockSetting = roadBlockData.Value as SMEventRoadBlockSetting;
				if (sMEventRoadBlockSetting != null)
				{
					list.Add(sMEventRoadBlockSetting.AccessoryIDOnOpened);
					int accessoryIDOnOpened = sMEventRoadBlockSetting.AccessoryIDOnOpened;
					AccessoryData accessoryData2 = GetAccessoryData(accessoryIDOnOpened);
					if (accessoryData2.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
					{
						list2.Add(accessoryIDOnOpened);
					}
				}
			}
			break;
		case Def.EVENT_TYPE.SM_BINGO:
		case Def.EVENT_TYPE.SM_LABYRINTH:
			foreach (KeyValuePair<int, SMEventRewardSetting> eventReward in eventPageData.EventRewardList)
			{
				list.Add(eventReward.Value.RewardID);
				int rewardID = eventReward.Value.RewardID;
				AccessoryData accessoryData = GetAccessoryData(rewardID);
				if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
				{
					list2.Add(rewardID);
				}
			}
			break;
		}
		return list2.ToArray();
	}

	public int GetEventStageNum(int a_eventID)
	{
		int num = 0;
		SMEventPageData eventPageData = GetEventPageData(a_eventID);
		foreach (int key in eventPageData.EventCourseList.Keys)
		{
			List<int> mapStageNo = eventPageData.GetMapStageNo(key);
			num += mapStageNo.Count;
		}
		return num;
	}

	public int GetEventStarNum(int a_eventID)
	{
		int num = 0;
		SMEventPageData eventPageData = GetEventPageData(a_eventID);
		foreach (int key in eventPageData.EventCourseList.Keys)
		{
			num += eventPageData.GetCourseTotalStars((short)key, 3);
		}
		return num;
	}

	public void LoadDownloadCompanionData()
	{
		if (_companionData == null)
		{
			_companionData = new List<CompanionData>();
		}
		else
		{
			_companionData.Clear();
		}
		BIJBinaryReader bIJBinaryReader = null;
		if (mDLManager != null)
		{
			bIJBinaryReader = mDLManager.GetDownloadFileReadStream("Companion_Ex.bin.bytes");
		}
		if (bIJBinaryReader == null)
		{
			bIJBinaryReader = new BIJResourceBinaryReader("Data/Companion.bin");
		}
		if (bIJBinaryReader == null)
		{
			return;
		}
		short num = bIJBinaryReader.ReadShort();
		for (short num2 = 0; num2 < num; num2++)
		{
			CompanionData companionData = new CompanionData();
			companionData.deserialize(bIJBinaryReader);
			int index = companionData.index;
			if (index < _companionData.Count)
			{
				_companionData[index] = companionData;
			}
			else
			{
				while (_companionData.Count < index)
				{
					CompanionData companionData2 = _companionData[0].Clone();
					companionData2.index = (short)_companionData.Count;
					_companionData.Add(companionData2);
				}
				_companionData.Add(companionData);
			}
		}
		bIJBinaryReader.Close();
	}

	public void ReloadCompanionData(bool aForce = false)
	{
		if (_companionData != null || aForce)
		{
			LoadDownloadCompanionData();
		}
	}

	public int getUnlockedCharacterCount(bool sailorCharacter, bool expand)
	{
		int num = 0;
		foreach (CompanionData mCompanionDatum in mCompanionData)
		{
			if (sailorCharacter)
			{
				if (!mCompanionDatum.transform)
				{
					continue;
				}
			}
			else if (mCompanionDatum.transform)
			{
				continue;
			}
			int num2 = -1;
			if (expand)
			{
				if (mCompanionDatum.IsExpanded())
				{
					num2 = mCompanionDatum.index;
				}
			}
			else if (mCompanionDatum.IsExpandBase() || !mCompanionDatum.HaveExpand())
			{
				num2 = mCompanionDatum.index;
			}
			if (num2 != -1 && mPlayer.IsCompanionUnlock(num2))
			{
				num++;
			}
		}
		return num;
	}

	public int getExpandedCompanionID(int aBaseID)
	{
		if (aBaseID >= mCompanionData.Count)
		{
			return -1;
		}
		CompanionData companionData = mCompanionData[aBaseID];
		CompanionData companionData2 = null;
		if (mPlayer.IsCompanionUnlock(aBaseID))
		{
			companionData2 = companionData;
		}
		if (companionData.HaveExpand())
		{
			for (int i = 0; i < mCompanionData.Count; i++)
			{
				CompanionData companionData3 = mCompanionData[i];
				if (companionData3.baseCharacterIndex == companionData.baseCharacterIndex && mPlayer.IsCompanionUnlock(companionData3.index) && companionData3.maxLevel > companionData.maxLevel && (companionData2 == null || companionData3.maxLevel > companionData2.maxLevel))
				{
					companionData2 = companionData3;
				}
			}
		}
		if (companionData2 == null)
		{
			return -1;
		}
		return companionData2.index;
	}

	public bool HaveBaseCompanion(int aID)
	{
		if (aID == -1 || aID >= mCompanionData.Count)
		{
			return false;
		}
		bool result = false;
		CompanionData companionData = mCompanionData[aID];
		if (companionData.IsExpanded())
		{
			do
			{
				int backCharacterIndex = companionData.backCharacterIndex;
				if (mPlayer.IsCompanionUnlock(backCharacterIndex))
				{
					result = true;
					break;
				}
				companionData = mCompanionData[backCharacterIndex];
			}
			while (!companionData.IsExpandBase());
		}
		return result;
	}

	public bool SucceedBaseCompanionLevel(int aID)
	{
		if (aID == -1 || aID >= mCompanionData.Count)
		{
			return false;
		}
		bool result = false;
		CompanionData companionData = mCompanionData[aID];
		if (companionData.IsExpanded())
		{
			int num = -1;
			do
			{
				int backCharacterIndex = companionData.backCharacterIndex;
				if (mPlayer.IsCompanionUnlock(backCharacterIndex))
				{
					num = mPlayer.GetCompanionLevel(backCharacterIndex);
					if (backCharacterIndex == mPlayer.Data.SelectedCompanion)
					{
						mCompanionIndex = aID;
						mPlayer.Data.SelectedCompanion = (short)mCompanionIndex;
						mPlayer.Data.LastUsedIcon = mCompanionIndex;
					}
					int num2 = mPlayer.Data.PlayerIcon;
					if (num2 >= 10000)
					{
						num2 -= 10000;
					}
					if (backCharacterIndex == num2)
					{
						if (mPlayer.Data.PlayerIcon >= 10000)
						{
							mPlayer.Data.PlayerIcon = aID + 10000;
						}
						else
						{
							mPlayer.Data.PlayerIcon = aID;
						}
					}
					break;
				}
				companionData = mCompanionData[backCharacterIndex];
			}
			while (!companionData.IsExpandBase());
			if (num > 0)
			{
				mPlayer.SetCompanionLevel(aID, num);
				result = true;
			}
		}
		return result;
	}

	public bool SetPartnerCollectionAttention(AccessoryData aAccessory)
	{
		bool flag = true;
		int gotIDByNum = aAccessory.GetGotIDByNum();
		CompanionData companionData = mCompanionData[gotIDByNum];
		if (companionData.HaveExpand())
		{
			int expandedCompanionID = getExpandedCompanionID(companionData.index);
			if (expandedCompanionID >= 0 && expandedCompanionID != gotIDByNum)
			{
				flag = false;
			}
		}
		if (flag)
		{
			mPlayer.SetCollectionNotify(aAccessory.Index);
		}
		else
		{
			mPlayer.CollectionNotified(aAccessory.Index);
		}
		return flag;
	}

	public void CorrectCompanionMaxLevel()
	{
		for (int i = 0; i < mCompanionData.Count; i++)
		{
			CompanionData companionData = mCompanionData[i];
			if (companionData.HaveExpand())
			{
				int expandedCompanionID = getExpandedCompanionID(companionData.index);
				if (expandedCompanionID != -1)
				{
					companionData = mCompanionData[expandedCompanionID];
				}
			}
			if (mPlayer.IsCompanionUnlock(companionData.index))
			{
				int companionLevel = mPlayer.GetCompanionLevel(companionData.index, false);
				if (companionLevel > companionData.maxLevel)
				{
					int num = companionLevel - companionData.maxLevel;
					mPlayer.SetCompanionLevel(companionData.index, companionData.maxLevel);
					int growUp = mPlayer.GrowUp;
					mPlayer.AddGrowup(num);
					int growUp2 = mPlayer.GrowUp;
					int decision_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
					ServerCram.LevelRaiseCheck(companionData.index, companionLevel, companionData.maxLevel, growUp, growUp2, decision_time);
				}
			}
		}
	}

	public void LoadDownloadSkillEffectData()
	{
		if (_skillEffectData == null)
		{
			_skillEffectData = new List<SkillEffectData>();
		}
		else
		{
			_skillEffectData.Clear();
		}
		BIJBinaryReader bIJBinaryReader = null;
		if (mDLManager != null)
		{
			bIJBinaryReader = mDLManager.GetDownloadFileReadStream("SkillEffect_Ex.bin.bytes");
		}
		if (bIJBinaryReader == null)
		{
			bIJBinaryReader = new BIJResourceBinaryReader("Data/SkillEffect.bin");
		}
		if (bIJBinaryReader == null)
		{
			return;
		}
		short num = bIJBinaryReader.ReadShort();
		for (short num2 = 0; num2 < num; num2++)
		{
			SkillEffectData skillEffectData = new SkillEffectData();
			skillEffectData.deserialize(bIJBinaryReader);
			int index = skillEffectData.index;
			if (index < _skillEffectData.Count)
			{
				_skillEffectData[index] = skillEffectData;
			}
			else
			{
				while (_skillEffectData.Count < index)
				{
					SkillEffectData skillEffectData2 = _skillEffectData[0].Clone();
					skillEffectData2.index = (short)_skillEffectData.Count;
					_skillEffectData.Add(skillEffectData2);
				}
				_skillEffectData.Add(skillEffectData);
			}
		}
		bIJBinaryReader.Close();
	}

	public void ReloadSkillEffectData(bool aForce = false)
	{
		if (_skillEffectData != null || aForce)
		{
			LoadDownloadSkillEffectData();
		}
	}

	public void LoadDownloadStoryDemoData()
	{
		if (_storyDemoData == null)
		{
			_storyDemoData = new List<StoryDemoData>();
		}
		else
		{
			_storyDemoData.Clear();
		}
		BIJBinaryReader bIJBinaryReader = null;
		if (mDLManager != null)
		{
			bIJBinaryReader = mDLManager.GetDownloadFileReadStream("StoryDemoData_Ex.bin.bytes");
		}
		if (bIJBinaryReader == null)
		{
			bIJBinaryReader = new BIJResourceBinaryReader("Data/StoryDemoData.bin");
		}
		if (bIJBinaryReader == null)
		{
			return;
		}
		short num = bIJBinaryReader.ReadShort();
		for (short num2 = 0; num2 < num; num2++)
		{
			StoryDemoData storyDemoData = new StoryDemoData();
			storyDemoData.deserialize(bIJBinaryReader);
			int index = storyDemoData.index;
			if (index < _storyDemoData.Count)
			{
				_storyDemoData[index] = storyDemoData;
			}
			else
			{
				while (_storyDemoData.Count < index)
				{
					StoryDemoData storyDemoData2 = _storyDemoData[0].Clone();
					storyDemoData2.index = (short)_storyDemoData.Count;
					_storyDemoData.Add(storyDemoData2);
				}
				_storyDemoData.Add(storyDemoData);
			}
		}
		bIJBinaryReader.Close();
	}

	public void ReloadStoryDemoData(bool aForce = false)
	{
		if (_storyDemoData != null || aForce)
		{
			LoadDownloadStoryDemoData();
		}
	}

	public void LoadDownloadVisualCollectionData()
	{
		if (_visualCollectionData == null)
		{
			_visualCollectionData = new List<VisualCollectionData>();
		}
		else
		{
			_visualCollectionData.Clear();
		}
		BIJBinaryReader bIJBinaryReader = null;
		if (mDLManager != null)
		{
			bIJBinaryReader = mDLManager.GetDownloadFileReadStream("VisualCollection_Ex.bin.bytes");
		}
		if (bIJBinaryReader == null)
		{
			bIJBinaryReader = new BIJResourceBinaryReader("Data/VisualCollection.bin");
		}
		if (bIJBinaryReader == null)
		{
			return;
		}
		short num = bIJBinaryReader.ReadShort();
		for (short num2 = 0; num2 < num; num2++)
		{
			VisualCollectionData visualCollectionData = new VisualCollectionData();
			visualCollectionData.deserialize(bIJBinaryReader);
			int index = visualCollectionData.index;
			if (index < _visualCollectionData.Count)
			{
				_visualCollectionData[index] = visualCollectionData;
			}
			else
			{
				while (_visualCollectionData.Count < index)
				{
					VisualCollectionData visualCollectionData2 = _visualCollectionData[0].Clone();
					visualCollectionData2.index = (short)_visualCollectionData.Count;
					_visualCollectionData.Add(visualCollectionData2);
				}
				_visualCollectionData.Add(visualCollectionData);
			}
		}
		bIJBinaryReader.Close();
	}

	public void ReloadVisualCollectionData(bool aForce = false)
	{
		if (_visualCollectionData != null || aForce)
		{
			LoadDownloadVisualCollectionData();
		}
	}

	public void SetAutoPopupStage(int a_stage)
	{
		AutoPopupStageNo = a_stage;
	}

	public void ResetAutoPopupStage()
	{
		AutoPopupStageNo = -1;
	}

	public void ClearPresentBox()
	{
		mPresentBoxList.Clear();
	}

	public void RemovePresentBox(int index)
	{
		AccessoryData item = mPresentBoxList[index];
		mPresentBoxList.Remove(item);
	}

	public void RemovePresentBox(AccessoryData box)
	{
		mPresentBoxList.Remove(box);
	}

	public int GetPresentBoxIndex(AccessoryData box)
	{
		return mPresentBoxList.IndexOf(box);
	}

	public int GetPresentBoxNum()
	{
		return mPresentBoxList.Count;
	}

	public AccessoryData GetPresentBox(int index)
	{
		return mPresentBoxList[index];
	}

	public void SetFriendHelp(GiftItem a_item)
	{
		if (a_item == null)
		{
			PlayingFriendHelp = null;
			return;
		}
		PlayingFriendHelp = new FriendHelpPlay();
		PlayingFriendHelp.Gift = a_item;
		PlayingFriendHelp.IsStageCleared = false;
	}

	public void CheckDevelopmentServer()
	{
		if (SingletonMonoBehaviour<Network>.Instance.AppBase_URL.CompareTo(DevelopmentServerURL) == 0)
		{
			mDebugUseDevelopmentServer = true;
		}
		else
		{
			mDebugUseDevelopmentServer = false;
		}
	}

	public void InitGameStateArg()
	{
		GlobalVariables.Reset();
		ClearCampaignData();
		mLoginBonusData.Reset();
		DeleteLocalFile(Constants.LOGINBONUS_BACKUP_FILE);
	}

	public GameStateManager.GAME_STATE GetNextGameState()
	{
		GameStateManager.GAME_STATE result = GameStateManager.GAME_STATE.MAP;
		SMEventPageData currentEventPageData = GetCurrentEventPageData();
		if (currentEventPageData != null)
		{
			switch (currentEventPageData.EventSetting.EventType)
			{
			case Def.EVENT_TYPE.SM_RALLY:
				result = GameStateManager.GAME_STATE.EVENT_RALLY;
				break;
			case Def.EVENT_TYPE.SM_STAR:
				result = GameStateManager.GAME_STATE.EVENT_STAR;
				break;
			case Def.EVENT_TYPE.SM_RALLY2:
				result = GameStateManager.GAME_STATE.EVENT_RALLY2;
				break;
			case Def.EVENT_TYPE.SM_BINGO:
				result = GameStateManager.GAME_STATE.EVENT_BINGO;
				break;
			case Def.EVENT_TYPE.SM_ADV:
				result = GameStateManager.GAME_STATE.ADVMENU;
				break;
			case Def.EVENT_TYPE.SM_LABYRINTH:
				result = GameStateManager.GAME_STATE.EVENT_LABYRINTH;
				break;
			}
		}
		return result;
	}

	public void EnterADV()
	{
		mPlayer.SetAdventure();
		mGameStateManager.SetNextGameState(GameStateManager.GAME_STATE.ADVMENU);
		LinkBannerList_Clear();
		LinkBannerManager.Instance.ClearLinkBanner(mLBRequest);
		mPlayer.AdvSaveData.PlayCount++;
		int playspan;
		CountAdvPlayerBoot(out playspan);
		ServerCram.SendAdvDau();
	}

	public void CreateDLError(ResourceDLErrorInfo.ERROR_CODE a_code, ResourceDLErrorInfo.PLACE a_place, string a_message)
	{
		if (DLErrorInfo == null)
		{
			DLErrorInfo = new ResourceDLErrorInfo(null, a_code, a_place, string.Empty, a_message);
		}
	}

	public void CreateDLError(BIJDLData a_data, ResourceDLErrorInfo.ERROR_CODE a_code, ResourceDLErrorInfo.PLACE a_place, string a_title, string a_message)
	{
		if (DLErrorInfo == null)
		{
			DLErrorInfo = new ResourceDLErrorInfo(a_data, a_code, a_place, a_title, a_message);
		}
	}

	public void CreateDLError(BIJDLData a_data, ResourceDLErrorInfo.ERROR_CODE a_code, ResourceDLErrorInfo.PLACE a_place, string a_title, Exception a_e)
	{
		if (DLErrorInfo == null)
		{
			DLErrorInfo = new ResourceDLErrorInfo(a_data, a_code, a_place, a_title, a_e);
		}
	}

	public void ResetDLErrorInfo()
	{
		DLErrorInfo = null;
	}

	public void SetDLErrorName(string a_name)
	{
		DLErrorInfo.Name = a_name;
	}

	public bool Server_DLFilelistCheck(ResourceDownloadManager.KIND aKind)
	{
		if (USE_RESOURCEDL)
		{
			mDLFilelistCheckDone = false;
			mDLFilelistKind = aKind;
			if (!Server.DLFilelistCheck(aKind))
			{
				mDLFilelistCheckDone = true;
				return false;
			}
		}
		return true;
	}

	private void Server_OnDLFilelistCheck(int result, Stream resStream)
	{
		if (result == 0)
		{
			string text = ResourceDownloadManager.getResourceDLPath(mDLFilelistKind);
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			bool flag = false;
			try
			{
				int num = 0;
				text += Constants.RESOURCEDL_SERVERFILE;
				byte[] array = new byte[4096];
				using (FileStream output = new FileStream(text, FileMode.Create))
				{
					using (BinaryWriter binaryWriter = new BinaryWriter(output))
					{
						while ((num = resStream.Read(array, 0, array.Length)) > 0)
						{
							binaryWriter.Write(array, 0, num);
						}
					}
				}
				flag = true;
			}
			catch (Exception)
			{
			}
			if (flag)
			{
				mDLManager.LoadLocalDLFilelist(mDLFilelistKind, text);
			}
		}
		mDLFilelistCheckDone = true;
	}

	public static void LockScreenRotate()
	{
		if (mPrevOrientation == ScreenOrientation.Unknown)
		{
			mPrevOrientation = Util.ScreenOrientation;
			mAutoRotateLL = Screen.autorotateToLandscapeLeft;
			mAutoRotateLR = Screen.autorotateToLandscapeRight;
			mAutoRotateP = Screen.autorotateToPortrait;
			mAutoRotatePU = Screen.autorotateToPortraitUpsideDown;
			switch (Util.ScreenOrientation)
			{
			case ScreenOrientation.Portrait:
				Util.ScreenOrientation = ScreenOrientation.Portrait;
				break;
			case ScreenOrientation.PortraitUpsideDown:
				Util.ScreenOrientation = ScreenOrientation.PortraitUpsideDown;
				break;
			case ScreenOrientation.LandscapeLeft:
				Util.ScreenOrientation = ScreenOrientation.LandscapeLeft;
				break;
			case ScreenOrientation.LandscapeRight:
				Util.ScreenOrientation = ScreenOrientation.LandscapeRight;
				break;
			}
		}
	}

	public static void UnlockScreenRotate()
	{
		if (mPrevOrientation != 0)
		{
			if (mAutoRotateLL | mAutoRotateLR | mAutoRotateP | mAutoRotatePU)
			{
				Util.ScreenOrientation = ScreenOrientation.AutoRotation;
				Screen.autorotateToLandscapeLeft = mAutoRotateLL;
				Screen.autorotateToLandscapeRight = mAutoRotateLR;
				Screen.autorotateToPortrait = mAutoRotateP;
				Screen.autorotateToPortraitUpsideDown = mAutoRotatePU;
			}
			else
			{
				Util.ScreenOrientation = mPrevOrientation;
			}
			mPrevOrientation = ScreenOrientation.Unknown;
		}
	}

	public void CheckScreenRotate()
	{
		if (mScreenOrientation == Util.ScreenOrientation)
		{
			return;
		}
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			mRoot.fitHeight = true;
			mRoot.fitWidth = false;
			mDeviceRatioScale = 1f;
			mDeviceOrientaion = ScreenOrientation.Portrait;
		}
		else
		{
			if (Util.ScreenOrientation != ScreenOrientation.LandscapeLeft && Util.ScreenOrientation != ScreenOrientation.LandscapeRight)
			{
				return;
			}
			mRoot.fitHeight = false;
			mRoot.fitWidth = true;
			mDeviceRatioScale = (float)mRoot.manualWidth / 950f;
			mDeviceOrientaion = ScreenOrientation.LandscapeLeft;
		}
		UpdateSignRootTransform();
		mGameStateManager.OnScreenChanged(Util.ScreenOrientation);
		mScreenOrientation = Util.ScreenOrientation;
	}

	public void SetUserAuthFromResponse()
	{
		mUserAuthSucceed = true;
		int uUID = mPlayer.UUID;
		int hiveId = mPlayer.HiveId;
		string searchID = mPlayer.SearchID;
		mPlayer.Data.LastGetAuthTime = DateTime.Now;
		int uUID2 = mUserUniqueID.UUID;
		int hiveID = mUserUniqueID.HiveID;
		int status = mUserUniqueID.Status;
		if (uUID2 == 0)
		{
			ServerCram.CheatUserCuu();
		}
		else if (mPlayer.UUID == 0)
		{
			mPlayer.SetUUID(uUID2);
		}
		else if (mPlayer.UUID != uUID2)
		{
			mPlayer.SetUUID(uUID2);
			mUserAuthChanged = true;
		}
		if (hiveID != 0)
		{
			if (mPlayer.HiveId == 0)
			{
				mPlayer.HiveId = hiveID;
			}
			else if (mPlayer.HiveId != hiveID)
			{
				mPlayer.HiveId = hiveID;
			}
		}
		if (status == 2 && mUserAuthChanged)
		{
			mUserAuthTransfered = true;
			ServerCramVariableData.mLocalUUID = uUID;
			ServerCramVariableData.mLocalHiveId = hiveId;
			ServerCramVariableData.mLocalSearchID = searchID;
		}
	}

	public void SetGameProfileFromResponse()
	{
		mPlayer.Data.LastGetGameInfoTime = DateTime.Now;
		mGameProfileSucceed = true;
		if (mGameProfile.AppVer > 1290)
		{
			UpdateVersionFlag = mGameProfile.UpdateVersionFlag;
		}
		if (mGameProfile.Timeout > 0)
		{
			Server.Timeout = mGameProfile.Timeout;
			ServerCram.Timeout = mGameProfile.Timeout;
			ServerIAP.Timeout = mGameProfile.Timeout;
		}
		if (mGameProfile.ReqTimeout > 0)
		{
			Network.Timeout = mGameProfile.ReqTimeout;
		}
		if (mGameProfile.IOTimeout > 0)
		{
			Network.IOTimeout = mGameProfile.IOTimeout;
		}
		UpdateEnableTransferFlag();
		TransferMode = mNetworkProfile.trans_mode;
		TransferText = mNetworkProfile.trans_text;
		TransferURL = mNetworkProfile.trans_url;
		if (ALWAYS_LOG)
		{
			BIJLog.Instance.LogLevel = BIJLog.Level.DEBUG;
		}
		else
		{
			BIJLog.Level logLevel = BIJLog.Level.INFO;
			if (Enum.IsDefined(typeof(BIJLog.Level), mGameProfile.LogLevelNum))
			{
				try
				{
					logLevel = (BIJLog.Level)(int)Enum.ToObject(typeof(BIJLog.Level), mGameProfile.LogLevelNum);
				}
				catch (Exception)
				{
					logLevel = BIJLog.Level.INFO;
				}
			}
			BIJLog.Instance.LogLevel = logLevel;
		}
		if (mOptions.LBCacheDeleteID != mGameProfile.LBCacheDelete)
		{
			LinkBannerManager.Instance.DeleteCache();
			mOptions.LBCacheDeleteID = mGameProfile.LBCacheDelete;
			SaveOptions();
		}
		mEventProfile.ConvertAdvGuerrillaSetting();
	}

	public void SetSegmentProfileFromResponse()
	{
		mPlayer.Data.LastGetSegmentInfoTime = DateTime.Now;
		mSegmentProfileSucceed = true;
		mSegmentProfileErrorCode = Server.ErrorCode.SUCCESS;
		List<ShopLimitedTime> list = new List<ShopLimitedTime>();
		List<ShopLimitedTime> list2 = new List<ShopLimitedTime>();
		List<ShopLimitedTime> list3 = new List<ShopLimitedTime>();
		for (int i = 0; i < mSegmentProfile.ShopItemList.Count; i++)
		{
			if (mSegmentProfile.ShopItemList[i].MugenHeartItem == 1)
			{
				list2.Add(mSegmentProfile.ShopItemList[i]);
				continue;
			}
			if (mSegmentProfile.ShopItemList[i].AllCrushItem == 1)
			{
				list3.Add(mSegmentProfile.ShopItemList[i]);
				continue;
			}
			int shopItemID = mSegmentProfile.ShopItemList[i].ShopItemID;
			if (mShopItemData.ContainsKey(shopItemID))
			{
				ShopItemData shopItemData = mShopItemData[shopItemID];
				bool flag = false;
				if (shopItemData.IsSale)
				{
					list.Add(mSegmentProfile.ShopItemList[i]);
				}
			}
		}
		mSegmentProfile.ShopSetList(list);
		mSegmentProfile.ShopShopMugenHeartItemList(list2);
		mSegmentProfile.SetShopAllCrushItemList(list3);
	}

	public void SetNicknameCheckFromResponse()
	{
		bool flag = false;
		string empty = string.Empty;
		int playerIcon = mPlayer.Data.PlayerIcon;
		int num = playerIcon;
		if (num >= 10000)
		{
			num -= 10000;
		}
		int companionLevel = mPlayer.GetCompanionLevel(num);
		int seriesToS = mPlayer.SeriesToS;
		int num2 = -1;
		if (!Def.IsAdvSeries((Def.SERIES)seriesToS) && !Def.IsEventSeries((Def.SERIES)seriesToS))
		{
			PlayerMapData data;
			mPlayer.Data.GetMapData((Def.SERIES)seriesToS, out data);
			num2 = data.OpenNoticeLevel;
		}
		mNickNameSucceed = true;
		if (!string.IsNullOrEmpty(mUserProfile.NickName))
		{
			if (string.IsNullOrEmpty(mPlayer.NickName))
			{
				mPlayer.NickName = ((!string.IsNullOrEmpty(mUserProfile.NickName)) ? mUserProfile.NickName : string.Empty);
			}
			else if (string.Compare(mPlayer.NickName, mUserProfile.NickName) != 0)
			{
				mPlayer.NickName = mUserProfile.NickName;
			}
		}
		if (mUserProfile.IconID != playerIcon)
		{
			flag = true;
		}
		if (mUserProfile.IconLvl != companionLevel)
		{
			flag = true;
		}
		if (!string.IsNullOrEmpty(mUserProfile.SearchID))
		{
			mPlayer.SearchID = mUserProfile.SearchID;
		}
		if (num2 != -1 && flag && !mUserAuthTransfered)
		{
			Network_NicknameEdit(string.Empty, playerIcon, companionLevel, seriesToS, num2);
		}
	}

	public void SetMaintenanceProfileFromResponse()
	{
		if (mPlayer != null)
		{
			mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnixTimeStampToDateTime(mMaintenanceProfile.ServerTime, true);
		}
		mMaintenanceProfileSucceed = true;
		ServerTime = DateTimeUtil.UnixTimeStampToDateTime(mMaintenanceProfile.ServerTime, true);
		IsMedalRecover = true;
		if (mDebugStopGemSale)
		{
			mMaintenanceProfile.GemSale = 0;
		}
		if (mDebugServiceFinish)
		{
			mMaintenanceProfile.ServiceFinish = 1;
			mMaintenanceProfile.ServiceFinishUrl = mDebugServiceFinishURL;
		}
		if (Debug_MaintenanceModeFlg)
		{
			if (Debug_MaintenanceModeCount == 1)
			{
				MaintenanceMode = true;
			}
			else
			{
				Debug_MaintenanceModeCount = 1;
				MaintenanceMode = mMaintenanceProfile.IsMaintenanceMode;
			}
		}
		else
		{
			MaintenanceMode = mMaintenanceProfile.IsMaintenanceMode;
		}
		MaintenanceMessage = mMaintenanceProfile.MaintenanceText;
		MaintenanceURL = mMaintenanceProfile.MaintenanceUrl;
		if (Debug_AdvMaintenanceMode)
		{
			Adv_MaintenanceMode = true;
		}
		else
		{
			Adv_MaintenanceMode = mMaintenanceProfile.IsAdvMaintenanceMode;
		}
	}

	public void SetBackupPath()
	{
		string text = Path.Combine(Application.persistentDataPath, Constants.BACKUP_FILE);
		if (File.Exists(text))
		{
			BackupPath = text;
			return;
		}
		text = Path.Combine(Application.persistentDataPath, Constants.BACKUP_FILE_OLD);
		if (File.Exists(text))
		{
			BackupPath = text;
		}
		else
		{
			BackupPath = string.Empty;
		}
	}

	public void SetGetGameDataFromResponse(Stream resStream, int dataVersion)
	{
		if (!string.IsNullOrEmpty(BackupPath) && File.Exists(BackupPath))
		{
			File.Delete(BackupPath);
		}
		try
		{
			int num = 0;
			byte[] array = new byte[4096];
			if (dataVersion >= 1100)
			{
				BackupPath = Path.Combine(Application.persistentDataPath, Constants.BACKUP_FILE);
			}
			else
			{
				BackupPath = Path.Combine(Application.persistentDataPath, Constants.BACKUP_FILE_OLD);
			}
			using (FileStream output = new FileStream(BackupPath, FileMode.Create))
			{
				using (BinaryWriter binaryWriter = new BinaryWriter(output))
				{
					while ((num = resStream.Read(array, 0, array.Length)) > 0)
					{
						binaryWriter.Write(array, 0, num);
					}
				}
			}
		}
		catch (Exception e)
		{
			BIJLog.E("GetGameState error.", e);
		}
		mGetGameDataSucceed = true;
	}

	public bool Network_ConnectionEstablish()
	{
		if (NO_NETWORK)
		{
			return true;
		}
		if (IsFirstConnection)
		{
			return true;
		}
		IsFirstConnection = true;
		if (!Server.ConnectionEstablish())
		{
			return false;
		}
		return true;
	}

	public bool Network_NicknameEdit(string a_editname)
	{
		Player player = mPlayer;
		int a_iconlvl = 0;
		if (player.Data.PlayerIcon >= 0)
		{
			int num = player.Data.PlayerIcon;
			if (num >= 10000)
			{
				num -= 10000;
			}
			a_iconlvl = player.GetCompanionLevel(num);
		}
		PlayerMapData data;
		player.Data.GetMapData((Def.SERIES)player.SeriesToS, out data);
		int openNoticeLevel = data.OpenNoticeLevel;
		return Network_NicknameEdit(a_editname, player.Data.PlayerIcon, a_iconlvl, player.SeriesToS, openNoticeLevel);
	}

	public bool Network_NicknameEdit(string a_editname, int a_icon, int a_iconlvl, int a_series, int a_stage, int a_emoid = 0)
	{
		NicknameEditSuccess = false;
		NicknameEditCheckDone = false;
		NicknameEditErrorCode = Server.ErrorCode.SUCCESS;
		bool flag = Server.NicknameEdit(a_editname, a_icon, a_iconlvl, a_series, a_stage, a_emoid);
		if (!flag)
		{
			NicknameEditCheckDone = true;
			NicknameEditErrorCode = Server.ErrorCode.OTHER;
		}
		return flag;
	}

	private void Server_OnNicknameEdit(int result, int code)
	{
		if (result == 0)
		{
			NicknameEditSuccess = true;
		}
		else
		{
			NicknameEditErrorCode = (Server.ErrorCode)code;
		}
		NicknameEditCheckDone = true;
	}

	private void OnMaintenanceDialogClosedRetry(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		mMaintenanceRetryFlg = true;
	}

	private void OnMaintenanceDialogClosedToTitle(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		IsTitleReturnMaintenance = true;
		StopMusic(0, 0.5f);
		mGameStateManager.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
	}

	private void MaintenanceModeDialogCheck(ConnectBase a_target)
	{
		if (mMaintenanceRetryFlg)
		{
			if (mPlayer != null)
			{
				mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			}
			a_target.StartConnect();
		}
	}

	public bool CheckMaintenanceModeToTitle(ConnectBase a_target)
	{
		bool result = false;
		mMaintenanceRetryFlg = false;
		if (MaintenanceMode)
		{
			a_target.SetResultWaitCallback(MaintenanceModeDialogCheck);
			CreateMaintenanceDialog(ConnectCommonErrorDialog.STYLE.GOTO_TITLE, OnMaintenanceDialogClosedToTitle);
			result = true;
		}
		return result;
	}

	public bool CheckMaintenanceModeInfinitRetry(ConnectBase a_target)
	{
		bool result = false;
		mMaintenanceRetryFlg = false;
		if (MaintenanceMode)
		{
			a_target.SetResultWaitCallback(MaintenanceModeDialogCheck);
			CreateMaintenanceDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY, OnMaintenanceDialogClosedRetry);
			result = true;
		}
		return result;
	}

	public ConnectCommonErrorDialog CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE a_case, int a_state, OnConnectRetry a_callback, OnConnectAbandon a_abandon)
	{
		if (mConnectErrorDialog != null)
		{
			UnityEngine.Object.Destroy(mConnectErrorDialog.gameObject);
			mConnectErrorDialog = null;
		}
		string empty = string.Empty;
		string empty2 = string.Empty;
		mConnectErrorState = a_state;
		mConnectRetry = a_callback;
		mConnectAbandon = a_abandon;
		RetryCount++;
		mConnectErrorDialog = Util.CreateGameObject("ConnectErrorDialog", mAnchor.gameObject).AddComponent<ConnectCommonErrorDialog>();
		mConnectErrorDialog.Init(empty, empty2, a_case, RetryCount, mConnectErrorState);
		mConnectErrorDialog.SetClosedCallback(OnConectErrorDialogClosed);
		return mConnectErrorDialog;
	}

	private void OnConectErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		if (i == ConnectCommonErrorDialog.SELECT_ITEM.RETRY)
		{
			if (mConnectRetry != null)
			{
				mConnectRetry(mConnectErrorState);
			}
		}
		else
		{
			RetryCount = 0;
			if (mConnectAbandon != null)
			{
				mConnectAbandon();
			}
		}
		mConnectErrorDialog = null;
	}

	public void CreateMaintenanceDialog(ConnectCommonErrorDialog.STYLE i, ConnectCommonErrorDialog.OnDialogClosed callback)
	{
		ConnectCommonErrorDialog connectCommonErrorDialog = Util.CreateGameObject("MaintenanceDialog", mAnchor.gameObject).AddComponent<ConnectCommonErrorDialog>();
		connectCommonErrorDialog.Init(string.Empty, string.Empty, i);
		connectCommonErrorDialog.SetClosedCallback(callback);
	}

	public void CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE i, ConnectCommonErrorDialog.OnDialogClosed callback, int state)
	{
		ConnectCommonErrorDialog connectCommonErrorDialog = Util.CreateGameObject("MaintenanceCheckErrorDialog", mAnchor.gameObject).AddComponent<ConnectCommonErrorDialog>();
		connectCommonErrorDialog.Init(string.Empty, string.Empty, i, RetryCount, state);
		connectCommonErrorDialog.SetClosedCallback(callback);
	}

	public bool Network_UserAuth()
	{
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetAuthTime, DateTime.Now);
		long gET_AUTHUUID_FREQ = GET_AUTHUUID_FREQ;
		if (gET_AUTHUUID_FREQ > 0 && num < gET_AUTHUUID_FREQ)
		{
			mUserAuthCheckDone = true;
			mUserAuthSucceed = true;
			return true;
		}
		if (NO_NETWORK)
		{
			mUserUniqueID = new UserAuthResponse();
			mUserUniqueID.UUID = 100;
			mUserUniqueID.HiveID = 83;
			Server_OnUserAuth(0, 0);
			return true;
		}
		mUserAuthSucceed = false;
		mUserAuthCheckDone = false;
		mUserAuthChanged = false;
		mUserAuthErrorCode = Server.ErrorCode.SUCCESS;
		if (!Server.UserAuthCheck())
		{
			mUserAuthErrorCode = Server.ErrorCode.OTHER;
			mUserAuthCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnUserAuth(int result, int code)
	{
		if (result == 0)
		{
			SetUserAuthFromResponse();
		}
		else
		{
			mPlayer.Data.LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
			mUserAuthErrorCode = (Server.ErrorCode)code;
		}
		mUserAuthCheckDone = true;
	}

	public bool Network_GetGameProfile(string a_datekey = "", string a_debugkey = "")
	{
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetGameInfoTime, DateTime.Now);
		long gET_GAME_INFO_FREQ = GET_GAME_INFO_FREQ;
		if (gET_GAME_INFO_FREQ > 0 && num < gET_GAME_INFO_FREQ)
		{
			mGameProfileCheckDone = true;
			mGameProfileSucceed = true;
			return true;
		}
		if (NO_NETWORK)
		{
			mGameProfile = new GameProfile();
			Server_OnGameProfile(0, 0);
			return true;
		}
		mGameProfileSucceed = false;
		mGameProfileCheckDone = false;
		UpdateVersionFlag = GameProfile.UpdateKind.NONE;
		if (!Server.GetGameInfo(a_datekey, a_debugkey))
		{
			mGameProfileCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnGameProfile(int result, int code)
	{
		if (result == 0)
		{
			SetGameProfileFromResponse();
		}
		else
		{
			mPlayer.Data.LastGetGameInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mGameProfileCheckDone = true;
	}

	public bool Network_GetSegmentProfile(string a_datekey = "", string a_debugkey = "")
	{
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetSegmentInfoTime, DateTime.Now);
		long gET_SEGMENT_INFO_FREQ = GET_SEGMENT_INFO_FREQ;
		if (gET_SEGMENT_INFO_FREQ > 0 && num < gET_SEGMENT_INFO_FREQ)
		{
			mSegmentProfileCheckDone = true;
			mSegmentProfileSucceed = true;
			return false;
		}
		if (NO_NETWORK)
		{
			mSegmentProfile = new SegmentProfile();
			Server_OnSegmentProfile(0, 0);
			SetDebugSegmentInfoNoNetwork();
			return true;
		}
		mSegmentProfileSucceed = false;
		mSegmentProfileCheckDone = false;
		if (!Server.GetSegmentInfo(a_datekey, a_debugkey))
		{
			mSegmentProfileCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnSegmentProfile(int result, int code)
	{
		if (result == 0)
		{
			SetSegmentProfileFromResponse();
		}
		else
		{
			mPlayer.Data.LastGetSegmentInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			mSegmentProfileErrorCode = (Server.ErrorCode)code;
		}
		mSegmentProfileCheckDone = true;
	}

	public void SetDebugSegmentInfoNoNetwork()
	{
		mSegmentProfile.HeartItem = 9;
		mSegmentProfile.RoadBlockItem = 16;
		mSegmentProfile.ContinueItemList = new List<int> { 12, 13, 14, 15 };
		mSegmentProfile.AdvMedalItem = 64;
		mSegmentProfile.AdvContinueItem = 65;
		int[] array = new int[6] { 9, 12, 13, 14, 15, 16 };
		for (int i = 0; i < array.Length; i++)
		{
			ShopLimitedTime item = new ShopLimitedTime(0L, 0L, array[i], 0, 0, 0, 0, 0);
			mSegmentProfile.ShopItemList.Add(item);
		}
	}

	public bool Network_NicknameCheck()
	{
		if (NO_NETWORK)
		{
			mUserProfile = new UserGetResponse();
			mUserProfile.NickName = "DUMMY_PLUG";
			mUserProfile.IconID = 0;
			mUserProfile.IconLvl = 1;
			mUserProfile.SearchID = "DUMMY";
			Server_OnNicknameCheck(0, 0);
			return true;
		}
		mNickNameSucceed = false;
		mNickNameCheckDone = false;
		if (!Server.NicknameCheck())
		{
			mNickNameCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnNicknameCheck(int result, int code)
	{
		if (result == 0)
		{
			SetNicknameCheckFromResponse();
		}
		else
		{
			NickNameErrorCode = (Server.ErrorCode)code;
		}
		mNickNameCheckDone = true;
	}

	public bool Network_GetMaintenanceProfile(string a_datekey = "", string a_debugkey = "")
	{
		if (mPlayer != null)
		{
			if (Debug_MaintenanceModeFlg && Debug_MaintenanceModeCount == 1)
			{
				mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			}
			if (DateTime.Now.AddSeconds(mServerTimeDiff * -1.0) <= mPlayer.Data.LastGetMaintenanceInfoTime)
			{
				mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			}
			long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetMaintenanceInfoTime, DateTime.Now.AddSeconds(mServerTimeDiff * -1.0));
			long gET_MAINTENANCE_INFO_FREQ = GET_MAINTENANCE_INFO_FREQ;
			if (gET_MAINTENANCE_INFO_FREQ > 0 && num < gET_MAINTENANCE_INFO_FREQ)
			{
				if (Debug_MaintenanceModeFlg && Debug_MaintenanceModeCount == 0)
				{
					Debug_MaintenanceModeCount = 1;
				}
				mMaintenanceProfileSucceed = true;
				mMaintenanceProfileCheckDone = true;
				return true;
			}
		}
		if (NO_NETWORK)
		{
			mMaintenanceProfile = new MaintenanceProfile();
			Server_OnMaintenanceProfile(0, 0);
			mMaintenanceProfile.ServerTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(DateTime.Now, true);
			return true;
		}
		mMaintenanceProfileSucceed = false;
		mMaintenanceProfileCheckDone = false;
		ResetMaintenance();
		if (!Server.GetMaintenanceInfo(a_datekey, a_debugkey))
		{
			mMaintenanceProfileCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnMaintenanceProfile(int result, int code)
	{
		if (result == 0)
		{
			SetMaintenanceProfileFromResponse();
		}
		else if (mPlayer != null)
		{
			mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mMaintenanceProfileCheckDone = true;
	}

	private void Server_OnSendDeviceToken(int result, int code)
	{
		if (result != 0)
		{
		}
	}

	private bool Network_SaveGameState(BIJBinaryReader dataReader, BIJBinaryReader metaReader, bool force, bool want)
	{
		mSaveGameStateDone = false;
		mSaveGameDataSucceed = false;
		if (NO_NETWORK)
		{
			Server_OnSaveGameState(0, 0);
			return true;
		}
		Dictionary<string, object> overwrites = null;
		if (mPlayer != null)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("tscore", mPlayer.GetRankingTotalScore());
			dictionary.Add("tstar", mPlayer.GetRankingTotalStar());
			overwrites = dictionary;
		}
		if (!Server.SaveGameState(dataReader, metaReader, force, want, overwrites))
		{
			mSaveGameStateDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnSaveGameState(int result, int code)
	{
		if (result == 0)
		{
			bool flag = false;
			if (!mOptions.ServerAlreadeyFirstSavedFlag)
			{
				flag = true;
			}
			bool serverForceSaveRequestByBackup = mOptions.ServerForceSaveRequestByBackup;
			mOptions.ServerAlreadeyFirstSavedFlag = true;
			mOptions.ServerForceSaveRequestByBackup = false;
			if (serverForceSaveRequestByBackup)
			{
				SaveOptions();
				mNeedBackupCheck = false;
			}
			else if (flag)
			{
				SaveOptions();
			}
			mSaveGameDataSucceed = true;
		}
		mSaveGameStateDone = true;
	}

	public bool Network_GetGameData()
	{
		int uUID = mPlayer.UUID;
		int hiveId = mPlayer.HiveId;
		return Network_GetGameData(false, hiveId, uUID);
	}

	private bool Network_GetGameData(bool usehive, int hiveId, int uuid)
	{
		if (NO_NETWORK)
		{
			mGetGameDataSucceed = true;
			mGetGameDataCheckDone = true;
			return true;
		}
		SetBackupPath();
		mGetGameDataCheckDone = false;
		mGetGameDataSucceed = false;
		if (!Server.GetServerGameData(usehive, hiveId, uuid))
		{
			mGetGameDataCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnGetGameData(int result, int code, Stream resStream, int dataVersion)
	{
		if (result == 0)
		{
			SetGetGameDataFromResponse(resStream, dataVersion);
		}
		mGetGameDataCheckDone = true;
	}

	public bool Network_SetStageClearData(int series, int _stage, int hiveId, int uuid, string dispname, IStageInfo _info, int eventID, short courseID, bool ForceOverwrite = false)
	{
		bool flag = false;
		if (mEventMode)
		{
			return Server.SetEventStageClearData(_stage, hiveId, uuid, dispname, _info, eventID, courseID);
		}
		return Server.SetStageClearData(series, _stage, hiveId, uuid, dispname, _info, ForceOverwrite);
	}

	private void Server_OnSetStageClearData(int result, int code)
	{
		if (result != 0)
		{
		}
	}

	public bool Network_LoginBonus()
	{
		DateTime tdt = DateTime.Now;
		if (ServerTime.HasValue)
		{
			tdt = ServerTime.Value;
		}
		if (USE_DEBUG_DIALOG && mDebugUseFakeServerTime && DebugFakeServerTime.HasValue)
		{
			tdt = DebugFakeServerTime.Value;
		}
		long num = DateTimeUtil.BetweenDays0(mPlayer.Data.LastLoginBonusTime, tdt);
		if (num <= 0)
		{
			long num2 = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastLoginBonusTime, DateTime.Now);
			long gET_LOGINBONUS_FREQ = GET_LOGINBONUS_FREQ;
			if (gET_LOGINBONUS_FREQ > 0 && num2 < gET_LOGINBONUS_FREQ)
			{
				mLoginBonusCheckDone = true;
				mLoginBonusSucceed = true;
				LoadLoginBonusLocalFile();
				return true;
			}
		}
		if (NO_NETWORK)
		{
			mLoginBonusData = new SMDLoginBonusData();
			Server_OnLoginBonus(0, 0);
			return true;
		}
		mLoginBonusSucceed = false;
		mLoginBonusCheckDone = false;
		string a_request = mLoginBonusGUID;
		string a_guid;
		if (!Server.LoginBonus(a_request, out a_guid))
		{
			mLoginBonusCheckDone = true;
			return false;
		}
		mLoginBonusGUID = a_guid;
		return true;
	}

	private void Server_OnLoginBonus(int result, int code)
	{
		if (result == 0)
		{
			mPlayer.Data.LastLoginBonusTime = DateTime.Now;
			mLoginBonusSucceed = true;
			if (mLoginBonusData != null && mLoginBonusData.HasLoginData)
			{
				mLoginBonusData.FinishedConnect();
				for (int i = 0; i < mLoginBonusData.login_bonus.Count; i++)
				{
					GetLoginBonusResponse getLoginBonusResponse = mLoginBonusData.login_bonus[i];
					int loginDayCount = getLoginBonusResponse.LoginDayCount;
					LoginBonusRewardData loginBonusRewardData = getLoginBonusResponse.RewardList[loginDayCount - 1];
					int cur_event = mPlayer.Data.CurrentEventID;
					if (!Def.IsEventSeries(mPlayer.Data.CurrentSeries))
					{
						cur_event = 0;
					}
					ServerCram.LoginBonusReceive((int)mPlayer.Data.CurrentSeries, cur_event, getLoginBonusResponse.LoginID, getLoginBonusResponse.Category, getLoginBonusResponse.Sheet, loginDayCount, loginBonusRewardData.RewardID, loginBonusRewardData.BonusID, loginBonusRewardData.Category, loginBonusRewardData.SubCategory, loginBonusRewardData.Quantity);
				}
			}
			mLoginBonusGUID = string.Empty;
			mLoginBonusErrorCode = Server.ErrorCode.SUCCESS;
		}
		else
		{
			mPlayer.Data.LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
			mLoginBonusErrorCode = (Server.ErrorCode)code;
		}
		mLoginBonusCheckDone = true;
	}

	public void LoadLoginBonusLocalFile()
	{
		if (!SingletonMonoBehaviour<GameMain>.Instance.ExistsLocalFile(Constants.LOGINBONUS_BACKUP_FILE))
		{
			return;
		}
		using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(Constants.LOGINBONUS_BACKUP_FILE))
		{
			string json = bIJEncryptDataReader.ReadUTF();
			SMDLoginBonusData obj = new SMDLoginBonusData();
			new MiniJSONSerializer().Populate(ref obj, json);
			if (obj != null && obj.HasLoginData)
			{
				SingletonMonoBehaviour<GameMain>.Instance.mLoginBonusData = obj;
				mLoginBonusData.FinishedConnect();
			}
			else
			{
				SingletonMonoBehaviour<GameMain>.Instance.DeleteLocalFile(Constants.LOGINBONUS_BACKUP_FILE);
			}
		}
	}

	public bool Network_CheckSave()
	{
		Save();
		if (NO_NETWORK)
		{
			Server_OnCheckSave(0, -1);
			return true;
		}
		if (!Server.CheckSave())
		{
			Server_OnCheckSave(1, 0);
			return false;
		}
		return true;
	}

	private void Server_OnCheckSave(int result, int code)
	{
		if (result == 0 && code == 0)
		{
			SendSaveData(false, false);
		}
	}

	public bool Network_BuyGem(int iapid, string data, string signature)
	{
		mBuyGemSucceed = false;
		mBuyGemCheckDone = false;
		if (!ServerIAP.CurrencyCharge(iapid, data, signature, (int)mBuyGemPlace))
		{
			mBuyGemCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnBuyGem(int result, object ret_obj)
	{
		if (result == 0)
		{
			try
			{
				MCChargeResponse mCChargeResponse = ret_obj as MCChargeResponse;
				int mc = mCChargeResponse.mc;
				int sc = mCChargeResponse.sc;
				long server_time = mCChargeResponse.server_time;
				mPlayer.SetDollar(mc, true);
				mPlayer.SetDollar(sc, false);
				mPlayer.ServerTime = server_time;
				mBuyGemSucceed = true;
			}
			catch
			{
				mBuyGemSucceed = false;
			}
		}
		else
		{
			mBuyGemSucceed = false;
		}
		mBuyGemCheckDone = true;
	}

	public bool Network_GetBirthday()
	{
		mGetBirthdaySucceed = false;
		mGetBirthdayCheckDone = false;
		if (!ServerIAP.BirthdayGet())
		{
			mGetBirthdayCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnGetBirthday(int result, object ret_obj)
	{
		if (result == 0)
		{
			try
			{
				BirthdayGetResponse birthdayGetResponse = ret_obj as BirthdayGetResponse;
				mGetBirthdayValue = birthdayGetResponse.birthday;
				mGetBirthdaySucceed = true;
			}
			catch
			{
				mGetBirthdaySucceed = false;
			}
		}
		else
		{
			mGetBirthdaySucceed = false;
		}
		mGetBirthdayCheckDone = true;
	}

	public bool Network_SetBirthday(string birthday)
	{
		mSetBirthdaySucceed = false;
		mSetBirthdayCheckDone = false;
		if (!ServerIAP.BirthdaySet(birthday))
		{
			mSetBirthdayCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnSetBirthday(int result, object ret_obj)
	{
		if (result == 0)
		{
			try
			{
				BirthdaySetResponse birthdaySetResponse = ret_obj as BirthdaySetResponse;
				mSetBirthdaySucceed = true;
			}
			catch
			{
				mSetBirthdaySucceed = false;
			}
		}
		else
		{
			mSetBirthdaySucceed = false;
		}
		mSetBirthdayCheckDone = true;
	}

	public bool Network_ChargeCheck(int iapid)
	{
		mChargeCheckSucceed = false;
		mChargeCheckCheckDone = false;
		if (!ServerIAP.ChargeCheck(iapid))
		{
			mChargeCheckCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnChargeCheck(int result, object ret_obj)
	{
		if (result == 0)
		{
			try
			{
				ChargeCheckResponse chargeCheckResponse = ret_obj as ChargeCheckResponse;
				mChargeCheckResult = chargeCheckResponse.result;
				if (chargeCheckResponse.limit_check != null)
				{
					mChargeCheckLimitResult = chargeCheckResponse.limit_check.result;
					if (mChargeCheckLimitResult && chargeCheckResponse.age_check != null)
					{
						mChargeCheckAgeResult = chargeCheckResponse.age_check.result;
						mChargeCheckAgeType = chargeCheckResponse.age_check.type;
					}
				}
				mChargeCheckSucceed = true;
			}
			catch
			{
				mChargeCheckSucceed = false;
			}
		}
		else
		{
			mChargeCheckSucceed = false;
		}
		mChargeCheckCheckDone = true;
	}

	public bool Network_GetGemCount()
	{
		long num = DateTimeUtil.BetweenMinutes(GetGemCountGetTime, DateTime.Now);
		long gET_GEMCOUNT_FREQ = GET_GEMCOUNT_FREQ;
		if (NO_NETWORK)
		{
			mGetGemCountSucceed = true;
			mGetGemCountCheckDone = true;
			return true;
		}
		if (GetGemCountFreqFlag)
		{
			if (gET_GEMCOUNT_FREQ > 0 && num < gET_GEMCOUNT_FREQ)
			{
				mGetGemCountCheckDone = true;
				mGetGemCountSucceed = true;
				return false;
			}
			if (mPlayer.Dollar < 10)
			{
				mGetGemCountCheckDone = true;
				mGetGemCountSucceed = true;
				return false;
			}
		}
		GetGemCountFreqFlag = true;
		mGetGemCountSucceed = false;
		mGetGemCountCheckDone = false;
		if (!ServerIAP.ChargeCheck())
		{
			mGetGemCountCheckDone = true;
			GetGemCountGetTime = DateTimeUtil.UnitLocalTimeEpoch;
			return false;
		}
		return true;
	}

	private void Server_OnGetGemCount(int result, object ret_obj)
	{
		if (result == 0)
		{
			try
			{
				MCCheckResponse mCCheckResponse = ret_obj as MCCheckResponse;
				int mc = mCCheckResponse.mc;
				int sc = mCCheckResponse.sc;
				long server_time = mCCheckResponse.server_time;
				mPlayer.SetDollar(mc, true);
				mPlayer.SetDollar(sc, false);
				mPlayer.ServerTime = server_time;
				mGetGemCountSucceed = true;
				GetGemCountGetTime = DateTime.Now;
			}
			catch
			{
				mGetGemCountSucceed = false;
			}
		}
		else
		{
			mGetGemCountSucceed = false;
		}
		mGetGemCountCheckDone = true;
	}

	public bool Network_BuyItem(int a_itemID)
	{
		if (NO_NETWORK)
		{
			mBuyItemSucceed = true;
			mBuyItemCheckDone = true;
			return true;
		}
		mBuyItemSucceed = false;
		mBuyItemCheckDone = false;
		if (!ServerIAP.CurrencySpend(a_itemID, ConnectRetryFlg))
		{
			mBuyItemCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnBuyItem(int result, object ret_obj)
	{
		MCSpendResponse mCSpendResponse = ret_obj as MCSpendResponse;
		try
		{
			if (result == 0)
			{
				int mc = mCSpendResponse.mc;
				int sc = mCSpendResponse.sc;
				long server_time = mCSpendResponse.server_time;
				mPlayer.SetDollar(mc, true);
				mPlayer.SetDollar(sc, false);
				mPlayer.ServerTime = server_time;
				mBuyItemSucceed = true;
			}
			else
			{
				int code = mCSpendResponse.code;
				mBuyItemErrorCode = (Server.ErrorCode)code;
				mBuyItemSucceed = false;
			}
		}
		catch
		{
			mBuyItemErrorCode = Server.ErrorCode.OTHER;
			mBuyItemSucceed = false;
		}
		mBuyItemCheckDone = true;
	}

	public bool Network_RewardCheck(int a_itemID)
	{
		if (NO_NETWORK)
		{
			mRewardCheckErrorCode = Server.ErrorCode.OTHER;
			mRewardCheckSucceed = false;
			mRewardCheckCheckDone = true;
			return true;
		}
		mRewardCheckSucceed = false;
		mRewardCheckCheckDone = false;
		mRewardCheckStatus = -1;
		if (!ServerIAP.SubcurrencyRewardCheck(a_itemID))
		{
			mRewardCheckCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnRewardCheck(int result, object ret_obj)
	{
		SCRewardCheckResponse sCRewardCheckResponse = ret_obj as SCRewardCheckResponse;
		try
		{
			if (result == 0)
			{
				int status = sCRewardCheckResponse.status;
				long server_time = sCRewardCheckResponse.server_time;
				mPlayer.ServerTime = server_time;
				mRewardCheckStatus = status;
				mRewardCheckSucceed = true;
			}
			else
			{
				int code = sCRewardCheckResponse.code;
				mRewardCheckErrorCode = (Server.ErrorCode)code;
				mRewardCheckSucceed = false;
			}
		}
		catch
		{
			mRewardCheckErrorCode = Server.ErrorCode.OTHER;
			mRewardCheckSucceed = false;
		}
		mRewardCheckCheckDone = true;
	}

	public bool Network_RewardSet(int a_itemID)
	{
		mRewardSetSucceed = false;
		mRewardSetCheckDone = false;
		mRewardSetStatus = -1;
		mRewardSetErrorCode = Server.ErrorCode.SUCCESS;
		if (!ServerIAP.SubcurrencyRewardSet(a_itemID))
		{
			mRewardSetCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnRewardSet(int result, object ret_obj)
	{
		SCRewardSetResponse sCRewardSetResponse = ret_obj as SCRewardSetResponse;
		try
		{
			if (result == 0)
			{
				int status = sCRewardSetResponse.status;
				long server_time = sCRewardSetResponse.server_time;
				mPlayer.ServerTime = server_time;
				mRewardSetStatus = status;
				mRewardSetSucceed = true;
			}
			else
			{
				int code = sCRewardSetResponse.code;
				mRewardSetErrorCode = (Server.ErrorCode)code;
				mRewardSetSucceed = false;
			}
		}
		catch
		{
			mRewardSetErrorCode = Server.ErrorCode.OTHER;
			mRewardSetSucceed = false;
		}
		mRewardSetCheckDone = true;
	}

	public void ClearCampaignData()
	{
		mCampaignList.Clear();
		mNextCampaignTime = 0L;
		mLastCampaignCheckTime = DateTimeUtil.UnitLocalTimeEpoch;
	}

	public bool IsCampaignOpen()
	{
		if (IsNextCampaignOpen())
		{
			return true;
		}
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (PurchaseManager.instance.IsGotStoreInfo(campaignData.GetIapName()) && campaignData.InTime())
			{
				return true;
			}
		}
		return false;
	}

	public bool IsCampaignAvailable()
	{
		if (IsNextCampaignOpen())
		{
			return true;
		}
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (PurchaseManager.instance.IsGotStoreInfo(campaignData.GetIapName()) && campaignData.InTime() && campaignData.limit_count != 0)
			{
				return true;
			}
		}
		return false;
	}

	public int GetFirstAvailableCampaignID(out int[] result)
	{
		List<int> list = new List<int>();
		int num = -1;
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (campaignData != null)
			{
				list.Add(campaignData.iap_campaign_id);
				if (PurchaseManager.instance.IsGotStoreInfo(campaignData.GetIapName()) && num == -1 && campaignData.InTime() && campaignData.limit_count != 0)
				{
					num = campaignData.iap_campaign_id;
				}
			}
		}
		result = list.ToArray();
		return num;
	}

	public string GetCampaignImageURL(int aCampaignId)
	{
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (campaignData != null && campaignData.iap_campaign_id == aCampaignId)
			{
				return campaignData.image;
			}
		}
		return null;
	}

	public int GetCampaignGroupID(int aCampaignId)
	{
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (campaignData != null && campaignData.iap_campaign_id == aCampaignId)
			{
				return campaignData.iap_campaign_group_id;
			}
		}
		return -1;
	}

	public string GetCampaignIapName(int aCampaignId)
	{
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (campaignData != null && campaignData.iap_campaign_id == aCampaignId)
			{
				return campaignData.GetIapName();
			}
		}
		return string.Empty;
	}

	public bool GetCampaignEndTime(int aCampaignId, ref DateTime result, bool is_to_local)
	{
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (campaignData != null && campaignData.iap_campaign_id == aCampaignId)
			{
				result = DateTimeUtil.UnixTimeStampToDateTime(campaignData.endtime, is_to_local);
				return true;
			}
		}
		return false;
	}

	public bool IsCampaignAvailable(int aCampaignId)
	{
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			CampaignData campaignData = mCampaignList[i];
			if (campaignData.iap_campaign_id == aCampaignId && PurchaseManager.instance.IsGotStoreInfo(campaignData.GetIapName()) && campaignData.InTime() && campaignData.limit_count != 0)
			{
				return true;
			}
		}
		return false;
	}

	public bool IsNextCampaignOpen()
	{
		if (mNextCampaignTime > 0)
		{
			long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now.AddSeconds(mServerTimeDiff * -1.0));
			if (num >= mNextCampaignTime)
			{
				return true;
			}
		}
		return false;
	}

	public bool GetCampaignBanner(bool aDLOnly)
	{
		if (mCampaignList.Count <= 0)
		{
			return false;
		}
		mCampaignBannerComplete = false;
		mCampaignBannerDLOnly = aDLOnly;
		LinkBannerSetting linkBannerSetting = new LinkBannerSetting();
		string image = mCampaignList[0].image;
		linkBannerSetting.BaseURL = image.Substring(0, image.LastIndexOf("/") + 1);
		for (int i = 0; i < mCampaignList.Count; i++)
		{
			LinkBannerDetail linkBannerDetail = new LinkBannerDetail();
			image = mCampaignList[i].image;
			linkBannerDetail.FileName = image.Substring(image.LastIndexOf("/") + 1);
			linkBannerDetail.Param = mCampaignList[i].iap_campaign_id;
			linkBannerSetting.BannerList.Add(linkBannerDetail);
		}
		mCampaignBannerReq.SetHighPriority(true);
		return LinkBannerManager.Instance.GetLinkBannerRequest(mCampaignBannerReq, linkBannerSetting, OnGetComplete_CampaignBanner, aDLOnly);
	}

	public void ClearCampaignBanner()
	{
		if (mCampaignBannerList != null)
		{
			mCampaignBannerList.Clear();
			mCampaignBannerList = null;
		}
	}

	public void OnGetComplete_CampaignBanner(List<LinkBannerInfo> aBannerList)
	{
		if (!mCampaignBannerDLOnly)
		{
			mCampaignBannerList = aBannerList;
		}
		mCampaignBannerComplete = true;
	}

	public bool Network_CampaignCheck(bool aTutorial = true)
	{
		long num = DateTimeUtil.BetweenMinutes(mLastCampaignCheckTime, DateTime.Now);
		long mCampaignCheckFreq = mGameProfile.mCampaignCheckFreq;
		if (num < mCampaignCheckFreq)
		{
			mCampaignCheckSucceed = true;
			mCampaignCheckDone = true;
			return true;
		}
		return Network_CampaignCheckForce(aTutorial);
	}

	public bool Network_CampaignCheckForce(bool aTutorial)
	{
		if (GameProfile.mOldPurchaseDialog)
		{
			mCampaignList.Clear();
			mNextCampaignTime = 0L;
			mCampaignCheckSucceed = true;
			mCampaignCheckDone = true;
			mLastCampaignCheckTime = DateTime.Now;
			return true;
		}
		if (NO_NETWORK)
		{
			mCampaignCheckErrorCode = Server.ErrorCode.OTHER;
			mCampaignCheckSucceed = false;
			mCampaignCheckDone = true;
			return true;
		}
		mCampaignCheckSucceed = false;
		mCampaignCheckDone = false;
		if (!PurchaseManager.instance.GetItemFromKIND(PItem.KIND.CA0).storeInfo)
		{
			aTutorial = false;
		}
		if (!ServerIAP.CampaignCheck(aTutorial, USE_DEBUG_TIER))
		{
			mCampaignCheckDone = true;
			ClearCampaignData();
			return false;
		}
		return true;
	}

	private void Server_OnCampaignCheck(int result, object ret_obj)
	{
		CampaignCheckResponse campaignCheckResponse = ret_obj as CampaignCheckResponse;
		try
		{
			if (result == 0)
			{
				if (campaignCheckResponse.iap_campaign_data == null)
				{
					mCampaignList.Clear();
				}
				else
				{
					mCampaignList = campaignCheckResponse.iap_campaign_data;
				}
				mNextCampaignTime = campaignCheckResponse.next_iap_campaign_starttime;
				mCampaignDataTime = campaignCheckResponse.server_time;
				mCampaignCheckSucceed = true;
				mLastCampaignCheckTime = DateTime.Now;
				ServerTime = DateTimeUtil.UnixTimeStampToDateTime(mCampaignDataTime, true);
			}
			else
			{
				int code = campaignCheckResponse.code;
				mCampaignCheckErrorCode = (Server.ErrorCode)code;
				mCampaignCheckSucceed = false;
				ClearCampaignData();
			}
		}
		catch
		{
			mCampaignCheckErrorCode = Server.ErrorCode.OTHER;
			mCampaignCheckSucceed = false;
			ClearCampaignData();
		}
		mCampaignCheckDone = true;
	}

	public bool Network_CampaignCharge(int aId, long aPurchaseTime)
	{
		if (NO_NETWORK)
		{
			mCampaignChargeErrorCode = Server.ErrorCode.OTHER;
			mCampaignChargeSucceed = false;
			mCampaignChargeDone = true;
			return true;
		}
		mCampaignChargeSucceed = false;
		mCampaignChargeDone = false;
		if (!ServerIAP.CampaignCharge(aId, aPurchaseTime))
		{
			mCampaignChargeDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnCampaignCharge(int result, object ret_obj)
	{
		CampaignChargeResponse campaignChargeResponse = ret_obj as CampaignChargeResponse;
		try
		{
			if (result == 0)
			{
				mCampaignDataTime = campaignChargeResponse.server_time;
				mCampaignChargeSucceed = true;
				ServerTime = DateTimeUtil.UnixTimeStampToDateTime(mCampaignDataTime, true);
			}
			else
			{
				int code = campaignChargeResponse.code;
				mCampaignChargeErrorCode = (Server.ErrorCode)code;
				mCampaignChargeSucceed = false;
			}
		}
		catch
		{
			mCampaignChargeErrorCode = Server.ErrorCode.OTHER;
			mCampaignChargeSucceed = false;
		}
		mCampaignChargeDone = true;
	}

	public bool Network_DebugCampaignUnset()
	{
		if (NO_NETWORK)
		{
			mDebugCampaignUnsetErrorCode = Server.ErrorCode.OTHER;
			mDebugCampaignUnsetSucceed = false;
			mDebugCampaignUnsetDone = true;
			return true;
		}
		mDebugCampaignUnsetSucceed = false;
		mDebugCampaignUnsetDone = false;
		if (!ServerIAP.DebugCampaignUnset())
		{
			mCampaignChargeDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnDebugCampaignUnset(int result, object ret_obj)
	{
		DebugCampaignUnsetResponse debugCampaignUnsetResponse = ret_obj as DebugCampaignUnsetResponse;
		try
		{
			if (result == 0)
			{
				mCampaignDataTime = debugCampaignUnsetResponse.server_time;
				mDebugCampaignUnsetSucceed = true;
				ClearCampaignData();
			}
			else
			{
				int code = debugCampaignUnsetResponse.code;
				mDebugCampaignUnsetErrorCode = (Server.ErrorCode)code;
				mDebugCampaignUnsetSucceed = false;
			}
		}
		catch
		{
			mDebugCampaignUnsetErrorCode = Server.ErrorCode.OTHER;
			mDebugCampaignUnsetSucceed = false;
		}
		mDebugCampaignUnsetDone = true;
	}

	public bool Network_GetSaveDataOfOthers(string search_id)
	{
		if (NO_NETWORK)
		{
			mGetSaveDataOfOthersSucceed = true;
			mGetSaveDataOfOthersCheckDone = true;
			return true;
		}
		mSearchIdCx = search_id;
		mGetSaveDataOfOthersCheckDone = false;
		mGetSaveDataOfOthersSucceed = false;
		if (!Server.GetServerSaveDataOfOthers(search_id))
		{
			mGetSaveDataOfOthersCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnGetSaveDataOfOthers(int result, int code, Stream resStream, int dataVersion)
	{
		if (result == 0)
		{
			if (dataVersion >= 1100)
			{
				SaveDataOfOthersPath = Path.Combine(Application.persistentDataPath, Constants.OTHER_PLAYER_FILE);
			}
			else
			{
				SaveDataOfOthersPath = Path.Combine(Application.persistentDataPath, Constants.OTHER_PLAYER_FILE_OLD);
			}
			if (!string.IsNullOrEmpty(SaveDataOfOthersPath) && File.Exists(SaveDataOfOthersPath))
			{
				File.Delete(SaveDataOfOthersPath);
			}
			try
			{
				int num = 0;
				byte[] array = new byte[4096];
				using (FileStream output = new FileStream(SaveDataOfOthersPath, FileMode.Create))
				{
					using (BinaryWriter binaryWriter = new BinaryWriter(output))
					{
						while ((num = resStream.Read(array, 0, array.Length)) > 0)
						{
							binaryWriter.Write(array, 0, num);
						}
					}
				}
			}
			catch (Exception e)
			{
				mGetSaveDataOfOthersCheckDone = true;
				BIJLog.E("GetGameState error.", e);
			}
			mGetSaveDataOfOthersSucceed = true;
			mGetSaveDataOfOthersCheckDone = true;
		}
		else
		{
			mGetSaveDataOfOthersCheckDone = true;
		}
	}

	public bool Network_GetSaveDataOfOthers_CX()
	{
		mGetSaveDataOfOthersCXSucceed = false;
		if (!Server.GetServerSaveDataOfOthersCX(mSearchIdCx))
		{
			mGetSaveDataOfOthersCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnGetSaveDataOfOthersCX(int result, int code, Stream resStream, int dataVersion)
	{
		if (result == 0)
		{
			SaveDataOfOthersPath = Path.Combine(Application.persistentDataPath, Constants.OTHER_PLAYER_FILE_CX);
			if (!string.IsNullOrEmpty(SaveDataOfOthersPath) && File.Exists(SaveDataOfOthersPath))
			{
				File.Delete(SaveDataOfOthersPath);
			}
			try
			{
				int num = 0;
				byte[] array = new byte[4096];
				using (FileStream output = new FileStream(SaveDataOfOthersPath, FileMode.Create))
				{
					using (BinaryWriter binaryWriter = new BinaryWriter(output))
					{
						while ((num = resStream.Read(array, 0, array.Length)) > 0)
						{
							binaryWriter.Write(array, 0, num);
						}
					}
				}
				mGetSaveDataOfOthersCXSucceed = true;
			}
			catch (Exception e)
			{
				BIJLog.E("GetGameState error.", e);
			}
		}
		mGetSaveDataOfOthersSucceed = true;
		mGetSaveDataOfOthersCheckDone = true;
	}

	public bool MapNetwork_SupportCheck(int custom_receive_count = -1)
	{
		bool result = false;
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetSupportTime, DateTime.Now);
		long gET_SUPPORT_FREQ = GET_SUPPORT_FREQ;
		if (gET_SUPPORT_FREQ > 0 && num < gET_SUPPORT_FREQ)
		{
			mMapSupportCheckDone = true;
			mMapSupportSucceed = true;
			return true;
		}
		if (NO_NETWORK)
		{
			mMapSupportCheckDone = true;
			mMapSupportSucceed = true;
			return true;
		}
		mMapSupportCheckDone = false;
		mMapSupportSucceed = false;
		MapSupportReceiveCount = 0;
		if (custom_receive_count != -1)
		{
			MapSupportRequestCount = custom_receive_count;
		}
		else
		{
			custom_receive_count = mGameProfile.MailGetCount;
			if (mGameProfile == null || mGameProfile.MailGetCount == 0)
			{
				MapSupportRequestCount = Constants.GIFT_AMOUNT;
			}
			else
			{
				MapSupportRequestCount = mGameProfile.MailGetCount;
			}
		}
		if (!Server.GetSupportGift(custom_receive_count))
		{
			mMapSupportCheckDone = true;
			return true;
		}
		return result;
	}

	public void MapNetwork_SupportGot()
	{
		if (mGotSupportIds.Count > 0)
		{
			Server.GotSupport(Server.MailType.SUPPORT, mGotSupportIds);
		}
		mGotSupportIds.Clear();
	}

	public bool MapNetwork_GiftCheck()
	{
		bool result = false;
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetGiftTime, DateTime.Now);
		long gET_GIFT_FREQ = GET_GIFT_FREQ;
		if (gET_GIFT_FREQ > 0 && num < gET_GIFT_FREQ)
		{
			mMapGiftCheckDone = true;
			mMapGiftSucceed = true;
			return true;
		}
		if (NO_NETWORK)
		{
			mMapGiftCheckDone = true;
			mMapGiftSucceed = true;
			return true;
		}
		mMapGiftCheckDone = false;
		mMapGiftSucceed = false;
		if (!Server.GetGift(mPlayer.Gifts.Count))
		{
			mMapGiftCheckDone = true;
			return true;
		}
		return result;
	}

	public void MapNetwork_GiftGot()
	{
		if (mGotGiftIds.Count > 0)
		{
			Server.GotGift(Server.MailType.GIFT, mGotGiftIds);
		}
		mGotGiftIds.Clear();
	}

	public bool MapNetwork_ApplyCheck()
	{
		bool result = false;
		mMapApplyRepeat = false;
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetApplyTime, DateTime.Now);
		long gET_APPLY_FREQ = GET_APPLY_FREQ;
		if (gET_APPLY_FREQ > 0 && num < gET_APPLY_FREQ)
		{
			mMapApplyCheckDone = true;
			mMapApplySucceed = true;
			return true;
		}
		if (NO_NETWORK)
		{
			mMapApplyCheckDone = true;
			mMapApplySucceed = true;
			return true;
		}
		int mailByFriendReq = mPlayer.GetMailByFriendReq();
		if (mailByFriendReq >= 100)
		{
			mMapApplyCheckDone = true;
			mMapApplySucceed = true;
			return true;
		}
		mMapApplyCheckDone = false;
		mMapApplySucceed = false;
		int num2 = 100 - mailByFriendReq;
		int num3 = mGameProfile.MailGetCount;
		if (num3 == 0)
		{
			num3 = Constants.GIFT_AMOUNT;
		}
		if (num2 > num3)
		{
			num2 = num3;
		}
		mMapApplyMailMax = num2;
		if (!Server.GetFriendApply(num2))
		{
			mMapApplyCheckDone = true;
			return true;
		}
		return result;
	}

	public void MapNetwork_ApplyGot()
	{
		if (mGotApplyIds.Count > 0)
		{
			Server.GotApply(Server.MailType.FRIEND, mGotApplyIds);
		}
		mGotApplyIds.Clear();
	}

	public bool MapNetwork_SupportPurchaseCheck()
	{
		bool result = false;
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetSupportPurchaseTime, DateTime.Now);
		long gET_SUPPORTPURCHASE_FREQ = GET_SUPPORTPURCHASE_FREQ;
		if (gET_SUPPORTPURCHASE_FREQ > 0 && num < gET_SUPPORTPURCHASE_FREQ)
		{
			mMapSupportPurchaseCheckDone = true;
			mMapSupportPurchaseSucceed = true;
			return true;
		}
		if (NO_NETWORK)
		{
			mMapSupportPurchaseCheckDone = true;
			mMapSupportPurchaseSucceed = true;
			return true;
		}
		mMapSupportPurchaseCheckDone = false;
		mMapSupportPurchaseSucceed = false;
		if (!ServerIAP.SubcurrencyCheck())
		{
			mMapSupportPurchaseCheckDone = true;
			return true;
		}
		return result;
	}

	public void Network_AllGiftFinished()
	{
	}

	public void Server_OnGetGift(int result, int code, Server.MailType a_kind, string json)
	{
		if (result == 0)
		{
			try
			{
				GetGiftTemp obj = new GetGiftTemp();
				new MiniJSONSerializer().Populate(ref obj, json);
				List<GetMailResponseGift> list = new List<GetMailResponseGift>();
				if (obj != null)
				{
					list.AddRange(obj.results);
				}
				if (list != null && list.Count > 0)
				{
					foreach (GetMailResponseGift item in list)
					{
						GiftItemData giftItemData = item.GetGiftItemData();
						if (giftItemData.IsValid())
						{
							GiftItem giftItem = new GiftItem();
							giftItem.SetData(giftItemData);
							giftItem.Data.SetGiftSource(a_kind);
							mPlayer.AddGift(giftItem);
							mGotGiftIds.Add(giftItem.Data.RemoteID);
							mNetworkDataModified = true;
							if (giftItem.Data.ItemCategory == 20 && IsPlayAdvMode())
							{
								giftItem = new GiftItem();
								giftItem.SetData(giftItemData);
								giftItem.Data.SetGiftSource(a_kind);
								giftItem.Data.ItemCategory = 31;
								giftItem.Data.ItemKind = 1;
								mPlayer.AddGift(giftItem);
							}
						}
					}
				}
				if (a_kind == Server.MailType.GIFT)
				{
					mPlayer.Data.LastGetGiftTime = DateTime.Now;
					mMapGiftSucceed = true;
				}
			}
			catch (Exception ex)
			{
				BIJLog.E("GetGift Error : " + ex);
			}
		}
		else
		{
			mPlayer.Data.LastGetGiftTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mMapGiftCheckDone = true;
	}

	public void Server_OnGetApply(int result, int code, Server.MailType a_kind, Stream resStream)
	{
		if (result == 0)
		{
			try
			{
				using (StreamReader streamReader = new StreamReader(resStream, Encoding.UTF8))
				{
					List<int> list = new List<int>();
					string json = streamReader.ReadToEnd();
					int num = 0;
					GetApplyTemp obj = new GetApplyTemp();
					new MiniJSONSerializer().Populate(ref obj, json);
					if (obj != null && obj.results != null && obj.results.Count > 0)
					{
						DateTime now = DateTime.Now;
						int got_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
						foreach (GetMailResponseApply result2 in obj.results)
						{
							bool flag = true;
							GiftItemData giftItemData = result2.GetGiftItemData();
							GiftItem giftItem = new GiftItem();
							giftItem.SetData(giftItemData);
							giftItem.Data.SetGiftSource(a_kind);
							if (giftItem.Data.RemoteTimeEpoch != 0)
							{
								long num2 = DateTimeUtil.BetweenDays0(giftItem.RemoteTime, now);
								long num3 = ((giftItem.GiftItemCategory != Def.ITEM_CATEGORY.FRIEND_REQ) ? Constants.GIFT_EXPIRED : Constants.GIFT_EXPIRED_FRIEND_REQ_FREQ);
								if (num2 >= num3)
								{
									num++;
									int itemCategory = giftItem.Data.ItemCategory;
									int itemKind = giftItem.Data.ItemKind;
									int itemID = giftItem.Data.ItemID;
									int quantity = giftItem.Data.Quantity;
									int lastPlaySeries = (int)SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlaySeries;
									int lastPlayLevel = SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlayLevel;
									ServerCram.LostGiftItem(itemCategory, itemKind, itemID, quantity, got_time, lastPlaySeries, lastPlayLevel);
									flag = false;
								}
							}
							if (flag)
							{
								mPlayer.AddGift(giftItem);
								mNetworkDataModified = true;
							}
							mGotApplyIds.Add(giftItem.Data.RemoteID);
						}
					}
					if (a_kind == Server.MailType.FRIEND)
					{
						if (obj.results.Count >= mMapApplyMailMax)
						{
							bool flag2 = false;
							if (num > 0)
							{
								int num4 = 5;
								if (mGameProfile.ApplyRepeatNum > 0)
								{
									num4 = mGameProfile.ApplyRepeatNum;
								}
								mMapApplyRepeatCnt++;
								if (mMapApplyRepeatCnt < num4)
								{
									flag2 = true;
								}
							}
							if (flag2)
							{
								mMapApplyRepeat = true;
							}
							else
							{
								mMapApplyRepeatCnt = 0;
							}
							GET_APPLY_FREQ = 5L;
						}
						else
						{
							GET_APPLY_FREQ = Constants.GET_APPLY_FREQ;
							mMapApplyRepeatCnt = 0;
						}
						mPlayer.Data.LastGetApplyTime = DateTime.Now;
						mMapApplySucceed = true;
					}
				}
			}
			catch (Exception ex)
			{
				BIJLog.E("Server_OnGetApply Error:" + ex);
			}
		}
		else
		{
			mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mMapApplyCheckDone = true;
	}

	public void Server_OnGetSupport(int result, int code, Server.MailType a_kind, Stream resStream)
	{
		if (result == 0)
		{
			try
			{
				using (StreamReader streamReader = new StreamReader(resStream, Encoding.UTF8))
				{
					List<int> list = new List<int>();
					string json = streamReader.ReadToEnd();
					GetSupportTemp obj = new GetSupportTemp();
					new MiniJSONSerializer().Populate(ref obj, json);
					if (obj != null && obj.results != null && obj.results.Count > 0)
					{
						foreach (GetMailResponseSupport result2 in obj.results)
						{
							GiftItemData giftItemData = result2.GetGiftItemData();
							GiftItem giftItem = new GiftItem();
							giftItem.SetData(giftItemData);
							giftItem.Data.SetGiftSource((Server.MailType)giftItemData.MailType);
							if (giftItem.Data.ItemCategory == 23)
							{
								mPlayer.ResetTimeCheaterPenalty(giftItem.Data.Quantity);
								int num = 0;
								if (ServerTime.HasValue)
								{
									num = (int)mMaintenanceProfile.ServerTime;
								}
								int num2 = (int)DateTimeUtil.DateTimeToUnixTimeStamp(DateTime.Now.ToUniversalTime());
								int num3 = (int)DateTimeUtil.DateTimeToUnixTimeStamp(mPlayer.Data.LastPlayTime.ToUniversalTime());
								ServerCram.CheatUserSut(num, num2, num3, -1);
							}
							else if (CheckCorrectReceiveGiftItem(giftItem))
							{
								MapSupportReceiveCount++;
								mPlayer.AddGift(giftItem);
							}
							mGotSupportIds.Add(giftItem.Data.RemoteID);
							mNetworkDataModified = true;
						}
					}
					if (a_kind == Server.MailType.SUPPORT)
					{
						mPlayer.Data.LastGetSupportTime = DateTime.Now;
						mMapSupportSucceed = true;
					}
				}
			}
			catch (Exception)
			{
			}
		}
		else
		{
			mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mMapSupportCheckDone = true;
	}

	public void Server_OnGetSupportPurchase(int result, object ret_obj)
	{
		if (result == 0)
		{
			SCCheckResponse sCCheckResponse = ret_obj as SCCheckResponse;
			if (sCCheckResponse != null && sCCheckResponse.results != null)
			{
				for (int i = 0; i < sCCheckResponse.results.Count; i++)
				{
					try
					{
						SCStatus sCStatus = sCCheckResponse.results[i];
						GiftItemData giftItemData = new GiftItemData();
						giftItemData.UseIcon = true;
						giftItemData.UsePlayerInfo = false;
						giftItemData.FromID = 0;
						giftItemData.RemoteID = sCStatus.id;
						giftItemData.GiftSource = (GiftMailType)sCStatus.mail_type;
						giftItemData.ItemCategory = sCStatus.category;
						giftItemData.ItemKind = sCStatus.sub_category;
						giftItemData.ItemID = sCStatus.itemid;
						giftItemData.Quantity = sCStatus.quantity;
						giftItemData.MailType = sCStatus.mail_type;
						giftItemData.Message = sCStatus.message;
						GiftItem giftItem = new GiftItem();
						giftItem.SetData(giftItemData);
						mPlayer.AddGift(giftItem);
						mGotSupportPurchaseIds.Add(giftItem.Data.RemoteID);
						mNetworkDataModified = true;
					}
					catch
					{
					}
				}
				mPlayer.Data.LastGetSupportPurchaseTime = DateTime.Now;
				mMapSupportPurchaseSucceed = true;
			}
		}
		else
		{
			mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mMapSupportPurchaseCheckDone = true;
	}

	public bool Network_ChargePriceCheck(long _past_seconds)
	{
		if (NO_NETWORK)
		{
			Server_OnChargePriceCheck(0, 0);
			return true;
		}
		mChargePriceCheckSucceed = false;
		mChargePriceCheckDone = false;
		if (!Server.ChargePriceCheck(_past_seconds))
		{
			mChargePriceCheckDone = true;
			return false;
		}
		return true;
	}

	private void Server_OnChargePriceCheck(int result, int code)
	{
		mChargePriceCheckDone = true;
		if (result == 0)
		{
			mChargePriceCheckSucceed = true;
		}
		else
		{
			mChargePriceCheckErrorCode = (Server.ErrorCode)code;
		}
	}

	public void PushCurrentStage(ISMStageData aStage, SMMapStageSetting aStageSetting)
	{
		mStageStuck.Insert(0, mCurrentStage);
		mStageInfoStuck.Insert(0, mCurrentStageInfo);
		mCurrentStage = aStage;
		mCurrentStageInfo = aStageSetting;
	}

	public void PopCurrentStage(out ISMStageData aPoppedStage, out SMMapStageSetting aPoppedStageSetting)
	{
		if (mStageStuck.Count > 0)
		{
			aPoppedStage = mCurrentStage;
			mCurrentStage = mStageStuck[0];
			mStageStuck.RemoveAt(0);
		}
		else
		{
			aPoppedStage = null;
		}
		if (mStageInfoStuck.Count > 0)
		{
			aPoppedStageSetting = mCurrentStageInfo;
			mCurrentStageInfo = mStageInfoStuck[0];
			mStageInfoStuck.RemoveAt(0);
		}
		else
		{
			aPoppedStageSetting = null;
		}
	}

	public void GetPreStage(out ISMStageData aStage, out SMMapStageSetting aStageSetting, int aPreCnt = 1)
	{
		if (aPreCnt <= 0)
		{
			aStage = null;
			aStageSetting = null;
		}
		if (mStageStuck.Count >= aPreCnt)
		{
			aStage = mStageStuck[aPreCnt - 1];
		}
		else
		{
			aStage = null;
		}
		if (mStageInfoStuck.Count >= aPreCnt)
		{
			aStageSetting = mStageInfoStuck[aPreCnt - 1];
		}
		else
		{
			aStageSetting = null;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		InternetReachability.Init();
	}

	private void AwakeSingleton()
	{
		ReleaseBuildSetting();
		SetTestServerSettings(CONNECT_TESTSERVER);
		Application.targetFrameRate = Def.TARGET_FRAMERATE;
		Time.timeScale = 1f;
		if (ALWAYS_LOG)
		{
			BIJLog.Instance.LogLevel = BIJLog.Level.DEBUG;
		}
		else
		{
			BIJLog.Instance.LogLevel = BIJLog.Level.ERROR;
		}
		OPEN_DEBUGDIALOG_BOOT = false;
		Util.ScreenOrientation = ScreenOrientation.AutoRotation;
		Screen.autorotateToLandscapeLeft = true;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;
		LockRotationOnApplicationPause = true;
		mAvailableRotation = BIJUnity.isAvailableRotation();
		if (!mAvailableRotation)
		{
			Screen.autorotateToLandscapeLeft = false;
			Screen.autorotateToLandscapeRight = false;
			Screen.autorotateToPortrait = false;
			Screen.autorotateToPortraitUpsideDown = false;
			switch (Util.ScreenOrientation)
			{
			case ScreenOrientation.LandscapeLeft:
				Screen.autorotateToLandscapeLeft = true;
				break;
			case ScreenOrientation.LandscapeRight:
				Screen.autorotateToLandscapeRight = true;
				break;
			case ScreenOrientation.Portrait:
				Screen.autorotateToPortrait = true;
				break;
			case ScreenOrientation.PortraitUpsideDown:
				Screen.autorotateToPortraitUpsideDown = true;
				break;
			}
		}
		CreateDLManager();
		mDialogManager = Util.CreateGameObject("DialogManager", null).AddComponent<DialogManager>();
		mCamera = GameObject.Find("Camera").GetComponent<Camera>();
		mUICamera = Camera.main.gameObject.GetComponent<UICamera>();
		mClippingCamera = Camera.main.gameObject.GetComponent<ScClippingCamera>();
		mAnchor = GameObject.Find("Anchor").GetComponent<UIAnchor>();
		TileSetting_Normal();
		mConcierge = new GameConcierge();
		Live2D.init();
	}

	public void TileSetting_Normal()
	{
		Vector2 vector = Util.LogScreenSize();
		float num = 1136f / Mathf.Max(vector.x, vector.y);
		num = ((!Util.IsWideScreen()) ? (num * 0.95f) : (num * 1f));
		num = Mathf.Min(num, 1f);
		Def.TILE_SCALE = Def.TILE_SCALE_DEFAULT * num;
		Def.TILE_PITCH_H = Def.TILE_PITCH_H_DEFAULT * num;
		Def.TILE_PITCH_V = Def.TILE_PITCH_V_DEFAULT * num;
		Def.TILE_HITCHECK_H = Def.TILE_PITCH_H / 1136f;
		Def.TILE_HITCHECK_V = Def.TILE_PITCH_V / 1136f;
	}

	public void TileSetting_Adv()
	{
		float num = Util.LogScreenSize().x / 639f;
		if (num > Def.ADV_DEVICE_SCALE_MAX)
		{
			num = Def.ADV_DEVICE_SCALE_MAX;
		}
		num = 1.15f;
		if (Util.IsWideScreen())
		{
			float num2 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
			float num3 = 0.5625f;
			float num4 = 0.4617052f;
			num -= (num3 - num2) / (num3 - num4) * 0.15f;
		}
		Def.TILE_SCALE = Def.TILE_SCALE_DEFAULT * num;
		Def.TILE_PITCH_H = Def.TILE_PITCH_H_DEFAULT * num;
		Def.TILE_PITCH_V = Def.TILE_PITCH_V_DEFAULT * num;
		Def.TILE_HITCHECK_H = Def.TILE_PITCH_H / 1136f;
		Def.TILE_HITCHECK_V = Def.TILE_PITCH_V / 1136f;
	}

	public void CreateDLManager()
	{
		if (mDLManagerObject == null)
		{
			mDLManagerObject = Util.CreateGameObject("ResourceDownloadManager_" + ResourceDownloadManager.DLmanagerCount, base.gameObject);
			mDLManager = mDLManagerObject.AddComponent<ResourceDownloadManager>();
			ResourceDownloadManager.DLmanagerCount++;
			return;
		}
		if (mDLManager.respawnReq)
		{
			if (mDLManagerObject != null)
			{
				UnityEngine.Object.Destroy(mDLManagerObject);
				mDLManagerObject = null;
				mDLManager = null;
			}
			mDLManagerObject = Util.CreateGameObject("ResourceDownloadManager_" + ResourceDownloadManager.DLmanagerCount, base.gameObject);
			mDLManager = mDLManagerObject.AddComponent<ResourceDownloadManager>();
			ResourceDownloadManager.DLmanagerCount++;
		}
		ResourceDLUtils.dbg1("DLManager is already exist.");
	}

	private IEnumerator Start()
	{
		IsLogoFinished = false;
		IsLoaded = false;
		IsFirstPlayFromStartup = true;
		Res.UpdateResourceDictionaty();
		BIJUnity.setup(base.gameObject.name, "BIJUnity_callback", true);
		mInputEnable = true;
		mSignRoot = Util.CreateGameObject("SignRoot", null);
		mRoot = GameObject.Find("UI Root (2D)").GetComponent<UIRoot>();
		do
		{
			mRoot.manualHeight = (int)Util.CalculateCameraHeight();
			mRoot.manualWidth = (int)Util.CalculateCameraHeight();
			mScreenOrientation = Util.ScreenOrientation;
			if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				mRoot.fitHeight = true;
				mRoot.fitWidth = false;
				mDeviceRatioScale = 1f;
			}
			else if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				mRoot.fitHeight = false;
				mRoot.fitWidth = true;
				mDeviceRatioScale = (float)mRoot.manualWidth / 950f;
			}
			else
			{
				mRoot.fitHeight = true;
				mRoot.fitWidth = false;
				mDeviceRatioScale = 1f;
			}
		}
		while (mRoot.manualHeight == 0 && mRoot.manualWidth == 0);
		TextAsset obj = Resources.Load("Localization/Language") as TextAsset;
		Localization.Load(obj);
		Localization.language = "Language";
		mFont = null;
		UnityEngine.Random.seed = (int)DateTime.Now.Ticks;
		mGameStateManager = Util.CreateGameObject("GameStateManager", null).AddComponent<GameStateManager>();
		Util.CreateGameObject("ResourceManager", null).AddComponent<ResourceManager>();
		Util.CreateGameObject("ResourceASyncLoader", null).AddComponent<ResourceASyncLoader>();
		Util.CreateGameObject("FBIconManager", null).AddComponent<FBIconManager>();
		LinkBannerManager.CreateLinkBannerManager();
		InformationManager.CreateInstance();
		mTutorialManager = new TutorialManager();
		InitAdv();
		mIsStarted = true;
		yield break;
	}

	public void BIJUnity_callback()
	{
		if (!(Network.SocialManager == null))
		{
			SocialManager.Instance.OnSetup();
		}
	}

	public IEnumerator InitGame()
	{
		InitGameFinished = false;
		DISPLAY_RANDOM_USER_NUM = Constants.DISPLAY_RANDOM_FRIEND_MAX;
		RANDOM_USER_MAX = Constants.RANDOM_FRIEND_MAX;
		APPLY_RANDOM_USER_MAX = Constants.APPLY_RANDOM_FRIEND_MAX;
		mEventName = Localization.Get("LevelInfo_Level");
		List<ResLive2DAnimation> animationList = new List<ResLive2DAnimation>();
		bool loadingWait = true;
		while (loadingWait)
		{
			loadingWait = false;
			for (int i = 0; i < animationList.Count; i++)
			{
				if (animationList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					loadingWait = true;
				}
			}
			yield return 0;
		}
		InitGameFinished = true;
	}

	private void UpdateSignRootTransform()
	{
		if (!(mSignRoot == null))
		{
			Vector2 vector = Util.LogScreenSize();
			if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				mSignRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
			}
			else if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				mSignRoot.transform.localPosition = new Vector3(0f, 0.02f, 0f);
			}
			mSignRoot.transform.localScale = new Vector3(1136f / vector.y, 1136f / vector.y, 1f);
		}
	}

	public void StartLoading()
	{
		mSignRoot.transform.localScale = Vector3.one;
		mSignRoot.transform.localPosition = Vector3.zero;
		if (mLaceAnimation == null)
		{
			GameObject original = Resources.Load<GameObject>("Animation/_Connecting");
			mLaceAnimation = UnityEngine.Object.Instantiate(original);
			mLaceAnimation.name = "_LaceAnimation";
			mLaceAnimation.transform.parent = mSignRoot.transform;
		}
		mLaceAnimation.SetActive(true);
		mLaceAnimation.GetComponent<Animator>().Play("connect");
		SpriteRenderer component = mLaceAnimation.GetComponent<SpriteRenderer>();
		if (component != null)
		{
			component.sortingOrder = 1000;
		}
		if (mLoading == null)
		{
			GameObject original2 = Resources.Load<GameObject>("Animation/_Loading_face");
			mLoading = UnityEngine.Object.Instantiate(original2);
			mLoading.name = "_Loading";
			mLoading.transform.parent = mSignRoot.transform;
		}
		mLoading.SetActive(true);
		mLoading.GetComponent<Animator>().Play("load_face");
		component = mLoading.GetComponent<SpriteRenderer>();
		if (component != null)
		{
			component.sortingOrder = 1001;
		}
		UpdateSignRootTransform();
	}

	public bool IsLoading()
	{
		return mLoading != null && mLoading.activeSelf;
	}

	public void FinishLoading()
	{
		if (mLaceAnimation != null)
		{
			UnityEngine.Object.Destroy(mLaceAnimation.gameObject);
			mLaceAnimation = null;
		}
		if (mLoading != null)
		{
			UnityEngine.Object.Destroy(mLoading.gameObject);
			mLoading = null;
		}
	}

	public void StartConnecting()
	{
		mSignRoot.transform.localScale = Vector3.one;
		mSignRoot.transform.localPosition = Vector3.zero;
		if (mLaceAnimation == null)
		{
			GameObject original = Resources.Load<GameObject>("Animation/_Connecting");
			mLaceAnimation = UnityEngine.Object.Instantiate(original);
			mLaceAnimation.name = "_LaceAnimation";
			mLaceAnimation.transform.parent = mSignRoot.transform;
			mConnectingStart = Time.realtimeSinceStartup;
		}
		mLaceAnimation.SetActive(true);
		mLaceAnimation.GetComponent<Animator>().Play("connect");
		SpriteRenderer component = mLaceAnimation.GetComponent<SpriteRenderer>();
		if (component != null)
		{
			component.sortingOrder = 1000;
		}
		if (mConnectingAnimation == null)
		{
			GameObject original2 = Resources.Load<GameObject>("Animation/_Connecting_face");
			mConnectingAnimation = UnityEngine.Object.Instantiate(original2);
			mConnectingAnimation.name = "_Connecting_face";
			mConnectingAnimation.transform.parent = mSignRoot.transform;
		}
		mConnectingAnimation.SetActive(true);
		mConnectingAnimation.GetComponent<Animator>().Play("connect_face");
		component = mConnectingAnimation.GetComponent<SpriteRenderer>();
		if (component != null)
		{
			component.sortingOrder = 1001;
		}
		UpdateSignRootTransform();
	}

	public bool IsConnecting()
	{
		return mConnectingAnimation != null && mConnectingAnimation.activeSelf;
	}

	public void FinishConnecting()
	{
		if (mLaceAnimation != null)
		{
			UnityEngine.Object.Destroy(mLaceAnimation.gameObject);
			mLaceAnimation = null;
		}
		if (mConnectingAnimation != null)
		{
			UnityEngine.Object.Destroy(mConnectingAnimation.gameObject);
			mConnectingAnimation = null;
		}
	}

	public void StartPause()
	{
		mSignRoot.transform.localScale = Vector3.one;
		mSignRoot.transform.localPosition = Vector3.zero;
		if (mPause == null)
		{
			GameObject original = Resources.Load<GameObject>("Animation/_Pause");
			mPause = UnityEngine.Object.Instantiate(original, Vector3.zero, Quaternion.identity) as GameObject;
			mPause.name = "_Pause";
			mPause.transform.parent = mSignRoot.transform;
		}
		mPause.SetActive(true);
		UpdateSignRootTransform();
	}

	public bool IsPause()
	{
		return mPause != null && mPause.activeSelf;
	}

	public void FinishPause()
	{
		if (mPause != null)
		{
			mPause.SetActive(false);
		}
	}

	public void LoadOptions()
	{
		bool flag = false;
		try
		{
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(Constants.OPTIONS_FILE))
			{
				mOptions = global::Options.Deserialize(bIJEncryptDataReader);
				bIJEncryptDataReader.Close();
			}
			flag = mOptions != null;
		}
		catch (Exception)
		{
			flag = false;
		}
		if (!flag)
		{
			mOptions = global::Options.CreateForNewPlayer();
			SaveOptions();
		}
	}

	public void SaveOptions()
	{
		try
		{
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(Constants.OPTIONS_FILE))
			{
				mOptions.Serialize(bIJEncryptDataWriter);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	public void LoadMetadata()
	{
		bool flag = false;
		try
		{
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(Constants.METADATA_FILE))
			{
				mPlayerMetaData = PlayerMetaData.Deserialize(bIJEncryptDataReader, 1290);
				bIJEncryptDataReader.Close();
			}
			flag = mPlayerMetaData != null;
		}
		catch (Exception)
		{
			flag = false;
		}
		if (!flag)
		{
			mPlayerMetaData = new PlayerMetaData();
			SaveMetadata();
		}
	}

	public void SaveMetadata()
	{
		try
		{
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(Constants.METADATA_FILE))
			{
				mPlayerMetaData.Serialize(bIJEncryptDataWriter, 1290);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	public void LoadPlayerData()
	{
		bool flag = false;
		BIJBinaryReader bIJBinaryReader = null;
		bool flag2 = false;
		UserDataInfo instance = UserDataInfo.Instance;
		string text = Path.Combine(BIJDataBinaryReader.GetBasePath(), Constants.PLAYER_FILE_OLD);
		string pLAYER_FILE = Constants.PLAYER_FILE;
		mPlayerFilePath = Path.Combine(BIJDataBinaryReader.GetBasePath(), pLAYER_FILE);
		if (File.Exists(mPlayerFilePath) || File.Exists(text))
		{
			instance.mDataState = UserDataInfo.DATA_STATE.LOAD_ERROR;
			try
			{
				string pLAYER_FILE_HASH = Constants.PLAYER_FILE_HASH;
				byte[] buffer = new byte[0];
				if (File.Exists(mPlayerFilePath))
				{
					using (BIJBinaryReader bIJBinaryReader2 = new BIJZippedEncryptFileBinaryReader(mPlayerFilePath, pLAYER_FILE_HASH))
					{
						buffer = bIJBinaryReader2.ReadAll();
					}
				}
				else
				{
					mPlayerFilePath = text;
					using (BIJBinaryReader bIJBinaryReader3 = new BIJZippedOneFileBinaryReader(mPlayerFilePath, pLAYER_FILE_HASH))
					{
						byte[] bytes = bIJBinaryReader3.ReadAll();
						buffer = CryptUtils.DecryptRJ128(bytes);
					}
				}
				bIJBinaryReader = new BIJMemoryBinaryReader(buffer);
				mPlayer = global::Player.Deserialize(bIJBinaryReader);
				flag = mPlayer != null;
			}
			catch (Exception)
			{
				flag = false;
				flag2 = true;
			}
			finally
			{
				if (bIJBinaryReader != null)
				{
					try
					{
						bIJBinaryReader.Close();
						bIJBinaryReader.Dispose();
					}
					catch (Exception)
					{
					}
					bIJBinaryReader = null;
				}
			}
			try
			{
				mPlayerFileSize = new FileInfo(mPlayerFilePath).Length;
			}
			catch (Exception)
			{
				mPlayerFileSize = 0L;
			}
		}
		else
		{
			instance.mDataState = UserDataInfo.DATA_STATE.NONE;
			mPlayerFileSize = -1L;
		}
		if (flag)
		{
			instance.mDataState = UserDataInfo.DATA_STATE.LOADED;
			if (mConcierge != null)
			{
				mConcierge.UpdateUserSegment();
			}
			if (!mOptions.ServerAlreadeyFirstSavedFlag)
			{
				mNeedBackupCheck = true;
			}
			else
			{
				mNeedBackupCheck = false;
			}
			if (mPlayerFilePath != text && File.Exists(text))
			{
				File.Delete(text);
			}
			return;
		}
		mNeedBackupCheck = true;
		mPlayer = global::Player.CreateForNewPlayer();
		SavePlayerData();
		if (!mOptions.ServerForceSaveRequestByBackup)
		{
			mOptions.ServerAlreadeyFirstSavedFlag = false;
			SaveOptions();
		}
		if (mPlayerFileSize > 0 && flag2)
		{
			mPlayer.Data.SumError = true;
			mPlayerFileSize = 0L;
		}
	}

	public bool CountPlayerBoot(out int playspan)
	{
		bool result = false;
		long num = DateTimeUtil.BetweenDays0(mPlayer.Data.LastPlayTime, DateTime.Now);
		if (mPlayer.Data.LastPlayTime == DateTimeUtil.UnitLocalTimeEpoch)
		{
			num = 0L;
		}
		long num2 = DateTimeUtil.BetweenDays0(mPlayer.Data.LastBootTime, DateTime.Now);
		if (mPlayer.Data.LastBootTime == DateTimeUtil.UnitLocalTimeEpoch)
		{
			num2 = 1L;
		}
		if (num >= 1)
		{
			mPlayer.Data.IntervalCount = num;
			mPlayer.ApplyFriends.Clear();
		}
		if (num == 1)
		{
			mPlayer.Data.ContinuousCount += num2;
		}
		else if (num == 0L)
		{
			if (mPlayer.Data.LastBootTime == DateTimeUtil.UnitLocalTimeEpoch)
			{
				mPlayer.Data.ContinuousCount = 1L;
			}
		}
		else if (num >= 0)
		{
			mPlayer.Data.ContinuousCount = 1L;
		}
		playspan = (int)num2;
		if (num2 >= 1)
		{
			mPlayer.Data.LastBootTime = DateTime.Now;
			mPlayer.Data.BootCount++;
			result = true;
		}
		return result;
	}

	public bool ExistsLocalFile(string a_path)
	{
		string persistentDataPath = Application.persistentDataPath;
		string path = Path.Combine(persistentDataPath, a_path);
		return File.Exists(path);
	}

	public void DeleteLocalFile(string a_path)
	{
		string persistentDataPath = Application.persistentDataPath;
		string path = Path.Combine(persistentDataPath, a_path);
		if (File.Exists(path))
		{
			File.Delete(path);
		}
	}

	public void ResetPlayerData(bool complete)
	{
		string persistentDataPath = Application.persistentDataPath;
		string path = Path.Combine(persistentDataPath, Constants.PLAYER_FILE);
		if (File.Exists(path))
		{
			File.Delete(path);
		}
		path = Path.Combine(persistentDataPath, Constants.METADATA_FILE);
		if (File.Exists(path))
		{
			File.Delete(path);
		}
		InitGameStateArg();
		GameStateSMMap.InitStaticData();
		mPlayer = global::Player.CreateForNewPlayer();
		Save();
		mConcierge.ResetGameConcierge();
		if (complete)
		{
			BIJSNS bIJSNS = SocialManager.Instance[SNS.Facebook];
			if (bIJSNS != null)
			{
				bIJSNS.Logout(null, null);
			}
			path = Path.Combine(persistentDataPath, Constants.OPTIONS_FILE);
			if (File.Exists(path))
			{
				File.Delete(path);
			}
			mOptions = global::Options.CreateForNewPlayer();
		}
		else
		{
			mOptions.ServerAlreadeyFirstSavedFlag = false;
		}
		SaveOptions();
	}

	public void SavePlayerData()
	{
		SavePlayerData(mPlayer);
	}

	public void SavePlayerData(Player player)
	{
		try
		{
			string pLAYER_FILE_HASH = Constants.PLAYER_FILE_HASH;
			string path = Path.Combine(BIJDataBinaryWriter.GetBasePath(), Constants.PLAYER_FILE);
			using (BIJBinaryWriter bIJBinaryWriter = new BIJZippedEncryptFileBinaryWriter(path, pLAYER_FILE_HASH))
			{
				player.Serialize(bIJBinaryWriter);
				bIJBinaryWriter.Flush();
			}
		}
		catch (Exception)
		{
		}
	}

	public void Save()
	{
		Save(false, false, false);
	}

	public void Save(bool saveToServer, bool forceSave)
	{
		Save(saveToServer, forceSave, false);
	}

	public void Save(bool saveToServer, bool forceSave, bool wantSave)
	{
		SavePlayerData();
		SaveMetadata();
		if (saveToServer)
		{
			SendSaveData(forceSave, wantSave);
		}
	}

	public void SendSaveData(bool forceSave, bool wantSave)
	{
		try
		{
			using (BIJBinaryReader dataReader = new BIJDataBinaryReader(Constants.PLAYER_FILE))
			{
				using (BIJBinaryReader metaReader = new BIJDataBinaryReader(Constants.METADATA_FILE))
				{
					Network_SaveGameState(dataReader, metaReader, forceSave, wantSave);
				}
			}
		}
		catch (Exception)
		{
			mSaveGameStateDone = true;
		}
	}

	public void LoadAdvSaveData()
	{
	}

	public void SaveAdvData(bool sendServer = false, bool force = false, bool want = false)
	{
		SaveAdvData(mPlayer.AdvSaveData, force, sendServer, want);
	}

	public void SaveAdvData(AdvSaveData advsavedata, bool force, bool sendServer, bool want)
	{
	}

	public void SendAdvSaveData(bool force, bool want)
	{
		try
		{
			using (BIJBinaryReader dataReader = new BIJDataBinaryReader(Constants.ADV_FILE))
			{
				using (BIJBinaryReader metaReader = new BIJDataBinaryReader(Constants.METADATA_FILE))
				{
					Network_AdvSendSaveData(dataReader, metaReader, force, want);
				}
			}
		}
		catch (Exception)
		{
			mAdvSendSaveDataCheckDone = true;
		}
	}

	public void LoadLocalMTData()
	{
	}

	public void LoadMTData()
	{
	}

	public void SaveMTData()
	{
	}

	public void ResetAdvData()
	{
	}

	public void ResetAdvMTData()
	{
	}

	public void InitializeStageStatus(Def.SERIES a_series, SMMapSsData a_routeData, int a_eventID = -1, short a_courseID = -1)
	{
		if (Def.IsAdvSeries(a_series) || a_routeData == null)
		{
			return;
		}
		SMMapPageData sMMapPageData;
		if (a_series == Def.SERIES.SM_EV)
		{
			if (a_eventID == -1)
			{
				return;
			}
			sMMapPageData = GetEventPageData(a_eventID);
		}
		else
		{
			sMMapPageData = GetSeriesMapPageData(a_series);
		}
		switch (a_series)
		{
		case Def.SERIES.SM_EV:
		{
			SMEventPageData sMEventPageData = sMMapPageData as SMEventPageData;
			if (a_courseID == -1)
			{
				break;
			}
			PlayerEventData data;
			mPlayer.Data.GetMapData(a_series, a_eventID, out data);
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			switch (sMEventPageData.EventSetting.EventType)
			{
			case Def.EVENT_TYPE.SM_RALLY:
			{
				int nextRoadBlockLevel = playerMapData.NextRoadBlockLevel;
				int playableMaxLevel = playerMapData.PlayableMaxLevel;
				int openNoticeLevel = playerMapData.OpenNoticeLevel;
				if (playableMaxLevel != 100)
				{
					break;
				}
				List<SMRoadBlockSetting> roadBlocks = sMEventPageData.GetRoadBlocks(a_courseID);
				for (int i = 0; i < roadBlocks.Count; i++)
				{
					int stageNo = Def.GetStageNo(roadBlocks[i].mStageNo, roadBlocks[i].mSubNo);
					Player.STAGE_STATUS value;
					if (playerMapData.LineStatus.TryGetValue(stageNo, out value))
					{
						playerMapData.LineStatus[stageNo] = global::Player.STAGE_STATUS.LOCK;
					}
					else
					{
						playerMapData.LineStatus.Add(stageNo, global::Player.STAGE_STATUS.LOCK);
					}
					data.CourseData[a_courseID] = playerMapData;
				}
				mPlayer.Data.SetMapData(a_eventID, data);
				int num = -1;
				int a_main;
				int a_sub;
				Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
				SMChapterSetting chapterSearch = sMEventPageData.GetChapterSearch(a_main, a_courseID);
				if (chapterSearch != null)
				{
					num = Def.GetStageNo(chapterSearch.mSubNo, 0);
				}
				chapterSearch = sMEventPageData.GetChapterSearch(a_main, a_courseID);
				num = Def.GetStageNo(chapterSearch.mSubNo, 0);
				List<int> list = new List<int>();
				List<SMMapRoute<float>.RouteNode> stages = a_routeData.GetStages(playableMaxLevel, num, true);
				for (int j = 0; j < stages.Count; j++)
				{
					SMMapRoute<float>.RouteNode routeNode = stages[j];
					int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
					Player.STAGE_STATUS value2;
					if (playerMapData.StageStatus.TryGetValue(stageNoByRouteOrder, out value2))
					{
						if (value2 == global::Player.STAGE_STATUS.NOOPEN)
						{
							list.Add(stageNoByRouteOrder);
						}
					}
					else
					{
						list.Add(stageNoByRouteOrder);
					}
				}
				{
					foreach (int item in list)
					{
						if (mPlayer.GetStageStatus(a_series, a_eventID, a_courseID, item) == global::Player.STAGE_STATUS.NOOPEN)
						{
							mPlayer.SetStageStatus(a_series, a_eventID, a_courseID, item, global::Player.STAGE_STATUS.LOCK);
						}
					}
					break;
				}
			}
			case Def.EVENT_TYPE.SM_RALLY2:
				break;
			case Def.EVENT_TYPE.SM_STAR:
				break;
			}
			break;
		}
		}
	}

	public void MakeMapInitializeData(Def.SERIES a_series, int a_eventID = -1, short a_courseID = -1)
	{
		if (Def.IsAdvSeries(a_series))
		{
			return;
		}
		SMMapPageData sMMapPageData;
		if (a_series == Def.SERIES.SM_EV)
		{
			if (a_eventID == -1)
			{
				return;
			}
			sMMapPageData = GetEventPageData(a_eventID);
		}
		else
		{
			sMMapPageData = GetSeriesMapPageData(a_series);
		}
		if (sMMapPageData == null)
		{
			return;
		}
		if (a_series == Def.SERIES.SM_EV)
		{
			SMEventPageData sMEventPageData = sMMapPageData as SMEventPageData;
			mPlayer.CreateEventData(a_eventID, (short)sMEventPageData.EventCourseList.Count, sMEventPageData.EventSetting.EventType);
			PlayerEventData data;
			switch (sMEventPageData.EventSetting.EventType)
			{
			case Def.EVENT_TYPE.SM_BINGO:
				if (!mPlayer.Data.mBingoEventDataDict.ContainsKey(a_eventID))
				{
					mPlayer.Data.MakeBingoEventData(a_eventID);
				}
				break;
			case Def.EVENT_TYPE.SM_LABYRINTH:
				mPlayer.Data.GetMapData(a_series, a_eventID, out data);
				if (data.LabyrinthData == null)
				{
					data.LabyrinthData = new PlayerEventLabyrinthData();
					data.LabyrinthData.Init();
				}
				break;
			case Def.EVENT_TYPE.SM_RALLY:
			{
				if (a_courseID == -1)
				{
					break;
				}
				mPlayer.Data.GetMapData(a_series, a_eventID, out data);
				PlayerMapData playerMapData3 = data.CourseData[a_courseID];
				foreach (KeyValuePair<string, SMRoadBlockSetting> roadBlockData in sMEventPageData.RoadBlockDataList)
				{
					SMEventRoadBlockSetting sMEventRoadBlockSetting3 = roadBlockData.Value as SMEventRoadBlockSetting;
					if (sMEventRoadBlockSetting3 == null || sMEventRoadBlockSetting3.mCourseNo != a_courseID || sMEventRoadBlockSetting3.mSubNo != 0)
					{
						continue;
					}
					int roadBlockLevel = sMEventRoadBlockSetting3.RoadBlockLevel;
					playerMapData3.NextRoadBlockLevel = roadBlockLevel;
					break;
				}
				playerMapData3.MaxLevel = Def.GetStageNo(sMEventPageData.GetMap(a_courseID).MaxLevel, 0);
				data.CourseData[a_courseID] = playerMapData3;
				mPlayer.Data.SetMapData(a_eventID, data);
				break;
			}
			case Def.EVENT_TYPE.SM_RALLY2:
			{
				mPlayer.Data.GetMapData(a_series, a_eventID, out data);
				List<int> list3 = new List<int>(sMEventPageData.EventCourseList.Keys);
				list3.Sort();
				{
					foreach (int item in list3)
					{
						short num2 = (short)item;
						PlayerMapData playerMapData2 = data.CourseData[num2];
						if (playerMapData2.NextRoadBlockLevel != 0)
						{
							continue;
						}
						List<SMRoadBlockSetting> list4 = new List<SMRoadBlockSetting>(sMEventPageData.RoadBlockDataList.Values);
						list4.Sort((SMRoadBlockSetting a, SMRoadBlockSetting b) => a.RoadBlockLevel - b.RoadBlockLevel);
						foreach (SMRoadBlockSetting item2 in list4)
						{
							if (item2 == null || item2.mCourseNo != num2 || item2.mSubNo != 0)
							{
								continue;
							}
							int stageNo3 = Def.GetStageNo(item2.mStageNo, 0);
							playerMapData2.NextRoadBlockLevel = stageNo3;
							break;
						}
						SMEventMapSetting sMEventMapSetting2 = sMEventPageData.GetMap(num2) as SMEventMapSetting;
						int a_rbNo2 = sMEventMapSetting2.CharaGetRoadblock[0];
						SMEventRoadBlockSetting sMEventRoadBlockSetting2 = sMEventPageData.GetRoadBlockByRBNo(a_rbNo2, num2) as SMEventRoadBlockSetting;
						int stageNo4 = sMEventRoadBlockSetting2.StageNo;
						playerMapData2.MaxLevel = stageNo4;
						data.CourseData[num2] = playerMapData2;
						mPlayer.Data.SetMapData(a_eventID, data);
					}
					break;
				}
			}
			case Def.EVENT_TYPE.SM_STAR:
			{
				mPlayer.Data.GetMapData(a_series, a_eventID, out data);
				List<int> list = new List<int>(sMEventPageData.EventCourseList.Keys);
				list.Sort();
				{
					foreach (int item3 in list)
					{
						short num = (short)item3;
						PlayerMapData playerMapData = data.CourseData[num];
						if (playerMapData.NextRoadBlockLevel != 0)
						{
							continue;
						}
						List<SMRoadBlockSetting> list2 = new List<SMRoadBlockSetting>(sMEventPageData.RoadBlockDataList.Values);
						list2.Sort((SMRoadBlockSetting a, SMRoadBlockSetting b) => a.RoadBlockLevel - b.RoadBlockLevel);
						foreach (SMRoadBlockSetting item4 in list2)
						{
							if (item4 == null || item4.mCourseNo != num || item4.mSubNo != 0)
							{
								continue;
							}
							int stageNo = Def.GetStageNo(item4.mStageNo, 0);
							playerMapData.NextRoadBlockLevel = stageNo;
							break;
						}
						SMEventMapSetting sMEventMapSetting = sMEventPageData.GetMap(num) as SMEventMapSetting;
						int a_rbNo = sMEventMapSetting.CharaGetRoadblock[0];
						SMEventRoadBlockSetting sMEventRoadBlockSetting = sMEventPageData.GetRoadBlockByRBNo(a_rbNo, num) as SMEventRoadBlockSetting;
						int stageNo2 = sMEventRoadBlockSetting.StageNo;
						playerMapData.MaxLevel = stageNo2;
						data.CourseData[num] = playerMapData;
						mPlayer.Data.SetMapData(a_eventID, data);
					}
					break;
				}
			}
			case Def.EVENT_TYPE.SM_ADV:
				break;
			}
			return;
		}
		PlayerMapData data2;
		mPlayer.Data.GetMapData(a_series, out data2);
		List<SMRoadBlockSetting> roadBlockInCourse = sMMapPageData.GetRoadBlockInCourse();
		foreach (SMRoadBlockSetting item5 in roadBlockInCourse)
		{
			if (item5.mSubNo != 0)
			{
				continue;
			}
			int roadBlockLevel2 = item5.RoadBlockLevel;
			data2.NextRoadBlockLevel = roadBlockLevel2;
			break;
		}
		data2.MaxLevel = Def.GetStageNo(sMMapPageData.GetMap().MaxLevel, 0);
		mPlayer.SetStageStatus(a_series, 100, global::Player.STAGE_STATUS.UNLOCK);
		mPlayer.Data.SetMapData(a_series, data2);
	}

	public void SaveServerCramVariableData()
	{
		try
		{
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter("bi_var.dat"))
			{
				mServerCramVariableData.Serialize(bIJEncryptDataWriter);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	public void LoadServerCramVariableData()
	{
		bool flag = false;
		try
		{
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader("bi_var.dat"))
			{
				mServerCramVariableData = ServerCramVariableData.Deserialize(bIJEncryptDataReader);
				bIJEncryptDataReader.Close();
			}
			flag = mServerCramVariableData != null;
		}
		catch (Exception)
		{
			flag = false;
		}
		if (!flag)
		{
			mServerCramVariableData = ServerCramVariableData.CreateForNewPlayer();
			SaveServerCramVariableData();
		}
	}

	public long GetLifeRecoverSec()
	{
		DateTime tdt = mPlayer.Data.OldestLifeUseTime.AddSeconds(Constants.LIFE_RECOVER_SEC);
		return DateTimeUtil.BetweenSeconds(DateTime.Now, tdt);
	}

	public DateTime GetFullRecoverDateTime()
	{
		int itemCount = mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1);
		if (itemCount >= mPlayer.Data.MaxNormalLifeCount)
		{
			return DateTimeUtil.UnitLocalTimeEpoch;
		}
		return mPlayer.Data.OldestLifeUseTime.AddSeconds(Constants.LIFE_RECOVER_SEC * (mPlayer.Data.MaxNormalLifeCount - itemCount));
	}

	public long GetMugenHeartEndSec()
	{
		DateTime tdt = mPlayer.Data.MugenHeartUseTime.AddSeconds(mPlayer.Data.mUseMugenHeartSetTime);
		return DateTimeUtil.BetweenSeconds(DateTime.Now.AddSeconds(mServerTimeDiff * -1.0), tdt);
	}

	private void Update()
	{
		frameDeltaMul = Time.deltaTime / (1f / (float)Application.targetFrameRate);
		if (!mIsStarted)
		{
			return;
		}
		mTutorialManager.Update(Time.deltaTime);
		if (USE_DEBUG_DIALOG)
		{
		}
		bool flag = BIJUnity.isAvailableRotation();
		if (mAvailableRotation != flag)
		{
			if (flag)
			{
				Screen.autorotateToLandscapeLeft = true;
				Screen.autorotateToLandscapeRight = true;
				Screen.autorotateToPortrait = true;
				Screen.autorotateToPortraitUpsideDown = true;
			}
			else
			{
				Screen.autorotateToLandscapeLeft = false;
				Screen.autorotateToLandscapeRight = false;
				Screen.autorotateToPortrait = false;
				Screen.autorotateToPortraitUpsideDown = false;
				switch (Util.ScreenOrientation)
				{
				case ScreenOrientation.LandscapeLeft:
					Screen.autorotateToLandscapeLeft = true;
					break;
				case ScreenOrientation.LandscapeRight:
					Screen.autorotateToLandscapeRight = true;
					break;
				case ScreenOrientation.Portrait:
					Screen.autorotateToPortrait = true;
					break;
				case ScreenOrientation.PortraitUpsideDown:
					Screen.autorotateToPortraitUpsideDown = true;
					break;
				}
			}
			mAvailableRotation = flag;
		}
		Vector2 vector = Util.LogScreenSize();
		CheckScreenRotate();
		if (IsLoaded)
		{
			if (Server.IsNetworking)
			{
				float num = Time.realtimeSinceStartup - mConnectingStart;
				if (!IsConnecting() && num > 0.5f)
				{
					StartConnecting();
				}
			}
			else if (IsConnecting())
			{
				FinishConnecting();
			}
			if (mPlayer != null)
			{
				mPlayer.Data.TotalPlayTime += Time.deltaTime;
				mPlayer.UpdateLifeCount(Time.deltaTime);
				mPlayer.UpdateMugenHeartState();
				mPlayer.UpdateAdvStaminaCount();
			}
		}
		if (mTouchDownTrg)
		{
			mTouchDownTrg = false;
		}
		if (mTouchUpTrg)
		{
			mTouchUpTrg = false;
		}
		if (mBackKeyTrg)
		{
			mBackKeyTrg = false;
		}
		if (mInputEnable)
		{
			InputCheck();
			return;
		}
		mTouchCnd = false;
		mTouchWasDrag = false;
		mTouchTime = 0;
		mTouchTimeSec = 0f;
		mTouchPosOld = mTouchPos;
		mBackKeyCnd = false;
	}

	private void InputCheck()
	{
		if (mUICamera.useMouse)
		{
			if (Input.GetMouseButton(0))
			{
				Vector3 mousePosition = Input.mousePosition;
				mTouchPosOld = mTouchPos;
				mTouchPos = new Vector2(mousePosition.x, mousePosition.y);
				if (!mTouchCnd)
				{
					mTouchDownTrg = true;
					mTouchWasDrag = false;
					InvokeCallbackEvent(EVENT_TYPE.OnTouchDown);
				}
				mTouchCnd = true;
				mTouchTime++;
				mTouchTimeSec += Time.deltaTime;
				if (!mTouchDownTrg && mTouchPosOld != mTouchPos)
				{
					mTouchWasDrag = true;
				}
			}
			else
			{
				if (mTouchCnd)
				{
					mTouchUpTrg = true;
					InvokeCallbackEvent(EVENT_TYPE.OnTouchUp);
				}
				mTouchCnd = false;
				mTouchWasDrag = false;
				mTouchTime = 0;
				mTouchTimeSec = 0f;
				mTouchPosOld = mTouchPos;
			}
		}
		else
		{
			mTouchCount = Input.touchCount;
			if (Input.touchCount > 0)
			{
				Touch touch = Input.GetTouch(0);
				mTouchPosOld = mTouchPos;
				mTouchPos = touch.position;
				if (touch.phase == TouchPhase.Began)
				{
					mTouchDownTrg = true;
					mTouchCnd = true;
					InvokeCallbackEvent(EVENT_TYPE.OnTouchDown);
				}
				else if (touch.phase == TouchPhase.Ended)
				{
					mTouchUpTrg = true;
					mTouchCnd = false;
					mTouchTime = 0;
					mTouchTimeSec = 0f;
					InvokeCallbackEvent(EVENT_TYPE.OnTouchUp);
				}
				else if (touch.phase == TouchPhase.Stationary)
				{
					mTouchCnd = true;
					mTouchTime++;
					mTouchTimeSec += Time.deltaTime;
				}
				else if (touch.phase == TouchPhase.Moved)
				{
					float num = mTouchPosOld.x - mTouchPos.x;
					float num2 = mTouchPosOld.y - mTouchPos.y;
					float num3 = Mathf.Sqrt(num * num + num2 * num2);
					if (num3 > 10f)
					{
						mTouchCnd = true;
						mTouchWasDrag = true;
						mTouchTime = 0;
						mTouchTimeSec = 0f;
					}
					else
					{
						mTouchCnd = true;
						mTouchTime++;
						mTouchTimeSec += Time.deltaTime;
					}
				}
				else
				{
					mTouchCnd = false;
					mTouchWasDrag = false;
					mTouchTime = 0;
					mTouchTimeSec = 0f;
				}
			}
			else
			{
				mTouchCnd = false;
				mTouchWasDrag = false;
				mTouchTime = 0;
				mTouchTimeSec = 0f;
				mTouchPosOld = mTouchPos;
			}
		}
		if (Input.GetKey(KeyCode.Escape))
		{
			if (!mBackKeyCnd)
			{
				mBackKeyTrg = true;
				InvokeCallbackEvent(EVENT_TYPE.OnBackKey);
			}
			mBackKeyCnd = true;
		}
		else
		{
			mBackKeyCnd = false;
		}
	}

	public void ResetTouch()
	{
		mTouchCnd = false;
		mTouchWasDrag = false;
		mTouchTime = 0;
		mTouchTimeSec = 0f;
		mTouchPosOld = mTouchPos;
	}

	public void LoadBoosterData(string name)
	{
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			short num = bIJResourceBinaryReader.ReadShort();
			for (int i = 0; i < num; i++)
			{
				BoosterData boosterData = new BoosterData();
				boosterData.deserialize(bIJResourceBinaryReader);
				mBoosterData.Add(boosterData);
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void LoadTileData(string name)
	{
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			mTileData_Normal.Clear();
			short num = bIJResourceBinaryReader.ReadShort();
			for (int i = 0; i < num; i++)
			{
				TileData tileData = new TileData();
				tileData.deserialize(bIJResourceBinaryReader);
				mTileData_Normal.Add(tileData);
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void LoadDailyChallenge(string name)
	{
		ScriptableObjectContainer scriptableObjectContainer = Resources.Load<ScriptableObjectContainer>(name);
		DailyChallengeSheetList dailyChallengeSheetList = ((!(scriptableObjectContainer != null)) ? null : (scriptableObjectContainer.Data as DailyChallengeSheetList));
		mDailyChallengeData.Clear();
		if (!(dailyChallengeSheetList != null))
		{
			return;
		}
		string[] sheetNames = dailyChallengeSheetList.SheetNames;
		if (sheetNames == null)
		{
			return;
		}
		string[] array = sheetNames;
		foreach (string sheet_name in array)
		{
			DailyChallengeSheet sheet = dailyChallengeSheetList.GetSheet(sheet_name);
			if (sheet != null)
			{
				mDailyChallengeData.Add(sheet);
			}
		}
	}

	public void LoadAdvTileData(string name)
	{
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			mTileData_Adv.Clear();
			short num = bIJResourceBinaryReader.ReadShort();
			for (int i = 0; i < num; i++)
			{
				TileData tileData = new TileData();
				tileData.deserialize(bIJResourceBinaryReader);
				mTileData_Adv.Add(tileData);
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void SwitchTileData(bool _adv)
	{
		if (!_adv)
		{
			mTileData = mTileData_Normal;
		}
		else
		{
			mTileData = mTileData_Adv;
		}
	}

	public void LoadShopData(string name)
	{
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			short num = bIJResourceBinaryReader.ReadShort();
			for (int i = 0; i < num; i++)
			{
				ShopItemData shopItemData = new ShopItemData();
				shopItemData.deserialize(bIJResourceBinaryReader);
				mShopItemData.Add(shopItemData.Index, shopItemData);
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void LoadDownloadShopData()
	{
		if (mDLManager == null)
		{
			return;
		}
		BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream("Shop_Ex.bin.bytes");
		if (downloadFileReadStream == null)
		{
			return;
		}
		mShopItemData.Clear();
		LoadShopData("Data/Shop.bin");
		short num = downloadFileReadStream.ReadShort();
		for (short num2 = 0; num2 < num; num2++)
		{
			ShopItemData shopItemData = new ShopItemData();
			shopItemData.deserialize(downloadFileReadStream);
			if (mShopItemData.ContainsKey(shopItemData.Index))
			{
				mShopItemData[shopItemData.Index] = shopItemData;
			}
			else
			{
				mShopItemData.Add(shopItemData.Index, shopItemData);
			}
		}
		downloadFileReadStream.Close();
	}

	public bool IsBoosterInSale(Def.BOOSTER_KIND a_booster)
	{
		bool result = false;
		BoosterIncludeSaleList = new Dictionary<Def.BOOSTER_KIND, List<int>>();
		if (!mTutorialManager.IsInitialTutorialCompleted())
		{
			return false;
		}
		List<int> list = new List<int>();
		if (mSegmentProfile.ShopAllCrushItemList != null && mSegmentProfile.ShopAllCrushItemList.Count > 0)
		{
			for (int i = 0; i < mSegmentProfile.ShopAllCrushItemList.Count; i++)
			{
				ShopLimitedTime shopLimitedTime = mSegmentProfile.ShopAllCrushItemList[i];
				if (!shopLimitedTime.IsEnable())
				{
					continue;
				}
				int shopItemID = shopLimitedTime.ShopItemID;
				if (!mShopItemData.ContainsKey(shopItemID))
				{
					continue;
				}
				int num = 0;
				int num2 = 0;
				int num3 = 0;
				num3 = SegmentProfile.ShopAllCrushItemList[i].ShopItemID;
				num = SegmentProfile.ShopAllCrushItemList[i].ShopItemLimit;
				num2 = SegmentProfile.ShopAllCrushItemList[i].ShopCount;
				if (mPlayer.IsEnablePurchaseItem(num3, num, num2))
				{
					ShopItemData shopItemData = mShopItemData[shopItemID];
					if (shopItemData.ItemDetail.IsBoosterInclude(a_booster))
					{
						list.Add(shopItemID);
						result = true;
					}
				}
			}
		}
		if (mSegmentProfile.ShopSetItemList != null && mSegmentProfile.ShopSetItemList.Count > 0)
		{
			for (int j = 0; j < mSegmentProfile.ShopSetItemList.Count; j++)
			{
				ShopLimitedTime shopLimitedTime2 = mSegmentProfile.ShopSetItemList[j];
				if (!shopLimitedTime2.IsEnable())
				{
					continue;
				}
				int shopItemID2 = shopLimitedTime2.ShopItemID;
				if (!mShopItemData.ContainsKey(shopItemID2))
				{
					continue;
				}
				int num4 = 0;
				int num5 = 0;
				int num6 = 0;
				num6 = SegmentProfile.ShopSetItemList[j].ShopItemID;
				num4 = SegmentProfile.ShopSetItemList[j].ShopItemLimit;
				num5 = SegmentProfile.ShopSetItemList[j].ShopCount;
				if (mPlayer.IsEnablePurchaseItem(num6, num4, num5))
				{
					ShopItemData shopItemData2 = mShopItemData[shopItemID2];
					if (shopItemData2.ItemDetail.IsBoosterInclude(a_booster))
					{
						list.Add(shopItemID2);
						result = true;
					}
				}
			}
		}
		if (BoosterIncludeSaleList.ContainsKey(a_booster))
		{
			BoosterIncludeSaleList[a_booster] = list;
		}
		else
		{
			BoosterIncludeSaleList.Add(a_booster, list);
		}
		return result;
	}

	public bool TryGetBoosterIncludedSaleList(Def.BOOSTER_KIND a_booster, out int[] result)
	{
		result = null;
		IsBoosterInSale(a_booster);
		if (BoosterIncludeSaleList != null && BoosterIncludeSaleList.ContainsKey(a_booster))
		{
			result = BoosterIncludeSaleList[a_booster].ToArray();
		}
		return result != null;
	}

	public void ClearBoosterIncludedSaleList()
	{
		if (BoosterIncludeSaleList != null)
		{
			BoosterIncludeSaleList.Clear();
		}
	}

	public void LoadTrophyExchangeData(string name)
	{
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			short num = bIJResourceBinaryReader.ReadShort();
			for (int i = 0; i < num; i++)
			{
				TrophyExchangeItemData trophyExchangeItemData = new TrophyExchangeItemData();
				trophyExchangeItemData.deserialize(bIJResourceBinaryReader);
				mTrophyExchangeItemData.Add(trophyExchangeItemData.Index, trophyExchangeItemData);
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void LoadDownloadTrophyExchangeData()
	{
		if (mDLManager == null)
		{
			return;
		}
		BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream("TrophyExchange_Ex.bin.bytes");
		if (downloadFileReadStream == null)
		{
			return;
		}
		mTrophyExchangeItemData.Clear();
		LoadTrophyExchangeData("Data/TrophyExchange.bin");
		short num = downloadFileReadStream.ReadShort();
		for (short num2 = 0; num2 < num; num2++)
		{
			TrophyExchangeItemData trophyExchangeItemData = new TrophyExchangeItemData();
			trophyExchangeItemData.deserialize(downloadFileReadStream);
			if (mTrophyExchangeItemData.ContainsKey(trophyExchangeItemData.Index))
			{
				mTrophyExchangeItemData[trophyExchangeItemData.Index] = trophyExchangeItemData;
			}
			else
			{
				mTrophyExchangeItemData.Add(trophyExchangeItemData.Index, trophyExchangeItemData);
			}
		}
		downloadFileReadStream.Close();
	}

	public void LoadEventData(string name)
	{
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			mEventData = new SMEventData();
			mEventData.Deserialize(bIJResourceBinaryReader);
			bIJResourceBinaryReader.Close();
		}
	}

	public void LoadDownloadEventData()
	{
		if (!(mDLManager == null))
		{
			BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream("EventData_Ex.bin.bytes");
			if (downloadFileReadStream != null)
			{
				mEventData.InSessionEventList.Clear();
				mEventData.RevivalEventList.Clear();
				mEventData = null;
				LoadEventData("Data/Event/EventData.bin");
				mEventData.Deserialize(downloadFileReadStream);
				downloadFileReadStream.Close();
			}
		}
	}

	public void LoadSpecialDailyData(string name)
	{
		mSpecialDailyEventData.Clear();
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			int num = bIJResourceBinaryReader.ReadInt();
			for (int i = 0; i < num; i++)
			{
				SMSpecialDailyEventData sMSpecialDailyEventData = new SMSpecialDailyEventData();
				sMSpecialDailyEventData.Deserialize(bIJResourceBinaryReader);
				if (!mSpecialDailyEventData.ContainsKey(sMSpecialDailyEventData.ID))
				{
					mSpecialDailyEventData.Add(sMSpecialDailyEventData.ID, sMSpecialDailyEventData);
				}
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void LoadDownloadSpecialDailyData()
	{
		if (mDLManager == null)
		{
			return;
		}
		BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream("SpecialDailyData_Ex.bin.bytes");
		if (downloadFileReadStream == null)
		{
			return;
		}
		LoadSpecialDailyData("Data/Event/SpecialDailyData.bin");
		int num = downloadFileReadStream.ReadInt();
		for (int i = 0; i < num; i++)
		{
			SMSpecialDailyEventData sMSpecialDailyEventData = new SMSpecialDailyEventData();
			sMSpecialDailyEventData.Deserialize(downloadFileReadStream);
			int iD = sMSpecialDailyEventData.ID;
			if (!mSpecialDailyEventData.ContainsKey(iD))
			{
				mSpecialDailyEventData.Add(iD, sMSpecialDailyEventData);
			}
			else
			{
				mSpecialDailyEventData[iD] = sMSpecialDailyEventData;
			}
		}
		downloadFileReadStream.Close();
	}

	public void LoadAlternateRewardData(string name)
	{
		mAlternateRewardData.Clear();
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			int num = bIJResourceBinaryReader.ReadInt();
			for (int i = 0; i < num; i++)
			{
				SMAlternateRewardData sMAlternateRewardData = new SMAlternateRewardData();
				sMAlternateRewardData.Deserialize(bIJResourceBinaryReader);
				if (!mAlternateRewardData.ContainsKey(sMAlternateRewardData.BaseAccessoryID))
				{
					mAlternateRewardData.Add(sMAlternateRewardData.BaseAccessoryID, sMAlternateRewardData);
				}
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void LoadDownloadAlternateRewardData()
	{
		if (mDLManager == null)
		{
			return;
		}
		BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream("AlternateRewardData_Ex.bin.bytes");
		if (downloadFileReadStream == null)
		{
			return;
		}
		LoadAlternateRewardData("Data/AlternateRewardData.bin");
		int num = downloadFileReadStream.ReadInt();
		for (int i = 0; i < num; i++)
		{
			SMAlternateRewardData sMAlternateRewardData = new SMAlternateRewardData();
			sMAlternateRewardData.Deserialize(downloadFileReadStream);
			int baseAccessoryID = sMAlternateRewardData.BaseAccessoryID;
			if (!mAlternateRewardData.ContainsKey(baseAccessoryID))
			{
				mAlternateRewardData.Add(baseAccessoryID, sMAlternateRewardData);
			}
			else
			{
				mAlternateRewardData[baseAccessoryID] = sMAlternateRewardData;
			}
		}
		downloadFileReadStream.Close();
	}

	public List<AccessoryData> GetAlternateReward(int a_accessory, bool a_lockedOnly = true)
	{
		List<AccessoryData> list = new List<AccessoryData>();
		if (mAlternateRewardData.ContainsKey(a_accessory))
		{
			SMAlternateRewardData sMAlternateRewardData = mAlternateRewardData[a_accessory];
			for (int i = 0; i < sMAlternateRewardData.RewardList.Count; i++)
			{
				int num = sMAlternateRewardData.RewardList[i];
				if (a_lockedOnly)
				{
					if (!mPlayer.IsAccessoryUnlock(num))
					{
						list.Add(GetAccessoryData(num));
					}
				}
				else
				{
					list.Add(GetAccessoryData(num));
				}
			}
		}
		return list;
	}

	public int GetAlternateRewardIdByBaseAccessoryId(int b_accessory)
	{
		foreach (KeyValuePair<int, SMAlternateRewardData> mAlternateRewardDatum in mAlternateRewardData)
		{
			if (mAlternateRewardDatum.Value == null || mAlternateRewardDatum.Value.BaseAccessoryID != b_accessory)
			{
				continue;
			}
			return mAlternateRewardDatum.Key;
		}
		return -1;
	}

	public static Material GetMixColorMixAlphaMarerial_PuzzleTile(Texture _tex)
	{
		if (mMixColorMixAlphaMarerial_PuzzleTile == null)
		{
			mMixColorMixAlphaMarerial_PuzzleTile = new Material(Shader.Find("BIJ/BIJ2DAlpha"));
			mMixColorMixAlphaMarerial_PuzzleTile.mainTexture = _tex;
		}
		return mMixColorMixAlphaMarerial_PuzzleTile;
	}

	public static void UnloadMixColorMixAlphaMarerial_PuzzleTile()
	{
		mMixColorMixAlphaMarerial_PuzzleTile = null;
	}

	public static void SafeDestroy(UnityEngine.Object obj)
	{
		if (obj != null)
		{
			if (Application.isEditor)
			{
				UnityEngine.Object.DestroyImmediate(obj);
			}
			else
			{
				UnityEngine.Object.Destroy(obj);
			}
		}
	}

	public static void SafeDestroy<T>(ref T mono) where T : MonoBehaviour
	{
		if (!((UnityEngine.Object)mono == (UnityEngine.Object)null) && !(mono.gameObject == null))
		{
			UnityEngine.Object.Destroy(mono.gameObject);
			mono = (T)null;
		}
	}

	private static UIAtlas CreateAtlasFromPrefab(string imageName)
	{
		GameObject gameObject = Resources.Load(imageName, typeof(GameObject)) as GameObject;
		return gameObject.GetComponent<UIAtlas>();
	}

	private static UIAtlas CreateAtlasFromPrefabWithInstantinate(string imageName)
	{
		GameObject original = Resources.Load(imageName, typeof(GameObject)) as GameObject;
		GameObject gameObject = UnityEngine.Object.Instantiate(original);
		gameObject.name = imageName.Replace('/', '_');
		original = null;
		return gameObject.GetComponent<UIAtlas>();
	}

	public static UIFont LoadFont()
	{
		if (mFont == null)
		{
			GameObject original = Resources.Load("Font/Font", typeof(GameObject)) as GameObject;
			GameObject gameObject = UnityEngine.Object.Instantiate(original);
			gameObject.name = "Font/Font".Replace('/', '_');
			gameObject.hideFlags = HideFlags.HideAndDontSave;
			original = null;
			mFont = gameObject.GetComponent<UIFont>();
		}
		return mFont;
	}

	public static void UnloadFont()
	{
		if (mFont != null)
		{
			SafeDestroy(mFont.gameObject);
			mFont = null;
		}
	}

	public string GetRBKeyImageName(Def.SERIES a_series)
	{
		string result = "icon_loadblock_key";
		switch (a_series)
		{
		case Def.SERIES.SM_R:
			result = "icon_loadblock_key_R";
			break;
		case Def.SERIES.SM_S:
			result = "icon_loadblock_key_S";
			break;
		case Def.SERIES.SM_SS:
			result = "icon_loadblock_key_SS";
			break;
		case Def.SERIES.SM_STARS:
			result = "icon_loadblock_key_STARS";
			break;
		case Def.SERIES.SM_GF00:
			result = "icon_loadblock_key_GF00";
			break;
		}
		return result;
	}

	public static GameObject LoadParticlePrefab(string path)
	{
		GameObject value = null;
		if (!mParticlePrefabs.TryGetValue(path, out value))
		{
			value = Resources.Load(path) as GameObject;
			if (value != null && value.GetComponent<ParticleSystem>() != null)
			{
				mParticlePrefabs.Add(path, value);
			}
		}
		return value;
	}

	public static GameObject GetParticle(string path, string name = null)
	{
		GameObject gameObject = null;
		GameObject gameObject2 = LoadParticlePrefab(path);
		if (gameObject2 != null && gameObject2.GetComponent<ParticleSystem>() != null)
		{
			gameObject = UnityEngine.Object.Instantiate(gameObject2, Vector3.zero, Quaternion.identity) as GameObject;
			if (gameObject != null)
			{
				if (!string.IsNullOrEmpty(name))
				{
					gameObject.name = name;
				}
				else
				{
					gameObject.name = gameObject2.name;
				}
			}
		}
		return gameObject;
	}

	public static void ClearParticle(bool force_default_clear = false)
	{
		List<string> list = new List<string>();
		foreach (string key in mParticlePrefabs.Keys)
		{
			bool flag = false;
			if (!force_default_clear)
			{
				string[] lOAD_PATICLES = Res.LOAD_PATICLES;
				foreach (string text in lOAD_PATICLES)
				{
					if (!(key != text))
					{
						flag = true;
						break;
					}
				}
			}
			if (!flag)
			{
				list.Add(key);
			}
		}
		foreach (string item in list)
		{
			mParticlePrefabs.Remove(item);
		}
		list.Clear();
		list = null;
	}

	public static void UnloadOriginalPrefabsAll()
	{
		for (int i = 0; i < Res.LOAD_PREFABSSALL.Length; i++)
		{
			if (mOriginalPrefabs[i] != null)
			{
				mOriginalPrefabs[i] = null;
			}
		}
	}

	public static void UnloadOriginalPrefab(Res.PREFAB _ps)
	{
		mOriginalPrefabs[(int)_ps] = null;
	}

	public static UnityEngine.Object GetOriginalPrefabGOs(Res.PREFAB _ps)
	{
		if (mOriginalPrefabs[(int)_ps] == null)
		{
			mOriginalPrefabs[(int)_ps] = Resources.Load(Res.LOAD_PREFABSSALL[(int)_ps]);
		}
		return mOriginalPrefabs[(int)_ps];
	}

	public static void PreLoadPrefabs_Game()
	{
		Res.PREFAB[] pRELOAD_PREFABS_GAME = Res.PRELOAD_PREFABS_GAME;
		foreach (Res.PREFAB ps in pRELOAD_PREFABS_GAME)
		{
			GetOriginalPrefabGOs(ps);
		}
	}

	private static GameObject LoadTweenPrefab(string path)
	{
		GameObject value = null;
		if (!mTweenPrefabs.TryGetValue(path, out value))
		{
			value = Resources.Load(path) as GameObject;
			if (value != null && value.GetComponent<UITweener>() != null)
			{
				mTweenPrefabs.Add(path, value);
			}
		}
		return value;
	}

	public static GameObject GetTween(string path, string name = null)
	{
		GameObject gameObject = null;
		GameObject gameObject2 = LoadTweenPrefab(path);
		if (gameObject2 != null && gameObject2.GetComponent<UITweener>() != null)
		{
			gameObject = UnityEngine.Object.Instantiate(gameObject2, Vector3.zero, Quaternion.identity) as GameObject;
			if (gameObject != null)
			{
				if (!string.IsNullOrEmpty(name))
				{
					gameObject.name = name;
				}
				else
				{
					gameObject.name = gameObject2.name;
				}
			}
		}
		return gameObject;
	}

	public void LoadSoundResources()
	{
		ResourceManager.LoadPreloadSounds();
	}

	public void UnloadSoundResources(bool _channelclean = false)
	{
		ResourceManager.UnloadPreloadSounds();
		if (_channelclean)
		{
			SoundManager.instance.ReleaseAllChannelClip();
		}
	}

	public void UnloadNotPreloadSoundResources(bool _channelclean = false)
	{
		ResourceManager.UnloadNotPreloadSounds();
		if (_channelclean)
		{
			SoundManager.instance.ReleaseAllChannelClip();
		}
	}

	public bool HasMusic(string name)
	{
		return SoundManager.instance.hasSound(name);
	}

	public void UnloadSoundResouece(string name)
	{
		SoundManager.instance.removeSound(name);
	}

	public void UnloadSoundResoueceForce(string name, int _playChannel)
	{
		SoundManager.instance.UnloadMusic(name, _playChannel);
	}

	public bool PlayMusic(string key, int channel, bool loop, bool stream = false, bool intro = false)
	{
		BGMInfo bGMInfo = mLastBGM;
		mLastBGM.name = key;
		mLastBGM.channel = channel;
		mLastBGM.loop = loop;
		mLastBGM.stream = stream;
		mLastBGM.intro = intro;
		if (!mOptions.BGMEnable)
		{
			return true;
		}
		bool flag = InternalPlayMusic(key, channel, loop, stream, intro);
		if (!flag)
		{
			mLastBGM = bGMInfo;
		}
		return flag;
	}

	private bool InternalPlayMusic(string name, int channel, bool loop, bool stream, bool intro)
	{
		SoundManager.instance.UnloadNowMusic(channel);
		bool flag;
		if (stream)
		{
			StartCoroutine(SoundManager.instance.playMusicFromStream(channel, name, loop));
			flag = true;
		}
		else
		{
			flag = SoundManager.instance.playMusic(channel, name, loop, intro);
		}
		if (flag)
		{
			SoundManager.instance.setMusicVolume(channel, 1f);
		}
		return flag;
	}

	public bool ReplayMusic()
	{
		if (string.IsNullOrEmpty(mLastBGM.name))
		{
			return true;
		}
		return InternalPlayMusic(mLastBGM.name, mLastBGM.channel, mLastBGM.loop, mLastBGM.stream, mLastBGM.intro);
	}

	public void StopAllMusic(float fadeTime = 0f, bool remind = true)
	{
		SoundManager.instance.stopAllMusic(fadeTime);
		if (remind)
		{
			mLastBGM.name = null;
		}
	}

	public void StopMusic(int channel, float fadeTime, bool remind = true)
	{
		SoundManager.instance.stopMusic(channel, fadeTime);
		if (remind)
		{
			mLastBGM.name = null;
		}
	}

	public void SetMusicVolume(float vol, int channel)
	{
		SoundManager.instance.setMusicVolume(0, vol);
	}

	public bool PlaySe(string key, int channel, float vol = 1f, float delay_secondtime = 0f)
	{
		Res.SoundDictInfo value;
		if (Res.ResSoundDict.TryGetValue(key, out value))
		{
			return PlaySe(key, channel, vol, !value.preLoad, delay_secondtime);
		}
		return false;
	}

	public bool PlaySe(string name, int channel, float vol, bool unloadAtPlayEnd, float delay_secondtime = 0f)
	{
		if (!mOptions.SEEnable)
		{
			return true;
		}
		if (channel == -1)
		{
			return SoundManager.instance.playAutoSe(name, vol, unloadAtPlayEnd, delay_secondtime);
		}
		return SoundManager.instance.playSe(channel, name, vol, unloadAtPlayEnd, delay_secondtime);
	}

	public void StopSeAll()
	{
		SoundManager.instance.stopSeAll();
		SoundManager.instance.stopAutoSeAll();
	}

	public void StopSe(int channel)
	{
		SoundManager.instance.stopSe(channel);
	}

	public void SetSeVolume(int channel, float vol)
	{
		SoundManager.instance.setSeVolume(channel, vol);
	}

	public void InitPurchase()
	{
		GameObject gameObject = new GameObject("PurchaseManager");
		mPurchaseManager = gameObject.AddComponent<PurchaseManager>();
		mPurchaseManager.SetItem();
	}

	public static PurchaseInfo MakePurchaseInfo(PItem pitem, int item_kind = 0, string payload = null, string receiptSignature = null, string receiptDataString = null, byte[] purchaseBytes = null, byte[] receiptBytes = null)
	{
		if (pitem == null)
		{
			return null;
		}
		PurchaseInfo purchaseInfo = new PurchaseInfo();
		purchaseInfo.IAPNo = PurchaseManager.instance.GetItemIdx(pitem.itemID);
		purchaseInfo.OrderName = pitem.itemID;
		purchaseInfo.OrderID = pitem.orderID;
		purchaseInfo.TierData = pitem;
		purchaseInfo.ItemKind = item_kind;
		purchaseInfo.PurchasedTime = DateTime.Now;
		purchaseInfo.Payload = payload;
		purchaseInfo.ReceiptSignature = ((receiptSignature != null) ? receiptSignature : string.Empty);
		purchaseInfo.ReceiptDataString = ((receiptDataString != null) ? receiptDataString : string.Empty);
		purchaseInfo.PurchaseBytes = purchaseBytes;
		purchaseInfo.ReceiptBytes = receiptBytes;
		return purchaseInfo;
	}

	public static PurchaseInfo MakePurchaseInfoAndroid(GooglePurchase _gp, PItem _p, int itemKind)
	{
		try
		{
			byte[] array = null;
			byte[] array2 = null;
			string text = null;
			string text2 = null;
			string text3 = null;
			text = _gp.developerPayload;
			text2 = _p.signature;
			text3 = _gp.originalJson;
			array = Encoding.UTF8.GetBytes(text3);
			array2 = array;
			return MakePurchaseInfo(_p, itemKind, text, text2, text3, array2, array);
		}
		catch (Exception)
		{
			return null;
		}
	}

	public static string GetDeveloperPayloadOriginal(string a_accountname)
	{
		string empty = string.Empty;
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("bne:");
		stringBuilder.Append(a_accountname);
		return stringBuilder.ToString();
	}

	public static string GetDeveloperPayload()
	{
		string result = string.Empty;
		string googleAccount = BIJUnity.getGoogleAccount();
		if (!string.IsNullOrEmpty(googleAccount))
		{
			result = GetDeveloperPayloadOriginal(googleAccount);
		}
		return result;
	}

	public static string MakeDeveloperPayload(string a_original)
	{
		string empty = string.Empty;
		if (string.IsNullOrEmpty(a_original))
		{
			return string.Empty;
		}
		BIJMD5 bIJMD = new BIJMD5(a_original);
		return bIJMD.ToString();
	}

	public static string MakeDeveloperPayload()
	{
		string developerPayload = GetDeveloperPayload();
		return MakeDeveloperPayload(developerPayload);
	}

	public static bool CheckDeveloperPayload(string payloadByReceipt)
	{
		return true;
	}

	public static void SetPurchase(PurchaseInfo _item, long _time = 0)
	{
		if (_item != null)
		{
			ITierData tierData = _item.TierData;
			Server.SetPurchase(_item, _time);
			if (_item.PurchaseBytes != null && _item.ReceiptBytes != null)
			{
				long purchaseCount = SingletonMonoBehaviour<GameMain>.Instance.mOptions.PurchaseCount + 1;
				Server.SaveJournal(_item, purchaseCount);
			}
			DateTime now = DateTime.Now;
			if (SingletonMonoBehaviour<GameMain>.Instance.mOptions.FirstPurchasedTime == DateTimeUtil.UnitLocalTimeEpoch)
			{
				SingletonMonoBehaviour<GameMain>.Instance.mOptions.FirstPurchasedTime = now;
			}
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.LastPurchasedTime = now;
			SingletonMonoBehaviour<GameMain>.Instance.SaveOptions();
			if (tierData != null)
			{
				string currencyCode = tierData.CurrencyCode;
				int itemNo = tierData.ItemNo;
				string itemID = tierData.ItemID;
				double cost = tierData.Cost;
				int item_num = 1;
				double num = cost;
				SingletonMonoBehaviour<Marketing>.Instance.PartytrackSendPayment(itemID, item_num, currencyCode, num);
				SingletonMonoBehaviour<Marketing>.Instance.AdjustSendPayment(itemNo, currencyCode, num);
			}
		}
	}

	public List<SNS> InitSocialPlugin()
	{
		List<SNS> list = new List<SNS>();
		if (IsSNSFunctionEnabled(SNS_FLAG.Startup) && IsSNSFunctionEnabled(SNS_FLAG.Startup, SNS.Facebook))
		{
			bool flag = false;
			if (IsSNSFunctionEnabled(SNS_FLAG.Auto) && IsSNSFunctionEnabled(SNS_FLAG.Auto, SNS.Facebook))
			{
				flag = true;
			}
			else if (mOptions != null && !string.IsNullOrEmpty(mOptions.Fbid))
			{
				flag = true;
			}
			if (flag)
			{
				SocialManager.Instance[SNS.Facebook].Login(delegate
				{
				}, null);
				list.Add(SNS.Facebook);
			}
		}
		if (IsSNSFunctionEnabled(SNS_FLAG.Startup) && IsSNSFunctionEnabled(SNS_FLAG.Startup, SNS.GooglePlus))
		{
			GooglePlusPlugin.PluginIF.enableLogging(true, true);
			string googlePlus_CLIENTID = Network.NetworkConstants.GooglePlus_CLIENTID;
			bool flag2 = false;
			if (IsSNSFunctionEnabled(SNS_FLAG.Auto) && IsSNSFunctionEnabled(SNS_FLAG.Auto, SNS.GooglePlus))
			{
				flag2 = true;
			}
			else if (mOptions != null && !string.IsNullOrEmpty(mOptions.Gpid))
			{
				flag2 = true;
			}
			GooglePlusPlugin.PluginIF.setup(googlePlus_CLIENTID, flag2);
			if (flag2)
			{
				list.Add(SNS.GooglePlus);
			}
		}
		if (IsSNSFunctionEnabled(SNS_FLAG.Startup) && IsSNSFunctionEnabled(SNS_FLAG.Startup, SNS.Instagram))
		{
			InstagramPlugin.PluginIF.enableLogging(true, true);
			string instagram_KEY = Network.NetworkConstants.Instagram_KEY;
			string instagram_SECRET = Network.NetworkConstants.Instagram_SECRET;
			bool flag3 = false;
			InstagramPlugin.PluginIF.setup(instagram_KEY, instagram_SECRET, flag3);
			if (flag3)
			{
				list.Add(SNS.Instagram);
			}
		}
		if (IsSNSFunctionEnabled(SNS_FLAG.Startup) && IsSNSFunctionEnabled(SNS_FLAG.Startup, SNS.GooglePlayGameServices))
		{
			GPGSPlugin.PluginIF.enableLogging(true, true);
			string googlePlus_APPID = Network.NetworkConstants.GooglePlus_APPID;
			string googlePlus_CLIENTID2 = Network.NetworkConstants.GooglePlus_CLIENTID;
			string empty = string.Empty;
			bool flag4 = false;
			if (IsSNSFunctionEnabled(SNS_FLAG.Auto) && IsSNSFunctionEnabled(SNS_FLAG.Auto, SNS.GooglePlayGameServices))
			{
				if (mPlayer.mIsNewPlayer)
				{
					flag4 = true;
				}
				else if (mOptions != null && !string.IsNullOrEmpty(mOptions.Gpgsid))
				{
					flag4 = true;
				}
			}
			else if (mOptions != null && !string.IsNullOrEmpty(mOptions.Gpgsid))
			{
				flag4 = true;
			}
			GPGSPlugin.PluginIF.setup(googlePlus_APPID, flag4, googlePlus_CLIENTID2, empty);
			if (flag4)
			{
				list.Add(SNS.GooglePlayGameServices);
			}
		}
		return list;
	}

	public void InitNetwork()
	{
		Network.NetworkConstants = NetworkConstants_Android.Load();
		Server.GetMaintenanceInfoEvent += Server_OnMaintenanceProfile;
		Server.UserAuthEvent += Server_OnUserAuth;
		Server.NicknameEditEvent += Server_OnNicknameEdit;
		Server.NicknameCheckEvent += Server_OnNicknameCheck;
		Server.GetSegmentInfoEvent += Server_OnSegmentProfile;
		ServerIAP.CurrencyChargeEvent += Server_OnBuyGem;
		ServerIAP.CurrencyCheckEvent += Server_OnGetGemCount;
		ServerIAP.CurrencySpendEvent += Server_OnBuyItem;
		ServerIAP.BirthdayGetEvent += Server_OnGetBirthday;
		ServerIAP.BirthdaySetEvent += Server_OnSetBirthday;
		ServerIAP.ChargeCheckEvent += Server_OnChargeCheck;
		ServerIAP.CampaignCheckEvent += Server_OnCampaignCheck;
		ServerIAP.CampaignChargeEvent += Server_OnCampaignCharge;
		ServerIAP.DebugCampaignUnsetEvent += Server_OnDebugCampaignUnset;
		Server.SendDeviceTokenEvent += Server_OnSendDeviceToken;
		Server.SaveGameStateEvent += Server_OnSaveGameState;
		Server.GetGameInfoEvent += Server_OnGameProfile;
		Server.SetStageClearDataEvent += Server_OnSetStageClearData;
		Server.SetEventStageClearDataEvent += Server_OnSetStageClearData;
		ServerIAP.SubcurrencyRewardCheckEvent += Server_OnRewardCheck;
		ServerIAP.SubcurrencyRewardSetEvent += Server_OnRewardSet;
		Server.GetServerSaveDataOfOthersEvent += Server_OnGetSaveDataOfOthers;
		Server.GetServerSaveDataOfOthersEventCX += Server_OnGetSaveDataOfOthersCX;
		Server.AdvGetAllItemEvent += Server_OnAdvGetAllItem;
		Server.AdvQuestStartEvent += Server_OnAdvQuestStart;
		Server.AdvQuestEndEvent += Server_OnAdvQuestEnd;
		Server.AdvGetGashaInfoEvent += Server_OnAdvGetGashaInfo;
		Server.AdvGashaPlayEvent += Server_OnAdvGashaPlay;
		Server.AdvGrowUpFigureEvent += Server_OnAdvGrowUpFigure;
		Server.AdvGetRewardInfoEvent += Server_OnAdvGetRewardInfo;
		Server.AdvSetRewardEvent += Server_OnAdvSetReward;
		Server.AdvSendSaveDataEvent += Server_OnAdvSendSaveData;
		Server.AdvSendGotMailEvent += Server_OnAdvSendGotMail;
		Server.AdvGetEventPointEvent += Server_OnAdvGetEventPoint;
		Server.AdvSetEventPointEvent += Server_OnAdvSetEventPoint;
		Server.AdvDebug_FigureAllDeleteEvent += Server_OnAdvDebug_FigureAllDelete;
		Server.AdvDebug_RewardAllDeleteEvent += Server_OnAdvDebug_RewardAllDelete;
		Server.AdvDebug_SetItemDataEvent += Server_OnAdvDebug_SetItemData;
		Server.AdvDebug_GetAllItemEvent += Server_OnAdvDebug_GetAllItem;
		Server.AdvDebug_GashaInfoAllDeleteEvent += Server_OnAdvDebug_GashaInfoAllDelete;
		Server.AdvDebug_FigureAllGrowUpEvent += Server_OnAdvDebug_FigureAllGrowUp;
		Server.AdvDebug_AddFigureEvent += Server_OnAdvDebug_AddFigure;
		Server.AdvDebug_RemoveFigureEvent += Server_OnAdvDebug_RemoveFigure;
		Server.AdvDebug_GetAllFigureEvent += Server_OnAdvDebug_GetAllFigure;
		Server.AdvDebug_SetFigureStatusEvent += Server_OnAdvDebug_SetFigureStatus;
		Server.ChargePriceCheckEvent += Server_OnChargePriceCheck;
		Server.CheckSaveEvent += Server_OnCheckSave;
		Server.AdvCheckSaveEvent += Server_OnAdvCheckSave;
		Server.GetServerGameDataEvent += Server_OnGetGameData;
		Server.GetGiftEvent += Server_OnGetGift;
		Server.GetApplyEvent += Server_OnGetApply;
		ServerIAP.SubcurrencyCheckEvent += Server_OnGetSupportPurchase;
		Server.GetSupportEvent += Server_OnGetSupport;
		Server.LoginBonusEvent += Server_OnLoginBonus;
		Server.AdvGetSaveDataEvent += Server_OnAdvGetSaveData;
		Server.AdvCheckMasterTableVerEvent += Server_OnAdvCheckMasterTableVer;
		Server.AdvGetMasterTableDataEvent += Server_OnAdvGetMasterTableData;
		Server.AdvGetMailEvent += Server_OnAdvGetMail;
		if (USE_RESOURCEDL)
		{
			Server.DLFilelistCheckEvent += Server_OnDLFilelistCheck;
		}
		mNetworkProfile = new NetworkProfile();
		mGameProfile = new GameProfile();
		mEventProfile = new EventProfile();
		mSegmentProfile = new SegmentProfile();
		mMaintenanceProfile = new MaintenanceProfile();
		mUserProfile = new UserGetResponse();
		mUserUniqueID = new UserAuthResponse();
		mLoginBonusData = new SMDLoginBonusData();
		Network.NetworkInterface = this;
		GameObject gameObject = Util.CreateGameObject("Network", base.gameObject);
		Network network = gameObject.AddComponent<Network>();
		if (SingletonMonoBehaviour<Network>.Instance != network)
		{
		}
		NotificationManager.IsNotificationEnabled = mOptions.UseNotification;
		NotificationManager.OnLoginSchedule += OnNotification_ScheduleLogin;
		NotificationManager.OnStatusSchedule += OnNotification_ScheduleStatus;
		GET_AUTHUUID_FREQ = Constants.GET_AUTHUUID_FREQ;
		GET_GAME_INFO_FREQ = Constants.GET_GAME_INFO_FREQ;
		GET_SEGMENT_INFO_FREQ = Constants.GET_SEGMENT_INFO_FREQ;
		GET_SUPPORT_FREQ = Constants.GET_SUPPORT_FREQ;
		GET_SUPPORTPURCHASE_FREQ = Constants.GET_SUPPORTPURCHASE_FREQ;
		GET_GIFT_FREQ = Constants.GET_GIFT_FREQ;
		GET_APPLY_FREQ = Constants.GET_APPLY_FREQ;
		GET_FRIEND_FREQ = Constants.GET_FRIEND_FREQ;
		GET_SOCIAL_FREQ = Constants.GET_SOCIAL_FREQ;
		GET_FRIEND_ICON_FREQ = Constants.GET_FRIEND_ICON_FREQ;
		GET_ONE_ICON_FREQ = Constants.GET_ONE_ICON_FREQ;
		GET_RANDOMUSER_FREQ = Constants.GET_RANDOM_USER_FREQ;
		GET_GEMCOUNT_FREQ = Constants.GET_GEMCOUNT_FREQ;
		GET_MAINTENANCE_INFO_FREQ = Constants.GET_MAINTENANCE_INFO_FREQ;
		GET_LOGINBONUS_FREQ = Constants.GET_LOGINBONUS_FREQ;
	}

	private void TermNetwork()
	{
		Server.GetMaintenanceInfoEvent -= Server_OnMaintenanceProfile;
		Server.GetServerSaveDataOfOthersEvent -= Server_OnGetSaveDataOfOthers;
		Server.GetServerSaveDataOfOthersEventCX -= Server_OnGetSaveDataOfOthersCX;
		Server.UserAuthEvent -= Server_OnUserAuth;
		Server.NicknameCheckEvent -= Server_OnNicknameCheck;
		Server.NicknameEditEvent -= Server_OnNicknameEdit;
		Server.GetSegmentInfoEvent -= Server_OnSegmentProfile;
		ServerIAP.CurrencyChargeEvent -= Server_OnBuyGem;
		ServerIAP.CurrencyCheckEvent -= Server_OnGetGemCount;
		ServerIAP.CurrencySpendEvent -= Server_OnBuyItem;
		ServerIAP.BirthdayGetEvent -= Server_OnGetBirthday;
		ServerIAP.BirthdaySetEvent -= Server_OnSetBirthday;
		ServerIAP.ChargeCheckEvent -= Server_OnChargeCheck;
		ServerIAP.CampaignCheckEvent -= Server_OnCampaignCheck;
		ServerIAP.CampaignChargeEvent -= Server_OnCampaignCharge;
		ServerIAP.DebugCampaignUnsetEvent -= Server_OnDebugCampaignUnset;
		Server.SendDeviceTokenEvent -= Server_OnSendDeviceToken;
		Server.SaveGameStateEvent -= Server_OnSaveGameState;
		Server.GetGameInfoEvent -= Server_OnGameProfile;
		Server.SetStageClearDataEvent -= Server_OnSetStageClearData;
		Server.SetEventStageClearDataEvent -= Server_OnSetStageClearData;
		Server.AdvGetAllItemEvent -= Server_OnAdvGetAllItem;
		Server.AdvQuestStartEvent -= Server_OnAdvQuestStart;
		Server.AdvQuestEndEvent -= Server_OnAdvQuestEnd;
		Server.AdvGetGashaInfoEvent -= Server_OnAdvGetGashaInfo;
		Server.AdvGashaPlayEvent -= Server_OnAdvGashaPlay;
		Server.AdvGrowUpFigureEvent -= Server_OnAdvGrowUpFigure;
		Server.AdvGetRewardInfoEvent -= Server_OnAdvGetRewardInfo;
		Server.AdvSetRewardEvent -= Server_OnAdvSetReward;
		Server.AdvSendSaveDataEvent -= Server_OnAdvSendSaveData;
		Server.AdvSendGotMailEvent -= Server_OnAdvSendGotMail;
		Server.AdvGetEventPointEvent -= Server_OnAdvGetEventPoint;
		Server.AdvSetEventPointEvent -= Server_OnAdvSetEventPoint;
		Server.AdvDebug_FigureAllDeleteEvent -= Server_OnAdvDebug_FigureAllDelete;
		Server.AdvDebug_RewardAllDeleteEvent -= Server_OnAdvDebug_RewardAllDelete;
		Server.AdvDebug_SetItemDataEvent -= Server_OnAdvDebug_SetItemData;
		Server.AdvDebug_GetAllItemEvent -= Server_OnAdvDebug_GetAllItem;
		Server.AdvDebug_GashaInfoAllDeleteEvent -= Server_OnAdvDebug_GashaInfoAllDelete;
		Server.AdvDebug_FigureAllGrowUpEvent -= Server_OnAdvDebug_FigureAllGrowUp;
		Server.AdvDebug_AddFigureEvent -= Server_OnAdvDebug_AddFigure;
		Server.AdvDebug_RemoveFigureEvent -= Server_OnAdvDebug_RemoveFigure;
		Server.AdvDebug_GetAllFigureEvent -= Server_OnAdvDebug_GetAllFigure;
		Server.AdvDebug_SetFigureStatusEvent -= Server_OnAdvDebug_SetFigureStatus;
		Server.ChargePriceCheckEvent -= Server_OnChargePriceCheck;
		Server.CheckSaveEvent -= Server_OnCheckSave;
		Server.AdvCheckSaveEvent -= Server_OnAdvCheckSave;
		ServerIAP.SubcurrencyRewardCheckEvent -= Server_OnRewardCheck;
		ServerIAP.SubcurrencyRewardSetEvent -= Server_OnRewardSet;
		NotificationManager.OnLoginSchedule -= OnNotification_ScheduleLogin;
		NotificationManager.OnStatusSchedule -= OnNotification_ScheduleStatus;
		Server.GetServerGameDataEvent -= Server_OnGetGameData;
		Server.LoginBonusEvent -= Server_OnLoginBonus;
		Server.GetGiftEvent -= Server_OnGetGift;
		Server.GetApplyEvent -= Server_OnGetApply;
		ServerIAP.SubcurrencyCheckEvent -= Server_OnGetSupportPurchase;
		Server.GetSupportEvent -= Server_OnGetSupport;
		Server.AdvGetSaveDataEvent -= Server_OnAdvGetSaveData;
		Server.AdvCheckMasterTableVerEvent -= Server_OnAdvCheckMasterTableVer;
		Server.AdvGetMasterTableDataEvent -= Server_OnAdvGetMasterTableData;
		Server.AdvGetMailEvent -= Server_OnAdvGetMail;
		if (USE_RESOURCEDL)
		{
			Server.DLFilelistCheckEvent -= Server_OnDLFilelistCheck;
		}
		if (SingletonMonoBehaviour<Network>.Instance != null)
		{
			SingletonMonoBehaviour<Network>.Instance.OnApplicationQuit();
		}
	}

	public static string Server_Result(int result)
	{
		return Server.ResultString(result);
	}

	public void InitMarketing()
	{
		Marketing.MarketingConstants = Network.NetworkConstants;
		GameObject gameObject = new GameObject("Marketing");
		gameObject.transform.parent = base.gameObject.transform;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localRotation = Quaternion.identity;
		Marketing marketing = gameObject.AddComponent<Marketing>();
		if (!(SingletonMonoBehaviour<Marketing>.Instance != marketing))
		{
		}
	}

	private void TermMarketing()
	{
		if (SingletonMonoBehaviour<Marketing>.Instance != null)
		{
			SingletonMonoBehaviour<Marketing>.Instance.OnApplicationQuit();
		}
	}

	public void SendTutorialStep(Def.TUTORIAL_INDEX index)
	{
		string text = index.ToString();
		if (text.IndexOf("ADV_") == 0)
		{
			int num = -1;
			for (int i = 0; i < ADV_TUTORIAL_MATRIX.GetLength(0); i++)
			{
				if (ADV_TUTORIAL_MATRIX[i, 0] == (int)index)
				{
					num = i;
					break;
				}
			}
			if (num >= 0 && num < ADV_TUTORIAL_MATRIX.GetLength(0))
			{
				int status = ADV_TUTORIAL_MATRIX[num, 1];
				int cur_aseries = ADV_TUTORIAL_MATRIX[num, 2];
				int cur_astage = ADV_TUTORIAL_MATRIX[num, 3];
				int advLastStage = mPlayer.AdvLastStage;
				int advMaxStamina = mPlayer.AdvMaxStamina;
				ServerCram.ArcadeTutorialState(status, cur_aseries, cur_astage, 1000, advLastStage, advMaxStamina);
			}
			return;
		}
		int num2 = -1;
		for (int j = 0; j < TUTORIAL_MATRIX.GetLength(0); j++)
		{
			if (TUTORIAL_MATRIX[j, 0] == (int)index)
			{
				num2 = j;
				break;
			}
		}
		if (num2 >= 0 && num2 < TUTORIAL_MATRIX.GetLength(0))
		{
			int status2 = TUTORIAL_MATRIX[num2, 1];
			int cur_series = TUTORIAL_MATRIX[num2, 2];
			int cur_stage = TUTORIAL_MATRIX[num2, 3];
			int skip = 0;
			if (mPlayer.IsTutorialSkipped())
			{
				skip = 1;
			}
			ServerCram.TutorialState(status2, cur_series, cur_stage, 1000, skip);
		}
	}

	public void SetRatingTutorial()
	{
		mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.RATING_F_MARS);
		mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.RATING_2);
	}

	public void CheckRatingTutorial()
	{
		if (mPlayer.IsCompanionUnlock(2))
		{
			mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.RATING_F_MARS);
		}
	}

	public void CheckSeriesMaxLevel()
	{
		List<Def.SERIES> list = new List<Def.SERIES>(Def.SeriesMapRouteData.Keys);
		foreach (Def.SERIES item in list)
		{
			PlayerMapData data;
			mPlayer.Data.GetMapData(item, out data);
			if (data != null)
			{
				SMMapPageData seriesMapPageData = GetSeriesMapPageData(item);
				if (seriesMapPageData != null)
				{
					data.MaxMainStageNum = Def.GetStageNo(seriesMapPageData.MaxMainStageNum, 0);
					mPlayer.Data.SetMapData(item, data);
				}
			}
		}
	}

	public bool DebugCheatStageClear_Main(Def.SERIES a_series, int a_stage, int a_star)
	{
		return DebugCheatStageClearBundle_Main(a_series, a_stage, a_star, true, true);
	}

	public int SMMapRouteNodeSort(SMMapRoute<float>.RouteNode a, SMMapRoute<float>.RouteNode b)
	{
		if (b.Value > a.Value)
		{
			return -1;
		}
		if (a.Value > b.Value)
		{
			return 1;
		}
		return 0;
	}

	public bool DebugCheatBingoStageClear(Def.SERIES a_series, int a_stage, int a_star, bool a_issupport = false, bool a_single = false)
	{
		if (a_star < 1)
		{
			a_star = 1;
		}
		Player player = mPlayer;
		bool flag = Def.IsEventSeries(a_series);
		int a_main = 0;
		int a_sub = 0;
		short a_course = 0;
		int a_eventID = 0;
		if (!flag)
		{
			Def.SplitStageNo(a_stage, out a_main, out a_sub);
		}
		else
		{
			Def.SplitServerStageNo(a_stage, out a_eventID, out a_course, out a_main, out a_sub);
		}
		PlayerMapData a_mapdata;
		PlayerEventData a_eventdata;
		GetPlayerMapDataFromSeries(a_series, a_eventID, a_course, out a_mapdata, out a_eventdata);
		if (a_mapdata == null)
		{
			return false;
		}
		if (a_single)
		{
			int stageNo = Def.GetStageNo(a_main, 0);
			PlayerEventClearData playerEventClearData = new PlayerEventClearData();
			playerEventClearData.Series = Def.SERIES.SM_EV;
			playerEventClearData.EventID = a_eventID;
			playerEventClearData.CourseID = a_course;
			playerEventClearData.StageNo = stageNo;
			playerEventClearData.HightScore = 0;
			playerEventClearData.GotStars = a_star;
			mPlayer.AddStageClearData(playerEventClearData);
			mPlayer.SetClearEventStageInfoForSupport(a_series, stageNo, a_eventID, a_course);
		}
		return true;
	}

	public bool DebugCheatLabyrinthStageClear(Def.SERIES a_series, int a_stage, int a_star, bool a_issupport = false, bool a_single = false)
	{
		if (a_star < 1)
		{
			a_star = 1;
		}
		Player player = mPlayer;
		bool flag = Def.IsEventSeries(a_series);
		int a_main = 0;
		int a_sub = 0;
		short a_course = 0;
		int a_eventID = 0;
		if (!flag)
		{
			Def.SplitStageNo(a_stage, out a_main, out a_sub);
		}
		else
		{
			Def.SplitServerStageNo(a_stage, out a_eventID, out a_course, out a_main, out a_sub);
		}
		PlayerMapData a_mapdata;
		PlayerEventData a_eventdata;
		GetPlayerMapDataFromSeries(a_series, a_eventID, a_course, out a_mapdata, out a_eventdata);
		if (a_mapdata == null)
		{
			return false;
		}
		if (a_single)
		{
			int stageNo = Def.GetStageNo(a_main, 0);
			PlayerEventClearData playerEventClearData = new PlayerEventClearData();
			playerEventClearData.Series = Def.SERIES.SM_EV;
			playerEventClearData.EventID = a_eventID;
			playerEventClearData.CourseID = a_course;
			playerEventClearData.StageNo = stageNo;
			playerEventClearData.HightScore = 0;
			playerEventClearData.GotStars = a_star;
			mPlayer.AddStageClearData(playerEventClearData);
			mPlayer.SetClearEventStageInfoForSupport(a_series, stageNo, a_eventID, a_course);
			PlayerEventLabyrinthData labyrinthData = a_eventdata.LabyrinthData;
			if (labyrinthData.ClearStageProcess(a_course, stageNo, false))
			{
				SMMapPageData eventPageData = GetEventPageData(a_eventID);
				SMEventPageData sMEventPageData = eventPageData as SMEventPageData;
				SMEventLabyrinthStageSetting labyrinthStageSetting = sMEventPageData.GetLabyrinthStageSetting(a_main, 0, a_course);
				int itemAmount = labyrinthStageSetting.ItemAmount;
				AddEventPoint(a_eventID, itemAmount);
				int num = 0;
				bool a_isClear;
				bool a_isComplete;
				a_eventdata.CheckEventStageClearReward(Def.EVENT_TYPE.SM_LABYRINTH, a_course, Def.LabyrinthStageList, out a_isClear, out a_isComplete);
				if (a_isClear)
				{
					a_eventdata.UpdateCourseClear(a_course, true);
					num = sMEventPageData.EventCourseList[a_course].CourseStagesAllClearRewardNum;
					AddEventPoint(a_eventID, num);
				}
				ServerCram.EventLabyrinthGetJewel(a_course, (int)mPlayer.Data.LastPlaySeries, mPlayer.Data.LastPlayLevel, a_eventID, 5, num + itemAmount, 0, 0, 0, itemAmount, 0, 0, num, a_eventdata.LabyrinthData.JewelAmount);
			}
		}
		return true;
	}

	public bool DebugCheatStageClearBundle_Main(Def.SERIES a_series, int a_stage, int a_star, bool a_issupport = false, bool a_single = false)
	{
		if (Def.IsAdvSeries(a_series))
		{
			return false;
		}
		if (a_star < 1)
		{
			a_star = 1;
		}
		Player player = mPlayer;
		bool flag = Def.IsEventSeries(a_series);
		int a_main = 0;
		int a_sub = 0;
		short a_course = 0;
		int a_eventID = 0;
		if (!flag)
		{
			Def.SplitStageNo(a_stage, out a_main, out a_sub);
		}
		else
		{
			Def.SplitServerStageNo(a_stage, out a_eventID, out a_course, out a_main, out a_sub);
		}
		PlayerMapData a_mapdata;
		PlayerEventData a_eventdata;
		GetPlayerMapDataFromSeries(a_series, a_eventID, a_course, out a_mapdata, out a_eventdata);
		if (!flag && a_mapdata == null)
		{
			return false;
		}
		float a_mapSize = 1136f;
		SMMapPageData sMMapPageData = null;
		if (!flag)
		{
			sMMapPageData = GetSeriesMapPageData(a_series);
			if (a_mapdata.NextRoadBlockLevel == 0)
			{
				MakeMapInitializeData(a_series);
			}
		}
		else
		{
			sMMapPageData = GetEventPageData(a_eventID);
			SMEventPageData sMEventPageData = sMMapPageData as SMEventPageData;
			if (sMEventPageData == null)
			{
				return false;
			}
			a_mapSize = GetMapSize(sMEventPageData.EventSetting.EventType);
			MakeMapInitializeData(a_series, a_eventID, a_course);
			GetPlayerMapDataFromSeries(a_series, a_eventID, a_course, out a_mapdata, out a_eventdata);
			if (!a_single)
			{
				switch (sMEventPageData.EventSetting.EventType)
				{
				case Def.EVENT_TYPE.SM_RALLY:
				case Def.EVENT_TYPE.SM_BINGO:
				case Def.EVENT_TYPE.SM_LABYRINTH:
					return false;
				}
			}
			else
			{
				if (sMEventPageData.EventSetting.EventType == Def.EVENT_TYPE.SM_BINGO)
				{
					return DebugCheatBingoStageClear(a_series, a_stage, a_star, a_issupport, a_single);
				}
				if (sMEventPageData.EventSetting.EventType == Def.EVENT_TYPE.SM_LABYRINTH)
				{
					return DebugCheatLabyrinthStageClear(a_series, a_stage, a_star, a_issupport, a_single);
				}
			}
		}
		SMMapSsData mapRouteData = GetMapRouteData(a_series, a_eventID, a_course, a_mapSize);
		InitializeStageStatus(a_series, mapRouteData, a_eventID, a_course);
		SMMapRoute<float>.RouteNode routeNode = null;
		List<SMMapRoute<float>.RouteNode> list = null;
		if (!a_single)
		{
			list = mapRouteData.GetStages(100, Def.GetStageNo(a_main, a_sub), true);
		}
		else
		{
			list = new List<SMMapRoute<float>.RouteNode>();
			SMMapRoute<float>.RouteNode stage = mapRouteData.GetStage(Def.GetStageNo(a_main, a_sub), true);
			if (stage != null)
			{
				list.Add(stage);
			}
		}
		if (list.Count == 0)
		{
			return false;
		}
		list.Sort(SMMapRouteNodeSort);
		foreach (SMMapRoute<float>.RouteNode item in list)
		{
			routeNode = item;
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(item.Value);
			int a_main2;
			int a_sub2;
			Def.SplitStageNo(stageNoByRouteOrder, out a_main2, out a_sub2);
			if (item == null || !routeNode.mIsStage)
			{
				continue;
			}
			SMMapStageSetting mapStage;
			if (flag)
			{
				SMEventPageData sMEventPageData2 = sMMapPageData as SMEventPageData;
				mapStage = sMEventPageData2.GetMapStage(a_main2, a_sub2, a_course);
			}
			else
			{
				mapStage = sMMapPageData.GetMapStage(a_main2, a_sub2);
			}
			bool flag2 = false;
			bool flag3 = false;
			bool flag4 = false;
			if (a_single)
			{
				if (a_main != a_main2 || a_sub != a_sub2)
				{
					continue;
				}
				flag2 = true;
				flag3 = true;
			}
			else if (a_sub2 == 0)
			{
				flag2 = true;
				flag3 = true;
			}
			else if (a_issupport)
			{
				if (item.mParent != null)
				{
					if (!item.mParent.IsSub)
					{
						flag3 = true;
					}
					else
					{
						flag4 = true;
					}
				}
			}
			else if (mDebugCheatClearIncludeSub)
			{
				flag3 = true;
				flag2 = true;
			}
			else if (item.mParent != null)
			{
				if (!item.mParent.IsSub)
				{
					flag3 = true;
				}
				else
				{
					flag4 = true;
				}
			}
			if (mapStage != null && flag2 && !a_issupport)
			{
				if (mapStage.StageEnterCondition == "PARTNER_F" && mapStage.EnterPartners.Count == 1)
				{
					int num = mapStage.EnterPartners[0];
					if (!player.IsCompanionUnlock(num))
					{
						AccessoryData accessoryData = null;
						List<AccessoryData> accessoryByType = GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
						for (int i = 0; i < accessoryByType.Count; i++)
						{
							AccessoryData accessoryData2 = accessoryByType[i];
							if (accessoryData2.GetCompanionID() == num)
							{
								accessoryData = accessoryData2;
								break;
							}
						}
						if (accessoryData != null)
						{
							player.AddAccessory(accessoryData);
						}
					}
				}
				if (mapStage.BoxID.Count > 0)
				{
					foreach (int item2 in mapStage.BoxID)
					{
						AccessoryData accessoryData3 = GetAccessoryData(item2);
						if (accessoryData3 == null)
						{
							continue;
						}
						switch (accessoryData3.AccessoryType)
						{
						case AccessoryData.ACCESSORY_TYPE.RB_KEY:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_R:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_S:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_SS:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_STARS:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_GF00:
							continue;
						}
						if (mDebugCheatClearGetItem)
						{
							AddAccessoryFromPresentBox(item2);
						}
					}
				}
			}
			bool flag5 = false;
			int num2 = a_main;
			int num3 = a_sub;
			int a_course2 = a_course;
			if (flag)
			{
				PlayerClearData _psd = null;
				if (player.GetStageClearData(a_series, a_eventID, a_course, stageNoByRouteOrder, out _psd))
				{
					if (_psd.GotStars < a_star)
					{
						_psd.GotStars = a_star;
					}
					continue;
				}
				Player.STAGE_STATUS stageStatus = player.GetStageStatus(a_series, a_eventID, a_course, stageNoByRouteOrder);
				if (flag2)
				{
					if (stageStatus == global::Player.STAGE_STATUS.NOOPEN || stageStatus == global::Player.STAGE_STATUS.LOCK || stageStatus == global::Player.STAGE_STATUS.UNLOCK)
					{
						player.SetStageStatus(a_series, a_eventID, a_course, stageNoByRouteOrder, global::Player.STAGE_STATUS.CLEAR);
					}
					PlayerEventClearData playerEventClearData = new PlayerEventClearData();
					playerEventClearData.Series = a_series;
					playerEventClearData.StageNo = stageNoByRouteOrder;
					playerEventClearData.HightScore = 0;
					playerEventClearData.GotStars = a_star;
					playerEventClearData.EventID = a_eventID;
					playerEventClearData.CourseID = a_course;
					player.AddStageClearData(playerEventClearData);
					int stageNo = Def.GetStageNo(a_main2, a_sub2);
					player.SetClearEventStageInfoForSupport(a_series, stageNo, a_eventID, a_course);
					SMEventPageData sMEventPageData3 = sMMapPageData as SMEventPageData;
					if (sMEventPageData3 != null && sMEventPageData3.EventSetting.EventType == Def.EVENT_TYPE.SM_STAR)
					{
						if (!a_issupport)
						{
							flag5 = true;
						}
						a_course2 = a_eventdata.CourseID;
					}
				}
				else if (flag3 && (stageStatus == global::Player.STAGE_STATUS.NOOPEN || stageStatus == global::Player.STAGE_STATUS.LOCK))
				{
					player.SetStageStatus(a_series, a_eventdata.EventID, a_eventdata.CourseID, stageNoByRouteOrder, global::Player.STAGE_STATUS.UNLOCK);
				}
			}
			else
			{
				flag5 = true;
				a_course2 = 0;
				if (flag3 || flag2)
				{
					player.SetStageStatus(a_series, stageNoByRouteOrder, global::Player.STAGE_STATUS.UNLOCK);
				}
				else if (flag4)
				{
					player.SetStageStatus(a_series, stageNoByRouteOrder, global::Player.STAGE_STATUS.LOCK);
				}
				PlayerClearData _psd2 = null;
				if (flag2)
				{
					if (!player.GetStageClearData(a_series, stageNoByRouteOrder, out _psd2))
					{
						_psd2 = new PlayerClearData();
						_psd2.Series = a_series;
						_psd2.StageNo = stageNoByRouteOrder;
						_psd2.HightScore = 0;
						_psd2.GotStars = a_star;
						player.AddStageClearData(_psd2);
						int stageNo2 = Def.GetStageNo(a_main2, a_sub2);
						player.SetClearStageInfoForSupport(a_series, stageNo2);
						if (!a_issupport)
						{
							DebugTutorialComplete(stageNoByRouteOrder);
						}
					}
					else if (_psd2.GotStars < a_star)
					{
						_psd2.GotStars = a_star;
					}
				}
			}
			if (!flag5)
			{
				continue;
			}
			SMRoadBlockSetting roadBlock = sMMapPageData.GetRoadBlock(a_main2, 0, a_course2);
			if (roadBlock == null || a_mapdata.IsUnlockedRoadBlock(roadBlock.RoadBlockID))
			{
				continue;
			}
			int num4 = num2;
			if (num4 > roadBlock.mStageNo)
			{
				int stageNo3 = Def.GetStageNo(roadBlock.mStageNo, 0);
				if (!flag)
				{
					a_mapdata.RoadBlockReachLevel = stageNo3;
					List<AccessoryData> roadBlockAccessory = GetRoadBlockAccessory(stageNo3, a_series);
					for (int j = 0; j < roadBlockAccessory.Count; j++)
					{
						player.AddAccessory(roadBlockAccessory[j]);
					}
				}
				else if (!a_issupport)
				{
					SMEventPageData sMEventPageData4 = sMMapPageData as SMEventPageData;
					if (sMEventPageData4 != null && sMEventPageData4.EventSetting.EventType == Def.EVENT_TYPE.SM_STAR)
					{
						SMEventRoadBlockSetting sMEventRoadBlockSetting = roadBlock as SMEventRoadBlockSetting;
						if (sMEventRoadBlockSetting != null && sMEventRoadBlockSetting.AccessoryIDOnOpened != 0)
						{
							AccessoryData accessoryData4 = SingletonMonoBehaviour<GameMain>.Instance.GetAccessoryData(sMEventRoadBlockSetting.AccessoryIDOnOpened);
							if (accessoryData4 != null && !player.IsAccessoryUnlock(accessoryData4.Index))
							{
								player.AddAccessory(accessoryData4, true, false, 12);
							}
						}
					}
				}
				List<SMRoadBlockSetting> roadBlockInCourse = sMMapPageData.GetRoadBlockInCourse(a_course2);
				roadBlockInCourse.Sort((SMRoadBlockSetting x, SMRoadBlockSetting y) => x.RoadBlockID - y.RoadBlockID);
				int num5 = roadBlock.RoadBlockID + 1;
				if (num5 >= roadBlockInCourse.Count)
				{
					a_mapdata.NextRoadBlockLevel = Def.GetStageNo(999, 0);
				}
				else
				{
					a_mapdata.UpdateUnlockedRoadBlock(roadBlock.RoadBlockID);
					a_mapdata.NextRoadBlockLevel = roadBlockInCourse[num5].RoadBlockLevel;
					a_mapdata.RoadBlockReachLevel = roadBlock.RoadBlockLevel;
				}
				player.SetRoadBlockKey(a_series, 0);
			}
			else if (num4 == roadBlock.mStageNo)
			{
				a_mapdata.RoadBlockReachLevel = roadBlock.RoadBlockLevel;
				DateTime now = DateTime.Now;
				a_mapdata.RoadBlockReachTime = now;
			}
			if (flag)
			{
				a_eventdata.CourseData[a_eventdata.CourseID] = a_mapdata;
				player.Data.SetMapData(a_eventID, a_eventdata);
			}
			else
			{
				player.Data.SetMapData(a_series, a_mapdata);
			}
		}
		if (routeNode != null)
		{
			if (routeNode.NextMain != null && routeNode.NextMain.mIsStage)
			{
				int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode.NextMain.Value);
				if (flag)
				{
					Player.STAGE_STATUS stageStatus2 = player.GetStageStatus(a_series, a_eventdata.EventID, a_eventdata.CourseID, stageNoByRouteOrder2);
					if (stageStatus2 == global::Player.STAGE_STATUS.NOOPEN || stageStatus2 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(a_series, a_eventdata.EventID, a_eventdata.CourseID, stageNoByRouteOrder2, global::Player.STAGE_STATUS.UNLOCK);
					}
				}
				else
				{
					Player.STAGE_STATUS stageStatus3 = player.GetStageStatus(a_series, stageNoByRouteOrder2);
					if (stageStatus3 == global::Player.STAGE_STATUS.NOOPEN || stageStatus3 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(a_series, stageNoByRouteOrder2, global::Player.STAGE_STATUS.UNLOCK);
					}
				}
			}
			if (routeNode.NextSub != null && routeNode.NextSub.mIsStage)
			{
				int stageNoByRouteOrder3 = Def.GetStageNoByRouteOrder(routeNode.NextSub.Value);
				if (flag)
				{
					Player.STAGE_STATUS stageStatus4 = player.GetStageStatus(a_series, a_eventdata.EventID, a_eventdata.CourseID, stageNoByRouteOrder3);
					if (stageStatus4 == global::Player.STAGE_STATUS.NOOPEN || stageStatus4 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(a_series, a_eventdata.EventID, a_eventdata.CourseID, stageNoByRouteOrder3, global::Player.STAGE_STATUS.UNLOCK);
					}
				}
				else
				{
					Player.STAGE_STATUS stageStatus5 = player.GetStageStatus(a_series, stageNoByRouteOrder3);
					if (stageStatus5 == global::Player.STAGE_STATUS.NOOPEN || stageStatus5 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(a_series, stageNoByRouteOrder3, global::Player.STAGE_STATUS.UNLOCK);
					}
				}
			}
		}
		if (flag)
		{
			SMEventPageData sMEventPageData5 = sMMapPageData as SMEventPageData;
			if (sMEventPageData5 != null && sMEventPageData5.EventSetting.EventType == Def.EVENT_TYPE.SM_RALLY2)
			{
				int stageNoByRouteOrder4 = Def.GetStageNoByRouteOrder(routeNode.NextMain.Value);
				for (int k = 0; k < stageNoByRouteOrder4; k += 100)
				{
					player.SetLineStatus(a_series, a_eventdata.EventID, 0, k, global::Player.STAGE_STATUS.CLEAR);
				}
			}
		}
		return true;
	}

	public void GetPlayerMapDataFromSeries(Def.SERIES a_series, int a_eventid, short a_courseID, out PlayerMapData a_mapdata, out PlayerEventData a_eventdata)
	{
		Player player = mPlayer;
		a_mapdata = null;
		a_eventdata = null;
		if (Def.IsEventSeries(a_series))
		{
			player.Data.GetMapData(a_series, a_eventid, out a_eventdata);
			if (a_eventdata != null)
			{
				a_mapdata = a_eventdata.CourseData[a_courseID];
			}
		}
		else
		{
			player.Data.GetMapData(a_series, out a_mapdata);
		}
	}

	public void DebugCheatStageClear_Adv(int a_dungeon, int a_stage = -1)
	{
	}

	public void DebugCheatStageClearBundle_Adv(int a_dungeon, int a_stage = -1)
	{
	}

	public static int DebugCheatStageClear(int _oldLevel, int _newLevel, bool a_sub = false, int gotstar = 1)
	{
		if (_newLevel <= _oldLevel)
		{
			return _oldLevel;
		}
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		Player player = instance.mPlayer;
		bool flag = false;
		switch (instance.mGameStateManager.CurrentState)
		{
		case GameStateManager.GAME_STATE.MAP:
		case GameStateManager.GAME_STATE.EVENT_RALLY:
		case GameStateManager.GAME_STATE.EVENT_STAR:
		case GameStateManager.GAME_STATE.EVENT_RALLY2:
			flag = true;
			break;
		}
		if (!flag)
		{
			return -1;
		}
		GameStateSMMapBase gameStateSMMapBase = instance.mGameStateManager.mGameState as GameStateSMMapBase;
		if (gameStateSMMapBase == null)
		{
			return -1;
		}
		SMMapPage mapPage = gameStateSMMapBase.MapPage;
		if (mapPage == null)
		{
			return -1;
		}
		bool flag2 = false;
		PlayerEventData data = null;
		PlayerMapData data2;
		if (Def.IsEventSeries(player.Data.CurrentSeries))
		{
			flag2 = true;
			player.Data.GetMapData(player.Data.CurrentSeries, player.Data.CurrentEventID, out data);
			if (data == null)
			{
				return -1;
			}
			data2 = data.CourseData[data.CourseID];
		}
		else
		{
			player.Data.GetMapData(player.Data.CurrentSeries, out data2);
		}
		SMMapRoute<float>.RouteNode routeNode = null;
		List<SMMapRoute<float>.RouteNode> stageList = mapPage.GetStageList(100, _newLevel);
		foreach (SMMapRoute<float>.RouteNode item in stageList)
		{
			routeNode = item;
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(item.Value);
			int a_main;
			int a_sub2;
			Def.SplitStageNo(stageNoByRouteOrder, out a_main, out a_sub2);
			if (!routeNode.mIsStage)
			{
				continue;
			}
			bool flag3 = false;
			if (a_sub2 == 0)
			{
				flag3 = true;
			}
			else if (a_sub)
			{
				flag3 = true;
			}
			SMMapStageSetting mapStage;
			if (flag2)
			{
				SMEventPageData sMEventPageData = gameStateSMMapBase.MapPageData as SMEventPageData;
				if (sMEventPageData == null)
				{
					return -1;
				}
				mapStage = sMEventPageData.GetMapStage(a_main, a_sub2, data.CourseID);
			}
			else
			{
				mapStage = gameStateSMMapBase.MapPageData.GetMapStage(a_main, a_sub2);
			}
			if (mapStage != null)
			{
				if (mapStage.StageEnterCondition == "PARTNER_F" && mapStage.EnterPartners.Count == 1)
				{
					int num = mapStage.EnterPartners[0];
					if (!instance.mPlayer.IsCompanionUnlock(num))
					{
						AccessoryData accessoryData = null;
						List<AccessoryData> accessoryByType = instance.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
						for (int i = 0; i < accessoryByType.Count; i++)
						{
							AccessoryData accessoryData2 = accessoryByType[i];
							if (accessoryData2.GetCompanionID() == num)
							{
								accessoryData = accessoryData2;
								break;
							}
						}
						if (accessoryData != null)
						{
							SingletonMonoBehaviour<GameMain>.Instance.mPlayer.AddAccessory(accessoryData);
						}
					}
				}
				else if (mapStage.BoxID.Count > 0)
				{
					foreach (int item2 in mapStage.BoxID)
					{
						AccessoryData accessoryData3 = instance.GetAccessoryData(item2);
						if (accessoryData3 == null)
						{
							continue;
						}
						switch (accessoryData3.AccessoryType)
						{
						case AccessoryData.ACCESSORY_TYPE.RB_KEY:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_R:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_S:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_SS:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_STARS:
						case AccessoryData.ACCESSORY_TYPE.RB_KEY_GF00:
							continue;
						}
						if (SingletonMonoBehaviour<GameMain>.Instance.mDebugCheatClearGetItem)
						{
							SingletonMonoBehaviour<GameMain>.Instance.AddAccessoryFromPresentBox(item2);
						}
					}
				}
			}
			int a_main2;
			int a_sub3;
			Def.SplitStageNo(_newLevel, out a_main2, out a_sub3);
			bool flag4 = false;
			int a_course = 0;
			if (flag2)
			{
				PlayerClearData _psd = null;
				if (player.GetStageClearData(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder, out _psd))
				{
					if (_psd.GotStars < gotstar)
					{
						_psd.GotStars = gotstar;
					}
					continue;
				}
				Player.STAGE_STATUS stageStatus = player.GetStageStatus(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder);
				if (flag3)
				{
					if (stageStatus == global::Player.STAGE_STATUS.NOOPEN || stageStatus == global::Player.STAGE_STATUS.LOCK || stageStatus == global::Player.STAGE_STATUS.UNLOCK)
					{
						player.SetStageStatus(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder, global::Player.STAGE_STATUS.CLEAR);
						mapPage.UnlockStageButton(stageNoByRouteOrder, false);
					}
					PlayerEventClearData playerEventClearData = new PlayerEventClearData();
					playerEventClearData.Series = player.Data.CurrentSeries;
					playerEventClearData.StageNo = stageNoByRouteOrder;
					playerEventClearData.HightScore = 0;
					playerEventClearData.GotStars = gotstar;
					playerEventClearData.EventID = data.EventID;
					playerEventClearData.CourseID = data.CourseID;
					player.AddStageClearData(playerEventClearData);
					int stageNo = Def.GetStageNo(a_main, a_sub2);
					player.SetClearEventStageInfo(stageNo);
					SMEventPageData sMEventPageData2 = gameStateSMMapBase.MapPageData as SMEventPageData;
					if (sMEventPageData2 != null && sMEventPageData2.EventSetting.EventType == Def.EVENT_TYPE.SM_STAR)
					{
						flag4 = true;
						a_course = data.CourseID;
					}
				}
				else if (stageStatus == global::Player.STAGE_STATUS.NOOPEN || stageStatus == global::Player.STAGE_STATUS.LOCK)
				{
					player.SetStageStatus(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder, global::Player.STAGE_STATUS.UNLOCK);
					mapPage.UnlockStageButton(stageNoByRouteOrder, false);
				}
			}
			else
			{
				flag4 = true;
				a_course = 0;
				player.SetStageStatus(player.Data.CurrentSeries, stageNoByRouteOrder, global::Player.STAGE_STATUS.UNLOCK);
				mapPage.UnlockStageButton(stageNoByRouteOrder, false);
				PlayerClearData _psd2 = null;
				if (flag3 && !player.GetStageClearData(player.Data.CurrentSeries, stageNoByRouteOrder, out _psd2))
				{
					_psd2 = new PlayerClearData();
					_psd2.Series = player.Data.CurrentSeries;
					_psd2.StageNo = stageNoByRouteOrder;
					_psd2.HightScore = 0;
					_psd2.GotStars = gotstar;
					player.AddStageClearData(_psd2);
					int stageNo2 = Def.GetStageNo(a_main, a_sub2);
					player.SetClearStageInfo(stageNo2);
					instance.DebugTutorialComplete(stageNoByRouteOrder);
				}
			}
			if (!flag4)
			{
				continue;
			}
			SMMapPageData mapPageData = gameStateSMMapBase.MapPageData;
			SMRoadBlockSetting roadBlock = mapPageData.GetRoadBlock(a_main, 0, a_course);
			if (roadBlock == null || data2.IsUnlockedRoadBlock(roadBlock.RoadBlockID))
			{
				continue;
			}
			int num2 = a_main2;
			if (num2 > roadBlock.mStageNo)
			{
				int stageNo3 = Def.GetStageNo(roadBlock.mStageNo, 0);
				if (!flag2)
				{
					data2.RoadBlockReachLevel = stageNo3;
					List<AccessoryData> roadBlockAccessory = instance.GetRoadBlockAccessory(stageNo3, player.Data.CurrentSeries);
					for (int j = 0; j < roadBlockAccessory.Count; j++)
					{
						instance.mPlayer.AddAccessory(roadBlockAccessory[j]);
					}
				}
				else
				{
					SMEventPageData sMEventPageData3 = gameStateSMMapBase.MapPageData as SMEventPageData;
					if (sMEventPageData3 != null && sMEventPageData3.EventSetting.EventType == Def.EVENT_TYPE.SM_STAR)
					{
						SMEventRoadBlockSetting sMEventRoadBlockSetting = roadBlock as SMEventRoadBlockSetting;
						if (sMEventRoadBlockSetting != null && sMEventRoadBlockSetting.AccessoryIDOnOpened != 0)
						{
							AccessoryData accessoryData4 = SingletonMonoBehaviour<GameMain>.Instance.GetAccessoryData(sMEventRoadBlockSetting.AccessoryIDOnOpened);
							if (accessoryData4 != null && !player.IsAccessoryUnlock(accessoryData4.Index))
							{
								player.AddAccessory(accessoryData4, true, false, 12);
							}
						}
					}
				}
				List<SMRoadBlockSetting> roadBlockInCourse = mapPageData.GetRoadBlockInCourse(a_course);
				roadBlockInCourse.Sort((SMRoadBlockSetting x, SMRoadBlockSetting y) => x.RoadBlockID - y.RoadBlockID);
				int num3 = roadBlock.RoadBlockID + 1;
				if (num3 >= roadBlockInCourse.Count)
				{
					data2.NextRoadBlockLevel = Def.GetStageNo(999, 0);
				}
				else
				{
					data2.UpdateUnlockedRoadBlock(roadBlock.RoadBlockID);
					data2.NextRoadBlockLevel = roadBlockInCourse[num3].RoadBlockLevel;
					data2.RoadBlockReachLevel = roadBlock.RoadBlockLevel;
				}
				instance.mPlayer.SetRoadBlockKey(player.Data.CurrentSeries, 0);
			}
			else if (num2 == roadBlock.mStageNo)
			{
				data2.RoadBlockReachLevel = roadBlock.RoadBlockLevel;
				DateTime now = DateTime.Now;
				data2.RoadBlockReachTime = now;
			}
			if (flag2)
			{
				data.CourseData[data.CourseID] = data2;
				player.Data.SetMapData(player.Data.CurrentEventID, data);
			}
			else
			{
				player.Data.SetMapData(player.Data.CurrentSeries, data2);
			}
		}
		if (routeNode != null)
		{
			if (routeNode.NextMain != null && routeNode.NextMain.mIsStage)
			{
				int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode.NextMain.Value);
				if (flag2)
				{
					Player.STAGE_STATUS stageStatus2 = player.GetStageStatus(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder2);
					if (stageStatus2 == global::Player.STAGE_STATUS.NOOPEN || stageStatus2 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder2, global::Player.STAGE_STATUS.UNLOCK);
						mapPage.UnlockStageButton(stageNoByRouteOrder2, false);
					}
				}
				else
				{
					Player.STAGE_STATUS stageStatus3 = player.GetStageStatus(player.Data.CurrentSeries, stageNoByRouteOrder2);
					if (stageStatus3 == global::Player.STAGE_STATUS.NOOPEN || stageStatus3 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(player.Data.CurrentSeries, stageNoByRouteOrder2, global::Player.STAGE_STATUS.UNLOCK);
						mapPage.UnlockStageButton(stageNoByRouteOrder2, false);
					}
				}
			}
			if (routeNode.NextSub != null && routeNode.NextSub.mIsStage)
			{
				int stageNoByRouteOrder3 = Def.GetStageNoByRouteOrder(routeNode.NextSub.Value);
				if (flag2)
				{
					Player.STAGE_STATUS stageStatus4 = player.GetStageStatus(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder3);
					if (stageStatus4 == global::Player.STAGE_STATUS.NOOPEN || stageStatus4 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(player.Data.CurrentSeries, data.EventID, data.CourseID, stageNoByRouteOrder3, global::Player.STAGE_STATUS.UNLOCK);
						mapPage.UnlockStageButton(stageNoByRouteOrder3, false);
					}
				}
				else
				{
					Player.STAGE_STATUS stageStatus5 = player.GetStageStatus(player.Data.CurrentSeries, stageNoByRouteOrder3);
					if (stageStatus5 == global::Player.STAGE_STATUS.NOOPEN || stageStatus5 == global::Player.STAGE_STATUS.LOCK)
					{
						player.SetStageStatus(player.Data.CurrentSeries, stageNoByRouteOrder3, global::Player.STAGE_STATUS.UNLOCK);
						mapPage.UnlockStageButton(stageNoByRouteOrder3, false);
					}
				}
			}
		}
		SMEventPageData sMEventPageData4 = gameStateSMMapBase.MapPageData as SMEventPageData;
		if (sMEventPageData4 != null && sMEventPageData4.EventSetting.EventType == Def.EVENT_TYPE.SM_RALLY2)
		{
			int stageNoByRouteOrder4 = Def.GetStageNoByRouteOrder(routeNode.NextMain.Value);
			for (int k = 0; k < stageNoByRouteOrder4; k += 100)
			{
				player.SetLineStatus(player.Data.CurrentSeries, data.EventID, 0, k, global::Player.STAGE_STATUS.CLEAR);
			}
		}
		SingletonMonoBehaviour<GameMain>.Instance.Save();
		return _newLevel;
	}

	public void DebugTutorialComplete(int stageno)
	{
	}

	public void LogDebug(string log, string prefix)
	{
	}

	public void ClearWonHistory()
	{
		mWonHistory.Clear();
	}

	public bool[] GetWonHistory(int start, int max_length)
	{
		if (start < 0 || start >= mWonHistory.Count)
		{
			return null;
		}
		int num = ((start + max_length >= mWonHistory.Count) ? (mWonHistory.Count - start) : max_length);
		bool[] array = new bool[num];
		for (int i = 0; i < num; i++)
		{
			array[i] = mWonHistory[mWonHistory.Count - 1 - (i + start)];
		}
		return array;
	}

	public int GetWonCount(int start, int max_length, bool won_flag)
	{
		bool[] wonHistory = GetWonHistory(start, max_length);
		int num = 0;
		if (wonHistory != null)
		{
			bool[] array = wonHistory;
			foreach (bool flag in array)
			{
				if (flag == won_flag)
				{
					num++;
				}
			}
		}
		return num;
	}

	public int GetWonMaxCountContinue(int start, int max_length, bool won_flag)
	{
		bool[] wonHistory = GetWonHistory(start, max_length);
		int num = 0;
		if (wonHistory != null)
		{
			int num2 = 0;
			bool[] array = wonHistory;
			foreach (bool flag in array)
			{
				if (flag != won_flag)
				{
					num2 = 0;
				}
				else if (++num2 > num || num == -1)
				{
					num = num2;
				}
			}
		}
		return num;
	}

	private Dictionary<string, string> _UniversalProperties()
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary["Language"] = SingletonMonoBehaviour<Network>.Instance.COUNTRY_CODE;
		dictionary["OS Version"] = SystemInfo.operatingSystem;
		dictionary["Version"] = BIJUnity.getBundleVersion().ToString();
		dictionary["Num Purchases"] = mOptions.PurchaseCount.ToString();
		dictionary["Current Level"] = mPlayer.CurrentLevel.ToString();
		dictionary["Current Episode"] = mPlayer.Level.ToString();
		dictionary["Total Playtime"] = mPlayer.Data.TotalPlayTime.ToString();
		dictionary["Num Sessions"] = mPlayer.Data.PlayCount.ToString();
		dictionary["Lives Remaining"] = mPlayer.LifeCount.ToString();
		dictionary["Current PC"] = mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 3).ToString();
		dictionary["Lifetime PC"] = mOptions.TotalMasterCurrency.ToString();
		dictionary["Current Coins"] = mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 4).ToString();
		dictionary["Lifetime Coins"] = mOptions.TotalSecondaryCurrency.ToString();
		dictionary["Overall Wins"] = mPlayer.Data.TotalWin.ToString();
		dictionary["Overall Loses"] = mPlayer.Data.TotalLose.ToString();
		dictionary["Overall Win Percentage"] = mPlayer.WinRate.ToString();
		dictionary["isFB"] = ((!string.IsNullOrEmpty(mOptions.Fbid)) ? "1" : "0");
		dictionary["isGC"] = ((!string.IsNullOrEmpty(mOptions.Gcid)) ? "1" : "0");
		dictionary["isGP"] = ((!string.IsNullOrEmpty(mOptions.Gpid)) ? "1" : "0");
		dictionary["isHIVE"] = ((mPlayer.HiveId != 0) ? "1" : "0");
		dictionary["Num Friends"] = mPlayer.Friends.Count.ToString();
		dictionary["App. Version"] = Def.AppVersionStr;
		dictionary["Srv. Version"] = Def.SrvVersionStr;
		dictionary["Dat. Version"] = Def.VersionStr;
		dictionary["Client Data Version"] = ((short)1000).ToString();
		return dictionary;
	}

	public string GetUserSegment()
	{
		return GetUserSegment("1");
	}

	public string GetUserSegment(string defaultValue)
	{
		string result = defaultValue;
		if (mNetworkProfile == null)
		{
			return result;
		}
		if (!string.IsNullOrEmpty(mNetworkProfile.useg))
		{
			result = mNetworkProfile.useg;
		}
		return result;
	}

	public void WinRate(int stageNumber, int stageFormatVersion, int stageRevision, string userSegment, string puzzleMode, bool isWon, int stageScore, int stageGotStars, int stageMoves, float stageTime, int continueCount, int maxCombo, int useSkill, Dictionary<Def.BOOSTER_KIND, int> useBooster, Dictionary<Def.BOOSTER_KIND, int> usePaidBooster, Dictionary<Def.TILE_KIND, int> dropPresentBox, SpecialSpawnData[] collectItems, int x_winScore, Dictionary<Def.TILE_FORM, int> ubaCreatedSpecialTileCount, Dictionary<Def.TILE_FORM, int> ubaCrushedSpecialTileCount, Dictionary<Def.TILE_KIND, int> ubaCrushedSpecialCrossCount, bool chanceAnnounce)
	{
		int tgt_stage = stageNumber;
		int num = (int)mPlayer.Data.CurrentSeries;
		int num3 = (LastWon = (isWon ? 1 : 0));
		mWonHistory.Add(isWon);
		int ptime = (int)stageTime;
		int num5 = (LastStar = stageGotStars);
		int lifeCount = mPlayer.LifeCount;
		int[] array = new int[15];
		useBooster.TryGetValue(Def.BOOSTER_KIND.WAVE_BOMB, out array[1]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.TAP_BOMB2, out array[2]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.RAINBOW, out array[3]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.ADD_SCORE, out array[4]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.SKILL_CHARGE, out array[5]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.SELECT_ONE, out array[6]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.SELECT_COLLECT, out array[7]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.COLOR_CRUSH, out array[8]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.BLOCK_CRUSH, out array[9]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.PAIR_MAKER, out array[13]);
		useBooster.TryGetValue(Def.BOOSTER_KIND.ALL_CRUSH, out array[14]);
		int num6 = array[1] + array[2] + array[3] + array[4] + array[5];
		int num7 = array[6] + array[7] + array[8] + array[9] + array[13] + array[14];
		int[] array2 = new int[15];
		int[] array3 = new int[15];
		for (int i = 0; i < array.Length; i++)
		{
			int value;
			if (!usePaidBooster.TryGetValue((Def.BOOSTER_KIND)i, out value))
			{
				value = 0;
			}
			array2[i] = array[i] - value;
			array3[i] = value;
		}
		int value2 = 0;
		dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX0, out value2);
		int value3 = 0;
		dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX1, out value3);
		int value4 = 0;
		dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX2, out value4);
		int last_login = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mPlayer.mLastLoginTime);
		int game_start = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.GameStartTime);
		int first_purchase = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.FirstPurchasedTime);
		long startupCount = mPlayer.Data.StartupCount;
		long playCount = mPlayer.Data.PlayCount;
		long totalWin = mPlayer.Data.TotalWin;
		long totalLose = mPlayer.Data.TotalLose;
		float winRate = mPlayer.WinRate;
		long purchaseCount = mOptions.PurchaseCount;
		int stage_win = 0;
		int stage_lose = 0;
		float stage_win_rate = 0f;
		PlayerStageData _psd = null;
		if (IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
		{
			tgt_stage = int.Parse(((mDebugDailyChallengeID <= 0) ? GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID : mDebugDailyChallengeID) + (SelectedDailyChallengeSheetNo - 1).ToString("00") + SelectedDailyChallengeStageNo.ToString("0"));
		}
		else if (mEventMode)
		{
			int eventID = mCurrentStage.EventID;
			short a_course = 0;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
			mPlayer.GetStageChallengeData(mCurrentStage.Series, eventID, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
			int stageNo = Def.GetStageNo(a_course, a_main, a_sub);
			tgt_stage = Def.GetStageNoForServer(eventID, stageNo);
		}
		else
		{
			mPlayer.GetStageChallengeData(mCurrentStage.Series, mCurrentStage.StageNumber, out _psd);
		}
		if (_psd != null)
		{
			stage_win = _psd.WinCount;
			stage_lose = _psd.LoseCount;
			stage_win_rate = _psd.WinRate;
		}
		int character_ID = mCompanionIndex;
		int companionLevel = mPlayer.GetCompanionLevel(mCompanionIndex);
		int num8 = 1;
		if (useSkill > 0)
		{
			num8 = ((num6 > 0 && num7 > 0) ? 8 : ((num7 > 0) ? 7 : ((num6 <= 0) ? 4 : 6)));
		}
		else if (num6 > 0 && num7 > 0)
		{
			num8 = 5;
		}
		else if (num7 > 0)
		{
			num8 = 3;
		}
		else if (num6 > 0)
		{
			num8 = 2;
		}
		if (continueCount > 0)
		{
			num8 += 100;
		}
		if (num3 == 0 && mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
		{
			num8 += 200;
		}
		if (chanceAnnounce)
		{
			num8 += 1000;
		}
		if (mPlayer.Data.UseSpecialChance)
		{
			num8 += 10000;
		}
		else if (mPlayer.Data.UsePlusChance)
		{
			num8 += 20000;
		}
		else if (mPlayer.Data.UseScoreUpChance)
		{
			num8 += 30000;
		}
		if (mPlayer.Data.UseContinueChance)
		{
			num8 += 100000;
		}
		int num9 = 0;
		int num10 = 0;
		int num11 = 0;
		int num12 = 0;
		int num13 = 0;
		if (ubaCreatedSpecialTileCount != null)
		{
			if (ubaCreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_H))
			{
				num9 += ubaCreatedSpecialTileCount[Def.TILE_FORM.WAVE_H];
			}
			if (ubaCreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_V))
			{
				num9 += ubaCreatedSpecialTileCount[Def.TILE_FORM.WAVE_V];
			}
			if (ubaCreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.BOMB))
			{
				num10 += ubaCreatedSpecialTileCount[Def.TILE_FORM.BOMB];
			}
			if (ubaCreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.RAINBOW))
			{
				num11 += ubaCreatedSpecialTileCount[Def.TILE_FORM.RAINBOW];
			}
			if (ubaCreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.PAINT))
			{
				num12 += ubaCreatedSpecialTileCount[Def.TILE_FORM.PAINT];
			}
			if (ubaCreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.TAP_BOMB))
			{
				num13 += ubaCreatedSpecialTileCount[Def.TILE_FORM.TAP_BOMB];
			}
		}
		int num14 = 0;
		int num15 = 0;
		int num16 = 0;
		int num17 = 0;
		int num18 = 0;
		if (ubaCrushedSpecialTileCount != null)
		{
			if (ubaCrushedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_H))
			{
				num14 += ubaCrushedSpecialTileCount[Def.TILE_FORM.WAVE_H];
			}
			if (ubaCrushedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_V))
			{
				num14 += ubaCrushedSpecialTileCount[Def.TILE_FORM.WAVE_V];
			}
			if (ubaCrushedSpecialTileCount.ContainsKey(Def.TILE_FORM.BOMB))
			{
				num15 += ubaCrushedSpecialTileCount[Def.TILE_FORM.BOMB];
			}
			if (ubaCrushedSpecialTileCount.ContainsKey(Def.TILE_FORM.RAINBOW))
			{
				num16 += ubaCrushedSpecialTileCount[Def.TILE_FORM.RAINBOW];
			}
			if (ubaCrushedSpecialTileCount.ContainsKey(Def.TILE_FORM.PAINT))
			{
				num17 += ubaCrushedSpecialTileCount[Def.TILE_FORM.PAINT];
			}
			if (ubaCrushedSpecialTileCount.ContainsKey(Def.TILE_FORM.TAP_BOMB))
			{
				num18 += ubaCrushedSpecialTileCount[Def.TILE_FORM.TAP_BOMB];
			}
		}
		int num19 = 0;
		int num20 = 0;
		int num21 = 0;
		int num22 = 0;
		int num23 = 0;
		int num24 = 0;
		int num25 = 0;
		int num26 = 0;
		int num27 = 0;
		int num28 = 0;
		if (ubaCrushedSpecialCrossCount != null)
		{
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.WAVE_WAVE))
			{
				num19 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.WAVE_WAVE];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.WAVE_BOMB))
			{
				num20 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.WAVE_BOMB];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.WAVE_RAINBOW))
			{
				num21 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.WAVE_RAINBOW];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.WAVE_PAINT))
			{
				num22 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.WAVE_PAINT];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.BOMB_BOMB))
			{
				num23 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.BOMB_BOMB];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.BOMB_RAINBOW))
			{
				num24 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.BOMB_RAINBOW];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.BOMB_PAINT))
			{
				num25 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.BOMB_PAINT];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.RAINBOW_RAINBOW))
			{
				num26 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.RAINBOW_RAINBOW];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.PAINT_RAINBOW))
			{
				num27 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.PAINT_RAINBOW];
			}
			if (ubaCrushedSpecialCrossCount.ContainsKey(Def.TILE_KIND.PAINT_PAINT))
			{
				num28 += ubaCrushedSpecialCrossCount[Def.TILE_KIND.PAINT_PAINT];
			}
		}
		if (IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
		{
			num = 200;
			int evt_id = ((mDebugDailyChallengeID <= 0) ? GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID : mDebugDailyChallengeID);
			ServerCram.DailyChallengeWinRate(tgt_stage, num, evt_id, SelectedDailyChallengeSheetNo - 1, stageRevision, userSegment, puzzleMode, num3, num8, stageScore, stageMoves, ptime, continueCount, maxCombo, lifeCount, last_login, game_start, first_purchase, purchaseCount, startupCount, playCount, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, character_ID, companionLevel, useSkill, num9, num10, num11, num12, num13, num14, num15, num16, num17, num18, num19, num20, num21, num22, num23, num24, num25, num26, num27, num28);
		}
		else if (mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			ISMStageData aStage;
			SMMapStageSetting aStageSetting;
			GetPreStage(out aStage, out aStageSetting);
			int eventID2 = aStage.EventID;
			int stageNoForServer = Def.GetStageNoForServer(eventID2, aStage.StageNumber);
			int mOrgStageScore = ServerCramVariableData.mOrgStageScore;
			ServerCram.StarGetWinRate(tgt_stage, stageNoForServer, eventID2, stageFormatVersion, stageRevision, userSegment, puzzleMode, num3, num8, mOrgStageScore, stageMoves, ptime, continueCount, maxCombo, first_purchase, character_ID, companionLevel, useSkill);
		}
		else if (mOldEventMode)
		{
			int eventID3 = mCurrentStage.EventID;
			ServerCram.OldEventWinRate(tgt_stage, eventID3, stageFormatVersion, stageRevision, userSegment, puzzleMode, num3, num8, stageScore, num5, stageMoves, ptime, continueCount, maxCombo, lifeCount, last_login, game_start, first_purchase, purchaseCount, startupCount, playCount, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, character_ID, companionLevel, useSkill, num9, num10, num11, num12, num13, num14, num15, num16, num17, num18, num19, num20, num21, num22, num23, num24, num25, num26, num27, num28);
		}
		else if (mEventMode)
		{
			int eventID4 = mCurrentStage.EventID;
			ServerCram.EventWinRate(tgt_stage, eventID4, stageFormatVersion, stageRevision, userSegment, puzzleMode, num3, num8, stageScore, num5, stageMoves, ptime, continueCount, maxCombo, lifeCount, last_login, game_start, first_purchase, purchaseCount, startupCount, playCount, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, character_ID, companionLevel, useSkill, num9, num10, num11, num12, num13, num14, num15, num16, num17, num18, num19, num20, num21, num22, num23, num24, num25, num26, num27, num28);
		}
		else
		{
			if (PlayingFriendHelp == null)
			{
				ServerCram.WinRate(tgt_stage, num, stageFormatVersion, stageRevision, userSegment, puzzleMode, num3, num8, stageScore, num5, stageMoves, ptime, continueCount, maxCombo, lifeCount, last_login, game_start, first_purchase, purchaseCount, startupCount, playCount, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, character_ID, companionLevel, useSkill, num9, num10, num11, num12, num13, num14, num15, num16, num17, num18, num19, num20, num21, num22, num23, num24, num25, num26, num27, num28);
			}
			else
			{
				int fromID = PlayingFriendHelp.Gift.Data.FromID;
			}
			if (mCurrentStageInfo.IsDailyEvent)
			{
				int num29 = 0;
				foreach (SpecialSpawnData specialSpawnData in collectItems)
				{
					for (int k = 0; k < mCurrentStageInfo.DailyEventBoxID.Count; k++)
					{
						if (specialSpawnData.ItemIndex == mCurrentStageInfo.DailyEventBoxID[k])
						{
							num29 = specialSpawnData.ItemIndex;
							break;
						}
					}
					if (num29 != 0)
					{
						break;
					}
				}
				ServerCram.DailyEventWinRate(tgt_stage, num, stageFormatVersion, stageRevision, userSegment, puzzleMode, num3, num8, stageScore, num5, stageMoves, ptime, continueCount, maxCombo, lifeCount, last_login, game_start, first_purchase, purchaseCount, startupCount, playCount, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, character_ID, companionLevel, useSkill, num29);
			}
		}
		int use_type = 0;
		if (mEventMode)
		{
			use_type = 1;
		}
		else if (PlayingFriendHelp != null)
		{
			use_type = 2;
		}
		string boosterNumString = ServerCram.GetBoosterNumString();
		for (int l = 0; l < array.Length; l++)
		{
			if (array[l] > 0)
			{
				int kind = BoosterID_ChangeShopID(l);
				ServerCram.UseItem(array[l], array2[l], array3[l], tgt_stage, num, use_type, kind, num3, playCount, boosterNumString);
				UserBehavior.Instance.UseBooster((Def.BOOSTER_KIND)l, (short)array[l], (Def.SERIES)num, mCurrentStage.WinType);
			}
		}
		short aMakeSpecial = (short)(num9 + num10 + num11 + num12 + num13);
		short aSwapSpecial = (short)(num19 + num20 + num21 + num22 + num23 + num24 + num25 + num26 + num27 + num28);
		UserBehavior.Instance.Play((Def.SERIES)num, mCurrentStage.WinType, isWon, (short)num5, aMakeSpecial, aSwapSpecial);
		UserBehavior.Save();
	}

	private void OnNotification_ScheduleLogin()
	{
		short iconCompanionPersonalID = GetIconCompanionPersonalID();
		int afterSeconds = 172800;
		if (USE_DEBUG_DIALOG && mDebugLogin00NotificationSec > 0)
		{
			afterSeconds = mDebugLogin00NotificationSec;
		}
		string key = string.Format("Notification_Login_00_{0:0000}", iconCompanionPersonalID);
		if (!Localization.Exists(key))
		{
			key = "Notification_Login_00_0000";
		}
		string title = Localization.Get("Notification_Title");
		string message = Localization.Get(key);
		NotificationManager.scheduleNotification(2, message, title, afterSeconds);
		afterSeconds = 345600;
		if (USE_DEBUG_DIALOG && mDebugLogin01NotificationSec > 0)
		{
			afterSeconds = mDebugLogin01NotificationSec;
		}
		key = string.Format("Notification_Login_01_{0:0000}", iconCompanionPersonalID);
		if (!Localization.Exists(key))
		{
			key = "Notification_Login_01_0000";
		}
		title = Localization.Get("Notification_Title");
		message = Localization.Get(key);
		NotificationManager.scheduleNotification(3, message, title, afterSeconds);
	}

	private void OnNotification_ScheduleStatus()
	{
		DateTime fullRecoverDateTime = GetFullRecoverDateTime();
		if (fullRecoverDateTime == DateTimeUtil.UnitLocalTimeEpoch)
		{
			return;
		}
		int num = (int)((fullRecoverDateTime - DateTime.Now).TotalSeconds + 0.5);
		if (num >= 1)
		{
			if (USE_DEBUG_DIALOG && mDebugLifeNotificationSec > 0)
			{
				num = mDebugLifeNotificationSec;
			}
			short iconCompanionPersonalID = GetIconCompanionPersonalID();
			string key = string.Format("Notification_HeartRecover{0:0000}", iconCompanionPersonalID);
			if (!Localization.Exists(key))
			{
				key = "Notification_HeartRecover0000";
			}
			string title = Localization.Get("Notification_Title");
			string message = Localization.Get(key);
			NotificationManager.scheduleNotification(1, message, title, num);
		}
	}

	private short GetIconCompanionPersonalID()
	{
		short result = 0;
		if (mPlayer != null)
		{
			int num = mPlayer.Data.PlayerIcon;
			if (num >= 10000)
			{
				num -= 10000;
			}
			if (mCompanionData.Count > num)
			{
				result = mCompanionData[num].personalId;
			}
		}
		return result;
	}

	public static void ApplicationQuit()
	{
		ApplicationQuit(null);
	}

	public static void ApplicationQuit(string openURL)
	{
		SingletonMonoBehaviour<GameMain>.Instance.DoApplicationQuit(openURL);
	}

	private void DoApplicationQuit(string openURL)
	{
		if (!IsQuitting)
		{
			IsQuitting = true;
			if (mLogManager != null)
			{
				mLogManager.Dump();
			}
			StartCoroutine(DelayedQuit(openURL));
			InternetReachability.Dispose();
		}
	}

	private IEnumerator DelayedQuit(string openURL)
	{
		IsWaitForQuit = true;
		float _waitTime3 = 0f;
		yield return 0;
		TermNetwork();
		TermMarketing();
		float _startTime = Time.realtimeSinceStartup;
		_waitTime3 = 0f;
		do
		{
			yield return new WaitForSeconds(0.05f);
			_waitTime3 = Time.realtimeSinceStartup - _startTime;
		}
		while (_waitTime3 < 0.5f && IsWaitForQuit);
		IsTerminated = true;
		if (!string.IsNullOrEmpty(openURL))
		{
			Application.OpenURL(openURL);
			yield return 0;
		}
		Application.Quit();
		yield return new WaitForSeconds(0.5f);
		Process _p = Process.GetCurrentProcess();
		ProcessThread[] _t = new ProcessThread[_p.Threads.Count];
		_p.Threads.CopyTo(_t, 0);
		for (int i = 0; i < _t.Length; i++)
		{
			_t[i].Dispose();
		}
		_p.Kill();
	}

	private void OnApplicationQuit()
	{
		if (!IsTerminated && !IsQuitting)
		{
			ApplicationQuit();
		}
	}

	private void OnApplicationFocus(bool focusStatus)
	{
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			adrenoParticled = false;
		}
		if (LockRotationOnApplicationPause)
		{
			if (pauseStatus)
			{
				LockRotation();
			}
			else
			{
				UnlockRotation();
			}
		}
		if (IsLoaded)
		{
			if (pauseStatus)
			{
				StartCoroutine(SingletonMonoBehaviour<GameMain>.Instance.CommonPauseProcess());
			}
			else
			{
				StartCoroutine(SingletonMonoBehaviour<GameMain>.Instance.CommonResumeProcess());
			}
		}
	}

	public IEnumerator CommonPauseProcess()
	{
		IsPlayable = false;
		IsMedalRecover = false;
		if (!DISABLE_TIMECHECK && mPlayer.CheckTimeCheatNoPenalty())
		{
			int _remote_time = 0;
			if (ServerTime.HasValue)
			{
				_remote_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(ServerTime.Value);
			}
			int _cur_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now.ToUniversalTime());
			int _local_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mPlayer.Data.LastPlayTime.ToUniversalTime());
			ServerCram.CheatUserSut(_remote_time, _cur_time, _local_time, 1);
		}
		mPlayer.Data.LastPlayTime = DateTime.Now;
		if (mGameStateManager.IsInAdvState())
		{
			mPlayer.AdvSaveData.LastPlayTime = DateTime.Now;
		}
		IsWaitForQuit = false;
		yield break;
	}

	public IEnumerator CommonResumeProcess()
	{
		mAvailableRotation = BIJUnity.isAvailableRotation();
		if (mAvailableRotation)
		{
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
			Screen.autorotateToPortrait = true;
			Screen.autorotateToPortraitUpsideDown = true;
		}
		else
		{
			Screen.autorotateToLandscapeLeft = false;
			Screen.autorotateToLandscapeRight = false;
			Screen.autorotateToPortrait = false;
			Screen.autorotateToPortraitUpsideDown = false;
			switch (Util.ScreenOrientation)
			{
			case ScreenOrientation.LandscapeLeft:
				Screen.autorotateToLandscapeLeft = true;
				break;
			case ScreenOrientation.LandscapeRight:
				Screen.autorotateToLandscapeRight = true;
				break;
			case ScreenOrientation.Portrait:
				Screen.autorotateToPortrait = true;
				break;
			case ScreenOrientation.PortraitUpsideDown:
				Screen.autorotateToPortraitUpsideDown = true;
				break;
			}
		}
		int playspan;
		if (CountPlayerBoot(out playspan))
		{
		}
		ServerCram.SendDau(mGameStateManager.IsInMainState());
		if (mGameStateManager.IsInAdvState())
		{
			CountAdvPlayerBoot(out playspan);
			ServerCram.SendAdvDau();
		}
		yield return null;
		GetGemCountFreqFlag = false;
		mDLFileIntegrityCheckDone = false;
		yield return null;
		yield return null;
		IsPlayable = true;
		IsWaitForQuit = false;
	}

	public static void LockRotation()
	{
		if (prevOrientation == ScreenOrientation.Unknown && mPrevOrientation == ScreenOrientation.Unknown)
		{
			prevOrientation = Util.ScreenOrientation;
			autorotateToLandscapeLeft = Screen.autorotateToLandscapeLeft;
			autorotateToLandscapeRight = Screen.autorotateToLandscapeRight;
			autorotateToPortrait = Screen.autorotateToPortrait;
			autorotateToPortraitUpsideDown = Screen.autorotateToPortraitUpsideDown;
			if (autorotateToLandscapeLeft | autorotateToLandscapeRight | autorotateToPortrait | autorotateToPortraitUpsideDown)
			{
				Screen.autorotateToLandscapeLeft = false;
				Screen.autorotateToLandscapeRight = false;
				Screen.autorotateToPortrait = false;
				Screen.autorotateToPortraitUpsideDown = false;
			}
			switch (Util.ScreenOrientation)
			{
			case ScreenOrientation.Portrait:
				Util.ScreenOrientation = ScreenOrientation.Portrait;
				break;
			case ScreenOrientation.PortraitUpsideDown:
				Util.ScreenOrientation = ScreenOrientation.PortraitUpsideDown;
				break;
			case ScreenOrientation.LandscapeLeft:
				Util.ScreenOrientation = ScreenOrientation.LandscapeLeft;
				break;
			case ScreenOrientation.LandscapeRight:
				Util.ScreenOrientation = ScreenOrientation.LandscapeRight;
				break;
			}
		}
	}

	public static void UnlockRotation()
	{
		if (prevOrientation == ScreenOrientation.Unknown)
		{
			return;
		}
		if (mPrevOrientation == ScreenOrientation.Unknown)
		{
			if (autorotateToPortrait | autorotateToPortraitUpsideDown | autorotateToLandscapeLeft | autorotateToLandscapeRight)
			{
				Util.ScreenOrientation = ScreenOrientation.AutoRotation;
				Screen.autorotateToLandscapeLeft = autorotateToLandscapeLeft;
				Screen.autorotateToLandscapeRight = autorotateToLandscapeRight;
				Screen.autorotateToPortrait = autorotateToPortrait;
				Screen.autorotateToPortraitUpsideDown = autorotateToPortraitUpsideDown;
			}
			else
			{
				Util.ScreenOrientation = prevOrientation;
			}
		}
		prevOrientation = ScreenOrientation.Unknown;
	}

	public void ToggleBGM()
	{
		bool bGMEnable = mOptions.BGMEnable;
		if (bGMEnable)
		{
			StopAllMusic(0f, false);
		}
		else
		{
			ReplayMusic();
		}
		mOptions.BGMEnable = !bGMEnable;
	}

	public void ToggleSE()
	{
		bool sEEnable = mOptions.SEEnable;
		if (sEEnable)
		{
			StopSeAll();
		}
		mOptions.SEEnable = !sEEnable;
	}

	public void ToggleNOTIFICATION()
	{
		bool useNotification = mOptions.UseNotification;
		mOptions.UseNotification = !useNotification;
		NotificationManager.IsNotificationEnabled = mOptions.UseNotification;
	}

	public void ToggleComboCutIn()
	{
		bool comboCutIn = mOptions.ComboCutIn;
		mOptions.ComboCutIn = !comboCutIn;
	}

	public static bool CanCommunication()
	{
		if (!IsPlayable)
		{
			return false;
		}
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (instance.mPlayer == null)
		{
			return false;
		}
		return true;
	}

	public static void ResetMaintenance()
	{
		MaintenanceMode = false;
		MaintenanceMessage = null;
		MaintenanceURL = null;
	}

	public void Debug_MaintenanceMode()
	{
		MaintenanceMode = mMaintenanceProfile.IsMaintenanceMode;
	}

	public bool CheckTimeCheater()
	{
		bool result = DoCheckTimeCheater();
		mPlayer.Data.LastPlayTime = DateTime.Now;
		return result;
	}

	private bool DoCheckTimeCheater()
	{
		if (DISABLE_TIMECHECK)
		{
			return false;
		}
		bool flag = mPlayer.CheckTimeCheatPenalty();
		bool isRemoteTimeCheater = IsRemoteTimeCheater;
		if (flag || isRemoteTimeCheater)
		{
			int num = 0;
			if (ServerTime.HasValue)
			{
				num = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(ServerTime.Value);
			}
			int num2 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now.ToUniversalTime());
			int num3 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mPlayer.Data.LastPlayTime.ToUniversalTime());
			ServerCram.CheatUserSut(num, num2, num3, flag ? 1 : 0);
			return true;
		}
		return false;
	}

	private void InitEnableTransferFlag()
	{
		IsEnableTransfer = false;
	}

	public void UpdateEnableTransferFlag()
	{
		if (mNetworkProfile == null)
		{
			InitEnableTransferFlag();
		}
		else
		{
			IsEnableTransfer = mNetworkProfile.trans_flag == 1;
		}
	}

	public Player GetSaveDataOfOthersPlayerData()
	{
		Player player = null;
		BIJBinaryReader bIJBinaryReader = null;
		try
		{
			string pLAYER_FILE_HASH = Constants.PLAYER_FILE_HASH;
			byte[] buffer = new byte[0];
			string path = Path.Combine(BIJDataBinaryReader.GetBasePath(), Constants.OTHER_PLAYER_FILE);
			if (File.Exists(path))
			{
				using (BIJBinaryReader bIJBinaryReader2 = new BIJZippedEncryptFileBinaryReader(path, pLAYER_FILE_HASH))
				{
					buffer = bIJBinaryReader2.ReadAll();
				}
			}
			else
			{
				path = Path.Combine(BIJDataBinaryReader.GetBasePath(), Constants.OTHER_PLAYER_FILE_OLD);
				if (File.Exists(path))
				{
					using (BIJBinaryReader bIJBinaryReader3 = new BIJZippedOneFileBinaryReader(path, pLAYER_FILE_HASH))
					{
						byte[] bytes = bIJBinaryReader3.ReadAll();
						buffer = CryptUtils.DecryptRJ128(bytes);
					}
				}
			}
			bIJBinaryReader = new BIJMemoryBinaryReader(buffer);
			return global::Player.Deserialize(bIJBinaryReader);
		}
		catch (Exception)
		{
			return null;
		}
		finally
		{
			if (bIJBinaryReader != null)
			{
				try
				{
					bIJBinaryReader.Close();
					bIJBinaryReader.Dispose();
				}
				catch (Exception)
				{
				}
				bIJBinaryReader = null;
			}
		}
	}

	public Player GetBackupPlayerData()
	{
		Player result = null;
		BIJBinaryReader bIJBinaryReader = null;
		UserDataInfo instance = UserDataInfo.Instance;
		string text = Path.Combine(BIJDataBinaryReader.GetBasePath(), Constants.BACKUP_FILE);
		string text2 = Path.Combine(BIJDataBinaryReader.GetBasePath(), Constants.BACKUP_FILE_OLD);
		if (File.Exists(text) || File.Exists(text2))
		{
			instance.mBackupDataState = UserDataInfo.DATA_STATE.LOAD_ERROR;
			try
			{
				string pLAYER_FILE_HASH = Constants.PLAYER_FILE_HASH;
				byte[] buffer = new byte[0];
				if (File.Exists(text))
				{
					try
					{
						ServerCramVariableData.mBackupFileSize = new FileInfo(text).Length;
					}
					catch (Exception)
					{
						ServerCramVariableData.mBackupFileSize = 0L;
					}
					using (BIJBinaryReader bIJBinaryReader2 = new BIJZippedEncryptFileBinaryReader(text, pLAYER_FILE_HASH))
					{
						buffer = bIJBinaryReader2.ReadAll();
					}
				}
				else if (File.Exists(text2))
				{
					try
					{
						ServerCramVariableData.mBackupFileSize = new FileInfo(text2).Length;
					}
					catch (Exception)
					{
						ServerCramVariableData.mBackupFileSize = 0L;
					}
					using (BIJBinaryReader bIJBinaryReader3 = new BIJZippedOneFileBinaryReader(text2, pLAYER_FILE_HASH))
					{
						byte[] bytes = bIJBinaryReader3.ReadAll();
						buffer = CryptUtils.DecryptRJ128(bytes);
					}
				}
				bIJBinaryReader = new BIJMemoryBinaryReader(buffer);
				result = global::Player.Deserialize(bIJBinaryReader);
				instance.mBackupDataState = UserDataInfo.DATA_STATE.LOADED;
			}
			catch (Exception)
			{
				ServerCramVariableData.mBackupFileSize = 0L;
				return null;
			}
			finally
			{
				if (bIJBinaryReader != null)
				{
					try
					{
						bIJBinaryReader.Close();
						bIJBinaryReader.Dispose();
					}
					catch (Exception)
					{
					}
					bIJBinaryReader = null;
				}
			}
		}
		else
		{
			instance.mBackupDataState = UserDataInfo.DATA_STATE.NONE;
			ServerCramVariableData.mBackupFileSize = -1L;
		}
		return result;
	}

	public int GetSeriesCampaignMoves(Def.SERIES a_series)
	{
		if (mEventProfile == null)
		{
			return 0;
		}
		if (!Def.SeriesMapRouteData.ContainsKey(a_series))
		{
			return 0;
		}
		DateTime dateTime = DateTimeUtil.Now();
		SeriesCampaignSetting seriesCampaign = mEventProfile.GetSeriesCampaign(a_series, (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime, true));
		if (seriesCampaign != null)
		{
			return seriesCampaign.Moves;
		}
		return 0;
	}

	public int GetSeriesCampaignMovesEvent(int a_eventID)
	{
		if (mEventProfile == null)
		{
			return 0;
		}
		DateTime dateTime = DateTimeUtil.Now();
		SeriesCampaignSetting seriesCampaign = mEventProfile.GetSeriesCampaign(Def.SERIES.SM_EV, (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime, true), a_eventID);
		if (seriesCampaign != null)
		{
			return seriesCampaign.Moves;
		}
		return 0;
	}

	public int GetSeriesCampaignStartDate(Def.SERIES a_series)
	{
		if (mEventProfile == null)
		{
			return 0;
		}
		if (!Def.SeriesMapRouteData.ContainsKey(a_series))
		{
			return 0;
		}
		DateTime dateTime = DateTimeUtil.Now();
		SeriesCampaignSetting seriesCampaign = mEventProfile.GetSeriesCampaign(a_series, (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime, true));
		if (seriesCampaign != null)
		{
			int num = 0;
			try
			{
				return int.Parse(DateTimeUtil.UnixTimeStampToDateTime(seriesCampaign.StartTime, true).ToString("yyyyMMdd"));
			}
			catch
			{
				return 0;
			}
		}
		return 0;
	}

	public bool IsSeasonEventMultiHolding(bool ignore_oldevent = false)
	{
		bool result = false;
		if (mEventProfile == null)
		{
			return false;
		}
		int num = 0;
		DateTime dateTime = DateTimeUtil.Now();
		for (int i = 0; i < mEventProfile.SeasonEventList.Count; i++)
		{
			SeasonEventSettings seasonEventSettings = mEventProfile.SeasonEventList[i];
			if (!ignore_oldevent || seasonEventSettings.EventID < 200)
			{
				DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.StartTime, true);
				DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.EndTime, true);
				if (dateTime2 <= dateTime && dateTime < dateTime3)
				{
					num++;
				}
			}
		}
		if (num > 1)
		{
			result = true;
		}
		return result;
	}

	public EventState IsSeasonEventHolding()
	{
		if (mEventProfile == null)
		{
			return EventState.None;
		}
		EventState eventState = EventState.None;
		DateTime dateTime = DateTimeUtil.Now();
		for (int i = 0; i < mEventProfile.SeasonEventList.Count; i++)
		{
			SeasonEventSettings seasonEventSettings = mEventProfile.SeasonEventList[i];
			DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.StartTime, true);
			DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.EndTime, true);
			if (dateTime2 <= dateTime && dateTime < dateTime3)
			{
				eventState |= EventState.Season;
				break;
			}
		}
		for (int j = 0; j < mEventProfile.RevivalEventList.Count; j++)
		{
			RevivalEventSettings revivalEventSettings = mEventProfile.RevivalEventList[j];
			DateTime dateTime4 = DateTimeUtil.UnixTimeStampToDateTime(revivalEventSettings.StartTime, true);
			DateTime dateTime5 = DateTimeUtil.UnixTimeStampToDateTime(revivalEventSettings.EndTime, true);
			if (dateTime4 <= dateTime && dateTime < dateTime5)
			{
				eventState |= EventState.Revival;
			}
		}
		return eventState;
	}

	public bool IsSeasonEventExpired(int a_eventID)
	{
		if (mEventProfile == null)
		{
			return true;
		}
		DateTime dateTime = DateTimeUtil.Now();
		for (int i = 0; i < mEventProfile.SeasonEventList.Count; i++)
		{
			SeasonEventSettings seasonEventSettings = mEventProfile.SeasonEventList[i];
			if (seasonEventSettings.EventID == a_eventID)
			{
				DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.StartTime, true);
				DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.EndTime, true);
				if (dateTime2 <= dateTime && dateTime < dateTime3)
				{
					return false;
				}
				return true;
			}
		}
		for (int j = 0; j < mEventProfile.RevivalEventList.Count; j++)
		{
			RevivalEventSettings revivalEventSettings = mEventProfile.RevivalEventList[j];
			if (revivalEventSettings.EventID == a_eventID)
			{
				DateTime dateTime4 = DateTimeUtil.UnixTimeStampToDateTime(revivalEventSettings.StartTime, true);
				DateTime dateTime5 = DateTimeUtil.UnixTimeStampToDateTime(revivalEventSettings.EndTime, true);
				if (dateTime4 <= dateTime && dateTime < dateTime5)
				{
					return false;
				}
				return true;
			}
		}
		return true;
	}

	public bool IsSeasonEventLastDay()
	{
		bool result = false;
		if ((IsSeasonEventHolding() & EventState.Season) == 0)
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		for (int i = 0; i < mEventProfile.SeasonEventList.Count; i++)
		{
			SeasonEventSettings seasonEventSettings = mEventProfile.SeasonEventList[i];
			DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.StartTime, true);
			DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.EndTime, true);
			if (dateTime2 <= dateTime && dateTime < dateTime3)
			{
				long num = DateTimeUtil.BetweenDays0(dateTime, dateTime3);
				if (num < Constants.EVENT_LASTDAY_NUM)
				{
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public bool IsSeasonEventLastDayFromEventID(int a_eventID)
	{
		bool result = false;
		if ((IsSeasonEventHolding() & EventState.Season) == 0)
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		for (int i = 0; i < mEventProfile.SeasonEventList.Count; i++)
		{
			SeasonEventSettings seasonEventSettings = mEventProfile.SeasonEventList[i];
			if (a_eventID != seasonEventSettings.EventID)
			{
				continue;
			}
			DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.StartTime, true);
			DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.EndTime, true);
			if (dateTime2 <= dateTime && dateTime < dateTime3)
			{
				long num = DateTimeUtil.BetweenDays0(dateTime, dateTime3);
				if (num < Constants.EVENT_LASTDAY_NUM)
				{
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public bool IsSPDailyEventCheck()
	{
		bool result = false;
		if (EventProfile.SDailyEvent == null)
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(mEventProfile.SDailyEvent.StartTime, true);
		DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(mEventProfile.SDailyEvent.EndTime, true);
		int sPDailyCount = mPlayer.GetSPDailyCount(mEventProfile.SDailyEvent.SpecialEventID);
		int key = 0;
		for (int i = 0; i < mSpecialDailyEventData.Count; i++)
		{
			if (EventProfile.SDailyEvent.SpecialEventID.CompareTo(mSpecialDailyEventData[i].ID) == 0)
			{
				key = mSpecialDailyEventData[i].ID;
				break;
			}
		}
		if (sPDailyCount >= mSpecialDailyEventData[key].RewardList.Count * 24)
		{
			return false;
		}
		DateTime dateTime3 = DateTimeUtil.Now();
		if (dateTime <= dateTime3 && dateTime3 < dateTime2)
		{
			result = true;
		}
		return result;
	}

	public bool SPDailyEventAdd(AccessoryData a_data)
	{
		bool result = true;
		int dailyID = a_data.DailyID;
		PlayerSPDailyData value;
		if (mPlayer.Data.SpecialDaily.TryGetValue(dailyID, out value))
		{
			int num = 24;
			if (value.Num >= num)
			{
				if (num <= value.Num && value.Num < num * 2)
				{
					if (!mPlayer.IsAccessoryUnlock(mSpecialDailyEventData[dailyID].RewardList[0].RewardID))
					{
						AccessoryData accessoryData = GetAccessoryData(mSpecialDailyEventData[dailyID].RewardList[0].RewardID);
						if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
						{
							AddAccessoryConnect(mSpecialDailyEventData[dailyID].RewardList[0].RewardID);
						}
						else
						{
							mPlayer.AddAccessory(accessoryData);
						}
					}
				}
				else if (num * 2 <= value.Num && value.Num < num * 3)
				{
					if (!mPlayer.IsAccessoryUnlock(mSpecialDailyEventData[dailyID].RewardList[1].RewardID))
					{
						AccessoryData accessoryData2 = GetAccessoryData(mSpecialDailyEventData[dailyID].RewardList[1].RewardID);
						if (accessoryData2.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
						{
							AddAccessoryConnect(mSpecialDailyEventData[dailyID].RewardList[1].RewardID);
						}
						else
						{
							mPlayer.AddAccessory(accessoryData2);
						}
					}
				}
				else if (num * 3 <= value.Num && value.Num < num * 4)
				{
					if (!mPlayer.IsAccessoryUnlock(mSpecialDailyEventData[dailyID].RewardList[2].RewardID))
					{
						AccessoryData accessoryData3 = GetAccessoryData(mSpecialDailyEventData[dailyID].RewardList[2].RewardID);
						if (accessoryData3.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
						{
							AddAccessoryConnect(mSpecialDailyEventData[dailyID].RewardList[2].RewardID);
						}
						else
						{
							mPlayer.AddAccessory(accessoryData3);
						}
					}
				}
				else if (num * 4 <= value.Num && value.Num < num * 5)
				{
					if (!mPlayer.IsAccessoryUnlock(mSpecialDailyEventData[dailyID].RewardList[3].RewardID))
					{
						AccessoryData accessoryData4 = GetAccessoryData(mSpecialDailyEventData[dailyID].RewardList[3].RewardID);
						if (accessoryData4.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
						{
							AddAccessoryConnect(mSpecialDailyEventData[dailyID].RewardList[3].RewardID);
						}
						else
						{
							mPlayer.AddAccessory(accessoryData4);
						}
					}
				}
				else if (num * 5 <= value.Num)
				{
					if (!mPlayer.IsAccessoryUnlock(mSpecialDailyEventData[dailyID].RewardList[4].RewardID))
					{
						AccessoryData accessoryData5 = GetAccessoryData(mSpecialDailyEventData[dailyID].RewardList[4].RewardID);
						if (accessoryData5.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
						{
							AddAccessoryConnect(mSpecialDailyEventData[dailyID].RewardList[4].RewardID);
						}
						else
						{
							mPlayer.AddAccessory(accessoryData5);
						}
					}
					else
					{
						result = false;
					}
				}
			}
		}
		return result;
	}

	public List<DailyEventSettings> GetDailyEvents()
	{
		if (mEventProfile == null)
		{
			return new List<DailyEventSettings>();
		}
		if (!mTutorialManager.IsInitialTutorialCompleted())
		{
			return new List<DailyEventSettings>();
		}
		List<DailyEventSettings> list = new List<DailyEventSettings>();
		DateTime dateTime = DateTimeUtil.Now();
		for (int i = 0; i < mEventProfile.DailyEventList.Count; i++)
		{
			DailyEventSettings dailyEventSettings = mEventProfile.DailyEventList[i];
			if (dailyEventSettings.Series == (int)mPlayer.Data.CurrentSeries)
			{
				DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(dailyEventSettings.StartTime, true);
				DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(dailyEventSettings.EndTime, true);
				if (dateTime2 <= dateTime && dateTime < dateTime3)
				{
					list.Add(dailyEventSettings);
				}
			}
		}
		list.Sort();
		return list;
	}

	public AccessoryData.ACCESSORY_TYPE GuessDailyEventPresentBoxType(List<int> BoxID)
	{
		if (BoxID == null || BoxID.Count < 1)
		{
			return AccessoryData.ACCESSORY_TYPE.NONE;
		}
		AccessoryData.ACCESSORY_TYPE aCCESSORY_TYPE = AccessoryData.ACCESSORY_TYPE.NONE;
		for (int i = 0; i < BoxID.Count; i++)
		{
			try
			{
				int num = BoxID[i];
				if (mPlayer.IsAccessoryUnlock(num))
				{
					continue;
				}
				AccessoryData accessoryData = GetAccessoryData(mPlayer.Data.CurrentSeries, num);
				if (accessoryData == null)
				{
					continue;
				}
				AccessoryData.ACCESSORY_TYPE accessoryType = accessoryData.AccessoryType;
				switch (accessoryType)
				{
				case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
					aCCESSORY_TYPE = AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE;
					goto end_IL_013e;
				case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
					aCCESSORY_TYPE = accessoryType;
					break;
				case AccessoryData.ACCESSORY_TYPE.TROPHY:
					aCCESSORY_TYPE = accessoryType;
					break;
				case AccessoryData.ACCESSORY_TYPE.SP_DAILY:
					if (EventProfile.SDailyEvent != null)
					{
						if (accessoryData.DailyID == EventProfile.SDailyEvent.SpecialEventID)
						{
							int sPDailyCount = mPlayer.GetSPDailyCount(accessoryData.DailyID);
							if (sPDailyCount >= mSpecialDailyEventData[accessoryData.DailyID].RewardList.Count * 24)
							{
								return AccessoryData.ACCESSORY_TYPE.NONE;
							}
							aCCESSORY_TYPE = accessoryType;
						}
						break;
					}
					return AccessoryData.ACCESSORY_TYPE.NONE;
				default:
					if (aCCESSORY_TYPE != AccessoryData.ACCESSORY_TYPE.HEART_PIECE || aCCESSORY_TYPE != AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE)
					{
						aCCESSORY_TYPE = accessoryType;
					}
					break;
				}
			}
			catch (Exception)
			{
			}
			continue;
			end_IL_013e:
			break;
		}
		return aCCESSORY_TYPE;
	}

	public string GetDailyEventPresentBoxAnimeKey(AccessoryData.ACCESSORY_TYPE accType)
	{
		switch (accType)
		{
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			return "MAP_DAILYBOX_BLUE";
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			return "MAP_DAILYBOX_PINK";
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			return "MAP_DAILYBOX_GREEN";
		case AccessoryData.ACCESSORY_TYPE.SP_DAILY:
			return "MAP_DAILYBOX_PINK";
		default:
			return "MAP_DAILYBOX_GREEN";
		}
	}

	public bool GetEventSchedule(int a_eventID, out DateTime a_start, out DateTime a_end, out bool a_ishold)
	{
		a_start = DateTime.Now;
		a_end = DateTime.Now;
		a_ishold = false;
		bool result = false;
		for (int i = 0; i < mEventProfile.SeasonEventList.Count; i++)
		{
			SeasonEventSettings seasonEventSettings = mEventProfile.SeasonEventList[i];
			DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.StartTime, true);
			DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(seasonEventSettings.EndTime, true);
			if (seasonEventSettings.EventID == a_eventID)
			{
				a_start = dateTime;
				a_end = dateTime2;
				result = true;
				DateTime dateTime3 = DateTimeUtil.Now();
				if (dateTime <= dateTime3 && dateTime3 < dateTime2)
				{
					a_ishold = true;
				}
				break;
			}
		}
		return result;
	}

	public bool IsRemainTimeButtonVisible()
	{
		int remainDailyEventTime = GetRemainDailyEventTime();
		int remainFriendHelpTime = GetRemainFriendHelpTime();
		return remainDailyEventTime > 0 || remainFriendHelpTime > 0;
	}

	public int GetRemainDailyEventTime()
	{
		List<DailyEventSettings> dailyEvents = GetDailyEvents();
		if (dailyEvents.Count < 1)
		{
			return -1;
		}
		long endTime = dailyEvents[0].EndTime;
		for (int i = 1; i < dailyEvents.Count; i++)
		{
			if (dailyEvents[i].EndTime < endTime)
			{
				endTime = dailyEvents[i].EndTime;
			}
		}
		DateTime tdt = DateTimeUtil.UnixTimeStampToDateTime(endTime, true);
		long num = DateTimeUtil.BetweenSeconds(DateTime.Now, tdt);
		return (int)num;
	}

	public int GetRemainFriendHelpTime()
	{
		if (mGameProfile != null && mGameProfile.FriendHelp == 1)
		{
			DateTime lastSentFriendHelpTime = mPlayer.Data.LastSentFriendHelpTime;
			long num = DateTimeUtil.BetweenSeconds(DateTime.Now, lastSentFriendHelpTime);
			return (int)num;
		}
		return -1;
	}

	public void AddEventPoint(int a_eventID, int a_addpoint, bool a_fromZero = false)
	{
		if (a_addpoint <= 0)
		{
			return;
		}
		SMEventPageData eventPageData = GetEventPageData(a_eventID);
		PlayerEventData data;
		mPlayer.Data.GetMapData(Def.SERIES.SM_EV, a_eventID, out data);
		Def.EVENT_TYPE eventType = eventPageData.EventSetting.EventType;
		if (eventType != Def.EVENT_TYPE.SM_LABYRINTH || data == null)
		{
			return;
		}
		PlayerEventLabyrinthData labyrinthData = data.LabyrinthData;
		int num = labyrinthData.JewelAmount;
		if (a_fromZero)
		{
			num = 0;
		}
		labyrinthData.JewelAmount += a_addpoint;
		GlobalLabyrinthEventData labyrinthData2 = GlobalVariables.Instance.GetLabyrinthData(a_eventID);
		List<int> list = new List<int>(eventPageData.EventRewardList.Keys);
		list.Sort();
		for (int i = 0; i < list.Count; i++)
		{
			int key = list[i];
			SMEventRewardSetting sMEventRewardSetting = eventPageData.EventRewardList[key];
			if (num >= sMEventRewardSetting.TotalNum || sMEventRewardSetting.TotalNum > labyrinthData.JewelAmount)
			{
				continue;
			}
			if (sMEventRewardSetting.IsGCItem == 0)
			{
				AccessoryData accessoryData = GetAccessoryData(sMEventRewardSetting.RewardID);
				if (accessoryData == null)
				{
					BIJLog.E("accessoryData is null.:" + sMEventRewardSetting.RewardID);
				}
				else if (!mPlayer.IsAccessoryUnlock(accessoryData.Index))
				{
					if (labyrinthData2.RewardAccessoryDataList == null)
					{
						labyrinthData2.RewardAccessoryDataList = new List<AccessoryData>();
					}
					labyrinthData2.RewardAccessoryDataList.Add(accessoryData);
					mPlayer.AddAccessory(accessoryData, true, false, 19);
					if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
					{
						mPlayer.AddAccessoryConnect(accessoryData.Index);
					}
					else if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
					{
						mPlayer.AddAdvAccessoryConnect(accessoryData.Index);
					}
					if (mServerCramVariableData.Labyrinth.ContainsKey(a_eventID))
					{
						ServerCramVariableData.LabyrinthInfo labyrinthInfo = mServerCramVariableData.Labyrinth[a_eventID];
						ServerCram.EventLabyrinthGetReward(sMEventRewardSetting.RewardID, a_eventID, labyrinthInfo.CourseClearCount.ContainsKey(0) ? labyrinthInfo.CourseClearCount[0] : 0, labyrinthInfo.CourseClearCount.ContainsKey(1) ? labyrinthInfo.CourseClearCount[1] : 0, labyrinthInfo.CourseClearCount.ContainsKey(2) ? labyrinthInfo.CourseClearCount[2] : 0, labyrinthInfo.CourseClearCount.ContainsKey(3) ? labyrinthInfo.CourseClearCount[3] : 0, labyrinthInfo.CourseClearCount.ContainsKey(4) ? labyrinthInfo.CourseClearCount[4] : 0, labyrinthInfo.CourseClearCount.ContainsKey(5) ? labyrinthInfo.CourseClearCount[5] : 0, labyrinthInfo.CourseClearCount.ContainsKey(6) ? labyrinthInfo.CourseClearCount[6] : 0, labyrinthInfo.CourseClearCount.ContainsKey(7) ? labyrinthInfo.CourseClearCount[7] : 0, labyrinthInfo.CourseClearCount.ContainsKey(8) ? labyrinthInfo.CourseClearCount[8] : 0, labyrinthInfo.CourseClearCount.ContainsKey(9) ? labyrinthInfo.CourseClearCount[9] : 0, labyrinthInfo.CourseClearCount.ContainsKey(10) ? labyrinthInfo.CourseClearCount[10] : 0, labyrinthInfo.CourseClearCount.ContainsKey(11) ? labyrinthInfo.CourseClearCount[11] : 0, labyrinthInfo.CourseClearCount.ContainsKey(12) ? labyrinthInfo.CourseClearCount[12] : 0, labyrinthInfo.TotalStageClearCount, labyrinthInfo.DoubleUpChanceCount, labyrinthData.JewelAmount);
					}
				}
			}
			else
			{
				AdvRewardListData rewardListData = GetRewardListData(sMEventRewardSetting.RewardID);
				if (rewardListData == null)
				{
					BIJLog.E("rewardListData is null.:" + sMEventRewardSetting.RewardID);
				}
				else
				{
					BIJLog.E("GC Item is Not Implemented!!!:" + sMEventRewardSetting.RewardID);
				}
			}
		}
		GlobalVariables.Instance.SetLabyrinthData(a_eventID, labyrinthData2);
	}

	public GiftUIMode CheckShowGiftDialog(GiftUIMode checkMode = GiftUIMode.NONE)
	{
		if (!IsLoaded)
		{
			return GiftUIMode.NONE;
		}
		List<GiftItem> modifiedGifts = mPlayer.GetModifiedGifts();
		ICollection<GiftUIMode> collection = DetectShowGiftDialog(ref modifiedGifts);
		if (collection == null || collection.Count < 1)
		{
			return GiftUIMode.NONE;
		}
		if (checkMode == GiftUIMode.NONE)
		{
			if (collection.Contains(GiftUIMode.RESCUE))
			{
				return GiftUIMode.RESCUE;
			}
			if (collection.Contains(GiftUIMode.SEND))
			{
				return GiftUIMode.SEND;
			}
			if (collection.Contains(GiftUIMode.RECEIVE))
			{
				return GiftUIMode.RECEIVE;
			}
			return GiftUIMode.NONE;
		}
		if (collection.Contains(checkMode))
		{
			return checkMode;
		}
		return GiftUIMode.NONE;
	}

	private ICollection<GiftUIMode> DetectShowGiftDialog(ref List<GiftItem> modifiedGifts)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		HashSet<GiftUIMode> hashSet = new HashSet<GiftUIMode>();
		if (modifiedGifts.Count > 0)
		{
			foreach (GiftItem modifiedGift in modifiedGifts)
			{
				if (modifiedGift.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ || modifiedGift.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_RES || modifiedGift.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ || modifiedGift.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_RES)
				{
					continue;
				}
				hashSet.Add(GiftUIMode.RECEIVE);
				break;
			}
		}
		List<MyFriend> heartSendableFriends = GetHeartSendableFriends();
		if (heartSendableFriends != null && heartSendableFriends.Count > 0)
		{
			if (mLastCheckHeartSendableFriends == null || mLastCheckHeartSendableFriends.Count < 1)
			{
				hashSet.Add(GiftUIMode.SEND);
			}
			else
			{
				foreach (MyFriend item in heartSendableFriends)
				{
					if (instance.mPlayer.GetNewFriendNotifyStatus(item.Data.UUID) == global::Player.NOTIFICATION_STATUS.NOTIFY)
					{
						hashSet.Add(GiftUIMode.SEND);
						break;
					}
				}
			}
			mLastCheckHeartSendableFriends = heartSendableFriends;
		}
		if (modifiedGifts.Count > 0)
		{
			foreach (GiftItem modifiedGift2 in modifiedGifts)
			{
				if (modifiedGift2.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ || modifiedGift2.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ)
				{
					hashSet.Add(GiftUIMode.RESCUE);
					instance.FirstGiftTab = GiftUIMode.RESCUE;
					break;
				}
			}
		}
		return hashSet;
	}

	public List<MyFriend> GetHeartSendableFriends()
	{
		List<MyFriend> list = new List<MyFriend>();
		if (!mTutorialManager.IsInitialTutorialCompleted())
		{
			return list;
		}
		if (mPlayer.Friends.Count > 0)
		{
			DateTime now = DateTime.Now;
			foreach (KeyValuePair<int, MyFriend> friend in mPlayer.Friends)
			{
				MyFriend value = friend.Value;
				if (value == null)
				{
					continue;
				}
				int uUID = value.Data.UUID;
				if (!mPlayer.SendInfo.ContainsKey(uUID))
				{
					list.Add(value);
					continue;
				}
				PlayerSendData playerSendData = mPlayer.SendInfo[uUID];
				if (DateTimeUtil.BetweenDays0(playerSendData.LastHeartSendTime, now) >= Constants.SEND_HEART_FREQ)
				{
					list.Add(value);
				}
			}
		}
		return list;
	}

	public void ClearModifiedGifts(GiftUIMode mode = GiftUIMode.NONE)
	{
		List<GiftItem> modifiedGifts = mPlayer.GetModifiedGifts();
		switch (mode)
		{
		case GiftUIMode.NONE:
			mPlayer.ClearModifiedGifts();
			return;
		case GiftUIMode.SEND:
			return;
		}
		foreach (GiftItem item in modifiedGifts)
		{
			if (item.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_RES)
			{
				continue;
			}
			if (item.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ || item.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ)
			{
				if (mode == GiftUIMode.RESCUE)
				{
					mPlayer.RemoveModifiedGift(item);
				}
			}
			else if (mode == GiftUIMode.RECEIVE)
			{
				mPlayer.RemoveModifiedGift(item);
			}
		}
	}

	public bool CheckCorrectReceiveGiftItem(GiftItem gift)
	{
		bool flag = true;
		bool flag2 = false;
		if (gift.Data.ItemCategory == 1)
		{
			int itemKind = gift.Data.ItemKind;
			int itemID = gift.Data.ItemID;
			int quantity = gift.Data.Quantity;
			switch ((Def.CONSUME_ID)itemKind)
			{
			case Def.CONSUME_ID.HEART:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.HEART_EX:
				if (itemID != 1)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.HEART_PIECE:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.GROWUP:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.GROWUP_PIECE:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.MAIN_CURRENCY:
			case Def.CONSUME_ID.MAIN_CURRENCY_EX:
				if (itemID != 1)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.ROADBLOCK_TICKET:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.SUB_CURRENCY:
			case Def.CONSUME_ID.SUB_CURRENCY_EX:
				flag = false;
				break;
			case Def.CONSUME_ID.GOODJOB:
				flag = false;
				break;
			case Def.CONSUME_ID.TROPHY:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.TOTAL_TROPHY:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_R:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_S:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_SS:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_STARS:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_GF00:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			case Def.CONSUME_ID.OLDEVENT_KEY:
				if (itemID == 0)
				{
					flag = false;
				}
				break;
			default:
				flag = false;
				break;
			}
			if (!flag)
			{
				flag2 = true;
			}
		}
		else
		{
			switch ((Def.ITEM_CATEGORY)gift.Data.ItemCategory)
			{
			case Def.ITEM_CATEGORY.ACCESSORY:
			{
				int itemKind2 = gift.Data.ItemKind;
				if (mAccessoryData.Count <= itemKind2)
				{
					flag = false;
					flag2 = true;
					break;
				}
				AccessoryData accessoryData = GetAccessoryData(itemKind2);
				if (accessoryData == null)
				{
					flag = false;
					flag2 = true;
				}
				else if (mPlayer.IsAccessoryUnlock(accessoryData.Index))
				{
					flag = false;
					flag2 = true;
				}
				else if (!IsEnableReceiveAccessoryType(accessoryData))
				{
					flag = false;
					flag2 = true;
				}
				break;
			}
			case Def.ITEM_CATEGORY.BOOSTER:
				flag = false;
				flag2 = true;
				break;
			case Def.ITEM_CATEGORY.FREE_BOOSTER:
			{
				int itemKind3 = gift.Data.ItemKind;
				if (mBoosterData.Count <= itemKind3)
				{
					flag = false;
					flag2 = true;
				}
				break;
			}
			case Def.ITEM_CATEGORY.COMPANION:
			{
				int itemKind4 = gift.Data.ItemKind;
				if (mCompanionData.Count <= itemKind4 || mPlayer.IsCompanionUnlock(itemKind4))
				{
					flag = false;
					flag2 = true;
				}
				break;
			}
			case Def.ITEM_CATEGORY.WALLPAPER:
			{
				AccessoryData accessoryWallPaperData = GetAccessoryWallPaperData(gift.Data.ItemKind);
				if (accessoryWallPaperData == null)
				{
					flag = false;
					flag2 = true;
				}
				else if (mPlayer.IsAccessoryUnlock(accessoryWallPaperData.Index))
				{
					flag = false;
					flag2 = true;
				}
				break;
			}
			case Def.ITEM_CATEGORY.WALLPAPER_PIECE:
			{
				AccessoryData accessoryWallPaperPieceData = GetAccessoryWallPaperPieceData(gift.Data.ItemKind, gift.Data.ItemID);
				if (accessoryWallPaperPieceData == null)
				{
					flag = false;
					flag2 = true;
				}
				else if (mPlayer.IsAccessoryUnlock(accessoryWallPaperPieceData.Index))
				{
					flag = false;
					flag2 = true;
				}
				break;
			}
			case Def.ITEM_CATEGORY.STAGE_OPEN:
			case Def.ITEM_CATEGORY.STAGE_OPEN_BUNDLE:
				flag = (Enum.IsDefined(typeof(Def.SERIES), gift.Data.ItemKind) ? true : false);
				break;
			case Def.ITEM_CATEGORY.PRESENT_HEART:
				flag = true;
				break;
			case Def.ITEM_CATEGORY.RELEASE_TIMECHEAT:
				flag = false;
				flag2 = true;
				break;
			case Def.ITEM_CATEGORY.BINGO_TICKET:
				flag = false;
				if (mEventPageData.ContainsKey(gift.Data.ItemKind) && mEventPageData[gift.Data.ItemKind].EventSetting.EventType == Def.EVENT_TYPE.SM_BINGO)
				{
					flag = true;
				}
				break;
			case Def.ITEM_CATEGORY.LABYRINTH_JEWEL:
				flag = false;
				if (mEventPageData.ContainsKey(gift.Data.ItemKind) && mEventPageData[gift.Data.ItemKind].EventSetting.EventType == Def.EVENT_TYPE.SM_LABYRINTH)
				{
					flag = true;
				}
				break;
			case Def.ITEM_CATEGORY.MESSAGE_CARD:
				flag = true;
				break;
			case Def.ITEM_CATEGORY.EVENT:
			case Def.ITEM_CATEGORY.STORY_DEMO:
			case Def.ITEM_CATEGORY.SKIN:
			case Def.ITEM_CATEGORY.TUTORIAL:
			case Def.ITEM_CATEGORY.PRESENT_BOOSTER:
			case Def.ITEM_CATEGORY.PRESENT_GOODJOB:
				flag = false;
				flag2 = true;
				break;
			default:
				flag = true;
				break;
			}
		}
		if (flag2)
		{
			mPlayer.RemoveGift(gift);
		}
		return flag;
	}

	public bool CheckCorrectRescueGiftItem(GiftItem gift)
	{
		bool flag = true;
		switch ((Def.ITEM_CATEGORY)gift.Data.ItemCategory)
		{
		case Def.ITEM_CATEGORY.FRIEND_REQ:
			return true;
		case Def.ITEM_CATEGORY.FRIEND_RES:
			return true;
		case Def.ITEM_CATEGORY.STAGEHELP_REQ:
			return false;
		case Def.ITEM_CATEGORY.STAGEHELP_RES:
			return false;
		case Def.ITEM_CATEGORY.ROADBLOCK_REQ:
			return true;
		case Def.ITEM_CATEGORY.ROADBLOCK_RES:
			return true;
		default:
			return false;
		}
	}

	public static bool CheckEULADialogShow()
	{
		bool result = false;
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (instance.mGameProfile.ToSVer > instance.mPlayer.Data.LastTermServiceID)
		{
			result = true;
		}
		else if (instance.mGameProfile.IsToSCheckTime)
		{
			int toSCheckTime = instance.mGameProfile.ToSCheckTime;
			if ((DateTime.Now - instance.mPlayer.Data.LastTermServiceDisplayTime).Hours > toSCheckTime)
			{
				result = true;
			}
		}
		if (instance.mGameProfile.PrivacyVer > instance.mPlayer.Data.LastPrivacyPolicyID)
		{
			result = true;
		}
		else if (instance.mGameProfile.IsPrivacyCheckTime)
		{
			int privacyCheckTime = instance.mGameProfile.PrivacyCheckTime;
			if ((DateTime.Now - instance.mPlayer.Data.LastPrivacyPolicyDisplayTime).Hours > privacyCheckTime)
			{
				result = true;
			}
		}
		return result;
	}

	public static void SetShowEULAVersion()
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		instance.mPlayer.Data.LastTermServiceID = instance.mGameProfile.ToSVer;
		instance.mPlayer.Data.LastTermServiceDisplayTime = DateTime.Now;
		instance.mPlayer.Data.LastPrivacyPolicyID = instance.mGameProfile.PrivacyVer;
		instance.mPlayer.Data.LastPrivacyPolicyDisplayTime = DateTime.Now;
		instance.Save();
	}

	public static bool IsSNSLogined(BIJSNS _sns)
	{
		if (_sns == null)
		{
			return false;
		}
		switch (_sns.Kind)
		{
		case SNS.Facebook:
			if (string.IsNullOrEmpty(SingletonMonoBehaviour<GameMain>.Instance.mOptions.Fbid))
			{
				return false;
			}
			break;
		case SNS.GooglePlus:
			if (string.IsNullOrEmpty(SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gpid))
			{
				return false;
			}
			break;
		case SNS.GameCenter:
			if (string.IsNullOrEmpty(SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gcid))
			{
				return false;
			}
			break;
		case SNS.Twitter:
			if (string.IsNullOrEmpty(SingletonMonoBehaviour<GameMain>.Instance.mOptions.Twid))
			{
				return false;
			}
			break;
		case SNS.GooglePlayGameServices:
			if (string.IsNullOrEmpty(SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gpgsid))
			{
				return false;
			}
			break;
		default:
			return false;
		}
		if (!_sns.IsOpEnabled(2))
		{
			return false;
		}
		return _sns.IsLogined();
	}

	public static bool IsSNSNeedLogin(BIJSNS _sns)
	{
		if (_sns == null)
		{
			return false;
		}
		if (!_sns.IsOpEnabled(2))
		{
			return false;
		}
		if (IsSNSLogined(_sns))
		{
			return false;
		}
		return true;
	}

	public static bool UpdateOptions(BIJSNS _sns, string id, string name)
	{
		if (_sns == null)
		{
			return false;
		}
		bool result = true;
		switch (_sns.Kind)
		{
		case SNS.Facebook:
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.Fbid = id;
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.FbName = name;
			break;
		case SNS.GooglePlus:
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gpid = id;
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.GpName = name;
			break;
		case SNS.GameCenter:
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gcid = id;
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.GcName = name;
			break;
		case SNS.Twitter:
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.Twid = id;
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.TwName = name;
			break;
		case SNS.GooglePlayGameServices:
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gpgsid = id;
			SingletonMonoBehaviour<GameMain>.Instance.mOptions.GpgsName = name;
			break;
		default:
			result = false;
			break;
		}
		return result;
	}

	public static bool TryGetSNSUserInfo(BIJSNS _sns, out string id, out string name)
	{
		id = null;
		name = null;
		if (_sns == null)
		{
			return false;
		}
		bool result = true;
		switch (_sns.Kind)
		{
		case SNS.Facebook:
			id = SingletonMonoBehaviour<GameMain>.Instance.mOptions.Fbid;
			name = SingletonMonoBehaviour<GameMain>.Instance.mOptions.FbName;
			break;
		case SNS.GooglePlus:
			id = SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gpid;
			name = SingletonMonoBehaviour<GameMain>.Instance.mOptions.GpName;
			break;
		case SNS.GameCenter:
			id = SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gcid;
			name = SingletonMonoBehaviour<GameMain>.Instance.mOptions.GcName;
			break;
		case SNS.Twitter:
			id = SingletonMonoBehaviour<GameMain>.Instance.mOptions.Twid;
			name = SingletonMonoBehaviour<GameMain>.Instance.mOptions.TwName;
			break;
		case SNS.GooglePlayGameServices:
			id = SingletonMonoBehaviour<GameMain>.Instance.mOptions.Gpgsid;
			name = SingletonMonoBehaviour<GameMain>.Instance.mOptions.GpgsName;
			break;
		default:
			result = false;
			break;
		}
		return result;
	}

	public static List<string> GetInviteExcludeIds(BIJSNS _sns)
	{
		if (_sns.Kind != SNS.Facebook)
		{
			return null;
		}
		HashSet<string> hashSet = new HashSet<string>();
		if (SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Friends != null && SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Friends.Count > 0)
		{
			foreach (KeyValuePair<int, MyFriend> friend in SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Friends)
			{
				if (friend.Key != 0)
				{
					MyFriend value = friend.Value;
					if (value != null && !string.IsNullOrEmpty(value.Data.Fbid))
					{
						hashSet.Add(value.Data.Fbid);
					}
				}
			}
		}
		if (SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastInviteTime != null && SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastInviteTime.Count > 0)
		{
			foreach (KeyValuePair<string, DateTime> item in SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastInviteTime)
			{
				if (!string.IsNullOrEmpty(item.Key))
				{
					DateTime value2 = item.Value;
					long num = DateTimeUtil.BetweenDays0(value2, DateTime.Now);
					long num2 = Constants.INVITE_COOLDOWN_INTERVAL;
					if (num2 > 0 && num < num2)
					{
						hashSet.Add(item.Key);
					}
				}
			}
		}
		return new List<string>(hashSet);
	}

	public static int ProcessInviteResult(BIJSNS _sns, IBIJSNSPostData _postdata)
	{
		return 1;
	}

	public static bool IsSNSFunctionEnabled(SNS_FLAG _flag)
	{
		return IsSNSFunctionEnabled(_flag, SNS.None);
	}

	public static bool IsSNSFunctionEnabled(SNS_FLAG _flag, SNS _sns)
	{
		if (_sns == SNS.None)
		{
			return GameProfile_CheckSNSFunctions(_flag);
		}
		return GameProfile_CheckSNSFlags(_flag, _sns);
	}

	private static bool GameProfile_CheckSNSFunctions(SNS_FLAG _flag)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (instance.mGameProfile != null && instance.mGameProfile.SNSFunctions != null)
		{
			string key = _flag.ToString();
			bool value = false;
			if (instance.mGameProfile.SNSFunctions.TryGetValue(key, out value))
			{
				if (value)
				{
				}
				return value;
			}
		}
		return false;
	}

	private static bool GameProfile_CheckSNSFlags(SNS_FLAG _flag, SNS _sns)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (instance.mGameProfile != null && instance.mGameProfile.SNSFlags != null)
		{
			string key = _sns.ToString();
			int value = 0;
			if (instance.mGameProfile.SNSFlags.TryGetValue(key, out value))
			{
				bool flag = ((uint)value & (uint)_flag) != 0;
				if (flag)
				{
				}
				return flag;
			}
		}
		return false;
	}

	public static bool IsFacebookMiniIconEnable()
	{
		SNS_FLAG flag = (SNS_FLAG)65487;
		if (IsSNSFunctionEnabled(flag, SNS.Facebook))
		{
			return true;
		}
		return false;
	}

	public static bool IsFacebookIconEnable()
	{
		if (IsSNSFunctionEnabled(SNS_FLAG.MyIcon) && IsSNSFunctionEnabled(SNS_FLAG.MyIcon, SNS.Facebook))
		{
			return true;
		}
		return false;
	}

	public static bool IsFacebookEnable(params SNS_FLAG[] flag)
	{
		if (flag != null)
		{
			foreach (SNS_FLAG flag2 in flag)
			{
				if (false || !IsSNSFunctionEnabled(flag2, SNS.Facebook))
				{
					return true;
				}
			}
			return true;
		}
		return false;
	}

	public static bool UnlockAchievement(BIJSNS _sns, string id)
	{
		if (false || !IsSNSFunctionEnabled(SNS_FLAG.Achievement) || !IsSNSFunctionEnabled(SNS_FLAG.Achievement, _sns.Kind))
		{
			return false;
		}
		if (!IsLoaded)
		{
			return false;
		}
		if (_sns == null || !_sns.IsEnabled() || !_sns.IsOpEnabled(10))
		{
			return false;
		}
		_sns.UnlockAchievement(id, null, null);
		return true;
	}

	public static bool IncrementAchievement(BIJSNS _sns, string id, int step)
	{
		if (false || !IsSNSFunctionEnabled(SNS_FLAG.Achievement) || !IsSNSFunctionEnabled(SNS_FLAG.Achievement, _sns.Kind))
		{
			return false;
		}
		if (!IsLoaded)
		{
			return false;
		}
		if (_sns == null || !_sns.IsEnabled() || !_sns.IsOpEnabled(11))
		{
			return false;
		}
		_sns.IncrementAchievement(id, step, null, null);
		return true;
	}

	public static bool ShowAchievements(BIJSNS _sns, BIJSNS.Handler handler, object userdata)
	{
		if (false || !IsSNSFunctionEnabled(SNS_FLAG.Achievement) || !IsSNSFunctionEnabled(SNS_FLAG.Achievement, _sns.Kind))
		{
			return false;
		}
		if (!IsLoaded)
		{
			return false;
		}
		if (_sns == null || !_sns.IsEnabled() || !_sns.IsOpEnabled(12))
		{
			return false;
		}
		_sns.ShowAchievements(handler, userdata);
		return true;
	}

	public void AchievementStageClear(int stageNo)
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (false || !IsSNSFunctionEnabled(SNS_FLAG.Achievement) || !IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			return;
		}
		if (mTutorialManager.IsInitialTutorialCompleted())
		{
		}
		Dictionary<int, string> aCHIEVEMENT_LEVELS_EN = ACHIEVEMENT_LEVELS_EN;
		foreach (KeyValuePair<int, string> item in aCHIEVEMENT_LEVELS_EN)
		{
			int key = item.Key;
			string value = item.Value;
			if (stageNo == key)
			{
				UnlockAchievement(bIJSNS, value);
			}
		}
		int totalStars = mPlayer.TotalStars;
		aCHIEVEMENT_LEVELS_EN = ACHIEVEMENT_STARS_EN;
		foreach (KeyValuePair<int, string> item2 in aCHIEVEMENT_LEVELS_EN)
		{
			int key2 = item2.Key;
			string value2 = item2.Value;
			if (totalStars >= key2)
			{
				UnlockAchievement(bIJSNS, value2);
			}
		}
		AchievementHighScore();
	}

	public void AchievementHighScore()
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (0 == 0 && IsSNSFunctionEnabled(SNS_FLAG.Achievement) && IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			long maxScore = mPlayer.Data.MaxScore;
			if (maxScore >= 10000)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.SCORE_BEGINNER_10000));
			}
			if (maxScore >= 25000)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.SCORE_HUNTER_25000));
			}
			if (maxScore >= 50000)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.SCORE_CHASER_50000));
			}
			if (maxScore >= 100000)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.SCORE_MASTER_100000));
			}
		}
	}

	public void AchievementStarCollection()
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (false || !IsSNSFunctionEnabled(SNS_FLAG.Achievement) || !IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			return;
		}
		bool flag = true;
		Dictionary<int, List<int>> _checkmap = new Dictionary<int, List<int>>();
		Dictionary<int, string> aCHIEVEMENT_COLLECTIONS_EN = ACHIEVEMENT_COLLECTIONS_EN;
		foreach (KeyValuePair<int, string> item in aCHIEVEMENT_COLLECTIONS_EN)
		{
			int key = item.Key;
			string value = item.Value;
			if (CheckCollectionAllDone(ref _checkmap, key))
			{
				UnlockAchievement(bIJSNS, value);
			}
		}
	}

	private bool CheckCollectionAllDone(ref Dictionary<int, List<int>> _checkmap, int mapNo)
	{
		List<int> value = null;
		if (_checkmap.TryGetValue(mapNo, out value) && value != null && value.Count > 0)
		{
			foreach (int item in value)
			{
				if (mPlayer.IsAccessoryUnlock(item))
				{
					continue;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	public void AchievementCombo(int comboNum)
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (0 == 0 && IsSNSFunctionEnabled(SNS_FLAG.Achievement) && IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			if (comboNum >= 10)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.COMBO_BEGINNER_10));
			}
			if (comboNum >= 20)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.COMBO_HUNTER_20));
			}
			if (comboNum >= 30)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.COMBO_CHASER_30));
			}
			if (comboNum >= 50)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.COMBO_MASTER_50));
			}
		}
	}

	public void AchievementFriend()
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (0 == 0 && IsSNSFunctionEnabled(SNS_FLAG.Achievement) && IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			int count = mPlayer.Friends.Count;
			if (count >= 1)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.FRIEND_FIRST_1));
			}
			if (count >= 10)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.FRIEND_FRIENDLY_10));
			}
			if (count >= 20)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.FRIEND_CONNECT_20));
			}
			if (count >= 30)
			{
				UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.FRIEND_GENIUS_30));
			}
		}
	}

	public void AchievementFriendHeart(int fuuid)
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (0 == 0 && IsSNSFunctionEnabled(SNS_FLAG.Achievement) && IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.SENDHEART_MESSAGE_1));
		}
	}

	public void AchievementFriendRoadblock(int fuuid)
	{
	}

	public void AchievementFriendPost(bool isScreenShot)
	{
	}

	public void AchievementFriendInvite()
	{
	}

	public void AchievementPartnerLvlMax()
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (false || !IsSNSFunctionEnabled(SNS_FLAG.Achievement) || !IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			return;
		}
		bool flag = false;
		foreach (KeyValuePair<int, byte> companion in mPlayer.Data.Companions)
		{
			if (companion.Value != 0)
			{
				int companionLevel = mPlayer.GetCompanionLevel(companion.Key);
				if (companionLevel >= Def.CompanionXPTable.Length - 1)
				{
					flag = true;
					break;
				}
			}
		}
		if (flag)
		{
			UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.MAKEUP_MASTER));
		}
	}

	public void AchievementPartnerUnlock()
	{
		BIJSNS bIJSNS = SocialManager.Instance[SNS.GooglePlayGameServices];
		if (false || !IsSNSFunctionEnabled(SNS_FLAG.Achievement) || !IsSNSFunctionEnabled(SNS_FLAG.Achievement, bIJSNS.Kind))
		{
			return;
		}
		bool flag = true;
		for (int i = 0; i < mFighter5_IDList.Length; i++)
		{
			if (!mPlayer.IsCompanionUnlock(mFighter5_IDList[i]))
			{
				flag = false;
				break;
			}
		}
		if (flag)
		{
			UnlockAchievement(bIJSNS, Def.GPAchievements.GetKey(Def.GPAchievements.KIND.MARSHAL_FIGHTER5));
		}
	}

	public void AchievementAllCheck()
	{
		AchievementPartnerUnlock();
		AchievementPartnerLvlMax();
		AchievementFriend();
		AchievementCombo((int)mPlayer.Data.MaxCombo);
		AchievementHighScore();
	}

	public void InitFriendsRanking(Def.SERIES series, int stageNo, ref List<StageRankingData> rankingData, Player pPlayer)
	{
		mLastCheckRankingSeries = series;
		mLastCheckRankingStageNo = stageNo;
		mLastCheckRankingStageData = new List<StageRankingData>();
		mLastCheckPlayerStatScore = 0;
		if (rankingData != null && rankingData.Count > 0)
		{
			foreach (StageRankingData rankingDatum in rankingData)
			{
				mLastCheckRankingStageData.Add(rankingDatum.Clone());
				if (rankingDatum.uuid == mPlayer.UUID)
				{
					mLastCheckPlayerStatScore = rankingDatum.clr_score;
				}
			}
		}
		mLastCheckRankUpStageScore = false;
		mLastCheckRankUpFriendsStar = false;
		mLastCheckRankUpFriendsLevel = false;
		mLastCheckRankUpFriendsTrophy = false;
	}

	public List<MyFriend> AllFriends()
	{
		IDictionary<int, MyFriend> friends = mPlayer.Friends;
		MyFriend value = new MyFriend();
		if (!mPlayer.Friends.ContainsKey(mPlayer.Data.UUID))
		{
			friends.Add(mPlayer.Data.UUID, value);
			mMyCheckAdd = true;
		}
		int num = 0;
		List<MyFriend> list = new List<MyFriend>();
		foreach (KeyValuePair<int, MyFriend> item in friends)
		{
			MyFriend value2 = item.Value;
			MyFriend value3 = item.Value;
			num++;
			if (num != mPlayer.Friends.Count)
			{
				if (value2 == null)
				{
					continue;
				}
				list.Add(value2);
			}
			if (num == mPlayer.Friends.Count)
			{
				value3.Data.UUID = mPlayer.UUID;
				value3.Data.IconID = mPlayer.Data.PlayerIcon;
				value3.Data.Name = mPlayer.Data.NickName;
				value3.Data.TotalStars = mPlayer.TotalStars;
				value3.Data.TotalScore = mPlayer.TotalScoreToS;
				value3.Data.TotalTrophy = mPlayer.TotalTrophy;
				int num2 = mPlayer.Data.PlayerIcon;
				if (num2 >= 10000)
				{
					num2 -= 10000;
				}
				value3.Data.IconLevel = mPlayer.GetCompanionLevel(num2);
				value3.Data.Level = mPlayer.LevelToS;
				value3.Icon = mPlayer.Icon;
				value3.Data.PlayStage = mPlayer.PlayStage;
				list.Add(value3);
				num++;
			}
		}
		if (mMyCheckAdd)
		{
			friends.Remove(mPlayer.Data.UUID);
		}
		return list;
	}

	public bool CheckRankUpStageScore(Def.SERIES series, int stageNo, int score, out StageRankingData rankingData, out int newPlayerRank, out int oldPlayerRank)
	{
		rankingData = null;
		newPlayerRank = -1;
		oldPlayerRank = -1;
		if (mLastCheckRankUpStageScore)
		{
			return false;
		}
		if (series != mLastCheckRankingSeries && stageNo != mLastCheckRankingStageNo)
		{
			return false;
		}
		if (mLastCheckRankingStageData == null && mLastCheckRankingStageData.Count < 1)
		{
			return false;
		}
		for (int num = mLastCheckRankingStageData.Count - 1; num >= 0; num--)
		{
			StageRankingData stageRankingData = mLastCheckRankingStageData[num];
			int clr_score = stageRankingData.clr_score;
			if (stageRankingData.uuid == mPlayer.UUID)
			{
				clr_score = mLastCheckPlayerStatScore;
			}
			if (score > clr_score)
			{
				newPlayerRank = num;
				rankingData = stageRankingData;
			}
			if (stageRankingData.uuid == mPlayer.UUID)
			{
				oldPlayerRank = num;
			}
		}
		if (rankingData == null)
		{
			return false;
		}
		if (rankingData.uuid == mPlayer.UUID)
		{
			return false;
		}
		MyFriend value = null;
		if (!mPlayer.Friends.TryGetValue(rankingData.uuid, out value) || value == null)
		{
			return false;
		}
		if (newPlayerRank == -1 || (oldPlayerRank != -1 && newPlayerRank > oldPlayerRank))
		{
			return false;
		}
		mLastCheckRankUpStageScore = true;
		mLastCheckRankingStageData = null;
		return true;
	}

	public bool CheckRankUpFriendsStar(Def.SERIES series, int stageNo, out MyFriend rankingData, out int newPlayerRank, out int oldPlayerRank)
	{
		rankingData = null;
		newPlayerRank = -1;
		oldPlayerRank = -1;
		if (mLastCheckRankUpFriendsStar)
		{
			return false;
		}
		mLastCheckRankUpFriendsStar = true;
		List<FriendRankingItem> list = new List<FriendRankingItem>();
		if (mPlayer != null && mPlayer.Friends != null && mPlayer.Friends.Count > 0)
		{
			foreach (KeyValuePair<int, MyFriend> friend in mPlayer.Friends)
			{
				list.Add(new FriendRankingItem(friend.Value));
			}
			list.Add(new FriendRankingItem());
			list.Sort(FriendRankingList.StarSort);
			long num = mPlayer.TotalStars;
			for (int num2 = list.Count - 1; num2 >= 0; num2--)
			{
				FriendRankingItem friendRankingItem = list[num2];
				if (num > friendRankingItem.TotalStar)
				{
					newPlayerRank = num2;
					rankingData = friendRankingItem.Info;
				}
				if (friendRankingItem.UUID == mPlayer.UUID)
				{
					oldPlayerRank = num2;
				}
			}
			if (rankingData == null)
			{
				mLastCheckPlayerStatStar = oldPlayerRank;
				return false;
			}
			if (newPlayerRank == -1 || (oldPlayerRank != -1 && newPlayerRank > mLastCheckPlayerStatStar))
			{
				mLastCheckPlayerStatStar = oldPlayerRank;
				return false;
			}
			mLastCheckPlayerStatStar = oldPlayerRank;
			mLastCheckRankUpFriendsStar = true;
			list.Clear();
			list = null;
			return true;
		}
		return false;
	}

	public bool CheckRankUpFriendsStage(Def.SERIES series, int stageNo, out MyFriend rankingData, out int newPlayerRank, out int oldPlayerRank)
	{
		rankingData = null;
		newPlayerRank = -1;
		oldPlayerRank = -1;
		List<FriendRankingItem> list = new List<FriendRankingItem>();
		if (mPlayer != null && mPlayer.Friends != null && mPlayer.Friends.Count > 0)
		{
			foreach (KeyValuePair<int, MyFriend> friend in mPlayer.Friends)
			{
				list.Add(new FriendRankingItem(friend.Value));
			}
			list.Add(new FriendRankingItem());
			list.Sort(FriendRankingList.LevelSort);
			List<PlayStageParam> playStage = mPlayer.PlayStage;
			int num = 0;
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == (int)mPlayer.Data.CurrentSeries)
				{
					num = playStage[i].Level;
					num /= 100;
					break;
				}
			}
			for (int num2 = list.Count - 1; num2 >= 0; num2--)
			{
				FriendRankingItem friendRankingItem = list[num2];
				int num3 = 1;
				if (friendRankingItem.UUID != mPlayer.UUID)
				{
					List<PlayStageParam> playStage2 = friendRankingItem.Info.Data.PlayStage;
					num3 = ((playStage2.Count != 0) ? HavePlayStageLevel(playStage2) : NoHavePlayStageLevel(friendRankingItem.Info.Data.Level));
				}
				else
				{
					num3 = num;
				}
				if (num > num3)
				{
					newPlayerRank = num2;
					rankingData = friendRankingItem.Info;
				}
				if (friendRankingItem.UUID == mPlayer.UUID)
				{
					oldPlayerRank = num2;
				}
			}
			if (rankingData == null)
			{
				mLastCheckPlayerStatLevel = oldPlayerRank;
				mLastCheckRankUpFriendsLevel = true;
				return false;
			}
			if (newPlayerRank == -1 || (oldPlayerRank != -1 && newPlayerRank > mLastCheckPlayerStatLevel))
			{
				mLastCheckPlayerStatLevel = oldPlayerRank;
				mLastCheckRankUpFriendsLevel = true;
				return false;
			}
			mLastCheckPlayerStatLevel = oldPlayerRank;
			if (mLastCheckRankUpFriendsLevel)
			{
				return false;
			}
			mLastCheckRankUpFriendsLevel = true;
			list.Clear();
			list = null;
			return true;
		}
		return false;
	}

	public int HavePlayStageLevel(List<PlayStageParam> stage)
	{
		int result = 1;
		for (int i = 0; i < stage.Count; i++)
		{
			if (stage[i].Series == (int)mPlayer.Data.CurrentSeries)
			{
				result = stage[i].Level;
				result /= 100;
				break;
			}
		}
		return result;
	}

	public int NoHavePlayStageLevel(int alevel)
	{
		if (mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST)
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(alevel, out a_main, out a_sub);
			return a_main;
		}
		return 1;
	}

	public bool CheckRankUpFriendsTrophy(Def.SERIES series, int stageNo, out MyFriend rankingData, out int newPlayerRank, out int oldPlayerRank)
	{
		rankingData = null;
		newPlayerRank = -1;
		oldPlayerRank = -1;
		if (mLastCheckRankUpFriendsLevel)
		{
			return false;
		}
		List<FriendRankingItem> list = new List<FriendRankingItem>();
		if (mPlayer != null && mPlayer.Friends != null && mPlayer.Friends.Count > 0)
		{
			foreach (KeyValuePair<int, MyFriend> friend in mPlayer.Friends)
			{
				list.Add(new FriendRankingItem(friend.Value));
			}
			list.Add(new FriendRankingItem());
			list.Sort(FriendRankingList.TrophySort);
			long totalTrophy = mPlayer.TotalTrophy;
			for (int num = list.Count - 1; num >= 0; num--)
			{
				FriendRankingItem friendRankingItem = list[num];
				if (totalTrophy > friendRankingItem.TotalTrophy)
				{
					newPlayerRank = num;
					rankingData = friendRankingItem.Info;
				}
				if (friendRankingItem.UUID == mPlayer.UUID)
				{
					oldPlayerRank = num;
				}
			}
			if (rankingData == null)
			{
				mLastCheckPlayerStatTrophy = oldPlayerRank;
				return false;
			}
			if (newPlayerRank == -1 || (oldPlayerRank != -1 && newPlayerRank > mLastCheckPlayerStatTrophy))
			{
				mLastCheckPlayerStatTrophy = oldPlayerRank;
				return false;
			}
			mLastCheckPlayerStatTrophy = oldPlayerRank;
			mLastCheckRankUpFriendsLevel = true;
			list.Clear();
			list = null;
			return true;
		}
		return false;
	}

	public int BoosterID_ChangeShopID(int boosterID)
	{
		int result = 0;
		for (int i = 0; i < mShopItemData.Count; i++)
		{
			if (boosterID == mShopItemData[i].BoosterID)
			{
				result = mShopItemData[i].Index;
				break;
			}
		}
		return result;
	}

	public void LinkBannerList_Clear()
	{
		if (linkbanner_InfoList != null)
		{
			while (linkbanner_InfoList.Count > 0)
			{
				linkbanner_InfoList.RemoveAt(0);
			}
			linkbanner_InfoList = null;
		}
	}

	public Live2DRender CreateLive2DRender(int textureWidth, int textureHeight, Color backGroundColor)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GetOriginalPrefabGOs(Res.PREFAB.LIVE2D_CAMERA));
		gameObject.transform.parent = mCamera.transform.parent;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		Live2DRender component = gameObject.GetComponent<Live2DRender>();
		component.BackgroundColor = backGroundColor;
		component.Init(textureWidth, textureHeight);
		return component;
	}

	public Live2DRender CreateLive2DRender(int textureWidth, int textureHeight)
	{
		return CreateLive2DRender(textureWidth, textureHeight, new Color(0.5f, 0.5f, 0.5f, 0f));
	}

	public RenderTexture CreateRenderTexture(int width, int height, int depth)
	{
		RenderTexture renderTexture = new RenderTexture(width, height, depth);
		renderTexture.generateMips = false;
		renderTexture.useMipMap = false;
		renderTexture.enableRandomWrite = false;
		renderTexture.format = RenderTextureFormat.ARGB32;
		return renderTexture;
	}

	[Conditional("BIJ_DEBUG")]
	public static void AddDebugInfo(string a_line, Color a_col, bool a_noerase = false)
	{
	}

	[Conditional("BIJ_DEBUG")]
	public static void AddDebugInfoSub(string a_line, Color a_col, bool a_noerase = false, int a_index = -1)
	{
	}

	[Conditional("BIJ_DEBUG")]
	public static void AddDebugInfo(string a_line, bool a_noerase = false)
	{
	}

	[Conditional("BIJ_DEBUG")]
	public static void AddDebugInfoSub(string a_line, bool a_noerase = false, int a_index = -1)
	{
	}

	public static void ClearDebugInfo(bool a_force = false, bool a_isMain = true)
	{
	}

	public static void DisplayDebugInfo(bool a_flg)
	{
	}

	public void DebugWinRate(int stageNumber, int stageFormatVersion, int stageRevision, string userSegment, string puzzleMode, bool isWon, int stageScore, int stageGotStars, int stageMoves, float stageTime, int continueCount, int maxCombo, int useSkill, Dictionary<Def.BOOSTER_KIND, int> useBooster, Dictionary<Def.TILE_KIND, int> dropPresentBox, int x_winScore, int clear_score, int clear_stars, int limit_moves)
	{
		int tgt_stage = stageNumber;
		int currentSeries = (int)mPlayer.Data.CurrentSeries;
		int num = (isWon ? 1 : 0);
		int ptime = (int)stageTime;
		int lifeCount = mPlayer.LifeCount;
		int value = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.WAVE_BOMB, out value);
		int value2 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.TAP_BOMB2, out value2);
		int value3 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.RAINBOW, out value3);
		int value4 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.ADD_SCORE, out value4);
		int value5 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.SKILL_CHARGE, out value5);
		int value6 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.SELECT_ONE, out value6);
		int value7 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.SELECT_COLLECT, out value7);
		int value8 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.COLOR_CRUSH, out value8);
		int value9 = 0;
		useBooster.TryGetValue(Def.BOOSTER_KIND.ALL_CRUSH, out value9);
		int num2 = value + value2 + value3 + value4 + value5;
		int num3 = value6 + value7 + value8 + value9;
		int value10 = 0;
		dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX0, out value10);
		int value11 = 0;
		dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX1, out value11);
		int value12 = 0;
		dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX2, out value12);
		int last_login = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mPlayer.mLastLoginTime);
		int game_start = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.GameStartTime);
		int first_purchase = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.FirstPurchasedTime);
		long startupCount = mPlayer.Data.StartupCount;
		long playCount = mPlayer.Data.PlayCount;
		long totalWin = mPlayer.Data.TotalWin;
		long totalLose = mPlayer.Data.TotalLose;
		float winRate = mPlayer.WinRate;
		long purchaseCount = mOptions.PurchaseCount;
		int stage_win = 0;
		int stage_lose = 0;
		float stage_win_rate = 0f;
		PlayerStageData _psd;
		if (mEventMode)
		{
			int eventID = mCurrentStage.EventID;
			short a_course = 0;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
			mPlayer.GetStageChallengeData(mCurrentStage.Series, eventID, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
			int stageNo = Def.GetStageNo(a_course, a_main, a_sub);
			tgt_stage = Def.GetStageNoForServer(eventID, stageNo);
		}
		else
		{
			mPlayer.GetStageChallengeData(mCurrentStage.Series, mCurrentStage.StageNumber, out _psd);
		}
		if (_psd != null)
		{
			stage_win = _psd.WinCount;
			stage_lose = _psd.LoseCount;
			stage_win_rate = _psd.WinRate;
		}
		int character_ID = mCompanionIndex;
		int companionLevel = mPlayer.GetCompanionLevel(mCompanionIndex);
		int num4 = 1;
		if (useSkill > 0)
		{
			num4 = ((num2 > 0 && num3 > 0) ? 8 : ((num3 > 0) ? 7 : ((num2 <= 0) ? 4 : 6)));
		}
		else if (num2 > 0 && num3 > 0)
		{
			num4 = 5;
		}
		else if (num3 > 0)
		{
			num4 = 3;
		}
		else if (num2 > 0)
		{
			num4 = 2;
		}
		if (continueCount > 0)
		{
			num4 += 100;
		}
		if (num == 0 && mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
		{
			num4 += 200;
		}
		if (mEventMode)
		{
			int eventID2 = mCurrentStage.EventID;
			ServerCram.DebugEventWinRate(tgt_stage, eventID2, stageFormatVersion, stageRevision, userSegment, puzzleMode, num, num4, stageScore, stageGotStars, stageMoves, ptime, continueCount, maxCombo, lifeCount, last_login, game_start, first_purchase, purchaseCount, startupCount, playCount, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, character_ID, companionLevel, useSkill, clear_score, clear_stars, limit_moves);
		}
		else
		{
			ServerCram.DebugWinRate(tgt_stage, currentSeries, stageFormatVersion, stageRevision, userSegment, puzzleMode, num, num4, stageScore, stageGotStars, stageMoves, ptime, continueCount, maxCombo, lifeCount, last_login, game_start, first_purchase, purchaseCount, startupCount, playCount, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, character_ID, companionLevel, useSkill, clear_score, clear_stars, limit_moves);
		}
	}

	public void SendGetBoostInShop(ShopItemData aItemData, int aGetType)
	{
		int num = 0;
		switch ((Def.BOOSTER_KIND)aItemData.BoosterID)
		{
		case Def.BOOSTER_KIND.WAVE_BOMB:
			num = aItemData.ItemDetail.WAVE_BOMB;
			break;
		case Def.BOOSTER_KIND.TAP_BOMB2:
			num = aItemData.ItemDetail.TAP_BOMB2;
			break;
		case Def.BOOSTER_KIND.RAINBOW:
			num = aItemData.ItemDetail.RAINBOW;
			break;
		case Def.BOOSTER_KIND.ADD_SCORE:
			num = aItemData.ItemDetail.ADD_SCORE;
			break;
		case Def.BOOSTER_KIND.SKILL_CHARGE:
			num = aItemData.ItemDetail.SKILL_CHARGE;
			break;
		case Def.BOOSTER_KIND.SELECT_ONE:
			num = aItemData.ItemDetail.SELECT_ONE;
			break;
		case Def.BOOSTER_KIND.SELECT_COLLECT:
			num = aItemData.ItemDetail.SELECT_COLLECT;
			break;
		case Def.BOOSTER_KIND.COLOR_CRUSH:
			num = aItemData.ItemDetail.COLOR_CRUSH;
			break;
		case Def.BOOSTER_KIND.BLOCK_CRUSH:
			num = aItemData.ItemDetail.BLOCK_CRUSH;
			break;
		case Def.BOOSTER_KIND.MUGEN_HEART15:
			num = aItemData.ItemDetail.MUGEN_HEART15;
			break;
		case Def.BOOSTER_KIND.MUGEN_HEART30:
			num = aItemData.ItemDetail.MUGEN_HEART30;
			break;
		case Def.BOOSTER_KIND.MUGEN_HEART60:
			num = aItemData.ItemDetail.MUGEN_HEART60;
			break;
		case Def.BOOSTER_KIND.PAIR_MAKER:
			num = aItemData.ItemDetail.PAIR_MAKER;
			break;
		case Def.BOOSTER_KIND.ALL_CRUSH:
			num = aItemData.ItemDetail.ALL_CRUSH;
			break;
		}
		if (aItemData.IsSetBundle)
		{
			num = 1;
		}
		if (aItemData.SaleNum == 1)
		{
			num = 1;
		}
		if (num != 0)
		{
			int index = aItemData.Index;
			int gemAmount = aItemData.GemAmount;
			SendGetBoost(num, gemAmount, aGetType, index);
		}
	}

	public void SendGetBoost(int aAmount, int aCost, int aGetType, Def.BOOSTER_KIND aBoosterId)
	{
		int aKind = BoosterID_ChangeShopID((int)aBoosterId);
		SendGetBoost(aAmount, aCost, aGetType, aKind);
	}

	public void SendGetBoost(int aAmount, int aCost, int aGetType, int aKind)
	{
		int num = (int)mPlayer.Data.LastPlaySeries;
		int num2 = mPlayer.Data.LastPlayLevel;
		if (aGetType == 8)
		{
			num = (int)mPlayer.Data.CurrentSeries;
			num2 = mCurrentStage.StageNumber;
		}
		int stage_win = 0;
		int stage_lose = 0;
		float stage_win_rate = 0f;
		PlayerStageData _psd = null;
		if (IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
		{
			int num3 = ((mDebugDailyChallengeID <= 0) ? GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID : mDebugDailyChallengeID);
			num = 200;
			num2 = int.Parse(num3 + (SelectedDailyChallengeSheetNo - 1).ToString("00") + SelectedDailyChallengeStageNo.ToString("0"));
		}
		else if (num == 5)
		{
			int num4 = mPlayer.Data.CurrentEventID;
			short a_course = 0;
			int a_eventID;
			int a_main;
			int a_sub;
			Def.SplitServerStageNo(num2, out a_eventID, out a_course, out a_main, out a_sub);
			if (num4 == -1)
			{
				num4 = a_eventID;
			}
			int stageNo = Def.GetStageNo(a_course, a_main, a_sub);
			mPlayer.GetStageChallengeData((Def.SERIES)num, num4, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
			num2 = Def.GetStageNoForServer(num4, stageNo);
		}
		else
		{
			mPlayer.GetStageChallengeData(mPlayer.Data.CurrentSeries, num2, out _psd);
		}
		if (_psd != null)
		{
			stage_win = _psd.WinCount;
			stage_lose = _psd.LoseCount;
			stage_win_rate = _psd.WinRate;
		}
		string boosterNumString = ServerCram.GetBoosterNumString();
		long totalWin = mPlayer.Data.TotalWin;
		long totalLose = mPlayer.Data.TotalLose;
		float winRate = mPlayer.WinRate;
		long purchaseCount = mOptions.PurchaseCount;
		ServerCram.GetBoost(aAmount, aCost, num, num2, aGetType, aKind, boosterNumString, totalWin, totalLose, winRate, stage_win, stage_lose, stage_win_rate, purchaseCount);
	}

	public void CreateInfinityEffects()
	{
		if (dbgEffectPlaying)
		{
			StopCoroutine("CoCreateInfinityEffects");
			dbgEffectPlaying = false;
		}
		else
		{
			StartCoroutine("CoCreateInfinityEffects");
			dbgEffectPlaying = true;
		}
	}

	private IEnumerator CoCreateInfinityEffects()
	{
		float interval = 0f;
		while (true)
		{
			interval += Time.deltaTime;
			if (interval >= dbgInterval)
			{
				for (int i = 0; i < dbgMakeCount; i++)
				{
					float ofsX = UnityEngine.Random.Range(-2000f, 2000f);
					float ofsY = UnityEngine.Random.Range(-2000f, 2000f);
					Util.CreateOneShotAnime("dbgEffect", dbgEffectKey, dbgParent, dbgPos + new Vector3(ofsX, ofsY, 0f), Vector3.one, 0f, 0f);
				}
				interval -= dbgInterval;
			}
			yield return 0;
		}
	}

	public void CreateInfinityScoreEffects()
	{
		if (dbgScoreEffectPlaying)
		{
			StopCoroutine("CoCreateInfinityScoreEffects");
			dbgScoreEffectPlaying = false;
		}
		else
		{
			StartCoroutine("CoCreateInfinityScoreEffects");
			dbgScoreEffectPlaying = true;
		}
	}

	private IEnumerator CoCreateInfinityScoreEffects()
	{
		float interval = 0f;
		while (true)
		{
			interval += Time.deltaTime;
			if (interval >= dbgInterval)
			{
				for (int i = 0; i < dbgMakeCount; i++)
				{
					GameObject go = (GameObject)UnityEngine.Object.Instantiate(GetOriginalPrefabGOs(Res.PREFAB.GETSCOREEFFECT));
					Util.SetGameObject(go, "Score", dbgParent);
					float ofsX = UnityEngine.Random.Range(-2000f, 2000f);
					float ofsY = UnityEngine.Random.Range(-2000f, 2000f);
					GetScoreEffect effect = go.GetComponent<GetScoreEffect>();
					effect.transform.localPosition = dbgPos + new Vector3(ofsX, ofsY, Def.PUZZLE_TILE_SCORE_Z);
					effect.Init(123, Def.TILE_KIND.COLOR0, 0f);
				}
				interval -= dbgInterval;
			}
			yield return 0;
		}
	}

	private void ReleaseBuildSetting()
	{
		USE_DEBUG_DIALOG = false;
		HIDDEN_DEBUG_BUTTON = false;
		USE_DEBUG_STATE = false;
		NO_NETWORK = false;
		DISABLE_TIMECHECK = false;
		ALWAYS_LOG = false;
		DEBUG_WINRATE = false;
		DEBUG_SHOW_FIGURE_ID = false;
		DEBUG_DEBUGSERVERINFO = false;
		CONNECT_TESTSERVER = false;
		USE_DEBUG_DIALOG = false;
		HIDDEN_DEBUG_BUTTON = false;
		USE_DEBUG_STATE = false;
		NO_NETWORK = false;
		DISABLE_TIMECHECK = false;
		ALWAYS_LOG = false;
		DEBUG_WINRATE = false;
		DEBUG_SHOW_FIGURE_ID = false;
		DEBUG_DEBUGSERVERINFO = false;
		CONNECT_TESTSERVER = false;
		USE_DEBUG_TIER = false;
	}

	public void SetTestServerSettings(bool a_flg)
	{
		if (a_flg && USE_DEBUG_DIALOG)
		{
			string text = "Data/Network/android_test.bin";
			using (BIJResourceBinaryReader reader = new BIJResourceBinaryReader(text))
			{
				NetworkConstants_Android.Unload();
				Network.NetworkConstants = NetworkConstants_Android.Load(reader);
			}
		}
	}

	public void CheckMailDeadLine()
	{
		if (mPlayer.Gifts.Count <= 0)
		{
			return;
		}
		bool flag = false;
		DateTime now = DateTime.Now;
		List<GiftItem> list = new List<GiftItem>();
		long num2;
		long num;
		foreach (KeyValuePair<string, GiftItem> gift in mPlayer.Gifts)
		{
			GiftItem value = gift.Value;
			if (value == null)
			{
				continue;
			}
			if (value.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ)
			{
				if (!mPlayer.IsValidFriendHelp(value.Data.ItemKind, value.Data.ItemID))
				{
					list.Add(value);
				}
			}
			else if (value.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ)
			{
				num = ((value.Data.RemoteTimeEpoch == 0) ? DateTimeUtil.BetweenDays0(value.Data.CreateTime, now) : DateTimeUtil.BetweenDays0(value.RemoteTime, now));
				num2 = Constants.GIFT_EXPIRED_FRIEND_REQ_FREQ;
				if (num >= num2)
				{
					list.Add(value);
				}
			}
			else if ((value.GiftItemCategory == Def.ITEM_CATEGORY.CONSUME || value.GiftItemCategory == Def.ITEM_CATEGORY.BOOSTER || value.GiftItemCategory == Def.ITEM_CATEGORY.FREE_BOOSTER) && value.Data.ItemID == 0)
			{
				list.Add(value);
			}
		}
		if (list.Count > 0)
		{
			foreach (GiftItem item in list)
			{
				int itemCategory = item.Data.ItemCategory;
				int itemKind = item.Data.ItemKind;
				int itemID = item.Data.ItemID;
				int quantity = item.Data.Quantity;
				int createTimeEpoch = item.CreateTimeEpoch;
				int lastPlaySeries = (int)SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlaySeries;
				int lastPlayLevel = SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlayLevel;
				ServerCram.LostGiftItem(itemCategory, itemKind, itemID, quantity, createTimeEpoch, lastPlaySeries, lastPlayLevel);
				mPlayer.RemoveGift(item);
			}
			flag = true;
		}
		num = DateTimeUtil.BetweenDays0(mPlayer.Data.LastGiftExpiredCheckTime, now);
		num2 = Constants.GIFT_EXPIRED_CHECK_FREQ;
		if (num2 <= 0 || num >= num2)
		{
			foreach (KeyValuePair<string, GiftItem> gift2 in mPlayer.Gifts)
			{
				if (gift2.Value.Data.ItemCategory != 34)
				{
					continue;
				}
				List<GiftItem> messageCardPresentList = GetMessageCardPresentList(gift2.Value.Data.MessageCardID);
				if (gift2.Value.Data.MessageCardPresentCount == messageCardPresentList.Count)
				{
					continue;
				}
				gift2.Value.Data.CreateTime = now;
				foreach (GiftItem item2 in messageCardPresentList)
				{
					item2.Data.CreateTime = now;
				}
			}
			List<GiftItem> list2 = new List<GiftItem>();
			foreach (KeyValuePair<string, GiftItem> gift3 in mPlayer.Gifts)
			{
				GiftItem value2 = gift3.Value;
				if (value2 == null)
				{
					continue;
				}
				num2 = ((value2.GiftItemCategory == Def.ITEM_CATEGORY.PRESENT_HEART || value2.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL) ? Constants.FRIENDHEART_GIFT_EXPIRED : Constants.GIFT_EXPIRED);
				if (num2 > 0)
				{
					num = DateTimeUtil.BetweenDays0(value2.Data.CreateTime, now);
					if (num >= num2)
					{
						list2.Add(value2);
					}
				}
			}
			if (list2.Count > 0)
			{
				foreach (GiftItem item3 in list2)
				{
					int itemCategory2 = item3.Data.ItemCategory;
					int itemKind2 = item3.Data.ItemKind;
					int itemID2 = item3.Data.ItemID;
					int quantity2 = item3.Data.Quantity;
					int createTimeEpoch2 = item3.CreateTimeEpoch;
					int lastPlaySeries2 = (int)SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlaySeries;
					int lastPlayLevel2 = SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlayLevel;
					ServerCram.LostGiftItem(itemCategory2, itemKind2, itemID2, quantity2, createTimeEpoch2, lastPlaySeries2, lastPlayLevel2);
					mPlayer.RemoveGift(item3);
				}
				flag = true;
			}
		}
		if (!IsPlayAdvMode() || mGameProfile.AdvSettings.SendMedal == 0)
		{
			List<GiftItem> list3 = new List<GiftItem>();
			foreach (KeyValuePair<string, GiftItem> gift4 in mPlayer.Gifts)
			{
				GiftItem value3 = gift4.Value;
				if (value3 != null && value3.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL)
				{
					list3.Add(value3);
				}
			}
			if (list3.Count > 0)
			{
				foreach (GiftItem item4 in list3)
				{
					mPlayer.RemoveGift(item4);
				}
				flag = true;
			}
		}
		if (flag)
		{
			Save();
		}
	}

	public List<GiftItem> GetMessageCardPresentList(int _cardID)
	{
		List<GiftItem> list = new List<GiftItem>();
		foreach (KeyValuePair<string, GiftItem> gift in mPlayer.Gifts)
		{
			if (gift.Value.Data.ItemCategory != 34 && _cardID == gift.Value.Data.MessageCardID)
			{
				list.Add(gift.Value);
			}
		}
		return list;
	}

	private void InvokeCallbackEvent(EVENT_TYPE type)
	{
		UnityEvent callbackEvent = GetCallbackEvent(type, false);
		if (callbackEvent != null)
		{
			callbackEvent.Invoke();
		}
	}

	private void RegisterCallbackEvent(UnityAction<UnityAction> fnc, params UnityAction[] cb)
	{
		if (fnc == null || cb == null)
		{
			return;
		}
		foreach (UnityAction unityAction in cb)
		{
			if (unityAction != null)
			{
				fnc(unityAction);
			}
		}
	}

	private UnityEvent GetCallbackEvent(EVENT_TYPE type, bool is_create)
	{
		if (is_create && mCallbackEvent == null)
		{
			mCallbackEvent = new Dictionary<EVENT_TYPE, UnityEvent>();
		}
		if (is_create && !mCallbackEvent.ContainsKey(type))
		{
			mCallbackEvent[type] = new UnityEvent();
		}
		return (!mCallbackEvent.ContainsKey(type)) ? null : mCallbackEvent[type];
	}

	public void AddCallbackEvent(EVENT_TYPE type, params UnityAction[] cb)
	{
		UnityEvent callbackEvent = GetCallbackEvent(type, true);
		if (callbackEvent != null)
		{
			RegisterCallbackEvent(callbackEvent.AddListener, cb);
		}
	}

	public void RemoveCallbackEvent(EVENT_TYPE type, params UnityAction[] cb)
	{
		UnityEvent callbackEvent = GetCallbackEvent(type, false);
		if (callbackEvent != null)
		{
			RegisterCallbackEvent(callbackEvent.RemoveListener, cb);
		}
	}

	public void ClearCallbackEvent(EVENT_TYPE type)
	{
		UnityEvent callbackEvent = GetCallbackEvent(type, false);
		if (callbackEvent != null)
		{
			callbackEvent.RemoveAllListeners();
		}
	}

	public void ClearCallbackEvent()
	{
		foreach (int value in Enum.GetValues(typeof(EVENT_TYPE)))
		{
			ClearCallbackEvent((EVENT_TYPE)value);
		}
	}

	public void SetCallbackEvent(EVENT_TYPE type, params UnityAction[] cb)
	{
		ClearCallbackEvent(type);
		AddCallbackEvent(type, cb);
	}

	public float GetMapSize(Def.EVENT_TYPE a_type)
	{
		float result = 1136f;
		if (a_type == Def.EVENT_TYPE.SM_RALLY2)
		{
			result = 1203f;
		}
		return result;
	}

	public void LoadEventSystem()
	{
		if (mEventSystemGO == null)
		{
			mEventSystemGO = (GameObject)UnityEngine.Object.Instantiate(GetOriginalPrefabGOs(Res.PREFAB.EVENTSYSTEM));
		}
	}

	public void NeverSleepTimeout()
	{
	}

	public void RestoreSleepTimeout()
	{
	}
}
