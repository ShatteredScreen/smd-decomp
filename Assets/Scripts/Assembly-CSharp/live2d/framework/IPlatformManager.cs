namespace live2d.framework
{
	public interface IPlatformManager
	{
		byte[] loadBytes(string path);

		string loadString(string path);

		ALive2DModel loadLive2DModel(string path);

		void loadTexture(ALive2DModel model, int no, string path);

		void log(string txt);
	}
}
