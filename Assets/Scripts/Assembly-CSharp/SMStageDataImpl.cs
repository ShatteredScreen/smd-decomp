using System;
using System.Collections.Generic;
using System.IO;

public class SMStageDataImpl : ISMStageData
{
	public enum DianaWayError
	{
		NONE = 0,
		NO_START = 1,
		MULTI_START = 2,
		NO_GOAL = 3,
		MULTI_GOAL = 4,
		NO_WAY = 5,
		NO_REACH_GOAL = 6,
		FOUND_WAY_ON_START = 7,
		NOT_FOUND_NEXT_TILEINFO = 8,
		MULTI_STARTWAY = 9,
		FOUND_WAY_ON_GOAL = 10,
		GOAL_DIRECTION_ERROR = 11
	}

	protected delegate void array_fn(int x, int y);

	private const string SIGNATURE = "SMD";

	private const string SIGNATURE_SD = "SPD";

	protected static readonly SMTile DEFAULT_TILE = new SMTile(Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL);

	protected static readonly SMAttributeTile DEFAULT_ATTRIBUTE_TILE = new SMAttributeTile(Def.TILE_KIND.WALL, Def.TILE_FORM.NORMAL);

	protected static readonly SMTileInfo DEFAULT_TILE_INFO = new SMTileInfo();

	protected SMGridData mGridData;

	private int mLimitMoves;

	private int mLimitTimeSec;

	protected int mPaper0;

	protected int mPaper1;

	protected int mPaper2;

	protected int mSilverCrystal;

	protected int mFindStencil0;

	protected int mFindStencil1;

	protected int mFindStencil2;

	protected int mFindStencil3;

	protected int mPlayAreaX;

	protected int mPlayAreaY;

	protected int mPlayAreaWidth;

	protected int mPlayAreaHeight;

	private static short[] SearchTile_x = new short[4] { 0, 0, 1, -1 };

	private static short[] SearchTile_y = new short[4] { 1, -1, 0, 0 };

	public virtual int FormatVersion { get; set; }

	public virtual int OriginalFormatVersion { get; protected set; }

	public virtual int Revision { get; set; }

	public virtual Def.SERIES Series { get; set; }

	public virtual int StageNumber { get; set; }

	public virtual int EventID { get; set; }

	public virtual string DisplayStageNumber { get; set; }

	public virtual int MainStageNumber { get; set; }

	public virtual int SubStageNumber { get; set; }

	public virtual int CandyKindNum { get; set; }

	public virtual bool UseColor0 { get; set; }

	public virtual bool UseColor1 { get; set; }

	public virtual bool UseColor2 { get; set; }

	public virtual bool UseColor3 { get; set; }

	public virtual bool UseColor4 { get; set; }

	public virtual bool UseColor5 { get; set; }

	public virtual bool UseColor6 { get; set; }

	public virtual Def.STAGE_TYPE StageType { get; set; }

	public virtual Def.STAGE_WIN_CONDITION WinType { get; set; }

	public virtual Def.STAGE_LOSE_CONDITION LoseType { get; set; }

	public virtual int LimitMoves
	{
		get
		{
			if (OverwriteLimitMoves > 0)
			{
				return OverwriteLimitMoves;
			}
			return mLimitMoves;
		}
		set
		{
			mLimitMoves = value;
		}
	}

	public virtual int LimitTimeSec
	{
		get
		{
			if (OverwriteLimitTimeSec > 0)
			{
				return OverwriteLimitTimeSec;
			}
			return mLimitTimeSec;
		}
		set
		{
			mLimitTimeSec = value;
		}
	}

	public virtual int OverwriteLimitMoves { get; set; }

	public virtual int OverwriteLimitTimeSec { get; set; }

	public virtual int BronzeScore { get; set; }

	public virtual int SilverScore { get; set; }

	public virtual int GoldScore { get; set; }

	public virtual int Color0 { get; set; }

	public virtual int Color1 { get; set; }

	public virtual int Color2 { get; set; }

	public virtual int Color3 { get; set; }

	public virtual int Color4 { get; set; }

	public virtual int Color5 { get; set; }

	public virtual int Color6 { get; set; }

	public virtual int Ingredient0 { get; set; }

	public virtual int Ingredient1 { get; set; }

	public virtual int Ingredient2 { get; set; }

	public virtual int Ingredient3 { get; set; }

	public virtual int Paper0
	{
		get
		{
			return mPaper0;
		}
		set
		{
			mPaper0 = value;
		}
	}

	public virtual int Paper1
	{
		get
		{
			return mPaper1;
		}
		set
		{
			mPaper1 = value;
		}
	}

	public virtual int Paper2
	{
		get
		{
			return mPaper2;
		}
		set
		{
			mPaper2 = value;
		}
	}

	public virtual Def.BOOSTER_KIND BoosterSlot1 { get; set; }

	public virtual Def.BOOSTER_KIND BoosterSlot2 { get; set; }

	public virtual Def.BOOSTER_KIND BoosterSlot3 { get; set; }

	public virtual Def.BOOSTER_KIND BoosterSlot4 { get; set; }

	public virtual Def.BOOSTER_KIND BoosterSlot5 { get; set; }

	public virtual Def.BOOSTER_KIND BoosterSlot6 { get; set; }

	public virtual Def.BOOSTER_KIND InPuzzleBoosterSlot1 { get; set; }

	public virtual Def.BOOSTER_KIND InPuzzleBoosterSlot2 { get; set; }

	public virtual Def.BOOSTER_KIND InPuzzleBoosterSlot3 { get; set; }

	public virtual Def.BOOSTER_KIND InPuzzleBoosterSlot4 { get; set; }

	public virtual float BonusFreq1 { get; set; }

	public virtual float BonusMove1 { get; set; }

	public virtual int BonusLimit1 { get; set; }

	public virtual int BonusMoveLimit1 { get; set; }

	public virtual int Wave { get; set; }

	public virtual int Bomb { get; set; }

	public virtual int Rainbow { get; set; }

	public virtual int WaveWave { get; set; }

	public virtual int BombBomb { get; set; }

	public virtual int RainbowRainbow { get; set; }

	public virtual int WaveBomb { get; set; }

	public virtual int BombRainbow { get; set; }

	public virtual int WaveRainbow { get; set; }

	public virtual string BGM { get; set; }

	public virtual string EventCode { get; set; }

	public virtual string SegmentCode { get; set; }

	public virtual string TestCode { get; set; }

	public virtual int Timer0 { get; set; }

	public virtual int Timer1 { get; set; }

	public virtual int Timer2 { get; set; }

	public virtual int Timer3 { get; set; }

	public virtual int Timer4 { get; set; }

	public virtual int Timer5 { get; set; }

	public virtual float TimerFreq { get; set; }

	public virtual int TimerLimit { get; set; }

	public virtual int TimerRemainMin { get; set; }

	public virtual int TimerRemainMax { get; set; }

	public virtual int Pair0 { get; set; }

	public virtual int PairLimit0 { get; set; }

	public virtual float JewelFreq { get; set; }

	public virtual int JewelLimit { get; set; }

	public virtual float FCookie1Freq { get; set; }

	public virtual int FCookie1Limit { get; set; }

	public virtual float FCookie2Freq { get; set; }

	public virtual int FCookie2Limit { get; set; }

	public virtual float FCookie3Freq { get; set; }

	public virtual int FCookie3Limit { get; set; }

	public virtual int BossHP { get; set; }

	public virtual Def.TILE_KIND BossType { get; set; }

	public virtual int BossPositionX { get; set; }

	public virtual int BossPositionY { get; set; }

	public virtual int AttackCandyNum { get; set; }

	public virtual bool AttackUseCandy0 { get; set; }

	public virtual bool AttackUseCandy1 { get; set; }

	public virtual bool AttackUseCandy2 { get; set; }

	public virtual bool AttackUseCandy3 { get; set; }

	public virtual bool AttackUseCandy4 { get; set; }

	public virtual bool AttackUseCandy5 { get; set; }

	public virtual bool AttackUseCandy6 { get; set; }

	public virtual int BossFirstAttack { get; set; }

	public virtual int BossAttackInterval { get; set; }

	public virtual int SilverCrystal
	{
		get
		{
			return mSilverCrystal;
		}
		protected set
		{
			mSilverCrystal = value;
		}
	}

	public virtual int EnemyGhost { get; set; }

	public virtual int BossID { get; set; }

	public virtual Def.BOSS_ATTACK BossAttackPattern { get; set; }

	public virtual int BossAttackParam { get; set; }

	public virtual string BossWeakChara { get; set; }

	public virtual int FindStencil0
	{
		get
		{
			return mFindStencil0;
		}
		set
		{
			mFindStencil0 = value;
		}
	}

	public virtual int FindStencil1
	{
		get
		{
			return mFindStencil1;
		}
		set
		{
			mFindStencil1 = value;
		}
	}

	public virtual int FindStencil2
	{
		get
		{
			return mFindStencil2;
		}
		set
		{
			mFindStencil2 = value;
		}
	}

	public virtual int FindStencil3
	{
		get
		{
			return mFindStencil3;
		}
		set
		{
			mFindStencil3 = value;
		}
	}

	public virtual List<StencilData> StencilDataList { get; set; }

	public virtual int BossAttackParam2 { get; set; }

	public virtual int BossAttackParam3 { get; set; }

	public virtual float FOrb1Freq { get; set; }

	public virtual int FOrb1Limit { get; set; }

	public virtual float FOrb2Freq { get; set; }

	public virtual int FOrb2Limit { get; set; }

	public virtual float FOrb3Freq { get; set; }

	public virtual int FOrb3Limit { get; set; }

	public virtual float FOrb4Freq { get; set; }

	public virtual int FOrb4Limit { get; set; }

	public virtual float FootPieceFreq1 { get; set; }

	public virtual int FootPieceLimit1 { get; set; }

	public virtual float FJewelFreq1 { get; set; }

	public virtual int FJewel1Limit1 { get; set; }

	public virtual int FJewel3Limit1 { get; set; }

	public virtual int FJewel5Limit1 { get; set; }

	public virtual int FJewel9Limit1 { get; set; }

	public virtual float FPerl00Freq1 { get; set; }

	public virtual int FPerl00Limit1 { get; set; }

	public virtual float FPerl01Freq1 { get; set; }

	public virtual int FPerl01Limit1 { get; set; }

	public virtual float FPerl02Freq1 { get; set; }

	public virtual int FPerl02Limit1 { get; set; }

	public virtual List<IntVector2> DianaWayPosList { get; set; }

	public virtual int Width
	{
		get
		{
			return mGridData.Width;
		}
		set
		{
			mGridData.Width = value;
		}
	}

	public virtual int Height
	{
		get
		{
			return mGridData.Height;
		}
		set
		{
			mGridData.Height = value;
		}
	}

	public virtual int PlayAreaX
	{
		get
		{
			return mPlayAreaX;
		}
		set
		{
			mPlayAreaX = value;
		}
	}

	public virtual int PlayAreaY
	{
		get
		{
			return mPlayAreaY;
		}
		set
		{
			mPlayAreaY = value;
		}
	}

	public virtual int PlayAreaWidth
	{
		get
		{
			return mPlayAreaWidth;
		}
		set
		{
			mPlayAreaWidth = value;
		}
	}

	public virtual int PlayAreaHeight
	{
		get
		{
			return mPlayAreaHeight;
		}
		set
		{
			mPlayAreaHeight = value;
		}
	}

	public virtual SMGridData GridData
	{
		get
		{
			return mGridData;
		}
		set
		{
			mGridData = value;
		}
	}

	public SMStageDataImpl()
		: this(Def.SERIES.SM_FIRST, -1, 0)
	{
	}

	public SMStageDataImpl(Def.SERIES a_seires, int eventID, int stage)
	{
		FormatVersion = 0;
		Revision = 0;
		Series = a_seires;
		StageNumber = stage;
		EventID = eventID;
		int a_main;
		int a_sub;
		Def.SplitStageNo(StageNumber, out a_main, out a_sub);
		MainStageNumber = a_main;
		SubStageNumber = a_sub;
		StageNumber = Def.GetStageNo(a_main, a_sub);
		CandyKindNum = 5;
		UseColor0 = true;
		UseColor1 = true;
		UseColor2 = true;
		UseColor3 = true;
		UseColor4 = true;
		UseColor5 = false;
		UseColor6 = false;
		StageType = Def.STAGE_TYPE.SCORE_ATTACK;
		WinType = Def.STAGE_WIN_CONDITION.SCORE;
		LoseType = Def.STAGE_LOSE_CONDITION.MOVES;
		LimitMoves = 30;
		OverwriteLimitMoves = 0;
		LimitTimeSec = 0;
		OverwriteLimitTimeSec = 0;
		BronzeScore = 500;
		SilverScore = 1000;
		GoldScore = 1500;
		Color0 = 0;
		Color1 = 0;
		Color2 = 0;
		Color3 = 0;
		Color4 = 0;
		Color5 = 0;
		Color6 = 0;
		Ingredient0 = 0;
		Ingredient1 = 0;
		Ingredient2 = 0;
		Ingredient3 = 0;
		BoosterSlot1 = Def.BOOSTER_KIND.WAVE_BOMB;
		BoosterSlot2 = Def.BOOSTER_KIND.TAP_BOMB2;
		BoosterSlot3 = Def.BOOSTER_KIND.RAINBOW;
		BoosterSlot4 = Def.BOOSTER_KIND.ADD_SCORE;
		BoosterSlot5 = Def.BOOSTER_KIND.SKILL_CHARGE;
		BoosterSlot6 = Def.BOOSTER_KIND.NONE;
		InPuzzleBoosterSlot1 = Def.BOOSTER_KIND.SELECT_ONE;
		InPuzzleBoosterSlot2 = Def.BOOSTER_KIND.SELECT_COLLECT;
		InPuzzleBoosterSlot3 = Def.BOOSTER_KIND.COLOR_CRUSH;
		InPuzzleBoosterSlot4 = Def.BOOSTER_KIND.BLOCK_CRUSH;
		BonusFreq1 = 0f;
		BonusMove1 = 0f;
		BonusLimit1 = 0;
		BonusMoveLimit1 = 0;
		BGM = string.Empty;
		Timer0 = 0;
		Timer1 = 0;
		Timer2 = 0;
		Timer3 = 0;
		Timer4 = 0;
		Timer5 = 0;
		TimerFreq = 0f;
		TimerLimit = 0;
		TimerRemainMin = 0;
		TimerRemainMax = 0;
		Pair0 = 0;
		PairLimit0 = 0;
		JewelFreq = 0f;
		JewelLimit = 0;
		FCookie1Freq = 0f;
		FCookie1Limit = 0;
		FCookie2Freq = 0f;
		FCookie2Limit = 0;
		FCookie3Freq = 0f;
		FCookie3Limit = 0;
		BossHP = 0;
		BossType = Def.TILE_KIND.NONE;
		BossPositionX = 20;
		BossPositionY = 20;
		AttackCandyNum = 0;
		AttackUseCandy0 = false;
		AttackUseCandy1 = false;
		AttackUseCandy2 = false;
		AttackUseCandy3 = false;
		AttackUseCandy4 = false;
		AttackUseCandy5 = false;
		AttackUseCandy6 = false;
		BossFirstAttack = 10;
		BossAttackInterval = 5;
		EnemyGhost = 0;
		StencilDataList = new List<StencilData>();
		FOrb1Freq = 0f;
		FOrb1Limit = 0;
		FOrb2Freq = 0f;
		FOrb2Limit = 0;
		FOrb3Freq = 0f;
		FOrb3Limit = 0;
		FOrb4Freq = 0f;
		FOrb4Limit = 0;
		FootPieceFreq1 = 0f;
		FootPieceLimit1 = 0;
		FJewelFreq1 = 0f;
		FJewel1Limit1 = 0;
		FJewel3Limit1 = 0;
		FJewel5Limit1 = 0;
		FJewel9Limit1 = 0;
		FPerl00Freq1 = 0f;
		FPerl00Limit1 = 0;
		FPerl01Freq1 = 0f;
		FPerl01Limit1 = 0;
		FPerl02Freq1 = 0f;
		FPerl02Limit1 = 0;
		DianaWayPosList = new List<IntVector2>();
		mGridData = new SMGridData(0, 0);
		OnLoaded();
	}

	public virtual void OnSaved()
	{
	}

	public virtual void OnLoaded()
	{
		mPaper0 = 0;
		mPaper1 = 0;
		mPaper2 = 0;
		mSilverCrystal = 0;
		EnemyGhost = 0;
		mFindStencil0 = 0;
		mFindStencil1 = 0;
		mFindStencil2 = 0;
		mFindStencil3 = 0;
		StencilDataList.Clear();
		DianaWayPosList.Clear();
		ArrayFn(delegate(int x, int y)
		{
			SMAttributeTile sMAttributeTile = GetAttributeTile(x, y) as SMAttributeTile;
			if (sMAttributeTile != null)
			{
				if (sMAttributeTile.Kind == Def.TILE_KIND.THROUGH_WALL0 || (sMAttributeTile.Next != null && sMAttributeTile.Next.Kind == Def.TILE_KIND.THROUGH_WALL0))
				{
					mPaper0++;
				}
				if (sMAttributeTile.Kind == Def.TILE_KIND.THROUGH_WALL1 || (sMAttributeTile.Next != null && sMAttributeTile.Next.Kind == Def.TILE_KIND.THROUGH_WALL1))
				{
					mPaper1++;
				}
				if (sMAttributeTile.Kind == Def.TILE_KIND.THROUGH_WALL2 || (sMAttributeTile.Next != null && sMAttributeTile.Next.Kind == Def.TILE_KIND.THROUGH_WALL2))
				{
					mPaper2++;
				}
				if (sMAttributeTile.Kind == Def.TILE_KIND.GROWN_UP2 || sMAttributeTile.Kind == Def.TILE_KIND.GROWN_UP1 || sMAttributeTile.Kind == Def.TILE_KIND.GROWN_UP0)
				{
					mSilverCrystal++;
				}
				if (sMAttributeTile.Kind == Def.TILE_KIND.INCREASE)
				{
					EnemyGhost++;
				}
			}
			SMTileInfo sMTileInfo = GetTileInfo(x, y) as SMTileInfo;
			if (sMTileInfo != null && sMTileInfo.TileAttribute_FIND)
			{
				switch (sMTileInfo.FindType)
				{
				case Def.TILE_KIND.FIND0:
					mFindStencil0++;
					break;
				case Def.TILE_KIND.FIND1:
					mFindStencil1++;
					break;
				case Def.TILE_KIND.FIND2:
					mFindStencil2++;
					break;
				case Def.TILE_KIND.FIND3:
					mFindStencil3++;
					break;
				}
				StencilData item = default(StencilData);
				item.Position = new IntVector2(x, y);
				item.Size = new IntVector2(sMTileInfo.FindSizeX, sMTileInfo.FindSizeY);
				item.StencilType = sMTileInfo.FindType;
				item.Degree = sMTileInfo.FindRotate;
				item.ImageName = sMTileInfo.FindImageName;
				StencilDataList.Add(item);
			}
		});
		DianaWayError a_error;
		if (WinType == Def.STAGE_WIN_CONDITION.GOAL && (!CalcDianaWay(out a_error) || a_error != 0))
		{
			BIJLog.E("Diana Goal Error!!" + a_error);
		}
	}

	protected void ArrayFn(array_fn fn)
	{
		for (int i = 0; i < Height; i++)
		{
			for (int j = 0; j < Width; j++)
			{
				fn(j, i);
			}
		}
	}

	public virtual bool IsTileAvailable(int x, int y)
	{
		return mGridData.IsTileAvailable(x, y);
	}

	public virtual ISMTile GetTile(int x, int y)
	{
		SMTile sMTile = mGridData.GetTile(x, y);
		if (sMTile == null)
		{
			sMTile = DEFAULT_TILE;
		}
		return sMTile;
	}

	public virtual void SetTile(int x, int y, SMTile tile)
	{
		mGridData.SetTile(x, y, tile);
	}

	public virtual ISMAttributeTile GetAttributeTile(int x, int y)
	{
		SMAttributeTile sMAttributeTile = mGridData.GetAttributeTile(x, y);
		if (sMAttributeTile == null)
		{
			sMAttributeTile = DEFAULT_ATTRIBUTE_TILE;
		}
		return sMAttributeTile;
	}

	public virtual void SetAttributeTile(int x, int y, SMAttributeTile tile)
	{
		mGridData.SetAttributeTile(x, y, tile);
	}

	public virtual void SetTileInfo(int x, int y, SMTileInfo info)
	{
		mGridData.SetTileInfo(x, y, info);
	}

	public virtual ISMTileInfo GetTileInfo(int x, int y)
	{
		SMTileInfo sMTileInfo = mGridData.GetTileInfo(x, y);
		if (sMTileInfo == null)
		{
			sMTileInfo = DEFAULT_TILE_INFO;
		}
		return sMTileInfo;
	}

	public virtual void Serialize(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteUTF("SMD");
		data.WriteInt(reqVersion);
		data.WriteInt(Revision);
		data.WriteByte((byte)Series);
		data.WriteInt(StageNumber);
		data.WriteInt(EventID);
		data.WriteInt(MainStageNumber);
		data.WriteInt(SubStageNumber);
		CandyKindNum = GetCandyNum();
		data.WriteInt(CandyKindNum);
		data.WriteBool(UseColor0);
		data.WriteBool(UseColor1);
		data.WriteBool(UseColor2);
		data.WriteBool(UseColor3);
		data.WriteBool(UseColor4);
		data.WriteBool(UseColor5);
		data.WriteBool(UseColor6);
		data.WriteUTF(StageType.ToString());
		data.WriteUTF(WinType.ToString());
		data.WriteUTF(LoseType.ToString());
		data.WriteInt(LimitMoves);
		data.WriteInt(LimitTimeSec);
		data.WriteInt(BronzeScore);
		data.WriteInt(SilverScore);
		data.WriteInt(GoldScore);
		data.WriteInt(Color0);
		data.WriteInt(Color1);
		data.WriteInt(Color2);
		data.WriteInt(Color3);
		data.WriteInt(Color4);
		data.WriteInt(Color5);
		data.WriteInt(Color6);
		data.WriteInt(Ingredient0);
		data.WriteInt(Ingredient1);
		data.WriteInt(Ingredient2);
		data.WriteInt(Ingredient3);
		data.WriteInt((int)BoosterSlot1);
		data.WriteInt((int)BoosterSlot2);
		data.WriteInt((int)BoosterSlot3);
		data.WriteInt((int)BoosterSlot4);
		data.WriteInt((int)BoosterSlot5);
		data.WriteInt((int)BoosterSlot6);
		data.WriteInt((int)InPuzzleBoosterSlot1);
		data.WriteInt((int)InPuzzleBoosterSlot2);
		data.WriteInt((int)InPuzzleBoosterSlot3);
		data.WriteFloat(BonusFreq1);
		data.WriteFloat(BonusMove1);
		data.WriteInt(BonusLimit1);
		data.WriteInt(BonusMoveLimit1);
		data.WriteInt(Wave);
		data.WriteInt(Bomb);
		data.WriteInt(Rainbow);
		data.WriteInt(WaveWave);
		data.WriteInt(BombBomb);
		data.WriteInt(RainbowRainbow);
		data.WriteInt(WaveBomb);
		data.WriteInt(BombRainbow);
		data.WriteInt(WaveRainbow);
		data.WriteUTF(BGM);
		data.WriteUTF(EventCode);
		data.WriteUTF(SegmentCode);
		data.WriteUTF(TestCode);
		data.WriteInt(Timer0);
		data.WriteInt(Timer1);
		data.WriteInt(Timer2);
		data.WriteInt(Timer3);
		data.WriteInt(Timer4);
		data.WriteInt(Timer5);
		data.WriteFloat(TimerFreq);
		data.WriteInt(TimerLimit);
		data.WriteInt(TimerRemainMin);
		data.WriteInt(TimerRemainMax);
		data.WriteInt(Pair0);
		data.WriteInt(PairLimit0);
		data.WriteFloat(JewelFreq);
		data.WriteInt(JewelLimit);
		data.WriteFloat(FCookie1Freq);
		data.WriteInt(FCookie1Limit);
		data.WriteFloat(FCookie2Freq);
		data.WriteInt(FCookie2Limit);
		data.WriteFloat(FCookie3Freq);
		data.WriteInt(FCookie3Limit);
		data.WriteShort((short)BossType);
		data.WriteInt(BossHP);
		data.WriteInt(BossPositionX);
		data.WriteInt(BossPositionY);
		data.WriteInt(AttackCandyNum);
		data.WriteBool(AttackUseCandy0);
		data.WriteBool(AttackUseCandy1);
		data.WriteBool(AttackUseCandy2);
		data.WriteBool(AttackUseCandy3);
		data.WriteBool(AttackUseCandy4);
		data.WriteBool(AttackUseCandy5);
		data.WriteBool(AttackUseCandy6);
		data.WriteInt(BossFirstAttack);
		data.WriteInt(BossAttackInterval);
		data.WriteInt(SilverCrystal);
		data.WriteInt(EnemyGhost);
		data.WriteInt(BossID);
		data.WriteShort((short)BossAttackPattern);
		data.WriteInt(BossAttackParam);
		data.WriteUTF(BossWeakChara);
		data.WriteInt(BossAttackParam2);
		data.WriteInt(BossAttackParam3);
		data.WriteFloat(FOrb1Freq);
		data.WriteInt(FOrb1Limit);
		data.WriteFloat(FOrb2Freq);
		data.WriteInt(FOrb2Limit);
		data.WriteFloat(FOrb3Freq);
		data.WriteInt(FOrb3Limit);
		data.WriteFloat(FOrb4Freq);
		data.WriteInt(FOrb4Limit);
		data.WriteFloat(FootPieceFreq1);
		data.WriteInt(FootPieceLimit1);
		data.WriteFloat(FJewelFreq1);
		data.WriteInt(FJewel1Limit1);
		data.WriteInt(FJewel3Limit1);
		data.WriteInt(FJewel5Limit1);
		data.WriteInt(FJewel9Limit1);
		data.WriteFloat(FPerl00Freq1);
		data.WriteInt(FPerl00Limit1);
		data.WriteFloat(FPerl01Freq1);
		data.WriteInt(FPerl01Limit1);
		data.WriteFloat(FPerl02Freq1);
		data.WriteInt(FPerl02Limit1);
		data.WriteInt(Width);
		data.WriteInt(Height);
		data.WriteInt(PlayAreaX);
		data.WriteInt(PlayAreaY);
		data.WriteInt(PlayAreaWidth);
		data.WriteInt(PlayAreaHeight);
		ArrayFn(delegate(int x, int y)
		{
			SMTile sMTile = GetTile(x, y) as SMTile;
			data.WriteInt(x);
			data.WriteInt(y);
			if (sMTile == null)
			{
				data.WriteBool(false);
			}
			else
			{
				data.WriteBool(true);
				sMTile.Serialize(ref data, reqVersion);
			}
		});
		ArrayFn(delegate(int x, int y)
		{
			SMAttributeTile sMAttributeTile = GetAttributeTile(x, y) as SMAttributeTile;
			data.WriteInt(x);
			data.WriteInt(y);
			if (sMAttributeTile == null)
			{
				data.WriteBool(false);
			}
			else
			{
				data.WriteBool(true);
				sMAttributeTile.Serialize(ref data, reqVersion);
			}
		});
		ArrayFn(delegate(int x, int y)
		{
			SMTileInfo a_info = GetTileInfo(x, y) as SMTileInfo;
			data.WriteInt(x);
			data.WriteInt(y);
			if (a_info == null)
			{
				data.WriteBool(false);
			}
			else if (CheckSerializeTileInfo(x, y, ref a_info))
			{
				data.WriteBool(true);
				a_info.Serialize(ref data, reqVersion);
			}
			else
			{
				data.WriteBool(false);
			}
		});
		OnSaved();
	}

	public virtual void Deserialize(BIJBinaryReader data, int reqVersion)
	{
		string text = data.ReadUTF();
		if ("SMD".CompareTo(text) != 0)
		{
			throw new IOException("File signature is invalid !!! CORRECT=SMD LOAD=" + text);
		}
		int num = data.ReadInt();
		FormatVersion = reqVersion;
		OriginalFormatVersion = num;
		Revision = data.ReadInt();
		if (num >= 10030)
		{
			Series = (Def.SERIES)data.ReadByte();
		}
		else
		{
			Series = Def.SERIES.SM_FIRST;
		}
		StageNumber = data.ReadInt();
		if (num >= 10100)
		{
			EventID = data.ReadInt();
		}
		else if (Series == Def.SERIES.SM_EV || Series == Def.SERIES.SM_ADV)
		{
			EventID = 0;
		}
		else
		{
			EventID = -1;
		}
		DisplayStageNumber = StageNumber.ToString();
		if (num >= 10020)
		{
			MainStageNumber = data.ReadInt();
			SubStageNumber = data.ReadInt();
		}
		else
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(StageNumber, out a_main, out a_sub);
			MainStageNumber = a_main;
			SubStageNumber = a_sub;
		}
		StageNumber = Def.GetStageNo(MainStageNumber, SubStageNumber);
		DisplayStageNumber = Def.GetDisplayStageNo(MainStageNumber, SubStageNumber);
		CandyKindNum = data.ReadInt();
		if (num >= 10010)
		{
			UseColor0 = data.ReadBool();
			UseColor1 = data.ReadBool();
			UseColor2 = data.ReadBool();
			UseColor3 = data.ReadBool();
			UseColor4 = data.ReadBool();
			UseColor5 = data.ReadBool();
			UseColor6 = data.ReadBool();
			CandyKindNum = GetCandyNum();
		}
		else
		{
			UseColor0 = false;
			UseColor1 = false;
			UseColor2 = false;
			UseColor3 = false;
			UseColor4 = false;
			UseColor5 = false;
			UseColor6 = false;
			switch (CandyKindNum)
			{
			case 6:
				UseColor5 = true;
				goto case 5;
			case 5:
				UseColor4 = true;
				goto case 4;
			case 4:
				UseColor3 = true;
				goto case 3;
			case 3:
				UseColor2 = true;
				goto case 2;
			case 2:
				UseColor1 = true;
				goto case 1;
			case 1:
				UseColor0 = true;
				break;
			}
		}
		string strB = data.ReadUTF();
		Def.STAGE_TYPE[] array = (Def.STAGE_TYPE[])Enum.GetValues(typeof(Def.STAGE_TYPE));
		Def.STAGE_TYPE stageType = Def.STAGE_TYPE.NONE;
		for (int i = 0; i < array.Length; i++)
		{
			if (string.Compare(array[i].ToString(), strB) == 0)
			{
				stageType = array[i];
				break;
			}
		}
		StageType = stageType;
		if (num >= 10010)
		{
			string strB2 = data.ReadUTF();
			string strB3 = data.ReadUTF();
			Def.STAGE_WIN_CONDITION[] array2 = (Def.STAGE_WIN_CONDITION[])Enum.GetValues(typeof(Def.STAGE_WIN_CONDITION));
			Def.STAGE_WIN_CONDITION winType = Def.STAGE_WIN_CONDITION.NONE;
			for (int j = 0; j < array2.Length; j++)
			{
				if (string.Compare(array2[j].ToString(), strB2) == 0)
				{
					winType = array2[j];
					break;
				}
			}
			WinType = winType;
			Def.STAGE_LOSE_CONDITION[] array3 = (Def.STAGE_LOSE_CONDITION[])Enum.GetValues(typeof(Def.STAGE_LOSE_CONDITION));
			Def.STAGE_LOSE_CONDITION loseType = Def.STAGE_LOSE_CONDITION.NONE;
			for (int k = 0; k < array3.Length; k++)
			{
				if (string.Compare(array3[k].ToString(), strB3) == 0)
				{
					loseType = array3[k];
					break;
				}
			}
			LoseType = loseType;
		}
		else
		{
			WinType = StageType.ToWin();
			LoseType = StageType.ToLose();
		}
		LimitMoves = data.ReadInt();
		LimitTimeSec = data.ReadInt();
		BronzeScore = data.ReadInt();
		SilverScore = data.ReadInt();
		GoldScore = data.ReadInt();
		Color0 = data.ReadInt();
		Color1 = data.ReadInt();
		Color2 = data.ReadInt();
		Color3 = data.ReadInt();
		Color4 = data.ReadInt();
		Color5 = data.ReadInt();
		Color6 = data.ReadInt();
		Ingredient0 = data.ReadInt();
		Ingredient1 = data.ReadInt();
		Ingredient2 = data.ReadInt();
		Ingredient3 = data.ReadInt();
		BoosterSlot1 = (Def.BOOSTER_KIND)data.ReadInt();
		BoosterSlot2 = (Def.BOOSTER_KIND)data.ReadInt();
		BoosterSlot3 = (Def.BOOSTER_KIND)data.ReadInt();
		if (num >= 10060)
		{
			BoosterSlot4 = (Def.BOOSTER_KIND)data.ReadInt();
			BoosterSlot5 = (Def.BOOSTER_KIND)data.ReadInt();
			BoosterSlot6 = (Def.BOOSTER_KIND)data.ReadInt();
		}
		else
		{
			BoosterSlot4 = Def.BOOSTER_KIND.NONE;
			BoosterSlot5 = Def.BOOSTER_KIND.NONE;
			BoosterSlot6 = Def.BOOSTER_KIND.NONE;
		}
		if (num >= 10070)
		{
			InPuzzleBoosterSlot1 = (Def.BOOSTER_KIND)data.ReadInt();
			InPuzzleBoosterSlot2 = (Def.BOOSTER_KIND)data.ReadInt();
			InPuzzleBoosterSlot3 = (Def.BOOSTER_KIND)data.ReadInt();
		}
		else
		{
			InPuzzleBoosterSlot1 = Def.BOOSTER_KIND.SELECT_ONE;
			InPuzzleBoosterSlot2 = Def.BOOSTER_KIND.SELECT_COLLECT;
			InPuzzleBoosterSlot3 = Def.BOOSTER_KIND.COLOR_CRUSH;
		}
		BonusFreq1 = data.ReadFloat();
		BonusMove1 = data.ReadFloat();
		BonusLimit1 = data.ReadInt();
		BonusMoveLimit1 = data.ReadInt();
		Wave = data.ReadInt();
		Bomb = data.ReadInt();
		Rainbow = data.ReadInt();
		WaveWave = data.ReadInt();
		BombBomb = data.ReadInt();
		RainbowRainbow = data.ReadInt();
		WaveBomb = data.ReadInt();
		BombRainbow = data.ReadInt();
		WaveRainbow = data.ReadInt();
		BGM = data.ReadUTF();
		EventCode = data.ReadUTF();
		SegmentCode = data.ReadUTF();
		TestCode = data.ReadUTF();
		Timer0 = data.ReadInt();
		Timer1 = data.ReadInt();
		Timer2 = data.ReadInt();
		Timer3 = data.ReadInt();
		Timer4 = data.ReadInt();
		Timer5 = data.ReadInt();
		TimerFreq = data.ReadFloat();
		TimerLimit = data.ReadInt();
		TimerRemainMin = data.ReadInt();
		TimerRemainMax = data.ReadInt();
		Pair0 = data.ReadInt();
		PairLimit0 = data.ReadInt();
		JewelFreq = data.ReadFloat();
		JewelLimit = data.ReadInt();
		FCookie1Freq = data.ReadFloat();
		FCookie1Limit = data.ReadInt();
		FCookie2Freq = data.ReadFloat();
		FCookie2Limit = data.ReadInt();
		FCookie3Freq = data.ReadFloat();
		FCookie3Limit = data.ReadInt();
		if (num >= 10040)
		{
			BossType = (Def.TILE_KIND)data.ReadShort();
			BossHP = data.ReadInt();
			BossPositionX = data.ReadInt();
			BossPositionY = data.ReadInt();
			AttackCandyNum = data.ReadInt();
			AttackUseCandy0 = data.ReadBool();
			AttackUseCandy1 = data.ReadBool();
			AttackUseCandy2 = data.ReadBool();
			AttackUseCandy3 = data.ReadBool();
			AttackUseCandy4 = data.ReadBool();
			AttackUseCandy5 = data.ReadBool();
			AttackUseCandy6 = data.ReadBool();
			BossFirstAttack = data.ReadInt();
			BossAttackInterval = data.ReadInt();
			mSilverCrystal = data.ReadInt();
			EnemyGhost = data.ReadInt();
		}
		else
		{
			BossType = Def.TILE_KIND.NONE;
			BossPositionX = 20;
			BossPositionY = 20;
			BossHP = 0;
			AttackCandyNum = 0;
			AttackUseCandy0 = false;
			AttackUseCandy1 = false;
			AttackUseCandy2 = false;
			AttackUseCandy3 = false;
			AttackUseCandy4 = false;
			AttackUseCandy5 = false;
			AttackUseCandy6 = false;
			BossFirstAttack = 0;
			BossAttackInterval = 0;
			mSilverCrystal = 0;
			EnemyGhost = 0;
		}
		if (num >= 10050)
		{
			BossID = data.ReadInt();
			BossAttackPattern = (Def.BOSS_ATTACK)data.ReadShort();
			BossAttackParam = data.ReadInt();
			BossWeakChara = data.ReadUTF();
		}
		else
		{
			BossID = 0;
			BossAttackPattern = Def.BOSS_ATTACK.NONE;
			BossAttackParam = 0;
			BossWeakChara = string.Empty;
		}
		if (num >= 10110)
		{
			BossAttackParam2 = data.ReadInt();
			BossAttackParam3 = data.ReadInt();
		}
		else
		{
			BossAttackParam2 = 0;
			BossAttackParam3 = 0;
		}
		if (num >= 10130)
		{
			FOrb1Freq = data.ReadFloat();
			FOrb1Limit = data.ReadInt();
			FOrb2Freq = data.ReadFloat();
			FOrb2Limit = data.ReadInt();
			FOrb3Freq = data.ReadFloat();
			FOrb3Limit = data.ReadInt();
			FOrb4Freq = data.ReadFloat();
			FOrb4Limit = data.ReadInt();
		}
		else
		{
			FOrb1Freq = 0f;
			FOrb1Limit = 0;
			FOrb2Freq = 0f;
			FOrb2Limit = 0;
			FOrb3Freq = 0f;
			FOrb3Limit = 0;
			FOrb4Freq = 0f;
			FOrb4Limit = 0;
		}
		if (num >= 10140)
		{
			FootPieceFreq1 = data.ReadFloat();
			FootPieceLimit1 = data.ReadInt();
		}
		else
		{
			FootPieceFreq1 = 0f;
			FootPieceLimit1 = 0;
		}
		if (num >= 10150)
		{
			FJewelFreq1 = data.ReadFloat();
			FJewel1Limit1 = data.ReadInt();
			FJewel3Limit1 = data.ReadInt();
			FJewel5Limit1 = data.ReadInt();
			FJewel9Limit1 = data.ReadInt();
			FPerl00Freq1 = data.ReadFloat();
			FPerl00Limit1 = data.ReadInt();
			FPerl01Freq1 = data.ReadFloat();
			FPerl01Limit1 = data.ReadInt();
			FPerl02Freq1 = data.ReadFloat();
			FPerl02Limit1 = data.ReadInt();
		}
		else
		{
			FJewelFreq1 = 0f;
			FJewel1Limit1 = 0;
			FJewel3Limit1 = 0;
			FJewel5Limit1 = 0;
			FJewel9Limit1 = 0;
			FPerl00Freq1 = 0f;
			FPerl00Limit1 = 0;
			FPerl01Freq1 = 0f;
			FPerl01Limit1 = 0;
			FPerl02Freq1 = 0f;
			FPerl02Limit1 = 0;
		}
		int w = data.ReadInt();
		int h = data.ReadInt();
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		bool flag = false;
		if (num >= 10010)
		{
			num2 = data.ReadInt();
			num3 = data.ReadInt();
			num4 = data.ReadInt();
			num5 = data.ReadInt();
			PlayAreaX = num2;
			PlayAreaY = num3;
			PlayAreaWidth = num4;
			PlayAreaHeight = num5;
		}
		else
		{
			flag = true;
		}
		mGridData = new SMGridData(w, h);
		int savedVersion = num;
		ArrayFn(delegate(int x, int y)
		{
			int num10 = data.ReadInt();
			int num11 = data.ReadInt();
			if (data.ReadBool())
			{
				SMTile sMTile = new SMTile();
				sMTile.Deserialize(ref data, savedVersion);
				SetTile(x, y, sMTile);
			}
		});
		ArrayFn(delegate(int x, int y)
		{
			int num8 = data.ReadInt();
			int num9 = data.ReadInt();
			if (data.ReadBool())
			{
				SMAttributeTile sMAttributeTile = new SMAttributeTile();
				sMAttributeTile.Deserialize(ref data, savedVersion);
				SetAttributeTile(x, y, sMAttributeTile);
			}
		});
		ArrayFn(delegate(int x, int y)
		{
			int num6 = data.ReadInt();
			int num7 = data.ReadInt();
			if (data.ReadBool())
			{
				SMTileInfo sMTileInfo = new SMTileInfo();
				sMTileInfo.Deserialize(ref data, savedVersion);
				SetTileInfo(x, y, sMTileInfo);
			}
		});
		if (flag)
		{
			CalcPlayArea();
		}
		OnLoaded();
	}

	private bool CheckSerializeTileInfo(int x, int y, ref SMTileInfo a_info)
	{
		return true;
	}

	public void CalcPlayArea()
	{
		int minx = 1000;
		int miny = 1000;
		int maxx = -1;
		int maxy = -1;
		ArrayFn(delegate(int x, int y)
		{
			SMAttributeTile attributeTile = mGridData.GetAttributeTile(x, y);
			if (attributeTile != null)
			{
				if (y < miny)
				{
					miny = y;
				}
				if (x < minx)
				{
					minx = x;
				}
				if (x >= maxx)
				{
					maxx = x;
				}
				if (y >= maxy)
				{
					maxy = y;
				}
			}
		});
		int playAreaWidth = maxx - minx + 1;
		int playAreaHeight = maxy - miny + 1;
		PlayAreaX = minx;
		PlayAreaY = miny;
		PlayAreaWidth = playAreaWidth;
		PlayAreaHeight = playAreaHeight;
		if (PlayAreaWidth > Width)
		{
			PlayAreaWidth = 0;
		}
		if (PlayAreaHeight > Height)
		{
			PlayAreaHeight = 0;
		}
	}

	private int GetCandyNum()
	{
		int num = 0;
		if (UseColor0)
		{
			num++;
		}
		if (UseColor1)
		{
			num++;
		}
		if (UseColor2)
		{
			num++;
		}
		if (UseColor3)
		{
			num++;
		}
		if (UseColor4)
		{
			num++;
		}
		if (UseColor5)
		{
			num++;
		}
		if (UseColor6)
		{
			num++;
		}
		return num;
	}

	public int GetTargetNum(Def.TILE_KIND a_kind)
	{
		return 0;
	}

	public Def.TILE_KIND[] GetNormaKindArray()
	{
		return new Def.TILE_KIND[1] { Def.TILE_KIND.NONE };
	}

	public bool CalcDianaWay(out DianaWayError a_error)
	{
		a_error = DianaWayError.NONE;
		DianaWayPosList.Clear();
		bool found = false;
		List<IntVector2> startList = new List<IntVector2>();
		ArrayFn(delegate(int x, int y)
		{
			SMTile tile = mGridData.GetTile(x, y);
			if (tile != null && tile.Kind == Def.TILE_KIND.DIANA)
			{
				startList.Add(new IntVector2(x, y));
				found = true;
			}
		});
		if (found && startList.Count == 1)
		{
			DianaWayPosList.Add(startList[0]);
			SMTileInfo tileInfo = mGridData.GetTileInfo(startList[0].x, startList[0].y);
			if (tileInfo != null && tileInfo.DianaWayKindNum > 0)
			{
				a_error = DianaWayError.FOUND_WAY_ON_START;
				return false;
			}
			found = false;
			Def.TILE_KIND goalKind = Def.TILE_KIND.NONE;
			List<IntVector2> goalList = new List<IntVector2>();
			ArrayFn(delegate(int x, int y)
			{
				SMAttributeTile attributeTile = mGridData.GetAttributeTile(x, y);
				if (attributeTile != null && (attributeTile.Kind == Def.TILE_KIND.DIANA_GOAL_L || attributeTile.Kind == Def.TILE_KIND.DIANA_GOAL_R))
				{
					goalKind = attributeTile.Kind;
					goalList.Add(new IntVector2(x, y));
					found = true;
				}
			});
			IntVector2 intVector = null;
			if (found && goalList.Count == 1)
			{
				intVector = goalList[0];
				SMTileInfo tileInfo2 = mGridData.GetTileInfo(intVector.x, intVector.y);
				if (tileInfo2 != null && tileInfo2.DianaWayKindNum > 0)
				{
					a_error = DianaWayError.FOUND_WAY_ON_GOAL;
					return false;
				}
				found = false;
				SMTileInfo sMTileInfo = null;
				int i = 0;
				int num = 0;
				int num2 = 0;
				bool flag = false;
				for (; i < SearchTile_x.Length; i++)
				{
					num = startList[0].x + SearchTile_x[i];
					num2 = startList[0].y + SearchTile_y[i];
					SMTileInfo tileInfo3 = mGridData.GetTileInfo(num, num2);
					if (tileInfo3 != null && tileInfo3.DianaWayKindNum > 0)
					{
						if (sMTileInfo == null)
						{
							sMTileInfo = tileInfo3;
							DianaWayPosList.Add(new IntVector2(num, num2));
							found = true;
						}
						else
						{
							flag = true;
						}
					}
				}
				if (flag)
				{
					a_error = DianaWayError.MULTI_STARTWAY;
					return false;
				}
				if (!found)
				{
					a_error = DianaWayError.NO_WAY;
					return false;
				}
				num = DianaWayPosList[DianaWayPosList.Count - 1].x;
				num2 = DianaWayPosList[DianaWayPosList.Count - 1].y;
				found = false;
				bool flag2 = false;
				DianaWayError dianaWayError = DianaWayError.NONE;
				Def.DIR dIR = Def.DIR.NONE;
				while (!flag2)
				{
					SMTileInfo sMTileInfo2 = null;
					switch ((SMTileInfo.DianaWayKind)sMTileInfo.DianaWayKindNum)
					{
					case SMTileInfo.DianaWayKind.UP:
						num2++;
						dIR = Def.DIR.UP;
						break;
					case SMTileInfo.DianaWayKind.DOWN:
						num2 += -1;
						dIR = Def.DIR.DOWN;
						break;
					case SMTileInfo.DianaWayKind.RIGHT:
						num++;
						dIR = Def.DIR.RIGHT;
						break;
					case SMTileInfo.DianaWayKind.LEFT:
						num += -1;
						dIR = Def.DIR.LEFT;
						break;
					case SMTileInfo.DianaWayKind.UP_RIGHT:
					case SMTileInfo.DianaWayKind.DOWN_RIGHT:
					case SMTileInfo.DianaWayKind.UP_LEFT:
					case SMTileInfo.DianaWayKind.DOWN_LEFT:
					{
						IntVector2 intVector2 = DianaWayPosList[DianaWayPosList.Count - 1];
						IntVector2 intVector3 = DianaWayPosList[DianaWayPosList.Count - 2];
						IntVector2 intVector4 = intVector2 - intVector3;
						if (intVector4.x == 0 && intVector4.y == 1)
						{
							num2++;
							dIR = Def.DIR.UP;
						}
						else if (intVector4.x == 0 && intVector4.y == -1)
						{
							num2 += -1;
							dIR = Def.DIR.DOWN;
						}
						else if (intVector4.x == 1 && intVector4.y == 0)
						{
							num++;
							dIR = Def.DIR.RIGHT;
						}
						else if (intVector4.x == -1 && intVector4.y == 0)
						{
							num += -1;
							dIR = Def.DIR.LEFT;
						}
						else
						{
							flag2 = false;
						}
						break;
					}
					}
					if (num == intVector.x && num2 == intVector.y)
					{
						if (goalKind == Def.TILE_KIND.DIANA_GOAL_L)
						{
							if (dIR == Def.DIR.UP || dIR == Def.DIR.RIGHT)
							{
								flag2 = true;
							}
						}
						else if (goalKind == Def.TILE_KIND.DIANA_GOAL_R && (dIR == Def.DIR.UP || dIR == Def.DIR.LEFT))
						{
							flag2 = true;
						}
						if (!flag2)
						{
							a_error = DianaWayError.GOAL_DIRECTION_ERROR;
							return false;
						}
					}
					else if (num == startList[0].x && num2 == startList[0].y)
					{
						dianaWayError = DianaWayError.FOUND_WAY_ON_START;
					}
					else
					{
						sMTileInfo2 = mGridData.GetTileInfo(num, num2);
						if (sMTileInfo2 != null && sMTileInfo2.DianaWayKindNum > 0)
						{
							sMTileInfo = sMTileInfo2;
							DianaWayPosList.Add(new IntVector2(num, num2));
						}
					}
					if (!flag2 && sMTileInfo2 == null)
					{
						dianaWayError = DianaWayError.NOT_FOUND_NEXT_TILEINFO;
						break;
					}
				}
				if (flag2)
				{
					DianaWayPosList.Add(intVector);
					return true;
				}
				if (dianaWayError == DianaWayError.NONE)
				{
					a_error = DianaWayError.NO_REACH_GOAL;
				}
				else
				{
					a_error = dianaWayError;
				}
				return false;
			}
			if (!found)
			{
				a_error = DianaWayError.NO_GOAL;
			}
			else if (goalList.Count > 1)
			{
				a_error = DianaWayError.MULTI_GOAL;
			}
			return false;
		}
		if (!found)
		{
			a_error = DianaWayError.NO_START;
		}
		else if (startList.Count > 1)
		{
			a_error = DianaWayError.MULTI_START;
		}
		return false;
	}

	public virtual void SetJewelSetting(float a_freq, int a_limit1, int a_limit3, int a_limit5, int a_limit9)
	{
		FJewelFreq1 = a_freq;
		FJewel1Limit1 = a_limit1;
		FJewel3Limit1 = a_limit3;
		FJewel5Limit1 = a_limit5;
		FJewel9Limit1 = a_limit9;
	}
}
