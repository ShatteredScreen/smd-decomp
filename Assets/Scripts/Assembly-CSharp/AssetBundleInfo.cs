using System.Collections.Generic;

public class AssetBundleInfo
{
	public string Name { get; set; }

	public List<string> AssetList { get; set; }

	public List<string> SceneList { get; set; }

	public string AssetBundleName
	{
		get
		{
			string name = Name;
			if (string.IsNullOrEmpty(name))
			{
				return string.Empty;
			}
			return name;
		}
	}

	public AssetBundleInfo()
	{
		AssetList = new List<string>();
		SceneList = new List<string>();
	}

	public void Deserialize(BIJBinaryReader aReader)
	{
		Name = aReader.ReadUTF();
		AssetList.Clear();
		int num = aReader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			string item = aReader.ReadUTF();
			AssetList.Add(item);
		}
		SceneList.Clear();
		num = aReader.ReadInt();
		for (int j = 0; j < num; j++)
		{
			string item2 = aReader.ReadUTF();
			SceneList.Add(item2);
		}
	}

	public bool IsInAssetBundle(string a_name)
	{
		for (int i = 0; i < AssetList.Count; i++)
		{
			string text = AssetList[i];
			int num = text.IndexOf(a_name);
			if (num >= 0)
			{
				return true;
			}
		}
		return false;
	}

	public bool HasSceneName(string a_scene)
	{
		bool result = false;
		for (int i = 0; i < SceneList.Count; i++)
		{
			string text = SceneList[i];
			if (a_scene == text)
			{
				result = true;
				break;
			}
		}
		return result;
	}
}
