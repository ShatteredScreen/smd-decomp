public sealed class CharUtil
{
	public static bool IsHiragana(char c)
	{
		return ('ぁ' <= c && c <= 'ゟ') || c == 'ー' || c == '゠';
	}

	public static bool IsFullwidthKatakana(char c)
	{
		return ('゠' <= c && c <= 'ヿ') || ('ㇰ' <= c && c <= 'ㇿ') || ('\u3099' <= c && c <= '\u309c');
	}

	public static bool IsHalfwidthKatakana(char c)
	{
		return '･' <= c && c <= 'ﾟ';
	}

	public static bool IsKatakana(char c)
	{
		return IsFullwidthKatakana(c) || IsHalfwidthKatakana(c);
	}

	public static bool IsKanji(char c)
	{
		return ('一' <= c && c <= '鿏') || ('豈' <= c && c <= '\ufaff') || ('㐀' <= c && c <= '䶿');
	}

	public static bool IsDigit(char c)
	{
		return char.IsDigit(c);
	}

	public static bool IsNumber(char c)
	{
		return char.IsNumber(c);
	}

	public static bool IsLatinLetter(char c)
	{
		return char.IsUpper(c) || char.IsLower(c);
	}

	public static bool IsLetterlikeSymbol(char c)
	{
		return '℀' <= c && c <= '⅏';
	}

	public static bool IsAsciiPrintable(char c)
	{
		return '\0' <= c && c <= '\u007f' && !char.IsControl(c);
	}
}
