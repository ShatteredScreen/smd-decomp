using System;
using System.IO;

public class ConnectAdvGetMail : ConnectBase
{
	private Stream mResStream;

	private bool mIsSetResponseData;

	public void SetResponseData(Stream a_stream)
	{
		mResStream = a_stream;
		mIsSetResponseData = true;
	}

	protected override void InitServerFlg()
	{
		GameMain.mAdvGetMailCheckDone = false;
		GameMain.mAdvGetMailSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mAdvGetMailCheckDone = true;
		GameMain.mAdvGetMailSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mAdvGetMailCheckDone = true;
		GameMain.mAdvGetMailSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		bool flag = false;
		if (mParam != null)
		{
			ConnectAdvGetMailParam connectAdvGetMailParam = mParam as ConnectAdvGetMailParam;
			if (connectAdvGetMailParam != null)
			{
				flag = connectAdvGetMailParam.IsForce;
			}
		}
		if (!flag)
		{
			long num = DateTimeUtil.BetweenMinutes(mGame.LastAdvGetMail, DateTime.Now);
			long gET_SUPPORTPURCHASE_FREQ = GameMain.GET_SUPPORTPURCHASE_FREQ;
			if (gET_SUPPORTPURCHASE_FREQ > 0 && num < gET_SUPPORTPURCHASE_FREQ)
			{
				ServerFlgSucceeded();
				return false;
			}
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			base.StateStart();
			return;
		}
		MakeConnectGUID();
		if (!Server.AdvGetMail(mConnectGUID, (base.RetryCount > 0) ? 1 : 0))
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		if (mIsSetResponseData)
		{
			mGame.SetAdvGetMailFromResponse();
		}
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
			mConnectResultKind = 3;
			base.StateConnectFinish();
			return false;
		}
		return false;
	}
}
