using UnityEngine;

public class RoadBlockNoKeyDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("RoadBlockStar_NgTitle");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("RoadBlockStar_NgDesc"), mBaseDepth + 4, new Vector3(0f, 77f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		UISprite sprite = Util.CreateSprite("chara_tibiusa", base.gameObject, image);
		Util.SetSpriteInfo(sprite, "chara_key00", mBaseDepth + 8, new Vector3(159f, -48f), Vector3.one, false);
		sprite = Util.CreateSprite("key", base.gameObject, image);
		Util.SetSpriteInfo(sprite, "chara_key01", mBaseDepth + 8, new Vector3(20f, 27f), Vector3.one, false);
		UIButton button = Util.CreateJellyImageButton("closebutton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 3, new Vector3(0f, -112f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Dialogclosed()
	{
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
