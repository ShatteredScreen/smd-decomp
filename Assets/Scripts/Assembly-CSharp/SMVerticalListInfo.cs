public struct SMVerticalListInfo
{
	public int landscape_maxPerLine;

	public float landscape_width;

	public float landscape_height;

	public int portrait_maxPerLine;

	public float portrait_width;

	public float portrait_height;

	public int scrollSpeed;

	public int itemHeight;

	public int maxDisplayCount;

	public SMVerticalListInfo(int lMaxPerLine, float lWidth, float lHeight, int pMaxPerLine, float pWidth, float pHeight, int _scrollSpeed, int _itemHeight, int _maxDisplayCount = 0)
	{
		landscape_maxPerLine = lMaxPerLine;
		landscape_width = lWidth;
		landscape_height = lHeight;
		portrait_maxPerLine = pMaxPerLine;
		portrait_width = pWidth;
		portrait_height = pHeight;
		scrollSpeed = _scrollSpeed;
		itemHeight = _itemHeight;
		maxDisplayCount = _maxDisplayCount;
	}
}
