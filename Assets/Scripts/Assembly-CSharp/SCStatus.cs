public class SCStatus : ParameterObject<SCStatus>
{
	public int id { get; set; }

	public string message { get; set; }

	public int category { get; set; }

	public int sub_category { get; set; }

	public int itemid { get; set; }

	public int quantity { get; set; }

	public int mail_type { get; set; }
}
