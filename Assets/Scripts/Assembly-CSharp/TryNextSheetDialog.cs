using UnityEngine;

public class TryNextSheetDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	private class RewardSpriteData
	{
		public string mImgName;

		public int mNum;

		public RewardSpriteData()
		{
			mImgName = string.Empty;
			mNum = 0;
		}
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private Vector2 mLogScreen = Util.LogScreenSize();

	private bool mHasNextSheet;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		mLogScreen = Util.LogScreenSize();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public void Init(bool a_nextsheet)
	{
		mHasNextSheet = a_nextsheet;
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		BIJImage image2 = ResourceManager.LoadImage("EVENTBINGO").Image;
		string titleDescKey = Localization.Get("BINGO_SheetComp_Title");
		string text = Localization.Get("BINGO_SheetComp_Desc1");
		if (mHasNextSheet)
		{
			text = text + "\n" + Localization.Get("BINGO_SheetComp_Desc2");
		}
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UISprite sprite = Util.CreateSprite("CongratulationImg", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "chara_bng00", mBaseDepth + 2, new Vector3(0f, 75f, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("Text", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(0f, -130f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(400, 100);
		uILabel.spacingY = 8;
		UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -220f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		mState.Reset(STATE.WAIT, true);
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
