public class UserEdit : ParameterObject<UserEdit>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("nickname")]
	public string NickName { get; set; }

	[MiniJSONAlias("iconid")]
	public int IconID { get; set; }

	[MiniJSONAlias("iconlvl")]
	public int IconLevel { get; set; }

	[MiniJSONAlias("emoid")]
	public int EmoticonID { get; set; }

	public UserEdit()
	{
		NickName = string.Empty;
	}
}
