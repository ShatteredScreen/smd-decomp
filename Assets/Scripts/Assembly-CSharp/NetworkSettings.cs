public static class NetworkSettings
{
	public const bool USE_LOG = true;

	public const bool DEBUG_MODE = false;

	public const bool REACHABILITY_CHECK = true;

	public const bool UUID_CHECK = true;

	public const bool DISABLE_CRAM_SEND = false;

	public const bool USE_LOG_CRAM = false;

	public const bool DEBUG_CRAM = false;

	public const bool DISABLE_SERVER_SEND = false;

	public const bool USE_LOG_SERVER = true;

	public const bool DEBUG_SERVER = false;

	public const bool DISABLE_IAP_SEND = false;

	public const bool USE_LOG_IAP = true;

	public const bool DEBUG_IAP = false;
}
