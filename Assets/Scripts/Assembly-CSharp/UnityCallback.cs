using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class UnityCallback
{
	public delegate void Callback(GameObject gameObject, object userdata);

	private Callback mOnCallback;

	[method: MethodImpl(32)]
	private event Callback mOnCallbackEvent;

	public void Set(Callback cb)
	{
		mOnCallback = cb;
	}

	public void Add(Callback cb)
	{
		this.mOnCallbackEvent = (Callback)Delegate.Combine(this.mOnCallbackEvent, cb);
	}

	public void Remove(Callback cb)
	{
		this.mOnCallbackEvent = (Callback)Delegate.Remove(this.mOnCallbackEvent, cb);
	}

	public void Clear()
	{
		mOnCallback = null;
		this.mOnCallbackEvent = null;
	}

	public void Invoke(GameObject gameObject, object userdata = null)
	{
		if (mOnCallback != null)
		{
			mOnCallback(gameObject, userdata);
		}
		else if (this.mOnCallbackEvent != null)
		{
			this.mOnCallbackEvent(gameObject, userdata);
		}
	}
}
