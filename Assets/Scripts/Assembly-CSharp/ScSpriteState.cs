using UnityEngine;

[RequireComponent(typeof(SsSprite))]
public class ScSpriteState : MonoBehaviour
{
	public enum ClippingState
	{
		NONE = 0,
		FIXED = 1,
		DYNAMIC = 2
	}

	public SsSprite _sprite;

	protected ClippingState _clippingState;

	protected bool _clippingIntersects = true;

	private Vector4 _clippingRect = Vector4.zero;

	public ClippingState clippingState
	{
		get
		{
			return _clippingState;
		}
		set
		{
			_clippingState = value;
		}
	}

	public virtual void Awake()
	{
		_sprite = GetComponent<SsSprite>();
	}

	public virtual void Start()
	{
		BIJZoomCamera.UpdateClippingEvent += UpdateClippingRect;
	}

	public virtual void OnDestroy()
	{
		BIJZoomCamera.UpdateClippingEvent -= UpdateClippingRect;
	}

	public virtual void Update()
	{
		if (clippingState == ClippingState.DYNAMIC)
		{
			DoUpdateClippingRect(_clippingRect);
		}
	}

	private bool IntersectsRect(Vector4 r2)
	{
		return !(r2.x > _sprite.bounds.max.x) && !(r2.z < _sprite.bounds.min.x) && !(r2.y > _sprite.bounds.max.y) && !(r2.w < _sprite.bounds.min.y);
	}

	public virtual void UpdateClippingRect(Camera camera, Vector4 rect)
	{
		if (clippingState != 0 && !(_sprite == null) && 0 == 0)
		{
			_clippingRect = rect;
			DoUpdateClippingRect(rect);
		}
	}

	protected void DoUpdateClippingRect(Vector4 rect)
	{
		if (rect.x == 0f && rect.y == 0f && rect.z == 0f && rect.w == 0f)
		{
			return;
		}
		bool flag = IntersectsRect(rect);
		if (flag != _clippingIntersects)
		{
			_clippingIntersects = flag;
			if (_clippingIntersects)
			{
				DoBecameVisible();
			}
			else
			{
				DoBecameInvisible();
			}
		}
	}

	protected virtual void DoBecameVisible()
	{
		if (clippingState != 0)
		{
			_sprite.DoBecameVisible();
		}
	}

	protected virtual void DoBecameInvisible()
	{
		if (clippingState != 0)
		{
			_sprite.DoBecameInvisible();
		}
	}
}
