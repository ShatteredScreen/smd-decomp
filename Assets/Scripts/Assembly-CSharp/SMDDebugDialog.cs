public class SMDDebugDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0,
		CANCEL = 1,
		PUSH_BACKKEY = 2
	}

	public enum PAGE
	{
		BOOK = 0
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	public void SetGameState(GameState a_state)
	{
	}

	public void SetInitializePage(PAGE a_page)
	{
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
	}
}
