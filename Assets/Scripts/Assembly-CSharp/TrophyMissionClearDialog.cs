using System;
using UnityEngine;

public class TrophyMissionClearDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private OnDialogClosed mCallback;

	private float mCounter;

	private bool mOKEnable = true;

	private UISsSprite mUISsAnime;

	private int mTrophyGetnum;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("GetAccessory_Item_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 63f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 104);
		uISprite.type = UIBasicSprite.Type.Sliced;
		UISprite sprite = Util.CreateSprite("IconImage", uISprite.gameObject, image);
		Util.SetSpriteInfo(sprite, "icon_ranking_trophy", mBaseDepth + 2, new Vector3(0f, 1f, 0f), Vector3.one, false);
		titleDescKey = string.Format(Localization.Get("Ranking_TrophyNum"));
		titleDescKey += string.Format(Localization.Get("GetAccessory_BOOSTER_Desc_02"), mTrophyGetnum);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, -25f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		uILabel.fontSize = 32;
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 4, new Vector3(0f, -113f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		mOKEnable = false;
	}

	public void Init(int trophyGetNum)
	{
		mTrophyGetnum = trophyGetNum;
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
		mGame.PlaySe("SE_DIALY_COMPLETE_STAMP", -1);
		mUISsAnime = Util.CreateGameObject("GrowHalo", base.gameObject).AddComponent<UISsSprite>();
		mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
		mUISsAnime.depth = mBaseDepth + 5;
		mUISsAnime.transform.localPosition = new Vector3(0f, 0f, 0f);
		mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
		mUISsAnime.PlayCount = 1;
		mUISsAnime.Play();
		UISsSprite uISsSprite = mUISsAnime;
		uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnGetAnimeFinished));
	}

	private void OnGetAnimeFinished(UISsSprite sprite)
	{
		if (mUISsAnime != null)
		{
			UnityEngine.Object.Destroy(mUISsAnime.gameObject);
			mUISsAnime = null;
		}
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		if (!GetBusy())
		{
			OnCancelPushed(null);
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
