using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/Condition Set")]
public sealed class SpecialChanceConditionSet : ScriptableObject
{
	public enum CHANCE_MODE
	{
		ERROR = -1,
		SPECIAL = 0,
		PLUS = 1,
		SCORE_UP = 2,
		CONTINUE = 3,
		JEWEL_DOUBLE_UP = 4,
		STAR_GET = 5
	}

	[SerializeField]
	private SpecialChanceConditionItem[] mItems;

	private Dictionary<int, SpecialChanceConditionItem> mCache;

	private Dictionary<int, SpecialChanceConditionItem> Cache
	{
		get
		{
			if (mCache == null)
			{
				mCache = (from n in mItems.Select((SpecialChanceConditionItem v, int i) => new
					{
						Value = v,
						Index = i
					})
					where n.Value != null
					select n).ToDictionary(k => k.Index, v => v.Value);
			}
			return mCache;
		}
	}

	public void SetData(int id, SpecialChanceConditionItem item)
	{
		if (id >= 0)
		{
			if (mItems == null)
			{
				mItems = new SpecialChanceConditionItem[id + 1];
				mCache = null;
			}
			if (id >= mItems.Length)
			{
				Array.Resize(ref mItems, id + 1);
				mCache = null;
			}
			mItems[id] = item;
		}
	}

	public bool IsAchieve(int condition_id, SpecialChanceConditionItem.Option option)
	{
		return Cache != null && Cache.ContainsKey(condition_id) && Cache[condition_id].IsAchieve(option);
	}

	public CHANCE_MODE GetChanceMode(int condition_id)
	{
		return (Cache == null || !Cache.ContainsKey(condition_id)) ? CHANCE_MODE.ERROR : Cache[condition_id].Mode;
	}
}
