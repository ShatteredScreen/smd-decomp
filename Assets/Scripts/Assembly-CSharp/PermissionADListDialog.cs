using System.Collections.Generic;
using UnityEngine;

public class PermissionADListDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	public string HEADER_PERMISSION_KEY = string.Empty;

	public string FOOTER_PERMISSION_KEY = string.Empty;

	public string[] PermissionKeyArray = new string[1] { "PermissionText_0" };

	private OnDialogClosed mCallback;

	private SMVerticalListInfo mPermissionListInfo;

	private SMVerticalList mPermissionList;

	private float mBasePosY;

	private int mItemHeight;

	private ConfirmDialog mConfirmDialog;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("PermissionTitle");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		mItemHeight = 218;
		mItemHeight = 190;
		mPermissionListInfo = new SMVerticalListInfo(1, 512f, 400f, 1, 512f, 400f, 1, mItemHeight);
		SetLayout(Util.ScreenOrientation);
		int num = 0;
		if (!string.IsNullOrEmpty(HEADER_PERMISSION_KEY))
		{
			num++;
		}
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		int num2 = 0;
		while (num2 < PermissionKeyArray.Length)
		{
			string key = PermissionKeyArray[num2];
			SMVerticalListItem item = PermissionADItem.Make(num, Localization.Get(key));
			list.Add(item);
			num2++;
			num++;
		}
		list.Sort();
		if (!string.IsNullOrEmpty(HEADER_PERMISSION_KEY))
		{
			list.Insert(0, PermissionADItem.Make(0, Localization.Get(HEADER_PERMISSION_KEY)));
		}
		if (!string.IsNullOrEmpty(FOOTER_PERMISSION_KEY))
		{
			list.Add(PermissionADItem.Make(++num, Localization.Get(FOOTER_PERMISSION_KEY)));
		}
		mPermissionList = SMVerticalList.Make(base.gameObject, this, mPermissionListInfo, list, 0f, 20f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mPermissionList.OnCreateItem = OnCreateItem_Permission;
		UIButton button = Util.CreateImageButton("ButtonCheck", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_check", "LC_button_check", "LC_button_check", mBaseDepth + 2, new Vector3(0f, -232f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCheckPushed", UIButtonMessage.Trigger.OnClick);
		SetOpenSeEnable(false);
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		}
	}

	private void OnCreateItem_Permission(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		PermissionADItem permissionADItem = item as PermissionADItem;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", num + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = 512;
		uISprite.height = mItemHeight;
		UILabel uILabel = Util.CreateLabel("Text" + index, itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, permissionADItem.Text, num + 2, new Vector3(-250f, 0f, 0f), 20, 485, 0, UIWidget.Pivot.Left);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.fontSize = 22;
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.lineWidth = 485;
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
		if (mPermissionList != null)
		{
			GameMain.SafeDestroy(mPermissionList.gameObject);
			mPermissionList = null;
		}
	}

	public override void OnBackKeyPress()
	{
		OnCheckPushed(null);
	}

	public void OnCheckPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			Close();
		}
	}

	private void EnableList(bool enabled)
	{
		if (!(mPermissionList == null))
		{
			NGUITools.SetActive(mPermissionList.gameObject, enabled);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
