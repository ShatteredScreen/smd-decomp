using System.Collections.Generic;
using UnityEngine;

public class EventRewardDialog : DialogBase
{
	public class RewardListItem : SMVerticalListItem
	{
		public int gem;

		public int star;
	}

	public delegate void OnDialogClosed();

	private DIALOG_SIZE mDialogSize;

	private string mTitle = "DemoEvStarRemuneration_Title";

	private string mDescription;

	private OnDialogClosed mCallback;

	private SMVerticalListInfo mRewardListInfo;

	private SMVerticalList mRewardList;

	private SMEventPageData mSMEventPageData;

	private GameObject mDialogCenter;

	private int mNumStar;

	public override void Start()
	{
		base.Start();
	}

	private void ListOpen()
	{
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		List<SMEventStarRewardSetting> list2 = new List<SMEventStarRewardSetting>(mSMEventPageData.MapStarRewardList.Values);
		AccessoryData accessoryData = null;
		for (int i = 0; i < list2.Count; i++)
		{
			accessoryData = mGame.GetAccessoryData(list2[i].Reward);
			RewardListItem rewardListItem = new RewardListItem();
			rewardListItem.gem = accessoryData.GetGotIDByNum();
			rewardListItem.star = list2[i].StarNum;
			list.Add(rewardListItem);
		}
		mDialogCenter = Util.CreateGameObject("GridRoot", base.gameObject);
		mDialogCenter.transform.localPosition = new Vector3(0f, -105f);
		mRewardList = SMVerticalList.Make(mDialogCenter, this, mRewardListInfo, list, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mRewardList.OnCreateItem = OnRewardBuild;
	}

	private void OnRewardBuild(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		if (index == 0)
		{
			mRewardList.SetScrollBarDisplay(false);
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("EVENTHUD").Image;
		UISprite uISprite = null;
		UIFont atlasFont = GameMain.LoadFont();
		float cellWidth = mRewardList.mList.cellWidth;
		float cellHeight = mRewardList.mList.cellHeight;
		uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		RewardListItem rewardListItem = (RewardListItem)item;
		uISprite = Util.CreateSprite("LastIcon", itemGO, image);
		Util.SetSpriteInfo(uISprite, "icon_84jem", mBaseDepth + 3, new Vector3(-161f, 0f, 0f), Vector3.one, false);
		uISprite.SetDimensions(66, 66);
		UILabel uILabel;
		if (mNumStar >= rewardListItem.star)
		{
			uISprite.color = Color.gray;
			uILabel = Util.CreateLabel("kakutoku", itemGO.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("DemoEvStarRemuneration_Got"), mBaseDepth + 4, new Vector3(-161f, 0f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.yellow;
		}
		UISprite uISprite2 = Util.CreateSprite("star", itemGO, image);
		Util.SetSpriteInfo(uISprite2, "star_gold", mBaseDepth + 2, new Vector3(-83f, 0f, 0f), Vector3.one, false);
		uISprite2.SetDimensions(37, 37);
		uILabel = Util.CreateLabel("leftDesc", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Xtext") + rewardListItem.star + " " + Localization.Get("DemoEvStarRemuneration_StarNorma"), mBaseDepth + 4, new Vector3(17f, -2f, 0f), 24, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Color.white;
		uILabel = Util.CreateLabel("RDesc", itemGO.gameObject, atlasFont);
		string _str = Localization.Get("Gift_ItemNameSD");
		StringUtils.CheckPlural(rewardListItem.gem, ref _str);
		Util.SetLabelInfo(uILabel, rewardListItem.gem + " " + _str, mBaseDepth + 4, new Vector3(49f, -2f, 0f), 28, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Color.white;
	}

	public void Init(SMEventPageData rewardData, int star)
	{
		mNumStar = star;
		mSMEventPageData = rewardData;
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(string.Empty);
		UILabel uILabel = Util.CreateLabel("Title", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 135, new Vector3(0f, 288f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.2627451f, 0.007843138f, 28f / 85f, 1f);
		string text = Util.MakeLText(mTitle);
		Util.SetLabelText(uILabel, text);
		uILabel = Util.CreateLabel("eventdesc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 135, new Vector3(0f, 173f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		text = Util.MakeLText("DemoEvStarRemuneration_Desc00");
		Util.SetLabelText(uILabel, text);
		mRewardListInfo = new SMVerticalListInfo(1, 652f, 695f, 1, 440f, 510f, 1, 75);
		UISprite uISprite = Util.CreateSprite("BoosterSelectBoard", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, -21f, 0f), Vector3.one, false);
		uISprite.SetDimensions(490, 487);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.color = new Color(0.2627451f, 0.007843138f, 28f / 85f, 1f);
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
		ListOpen();
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		mCallback();
		Object.Destroy(base.gameObject);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
