using UnityEngine;

public struct CharaSilhouettte
{
	public Vector3 Position { get; set; }

	public int CharaID { get; set; }

	public string ImageName { get; set; }
}
