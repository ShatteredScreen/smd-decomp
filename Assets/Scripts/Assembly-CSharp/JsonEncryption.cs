using System;

public class JsonEncryption : ICloneable
{
	[MiniJSONAlias("hash")]
	public string Hash { get; set; }

	[MiniJSONAlias("original_json")]
	public string OriginalJson { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public JsonEncryption Clone()
	{
		return MemberwiseClone() as JsonEncryption;
	}
}
