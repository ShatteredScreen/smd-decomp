using UnityEngine;

public class BoosterMessageDialog : DialogBase
{
	public delegate void OnDialogClosed(Def.BOOSTER_KIND b, bool cancel);

	public delegate void CencelPushedDelegate();

	private OnDialogClosed mCallback;

	private Def.BOOSTER_KIND mBoosterKind;

	private string mMessage;

	private bool mCanceled;

	private bool mTutorialMode;

	private Color mTextColor;

	private CencelPushedDelegate OnCancelPushedTrigger = delegate
	{
	};

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.SMALL, "DIALOG_BASE", "instruction_panel11", 470, 180);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, mMessage, mBaseDepth + 3, Vector3.zero, 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(410, 48);
		uILabel.color = mTextColor;
		if (mBoosterKind == Def.BOOSTER_KIND.BLOCK_CRUSH || mBoosterKind == Def.BOOSTER_KIND.ALL_CRUSH)
		{
			UIButton button = Util.CreateJellyImageButton("Enter", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_invocation", "LC_button_invocation", "LC_button_invocation", mBaseDepth + 3, new Vector3(0f, -46f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnEnterPushed", UIButtonMessage.Trigger.OnClick);
			uILabel.transform.localPosition = new Vector3(0f, 15f, 0f);
		}
		if (!mTutorialMode)
		{
			CreateCancelButton("OnCancelPushed");
		}
		SetLayout(Util.ScreenOrientation);
	}

	public void Init(Def.BOOSTER_KIND boosterKind, bool tutorialMode, CencelPushedDelegate onCancelPushedTrigger, bool usable = true)
	{
		mBoosterKind = boosterKind;
		if (usable)
		{
			mMessage = Localization.Get(mGame.mBoosterData[(int)boosterKind].useMessage);
			mTextColor = new Color(0.2509804f, 0.20784314f, 6f / 85f, 1f);
		}
		else
		{
			mMessage = Localization.Get(mGame.mBoosterData[(int)boosterKind].useNgMessage);
			mTextColor = new Color(66f / 85f, 0f, 0f, 1f);
		}
		mCanceled = false;
		mTutorialMode = tutorialMode;
		OnCancelPushedTrigger = onCancelPushedTrigger;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		SetLayout(o);
	}

	private void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			base.gameObject.transform.localPosition = new Vector3(0f, 200f, 0f);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			Vector2 vector = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, o);
			base.gameObject.transform.localPosition = new Vector3(-360f, vector.y + 76f, 0f);
			break;
		}
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mBoosterKind, mCanceled);
		}
	}

	public override void OnBackKeyPress()
	{
		if (!mTutorialMode)
		{
			OnCancelPushed(null);
		}
	}

	private void OnEnterPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mCanceled = false;
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mCanceled = true;
			OnCancelPushedTrigger();
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
