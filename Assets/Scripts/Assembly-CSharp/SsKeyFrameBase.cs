using System;
using UnityEngine;

[Serializable]
public class SsKeyFrameBase<T> : SsKeyFrameInterface
{
	public T Value;

	[SerializeField]
	private int _Time;

	[SerializeField]
	private SsKeyValueType _ValueType;

	[SerializeField]
	private SsCurveParams _Curve;

	public int Time
	{
		get
		{
			return _Time;
		}
		set
		{
			_Time = value;
		}
	}

	public SsKeyValueType ValueType
	{
		get
		{
			return _ValueType;
		}
		set
		{
			_ValueType = value;
		}
	}

	public SsCurveParams Curve
	{
		get
		{
			return _Curve;
		}
		set
		{
			_Curve = value;
		}
	}

	public object ObjectValue
	{
		get
		{
			return Value;
		}
		set
		{
			Value = (T)value;
		}
	}

	public bool EqualsValue(SsKeyFrameInterface rhs)
	{
		if (_Curve != null && rhs.Curve != null)
		{
			if (!_Curve.Equals(rhs.Curve))
			{
				return false;
			}
		}
		else if (_Curve != rhs.Curve)
		{
			return false;
		}
		SsKeyFrameBase<T> ssKeyFrameBase = (SsKeyFrameBase<T>)rhs;
		if (!Value.Equals(ssKeyFrameBase.Value))
		{
			return false;
		}
		return true;
	}

	public override string ToString()
	{
		return string.Concat("MyType: ", typeof(T), ", ValueType: ", ValueType, ", Time: ", Time, ", Value {", Value, "}, Curve {", Curve, "}\n");
	}
}
