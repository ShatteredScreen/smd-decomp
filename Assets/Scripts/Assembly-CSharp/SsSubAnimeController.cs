using System;

[Serializable]
public class SsSubAnimeController
{
	public delegate void SubAnimationCallback(SsSubAnimeController subAnime);

	public SubAnimationCallback AnimationFinished;

	public SsAnimation Animation;

	public float Frame;

	public float Speed = 1f;

	public int PlayCount;

	public int CurrentPlayCount;

	public bool BindsToAllParts;

	public bool BindsByPartName;

	public bool IsPlaying = true;

	public void Play()
	{
		IsPlaying = true;
	}

	public void Pause()
	{
		IsPlaying = false;
	}

	public void Reset()
	{
		Pause();
		CurrentPlayCount = 0;
	}

	public void StepFrame(float deltaTime)
	{
		if (Animation == null || !IsPlaying)
		{
			return;
		}
		Frame += deltaTime * Speed * (float)Animation.FPS;
		if (!(Frame < 0f) && (int)Frame <= Animation.EndFrame)
		{
			return;
		}
		bool flag = true;
		if (PlayCount > 0 && ++CurrentPlayCount >= PlayCount)
		{
			flag = false;
			IsPlaying = false;
			Frame = ((Frame < 0f) ? 0f : ((!(Frame > (float)Animation.EndFrame)) ? Frame : ((float)Animation.EndFrame)));
			CurrentPlayCount = PlayCount;
			if (AnimationFinished != null)
			{
				AnimationFinished(this);
			}
		}
		if (flag)
		{
			Frame %= Animation.EndFrame;
			if (Frame < 0f)
			{
				Frame = (float)(Animation.EndFrame + 1) + Frame;
			}
		}
	}
}
