using System;

public class PlayerShopData : SMJsonData, ICloneable
{
	public int Version { get; set; }

	public int ShopItemID { get; set; }

	public int ShopPurchaseItemNum { get; set; }

	public int ShopCount { get; set; }

	public PlayerShopData()
	{
		ShopItemID = 0;
		ShopPurchaseItemNum = 0;
		ShopCount = 0;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(ShopItemID);
		data.WriteInt(ShopPurchaseItemNum);
		data.WriteInt(ShopCount);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		ShopItemID = data.ReadInt();
		ShopPurchaseItemNum = data.ReadInt();
		ShopCount = data.ReadInt();
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerShopData obj = new PlayerShopData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public PlayerShopData Clone()
	{
		return MemberwiseClone() as PlayerShopData;
	}
}
