using System.Collections.Generic;
using System.Text;

public class AdvertiseFlag
{
	public int Kind { get; set; }

	[MiniJSONArray(typeof(int))]
	public List<int> Actions { get; set; }

	public int RestrictType { get; set; }

	[MiniJSONArray(typeof(int))]
	public List<int> Restricts { get; set; }

	public AdvertiseFlag()
	{
		Kind = 0;
		Actions = new List<int>();
		RestrictType = 0;
		Restricts = new List<int>();
	}

	public override string ToString()
	{
		string empty = string.Empty;
		StringBuilder stringBuilder = new StringBuilder();
		int num = ((Actions != null) ? Actions.Count : 0);
		for (int i = 0; i < num; i++)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(",");
			}
			stringBuilder.Append(Actions[i]);
		}
		empty = stringBuilder.ToString();
		string empty2 = string.Empty;
		StringBuilder stringBuilder2 = new StringBuilder();
		int num2 = ((Restricts != null) ? Restricts.Count : 0);
		for (int j = 0; j < num2; j++)
		{
			if (stringBuilder2.Length > 0)
			{
				stringBuilder2.Append(",");
			}
			stringBuilder2.Append(Restricts[j]);
		}
		empty2 = stringBuilder2.ToString();
		return string.Format("[AdvertiseFlag Kind={0} Actions=[{1}] RestrictType={2} Restricts=[{3}]]", Kind, empty, RestrictType, empty2);
	}
}
