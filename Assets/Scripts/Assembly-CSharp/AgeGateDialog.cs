using UnityEngine;

public class AgeGateDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0,
		CANCEL = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private new void Start()
	{
	}

	private new void Update()
	{
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(Localization.Get("Rules_Title00"));
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
