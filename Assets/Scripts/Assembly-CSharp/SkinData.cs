using System.Collections.Generic;
using UnityEngine;

public class SkinData
{
	public short index;

	public string upBgSpriteName;

	public Color panelColor;

	public Color edgeColor;

	public Color characterColor;

	public List<string> name = new List<string>();

	public List<string> bgUrl = new List<string>();

	public List<string> bgPath = new List<string>();

	public List<string> hudUrl = new List<string>();

	public List<string> hudPath = new List<string>();

	public SkinData()
	{
		index = 0;
		panelColor = default(Color);
		edgeColor = default(Color);
		characterColor = default(Color);
	}

	public void deserialize(BIJBinaryReader stream)
	{
		index = stream.ReadShort();
		upBgSpriteName = stream.ReadUTF();
		string text = stream.ReadUTF();
		string[] array = text.Split(',');
		panelColor = new Color(float.Parse(array[0]) / 255f, float.Parse(array[1]) / 255f, float.Parse(array[2]) / 255f);
		text = stream.ReadUTF();
		array = text.Split(',');
		edgeColor = new Color(float.Parse(array[0]) / 255f, float.Parse(array[1]) / 255f, float.Parse(array[2]) / 255f);
		text = stream.ReadUTF();
		array = text.Split(',');
		characterColor = new Color(float.Parse(array[0]) / 255f, float.Parse(array[1]) / 255f, float.Parse(array[2]) / 255f);
		text = stream.ReadUTF();
		name.Add(text);
		text = stream.ReadUTF();
		bgUrl.Add(text);
		text = stream.ReadUTF();
		bgPath.Add(text);
		text = stream.ReadUTF();
		hudUrl.Add(text);
		text = stream.ReadUTF();
		hudPath.Add(text);
		text = stream.ReadUTF();
		name.Add(text);
		text = stream.ReadUTF();
		bgUrl.Add(text);
		text = stream.ReadUTF();
		bgPath.Add(text);
		text = stream.ReadUTF();
		hudUrl.Add(text);
		text = stream.ReadUTF();
		hudPath.Add(text);
		text = stream.ReadUTF();
		name.Add(text);
		text = stream.ReadUTF();
		bgUrl.Add(text);
		text = stream.ReadUTF();
		bgPath.Add(text);
		text = stream.ReadUTF();
		hudUrl.Add(text);
		text = stream.ReadUTF();
		hudPath.Add(text);
	}
}
