public class SMSpecialDailyRewardData
{
	public int RewardID;

	public virtual void Serialize(BIJFileBinaryWriter sw)
	{
		sw.WriteInt(1290);
		sw.WriteInt(RewardID);
	}

	public virtual void Deserialize(BIJBinaryReader a_reader)
	{
		int num = a_reader.ReadInt();
		RewardID = a_reader.ReadInt();
	}

	public string GetString()
	{
		string empty = string.Empty;
		string text = empty;
		return text + "  RewardID:" + RewardID + "\n";
	}
}
