using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameStateTitle : GameState
{
	public enum STATE
	{
		LOAD_WAIT = 0,
		LOAD_WAIT2 = 1,
		UNLOAD_WAIT = 2,
		NETWORK_PROLOGUE = 3,
		NETWORK_00 = 4,
		NETWORK_EPILOGUE = 5,
		MAIN = 6,
		EULA = 7,
		EULA_TERM_OF_USE = 8,
		GDPR = 9,
		PURCHASE_PRICE = 10,
		MAINTENANCE = 11,
		MAINTENANCE_CHECK = 12,
		MAINTENANCE_WAIT = 13,
		NICKNAME_CHECK = 14,
		NICKNAME_WAIT = 15,
		GAMEINFO_CHECK = 16,
		GAMEINFO_WAIT = 17,
		GEM_CHECK = 18,
		GEM_WAIT = 19,
		OPTION = 20,
		NETWORK_LOGIN00 = 21,
		NETWORK_LOGIN01 = 22,
		NETWORK_LOGOUT = 23,
		TRANSFERED_INIT_START = 24,
		TRANSFERED_NICKNAME_WAIT = 25,
		TRANSFERED_INIT_DIALOG = 26,
		NETWORK_AUTH_CHECK = 27,
		NETWORK_AUTH_WAIT = 28,
		TRANSFERE_AUTH_CHECK = 29,
		TRANSFERE_AUTH_WAIT = 30,
		WAIT = 31
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private GameStateManager mGameState;

	private GameObject mAnchorBottom;

	private GameObject mAnchorTop;

	private GameObject mAnchorTopRight;

	private GameObject mAnchorTopLeft;

	private GameObject mAnchorBottomLeft;

	private UISprite mBg00;

	private UISprite mLogo;

	private UISprite mBack;

	private UISprite mCharacter;

	private UISprite mCopyright0;

	private UISprite mCopyright1;

	private UISprite mCopyright2;

	private bool mLaceMove;

	private Camera mCamera;

	private UIButton mPlayButton;

	private UIButton mTransferButton;

	private UIButton mOptionButton;

	private UIButton mSupportButton;

	protected List<UIButton> mTitleButtons = new List<UIButton>();

	private UILabel mFacebookName;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private EULADialog mEULADialog;

	private EulaAcceptDialog mEulaAcceptDialog;

	private bool mEULADialogDisplayReq;

	private WebViewDialog mWebViewDialog;

	private bool mNetworkRequest;

	private int mBaseDepth = 50;

	private static readonly Color32 TEXT_MESSAGE = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

	private bool mFromSupport;

	private bool mInMaintenance;

	private STATE mStateAfterMaintenanceCheck = STATE.MAIN;

	private ConnectCommonErrorDialog mMaintenanceErrorDialog;

	private ParticleSystem mPSforAdreno;

	public override void Start()
	{
		base.Start();
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
		mPlayButton = null;
	}

	public override void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.LOAD_WAIT:
		{
			if (!isLoadFinished())
			{
				break;
			}
			if (mGame.mMaintenanceProfile.IsServiceFinished)
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.SERVICE_FINISH);
				mState.Change(STATE.UNLOAD_WAIT);
				break;
			}
			StartCoroutine(mGame.InitGame());
			switch (mGameState.PrevState)
			{
			case GameStateManager.GAME_STATE.MAP:
			case GameStateManager.GAME_STATE.PUZZLE:
			case GameStateManager.GAME_STATE.EVENT_RALLY:
			case GameStateManager.GAME_STATE.EVENT_STAR:
			case GameStateManager.GAME_STATE.EVENT_RALLY2:
			case GameStateManager.GAME_STATE.ADVPUZZLE:
			case GameStateManager.GAME_STATE.ADVMENU:
				mGame.Save();
				break;
			case GameStateManager.GAME_STATE.TRANSFER:
			case GameStateManager.GAME_STATE.SUPPORT:
				mFromSupport = true;
				break;
			}
			mGame.PlayMusic("VOICE_001", 1, false);
			mGame.PlayMusic("BGM_PUZZLE_CHANCE", 0, true);
			mState.Change(STATE.LOAD_WAIT2);
			GameObject particle = GameMain.GetParticle("Particles/DummyVerts");
			particle.transform.parent = mRoot.transform;
			particle.layer = mRoot.layer;
			particle.transform.localScale = Vector3.one;
			mPSforAdreno = particle.GetComponent<ParticleSystem>();
			mPSforAdreno.loop = false;
			mPSforAdreno.transform.localPosition = new Vector3(0f, 0f, 0f);
			mPSforAdreno.Stop();
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9000);
				mGame.Save();
			}
			Camera component = GameObject.Find("Camera").GetComponent<Camera>();
			component.backgroundColor = Color.black;
			break;
		}
		case STATE.LOAD_WAIT2:
			if (mGame.InitGameFinished)
			{
				if (mGame.IsTitleReturnMaintenance)
				{
					mGame.IsTitleReturnMaintenance = false;
					OnMaintenanceMode();
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_PROLOGUE:
			if (GameMain.IsPlayable)
			{
				mState.Change(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00:
			switch (GameMain.UpdateVersionFlag)
			{
			case GameProfile.UpdateKind.CONFIRM:
				OnUpdateVersionConfrim();
				break;
			case GameProfile.UpdateKind.FORCE:
				OnUpdateVersionForce();
				break;
			default:
				mState.Change(STATE.NETWORK_EPILOGUE);
				break;
			}
			break;
		case STATE.NETWORK_EPILOGUE:
		{
			UserDataInfo instance = UserDataInfo.Instance;
			if (instance.mResetReason != -1)
			{
				InheritingConfirmDialog.REASON aReason = InheritingConfirmDialog.REASON.LOAD_ERROR;
				if (instance.mResetReason == 101)
				{
					aReason = InheritingConfirmDialog.REASON.AUTHKEY_ERROR;
				}
				ShowInheritingConfirmDialog(aReason, instance.mResetReason);
				instance.mResetReason = -1;
				instance.Save();
			}
			else
			{
				CheckRollback();
				GameMain.IsPlayable = true;
				mState.Change(STATE.MAIN);
			}
			break;
		}
		case STATE.MAIN:
			if (mNetworkRequest)
			{
				mNetworkRequest = false;
				mState.Reset(STATE.NETWORK_PROLOGUE);
				break;
			}
			if (GameMain.mUserAuthTransfered)
			{
				GameMain.IsPlayable = false;
				mState.Change(STATE.TRANSFERED_INIT_START);
				break;
			}
			if (GameMain.mUserAuthTransferError || mFromSupport)
			{
				mFromSupport = false;
				GameMain.IsPlayable = false;
				mState.Change(STATE.TRANSFERE_AUTH_CHECK);
				break;
			}
			if (GameStateTransfer.isTransferInterrupted())
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("Transfer_Restart_Title"), Localization.Get("Transfer_Restart_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnContinueTransferDialogClosed);
				GameMain.IsPlayable = false;
				mState.Change(STATE.WAIT);
			}
			if (!mGame.adrenoParticled)
			{
				if (!mPSforAdreno.isPlaying)
				{
					mPSforAdreno.Emit(25000);
				}
				mGame.adrenoParticled = true;
			}
			if (mPlayButton != null)
			{
				NGUITools.SetActive(mPlayButton.gameObject, GameMain.IsPlayable);
			}
			if (mGame.mBackKeyTrg)
			{
				OnBackKeyPress();
			}
			break;
		case STATE.EULA:
			if (!mState.IsChanged())
			{
				break;
			}
			if (GameMain.CheckEULADialogShow())
			{
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
				{
					mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9010);
					mGame.Save();
				}
				StartCoroutine(GrayOut());
				SetEnableButtonFlg(false);
				mEulaAcceptDialog = Util.CreateGameObject("EulaAcceptDialog", mRoot).AddComponent<EulaAcceptDialog>();
				mEulaAcceptDialog.SetCallback(OnEulaAcceptDialogClosed);
			}
			else
			{
				mState.Change(STATE.GDPR);
			}
			break;
		case STATE.EULA_TERM_OF_USE:
			OnWebViewDialogEULA();
			break;
		case STATE.GDPR:
			if (mState.IsChanged())
			{
				List<GDPRInfo> list = GDPRAcceptDialog.CheckGDPRAccept(mGame.GameProfile.mGDPRSetting, mGame.mPlayer.Data);
				if (list.Count > 0)
				{
					GameObject gameObject = Util.CreateGameObject("GDPRAcceptDialog", mRoot);
					GDPRAcceptDialog gDPRAcceptDialog = gameObject.AddComponent<GDPRAcceptDialog>();
					gDPRAcceptDialog.SetGDPRInfo(list);
					gDPRAcceptDialog.SetClosedCallback(OnGDPRAcceptDialogClosed);
				}
				else
				{
					mState.Change(STATE.PURCHASE_PRICE);
				}
			}
			break;
		case STATE.PURCHASE_PRICE:
			if (mState.IsChanged())
			{
				DateTime dateTime = DateTimeUtil.Now();
				int num = -1;
				for (int j = 0; j < mGame.SegmentProfile.ChanceList.Count; j++)
				{
					if (mGame.SegmentProfile.ChanceList != null && j < mGame.SegmentProfile.ChanceList.Count)
					{
						ChanceCommInfo common = mGame.SegmentProfile.ChanceList[j].Common;
						if (common != null && !(dateTime < common.StartTime) && !(common.EndTime < dateTime) && mGame.SegmentProfile.ChanceList[j].Data != null)
						{
							num = j;
							break;
						}
					}
				}
				if (num == -1)
				{
					OnStart();
				}
				else
				{
					mGame.Network_ChargePriceCheck(mGame.SegmentProfile.ChanceList[num].Common.Period);
				}
			}
			else
			{
				if (!GameMain.mChargePriceCheckDone)
				{
					break;
				}
				if (GameMain.mChargePriceCheckSucceed)
				{
					OnStart();
					break;
				}
				Server.ErrorCode mUserAuthErrorCode = GameMain.mChargePriceCheckErrorCode;
				if (mUserAuthErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, delegate(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
					{
						mState.Change((STATE)state);
					}, 10);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 10, delegate(int a_state)
					{
						mState.Change((STATE)a_state);
					}, null);
				}
				mState.Reset(STATE.WAIT);
			}
			break;
		case STATE.MAINTENANCE:
			OnMaintenanceDialog();
			break;
		case STATE.MAINTENANCE_CHECK:
			if (mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty))
			{
				mState.Change(STATE.MAINTENANCE_WAIT);
				break;
			}
			mMaintenanceErrorDialog = Util.CreateGameObject("MaintenanceCheckErrorDialog", mRoot).AddComponent<ConnectCommonErrorDialog>();
			mMaintenanceErrorDialog.Init(string.Empty, string.Empty, ConnectCommonErrorDialog.STYLE.RETRY_CLOSE);
			mMaintenanceErrorDialog.SetClosedCallback(OnMaintenanceCheckErrorDialogClosed);
			mState.Change(STATE.WAIT);
			break;
		case STATE.MAINTENANCE_WAIT:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (!GameMain.MaintenanceMode)
				{
					if (!mInMaintenance)
					{
						mState.Change(mStateAfterMaintenanceCheck);
						mStateAfterMaintenanceCheck = STATE.MAIN;
					}
					else
					{
						mState.Change(STATE.NICKNAME_CHECK);
					}
				}
				else if (mGame.mMaintenanceProfile.IsServiceFinished)
				{
					mGameState.SetNextGameState(GameStateManager.GAME_STATE.SERVICE_FINISH);
					mState.Change(STATE.UNLOAD_WAIT);
				}
				else
				{
					OnMaintenanceMode();
				}
			}
			else
			{
				mMaintenanceErrorDialog = Util.CreateGameObject("MaintenanceCheckErrorDialog", mRoot).AddComponent<ConnectCommonErrorDialog>();
				mMaintenanceErrorDialog.Init(string.Empty, string.Empty, ConnectCommonErrorDialog.STYLE.RETRY_CLOSE);
				mMaintenanceErrorDialog.SetClosedCallback(OnMaintenanceCheckErrorDialogClosed);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NICKNAME_CHECK:
			mGame.Network_NicknameCheck();
			mState.Change(STATE.NICKNAME_WAIT);
			break;
		case STATE.NICKNAME_WAIT:
			if (GameMain.mNickNameCheckDone)
			{
				if (GameMain.mNickNameSucceed)
				{
					mState.Change(STATE.GAMEINFO_CHECK);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 14, OnConnectErrorRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.GAMEINFO_CHECK:
			mGame.mPlayer.Data.LastGetGameInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.GAMEINFO_WAIT);
			break;
		case STATE.GAMEINFO_WAIT:
			if (GameMain.mGameProfileCheckDone)
			{
				if (GameMain.mGameProfileSucceed)
				{
					mState.Change(STATE.GEM_CHECK);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 16, OnConnectErrorRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.GEM_CHECK:
			mGame.GetGemCountFreqFlag = false;
			mGame.Network_GetGemCount();
			mState.Change(STATE.GEM_WAIT);
			break;
		case STATE.GEM_WAIT:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(mStateAfterMaintenanceCheck);
					mStateAfterMaintenanceCheck = STATE.MAIN;
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 18, OnConnectErrorRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.OPTION:
			GoToOption();
			break;
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						mGame.SaveOptions();
					}
					SNSError(_sns);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns3 = mSNS;
			_sns3.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns3, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns3))
				{
					if (GameMain.UpdateOptions(_sns3, string.Empty, string.Empty))
					{
						SetFacebookName();
						mGame.SaveOptions();
					}
					SNSError(_sns3);
				}
				else
				{
					SetFacebookName();
					mState.Change(STATE.MAIN);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGOUT:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Logout(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					string id = null;
					string text = null;
					GameMain.TryGetSNSUserInfo(_sns2, out id, out text);
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						mGame.SaveOptions();
						mGame.mPlayer.ResetFriends();
						mGame.Save();
					}
					mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				SetFacebookName();
				mState.Change(STATE.MAIN);
			}, null);
			break;
		}
		case STATE.TRANSFERED_INIT_START:
		{
			ServerCram.SendDataTransferDialog(100, mGame.mPlayer, mGame.mPlayer.Data, mGame.mPlayer.AdvSaveData, UserDataInfo.Instance.AuthKey);
			int uUID = mGame.mPlayer.UUID;
			int hiveId = mGame.mPlayer.HiveId;
			mGame.ResetPlayerData(true);
			mGame.mPlayer.SetUUID(uUID);
			mGame.mPlayer.HiveId = hiveId;
			mGame.Save(true, true);
			Server.RemoveFriend();
			mGame.LoadMTData();
			mGame.Network_NicknameCheck();
			mState.Change(STATE.TRANSFERED_NICKNAME_WAIT);
			break;
		}
		case STATE.TRANSFERED_NICKNAME_WAIT:
			if (GameMain.mNickNameCheckDone)
			{
				ShowInheritingConfirmDialog(InheritingConfirmDialog.REASON.AUTH_ERROR, 100);
				mState.Change(STATE.TRANSFERED_INIT_DIALOG);
			}
			break;
		case STATE.NETWORK_AUTH_CHECK:
			Server.ManualConnecting = true;
			mGame.Network_UserAuth();
			mState.Change(STATE.NETWORK_AUTH_WAIT);
			break;
		case STATE.NETWORK_AUTH_WAIT:
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			Server.ManualConnecting = false;
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mGame.RetryCount = 0;
					mConfirmDialog = Util.CreateGameObject("AuthSuccessDialog", mRoot).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("Connect_Success_Title"), Localization.Get("Connect_Success_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
					mConfirmDialog.SetClosedCallback(OnAuthSuccessDialogClosed);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}
			else
			{
				Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
				if (mUserAuthErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 27, OnConnectErrorRetry, OnConnectErrorAuthAbandon);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 27, OnConnectErrorRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.TRANSFERE_AUTH_CHECK:
			mGame.mPlayer.Data.LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
			Server.ManualConnecting = true;
			mGame.Network_UserAuth();
			mState.Change(STATE.TRANSFERE_AUTH_WAIT);
			break;
		case STATE.TRANSFERE_AUTH_WAIT:
			if (GameMain.mUserAuthCheckDone)
			{
				Server.ManualConnecting = false;
				if (GameMain.mUserAuthSucceed)
				{
					GameMain.mUserAuthTransferError = false;
					GameMain.IsPlayable = true;
					mState.Change(STATE.MAIN);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 29, OnConnectErrorRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		}
		mState.Update();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
		{
			mCharacter.transform.localPosition = new Vector3(0f, 532f, 0f);
			mCharacter.transform.localScale = new Vector3(0.9f, 0.9f, 0f);
			mLogo.transform.localPosition = new Vector3(0f, -250f, 0f);
			mLogo.transform.localScale = new Vector3(1f, 1f, 0f);
			mCopyright0.transform.localPosition = new Vector3(0f, 173f, 0f);
			mCopyright1.transform.localPosition = new Vector3(0f, 150f, 0f);
			mPlayButton.transform.localPosition = new Vector3(0f, 283f, -5f);
		}
		else
		{
			mCharacter.transform.localPosition = new Vector3(-240f, 393f, 0f);
			mCharacter.transform.localScale = new Vector3(0.9f, 0.9f, 0f);
			mLogo.transform.localPosition = new Vector3(260f, -170f, 0f);
			mLogo.transform.localScale = new Vector3(0.9f, 0.9f, 0f);
			mCopyright0.transform.localPosition = new Vector3(0f, 173f, 0f);
			mCopyright1.transform.localPosition = new Vector3(0f, 150f, 0f);
			mPlayButton.transform.localPosition = new Vector3(260f, 245f, -5f);
		}
		if (mBack != null)
		{
			Vector2 vector = Util.LogScreenSize();
			float num = vector.y / 1022f;
			mBack.transform.localScale = new Vector3(num, num, 1f);
			mBack.SetDimensions((int)(vector.x / num), (int)(vector.y / num));
		}
	}

	public override IEnumerator LoadGameState()
	{
		mGame.mAdvMode = false;
		while (!GameMain.IsLogoFinished)
		{
			yield return 0;
		}
		mGame.StartLoading();
		if (GameMain.USE_RESOURCEDL)
		{
		}
		List<ResourceInstance> iconResourceList = new List<ResourceInstance>
		{
			ResourceManager.LoadImageAsync("ICON"),
			ResourceManager.LoadSsAnimationAsync("EFFECT_ICON_AVATER"),
			ResourceManager.LoadSsAnimationAsync("MAP_SNS_ICON"),
			ResourceManager.LoadSsAnimationAsync("MAP_STRAY_ICON"),
			ResourceManager.LoadSsAnimationAsync("ICON_CURSOR")
		};
		ResourceDLUtils.dbg1("Load Title Resources", true);
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		mAnchorBottom = Util.CreateAnchorWithChild("TitleAnchorBottom", camera.gameObject, UIAnchor.Side.Bottom, camera).gameObject;
		mAnchorTop = Util.CreateAnchorWithChild("TitleAnchorTop", camera.gameObject, UIAnchor.Side.Top, camera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("TitleAnchorBottomLeft", camera.gameObject, UIAnchor.Side.BottomLeft, camera).gameObject;
		mAnchorTopLeft = Util.CreateAnchorWithChild("TitleAnchorTopLeft", camera.gameObject, UIAnchor.Side.TopLeft, camera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("TitleAnchortopRight", camera.gameObject, UIAnchor.Side.TopRight, camera).gameObject;
		yield return 0;
		ResImage resTitleImage = ResourceManager.LoadImageAsync("TITLE");
		while (resTitleImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		BIJImage titleAtlas = resTitleImage.Image;
		BIJImage logoAtlas = ResourceManager.LoadImage("LOGO").Image;
		BIJImage hudAtlas = ResourceManager.LoadImage("HUD").Image;
		UIFont font = GameMain.LoadFont();
		yield return 0;
		mGame.LoadSoundResources();
		yield return 0;
		Color32 text_message = TEXT_MESSAGE;
		ResSound resSound = ResourceManager.LoadSoundAsync("BGM_PUZZLE_CHANCE");
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mBack = Util.CreateSprite("Bg01", mRoot, logoAtlas);
		Util.SetSpriteInfo(mBack, "back", 0, Vector3.zero, Vector3.one, false);
		mBack.type = UIBasicSprite.Type.Tiled;
		mLogo = Util.CreateSprite("Logo", mAnchorTop.gameObject, titleAtlas);
		Util.SetSpriteInfo(mLogo, "titleLogo", 2, new Vector3(0f, -200f, 0f), Vector3.one, false);
		mCharacter = Util.CreateSprite("Character", mAnchorBottom.gameObject, titleAtlas);
		if (UnityEngine.Random.Range(0, 100) < 50)
		{
			Util.SetSpriteInfo(mCharacter, "character00", 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
		}
		else
		{
			Util.SetSpriteInfo(mCharacter, "character01", 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
		}
		mCopyright0 = Util.CreateSprite("mCopyright0", mAnchorBottom.gameObject, titleAtlas);
		Util.SetSpriteInfo(mCopyright0, "copyright00", 2, new Vector3(0f, 180f, 0f), Vector3.one, false);
		mCopyright1 = Util.CreateSprite("mCopyright1", mAnchorBottom.gameObject, titleAtlas);
		Util.SetSpriteInfo(mCopyright1, "copyright01", 2, new Vector3(0f, 155f, 0f), Vector3.one, false);
		UISprite Board2 = Util.CreateSprite("ItemsR", mAnchorBottom.gameObject, hudAtlas);
		Util.SetSpriteInfo(Board2, "menubar_frame", mBaseDepth, new Vector3(-2f, -123f, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		Board2 = Util.CreateSprite("ItemsL", mAnchorBottom.gameObject, hudAtlas);
		Util.SetSpriteInfo(Board2, "menubar_frame", mBaseDepth, new Vector3(2f, -123f, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mLaceMove = true;
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, 1, new Vector3(30f, -110f, 0f), new Vector3(1f, 1f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, 1, new Vector3(-140f, -10f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, 1, new Vector3(190f, 70f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, 1, new Vector3(-20f, 180f, 0f), new Vector3(1f, 1f, 1f), 15f));
		while (!GameMain.IsLoaded)
		{
			yield return 0;
		}
		mPlayButton = Util.CreateJellyImageButton("StartButton", mAnchorBottom.gameObject, titleAtlas);
		Util.SetImageButtonInfo(mPlayButton, "LC_btn_play", "LC_btn_play", "LC_btn_play", 10, new Vector3(0f, 310f, -5f), Vector3.one);
		Util.AddImageButtonMessage(mPlayButton, this, "OnStartButtonPushed", UIButtonMessage.Trigger.OnClick);
		NGUITools.SetActive(mPlayButton.gameObject, GameMain.IsPlayable);
		mTitleButtons.Add(mPlayButton);
		float Y_icon = 54f;
		mTransferButton = Util.CreateJellyImageButton("TransferButton", mAnchorBottom.gameObject, titleAtlas);
		Util.SetImageButtonInfo(mTransferButton, "button_modelchange", "button_modelchange", "button_modelchange", mBaseDepth + 1, new Vector3(-140f, Y_icon, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTransferButton, this, "OnTransferButtonPushed", UIButtonMessage.Trigger.OnClick);
		mTitleButtons.Add(mTransferButton);
		mOptionButton = Util.CreateJellyImageButton("OptionButton", mAnchorBottom.gameObject, titleAtlas);
		Util.SetImageButtonInfo(mOptionButton, "button_option02", "button_option02", "button_option02", mBaseDepth + 1, new Vector3(0f, Y_icon, 0f), Vector3.one);
		Util.AddImageButtonMessage(mOptionButton, this, "OnOptionButtonPushed", UIButtonMessage.Trigger.OnClick);
		mTitleButtons.Add(mOptionButton);
		mSupportButton = Util.CreateJellyImageButton("SupportButton", mAnchorBottom.gameObject, titleAtlas);
		Util.SetImageButtonInfo(mSupportButton, "button_help02", "button_help02", "button_help02", mBaseDepth + 1, new Vector3(140f, Y_icon, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSupportButton, this, "OnSupportButtonPushed", UIButtonMessage.Trigger.OnClick);
		mTitleButtons.Add(mSupportButton);
		SetFacebookName();
		if (GameMain.USE_DEBUG_DIALOG && !GameMain.HIDDEN_DEBUG_BUTTON)
		{
			UIButton button = Util.CreateJellyImageButton(atlas: ResourceManager.LoadImage("HUD").Image, name: "DebugButton", parent: mAnchorTopLeft.gameObject, a_type: Util.BUTTON_TYPE.NORMAL2);
			Util.SetImageButtonInfo(button, "button_debug", "button_debug", "button_debug", 50, new Vector3(50f, -340f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnDebugPushed", UIButtonMessage.Trigger.OnClick);
			Color col = Color.white;
			if (ResourceManager.mABLoadError)
			{
				col = ResourceManager.mDebugButtonColor;
			}
			button.disabledColor = col;
			button.defaultColor = col;
			button.hover = col;
			button.pressed = col;
			button.UpdateColor(true);
		}
		if (GameMain.IsFirstPlayFromStartup)
		{
			ServerCram.SendDau(false);
		}
		GameMain.IsPlayable = true;
		mNetworkRequest = true;
		if (GameMain.USE_RESOURCEDL)
		{
			ResourceManager.UnloadAssetBundles();
		}
		SetLayout(Util.ScreenOrientation);
		mGame.FinishLoading();
		LoadGameStateFinished();
		yield return 0;
	}

	private IEnumerator LaceRotate(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		ResImage resTitleImage = ResourceManager.LoadImage("TITLE");
		UISprite sprite = Util.CreateSprite("Lace", parent, resTitleImage.Image);
		Util.SetSpriteInfo(sprite, "title_lace", depth, pos, scale, false);
		float angle = 0f;
		while (mLaceMove)
		{
			angle += Time.deltaTime * speed;
			sprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	public override IEnumerator UnloadGameState()
	{
		ResourceManager.UnloadSound("BGM_PUZZLE_CHANCE");
		mLaceMove = false;
		UnityEngine.Object.Destroy(mAnchorBottom.gameObject);
		mAnchorBottom = null;
		UnityEngine.Object.Destroy(mAnchorBottomLeft.gameObject);
		mAnchorBottomLeft = null;
		UnityEngine.Object.Destroy(mAnchorTop.gameObject);
		mAnchorTop = null;
		UnityEngine.Object.Destroy(mAnchorTopRight.gameObject);
		mAnchorTopRight = null;
		UnityEngine.Object.Destroy(mAnchorTopLeft.gameObject);
		mAnchorTopLeft = null;
		yield return 0;
		mBg00 = null;
		mLogo = null;
		mCharacter = null;
		mBack = null;
		mCopyright0 = null;
		mCopyright1 = null;
		mCopyright2 = null;
		yield return 0;
		ResourceManager.UnloadSsAnimationAll();
		yield return 0;
		ResourceManager.UnloadImageAll();
		yield return 0;
		UnloadGameStateFinished();
		yield return 0;
	}

	public void OnBackKeyPress()
	{
		ConfirmDialog confirmDialog = Util.CreateGameObject("QuitDialog", mRoot).AddComponent<ConfirmDialog>();
		confirmDialog.SetClosedCallback(OnQuitDialogClosed);
		confirmDialog.Init(Localization.Get("Quit_Title"), Localization.Get("Quit_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
		mState.Change(STATE.WAIT);
	}

	public void OnQuitDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			Application.Quit();
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	public void SetEnableButtonFlg(bool flg = true)
	{
		for (int i = 0; i < mTitleButtons.Count; i++)
		{
			BoxCollider component = mTitleButtons[i].GetComponent<BoxCollider>();
			if (component != null)
			{
				component.enabled = flg;
			}
		}
	}

	private void OnApplicationPause(bool pauseState)
	{
		if (!pauseState)
		{
			mNetworkRequest = true;
			mGame.mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
	}

	public void OnStartButtonPushed()
	{
		if (!GameMain.IsPlayable)
		{
			mNetworkRequest = true;
		}
		else
		{
			if (mState.GetStatus() != STATE.MAIN)
			{
				return;
			}
			UserDataInfo instance = UserDataInfo.Instance;
			if (instance.mShowInheriting)
			{
				ServerCram.SendDataTransferDialog(0, null, null, null, null);
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9001);
				mGame.Save();
				ShowInheritingConfirmDialog(InheritingConfirmDialog.REASON.STARTUP, 0);
				instance.mShowInheriting = false;
				instance.Save();
			}
			else if (string.IsNullOrEmpty(mGame.mUserUniqueID.TransferPass))
			{
				ShowRemindPasswordIssueDialog();
				if (instance.mShowPassword)
				{
					instance.mShowPassword = false;
					instance.Save();
				}
			}
			else if (instance.mShowPassword)
			{
				ShowTransferPasswordDialog();
				instance.mShowPassword = false;
				instance.Save();
			}
			else
			{
				mState.Change(STATE.MAINTENANCE_CHECK);
				mStateAfterMaintenanceCheck = STATE.EULA;
			}
		}
	}

	private void OnStart()
	{
		mState.Reset(STATE.WAIT, true);
		if (GameMain.IsRemoteTimeCheater)
		{
			int num = 0;
			if (GameMain.ServerTime.HasValue)
			{
				num = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(GameMain.ServerTime.Value);
			}
			int num2 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now.ToUniversalTime());
			int num3 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mGame.mPlayer.Data.LastPlayTime.ToUniversalTime());
			ServerCram.CheatUserSut(num, num2, num3, 0);
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmUUID", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("NO_UUID_Title"), Localization.Get("NO_UUID_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnUUIDErrorDialogClosed);
		}
		else
		{
			mGame.PlaySe("VOICE_002", -1);
			mGame.StopMusic(1, 0.2f);
			StopCoroutine("PlayGame");
			StartCoroutine("PlayGame");
		}
	}

	private IEnumerator PlayGame()
	{
		float startTime = Time.realtimeSinceStartup;
		mGame.mPlayer.Data.PlayCount++;
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		Def.SERIES prevSeries = mGame.mPlayer.Data.PreviousSeries;
		if (Def.IsEventSeries(currentSeries))
		{
			if (Def.IsEventSeries(prevSeries) || Def.IsAdvSeries(prevSeries))
			{
				mGame.mPlayer.SetNextSeries(Def.SERIES.SM_FIRST);
			}
			else
			{
				mGame.mPlayer.SetNextSeries(prevSeries);
			}
		}
		else if (Def.IsAdvSeries(currentSeries))
		{
			if (Def.IsEventSeries(prevSeries) || Def.IsAdvSeries(prevSeries))
			{
				mGame.mPlayer.SetNextSeries(Def.SERIES.SM_FIRST);
			}
			else
			{
				mGame.mPlayer.SetNextSeries(prevSeries);
			}
		}
		mGame.CheckRatingTutorial();
		mGame.CheckSeriesMaxLevel();
		float _start = Time.realtimeSinceStartup;
		MissionManager.SetMissionProgress(mGame.mPlayer.Data);
		MissionManager.ReflectPlayerData();
		foreach (SeasonEventSettings setting in mGame.EventProfile.SeasonEventList)
		{
			if (setting.EventID < 200 || setting.EventID >= 1000)
			{
				continue;
			}
			DateTime start = DateTimeUtil.UnixTimeStampToDateTime(setting.StartTime, true);
			DateTime end = DateTimeUtil.UnixTimeStampToDateTime(setting.EndTime, true);
			DateTime now = DateTimeUtil.Now();
			if (!mGame.mPlayer.Data.OldEventDataDict.ContainsKey((short)setting.EventID) && start <= now && now <= end)
			{
				mGame.mPlayer.Data.SetOldEventData((short)setting.EventID, new PlayerOldEventData
				{
					EventID = (short)setting.EventID,
					CloseTime = DateTimeUtil.UnitLocalTimeEpoch,
					Version = 1290,
					ExtendCount = 0,
					ExtendedOfferDone = true
				});
				int checkMinID2 = 203;
				checkMinID2 = 202;
				if (setting.EventID > checkMinID2)
				{
					mGame.mPlayer.Data.IsOldEventOpenGuide = true;
				}
			}
		}
		mGame.CorrectCompanionMaxLevel();
		mGame.Network_CheckSave();
		while (GameMain.IsServerSaving)
		{
			yield return 0;
		}
		mGame.SaveOptions();
		NotificationManager.Instance.playGame();
		if (GameMain.IsFirstPlayFromStartup)
		{
			ServerCram.SendDau(true);
		}
		Network.ItemPossessManager.SendItemPossessionCount();
		GameMain.IsFirstPlayFromStartup = false;
		while (Time.realtimeSinceStartup - startTime < 1f)
		{
			yield return 0;
		}
		mGame.StopMusic(0, 0.2f);
		bool useMask = false;
		if (mGame.mPlayer.PlayableMaxLevel == 100 && !mGame.mPlayer.IsStoryCompleted(0))
		{
			useMask = true;
		}
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP, useMask);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public void OnOptionButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mState.Change(STATE.MAINTENANCE_CHECK);
			mStateAfterMaintenanceCheck = STATE.OPTION;
		}
	}

	public void GoToOption()
	{
		mGame.StopMusic(1, 0.2f);
		mGame.StopMusic(0, 0.2f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.OPTION);
		mState.Reset(STATE.UNLOAD_WAIT, true);
	}

	private void OnOptionDialogClosed()
	{
	}

	public void OnSupportButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.StopMusic(0, 0.1f);
			mGame.PlaySe("SE_POSITIVE", -1);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.SUPPORT);
			mState.Change(STATE.UNLOAD_WAIT);
			GameStateTransfer.mOperationMode = GameStateTransfer.OperationMode.NORMAL;
		}
	}

	private void OnMainTennanceDialogClosed()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		mState.Change(STATE.MAIN);
		UnityEngine.Object.Destroy(mWebViewDialog.gameObject);
		mWebViewDialog = null;
	}

	private void OnMaintenanceCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state = 0)
	{
		mMaintenanceErrorDialog = null;
		if (i == ConnectCommonErrorDialog.SELECT_ITEM.RETRY)
		{
			mState.Change(STATE.MAINTENANCE_CHECK);
			return;
		}
		mState.Change(STATE.MAIN);
		mStateAfterMaintenanceCheck = STATE.MAIN;
	}

	private void OnHelpDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mWebViewDialog.ShowWebview(true);
		}
		else
		{
			mWebViewDialog.ShowWebview(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
			mConfirmDialog.SetClosedCallback(OnHelpErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	private void OnHelpErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (mWebViewDialog != null)
		{
			switch (item)
			{
			case ConfirmDialog.SELECT_ITEM.POSITIVE:
				mWebViewDialog.Reload();
				break;
			case ConfirmDialog.SELECT_ITEM.NEGATIVE:
				mWebViewDialog.Close();
				break;
			}
		}
		mConfirmDialog = null;
	}

	private void OnTermOfUsePushed()
	{
		if (!(mWebViewDialog != null))
		{
			mState.Change(STATE.EULA_TERM_OF_USE);
			GameObject gameObject = Util.CreateGameObject("WebViewDialog_EULA", mRoot);
			mWebViewDialog = gameObject.AddComponent<WebViewDialog>();
			mWebViewDialog.Title = Localization.Get("EULA_WebTitle");
			mWebViewDialog.SetClosedCallback(OnWebViewDialogEULAClosed);
			mWebViewDialog.SetPageMovedCallback(OnWebViewDialogEULAPageMoved);
			mWebViewDialog.SetBaseDepth(120);
			mWebViewDialog.SetTitleLabelSize(28);
		}
	}

	public void OnEULADialogClosed(EULADialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		SetEnableButtonFlg();
		mEULADialog = null;
		if (item == EULADialog.SELECT_ITEM.POSITIVE)
		{
			GameMain.SetShowEULAVersion();
			mState.Change(STATE.GDPR);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
		{
			if (item == EULADialog.SELECT_ITEM.POSITIVE)
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9011);
			}
			else
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9012);
			}
			mGame.Save();
		}
	}

	public void OnEulaAcceptDialogClosed(EulaAcceptDialog.SELECT_ITEM item)
	{
		mEulaAcceptDialog = null;
		if (item == EulaAcceptDialog.SELECT_ITEM.POSITIVE)
		{
			OnEULADialogClosed(EULADialog.SELECT_ITEM.POSITIVE);
		}
		else
		{
			OnEULADialogClosed(EULADialog.SELECT_ITEM.NEGATIVE);
		}
	}

	private void OnWebViewDialogEULA()
	{
		if (mWebViewDialog == null)
		{
			mState.Change(STATE.MAIN);
		}
		else if (mWebViewDialog.IsOpend)
		{
			mState.Reset(STATE.WAIT, true);
			mWebViewDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_PAGE);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.EULA_FileURL);
			mWebViewDialog.LoadURL(stringBuilder.ToString());
		}
	}

	private void OnWebViewDialogEULAClosed()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		mState.Change(STATE.EULA);
		if (mWebViewDialog != null)
		{
			UnityEngine.Object.Destroy(mWebViewDialog.gameObject);
			mWebViewDialog = null;
		}
	}

	private void OnWebViewDialogEULAPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mWebViewDialog.ShowWebview(true);
		}
		else
		{
			mWebViewDialog.ShowWebview(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
			mConfirmDialog.SetClosedCallback(OnHelpErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(150);
		}
	}

	public void OnGDPRAcceptDialogClosed(List<GDPRAcceptInfo> aAcceptList)
	{
		Dictionary<int, GDPRAcceptInfo> gDPRAcceptDict = mGame.mPlayer.Data.GDPRAcceptDict;
		string mTokenAD = mGame.GameProfile.mGDPRSetting.mTokenAD;
		List<string> list = new List<string>();
		List<int> list2 = new List<int>();
		for (int i = 0; i < aAcceptList.Count; i++)
		{
			int mKind = aAcceptList[i].mKind;
			if (gDPRAcceptDict.ContainsKey(mKind))
			{
				gDPRAcceptDict[mKind] = aAcceptList[i];
			}
			else
			{
				gDPRAcceptDict.Add(mKind, aAcceptList[i]);
			}
			list.Add(aAcceptList[i].mAdjustKey);
			int item = 0;
			if (aAcceptList[i].mAccept)
			{
				item = 1;
			}
			list2.Add(item);
		}
		mGame.Save();
		SingletonMonoBehaviour<Marketing>.Instance.AdjustSendGDPR(mTokenAD, list, list2);
		mState.Change(STATE.PURCHASE_PRICE);
	}

	public void OnMoreGamesButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			Application.OpenURL(SingletonMonoBehaviour<Network>.Instance.MoreGames_URL);
		}
	}

	public void OnFacebookButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mSNS = SocialManager.Instance[SNS.Facebook];
			if (GameMain.IsSNSLogined(mSNS))
			{
				mState.Reset(STATE.NETWORK_LOGOUT, true);
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	public void SetFacebookName()
	{
		if (!(mFacebookName == null))
		{
			BIJSNS sns = SocialManager.Instance[SNS.Facebook];
			if (GameMain.IsSNSLogined(sns))
			{
				string fbName = mGame.mOptions.FbName;
				mFacebookName.text = fbName;
			}
			else
			{
				mFacebookName.text = Localization.Get("Facebook_Login");
			}
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void OnTransferButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.StopMusic(0, 0.1f);
			mGame.PlaySe("SE_POSITIVE", -1);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TRANSFER);
			mState.Change(STATE.UNLOAD_WAIT);
			GameStateTransfer.mOperationMode = GameStateTransfer.OperationMode.NORMAL;
		}
	}

	private void OnContinueTransferDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mGame.StopMusic(0, 0.1f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TRANSFER);
		mState.Change(STATE.UNLOAD_WAIT);
		GameStateTransfer.mOperationMode = GameStateTransfer.OperationMode.NORMAL;
	}

	private void OnTransferedInitDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		GameMain.mUserAuthTransfered = false;
		GameMain.IsPlayable = true;
		mState.Change(STATE.MAIN);
	}

	private void OnMaintenanceMode()
	{
		mInMaintenance = true;
		mStateAfterMaintenanceCheck = STATE.MAIN;
		mState.Reset(STATE.WAIT, true);
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string arg = string.Empty;
		ConfirmDialog.CONFIRM_DIALOG_STYLE style = ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY;
		if (!string.IsNullOrEmpty(GameMain.MaintenanceURL))
		{
			arg = Localization.Get("MaintenrMode_Desc_URL");
			style = ConfirmDialog.CONFIRM_DIALOG_STYLE.CONFIRM_ONLY;
		}
		string desc = string.Format(Localization.Get("MaintenanceMode_Desc"), arg);
		if (!string.IsNullOrEmpty(GameMain.MaintenanceMessage))
		{
			desc = GameMain.MaintenanceMessage;
		}
		mConfirmDialog.Init(Localization.Get("MaintenanceMode_Title"), desc, style);
		mConfirmDialog.SetClosedCallback(OnMaintenanceModeDialogClosed);
		mConfirmDialog.SetBaseDepth(70);
	}

	public void OnMaintenanceModeDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mState.Reset(STATE.MAINTENANCE, true);
		mGame.PlaySe("SE_POSITIVE", -1);
		if (!string.IsNullOrEmpty(GameMain.MaintenanceURL))
		{
			GameObject gameObject = Util.CreateGameObject("MaintenanceDialog", mRoot);
			mWebViewDialog = gameObject.AddComponent<WebViewDialog>();
			mWebViewDialog.Title = Localization.Get("Maintenance_Title");
			mWebViewDialog.SetClosedCallback(OnMainTennanceDialogClosed);
			mWebViewDialog.SetPageMovedCallback(OnHelpDialogPageMoved);
		}
		else
		{
			mState.Reset(STATE.MAIN, true);
		}
		mConfirmDialog = null;
	}

	private void OnMaintenanceDialog()
	{
		if (mWebViewDialog == null)
		{
			mState.Change(STATE.MAIN);
		}
		else if (mWebViewDialog.IsOpend)
		{
			mState.Reset(STATE.WAIT, true);
			mWebViewDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_PAGE);
			StringBuilder stringBuilder = new StringBuilder();
			Dictionary<string, object> p = new Dictionary<string, object>();
			stringBuilder.Append(GameMain.MaintenanceURL);
			string supportParams = Network.GetSupportParams(ref p);
			if (p.Count > 0)
			{
				stringBuilder.Append("?");
				stringBuilder.Append(supportParams);
			}
			mWebViewDialog.LoadURL(stringBuilder.ToString());
		}
	}

	private void OnUpdateVersionConfrim()
	{
		if (mState.GetStatus() == STATE.NETWORK_00)
		{
			mState.Reset(STATE.WAIT, true);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("UpdateVersion_Confirm_Title"), Localization.Get("UpdateVersion_Confirm_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			mConfirmDialog.SetClosedCallback(OnUpdateConfirmDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	public void OnUpdateConfirmDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			string url = string.Format(SingletonMonoBehaviour<Network>.Instance.Store_URL, BIJUnity.getCountryCode());
			Application.OpenURL(url);
			GameMain.ApplicationQuit();
		}
		else
		{
			mState.Change(STATE.NETWORK_EPILOGUE);
		}
		mConfirmDialog = null;
	}

	private void OnUpdateVersionForce()
	{
		if (mState.GetStatus() == STATE.NETWORK_00)
		{
			mState.Reset(STATE.WAIT, true);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("UpdateVersion_Force_Title"), Localization.Get("UpdateVersion_Force_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnUpdateForceDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	public void OnUpdateForceDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		string url = string.Format(SingletonMonoBehaviour<Network>.Instance.Store_URL, BIJUnity.getCountryCode());
		Application.OpenURL(url);
		GameMain.ApplicationQuit();
		mConfirmDialog = null;
	}

	public void ShowTransferPasswordDialog()
	{
		mState.Reset(STATE.WAIT, true);
		StartCoroutine(GrayOut());
		TransferPasswordDialog transferPasswordDialog = Util.CreateGameObject("TransferPasswordDialog", mRoot).AddComponent<TransferPasswordDialog>();
		transferPasswordDialog.Init(OnTransferPasswordDialogClosed);
		transferPasswordDialog.SetBaseDepth(70);
	}

	public void OnTransferPasswordDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.MAINTENANCE_CHECK);
		mStateAfterMaintenanceCheck = STATE.EULA;
	}

	public void ShowRemindPasswordIssueDialog()
	{
		mState.Reset(STATE.WAIT, true);
		StartCoroutine(GrayOut());
		RemindPasswordIssueDialog remindPasswordIssueDialog = Util.CreateGameObject("RemindPasswordIssueDialog", mRoot).AddComponent<RemindPasswordIssueDialog>();
		remindPasswordIssueDialog.Init(OnRemindPasswordIssueDialogClosed);
		remindPasswordIssueDialog.SetBaseDepth(70);
	}

	public void OnRemindPasswordIssueDialogClosed(RemindPasswordIssueDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		if (item == RemindPasswordIssueDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Reset(STATE.MAIN, true);
			OnTransferButtonPushed();
			GameStateTransfer.mOperationMode = GameStateTransfer.OperationMode.AUTO_TRANSFER;
		}
		else
		{
			mState.Change(STATE.MAINTENANCE_CHECK);
			mStateAfterMaintenanceCheck = STATE.EULA;
		}
	}

	public void ShowInheritingConfirmDialog(InheritingConfirmDialog.REASON aReason, int aMaltaReason)
	{
		mState.Reset(STATE.WAIT, true);
		StartCoroutine(GrayOut());
		SetEnableButtonFlg(false);
		InheritingConfirmDialog inheritingConfirmDialog = Util.CreateGameObject("InheritingConfirmDialog", mRoot).AddComponent<InheritingConfirmDialog>();
		inheritingConfirmDialog.Init(aReason, aMaltaReason, OnInheritingConfirmDialogClosed);
		inheritingConfirmDialog.SetBaseDepth(70);
	}

	public void OnInheritingConfirmDialogClosed(InheritingConfirmDialog.SELECT_ITEM item, InheritingConfirmDialog.REASON reason, int maltaReason)
	{
		SetEnableButtonFlg();
		StartCoroutine(GrayIn());
		ServerCram.DataTransferSelect(maltaReason, (int)item);
		UserDataInfo instance = UserDataInfo.Instance;
		instance.mDataState = UserDataInfo.DATA_STATE.LOADED;
		instance.mAuthKeyState = UserDataInfo.DATA_STATE.LOADED;
		if (reason == InheritingConfirmDialog.REASON.AUTH_ERROR)
		{
			GameMain.mUserAuthTransfered = false;
		}
		if (item == InheritingConfirmDialog.SELECT_ITEM.NEWGAME)
		{
			switch (reason)
			{
			case InheritingConfirmDialog.REASON.STARTUP:
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9002);
				mGame.Save();
				mState.Change(STATE.MAINTENANCE_CHECK);
				mStateAfterMaintenanceCheck = STATE.EULA;
				break;
			case InheritingConfirmDialog.REASON.LOAD_ERROR:
			case InheritingConfirmDialog.REASON.AUTHKEY_ERROR:
				GameMain.IsPlayable = true;
				mState.Change(STATE.MAIN);
				break;
			case InheritingConfirmDialog.REASON.AUTH_ERROR:
				GameMain.IsPlayable = true;
				mState.Change(STATE.MAIN);
				break;
			}
		}
		else
		{
			if (reason == InheritingConfirmDialog.REASON.STARTUP)
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9003);
				mGame.Save();
			}
			mState.Reset(STATE.MAIN, true);
			OnTransferButtonPushed();
			GameStateTransfer.mOperationMode = GameStateTransfer.OperationMode.AUTO_INHERITING;
			GameStateTransfer.mTransferReason = maltaReason;
			Server.ManualConnecting = false;
		}
	}

	public void CheckRollback()
	{
		UserDataInfo instance = UserDataInfo.Instance;
		Player mPlayer = SingletonMonoBehaviour<GameMain>.Instance.mPlayer;
		Player player = null;
		AdvSaveData aAdvBackupData = null;
		bool flag = false;
		bool flag2 = false;
		if (instance.mDataState == UserDataInfo.DATA_STATE.LOADED)
		{
			player = mGame.GetBackupPlayerData();
			if (instance.mBackupDataState == UserDataInfo.DATA_STATE.LOADED && player.Data.Version <= 1290 && (player.TotalStars > mPlayer.TotalStars || player.TotalScoreToS > mPlayer.TotalScoreToS || (player.TotalTrophy != mPlayer.TotalTrophy && player.Experience > mPlayer.Experience)))
			{
				flag = true;
			}
		}
		if (flag || flag2)
		{
			int num = 0;
			int num2 = 0;
			if (flag)
			{
				num++;
			}
			if (flag2)
			{
				num += 2;
			}
			if (instance.mBackupDataState == UserDataInfo.DATA_STATE.LOADED)
			{
				num2++;
			}
			if (instance.mBackupAdvDataState == UserDataInfo.DATA_STATE.LOADED)
			{
				num2 += 2;
			}
			ServerCram.SendDataRollBack(num, num2, mPlayer, mPlayer.AdvSaveData, player, aAdvBackupData);
		}
	}

	private void OnUUIDErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		mState.Reset(STATE.NETWORK_AUTH_CHECK, true);
	}

	public void OnDebugPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.StopMusic(0, 0.2f);
			SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
			sMDDebugDialog.SetGameState(this);
			sMDDebugDialog.SetClosedCallback(delegate
			{
				mGame.PlayMusic("BGM_PUZZLE_CHANCE", 0, true);
			});
		}
	}

	private void OnConnectErrorRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorAuthAbandon()
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		mState.Change(STATE.MAIN);
	}

	private void OnAuthSuccessDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
	}

	private void CreateAdvButton(int baseDepth)
	{
		GameObject parent = mAnchorTopLeft.gameObject;
		UIFont atlasFont = GameMain.LoadFont();
		float x = 100f;
		float y = -200f;
		UIButton uIButton = Util.CreateJellyImageButton("advbutton", parent, ResourceManager.LoadImage("HUD").Image);
		Util.SetImageButtonInfo(uIButton, "button_comingsoon", "button_comingsoon", "button_comingsoon", baseDepth + 2, new Vector3(x, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnAdvPushed", UIButtonMessage.Trigger.OnClick);
		UILabel uILabel = Util.CreateLabel("label", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, "GameCenter\nEnterance\n↓", baseDepth + 3, new Vector3(0f, 40f, 0f), 20, 0, 0, UIWidget.Pivot.Bottom);
		uILabel.color = Color.yellow;
		uILabel.effectColor = Color.red;
		uILabel.effectDistance = new Vector2(2f, 2f);
		uILabel.effectStyle = UILabel.Effect.Outline8;
		uILabel.spacingX = 3;
	}

	public void OnAdvPushed(GameObject go)
	{
		mState.Reset(STATE.WAIT, true);
		mGame.PlaySe("SE_POSITIVE", -1);
		MissionManager.SetMissionProgress(mGame.mPlayer.Data);
		MissionManager.ReflectPlayerData();
		mGame.mAdvMode = true;
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.ADVMENU);
	}
}
