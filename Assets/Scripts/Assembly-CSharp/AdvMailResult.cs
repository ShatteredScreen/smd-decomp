public class AdvMailResult
{
	[MiniJSONAlias("id")]
	public int id { get; set; }

	[MiniJSONAlias("from_uuid")]
	public int from_uuid { get; set; }

	[MiniJSONAlias("message")]
	public string message { get; set; }

	[MiniJSONAlias("category")]
	public int category { get; set; }

	[MiniJSONAlias("sub_category")]
	public int sub_category { get; set; }

	[MiniJSONAlias("itemid")]
	public int itemid { get; set; }

	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }

	[MiniJSONAlias("mail_type")]
	public int mail_type { get; set; }

	[MiniJSONAlias("ctime")]
	public int ctime { get; set; }
}
