using System;
using System.Reflection;
using System.Text;

public class NetworkProfile : ICloneable
{
	public bool CheckReceipt
	{
		get
		{
			return 1 == arck;
		}
	}

	public bool bl { get; set; }

	public int pd { get; set; }

	public int av { get; set; }

	public int adv { get; set; }

	public int afu { get; set; }

	public int iv { get; set; }

	public int idv { get; set; }

	public int ifu { get; set; }

	public int arck { get; set; }

	public int irck { get; set; }

	public int asto { get; set; }

	public int isto { get; set; }

	public int gifrq { get; set; }

	public int getfs { get; set; }

	public int log { get; set; }

	public string useg { get; set; }

	public int trans_flag { get; set; }

	public int trans_mode { get; set; }

	public string trans_text { get; set; }

	public string trans_url { get; set; }

	public int google_flag { get; set; }

	public int gp_flag { get; set; }

	public int gpgs_flag { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public NetworkProfile Clone()
	{
		return MemberwiseClone() as NetworkProfile;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[NetworkProfile: {0}]", stringBuilder.ToString());
	}
}
