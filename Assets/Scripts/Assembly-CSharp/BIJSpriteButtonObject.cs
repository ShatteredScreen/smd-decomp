using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(BIJSpriteObject))]
public class BIJSpriteButtonObject : MonoBehaviour
{
	public delegate void EventFunc(GameObject go);

	[SerializeField]
	public bool Enable = true;

	[SerializeField]
	public int UpImageIndex = -1;

	[SerializeField]
	public int DownImageIndex = -1;

	[SerializeField]
	public int DisableImageIndex = -1;

	private BIJSpriteObject mSpriteObject;

	[method: MethodImpl(32)]
	public event EventFunc OnPushDown;

	[method: MethodImpl(32)]
	public event EventFunc OnPushUp;

	public virtual void Init()
	{
		if (Enable)
		{
			mSpriteObject.Index = UpImageIndex;
		}
		else
		{
			mSpriteObject.Index = ((DisableImageIndex == -1) ? UpImageIndex : DisableImageIndex);
		}
	}

	public virtual void Uninit()
	{
	}

	private void Awake()
	{
		mSpriteObject = GetComponent<BIJSpriteObject>();
		mSpriteObject.Clickable = true;
		mSpriteObject.OnEventFunc += OnPushEvent;
		Init();
	}

	private void Update()
	{
	}

	private void OnDestroy()
	{
		Uninit();
	}

	private void OnPushEvent(int param)
	{
		switch (param)
		{
		case 0:
			if (Enable && DownImageIndex != -1)
			{
				mSpriteObject.Index = DownImageIndex;
			}
			if (this.OnPushDown != null)
			{
				this.OnPushDown(base.gameObject);
			}
			break;
		case 1:
			if (UpImageIndex != -1)
			{
				mSpriteObject.Index = UpImageIndex;
			}
			if (this.OnPushUp != null)
			{
				this.OnPushUp(base.gameObject);
			}
			break;
		}
	}
}
