using System;
using System.Collections.Generic;
using System.IO;

public class BIJPackOffset
{
	private List<BIJImageOffset> mOffsets;

	public byte Format;

	public int Count
	{
		get
		{
			return (mOffsets != null) ? mOffsets.Count : 0;
		}
	}

	public BIJImageOffset this[int index]
	{
		get
		{
			if (mOffsets == null || index < 0 || index >= mOffsets.Count)
			{
				throw new IndexOutOfRangeException();
			}
			return mOffsets[index];
		}
	}

	public BIJPackOffset(string name)
	{
		Format = 0;
		mOffsets = null;
		short num = 0;
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			Format = bIJResourceBinaryReader.ReadByte();
			num = bIJResourceBinaryReader.ReadShort();
			mOffsets = new List<BIJImageOffset>();
			for (int i = 0; i < num; i++)
			{
				BIJImageOffset bIJImageOffset = new BIJImageOffset();
				if (Format == 2)
				{
					bIJImageOffset.name = bIJResourceBinaryReader.ReadUTF();
				}
				bIJImageOffset.x = bIJResourceBinaryReader.ReadShort();
				bIJImageOffset.y = bIJResourceBinaryReader.ReadShort();
				bIJImageOffset.width = bIJResourceBinaryReader.ReadShort();
				bIJImageOffset.height = bIJResourceBinaryReader.ReadShort();
				if (Format == 2)
				{
					bIJImageOffset.paddingLeft = bIJResourceBinaryReader.ReadShort();
					bIJImageOffset.paddingTop = bIJResourceBinaryReader.ReadShort();
					bIJImageOffset.paddingRight = bIJResourceBinaryReader.ReadShort();
					bIJImageOffset.paddingBottom = bIJResourceBinaryReader.ReadShort();
				}
				mOffsets.Add(bIJImageOffset);
			}
		}
		if (Count != num)
		{
			if (mOffsets != null)
			{
				mOffsets.Clear();
			}
			mOffsets = null;
			throw new IOException();
		}
	}

	public BIJImageOffset OffsetOf(int index)
	{
		try
		{
			return this[index];
		}
		catch (IndexOutOfRangeException)
		{
		}
		return null;
	}
}
