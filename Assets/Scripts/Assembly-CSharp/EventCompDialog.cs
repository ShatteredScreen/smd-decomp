using System.Collections;
using UnityEngine;

public class EventCompDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0
	}

	public enum STATE
	{
		INIT = 0,
		ANIMATION_HIT = 1,
		ANIMATION_HIT_WAIT = 2,
		ANIMATION_WIN = 3,
		ANIMATION_WIN_WAIT = 4,
		MAIN = 5,
		MAIN2 = 6,
		WAIT = 7
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private OnDialogClosed mCallback;

	private Partner mPartner;

	private CompanionData mCompanionData;

	private BIJImage atlas;

	private UISsSprite mPartnerHalo;

	private UIButton mButton;

	private float mStateTime;

	private new string mLoadImageAtlas;

	private Def.EVENT_TYPE mEventType = Def.EVENT_TYPE.SM_NONE;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(int a_companionID, string a_atlas, Def.EVENT_TYPE a_eventType = Def.EVENT_TYPE.SM_NONE)
	{
		mCompanionData = mGame.mCompanionData[a_companionID];
		if (mCompanionData == null)
		{
			BIJLog.E("Companion Data is NULL !!!");
		}
		mLoadImageAtlas = a_atlas;
		mEventType = a_eventType;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mState.Change(STATE.ANIMATION_HIT);
			break;
		case STATE.ANIMATION_HIT:
			BuildDialogInternal();
			mState.Change(STATE.ANIMATION_HIT_WAIT);
			break;
		case STATE.ANIMATION_HIT_WAIT:
			if (mPartner.IsMotionFinished())
			{
				mState.Change(STATE.ANIMATION_WIN);
			}
			break;
		case STATE.ANIMATION_WIN:
			mPartner.StartMotion(CompanionData.MOTION.WIN, true);
			mState.Change(STATE.ANIMATION_WIN_WAIT);
			break;
		case STATE.ANIMATION_WIN_WAIT:
			if (mState.IsChanged())
			{
				mStateTime = 0f;
			}
			mStateTime += Time.deltaTime;
			if (mStateTime > 0.2f)
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.MAIN:
			mButton = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
			Util.SetImageButtonInfo(mButton, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButton, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
			mState.Change(STATE.MAIN2);
			break;
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		mPartner.transform.localPosition = Vector3.zero;
		int level = mGame.mPlayer.GetCompanionLevel(mCompanionData.index);
		mPartner.Init(mCompanionData.index, Vector3.zero, 1f, level, base.gameObject, true, mBaseDepth + 2);
		while (!mPartner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
	}

	private void BuildDialogInternal()
	{
		UIFont atlasFont = GameMain.LoadFont();
		string empty = string.Empty;
		empty = ((mEventType != Def.EVENT_TYPE.SM_LABYRINTH) ? Localization.Get(mGame.mOldEventMode ? "ORevComp_ORev_Desc" : ((!mGame.IsDailyChallengePuzzle) ? "ORevComp_Desc" : "DC_Comp_Desc")) : Localization.Get("Labyrinth_Comp_Desc"));
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, empty, mBaseDepth + 2, new Vector3(0f, -155f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.spacingY = 6;
		uILabel.SetDimensions(550, 60);
		mPartnerHalo = Util.CreateGameObject("PartnerHalo", base.gameObject).AddComponent<UISsSprite>();
		mPartnerHalo.Animation = ResourceManager.LoadSsAnimation("MAP_HALO").SsAnime;
		mPartnerHalo.depth = mBaseDepth + 1;
		mPartnerHalo.gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1f);
		mPartnerHalo.gameObject.transform.localPosition = new Vector3(0f, 35f, 0f);
		mPartnerHalo.Play();
		mPartner.SetPartnerUIScreenDepth(mBaseDepth + 5);
		if (mCompanionData.IsTall())
		{
			mPartner.SetPartnerScreenScale(0.5f);
		}
		else
		{
			mPartner.SetPartnerScreenScale(0.58f);
		}
		mPartner.SetPartnerScreenPos(new Vector3(0f, -98f, 0f));
		mPartner.SetPartnerScreenEnable(true);
		mPartner.MakeUIShadow(atlas, "character_foot2", mBaseDepth + 3);
	}

	public override void BuildDialog()
	{
		atlas = ResourceManager.LoadImage("HUD").Image;
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		string empty = string.Empty;
		ResImage resImage = ResourceManager.LoadImage(mLoadImageAtlas);
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(empty);
		UISprite sprite = Util.CreateSprite("TitleSprite", base.gameObject, resImage.Image);
		Util.SetSpriteInfo(sprite, "LC_complete_title", mBaseDepth + 2, new Vector3(0f, 290f, 0f), Vector3.one, false);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN2)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void OnOpenFinished()
	{
		mGame.Network_NicknameEdit(string.Empty);
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mPartner != null)
		{
			Object.Destroy(mPartner.gameObject);
		}
		if (mPartnerHalo != null)
		{
			Object.Destroy(mPartnerHalo.gameObject);
		}
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.MAIN2, true);
		}
	}

	private void OnLevelUpAnimationFinished(UISsSprite sprite)
	{
		mState.Change(STATE.ANIMATION_HIT);
	}
}
