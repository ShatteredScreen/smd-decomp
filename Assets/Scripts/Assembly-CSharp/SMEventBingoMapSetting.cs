using System.Collections.Generic;
using System.Text;

public class SMEventBingoMapSetting : SMMapPageSettingBase
{
	public string EventLive2d = string.Empty;

	public string ReLotteryLive2d = string.Empty;

	public List<int> CourseOpenKeyIdList;

	public int FirstGetBingoNum;

	public int SecondGetBingoNum;

	public int FreeTicket;

	public string EventLive2dMotionLv2 = string.Empty;

	public string EventLive2dMotionLv5 = string.Empty;

	public string EventLive2dMotionComp = string.Empty;

	public int EventCockpitCharaID = -1;

	public SMEventBingoMapSetting()
	{
		CourseOpenKeyIdList = new List<int>();
	}

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteUTF(EventLive2d);
		a_writer.WriteUTF(ReLotteryLive2d);
		a_writer.WriteInt(CourseOpenKeyIdList.Count + 1);
		a_writer.WriteInt(0);
		for (int i = 0; i < CourseOpenKeyIdList.Count; i++)
		{
			a_writer.WriteInt(CourseOpenKeyIdList[i]);
		}
		a_writer.WriteInt(FirstGetBingoNum);
		a_writer.WriteInt(SecondGetBingoNum);
		a_writer.WriteInt(FreeTicket);
		a_writer.WriteUTF(EventLive2dMotionLv2);
		a_writer.WriteUTF(EventLive2dMotionLv5);
		a_writer.WriteUTF(EventLive2dMotionComp);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		EventLive2d = a_reader.ReadUTF();
		ReLotteryLive2d = a_reader.ReadUTF();
		int num = a_reader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			CourseOpenKeyIdList.Add(a_reader.ReadInt());
		}
		FirstGetBingoNum = a_reader.ReadInt();
		SecondGetBingoNum = a_reader.ReadInt();
		FreeTicket = a_reader.ReadInt();
		EventLive2dMotionLv2 = a_reader.ReadUTF();
		EventLive2dMotionLv5 = a_reader.ReadUTF();
		EventLive2dMotionComp = a_reader.ReadUTF();
		if (string.IsNullOrEmpty(EventLive2dMotionLv2))
		{
			EventLive2dMotionLv2 = "motions/wait00.mtn";
		}
		if (string.IsNullOrEmpty(EventLive2dMotionLv5))
		{
			EventLive2dMotionLv5 = "motions/wait00.mtn";
		}
		if (string.IsNullOrEmpty(EventLive2dMotionComp))
		{
			EventLive2dMotionComp = EventLive2dMotionLv5;
		}
		try
		{
			if (!string.IsNullOrEmpty(EventLive2d))
			{
				EventCockpitCharaID = int.Parse(EventLive2d);
			}
		}
		catch
		{
			StringBuilder stringBuilder = new StringBuilder("Parse Error:");
			stringBuilder = stringBuilder.Append(EventLive2d);
			BIJLog.E(stringBuilder.ToString());
			EventCockpitCharaID = -1;
		}
	}
}
