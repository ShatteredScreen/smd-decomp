using UnityEngine;

public class SsFpsCounter : MonoBehaviour
{
	private float updateInterval = 0.5f;

	private double lastInterval;

	private int frames;

	private float fps;

	public void Start()
	{
		lastInterval = Time.realtimeSinceStartup;
		frames = 0;
	}

	public void OnGUI()
	{
		GUILayout.Label(string.Empty + fps.ToString("f2"));
	}

	public void Update()
	{
		frames++;
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		if ((double)realtimeSinceStartup > lastInterval + (double)updateInterval)
		{
			fps = (float)frames / (float)((double)realtimeSinceStartup - lastInterval);
			frames = 0;
			lastInterval = realtimeSinceStartup;
		}
	}
}
