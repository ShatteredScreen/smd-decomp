using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class UISsPartP : IComparable<UISsPartP>
{
	public delegate void KeyframeCallback(UISsPartP part, SsAttrValueInterface value);

	public UISsPartPDraw mUISsPartDraw;

	internal UISsSpriteP _mgr;

	internal SsPartRes _res;

	internal UISsPartP _parent;

	internal int _priority;

	internal bool _visible;

	private bool _forceVisibleAvailable;

	private bool _forceVisible = true;

	private bool _forceAlphaAvailable;

	private float _forceAlpha = 1f;

	internal Vector3 _pos;

	internal Quaternion _quaternion;

	internal Vector3 _scale;

	internal Transform _transform;

	internal Material _material;

	internal Matrix4x4 _pivotMatrix;

	private Vector3[] _orgVertices;

	internal Vector3[] _vertPositions;

	private int[] _triIndices;

	private int _index;

	public int _vIndex;

	private Vector3 _rootPivot;

	private int _subMeshIndex;

	public int _imgOfsX;

	public int _imgOfsY;

	public int _imgOfsW;

	public int _imgOfsH;

	public Vector3 _originOffset = Vector3.zero;

	private bool _hasTransparency;

	internal bool _flipH;

	internal bool _flipV;

	private static int[,] _flippedUvIndices = new int[3, 4]
	{
		{ 1, 0, 3, 2 },
		{ 3, 2, 1, 0 },
		{ 2, 3, 0, 1 }
	};

	private SsColorBlendKeyValue _colorBlendKeyValue;

	private Material _originalMaterial;

	private Material _individualMaterial;

	private Color _materialColor;

	private Color _vertexColor;

	private float _alpha;

	private bool _useCgShader;

	private SsShaderType _shaderType;

	private SsColorBlendOperation _colorBlendType;

	public bool modifyVertex;

	public bool directSizeSet;

	private List<SsSubAnimeController> _subAnimes = new List<SsSubAnimeController>();

	private List<SsPartRes> _subAnimePartRes = new List<SsPartRes>();

	private static Vector3 sVec3Min = new Vector3(float.MinValue, float.MinValue);

	private static Vector3 sVec3Max = new Vector3(float.MaxValue, float.MaxValue);

	internal bool ExistsOnUserDataKey
	{
		get
		{
			return this.OnUserDataKey != null;
		}
	}

	internal bool ExistsOnSoundKey
	{
		get
		{
			return this.OnSoundKey != null;
		}
	}

	public UISsSpriteP Sprite
	{
		get
		{
			return _mgr;
		}
	}

	internal Mesh _mesh
	{
		get
		{
			return (!(_mgr != null)) ? null : _mgr._mesh;
		}
	}

	public Matrix4x4 RootToPartMatrix
	{
		get
		{
			return _pivotMatrix;
		}
	}

	public SsColorBlendOperation ColorBlendType
	{
		get
		{
			return _colorBlendType;
		}
		set
		{
			_colorBlendType = value;
			_shaderType = SsShaderManager.EnumToType(value, _res.AlphaBlendType, SsMaterialColorBlendOperation.Non);
			_material = _res.imageFile.GetMaterial(_shaderType);
			_mgr._materials[_subMeshIndex] = _material;
			_mgr._matChanged = true;
		}
	}

	private float AlphaValue
	{
		get
		{
			return _alpha;
		}
		set
		{
			_alpha = value;
			if (_useCgShader && ColorBlendType != 0)
			{
				for (int i = 0; i < 4; i++)
				{
					_mgr._extras[_vIndex + i][1] = value;
				}
				_mgr._extraChanged = true;
			}
			else
			{
				for (int j = 0; j < 4; j++)
				{
					_mgr._colors[_vIndex + j].a = value;
				}
				_mgr._colorChanged = true;
			}
		}
	}

	[method: MethodImpl(32)]
	public event KeyframeCallback OnUserDataKey;

	[method: MethodImpl(32)]
	public event KeyframeCallback OnSoundKey;

	public UISsPartP(UISsSpriteP manager, int index, SsPartRes partRes, SsImageFile imageFile, UISsPartPDraw uisspartdraw)
	{
		_mgr = manager;
		_index = index;
		_vIndex = (index - 1) * 4;
		_subMeshIndex = index - 1;
		_res = partRes;
		mUISsPartDraw = uisspartdraw;
		if (_res.HasParent)
		{
			_parent = _mgr.Sprite(_res.ParentId);
			if (_parent == null)
			{
				UnityEngine.Debug.LogError("##### parent sprite must be created already!!");
				UnityEngine.Debug.Break();
				return;
			}
		}
		_pivotMatrix = Matrix4x4.identity;
		switch (_res.Type)
		{
		case SsPartType.Root:
			_rootPivot = new Vector3(0f - _res.PosX(0), _res.PosY(0), 0f);
			break;
		case SsPartType.Normal:
		case SsPartType.Bound:
		{
			_vertPositions = new Vector3[4];
			_orgVertices = new Vector3[4];
			for (int i = 0; i < _vertPositions.Length; i++)
			{
				_orgVertices[i] = (_vertPositions[i] = _res.OrgVertices[i]);
			}
			break;
		}
		}
		_visible = !_res.Hide(0);
		_flipH = false;
		_flipV = false;
		_pos = Vector3.zero;
		_quaternion = Quaternion.identity;
		_scale = Vector3.one;
		if (_res.Type != 0 && _res.Type != SsPartType.Bound)
		{
			return;
		}
		_useCgShader = SystemInfo.graphicsShaderLevel >= 20;
		if (_res.Type == SsPartType.Bound)
		{
			_vertexColor = new Color(1f, 0f, 0f, 1f);
			for (int j = 0; j < 4; j++)
			{
				_mgr._uvs[_vIndex + j] = Vector2.zero;
				_mgr._colors[_vIndex + j] = _vertexColor;
			}
			_visible = false;
		}
		else
		{
			_vertexColor = new Color(1f, 1f, 1f, 1f);
			for (int k = 0; k < 4; k++)
			{
				_mgr._uvs[_vIndex + k] = _res.UVs[k];
				_mgr._colors[_vIndex + k] = _vertexColor;
			}
			ColorBlendType = SsColorBlendOperation.Non;
		}
		if (_res.Type == SsPartType.Bound)
		{
			_hasTransparency = true;
			AlphaValue = 0.5f;
		}
		else
		{
			_hasTransparency = _res.HasTrancparency || (_parent != null && _res.Inherits(SsKeyAttr.Trans));
			AlphaValue = _res.Trans(0);
			_material = imageFile.GetMaterial(_shaderType);
		}
		Update(true);
		_triIndices = new int[6]
		{
			_vIndex,
			_vIndex + 1,
			_vIndex + 2,
			_vIndex + 2,
			_vIndex + 3,
			_vIndex
		};
		SetToSubmeshArray(_index - 1);
	}

	public void ForceShow(bool v)
	{
		_forceVisibleAvailable = true;
		_forceVisible = v;
		if (_triIndices != null)
		{
			SetToSubmeshArray(_subMeshIndex);
		}
	}

	public void ResetForceShow()
	{
		_forceVisibleAvailable = false;
		if (_triIndices != null)
		{
			SetToSubmeshArray(_subMeshIndex);
		}
	}

	public void ForceAlpha(float v)
	{
		_forceAlphaAvailable = true;
		_forceAlpha = v;
	}

	public void ResetForceAlpha()
	{
		_forceAlphaAvailable = false;
	}

	public Material GetMaterial()
	{
		return _material;
	}

	public void SetColor(Color _col)
	{
		if (_useCgShader && ColorBlendType != 0)
		{
			for (int i = 0; i < 4; i++)
			{
				_mgr._extras[_vIndex + i][1] = _col.a;
			}
			_mgr._extraChanged = true;
		}
		for (int j = 0; j < 4; j++)
		{
			_mgr._colors[_vIndex + j] = _col;
		}
		_mgr._colorChanged = true;
	}

	public int CompareTo(UISsPartP other)
	{
		if (_priority == other._priority)
		{
			return _index.CompareTo(other._index);
		}
		return _priority.CompareTo(other._priority);
	}

	internal void SetToSubmeshArray(int index)
	{
		bool visible = _visible;
		if (_mesh != null)
		{
			_mesh.SetTriangles((!visible) ? null : _triIndices, index);
		}
		_subMeshIndex = index;
	}

	internal void Show(bool v)
	{
		_visible = v;
		if (_mesh != null)
		{
			_mesh.SetTriangles((!v) ? null : _triIndices, _subMeshIndex);
		}
	}

	internal void Update(bool initialize = false)
	{
		UpdateSub(_res, (int)_mgr.AnimFrame, initialize);
		if (_subAnimes != null && _subAnimes.Count > 0)
		{
			int i = 0;
			for (int count = _subAnimes.Count; i < count; i++)
			{
				SsSubAnimeController ssSubAnimeController = _subAnimes[i];
				UpdateSub(_subAnimePartRes[i], (int)ssSubAnimeController.Frame);
			}
			_subAnimes.Clear();
			_subAnimePartRes.Clear();
		}
	}

	internal void AddSubAnime(SsSubAnimeController subAnime, SsPartRes subAnimePartRes)
	{
		if (subAnimePartRes.HasAttrFlags(SsKeyAttrFlags.AllMask))
		{
			_subAnimes.Add(subAnime);
			_subAnimePartRes.Add(subAnimePartRes);
		}
	}

	internal void UpdateSub(SsPartRes res, int frame, bool initialize = false)
	{
		if (res.HasAttrFlags(SsKeyAttrFlags.Prio))
		{
			int num = (int)res.Prio(frame);
			if (_priority != num)
			{
				_priority = num;
				_mgr._prioChanged = true;
			}
		}
		if (res.HasAttrFlags(SsKeyAttrFlags.Hide))
		{
			if (res.IsRoot)
			{
				_visible = !res.Hide(frame);
			}
			else if (res.Type == SsPartType.Normal)
			{
				bool flag = !res.IsBeforeFirstKey(frame) && ((_parent == null || _parent._res.IsRoot || !(res.InheritRate(SsKeyAttr.Hide) > 0.5f)) ? (!res.Hide(frame)) : _parent._visible);
				if (_forceVisibleAvailable)
				{
					flag = _forceVisible;
				}
				if (flag != _visible)
				{
					Show(flag);
				}
			}
		}
		if (res.HasAttrFlags(SsKeyAttrFlags.PartsCol))
		{
			SsColorBlendKeyValue ssColorBlendKeyValue = res.PartsCol(frame);
			SsColorBlendOperation ssColorBlendOperation = ColorBlendType;
			if (ssColorBlendKeyValue == null)
			{
				if (_colorBlendKeyValue != null)
				{
					ssColorBlendOperation = SsColorBlendOperation.Non;
					for (int i = 0; i < 4; i++)
					{
						_mgr._colors[_vIndex + i] = _vertexColor;
					}
					_mgr._colorChanged = true;
				}
			}
			else
			{
				ssColorBlendOperation = ssColorBlendKeyValue.Operation;
				if (ssColorBlendKeyValue.Target == SsColorBlendTarget.Vertex)
				{
					for (int j = 0; j < 4; j++)
					{
						_mgr._colors[_vIndex + j] = GetBlendedColor(ssColorBlendKeyValue.Colors[j], ssColorBlendKeyValue.Operation);
					}
				}
				else
				{
					Color blendedColor = GetBlendedColor(ssColorBlendKeyValue.Colors[0], ssColorBlendKeyValue.Operation);
					for (int k = 0; k < 4; k++)
					{
						_mgr._colors[_vIndex + k] = blendedColor;
					}
				}
				_mgr._colorChanged = true;
			}
			_colorBlendKeyValue = ssColorBlendKeyValue;
			if (_mgr._colorChanged && ssColorBlendOperation != ColorBlendType)
			{
				ColorBlendType = ssColorBlendOperation;
				AlphaValue = AlphaValue;
			}
		}
		if (_hasTransparency)
		{
			float num2 = res.Trans(frame);
			if (_parent != null && res.Inherits(SsKeyAttr.Trans))
			{
				float num3 = ((!(_parent._material == null)) ? _parent.AlphaValue : _parent._res.Trans(frame));
				num2 = num3 * num2;
			}
			if (_forceAlphaAvailable)
			{
				num2 = _forceAlpha;
			}
			if (num2 != AlphaValue)
			{
				AlphaValue = num2;
			}
		}
		if (res.HasAttrFlags(SsKeyAttrFlags.Scale))
		{
			Vector3 vector = new Vector3(res.ScaleX(frame), res.ScaleY(frame), 1f);
			if (vector != _scale)
			{
				_scale = vector;
				_mgr._vertChanged = true;
			}
		}
		if (res.HasAttrFlags(SsKeyAttrFlags.Angle))
		{
			float num4 = res.Angle(frame);
			if (_parent)
			{
				if (_parent._pivotMatrix.m00 * _parent._pivotMatrix.m11 < 0f)
				{
					num4 *= -1f;
				}
				if (_mgr.hFlip ^ _mgr.vFlip)
				{
					num4 *= -1f;
				}
			}
			Quaternion quaternion = Quaternion.Euler(0f, 0f, num4);
			if (quaternion != _quaternion)
			{
				_quaternion = quaternion;
				_mgr._vertChanged = true;
			}
		}
		if (res.HasAttrFlags(SsKeyAttrFlags.Pos))
		{
			Vector3 pos = new Vector3(res.PosX(frame), 0f - res.PosY(frame));
			if (res.IsRoot)
			{
				pos += _rootPivot;
			}
			_pos = pos;
			_mgr._vertChanged = true;
		}
		bool flag2 = false;
		if (res.HasAttrFlags(SsKeyAttrFlags.ImageOffset))
		{
			int num5 = res.ImageOffsetX(frame);
			if (num5 != _imgOfsX)
			{
				_imgOfsX = num5;
				_mgr._uvChanged = true;
			}
			num5 = res.ImageOffsetY(frame);
			if (num5 != _imgOfsY)
			{
				_imgOfsY = num5;
				_mgr._uvChanged = true;
			}
			bool flag3 = false;
			if (modifyVertex)
			{
				flag3 = true;
			}
			if (!directSizeSet)
			{
				num5 = res.ImageOffsetW(frame);
				if (num5 != _imgOfsW)
				{
					_imgOfsW = num5;
					_mgr._uvChanged = true;
					flag3 = true;
				}
				num5 = res.ImageOffsetH(frame);
				if (num5 != _imgOfsH)
				{
					_imgOfsH = num5;
					_mgr._uvChanged = true;
					flag3 = true;
				}
			}
			if (flag3)
			{
				Vector2 size = res.PicArea.WH();
				size.x += _imgOfsW;
				size.y += _imgOfsH;
				_orgVertices = res.GetVertices(size);
				flag2 = true;
			}
			if (_mgr._uvChanged)
			{
				res.CalcUVs(_imgOfsX, _imgOfsY, _imgOfsW, _imgOfsH);
			}
		}
		if (res.HasAttrFlags(SsKeyAttrFlags.OriginOffset) || modifyVertex)
		{
			if (!directSizeSet)
			{
				int num6 = -res.OriginOffsetX(frame);
				if ((float)num6 != _originOffset.x)
				{
					_originOffset.x = num6;
					flag2 = true;
				}
				int num7 = res.OriginOffsetY(frame);
				if ((float)num7 != _originOffset.y)
				{
					_originOffset.y = num7;
					flag2 = true;
				}
			}
			else
			{
				flag2 = true;
			}
		}
		if (res.HasAttrFlags(SsKeyAttrFlags.Vertex))
		{
			flag2 = true;
		}
		if (flag2 && _vertPositions != null)
		{
			bool flag4 = modifyVertex && directSizeSet;
			int l = 0;
			for (int num8 = _vertPositions.Length; l < num8; l++)
			{
				_vertPositions[l] = _orgVertices[l];
				if (res.HasAttrFlags(SsKeyAttrFlags.Vertex))
				{
					_vertPositions[l] += res.Vertex(frame).Vertex3(l);
				}
				if (res.HasAttrFlags(SsKeyAttrFlags.OriginOffset) || flag4)
				{
					_vertPositions[l] += _originOffset;
				}
			}
			flag2 = false;
			_mgr._vertChanged = true;
		}
		bool flag5 = !res.IsRoot && _mgr._animation.hvFlipForImageOnly;
		bool flag6 = false;
		if (res.IsRoot)
		{
			flag6 = _mgr.hFlip;
		}
		else
		{
			if (flag5 && _parent != null && res.Inherits(SsKeyAttr.FlipH) && !_parent._res.IsRoot)
			{
				flag6 = _parent._flipH;
			}
			if (res.FlipH(frame))
			{
				flag6 = !flag6;
			}
		}
		if (!flag5 && ((flag6 && _scale.x > 0f) || (!flag6 && _scale.x < 0f)))
		{
			_scale.x *= -1f;
			_mgr._vertChanged = true;
		}
		bool flag7 = false;
		if (res.IsRoot)
		{
			flag7 = _mgr.vFlip;
		}
		else
		{
			if (flag5 && _parent != null && res.Inherits(SsKeyAttr.FlipV) && !_parent._res.IsRoot)
			{
				flag7 = _parent._flipV;
			}
			if (res.FlipV(frame))
			{
				flag7 = !flag7;
			}
		}
		if (!flag5 && ((flag7 && _scale.y > 0f) || (!flag7 && _scale.y < 0f)))
		{
			_scale.y *= -1f;
			_mgr._vertChanged = true;
		}
		if (flag6 != _flipH || flag7 != _flipV)
		{
			_flipH = flag6;
			_flipV = flag7;
			if (flag5)
			{
				_mgr._uvChanged = true;
			}
		}
		if (_mgr._uvChanged && res.UVs != null && res.UVs.Length == 4)
		{
			if (flag5)
			{
				int num9 = -1;
				if (flag7)
				{
					num9 = 1;
				}
				if (flag6)
				{
					num9++;
				}
				for (int m = 0; m < 4; m++)
				{
					_mgr._uvs[_vIndex + m] = res.UVs[(num9 < 0) ? m : _flippedUvIndices[num9, m]];
				}
			}
			else
			{
				for (int n = 0; n < 4; n++)
				{
					_mgr._uvs[_vIndex + n] = res.UVs[n];
				}
			}
		}
		if (_mgr._vertChanged)
		{
			Vector3 pos2 = _pos;
			Vector3 scale = _scale;
			if (_parent)
			{
				if (!res.Inherits(SsKeyAttr.ScaleX))
				{
					scale.x /= _parent._scale.x;
					pos2.x /= _parent._scale.x;
				}
				if (!res.Inherits(SsKeyAttr.ScaleY))
				{
					scale.y /= _parent._scale.y;
					pos2.y /= _parent._scale.y;
				}
			}
			_pivotMatrix.SetTRS(pos2, _quaternion, scale);
			if (_parent)
			{
				_pivotMatrix = _parent._pivotMatrix * _pivotMatrix;
			}
			if (_vertPositions != null)
			{
				int num10 = 0;
				int num11 = _vIndex;
				for (int num12 = _vIndex + _vertPositions.Length; num11 < num12; num11++)
				{
					Vector3 vector2 = _pivotMatrix.MultiplyPoint3x4(_vertPositions[num10]);
					_mgr._vertices[num11] = vector2;
					num10++;
				}
			}
		}
		if ((bool)_transform && _mgr._vertChanged)
		{
			_transform.localPosition = _pos;
			_transform.localRotation = _quaternion;
			_transform.localScale = _scale;
		}
		modifyVertex = false;
		directSizeSet = false;
		if (!initialize)
		{
			if (ExistsOnUserDataKey && _res.HasAttrFlags(SsKeyAttrFlags.User))
			{
				_OnEvent(SsKeyAttr.User);
			}
			if (ExistsOnSoundKey && _res.HasAttrFlags(SsKeyAttrFlags.Sound))
			{
				_OnEvent(SsKeyAttr.Sound);
			}
		}
	}

	private void _OnEvent(SsKeyAttr attr)
	{
		SsAttrValue[] attrValues = _res.GetAttrValues(attr);
		if (attrValues.Length == 0 || (int)_mgr._animeFrame == (int)_mgr._prevFrame)
		{
			return;
		}
		int num = (int)_mgr._animeFrame;
		int num2 = (int)_mgr._prevFrame;
		for (int i = 0; i < _mgr._playFrameLength; i++)
		{
			num2 = _mgr._StepFrameFromPrev(num2);
			SsAttrValue ssAttrValue = ((num2 >= attrValues.Length) ? attrValues[0] : attrValues[num2]);
			if (!ssAttrValue.HasValue)
			{
				SsKeyFrameInterface key = _res.GetKey(attr, ssAttrValue.RefKeyIndex);
				if (key.Time == num2)
				{
					switch (attr)
					{
					case SsKeyAttr.User:
					{
						SsUserDataKeyFrame ssUserDataKeyFrame = (SsUserDataKeyFrame)key;
						this.OnUserDataKey(this, ssUserDataKeyFrame.Value);
						break;
					}
					case SsKeyAttr.Sound:
					{
						SsSoundKeyFrame ssSoundKeyFrame = (SsSoundKeyFrame)key;
						this.OnSoundKey(this, ssSoundKeyFrame.Value);
						break;
					}
					default:
						UnityEngine.Debug.LogWarning("Not implemented event: " + attr);
						break;
					}
				}
			}
			if (num2 == num)
			{
				break;
			}
		}
	}

	private Color GetBlendedColor(SsColorRef src, SsColorBlendOperation op)
	{
		Color result = (Color)src;
		if (op == SsColorBlendOperation.Mul)
		{
		}
		return result;
	}

	public Transform CreateTransform()
	{
		if ((bool)_transform)
		{
			return _transform;
		}
		GameObject gameObject = new GameObject(_res.Name);
		_transform = gameObject.transform;
		if (_parent)
		{
			if (_parent._transform == null)
			{
				_transform.parent = _parent.CreateTransform();
			}
			else
			{
				_transform.parent = _parent._transform;
			}
		}
		else
		{
			_transform.parent = _mgr._transform;
		}
		_transform.localPosition = _pos;
		_transform.localRotation = _quaternion;
		_transform.localScale = _scale;
		return _transform;
	}

	public bool Intersects(UISsPartP other, bool ignoreZ)
	{
		if (!ignoreZ && _mgr._vertices[_vIndex].z != other._mgr._vertices[other._vIndex].z)
		{
			return false;
		}
		Vector3[] array = MakeGlobalPoints(_mgr.transform.position, _mgr._vertices, _vIndex);
		Vector3[] array2 = MakeGlobalPoints(other._mgr.transform.position, other._mgr._vertices, other._vIndex);
		for (int i = 0; i < 4; i++)
		{
			if (ContainsPointInPlane(array, array2[i]))
			{
				return true;
			}
		}
		for (int j = 0; j < 4; j++)
		{
			if (ContainsPointInPlane(array2, array[j]))
			{
				return true;
			}
		}
		return false;
	}

	internal void DrawBoundingPart()
	{
		Vector3[] array = MakeGlobalPoints(_mgr.transform.position, _mgr._vertices, _vIndex);
		DrawQuadrangle(array[0], array[1], array[2], array[3], Color.green);
	}

	private Vector3[] MakeGlobalPoints(Vector3 gpos, Vector3[] verts, int startIndex)
	{
		Vector3[] array = new Vector3[4];
		for (int i = 0; i < 4; i++)
		{
			array[i] = gpos + verts[startIndex + i];
		}
		return array;
	}

	private bool ContainsPointInPlane(Vector3[] v, Vector3 pt)
	{
		Vector3[] array = new Vector3[4]
		{
			v[0] - pt,
			v[1] - pt,
			v[2] - pt,
			v[3] - pt
		};
		if (Vector3.Cross(array[0], array[1]).z <= 0f && Vector3.Cross(array[1], array[2]).z <= 0f && Vector3.Cross(array[2], array[3]).z <= 0f && Vector3.Cross(array[3], array[0]).z <= 0f)
		{
			return true;
		}
		return false;
	}

	public bool IntersectsByAABB(UISsPartP other, bool ignoreZ)
	{
		Vector3[] vertices = _mgr._vertices;
		Vector3[] vertices2 = other._mgr._vertices;
		if (!ignoreZ && vertices[_vIndex].z != vertices2[other._vIndex].z)
		{
			return false;
		}
		Vector3 lt;
		Vector3 rb;
		MakeMinMaxPosition(_mgr.transform.position, vertices, _vIndex, out lt, out rb);
		Vector3 lt2;
		Vector3 rb2;
		MakeMinMaxPosition(other._mgr.transform.position, vertices2, other._vIndex, out lt2, out rb2);
		if (rb.x >= lt2.x && rb.y >= lt2.y && rb2.x >= lt.x && rb2.y >= lt.y)
		{
			return true;
		}
		return false;
	}

	internal void DrawBoundingPartAABB()
	{
		Vector3 lt;
		Vector3 rb;
		MakeMinMaxPosition(_mgr.transform.position, _mgr._vertices, _vIndex, out lt, out rb);
		DrawRectangle(lt, rb, Color.red);
	}

	private void MakeMinMaxPosition(Vector3 gpos, Vector3[] verts, int startIndex, out Vector3 lt, out Vector3 rb)
	{
		lt = sVec3Max;
		rb = sVec3Min;
		for (int i = 0; i < 4; i++)
		{
			Vector3 vector = verts[startIndex + i] + gpos;
			if (vector.x < lt.x)
			{
				lt.x = vector.x;
			}
			if (vector.x > rb.x)
			{
				rb.x = vector.x;
			}
			if (vector.y < lt.y)
			{
				lt.y = vector.y;
			}
			if (vector.y > rb.y)
			{
				rb.y = vector.y;
			}
		}
	}

	internal void DrawBounds(Bounds bounds, Color color)
	{
	}

	internal void DrawRectangle(Vector3 lt, Vector3 rb, Color color)
	{
	}

	internal void DrawQuadrangle(Vector3 lt, Vector3 rt, Vector3 rb, Vector3 lb, Color color)
	{
	}

	public Material IndividualizeMaterial(bool mulMatColor)
	{
		if (_material == null)
		{
			return null;
		}
		if (_individualMaterial != null)
		{
			return _individualMaterial;
		}
		_individualMaterial = UnityEngine.Object.Instantiate(_material);
		if (_originalMaterial == null)
		{
			_originalMaterial = _material;
		}
		_material = _individualMaterial;
		if (mulMatColor)
		{
			SsShaderType t = SsShaderManager.EnumToType(_colorBlendType, _res.AlphaBlendType, SsMaterialColorBlendOperation.Mul);
			_material.shader = SsShaderManager.Get(t, false);
		}
		_mgr._materials[_subMeshIndex] = _material;
		_mgr._matChanged = true;
		return _material;
	}

	public void ChangeMaterial(Material mat)
	{
		if (_originalMaterial == null)
		{
			_originalMaterial = _material;
		}
		_material = mat;
		_mgr._materials[_subMeshIndex] = _material;
		_mgr._matChanged = true;
	}

	public void RevertChangedMaterial()
	{
		if (!(_originalMaterial == null))
		{
			if (_individualMaterial != null)
			{
				UnityEngine.Object.Destroy(_individualMaterial);
			}
			_mgr._materials[_subMeshIndex] = (_material = _originalMaterial);
			_mgr._matChanged = true;
		}
	}

	internal void DeleteResources()
	{
		if (_individualMaterial != null)
		{
			UnityEngine.Object.Destroy(_individualMaterial);
		}
	}

	public static bool operator true(UISsPartP p)
	{
		return p != null;
	}

	public static bool operator false(UISsPartP p)
	{
		return p == null;
	}
}
