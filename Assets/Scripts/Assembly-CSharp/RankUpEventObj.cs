using UnityEngine;

public class RankUpEventObj
{
	private GameMain mGame = SingletonMonoBehaviour<GameMain>.Instance;

	private UILabel OldLabel;

	private UILabel NewLabel;

	public UISprite Base { get; set; }

	private UITexture Photo { get; set; }

	private UISprite Icon { get; set; }

	private UILabel Name { get; set; }

	private UISprite Rank { get; set; }

	private NumberImageString RankNo { get; set; }

	private RankUpItem Item { get; set; }

	private int Depth { get; set; }

	public UISprite Arrow { get; set; }

	public RankUpEventObj(GameObject parent, UIFont font, string name, int depth, Vector3 pos, RankUpItem item, bool mydata)
	{
		Photo = null;
		Icon = null;
		Name = null;
		Rank = null;
		RankNo = null;
		Item = item;
		Depth = depth;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		Base = Util.CreateSprite("FriendBoard", parent.gameObject, image);
		Util.SetSpriteInfo(Base, "instruction_panel2", depth, pos, Vector3.one, false);
		Base.type = UIBasicSprite.Type.Sliced;
		Base.SetDimensions(490, 108);
		Rank = Util.CreateSprite("RankNo", Base.gameObject, image);
		Util.SetSpriteInfo(Rank, "null", depth + 1, new Vector3(-198f, 0f, 0f), Vector3.one * 0.8f, false);
		switch (item.RankOld)
		{
		case 0:
			Util.SetSpriteImageName(Rank, "ranking1_02", Vector3.one, false);
			break;
		case 1:
			Util.SetSpriteImageName(Rank, "ranking2_02", Vector3.one, false);
			break;
		case 2:
			Util.SetSpriteImageName(Rank, "ranking3_02", Vector3.one, false);
			break;
		default:
			OldLabel = Util.CreateLabel("Number", Rank.gameObject, font);
			Util.SetLabelInfo(OldLabel, string.Empty + (item.RankOld + 1), depth + 2, new Vector3(0f, 0f, 0f), 46, 0, 0, UIWidget.Pivot.Center);
			OldLabel.color = new Color(19f / 51f, 2f / 3f, 0.6039216f, 1f);
			OldLabel.effectStyle = UILabel.Effect.Shadow;
			break;
		case -1:
			break;
		}
		float num = -25f;
		bool flag = false;
		int num2 = item.IconID;
		UITexture uITexture = null;
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		UISprite uISprite3 = null;
		UISprite uISprite4 = null;
		if (GameMain.IsFacebookIconEnable() && num2 > 9999)
		{
			num2 -= 10000;
			if (item.Icon != null)
			{
				uITexture = Util.CreateGameObject("Icon", Base.gameObject).AddComponent<UITexture>();
				uITexture.depth = depth + 2;
				uITexture.transform.localPosition = new Vector3(-46f, -6f, 0f);
				uITexture.mainTexture = item.Icon;
				uITexture.SetDimensions(65, 65);
				if (item.Mode == RankUpDialog.MODE.SCORE)
				{
					uITexture.transform.localPosition = new Vector3(-6f, -6f, 0f);
				}
			}
			else
			{
				uISprite4 = Util.CreateSprite("FriendIcon", Base.gameObject, image2);
				Util.SetSpriteInfo(uISprite4, "icon_chara_facebook", depth + 2, new Vector3(-46f, -6f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(65, 65);
				if (item.Mode == RankUpDialog.MODE.SCORE)
				{
					uISprite4.transform.localPosition = new Vector3(-6f, -6f, 0f);
				}
			}
			flag = true;
			if (num2 >= mGame.mCompanionData.Count)
			{
				num2 = 0;
			}
			string iconName = mGame.mCompanionData[num2].iconName;
			uISprite = Util.CreateSprite("RightTopIcon", Base.gameObject, image2);
			Util.SetSpriteInfo(uISprite, iconName, depth + 3, new Vector3(-22f, 26f, 0f), Vector3.one, false);
			uISprite.SetDimensions(50, 50);
			if (item.Mode == RankUpDialog.MODE.SCORE)
			{
				uISprite.transform.localPosition = new Vector3(22f, 26f, 0f);
			}
		}
		if (!flag)
		{
			if (num2 >= 10000)
			{
				num2 -= 10000;
			}
			if (num2 >= mGame.mCompanionData.Count)
			{
				num2 = 0;
			}
			string iconName2 = mGame.mCompanionData[num2].iconName;
			uISprite2 = Util.CreateSprite("Icon", Base.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, iconName2, depth + 2, new Vector3(-34f, 3f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(84, 84);
			if (item.Mode == RankUpDialog.MODE.SCORE)
			{
				uISprite2.transform.localPosition = new Vector3(-3f, 3f, 0f);
			}
			iconName2 = ((item.MyLevel <= 0) ? "lv_1" : ("lv_" + item.MyLevel));
			if (mydata)
			{
				CompanionData companionData = mGame.mCompanionData[num2];
				iconName2 = ((item.MyLevel >= companionData.maxLevel) ? Def.CharacterIconLevelMaxImageTbl[item.MyLevel] : Def.CharacterIconLevelImageTbl[item.MyLevel]);
			}
			uISprite3 = Util.CreateSprite("Lv", uISprite2.gameObject, image);
			Util.SetSpriteInfo(uISprite3, iconName2, depth + 3, new Vector3(0f, -36f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(item.FbID))
			{
				string imageName = "icon_sns00_mini";
				UISprite sprite = Util.CreateSprite("FacebookIcon", uISprite3.gameObject, image2);
				Util.SetSpriteInfo(sprite, imageName, depth + 4, new Vector3(25f, 63f, 0f), Vector3.one, false);
			}
		}
		Name = Util.CreateLabel("Name", Base.gameObject, font);
		Util.SetLabelInfo(Name, item.Name, depth + 1, new Vector3(71f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		Name.color = Def.DEFAULT_MESSAGE_COLOR;
		Name.maxLineCount = 1;
		Name.overflowMethod = UILabel.Overflow.ShrinkContent;
		Name.SetDimensions(277, 26);
		if (item.Mode == RankUpDialog.MODE.SCORE)
		{
			Name.fontSize = 24;
			Name.transform.localPosition = new Vector3(131f, 0f, 0f);
		}
		Name.transform.localPosition = new Vector3(86f, 26f);
		if (uITexture != null)
		{
			uITexture.transform.localPosition = new Vector3(-119f, -6f);
		}
		if (uISprite != null)
		{
			uISprite.transform.localPosition = new Vector3(-92f, 26f);
		}
		if (uISprite2 != null)
		{
			uISprite2.transform.localPosition = new Vector3(-109f, 3f);
		}
		if (uISprite4 != null)
		{
			uISprite4.transform.localPosition = new Vector3(-119f, -6f);
		}
		UISprite uISprite5 = Util.CreateSprite("Linetop", Base.gameObject, image);
		Util.SetSpriteInfo(uISprite5, "line00", depth + 1, new Vector3(86f, 7f, 0f), Vector3.one, false);
		uISprite5.type = UIBasicSprite.Type.Sliced;
		uISprite5.SetDimensions(292, 6);
		switch (item.Mode)
		{
		case RankUpDialog.MODE.STAGE:
		{
			SMDTools sMDTools = new SMDTools();
			sMDTools.SMDSeries(Base.gameObject, depth, (int)mGame.mPlayer.Data.CurrentSeries, item.Stage, 31f, -8f);
			break;
		}
		case RankUpDialog.MODE.STAR:
		{
			uISprite5 = Util.CreateSprite("StarIcon", Base.gameObject, image);
			Util.SetSpriteInfo(uISprite5, "icon_ranking_star", depth + 2, new Vector3(-7f, -21f, 0f), Vector3.one, false);
			uISprite5.SetDimensions(54, 54);
			UILabel uILabel = Util.CreateLabel("StageNum", uISprite5.gameObject, font);
			Util.SetLabelInfo(uILabel, string.Empty + item.Star, depth + 3, new Vector3(35f, -5f, 0f), 30, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			break;
		}
		case RankUpDialog.MODE.TROPHY:
		{
			uISprite5 = Util.CreateSprite("StarIcon", Base.gameObject, image);
			Util.SetSpriteInfo(uISprite5, "icon_ranking_trophy", depth + 2, new Vector3(-7f, -21f, 0f), Vector3.one, false);
			uISprite5.color = new Color(1f, 1f, 1f, 0.85f);
			uISprite5.SetDimensions(50, 50);
			UILabel uILabel = Util.CreateLabel("StageNum", Base.gameObject, font);
			Util.SetLabelInfo(uILabel, string.Empty + item.Trophy, depth + 3, new Vector3(35f, -5f, 0f), 30, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			break;
		}
		case RankUpDialog.MODE.SCORE:
		{
			string text = string.Format(Localization.Get("Ranking_ScoreNum"));
			UILabel uILabel = Util.CreateLabel("StageDesk", Base.gameObject, font);
			Util.SetLabelInfo(uILabel, text, depth + 3, new Vector3(-51f, -19f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel = Util.CreateLabel("StageNum", Base.gameObject, font);
			Util.SetLabelInfo(uILabel, string.Empty + item.DisplayValue, depth + 3, new Vector3(32f, -19f, 0f), 25, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			break;
		}
		}
	}

	public void ChangeCurrent()
	{
		switch (Item.RankNew)
		{
		case 0:
			Util.SetSpriteImageName(Rank, "ranking1_02", Vector3.one, false);
			break;
		case 1:
			Util.SetSpriteImageName(Rank, "ranking2_02", Vector3.one, false);
			break;
		case 2:
			Util.SetSpriteImageName(Rank, "ranking3_02", Vector3.one, false);
			break;
		default:
			Util.SetSpriteImageName(Rank, string.Empty, Vector3.one * 0.8f, false);
			break;
		}
		switch (Item.RankNew)
		{
		case 0:
		case 1:
		case 2:
			if (RankNo != null)
			{
				Object.Destroy(RankNo.gameObject);
				RankNo = null;
			}
			break;
		default:
			if (RankNo == null)
			{
				UIFont atlasFont = GameMain.LoadFont();
				NewLabel = Util.CreateLabel("Number", Rank.gameObject, atlasFont);
				Util.SetLabelInfo(NewLabel, string.Empty + (Item.RankNew + 1), Depth + 2, new Vector3(0f, 0f, 0f), 46, 0, 0, UIWidget.Pivot.Center);
				NewLabel.color = new Color(19f / 51f, 2f / 3f, 0.6039216f, 1f);
				NewLabel.effectStyle = UILabel.Effect.Shadow;
			}
			else
			{
				RankNo.SetNum(Item.RankNew + 1);
			}
			break;
		}
		if (OldLabel != null)
		{
			Object.Destroy(OldLabel.gameObject);
		}
	}

	public void CreateArrow()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		Arrow = Util.CreateSprite("Arrow", Base.gameObject, image);
		if (Item.RankOld < 0 || Item.RankNew < Item.RankOld)
		{
			Util.SetSpriteInfo(Arrow, "arrow_up", Depth + 1, new Vector3(240f, 16f, 0f), Vector3.one, false);
		}
		else
		{
			Util.SetSpriteInfo(Arrow, "arrow_down", Depth + 1, new Vector3(240f, -16f, 0f), Vector3.one, false);
		}
	}

	public void newArrow(bool up = true)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		Arrow = Util.CreateSprite("Arrow", Base.gameObject, image);
		if (up)
		{
			Util.SetSpriteInfo(Arrow, "rankup_arrow01", Depth + 1, new Vector3(240f, 16f, 0f), Vector3.one, false);
		}
		else
		{
			Util.SetSpriteInfo(Arrow, "rankup_arrow00", Depth + 1, new Vector3(240f, -16f, 0f), Vector3.one, false);
		}
	}
}
