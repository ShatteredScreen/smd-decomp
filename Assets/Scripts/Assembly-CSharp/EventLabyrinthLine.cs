using System.Collections;
using UnityEngine;

public class EventLabyrinthLine : MapLine
{
	public enum StatusChangeType
	{
		NONE = 0,
		TO_UNLOCK = 1
	}

	public const int FRAME_LOCK = 1;

	public const int FRAME_CLEAR = 0;

	public const int FRAME_UNLOCK = 1;

	public string mPngName = "null";

	public Player.STAGE_STATUS mStatus;

	protected short mCourseID;

	private StatusChangeType mLineChangeType;

	public bool IsUnlockFinished { get; private set; }

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public void SetStatusTypeReserved(StatusChangeType a_flg)
	{
		mLineChangeType = a_flg;
	}

	public void Init(string _name, BIJImage atlas, int a_mapIndex, float x, float y, Vector3 scale, float a_rad, string a_animeKey, Player.STAGE_STATUS a_status, string a_pngname, bool a_animePause)
	{
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mAtlas = atlas;
		mScale = scale;
		base._zrot = a_rad;
		mPngName = a_pngname;
		float mAP_LINE_Z = Def.MAP_LINE_Z;
		base._pos = new Vector3(x, y, mAP_LINE_Z);
		base.gameObject.name = "line_" + _name;
		mChangeParts = null;
		mStatus = a_status;
		int num = -1;
		switch (mStatus)
		{
		case Player.STAGE_STATUS.LOCK:
			num = 1;
			break;
		case Player.STAGE_STATUS.CLEAR:
			num = 0;
			break;
		default:
			num = ((mLineChangeType != StatusChangeType.TO_UNLOCK) ? 1 : 1);
			break;
		}
		PlayAnime(AnimeKey, false, true);
		_sprite.AnimFrame = num;
		_sprite.PlayAtStart = false;
		if (mStatus == Player.STAGE_STATUS.UNLOCK)
		{
			_sprite.AnimationFinished = OnAnimationFinished;
			if (mLineChangeType == StatusChangeType.TO_UNLOCK)
			{
				_sprite.Pause();
			}
			else
			{
				_sprite.Play();
			}
		}
		else
		{
			_sprite.AnimationFinished = null;
			_sprite.Pause();
		}
		IsLockTapAnime = false;
		HasTapAnime = false;
		mFirstInitialized = true;
	}

	public override void OnAnimationFinished(SsSprite sprite)
	{
		if (mStatus == Player.STAGE_STATUS.UNLOCK)
		{
			sprite.AnimFrame = 1f;
			sprite.Play();
		}
	}

	public IEnumerator UnlockProcess()
	{
		SetStatusTypeReserved(StatusChangeType.NONE);
		IsUnlockFinished = false;
		SsPart part = _sprite.GetPart("00");
		if (part == null)
		{
			IsUnlockFinished = true;
			yield break;
		}
		SsPart.KeyframeCallback handler = null;
		handler = delegate(SsPart a_part, SsAttrValueInterface a_val)
		{
			SsUserDataKeyValue ssUserDataKeyValue = a_val as SsUserDataKeyValue;
			if (ssUserDataKeyValue.String.CompareTo("CLEAR_FINISH") == 0)
			{
				a_part.OnUserDataKey -= handler;
				IsUnlockFinished = true;
			}
		};
		part.OnUserDataKey += handler;
		_sprite.AnimFrame = 1f;
		_sprite.AnimationFinished = OnAnimationFinished;
		_sprite.Play();
		float _start = Time.realtimeSinceStartup;
		while (!IsUnlockFinished)
		{
			float timeSpan = Time.realtimeSinceStartup;
			if (timeSpan - _start > 3f)
			{
				part.OnUserDataKey -= handler;
				IsUnlockFinished = true;
			}
			yield return null;
		}
	}
}
