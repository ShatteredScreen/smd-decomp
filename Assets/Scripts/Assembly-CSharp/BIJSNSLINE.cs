using System.Collections;
using UnityEngine;

public class BIJSNSLINE : BIJSNSAppSwitcher
{
	public override SNS Kind
	{
		get
		{
			return SNS.LINE;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		switch (type)
		{
		case 7:
		case 8:
		case 9:
			return true;
		default:
			return false;
		}
	}

	private IEnumerator BIJSNSLINE_PluginFailed(int seqno, string message, object result)
	{
		yield return new WaitForSeconds(0.5f);
		SetSpawn(0);
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSLINE.PluginFailed failed: seqno=" + seqno + " current=" + base.CurrentSeqNo + " message=" + message);
		if (base.CurrentSeqNo == seqno)
		{
			base.CurrentRes = new Response(1, message, result);
			ChangeStep(STEP.COMM_RESULT);
		}
	}

	public override void PostMessage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSLINE.PostMessage");
		if (spawn)
		{
			CallCallback(handler, new Response(1, "LINE already invoked.", null), userdata);
			return;
		}
		int type = 7;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string text = BIJSNS.ToSS(postdata.Message);
			string imageUrl = BIJSNS.ToSS(postdata.ImageURL);
			BIJSNS.dbg("@@@ SNS @@@ " + text);
			bool flag = BIJUnity.invokeLINEWithMessage(title, text, imageUrl);
			BIJSNS.log("SNSInvokeLINEWithMessage: " + flag);
			if (!flag)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSLINE_PluginFailed(num, "LINE invoke failed at posting", null));
			}
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "LINE not requested", null), userdata);
		}
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSLINE.UploadImage");
		if (spawn)
		{
			CallCallback(handler, new Response(1, "LINE already invoked.", null), userdata);
			return;
		}
		int type = 9;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string text = BIJSNS.ToSS(postdata.Message);
			string imageUrl = BIJSNS.ToSS(postdata.ImageURL);
			BIJSNS.dbg("@@@ SNS @@@ " + text);
			bool flag = BIJUnity.invokeLINEWithImage(title, text, imageUrl);
			BIJSNS.log("SNSInvokeLINEWithMessage: " + flag);
			if (!flag)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSLINE_PluginFailed(num, "LINE invoke failed at posting", null));
			}
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "LINE not requested", null), userdata);
		}
	}

	public override void Invite(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSSMS.Invite");
		if (spawn)
		{
			CallCallback(handler, new Response(1, "LINE already invoked.", null), userdata);
			return;
		}
		int type = 8;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string text = BIJSNS.ToSS(postdata.Message);
			string imageUrl = BIJSNS.ToSS(postdata.ImageURL);
			BIJSNS.dbg("@@@ SNS @@@ " + text);
			bool flag = BIJUnity.invokeLINEWithMessage(title, text, imageUrl);
			BIJSNS.log("SNSInvokeLINEWithMessage: " + flag);
			if (!flag)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSLINE_PluginFailed(num, "LINE invoke failed at inviting", null));
			}
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "LINE not requested", null), userdata);
		}
	}
}
