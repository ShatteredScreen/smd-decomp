public class MCSpendRequest : ParameterObject<MCSpendRequest>
{
	public int bhiveid { get; set; }

	public string udid { get; set; }

	public string uniq_id { get; set; }

	public int uuid { get; set; }

	public int itemid { get; set; }

	public long spend_time { get; set; }

	public int series { get; set; }

	public int lvl { get; set; }

	public int mc { get; set; }

	public int sc { get; set; }

	public int tgt_series { get; set; }

	public int tgt_stage { get; set; }

	public bool retry { get; set; }
}
