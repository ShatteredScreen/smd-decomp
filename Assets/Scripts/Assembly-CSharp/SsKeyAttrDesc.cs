public class SsKeyAttrDesc
{
	public SsKeyAttr Attr { get; set; }

	public SsKeyValueType ValueType { get; set; }

	public SsKeyCastType CastType { get; set; }

	public bool NeedsInterpolatable
	{
		get
		{
			SsKeyValueType valueType = ValueType;
			if (valueType == SsKeyValueType.User || valueType == SsKeyValueType.Sound || valueType == SsKeyValueType.Param)
			{
				return false;
			}
			return true;
		}
	}

	public SsKeyAttrDesc(SsKeyAttr a, SsKeyValueType v, SsKeyCastType c)
	{
		Attr = a;
		ValueType = v;
		CastType = c;
	}
}
