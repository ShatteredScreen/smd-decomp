using System;

[Serializable]
public class SsSoundKeyValue : SsAttrValueInterface
{
	public SsSoundKeyFlags Flags;

	public byte SoundId;

	public byte NoteOn;

	public byte Volume;

	public byte LoopNum;

	public uint UserData;

	public SsSoundKeyValue()
	{
	}

	public SsSoundKeyValue(SsSoundKeyValue r)
	{
		Flags = r.Flags;
		SoundId = r.SoundId;
		NoteOn = r.NoteOn;
		Volume = r.Volume;
		LoopNum = r.LoopNum;
		UserData = r.UserData;
	}

	public SsAttrValueInterface Clone()
	{
		return new SsSoundKeyValue(this);
	}

	public override string ToString()
	{
		return string.Concat("Flags: ", Flags, ", SoundId: ", SoundId, ", NoteOn: ", NoteOn, ", Volume: ", Volume, ", LoopNum: ", LoopNum, ", UserData: ", UserData);
	}
}
