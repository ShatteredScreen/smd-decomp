using UnityEngine;

public class AgeReConfirmationDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum CONFIRM_DIALOG_STYLE
	{
		OK_ONLY = 0,
		YES_NO = 1,
		CLOSEBUTTON = 2,
		OK_CANCEL = 3,
		CLOSE_ONLY = 4,
		RETRY_CLOSE = 5,
		CONFIRM_CLOSE = 6,
		CONFIRM_ONLY = 7,
		TIMER = 8,
		IMAGE_CHOICE = 9
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private CONFIRM_DIALOG_STYLE mConfirmStyle;

	private DIALOG_SIZE mDialogSize;

	private float mCloseTimer = 1f;

	private string mImagePOSIData = string.Empty;

	private string mImageNEGAData = string.Empty;

	private int ReservedSortingOrder;

	public int mYearInt;

	public int mMonthInt;

	public int mDayint;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy() && mConfirmStyle == CONFIRM_DIALOG_STYLE.TIMER)
		{
			mCloseTimer -= Time.deltaTime;
			if (mCloseTimer <= 0f || mGame.mBackKeyTrg)
			{
				Close();
			}
		}
	}

	public override void BuildDialog()
	{
		if (ReservedSortingOrder > 0)
		{
			mPanel.sortingOrder = ReservedSortingOrder;
			mPanel.depth = ReservedSortingOrder;
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = string.Format(Localization.Get("Purchase_ReconfirmDesc1"), mYearInt, mMonthInt, mDayint);
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		float num = -110f;
		float num2 = -280f;
		num = 50f;
		num2 = -90f;
		titleDescKey = string.Format(Localization.Get("Purchase_ReconfirmDesc2"));
		UILabel uILabel = Util.CreateLabel("Desc2", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 67f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.fontSize = 28;
		titleDescKey = string.Format(Localization.Get("Purchase_ReconfirmDesc3"));
		uILabel = Util.CreateLabel("Desc3", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, -26f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.color = Color.red;
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		string text = "LC_button_yes";
		Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(-113f, -113f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(113f, -113f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(int YearInt, int MonthInt, int Dayint)
	{
		mYearInt = YearInt;
		mMonthInt = MonthInt;
		mDayint = Dayint;
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetCloseTimer(float timer)
	{
		mCloseTimer = timer;
	}

	public void SetSortOrder(int a_order)
	{
		ReservedSortingOrder = a_order;
	}
}
