using System;

public class ConnectChargeCheck : ConnectBase
{
	private MCCheckResponse mResponse;

	private bool mIsSetResponseData;

	public void SetResponseData(MCCheckResponse a_response)
	{
		mResponse = a_response;
		mIsSetResponseData = true;
	}

	protected override void InitServerFlg()
	{
		GameMain.mGetGemCountSucceed = false;
		GameMain.mGetGemCountCheckDone = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mGetGemCountSucceed = true;
		GameMain.mGetGemCountCheckDone = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mGetGemCountSucceed = true;
		GameMain.mGetGemCountCheckDone = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mGame.GetGemCountGetTime, DateTime.Now);
		long gET_GEMCOUNT_FREQ = GameMain.GET_GEMCOUNT_FREQ;
		if (mGame.GetGemCountFreqFlag)
		{
			if (gET_GEMCOUNT_FREQ > 0 && num < gET_GEMCOUNT_FREQ)
			{
				ServerFlgSucceeded();
				return false;
			}
			if (mPlayer.Dollar < 10)
			{
				ServerFlgSucceeded();
				return false;
			}
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		mGame.GetGemCountFreqFlag = true;
		InitServerFlg();
		if (!ServerIAP.ChargeCheck())
		{
			ServerFlgFailed();
			mGame.GetGemCountGetTime = DateTimeUtil.UnitLocalTimeEpoch;
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		if (mResponse != null)
		{
			int mc = mResponse.mc;
			int sc = mResponse.sc;
			long server_time = mResponse.server_time;
			mPlayer.SetDollar(mc, true);
			mPlayer.SetDollar(sc, false);
			mPlayer.ServerTime = server_time;
			mGame.GetGemCountGetTime = DateTime.Now;
		}
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mGame.GetGemCountGetTime = DateTimeUtil.UnitLocalTimeEpoch;
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
