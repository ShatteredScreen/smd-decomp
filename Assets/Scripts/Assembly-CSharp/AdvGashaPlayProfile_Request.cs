public class AdvGashaPlayProfile_Request : RequestBase
{
	[MiniJSONAlias("group_id")]
	public int GroupID { get; set; }

	[MiniJSONAlias("gasha_id")]
	public int GashaID { get; set; }

	[MiniJSONAlias("price_id")]
	public int PriceID { get; set; }

	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }

	[MiniJSONAlias("is_retry")]
	public int IsRetry { get; set; }

	[MiniJSONAlias("bhiveid")]
	public int bhiveid { get; set; }

	[MiniJSONAlias("series")]
	public int series { get; set; }

	[MiniJSONAlias("lvl")]
	public int lvl { get; set; }

	[MiniJSONAlias("mc")]
	public int mc { get; set; }

	[MiniJSONAlias("sc")]
	public int sc { get; set; }

	[MiniJSONAlias("tgt_series")]
	public int tgt_series { get; set; }

	[MiniJSONAlias("tgt_stage")]
	public int tgt_stage { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string uniq_id { get; set; }
}
