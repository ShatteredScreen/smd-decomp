using System.Collections.Generic;

public class WebSaleInfo
{
	[MiniJSONAlias("mainurl")]
	public string MainURL { get; set; }

	[MiniJSONAlias("detailbaseurl")]
	public string DetailBaseURL { get; set; }

	[MiniJSONAlias("detail")]
	public List<int> DetailList { get; set; }

	public WebSaleInfo()
	{
		MainURL = string.Empty;
		DetailBaseURL = string.Empty;
		DetailList = new List<int>();
	}
}
