using System;

public class GetWallPaperResponse : ICloneable
{
	[MiniJSONAlias("url")]
	public string URL { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public GetWallPaperResponse Clone()
	{
		return MemberwiseClone() as GetWallPaperResponse;
	}
}
