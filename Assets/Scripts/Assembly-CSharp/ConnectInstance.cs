using System;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class ConnectInstance : MonoBehaviour, IComparable<ConnectInstance>
{
	public enum STATE
	{
		INIT = 0,
		CONNECT = 1,
		CONNECT_WAIT = 2,
		FINISH = 3,
		DELETE_WAIT = 4,
		ERRORDIALOG_WAIT = 5,
		WAIT = 6
	}

	public delegate void OnFinishedHandler(List<ConnectResult> a_result, bool a_error, bool a_abort);

	[SerializeField]
	private ConnectBase mConnectItem;

	private OnFinishedHandler FinishedCallback;

	private List<ConnectResult> mResultList = new List<ConnectResult>();

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	[SerializeField]
	private STATE mCurrentState;

	private GameObject mErrorDialogParent;

	protected ConnectCommonErrorDialog mConnectErrorDialog;

	public List<ConnectBase> ConnectItemList { get; private set; }

	[SerializeField]
	public bool ShowAnime { get; private set; }

	[SerializeField]
	public bool IsErrorDialogWait { get; private set; }

	public bool EnableDelete
	{
		get
		{
			return mState.GetStatus() == STATE.DELETE_WAIT;
		}
	}

	public bool IsErrorSet { get; protected set; }

	public string ErrorTitle { get; protected set; }

	public string ErrorDesc { get; protected set; }

	public ConnectCommonErrorDialog.STYLE ErrorDialogStyle { get; protected set; }

	public short ErrorCount { get; protected set; }

	public ConnectCommonErrorDialog.OnDialogClosed ErrorDialogClosed { get; protected set; }

	public void SetErrorDialogWait(bool a_flg)
	{
		IsErrorDialogWait = false;
	}

	public void SetFinishHandler(OnFinishedHandler a_callback)
	{
		FinishedCallback = a_callback;
	}

	public void ClearErrorInfo()
	{
		ErrorTitle = null;
		ErrorDesc = null;
		ErrorDialogStyle = ConnectCommonErrorDialog.STYLE.NONE;
		ErrorDialogClosed = null;
		ErrorCount = 0;
		IsErrorSet = false;
	}

	protected virtual void OnConnectErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		if (ErrorDialogClosed != null)
		{
			ErrorDialogClosed(i, a_state);
		}
		mConnectErrorDialog = null;
		if (ShowAnime)
		{
			SingletonMonoBehaviour<GameMain>.Instance.StartConnecting();
		}
		ClearErrorInfo();
		mState.Change(STATE.CONNECT_WAIT);
	}

	private void ShowErrorDialog()
	{
		if (IsErrorSet)
		{
			if (ShowAnime)
			{
				SingletonMonoBehaviour<GameMain>.Instance.FinishConnecting();
			}
			mConnectErrorDialog = Util.CreateGameObject("ConnectErrorDialog", mErrorDialogParent).AddComponent<ConnectCommonErrorDialog>();
			mConnectErrorDialog.Init(ErrorTitle, ErrorDesc, ErrorDialogStyle, ErrorCount, -1);
			mConnectErrorDialog.SetClosedCallback(OnConnectErrorDialogClosed);
		}
		else
		{
			BIJLog.E("error dialog title is null");
		}
		mState.Change(STATE.WAIT);
	}

	public void Init(List<ConnectBase> a_list, GameObject a_errorDialogParent, bool a_errorDialogWait, bool a_anime)
	{
		ConnectItemList = new List<ConnectBase>(a_list);
		mErrorDialogParent = a_errorDialogParent;
		IsErrorDialogWait = a_errorDialogWait;
		ShowAnime = a_anime;
	}

	public ConnectBase GetConnectItemFromAPI(int a_api)
	{
		ConnectBase result = null;
		if (ConnectItemList != null)
		{
			int num = ConnectItemList.FindIndex((ConnectBase a) => a.ConnectAPI == a_api);
			if (num != -1)
			{
				result = ConnectItemList[num];
			}
			else
			{
				BIJLog.E("Not Found API:" + a_api);
			}
		}
		return result;
	}

	public void StartConnect()
	{
		if (mState.GetStatus() != 0)
		{
			return;
		}
		if (ConnectItemList == null)
		{
			BIJLog.E("Not set connectAPI");
			return;
		}
		if (ShowAnime)
		{
			SingletonMonoBehaviour<GameMain>.Instance.StartConnecting();
		}
		mState.Change(STATE.CONNECT);
	}

	public void Update()
	{
		mCurrentState = mState.GetStatus();
		switch (mCurrentState)
		{
		case STATE.CONNECT:
			if (mConnectItem == null)
			{
				if (ConnectItemList.Count > 0)
				{
					mConnectItem = ConnectItemList[0];
					mConnectItem.StartConnect();
					if (mConnectItem.ConnectAPI >= 1000)
					{
					}
					mState.Change(STATE.CONNECT_WAIT);
				}
				else
				{
					mState.Change(STATE.FINISH);
				}
			}
			else if (mConnectItem.ConnectAPI >= 1000)
			{
				BIJLog.E(string.Concat(mCurrentState, " ConnectItem not null:", (Server.API)mConnectItem.ConnectAPI, "=>"));
			}
			else
			{
				BIJLog.E(string.Concat(mCurrentState, " ConnectItem not null IAP:", (ServerIAP.API)mConnectItem.ConnectAPI, "=>"));
			}
			break;
		case STATE.CONNECT_WAIT:
			if (mConnectItem != null)
			{
				if (!mConnectItem.IsConnecting)
				{
					ConnectResult result = mConnectItem.GetResult();
					mResultList.Add(result);
					ConnectItemList.Remove(mConnectItem);
					UnityEngine.Object.Destroy(mConnectItem.gameObject);
					mConnectItem = null;
					if (result.IsError && ConnectItemList.Count > 0)
					{
						while (ConnectItemList.Count > 0)
						{
							ConnectBase connectBase = ConnectItemList[0];
							ConnectItemList.RemoveAt(0);
							if (connectBase != null)
							{
								UnityEngine.Object.Destroy(connectBase.gameObject);
								connectBase = null;
							}
						}
						ConnectItemList.Clear();
					}
					if (ConnectItemList.Count > 0)
					{
						mState.Change(STATE.CONNECT);
					}
					else
					{
						mState.Change(STATE.FINISH);
					}
				}
				else if (mConnectItem.ShowConnectErrorDialog)
				{
					IsErrorSet = true;
					ErrorTitle = mConnectItem.ErrorTitle;
					ErrorDesc = mConnectItem.ErrorDesc;
					ErrorDialogStyle = mConnectItem.ErrorDialogStyle;
					ErrorDialogClosed = mConnectItem.ErrorDialogClosed;
					ErrorCount = mConnectItem.RetryCount;
					if (IsErrorDialogWait)
					{
						mState.Change(STATE.ERRORDIALOG_WAIT);
					}
					else
					{
						ShowErrorDialog();
					}
				}
			}
			else
			{
				BIJLog.E(string.Concat(mCurrentState, "ConnectItem is null"));
			}
			break;
		case STATE.FINISH:
			ConnectFinish();
			mState.Change(STATE.DELETE_WAIT);
			break;
		case STATE.DELETE_WAIT:
			if (mState.IsChanged() && ShowAnime)
			{
				SingletonMonoBehaviour<GameMain>.Instance.FinishConnecting();
			}
			break;
		case STATE.ERRORDIALOG_WAIT:
			if (!IsErrorDialogWait)
			{
				ShowErrorDialog();
			}
			break;
		}
		mState.Update();
	}

	public int CompareTo(ConnectInstance a_item)
	{
		if (ConnectItemList.Count != a_item.ConnectItemList.Count)
		{
			return 1;
		}
		if (ShowAnime != a_item.ShowAnime)
		{
			return 1;
		}
		return 0;
	}

	public void ConnectFinish()
	{
		if (FinishedCallback == null)
		{
			return;
		}
		bool a_error = false;
		bool a_abort = false;
		for (int i = 0; i < mResultList.Count; i++)
		{
			if (mResultList[i].IsError)
			{
				a_error = true;
				a_abort = mResultList[i].ErrorType == ConnectResult.ERROR_TYPE.Abandon;
				break;
			}
		}
		FinishedCallback(mResultList, a_error, a_abort);
	}
}
