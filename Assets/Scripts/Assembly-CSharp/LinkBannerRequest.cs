using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class LinkBannerRequest
{
	private enum STATE
	{
		INIT = 0,
		IDLE = 1,
		GET_BANNER = 2,
		WAIT_LOAD_BANNER = 3,
		WAIT_DL_BANNER = 4,
		WAIT_COMMUNICATION = 5
	}

	public sealed class OnReceiveEveryTimeCallback : UnityEvent<LinkBannerInfo>
	{
	}

	private STATE mState;

	private static GameMain mGame;

	private static string mSavePath;

	public LinkBannerManager.OnGetCompleteCallback mCompleteCallback;

	public OnReceiveEveryTimeCallback mReceiveEveryTimeCallback;

	public LinkBannerSetting mNewSetting;

	public bool mNewDLOnly;

	public bool mClear;

	private string mServerBaseURL;

	private List<LinkBannerInfo> mInfoList;

	private LinkBannerInfo mCurrentInfo;

	private int mIndex;

	private bool mDLOnly;

	private bool mHighPriority;

	private WWW mWww;

	public LinkBannerRequest()
	{
		mState = STATE.INIT;
		mCompleteCallback = null;
		mReceiveEveryTimeCallback = null;
		mNewSetting = null;
		mNewDLOnly = false;
		mServerBaseURL = string.Empty;
		mInfoList = new List<LinkBannerInfo>();
		mCurrentInfo = null;
		mIndex = 0;
		mDLOnly = false;
		mHighPriority = false;
		mClear = false;
		mWww = null;
	}

	~LinkBannerRequest()
	{
		Clear();
		mInfoList = null;
		if (mWww != null)
		{
			mWww.Dispose();
			mWww = null;
		}
	}

	public void SetHighPriority(bool aPriority)
	{
		mHighPriority = aPriority;
	}

	public void Update()
	{
		switch (mState)
		{
		case STATE.INIT:
			if (mGame == null)
			{
				mGame = SingletonMonoBehaviour<GameMain>.Instance;
			}
			if (string.IsNullOrEmpty(mSavePath))
			{
				mSavePath = Path.Combine(BIJUnity.getTempPath(), "linkbanner");
				if (!Directory.Exists(mSavePath))
				{
					Directory.CreateDirectory(mSavePath);
				}
				mSavePath += "/";
			}
			mState = STATE.IDLE;
			break;
		case STATE.IDLE:
		{
			UpdateBannerList(true);
			for (int i = 0; i < mInfoList.Count; i++)
			{
				if (!mInfoList[i].isDone)
				{
					mIndex = i;
					mState = STATE.GET_BANNER;
					break;
				}
			}
			break;
		}
		case STATE.WAIT_COMMUNICATION:
			if (LinkBannerManager.Instance.EnableDownloadBanner())
			{
				mState = STATE.GET_BANNER;
			}
			break;
		case STATE.GET_BANNER:
		{
			if (!mHighPriority && !mGame.mGameStateManager.EnableBackgroundProcess())
			{
				break;
			}
			mCurrentInfo = mInfoList[mIndex];
			string text = mSavePath + mCurrentInfo.fileName;
			string empty = string.Empty;
			if (File.Exists(text))
			{
				if (mDLOnly)
				{
					GoToNext();
					break;
				}
				empty = "file:///" + text;
				mState = STATE.WAIT_LOAD_BANNER;
			}
			else
			{
				if (!LinkBannerManager.Instance.EnableDownloadBanner())
				{
					mState = STATE.WAIT_COMMUNICATION;
					break;
				}
				empty = mServerBaseURL + mCurrentInfo.fileName;
				mState = STATE.WAIT_DL_BANNER;
			}
			mWww = new WWW(empty);
			break;
		}
		case STATE.WAIT_LOAD_BANNER:
		{
			if (!mWww.isDone && mWww.error == null)
			{
				break;
			}
			bool flag = true;
			if (mWww.error == null && mWww.bytes != null && mWww.bytes.Length > 0)
			{
				try
				{
					byte[] bytes = mWww.bytes;
					byte[] data = CryptUtils.DecryptRJ128(bytes);
					mCurrentInfo.banner = new Texture2D(0, 0);
					if (mCurrentInfo.banner.LoadImage(data) && mCurrentInfo.banner.height > 0 && mCurrentInfo.banner.width > 0)
					{
						flag = false;
					}
				}
				catch (Exception)
				{
				}
			}
			if (flag)
			{
				if (mWww.error != null)
				{
				}
				mWww.Dispose();
				mWww = null;
				File.Delete(mSavePath + mCurrentInfo.fileName);
				string url = mServerBaseURL + mCurrentInfo.fileName;
				mWww = new WWW(url);
				mState = STATE.WAIT_DL_BANNER;
			}
			else
			{
				GoToNext();
			}
			break;
		}
		case STATE.WAIT_DL_BANNER:
			if (!LinkBannerManager.Instance.EnableDownloadBanner() && CancelDownloadBanner())
			{
				mState = STATE.WAIT_COMMUNICATION;
			}
			else
			{
				if (!mWww.isDone && mWww.error == null)
				{
					break;
				}
				if (mWww.error != null)
				{
					mCurrentInfo.isError = true;
				}
				else
				{
					mCurrentInfo.banner = mWww.texture;
					try
					{
						using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(mSavePath + mCurrentInfo.fileName, Encoding.UTF8, true))
						{
							bIJEncryptDataWriter.WriteAll(mCurrentInfo.banner.EncodeToPNG());
						}
					}
					catch (Exception)
					{
					}
					if (mDLOnly)
					{
						UnityEngine.Object.Destroy(mCurrentInfo.banner);
						mCurrentInfo.banner = null;
					}
				}
				GoToNext();
			}
			break;
		}
	}

	private void GoToNext()
	{
		if (mWww != null)
		{
			mWww.Dispose();
			mWww = null;
		}
		if (mCurrentInfo != null && mCurrentInfo.banner != null)
		{
			mCurrentInfo.banner.wrapMode = TextureWrapMode.Clamp;
		}
		if (mReceiveEveryTimeCallback != null)
		{
			mReceiveEveryTimeCallback.Invoke(mInfoList[mIndex]);
		}
		mCurrentInfo.isDone = true;
		mCurrentInfo = null;
		mIndex++;
		UpdateBannerList();
		while (mIndex < mInfoList.Count && mInfoList[mIndex].isDone)
		{
			mIndex++;
		}
		if (mIndex >= mInfoList.Count)
		{
			CompleteCallback();
			mState = STATE.IDLE;
		}
		else
		{
			mState = STATE.GET_BANNER;
		}
	}

	private bool CancelDownloadBanner()
	{
		if (mWww != null)
		{
			if (mWww.isDone && mWww.error == null)
			{
				return false;
			}
			mWww.Dispose();
			mWww = null;
			return true;
		}
		return false;
	}

	private void CompleteCallback()
	{
		if (mCompleteCallback != null)
		{
			List<LinkBannerInfo> list = new List<LinkBannerInfo>();
			for (int i = 0; i < mInfoList.Count; i++)
			{
				LinkBannerInfo linkBannerInfo = mInfoList[i];
				if (!linkBannerInfo.isError)
				{
					list.Add(linkBannerInfo);
				}
			}
			mCompleteCallback(list);
		}
		if (mDLOnly)
		{
			Clear();
			mDLOnly = false;
		}
	}

	private void UpdateBannerList(bool aNeedCallback = false)
	{
		if (mClear)
		{
			Clear();
			mClear = false;
		}
		if (mNewSetting == null)
		{
			return;
		}
		mServerBaseURL = mNewSetting.BaseURL;
		if (mDLOnly && !mNewDLOnly)
		{
			Clear();
		}
		mDLOnly = mNewDLOnly;
		List<LinkBannerInfo> list = mInfoList;
		mInfoList = new List<LinkBannerInfo>();
		bool flag = false;
		foreach (LinkBannerDetail banner in mNewSetting.BannerList)
		{
			LinkBannerInfo linkBannerInfo = null;
			for (int i = 0; i < list.Count; i++)
			{
				LinkBannerInfo linkBannerInfo2 = list[i];
				if (banner.FileName == linkBannerInfo2.fileName && banner.Action == linkBannerInfo2.action.ToString() && banner.Param == linkBannerInfo2.param && linkBannerInfo2.isDone && !linkBannerInfo2.isError && linkBannerInfo2.banner != null)
				{
					linkBannerInfo = linkBannerInfo2;
					list.RemoveAt(i);
					break;
				}
			}
			if (linkBannerInfo == null)
			{
				linkBannerInfo = new LinkBannerInfo();
				linkBannerInfo.fileName = banner.FileName;
				linkBannerInfo.SetAction(banner.Action);
				linkBannerInfo.param = banner.Param;
				flag = true;
			}
			mInfoList.Add(linkBannerInfo);
		}
		for (int j = 0; j < list.Count; j++)
		{
			LinkBannerInfo linkBannerInfo3 = list[j];
			if (linkBannerInfo3.banner != null)
			{
				UnityEngine.Object.Destroy(linkBannerInfo3.banner);
				linkBannerInfo3.banner = null;
			}
		}
		list.Clear();
		mIndex = 0;
		mNewSetting = null;
		if (aNeedCallback && !flag)
		{
			CompleteCallback();
		}
	}

	private void Clear()
	{
		if (mInfoList != null)
		{
			for (int i = 0; i < mInfoList.Count; i++)
			{
				LinkBannerInfo linkBannerInfo = mInfoList[i];
				if (linkBannerInfo.banner != null)
				{
					UnityEngine.Object.Destroy(linkBannerInfo.banner);
					linkBannerInfo.banner = null;
				}
			}
			mInfoList.Clear();
		}
		mIndex = 0;
	}

	public void ClearLinkBanner()
	{
		mClear = true;
		if (mNewSetting != null)
		{
			mNewSetting = null;
			mNewDLOnly = false;
		}
	}

	public void Pause()
	{
		if (mState == STATE.WAIT_DL_BANNER)
		{
			CancelDownloadBanner();
		}
	}

	public void Resume()
	{
	}
}
