using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

public abstract class BIJSNS
{
	public class Response
	{
		public int result;

		public string message = string.Empty;

		public object detail;

		public int reqseq;

		public Response(int _result, string _message, object _detail)
		{
			result = _result;
			message = _message;
			detail = _detail;
			reqseq = 0;
		}

		public Response(int _result, string _message, object _detail, int seqno)
		{
			result = _result;
			message = _message;
			detail = _detail;
			reqseq = seqno;
		}

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("#" + reqseq);
			stringBuilder.Append(" Result:[" + result + "]");
			stringBuilder.Append(" " + message);
			if (detail != null)
			{
				stringBuilder.Append("\r\n");
				detail.ToDump(stringBuilder);
			}
			return stringBuilder.ToString();
		}
	}

	public interface IEventCallback
	{
		bool EqualsSeqNo(int _seqno);

		void Register();

		void Unregister();
	}

	protected class Request
	{
		public int seqno;

		public int type;

		public float timestamp;

		public Handler handler;

		public object userdata;

		public Response response;

		public IEventCallback eventcb;

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("#" + seqno);
			stringBuilder.Append(" type=" + type);
			stringBuilder.Append(" timestamp=" + timestamp);
			stringBuilder.Append(" handler=" + ((handler != null) ? handler.ToString() : "(null)"));
			stringBuilder.Append(" userdata=" + ((userdata != null) ? userdata.ToString() : "(null)"));
			stringBuilder.Append(" response=" + ((response != null) ? response.ToString() : "(null)"));
			stringBuilder.Append(" eventcb=" + ((eventcb != null) ? eventcb.ToString() : "(null)"));
			return stringBuilder.ToString();
		}
	}

	public enum OP_TYPE
	{
		NONE = 0,
		CLEARDATA = 1,
		LOGIN = 2,
		LOGOUT = 3,
		GETUSERINFO = 4,
		GETFRIENDS = 5,
		FETCHUSERIMAGE = 6,
		POSTMESSAGE = 7,
		INVITE = 8,
		UPLOADIMAGE = 9,
		UNLOCKACHIEVEMENT = 10,
		INCREMENTACHIEVEMENT = 11,
		SHOWACHIEVEMENTS = 12
	}

	protected enum STEP
	{
		NONE = 0,
		IDLE = 1,
		COMM_WAIT = 2,
		COMM_RESULT = 3
	}

	public delegate void Handler(Response res, object userdata);

	protected const bool LOG = false;

	protected const bool DEBUG = false;

	protected const int DEBUG_FRAME_COUNT = 100;

	public const int RESULT_SUCCESS = 0;

	public const int RESULT_ERORR = 1;

	public const int RESULT_CANCELED = 2;

	public const int SEQ_INVALID = 0;

	protected List<Request> mRequestQueue = new List<Request>();

	protected GameObject mSurrogateGameObject;

	protected MonoBehaviour mSurrogateMonoBehaviour;

	private static int mFireSeqNo = 0;

	private Request mCurrentReq;

	private float mWaitLast;

	private float mWaitDuration;

	private float mWaitTimeout;

	private static readonly byte[] Unity_Question_Mark_PNG = new byte[122]
	{
		137, 80, 78, 71, 13, 10, 26, 10, 0, 0,
		0, 13, 73, 72, 68, 82, 0, 0, 0, 8,
		0, 0, 0, 8, 8, 2, 0, 0, 0, 75,
		109, 41, 220, 0, 0, 0, 65, 73, 68, 65,
		84, 8, 29, 85, 142, 81, 10, 0, 48, 8,
		66, 107, 236, 254, 87, 110, 106, 35, 172, 143,
		74, 243, 65, 89, 85, 129, 202, 100, 239, 146,
		115, 184, 183, 11, 109, 33, 29, 126, 114, 141,
		75, 213, 65, 44, 131, 70, 24, 97, 46, 50,
		34, 72, 25, 39, 181, 9, 251, 205, 14, 10,
		78, 123, 43, 35, 17, 17, 228, 109, 164, 219,
		0, 0, 0, 0, 73, 69, 78, 68, 174, 66,
		96, 130
	};

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private STEP mStep;

	private STEP mOldStep;

	private float _onwaittime;

	public GameObject surrogateGameObject
	{
		get
		{
			return mSurrogateGameObject;
		}
	}

	public MonoBehaviour surrogateMonoBehaviour
	{
		get
		{
			return mSurrogateMonoBehaviour;
		}
	}

	protected Request CurrentReq
	{
		get
		{
			return mCurrentReq;
		}
	}

	public int CurrentSeqNo
	{
		get
		{
			if (CurrentReq != null)
			{
				return CurrentReq.seqno;
			}
			return 0;
		}
	}

	protected Response CurrentRes
	{
		get
		{
			if (CurrentReq != null)
			{
				return null;
			}
			return CurrentReq.response;
		}
		set
		{
			if (CurrentReq != null)
			{
				CurrentReq.response = value;
			}
		}
	}

	public abstract SNS Kind { get; }

	public virtual bool IsNetworking
	{
		get
		{
			return mStep == STEP.COMM_WAIT;
		}
	}

	protected STEP CurrentStep
	{
		get
		{
			return mStep;
		}
	}

	protected static string ToSS(object o)
	{
		return (o != null) ? o.ToString() : string.Empty;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(Kind);
		stringBuilder.Append(" ");
		stringBuilder.Append(ToSS(GetType()));
		return stringBuilder.ToString();
	}

	protected static int FireSeqNo()
	{
		mFireSeqNo++;
		if (mFireSeqNo == 0)
		{
			mFireSeqNo++;
		}
		return mFireSeqNo;
	}

	protected bool CallCallback(Handler handler, Response res, object userdata)
	{
		try
		{
			if (handler != null)
			{
				handler(res, userdata);
				return true;
			}
		}
		catch (Exception ex)
		{
			BIJLog.E("CallCallback Exception e = " + ex);
		}
		return false;
	}

	public bool IsValidTexture(ref Texture2D tex)
	{
		if (tex == null)
		{
			return false;
		}
		byte[] second = tex.EncodeToPNG();
		return !Unity_Question_Mark_PNG.SequenceEqual(second);
	}

	protected void SetTickWait(float _timeout)
	{
		mWaitLast = Time.realtimeSinceStartup;
		mWaitDuration = 0f;
		mWaitTimeout = _timeout;
	}

	protected bool CheckTickWait()
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		mWaitDuration += realtimeSinceStartup - mWaitLast;
		mWaitLast = realtimeSinceStartup;
		return mWaitDuration > mWaitTimeout;
	}

	protected static string tag()
	{
		return MethodBase.GetCurrentMethod().DeclaringType.Name;
	}

	protected static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	protected static void log(string format, params object[] args)
	{
	}

	protected static void dbg(string format, params object[] args)
	{
	}

	public virtual bool Init(GameObject go, MonoBehaviour mb)
	{
		mSurrogateGameObject = go;
		mSurrogateMonoBehaviour = mb;
		ChangeStep(STEP.NONE);
		return true;
	}

	public virtual void Term()
	{
		Stop();
	}

	public virtual bool Start()
	{
		ChangeStep(STEP.IDLE);
		return true;
	}

	public virtual void Stop()
	{
		ChangeStep(STEP.NONE);
	}

	public virtual void OnResume()
	{
	}

	public virtual void OnSuspned()
	{
	}

	public virtual bool IsEnabled()
	{
		return false;
	}

	public virtual bool IsOpEnabled(int type)
	{
		return false;
	}

	public virtual int GetOpType()
	{
		return CurrentReq.type;
	}

	public bool IsTargetCallback(int seqno)
	{
		Request currentReq = CurrentReq;
		if (currentReq != null && currentReq.eventcb != null && currentReq.eventcb.EqualsSeqNo(currentReq.seqno))
		{
			return true;
		}
		return false;
	}

	public abstract IEventCallback CreateEventCallback(int seqno);

	public abstract bool ReplyEvent(int seqno, Response res);

	public virtual bool IsLogined()
	{
		return false;
	}

	public abstract void ClearData(Handler handler, object userdata);

	public abstract void Login(Handler handler, object userdata);

	public abstract void Logout(Handler handler, object userdata);

	public abstract void GetUserInfo(string userId, Handler handler, object userdata);

	public abstract void GetFriends(string userId, int limit, Handler handler, object userdata);

	public abstract void FetchUserImage(string userId, Handler handler, object userdata);

	public abstract void PostMessage(IBIJSNSPostData data, Handler handler, object userdata);

	public abstract void Invite(IBIJSNSPostData data, Handler handler, object userdata);

	public abstract void UploadImage(IBIJSNSPostData data, Handler handler, object userdata);

	public abstract void UnlockAchievement(string id, Handler handler, object userdata);

	public abstract void IncrementAchievement(string id, int step, Handler handler, object userdata);

	public abstract void ShowAchievements(Handler handler, object userdata);

	protected void ChangeStep(STEP step)
	{
		mOldStep = mStep;
		mStep = step;
	}

	protected bool IsStepChanged()
	{
		STEP _oldStep = STEP.NONE;
		return IsStepChanged(out _oldStep);
	}

	protected bool IsStepChanged(out STEP _oldStep)
	{
		bool result = mStep != mOldStep;
		_oldStep = mOldStep;
		mOldStep = mStep;
		return result;
	}

	public virtual void Update()
	{
		switch (mStep)
		{
		case STEP.IDLE:
			if (IsStepChanged())
			{
			}
			mCurrentReq = PopReq();
			if (CurrentReq != null)
			{
				if (CurrentReq.eventcb != null)
				{
					CurrentReq.eventcb.Register();
				}
				ChangeStep(STEP.COMM_WAIT);
			}
			break;
		case STEP.COMM_WAIT:
		{
			STEP _oldStep = STEP.NONE;
			if (IsStepChanged(out _oldStep))
			{
				_onwaittime = Time.realtimeSinceStartup;
			}
			OnWait(Time.realtimeSinceStartup - _onwaittime);
			break;
		}
		case STEP.COMM_RESULT:
		{
			Request currentReq = CurrentReq;
			if (currentReq != null)
			{
				if (currentReq.eventcb != null)
				{
					currentReq.eventcb.Unregister();
				}
				CallCallback(currentReq.handler, currentReq.response, currentReq.userdata);
			}
			ChangeStep(STEP.IDLE);
			break;
		}
		}
	}

	public virtual void OnWait(float waitTime)
	{
	}

	protected int PushReq(int type, Handler handler, object userdata)
	{
		IEventCallback eventcb;
		return PushReq(type, handler, userdata, out eventcb);
	}

	protected int PushReq(int type, Handler handler, object userdata, out IEventCallback eventcb)
	{
		eventcb = null;
		if (mStep == STEP.NONE)
		{
			return 0;
		}
		Request request = new Request();
		try
		{
			int seqno = (request.seqno = FireSeqNo());
			request.type = type;
			request.timestamp = Time.realtimeSinceStartup;
			request.handler = handler;
			request.userdata = userdata;
			request.response = null;
			eventcb = CreateEventCallback(seqno);
			request.eventcb = eventcb;
			lock (mRequestQueue)
			{
				mRequestQueue.Add(request);
			}
		}
		catch (Exception)
		{
			return 0;
		}
		return request.seqno;
	}

	protected Request PeekReq()
	{
		if (mStep == STEP.NONE)
		{
			return null;
		}
		Request result = null;
		try
		{
			lock (mRequestQueue)
			{
				if (mRequestQueue.Count > 0)
				{
					result = mRequestQueue[0];
				}
			}
		}
		catch (Exception)
		{
			return null;
		}
		return result;
	}

	protected Request PopReq()
	{
		if (mStep == STEP.NONE)
		{
			return null;
		}
		Request request = null;
		try
		{
			lock (mRequestQueue)
			{
				if (mRequestQueue.Count > 0)
				{
					request = mRequestQueue[0];
					if (!mRequestQueue.Remove(request))
					{
						request = null;
					}
				}
			}
		}
		catch (Exception)
		{
			return null;
		}
		return request;
	}
}
