using System;
using System.Collections.Generic;

public class AdvCheckMasterTableVerProfile_Response : ICloneable
{
	[MiniJSONAlias("masters")]
	public List<AdvMasterDataInfo> MasterTableInfoDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvCheckMasterTableVerProfile_Response Clone()
	{
		return MemberwiseClone() as AdvCheckMasterTableVerProfile_Response;
	}
}
