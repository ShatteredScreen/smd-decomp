public class SCRewardSetResponse : ParameterObject<SCRewardSetResponse>
{
	public int code { get; set; }

	public int code_ex { get; set; }

	public int status { get; set; }

	public long server_time { get; set; }
}
