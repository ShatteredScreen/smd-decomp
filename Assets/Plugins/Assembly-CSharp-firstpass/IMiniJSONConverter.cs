public interface IMiniJSONConverter
{
	object ConvertTo(object obj);

	object ConvertFrom(object json);
}
