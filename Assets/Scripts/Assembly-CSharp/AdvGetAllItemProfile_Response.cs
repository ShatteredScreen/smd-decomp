using System;
using System.Collections.Generic;

public class AdvGetAllItemProfile_Response : ICloneable
{
	[MiniJSONAlias("figures")]
	public List<AdvFigureData> FigureDataList { get; set; }

	[MiniJSONAlias("items")]
	public List<AdvItemData> ItemDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGetAllItemProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGetAllItemProfile_Response;
	}
}
