using System.Collections;
using UnityEngine;

public class BIJSNSSMS : BIJSNSAppSwitcher
{
	public override SNS Kind
	{
		get
		{
			return SNS.SMS;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		if (type == 7 || type == 8)
		{
			return true;
		}
		return false;
	}

	private IEnumerator BIJSNSSMS_PluginFailed(int seqno, string message, object result)
	{
		yield return new WaitForSeconds(0.5f);
		SetSpawn(0);
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSSMS.PluginFailed failed: seqno=" + seqno + " current=" + base.CurrentSeqNo + " message=" + message);
		if (base.CurrentSeqNo == seqno)
		{
			base.CurrentRes = new Response(1, message, result);
			ChangeStep(STEP.COMM_RESULT);
		}
	}

	private void DoPostMessage(OP_TYPE type, IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSSMS.DoPostMessage: " + type);
		if (spawn)
		{
			CallCallback(handler, new Response(1, "SMS already invoked.", null), userdata);
			return;
		}
		int num = PushReq((int)type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string text = BIJSNS.ToSS(postdata.Message);
			string imageUrl = BIJSNS.ToSS(postdata.ImageURL);
			BIJSNS.dbg("@@@ SNS @@@ " + text);
			bool flag = BIJUnity.invokeSMSWithMessage(title, text, imageUrl);
			BIJSNS.log("SNSInvokeSMSWithMessage: " + flag);
			if (!flag)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSSMS_PluginFailed(num, "SMS invoke failed at posting", null));
			}
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "SMS not requested", null), userdata);
		}
	}

	public override void PostMessage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSSMS.PostMessage");
		DoPostMessage(OP_TYPE.POSTMESSAGE, postdata, handler, userdata);
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSSMS.UploadImage");
		DoPostMessage(OP_TYPE.UPLOADIMAGE, postdata, handler, userdata);
	}

	public override void Invite(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSSMS.Invite");
		DoPostMessage(OP_TYPE.INVITE, postdata, handler, userdata);
	}
}
