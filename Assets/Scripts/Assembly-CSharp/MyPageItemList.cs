using System.Collections.Generic;
using UnityEngine;

public class MyPageItemList : ListBase<MyPageItem>
{
	protected const float LIST_WIDTH = 520f;

	protected const float LIST_HEIGHT = 490f;

	private GameMain mGame;

	public MonoBehaviour messageTaget;

	public List<MyPageItem> items = new List<MyPageItem>();

	private float posX;

	private float posY;

	public bool initilized;

	private int gamestate;

	public GameObject mTween;

	public static MyPageItemList Make(GameObject _parent, MonoBehaviour _messageTaget, List<MyPageItem> _items, float _posX, float _posY, int _baseDepth)
	{
		GameObject gameObject = Util.CreateGameObject("MyPageItemList", _parent);
		MyPageItemList myPageItemList = gameObject.AddComponent<MyPageItemList>();
		myPageItemList.items = _items;
		myPageItemList.posX = _posX;
		myPageItemList.posY = _posY;
		myPageItemList.mBaseDepth = _baseDepth;
		myPageItemList.messageTaget = _messageTaget;
		return myPageItemList;
	}

	public static void MakeAddItem(List<MyPageItem> _tgList, MyPageItem.KIND _kind, CompanionData _companionData)
	{
		MyPageItem myPageItem = new MyPageItem();
		myPageItem.kind = _kind;
		myPageItem.companionData = _companionData;
		_tgList.Add(myPageItem);
	}

	public override void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	protected override int GetScrollingDeltaY()
	{
		return 1;
	}

	protected override int GetCellHeight()
	{
		return 168;
	}

	protected void CreateWindow(GameObject parent, string name)
	{
		GameObject gameObject = Util.CreateGameObject("ListParentObject", parent);
		gameObject.transform.localPosition = new Vector3(posX, posY, 0f);
		mTween = gameObject;
		mWindowRect = new Rect(posX - 260f, posY - 245f, 520f, 490f);
	}

	protected override void CreateContents(GameObject parent, string name)
	{
		CreateList(parent, "List");
	}

	protected override void CreateScrollBar(GameObject parent, string name)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UISprite uISprite = Util.CreateSprite("scf", parent, image);
		Util.SetSpriteInfo(uISprite, "173", mBaseDepth + 3, new Vector3(253f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 14;
		uISprite.height = 490;
		UISprite uISprite2 = Util.CreateSprite("scf", parent, image);
		Util.SetSpriteInfo(uISprite2, "174", mBaseDepth + 2, new Vector3(253f, 0f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.width = 14;
		uISprite2.height = 490;
		GameObject gameObject = Util.CreateGameObject("Scroll", parent);
		mScroll = gameObject.AddComponent<UIScrollBar>();
		mScroll.fillDirection = UIProgressBar.FillDirection.TopToBottom;
		mScroll.foregroundWidget = uISprite;
		mScroll.backgroundWidget = uISprite2;
	}

	protected override void CreateItem(GameObject itemGO, int index, MyPageItem item, MyPageItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		float cellWidth = mList.cellWidth;
		float cellHeight = mList.cellHeight;
		float num = 0f;
		float num2 = 0f;
		float num3 = mList.cellWidth - 15f;
		float num4 = mList.cellHeight - 4f;
		UISprite uISprite = Util.CreateSprite("Board", itemGO, image);
		Util.SetSpriteInfo(uISprite, "149", mBaseDepth + 1, new Vector3(-10f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(500, 165);
		if (item.kind == MyPageItem.KIND.COMPANION)
		{
			UILabel uILabel = Util.CreateLabel("Name", itemGO, atlasFont);
			Util.SetLabelInfo(uILabel, item.companionData.name, mBaseDepth + 2, new Vector3(60f, 52f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(360, 24);
			uISprite = Util.CreateSprite("Icon", itemGO, image);
			Util.SetSpriteInfo(uISprite, item.companionData.iconName, mBaseDepth + 2, new Vector3(-176f, 0f, 0f), Vector3.one, false);
			uISprite = Util.CreateSprite("Selected", itemGO, image);
			Util.SetSpriteInfo(uISprite, "14", mBaseDepth + 3, new Vector3(-176f, -60f, 0f), Vector3.one, false);
			if (mGame.mPlayer.Data.SelectedCompanion != item.companionData.index)
			{
				uISprite.enabled = false;
			}
			item.selectedIcon = uISprite;
			string text;
			bool buttonEnable;
			if (mGame.mPlayer.Data.SelectedCompanion == item.companionData.index)
			{
				text = "22";
				buttonEnable = false;
				item.selected = true;
			}
			else
			{
				text = "22";
				buttonEnable = true;
				item.selected = false;
			}
			UIButton uIButton = Util.CreateJellyImageButton("ChangeButton", itemGO, image);
			Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 2, new Vector3(100f, -20f, 0f), Vector3.one);
			item.button = uIButton.gameObject.GetComponent<JellyImageButton>();
			item.button.SetButtonEnable(buttonEnable);
			Util.AddImageButtonMessage(uIButton, messageTaget, "OnCompanionChangePushed", UIButtonMessage.Trigger.OnClick);
		}
	}

	public override void Update()
	{
		switch (gamestate)
		{
		case 0:
			ResetContents();
			CreateWindow(base.gameObject, "Window");
			CreateContents(mTween, "Contents");
			UpdateList();
			initilized = true;
			gamestate = 4096;
			break;
		case 4096:
			OnScrollAdjust();
			gamestate = 8192;
			break;
		}
	}

	private void ResetContents()
	{
		if (mTween != null && mTween.gameObject != null)
		{
			GameMain.SafeDestroy(mTween.gameObject);
			mTween = null;
		}
	}

	public void Reconstruct()
	{
		gamestate = 0;
	}

	public new int ListIndexOf(GameObject go)
	{
		if (mMapItems == null || mMapItems.Count < 1)
		{
			return -1;
		}
		int value = -1;
		if (!mMapItems.TryGetValue(go, out value))
		{
			return -1;
		}
		return value;
	}

	public new int FindListIndexOf(GameObject go)
	{
		UIDragScrollView uIDragScrollView = NGUITools.FindInParents<UIDragScrollView>(go);
		if (uIDragScrollView == null)
		{
			return -1;
		}
		return ListIndexOf(uIDragScrollView.gameObject);
	}

	public MyPageItem FindListItem(GameObject go)
	{
		int num = FindListIndexOf(go);
		if (num < 0 || items.Count <= num)
		{
			return null;
		}
		return items[num];
	}

	public void RemoveListItem(MyPageItem item)
	{
		items.Remove(item);
		Reconstruct();
	}

	public void ClearListItem()
	{
		items.Clear();
	}

	protected override List<MyPageItem> GetListItem()
	{
		return items;
	}
}
