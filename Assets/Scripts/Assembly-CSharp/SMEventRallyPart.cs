using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEventRallyPart : SMMapPart
{
	protected const string EVENT_IMAGE_NAME = "event_map_{0:00}";

	private short mCourseID;

	public void SetCourseID(short a_couseID)
	{
		mCourseID = a_couseID;
	}

	public override void Init(Def.SERIES a_series, int a_index, Vector3 a_mapScale, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadblockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB, bool a_isAvailable, bool useBg = true)
	{
		mSeries = a_series;
		mMapScale = a_mapScale;
		mMapIndex = a_index;
		if (useBg)
		{
			BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
			mBGImage = Util.CreateGameObject("BG" + (a_index + 1), base.gameObject).AddComponent<SMEventBG>();
			mBGImage.Active(image, "null", Vector3.zero, mMapScale);
		}
		else
		{
			mBGImage = null;
		}
		mMapAccessoryCallback = a_accessoryCB;
		mMapRoadBlockCallback = a_roadblockCB;
		mMapStrayUserCallback = a_snsCB;
		mMapChangeCallback = a_mapChangeCB;
		mIsAvailable = a_isAvailable;
	}

	protected override void Update()
	{
		if (mMovableObjectSpawnList.Count <= 0)
		{
			return;
		}
		mMovableObjectSpawnTime += Time.deltaTime;
		if (mMovableObjectSpawnTime > 5f)
		{
			mMovableObjectSpawnTime = 0f;
			int num = Random.Range(0, mMovableObjectSpawnList.Count);
			MovableObjectSpawn movableObjectSpawn = mMovableObjectSpawnList[num];
			MapMovableObject mapMovableObject = Util.CreateGameObject("MovableObject", mGameState.MapPageRoot).AddComponent<MapMovableObject>();
			float a_deg = 0f;
			bool a_noAngle = true;
			if (movableObjectSpawn.AngleRange > 0f)
			{
				a_deg = Random.Range((0f - movableObjectSpawn.AngleRange) / 2f, movableObjectSpawn.AngleRange / 2f);
				a_noAngle = false;
			}
			string movableAnimeKey = GetMovableAnimeKey(mSeries, mMapIndex, movableObjectSpawn.AnimeKey);
			mapMovableObject.Init(0, movableObjectSpawn.AnimeKey, movableObjectSpawn.Atlas, movableObjectSpawn.Position.x, movableObjectSpawn.Position.y, Vector3.one, a_deg, num, mMapIndex, mSeries, a_noAngle, movableAnimeKey);
			mapMovableObject.SetDisappearedCallback(base.OnMovableObjectDisappearedCallback);
			mMapMovableObjectList.Add(mapMovableObject);
		}
	}

	public override IEnumerator Active(SMMapSsData a_mapSsData, int a_mapIndex, bool a_inPlayer)
	{
		List<SsMapEntity> accessoryList = a_mapSsData.GetAccessoryListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapEntityList = a_mapSsData.GetMapEntityListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapRBList = a_mapSsData.GetMapRBListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapERBList = a_mapSsData.GetMapERBListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapEmitterList = a_mapSsData.GetMapEmitterListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapMovableList = a_mapSsData.GetMapMoveObjectListInMapIndex(a_mapIndex);
		List<SsMapEntity> strayUserList = a_mapSsData.GetMapStrayUserListInMapIndex(a_mapIndex);
		List<SsMapEntity> silhouetteList = a_mapSsData.GetMapSilhouetteListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapSignList = a_mapSsData.GetMapSignListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapMaskList = a_mapSsData.GetMapMaskListInMapIndex(a_mapIndex);
		IsActiveLoading = true;
		mStrayUserEntityList = strayUserList;
		string map_key = GetMapImageKey();
		NGUITools.SetActive(base.gameObject, true);
		ResImage mapRes = ResourceManager.LoadImageAsync(map_key);
		ResSsAnimation endAnime = ResourceManager.LoadSsAnimationAsync("MAP_END", true);
		for (int i14 = 0; i14 < mapEmitterList.Count; i14++)
		{
			SsMapEntity entity = mapEmitterList[i14];
			EnvParticle accesory = CreateMapEmitter(entity.MapOffsetCounter, entity.AnimeKey, entity.Position, entity.Scale);
			mMapEmitterList.Add(accesory);
		}
		while (mapRes.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		BIJImage atlasMap = mapRes.Image;
		if (mBGImage != null)
		{
			mBGImage.Active(atlasMap, string.Format("event_map_{0:00}", mMapIndex + 1), new Vector3(0f, 0f, Def.MAP_BASE_Z), mMapScale);
		}
		if (accessoryList.Count > 0)
		{
			string accessoryAnimeKey = GetMapAccessoryAnimeKey(mSeries, mMapIndex);
			ResSsAnimation accessoryAnime = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey, true);
			ResSsAnimation accessoryAnimeTap = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey + "_TAP", true);
			ResSsAnimation accessoryAnimeTapLock = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey + "_TAPLOCK", true);
			ResSsAnimation accessoryAnimeLock = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey + "_LOCK", true);
			while (accessoryAnime.LoadState != ResourceInstance.LOADSTATE.DONE || accessoryAnimeTap.LoadState != ResourceInstance.LOADSTATE.DONE || accessoryAnimeTapLock.LoadState != ResourceInstance.LOADSTATE.DONE || accessoryAnimeLock.LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
			for (int i13 = 0; i13 < accessoryList.Count; i13++)
			{
				SsMapEntity entity11 = accessoryList[i13];
				if (entity11.EnableDisplay)
				{
					EventAccessory accessory6 = CreateMapAccessory(data: mGame.GetAccessoryData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, mCourseID, entity11.AnimeKey), counter: entity11.MapOffsetCounter, animeName: string.Empty, atlas: atlasMap, pos: entity11.Position, scale: entity11.Scale);
					mMapAccessoryList.Add(accessory6);
				}
			}
		}
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		for (int i12 = 0; i12 < mapEntityList.Count; i12++)
		{
			string animeKey = mapEntityList[i12].AnimeKey;
			ResSsAnimation entityAnime = ResourceManager.LoadSsAnimationAsync(animeKey, true);
			asyncList.Add(entityAnime);
		}
		for (int i11 = 0; i11 < asyncList.Count; i11++)
		{
			while (asyncList[i11].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		for (int i10 = 0; i10 < mapEntityList.Count; i10++)
		{
			SsMapEntity entity2 = mapEntityList[i10];
			MapEntity accesory2 = CreateMapEntity(entity2.MapOffsetCounter, entity2.AnimeKey, atlasMap, entity2.Position, entity2.Scale);
			mMapEntityList.Add(accesory2);
		}
		BIJImage atlasCommon = mapRes.Image;
		if (mapRBList.Count > 0)
		{
			asyncList.Clear();
			string animeKey = GetEventRBAnimeKey(mSeries, mMapIndex);
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_UNLOCK", true));
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_UNLOCKED", true));
			for (int i9 = 0; i9 < asyncList.Count; i9++)
			{
				while (asyncList[i9].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
			}
			int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
			for (int i8 = 0; i8 < mapRBList.Count; i8++)
			{
				SsMapEntity entity10 = mapRBList[i8];
				float order2 = float.Parse(entity10.AnimeKey);
				bool isUnlocked = true;
				int roadLevel = Def.GetStageNoByRouteOrder(order2);
				if (nextRoadBlockLevel <= roadLevel)
				{
					isUnlocked = false;
				}
				SMMapPageData mapData3 = mSMMapPageData;
				SMEventRoadBlockSetting setting2 = null;
				int mainStage;
				int subStage;
				Def.SplitStageNo(roadLevel, out mainStage, out subStage);
				foreach (KeyValuePair<string, SMRoadBlockSetting> roadBlockData in mapData3.RoadBlockDataList)
				{
					SMRoadBlockSetting s2 = roadBlockData.Value;
					if (s2.mStageNo == mainStage)
					{
						setting2 = s2 as SMEventRoadBlockSetting;
						break;
					}
				}
				if (setting2 != null)
				{
					MapRoadBlock accessory5 = CreateRoadBlock(entity10.MapOffsetCounter, entity10.AnimeKey, setting2, atlasCommon, entity10.Position, entity10.Scale, isUnlocked, entity10.Angle);
					mMapRBList.Add(accessory5);
				}
			}
		}
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out eventData);
		bool isUpdate = false;
		if (mapERBList.Count > 0)
		{
			asyncList.Clear();
			string animeKey = GetEventRBAnimeKey2(mSeries, mMapIndex) + "_LOCK";
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			animeKey = GetEventRBAnimeKey2(mSeries, mMapIndex) + "_UNLOCK";
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			for (int i7 = 0; i7 < asyncList.Count; i7++)
			{
				while (asyncList[i7].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
			}
			for (int i6 = 0; i6 < mapERBList.Count; i6++)
			{
				SsMapEntity entity9 = mapERBList[i6];
				float order = float.Parse(entity9.AnimeKey);
				SMEventPageData eventPageData = mSMMapPageData as SMEventPageData;
				List<SMRoadBlockSetting> courseRoadBlockList = eventPageData.GetRoadBlocks(mCourseID);
				int stageNo = Def.GetStageNoByRouteOrder(order);
				SMEventRoadBlockSetting setting = null;
				for (int x = 0; x < courseRoadBlockList.Count; x++)
				{
					SMEventRoadBlockSetting s = courseRoadBlockList[x] as SMEventRoadBlockSetting;
					int roadStageNo2 = Def.GetStageNo(s.mStageNo, s.mSubNo);
					if (roadStageNo2 == stageNo)
					{
						setting = s;
						break;
					}
				}
				if (setting == null)
				{
					continue;
				}
				if (setting.UnlockStars != 0)
				{
					if (mGame.mPlayer.IsAccessoryUnlock(setting.UnlockAccessoryID()))
					{
						PlayerMapData mapData2 = eventData.CourseData[mCourseID];
						if (mapData2.IsUnlockedRoadBlock(setting.RoadBlockID))
						{
							continue;
						}
						int roadStageNo = Def.GetStageNo(setting.mStageNo, setting.mSubNo);
						if (mCourseID == eventData.CourseID && !mGame.mPlayer.UnlockedRBList.Contains(roadStageNo))
						{
							mGame.mPlayer.UnlockedRBList.Add(roadStageNo);
							mapData2.UpdateUnlockedRoadBlock(setting.RoadBlockID);
							eventData.CourseData[mCourseID] = mapData2;
							isUpdate = true;
							if (mapData2.LineStatus.ContainsKey(roadStageNo) && mapData2.LineStatus[roadStageNo] != Player.STAGE_STATUS.LOCK)
							{
							}
						}
						EventRoadBlock accessory4 = CreateEventRoadBlock(a_animeKey: GetEventRBAnimeKey2(mSeries, mMapIndex), counter: entity9.MapOffsetCounter, animeName: entity9.AnimeKey, a_data: setting, atlas: atlasCommon, pos: entity9.Position, scale: entity9.Scale, a_isUnlocked: false, a_angle: entity9.Angle);
						mEventRBList.Add(accessory4);
					}
					else
					{
						EventRoadBlock accessory3 = CreateEventRoadBlock(a_animeKey: GetEventRBAnimeKey2(mSeries, mMapIndex), counter: entity9.MapOffsetCounter, animeName: entity9.AnimeKey, a_data: setting, atlas: atlasCommon, pos: entity9.Position, scale: entity9.Scale, a_isUnlocked: false, a_angle: entity9.Angle);
						mEventRBList.Add(accessory3);
					}
				}
				else
				{
					mGame.mPlayer.UnlockAccessory(setting.UnlockAccessoryID());
					PlayerMapData mapData = eventData.CourseData[mCourseID];
					mapData.UpdateUnlockedRoadBlock(setting.RoadBlockID);
				}
			}
		}
		if (isUpdate)
		{
			mGame.mPlayer.Data.SetMapData(eventData.EventID, eventData);
		}
		if (mapSignList.Count > 0)
		{
			asyncList.Clear();
			string animeKey = GetSignAnimeKey(mSeries, mMapIndex) + "_LOCK";
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			animeKey = GetSignAnimeKey(mSeries, mMapIndex) + "_UNLOCK";
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			animeKey = GetSignAnimeKey(mSeries, mMapIndex) + "_UNLOCK_IDLE";
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			for (int i5 = 0; i5 < asyncList.Count; i5++)
			{
				while (asyncList[i5].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
			}
			for (int i4 = 0; i4 < mapSignList.Count; i4++)
			{
				SsMapEntity entity8 = mapSignList[i4];
				EventSign accessory2 = CreateSign(entity8.MapOffsetCounter, entity8.AnimeKey, atlasMap, entity8.Position, entity8.Scale);
				mMapSignList.Add(accessory2);
			}
		}
		if (mapMaskList.Count > 0)
		{
			asyncList.Clear();
			string animeKey = GetMaskAnimeKey(mSeries, mMapIndex);
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			for (int i3 = 0; i3 < asyncList.Count; i3++)
			{
				while (asyncList[i3].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
			}
			for (int i2 = 0; i2 < mapMaskList.Count; i2++)
			{
				SsMapEntity entity7 = mapMaskList[i2];
				bool isLock = true;
				MapMask accessory = CreateMask(entity7.MapOffsetCounter, entity7.AnimeKey, atlasMap, entity7.Position, entity7.Scale, isLock);
				mMapMaskList.Add(accessory);
			}
		}
		asyncList.Clear();
		for (int n = 0; n < mapMovableList.Count; n++)
		{
			SsMapEntity entity3 = mapMovableList[n];
			string animeKey = GetMovableAnimeKey(mSeries, mMapIndex, entity3.AnimeKey);
			ResSsAnimation entityAnime2 = ResourceManager.LoadSsAnimationAsync(animeKey, true);
			asyncList.Add(entityAnime2);
		}
		for (int m = 0; m < asyncList.Count; m++)
		{
			while (asyncList[m].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		for (int l = 0; l < mapMovableList.Count; l++)
		{
			SsMapEntity entity4 = mapMovableList[l];
			MovableObjectSpawn spawn = CreateMovableObjectSpawn(entity4.MapOffsetCounter, entity4.AnimeKey, atlasMap, entity4.Position, entity4.Angle);
			mMovableObjectSpawnList.Add(spawn);
			yield return 0;
		}
		MakeStrayUser(a_inPlayer);
		if (!mIsAvailable && mMapEndAnime == null)
		{
			while (endAnime.LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
			mMapEndAnime = Util.CreateLoopAnime("MapEnd" + (mMapIndex + 1), "MAP_END", base.gameObject, new Vector3(0f, 0f, Def.MAP_END_Z), Vector3.one, 0f, 0f, false, null);
		}
		yield return 0;
		asyncList.Clear();
		for (int k = 0; k < silhouetteList.Count; k++)
		{
			SsMapEntity entity5 = silhouetteList[k];
			if (entity5.EnableDisplay)
			{
				string animName = "SILHOUETTE_" + entity5.AnimeKey;
				ResSsAnimation entityAnime3 = ResourceManager.LoadSsAnimationAsync(animName, true);
				asyncList.Add(entityAnime3);
			}
		}
		for (int j = 0; j < asyncList.Count; j++)
		{
			while (asyncList[j].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		for (int i = 0; i < silhouetteList.Count; i++)
		{
			SsMapEntity entity6 = silhouetteList[i];
			if (entity6.EnableDisplay)
			{
				string animName2 = "SILHOUETTE_" + entity6.AnimeKey;
				Vector3 pos = entity6.Position;
				pos.z = Def.MAP_SILHOUETTE_Z;
				MapEntity accesory3 = CreateMapEntity(entity6.MapOffsetCounter, animName2, atlasMap, pos, entity6.Scale);
				mMapEntityList.Add(accesory3);
				yield return 0;
			}
		}
		IsActiveLoading = false;
		yield return 0;
	}

	public new EventSign CreateSign(int counter, string animeName, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		EventSign eventSign = Util.CreateGameObject("Sign" + mMapIndex, base.gameObject).AddComponent<EventSign>();
		eventSign.SetCourseID(mCourseID);
		eventSign.Init(counter, animeName, atlas, pos.x, pos.y, scale, mMapIndex, GetSignAnimeKey(mSeries, mMapIndex));
		return eventSign;
	}

	public override MapRoadBlock CreateRoadBlock(int counter, string animeName, SMRoadBlockSetting a_data, BIJImage atlas, Vector3 pos, Vector3 scale, bool a_isUnlocked, float a_angle)
	{
		EventStarRoadBlock eventStarRoadBlock = Util.CreateGameObject("RB" + counter, base.gameObject).AddComponent<EventStarRoadBlock>();
		eventStarRoadBlock.SetCourseID(mCourseID);
		eventStarRoadBlock.Init(counter, animeName, a_data, atlas, pos.x, pos.y, scale, a_isUnlocked, a_angle, GetEventRBAnimeKey(mSeries, mMapIndex));
		eventStarRoadBlock.SetPushedCallback(mMapRoadBlockCallback);
		return eventStarRoadBlock;
	}

	public EventRoadBlock GetEventRoadBlock(int a_stage)
	{
		for (int i = 0; i < mEventRBList.Count; i++)
		{
			int stageNo = Def.GetStageNo(mEventRBList[i].mData.mStageNo, mEventRBList[i].mData.mSubNo);
			if (stageNo == a_stage)
			{
				return mEventRBList[i];
			}
		}
		return null;
	}

	public override bool UnlockRoadBlock(int a_roadStage)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_roadStage, out a_main, out a_sub);
		foreach (EventRoadBlock mEventRB in mEventRBList)
		{
			if (mEventRB.mData.mStageNo == a_main && mEventRB.mData.mSubNo == a_sub)
			{
				mEventRB.UnlockAnime();
				return true;
			}
		}
		return false;
	}

	public EventRoadBlock CreateEventRoadBlock(int counter, string animeName, SMRoadBlockSetting a_data, BIJImage atlas, Vector3 pos, Vector3 scale, bool a_isUnlocked, float a_angle, string a_animeKey)
	{
		EventRoadBlock eventRoadBlock = Util.CreateGameObject("RB" + a_data.RoadBlockID, base.gameObject).AddComponent<EventRoadBlock>();
		eventRoadBlock.Init(counter, animeName, a_data, atlas, pos.x, pos.y, scale, a_isUnlocked, a_angle, a_animeKey);
		eventRoadBlock.SetPushedCallback(null);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			eventRoadBlock.SetDebugInfo(a_data);
		}
		return eventRoadBlock;
	}

	public new EventAccessory CreateMapAccessory(int counter, string animeName, AccessoryData data, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		bool a_alreadyHas = false;
		if (mGame.mPlayer.IsAccessoryUnlock(data.Index))
		{
			a_alreadyHas = true;
		}
		EventAccessory eventAccessory = Util.CreateGameObject("Accessory" + data.Index, base.gameObject).AddComponent<EventAccessory>();
		eventAccessory.SetCourseID(mCourseID);
		string mapAccessoryAnimeKey = GetMapAccessoryAnimeKey(mSeries, mMapIndex);
		eventAccessory.Init(counter, animeName, data, atlas, pos.x, pos.y, scale, a_alreadyHas, mMapIndex, mSeries, mapAccessoryAnimeKey);
		eventAccessory.SetPushedCallback(mMapAccessoryCallback);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			eventAccessory.SetDebugInfo(data);
		}
		return eventAccessory;
	}

	protected int GetEventID()
	{
		SMEventPageData sMEventPageData = mSMMapPageData as SMEventPageData;
		return sMEventPageData.EventID;
	}

	public override string GetMapImageKey()
	{
		string text = Def.SeriesPrefix[mSeries];
		SMEventPageData sMEventPageData = mSMMapPageData as SMEventPageData;
		return sMEventPageData.EventSetting.MapAtlasKey;
	}

	public override string GetMapAccessoryAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_ACCESSORY_{1}", seriesString, a_mapIndex + 1);
	}

	public override string GetButtonAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_STAGEBUTTON", seriesString, a_mapIndex + 1);
	}

	public override string GetLineAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_LINE", seriesString, a_mapIndex + 1);
	}

	public string GetEventRBAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_ROADBLOCK", seriesString, a_mapIndex + 1);
	}

	public string GetEventRBAnimeKey2(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_ROADBLOCK2", seriesString, a_mapIndex + 1);
	}

	public override string GetSignAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		SMEventPageData sMEventPageData = mSMMapPageData as SMEventPageData;
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		int key = mCourseID;
		SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[key];
		return sMEventCourseSetting.SignAnimeKey;
	}

	public override string GetMaskAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_MASK_{1}", seriesString, a_mapIndex + 1);
	}

	public override string GetMovableAnimeKey(Def.SERIES a_series, int a_mapIndex, string a_name)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_MOVE_{0}_{1}_", seriesString, a_mapIndex + 1) + a_name;
	}

	public override void OpenNewArea()
	{
		float y = mMapEndAnime.transform.localPosition.y;
		OpenNewAreaHide();
		Util.CreateOneShotAnime("MapEndUnlock" + (mMapIndex + 1), "MAP_END_UNLOCK", base.gameObject, new Vector3(0f, y, Def.MAP_END_Z), Vector3.one, 0f, 0f);
	}
}
