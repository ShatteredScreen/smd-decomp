using System;
using UnityEngine;

public class MessageBar : MonoBehaviour
{
	public enum STATE
	{
		IN = 0,
		WAIT = 1,
		OUT = 2,
		END = 3
	}

	public delegate void FinishDelegate();

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.IN);

	private GameMain mGame;

	private int mBaseDepth = 60;

	private float mMoveAngle;

	private float mWaitTime;

	private float mBaseY;

	private FinishDelegate OnFinished = delegate
	{
	};

	private void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.IN:
		{
			if (mState.IsChanged())
			{
				mMoveAngle = 0f;
			}
			mMoveAngle += Time.deltaTime * 480f;
			if (mMoveAngle >= 90f)
			{
				mMoveAngle = 90f;
				mState.Change(STATE.WAIT);
			}
			float num = 1f - Mathf.Sin(mMoveAngle * ((float)Math.PI / 180f));
			base.transform.localPosition = new Vector3(0f, mBaseY + 580f * num, 0f);
			break;
		}
		case STATE.WAIT:
			if (mState.IsChanged())
			{
				mWaitTime = 3f;
			}
			mWaitTime -= Time.deltaTime;
			if (mWaitTime <= 0f || (mGame.mTouchDownTrg && mWaitTime <= 2f))
			{
				mState.Change(STATE.OUT);
			}
			break;
		case STATE.OUT:
		{
			if (mState.IsChanged())
			{
				mMoveAngle = 90f;
			}
			mMoveAngle -= Time.deltaTime * 480f;
			if (mMoveAngle <= 0f)
			{
				mMoveAngle = 0f;
				mState.Change(STATE.END);
				OnFinished();
			}
			float num = 1f - Mathf.Sin(mMoveAngle * ((float)Math.PI / 180f));
			base.transform.localPosition = new Vector3(0f, mBaseY - 580f * num, 0f);
			break;
		}
		case STATE.END:
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		mState.Update();
	}

	public void Init(string message, string iconImageName, FinishDelegate finishCallback)
	{
		UIPanel uIPanel = base.gameObject.AddComponent<UIPanel>();
		uIPanel.depth = 40;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		mBaseY = 0f;
		base.transform.localPosition = new Vector3(0f, mBaseY + 580f, 0f);
		UISprite uISprite = Util.CreateSprite("Bar", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "message_bar", mBaseDepth, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions(1386, 101);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, message, mBaseDepth + 1, Vector3.zero, 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(31f / 51f, 0.6f, 31f / 85f, 1f);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(500, 32);
		if (iconImageName != string.Empty)
		{
			uISprite = Util.CreateSprite("Icon", base.gameObject, image);
			Util.SetSpriteInfo(uISprite, iconImageName, mBaseDepth + 1, Vector3.zero, Vector3.one, false);
			uILabel.transform.localPosition = new Vector3((float)uISprite.width / 2f, 0f, 0f);
			uISprite.transform.localPosition = new Vector3(0f - uILabel.printedSize.x / 2f - 10f, 0f, 0f);
		}
		if (finishCallback != null)
		{
			OnFinished = finishCallback;
		}
	}

	public bool IsFinished()
	{
		if (mState.GetStatus() == STATE.END)
		{
			return true;
		}
		return false;
	}
}
