public class BIJImageOffset
{
	public string name;

	public short x;

	public short y;

	public short width;

	public short height;

	public short paddingLeft;

	public short paddingTop;

	public short paddingRight;

	public short paddingBottom;

	public BIJImageOffset()
		: this(null, 0, 0, 0, 0, 0, 0, 0, 0)
	{
	}

	public BIJImageOffset(string name, short x, short y, short width, short height, short paddingLeft, short paddingTop, short paddingRight, short paddingBottom)
	{
		this.name = name;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.paddingLeft = paddingLeft;
		this.paddingTop = paddingTop;
		this.paddingRight = paddingRight;
		this.paddingBottom = paddingBottom;
	}
}
