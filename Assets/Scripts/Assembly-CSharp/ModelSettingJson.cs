using System.Collections.Generic;
using System.Text;
using live2d;

public class ModelSettingJson : ModelSetting
{
	private const string NAME = "name";

	private const string ID = "id";

	private const string MODEL = "model";

	private const string TEXTURES = "textures";

	private const string HIT_AREAS = "hit_areas";

	private const string PHYSICS = "physics";

	private const string POSE = "pose";

	private const string EXPRESSIONS = "expressions";

	private const string MOTION_GROUPS = "motions";

	private const string SOUND = "sound";

	private const string FADE_IN = "fade_in";

	private const string FADE_OUT = "fade_out";

	private const string VALUE = "val";

	private const string FILE = "file";

	private const string EYEBLINK = "eyeblink";

	private const string INIT_PARTS_VISIBLE = "init_parts_visible";

	private const string INIT_PARAM = "init_param";

	private const string LAYOUT = "layout";

	private Value json;

	public ModelSettingJson(string str)
	{
		char[] jsonBytes = str.ToCharArray();
		json = Json.parseFromBytes(jsonBytes);
	}

	public ModelSettingJson(byte[] bytes)
	{
		json = Json.parseFromBytes(Encoding.UTF8.GetChars(bytes));
	}

	public bool ExistMotion(string name)
	{
		return json.get("motions").getMap(null).ContainsKey(name);
	}

	public bool ExistMotionExpandParam(string name, int n, string expand)
	{
		return json.get("motions").get(name).get(n)
			.getMap(null)
			.ContainsKey(expand);
	}

	public bool ExistMotionSound(string name, int n)
	{
		return json.get("motions").get(name).get(n)
			.getMap(null)
			.ContainsKey("sound");
	}

	public bool ExistMotionFadeIn(string name, int n)
	{
		return json.get("motions").get(name).get(n)
			.getMap(null)
			.ContainsKey("fade_in");
	}

	public bool ExistMotionFadeOut(string name, int n)
	{
		return json.get("motions").get(name).get(n)
			.getMap(null)
			.ContainsKey("fade_out");
	}

	public string GetModelName()
	{
		if (!json.getMap(null).ContainsKey("name"))
		{
			return null;
		}
		return json.get("name").toString();
	}

	public string GetModelFile()
	{
		if (!json.getMap(null).ContainsKey("model"))
		{
			return null;
		}
		return json.get("model").toString();
	}

	public int GetTextureNum()
	{
		if (!json.getMap(null).ContainsKey("textures"))
		{
			return 0;
		}
		return json.get("textures").getVector(null).Count;
	}

	public string GetTextureFile(int n)
	{
		return json.get("textures").get(n).toString();
	}

	public int GetHitAreasNum()
	{
		if (!json.getMap(null).ContainsKey("hit_areas"))
		{
			return 0;
		}
		return json.get("hit_areas").getVector(null).Count;
	}

	public string GetHitAreaID(int n)
	{
		return json.get("hit_areas").get(n).get("id")
			.toString();
	}

	public string GetHitAreaName(int n)
	{
		return json.get("hit_areas").get(n).get("name")
			.toString();
	}

	public string GetPhysicsFile()
	{
		if (!json.getMap(null).ContainsKey("physics"))
		{
			return null;
		}
		return json.get("physics").toString();
	}

	public string GetPoseFile()
	{
		if (!json.getMap(null).ContainsKey("pose"))
		{
			return null;
		}
		return json.get("pose").toString();
	}

	public int GetMotionNum(string name)
	{
		if (!ExistMotion(name))
		{
			return 0;
		}
		return json.get("motions").get(name).getVector(null)
			.Count;
	}

	public string GetMotionFile(string name, int n)
	{
		if (!ExistMotion(name))
		{
			return null;
		}
		return json.get("motions").get(name).get(n)
			.get("file")
			.toString();
	}

	public string GetMotionExpandParam(string name, int n, string expand)
	{
		if (!ExistMotion(name))
		{
			return null;
		}
		if (!ExistMotionExpandParam(name, n, expand))
		{
			return null;
		}
		return json.get("motions").get(name).get(n)
			.get(expand)
			.toString();
	}

	public bool IsMotionExpandParam(string name, int n, string expand)
	{
		string motionExpandParam = GetMotionExpandParam(name, n, expand);
		if (motionExpandParam == null)
		{
			return false;
		}
		int result = 0;
		if (!int.TryParse(motionExpandParam, out result))
		{
			return false;
		}
		if (result == 0)
		{
			return false;
		}
		return true;
	}

	public string GetMotionSound(string name, int n)
	{
		if (!ExistMotionSound(name, n))
		{
			return null;
		}
		return json.get("motions").get(name).get(n)
			.get("sound")
			.toString();
	}

	public int GetMotionFadeIn(string name, int n)
	{
		return ExistMotionFadeIn(name, n) ? json.get("motions").get(name).get(n)
			.get("fade_in")
			.toInt() : 1000;
	}

	public int GetMotionFadeOut(string name, int n)
	{
		return ExistMotionFadeOut(name, n) ? json.get("motions").get(name).get(n)
			.get("fade_out")
			.toInt() : 1000;
	}

	public string[] GetMotionGroupNames()
	{
		if (!json.getMap(null).ContainsKey("motions"))
		{
			return null;
		}
		object[] array = json.get("motions").keySet().ToArray();
		if (array.Length == 0)
		{
			return null;
		}
		string[] array2 = new string[array.Length];
		for (int i = 0; i < array2.Length; i++)
		{
			array2[i] = (string)array[i];
		}
		return array2;
	}

	public bool GetLayout(Dictionary<string, float> layout)
	{
		if (!json.getMap(null).ContainsKey("layout"))
		{
			return false;
		}
		Dictionary<string, Value> map = json.get("layout").getMap(null);
		string[] array = new string[map.Count];
		map.Keys.CopyTo(array, 0);
		for (int i = 0; i < array.Length; i++)
		{
			layout.Add(array[i], json.get("layout").get(array[i]).toFloat());
		}
		return true;
	}

	public int GetInitParamNum()
	{
		if (!json.getMap(null).ContainsKey("init_param"))
		{
			return 0;
		}
		return json.get("init_param").getVector(null).Count;
	}

	public float GetInitParamValue(int n)
	{
		return json.get("init_param").get(n).get("val")
			.toFloat();
	}

	public string GetInitParamID(int n)
	{
		return json.get("init_param").get(n).get("id")
			.toString();
	}

	public int GetInitPartsVisibleNum()
	{
		if (!json.getMap(null).ContainsKey("init_parts_visible"))
		{
			return 0;
		}
		return json.get("init_parts_visible").getVector(null).Count;
	}

	public float GetInitPartsVisibleValue(int n)
	{
		return json.get("init_parts_visible").get(n).get("val")
			.toFloat();
	}

	public string GetInitPartsVisibleID(int n)
	{
		return json.get("init_parts_visible").get(n).get("id")
			.toString();
	}

	public int GetExpressionNum()
	{
		if (!json.getMap(null).ContainsKey("expressions"))
		{
			return 0;
		}
		return json.get("expressions").getVector(null).Count;
	}

	public string GetExpressionFile(int n)
	{
		return json.get("expressions").get(n).get("file")
			.toString();
	}

	public string GetExpressionName(int n)
	{
		return json.get("expressions").get(n).get("name")
			.toString();
	}

	public string GetExpressionExpandParam(int n, string expand)
	{
		return json.get("expressions").get(n).get(expand)
			.toString();
	}

	public bool IsExpressionExpandParam(int n, string expand)
	{
		string expressionExpandParam = GetExpressionExpandParam(n, expand);
		if (expressionExpandParam == null)
		{
			return false;
		}
		int result = 0;
		if (!int.TryParse(expressionExpandParam, out result))
		{
			return false;
		}
		if (result == 0)
		{
			return false;
		}
		return true;
	}

	public string[] GetTextureFiles()
	{
		string[] array = new string[GetTextureNum()];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = GetTextureFile(i);
		}
		return array;
	}

	public string[] GetExpressionFiles()
	{
		string[] array = new string[GetExpressionNum()];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = GetExpressionFile(i);
		}
		return array;
	}

	public string[] GetExpressionNames()
	{
		string[] array = new string[GetExpressionNum()];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = GetExpressionName(i);
		}
		return array;
	}
}
