using System.Collections.Generic;

public class AssetBundleList
{
	public Dictionary<string, AssetBundleInfo> mDict { get; set; }

	public AssetBundleList()
	{
		mDict = new Dictionary<string, AssetBundleInfo>();
	}

	public void AddABInfoData(AssetBundleInfo aData)
	{
		mDict.Add(aData.AssetBundleName, aData);
	}

	public AssetBundleInfo GetAssetBundleInfo(string a_bundleName)
	{
		AssetBundleInfo result = null;
		if (mDict.ContainsKey(a_bundleName))
		{
			result = mDict[a_bundleName];
		}
		return result;
	}

	public List<string> GetResouecesFromAssetBundle(string a_bundleName)
	{
		AssetBundleInfo value = null;
		if (mDict.TryGetValue(a_bundleName, out value))
		{
			return value.AssetList;
		}
		return null;
	}

	public List<string> GetAssetBundleListFromScene(string a_scene)
	{
		List<string> list = new List<string>();
		if (mDict == null)
		{
			return list;
		}
		List<AssetBundleInfo> list2 = new List<AssetBundleInfo>(mDict.Values);
		for (int i = 0; i < list2.Count; i++)
		{
			AssetBundleInfo assetBundleInfo = list2[i];
			if (assetBundleInfo.HasSceneName(a_scene))
			{
				string assetBundleName = assetBundleInfo.AssetBundleName;
				if (!list.Contains(assetBundleName))
				{
					list.Add(assetBundleName);
				}
			}
		}
		return list;
	}
}
