using System;

public class PlayerStageData : SMJsonData, ICloneable
{
	public int Version { get; set; }

	public Def.SERIES Series { get; set; }

	public int StageNo { get; set; }

	public int WinCount { get; set; }

	public int LoseCount { get; set; }

	public float WinRate
	{
		get
		{
			if (WinCount + LoseCount < 1)
			{
				return 0f;
			}
			double num = (double)WinCount / (double)(WinCount + LoseCount);
			return Convert.ToSingle(num * 100.0);
		}
	}

	public PlayerStageData()
	{
		Version = 1290;
		Series = Def.SERIES.SM_FIRST;
		StageNo = 0;
		WinCount = 0;
		LoseCount = 0;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override bool Equals(object obj)
	{
		PlayerStageData playerStageData = obj as PlayerStageData;
		if (playerStageData == null)
		{
			return false;
		}
		if (playerStageData.Series != Series)
		{
			return false;
		}
		if (playerStageData.WinCount == WinCount && playerStageData.LoseCount == LoseCount)
		{
			return true;
		}
		return false;
	}

	public PlayerStageData Clone()
	{
		return MemberwiseClone() as PlayerStageData;
	}

	public void Update(bool a_isWon)
	{
		if (a_isWon)
		{
			WinCount++;
		}
		else
		{
			LoseCount++;
		}
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(Version);
		data.WriteByte((byte)Series);
		data.WriteInt(StageNo);
		data.WriteShort((short)WinCount);
		data.WriteShort((short)LoseCount);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		Version = data.ReadInt();
		Series = (Def.SERIES)data.ReadByte();
		StageNo = data.ReadInt();
		WinCount = data.ReadShort();
		LoseCount = data.ReadShort();
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerStageData obj = new PlayerStageData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}
}
