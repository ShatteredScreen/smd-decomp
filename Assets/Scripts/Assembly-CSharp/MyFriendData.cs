using System;
using System.Collections.Generic;

public class MyFriendData : SMJsonData, ICloneable
{
	private string mName;

	public int Version { get; set; }

	[MiniJSONAlias("uuid")]
	public int UUID { get; set; }

	[MiniJSONAlias("nickname")]
	public string Name
	{
		get
		{
			return mName;
		}
		set
		{
			mName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	[MiniJSONAlias("hiveid")]
	public string HiveId { get; set; }

	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int Level { get; set; }

	[MiniJSONAlias("xp")]
	public int Experience { get; set; }

	[MiniJSONAlias("fbid")]
	public string Fbid { get; set; }

	public string FbName { get; set; }

	[MiniJSONAlias("gcid")]
	public string Gcid { get; set; }

	public string GcName { get; set; }

	[MiniJSONAlias("gpid")]
	public string Gpid { get; set; }

	public string GpName { get; set; }

	[MiniJSONAlias("updatetime")]
	public int UpdateTimeEpoch { get; set; }

	public DateTime IconModified { get; set; }

	public DateTime LastHeartSendTime { get; set; }

	public DateTime LastRoadBlockRequestTime { get; set; }

	public DateTime LastFriendHelpRequestTime { get; set; }

	[MiniJSONAlias("total_score")]
	public long TotalScore { get; set; }

	[MiniJSONAlias("total_star")]
	public long TotalStars { get; set; }

	[MiniJSONAlias("total_trophy")]
	public long TotalTrophy { get; set; }

	[MiniJSONAlias("iconid")]
	public int IconID { get; set; }

	[MiniJSONAlias("iconlvl")]
	public int IconLevel { get; set; }

	[MiniJSONAlias("play_stage")]
	public List<PlayStageParam> PlayStage { get; set; }

	public MyFriendData()
	{
		IconID = 0;
		PlayStage = new List<PlayStageParam>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(Version);
		data.WriteInt(UUID);
		data.WriteUTF(Name);
		data.WriteUTF(HiveId);
		data.WriteInt(Series);
		data.WriteInt(Level);
		data.WriteInt(Experience);
		data.WriteInt(IconID);
		data.WriteUTF(Fbid);
		data.WriteUTF(FbName);
		data.WriteUTF(Gcid);
		data.WriteUTF(GcName);
		data.WriteUTF(Gpid);
		data.WriteUTF(GpName);
		data.WriteInt(UpdateTimeEpoch);
		data.WriteDateTime(IconModified);
		data.WriteDateTime(LastHeartSendTime);
		data.WriteDateTime(LastRoadBlockRequestTime);
		data.WriteDateTime(LastFriendHelpRequestTime);
		data.WriteLong(TotalScore);
		data.WriteLong(TotalStars);
		data.WriteLong(TotalTrophy);
		data.WriteInt(IconID);
		data.WriteInt(IconLevel);
		data.WriteInt(PlayStage.Count);
		for (int i = 0; i < PlayStage.Count; i++)
		{
			data.WriteShort((short)PlayStage[i].Series);
			data.WriteInt(PlayStage[i].Level);
		}
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		Version = data.ReadInt();
		UUID = data.ReadInt();
		Name = data.ReadUTF();
		HiveId = data.ReadUTF();
		Series = data.ReadInt();
		Level = data.ReadInt();
		Experience = data.ReadInt();
		IconID = data.ReadInt();
		Fbid = data.ReadUTF();
		FbName = data.ReadUTF();
		Gcid = data.ReadUTF();
		GcName = data.ReadUTF();
		Gpid = data.ReadUTF();
		GpName = data.ReadUTF();
		UpdateTimeEpoch = data.ReadInt();
		IconModified = data.ReadDateTime();
		LastHeartSendTime = data.ReadDateTime();
		LastRoadBlockRequestTime = data.ReadDateTime();
		LastFriendHelpRequestTime = data.ReadDateTime();
		TotalScore = data.ReadLong();
		TotalStars = data.ReadLong();
		TotalTrophy = data.ReadLong();
		IconID = data.ReadInt();
		IconLevel = data.ReadInt();
		if (reqVersion >= 1020)
		{
			LastHeartSendTime = DateTimeUtil.UnitLocalTimeEpoch;
			LastRoadBlockRequestTime = DateTimeUtil.UnitLocalTimeEpoch;
			LastFriendHelpRequestTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		if (reqVersion >= 1040)
		{
			PlayStage = new List<PlayStageParam>();
			int num = data.ReadInt();
			for (int i = 0; i < num; i++)
			{
				int series = data.ReadShort();
				int level = data.ReadInt();
				PlayStageParam playStageParam = new PlayStageParam();
				playStageParam.Series = series;
				playStageParam.Level = level;
				PlayStage.Add(playStageParam);
			}
		}
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			MyFriendData obj = new MyFriendData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public int GetPlayableLevel(Def.SERIES a_series)
	{
		for (int i = 0; i < PlayStage.Count; i++)
		{
			PlayStageParam playStageParam = PlayStage[i];
			if (playStageParam.Series == (int)a_series)
			{
				return playStageParam.Level;
			}
		}
		if (a_series == Def.SERIES.SM_FIRST && a_series == (Def.SERIES)Series)
		{
			return Level;
		}
		return 100;
	}

	public MyFriendData Clone()
	{
		return MemberwiseClone() as MyFriendData;
	}
}
