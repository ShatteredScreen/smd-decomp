public class LinkBannerDetail
{
	[MiniJSONAlias("file")]
	public string FileName { get; set; }

	[MiniJSONAlias("action")]
	public string Action { get; set; }

	[MiniJSONAlias("param")]
	public int Param { get; set; }

	public LinkBannerDetail()
	{
		FileName = string.Empty;
		Action = string.Empty;
		Param = 0;
	}
}
