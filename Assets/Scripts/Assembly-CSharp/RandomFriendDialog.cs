using System;
using System.Collections.Generic;
using UnityEngine;

public class RandomFriendDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		NETWORK_00 = 3,
		NETWORK_00_1 = 4,
		NETWORK_00_2 = 5,
		NETWORK_01_1 = 6,
		NETWORK_01_2 = 7
	}

	private class _GetRandomUserData
	{
		public List<SearchUserData> results { get; set; }
	}

	public delegate void OnDialogClosed(bool empty, bool facebookLogin);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private bool mIsStage;

	private int mSeries;

	private int mStageNo;

	private bool mStageRandomUserSucceed;

	private bool mStageRandomUserCheckDone;

	private bool mApplyUserSucceed;

	private bool mApplyUserCheckDone;

	private SMVerticalListInfo mRandomUserListInfo;

	private SMVerticalList mRandomUserList;

	private List<SMVerticalListItem> mList = new List<SMVerticalListItem>();

	private List<RandomIconCheckData> mRandomIconCheckData = new List<RandomIconCheckData>();

	private SearchUserData mSelectedData;

	private GameObject mSelectedDataGameObject;

	private STATE mFirstState = STATE.NETWORK_00;

	private ConfirmDialog mConfirmDialog;

	private NickNameDialog mNickNameDialog;

	public bool mFaceBookLogin;

	private OnDialogClosed mCallback;

	private FBUserManager mFBUserManager;

	private UILabel mRemainLabel;

	private DateTime mNow;

	public override void Start()
	{
		mFBUserManager = SingletonMonoBehaviour<GameMain>.Instance.mFbUserMngRandom;
		mFBUserManager.SetPriority(true);
		base.Start();
		Server.GiveApplyEvent += Server_OnGiveApply;
		mNow = DateTime.Now;
	}

	public override void OnDestroy()
	{
		mFBUserManager.SetPriority(false);
		base.OnDestroy();
	}

	public void Init()
	{
		mIsStage = false;
		if (!mIsStage)
		{
			mFirstState = STATE.MAIN;
		}
	}

	public void Init(int a_series, int a_stageno)
	{
		mIsStage = true;
		mSeries = a_series;
		mStageNo = a_stageno;
		Server.GetRandomUserEvent += Server_OnGetRandomUser;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mState.Change(mFirstState);
			break;
		case STATE.MAIN:
		{
			if (mRandomIconCheckData.Count <= 0)
			{
				break;
			}
			BIJImage image = ResourceManager.LoadImage("ICON").Image;
			int num = mBaseDepth + 5;
			for (int num2 = mRandomIconCheckData.Count - 1; num2 >= 0; num2--)
			{
				RandomIconCheckData randomIconCheckData = mRandomIconCheckData[num2];
				FBUserData.STATE iconStatus = mFBUserManager.GetIconStatus(randomIconCheckData.uuid);
				if (iconStatus == FBUserData.STATE.SUCCESS || iconStatus == FBUserData.STATE.FAILURE)
				{
					if (randomIconCheckData.loadingIcon != null)
					{
						UnityEngine.Object.Destroy(randomIconCheckData.loadingIcon);
					}
					Texture2D icon = mFBUserManager.GetIcon(randomIconCheckData.uuid);
					if (icon != null)
					{
						UITexture uITexture = Util.CreateGameObject("Icon", randomIconCheckData.boardImage.gameObject).AddComponent<UITexture>();
						uITexture.depth = num + 2;
						uITexture.transform.localPosition = new Vector3(-166f, -8f, 0f);
						uITexture.mainTexture = icon;
						uITexture.SetDimensions(65, 65);
						int num3 = randomIconCheckData.iconid - 10000;
						if (num3 >= mGame.mCompanionData.Count)
						{
							num3 = 0;
						}
						string iconName = mGame.mCompanionData[num3].iconName;
						UISprite uISprite = Util.CreateSprite("CharaIcon", uITexture.gameObject, image);
						Util.SetSpriteInfo(uISprite, iconName, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
						uISprite.SetDimensions(45, 45);
					}
					else
					{
						string imageName = "icon_chara_facebook";
						UISprite uISprite2 = Util.CreateSprite("Icon", randomIconCheckData.boardImage.gameObject, image);
						Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-166f, -8f, 0f), Vector3.one, false);
						uISprite2.SetDimensions(65, 65);
						int num4 = randomIconCheckData.iconid - 10000;
						if (num4 > mGame.mCompanionData.Count)
						{
							num4 = 0;
						}
						string iconName2 = mGame.mCompanionData[num4].iconName;
						UISprite uISprite3 = Util.CreateSprite("CharaIcon", uISprite2.gameObject, image);
						Util.SetSpriteInfo(uISprite3, iconName2, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
						uISprite3.SetDimensions(45, 45);
					}
					mRandomIconCheckData.RemoveAt(num2);
				}
			}
			break;
		}
		case STATE.NETWORK_00:
		{
			if (mGame.RandomUserList.Count > 3)
			{
				mState.Change(STATE.NETWORK_00_2);
				break;
			}
			STATE aStatus = Network_GetRandomUser();
			mState.Change(aStatus);
			break;
		}
		case STATE.NETWORK_00_1:
			if (mStageRandomUserCheckDone)
			{
				mState.Change(STATE.NETWORK_00_2);
			}
			break;
		case STATE.NETWORK_00_2:
			BuildDialogList(mGame.RandomUserList);
			mState.Change(STATE.MAIN);
			break;
		case STATE.NETWORK_01_1:
			if (mApplyUserCheckDone)
			{
				mState.Change(STATE.NETWORK_01_2);
			}
			break;
		case STATE.NETWORK_01_2:
			mState.Change(STATE.MAIN);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("RandomFriend_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		long num = DateTimeUtil.BetweenDays0(mGame.mPlayer.Data.LastApplyRandomUserTime, DateTime.Now);
		if (num > 0)
		{
			mGame.mPlayer.Data.ApplyRandomUserNum = 0;
		}
		if (!mIsStage)
		{
			BuildDialogList(mGame.RandomUserList);
		}
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpenFinished()
	{
		string searchID = mGame.mPlayer.Data.SearchID;
		string nickName = mGame.mPlayer.Data.NickName;
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE2) && searchID == nickName)
		{
			mNickNameDialog = Util.CreateGameObject("NickNameDialog", base.ParentGameObject).AddComponent<NickNameDialog>();
			mNickNameDialog.Init(true);
			mNickNameDialog.SetClosedCallback(OnLogined);
			mNickNameDialog.SetBaseDepth(mBaseDepth + 70);
			mState.Change(STATE.WAIT);
		}
		else
		{
			mGame.mPlayer.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE2.ToString());
		}
		float posX = 0f;
		float num = 512f;
		mRandomUserListInfo = new SMVerticalListInfo(1, num, 430f, 1, num, 430f, 1, 140);
		mRandomUserList = SMVerticalList.Make(base.gameObject, this, mRandomUserListInfo, mList, posX, 15f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mRandomUserList.OnCreateItem = OnCreateItem;
	}

	public void OnLogined(bool FacebookLogin)
	{
		mGame.mPlayer.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE2.ToString());
		mState.Change(STATE.MAIN);
		if (FacebookLogin)
		{
			mFaceBookLogin = true;
		}
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mRandomUserList == null || mRandomUserList.items.Count == 0, mFaceBookLogin);
		}
		if (mIsStage)
		{
			Server.GetRandomUserEvent -= Server_OnGetRandomUser;
		}
		Server.GiveApplyEvent -= Server_OnGiveApply;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void BuildDialogList(List<SearchUserData> a_list)
	{
		if (a_list.Count == 0)
		{
			BuildDialogNoApplyError();
			return;
		}
		mList.Clear();
		for (int i = 0; i < a_list.Count; i++)
		{
			SearchUserData searchUserData = a_list[i];
			if (searchUserData.uuid != mGame.mPlayer.UUID)
			{
				if (!mGame.mPlayer.Friends.ContainsKey(searchUserData.uuid) && !mGame.mPlayer.ApplyFriends.ContainsKey(searchUserData.uuid))
				{
					mList.Add(searchUserData);
				}
				if (mList.Count >= GameMain.DISPLAY_RANDOM_USER_NUM)
				{
					break;
				}
			}
		}
		if (mList.Count == 0)
		{
			BuildDialogNoApplyError();
			return;
		}
		UIFont atlasFont = GameMain.LoadFont();
		UILabel uILabel = Util.CreateLabel("RemainDesc", base.gameObject, atlasFont);
		string text = string.Format(Localization.Get("Friend_Request_CloseNotice"), GameMain.APPLY_RANDOM_USER_MAX, GameMain.APPLY_RANDOM_USER_MAX);
		text += string.Format(Localization.Get("Friend_Request_CloseCount"), mGame.mPlayer.Data.ApplyRandomUserNum, GameMain.APPLY_RANDOM_USER_MAX);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 1, new Vector3(0f, -250f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.fontSize = 24;
		mRemainLabel = uILabel;
	}

	private void BuildDialogError()
	{
		if (mConfirmDialog != null)
		{
			UnityEngine.Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01");
		mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(delegate
		{
			mState.Reset(STATE.MAIN);
		});
		mState.Reset(STATE.WAIT, true);
	}

	private void BuildDialogNoApplyError()
	{
		if (mConfirmDialog != null)
		{
			UnityEngine.Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("ID_Search_NotFound_Title"), Localization.Get("ID_Search_NotFound"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(delegate
		{
			mState.Reset(STATE.MAIN);
		});
		mState.Reset(STATE.WAIT, true);
	}

	private void BuildDialogApplyError()
	{
		if (mConfirmDialog != null)
		{
			UnityEngine.Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Friend_Request_Result_NG"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(delegate
		{
			mState.Reset(STATE.MAIN);
		});
		mState.Reset(STATE.WAIT, true);
	}

	private void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		SearchUserData searchUserData = item as SearchUserData;
		string imageName = "icon_chara_facebook";
		string text = "lv_1";
		int num2 = 0;
		num2 = ((searchUserData.iconid >= 10000) ? (searchUserData.iconid - 10000) : searchUserData.iconid);
		if (num2 >= 0)
		{
			if (num2 >= mGame.mCompanionData.Count)
			{
				num2 = 0;
			}
			CompanionData companionData = mGame.mCompanionData[num2];
			imageName = companionData.iconName;
		}
		int iconlvl = searchUserData.iconlvl;
		text = "lv_" + iconlvl;
		int a_main;
		int a_sub;
		Def.SplitStageNo(searchUserData.lvl, out a_main, out a_sub);
		string text2 = string.Format(Localization.Get("Stage_Data"), a_main);
		string nickname = searchUserData.nickname;
		float cellWidth = mRandomUserList.mList.cellWidth;
		float cellHeight = mRandomUserList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", num, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		UISprite uISprite2 = Util.CreateSprite("Instruction", itemGO.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel2", num, new Vector3(0f, 10f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(430, 128);
		UISprite uISprite7;
		if (GameMain.IsFacebookIconEnable() && searchUserData.iconid > 9999)
		{
			Texture2D icon = mFBUserManager.GetIcon(searchUserData.uuid);
			switch (mFBUserManager.GetIconStatus(searchUserData.uuid))
			{
			case FBUserData.STATE.DEFAULT:
			case FBUserData.STATE.FAILURE:
			{
				string imageName2 = "icon_chara_facebook";
				UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite4, imageName2, num + 2, new Vector3(-163f, -8f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(65, 65);
				int index4 = searchUserData.iconid - 10000;
				if (num2 >= mGame.mCompanionData.Count)
				{
					num2 = 0;
				}
				string iconName3 = mGame.mCompanionData[index4].iconName;
				UISprite uISprite6 = Util.CreateSprite("CharaIcon", uISprite4.gameObject, image2);
				Util.SetSpriteInfo(uISprite6, iconName3, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
				uISprite6.SetDimensions(45, 45);
				break;
			}
			case FBUserData.STATE.LOADING:
			{
				string imageName2 = "icon_chara_facebook02";
				UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite4, imageName2, num + 2, new Vector3(-163f, -8f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(84, 84);
				RandomIconCheckData randomIconCheckData = new RandomIconCheckData();
				randomIconCheckData.boardImage = uISprite2;
				randomIconCheckData.loadingIcon = uISprite4;
				randomIconCheckData.uuid = searchUserData.uuid;
				randomIconCheckData.iconid = searchUserData.iconid;
				mRandomIconCheckData.Add(randomIconCheckData);
				break;
			}
			case FBUserData.STATE.SUCCESS:
				icon = mFBUserManager.GetIcon(searchUserData.uuid);
				if (icon != null)
				{
					UITexture uITexture = Util.CreateGameObject("Icon", uISprite2.gameObject).AddComponent<UITexture>();
					uITexture.depth = num + 2;
					uITexture.transform.localPosition = new Vector3(-163f, -8f, 0f);
					uITexture.mainTexture = icon;
					uITexture.SetDimensions(65, 65);
					int index2 = searchUserData.iconid - 10000;
					if (num2 >= mGame.mCompanionData.Count)
					{
						num2 = 0;
					}
					string iconName = mGame.mCompanionData[index2].iconName;
					UISprite uISprite3 = Util.CreateSprite("CharaIcon", uITexture.gameObject, image2);
					Util.SetSpriteInfo(uISprite3, iconName, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
					uISprite3.SetDimensions(45, 45);
				}
				else
				{
					string imageName2 = "icon_chara_facebook";
					UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
					Util.SetSpriteInfo(uISprite4, imageName2, num + 2, new Vector3(-163f, -8f, 0f), Vector3.one, false);
					uISprite4.SetDimensions(65, 65);
					int index3 = searchUserData.iconid - 10000;
					if (num2 >= mGame.mCompanionData.Count)
					{
						num2 = 0;
					}
					string iconName2 = mGame.mCompanionData[index3].iconName;
					UISprite uISprite5 = Util.CreateSprite("CharaIcon", uISprite4.gameObject, image2);
					Util.SetSpriteInfo(uISprite5, iconName2, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
					uISprite5.SetDimensions(45, 45);
				}
				break;
			}
		}
		else
		{
			uISprite7 = Util.CreateSprite("FriendIcon", uISprite2.gameObject, image2);
			Util.SetSpriteInfo(uISprite7, imageName, num + 1, new Vector3(-154f, 3f, 0f), Vector3.one, false);
			uISprite7.SetDimensions(80, 80);
			UISprite sprite = Util.CreateSprite("CharaLv", uISprite2.gameObject, image);
			Util.SetSpriteInfo(sprite, text, num + 2, new Vector3(-154f, -37f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(searchUserData.fbid))
			{
				string imageName3 = "icon_sns00_mini";
				UISprite sprite2 = Util.CreateSprite("FacebookIcon", uISprite7.gameObject, image2);
				Util.SetSpriteInfo(sprite2, imageName3, num + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
			}
		}
		uISprite7 = Util.CreateSprite("Line00", uISprite2.gameObject, image);
		Util.SetSpriteInfo(uISprite7, "line00", num + 1, new Vector3(53f, 17f, 0f), Vector3.one, false);
		uISprite7.type = UIBasicSprite.Type.Sliced;
		uISprite7.SetDimensions(292, 6);
		UILabel uILabel = Util.CreateLabel("LastLogin", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("LastVisit"), num + 2, new Vector3(-104f, 3f, 0f), 16, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		DateTime fdt = DateTimeUtil.UnixTimeStampToDateTime(searchUserData.UpdateTimeEpoch, true);
		long num3 = DateTimeUtil.BetweenDays0(fdt, mNow);
		string text3 = ((num3 <= 0) ? Localization.Get("LastVisitData02") : ((num3 < 30) ? string.Format(Localization.Get("LastVisitData00"), num3) : string.Format(Localization.Get("LastVisitData01"), 30)));
		UILabel uILabel2 = Util.CreateLabel("LastLoginDays", uISprite2.gameObject, atlasFont);
		float x = uILabel.transform.localPosition.x + (float)uILabel.width;
		float y = uILabel.transform.localPosition.y;
		Util.SetLabelInfo(uILabel2, text3, num + 2, new Vector3(x, y, 0f), 16, 0, 0, UIWidget.Pivot.Left);
		uILabel2.color = new Color(1f, 0.1f, 0.1f);
		uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel2.SetDimensions(200, 16);
		int num4 = searchUserData.series;
		int stageNumber = 1;
		List<PlayStageParam> playStage = searchUserData.PlayStage;
		if (playStage != null && playStage.Count != 0)
		{
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == num4)
				{
					stageNumber = playStage[i].Level / 100;
					break;
				}
			}
		}
		else
		{
			num4 = 0;
			Def.SplitStageNo(searchUserData.lvl, out a_main, out a_sub);
			stageNumber = a_main;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(uISprite2.gameObject, num, num4, stageNumber, -17f, -18f);
		uILabel = Util.CreateLabel("UserName", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, nickname, num + 1, new Vector3(53f, 34f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(297, 26);
		UIButton button = Util.CreateJellyImageButton("ApplyButton", itemGO, image);
		Util.SetImageButtonInfo(button, "LC_button_application2", "LC_button_application2", "LC_button_application2", num + 1, new Vector3(137f, -20f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnFriendPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void OnFriendPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mGame.mPlayer.Friends.Count >= Constants.GAME_FRIEND_MAX)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mConfirmDialog = Util.CreateGameObject("LimitFriendDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Approval_Friend_Title"), Localization.Get("Friend_Request_Result_Mine_Full"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Change(STATE.WAIT);
			return;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		if (mGame.mPlayer.Data.ApplyRandomUserNum >= GameMain.APPLY_RANDOM_USER_MAX)
		{
			mConfirmDialog = Util.CreateGameObject("LimitApplyDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Friend_Request_Close_Title"), Localization.Get("Friend_Request_Close_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Change(STATE.WAIT);
			return;
		}
		SearchUserData searchUserData = mRandomUserList.FindListItem(go) as SearchUserData;
		if (searchUserData == null)
		{
			BuildDialogApplyError();
			return;
		}
		mSelectedData = searchUserData;
		mSelectedDataGameObject = go;
		Network_GiveApply(searchUserData.uuid);
	}

	private void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
	}

	private void Network_GiveApply(int a_target)
	{
		mApplyUserCheckDone = false;
		mApplyUserSucceed = false;
		GiftItemData giftItemData = new GiftItemData();
		giftItemData.FromID = mGame.mPlayer.UUID;
		giftItemData.Message = mGame.mPlayer.NickName;
		giftItemData.ItemCategory = 16;
		giftItemData.ItemKind = 0;
		giftItemData.ItemID = 0;
		giftItemData.Quantity = 1;
		giftItemData.PlayStage = mGame.mPlayer.PlayStage;
		if (!Server.GiveApply(giftItemData, a_target))
		{
			mApplyUserCheckDone = true;
			BuildDialogApplyError();
		}
		else
		{
			mState.Change(STATE.NETWORK_01_1);
		}
	}

	private void Server_OnGiveApply(int a_result, int a_code)
	{
		if (a_result == 0)
		{
			if (mSelectedData != null)
			{
				MyFriend myFriend = new MyFriend();
				myFriend.Data.UUID = mSelectedData.uuid;
				myFriend.Data.Name = mSelectedData.nickname;
				myFriend.Data.Level = mSelectedData.lvl;
				myFriend.Data.IconID = mSelectedData.iconid;
				myFriend.Data.IconLevel = mSelectedData.iconlvl;
				myFriend.Data.Series = mSelectedData.series;
				if (!mGame.mPlayer.ApplyFriends.ContainsKey(myFriend.Data.UUID))
				{
					mGame.mPlayer.ApplyFriends.Add(myFriend.Data.UUID, myFriend);
				}
				mGame.mPlayer.Data.LastApplyRandomUserTime = DateTime.Now;
				mGame.mPlayer.Data.ApplyRandomUserNum++;
				string text = string.Format(Localization.Get("Friend_Request_CloseNotice"), GameMain.APPLY_RANDOM_USER_MAX, GameMain.APPLY_RANDOM_USER_MAX);
				text += string.Format(Localization.Get("Friend_Request_CloseCount"), mGame.mPlayer.Data.ApplyRandomUserNum, GameMain.APPLY_RANDOM_USER_MAX);
				mRemainLabel.text = text;
				int index = mRandomUserList.FindListIndexOf(mSelectedDataGameObject);
				mRandomUserList.RemoveListItem(index);
				mRandomUserList.OnScreenChanged(true);
				mGame.Save();
			}
			else
			{
				BuildDialogApplyError();
			}
			mSelectedData = null;
			mSelectedDataGameObject = null;
			mApplyUserSucceed = true;
			mState.Reset(STATE.NETWORK_01_2, true);
		}
		else
		{
			mApplyUserSucceed = false;
			BuildDialogApplyError();
		}
		mApplyUserCheckDone = true;
	}

	private STATE Network_GetRandomUser()
	{
		STATE result = STATE.NETWORK_00_1;
		long num = DateTimeUtil.BetweenMinutes(mGame.LastRandomUserGetTime, DateTime.Now);
		long gET_RANDOMUSER_FREQ = GameMain.GET_RANDOMUSER_FREQ;
		if (gET_RANDOMUSER_FREQ > 0 && num < gET_RANDOMUSER_FREQ)
		{
			mStageRandomUserCheckDone = true;
			mStageRandomUserSucceed = true;
			return STATE.NETWORK_00_2;
		}
		mStageRandomUserCheckDone = false;
		mStageRandomUserSucceed = false;
		if (mGame.RandomUserList == null)
		{
			mGame.RandomUserList = new List<SearchUserData>();
		}
		mGame.RandomUserList.Clear();
		int a_series = mSeries;
		int a_stage = mStageNo;
		int a_range = 500;
		int rANDOM_USER_MAX = GameMain.RANDOM_USER_MAX;
		mGame.LastRandomUserGetTime = DateTime.Now;
		if (!Server.GetRandomUser(a_series, a_stage, a_range, rANDOM_USER_MAX))
		{
			mGame.LastRandomUserGetTime = DateTimeUtil.UnitLocalTimeEpoch;
			mStageRandomUserCheckDone = true;
			return STATE.NETWORK_00_2;
		}
		return result;
	}

	private void Server_OnGetRandomUser(int result, int code, string json)
	{
		if (result == 0)
		{
			try
			{
				_GetRandomUserData obj = new _GetRandomUserData();
				new MiniJSONSerializer().Populate(ref obj, json);
				if (obj != null && obj.results != null && obj.results.Count > 0)
				{
					List<SearchUserData> strayUserList = mGame.mPlayer.GetStrayUserList(obj.results);
					foreach (SearchUserData item in strayUserList)
					{
						mGame.RandomUserList.Add(item.Clone());
					}
				}
				mStageRandomUserSucceed = true;
			}
			catch (Exception)
			{
				mGame.RandomUserList.Clear();
			}
		}
		mStageRandomUserCheckDone = true;
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
