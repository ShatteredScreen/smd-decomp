using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using live2d.framework;

public class ResourceManager : SingletonMonoBehaviour<ResourceManager>
{
	private class AssetBundleRef
	{
		public enum LOADSTATE
		{
			INIT = 0,
			LOADING = 1,
			DONE = 2
		}

		public AssetBundle AssetBundleInstance;

		public string BundleName;

		public LOADSTATE LoadState;

		public int LoadingCount;

		public AssetBundleRef(string a_bundleName)
		{
			BundleName = a_bundleName;
			LoadState = LOADSTATE.INIT;
		}
	}

	private class AssetBundleRequest
	{
		public string mAssetBundleName;

		public AssetBundleRequest(string a_name)
		{
			mAssetBundleName = a_name;
		}
	}

	public delegate IEnumerator LoadBinaryHandler(BIJBinaryReader stream);

	private static Dictionary<string, ResImage> ImageDict = new Dictionary<string, ResImage>();

	private static Dictionary<string, ResSsAnimation> SsAnimationDict = new Dictionary<string, ResSsAnimation>();

	private static Dictionary<string, ResLive2DAnimation> Live2DAnimationDict = new Dictionary<string, ResLive2DAnimation>();

	private static Dictionary<string, ResSound> SoundDict = new Dictionary<string, ResSound>();

	private static Dictionary<string, ResScriptableObject> ScriptableObjectDict = new Dictionary<string, ResScriptableObject>();

	private static List<string> Live2DAnimationProtectList = new List<string>();

	private static Dictionary<string, AssetBundleRef> mAssetBundleDict = new Dictionary<string, AssetBundleRef>();

	private static Dictionary<string, AssetBundleRequest> mAssetBundleRequestDict = new Dictionary<string, AssetBundleRequest>();

	public string AssetBundleError = string.Empty;

	public static bool mABLoadError = false;

	public static Color mDebugButtonColor = Color.white;

	public static AssetBundleList ABInfoList = new AssetBundleList();

	public static Dictionary<string, Shader> ssAnimeMatShaderLink = new Dictionary<string, Shader>();

	public static void ResMngLog(string str)
	{
	}

	public static void UnloadResource(ResourceInstance a_ri, string key)
	{
		switch (a_ri.ResourceType)
		{
		case ResourceInstance.TYPE.IMAGE:
			UnloadImage(key);
			break;
		case ResourceInstance.TYPE.L2D_ANIMATION:
			UnloadLive2DAnimation(key);
			break;
		case ResourceInstance.TYPE.SOUND:
			UnloadSound(key);
			break;
		case ResourceInstance.TYPE.SS_ANIMATION:
			UnloadSsAnimation(key);
			break;
		case ResourceInstance.TYPE.TEXTURE:
			break;
		case ResourceInstance.TYPE.SCRIPTABLE_OBJECT:
			UnloadScriptableObject(key);
			break;
		}
	}

	public static void UnloadResourceByAssetBundleName(string assetBundleName)
	{
		List<string> resouecesFromAssetBundle = ABInfoList.GetResouecesFromAssetBundle(assetBundleName);
		if (resouecesFromAssetBundle == null)
		{
			return;
		}
		string empty = string.Empty;
		List<string> list = new List<string>();
		for (int i = 0; i < resouecesFromAssetBundle.Count; i++)
		{
			empty = resouecesFromAssetBundle[i];
			if (empty.Contains(".prefab"))
			{
				list.Add(empty);
			}
			else if (empty.Contains("ssa.asset"))
			{
				string fileName = empty.Substring(0, empty.LastIndexOf(".asset"));
				ResSsAnimation resSsAnimation = GetResSsAnimation(fileName);
				if (resSsAnimation != null && resSsAnimation.LoadState == ResourceInstance.LOADSTATE.DONE)
				{
					UnloadSsAnimation(resSsAnimation);
				}
			}
			else if (empty.Contains(".mp3"))
			{
				string fileName2 = empty.Substring(0, empty.LastIndexOf(".mp3"));
				ResSound resSound = GetResSound(fileName2);
				if (resSound != null && resSound.LoadState == ResourceInstance.LOADSTATE.DONE)
				{
					UnloadSound(resSound);
				}
			}
			else if (empty.Contains("model.json.bytes"))
			{
				string fileName3 = empty.Substring(0, empty.LastIndexOf(".bytes"));
				ResLive2DAnimation resLive2DAnimation = GetResLive2DAnimation(fileName3);
				if (resLive2DAnimation != null && resLive2DAnimation.LoadState == ResourceInstance.LOADSTATE.DONE)
				{
					UnloadLive2DAnimation(resLive2DAnimation);
				}
			}
		}
		for (int j = 0; j < list.Count; j++)
		{
			empty = list[j];
			if (!empty.Contains(".prefab"))
			{
				continue;
			}
			string fileName4 = empty.Substring(0, empty.LastIndexOf(".prefab"));
			ResImage resImage = GetResImage(fileName4);
			if (resImage != null)
			{
				if (resImage.LoadState == ResourceInstance.LOADSTATE.DONE)
				{
					UnloadImage(resImage);
				}
				continue;
			}
			ResScriptableObject resScriptableObject = GetResScriptableObject(fileName4);
			if (resScriptableObject != null && resScriptableObject.LoadState == ResourceInstance.LOADSTATE.DONE)
			{
				UnloadScriptableObject(resScriptableObject);
			}
		}
	}

	public static bool IsLoaded(string a_key)
	{
		Res.ImageDictInfo value;
		if (Res.ResImageDict.TryGetValue(a_key, out value))
		{
			string path = value.path;
			if (ImageDict.ContainsKey(path))
			{
				return true;
			}
		}
		Res.SsAnimDictInfo value2;
		if (Res.ResSsAnimationDict.TryGetValue(a_key, out value2))
		{
			string path = value2.path;
			if (SsAnimationDict.ContainsKey(path))
			{
				return true;
			}
		}
		Res.Live2DAnimDictInfo value3;
		if (Res.ResLive2DAnimationDict.TryGetValue(a_key, out value3))
		{
			string path = value3.path;
			if (Live2DAnimationDict.ContainsKey(path))
			{
				return true;
			}
		}
		Res.SoundDictInfo value4;
		if (Res.ResSoundDict.TryGetValue(a_key, out value4))
		{
			string path = value4.path;
			if (SoundDict.ContainsKey(path))
			{
				return true;
			}
		}
		Res.ScriptableObjectDictInfo value5;
		if (Res.ResScriptableObjectDict.TryGetValue(a_key, out value5))
		{
			string path = value5.path;
			if (ScriptableObjectDict.ContainsKey(path))
			{
				return true;
			}
		}
		return false;
	}

	public static string GetAssetBundleNameByResourceKey(string aKey)
	{
		Res.ImageDictInfo value;
		if (Res.ResImageDict.TryGetValue(aKey, out value))
		{
			return value.abName;
		}
		Res.SsAnimDictInfo value2;
		if (Res.ResSsAnimationDict.TryGetValue(aKey, out value2))
		{
			return value2.abName;
		}
		Res.Live2DAnimDictInfo value3;
		if (Res.ResLive2DAnimationDict.TryGetValue(aKey, out value3))
		{
			return value3.abName;
		}
		Res.SoundDictInfo value4;
		if (Res.ResSoundDict.TryGetValue(aKey, out value4))
		{
			return value4.abName;
		}
		Res.ScriptableObjectDictInfo value5;
		if (Res.ResScriptableObjectDict.TryGetValue(aKey, out value5))
		{
			return value5.abName;
		}
		return string.Empty;
	}

	public static void LoadAssetBundleList()
	{
		ResourceDLUtils.dbg1("LoadAssetBundleList", true);
		if (ABInfoList != null)
		{
			ABInfoList.mDict.Clear();
		}
		ResourceDownloadManager mDLManager = SingletonMonoBehaviour<GameMain>.Instance.mDLManager;
		foreach (int value in Enum.GetValues(typeof(ResourceDownloadManager.KIND)))
		{
			string assetBundleDictionaryFileName = mDLManager.GetAssetBundleDictionaryFileName((ResourceDownloadManager.KIND)value);
			if (string.IsNullOrEmpty(assetBundleDictionaryFileName))
			{
				continue;
			}
			BIJBinaryReader downloadFileReadStream = mDLManager.GetDownloadFileReadStream(assetBundleDictionaryFileName);
			if (downloadFileReadStream != null)
			{
				int num = downloadFileReadStream.ReadInt();
				for (int i = 0; i < num; i++)
				{
					AssetBundleInfo assetBundleInfo = new AssetBundleInfo();
					assetBundleInfo.Deserialize(downloadFileReadStream);
					ABInfoList.AddABInfoData(assetBundleInfo);
				}
			}
		}
	}

	public static ResImage LoadImage(string key)
	{
		Res.ImageDictInfo value;
		if (!Res.ResImageDict.TryGetValue(key, out value))
		{
			return null;
		}
		return LoadImage(value.path, value.abName, value.preLoad);
	}

	public static ResImage LoadImage(string fileName, string bundleName, bool preload)
	{
		ResImage value;
		if (!ImageDict.TryGetValue(fileName, out value))
		{
			value = new ResImage(fileName);
			ResourceDLUtils.dbg1(fileName);
			ResMngLog("LoadImage 2");
			value.Preload = preload;
			value.LoadState = ResourceInstance.LOADSTATE.LOADING;
			string a_name = fileName + ".prefab";
			if (bundleName.Length > 0 && GameMain.USE_RESOURCEDL)
			{
				ResMngLog("LoadImage 3");
				GameObject gameObject = LoadResourceFromAssetBundle<GameObject>(a_name, bundleName);
				ResMngLog("LoadImage 4A");
				if (gameObject != null)
				{
					ResMngLog("LoadImage 5A");
					value.Atlas = gameObject.GetComponent<UIAtlas>();
					value.mPrefab = gameObject;
				}
			}
			else
			{
				value.Atlas = CreateAtlasFromPrefab(fileName, value);
			}
			value.LoadState = ResourceInstance.LOADSTATE.DONE;
			ResMngLog("LoadImage 6");
			if (value.Atlas == null)
			{
				ResMngLog("LoadImage 7");
				value = null;
			}
			else
			{
				ResMngLog("LoadImage 8");
				ImageDict.Add(fileName, value);
			}
		}
		return value;
	}

	public static ResImage LoadImageAsync(string key, bool waitAllLoad = false)
	{
		Res.ImageDictInfo value;
		if (!Res.ResImageDict.TryGetValue(key, out value))
		{
			return null;
		}
		string path = value.path;
		ResImage value2;
		if (!ImageDict.TryGetValue(path, out value2))
		{
			value2 = new ResImage(path);
			value2.Preload = value.preLoad;
			ResourceASyncLoader.RequestAsyncLoad(key, value, value2, waitAllLoad);
			ImageDict.Add(path, value2);
		}
		return value2;
	}

	public static ResImage GetResImage(string fileName, bool isCreate = false)
	{
		ResImage value = null;
		if (!ImageDict.TryGetValue(fileName, out value) && isCreate)
		{
			value = new ResImage(fileName);
			foreach (Res.ImageDictInfo value2 in Res.ResImageDict.Values)
			{
				if (value2.path == fileName)
				{
					value.Preload = value2.preLoad;
					break;
				}
			}
			ImageDict.Add(fileName, value);
		}
		return value;
	}

	public static void UnloadImage(string key, bool aForce = false)
	{
		Res.ImageDictInfo value;
		if (Res.ResImageDict.TryGetValue(key, out value))
		{
			_UnloadImage(value.path, value.preLoad, aForce);
		}
	}

	public static void UnloadImage(ResImage aRes, bool aForce = false)
	{
		_UnloadImage(aRes.Name, aRes.Preload, aForce);
	}

	private static void _UnloadImage(string aFile, bool aPreload, bool aForce)
	{
		ResImage value;
		if ((!aPreload || aForce) && ImageDict.TryGetValue(aFile, out value))
		{
			ImageDict.Remove(aFile);
			if (value.mPrefab != null)
			{
				GameMain.SafeDestroy(value.mPrefab);
				value.mPrefab = null;
			}
			value.Atlas = null;
		}
	}

	public static void UnloadImageAll(bool aForceAll = false)
	{
		Dictionary<string, ResImage> dictionary = new Dictionary<string, ResImage>();
		foreach (KeyValuePair<string, ResImage> item in ImageDict)
		{
			if (!aForceAll && item.Value.Preload)
			{
				dictionary.Add(item.Key, item.Value);
				continue;
			}
			if (item.Value.mPrefab != null)
			{
				GameMain.SafeDestroy(item.Value.mPrefab);
				item.Value.mPrefab = null;
			}
			item.Value.Atlas = null;
		}
		ImageDict.Clear();
		ImageDict = dictionary;
	}

	public static bool EnableLoadImage(string key)
	{
		Res.ImageDictInfo value;
		if (!Res.ResImageDict.TryGetValue(key, out value))
		{
			return false;
		}
		if (value.abName.Length > 0 && GameMain.USE_RESOURCEDL)
		{
			ResourceDownloadManager mDLManager = SingletonMonoBehaviour<GameMain>.Instance.mDLManager;
			ResourceDownloadManager.KIND kindWithFileName = ResourceDownloadManager.getKindWithFileName(value.abName);
			if (!mDLManager.DLFilelistHashCheck(kindWithFileName))
			{
				return false;
			}
			return mDLManager.CheckDLFileIntegrity(kindWithFileName, value.abName + ".unity3d");
		}
		return true;
	}

	public static string GetAssetBundleNameByImageKey(string key)
	{
		string result = string.Empty;
		Res.ImageDictInfo value;
		if (Res.ResImageDict.TryGetValue(key, out value))
		{
			result = value.abName;
		}
		return result;
	}

	private static UIAtlas CreateAtlasFromPrefab(string fileName, ResImage res)
	{
		ResMngLog("CreateAtlasFromPrefab:start " + fileName);
		GameObject original = Resources.Load(fileName, typeof(GameObject)) as GameObject;
		UIAtlas component = (res.mPrefab = UnityEngine.Object.Instantiate(original)).GetComponent<UIAtlas>();
		ResMngLog("CreateAtlasFromPrefab:end");
		return component;
	}

	private static UIAtlas CreateAtlasFromPrefabWithInstantinate(string fileName)
	{
		GameObject original = Resources.Load(fileName, typeof(GameObject)) as GameObject;
		GameObject gameObject = UnityEngine.Object.Instantiate(original);
		gameObject.name = fileName.Replace('/', '_');
		return gameObject.GetComponent<UIAtlas>();
	}

	public static ResSsAnimation LoadSsAnimation(string key)
	{
		Res.SsAnimDictInfo value;
		if (!Res.ResSsAnimationDict.TryGetValue(key, out value))
		{
			return null;
		}
		return LoadSsAnimation(value.path, value.abName, value.preLoad);
	}

	public static ResSsAnimation LoadSsAnimation(string fileName, string bundleName, bool preload)
	{
		ResSsAnimation value;
		if (!SsAnimationDict.TryGetValue(fileName, out value))
		{
			value = new ResSsAnimation(fileName);
			value.Preload = preload;
			value.LoadState = ResourceInstance.LOADSTATE.LOADING;
			string a_name = fileName + ".asset";
			if (bundleName.Length > 0 && GameMain.USE_RESOURCEDL)
			{
				ResMngLog("LoadSsAnimation::Load from AssetBundle " + fileName);
				value.SsAnime = LoadResourceFromAssetBundle<SsAnimation>(a_name, bundleName);
				CheckSsAnimeMatShaderLink(value);
			}
			else
			{
				ResMngLog("LoadSsAnimation::Load inApp " + fileName);
				SsAnimation original = Resources.Load(fileName, typeof(SsAnimation)) as SsAnimation;
				value.SsAnime = UnityEngine.Object.Instantiate(original);
			}
			value.LoadState = ResourceInstance.LOADSTATE.DONE;
			if (value.SsAnime == null)
			{
				value = null;
			}
			else
			{
				SsAnimationDict.Add(fileName, value);
			}
		}
		return value;
	}

	public static void CheckSsAnimeMatShaderLink(ResSsAnimation _resSsAnime)
	{
		SsImageFile[] imageList = _resSsAnime.SsAnime.ImageList;
		foreach (SsImageFile ssImageFile in imageList)
		{
			if (ssImageFile == null)
			{
				continue;
			}
			Material[] materials = ssImageFile.materials;
			foreach (Material material in materials)
			{
				if (!(material == null))
				{
					Shader value;
					if (!ssAnimeMatShaderLink.TryGetValue(material.name, out value))
					{
						material.shader = Shader.Find(material.shader.name);
						ssAnimeMatShaderLink[material.name] = material.shader;
					}
					else
					{
						material.shader = value;
					}
				}
			}
		}
	}

	public static ResSsAnimation LoadSsAnimationAsync(string key, bool waitAllLoad = false)
	{
		Res.SsAnimDictInfo value;
		if (!Res.ResSsAnimationDict.TryGetValue(key, out value))
		{
			return null;
		}
		string path = value.path;
		ResSsAnimation value2;
		if (!SsAnimationDict.TryGetValue(path, out value2))
		{
			value2 = new ResSsAnimation(path);
			value2.Preload = value.preLoad;
			ResourceASyncLoader.RequestAsyncLoad(key, value, value2, waitAllLoad);
			SsAnimationDict.Add(path, value2);
		}
		return value2;
	}

	public static ResSsAnimation GetResSsAnimation(string fileName, bool isCreate = false)
	{
		ResSsAnimation value = null;
		if (!SsAnimationDict.TryGetValue(fileName, out value) && isCreate)
		{
			value = new ResSsAnimation(fileName);
			foreach (Res.SsAnimDictInfo value2 in Res.ResSsAnimationDict.Values)
			{
				if (value2.path == fileName)
				{
					value.Preload = value2.preLoad;
					break;
				}
			}
			SsAnimationDict.Add(fileName, value);
		}
		return value;
	}

	public static string GetFilename(string a_name)
	{
		string text = a_name;
		if (text.Contains("/"))
		{
			text = text.Substring(text.LastIndexOf('/') + 1);
		}
		return text;
	}

	public static void UnloadSsAnimation(string key, bool aForce = false)
	{
		Res.SsAnimDictInfo value;
		if (Res.ResSsAnimationDict.TryGetValue(key, out value))
		{
			_UnloadSsAnimation(value.path, value.preLoad, aForce);
		}
	}

	public static void UnloadSsAnimation(ResSsAnimation aRes, bool aForce = false)
	{
		_UnloadSsAnimation(aRes.Name, aRes.Preload, aForce);
	}

	private static void _UnloadSsAnimation(string aFile, bool aPreLoad, bool aForce)
	{
		ResSsAnimation value;
		if ((!aPreLoad || aForce) && SsAnimationDict.TryGetValue(aFile, out value))
		{
			SsAnimationDict.Remove(aFile);
			GameMain.SafeDestroy(value.SsAnime);
			value.SsAnime = null;
		}
	}

	public static void UnloadSsAnimationAll(bool aForceAll = false)
	{
		Dictionary<string, ResSsAnimation> dictionary = new Dictionary<string, ResSsAnimation>();
		foreach (KeyValuePair<string, ResSsAnimation> item in SsAnimationDict)
		{
			if (!aForceAll && item.Value.Preload)
			{
				dictionary.Add(item.Key, item.Value);
				continue;
			}
			GameMain.SafeDestroy(item.Value.SsAnime);
			item.Value.SsAnime = null;
		}
		SsAnimationDict.Clear();
		ssAnimeMatShaderLink.Clear();
		SsAnimationDict = dictionary;
		foreach (ResSsAnimation value in SsAnimationDict.Values)
		{
			CheckSsAnimeMatShaderLink(value);
		}
	}

	public static string GetAssetBundleNameBySsAnimationKey(string key)
	{
		string result = string.Empty;
		Res.SsAnimDictInfo value;
		if (Res.ResSsAnimationDict.TryGetValue(key, out value))
		{
			result = value.abName;
		}
		return result;
	}

	public static ResLive2DAnimation LoadLive2DAnimation(string key)
	{
		Res.Live2DAnimDictInfo value;
		if (!Res.ResLive2DAnimationDict.TryGetValue(key, out value))
		{
			return null;
		}
		return LoadLive2DAnimation(value.path, value.abName);
	}

	public static ResLive2DAnimation LoadLive2DAnimation(string fileName, string bundleName)
	{
		ResLive2DAnimation value;
		if (!Live2DAnimationDict.TryGetValue(fileName, out value))
		{
			string a_name = fileName + ".bytes";
			TextAsset textAsset = null;
			value = new ResLive2DAnimation(fileName);
			value.LoadState = ResourceInstance.LOADSTATE.LOADING;
			Dictionary<string, byte[]> dictionary = new Dictionary<string, byte[]>();
			BIJBinaryReader bIJBinaryReader = null;
			bool flag = false;
			if (bundleName.Length == 0 || !GameMain.USE_RESOURCEDL)
			{
				ResMngLog("LoadLive2DAnimation::Load inApp " + fileName);
				bIJBinaryReader = new BIJResourceBinaryReader(fileName);
			}
			else
			{
				ResMngLog("LoadLive2DAnimation::Load from AssetBundle " + fileName);
				flag = true;
				textAsset = LoadResourceFromAssetBundle<TextAsset>(a_name, bundleName);
				bIJBinaryReader = new BIJMemoryBinaryReader(textAsset.bytes);
			}
			int num = bIJBinaryReader.ReadInt();
			for (int i = 0; i < num; i++)
			{
				string key = bIJBinaryReader.ReadUTF();
				int num2 = bIJBinaryReader.ReadInt();
				byte[] array = new byte[num2];
				bIJBinaryReader.ReadRaw(array, 0, num2);
				if (!dictionary.ContainsKey(key))
				{
					dictionary.Add(key, array);
				}
			}
			bIJBinaryReader.Close();
			if (textAsset != null)
			{
				GameMain.SafeDestroy(textAsset);
			}
			byte[] value2;
			if (dictionary.TryGetValue("model.json", out value2))
			{
				value.ModelSetting = new ModelSettingJson(value2);
			}
			if (value.ModelSetting == null)
			{
				value = null;
			}
			else
			{
				value.BasePath = Path.GetDirectoryName(fileName);
				string modelFile = value.ModelSetting.GetModelFile();
				if (dictionary.TryGetValue(modelFile, out value2))
				{
					value.ModelData = value2;
				}
				modelFile = value.ModelSetting.GetPhysicsFile();
				if (modelFile != null && dictionary.TryGetValue(modelFile, out value2))
				{
					value.PhysicsData = value2;
				}
				string[] motionGroupNames = value.ModelSetting.GetMotionGroupNames();
				if (motionGroupNames != null)
				{
					value.MotionDataDict = new Dictionary<string, byte[]>();
					for (int j = 0; j < value.ModelSetting.GetMotionNum(motionGroupNames[0]); j++)
					{
						modelFile = value.ModelSetting.GetMotionFile(motionGroupNames[0], j);
						if (dictionary.TryGetValue(modelFile, out value2))
						{
							value.MotionDataDict.Add(value.ModelSetting.GetMotionFile(motionGroupNames[0], j), value2);
						}
					}
				}
				if (value.ModelSetting.GetExpressionNum() > 0)
				{
					value.ExpressionDataDict = new Dictionary<string, byte[]>();
					for (int k = 0; k < value.ModelSetting.GetExpressionNum(); k++)
					{
						modelFile = value.ModelSetting.GetExpressionFile(k);
						if (dictionary.TryGetValue(modelFile, out value2))
						{
							value.ExpressionDataDict.Add(value.ModelSetting.GetExpressionFile(k), value2);
						}
					}
				}
				if (value.ModelSetting.GetTextureNum() > 0)
				{
					value.Textures = new Texture2D[value.ModelSetting.GetTextureNum()];
					for (int l = 0; l < value.Textures.Length; l++)
					{
						modelFile = value.BasePath + "/" + value.ModelSetting.GetTextureFile(l);
						if (!flag)
						{
							Texture2D original = Resources.Load<Texture2D>(modelFile);
							value.Textures[l] = UnityEngine.Object.Instantiate(original);
						}
						else
						{
							a_name = modelFile + ".png";
							value.Textures[l] = LoadResourceFromAssetBundle<Texture2D>(a_name, bundleName);
						}
					}
				}
				modelFile = value.ModelSetting.GetPoseFile();
				if (modelFile != null && dictionary.TryGetValue(modelFile, out value2))
				{
					value.Pose = L2DPose.load(value2);
				}
				value.LoadState = ResourceInstance.LOADSTATE.DONE;
				Live2DAnimationDict.Add(fileName, value);
			}
		}
		return value;
	}

	public static ResLive2DAnimation LoadLive2DAnimationAsync(string key)
	{
		Res.Live2DAnimDictInfo value;
		if (!Res.ResLive2DAnimationDict.TryGetValue(key, out value))
		{
			return null;
		}
		string path = value.path;
		ResLive2DAnimation value2;
		if (!Live2DAnimationDict.TryGetValue(path, out value2))
		{
			value2 = new ResLive2DAnimation(path);
			ResourceASyncLoader.RequestAsyncLoad(key, value, value2);
			Live2DAnimationDict.Add(path, value2);
		}
		return value2;
	}

	public static ResLive2DAnimation GetResLive2DAnimation(string fileName, bool isCreate = false)
	{
		ResLive2DAnimation value = null;
		if (!Live2DAnimationDict.TryGetValue(fileName, out value) && isCreate)
		{
			value = new ResLive2DAnimation(fileName);
			Live2DAnimationDict.Add(fileName, value);
		}
		return value;
	}

	public static void UnloadLive2DAnimation(string key, bool aForce = false)
	{
		Res.Live2DAnimDictInfo value;
		if (Res.ResLive2DAnimationDict.TryGetValue(key, out value))
		{
			_UnloadLive2DAnimation(value.path, aForce);
		}
	}

	public static void UnloadLive2DAnimation(ResLive2DAnimation aRes, bool aForce = false)
	{
		_UnloadLive2DAnimation(aRes.Name, aForce);
	}

	private static void _UnloadLive2DAnimation(string aFile, bool aForce)
	{
		if (Live2DAnimationProtectList.Contains(aFile))
		{
			if (!aForce)
			{
				return;
			}
			ResMngLog("UnloadLive2DAnimation :: force unload " + aFile);
			Live2DAnimationProtectList.Remove(aFile);
		}
		ResLive2DAnimation value;
		if (Live2DAnimationDict.TryGetValue(aFile, out value))
		{
			Live2DAnimationDict.Remove(aFile);
			value.ModelSetting = null;
			value.ModelData = null;
			value.PhysicsData = null;
			value.MotionDataDict.Clear();
			value.ExpressionDataDict.Clear();
			for (int i = 0; i < value.Textures.Length; i++)
			{
				value.Textures[i] = null;
			}
			value.Pose = null;
		}
	}

	public static void UnloadLive2DAnimationAll(bool aForceAll = false)
	{
		Dictionary<string, ResLive2DAnimation> dictionary = new Dictionary<string, ResLive2DAnimation>();
		foreach (KeyValuePair<string, ResLive2DAnimation> item in Live2DAnimationDict)
		{
			if (!aForceAll && Live2DAnimationProtectList.Contains(item.Key))
			{
				dictionary.Add(item.Key, item.Value);
				continue;
			}
			item.Value.ModelSetting = null;
			item.Value.ModelData = null;
			item.Value.PhysicsData = null;
			item.Value.MotionDataDict.Clear();
			item.Value.ExpressionDataDict.Clear();
			for (int i = 0; i < item.Value.Textures.Length; i++)
			{
				item.Value.Textures[i] = null;
			}
			item.Value.Pose = null;
		}
		Live2DAnimationDict.Clear();
		Live2DAnimationDict = dictionary;
	}

	public static string GetAssetBundleNameByLive2DAnimationKey(string key)
	{
		string result = string.Empty;
		Res.Live2DAnimDictInfo value;
		if (Res.ResLive2DAnimationDict.TryGetValue(key, out value))
		{
			result = value.abName;
		}
		return result;
	}

	public static void ProtectLive2DAnimation(string key, bool protect = true)
	{
		ResMngLog("ProtectLive2DAnimation start :: key = " + key + ", protect = " + protect);
		Res.Live2DAnimDictInfo value;
		if (!Res.ResLive2DAnimationDict.TryGetValue(key, out value))
		{
			return;
		}
		string path = value.path;
		if (protect)
		{
			if (!Live2DAnimationProtectList.Contains(path))
			{
				ResMngLog("ProtectLive2DAnimation :: protected " + key);
				Live2DAnimationProtectList.Add(path);
			}
		}
		else if (Live2DAnimationProtectList.Contains(path))
		{
			ResMngLog("ProtectLive2DAnimation :: not protect " + key);
			Live2DAnimationProtectList.Remove(path);
		}
	}

	public static bool IsProtectedLive2DAnimation(string key)
	{
		Res.Live2DAnimDictInfo value;
		if (!Res.ResLive2DAnimationDict.TryGetValue(key, out value))
		{
			return false;
		}
		string path = value.path;
		if (!Live2DAnimationProtectList.Contains(path))
		{
			return false;
		}
		return true;
	}

	public static ResSound LoadSound(string key, bool isStream)
	{
		Res.SoundDictInfo value;
		if (!Res.ResSoundDict.TryGetValue(key, out value))
		{
			return null;
		}
		return LoadSound(value.path, value);
	}

	public static ResSound LoadSound(string fileName, string bundleName)
	{
		Res.SoundDictInfo soundDictInfo = null;
		foreach (Res.SoundDictInfo value in Res.ResSoundDict.Values)
		{
			if (value.path == fileName)
			{
				soundDictInfo = value;
				break;
			}
		}
		if (soundDictInfo == null)
		{
			return null;
		}
		return LoadSound(soundDictInfo.path, soundDictInfo);
	}

	public static ResSound LoadSound(string fileName, Res.SoundDictInfo dictInfo)
	{
		ResSound value;
		if (!SoundDict.TryGetValue(fileName, out value))
		{
			value = new ResSound(fileName);
			value.mLoopStart = dictInfo.loopStart;
			value.mLoopEnd = dictInfo.loopEnd;
			value.LoadState = ResourceInstance.LOADSTATE.LOADING;
			string a_name = fileName + ".mp3";
			string abName = dictInfo.abName;
			if (abName.Length > 0 && GameMain.USE_RESOURCEDL)
			{
				ResMngLog("LoadSound from AssetBundle : " + fileName);
				value.mAudioClip = LoadResourceFromAssetBundle<AudioClip>(a_name, abName);
			}
			else
			{
				AudioClip original = (AudioClip)Resources.Load(fileName);
				value.mAudioClip = UnityEngine.Object.Instantiate(original);
			}
			value.LoadState = ResourceInstance.LOADSTATE.DONE;
			if (value.mAudioClip == null)
			{
				ResMngLog("LoadSound return null : " + fileName);
			}
			else
			{
				ResMngLog("LoadSound : " + fileName);
			}
			SoundDict.Add(fileName, value);
		}
		return value;
	}

	public static ResSound LoadSoundAsync(string key)
	{
		Res.SoundDictInfo value;
		if (!Res.ResSoundDict.TryGetValue(key, out value))
		{
			return null;
		}
		string path = value.path;
		ResSound value2;
		if (!SoundDict.TryGetValue(path, out value2))
		{
			value2 = new ResSound(path);
			value2.mLoopStart = value.loopStart;
			value2.mLoopEnd = value.loopEnd;
			ResourceASyncLoader.RequestAsyncLoad(key, value, value2);
			SoundDict.Add(path, value2);
		}
		return value2;
	}

	public static ResSound GetResSound(string fileName, bool isCreate = false)
	{
		ResSound value = null;
		if (!SoundDict.TryGetValue(fileName, out value) && isCreate)
		{
			Res.SoundDictInfo soundDictInfo = null;
			foreach (Res.SoundDictInfo value2 in Res.ResSoundDict.Values)
			{
				if (value2.path == fileName)
				{
					soundDictInfo = value2;
					break;
				}
			}
			value = new ResSound(fileName);
			if (soundDictInfo != null)
			{
				value.mLoopStart = soundDictInfo.loopStart;
				value.mLoopEnd = soundDictInfo.loopEnd;
			}
			SoundDict.Add(fileName, value);
		}
		return value;
	}

	public static bool HasSound(string key)
	{
		Res.SoundDictInfo value;
		if (!Res.ResSoundDict.TryGetValue(key, out value))
		{
			return false;
		}
		string path = value.path;
		ResSound value2;
		if (SoundDict.TryGetValue(path, out value2))
		{
			if (value2.mAudioClip == null)
			{
				return false;
			}
			return true;
		}
		return false;
	}

	public static void RemoveSound(string key)
	{
	}

	public static void UnloadSound(string key)
	{
		Res.SoundDictInfo value;
		if (Res.ResSoundDict.TryGetValue(key, out value))
		{
			_UnloadSound(value.path);
		}
	}

	public static void UnloadSound(ResSound aRes)
	{
		_UnloadSound(aRes.Name);
	}

	private static void _UnloadSound(string aFile)
	{
		ResSound value;
		if (SoundDict.TryGetValue(aFile, out value))
		{
			SoundDict.Remove(aFile);
			GameMain.SafeDestroy(value.mAudioClip);
			value.mAudioClip = null;
		}
	}

	public static void UnloadSoundAll()
	{
		foreach (ResSound value in SoundDict.Values)
		{
			GameMain.SafeDestroy(value.mAudioClip);
			value.mAudioClip = null;
		}
		SoundDict.Clear();
	}

	public static void LoadPreloadSounds()
	{
		foreach (KeyValuePair<string, Res.SoundDictInfo> item in Res.ResSoundDict)
		{
			if (item.Value.preLoad)
			{
				LoadSound(item.Key, false);
			}
		}
	}

	public static void UnloadPreloadSounds()
	{
		foreach (KeyValuePair<string, Res.SoundDictInfo> item in Res.ResSoundDict)
		{
			if (item.Value.preLoad)
			{
				UnloadSound(item.Key);
			}
		}
	}

	public static void UnloadNotPreloadSounds()
	{
		foreach (KeyValuePair<string, Res.SoundDictInfo> item in Res.ResSoundDict)
		{
			if (!item.Value.preLoad)
			{
				UnloadSound(item.Key);
			}
		}
	}

	public static string GetAssetBundleNameBySoundKey(string key)
	{
		string result = string.Empty;
		Res.SoundDictInfo value;
		if (Res.ResSoundDict.TryGetValue(key, out value))
		{
			result = value.abName;
		}
		return result;
	}

	public static ResScriptableObject LoadScriptableObject(string key)
	{
		Res.ScriptableObjectDictInfo value;
		if (!Res.ResScriptableObjectDict.TryGetValue(key, out value))
		{
			return null;
		}
		return LoadScriptableObject(value.path, value.abName, value.preLoad);
	}

	public static ResScriptableObject LoadScriptableObject(string fileName, string bundleName, bool preload)
	{
		ResScriptableObject value;
		if (!ScriptableObjectDict.TryGetValue(fileName, out value))
		{
			value = new ResScriptableObject(fileName);
			ResourceDLUtils.dbg1(fileName);
			ResMngLog("LoadScriptableObject 2");
			value.Preload = preload;
			value.LoadState = ResourceInstance.LOADSTATE.LOADING;
			string a_name = fileName + ".prefab";
			if (bundleName.Length > 0 && GameMain.USE_RESOURCEDL)
			{
				ResMngLog("LoadScriptableObject 3");
				GameObject gameObject = LoadResourceFromAssetBundle<GameObject>(a_name, bundleName);
				ResMngLog("LoadScriptableObject 4A");
				if (gameObject != null)
				{
					ResMngLog("LoadScriptableObject 5A");
					value.Container = gameObject.GetComponent<ScriptableObjectContainer>();
					value.mPrefab = gameObject;
				}
			}
			else
			{
				value.Container = CreateScriptableObjectFromPrefab(fileName, value);
			}
			value.LoadState = ResourceInstance.LOADSTATE.DONE;
			ResMngLog("LoadScriptableObject 6");
			if (value.Container == null)
			{
				ResMngLog("LoadScriptableObject 7");
				value = null;
			}
			else
			{
				ResMngLog("LoadScriptableObject 8");
				ScriptableObjectDict.Add(fileName, value);
			}
		}
		return value;
	}

	public static ResScriptableObject LoadScriptableObjectAsync(string key, bool waitAllLoad = false)
	{
		Res.ScriptableObjectDictInfo value;
		if (!Res.ResScriptableObjectDict.TryGetValue(key, out value))
		{
			return null;
		}
		string path = value.path;
		ResScriptableObject value2;
		if (!ScriptableObjectDict.TryGetValue(path, out value2))
		{
			value2 = new ResScriptableObject(path);
			value2.Preload = value.preLoad;
			ResourceASyncLoader.RequestAsyncLoad(key, value, value2, waitAllLoad);
			ScriptableObjectDict.Add(path, value2);
		}
		return value2;
	}

	public static ResScriptableObject GetResScriptableObject(string fileName, bool isCreate = false)
	{
		ResScriptableObject value = null;
		if (!ScriptableObjectDict.TryGetValue(fileName, out value) && isCreate)
		{
			value = new ResScriptableObject(fileName);
			foreach (Res.ScriptableObjectDictInfo value2 in Res.ResScriptableObjectDict.Values)
			{
				if (value2.path == fileName)
				{
					value.Preload = value2.preLoad;
					break;
				}
			}
			ScriptableObjectDict.Add(fileName, value);
		}
		return value;
	}

	public static void UnloadScriptableObject(string key)
	{
		Res.ScriptableObjectDictInfo value;
		if (Res.ResScriptableObjectDict.TryGetValue(key, out value))
		{
			_UnloadScriptableObject(value.path);
		}
	}

	public static void UnloadScriptableObject(ResScriptableObject aRes)
	{
		_UnloadScriptableObject(aRes.Name);
	}

	private static void _UnloadScriptableObject(string aFile)
	{
		ResScriptableObject value;
		if (ScriptableObjectDict.TryGetValue(aFile, out value))
		{
			ScriptableObjectDict.Remove(aFile);
			if (value.mPrefab != null)
			{
				GameMain.SafeDestroy(value.mPrefab);
				value.mPrefab = null;
			}
			value.Container = null;
		}
	}

	public static void UnloadScriptableObjectAll(bool aForceAll = false)
	{
		Dictionary<string, ResScriptableObject> dictionary = new Dictionary<string, ResScriptableObject>();
		foreach (KeyValuePair<string, ResScriptableObject> item in ScriptableObjectDict)
		{
			if (!aForceAll && item.Value.Preload)
			{
				dictionary.Add(item.Key, item.Value);
				continue;
			}
			if (item.Value.mPrefab != null)
			{
				GameMain.SafeDestroy(item.Value.mPrefab);
				item.Value.mPrefab = null;
			}
			item.Value.Container = null;
		}
		ScriptableObjectDict.Clear();
		ScriptableObjectDict = dictionary;
	}

	public static bool EnableLoadScriptableObject(string key)
	{
		Res.ScriptableObjectDictInfo value;
		if (!Res.ResScriptableObjectDict.TryGetValue(key, out value))
		{
			return false;
		}
		if (value.abName.Length > 0 && GameMain.USE_RESOURCEDL)
		{
			ResourceDownloadManager mDLManager = SingletonMonoBehaviour<GameMain>.Instance.mDLManager;
			ResourceDownloadManager.KIND kindWithFileName = ResourceDownloadManager.getKindWithFileName(value.abName);
			if (!mDLManager.DLFilelistHashCheck(kindWithFileName))
			{
				return false;
			}
			return mDLManager.CheckDLFileIntegrity(kindWithFileName, value.abName + ".unity3d");
		}
		return true;
	}

	public static string GetAssetBundleNameByScriptableObjectKey(string key)
	{
		string result = string.Empty;
		Res.ScriptableObjectDictInfo value;
		if (Res.ResScriptableObjectDict.TryGetValue(key, out value))
		{
			result = value.abName;
		}
		return result;
	}

	private static ScriptableObjectContainer CreateScriptableObjectFromPrefab(string fileName, ResScriptableObject res)
	{
		ResMngLog("CreateScriptableObjectFromPrefab:start " + fileName);
		GameObject original = Resources.Load(fileName, typeof(GameObject)) as GameObject;
		GameObject gameObject = (res.mPrefab = UnityEngine.Object.Instantiate(original));
		ScriptableObjectContainer scriptableObjectContainer = gameObject.GetComponent<ScriptableObjectContainer>();
		if (scriptableObjectContainer == null)
		{
			scriptableObjectContainer = gameObject.AddComponent<ScriptableObjectContainer>();
		}
		ResMngLog("CreateScriptableObjectFromPrefab:end");
		return scriptableObjectContainer;
	}

	private static ScriptableObjectContainer CreateScriptableObjectFromPrefabWithInstantinate(string fileName)
	{
		GameObject original = Resources.Load(fileName, typeof(GameObject)) as GameObject;
		GameObject gameObject = UnityEngine.Object.Instantiate(original);
		gameObject.name = fileName.Replace('/', '_');
		ScriptableObjectContainer scriptableObjectContainer = gameObject.GetComponent<ScriptableObjectContainer>();
		if (scriptableObjectContainer == null)
		{
			scriptableObjectContainer = gameObject.AddComponent<ScriptableObjectContainer>();
		}
		return scriptableObjectContainer;
	}

	private static void _loadPng(ResTexture aRes, byte[] aData)
	{
		aRes.Tex = new Texture2D(0, 0);
		aRes.Tex.LoadImage(aData);
	}

	private static IEnumerator _loadPngAsync(ResTexture aRes, string fileName)
	{
		string name = fileName;
		if (name.LastIndexOf(".") > 0 && !name.Contains(".bytes"))
		{
			name += ".bytes";
		}
		ResourceDownloadManager.KIND kind = ResourceDownloadManager.getKindWithFileName(fileName);
		string url = "file:///" + ResourceDownloadManager.getResourceDLPath(kind) + name;
		using (WWW www = new WWW(url))
		{
			while (!www.isDone && www.error == null)
			{
				yield return null;
			}
			string wwwError = www.error;
			if (wwwError != null)
			{
				aRes.LoadState = ResourceInstance.LOADSTATE.DONE;
				yield break;
			}
			byte[] encryptedData = www.bytes;
			byte[] decryptedData = CryptUtils.DecryptRJ128(encryptedData);
			_loadPng(aRes, decryptedData);
		}
		aRes.LoadState = ResourceInstance.LOADSTATE.DONE;
	}

	public static ResTexture LoadPng(string fileName)
	{
		ResTexture resTexture = new ResTexture(fileName);
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (!instance.mDLManager.IsDownloadFileExist(fileName))
		{
			resTexture.LoadState = ResourceInstance.LOADSTATE.DONE;
			return resTexture;
		}
		resTexture.isExist = true;
		byte[] bytesDownloadFile = instance.mDLManager.GetBytesDownloadFile(fileName);
		if (bytesDownloadFile == null)
		{
			resTexture.LoadState = ResourceInstance.LOADSTATE.DONE;
			return resTexture;
		}
		_loadPng(resTexture, bytesDownloadFile);
		resTexture.LoadState = ResourceInstance.LOADSTATE.DONE;
		return resTexture;
	}

	public ResTexture LoadPngAsync(string fileName)
	{
		ResTexture resTexture = new ResTexture(fileName);
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (!instance.mDLManager.IsDownloadFileExist(fileName))
		{
			resTexture.LoadState = ResourceInstance.LOADSTATE.DONE;
			return resTexture;
		}
		resTexture.isExist = true;
		resTexture.LoadState = ResourceInstance.LOADSTATE.LOADING;
		StartCoroutine(_loadPngAsync(resTexture, fileName));
		return resTexture;
	}

	public static IEnumerator LoadBinaryFile(string a_filename, LoadBinaryHandler callback)
	{
		BIJDataBinaryReader stream = new BIJDataBinaryReader(a_filename);
		if (callback != null)
		{
			yield return callback(stream);
		}
		stream.Close();
	}

	public static T LoadResourceFromAssetBundle<T>(string a_name, string a_bundleName, AssetBundle a_bundle = null) where T : UnityEngine.Object
	{
		ResMngLog("LoadResourceFromAssetBundle start::" + a_name + " bundleName=" + a_bundleName);
		if (a_bundleName.Length == 0)
		{
			return (T)null;
		}
		ResMngLog("LoadResourceFromAssetBundle 1");
		AssetBundle assetBundle = a_bundle;
		if (assetBundle == null)
		{
			assetBundle = GetAssetBundle(a_bundleName);
		}
		ResMngLog("LoadResourceFromAssetBundle 2");
		if (ResourceDLUtils.IsDebugLoadResourceNoBundleError())
		{
			assetBundle.Unload(true);
			assetBundle = null;
		}
		ResMngLog("LoadResourceFromAssetBundle 3");
		if (assetBundle == null)
		{
			ResourceDLUtils.dbg1("bundle null :" + a_name, true);
			return (T)null;
		}
		ResMngLog("LoadResourceFromAssetBundle 4");
		UnityEngine.Object @object = assetBundle.LoadAsset("Assets/Resources/" + a_name);
		ResMngLog("LoadResourceFromAssetBundle 4-1");
		if (@object == null)
		{
			ResMngLog("LoadResourceFromAssetBundle 4-2");
			ResourceDLUtils.dbg1("bundle.Load miss :" + a_name, true);
			return (T)null;
		}
		ResMngLog("LoadResourceFromAssetBundle 5");
		T val = UnityEngine.Object.Instantiate(@object) as T;
		if (ResourceDLUtils.IsDebugLoadResourceNoGameObjectError())
		{
			val = (T)null;
		}
		ResMngLog("LoadResourceFromAssetBundle 6");
		if ((UnityEngine.Object)val == (UnityEngine.Object)null)
		{
			ResourceDLUtils.dbg1("ret null :" + a_name, true);
			return (T)null;
		}
		ResMngLog("LoadResourceFromAssetBundle end");
		return val;
	}

	public static UnityEngine.AssetBundleRequest LoadResourceAsyncFromAssetBundle(string a_name, string a_bundleName, AssetBundle a_bundle = null)
	{
		if (a_bundleName.Length == 0)
		{
			return null;
		}
		AssetBundle assetBundle = a_bundle;
		if (assetBundle == null)
		{
			assetBundle = GetAssetBundle(a_bundleName);
		}
		if (ResourceDLUtils.IsDebugLoadResourceNoBundleError())
		{
			assetBundle.Unload(true);
			assetBundle = null;
		}
		if (assetBundle == null)
		{
			ResourceDLUtils.dbg1("bundle null :" + a_name, true);
			return null;
		}
		return assetBundle.LoadAssetAsync("Assets/Resources/" + a_name, typeof(UnityEngine.Object));
	}

	public static void EnableLoadAllResourceFromAssetBundle()
	{
		ResourceASyncLoader.setAllLoadEnable(true);
	}

	public static void DisableAsyncLoad(ResourceASyncLoader.WAIT_REQUEST aRequest)
	{
		ResourceASyncLoader.DisableAsyncLoad(aRequest);
	}

	public static void EnableAsyncLoad(ResourceASyncLoader.WAIT_REQUEST aRequest)
	{
		ResourceASyncLoader.EnableAsyncLoad(aRequest);
	}

	public IEnumerator LoadAssetBundles()
	{
		ResourceDLUtils.dbg1("LoadAssetBundles", true);
		if (mAssetBundleRequestDict.Count == 0)
		{
			yield break;
		}
		foreach (AssetBundleRequest value in mAssetBundleRequestDict.Values)
		{
			ResourceDLUtils.dbg1("LOAD = " + value.mAssetBundleName, true);
			yield return StartCoroutine(LoadAssetBundle(value.mAssetBundleName, delegate(AssetBundle ab, string arg)
			{
				SingletonMonoBehaviour<ResourceManager>.Instance.AssetBundleError = arg;
			}));
			if (SingletonMonoBehaviour<ResourceManager>.Instance.AssetBundleError != string.Empty)
			{
				ResourceDLUtils.err("ERROR = " + SingletonMonoBehaviour<ResourceManager>.Instance.AssetBundleError, true);
				yield break;
			}
		}
		ResourceDLUtils.dbg1("LoadAssetBundles finished", true);
	}

	public IEnumerator LoadSceneResources(string a_scene)
	{
		ResMngLog("LoadSceneResources start : scene=" + a_scene);
		ClearRequest();
		ResourceASyncLoader.setAllLoadEnable(false);
		List<string> aseetBundleList = ABInfoList.GetAssetBundleListFromScene(a_scene);
		if (aseetBundleList.Count <= 0)
		{
			ResMngLog("LoadSceneResources need not AseetBundle");
		}
		else
		{
			AddRequests(aseetBundleList);
			yield return StartCoroutine(LoadAssetBundles());
		}
		foreach (KeyValuePair<string, Res.ImageDictInfo> pair3 in Res.ResImageDict)
		{
			if (pair3.Value.sceneList.Contains(a_scene))
			{
				LoadImage(pair3.Key);
			}
		}
		foreach (KeyValuePair<string, Res.SsAnimDictInfo> pair2 in Res.ResSsAnimationDict)
		{
			if (pair2.Value.sceneList.Contains(a_scene))
			{
				LoadSsAnimation(pair2.Key);
			}
		}
		foreach (KeyValuePair<string, Res.SoundDictInfo> pair in Res.ResSoundDict)
		{
			if (pair.Value.sceneList != null && pair.Value.sceneList.Contains(a_scene))
			{
				LoadSound(pair.Key, false);
			}
		}
		for (int i = 0; i < aseetBundleList.Count; i++)
		{
			if (!ResourceASyncLoader.IsAssetBundleAccessing(aseetBundleList[i]))
			{
				UnloadAssetBundle(aseetBundleList[i]);
			}
		}
		ResMngLog("LoadSceneResources end");
	}

	public IEnumerator UnloadSceneResources(string a_scene, string a_next = "NEXT")
	{
		ResMngLog("UnloadSceneResources start : scene=" + a_scene + ", next=" + a_next);
		ClearRequest();
		foreach (KeyValuePair<string, Res.ImageDictInfo> pair3 in Res.ResImageDict)
		{
			if (pair3.Value.sceneList.Contains(a_scene) && !pair3.Value.sceneList.Contains(a_next))
			{
				UnloadImage(pair3.Key);
			}
		}
		foreach (KeyValuePair<string, Res.SsAnimDictInfo> pair2 in Res.ResSsAnimationDict)
		{
			if (pair2.Value.sceneList.Contains(a_scene) && !pair2.Value.sceneList.Contains(a_next))
			{
				UnloadSsAnimation(pair2.Key);
			}
		}
		foreach (KeyValuePair<string, Res.SoundDictInfo> pair in Res.ResSoundDict)
		{
			if (pair.Value.sceneList != null && pair.Value.sceneList.Contains(a_scene) && !pair.Value.sceneList.Contains(a_next))
			{
				UnloadSound(pair.Key);
			}
		}
		ResMngLog("UnloadSceneResources end");
		yield break;
	}

	public static AssetBundle GetAssetBundle(string a_key)
	{
		AssetBundleRef value;
		if (mAssetBundleDict.TryGetValue(a_key, out value))
		{
			return value.AssetBundleInstance;
		}
		return null;
	}

	public IEnumerator LoadAssetBundle(string a_bundleName, Action<AssetBundle, string> a_callback = null)
	{
		if (AlreadyLoading(a_bundleName))
		{
			while (!AlreadyLoaded(a_bundleName))
			{
				ResourceDLUtils.dbg1("AlreadyLoading AB name=" + a_bundleName, true);
				yield return null;
			}
			ResourceDLUtils.dbg1("AlreadyLoaded AB name=" + a_bundleName, true);
			yield break;
		}
		AssetBundleRef abRef = new AssetBundleRef(a_bundleName)
		{
			LoadState = AssetBundleRef.LOADSTATE.LOADING
		};
		mAssetBundleDict.Add(a_bundleName, abRef);
		string url = GetAssetBundlePath(a_bundleName);
		ResourceDLUtils.dbg1("Load AB name=" + a_bundleName, true);
		using (WWW www = new WWW(url))
		{
			while (!www.isDone && www.error == null)
			{
				yield return null;
			}
			string wwwError = www.error;
			if (ResourceDLUtils.IsDebugLoadAssetBundleWWWError())
			{
				wwwError = "error test Asset Bundle www.";
			}
			if (wwwError != null)
			{
				abRef.LoadState = AssetBundleRef.LOADSTATE.INIT;
				mAssetBundleDict.Remove(a_bundleName);
				if (a_callback != null)
				{
					a_callback(null, wwwError);
				}
				yield break;
			}
			byte[] encryptedData = www.bytes;
			byte[] decryptedData = CryptUtils.DecryptRJ128(encryptedData);
			AssetBundleCreateRequest acr = AssetBundle.LoadFromMemoryAsync(decryptedData);
			yield return acr;
			AssetBundle ab = acr.assetBundle;
			if (ab == null)
			{
				abRef.LoadState = AssetBundleRef.LOADSTATE.INIT;
				mAssetBundleDict.Remove(a_bundleName);
				if (a_callback != null)
				{
					a_callback(null, wwwError);
				}
				yield break;
			}
			abRef.AssetBundleInstance = ab;
			abRef.LoadState = AssetBundleRef.LOADSTATE.DONE;
			ResourceDLUtils.dbg1("load success!! : " + a_bundleName, true);
			if (a_callback != null)
			{
				a_callback(ab, string.Empty);
			}
			www.Dispose();
		}
	}

	public static void UnloadAssetBundles(bool a_allObjects = false)
	{
		if (mAssetBundleDict.Count == 0)
		{
			return;
		}
		string[] array = new string[mAssetBundleDict.Count];
		mAssetBundleDict.Keys.CopyTo(array, 0);
		for (int i = 0; i < array.Length; i++)
		{
			if (!ResourceASyncLoader.IsAssetBundleAccessing(array[i]))
			{
				UnloadAssetBundle(array[i], a_allObjects);
			}
		}
	}

	public static void UnloadAssetBundle(string a_key, bool a_allObjects = false)
	{
		AssetBundleRef assetBundleRef = GetAssetBundleRef(a_key);
		if (assetBundleRef == null)
		{
			return;
		}
		if (assetBundleRef.LoadState == AssetBundleRef.LOADSTATE.LOADING)
		{
			ResMngLog("UnloadAssetBundle disable while loading!! key=" + a_key);
			return;
		}
		if (assetBundleRef.LoadingCount > 0)
		{
			ResMngLog("UnloadAssetBundle disable while resource loading!! key=" + a_key);
			return;
		}
		if (assetBundleRef.AssetBundleInstance != null)
		{
			assetBundleRef.AssetBundleInstance.Unload(a_allObjects);
			assetBundleRef.AssetBundleInstance = null;
		}
		mAssetBundleDict.Remove(a_key);
		ResMngLog("UnloadAssetBundle key=" + a_key);
	}

	public static bool AlreadyLoaded(string a_key)
	{
		AssetBundleRef assetBundleRef = GetAssetBundleRef(a_key);
		if (assetBundleRef != null && assetBundleRef.AssetBundleInstance != null)
		{
			return true;
		}
		return false;
	}

	public static bool AlreadyLoading(string a_key)
	{
		AssetBundleRef assetBundleRef = GetAssetBundleRef(a_key);
		if (assetBundleRef != null && assetBundleRef.LoadState != 0)
		{
			return true;
		}
		return false;
	}

	public static void AddAssetBundleRequest(string a_name)
	{
		if (!mAssetBundleRequestDict.ContainsKey(a_name))
		{
			mAssetBundleRequestDict.Add(a_name, new AssetBundleRequest(a_name));
		}
	}

	private static AssetBundleRef GetAssetBundleRef(string a_key)
	{
		AssetBundleRef value;
		if (mAssetBundleDict.TryGetValue(a_key, out value))
		{
			return value;
		}
		return null;
	}

	private static string GetAssetBundlePath(string a_bundleName)
	{
		ResourceDownloadManager.KIND kindWithFileName = ResourceDownloadManager.getKindWithFileName(a_bundleName);
		return "file:///" + ResourceDownloadManager.getResourceDLPath(kindWithFileName) + a_bundleName + ".unity3d";
	}

	public static void AddLoadingCounter(string a_key, int a_count)
	{
		AssetBundleRef value;
		if (mAssetBundleDict.TryGetValue(a_key, out value))
		{
			value.LoadingCount += a_count;
			mAssetBundleDict[a_key] = value;
		}
	}

	public static int GetLoadCount(string a_key)
	{
		AssetBundleRef value;
		if (mAssetBundleDict.TryGetValue(a_key, out value))
		{
			return value.LoadingCount;
		}
		return 0;
	}

	public static void AddRequest(string a_bundleName)
	{
		if (a_bundleName.Length != 0)
		{
			AddAssetBundleRequest(a_bundleName);
		}
	}

	public static void AddRequests(List<string> a_nameList)
	{
		for (int i = 0; i < a_nameList.Count; i++)
		{
			AddRequest(a_nameList[i]);
		}
	}

	public static void AddRequestImage(string key)
	{
		Res.ImageDictInfo value;
		if (Res.ResImageDict.TryGetValue(key, out value) && value.abName.Length != 0)
		{
			AddRequest(value.abName);
		}
	}

	public static void AddRequestSsAnimation(string key)
	{
		Res.SsAnimDictInfo value;
		if (Res.ResSsAnimationDict.TryGetValue(key, out value) && value.abName.Length != 0)
		{
			AddRequest(value.abName);
		}
	}

	public static void AddRequestLive2dAnimation(string key)
	{
		Res.Live2DAnimDictInfo value;
		if (Res.ResLive2DAnimationDict.TryGetValue(key, out value) && value.abName.Length != 0)
		{
			AddRequest(value.abName);
		}
	}

	public static void AddRequestSound(string key)
	{
		Res.SoundDictInfo value;
		if (Res.ResSoundDict.TryGetValue(key, out value) && value.abName.Length != 0)
		{
			AddRequest(value.abName);
		}
	}

	public static void ClearRequest()
	{
		mAssetBundleRequestDict.Clear();
	}
}
