using UnityEngine;

public class InputInfo
{
	public enum SWIPE_DIR
	{
		NONE = 0,
		UP = 2,
		DOWN = 4,
		LEFT = 8,
		RIGHT = 0x10
	}

	public Vector3 FirstScreenPosition;

	public Vector3 OldScreenPosition;

	public Vector3 ScreenPosition;

	public Vector3 DeltaPosition;

	public bool IsDown;

	public bool IsUp;

	public bool IsDrag;

	public bool IsStay;

	public int TouchFrameCount;

	public float TouchTime;

	public bool IsDownTrigger;

	public bool IsUpTrigger;

	public bool IsFlickTrigger;

	public int SwipeDir;

	public void SetTouchInfo(Touch a_touch)
	{
		OldScreenPosition = ScreenPosition;
		ScreenPosition = a_touch.position;
		DeltaPosition = a_touch.deltaPosition;
	}

	public void SetMouseInfo()
	{
		OldScreenPosition = ScreenPosition;
	}

	public void Reset()
	{
		ScreenPosition = new Vector3(0f, 0f, 0f);
		FirstScreenPosition = ScreenPosition;
		OldScreenPosition = ScreenPosition;
		DeltaPosition = new Vector3(0f, 0f, 0f);
		IsDown = false;
		IsUp = false;
		IsDrag = false;
		IsStay = false;
		TouchFrameCount = 0;
		TouchTime = 0f;
		IsDownTrigger = false;
		IsUpTrigger = false;
		IsFlickTrigger = false;
	}

	public void SetFlickDir()
	{
		SwipeDir = 0;
	}
}
