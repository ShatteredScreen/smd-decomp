using System.Collections.Generic;

public class LinkBannerSetting
{
	[MiniJSONAlias("baseurl")]
	public string BaseURL { get; set; }

	[MiniJSONAlias("banner")]
	public List<LinkBannerDetail> BannerList { get; set; }

	public LinkBannerSetting()
	{
		BaseURL = string.Empty;
		BannerList = new List<LinkBannerDetail>();
	}
}
