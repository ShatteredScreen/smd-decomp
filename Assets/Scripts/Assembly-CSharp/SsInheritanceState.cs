using System;

[Serializable]
public class SsInheritanceState
{
	public SsInheritanceType Type;

	public SsInheritanceParam[] Values;

	public SsInheritanceState()
	{
		Values = new SsInheritanceParam[21];
	}

	public void Initialize(int iVersion, bool imageFlip)
	{
		Type = SsInheritanceType.Parent;
		for (int i = 0; i < Values.Length; i++)
		{
			Values[i] = new SsInheritanceParam();
			SsKeyAttr ssKeyAttr = (SsKeyAttr)i;
			if (ssKeyAttr == SsKeyAttr.FlipV || ssKeyAttr == SsKeyAttr.FlipH || ssKeyAttr == SsKeyAttr.Hide)
			{
				Values[i].Use = false;
				Values[i].Rate = 0f;
			}
			else
			{
				Values[i].Use = true;
				Values[i].Rate = 1f;
			}
		}
	}

	public override string ToString()
	{
		string text = string.Concat("Type: ", Type, "\n");
		int num = 0;
		SsInheritanceParam[] values = Values;
		foreach (SsInheritanceParam ssInheritanceParam in values)
		{
			string text2 = text;
			text = string.Concat(text2, "\tValues[", Enum.GetName(typeof(SsKeyAttr), num), "]: ", ssInheritanceParam, "\n");
			num++;
		}
		return text + "\n";
	}
}
