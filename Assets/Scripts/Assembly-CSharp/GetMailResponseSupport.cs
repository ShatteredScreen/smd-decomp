using System;

public class GetMailResponseSupport : ICloneable
{
	[MiniJSONAlias("id")]
	public int MailID { get; set; }

	[MiniJSONAlias("from_uuid")]
	public int FromUUID { get; set; }

	[MiniJSONAlias("message")]
	public string Message { get; set; }

	[MiniJSONAlias("category")]
	public int CategoryMain { get; set; }

	[MiniJSONAlias("sub_category")]
	public int CategorySub { get; set; }

	[MiniJSONAlias("itemid")]
	public int ItemID { get; set; }

	[MiniJSONAlias("quantity")]
	public int Quantity { get; set; }

	[MiniJSONAlias("mail_type")]
	public int MailType { get; set; }

	[MiniJSONAlias("ctime")]
	public int RemoteTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public GetMailResponseSupport Clone()
	{
		return MemberwiseClone() as GetMailResponseSupport;
	}

	public virtual GiftItemData GetGiftItemData()
	{
		GiftItemData giftItemData = new GiftItemData();
		giftItemData.RemoteID = MailID;
		giftItemData.FromID = FromUUID;
		giftItemData.Message = Message;
		giftItemData.ItemCategory = CategoryMain;
		giftItemData.ItemKind = CategorySub;
		giftItemData.ItemID = ItemID;
		giftItemData.Quantity = Quantity;
		giftItemData.MailType = MailType;
		giftItemData.RemoteTimeEpoch = RemoteTime;
		giftItemData.UseIcon = false;
		giftItemData.IconID = 0;
		giftItemData.IconLevel = 0;
		giftItemData.UsePlayerInfo = false;
		giftItemData.Series = 0;
		giftItemData.SeriesStage = 0;
		return giftItemData;
	}
}
