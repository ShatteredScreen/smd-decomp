using System;
using System.Collections;
using UnityEngine;

public class TutorialArrow : MonoBehaviour
{
	public enum STYLE
	{
		ARROW = 0,
		FINGER = 1,
		FINGER_TWEEN = 2
	}

	private STYLE mStyle;

	private Vector3 mTargetPos;

	private GameObject mTarget;

	private UISprite mSprite;

	private Vector3 mTargetOfs;

	private Vector3 mTargetPosOfs;

	private UISprite mEffect;

	private Def.DIR mDir;

	private bool mVFlip;

	private Def.DIR mArrow;

	private float mProgression;

	private int mBasedepth = 80;

	private float mYposition;

	private GameObject mDestTarget;

	private Vector3 mDestTargetPos;

	private void OnDestroy()
	{
		if (mEffect != null)
		{
			UnityEngine.Object.Destroy(mEffect.gameObject);
			mEffect = null;
		}
	}

	private void Start()
	{
		UIPanel uIPanel = base.gameObject.AddComponent<UIPanel>();
		uIPanel.sortingOrder = SingletonMonoBehaviour<GameMain>.Instance.mDialogManager.mDepth + 2;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mSprite = Util.CreateSprite("Arrow", base.gameObject, image);
		if (mStyle == STYLE.ARROW)
		{
			Util.SetSpriteInfo(mSprite, "tutorial_arrow", mBasedepth, Vector3.zero, Vector3.one, false);
			mTargetOfs = new Vector3(0f, -34f, 0f);
		}
		else
		{
			Util.SetSpriteInfo(mSprite, "tutorial_finger", mBasedepth, Vector3.zero, Vector3.one, false);
			mTargetOfs = new Vector3(18f, -63f, 0f);
		}
		if (mVFlip)
		{
			Vector3 localScale = mSprite.transform.localScale;
			localScale.y *= -1f;
			mSprite.transform.localScale = localScale;
			mTargetOfs = new Vector3(0f, mYposition, 0f);
		}
		if (mArrow == Def.DIR.LEFT || mDir == Def.DIR.NONE_LEFT)
		{
			mSprite.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			mTargetOfs = new Vector3(150f, 0f, 0f);
		}
		if (mArrow == Def.DIR.RIGHT)
		{
			mSprite.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, 270f);
			mTargetOfs = new Vector3(-150f, 0f, 0f);
		}
		if (mArrow == Def.DIR.DOWN)
		{
			mSprite.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);
			mTargetOfs = new Vector3(0f, 90f, 0f);
		}
		mProgression = 0f;
	}

	private void Update()
	{
		Vector2 vector = Util.LogScreenSize();
		mTargetPos = mTarget.transform.position * (vector.y / 2f);
		if (mDestTarget != null)
		{
			mDestTargetPos = mDestTarget.transform.position * (vector.y / 2f);
		}
		Vector3 position = Vector3.zero;
		float x = 0f;
		float num = 0f;
		if (mStyle == STYLE.ARROW)
		{
			if (mArrow == Def.DIR.NONE)
			{
				mProgression += Time.deltaTime * 180f;
				if (mProgression > 180f)
				{
					mProgression -= 180f;
				}
				num = (0f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))) * 40f;
				if (mVFlip)
				{
					num *= -1f;
				}
			}
			else
			{
				mProgression += Time.deltaTime * 180f;
				if (mProgression > 180f)
				{
					mProgression -= 180f;
				}
				x = (float)Def.DIR_OFS[(int)mArrow].x * (0f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))) * 40f;
				num = (float)Def.DIR_OFS[(int)mArrow].y * (0f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))) * 40f;
				if (mArrow == Def.DIR.LEFT)
				{
					mTargetOfs = new Vector3(95f, 0f, 0f);
				}
				else if (mArrow == Def.DIR.RIGHT)
				{
					mTargetOfs = new Vector3(-95f, 0f, 0f);
				}
			}
			position = (mTargetPos + mTargetOfs + mTargetPosOfs + new Vector3(x, num, -1f)) / (vector.y / 2f);
		}
		else if (mStyle == STYLE.FINGER)
		{
			if (mDir == Def.DIR.NONE)
			{
				mProgression += Time.deltaTime * 180f;
				if (mProgression > 180f)
				{
					mProgression -= 180f;
					BIJImage image = ResourceManager.LoadImage("HUD").Image;
					mEffect = Util.CreateSprite("Effect", base.gameObject, image);
					Util.SetSpriteInfo(mEffect, "efc_shine_ring00", mBasedepth - 1, Vector3.zero, Vector3.one, false);
					mEffect.transform.position = mTargetPos / (vector.y / 2f);
					StartCoroutine(TapEFfect());
				}
				num = (0f - Mathf.Sin(mProgression * ((float)Math.PI / 180f))) * 40f;
				if (mVFlip)
				{
					num *= -1f;
				}
			}
			else if (mDir == Def.DIR.NONE_LEFT)
			{
				mProgression += Time.deltaTime * 180f;
				if (mProgression > 180f)
				{
					mProgression -= 180f;
					BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
					mEffect = Util.CreateSprite("Effect", base.gameObject, image2);
					Util.SetSpriteInfo(mEffect, "efc_shine_ring00", mBasedepth - 1, Vector3.zero, Vector3.one, false);
					mEffect.transform.position = mTargetPos / (vector.y / 2f);
					StartCoroutine(TapEFfect());
				}
				x = (float)Def.DIR_OFS[6].x * Def.TILE_PITCH_H * (mProgression / 180f);
			}
			else
			{
				mProgression += Time.deltaTime;
				if (mProgression > 1f)
				{
					mProgression = 0f;
				}
				x = (float)Def.DIR_OFS[(int)mDir].x * Def.TILE_PITCH_H * mProgression;
				num = (float)Def.DIR_OFS[(int)mDir].y * Def.TILE_PITCH_V * mProgression;
			}
			position = (mTargetPos + mTargetOfs + mTargetPosOfs + new Vector3(x, num, -1f)) / (vector.y / 2f);
		}
		else if (mStyle == STYLE.FINGER_TWEEN)
		{
			mProgression += Time.deltaTime * 60f;
			if (mProgression > 90f)
			{
				mProgression -= 90f;
			}
			float t = Mathf.Sin(mProgression * ((float)Math.PI / 180f));
			Vector3 vector2 = Vector3.Lerp(mTargetPos, mDestTargetPos, t);
			vector2 += mTargetOfs + mTargetPosOfs;
			x = vector2.x;
			num = vector2.y;
			if (mVFlip)
			{
				num *= -1f;
			}
			position = new Vector3(x, num, mTargetPos.z) / (vector.y / 2f);
		}
		mSprite.transform.position = position;
	}

	public void Init(STYLE style, GameObject target, bool vFlip, Def.DIR dir = Def.DIR.DOWN, int depth = 80, Def.DIR Arrow = Def.DIR.NONE, float Yposition = 0f)
	{
		mStyle = style;
		mTarget = target;
		mVFlip = vFlip;
		mDir = dir;
		mBasedepth = depth;
		mArrow = Arrow;
		mYposition = Yposition;
		mTargetPosOfs = Vector3.zero;
	}

	public void Init(STYLE style, GameObject target, GameObject dest, bool vFlip, Def.DIR dir = Def.DIR.DOWN, int depth = 80, Def.DIR Arrow = Def.DIR.NONE, float Yposition = 0f)
	{
		Init(style, target, vFlip, dir, depth, Arrow, Yposition);
		mDestTarget = dest;
	}

	public void SetTargetPosOfs(float x, float y)
	{
		mTargetPosOfs = new Vector3(x, y, 0f);
	}

	private IEnumerator TapEFfect()
	{
		float angle = 0f;
		while (angle < 90f && mEffect != null)
		{
			angle += Time.deltaTime * 190f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float scale = Mathf.Sin(angle * ((float)Math.PI / 180f)) * 2f;
			float alpha = 1f - Mathf.Sin(angle * ((float)Math.PI / 180f));
			mEffect.transform.localScale = new Vector3(scale, scale, 1f);
			mEffect.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		if (mEffect != null)
		{
			UnityEngine.Object.Destroy(mEffect.gameObject);
			mEffect = null;
		}
	}
}
