public class ByteVector2
{
	public sbyte x;

	public sbyte y;

	public ByteVector2(sbyte x, sbyte y)
	{
		this.x = x;
		this.y = y;
	}

	public override string ToString()
	{
		return string.Format("X={0} Y={1}", x, y);
	}

	public override int GetHashCode()
	{
		return x * 65535 + y;
	}

	public override bool Equals(object obj)
	{
		return obj is IntVector2 && obj.GetHashCode() == GetHashCode();
	}

	public static ByteVector2 operator +(ByteVector2 a, ByteVector2 b)
	{
		return new ByteVector2((sbyte)(a.x + b.x), (sbyte)(a.y + b.y));
	}

	public static ByteVector2 operator -(ByteVector2 a, ByteVector2 b)
	{
		return new ByteVector2((sbyte)(a.x - b.x), (sbyte)(a.y - b.y));
	}

	public static ByteVector2 operator *(ByteVector2 a, ByteVector2 b)
	{
		return new ByteVector2((sbyte)(a.x * b.x), (sbyte)(a.y * b.y));
	}

	public static ByteVector2 operator /(ByteVector2 a, ByteVector2 b)
	{
		return new ByteVector2((sbyte)(a.x / b.x), (sbyte)(a.y / b.y));
	}
}
