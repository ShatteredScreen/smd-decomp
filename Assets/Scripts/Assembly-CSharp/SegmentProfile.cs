using System;
using System.Collections.Generic;

public class SegmentProfile : ICloneable
{
	[MiniJSONAlias("shopitem")]
	public List<ShopLimitedTime> ShopItemList { get; set; }

	[MiniJSONAlias("shoplistorder")]
	public List<List<int>> ShopListOrder { get; set; }

	[MiniJSONAlias("shop_sale_banner_baseurl")]
	public string ShopSaleBannerBaseURL { get; set; }

	[MiniJSONAlias("shop_dialog_banner_baseurl")]
	public string ShopDialogBannerBaseURL { get; set; }

	[MiniJSONAlias("heart")]
	public int HeartItem { get; set; }

	[MiniJSONAlias("continueitems")]
	public List<int> ContinueItemList { get; set; }

	[MiniJSONAlias("roadblock")]
	public int RoadBlockItem { get; set; }

	[MiniJSONAlias("stageskip_flag")]
	public List<int> StageSkipFlagList { get; set; }

	[MiniJSONAlias("stageskip_losecount")]
	public int StageSkipLoseCount { get; set; }

	[MiniJSONAlias("stageskip_shopitemindex")]
	public int StageSkipShopItemIndex { get; set; }

	[MiniJSONAlias("adv_medal")]
	public int AdvMedalItem { get; set; }

	[MiniJSONAlias("adv_continue")]
	public int AdvContinueItem { get; set; }

	[MiniJSONAlias("adv_rec_settings")]
	public AdvRecSetting AdvRecSettings { get; set; }

	[MiniJSONAlias("guide_to_map_rec")]
	public int GuideToMapRec { get; set; }

	[MiniJSONAlias("guide_to_adv_settings")]
	public GuideToSetting GuideToAdvSettings { get; set; }

	[MiniJSONAlias("sales")]
	public List<CampaignSaleEntry> CampaignSaleList { get; protected set; }

	[MiniJSONAlias("websale")]
	public WebSaleInfo WebSale { get; set; }

	[MiniJSONAlias("RookiePlayHour")]
	public int RookiePlayHour { get; set; }

	[MiniJSONAlias("DropoutIntervalDays")]
	public int DropoutLoginIntervalDays { get; set; }

	[MiniJSONAlias("NoviceStripeCount")]
	public int NoviceStripeCount { get; set; }

	[MiniJSONAlias("ratioChanges1")]
	public List<RatioChange1Info> RatioChange1List { get; protected set; }

	[MiniJSONAlias("ratioChanges7")]
	public List<RatioChange7Info> RatioChange7List { get; protected set; }

	[MiniJSONAlias("transferSameUuid")]
	public int transferSameUuid { get; set; }

	[MiniJSONAlias("chance_settings")]
	public List<ChanceInfo> ChanceList { get; set; }

	[MiniJSONAlias("labyrinth_bonus")]
	public int LabyrinthBonusItemID { get; protected set; }

	public List<ShopLimitedTime> ShopSetItemList { get; protected set; }

	public List<ShopLimitedTime> ShopMugenHeartItemList { get; protected set; }

	public List<ShopLimitedTime> ShopAllCrushItemList { get; protected set; }

	[MiniJSONAlias("datekey")]
	public long DateKey { get; set; }

	public SegmentProfile()
	{
		RatioChange1List = new List<RatioChange1Info>();
		RatioChange7List = new List<RatioChange7Info>();
		ShopItemList = new List<ShopLimitedTime>();
		ShopSetItemList = new List<ShopLimitedTime>();
		ShopMugenHeartItemList = new List<ShopLimitedTime>();
		ShopAllCrushItemList = new List<ShopLimitedTime>();
		WebSale = new WebSaleInfo();
		StageSkipFlagList = new List<int>();
		StageSkipFlagList.Add(0);
		StageSkipFlagList.Add(0);
		StageSkipFlagList.Add(0);
		StageSkipLoseCount = 4;
		StageSkipShopItemIndex = 9;
		AdvRecSettings = new AdvRecSetting();
		GuideToAdvSettings = new GuideToSetting();
		ChanceList = new List<ChanceInfo>();
		ShopSaleBannerBaseURL = string.Empty;
		ShopDialogBannerBaseURL = string.Empty;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public SegmentProfile Clone()
	{
		return MemberwiseClone() as SegmentProfile;
	}

	public void ShopSetList(List<ShopLimitedTime> i)
	{
		ShopSetItemList = i;
	}

	public void ShopShopMugenHeartItemList(List<ShopLimitedTime> i)
	{
		ShopMugenHeartItemList = i;
	}

	public void SetShopAllCrushItemList(List<ShopLimitedTime> a_list)
	{
		ShopAllCrushItemList = a_list;
	}

	public CampaignSaleEntry GetIAPSale(string _identifier)
	{
		if (CampaignSaleList == null)
		{
			BIJLog.E("this.CampaignSaleList is Null");
			return null;
		}
		int i = 0;
		for (int count = CampaignSaleList.Count; i < count; i++)
		{
			if (CampaignSaleList[i].Kind == 0 && ((GameMain.USE_DEBUG_TIER && CampaignSaleList[i].Key == _identifier) || (!GameMain.USE_DEBUG_TIER && CampaignSaleList[i].KeyBNE.ToLower() == _identifier.ToLower())))
			{
				CampaignSaleList[i].ShowId = i;
				return CampaignSaleList[i];
			}
		}
		return null;
	}

	public bool IsStageSkipEnable_NormalStage()
	{
		return StageSkipFlagList[0] != 0;
	}

	public bool IsStageSkipEnable_SubStage()
	{
		return StageSkipFlagList[1] != 0;
	}

	public bool IsStageSkipEnable_EventStage()
	{
		return StageSkipFlagList[2] != 0;
	}

	public ShopLimitedTime GetShopLimitedTime(int a_shopid)
	{
		for (int i = 0; i < ShopItemList.Count; i++)
		{
			if (ShopItemList[i].ShopItemID == a_shopid)
			{
				return ShopItemList[i];
			}
		}
		return null;
	}
}
