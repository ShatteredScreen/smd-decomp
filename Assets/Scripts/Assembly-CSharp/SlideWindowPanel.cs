using System;
using System.Collections.Generic;
using UnityEngine;

public class SlideWindowPanel : MonoBehaviour
{
	private enum SW_STATE
	{
		INIT = 0,
		MAIN = 1,
		CURSOR_MOVE_INIT = 2,
		ORB_MOVE_INIT = 3,
		FLICK_MOVE_INIT = 4,
		MOVE = 5,
		WAIT = 6
	}

	public enum DIRECTION
	{
		LEFT = 0,
		RIGHT = 1
	}

	private class SlidePanelImgSetting
	{
		public int mIndex;

		public Texture2D mTextureData;
	}

	private class AdvIndexButtonInstance : MonoBehaviour
	{
		public delegate void OnIndexButtonPushed(int i);

		private OnIndexButtonPushed mCallback;

		public int index;

		public UITexture texture;

		public static AdvIndexButtonInstance make(GameObject _parent, int _index, float _posX, Texture2D _textureData, int _baseDepth, int _slideCount)
		{
			UITexture uITexture = Util.CreateJellyTextureButton("bn" + _slideCount, _parent, _textureData);
			AdvIndexButtonInstance advIndexButtonInstance = uITexture.gameObject.AddComponent<AdvIndexButtonInstance>();
			advIndexButtonInstance.texture = uITexture;
			Util.SetTextureButtonCallback(advIndexButtonInstance.texture, advIndexButtonInstance, "OnIndexButtonCallBack", UIButtonMessage.Trigger.OnClick);
			advIndexButtonInstance.texture.depth = _baseDepth;
			advIndexButtonInstance.transform.localPosition = new Vector3(_posX, 0f, 0f);
			advIndexButtonInstance.index = _index;
			return advIndexButtonInstance;
		}

		public void setCallBack(OnIndexButtonPushed _mCallback)
		{
			mCallback = _mCallback;
		}

		private void OnIndexButtonCallBack()
		{
			if (mCallback != null)
			{
				mCallback(index);
			}
		}
	}

	private class AdvOrbButtonInstance : MonoBehaviour
	{
		public delegate void OnOrbButtonPushed(int i);

		private OnOrbButtonPushed mCallback;

		public int index;

		public UISprite sprite;

		public UIButton button;

		public static AdvOrbButtonInstance make(GameObject _parent, int _index, float _posX, BIJImage _Atlas, int _baseDepth, string _ImgName)
		{
			UIButton uIButton = Util.CreateJellyImageButton("orb" + _index, _parent, _Atlas);
			AdvOrbButtonInstance advOrbButtonInstance = uIButton.gameObject.AddComponent<AdvOrbButtonInstance>();
			advOrbButtonInstance.button = uIButton;
			Util.SetImageButtonInfo(advOrbButtonInstance.button, _ImgName, _ImgName, _ImgName, _baseDepth + 3, new Vector3(_posX, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(advOrbButtonInstance.button, advOrbButtonInstance, "OnOrbButtonCallBack", UIButtonMessage.Trigger.OnClick);
			advOrbButtonInstance.sprite = advOrbButtonInstance.button.gameObject.GetComponent<UISprite>();
			advOrbButtonInstance.index = _index;
			return advOrbButtonInstance;
		}

		public void setCallBack(OnOrbButtonPushed _mCallback)
		{
			mCallback = _mCallback;
		}

		private void OnOrbButtonCallBack()
		{
			mCallback(index);
		}
	}

	public delegate void OnIndexButtonPushed(int i);

	private SW_STATE mState = SW_STATE.WAIT;

	private int mBaseDepth;

	private float mClipX;

	private float mClipY;

	public DIRECTION mSlideDirection = DIRECTION.RIGHT;

	private string mCursorImgName;

	private BIJImage mCursorAtlas;

	public float mCursorPos = 21.5f;

	private bool mUseCursor;

	private UIButton mLeftCursor;

	private UIButton mRightCursor;

	private string mOrbImgName;

	private BIJImage mOrbAtlas;

	private bool mUseOrb;

	private GameObject mOrbRoot;

	private float mOrbPosX;

	public float mOrbPos = -10f;

	private List<JellyImageButton> mOrbGoList = new List<JellyImageButton>();

	private List<SlidePanelImgSetting> mSlideImgList = new List<SlidePanelImgSetting>();

	public bool mLoopEnable;

	public bool mAutoMoveEnable;

	public int mAutoMoveSec = 6;

	private DateTime mAutoMoveStartTime;

	private OnIndexButtonPushed mCallback;

	private GameObject mListRoot;

	private float mPosX;

	private int mSlideCount;

	private bool mSlideEnable = true;

	private int mNowViewPanelId;

	private int mMovedSlidePosX;

	private int mMoveValue;

	private int mMoveDirection = 1;

	private int mMovedPanelId;

	private List<BoxCollider> mColliderList = new List<BoxCollider>();

	private bool mFlickNow;

	private float mTheLastNowPosX;

	private Vector3 mNowGoPos;

	private bool mEnableButtonNow = true;

	private int mConstDirection
	{
		get
		{
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				return 1;
			}
			return -1;
		}
	}

	public void Start()
	{
		mState = SW_STATE.INIT;
	}

	public void Update()
	{
		switch (mState)
		{
		case SW_STATE.INIT:
			CreateSlideWindow();
			mState = SW_STATE.MAIN;
			break;
		case SW_STATE.MAIN:
			SetLayout();
			CheckAutoMove();
			break;
		case SW_STATE.CURSOR_MOVE_INIT:
			mMovedSlidePosX = (int)(mListRoot.transform.localPosition.x + mClipX * (float)mMoveDirection);
			mMoveValue = (int)mClipX / 8;
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				if (mMoveDirection == -1)
				{
					mMovedPanelId = mNowViewPanelId + 1;
				}
				else
				{
					mMovedPanelId = mNowViewPanelId - 1;
				}
			}
			else if (mMoveDirection == -1)
			{
				mMovedPanelId = mNowViewPanelId - 1;
			}
			else
			{
				mMovedPanelId = mNowViewPanelId + 1;
			}
			mState = SW_STATE.MOVE;
			break;
		case SW_STATE.ORB_MOVE_INIT:
		{
			int num3;
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				if (mMovedPanelId > mNowViewPanelId)
				{
					num3 = mMovedPanelId - mNowViewPanelId;
					mMoveDirection = -1;
				}
				else
				{
					num3 = mNowViewPanelId - mMovedPanelId;
					mMoveDirection = 1;
				}
			}
			else if (mMovedPanelId > mNowViewPanelId)
			{
				num3 = mMovedPanelId - mNowViewPanelId;
				mMoveDirection = 1;
			}
			else
			{
				num3 = mNowViewPanelId - mMovedPanelId;
				mMoveDirection = -1;
			}
			mMovedSlidePosX = (int)(mClipX * (float)(mConstDirection * -1)) * mMovedPanelId;
			mMoveValue = (int)(mClipX * (float)num3) / (8 + (num3 - 1));
			mState = SW_STATE.MOVE;
			break;
		}
		case SW_STATE.FLICK_MOVE_INIT:
		{
			float num2 = mListRoot.transform.localPosition.x - mTheLastNowPosX;
			if (num2 >= 0f)
			{
				if (num2 > mClipX / 4f)
				{
					mMoveDirection = 1;
					if (mSlideDirection == DIRECTION.RIGHT)
					{
						if (mMoveDirection == -1)
						{
							mMovedPanelId = mNowViewPanelId + 1;
						}
						else
						{
							mMovedPanelId = mNowViewPanelId - 1;
						}
					}
					else if (mMoveDirection == -1)
					{
						mMovedPanelId = mNowViewPanelId - 1;
					}
					else
					{
						mMovedPanelId = mNowViewPanelId + 1;
					}
				}
				else
				{
					mMoveDirection = -1;
					mMovedPanelId = mNowViewPanelId;
				}
			}
			else if (num2 < mClipX / 4f * -1f)
			{
				mMoveDirection = -1;
				if (mSlideDirection == DIRECTION.RIGHT)
				{
					if (mMoveDirection == -1)
					{
						mMovedPanelId = mNowViewPanelId + 1;
					}
					else
					{
						mMovedPanelId = mNowViewPanelId - 1;
					}
				}
				else if (mMoveDirection == -1)
				{
					mMovedPanelId = mNowViewPanelId - 1;
				}
				else
				{
					mMovedPanelId = mNowViewPanelId + 1;
				}
			}
			else
			{
				mMoveDirection = 1;
				mMovedPanelId = mNowViewPanelId;
			}
			mMovedSlidePosX = (int)(mClipX * (float)(mConstDirection * -1)) * mMovedPanelId;
			mMoveValue = (int)mClipX / 8;
			mState = SW_STATE.MOVE;
			break;
		}
		case SW_STATE.MOVE:
		{
			if (mListRoot.transform.localPosition.x == (float)mMovedSlidePosX)
			{
				mNowViewPanelId = mMovedPanelId;
				CheckLoopStatus();
				ChangeCursorStatus();
				ChangeOrbStatus();
				if (mLoopEnable)
				{
					mAutoMoveStartTime = DateTime.Now.AddSeconds(mAutoMoveSec);
				}
				mSlideEnable = true;
				mState = SW_STATE.MAIN;
			}
			int num = (int)mListRoot.transform.localPosition.x + mMoveValue * mMoveDirection;
			if (num * mMoveDirection <= mMovedSlidePosX * mMoveDirection)
			{
				mListRoot.transform.localPosition = new Vector3(num, 0f, 0f);
			}
			else
			{
				mListRoot.transform.localPosition = new Vector3(mMovedSlidePosX, 0f, 0f);
			}
			break;
		}
		case SW_STATE.WAIT:
			break;
		}
	}

	public void Init(int _baseDepth, float _clipX, float _clipY)
	{
		mBaseDepth = _baseDepth;
		mClipX = _clipX;
		mClipY = _clipY;
	}

	public void SetCursorSettings(string _CursorImgName, BIJImage _CursorAtlas)
	{
		mCursorImgName = _CursorImgName;
		mCursorAtlas = _CursorAtlas;
		mUseCursor = true;
	}

	public void SetOrbSettings(string _OrbImgName, BIJImage _OrbAtlas)
	{
		mOrbImgName = _OrbImgName;
		mOrbAtlas = _OrbAtlas;
		mUseOrb = true;
	}

	public void setCallBack(OnIndexButtonPushed _mCallback)
	{
		mCallback = _mCallback;
	}

	public void AddSlideWindowPanel(Texture2D _textureData, int _index = -1)
	{
		SlidePanelImgSetting slidePanelImgSetting = new SlidePanelImgSetting();
		slidePanelImgSetting.mIndex = _index;
		slidePanelImgSetting.mTextureData = _textureData;
		mSlideImgList.Add(slidePanelImgSetting);
	}

	private void CreateSlideWindow()
	{
		GameObject gameObject = Util.CreateGameObject("PanelListGo", base.gameObject);
		gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
		UIPanel uIPanel = gameObject.AddComponent<UIPanel>();
		uIPanel.sortingOrder = mBaseDepth - 2;
		uIPanel.depth = mBaseDepth;
		uIPanel.clipping = UIDrawCall.Clipping.SoftClip;
		uIPanel.baseClipRegion = new Vector4(0f, 0f, mClipX, mClipY);
		uIPanel.clipSoftness = new Vector2(0f, 0f);
		mListRoot = Util.CreateGameObject("List", uIPanel.gameObject);
		mListRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
		mSlideCount = mSlideImgList.Count;
		for (int i = 0; i < mSlideCount; i++)
		{
			if (mSlideImgList[i].mIndex != -1)
			{
				AdvIndexButtonInstance advIndexButtonInstance = AdvIndexButtonInstance.make(mListRoot, mSlideImgList[i].mIndex, mPosX, mSlideImgList[i].mTextureData, mBaseDepth, i);
				advIndexButtonInstance.setCallBack(OnIndexButtonPushedCallBack);
				mPosX = advIndexButtonInstance.texture.transform.localPosition.x + (float)(mConstDirection * mSlideImgList[i].mTextureData.width);
				mColliderList.Add(advIndexButtonInstance.gameObject.GetComponent<BoxCollider>());
				continue;
			}
			GameObject gameObject2 = Util.CreateGameObject("bn" + i, mListRoot);
			gameObject2.transform.localPosition = new Vector3(mPosX, 0f, 0f);
			UITexture uITexture = gameObject2.AddComponent<UITexture>();
			uITexture.mainTexture = mSlideImgList[i].mTextureData;
			uITexture.width = mSlideImgList[i].mTextureData.width;
			uITexture.height = mSlideImgList[i].mTextureData.height;
			uITexture.depth = mBaseDepth;
			mPosX = gameObject2.transform.localPosition.x + (float)(mConstDirection * uITexture.width);
		}
		if (mSlideCount <= 1)
		{
			mLoopEnable = false;
			mUseCursor = false;
			mAutoMoveEnable = false;
			mUseOrb = false;
		}
		else
		{
			FlickWindow flickWindow = base.gameObject.AddComponent<FlickWindow>();
			mNowGoPos = Camera.main.WorldToScreenPoint(base.gameObject.transform.position);
			float _posX;
			float _posY;
			GetObjectScreenSize(new Vector3(mClipX, mClipY), out _posX, out _posY);
			flickWindow.Init(mNowGoPos.x - _posX / 2f, mNowGoPos.y - _posY / 2f, _posX, _posY);
			flickWindow.SetFlickStartCallBack(GetFlickPos);
			flickWindow.SetFlickEndCallBack(EndFlick);
		}
		if (mLoopEnable)
		{
			if (mSlideImgList[0].mIndex != -1)
			{
				AdvIndexButtonInstance advIndexButtonInstance2 = AdvIndexButtonInstance.make(mListRoot, mSlideImgList[0].mIndex, mPosX, mSlideImgList[0].mTextureData, mBaseDepth, mSlideCount);
				advIndexButtonInstance2.setCallBack(OnIndexButtonPushedCallBack);
				mColliderList.Add(advIndexButtonInstance2.gameObject.GetComponent<BoxCollider>());
			}
			else
			{
				GameObject gameObject3 = Util.CreateGameObject("bn" + mSlideCount, mListRoot);
				gameObject3.transform.localPosition = new Vector3(mPosX, 0f, 0f);
				UITexture uITexture2 = gameObject3.AddComponent<UITexture>();
				uITexture2.mainTexture = mSlideImgList[0].mTextureData;
				uITexture2.width = mSlideImgList[0].mTextureData.width;
				uITexture2.height = mSlideImgList[0].mTextureData.height;
				uITexture2.depth = mBaseDepth;
			}
			if (mSlideImgList[mSlideCount - 1].mIndex != -1)
			{
				AdvIndexButtonInstance advIndexButtonInstance3 = AdvIndexButtonInstance.make(mListRoot, mSlideImgList[mSlideCount - 1].mIndex, -(mConstDirection * mSlideImgList[mSlideCount - 1].mTextureData.width), mSlideImgList[mSlideCount - 1].mTextureData, mBaseDepth, mSlideCount + 1);
				advIndexButtonInstance3.setCallBack(OnIndexButtonPushedCallBack);
			}
			else
			{
				GameObject gameObject4 = Util.CreateGameObject("bn" + (mSlideCount + 1), mListRoot);
				gameObject4.transform.localPosition = new Vector3(-(mConstDirection * mSlideImgList[mSlideCount - 1].mTextureData.width), 0f, 0f);
				UITexture uITexture3 = gameObject4.AddComponent<UITexture>();
				uITexture3.mainTexture = mSlideImgList[mSlideCount - 1].mTextureData;
				uITexture3.width = mSlideImgList[mSlideCount - 1].mTextureData.width;
				uITexture3.height = mSlideImgList[mSlideCount - 1].mTextureData.height;
				uITexture3.depth = mBaseDepth;
			}
		}
		else
		{
			mAutoMoveEnable = false;
		}
		if (mUseCursor)
		{
			mLeftCursor = Util.CreateJellyImageButton("LeftCursor", base.gameObject, mCursorAtlas);
			Util.SetImageButtonInfo(mLeftCursor, mCursorImgName, mCursorImgName, mCursorImgName, mBaseDepth + 30, new Vector3(0f - mCursorPos, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mLeftCursor, this, "OnLeft_Callback", UIButtonMessage.Trigger.OnClick);
			mLeftCursor.transform.localPosition = new Vector3(mLeftCursor.transform.localPosition.x - mClipX / 2f, mLeftCursor.transform.localPosition.y, mLeftCursor.transform.localPosition.z);
			mColliderList.Add(mLeftCursor.gameObject.GetComponent<BoxCollider>());
			mRightCursor = Util.CreateJellyImageButton("RightCursor", base.gameObject, mCursorAtlas);
			Util.SetImageButtonInfo(mRightCursor, mCursorImgName, mCursorImgName, mCursorImgName, mBaseDepth + 30, new Vector3(mCursorPos, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mRightCursor, this, "OnRight_Callback", UIButtonMessage.Trigger.OnClick);
			mRightCursor.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
			mRightCursor.transform.localPosition = new Vector3(mRightCursor.transform.localPosition.x + mClipX / 2f, mRightCursor.transform.localPosition.y, mRightCursor.transform.localPosition.z);
			mColliderList.Add(mRightCursor.gameObject.GetComponent<BoxCollider>());
			ChangeCursorStatus();
		}
		if (mUseOrb)
		{
			mOrbRoot = Util.CreateGameObject("OrbList", base.gameObject);
			mOrbRoot.transform.localPosition = new Vector3(0f, mOrbPos - mClipY / 2f, 0f);
			float x = 0f;
			for (int j = 0; j < mSlideCount; j++)
			{
				AdvOrbButtonInstance advOrbButtonInstance = AdvOrbButtonInstance.make(mOrbRoot, j, mOrbPosX, mOrbAtlas, mBaseDepth, mOrbImgName);
				advOrbButtonInstance.setCallBack(OnOrbButtonPushedCallBack);
				mOrbGoList.Add(advOrbButtonInstance.button.GetComponent<JellyImageButton>());
				mColliderList.Add(advOrbButtonInstance.button.gameObject.GetComponent<BoxCollider>());
				mOrbPosX += (float)mConstDirection * ((float)advOrbButtonInstance.sprite.width * 1.5f);
				mOrbRoot.transform.localPosition = new Vector3(x, mOrbRoot.transform.localPosition.y, 0f);
				x = mOrbRoot.transform.localPosition.x - (float)mConstDirection * ((float)advOrbButtonInstance.sprite.width * 0.75f);
			}
			ChangeOrbStatus();
		}
		if (mLoopEnable)
		{
			mAutoMoveStartTime = DateTime.Now.AddSeconds(mAutoMoveSec);
		}
	}

	private void ChangeCursorStatus()
	{
		if (!mUseCursor || mLoopEnable)
		{
			return;
		}
		NGUITools.SetActive(mLeftCursor.gameObject, true);
		NGUITools.SetActive(mRightCursor.gameObject, true);
		if (mNowViewPanelId == 0)
		{
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				NGUITools.SetActive(mLeftCursor.gameObject, false);
			}
			else
			{
				NGUITools.SetActive(mRightCursor.gameObject, false);
			}
		}
		else if (mNowViewPanelId == mSlideCount - 1)
		{
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				NGUITools.SetActive(mRightCursor.gameObject, false);
			}
			else
			{
				NGUITools.SetActive(mLeftCursor.gameObject, false);
			}
		}
	}

	private void ChangeOrbStatus()
	{
		if (!mUseOrb)
		{
			return;
		}
		for (int i = 0; i < mOrbGoList.Count; i++)
		{
			if (i == mNowViewPanelId)
			{
				mOrbGoList[i].SetButtonEnable(true);
			}
			else
			{
				mOrbGoList[i].SetButtonEnable(false);
			}
		}
	}

	private void CheckLoopStatus()
	{
		if (mLoopEnable)
		{
			if (mNowViewPanelId == -1)
			{
				mNowViewPanelId = mSlideCount - 1;
				mMovedSlidePosX = (int)(mClipX * (float)(mConstDirection * -1)) * mNowViewPanelId;
			}
			else if (mNowViewPanelId == mSlideCount)
			{
				mNowViewPanelId = 0;
				mMovedSlidePosX = mNowViewPanelId;
			}
		}
	}

	private void CheckAutoMove()
	{
		if (!mAutoMoveEnable)
		{
			return;
		}
		if (!mEnableButtonNow)
		{
			mAutoMoveStartTime = DateTime.Now.AddSeconds(mAutoMoveSec);
		}
		else if (DateTimeUtil.BetweenSeconds(DateTime.Now, mAutoMoveStartTime) <= 0)
		{
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				OnRight_Callback();
			}
			else
			{
				OnLeft_Callback();
			}
		}
	}

	private void OnIndexButtonPushedCallBack(int index)
	{
		if (mCallback != null)
		{
			mSlideEnable = true;
			FlickWindow component = base.gameObject.GetComponent<FlickWindow>();
			if (component != null)
			{
				mFlickNow = false;
				component.FlickClear();
			}
			mCallback(index);
		}
	}

	private void OnOrbButtonPushedCallBack(int index)
	{
		if (mSlideEnable && mNowViewPanelId != index)
		{
			mSlideEnable = false;
			mMovedPanelId = index;
			mState = SW_STATE.ORB_MOVE_INIT;
		}
	}

	private void OnRight_Callback()
	{
		if (mSlideEnable)
		{
			mSlideEnable = false;
			mMoveDirection = -1;
			mState = SW_STATE.CURSOR_MOVE_INIT;
		}
	}

	private void OnLeft_Callback()
	{
		if (mSlideEnable)
		{
			mSlideEnable = false;
			mMoveDirection = 1;
			mState = SW_STATE.CURSOR_MOVE_INIT;
		}
	}

	public void SetEnableButtonAction(bool buttonflg)
	{
		if (mFlickNow)
		{
			EndFlick();
			FlickWindow component = base.gameObject.GetComponent<FlickWindow>();
			if (component != null)
			{
				component.FlickClear();
			}
		}
		for (int i = 0; i < mColliderList.Count; i++)
		{
			mColliderList[i].enabled = buttonflg;
		}
		mEnableButtonNow = buttonflg;
	}

	private void GetFlickPos(float _directionX, float _directionY)
	{
		if (mEnableButtonNow && (mSlideEnable || mFlickNow))
		{
			if (!mFlickNow)
			{
				mFlickNow = true;
				mSlideEnable = false;
				mTheLastNowPosX = mListRoot.transform.localPosition.x;
			}
			if (_directionX <= mClipX - 1f && _directionX >= (mClipX - 1f) * -1f)
			{
				float x = mTheLastNowPosX + _directionX;
				mListRoot.transform.localPosition = new Vector3(x, 0f, 0f);
			}
		}
	}

	private void EndFlick()
	{
		if (mEnableButtonNow)
		{
			mFlickNow = false;
			mState = SW_STATE.FLICK_MOVE_INIT;
		}
	}

	private void SetLayout()
	{
		FlickWindow component = base.gameObject.GetComponent<FlickWindow>();
		if (component != null && !mFlickNow)
		{
			Vector3 vector = Camera.main.WorldToScreenPoint(base.gameObject.transform.position);
			if (mNowGoPos != vector)
			{
				float _posX;
				float _posY;
				GetObjectScreenSize(new Vector3(mClipX, mClipY), out _posX, out _posY);
				component.UpdateInitPos(vector.x - _posX / 2f, vector.y - _posY / 2f, _posX, _posY);
				mNowGoPos = vector;
			}
		}
	}

	private void GetObjectScreenSize(Vector3 pos, out float _posX, out float _posY)
	{
		GameObject gameObject = Util.CreateGameObject("getsize", base.gameObject);
		gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f));
		gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x + pos.x, gameObject.transform.localPosition.y + pos.y);
		_posX = Camera.main.WorldToScreenPoint(gameObject.transform.position).x;
		_posY = Camera.main.WorldToScreenPoint(gameObject.transform.position).y;
		GameMain.SafeDestroy(gameObject);
	}
}
