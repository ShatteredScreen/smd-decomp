using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class LazerRenderer : MonoBehaviour
{
	public GameMain mGame;

	private short[] mVertices;

	public BIJImage mAtlas;

	private MeshFilter mMeshFilter;

	private MeshRenderer mMeshRenderer;

	private Mesh mMesh;

	private Material mMaterial;

	private List<LazerInstanceBase> mLazerList;

	private float mDeltaTime;

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_TILE");
		mAtlas = resImage.Image;
		mMeshFilter = GetComponent<MeshFilter>();
		mMeshRenderer = GetComponent<MeshRenderer>();
		mMesh = new Mesh();
		mMaterial = new Material(GameMain.DefaultShader);
		mMeshFilter.sharedMesh = mMesh;
		mMeshRenderer.sharedMaterial = mMaterial;
		mMeshRenderer.sharedMaterial.mainTexture = mAtlas.Texture;
		mMeshRenderer.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRenderer.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		mLazerList = new List<LazerInstanceBase>();
		mDeltaTime = 0f;
	}

	private void OnDestroy()
	{
		GameMain.SafeDestroy(mMeshFilter.sharedMesh);
		mMeshFilter.sharedMesh = null;
		mMesh = null;
		GameMain.SafeDestroy(mMeshRenderer.sharedMaterial);
		mMeshRenderer.sharedMaterial = null;
		mMaterial = null;
		mMeshFilter = null;
		mMeshRenderer = null;
		mAtlas = null;
	}

	private void UpdateArray()
	{
		mMesh.uv = null;
		mMesh.triangles = null;
		mMesh.colors32 = null;
		mMesh.vertices = null;
		BIJNGUIImage bIJNGUIImage = (BIJNGUIImage)mAtlas;
		float z = base.gameObject.transform.localPosition.z;
		int num = 0;
		for (int i = 0; i < mLazerList.Count; i++)
		{
			num += mLazerList[i].GetPartsNum();
		}
		if (num == 0)
		{
			return;
		}
		Vector3[] array = new Vector3[num * 4];
		Vector2[] array2 = new Vector2[num * 4];
		int[] array3 = new int[num * 6];
		Color32[] array4 = new Color32[num * 4];
		int num2 = 0;
		for (int j = 0; j < mLazerList.Count; j++)
		{
			string mSpriteName = mLazerList[j].mSpriteName;
			Vector2 offset = bIJNGUIImage.GetOffset(mSpriteName);
			Vector2 size = bIJNGUIImage.GetSize(mSpriteName);
			Vector2[] array5 = new Vector2[4];
			int partsNum = mLazerList[j].GetPartsNum();
			float num3 = size.y / (float)partsNum;
			float num4 = 0f;
			for (int k = 0; k < partsNum; k++)
			{
				int num5 = (mLazerList[j].mTrailPtr - mLazerList[j].mLength + k + mLazerList[j].mMaxLength) % mLazerList[j].mMaxLength;
				int num6 = (num5 + 1) % mLazerList[j].mMaxLength;
				Vector2 vector = mLazerList[j].mTrail[num5];
				Vector2 vector2 = mLazerList[j].mTrail[num6];
				float f = Mathf.Atan2(vector2.y - vector.y, vector2.x - vector.x);
				float mWidth = mLazerList[j].mWidth;
				if (k == 0)
				{
					array5[0] = new Vector2(Mathf.Sin(f) * (0f - mWidth), Mathf.Cos(f) * mWidth) + vector;
					array5[1] = new Vector2(Mathf.Sin(f) * mWidth, Mathf.Cos(f) * (0f - mWidth)) + vector;
				}
				else
				{
					array5[0] = array5[2];
					array5[1] = array5[3];
				}
				array5[2] = new Vector2(Mathf.Sin(f) * (0f - mWidth), Mathf.Cos(f) * mWidth) + vector2;
				array5[3] = new Vector2(Mathf.Sin(f) * mWidth, Mathf.Cos(f) * (0f - mWidth)) + vector2;
				array[num2 * 4] = new Vector3(array5[0].x, array5[0].y, z);
				array[num2 * 4 + 1] = new Vector3(array5[1].x, array5[1].y, z);
				array[num2 * 4 + 2] = new Vector3(array5[2].x, array5[2].y, z);
				array[num2 * 4 + 3] = new Vector3(array5[3].x, array5[3].y, z);
				float x = offset.x;
				float y = offset.y + num4;
				float x2 = offset.x + size.x;
				num4 += num3;
				float y2 = offset.y + num4;
				array2[num2 * 4] = new Vector2(x, y);
				array2[num2 * 4 + 1] = new Vector2(x2, y);
				array2[num2 * 4 + 2] = new Vector2(x, y2);
				array2[num2 * 4 + 3] = new Vector2(x2, y2);
				array3[num2 * 6] = num2 * 4 + 2;
				array3[num2 * 6 + 1] = num2 * 4 + 1;
				array3[num2 * 6 + 2] = num2 * 4;
				array3[num2 * 6 + 3] = num2 * 4 + 2;
				array3[num2 * 6 + 4] = num2 * 4 + 3;
				array3[num2 * 6 + 5] = num2 * 4 + 1;
				array4[num2 * 4] = Color.white;
				array4[num2 * 4 + 1] = Color.white;
				array4[num2 * 4 + 2] = Color.white;
				array4[num2 * 4 + 3] = Color.white;
				num2++;
			}
		}
		mMesh.vertices = array;
		mMesh.uv = array2;
		mMesh.triangles = array3;
		mMesh.colors32 = array4;
	}

	private void Start()
	{
	}

	private void Update()
	{
		mDeltaTime += Time.deltaTime;
		while (mDeltaTime > 1f / 30f)
		{
			int count = mLazerList.Count;
			for (int num = count - 1; num >= 0; num--)
			{
				mLazerList[num].Update(1f / 30f);
				if (mLazerList[num].mFinished)
				{
					mLazerList.RemoveAt(num);
				}
			}
			UpdateArray();
			mDeltaTime -= 1f / 30f;
		}
	}

	public void AddLazer(LazerInstanceBase lazer)
	{
		mLazerList.Add(lazer);
	}
}
