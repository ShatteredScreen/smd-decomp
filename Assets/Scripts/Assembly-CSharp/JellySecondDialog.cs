using System;
using UnityEngine;

public class JellySecondDialog : JellyDialog
{
	private enum STATE
	{
		INIT = 0,
		OPEN = 1,
		CLOSE = 2,
		WAIT = 3
	}

	private STATE mState;

	protected Vector2 mLogScreen;

	private float mStartPositionY;

	private float mEndPositionY;

	private float mOpenTime = 1f;

	private float mCloseTime = 1f;

	protected override void Awake()
	{
		mTarget = base.gameObject;
		mDelay = 0f;
		Reset();
		mLogScreen = Util.LogScreenSize();
	}

	protected override void Start()
	{
	}

	public virtual void SetInitialPosition(float a_startPos = 0f, float a_endPos = 0f, float a_openTime = 1f, float a_closeTime = 1f)
	{
		mStartPositionY = a_startPos;
		mEndPositionY = a_endPos;
		mOpenTime = a_openTime;
		mCloseTime = a_closeTime;
	}

	protected override void Update()
	{
		if (mDelay > 0f)
		{
			mDelay -= Time.deltaTime;
			return;
		}
		float num = -100000f;
		switch (mState)
		{
		case STATE.INIT:
			mMoveFinished = false;
			mTarget.transform.localScale = new Vector3(mTargetScale, mTargetScale, 1f);
			mState = STATE.WAIT;
			break;
		case STATE.OPEN:
			mAngle += Time.deltaTime * 90f / mOpenTime;
			if (mOpenImmediate)
			{
				mAngle = 90f;
			}
			if (mAngle >= 90f)
			{
				mAngle = 90f;
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			num = mStartPositionY + Mathf.Sin(mAngle * ((float)Math.PI / 180f)) * (mEndPositionY - mStartPositionY);
			mTarget.transform.localPosition = new Vector3(0f, num, 0f);
			break;
		case STATE.CLOSE:
			mAngle += Time.deltaTime * 90f / mCloseTime;
			if (mAngle >= 90f)
			{
				mAngle = 90f;
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			num = mEndPositionY + Mathf.Sin(mAngle * ((float)Math.PI / 180f)) * (mStartPositionY - mEndPositionY);
			mTarget.transform.localPosition = new Vector3(0f, num, 0f);
			break;
		case STATE.WAIT:
			break;
		}
	}

	public override void Reset()
	{
		mState = STATE.INIT;
	}

	public override void Open(bool immediate = false)
	{
		mAngle = 0f;
		mOpenImmediate = immediate;
		mMoveFinished = false;
		mState = STATE.OPEN;
	}

	public override void Close(bool immediate = false)
	{
		mCloseImmediate = immediate;
		mAngle = 0f;
		mMoveFinished = false;
		mState = STATE.CLOSE;
	}

	public override bool IsMoveFinished()
	{
		return mMoveFinished;
	}

	public override void SetMoveFinished(bool flag)
	{
		mMoveFinished = flag;
	}

	public override void SetTarget(GameObject target)
	{
		mTarget = target;
	}

	public override void SetDelay(float delayTime)
	{
		mDelay = delayTime;
	}
}
