using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using UnityEngine;

public class ServerIAP : MonoBehaviour
{
	public enum API
	{
		MCCharge = 0,
		MCSpend = 1,
		MCCheck = 2,
		SCCheck = 3,
		SCCharge = 4,
		BirthdaySet = 5,
		BirthdayGet = 6,
		MCChargeCheck = 7,
		SCRewardCheck = 8,
		SCRewardset = 9,
		DebugSCCharge = 10,
		CampaignCheck = 11,
		CampaignCharge = 12,
		DebugCampaignUnset = 13
	}

	public enum BuyGemPlace
	{
		NONE = -1,
		MAP = 0,
		HEART_MAP = 1,
		SALE_MAP = 2,
		SALE_EVENTMAP = 3,
		ITEM_SHOP = 4,
		STAGEINFO = 5,
		ITEM_STAGE = 6,
		CONTINUE_STAGE = 7,
		HEART_STAGE = 8,
		TRANSACTION = 9,
		EVENTMAP = 10,
		HEART_EVENTMAP = 11,
		SHOP = 12,
		RB_MAP = 13,
		RB_EVENTMAP = 14,
		LINKBANNER_SALEMAP = 15,
		ADV_STAMINA = 16,
		ADV_STAMINA_STAGE = 17,
		ADV_COKPIT = 18,
		ADV_CONTINUE_STAGE = 19,
		ADV_GACHA = 20,
		BIGINNER_DIALOG = 21,
		BINGO_RELOTTERY_BUY_HEART = 22,
		OPEN_OLDEVENT_IN_GAMESTATE = 23,
		CONTINUE_OLDEVENT_IN_MAP = 24,
		CONTINUE_OLDEVENT_IN_EVENT = 25,
		CONTINUE_OLDEVENT_IN_GAMESTATE = 26
	}

	private class IAPRequest
	{
		public API _api;

		public HttpAsync _httpAsync;

		public HttpAsync.ResponseHandler _responseHandler;

		public Action<int, object> _handler;

		public object _userdata;

		public DateTime _time;

		public int _serverResult;
	}

	private const int POST_MAX = 10;

	private static ThreadSafeDictionary<API, bool> mConnectFlags;

	private static object mInstanceLock;

	private static ServerIAP mInstance;

	private static readonly DateTime _BASE_TIME;

	private Queue<IAPRequest> mRequests = new Queue<IAPRequest>();

	private readonly object mRequestsLock = new object();

	private ManualResetEvent mRequestsNotify = new ManualResetEvent(false);

	private Thread mThread;

	private ManualResetEvent mThreadLock;

	private volatile bool mIsRunning;

	private Queue<IAPRequest> mResponses = new Queue<IAPRequest>();

	private readonly object mResponsesLock = new object();

	public static bool IsNetworking
	{
		get
		{
			return ManualConnecting || IsConnecting(API.MCCharge) || IsConnecting(API.MCSpend) || IsConnecting(API.MCCheck) || IsConnecting(API.SCCheck) || IsConnecting(API.SCCharge) || IsConnecting(API.BirthdaySet) || IsConnecting(API.BirthdayGet) || IsConnecting(API.MCChargeCheck) || IsConnecting(API.CampaignCheck) || IsConnecting(API.CampaignCharge) || IsConnecting(API.DebugCampaignUnset);
		}
	}

	private static string APIVersion
	{
		get
		{
			return "v3";
		}
	}

	public static ServerIAP Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					mInstance = (ServerIAP)gameObject.GetComponent(typeof(ServerIAP));
				}
				return mInstance;
			}
		}
	}

	public static string BaseURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.IAP_URL);
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppName);
			return stringBuilder.ToString();
		}
	}

	public static int Timeout { get; set; }

	public static bool ManualConnecting { get; set; }

	public static bool UseEncryption { get; set; }

	private static bool IsSendable
	{
		get
		{
			if (InternetReachability.Status == NetworkReachability.NotReachable)
			{
				return false;
			}
			return true;
		}
	}

	[method: MethodImpl(32)]
	public static event Action<int, object> BirthdaySetEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> BirthdayGetEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> ChargeCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> CampaignCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> CampaignChargeEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> DebugCampaignUnsetEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> CurrencyChargeEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> CurrencySpendEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> CurrencyCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> SubcurrencyCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> SubcurrencyChargeEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> SubcurrencyRewardCheckEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> SubcurrencyRewardSetEvent;

	[method: MethodImpl(32)]
	public static event Action<int, object> DebugSubcurrencyChargeEvent;

	static ServerIAP()
	{
		mInstanceLock = new object();
		mInstance = null;
		_BASE_TIME = new DateTime(2014, 1, 1);
		mConnectFlags = new ThreadSafeDictionary<API, bool>();
		Timeout = 5000;
		ManualConnecting = false;
		UseEncryption = true;
	}

	private static bool IsConnecting(API api)
	{
		bool value = false;
		if (!mConnectFlags.TryGetValue(api, out value))
		{
			return false;
		}
		return value;
	}

	private static void SetConnecting(API api, bool connecting)
	{
		mConnectFlags[api] = connecting;
	}

	public static bool BirthdaySet(DateTime birthday)
	{
		return BirthdaySet(birthday.ToString("yyyyMMdd"));
	}

	public static bool BirthdaySet(string birthday)
	{
		BirthdaySetRequest birthdaySetRequest = new BirthdaySetRequest();
		birthdaySetRequest.udid = Network.BNIDPublishID;
		birthdaySetRequest.uniq_id = Network.BNIDAuthID;
		birthdaySetRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		birthdaySetRequest.birthday = birthday;
		birthdaySetRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<BirthdaySetRequest, BirthdaySetResponse>(API.BirthdaySet, "/user/birthday/register", birthdaySetRequest, ServerIAP.BirthdaySetEvent);
	}

	public static bool BirthdayGet()
	{
		BirthdayGetRequest birthdayGetRequest = new BirthdayGetRequest();
		birthdayGetRequest.udid = Network.BNIDPublishID;
		birthdayGetRequest.uniq_id = Network.BNIDAuthID;
		birthdayGetRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		birthdayGetRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<BirthdayGetRequest, BirthdayGetResponse>(API.BirthdayGet, "/user/birthday", birthdayGetRequest, ServerIAP.BirthdayGetEvent);
	}

	public static bool ChargeCheck(int iapid)
	{
		ChargeCheckRequest chargeCheckRequest = new ChargeCheckRequest();
		chargeCheckRequest.udid = Network.BNIDPublishID;
		chargeCheckRequest.uniq_id = Network.BNIDAuthID;
		chargeCheckRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		chargeCheckRequest.iapid = iapid;
		chargeCheckRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<ChargeCheckRequest, ChargeCheckResponse>(API.MCChargeCheck, "/currency/charge/check", chargeCheckRequest, ServerIAP.ChargeCheckEvent);
	}

	public static bool CampaignCheck(bool aTutorial, bool aDebug)
	{
		CampaignCheckRequest campaignCheckRequest = new CampaignCheckRequest();
		campaignCheckRequest.udid = Network.BNIDPublishID;
		campaignCheckRequest.uniq_id = Network.BNIDAuthID;
		campaignCheckRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		campaignCheckRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		if (aTutorial)
		{
			campaignCheckRequest.is_tutorial_clear = 1;
		}
		else
		{
			campaignCheckRequest.is_tutorial_clear = 0;
		}
		if (aDebug)
		{
			campaignCheckRequest.is_debug = 1;
		}
		else
		{
			campaignCheckRequest.is_debug = 0;
		}
		return Communicate<CampaignCheckRequest, CampaignCheckResponse>(API.CampaignCheck, "/currency/campaign/check", campaignCheckRequest, ServerIAP.CampaignCheckEvent);
	}

	public static bool CampaignCharge(int aId, long aPurchaseTime)
	{
		CampaignChargeRequest campaignChargeRequest = new CampaignChargeRequest();
		campaignChargeRequest.udid = Network.BNIDPublishID;
		campaignChargeRequest.uniq_id = Network.BNIDAuthID;
		campaignChargeRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		campaignChargeRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		campaignChargeRequest.iap_campaign_id = aId;
		campaignChargeRequest.purchase_time = aPurchaseTime;
		return Communicate<CampaignChargeRequest, CampaignChargeResponse>(API.CampaignCharge, "/currency/campaign/charge", campaignChargeRequest, ServerIAP.CampaignChargeEvent);
	}

	public static bool DebugCampaignUnset()
	{
		DebugCampaignUnsetRequest debugCampaignUnsetRequest = new DebugCampaignUnsetRequest();
		debugCampaignUnsetRequest.udid = Network.BNIDPublishID;
		debugCampaignUnsetRequest.uniq_id = Network.BNIDAuthID;
		debugCampaignUnsetRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		debugCampaignUnsetRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<DebugCampaignUnsetRequest, DebugCampaignUnsetResponse>(API.DebugCampaignUnset, "/currency/campaign/unset", debugCampaignUnsetRequest, ServerIAP.DebugCampaignUnsetEvent);
	}

	public static bool CurrencyCharge(int iapid, string data, string signature, int a_place)
	{
		MCReceipt mCReceipt = new MCReceipt();
		mCReceipt.data = data;
		mCReceipt.signature = signature;
		return CurrencyCharge(iapid, mCReceipt, a_place);
	}

	public static bool CurrencyCharge(int iapid, MCReceipt receipt, int a_place)
	{
		MCChargeRequest mCChargeRequest = new MCChargeRequest();
		mCChargeRequest.udid = Network.BNIDPublishID;
		mCChargeRequest.uniq_id = Network.BNIDAuthID;
		mCChargeRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		mCChargeRequest.series = SingletonMonoBehaviour<Network>.Instance.Series;
		mCChargeRequest.lvl = SingletonMonoBehaviour<Network>.Instance.Level;
		mCChargeRequest.mc = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		mCChargeRequest.sc = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		mCChargeRequest.tgt_series = SingletonMonoBehaviour<Network>.Instance.LastSeries;
		mCChargeRequest.tgt_stage = SingletonMonoBehaviour<Network>.Instance.LastStageNo;
		mCChargeRequest.api_version = APIVersion;
		mCChargeRequest.iapid = iapid;
		mCChargeRequest.price_currency_code = PurchaseManager.instance.GetCurrencyCode(iapid);
		mCChargeRequest.price = PurchaseManager.instance.GetPrice(iapid);
		mCChargeRequest.formatted_price = PurchaseManager.instance.GetPriceText(iapid);
		mCChargeRequest.receipt = receipt;
		mCChargeRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		mCChargeRequest.place = a_place;
		return Communicate<MCChargeRequest, MCChargeResponse>(API.MCCharge, "/currency/charge", mCChargeRequest, ServerIAP.CurrencyChargeEvent);
	}

	public static bool CurrencySpend(int itemid, bool retry)
	{
		MCSpendRequest mCSpendRequest = new MCSpendRequest();
		mCSpendRequest.udid = Network.BNIDPublishID;
		mCSpendRequest.uniq_id = Network.BNIDAuthID;
		mCSpendRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		mCSpendRequest.series = SingletonMonoBehaviour<Network>.Instance.Series;
		mCSpendRequest.lvl = SingletonMonoBehaviour<Network>.Instance.Level;
		mCSpendRequest.mc = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		mCSpendRequest.sc = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		mCSpendRequest.tgt_series = SingletonMonoBehaviour<Network>.Instance.LastSeries;
		mCSpendRequest.tgt_stage = SingletonMonoBehaviour<Network>.Instance.LastStageNo;
		mCSpendRequest.itemid = itemid;
		mCSpendRequest.retry = retry;
		mCSpendRequest.spend_time = SingletonMonoBehaviour<Network>.Instance.ServerTime;
		mCSpendRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<MCSpendRequest, MCSpendResponse>(API.MCSpend, "/currency/spend", mCSpendRequest, ServerIAP.CurrencySpendEvent);
	}

	public static bool ChargeCheck()
	{
		MCCheckRequest mCCheckRequest = new MCCheckRequest();
		mCCheckRequest.udid = Network.BNIDPublishID;
		mCCheckRequest.uniq_id = Network.BNIDAuthID;
		mCCheckRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		mCCheckRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<MCCheckRequest, MCCheckResponse>(API.MCCheck, "/currency/check", mCCheckRequest, ServerIAP.CurrencyCheckEvent);
	}

	public static bool SubcurrencyCheck()
	{
		SCCheckRequest sCCheckRequest = new SCCheckRequest();
		sCCheckRequest.udid = Network.BNIDPublishID;
		sCCheckRequest.uniq_id = Network.BNIDAuthID;
		sCCheckRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		sCCheckRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<SCCheckRequest, SCCheckResponse>(API.SCCheck, "/sub-currency/check", sCCheckRequest, ServerIAP.SubcurrencyCheckEvent);
	}

	public static bool SubcurrencyCharge(params int[] target_ids)
	{
		List<SCTarget> list = new List<SCTarget>();
		if (target_ids != null && target_ids.Length > 0)
		{
			for (int i = 0; i < target_ids.Length; i++)
			{
				SCTarget sCTarget = new SCTarget();
				sCTarget.id = target_ids[i];
				list.Add(sCTarget);
			}
		}
		return SubcurrencyCharge(list);
	}

	public static bool SubcurrencyCharge(List<SCTarget> targets)
	{
		SCChargeRequest sCChargeRequest = new SCChargeRequest();
		sCChargeRequest.udid = Network.BNIDPublishID;
		sCChargeRequest.uniq_id = Network.BNIDAuthID;
		sCChargeRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		sCChargeRequest.series = SingletonMonoBehaviour<Network>.Instance.Series;
		sCChargeRequest.lvl = SingletonMonoBehaviour<Network>.Instance.Level;
		sCChargeRequest.mc = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		sCChargeRequest.sc = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		sCChargeRequest.tgt_series = SingletonMonoBehaviour<Network>.Instance.LastSeries;
		sCChargeRequest.tgt_stage = SingletonMonoBehaviour<Network>.Instance.LastStageNo;
		sCChargeRequest.targets = targets;
		sCChargeRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<SCChargeRequest, SCChargeResponse>(API.SCCharge, "/sub-currency/charge", sCChargeRequest, ServerIAP.SubcurrencyChargeEvent);
	}

	public static bool SubcurrencyRewardCheck(int a_accessoryID)
	{
		SCRewardCheckRequest sCRewardCheckRequest = new SCRewardCheckRequest();
		sCRewardCheckRequest.udid = Network.BNIDPublishID;
		sCRewardCheckRequest.uniq_id = Network.BNIDAuthID;
		sCRewardCheckRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		sCRewardCheckRequest.rewardid = a_accessoryID;
		sCRewardCheckRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<SCRewardCheckRequest, SCRewardCheckResponse>(API.SCRewardCheck, "/sub-currency/reward/check", sCRewardCheckRequest, ServerIAP.SubcurrencyRewardCheckEvent);
	}

	public static bool SubcurrencyRewardSet(int a_accessoryID)
	{
		SCRewardSetRequest sCRewardSetRequest = new SCRewardSetRequest();
		sCRewardSetRequest.udid = Network.BNIDPublishID;
		sCRewardSetRequest.uniq_id = Network.BNIDAuthID;
		sCRewardSetRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		sCRewardSetRequest.rewardid = a_accessoryID;
		sCRewardSetRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<SCRewardSetRequest, SCRewardSetResponse>(API.SCRewardset, "/sub-currency/reward/set", sCRewardSetRequest, ServerIAP.SubcurrencyRewardSetEvent);
	}

	public static bool DebugSubCurrencyCharge(IGiftItem gift)
	{
		SCDebugSetRequest sCDebugSetRequest = new SCDebugSetRequest();
		sCDebugSetRequest.udid = Network.BNIDPublishID;
		sCDebugSetRequest.uniq_id = Network.BNIDAuthID;
		sCDebugSetRequest.uuid = SingletonMonoBehaviour<Network>.Instance.UUID;
		sCDebugSetRequest.category = gift.ItemCategory;
		sCDebugSetRequest.sub_category = gift.ItemKind;
		sCDebugSetRequest.itemid = gift.ItemID;
		sCDebugSetRequest.quantity = gift.Quantity;
		sCDebugSetRequest.bhiveid = SingletonMonoBehaviour<Network>.Instance.HiveId;
		return Communicate<SCDebugSetRequest, SCDebugSetResponse>(API.DebugSCCharge, "/sub-currency/set", sCDebugSetRequest, ServerIAP.DebugSubcurrencyChargeEvent);
	}

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
		try
		{
			long num = time();
		}
		catch (Exception)
		{
		}
	}

	private static void dbg(string msg)
	{
	}

	public static string ToSS(object o)
	{
		if (o == null)
		{
			return string.Empty;
		}
		return o.ToString();
	}

	public static string ResultString(int result)
	{
		switch (result)
		{
		case 0:
			return "SUCCESS";
		case 1:
			return "ERROR";
		case 4:
			return "CANCELED";
		default:
			return "UNKNOWN!!";
		}
	}

	private void Awake()
	{
		mThread = new Thread(_ThreadMain);
	}

	private void Start()
	{
		mIsRunning = true;
		mThread.Start();
	}

	private void Update()
	{
		IAPRequest iAPRequest = null;
		try
		{
			lock (mResponsesLock)
			{
				if (mResponses.Count > 0)
				{
					iAPRequest = mResponses.Dequeue();
				}
			}
			if (iAPRequest == null)
			{
				return;
			}
			log(string.Format("Response(#{0}) - {1} {2}", (iAPRequest._httpAsync == null) ? (-1) : iAPRequest._httpAsync.SeqNo, ResultString(iAPRequest._serverResult), iAPRequest._api));
			if (iAPRequest._responseHandler != null)
			{
				iAPRequest._responseHandler(iAPRequest._httpAsync);
			}
			if (iAPRequest._httpAsync == null)
			{
				return;
			}
			try
			{
				iAPRequest._httpAsync.Dispose();
			}
			catch (Exception)
			{
			}
		}
		catch (Exception)
		{
		}
	}

	private void OnApplicationQuit()
	{
		int num = 0;
		do
		{
			mIsRunning = false;
			mRequestsNotify.Set();
			mThread.Join(100);
		}
		while (mThread.IsAlive && num++ < 3);
		if (mThread.IsAlive)
		{
			mThread.Abort();
		}
		lock (mRequestsLock)
		{
			foreach (IAPRequest mRequest in mRequests)
			{
				mRequest._httpAsync.Abort();
			}
			mRequests.Clear();
		}
	}

	private int GetRequestsWaitTimeMillis()
	{
		return -1;
	}

	private int GetResponsesWaitTimeMillis()
	{
		return 10;
	}

	private void _ThreadMain()
	{
		while (mIsRunning)
		{
			try
			{
				mRequestsNotify.WaitOne(GetRequestsWaitTimeMillis());
				mRequestsNotify.Reset();
				IAPRequest iAPRequest = null;
				do
				{
					iAPRequest = null;
					lock (mRequestsLock)
					{
						if (mRequests.Count > 0)
						{
							iAPRequest = mRequests.Dequeue();
						}
					}
					if (iAPRequest == null)
					{
						continue;
					}
					try
					{
						mThreadLock = new ManualResetEvent(false);
						iAPRequest._time = DateTime.Now;
						iAPRequest._httpAsync.GetResponse(delegate(HttpAsync httpAsync)
						{
							IAPRequest iAPRequest2 = httpAsync.UserData as IAPRequest;
							if (iAPRequest2 != null)
							{
								dbg(string.Format("(#{0}): ServerIAP HttpAsync Result={1}    {2} ({3}sec)", iAPRequest2._httpAsync.SeqNo, iAPRequest2._httpAsync.Result, iAPRequest2._httpAsync.Request.RequestUri, ((DateTime.Now - iAPRequest2._time).TotalMilliseconds / 1000.0).ToString("0.000")));
							}
							mThreadLock.Set();
						}, iAPRequest);
						bool flag = false;
						while (!mThreadLock.WaitOne(GetResponsesWaitTimeMillis()))
						{
							if (!mIsRunning)
							{
								flag = true;
								break;
							}
						}
						if (flag)
						{
							iAPRequest._serverResult = 4;
						}
						else
						{
							switch (iAPRequest._httpAsync.Result)
							{
							case HttpAsync.RESULT.SUCCESS:
							{
								HttpWebResponse httpWebResponse = iAPRequest._httpAsync.Response as HttpWebResponse;
								if (httpWebResponse != null)
								{
									log(string.Format("(#{0}): HTTP Result={1}   (len={2})", iAPRequest._httpAsync.SeqNo, httpWebResponse.StatusCode, httpWebResponse.ContentLength));
									if (httpWebResponse.StatusCode == HttpStatusCode.OK)
									{
										iAPRequest._serverResult = 0;
									}
									else
									{
										iAPRequest._serverResult = 1;
									}
								}
								else
								{
									iAPRequest._serverResult = 1;
								}
								break;
							}
							case HttpAsync.RESULT.ERROR:
								iAPRequest._serverResult = 1;
								break;
							case HttpAsync.RESULT.TIMEOUT:
								iAPRequest._serverResult = 3;
								break;
							default:
								iAPRequest._serverResult = 1;
								break;
							}
						}
					}
					catch (Exception)
					{
						iAPRequest._serverResult = 1;
					}
					lock (mResponsesLock)
					{
						mResponses.Enqueue(iAPRequest);
					}
					SetConnecting(iAPRequest._api, false);
				}
				while (iAPRequest != null);
			}
			catch (Exception e)
			{
				BIJLog.E("Server comminucation something missing.", e);
			}
		}
	}

	private bool PostRequest(API api, ref HttpAsync httpAsync, Action<int, object> handler, HttpAsync.ResponseHandler responseHandler, object userdata)
	{
		int num = 0;
		lock (mRequestsLock)
		{
			num = mRequests.Count;
		}
		if (num >= 10)
		{
			return false;
		}
		IAPRequest iAPRequest = new IAPRequest();
		iAPRequest._api = api;
		iAPRequest._httpAsync = httpAsync;
		iAPRequest._handler = handler;
		iAPRequest._responseHandler = responseHandler;
		iAPRequest._userdata = userdata;
		iAPRequest._serverResult = 0;
		SetConnecting(iAPRequest._api, true);
		lock (mRequestsLock)
		{
			mRequests.Enqueue(iAPRequest);
		}
		mRequestsNotify.Set();
		return true;
	}

	private static bool Communicate<TRequest, TResponse>(API _api, string _path, TRequest _req, Action<int, object> _handler) where TRequest : class, new() where TResponse : class, new()
	{
		if (!IsSendable)
		{
			return false;
		}
		if (IsConnecting(_api))
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.UUID == 0)
		{
			return false;
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(BaseURL);
		stringBuilder.Append(_path);
		string p = new MiniJSONSerializer().Serialize(_req);
		bool flag = false;
		try
		{
			return Network.CreateHttpPostAsyncJson(stringBuilder.ToString(), p, Timeout, delegate(HttpAsync httpAsync)
			{
				if (!Instance.PostRequest(_api, ref httpAsync, _handler, delegate(HttpAsync _httpAsync)
				{
					int num = 1;
					TResponse arg = (TResponse)null;
					if (_httpAsync != null)
					{
						IAPRequest iAPRequest = _httpAsync.UserData as IAPRequest;
						if (iAPRequest != null)
						{
							num = iAPRequest._serverResult;
						}
						dbg(string.Format("(#{0}): result={1}", _httpAsync.SeqNo, ResultString(num)));
						if (_httpAsync.Response != null)
						{
							HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
							try
							{
								using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
								{
									string text = streamReader.ReadToEnd();
									dbg(string.Format("(#{0}): {1}={2}", _httpAsync.SeqNo, _api, text));
									arg = (TResponse)new MiniJSONSerializer().Deserialize<TResponse>(text);
								}
							}
							catch (Exception e2)
							{
								BIJLog.E("response error.", e2);
								num = 1;
							}
							finally
							{
								if (httpWebResponse != null)
								{
									httpWebResponse.Close();
								}
							}
						}
					}
					if (_handler != null)
					{
						_handler(num, arg);
					}
				}, null))
				{
					BIJLog.E("post error.");
					if (_handler != null)
					{
						_handler(1, null);
					}
				}
			}, UseEncryption);
		}
		catch (Exception e)
		{
			BIJLog.E("request error.", e);
			return false;
		}
	}
}
