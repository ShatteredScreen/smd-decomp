using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapChangeDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2
	}

	public enum SELECT_ITEM
	{
		CLEAR = 0,
		UNLOCK_NEXTSEASON = 1,
		CANCEL = 2
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	public STATE StateStatus;

	public STATE OverwriteState = STATE.WAIT;

	private STATE mPrevStateError = STATE.MAIN;

	private List<string> mPreLoadAnimationKeys = new List<string>();

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private ConfirmDialog mConfirmDialog;

	private Live2DRender mL2DRender;

	private UITexture mL2DTexture;

	private Live2DInstance mL2DInstLuna;

	private Live2DInstance mL2DInstArtemis;

	public override void Start()
	{
		base.Start();
	}

	public override IEnumerator BuildResource()
	{
		ResLive2DAnimation anim = ResourceManager.LoadLive2DAnimationAsync("LUNA");
		ResLive2DAnimation anim2 = ResourceManager.LoadLive2DAnimationAsync("ARTEMIS");
		mPreLoadAnimationKeys.Add("LUNA");
		mPreLoadAnimationKeys.Add("ARTEMIS");
		while (anim.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (anim2.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mState.Change(STATE.MAIN);
			break;
		case STATE.MAIN:
			if (mState.IsChanged() && mL2DRender != null)
			{
				mL2DTexture = Util.CreateGameObject("Texture", base.gameObject).AddComponent<UITexture>();
				mL2DTexture.mainTexture = mL2DRender.RenderTexture;
				mL2DTexture.SetDimensions(568, 568);
				mL2DTexture.transform.localPosition = new Vector3(0f, 166f, 0f);
				mL2DTexture.depth = mBaseDepth + 5;
				mL2DTexture.transform.localScale = new Vector3(1.18f, 1.18f, 1f);
			}
			break;
		}
		mState.Update();
	}

	public void Init()
	{
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		mState.Change(STATE.INIT);
		mL2DRender = mGame.CreateLive2DRender(568, 568);
		mL2DInstLuna = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
		mL2DInstLuna.Init("LUNA", Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
		mL2DInstLuna.StartMotion("motions/talk00.mtn", true);
		mL2DInstLuna.SetExpression("expressions/F00.exp.json");
		mL2DInstLuna.Position = new Vector3(-180f, 0f, -50f);
		mL2DRender.ResisterInstance(mL2DInstLuna);
		mL2DInstArtemis = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
		mL2DInstArtemis.Init("ARTEMIS", Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
		mL2DInstArtemis.StartMotion("motions/talk00.mtn", true);
		mL2DInstArtemis.SetExpression("expressions/F01.exp.json");
		mL2DInstArtemis.Position = new Vector3(180f, 0f, -50f);
		mL2DRender.ResisterInstance(mL2DInstArtemis);
		string titleDescKey = Localization.Get("RoadBlock_TitleMax");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("RoadBlock_MaxDesc"), mBaseDepth + 4, new Vector3(0f, 144f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		mGame.PlaySe("VOICE_009", -1);
		CreateCancelButton("OnCancelPushed");
	}

	private void TutorialStartSub_Message(Def.TUTORIAL_INDEX index)
	{
	}

	public void OnTutorialFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		if (mL2DInstLuna != null)
		{
			Object.Destroy(mL2DInstLuna.gameObject);
			mL2DInstLuna = null;
		}
		if (mL2DInstArtemis != null)
		{
			Object.Destroy(mL2DInstArtemis.gameObject);
			mL2DInstArtemis = null;
		}
		if (mL2DRender != null)
		{
			Object.Destroy(mL2DTexture.gameObject);
			Object.Destroy(mL2DRender.gameObject);
			mL2DTexture = null;
			mL2DRender = null;
		}
		for (int i = 0; i < mPreLoadAnimationKeys.Count; i++)
		{
			ResourceManager.UnloadLive2DAnimation(mPreLoadAnimationKeys[i]);
		}
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
