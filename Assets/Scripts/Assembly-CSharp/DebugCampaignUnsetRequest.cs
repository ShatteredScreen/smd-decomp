public class DebugCampaignUnsetRequest : ParameterObject<DebugCampaignUnsetRequest>
{
	public int bhiveid { get; set; }

	public int uuid { get; set; }

	public string udid { get; set; }

	public string uniq_id { get; set; }
}
