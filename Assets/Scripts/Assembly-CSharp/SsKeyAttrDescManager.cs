using System.Collections.Generic;

public static class SsKeyAttrDescManager
{
	private static Dictionary<string, SsKeyAttrDesc> _list = new Dictionary<string, SsKeyAttrDesc>
	{
		{
			"POSX",
			new SsKeyAttrDesc(SsKeyAttr.PosX, SsKeyValueType.Data, SsKeyCastType.Float)
		},
		{
			"POSY",
			new SsKeyAttrDesc(SsKeyAttr.PosY, SsKeyValueType.Data, SsKeyCastType.Float)
		},
		{
			"ANGL",
			new SsKeyAttrDesc(SsKeyAttr.Angle, SsKeyValueType.Data, SsKeyCastType.Degree)
		},
		{
			"SCAX",
			new SsKeyAttrDesc(SsKeyAttr.ScaleX, SsKeyValueType.Data, SsKeyCastType.Float)
		},
		{
			"SCAY",
			new SsKeyAttrDesc(SsKeyAttr.ScaleY, SsKeyValueType.Data, SsKeyCastType.Float)
		},
		{
			"TRAN",
			new SsKeyAttrDesc(SsKeyAttr.Trans, SsKeyValueType.Data, SsKeyCastType.Float)
		},
		{
			"PRIO",
			new SsKeyAttrDesc(SsKeyAttr.Prio, SsKeyValueType.Data, SsKeyCastType.Int)
		},
		{
			"FLPH",
			new SsKeyAttrDesc(SsKeyAttr.FlipH, SsKeyValueType.Param, SsKeyCastType.Bool)
		},
		{
			"FLPV",
			new SsKeyAttrDesc(SsKeyAttr.FlipV, SsKeyValueType.Param, SsKeyCastType.Bool)
		},
		{
			"HIDE",
			new SsKeyAttrDesc(SsKeyAttr.Hide, SsKeyValueType.Param, SsKeyCastType.Bool)
		},
		{
			"PCOL",
			new SsKeyAttrDesc(SsKeyAttr.PartsCol, SsKeyValueType.Color, SsKeyCastType.Other)
		},
		{
			"PALT",
			new SsKeyAttrDesc(SsKeyAttr.PartsPal, SsKeyValueType.Palette, SsKeyCastType.Other)
		},
		{
			"VERT",
			new SsKeyAttrDesc(SsKeyAttr.Vertex, SsKeyValueType.Vertex, SsKeyCastType.Other)
		},
		{
			"UDAT",
			new SsKeyAttrDesc(SsKeyAttr.User, SsKeyValueType.User, SsKeyCastType.Other)
		},
		{
			"IMGX",
			new SsKeyAttrDesc(SsKeyAttr.ImageOffsetX, SsKeyValueType.Data, SsKeyCastType.Int)
		},
		{
			"IMGY",
			new SsKeyAttrDesc(SsKeyAttr.ImageOffsetY, SsKeyValueType.Data, SsKeyCastType.Int)
		},
		{
			"IMGW",
			new SsKeyAttrDesc(SsKeyAttr.ImageOffsetW, SsKeyValueType.Data, SsKeyCastType.Int)
		},
		{
			"IMGH",
			new SsKeyAttrDesc(SsKeyAttr.ImageOffsetH, SsKeyValueType.Data, SsKeyCastType.Int)
		},
		{
			"ORFX",
			new SsKeyAttrDesc(SsKeyAttr.OriginOffsetX, SsKeyValueType.Data, SsKeyCastType.Int)
		},
		{
			"ORFY",
			new SsKeyAttrDesc(SsKeyAttr.OriginOffsetY, SsKeyValueType.Data, SsKeyCastType.Int)
		},
		{
			"SNDF",
			new SsKeyAttrDesc(SsKeyAttr.Sound, SsKeyValueType.Sound, SsKeyCastType.Other)
		}
	};

	public static SsKeyAttrDesc Get(string tagName)
	{
		SsKeyAttrDesc value;
		if (_list.TryGetValue(tagName, out value))
		{
			return value;
		}
		return null;
	}

	public static SsKeyAttrDesc GetById(SsKeyAttr attr)
	{
		foreach (SsKeyAttrDesc value in _list.Values)
		{
			if (value.Attr == attr)
			{
				return value;
			}
		}
		return null;
	}
}
