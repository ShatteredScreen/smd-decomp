public static class SocialSettings
{
	public const bool USE_LOG_GOOGLE_PLUS = true;

	public const bool DEBUG_GOOGLE_PLUS = true;

	public const bool USE_LOG_TWITTER = true;

	public const bool DEBUG_TWITTER = true;

	public const bool USE_LOG_INSTAGRAM = true;

	public const bool DEBUG_INSTAGRAM = true;

	public const bool USE_LOG_GPGS = true;

	public const bool DEBUG_GPGS = true;
}
