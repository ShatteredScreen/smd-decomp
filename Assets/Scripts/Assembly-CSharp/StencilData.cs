public struct StencilData
{
	public Def.TILE_KIND StencilType;

	public IntVector2 Position;

	public IntVector2 Size;

	public int Degree;

	public string ImageName;
}
