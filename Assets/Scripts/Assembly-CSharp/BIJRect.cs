public class BIJRect
{
	public int X;

	public int Y;

	public int Width;

	public int Height;

	public static readonly BIJRect EMPTY = new BIJRect(0, 0, 0, 0);

	public BIJRect()
		: this(0, 0, 0, 0)
	{
	}

	public BIJRect(int x, int y, int width, int height)
	{
		X = x;
		Y = y;
		Width = width;
		Height = height;
	}

	public override string ToString()
	{
		return string.Format("({0,0}, {1,0})-[{2,0} x {3,0}]", X, Y, Width, Height);
	}
}
