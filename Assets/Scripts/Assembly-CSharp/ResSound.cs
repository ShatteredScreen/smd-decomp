using UnityEngine;

public class ResSound : ResourceInstance
{
	public AudioClip mAudioClip;

	public float mLoopStart;

	public float mLoopEnd;

	public ResSound(string name)
		: base(name)
	{
		ResourceType = TYPE.SOUND;
		mLoopStart = 0f;
		mLoopEnd = 0f;
	}
}
