using System;
using System.Collections.Generic;
using UnityEngine;

public class WebSaleDialog : DialogBase
{
	public enum STATE
	{
		BASE_INIT_WAIT = 0,
		OPEN = 1,
		WAIT_OPEN = 2,
		CLOSE = 3,
		MAIN = 4,
		WAIT = 5,
		WATCH_DIALOG_CLOSED = 6,
		WATCH_PURCHASECNF_CLOSING = 7,
		WATCH_ERRDLG_CLOSING = 8,
		MAINTENANCE_CHECK_START = 9,
		MAINTENANCE_CHECK_WAIT = 10,
		MAINTENANCE_DIALOG_WAIT = 11,
		MAINTENANCE_ERRORDIALOG_WAIT = 12,
		MAINTENANCE_ERRORDIALOG_WAIT_02 = 13,
		NETWORK_00 = 14,
		NETWORK_00_1 = 15,
		NETWORK_00_2 = 16,
		NETWORK_01 = 17,
		NETWORK_01_1 = 18,
		NETWORK_01_2 = 19,
		MAINTENANCE_CHECK_START02 = 20,
		MAINTENANCE_CHECK_WAIT02 = 21,
		MAINTENANCE_DIALOG_WAIT02 = 22,
		MAINTENANCE_DIALOG_WAIT02_02 = 23,
		MAINTENANCE_ERRORDIALOG_WAIT02 = 24,
		MAINTENANCE_ERRORDIALOG_WAIT02_02 = 25,
		NETWORK_02 = 26,
		NETWORK_02_1 = 27,
		NETWORK_02_2 = 28,
		NETWORK_02_3 = 29,
		AUTHERROR_WAIT = 30,
		NETWORK_02_2_1 = 31,
		NETWORK_02_2_2 = 32,
		NETWORK_02_2_3 = 33,
		NETWORK_02_2_4 = 34,
		NETWORK_02_2_5 = 35,
		NETWORK_02_2_6 = 36
	}

	public enum SELECT_ITEM
	{
		DETAIL = 0,
		BUY = 1,
		PURCHASE = 2,
		EULA = 3,
		NEGATIVE = 4
	}

	public enum WEBVIEW_STATE
	{
		NONE = 0,
		PROGRESS = 1,
		ERROR = 2
	}

	public enum MODE
	{
		TOP = 0,
		DETAIL = 1
	}

	public enum PLACE
	{
		IN_SHOP = 1,
		PRE_STAGE = 8,
		IN_STAGE = 9,
		MAP_SALE = 10,
		EVENT_SALE = 11,
		LINBANNER_MAP_SALE = 14,
		LINBANNER_EVENT_SALE = 15
	}

	public delegate void OnDialogClosed(SELECT_ITEM i, int id);

	private const string CB_Webview_onError = "Webview_onError";

	private const string CB_Webview_onClose = "Webview_onClose";

	private const string CB_Webview_onPageStarted = "Webview_onPageStarted";

	private const string CB_Webview_onPageFinished = "Webview_onPageFinished";

	private const string CB_Webview_onProgress = "Webview_onProgress";

	private StatusManager<STATE> mState;

	private SELECT_ITEM mSelectItem;

	private WEBVIEW_STATE mWebViewState;

	private MODE mMode;

	private int mIndex;

	private int mSelectedIndex;

	private string mURL;

	private WebSaleInfo mWebSale;

	private int mSaleItemID;

	private ShopItemData mItemData;

	private int mPrice;

	private WebViewObject mWebViewObject;

	private bool mLoadURLCall;

	private string mLoadURL;

	private string mFallbackURL;

	private Vector2 mWebViewOrign;

	private Vector2 mWebViewSize;

	private UIButton mDetailBuyBtn;

	private UIButton mCloseBtn;

	private Vector3 mCloseBtnPos;

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private STATE mBackStatus;

	private DialogBase mWatchClosedDialog;

	private ConfirmDialog mWatchClosingDialog;

	private string mGuID;

	public PLACE mPlace { get; set; }

	public override void Start()
	{
		base.Start();
	}

	private bool IsMaintenanceChecking()
	{
		switch (mState.GetStatus())
		{
		case STATE.MAINTENANCE_CHECK_START:
		case STATE.MAINTENANCE_CHECK_WAIT:
		case STATE.MAINTENANCE_DIALOG_WAIT:
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT:
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT_02:
		case STATE.MAINTENANCE_CHECK_START02:
		case STATE.MAINTENANCE_CHECK_WAIT02:
		case STATE.MAINTENANCE_DIALOG_WAIT02:
		case STATE.MAINTENANCE_DIALOG_WAIT02_02:
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT02:
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT02_02:
			return true;
		default:
			return false;
		}
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy() && !IsMaintenanceChecking())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.BASE_INIT_WAIT:
			mState.Change(STATE.MAINTENANCE_CHECK_START);
			break;
		case STATE.OPEN:
		{
			bool flag2 = false;
			if (mMode == MODE.DETAIL)
			{
				CreateWebViewObject();
			}
			if (!LoadURL(mURL, null))
			{
				mState.Change(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.WAIT_OPEN);
			}
			break;
		}
		case STATE.WAIT_OPEN:
			if (mLoadURLCall)
			{
				break;
			}
			if (mWebViewState == WEBVIEW_STATE.ERROR)
			{
				if (mMode == MODE.DETAIL)
				{
					mWatchClosingDialog = ShowNetworkErrorDialog(STATE.WATCH_ERRDLG_CLOSING, STATE.CLOSE);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.MAIN:
			if (mGame.mBackKeyTrg)
			{
				OnBackKeyPress();
			}
			break;
		case STATE.WATCH_DIALOG_CLOSED:
			if ((bool)mWatchClosedDialog && mWatchClosedDialog.IsClosed())
			{
				mWatchClosedDialog = null;
				mJelly.ResetReduce();
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.WATCH_PURCHASECNF_CLOSING:
			if ((bool)mWatchClosingDialog && mWatchClosingDialog.IsClosing())
			{
				if (mWatchClosingDialog.GetSelectItem() == ConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					OnConfirmClosed(ConfirmDialog.SELECT_ITEM.POSITIVE);
					mWatchClosingDialog.SetClosedCallback(null);
				}
				else
				{
					mWatchClosedDialog = mWatchClosingDialog;
					mState.Change(STATE.WATCH_DIALOG_CLOSED);
				}
				mWatchClosingDialog = null;
			}
			break;
		case STATE.WATCH_ERRDLG_CLOSING:
			if ((bool)mWatchClosingDialog && mWatchClosingDialog.IsClosing())
			{
				OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM.POSITIVE);
				mWatchClosingDialog.SetClosedCallback(null);
				mWatchClosingDialog = null;
			}
			break;
		case STATE.MAINTENANCE_CHECK_START:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT:
			switch (MaintenanceCheckResult())
			{
			case MAINTENANCE_RESULT.OK:
				if (mMode == MODE.DETAIL)
				{
					mState.Change(STATE.NETWORK_00);
				}
				else
				{
					mState.Change(STATE.NETWORK_01);
				}
				break;
			case MAINTENANCE_RESULT.ERROR:
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
				mJelly.Reduce(0f);
				mState.Change(STATE.MAINTENANCE_ERRORDIALOG_WAIT);
				break;
			case MAINTENANCE_RESULT.IN_MAINTENANCE:
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
				mJelly.Reduce(0f);
				mState.Change(STATE.MAINTENANCE_DIALOG_WAIT);
				break;
			}
			break;
		case STATE.MAINTENANCE_DIALOG_WAIT:
			if (mMaintenanceDialog.IsClosing())
			{
				mMaintenanceDialog.SetClosedCallback(null);
				OnMaintenanceDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM.GIVEUP, 0);
			}
			break;
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT:
			if (mMaintenanceErrorDialog.IsClosed())
			{
				mJelly.ResetReduce();
				mState.Change(STATE.MAINTENANCE_ERRORDIALOG_WAIT_02);
			}
			break;
		case STATE.NETWORK_00:
			if (!mGame.Network_UserAuth())
			{
				if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					mWatchClosingDialog = ShowNetworkErrorDialog(STATE.WATCH_ERRDLG_CLOSING, STATE.CLOSE);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		case STATE.NETWORK_00_1:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00_2);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
				break;
			}
			Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mUserAuthErrorCode;
			if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 14, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mWatchClosingDialog = ShowNetworkErrorDialog(STATE.WATCH_ERRDLG_CLOSING, STATE.CLOSE);
			}
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			break;
		}
		case STATE.NETWORK_00_2:
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			if (!mGame.Network_GetGemCount())
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.OPEN);
				}
				else if (mMode == MODE.DETAIL)
				{
					mWatchClosingDialog = ShowNetworkErrorDialog(STATE.WATCH_ERRDLG_CLOSING, STATE.CLOSE);
				}
				else
				{
					mState.Change(STATE.OPEN);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_01_1);
			}
			break;
		case STATE.NETWORK_01_1:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_01_2);
				}
				else if (mMode == MODE.DETAIL)
				{
					mWatchClosingDialog = ShowNetworkErrorDialog(STATE.WATCH_ERRDLG_CLOSING, STATE.CLOSE);
				}
				else
				{
					mState.Change(STATE.OPEN);
				}
			}
			break;
		case STATE.NETWORK_01_2:
			mState.Change(STATE.OPEN);
			break;
		case STATE.MAINTENANCE_CHECK_START02:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.CLOSE);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT02);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT02:
			switch (MaintenanceCheckResult())
			{
			case MAINTENANCE_RESULT.OK:
				mState.Change(STATE.NETWORK_02);
				break;
			case MAINTENANCE_RESULT.IN_MAINTENANCE:
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
				mJelly.Reduce(0f);
				mState.Change(STATE.MAINTENANCE_DIALOG_WAIT02);
				break;
			case MAINTENANCE_RESULT.ERROR:
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
				mJelly.Reduce(0f);
				mState.Change(STATE.MAINTENANCE_ERRORDIALOG_WAIT02);
				break;
			}
			break;
		case STATE.MAINTENANCE_DIALOG_WAIT02:
			if (mMaintenanceDialog.IsClosed())
			{
				mJelly.ResetReduce();
				mState.Change(STATE.MAINTENANCE_DIALOG_WAIT02_02);
			}
			break;
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT02:
			if (mMaintenanceErrorDialog.IsClosed())
			{
				mJelly.ResetReduce();
				mState.Change(STATE.MAINTENANCE_ERRORDIALOG_WAIT02_02);
			}
			break;
		case STATE.NETWORK_02:
		{
			bool flag3 = mGame.Network_BuyItem(mItemData.Index);
			mGame.ConnectRetryFlg = false;
			if (!flag3)
			{
				mWatchClosedDialog = ShowNetworkErrorDialog(STATE.WATCH_DIALOG_CLOSED, STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_1);
			}
			break;
		}
		case STATE.NETWORK_02_1:
			if (!GameMain.mBuyItemCheckDone)
			{
				break;
			}
			if (GameMain.mBuyItemSucceed)
			{
				mState.Change(STATE.NETWORK_02_2);
				break;
			}
			switch (GameMain.mBuyItemErrorCode)
			{
			case Server.ErrorCode.MAINTENANCE_MODE:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 26, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			case Server.ErrorCode.DUPLICATE_DATA:
			{
				ConfirmDialog confirmDialog2 = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog2.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog2.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mWatchClosedDialog = confirmDialog2;
				break;
			}
			default:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 26, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			}
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mJelly.Reduce(0f);
			mState.Change(STATE.WATCH_DIALOG_CLOSED);
			break;
		case STATE.NETWORK_02_2:
		{
			int countID = 0;
			for (int i = 0; i < mGame.SegmentProfile.ShopSetItemList.Count; i++)
			{
				ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopSetItemList[i];
				if (shopLimitedTime.ShopItemID == mItemData.Index)
				{
					countID = shopLimitedTime.ShopCount;
					break;
				}
			}
			mGame.mPlayer.BoughtItem(mItemData.ItemDetail);
			mGame.mPlayer.AddPurchaseItem(mItemData.Index, countID);
			mGame.Save();
			mGame.mOptions.BuyBoostCount++;
			mGame.SaveOptions();
			mGame.SendGetBoostInShop(mItemData, (int)mPlace);
			bool flag = false;
			foreach (KeyValuePair<ShopItemDetail.ItemKind, int> bundleItem in mItemData.ItemDetail.BundleItems)
			{
				if (bundleItem.Key == ShopItemDetail.ItemKind.GROWUP)
				{
					int value = bundleItem.Value;
					int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
					int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
					int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
					int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
					int advLastStage = mGame.mPlayer.AdvLastStage;
					int advMaxStamina = mGame.mPlayer.AdvMaxStamina;
					ServerCram.ArcadeGetItem(value, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 10, 1, (int)mPlace, advLastStage, advMaxStamina);
				}
				if ((bundleItem.Key == ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET && bundleItem.Value > 0) || (bundleItem.Key == ShopItemDetail.ItemKind.PREMIUM_TICKET && bundleItem.Value > 0))
				{
					flag = true;
				}
			}
			if (flag)
			{
				mState.Change(STATE.NETWORK_02_2_1);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_3);
			}
			break;
		}
		case STATE.NETWORK_02_2_1:
		{
			if (!mItemData.ItemDetail.BundleItems.ContainsKey(ShopItemDetail.ItemKind.PREMIUM_TICKET))
			{
				mState.Reset(STATE.NETWORK_02_2_3, true);
				goto case STATE.NETWORK_02_2_3;
			}
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			mGame.Network_AdvSetReward(new int[1] { mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_TICKET] }, null, mGuID, is_retry);
			mState.Change(STATE.NETWORK_02_2_2);
			break;
		}
		case STATE.NETWORK_02_2_2:
			if (!GameMain.mAdvSetRewardCheckDone)
			{
				break;
			}
			if (GameMain.mAdvSetRewardSucceed)
			{
				mGame.mPlayer.AdvSaveData.AdvGotRewardList.Add(mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_TICKET]);
				mGame.SaveAdvData();
				mGuID = null;
				mState.Change(STATE.NETWORK_02_2_3);
				break;
			}
			switch (GameMain.mBuyItemErrorCode)
			{
			case Server.ErrorCode.MAINTENANCE_MODE:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 26, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			case Server.ErrorCode.DUPLICATE_DATA:
			{
				ConfirmDialog confirmDialog3 = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog3.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog3.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mWatchClosedDialog = confirmDialog3;
				break;
			}
			default:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 31, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			}
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mJelly.Reduce(0f);
			mState.Change(STATE.WATCH_DIALOG_CLOSED);
			break;
		case STATE.NETWORK_02_2_3:
		{
			if (!mItemData.ItemDetail.BundleItems.ContainsKey(ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET))
			{
				mState.Reset(STATE.NETWORK_02_2_5, true);
				goto case STATE.NETWORK_02_2_5;
			}
			int is_retry2 = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry2 = 1;
			}
			mGame.Network_AdvSetReward(new int[1] { mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET] }, null, mGuID, is_retry2);
			mState.Change(STATE.NETWORK_02_2_4);
			break;
		}
		case STATE.NETWORK_02_2_4:
			if (!GameMain.mAdvSetRewardCheckDone)
			{
				break;
			}
			if (GameMain.mAdvSetRewardSucceed)
			{
				mGame.mPlayer.AdvSaveData.AdvGotRewardList.Add(mItemData.ItemDetail.BundleItems[ShopItemDetail.ItemKind.PREMIUM_SUB_TICKET]);
				mGame.SaveAdvData();
				mGuID = null;
				mState.Change(STATE.NETWORK_02_2_5);
				break;
			}
			switch (GameMain.mBuyItemErrorCode)
			{
			case Server.ErrorCode.MAINTENANCE_MODE:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 26, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			case Server.ErrorCode.DUPLICATE_DATA:
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mWatchClosedDialog = confirmDialog;
				break;
			}
			default:
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 33, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
				break;
			}
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mJelly.Reduce(0f);
			mState.Change(STATE.WATCH_DIALOG_CLOSED);
			break;
		case STATE.NETWORK_02_2_5:
			mGame.Network_AdvGetAllItem();
			mState.Change(STATE.NETWORK_02_2_6);
			break;
		case STATE.NETWORK_02_2_6:
		{
			if (!GameMain.mAdvGetAllItemCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetAllItemSucceed)
			{
				mState.Change(STATE.NETWORK_02_3);
				break;
			}
			Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mAdvGetAllItemErrorCode;
			if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 26, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
			}
			else
			{
				mWatchClosedDialog = mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 33, OnConnectErrorBuyItemRetry, OnConnectErrorBuyItemAbandon);
			}
			break;
		}
		case STATE.NETWORK_02_3:
			mSelectItem = SELECT_ITEM.BUY;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mState.Change(STATE.CLOSE);
			Close();
			break;
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mState.Change(STATE.CLOSE);
			Close();
			break;
		}
		mState.Update();
	}

	public void Init(MODE mode, int aIndex = -1)
	{
		mMode = mode;
		mIndex = aIndex;
		mSelectedIndex = mIndex;
		mWebSale = mGame.SegmentProfile.WebSale;
		if (mMode == MODE.DETAIL)
		{
			mSaleItemID = mWebSale.DetailList[mIndex - 1];
			mItemData = mGame.mShopItemData[mSaleItemID];
			mPrice = mItemData.GemAmount;
		}
		mState = new StatusManager<STATE>(STATE.BASE_INIT_WAIT);
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mURL = string.Empty;
		switch (mMode)
		{
		case MODE.TOP:
			mURL = mWebSale.MainURL;
			break;
		case MODE.DETAIL:
			mURL = string.Format(mWebSale.DetailBaseURL, mWebSale.DetailList[mIndex - 1]);
			break;
		}
		int forceWidth = 620;
		int num = 630;
		InitDialogFrame(DIALOG_SIZE.LARGE, "DIALOG_BASE", "instruction_panel13", forceWidth, num);
		base.transform.localPosition = Vector3.zero;
		int num2 = 20;
		int num3 = 5;
		int num4 = 6;
		int num5 = 182;
		int num6 = 0;
		int num7 = 2;
		int num8 = 58;
		int num9 = -num / 2 + num3 + num8 / 2 + 13;
		string empty = string.Empty;
		int num10 = -32;
		bool isEula = false;
		if (mMode == MODE.DETAIL)
		{
			isEula = true;
			empty = "LC_button_buy1";
			mDetailBuyBtn = CreateBuyButton(empty, image, "OnBuyPushed_Detail", new Vector3(num6, num9, 0f), mIndex);
			int num11 = num - (num8 + num2 + num3 + num7) + 5 + num10;
			int num12 = num / 2 - (num11 / 2 + num2);
			mWebViewOrign = new Vector2(num6, num12);
			mWebViewSize = new Vector2(560f + (float)num10, num11);
		}
		else
		{
			empty = "LC_button_detail";
			if (mWebSale.DetailList.Count >= 3)
			{
				CreateDetailButton(empty, image, "OnDetailPushed_01", new Vector3(num6 - (num4 + num5), num9, 0f));
				CreateDetailButton(empty, image, "OnDetailPushed_02", new Vector3(num6, num9, 0f));
				CreateDetailButton(empty, image, "OnDetailPushed_03", new Vector3(num6 + (num4 + num5), num9, 0f));
			}
			else if (mWebSale.DetailList.Count == 2)
			{
				CreateDetailButton(empty, image, "OnDetailPushed_01", new Vector3(num6 - (num4 * 2 + num5 * 3) / 4, num9, 0f));
				CreateDetailButton(empty, image, "OnDetailPushed_02", new Vector3(num6 + (num4 * 2 + num5 * 3) / 4, num9, 0f));
			}
			else if (mWebSale.DetailList.Count == 1)
			{
				CreateDetailButton(empty, image, "OnDetailPushed_01", new Vector3(num6, num9, 0f));
			}
			int num13 = num - (num8 + num2 + num3 + num7) + 5 + num10;
			int num14 = num / 2 - (num13 / 2 + num2);
			mWebViewOrign = new Vector2(num6, num14);
			mWebViewSize = new Vector2(560 + num10, num13);
			CreateWebViewObject();
		}
		mGemFramePositionOffsetP.y = -7f;
		CreateGemWindow(isEula);
		mCloseBtn = CreateCancelButton("OnNegativePushed");
		mCloseBtnPos = mCloseBtn.transform.localPosition;
		if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
		{
			mCloseBtn.transform.localPosition = new Vector3(300f, 285f);
		}
		else if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			mCloseBtn.transform.localPosition = new Vector3(290f, 320f);
		}
		SetLayout(Util.ScreenOrientation);
	}

	private void CreateWebViewObject()
	{
		if (mWebViewObject != null)
		{
			UnityEngine.Object.Destroy(mWebViewObject.gameObject);
			mWebViewObject = null;
		}
		GameObject gameObject = Util.CreateGameObject("WebView", base.gameObject);
		mWebViewObject = gameObject.AddComponent<WebViewObject>();
		mWebViewObject.Init(OnWebViewObjectCallback);
		mWebViewObject.SetVisibility(false);
		mWebViewObject.SetExternalSpawnMode(2);
		UpdateWebViewMargin();
	}

	private UIButton CreateDetailButton(string aSpriteName, BIJImage aAtlas, string aClickFuncName, Vector3 aPos)
	{
		UIButton uIButton = Util.CreateJellyImageButton("DetailButton", base.gameObject, aAtlas);
		Util.SetImageButtonInfo(uIButton, aSpriteName, aSpriteName, aSpriteName, mBaseDepth + 4, aPos, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, aClickFuncName, UIButtonMessage.Trigger.OnClick);
		return uIButton;
	}

	private UIButton CreateBuyButton(string aSpriteName, BIJImage aAtlas, string aClickFuncName, Vector3 aPos, int aIndex)
	{
		int num = mWebSale.DetailList[aIndex - 1];
		bool flag = true;
		for (int i = 0; i < mGame.SegmentProfile.ShopSetItemList.Count; i++)
		{
			ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopSetItemList[i];
			if (num == shopLimitedTime.ShopItemID)
			{
				flag = mGame.mPlayer.IsEnablePurchaseItem(num, shopLimitedTime.ShopItemLimit, shopLimitedTime.ShopCount);
				break;
			}
		}
		if (!flag)
		{
			UISprite sprite = Util.CreateSprite("SoldOut", base.gameObject, aAtlas);
			Util.SetSpriteInfo(sprite, "LC_button_soldout", mBaseDepth + 3, aPos, Vector3.one, false);
			return null;
		}
		UIButton uIButton = Util.CreateJellyImageButton("BuyButton", base.gameObject, aAtlas);
		Util.SetImageButtonInfo(uIButton, aSpriteName, aSpriteName, aSpriteName, mBaseDepth + 4, aPos, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, aClickFuncName, UIButtonMessage.Trigger.OnClick);
		UIFont atlasFont = GameMain.LoadFont();
		UILabel uILabel = Util.CreateLabel("Price", uIButton.gameObject, atlasFont);
		ShopItemData shopItemData = mGame.mShopItemData[num];
		Util.SetLabelInfo(uILabel, string.Empty + shopItemData.GemAmount, mBaseDepth + 5, new Vector3(50f, -5f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.fontSize = 20;
		if (shopItemData.GemAmount > mGame.mPlayer.Dollar)
		{
			uILabel.color = new Color(1f, 0.1f, 0.1f);
		}
		return uIButton;
	}

	private void OnWebViewObjectCallback(string message)
	{
		if (string.Compare("Webview_onPageStarted", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.PROGRESS;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
			}
		}
		else if (string.Compare("Webview_onPageFinished", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS || mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.NONE;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(true);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onProgress", message) == 0)
		{
			if (mWebViewState != WEBVIEW_STATE.PROGRESS)
			{
			}
		}
		else if (string.Compare("Webview_onError", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS)
			{
				mWebViewState = WEBVIEW_STATE.ERROR;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onClose", message) == 0 && mWebViewState == WEBVIEW_STATE.NONE)
		{
			mState.Change(STATE.CLOSE);
			Close();
		}
	}

	private void UpdateWebViewMargin()
	{
		Vector2 vector = Util.LogScreenSize();
		Vector2 vector2 = new Vector2(vector.x - mWebViewSize.x, vector.y - mWebViewSize.y);
		int left = (int)(Util.ToDevX(vector2.x / 2f) + Util.ToDevX(mWebViewOrign.x));
		int top = (int)(Util.ToDevY(vector2.y / 2f) - Util.ToDevY(mWebViewOrign.y));
		int right = (int)(Util.ToDevX(vector2.x / 2f) - Util.ToDevX(mWebViewOrign.x));
		int bottom = (int)(Util.ToDevY(vector2.y / 2f) + Util.ToDevY(mWebViewOrign.y));
		if (mWebViewObject != null)
		{
			mWebViewObject.SetMargins(left, top, right, bottom);
		}
	}

	private bool LoadURL(string url, string fallback)
	{
		if (mWebViewObject != null)
		{
			mLoadURLCall = true;
			mWebViewState = WEBVIEW_STATE.NONE;
			mLoadURL = url;
			mFallbackURL = fallback;
			mWebViewObject.LoadURL(url);
			return true;
		}
		return false;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mCloseBtn != null)
			{
				mCloseBtn.transform.localPosition = new Vector3(290f, 320f);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mCloseBtn != null)
			{
				mCloseBtn.transform.localPosition = new Vector3(300f, 285f);
			}
			break;
		}
		UpdateWebViewMargin();
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem, mSelectedIndex);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnBuyPushed_01(GameObject go)
	{
		OnBuyPushed(go, 1);
	}

	public void OnBuyPushed_02(GameObject go)
	{
		OnBuyPushed(go, 2);
	}

	public void OnBuyPushed_03(GameObject go)
	{
		OnBuyPushed(go, 3);
	}

	public void OnBuyPushed_Detail(GameObject go)
	{
		OnBuyPushed(go, mIndex);
	}

	public void OnBuyPushed(GameObject go, int aIndex)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		mSelectedIndex = aIndex;
		mSaleItemID = mWebSale.DetailList[aIndex - 1];
		mItemData = mGame.mShopItemData[mSaleItemID];
		mPrice = mItemData.GemAmount;
		double num = 0.0;
		double num2 = 0.0;
		for (int i = 0; i < mGame.SegmentProfile.ShopSetItemList.Count; i++)
		{
			ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopSetItemList[i];
			if (shopLimitedTime.ShopItemID == mItemData.Index)
			{
				num = shopLimitedTime.StartTime;
				num2 = shopLimitedTime.EndTime;
				break;
			}
		}
		DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(num, true);
		DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(num2, true);
		DateTime dateTime3 = DateTimeUtil.Now();
		if ((num != 0.0 && dateTime3 < dateTime) || (num2 != 0.0 && dateTime3 > dateTime2) || num == 0.0 || num2 == 0.0)
		{
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mJelly.Reduce(0f);
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("ShopItemTitle_BuySale_Timeout"), Localization.Get("ShopItemDesc_BuySale_Timeout"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
			confirmDialog.SetClosedCallback(OnSaleTimeOutClosed);
			mWatchClosedDialog = confirmDialog;
			mState.Change(STATE.WATCH_DIALOG_CLOSED);
		}
		else if (mPrice <= mGame.mPlayer.Dollar)
		{
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mJelly.Reduce(0f);
			GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
			gemUseConfirmDialog.Init(mPrice, 20);
			gemUseConfirmDialog.SetClosedCallback(OnGemUseConfirmClosed);
			mWatchClosedDialog = gemUseConfirmDialog;
			mState.Change(STATE.WATCH_DIALOG_CLOSED);
		}
		else
		{
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mJelly.Reduce(0f);
			mWatchClosingDialog = Util.CreateGameObject("ConfirmDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mWatchClosingDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			mWatchClosingDialog.SetClosedCallback(OnConfirmClosed);
			mState.Change(STATE.WATCH_PURCHASECNF_CLOSING);
		}
	}

	public void OnGemUseConfirmClosed(GemUseConfirmDialog.SELECT_ITEM item, int aValue)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		if (item == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Change((STATE)aValue);
			return;
		}
		mSelectedIndex = mIndex;
		mState.Change(STATE.MAIN);
	}

	public void OnSaleTimeOutClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		mSelectedIndex = mIndex;
		mState.Change(STATE.MAIN);
	}

	public void OnConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mSelectedIndex = mIndex;
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mSelectItem = SELECT_ITEM.PURCHASE;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mState.Change(STATE.CLOSE);
			Close();
		}
		else
		{
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(true);
			}
			mState.Change(STATE.MAIN);
		}
	}

	public void OnDetailPushed_01(GameObject go)
	{
		OnDetailPushed(go, 1);
	}

	public void OnDetailPushed_02(GameObject go)
	{
		OnDetailPushed(go, 2);
	}

	public void OnDetailPushed_03(GameObject go)
	{
		OnDetailPushed(go, 3);
	}

	public void OnDetailPushed(GameObject go, int aIndex)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.DETAIL;
			mSelectedIndex = aIndex;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mState.Change(STATE.CLOSE);
			Close();
		}
	}

	public override void OnPushedEulaDisplay()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = SELECT_ITEM.EULA;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mState.Change(STATE.CLOSE);
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy() && (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAINTENANCE_CHECK_WAIT || mState.GetStatus() == STATE.MAINTENANCE_DIALOG_WAIT))
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			mState.Change(STATE.CLOSE);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}

	private ConfirmDialog ShowNetworkErrorDialog(STATE aChangeStatus, STATE aBackStatus)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(false);
		}
		mJelly.Reduce(0f);
		mBackStatus = aBackStatus;
		ConfirmDialog confirmDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		confirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(OnNetworkErrorDialogClosed);
		mState.Change(aChangeStatus);
		return confirmDialog;
	}

	private void OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		if (mBackStatus == STATE.CLOSE)
		{
			mSelectItem = SELECT_ITEM.NEGATIVE;
			mState.Change(mBackStatus);
			Close();
			return;
		}
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		mState.Change(mBackStatus);
	}

	private void OnConnectErrorAuthRetry(int a_state)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorAuthAbandon()
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		mSelectItem = SELECT_ITEM.NEGATIVE;
		mGame.PlaySe("SE_NEGATIVE", -1);
		mState.Change(STATE.CLOSE);
		Close();
	}

	private void OnDuplicateDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mSelectedIndex = mIndex;
		mGame.GetGemCountFreqFlag = false;
		mState.Change(STATE.NETWORK_00);
	}

	private void OnConnectErrorBuyItemRetry(int a_state)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		mGame.ConnectRetryFlg = true;
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorBuyItemAbandon()
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		mGame.ConnectRetryFlg = false;
		mGame.PlaySe("SE_NEGATIVE", -1);
		mState.Change(STATE.MAIN);
	}

	public override void OnMaintenanceDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM item, int a_state)
	{
		base.OnMaintenanceDialogClosed(item, a_state);
		STATE status = mState.GetStatus();
		if (status != STATE.MAINTENANCE_DIALOG_WAIT && status == STATE.MAINTENANCE_DIALOG_WAIT02_02)
		{
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(true);
			}
			mState.Change(STATE.MAIN);
		}
	}

	public override void OnMaintenanceCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM item, int a_state)
	{
		base.OnMaintenanceCheckErrorDialogClosed(item, a_state);
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(true);
		}
		switch (mState.GetStatus())
		{
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT_02:
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
			break;
		case STATE.MAINTENANCE_ERRORDIALOG_WAIT02_02:
			mState.Change(STATE.MAIN);
			break;
		}
	}

	public override void Close()
	{
		base.Close();
	}
}
