using System.Collections;
using UnityEngine;

public class EventBingoCockpit : EventCockpit
{
	protected const float SaleIconBingoBasePosX_P = -55f;

	protected const float SaleIconBingoBasePosY_P = 304f;

	protected const float SaleIconBingoBasePosX_L = -55f;

	protected const float SaleIconBingoBasePosY_L = 304f;

	protected const float SaleIconBingoPlaceInterval = -110f;

	private UISprite mTitleBoard;

	private int mTotalBingoCount;

	private bool InitFirst = true;

	private bool Live2DLoadEnd;

	private Live2DRender mNekoL2DRender;

	private UITexture mNekoL2DTexture;

	private GameObject mReLotteryRoot;

	private UIButton mReLottery;

	private UISprite mFukidashiSprite;

	private Transform mCharacterTrans;

	private JellyImageButton reLotteryJelly;

	private BoxCollider reLotteryCollider;

	private GameObject mFukidashiRoot;

	private GameObject mAtoNanbingoGO;

	private GameObject mDeGetGO;

	private bool IsMoveNow;

	protected GameObject mFooterRoot;

	protected override void Update()
	{
		if (mGSM == null)
		{
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (InitFirst)
			{
				InitFirst = false;
				CreateCockpitStatus(40);
				CreateCockpitItems(40);
				CreateCockpitDebug(40, -90);
				CreateSaleItem(40);
				CreateMugenHeart(40);
				StartCoroutine(CreateRelotteryItems(40));
				if (!mButtonEnableFlg)
				{
					SetEnableButtonAction(false);
					mButtonEnableFlg = true;
				}
			}
			if (Live2DLoadEnd)
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.OPEN:
			if (mState.IsChanged() && mOpenCallback != null)
			{
				mOpenCallback(false);
			}
			break;
		case STATE.MAIN:
			UpdateSaleFlg();
			if (base.mShopflg)
			{
				base.ShopBagEnable = true;
			}
			else
			{
				base.ShopBagEnable = false;
			}
			UpdateMugenHeartIconFlg();
			if (base.mMugenflg)
			{
				base.ShopMugenHeartEnable = true;
			}
			else
			{
				base.ShopMugenHeartEnable = false;
			}
			HowToButtonLocation();
			break;
		case STATE.MOVE:
			if (!mState.IsChanged())
			{
				break;
			}
			switch (mState.GetPreStatus())
			{
			case STATE.OPEN:
				if (mCloseCallback != null)
				{
					mCloseCallback(true);
				}
				break;
			case STATE.CLOSE:
				if (mOpenCallback != null)
				{
					mOpenCallback(true);
				}
				break;
			}
			break;
		}
		mState.Update();
		UpdateUIPanel();
		UpdateHUD();
		UpdateDrawFlag();
		UpdateDrawOptionFlag();
		UpdateStatus();
	}

	protected override void CreateSaleItem(int baseDepth)
	{
		float x = ShopBugLocation().x;
		float y = ShopBugLocation().y;
		GameObject parent = ((Util.ScreenOrientation != ScreenOrientation.Portrait && Util.ScreenOrientation != ScreenOrientation.PortraitUpsideDown) ? mAnchorBL.gameObject : mAnchorBR.gameObject);
		UpdateSaleFlg();
		shopBagbutton = Util.CreateJellyImageButton("shopbagbutton", parent, HudAtlas);
		Util.SetImageButtonInfo(shopBagbutton, "button_itemset3", "button_itemset3", "button_itemset3", baseDepth + 2, new Vector3(x, y, 0f), Vector3.one);
		mCockpitButtons.Add(shopBagbutton);
		Util.AddImageButtonMessage(shopBagbutton, mGSM, "OnStoreBagPushed", UIButtonMessage.Trigger.OnClick);
		NGUITools.SetActive(shopBagbutton.gameObject, false);
		UISprite component = shopBagbutton.GetComponent<UISprite>();
		if ((bool)component)
		{
			int num = 100;
			int num2 = 100;
			if (component.width > component.height)
			{
				num = 100;
				num2 = (int)((float)num / (float)component.width * (float)component.height);
			}
			else if (component.height > component.width)
			{
				num2 = 100;
				num = (int)((float)num2 / (float)component.height * (float)component.width);
			}
			component.SetDimensions(num, num2);
		}
		StartCoroutine(UpdateShopBagbutton(shopBagbutton, baseDepth + 4));
		shopBagSalesprite = Util.CreateSprite("shopBugSale", shopBagbutton.gameObject, HudAtlas);
		Util.SetSpriteInfo(shopBagSalesprite, "LC_sale_mini00", baseDepth + 3, new Vector3(0f, -49f, 0f), Vector2.one, false);
		Vector3 position_offset = new Vector3(-10f, -5f);
		if (mGame.mShopItemData.ContainsKey(base.mSaleItemID) && !string.IsNullOrEmpty(mGame.mShopItemData[base.mSaleItemID].SaleIconName))
		{
			position_offset = Vector3.zero;
		}
		StartCoroutine(UpdateSaleBound(shopBagSalesprite, position_offset));
	}

	protected override Vector3 ShopBugLocation()
	{
		Vector3 result;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			if (shopBagbutton != null)
			{
				shopBagbutton.transform.SetParent(mAnchorBR.transform);
			}
			result = ((!base.mMugenflg) ? new Vector3(-55f, 304f) : new Vector3(-165f, 304f));
			if (mButtonDebug != null)
			{
				mButtonDebug.transform.localPosition = new Vector3(60f, -320f, 0f);
			}
		}
		else
		{
			if (shopBagbutton != null)
			{
				shopBagbutton.transform.SetParent(mAnchorBL.transform);
			}
			result = ((!base.mMugenflg) ? new Vector3(335f, 304f) : new Vector3(225f, 304f));
			if (mButtonDebug != null)
			{
				mButtonDebug.transform.localPosition = new Vector3(60f, -200f, 0f);
			}
		}
		return result;
	}

	protected override void CreateMugenHeart(int baseDepth)
	{
		float x = MugenHeartLocation().x;
		float y = MugenHeartLocation().y;
		GameObject parent = ((Util.ScreenOrientation != ScreenOrientation.Portrait && Util.ScreenOrientation != ScreenOrientation.PortraitUpsideDown) ? mAnchorBL.gameObject : mAnchorBR.gameObject);
		UpdateMugenHeartIconFlg();
		mugenHeartbutton = Util.CreateJellyImageButton("mugenheartbutton", parent, HudAtlas);
		Util.SetImageButtonInfo(mugenHeartbutton, "button_heartinfinite", "button_heartinfinite", "button_heartinfinite", baseDepth + 2, new Vector3(x, y, 0f), Vector3.one);
		mCockpitButtons.Add(mugenHeartbutton);
		Util.AddImageButtonMessage(mugenHeartbutton, mGSM, "OnMugenHeartButtonPushed", UIButtonMessage.Trigger.OnClick);
		NGUITools.SetActive(mugenHeartbutton.gameObject, false);
		StartCoroutine(UpdateShopMugenHeartbutton(mugenHeartbutton));
		string imageName = "LC_sale_mini00";
		if (mMugenHeartItemID != 0 && mGame.mShopItemData[mMugenHeartItemID].SaleNum == 0)
		{
			imageName = "LC_sale_mini01";
		}
		mugenHeartSalesprite = Util.CreateSprite("mugenHeartSprite", mugenHeartbutton.gameObject, HudAtlas);
		Util.SetSpriteInfo(mugenHeartSalesprite, imageName, baseDepth + 3, new Vector3(0f, -44f, 0f), Vector2.one, false);
		StartCoroutine(UpdateSaleBound(mugenHeartSalesprite, Vector3.zero, true));
	}

	protected override Vector3 MugenHeartLocation()
	{
		Vector3 result;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			if (mugenHeartbutton != null)
			{
				mugenHeartbutton.transform.SetParent(mAnchorBR.transform);
			}
			result = new Vector3(-55f, 299f);
		}
		else
		{
			if (mugenHeartbutton != null)
			{
				mugenHeartbutton.transform.SetParent(mAnchorBL.transform);
			}
			result = new Vector3(335f, 299f);
		}
		return result;
	}

	protected override Vector3 AllCrushIconLocation()
	{
		int num = 0;
		if (base.mShopflg)
		{
			num++;
		}
		if (base.mMugenflg)
		{
			num++;
		}
		return (Util.ScreenOrientation != ScreenOrientation.Portrait && Util.ScreenOrientation != ScreenOrientation.PortraitUpsideDown) ? new Vector3(-55f + -110f * (float)num, 304f) : new Vector3(-55f + -110f * (float)num, 304f);
	}

	protected void HowToButtonLocation()
	{
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			if (mHowToPlay != null)
			{
				mHowToPlay.transform.SetParent(mAnchorBR.transform);
				mHowToPlay.SetLocalPosition(-55f, 55f);
			}
		}
		else if (mHowToPlay != null)
		{
			mHowToPlay.transform.SetParent(mAnchorBL.transform);
			mHowToPlay.SetLocalPosition(487f, 55f);
		}
	}

	public void CreateEventTitleBoard()
	{
		if (mTitleBoard != null)
		{
			Object.Destroy(mTitleBoard.gameObject);
			mTitleBoard = null;
		}
		int currentEventID = mGame.mPlayer.Data.CurrentEventID;
		SMEventPageData eventPageData = GameMain.GetEventPageData(currentEventID);
		BIJImage image = ResourceManager.LoadImage(eventPageData.EventSetting.MapAtlasKey).Image;
		BIJImage image2 = ResourceManager.LoadImage("EVENTHUD").Image;
		SMSeasonEventSetting sMSeasonEventSetting = mGame.mEventData.InSessionEventList[currentEventID];
		BIJImage image3 = ResourceManager.LoadImage(sMSeasonEventSetting.BannerAtlas).Image;
		float y = -125f;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			y = -120f;
		}
		mTitleBoard = Util.CreateSprite("TitleBoard", mAnchorTL.gameObject, image3);
		Util.SetSpriteInfo(mTitleBoard, eventPageData.EventSetting.MapEventTitle, 35, new Vector3(190f, y, 0f), Vector3.one, false);
		UISprite uISprite = Util.CreateSprite("TotalBigoBoard", mTitleBoard.gameObject, image);
		Util.SetSpriteInfo(uISprite, "LC_event_instruction_panel02", 35, new Vector3(0f, -95f, 0f), Vector3.one, false);
		UISprite uISprite2 = Util.CreateSprite("Denomi1", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite2, "event01_num0", 36, new Vector3(42f, -11f, 0f), Vector3.one, false);
		uISprite2.gameObject.transform.localScale = new Vector3(0.7f, 0.7f);
		uISprite2 = Util.CreateSprite("Denomi10", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite2, "event01_num4", 36, new Vector3(23f, -11f, 0f), Vector3.one, false);
		uISprite2.gameObject.transform.localScale = new Vector3(0.7f, 0.7f);
		uISprite2 = Util.CreateSprite("slash", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite2, "event01_num_slash", 36, new Vector3(9f, -11f, 0f), Vector3.one, false);
		uISprite2.gameObject.transform.localScale = new Vector3(0.7f, 0.7f);
		uISprite2 = Util.CreateSprite("Mole1", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite2, "event01_num" + mTotalBingoCount % 10, 36, new Vector3(-12f, -7f, 0f), Vector3.one, false);
		uISprite2.gameObject.transform.localScale = new Vector3(1.1f, 1.1f);
		if (mTotalBingoCount / 10 != 0)
		{
			uISprite2 = Util.CreateSprite("Mole10", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, "event01_num" + mTotalBingoCount / 10, 36, new Vector3(-42f, -7f, 0f), Vector3.one, false);
			uISprite2.gameObject.transform.localScale = new Vector3(1.1f, 1.1f);
		}
	}

	protected override void CreateCockpitStatus(int baseDepth)
	{
		base.CreateCockpitStatus(baseDepth);
		CreateEventTitleBoard();
		UIPanel component = mCockpitRoot.GetComponent<UIPanel>();
		if (component != null)
		{
			Object.Destroy(component);
		}
		mAnchorT.gameObject.AddComponent<UIPanel>().sortingOrder = 38;
		mAnchorTL.gameObject.AddComponent<UIPanel>().sortingOrder = 38;
		mAnchorTR.gameObject.AddComponent<UIPanel>().sortingOrder = 38;
		mAnchorB.gameObject.AddComponent<UIPanel>().sortingOrder = 37;
		mAnchorBL.gameObject.AddComponent<UIPanel>().sortingOrder = 38;
		mAnchorBR.gameObject.AddComponent<UIPanel>().sortingOrder = 38;
		mAnchorC.gameObject.AddComponent<UIPanel>().sortingOrder = 7;
		SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		BIJImage image = ResourceManager.LoadImage(currentEventPageData.EventSetting.MapAtlasKey).Image;
		UIFont atlasFont = GameMain.LoadFont();
		GameObject gameObject = null;
		gameObject = Util.CreateGameObject("Character", mAnchorC.gameObject);
		mCharacterTrans = gameObject.transform;
		mCharacterTrans.SetLocalPosition(136f, 320f);
		CreateLive2d(mGame.mCompanionData[currentEventPageData.BingoMapSetting.EventCockpitCharaID], CompanionData.MOTION.STAY0, new IntVector2(275, 275), new IntVector2(275, 275), mGSM.MapPageRoot, gameObject, Vector3.zero, 32, 0.5f);
		int num = 0;
		if (mTotalBingoCount < currentEventPageData.BingoMapSetting.FirstGetBingoNum)
		{
			num = currentEventPageData.BingoMapSetting.FirstGetBingoNum;
		}
		else if (mTotalBingoCount < currentEventPageData.BingoMapSetting.SecondGetBingoNum)
		{
			num = currentEventPageData.BingoMapSetting.SecondGetBingoNum;
		}
		if (num > 0)
		{
			float num2 = 0f;
			num2 = 5f;
			mFukidashiRoot = Util.CreateGameObject("FukidashiParent", gameObject);
			UISprite uISprite = Util.CreateSprite("Fukidashi", mFukidashiRoot, image);
			Util.SetSpriteInfo(uISprite, "bng_fukidashi00", 36, new Vector3(41f, 76f, 0f), Vector3.one, false, UIWidget.Pivot.BottomLeft);
			uISprite.SetDimensions(138, 86);
			TweenUtil.ScaleTo(uISprite.gameObject, new Vector3(1.05f, 1.05f, 1f), iTween.EaseType.spring, iTween.LoopType.pingPong, 1f);
			mFukidashiSprite = uISprite;
			UILabel uILabel = Util.CreateLabel("AtoNanbingo", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Format(Localization.Get("BINGO_CharacterIncentive_00"), num), 37, new Vector3(69f, 53f + num2, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = new Color(14f / 15f, 22f / 85f, 23f / 51f);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(122, 26);
			UILabel uILabel2 = Util.CreateLabel("DeGet", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel2, Localization.Get("BINGO_CharacterIncentive_01"), 37, new Vector3(69f, 32f + num2, 0f), 16, 0, 0, UIWidget.Pivot.Center);
			uILabel2.color = new Color(14f / 15f, 22f / 85f, 23f / 51f);
			mAtoNanbingoGO = uILabel.gameObject;
			mDeGetGO = uILabel2.gameObject;
			SetFukidashiLayout(Util.ScreenOrientation);
		}
	}

	protected override void CreateCockpitItems(int baseDepth)
	{
		base.CreateCockpitItems(baseDepth);
		GameObject parent = mAnchorB.gameObject;
		float num = 0f;
		float num2 = 170f;
		mFooterRoot = Util.CreateGameObject("FooterParent", parent);
		UISprite sprite = Util.CreateSprite("StatusR", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(-2f, num - num2, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite = Util.CreateSprite("StatusL", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(2f, num - num2, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		HowToButtonLocation();
		SetLayout(Util.ScreenOrientation);
	}

	private void UpdateHUD()
	{
	}

	public void SetTotalBingoNum(int _count)
	{
		mTotalBingoCount = _count;
	}

	public IEnumerator CreateRelotteryItems(int depth)
	{
		GameObject go = mAnchorBL.gameObject;
		int eventID = mGame.mPlayer.Data.CurrentEventID;
		SMEventPageData mapdata = GameMain.GetEventPageData(eventID);
		BIJImage EventMapAtlas = ResourceManager.LoadImage(mapdata.EventSetting.MapAtlasKey).Image;
		mReLotteryRoot = Util.CreateGameObject("ReLotteryRoot", go);
		mReLotteryRoot.transform.localPosition = new Vector3(80f, 290f, 0f);
		UISprite lace = Util.CreateSprite(atlas: ResourceManager.LoadImage("EVENTBINGO").Image, name: "runa_lace", parent: mReLotteryRoot);
		Util.SetSpriteInfo(lace, "runa_lace", depth + 1, new Vector3(0f, 21f, 0f), Vector3.one, false);
		UISprite GaraGara = Util.CreateSprite("GaraGara", mReLotteryRoot, EventMapAtlas);
		Util.SetSpriteInfo(GaraGara, "event_lottery", depth + 2, new Vector3(15f, 25f, 0f), Vector3.one, false);
		mNekoL2DRender = mGame.CreateLive2DRender(400, 400);
		Live2DInstance mNekoL2DInst = Util.CreateGameObject("CharNeko", mNekoL2DRender.gameObject).AddComponent<Live2DInstance>();
		mNekoL2DInst.Init(mapdata.BingoMapSetting.ReLotteryLive2d, Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER, new Color(1f, 1f, 1f, 1f), true);
		while (!mNekoL2DInst.IsLoadFinished())
		{
			yield return 0;
		}
		mNekoL2DInst.Position = new Vector3(0f, 0f, -50f);
		mNekoL2DInst.StartMotion("motions/talk00.mtn", true);
		mNekoL2DRender.ResisterInstance(mNekoL2DInst);
		mNekoL2DTexture = Util.CreateGameObject("Neko", mReLotteryRoot).AddComponent<UITexture>();
		mNekoL2DTexture.mainTexture = mNekoL2DRender.RenderTexture;
		mNekoL2DTexture.SetDimensions(400, 400);
		mNekoL2DTexture.transform.localPosition = new Vector3(-30f, 170f, 0f);
		mNekoL2DTexture.depth = depth + 3;
		mReLottery = Util.CreateJellyImageButton("ReLottery", mReLotteryRoot, EventMapAtlas);
		Util.SetImageButtonInfo(mReLottery, "LC_button_lottery", "LC_button_lottery", "LC_button_lottery", depth + 4, new Vector3(10f, -30f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mReLottery, mGSM, "OnReLotteryButtonPushed", UIButtonMessage.Trigger.OnClick);
		BoxCollider collider = mReLottery.GetComponent<BoxCollider>();
		if (collider != null)
		{
			collider.center = new Vector3(-10f, 38f, 0f);
			collider.size = new Vector3(115f, 110f, 1f);
		}
		mCockpitButtons.Add(mReLottery);
		Live2DLoadEnd = true;
	}

	protected override void OnDestroy()
	{
		if (mNekoL2DTexture != null)
		{
			Object.Destroy(mNekoL2DTexture.gameObject);
		}
		if (mNekoL2DRender != null)
		{
			Object.Destroy(mNekoL2DRender.gameObject);
		}
		base.OnDestroy();
	}

	public void UpdateReLotteryButton(bool setEnable)
	{
		if (mReLottery != null)
		{
			if (reLotteryJelly == null)
			{
				reLotteryJelly = mReLottery.GetComponent<JellyImageButton>();
			}
			reLotteryJelly.SetButtonEnable(setEnable);
			if (reLotteryCollider == null)
			{
				reLotteryCollider = mReLottery.GetComponent<BoxCollider>();
			}
			reLotteryCollider.enabled = setEnable;
		}
	}

	public void MoveReLotteryButton(bool outsideMode)
	{
		StartCoroutine(MoveReLotteryButtonStart(outsideMode));
	}

	private IEnumerator MoveReLotteryButtonStart(bool outsideMode)
	{
		if (mReLotteryRoot == null)
		{
			yield break;
		}
		while (IsMoveNow)
		{
			yield return null;
		}
		IsMoveNow = true;
		int moveBase = 1;
		float nowPosX = mReLotteryRoot.transform.localPosition.x;
		float nowPosY = mReLotteryRoot.transform.localPosition.y;
		if (outsideMode)
		{
			moveBase = -1;
		}
		for (int k = 1; k <= 270; k++)
		{
			mReLotteryRoot.transform.localPosition = new Vector3(nowPosX + (float)(k * moveBase), nowPosY, 0f);
			if (k % 20 == 0)
			{
				yield return null;
			}
			if (!outsideMode && mReLotteryRoot.transform.localPosition.x > 80f)
			{
				break;
			}
		}
		if (!outsideMode)
		{
			mReLotteryRoot.transform.localPosition = new Vector3(80f, 300f, 0f);
			for (int j = 1; j <= 3; j++)
			{
				mReLotteryRoot.transform.localPosition = new Vector3(mReLotteryRoot.transform.localPosition.x + (float)j, nowPosY, 0f);
				yield return null;
			}
			for (int i = 1; i <= 3; i++)
			{
				mReLotteryRoot.transform.localPosition = new Vector3(mReLotteryRoot.transform.localPosition.x - (float)i, nowPosY, 0f);
				yield return null;
			}
			mReLotteryRoot.transform.localPosition = new Vector3(80f, nowPosY, 0f);
		}
		else
		{
			mReLotteryRoot.transform.localPosition = new Vector3(-190f, nowPosY, 0f);
		}
		IsMoveNow = false;
	}

	public override IEnumerator JumpUpLive2dCharacter(CompanionData.MOTION motion)
	{
		if (mFukidashiSprite != null)
		{
			NGUITools.SetActive(mFukidashiSprite.gameObject, false);
		}
		yield return StartCoroutine(base.JumpUpLive2dCharacter(motion));
	}

	public override IEnumerator JumpDownLive2dCharacter(CompanionData.MOTION motion, CompanionData.MOTION after_loop_motion = CompanionData.MOTION.STAY0)
	{
		yield return StartCoroutine(base.JumpDownLive2dCharacter(motion, after_loop_motion));
		if (mFukidashiSprite != null)
		{
			NGUITools.SetActive(mFukidashiSprite.gameObject, true);
		}
	}

	public override void SetLayout(ScreenOrientation o)
	{
		base.SetLayout(o);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mCharacterTrans != null)
			{
				mCharacterTrans.SetParent(mAnchorC.transform);
				mCharacterTrans.SetLocalPosition(136f, 320f);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mCharacterTrans != null)
			{
				mCharacterTrans.SetParent(mAnchorBL.transform);
				mCharacterTrans.SetLocalPosition(463f, 320f);
			}
			break;
		}
		SetFukidashiLayout(o);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = Vector3.zero;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = new Vector3(0f, -200f, 0f);
			}
			break;
		}
	}

	private void SetFukidashiLayout(ScreenOrientation o)
	{
		if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
		{
			if (mFukidashiRoot != null)
			{
				mFukidashiRoot.transform.SetLocalScale(1f, 1f);
				mFukidashiRoot.transform.SetLocalPosition(0f, 0f, 0f);
			}
			if (mAtoNanbingoGO != null)
			{
				mAtoNanbingoGO.transform.SetLocalScale(1f, 1f);
			}
			if (mDeGetGO != null)
			{
				mDeGetGO.transform.SetLocalScale(1f, 1f);
			}
		}
		else
		{
			if (mFukidashiRoot != null)
			{
				mFukidashiRoot.transform.SetLocalScale(-1f, 1f);
				mFukidashiRoot.transform.SetLocalPosition(0f, -50f, 0f);
			}
			if (mAtoNanbingoGO != null)
			{
				mAtoNanbingoGO.transform.SetLocalScale(-1f, 1f);
			}
			if (mDeGetGO != null)
			{
				mDeGetGO.transform.SetLocalScale(-1f, 1f);
			}
		}
	}
}
