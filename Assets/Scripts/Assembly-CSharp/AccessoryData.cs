using System;

public class AccessoryData : ICloneable
{
	public enum ACCESSORY_TYPE
	{
		NONE = 0,
		PARTNER = 1,
		GROWUP = 2,
		GROWUP_PIECE = 3,
		GROWUP_F = 4,
		HEART = 5,
		HEART_PIECE = 6,
		SP_DAILY = 7,
		GEM = 8,
		BOOSTER = 9,
		RB_KEY = 10,
		WALL_PAPER = 11,
		WALL_PAPER_PIECE = 12,
		ERB_KEY = 13,
		REVIVAL_EVENT = 14,
		TROPHY = 15,
		RB_KEY_R = 16,
		RB_KEY_S = 17,
		RB_KEY_SS = 18,
		RB_KEY_STARS = 19,
		ADV_ITEM = 20,
		BINGO_KEY = 21,
		BINGO_TICKET = 22,
		OLDEVENT_KEY = 23,
		LABYRINTH_KEY = 24,
		RB_KEY_GF00 = 25,
		MAX = 26
	}

	public enum RECEIVE_TYPE
	{
		INFINITY = 0,
		ONCE = 1
	}

	public int Index;

	public Def.SERIES Series;

	public string Name;

	public ACCESSORY_TYPE AccessoryType;

	public string IconName;

	public string AccessoryUnlockScene;

	public string UnlockCondition;

	public string GotID;

	public int PageIndex;

	public string LastStageNo;

	public int IsStop;

	public string AnimeLayerName;

	public string ImageName;

	public int IsFlip;

	public int RelationID;

	public string DemoName;

	public string Description;

	public int EventID;

	public short CourseID;

	public int DailyID;

	public short ReceiveType = 1;

	public int MainStage;

	public int SubStage;

	public bool IsInfinityReceive
	{
		get
		{
			return ReceiveType == 0;
		}
	}

	public bool IsOnceReceive
	{
		get
		{
			return ReceiveType == 1;
		}
	}

	public bool IsMultiReceive
	{
		get
		{
			return !IsInfinityReceive && !IsOnceReceive;
		}
	}

	public bool HasDemo
	{
		get
		{
			return false;
		}
	}

	public AccessoryData()
	{
		Index = 0;
		Series = Def.SERIES.SM_FIRST;
		Name = string.Empty;
		AccessoryType = ACCESSORY_TYPE.NONE;
		IconName = string.Empty;
		AccessoryUnlockScene = string.Empty;
		UnlockCondition = string.Empty;
		GotID = "NONE";
		PageIndex = 0;
		LastStageNo = string.Empty;
		IsStop = 0;
		AnimeLayerName = "NONE";
		ImageName = "NONE";
		IsFlip = 0;
		RelationID = 0;
		DemoName = "NONE";
		Description = string.Empty;
		EventID = -1;
		CourseID = 0;
		DailyID = -1;
		ReceiveType = 1;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AccessoryData Clone()
	{
		return MemberwiseClone() as AccessoryData;
	}

	public void Deserialize(BIJBinaryReader a_reader)
	{
		Index = a_reader.ReadInt();
		Series = (Def.SERIES)a_reader.ReadShort();
		string strA = a_reader.ReadUTF();
		ACCESSORY_TYPE[] array = (ACCESSORY_TYPE[])Enum.GetValues(typeof(ACCESSORY_TYPE));
		for (int i = 0; i < array.Length; i++)
		{
			if (string.Compare(strA, array[i].ToString()) == 0)
			{
				AccessoryType = array[i];
				break;
			}
		}
		AccessoryUnlockScene = a_reader.ReadUTF();
		UnlockCondition = a_reader.ReadUTF();
		GotID = a_reader.ReadUTF();
		PageIndex = a_reader.ReadInt();
		LastStageNo = a_reader.ReadUTF();
		IsStop = a_reader.ReadInt();
		AnimeLayerName = a_reader.ReadUTF();
		ImageName = a_reader.ReadUTF();
		IsFlip = a_reader.ReadInt();
		RelationID = a_reader.ReadInt();
		EventID = a_reader.ReadInt();
		CourseID = a_reader.ReadShort();
		DailyID = a_reader.ReadInt();
		ReceiveType = a_reader.ReadShort();
		MainStage = 0;
		SubStage = 0;
		string[] array2 = LastStageNo.Split('_');
		if (array2.Length == 1)
		{
			MainStage = int.Parse(array2[0]);
		}
		else if (array2.Length == 2)
		{
			MainStage = int.Parse(array2[0]);
			SubStage = int.Parse(array2[1]);
		}
		if (Def.IsEventSeries(Series))
		{
			CourseID = (short)(MainStage / 1000);
			MainStage %= 1000;
		}
	}

	public int GetGotIDByNum()
	{
		int result = -1;
		if (!int.TryParse(GotID, out result))
		{
			result = -1;
		}
		return result;
	}

	public int GetNum()
	{
		int a_num = 0;
		switch (AccessoryType)
		{
		case ACCESSORY_TYPE.PARTNER:
		case ACCESSORY_TYPE.GROWUP_F:
		case ACCESSORY_TYPE.RB_KEY:
		case ACCESSORY_TYPE.WALL_PAPER:
		case ACCESSORY_TYPE.WALL_PAPER_PIECE:
		case ACCESSORY_TYPE.ERB_KEY:
		case ACCESSORY_TYPE.RB_KEY_R:
		case ACCESSORY_TYPE.RB_KEY_S:
		case ACCESSORY_TYPE.RB_KEY_SS:
		case ACCESSORY_TYPE.RB_KEY_STARS:
		case ACCESSORY_TYPE.RB_KEY_GF00:
			a_num = 1;
			break;
		case ACCESSORY_TYPE.GROWUP:
		case ACCESSORY_TYPE.GROWUP_PIECE:
		case ACCESSORY_TYPE.HEART:
		case ACCESSORY_TYPE.HEART_PIECE:
		case ACCESSORY_TYPE.SP_DAILY:
		case ACCESSORY_TYPE.TROPHY:
		case ACCESSORY_TYPE.OLDEVENT_KEY:
		case ACCESSORY_TYPE.LABYRINTH_KEY:
			a_num = GetGotIDByNum();
			break;
		case ACCESSORY_TYPE.BOOSTER:
		{
			Def.BOOSTER_KIND a_kind;
			GetBooster(out a_kind, out a_num);
			break;
		}
		}
		return a_num;
	}

	public int GetDemoByNum()
	{
		int result = -1;
		if (!int.TryParse(DemoName, out result))
		{
			result = -1;
		}
		return result;
	}

	public int GetCompanionID()
	{
		if (AccessoryType == ACCESSORY_TYPE.PARTNER)
		{
			return GetGotIDByNum();
		}
		return -1;
	}

	public void GetBooster(out Def.BOOSTER_KIND a_kind, out int a_num)
	{
		a_kind = Def.BOOSTER_KIND.NONE;
		a_num = 0;
		if (GotID.Contains("_"))
		{
			string[] array = GotID.Split('_');
			int result;
			if (int.TryParse(array[0], out result))
			{
				a_kind = (Def.BOOSTER_KIND)result;
			}
			int result2;
			if (int.TryParse(array[1], out result2))
			{
				a_num = result2;
			}
		}
	}

	public int GetWallPaperIndex()
	{
		return IsFlip;
	}

	public int GetWallPaperPiecePlace()
	{
		int result = -1;
		if (!int.TryParse(LastStageNo, out result))
		{
			result = -1;
		}
		return result;
	}

	public int GetWallPaperPieceIndex()
	{
		return GetWallPaperPiecePlace();
	}

	public int GetRBKeyStageNo()
	{
		int result = -1;
		if (!int.TryParse(LastStageNo, out result))
		{
			result = -1;
		}
		return result;
	}
}
