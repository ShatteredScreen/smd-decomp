using UnityEngine;

public class GooglePlusPlugin : MonoBehaviour
{
	private static GooglePlusPlugin mInstance;

	private GooglePlusIF mPluginIF;

	public static GooglePlusPlugin Instance
	{
		get
		{
			if (mInstance == null)
			{
				GameObject gameObject = new GameObject("GooglePlus");
				mInstance = gameObject.GetComponent<GooglePlusPlugin>();
				if (mInstance == null)
				{
					mInstance = gameObject.AddComponent<GooglePlusPlugin>();
				}
			}
			return mInstance;
		}
	}

	public static GooglePlusIF PluginIF
	{
		get
		{
			return Instance.mPluginIF;
		}
	}

	private void Awake()
	{
		if (mInstance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		mInstance = this;
		Object.DontDestroyOnLoad(base.gameObject);
		mPluginIF = base.gameObject.AddComponent<GooglePlusAndroid>();
	}

	private void OnDestroy()
	{
	}

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnApplicationPause(bool pause)
	{
	}
}
