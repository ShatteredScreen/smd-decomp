public interface IGiftItem
{
	int FromID { get; }

	string Message { get; }

	int ItemCategory { get; }

	int ItemKind { get; }

	int ItemID { get; }

	int Quantity { get; }
}
