using System.Collections;
using UnityEngine;

public class SeriesCampaignButton : MonoBehaviour
{
	public delegate void OnPush(Def.SERIES a_series);

	private Def.SERIES mSeries;

	private UISprite mChara;

	private UISprite mPopup;

	private UIButton mButton;

	private OnPush mCallback;

	private int mDepth;

	private Coroutine mButtonAnime;

	private bool mIsAnimeStop;

	public bool IsLoaded { get; private set; }

	public void SetCallback(OnPush a_callback)
	{
		mCallback = a_callback;
	}

	public static SeriesCampaignButton Make(GameObject a_parent, Def.SERIES a_series, Vector3 a_pos, int a_depth)
	{
		SeriesCampaignButton seriesCampaignButton = Util.CreateGameObject("SeriesButton" + a_series, a_parent).AddComponent<SeriesCampaignButton>();
		seriesCampaignButton.mSeries = a_series;
		seriesCampaignButton.mDepth = a_depth;
		seriesCampaignButton.transform.localPosition = a_pos;
		seriesCampaignButton.StartCoroutine(seriesCampaignButton.Load());
		return seriesCampaignButton;
	}

	public IEnumerator Load()
	{
		ResImage image = ResourceManager.LoadImageAsync("SERIESCAMPAIGN");
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		mButton = Util.CreateJellyImageButton("JellyButton", base.gameObject, image.Image);
		Util.SetImageButtonInfo(mButton, "null", "null", "null", mDepth - 1, Vector3.zero, Vector3.one);
		mChara = Util.CreateSprite("SCChara", mButton.gameObject, image.Image);
		Util.SetSpriteInfo(mChara, string.Format("campaign_chara{0:00}", (int)mSeries), mDepth + 10, new Vector3(0f, 0f, 0f), Vector3.one, false, UIWidget.Pivot.Bottom);
		mPopup = Util.CreateSprite("SCPop", mButton.gameObject, image.Image);
		Util.SetSpriteInfo(mPopup, "LC_campaign_hukidashi01", mDepth + 11, new Vector3(0f, -15f, 0f), Vector3.one, false);
		BoxCollider collider = mButton.GetComponent<BoxCollider>();
		collider.size = new Vector3(130f, 100f, 1f);
		collider.center = new Vector3(0f, 20f, 0f);
		Util.SetImageButtonMessage(mButton, this, "OnPushFunc", UIButtonMessage.Trigger.OnClick);
		IsLoaded = true;
		mButtonAnime = StartCoroutine(ButtonAnime());
	}

	public IEnumerator ButtonAnime()
	{
		Transform t = mPopup.gameObject.transform;
		Vector3 tempPos = t.localPosition;
		Util.TriFunc func = new Util.TriFunc(0f, 180f, Util.TriFunc.MAXFUNC.LOOP);
		float deg = 0f;
		bool isFirstBound = true;
		while (!mIsAnimeStop)
		{
			deg = Time.deltaTime * 540f * ((!isFirstBound) ? 1.5f : 0.8f);
			bool isMax = func.AddDegree(deg);
			float y = func.Sin() * 20f * ((!isFirstBound) ? 0.5f : 1f);
			t.localPosition = new Vector3(tempPos.x, tempPos.y + y, tempPos.z);
			if (isMax)
			{
				if (isFirstBound)
				{
					isFirstBound = false;
				}
				else
				{
					yield return new WaitForSeconds(1.8f);
					isFirstBound = true;
				}
			}
			yield return null;
		}
		t.localPosition = tempPos;
	}

	public void SetEnable(bool a_flg)
	{
		if (mButton != null)
		{
			JellyImageButton component = mButton.GetComponent<JellyImageButton>();
			if (component != null)
			{
				component.SetEnable(a_flg);
				mIsAnimeStop = !a_flg;
			}
		}
	}

	private void OnPushFunc(GameObject a_obj)
	{
		JellyImageButton component = a_obj.GetComponent<JellyImageButton>();
		if (component != null && component.IsEnable() && mCallback != null)
		{
			mCallback(mSeries);
		}
	}
}
