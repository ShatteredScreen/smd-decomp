using System;
using System.IO;
using System.Text;

public class BIJEncryptDataWriter : BIJBinaryWriter
{
	private MemoryStream mStream;

	public BIJEncryptDataWriter(string name)
		: this(name, Encoding.UTF8, true)
	{
	}

	public BIJEncryptDataWriter(string name, Encoding encoding)
		: this(name, encoding, true)
	{
	}

	public BIJEncryptDataWriter(string name, Encoding encoding, bool mkdir)
	{
		mStream = null;
		Writer = null;
		try
		{
			mPath = System.IO.Path.Combine(getBasePath(), name);
			if (mkdir)
			{
				string directoryName = System.IO.Path.GetDirectoryName(mPath);
				if (!string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
				{
					Directory.CreateDirectory(directoryName);
				}
			}
			mStream = new MemoryStream();
			Writer = new BinaryWriter(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	private static string getBasePath()
	{
		return BIJUnity.getDataPath();
	}

	public override void Close()
	{
		if (mStream == null)
		{
			return;
		}
		byte[] bytes = CryptUtils.EncryptRJ128(mStream.ToArray());
		File.WriteAllBytes(mPath, bytes);
		base.Close();
		try
		{
			if (mStream != null)
			{
				mStream.Close();
			}
		}
		catch (Exception)
		{
		}
		mStream = null;
	}
}
