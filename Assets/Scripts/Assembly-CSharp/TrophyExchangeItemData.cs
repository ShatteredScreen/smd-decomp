using System;

public class TrophyExchangeItemData : ICloneable
{
	private bool mIsSetBundle;

	public int Index { get; set; }

	public int UseDefaultTrophy { get; set; }

	public int UseTrophy { get; set; }

	public int SaleNum { get; set; }

	public int DefaultNum { get; set; }

	public string Detail { get; set; }

	public string IconName { get; set; }

	public string Name { get; set; }

	public string Desc { get; set; }

	public int Category { get; set; }

	public int ItemId { get; set; }

	public string str { get; set; }

	public bool IsSetBundle
	{
		get
		{
			return mIsSetBundle;
		}
	}

	public TrophyItemDetail ItemDetail { get; set; }

	public bool IsSale
	{
		get
		{
			return SaleNum == 1;
		}
	}

	public TrophyExchangeItemData()
	{
		Index = 0;
		UseDefaultTrophy = 0;
		UseTrophy = 0;
		SaleNum = 0;
		DefaultNum = 0;
		Detail = string.Empty;
		IconName = string.Empty;
		Name = string.Empty;
		Desc = string.Empty;
		Category = 0;
		ItemId = 0;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void deserialize(BIJBinaryReader stream)
	{
		Index = stream.ReadShort();
		UseDefaultTrophy = stream.ReadShort();
		UseTrophy = stream.ReadShort();
		SaleNum = stream.ReadShort();
		DefaultNum = stream.ReadShort();
		Detail = stream.ReadUTF();
		IconName = stream.ReadUTF();
		Name = stream.ReadUTF();
		Desc = stream.ReadUTF();
		Category = stream.ReadShort();
		ItemId = stream.ReadShort();
		TrophyItemDetail obj = new TrophyItemDetail();
		new MiniJSONSerializer().Populate(ref obj, Detail);
		ItemDetail = obj;
		ItemDetail.SetItem();
		mIsSetBundle = ItemDetail.IsSetBundle();
	}

	public TrophyExchangeItemData Clone()
	{
		return MemberwiseClone() as TrophyExchangeItemData;
	}
}
