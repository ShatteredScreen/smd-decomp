using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(UIPanel))]
public class DialogBase : MonoBehaviour
{
	public enum BASE_STATE
	{
		INIT = 0,
		OPEN = 1,
		MAIN = 2,
		CLOSE = 3,
		AFTER_CLOSE = 4,
		WAIT = 5,
		EULA_WAIT = 6,
		MAINTENANCE_WAIT = 7,
		MAINTENANCE_DIALOG_WAIT = 8
	}

	public enum CALLBACK_STATE
	{
		OnOpening = 0,
		OnOpenFinished = 1,
		OnScreenChanged = 2,
		OnBackKeyPress = 3,
		OnCloseFinished = 4
	}

	public enum MAINTENANCE_RESULT
	{
		ERROR = -1,
		CHECKING = 0,
		OK = 1,
		IN_MAINTENANCE = 2
	}

	public enum DIALOG_SIZE
	{
		XSMALL = 0,
		SMALL = 1,
		MEDIUM = 2,
		LARGE = 3,
		XLARGE = 4
	}

	public enum DIALOG_STYLE
	{
		TITLED = 0,
		NONTITLED = 1
	}

	protected StatusManager<BASE_STATE> mBaseState = new StatusManager<BASE_STATE>(BASE_STATE.INIT);

	protected GameMain mGame;

	protected JellyDialog mJelly;

	protected Def.DIALOG_PRODUCT mJellyflg;

	protected float mJellyDialogOpenSpeed = 1f;

	protected float mJellyDialogCloseSpeed = 1f;

	protected bool mParticleFeather = true;

	protected int mDialogDepth = 50;

	protected int mDialogUsedDepth;

	protected int mBaseDepth = 50;

	protected float mBaseZ;

	protected float mAfterCloseWaitTimer;

	protected UISprite mTitleBoard;

	protected UISprite mDialogFrame;

	protected int mDialogFrameWidth;

	protected bool mOpenSeEnable = true;

	protected bool mOpenImmediate;

	protected bool mCloseImmediate;

	private bool mReuseDialog;

	protected bool mFocus;

	protected UIPanel mPanel;

	protected string mFrameTitleSpriteName = "popup_panel_title";

	protected string mFrameSpriteName = "instruction_panel";

	protected int mDialogTitlesize;

	protected string mLoadImageAtlas = "DIALOG_BASE";

	protected UISprite mGemFrame;

	protected UILabel mGemNumLabel;

	protected int mGemDisplayNum;

	protected UIButton mEulaButton;

	protected Vector3 mGemFramePositionOffsetP = Vector3.zero;

	protected Vector3 mGemFramePositionOffsetL = Vector3.zero;

	protected static readonly Vector2 BUILD_DIALOG_POSITION_OFFSET = new Vector2(0f, 0f);

	protected ScreenOrientation mOrientation;

	private bool mBuildFinished;

	private bool mMaintenanceCheckOnStart;

	private bool mCloseOnMaintenance;

	protected ConnectCommonErrorDialog mMaintenanceDialog;

	protected ConnectCommonErrorDialog mMaintenanceErrorDialog;

	private ConnectCommonErrorDialog.STYLE mMaintenanceDialogStyle;

	private ConnectCommonErrorDialog.STYLE mMaintenanceErrorDialogStyle;

	private UnityEvent[] mCallback;

	protected static float FRAME_EDGE_WIDTH = 10f;

	public GameObject ParentGameObject
	{
		get
		{
			return base.gameObject.transform.parent.gameObject;
		}
	}

	public bool ParticleFeather
	{
		set
		{
			mParticleFeather = value;
		}
	}

	public virtual void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mGame.mDialogManager.GetNewDialogDepth(out mDialogDepth, out mBaseZ);
		mGame.mDialogManager.Resister(this);
		mPanel = GetComponent<UIPanel>();
		mPanel.depth = mDialogDepth;
		mPanel.sortingOrder = mDialogDepth;
		mBuildFinished = false;
		mDialogFrameWidth = 0;
		if (mJellyflg == Def.DIALOG_PRODUCT.NONE)
		{
			mJelly = base.gameObject.AddComponent<JellyDialog>();
		}
		else if (mJellyflg == Def.DIALOG_PRODUCT.EVENT_DIALOG)
		{
			mJelly = base.gameObject.AddComponent<JellySecondDialog>();
			mJelly.SetTargetScale(1f);
		}
		else if (mJellyflg == Def.DIALOG_PRODUCT.TROPHY_DIALOG)
		{
			mJelly = base.gameObject.AddComponent<JellyThirdDialog>();
		}
		else if (mJellyflg == Def.DIALOG_PRODUCT.LOSE_DIALOG)
		{
			mJelly = base.gameObject.AddComponent<JellySecondDialog>();
		}
		mMaintenanceCheckOnStart = false;
		mCloseOnMaintenance = false;
	}

	public virtual void Start()
	{
		if (mBaseState.GetStatus() == BASE_STATE.INIT)
		{
			mOrientation = Util.ScreenOrientation;
			StartCoroutine(Build());
		}
	}

	public virtual void Update()
	{
		switch (mBaseState.GetStatus())
		{
		case BASE_STATE.INIT:
			if (!mBuildFinished)
			{
				break;
			}
			if (mReuseDialog)
			{
				mJelly.Reset();
				mReuseDialog = false;
				break;
			}
			OnOpening();
			InvokeCallback(CALLBACK_STATE.OnOpening);
			mJelly.Open(mOpenImmediate);
			mBaseState.Change(BASE_STATE.OPEN);
			if (mOpenSeEnable)
			{
				mGame.PlaySe("SE_DIALOG_OPEN", -1);
			}
			break;
		case BASE_STATE.OPEN:
			if (mJelly.IsMoveFinished())
			{
				OnOpenFinished();
				InvokeCallback(CALLBACK_STATE.OnOpenFinished);
				if (mMaintenanceCheckOnStart)
				{
					mBaseState.Change(BASE_STATE.MAINTENANCE_WAIT);
				}
				else
				{
					mBaseState.Change(BASE_STATE.MAIN);
				}
			}
			break;
		case BASE_STATE.MAIN:
			if (mOrientation != Util.ScreenOrientation)
			{
				OnScreenChanged(Util.ScreenOrientation);
				InvokeCallback(CALLBACK_STATE.OnScreenChanged);
				SetLayoutGemDisp(Util.ScreenOrientation);
				mOrientation = Util.ScreenOrientation;
			}
			if (mGame.mBackKeyTrg && !mGame.mTutorialManager.IsTutorialPlaying())
			{
				OnBackKeyPress();
				InvokeCallback(CALLBACK_STATE.OnBackKeyPress);
			}
			break;
		case BASE_STATE.CLOSE:
			if (mJelly.IsMoveFinished())
			{
				mAfterCloseWaitTimer = 0.2f;
				NGUITools.SetActiveChildren(base.gameObject, false);
				mReuseDialog = true;
				mBaseState.Change(BASE_STATE.AFTER_CLOSE);
			}
			break;
		case BASE_STATE.AFTER_CLOSE:
			mAfterCloseWaitTimer -= Time.deltaTime;
			if (mAfterCloseWaitTimer < 0f)
			{
				mBaseState.Change(BASE_STATE.WAIT);
				InvokeCallback(CALLBACK_STATE.OnCloseFinished);
				OnCloseFinished();
			}
			break;
		case BASE_STATE.MAINTENANCE_WAIT:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (!GameMain.MaintenanceMode)
				{
					mBaseState.Change(BASE_STATE.MAIN);
					break;
				}
				mMaintenanceDialog = Util.CreateGameObject("MaintenanceDialog", ParentGameObject).AddComponent<ConnectCommonErrorDialog>();
				mMaintenanceDialog.Init(string.Empty, string.Empty, mMaintenanceDialogStyle);
				mMaintenanceDialog.SetClosedCallback(OnMaintenanceDialogClosed);
				mBaseState.Change(BASE_STATE.MAINTENANCE_DIALOG_WAIT);
			}
			else
			{
				mMaintenanceErrorDialog = Util.CreateGameObject("MaintenanceCheckErrorDialog", ParentGameObject).AddComponent<ConnectCommonErrorDialog>();
				mMaintenanceErrorDialog.Init(string.Empty, string.Empty, mMaintenanceErrorDialogStyle);
				mMaintenanceErrorDialog.SetClosedCallback(OnMaintenanceCheckErrorDialogClosed);
				mBaseState.Change(BASE_STATE.MAINTENANCE_DIALOG_WAIT);
			}
			break;
		}
		mBaseState.Update();
		UpdateGemDisp();
	}

	public virtual void OnDestroy()
	{
		mGame.mDialogManager.Unresister(this);
	}

	public virtual void OnOpening()
	{
		NGUITools.SetActiveChildren(base.gameObject, true);
		if (mParticleFeather)
		{
			mGame.mDialogManager.OpenParticle(base.transform.localPosition);
		}
	}

	public virtual void OnOpenFinished()
	{
	}

	public virtual void OnCloseFinished()
	{
	}

	public virtual void OnBackKeyPress()
	{
	}

	private IEnumerator Build()
	{
		yield return StartCoroutine(BuildResource());
		BuildDialog();
		mBuildFinished = true;
	}

	public virtual IEnumerator BuildResource()
	{
		yield return 0;
	}

	public virtual void BuildDialog()
	{
	}

	public virtual void Close()
	{
		mJelly.Close(mCloseImmediate);
		mBaseState.Reset(BASE_STATE.CLOSE, true);
	}

	public virtual bool IsClosing()
	{
		switch (mBaseState.GetStatus())
		{
		case BASE_STATE.CLOSE:
		case BASE_STATE.AFTER_CLOSE:
		case BASE_STATE.WAIT:
			return true;
		default:
			return false;
		}
	}

	public virtual bool IsClosed()
	{
		BASE_STATE status = mBaseState.GetStatus();
		if (status == BASE_STATE.AFTER_CLOSE || status == BASE_STATE.WAIT)
		{
			return true;
		}
		return false;
	}

	public bool GetBusy()
	{
		if (mBaseState.GetStatus() == BASE_STATE.MAIN)
		{
			return false;
		}
		return true;
	}

	public void SetFrameTitleSpriteName(string name)
	{
		mFrameTitleSpriteName = name;
	}

	public void SetFrameSpriteName(string name)
	{
		mFrameSpriteName = name;
	}

	public void SetFrameSpriteBaceAtlas(string atlas)
	{
		mLoadImageAtlas = atlas;
	}

	public void SetDialogTitleSize(int size)
	{
		mDialogTitlesize = size;
	}

	public virtual void InitDialogFrame(DIALOG_SIZE size, string atlasKey = null, string spriteKey = null, int forceWidth = 0, int forceHeight = 0)
	{
		if (string.IsNullOrEmpty(atlasKey))
		{
			atlasKey = "DIALOG_BASE";
		}
		if (string.IsNullOrEmpty(spriteKey))
		{
			spriteKey = "instruction_panel";
		}
		ResImage resImage = ResourceManager.LoadImage(atlasKey);
		mDialogFrame = Util.CreateSprite("DlgBase", base.gameObject, resImage.Image);
		Util.SetSpriteInfo(mDialogFrame, spriteKey, mDialogDepth + mDialogUsedDepth, Vector3.zero, Vector3.one, false);
		mDialogFrame.type = UIBasicSprite.Type.Sliced;
		mDialogUsedDepth++;
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		int w = 590;
		if (forceWidth > 0)
		{
			w = forceWidth;
		}
		int h = 0;
		switch (size)
		{
		case DIALOG_SIZE.XSMALL:
			h = 180;
			break;
		case DIALOG_SIZE.SMALL:
			h = 220;
			break;
		case DIALOG_SIZE.MEDIUM:
			h = 390;
			break;
		case DIALOG_SIZE.LARGE:
			h = 600;
			break;
		case DIALOG_SIZE.XLARGE:
			h = 900;
			break;
		}
		if (forceHeight > 0)
		{
			h = forceHeight;
		}
		base.gameObject.transform.localPosition = BUILD_DIALOG_POSITION_OFFSET;
		mDialogFrame.SetDimensions(w, h);
		boxCollider.center = new Vector3(0f, 12.5f, 0f);
		if (size == DIALOG_SIZE.XLARGE)
		{
			boxCollider.size = new Vector3(700f, 925f, 1f);
		}
		else
		{
			boxCollider.size = new Vector3(700f, 625f, 1f);
		}
		mDialogFrameWidth = w;
		UpdateBaseDepth();
	}

	public virtual void InitDialogTitle(string titleDescKey, string atlasKey = null, string spriteKey = null, int forceWidth = 0, int forceHeight = 0)
	{
		if (string.IsNullOrEmpty(atlasKey))
		{
			atlasKey = "DIALOG_BASE";
		}
		if (string.IsNullOrEmpty(spriteKey))
		{
			spriteKey = "popup_panel_title";
		}
		ResImage resImage = ResourceManager.LoadImage(atlasKey);
		UIFont atlasFont = GameMain.LoadFont();
		UILabel uILabel = null;
		Vector2 localSize = mDialogFrame.localSize;
		int w = 442;
		if (forceWidth != 0)
		{
			w = forceWidth;
		}
		int h = 85;
		if (forceHeight != 0)
		{
			h = forceHeight;
		}
		mTitleBoard = Util.CreateSprite("TitleBoard", base.gameObject, resImage.Image);
		Util.SetSpriteInfo(mTitleBoard, spriteKey, mDialogDepth + mDialogUsedDepth, new Vector3(0f, localSize.y / 2f - FRAME_EDGE_WIDTH, 0f), Vector3.one, false);
		mTitleBoard.type = UIBasicSprite.Type.Sliced;
		mTitleBoard.SetDimensions(w, h);
		mDialogUsedDepth++;
		uILabel = Util.CreateLabel("Title", mTitleBoard.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mDialogDepth + mDialogUsedDepth, new Vector3(0f, -2f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
		mDialogUsedDepth++;
		Util.SetLabelText(uILabel, titleDescKey);
		uILabel.color = Def.DEFAULT_DIALOG_TITLE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(370, 38);
		UpdateBaseDepth();
	}

	public virtual void InitDialogFrame(DIALOG_SIZE size, DIALOG_STYLE style, string title, bool makeCharm = true, int forceWidth = 0, int forceHeight = 0)
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage(mLoadImageAtlas);
		UIFont atlasFont = GameMain.LoadFont();
		UILabel uILabel = null;
		mDialogFrame = Util.CreateSprite("DlgBase", base.gameObject, resImage2.Image);
		Util.SetSpriteInfo(mDialogFrame, mFrameSpriteName, mDialogDepth + mDialogUsedDepth, Vector3.zero, Vector3.one, false);
		mDialogUsedDepth++;
		mDialogFrame.type = UIBasicSprite.Type.Advanced;
		mDialogFrame.centerType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.leftType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.topType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.rightType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.bottomType = UIBasicSprite.AdvancedType.Tiled;
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		int w = 590;
		if (forceWidth > 0)
		{
			w = forceWidth;
		}
		int h = 0;
		switch (size)
		{
		case DIALOG_SIZE.XSMALL:
			h = 180;
			break;
		case DIALOG_SIZE.SMALL:
			h = 220;
			break;
		case DIALOG_SIZE.MEDIUM:
			h = 390;
			break;
		case DIALOG_SIZE.LARGE:
			h = 600;
			break;
		case DIALOG_SIZE.XLARGE:
			h = 900;
			break;
		}
		if (forceHeight > 0)
		{
			h = forceHeight;
		}
		mDialogFrameWidth = w;
		float y = -5f;
		switch (size)
		{
		case DIALOG_SIZE.XSMALL:
			mDialogFrame.SetDimensions(w, h);
			base.gameObject.transform.localPosition = new Vector3(0f, y, 0f);
			boxCollider.center = new Vector3(0f, 12.5f, 0f);
			boxCollider.size = new Vector3(700f, 625f, 1f);
			break;
		case DIALOG_SIZE.SMALL:
			mDialogFrame.SetDimensions(w, h);
			base.gameObject.transform.localPosition = new Vector3(0f, y, 0f);
			boxCollider.center = new Vector3(0f, 12.5f, 0f);
			boxCollider.size = new Vector3(700f, 625f, 1f);
			break;
		case DIALOG_SIZE.MEDIUM:
			mDialogFrame.SetDimensions(w, h);
			base.gameObject.transform.localPosition = new Vector3(0f, y, 0f);
			boxCollider.center = new Vector3(0f, 12.5f, 0f);
			boxCollider.size = new Vector3(700f, 625f, 1f);
			break;
		case DIALOG_SIZE.LARGE:
			mDialogFrame.SetDimensions(w, h);
			base.gameObject.transform.localPosition = new Vector3(0f, y, 0f);
			boxCollider.center = new Vector3(0f, 12.5f, 0f);
			boxCollider.size = new Vector3(700f, 625f, 1f);
			break;
		case DIALOG_SIZE.XLARGE:
			mDialogFrame.SetDimensions(w, h);
			base.gameObject.transform.localPosition = new Vector3(0f, y, 0f);
			boxCollider.center = new Vector3(0f, 12.5f, 0f);
			boxCollider.size = new Vector3(700f, 925f, 1f);
			break;
		}
		Vector2 localSize = mDialogFrame.localSize;
		if (style == DIALOG_STYLE.TITLED)
		{
			mTitleBoard = Util.CreateSprite("TitleBoard", base.gameObject, resImage.Image);
			Util.SetSpriteInfo(mTitleBoard, mFrameTitleSpriteName, mDialogDepth + mDialogUsedDepth, new Vector3(0f, localSize.y / 2f - FRAME_EDGE_WIDTH, 0f), Vector3.one, false);
			mDialogUsedDepth++;
			if (mDialogTitlesize != 0)
			{
				mTitleBoard.SetDimensions(mDialogTitlesize, 85);
			}
			uILabel = Util.CreateLabel("Title", mTitleBoard.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty, mDialogDepth + mDialogUsedDepth, new Vector3(0f, -2f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
			mDialogUsedDepth++;
			Util.SetLabelText(uILabel, title);
			uILabel.color = new Color(31f / 51f, 0.6f, 31f / 85f, 1f);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(370, 38);
		}
		UpdateBaseDepth();
	}

	protected void UpdateBaseDepth()
	{
		mBaseDepth = mDialogDepth + mDialogUsedDepth + 1;
	}

	public void SetJellyTargetScale(float scale)
	{
		if (mJelly != null)
		{
			mJelly.SetTargetScale(scale);
		}
	}

	public void SetOpenSeEnable(bool enable)
	{
		mOpenSeEnable = enable;
	}

	public void SetOpenImmediate(bool immediate)
	{
		mOpenImmediate = immediate;
	}

	public void SetCloseImmediate(bool immediate)
	{
		mCloseImmediate = immediate;
	}

	public void SetBaseDepth(int depth)
	{
	}

	public int GetSortingOder()
	{
		return mPanel.sortingOrder;
	}

	public UISprite GetTitleBoard()
	{
		return mTitleBoard;
	}

	public UISprite GetDialogFrame()
	{
		return mDialogFrame;
	}

	protected Vector3 GetPixelPos(Vector2 posRatio)
	{
		if (mDialogFrame == null)
		{
			return Vector3.zero;
		}
		Vector2 localSize = mDialogFrame.localSize;
		return new Vector3(localSize.x / 2f * posRatio.x, localSize.y / 2f * posRatio.y, 0f);
	}

	public void OnFocus()
	{
		mFocus = true;
	}

	public void OnFocusLost()
	{
		mFocus = false;
	}

	public virtual void OnScreenChanged(ScreenOrientation o)
	{
	}

	public virtual UIButton CreateCancelButton(string funcName)
	{
		if (mDialogFrame == null)
		{
			return null;
		}
		Vector2 localSize = mDialogFrame.localSize;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIButton uIButton = Util.CreateJellyImageButton("ButtonCancel", base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, "button_return", "button_return", "button_return", mBaseDepth + 10, new Vector3(localSize.x / 2f - FRAME_EDGE_WIDTH, localSize.y / 2f - FRAME_EDGE_WIDTH, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, funcName, UIButtonMessage.Trigger.OnClick);
		return uIButton;
	}

	public virtual void CreateGemWindow(bool isEula = false)
	{
		if (!(mDialogFrame == null))
		{
			Vector2 localSize = mDialogFrame.localSize;
			Vector3 vector = ((Util.ScreenOrientation != ScreenOrientation.LandscapeLeft && Util.ScreenOrientation != ScreenOrientation.LandscapeRight) ? mGemFramePositionOffsetP : mGemFramePositionOffsetL);
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			UIFont atlasFont = GameMain.LoadFont();
			mGemFrame = Util.CreateSprite("GemFrame", base.gameObject, image);
			Util.SetSpriteInfo(mGemFrame, "instruction_panel8", mBaseDepth, new Vector3(-180f + vector.x, (0f - localSize.y) / 2f - 30f + vector.y, 0f), Vector3.one, false);
			mGemFrame.type = UIBasicSprite.Type.Sliced;
			mGemFrame.SetDimensions(190, 70);
			UISprite sprite = Util.CreateSprite("GemIcon", mGemFrame.gameObject, image);
			Util.SetSpriteInfo(sprite, "jem_panel", mBaseDepth + 1, new Vector3(-4f, 0f, 0f), Vector3.one, false);
			mGemDisplayNum = mGame.mPlayer.Dollar;
			mGemNumLabel = Util.CreateLabel("GemNum", mGemFrame.gameObject, atlasFont);
			Util.SetLabelInfo(mGemNumLabel, string.Empty + mGemDisplayNum, mBaseDepth + 2, new Vector3(16f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			mGemNumLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			SetLayoutGemDisp(Util.ScreenOrientation);
		}
	}

	private void UpdateGemDisp()
	{
		if (mGemFrame != null && mGemDisplayNum != mGame.mPlayer.Dollar)
		{
			mGemDisplayNum = mGame.mPlayer.Dollar;
			mGemNumLabel.text = string.Empty + mGemDisplayNum;
		}
	}

	protected virtual void SetLayoutGemDisp(ScreenOrientation o)
	{
		if (mGemFrame == null)
		{
			return;
		}
		Vector2 localSize = mDialogFrame.localSize;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mGemFrame.transform.localPosition = new Vector3(-194f + mGemFramePositionOffsetP.x, (0f - localSize.y) / 2f - 30f + mGemFramePositionOffsetP.y, 0f);
			if (mEulaButton != null)
			{
				Vector3 localPosition2 = mGemFrame.transform.localPosition;
				localPosition2.x = (localSize.x - 210f) / 2f - 30f;
				mEulaButton.transform.localPosition = localPosition2;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mGemFrame.transform.localPosition = new Vector3(localSize.x / 2f + 95f + mGemFramePositionOffsetL.x, (0f - localSize.y) / 2f + 50f + mGemFramePositionOffsetL.y, 0f);
			if (mEulaButton != null)
			{
				Vector3 localPosition = mGemFrame.transform.localPosition;
				localPosition.y += (mGemFrame.localSize.y + 38f) / 2f + 10f;
				mEulaButton.transform.localPosition = localPosition;
			}
			break;
		}
	}

	public virtual void OnPushedEulaDisplay()
	{
		if (!GetBusy())
		{
			mBaseState.Change(BASE_STATE.EULA_WAIT);
			EULADialog eULADialog = Util.CreateGameObject("EULADialog", base.gameObject).AddComponent<EULADialog>();
			eULADialog.Init(EULADialog.MODE.ASCT);
			eULADialog.SetCallback(OnEULADialogClosed);
		}
	}

	public virtual void OnEULADialogClosed(EULADialog.SELECT_ITEM item)
	{
		mBaseState.Change(BASE_STATE.MAIN);
	}

	public static void SetDialogLabelEffect(UILabel label)
	{
		label.overflowMethod = UILabel.Overflow.ResizeHeight;
		label.color = Def.DEFAULT_MESSAGE_COLOR;
		label.fontSize = 26;
		label.spacingY = 6;
	}

	public static void SetDialogLabelEffect2(UILabel label)
	{
		label.color = Def.DEFAULT_MESSAGE_COLOR;
		label.fontSize = 26;
		label.spacingY = 6;
	}

	public IEnumerator UnloadUnusedAssets()
	{
		yield return Resources.UnloadUnusedAssets();
	}

	public void WillNotOpen()
	{
		NGUITools.SetActiveChildren(base.gameObject, false);
		mBaseState.Reset(BASE_STATE.AFTER_CLOSE, true);
	}

	private void _MaintenanceCheckStart()
	{
		if (!mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty))
		{
		}
	}

	protected bool MaintenanceCheckOnStart(ConnectCommonErrorDialog.STYLE aDialogStyle, ConnectCommonErrorDialog.STYLE aErrorDialogStyle, bool isClose = false)
	{
		if (mBaseState.GetStatus() != 0)
		{
			return false;
		}
		mMaintenanceDialogStyle = aDialogStyle;
		mMaintenanceErrorDialogStyle = aErrorDialogStyle;
		mCloseOnMaintenance = isClose;
		_MaintenanceCheckStart();
		mMaintenanceCheckOnStart = true;
		return true;
	}

	protected bool MaintenanceCheck(ConnectCommonErrorDialog.STYLE aDialogStyle, ConnectCommonErrorDialog.STYLE aErrorDialogStyle, bool isClose = false)
	{
		if (GetBusy())
		{
			return false;
		}
		mMaintenanceDialogStyle = aDialogStyle;
		mMaintenanceErrorDialogStyle = aErrorDialogStyle;
		mCloseOnMaintenance = isClose;
		_MaintenanceCheckStart();
		mBaseState.Change(BASE_STATE.MAINTENANCE_WAIT);
		return true;
	}

	protected MAINTENANCE_RESULT MaintenanceCheckResult()
	{
		if (!GameMain.mMaintenanceProfileCheckDone)
		{
			return MAINTENANCE_RESULT.CHECKING;
		}
		if (!GameMain.mMaintenanceProfileSucceed)
		{
			return MAINTENANCE_RESULT.ERROR;
		}
		if (GameMain.MaintenanceMode)
		{
			return MAINTENANCE_RESULT.IN_MAINTENANCE;
		}
		return MAINTENANCE_RESULT.OK;
	}

	public virtual void OnMaintenanceDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM item, int a_state)
	{
		mMaintenanceDialog = null;
		if (item == ConnectCommonErrorDialog.SELECT_ITEM.GIVEUP)
		{
			mBaseState.Reset(BASE_STATE.MAIN, true);
			if (mCloseOnMaintenance)
			{
				OnBackKeyPress();
			}
		}
		else
		{
			_MaintenanceCheckStart();
			mBaseState.Change(BASE_STATE.MAINTENANCE_WAIT);
		}
	}

	public virtual void OnMaintenanceCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM item, int a_state)
	{
		mMaintenanceErrorDialog = null;
		if (item == ConnectCommonErrorDialog.SELECT_ITEM.GIVEUP)
		{
			mBaseState.Reset(BASE_STATE.MAIN);
			if (mCloseOnMaintenance)
			{
				OnBackKeyPress();
			}
		}
		else
		{
			_MaintenanceCheckStart();
			mBaseState.Change(BASE_STATE.MAINTENANCE_WAIT);
		}
	}

	private void ExecCallbackMethod(UnityAction<UnityAction> method, params UnityAction[] calls)
	{
		if (method == null || calls == null)
		{
			return;
		}
		foreach (UnityAction unityAction in calls)
		{
			if (unityAction != null)
			{
				method(unityAction);
			}
		}
	}

	private UnityEvent GetCallback(CALLBACK_STATE type, bool force_create)
	{
		if (mCallback == null && force_create)
		{
			mCallback = new UnityEvent[Enum.GetNames(typeof(CALLBACK_STATE)).Length];
		}
		if (mCallback != null && mCallback[(byte)type] == null && force_create)
		{
			mCallback[(byte)type] = new UnityEvent();
		}
		return (mCallback == null) ? null : mCallback[(byte)type];
	}

	private void InvokeCallback(CALLBACK_STATE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.Invoke();
		}
	}

	public void AddCallback(CALLBACK_STATE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.AddListener, calls);
			}
		}
	}

	public void RemoveCallback(CALLBACK_STATE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.RemoveListener, calls);
			}
		}
	}

	public void ClearCallback(CALLBACK_STATE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.RemoveAllListeners();
		}
	}

	public void ClearCallback()
	{
		foreach (int value in Enum.GetValues(typeof(CALLBACK_STATE)))
		{
			ClearCallback((CALLBACK_STATE)value);
		}
	}

	public void SetCallback(CALLBACK_STATE type, params UnityAction[] calls)
	{
		ClearCallback(type);
		AddCallback(type, calls);
	}
}
