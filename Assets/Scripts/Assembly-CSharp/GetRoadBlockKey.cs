using UnityEngine;

public class GetRoadBlockKey : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum CONFIRM_DIALOG_STYLE
	{
		OK_ONLY = 0,
		YES_NO = 1,
		OK_CANCEL = 2,
		CLOSE_ONLY = 3,
		RETRY_CLOSE = 4,
		CONFIRM_CLOSE = 5,
		TIMER = 6
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private CONFIRM_DIALOG_STYLE mConfirmStyle;

	private DIALOG_SIZE mDialogSize;

	private string mTitle;

	private string mDescription;

	private float mCloseTimer = 1f;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy() && mConfirmStyle == CONFIRM_DIALOG_STYLE.TIMER)
		{
			mCloseTimer -= Time.deltaTime;
			if (mCloseTimer <= 0f || mGame.mBackKeyTrg)
			{
				Close();
			}
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(mDialogSize);
		InitDialogTitle(mTitle);
		float y = -110f;
		float y2 = -280f;
		switch (mDialogSize)
		{
		case DIALOG_SIZE.SMALL:
			y = -10f;
			y2 = -130f;
			break;
		case DIALOG_SIZE.MEDIUM:
			y = 50f;
			y2 = -90f;
			break;
		case DIALOG_SIZE.LARGE:
			y = -180f;
			y2 = -490f;
			break;
		case DIALOG_SIZE.XLARGE:
			y = -310f;
			y2 = -690f;
			break;
		}
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, mDescription, mBaseDepth + 4, new Vector3(0f, y, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		switch (mConfirmStyle)
		{
		case CONFIRM_DIALOG_STYLE.OK_ONLY:
		case CONFIRM_DIALOG_STYLE.CLOSE_ONLY:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			string text = "LC_button_yes";
			if (mConfirmStyle == CONFIRM_DIALOG_STYLE.CLOSE_ONLY)
			{
				text = "LC_button_close";
			}
			Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(0f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.RETRY_CLOSE:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_retry", "LC_button_retry", "LC_button_retry", mBaseDepth + 4, new Vector3(-130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 4, new Vector3(130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.CONFIRM_CLOSE:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_check", "LC_button_check", "LC_button_check", mBaseDepth + 4, new Vector3(-130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 4, new Vector3(130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.TIMER:
			break;
		default:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		}
	}

	public void Init(string title, string desc, CONFIRM_DIALOG_STYLE style, bool openSeEnable = true)
	{
		Init(title, desc, style, DIALOG_SIZE.MEDIUM);
		SetOpenSeEnable(openSeEnable);
	}

	public void Init(string title, string desc, CONFIRM_DIALOG_STYLE style, DIALOG_SIZE size)
	{
		mTitle = title;
		mDescription = desc;
		mConfirmStyle = style;
		mDialogSize = size;
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetCloseTimer(float timer)
	{
		mCloseTimer = timer;
	}
}
