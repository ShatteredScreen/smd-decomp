using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMDTaskManager : SingletonMonoBehaviour<SMDTaskManager>
{
	public class SMDTask
	{
		public delegate void OnFinishedCallback(SMDTask a_task);

		private IEnumerator mCoroutine;

		private bool mIsRunning;

		private bool mIsPause;

		private bool mIsStop;

		private OnFinishedCallback mFinishedCallback;

		public string Name { get; private set; }

		public SMDTask(string a_name, IEnumerator a_coroutine)
		{
			Name = a_name;
			mCoroutine = a_coroutine;
		}

		public void SetFinishCallback(OnFinishedCallback a_callback)
		{
			mFinishedCallback = a_callback;
		}

		public void Start()
		{
			mIsRunning = true;
			SingletonMonoBehaviour<SMDTaskManager>.Instance.StartCoroutine(Run());
		}

		public IEnumerator Run()
		{
			IEnumerator e = mCoroutine;
			while (mIsRunning)
			{
				if (mIsPause)
				{
					yield return null;
				}
				else if (e != null && e.MoveNext())
				{
					yield return e.Current;
				}
				else
				{
					mIsRunning = false;
				}
			}
			if (mFinishedCallback != null)
			{
				mFinishedCallback(this);
			}
		}

		public void Pause()
		{
			mIsPause = true;
		}

		public void Restart()
		{
			mIsPause = false;
		}

		public void Stop()
		{
			mIsStop = true;
			mIsRunning = false;
		}
	}

	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		INTERVAL = 2,
		WAIT = 3
	}

	public class RunningTask
	{
		public SMDTask Task;

		public OnFinishedCallback Callback;

		public RunningTask(SMDTask a_task, OnFinishedCallback a_cb)
		{
			Task = a_task;
			Callback = a_cb;
		}
	}

	public delegate void OnFinishedCallback(string a_name);

	public const float TASK_WAIT_TIME = 0.2f;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	public STATE mCurrentState;

	private Dictionary<string, RunningTask> mRunningTaskList = new Dictionary<string, RunningTask>();

	private List<RunningTask> mTaskQueue = new List<RunningTask>();

	private float mWaitTime;

	public SMDTaskManager()
	{
		SingletonMonoBehaviour<SMDTaskManager>.SetSingletonType(typeof(SMDTaskManager));
	}

	private void AwakeSingleton()
	{
	}

	private void Update()
	{
		mCurrentState = mState.GetStatus();
		switch (mCurrentState)
		{
		case STATE.INIT:
			mWaitTime = Time.realtimeSinceStartup;
			mState.Change(STATE.MAIN);
			break;
		case STATE.MAIN:
			if (mTaskQueue.Count > 0)
			{
				RunningTask runningTask = mTaskQueue[0];
				mTaskQueue.RemoveAt(0);
				if (mRunningTaskList.ContainsKey(runningTask.Task.Name))
				{
					mTaskQueue.Add(runningTask);
				}
				mRunningTaskList.Add(runningTask.Task.Name, runningTask);
				runningTask.Task.Start();
				mState.Change(STATE.INTERVAL);
			}
			break;
		case STATE.INTERVAL:
			if (mState.IsChanged())
			{
				mWaitTime = Time.realtimeSinceStartup;
			}
			if (Time.realtimeSinceStartup - mWaitTime > 0.2f)
			{
				mState.Change(STATE.MAIN);
			}
			break;
		}
		mState.Update();
	}

	public static bool CreateTask(string a_name, IEnumerator a_coroutine, OnFinishedCallback a_callback = null)
	{
		if (SingletonMonoBehaviour<SMDTaskManager>.Instance.mRunningTaskList.ContainsKey(a_name) || SingletonMonoBehaviour<SMDTaskManager>.Instance.mTaskQueue.Exists((RunningTask a) => a.Task.Name == a_name))
		{
			return false;
		}
		SMDTask sMDTask = new SMDTask(a_name, a_coroutine);
		sMDTask.SetFinishCallback(delegate(SMDTask a_task)
		{
			if (SingletonMonoBehaviour<SMDTaskManager>.Instance.mRunningTaskList.ContainsKey(a_task.Name))
			{
				RunningTask runningTask = SingletonMonoBehaviour<SMDTaskManager>.Instance.mRunningTaskList[a_task.Name];
				if (runningTask.Callback != null)
				{
					runningTask.Callback(a_task.Name);
				}
				SingletonMonoBehaviour<SMDTaskManager>.Instance.mRunningTaskList.Remove(a_task.Name);
			}
		});
		SingletonMonoBehaviour<SMDTaskManager>.Instance.mTaskQueue.Add(new RunningTask(sMDTask, a_callback));
		return true;
	}
}
