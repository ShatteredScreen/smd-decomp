using System;
using System.Net;
using System.Threading;

public class HttpAsync : IDisposable
{
	public enum RESULT
	{
		NONE = 0,
		SUCCESS = 1,
		ERROR = 2,
		TIMEOUT = 3,
		NOTFOUND = 4,
		ABORT = 5
	}

	public delegate void ResponseHandler(HttpAsync httpAsync);

	public const int DEFAULT_TIMEOUT = 5000;

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private bool mDisposed;

	private RegisteredWaitHandle mWaitHandle;

	public int SeqNo { get; private set; }

	public bool IsBusy { get; private set; }

	public int Timeout { get; set; }

	public WebRequest Request { get; set; }

	public ResponseHandler Handler { get; private set; }

	public RESULT Result { get; private set; }

	public Exception Error { get; private set; }

	public WebResponse Response { get; private set; }

	public object UserData { get; private set; }

	public bool WaitResponseCallback { get; private set; }

	public bool IsRequestTimeout { get; private set; }

	public HttpAsync(int seqNo, WebRequest request, int timeout, bool requestTimeout = false)
	{
		mDisposed = false;
		SeqNo = seqNo;
		Timeout = timeout;
		Request = request;
		IsBusy = false;
		Result = RESULT.NONE;
		Response = null;
		Error = null;
		UserData = null;
		IsRequestTimeout = requestTimeout;
	}

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
	}

	private static void dbg(string msg)
	{
	}

	private static void assert(bool exp, string msg)
	{
	}

	public void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
	}

	protected virtual void Dispose(bool disposing)
	{
		if (mDisposed)
		{
			return;
		}
		if (disposing)
		{
			if (Request != null)
			{
				Request = null;
			}
			if (Response != null)
			{
				Response = null;
			}
			if (Error != null)
			{
				Error = null;
			}
		}
		mDisposed = true;
	}

	public void GetResponse()
	{
		GetResponse(null, null);
	}

	public void GetResponse(ResponseHandler handler, object userdata)
	{
		dbg(string.Format("GetResponse(#{0}) - th={1} 0 Begin", SeqNo, Thread.CurrentThread.GetHashCode().ToString()));
		if (mDisposed)
		{
			dbg("GetResponse - Disposed!");
			Result = RESULT.ERROR;
			Error = new Exception("Already disposed");
			if (Handler != null)
			{
				Handler(this);
			}
			return;
		}
		if (IsBusy)
		{
			dbg("GetResponse - Abort");
			return;
		}
		dbg("GetResponse - 1");
		IsBusy = true;
		Result = RESULT.SUCCESS;
		Handler = handler;
		UserData = userdata;
		WaitResponseCallback = true;
		dbg("GetResponse - 2");
		DateTime now = DateTime.Now;
		IAsyncResult asyncResult = Request.BeginGetResponse(ResponseCallback, this);
		if (asyncResult == null)
		{
			dbg("GetResponse - BeginGetResponse failed.");
			Result = RESULT.ERROR;
			Error = new Exception("Could not begin HTTP connection.");
			if (Handler != null)
			{
				Handler(this);
			}
			IsBusy = false;
			return;
		}
		dbg("GetResponse - 3");
		mWaitHandle = ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, ScanTimeoutCallback, this, Timeout, true);
		dbg("GetResponse - 4");
		while (!asyncResult.IsCompleted)
		{
		}
		while (WaitResponseCallback)
		{
			long num = (long)(DateTime.Now - now).TotalMilliseconds;
			if (num > Timeout)
			{
				dbg("!! ResponseCallback Timeout !!" + num + ">" + Timeout);
				Result = RESULT.TIMEOUT;
				Error = new Exception("ResponseCallback Timeout.");
				if (Handler != null)
				{
					Handler(this);
				}
				IsBusy = false;
				return;
			}
			Thread.Sleep(10);
		}
		dbg("GetResponse - 5");
		dbg("GetResponse - 6");
		if (Handler != null)
		{
			Handler(this);
		}
		dbg("GetResponse - 7");
		IsBusy = false;
		dbg("GetResponse - 8 End");
	}

	public void Abort()
	{
		dbg("Abort");
		if (Request != null)
		{
			Result = RESULT.TIMEOUT;
			Request.Abort();
			return;
		}
		Result = RESULT.ABORT;
		try
		{
			BIJLog.E(string.Format("Abort and Request is null ({0}) - th={1}", SeqNo, Thread.CurrentThread.GetHashCode().ToString()));
		}
		catch
		{
			BIJLog.E("Abort and Request is null");
		}
	}

	private void ResponseCallback(IAsyncResult asyncResult)
	{
		dbg(string.Format("ResponseCallback({0}) - th={1}", SeqNo, Thread.CurrentThread.GetHashCode().ToString()));
		try
		{
			Result = RESULT.SUCCESS;
			Response = Request.EndGetResponse(asyncResult);
		}
		catch (WebException ex)
		{
			log("ResponseCallback failed: " + ex.ToString());
			Error = ex;
			Response = ex.Response;
			log("ResponseCallback status=" + ex.Status);
			if (ex.Status == WebExceptionStatus.ProtocolError)
			{
				HttpWebResponse httpWebResponse = Response as HttpWebResponse;
				if (httpWebResponse != null)
				{
					dbg("ResponseCallback ProtocolError - HTTP status=" + httpWebResponse.StatusCode);
					switch (httpWebResponse.StatusCode)
					{
					case HttpStatusCode.NotFound:
						Result = RESULT.NOTFOUND;
						dbg("ResponseCallback ProtocolError - Result=RESULT.NOTFOUND");
						break;
					case HttpStatusCode.RequestTimeout:
						Result = RESULT.TIMEOUT;
						dbg("ResponseCallback ProtocolError - Result=RESULT.TIMEOUT");
						break;
					default:
						Result = RESULT.ERROR;
						dbg("ResponseCallback ProtocolError - Result=RESULT.ERROR");
						break;
					}
				}
				else
				{
					dbg("ResponseCallback ProtocolError - No response.");
					Result = RESULT.ABORT;
				}
			}
			else
			{
				dbg("ResponseCallback error: " + ex.Status);
				if (Result != RESULT.TIMEOUT)
				{
					Result = RESULT.ABORT;
				}
			}
		}
		WaitResponseCallback = false;
	}

	private void ScanTimeoutCallback(object state, bool timeout)
	{
		dbg(string.Format("ScanTimeoutCallback({0}) - th={1} timeout={2}", SeqNo, Thread.CurrentThread.GetHashCode().ToString(), timeout));
		try
		{
			assert(this == state, "ScanTimeoutCallback invalid state");
			if (timeout)
			{
				Abort();
				return;
			}
			RegisteredWaitHandle registeredWaitHandle = state as RegisteredWaitHandle;
			if (registeredWaitHandle != null)
			{
				registeredWaitHandle.Unregister(null);
			}
		}
		catch (Exception ex)
		{
			log("ScanTimeoutCallback failed: " + ex.ToString());
			Result = RESULT.ERROR;
			Error = ex;
		}
	}
}
