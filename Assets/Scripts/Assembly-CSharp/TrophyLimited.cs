public class TrophyLimited
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("id")]
	public int TrophyItemID { get; set; }

	[MiniJSONAlias("limit")]
	public int TrophyItemLimit { get; set; }

	[MiniJSONAlias("cnt")]
	public int TrophyCount { get; set; }

	public TrophyLimited()
	{
		TrophyItemLimit = 1;
	}

	public TrophyLimited(long start, long end, int id, int limit, int cnt)
	{
		StartTime = start;
		EndTime = end;
		TrophyItemID = id;
		TrophyItemLimit = limit;
		TrophyCount = cnt;
	}
}
