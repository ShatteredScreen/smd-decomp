using System.Collections.Generic;

public class GlobalLabyrinthEventData
{
	public short LabyrinthSelectedCourseID = -1;

	public List<AccessoryData> RewardAccessoryDataList;

	public List<AdvRewardListData> RewardGCAccessoryDataList;

	public int LastClearedStage = -1;

	public Dictionary<short, int> CourseStagesAllClearList;

	public Dictionary<short, int> CourseStarCompleteList;

	public bool DoCreateReward;

	public void RestartCourse()
	{
		LastClearedStage = -1;
	}
}
