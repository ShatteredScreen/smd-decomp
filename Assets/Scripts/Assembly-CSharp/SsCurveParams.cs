using System;

[Serializable]
public class SsCurveParams
{
	public SsInterpolationType Type;

	public float StartT;

	public float StartV;

	public float EndT;

	public float EndV;

	public bool IsNone
	{
		get
		{
			return Type == SsInterpolationType.None;
		}
	}

	public bool Equals(SsCurveParams r)
	{
		if (Type != r.Type)
		{
			return false;
		}
		if (StartT != r.StartT)
		{
			return false;
		}
		if (StartV != r.StartV)
		{
			return false;
		}
		if (EndT != r.EndT)
		{
			return false;
		}
		if (EndV != r.EndV)
		{
			return false;
		}
		return true;
	}

	public override string ToString()
	{
		return string.Concat("Type: ", Type, ", StartT: ", StartT, ", StartV: ", StartV, ", EndT: ", EndT, ", EndV: ", EndV);
	}
}
