using System;
using System.Collections.Generic;

public class ConnectGetRandomUser : ConnectBase
{
	private class _GetRandomUserData
	{
		public List<SearchUserData> results { get; set; }
	}

	protected string mJson;

	public void SetResponseData(string a_json)
	{
		mJson = a_json;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mGame.LastRandomUserGetTime, DateTime.Now);
		long gET_RANDOMUSER_FREQ = GameMain.GET_RANDOMUSER_FREQ;
		if (gET_RANDOMUSER_FREQ > 0 && num < gET_RANDOMUSER_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			base.StateStart();
			return;
		}
		int series = SingletonMonoBehaviour<Network>.Instance.Series;
		int level = SingletonMonoBehaviour<Network>.Instance.Level;
		int a_range = 500;
		int rANDOM_USER_MAX = GameMain.RANDOM_USER_MAX;
		if (!Server.GetRandomUser(series, level, a_range, rANDOM_USER_MAX))
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		try
		{
			_GetRandomUserData obj = new _GetRandomUserData();
			new MiniJSONSerializer().Populate(ref obj, mJson);
			mGame.RandomUserList = new List<SearchUserData>();
			if (obj != null && obj.results != null && obj.results.Count > 0)
			{
				List<SearchUserData> strayUserList = mGame.mPlayer.GetStrayUserList(obj.results);
				foreach (SearchUserData item in strayUserList)
				{
					mGame.RandomUserList.Add(item.Clone());
				}
			}
		}
		catch (Exception ex)
		{
			BIJLog.E("GetRandomUser Error : " + ex);
			mGame.RandomUserList.Clear();
		}
		mGame.LastRandomUserGetTime = DateTime.Now;
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mGame.LastRandomUserGetTime = DateTimeUtil.UnitLocalTimeEpoch;
			mConnectResultKind = 3;
			base.StateConnectFinish();
			return false;
		}
		return false;
	}
}
