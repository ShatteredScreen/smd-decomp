public class SCDebugSetResponse : ParameterObject<SCDebugSetResponse>
{
	public int code { get; set; }

	public int code_ex { get; set; }

	public long server_time { get; set; }
}
