using System.Collections.Generic;
using UnityEngine;

public class GrowupPartnerSelectDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		PLAY = 0,
		CANCEL = 1
	}

	public enum STATE
	{
		MAIN = 0,
		TUTORIAL = 1,
		WAIT = 2
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private UIButton mInTutorialButton;

	private UIButton mCancelButton;

	private AccessoryData mData;

	public AccessoryData.ACCESSORY_TYPE AccessoryType;

	private SMVerticalListInfo mIconListInfo;

	private SMVerticalList mIconList;

	private GameObject mListCenter;

	private int mGrowthDisplayNum;

	private UISprite mGrowthNumFrame;

	private UILabel mGrowthNumLabel;

	public AccessoryData Data
	{
		get
		{
			return mData;
		}
	}

	public bool IsCloseButton { get; private set; }

	public int SelectedPartnerNo { get; private set; }

	public bool CanRepeat { get; set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(AccessoryData a_data, bool a_close, AccessoryData.ACCESSORY_TYPE a_type)
	{
		mData = a_data;
		SelectedPartnerNo = 0;
		IsCloseButton = a_close;
		AccessoryType = a_type;
	}

	public void Init(bool a_close)
	{
		IsCloseButton = a_close;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
		{
			if (!(mInTutorialButton != null) || !mGame.mTutorialManager.IsTutorialPlaying() || mGame.mTutorialManager.GetCurrentTutorial() != Def.TUTORIAL_INDEX.MAP_LEVEL9_1_2)
			{
				break;
			}
			int num2 = 0;
			List<PlayStageParam> playStage2 = mGame.mPlayer.PlayStage;
			for (int j = 0; j < playStage2.Count; j++)
			{
				if (playStage2[j].Series == 0)
				{
					num2 = playStage2[j].Level;
					break;
				}
			}
			if (num2 == 900)
			{
				mGame.mTutorialManager.SetTutorialArrow(mCancelButton.gameObject, mCancelButton.gameObject);
				mState.Change(STATE.TUTORIAL);
			}
			break;
		}
		case STATE.TUTORIAL:
		{
			if (!mGame.mTutorialManager.IsTutorialPlaying() || mGame.mTutorialManager.GetCurrentTutorial() != Def.TUTORIAL_INDEX.MAP_LEVEL9_2)
			{
				break;
			}
			int num = 0;
			List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == 0)
				{
					num = playStage[i].Level;
					break;
				}
			}
			if (num == 900)
			{
				mGame.mTutorialManager.SetTutorialFinger(mInTutorialButton.gameObject, mInTutorialButton.gameObject, Def.DIR.NONE, 100);
				mGame.mTutorialManager.SetTutorialArrowOffset(0f, -40f);
				mGame.mTutorialManager.AddAllowedButton(mInTutorialButton.gameObject);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		}
		mState.Update();
		UpdateGrowthDisp();
	}

	public override void BuildDialog()
	{
		UIFont atlasFont = GameMain.LoadFont();
		string empty = string.Empty;
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(empty);
		UILabel uILabel = Util.CreateLabel("Title", mTitleBoard.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 4, new Vector3(32f, -4f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		empty = Util.MakeLText("GetLevelup_Title_00");
		Util.SetLabelText(uILabel, empty);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.fontSize = 38;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UISprite sprite = Util.CreateSprite("IconLVUP", mTitleBoard.gameObject, image);
		Util.SetSpriteInfo(sprite, "icon_level", mBaseDepth + 1, new Vector3(-148f, 1f, 0f), Vector3.one, false);
		mIconListInfo = new SMVerticalListInfo(4, 490f, 410f, 4, 490f, 410f, 1, 130);
		uILabel = Util.CreateLabel("DescTop", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("GetLevelup_Desk"), mBaseDepth + 2, new Vector3(0f, 222f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("DescBot", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("GetLevelupAction_Desk"), mBaseDepth + 2, new Vector3(0f, -248f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mCancelButton = CreateCancelButton("OnCancelPushed");
		if (mData == null)
		{
			CreateGrowthWindow();
		}
	}

	private void CreateGrowthWindow()
	{
		if (!(mDialogFrame == null))
		{
			Vector2 localSize = mDialogFrame.localSize;
			Vector3 vector = ((Util.ScreenOrientation != ScreenOrientation.LandscapeLeft && Util.ScreenOrientation != ScreenOrientation.LandscapeRight) ? mGemFramePositionOffsetP : mGemFramePositionOffsetL);
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			UIFont atlasFont = GameMain.LoadFont();
			mGrowthNumFrame = Util.CreateSprite("GrowthFrame", base.gameObject, image);
			Util.SetSpriteInfo(mGrowthNumFrame, "instruction_panel8", mBaseDepth, new Vector3(0f + vector.x, (0f - localSize.y) / 2f - 30f + vector.y, 0f), Vector3.one, false);
			mGrowthNumFrame.type = UIBasicSprite.Type.Sliced;
			mGrowthNumFrame.SetDimensions(190, 70);
			UISprite sprite = Util.CreateSprite("GrowthIcon", mGrowthNumFrame.gameObject, image);
			Util.SetSpriteInfo(sprite, "growth_panel", mBaseDepth + 1, new Vector3(-4f, 0f, 0f), Vector3.one, false);
			mGrowthDisplayNum = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 10);
			mGrowthNumLabel = Util.CreateLabel("GrowthNum", mGrowthNumFrame.gameObject, atlasFont);
			Util.SetLabelInfo(mGrowthNumLabel, string.Empty + mGrowthDisplayNum, mBaseDepth + 2, new Vector3(16f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			mGrowthNumLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			OnScreenChanged(Util.ScreenOrientation);
		}
	}

	private void UpdateGrowthDisp()
	{
		if (mGrowthNumFrame != null && mGrowthDisplayNum != mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 10))
		{
			mGrowthDisplayNum = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 10);
			mGrowthNumLabel.text = string.Empty + mGrowthDisplayNum;
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!(mGrowthNumFrame == null))
		{
			Vector2 localSize = mDialogFrame.localSize;
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mGrowthNumFrame.transform.localPosition = new Vector3(-194f + mGemFramePositionOffsetP.x, (0f - localSize.y) / 2f - 30f + mGemFramePositionOffsetP.y, 0f);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mGrowthNumFrame.transform.localPosition = new Vector3(localSize.x / 2f + 95f + mGemFramePositionOffsetL.x, (0f - localSize.y) / 2f + 50f + mGemFramePositionOffsetL.y, 0f);
				break;
			}
		}
	}

	private void ListOpen()
	{
		List<SMVerticalListItem> items = PartnerListItem.CreatePartnerList();
		mListCenter = Util.CreateGameObject("GridRoot", base.gameObject);
		mListCenter.transform.localPosition = new Vector3(-8f, -12f, 0f);
		mIconList = SMVerticalList.Make(mListCenter, this, mIconListInfo, items, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mIconList.OnCreateItem = OnCreateIconListItem;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UISprite uISprite = Util.CreateSprite("ListBg", mListCenter.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(8f, 0f, 0f), Vector3.one, false);
		uISprite.SetDimensions(530, 430);
		uISprite.type = UIBasicSprite.Type.Sliced;
	}

	private void OnCreateIconListItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIButton uIButton = null;
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		GameObject gameObject = null;
		uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)mIconList.mList.cellWidth;
		uISprite.height = (int)mIconList.mList.cellHeight;
		PartnerListItem partnerListItem = (PartnerListItem)item;
		if (partnerListItem.data == null)
		{
			if (partnerListItem.category != byte.MaxValue)
			{
				PartnerListItem.CreateCategoryRibbon(itemGO, mBaseDepth + 2, partnerListItem.category);
			}
			return;
		}
		string text = "icon_chara_question";
		string imageName = "null";
		bool flag = false;
		int companionLevel = mGame.mPlayer.GetCompanionLevel(partnerListItem.data.index);
		if (partnerListItem.enable)
		{
			text = partnerListItem.data.iconName;
			if (partnerListItem.data.maxLevel > companionLevel)
			{
				imageName = Def.CharacterIconLevelImageTbl[companionLevel];
				flag = true;
			}
			else
			{
				imageName = Def.CharacterIconLevelMaxImageTbl[companionLevel];
			}
		}
		if (flag)
		{
			uIButton = Util.CreateJellyImageButton("Icon_" + partnerListItem.data.index, itemGO, image2);
			Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 2, Vector3.one, Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnCharacterPushed", UIButtonMessage.Trigger.OnClick);
			uIButton.GetComponent<JellyImageButton>().SetBaseScale(0.9f);
			uIButton.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
			UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView.scrollView = mIconList.mListScrollView;
			gameObject = uIButton.gameObject;
			if (partnerListItem.data.index == 0 && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL9_7))
			{
				mInTutorialButton = uIButton;
			}
		}
		else
		{
			uISprite = Util.CreateSprite("Icon_" + partnerListItem.data.index, itemGO, image2);
			Util.SetSpriteInfo(uISprite, text, mBaseDepth + 2, Vector3.zero, new Vector3(0.9f, 0.9f, 1f), false);
			gameObject = uISprite.gameObject;
		}
		uISprite2 = Util.CreateSprite("Lvl_" + partnerListItem.data.index, gameObject, image);
		Util.SetSpriteInfo(uISprite2, imageName, mBaseDepth + 3, new Vector3(0f, -55f, 0f), Vector3.one, false);
	}

	public void OnCharacterPushed(GameObject a_go)
	{
		if (GetBusy())
		{
			return;
		}
		if (a_go != null)
		{
			string text = a_go.name;
			if (text.Contains("_"))
			{
				string[] array = text.Split('_');
				if (array.Length > 1)
				{
					int selectedPartnerNo = int.Parse(array[1]);
					SelectedPartnerNo = selectedPartnerNo;
				}
			}
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		mSelectItem = SELECT_ITEM.PLAY;
		Close();
	}

	public override void OnOpenFinished()
	{
		ListOpen();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
