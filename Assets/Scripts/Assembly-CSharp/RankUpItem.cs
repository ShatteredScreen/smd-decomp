using UnityEngine;

public class RankUpItem
{
	public Texture2D Icon { get; set; }

	public int IconID { get; set; }

	public string FbID { get; set; }

	public string Gender { get; set; }

	public string Name { get; set; }

	public int RankOld { get; set; }

	public int RankNew { get; set; }

	public int DisplayValue { get; set; }

	public RankUpDialog.MODE Mode { get; set; }

	public long Star { get; set; }

	public long Trophy { get; set; }

	public int Stage { get; set; }

	public int StageIcon { get; set; }

	public int MyLevel { get; set; }
}
