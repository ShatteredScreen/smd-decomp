using UnityEngine;

public class TrophyItemListData
{
	public GameObject itemGo;

	public TrophyItemListItem data;

	public UILabel limitLabel;

	public UILabel trophyNumLabel;

	public UIButton button;
}
