public class CampaignChargeRequest : ParameterObject<CampaignChargeRequest>
{
	public int bhiveid { get; set; }

	public int uuid { get; set; }

	public string udid { get; set; }

	public string uniq_id { get; set; }

	public int iap_campaign_id { get; set; }

	public long purchase_time { get; set; }
}
