public class RatioChange7Info
{
	[MiniJSONAlias("id")]
	public int Id { get; set; }

	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("event")]
	public int EventID { get; set; }

	[MiniJSONAlias("stageMin")]
	public int StageMin { get; set; }

	[MiniJSONAlias("stageMax")]
	public int StageMax { get; set; }

	[MiniJSONAlias("segmentType")]
	public int SegmentType { get; set; }

	[MiniJSONAlias("intervalHour")]
	public int IntervalHour { get; set; }

	[MiniJSONAlias("count")]
	public int ContinueCount { get; set; }

	[MiniJSONAlias("activeRatio")]
	public int ActiveRatio { get; set; }

	[MiniJSONAlias("minRatio")]
	public int MinRatio { get; set; }

	[MiniJSONAlias("maxRatio")]
	public int MaxRatio { get; set; }

	[MiniJSONAlias("ABParam")]
	public int ABParam { get; set; }

	public RatioChange7Info()
	{
		Id = 0;
		Series = 0;
		StageMin = -1;
		StageMax = -1;
		SegmentType = 0;
		IntervalHour = 24;
		ContinueCount = 1;
		ActiveRatio = 0;
		MinRatio = 0;
		MaxRatio = 10;
		ABParam = 0;
	}
}
