using System;

public class SMSeasonEventSetting
{
	public int EventID { get; set; }

	public string Name { get; set; }

	public string BannerAtlas { get; set; }

	public string BannerName { get; set; }

	public int RewardAccessoryID { get; set; }

	public DateTime StartTime { get; set; }

	public DateTime EndTime { get; set; }

	public string HelpURL { get; set; }

	public virtual void Serialize(BIJFileBinaryWriter sw)
	{
		sw.WriteInt(EventID);
		sw.WriteUTF(Name);
		sw.WriteUTF(BannerAtlas);
		sw.WriteUTF(BannerName);
		sw.WriteInt(RewardAccessoryID);
		sw.WriteDateTime(StartTime);
		sw.WriteDateTime(EndTime);
		sw.WriteUTF(HelpURL);
	}

	public virtual void Deserialize(BIJBinaryReader a_reader)
	{
		EventID = a_reader.ReadInt();
		Name = a_reader.ReadUTF();
		BannerAtlas = a_reader.ReadUTF();
		BannerName = a_reader.ReadUTF();
		RewardAccessoryID = a_reader.ReadInt();
		StartTime = a_reader.ReadDateTime();
		EndTime = a_reader.ReadDateTime();
		HelpURL = a_reader.ReadUTF();
	}
}
