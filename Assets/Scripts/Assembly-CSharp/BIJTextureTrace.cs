using System.Text;
using UnityEngine;

public class BIJTextureTrace : MonoBehaviour
{
	private Rect mDrawRect;

	private GUIStyle mStyle;

	private void Awake()
	{
		mDrawRect = new Rect(0f, 0f, Screen.width, Screen.height);
		GUIStyleState gUIStyleState = new GUIStyleState();
		gUIStyleState.textColor = Color.white;
		mStyle = new GUIStyle();
		mStyle.normal = gUIStyleState;
		mStyle.contentOffset = new Vector2(2f, 2f);
		mStyle.fontSize = 10;
		mStyle.alignment = TextAnchor.UpperRight;
	}

	private void OnGUI()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Texture[] array = Object.FindObjectsOfType(typeof(Texture)) as Texture[];
		foreach (Texture texture in array)
		{
			string value = string.Format("#{0} {1}x{2} {3}", texture.GetNativeTextureID(), texture.width, texture.height, (!string.IsNullOrEmpty(texture.name)) ? texture.name : texture.GetType().Name);
			stringBuilder.AppendLine(value);
		}
		GUI.Label(mDrawRect, new GUIContent(stringBuilder.ToString()), mStyle);
	}
}
