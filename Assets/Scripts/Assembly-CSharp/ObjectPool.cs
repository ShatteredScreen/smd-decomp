using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : MonoBehaviour
{
	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private IList<T> mObjects;

	public int Count
	{
		get
		{
			return (mObjects != null) ? mObjects.Count : 0;
		}
	}

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string format, params object[] args)
	{
	}

	private static void dbg(string format, params object[] args)
	{
	}

	private static void assert(bool exp, string format, params object[] args)
	{
	}

	public void Init(int initialCapacity)
	{
		Init(initialCapacity, false);
	}

	public void Init(int initialCapacity, bool uselock)
	{
		if (uselock)
		{
			mObjects = new ThreadSafeList<T>(initialCapacity);
		}
		else
		{
			mObjects = new List<T>(initialCapacity);
		}
	}

	public T GetObject()
	{
		T result = (T)null;
		if (mObjects.Count > 0)
		{
			result = mObjects[0];
			mObjects.RemoveAt(0);
		}
		return result;
	}

	public void PutObject(T item)
	{
		mObjects.Add(item);
	}
}
