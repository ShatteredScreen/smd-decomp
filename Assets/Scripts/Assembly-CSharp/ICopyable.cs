public interface ICopyable
{
	void CopyTo(object _obj);
}
