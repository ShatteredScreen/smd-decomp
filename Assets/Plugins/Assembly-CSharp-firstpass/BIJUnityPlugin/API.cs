using UnityEngine;

namespace BIJUnityPlugin
{
	internal static class API
	{
		public static void BIJUnity_setup(string _gameObject, string _callback, bool _isWW)
		{
		}

		public static string BIJUnity_getDeviceID()
		{
			return SystemInfo.deviceUniqueIdentifier;
		}

		public static string BIJUnity_getOriginalUniqueId()
		{
			return "debug";
		}
	}
}
