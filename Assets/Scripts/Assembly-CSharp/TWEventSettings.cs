using System;
using System.Collections.Generic;

public class TWEventSettings : ICloneable
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("tower_settings")]
	public List<TowerSettings> TowerSetList { get; set; }

	public TWEventSettings()
	{
		TowerSetList = new List<TowerSettings>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public TWEventSettings Clone()
	{
		return MemberwiseClone() as TWEventSettings;
	}
}
