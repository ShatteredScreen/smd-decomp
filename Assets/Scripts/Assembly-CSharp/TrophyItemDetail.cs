using System.Collections.Generic;

public class TrophyItemDetail
{
	public enum ItemKind
	{
		NONE = 0,
		WAVE_BOMB = 1,
		TAP_BOMB2 = 2,
		RAINBOW = 3,
		ADD_SCORE = 4,
		SKILL_CHARGE = 5,
		SELECT_ONE = 6,
		SELECT_COLLECT = 7,
		COLOR_CRUSH = 8,
		HEART = 9,
		CONTINUE = 10,
		ROADBLOCK = 11,
		ACCESSORY = 12,
		ROADBLOCK_TICKET = 13,
		GROWUP = 14,
		GROWUPPEACE = 15,
		HEARTPEACE = 16,
		BLOCK_CRUSH = 17,
		PAIR_MAKER = 18,
		OLDEVENTKEY = 19
	}

	public int NONE { get; set; }

	[MiniJSONAlias("stripe_popping")]
	public int WAVE_BOMB { get; set; }

	[MiniJSONAlias("lunaP_double")]
	public int TAP_BOMB2 { get; set; }

	[MiniJSONAlias("rainbow")]
	public int RAINBOW { get; set; }

	[MiniJSONAlias("score")]
	public int ADD_SCORE { get; set; }

	[MiniJSONAlias("skill")]
	public int SKILL_CHARGE { get; set; }

	[MiniJSONAlias("piece_crush")]
	public int SELECT_ONE { get; set; }

	[MiniJSONAlias("itemcatch")]
	public int SELECT_COLLECT { get; set; }

	[MiniJSONAlias("color_crush")]
	public int COLOR_CRUSH { get; set; }

	[MiniJSONAlias("heart")]
	public int HEART { get; set; }

	[MiniJSONAlias("continue")]
	public int CONTINUE { get; set; }

	[MiniJSONAlias("roadblock")]
	public int ROADBLOCK { get; set; }

	[MiniJSONAlias("accessory")]
	public int ACCESSORY { get; set; }

	[MiniJSONAlias("roadblockticket")]
	public int ROADBLOCK_TICKET { get; set; }

	[MiniJSONAlias("growup")]
	public int GROWUP { get; set; }

	[MiniJSONAlias("growuppeace")]
	public int GROWUPPEACE { get; set; }

	[MiniJSONAlias("heartpeace")]
	public int HEARTPEACE { get; set; }

	[MiniJSONAlias("block_crush")]
	public int BLOCK_CRUSH { get; set; }

	[MiniJSONAlias("pair_maker")]
	public int PAIR_MAKER { get; set; }

	[MiniJSONAlias("old_eventkey")]
	public int OLDEVENTKEY { get; set; }

	public Dictionary<ItemKind, int> BundleItems { get; protected set; }

	public TrophyItemDetail()
	{
		BundleItems = new Dictionary<ItemKind, int>();
	}

	public bool IsSetBundle()
	{
		return BundleItems.Count > 1;
	}

	public void SetItem()
	{
		if (WAVE_BOMB > 0)
		{
			BundleItems.Add(ItemKind.WAVE_BOMB, WAVE_BOMB);
		}
		if (TAP_BOMB2 > 0)
		{
			BundleItems.Add(ItemKind.TAP_BOMB2, TAP_BOMB2);
		}
		if (RAINBOW > 0)
		{
			BundleItems.Add(ItemKind.RAINBOW, RAINBOW);
		}
		if (ADD_SCORE > 0)
		{
			BundleItems.Add(ItemKind.ADD_SCORE, ADD_SCORE);
		}
		if (SKILL_CHARGE > 0)
		{
			BundleItems.Add(ItemKind.SKILL_CHARGE, SKILL_CHARGE);
		}
		if (SELECT_ONE > 0)
		{
			BundleItems.Add(ItemKind.SELECT_ONE, SELECT_ONE);
		}
		if (SELECT_COLLECT > 0)
		{
			BundleItems.Add(ItemKind.SELECT_COLLECT, SELECT_COLLECT);
		}
		if (COLOR_CRUSH > 0)
		{
			BundleItems.Add(ItemKind.COLOR_CRUSH, COLOR_CRUSH);
		}
		if (HEART > 0)
		{
			BundleItems.Add(ItemKind.HEART, HEART);
		}
		if (ACCESSORY > 0)
		{
			BundleItems.Add(ItemKind.ACCESSORY, ACCESSORY);
		}
		if (ROADBLOCK_TICKET > 0)
		{
			BundleItems.Add(ItemKind.ROADBLOCK_TICKET, ROADBLOCK_TICKET);
		}
		if (GROWUP > 0)
		{
			BundleItems.Add(ItemKind.GROWUP, GROWUP);
		}
		if (GROWUPPEACE > 0)
		{
			BundleItems.Add(ItemKind.GROWUPPEACE, GROWUPPEACE);
		}
		if (HEARTPEACE > 0)
		{
			BundleItems.Add(ItemKind.HEARTPEACE, HEARTPEACE);
		}
		if (BLOCK_CRUSH > 0)
		{
			BundleItems.Add(ItemKind.BLOCK_CRUSH, BLOCK_CRUSH);
		}
		if (PAIR_MAKER > 0)
		{
			BundleItems.Add(ItemKind.PAIR_MAKER, PAIR_MAKER);
		}
		if (OLDEVENTKEY > 0)
		{
			BundleItems.Add(ItemKind.OLDEVENTKEY, OLDEVENTKEY);
		}
	}

	public void SetDefault(int a_default)
	{
		List<ItemKind> list = new List<ItemKind>(BundleItems.Keys);
		if (list.Count == 1)
		{
			BundleItems[list[0]] = a_default;
		}
	}

	public static Def.BOOSTER_KIND GetBoosterID(ItemKind aItemKind)
	{
		Def.BOOSTER_KIND result = Def.BOOSTER_KIND.NONE;
		switch (aItemKind)
		{
		case ItemKind.WAVE_BOMB:
			result = Def.BOOSTER_KIND.WAVE_BOMB;
			break;
		case ItemKind.TAP_BOMB2:
			result = Def.BOOSTER_KIND.TAP_BOMB2;
			break;
		case ItemKind.RAINBOW:
			result = Def.BOOSTER_KIND.RAINBOW;
			break;
		case ItemKind.ADD_SCORE:
			result = Def.BOOSTER_KIND.ADD_SCORE;
			break;
		case ItemKind.SKILL_CHARGE:
			result = Def.BOOSTER_KIND.SKILL_CHARGE;
			break;
		case ItemKind.SELECT_ONE:
			result = Def.BOOSTER_KIND.SELECT_ONE;
			break;
		case ItemKind.SELECT_COLLECT:
			result = Def.BOOSTER_KIND.SELECT_COLLECT;
			break;
		case ItemKind.COLOR_CRUSH:
			result = Def.BOOSTER_KIND.COLOR_CRUSH;
			break;
		case ItemKind.BLOCK_CRUSH:
			result = Def.BOOSTER_KIND.BLOCK_CRUSH;
			break;
		case ItemKind.PAIR_MAKER:
			result = Def.BOOSTER_KIND.PAIR_MAKER;
			break;
		}
		return result;
	}
}
