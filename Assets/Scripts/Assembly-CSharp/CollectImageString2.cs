using UnityEngine;

public class CollectImageString2 : NumberImageString
{
	private int mTargetNum;

	private int mCollectNum;

	private int CalculateNum(int targetNum, int collectNum)
	{
		mTargetNum = targetNum;
		mCollectNum = collectNum;
		return Mathf.Max(0, Mathf.Min(mTargetNum - mCollectNum, 99));
	}

	public int GetCurrentNum()
	{
		return CalculateNum(mTargetNum, mCollectNum);
	}

	public bool IsNumZero()
	{
		return GetCurrentNum() == 0;
	}

	public void Init(int targetNum, int collectNum, int depth, float scale, BIJImage atlas)
	{
		int num = CalculateNum(targetNum, collectNum);
		Init(3, num, depth, scale, UIWidget.Pivot.Center, false, atlas);
	}

	public new void SetNum(int collectNum)
	{
		SetNum(mTargetNum, collectNum);
	}

	public void SetNum(int targetNum, int collectNum)
	{
		int num = CalculateNum(targetNum, collectNum);
		base.SetNum(num);
	}

	public int GetTargetNum()
	{
		return mTargetNum;
	}

	public int GetCollectNum()
	{
		return mCollectNum;
	}
}
