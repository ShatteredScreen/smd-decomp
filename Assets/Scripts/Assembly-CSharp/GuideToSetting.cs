public class GuideToSetting
{
	[MiniJSONAlias("linkbanner")]
	public LinkBannerSetting LinkBanner { get; set; }

	[MiniJSONAlias("rec")]
	public int Rec { get; set; }
}
