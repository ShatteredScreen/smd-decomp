using System.Collections.Generic;
using UnityEngine;

public class PartnerListItem : SMVerticalListItem
{
	public bool enable;

	public CompanionData data;

	public byte category = byte.MaxValue;

	public static List<SMVerticalListItem> CreatePartnerList(bool aLastUseIcon = false)
	{
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		List<PartnerListItem> list2 = new List<PartnerListItem>();
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (aLastUseIcon)
		{
			PartnerListItem partnerListItem = new PartnerListItem();
			partnerListItem.data = instance.mCompanionData[0];
			partnerListItem.enable = true;
			list.Add(partnerListItem);
		}
		for (int i = 0; i < instance.mCompanionData.Count; i++)
		{
			if ((GameMain.USE_DEBUG_DIALOG || instance.mCompanionData[i].sortCategory != byte.MaxValue) && instance.mPlayer.IsCompanionUnlock(instance.mCompanionData[i].index) && !instance.mCompanionData[i].IsExpanded())
			{
				int index = i;
				if (instance.mCompanionData[i].HaveExpand() && instance.mCompanionData[i].IsExpandBase())
				{
					index = instance.getExpandedCompanionID(i);
				}
				PartnerListItem partnerListItem = new PartnerListItem();
				partnerListItem.data = instance.mCompanionData[index];
				partnerListItem.enable = instance.mPlayer.IsCompanionUnlock(instance.mCompanionData[index].index);
				list2.Add(partnerListItem);
			}
		}
		list2.Sort(CompareByCategoryAndOrder);
		byte b = byte.MaxValue;
		int num = 0;
		if (aLastUseIcon)
		{
			num = 3;
		}
		while (list2.Count > 0)
		{
			PartnerListItem partnerListItem = list2[0];
			if (partnerListItem.data.sortCategory != b)
			{
				while (num > 0)
				{
					PartnerListItem item = new PartnerListItem();
					list.Add(item);
					num--;
				}
				b = partnerListItem.data.sortCategory;
				for (int j = 0; j < 4; j++)
				{
					PartnerListItem partnerListItem2 = new PartnerListItem();
					if (j == 0)
					{
						partnerListItem2.category = b;
					}
					list.Add(partnerListItem2);
				}
			}
			list.Add(partnerListItem);
			list2.RemoveAt(0);
			num--;
			num &= 3;
		}
		num = (4 - list.Count % 4) % 4;
		for (int k = 0; k < num; k++)
		{
			PartnerListItem partnerListItem = new PartnerListItem();
			list.Add(partnerListItem);
		}
		return list;
	}

	private static int CompareByCategoryAndOrder(PartnerListItem a, PartnerListItem b)
	{
		if (a.data.sortCategory == b.data.sortCategory)
		{
			return a.data.sortOder - b.data.sortOder;
		}
		return a.data.sortCategory - b.data.sortCategory;
	}

	public static void CreateCategoryRibbon(GameObject aParent, int aDepth, byte aCategory)
	{
		ResImage resImage = ResourceManager.LoadImage("ICON");
		UISprite uISprite = Util.CreateSprite("CategoryRibbon", aParent, resImage.Image);
		Util.SetSpriteInfo(uISprite, "iconchoice_ribbon", aDepth, new Vector3(185f, -20f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(400, 74);
		Vector3[] array = new Vector3[4]
		{
			new Vector3(-92f, 27f, 0f),
			new Vector3(91f, 27f, 0f),
			new Vector3(-92f, -23f, 0f),
			new Vector3(91f, -23f, 0f)
		};
		Vector3[] array2 = new Vector3[4]
		{
			Vector3.one,
			new Vector3(-1f, 1f, 1f),
			new Vector3(1f, -1f, 1f),
			new Vector3(-1f, -1f, 1f)
		};
		for (int i = 0; i < array.Length; i++)
		{
			UISprite uISprite2 = Util.CreateSprite("RibbonLine", uISprite.gameObject, resImage.Image);
			Util.SetSpriteInfo(uISprite2, "iconchoice_line", aDepth + 1, array[i], Vector3.one, false);
			uISprite2.type = UIBasicSprite.Type.Sliced;
			uISprite2.SetDimensions(184, 12);
			uISprite2.transform.localScale = array2[i];
		}
		UIFont atlasFont = GameMain.LoadFont();
		UILabel label = Util.CreateLabel("Category", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(label, GetCategoryStr(aCategory), aDepth + 2, new Vector3(0f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
	}

	public static string GetCategoryStr(byte aCategory)
	{
		string result = string.Empty;
		switch (aCategory)
		{
		case 10:
			result = Localization.Get("SeasonTitle_00");
			break;
		case 20:
			result = Localization.Get("SeasonTitle_01");
			break;
		case 30:
			result = Localization.Get("SeasonTitle_02");
			break;
		case 40:
			result = Localization.Get("SeasonTitle_03");
			break;
		case 50:
			result = Localization.Get("SeasonTitle_04");
			break;
		case 60:
			result = Localization.Get("CharaCategory_05");
			break;
		case 70:
			result = Localization.Get("CharaCategory_06");
			break;
		case 80:
			result = Localization.Get("CharaCategory_07");
			break;
		case 90:
			result = Localization.Get("CharaCategory_08");
			break;
		}
		return result;
	}
}
