public class ConnectResult
{
	public enum ERROR_TYPE
	{
		None = 0,
		Error = 1,
		Abandon = 2,
		Transfered = 3,
		NoConnect = 100
	}

	public int API { get; private set; }

	public ERROR_TYPE ErrorType { get; private set; }

	public bool IsError
	{
		get
		{
			return ErrorType != ERROR_TYPE.None;
		}
	}

	public ConnectResult(int a_api, ERROR_TYPE a_type)
	{
		API = a_api;
		ErrorType = a_type;
	}

	public ConnectResult(int a_api)
	{
		API = a_api;
		ErrorType = ERROR_TYPE.NoConnect;
	}
}
