using UnityEngine;

public class FBIconRequest
{
	public int uuid { get; set; }

	public string fbid { get; set; }

	public Texture2D icon { get; set; }

	public bool isDone { get; set; }

	public bool isPlayer { get; set; }

	public FBIconRequest()
	{
		icon = null;
		isDone = false;
		isPlayer = false;
	}
}
