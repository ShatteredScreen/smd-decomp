public class MissionCountPuzzle
{
	public int mHeart { get; set; }

	public int mRed { get; set; }

	public int mBlue { get; set; }

	public int mGreen { get; set; }

	public int mStar { get; set; }

	public int mMoon { get; set; }

	public int mBarrier { get; set; }

	public int mAccessory { get; set; }

	public int mCrystal { get; set; }

	public int mLuna { get; set; }

	public int mIncrease { get; set; }

	public int mPresent { get; set; }

	public int mCake { get; set; }

	public MissionCountPuzzle()
	{
		Init();
	}

	public void Init()
	{
		mHeart = 0;
		mRed = 0;
		mBlue = 0;
		mGreen = 0;
		mStar = 0;
		mMoon = 0;
		mBarrier = 0;
		mAccessory = 0;
		mCrystal = 0;
		mLuna = 0;
		mIncrease = 0;
		mPresent = 0;
		mCake = 0;
	}
}
