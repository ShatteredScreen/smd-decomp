public class TipsPanelSetting : SlidePanelSetting
{
	public string TipsKey = string.Empty;

	public TipsPanelSetting(string a_key, int width, int height, int index = -1)
		: base(width, height, index)
	{
		TipsKey = a_key;
	}
}
