public class SMMapSetting : SMMapPageSettingBase
{
	public short ModuleMaxPageNum;

	public short MaxLevel;

	public short AvailableMaxLevel;

	public short ModuleMaxLevel;

	public short MaxRoadBlockID;

	public short ModuleMaxRoadBlockID;

	public short MaxChapterID;

	public short ModuleMaxChapterID;

	public short SeasonLastDemoID = -1;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteShort((short)mCourseNo);
		a_writer.WriteShort(ModuleMaxPageNum);
		a_writer.WriteShort(MaxLevel);
		a_writer.WriteShort(AvailableMaxLevel);
		a_writer.WriteShort(ModuleMaxLevel);
		a_writer.WriteShort(MaxRoadBlockID);
		a_writer.WriteShort(ModuleMaxRoadBlockID);
		a_writer.WriteShort(MaxChapterID);
		a_writer.WriteShort(ModuleMaxChapterID);
		a_writer.WriteShort(SeasonLastDemoID);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		mCourseNo = a_reader.ReadShort();
		ModuleMaxPageNum = a_reader.ReadShort();
		MaxLevel = a_reader.ReadShort();
		AvailableMaxLevel = a_reader.ReadShort();
		ModuleMaxLevel = a_reader.ReadShort();
		MaxRoadBlockID = a_reader.ReadShort();
		ModuleMaxRoadBlockID = a_reader.ReadShort();
		MaxChapterID = a_reader.ReadShort();
		ModuleMaxChapterID = a_reader.ReadShort();
		SeasonLastDemoID = a_reader.ReadShort();
		mStageNo = (short)mCourseNo;
		mSubNo = 0;
	}
}
