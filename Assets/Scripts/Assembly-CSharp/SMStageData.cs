using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using UnityEngine;

public class SMStageData : ISMStageData
{
	public class EventArgs
	{
		public int StageNumber { get; set; }

		public ISMStageData StageData { get; set; }

		public object UserData { get; set; }

		public string Error { get; set; }

		public string UserSegment { get; set; }

		public ISMStageData AssetData { get; set; }

		public ISMStageData CacheData { get; set; }

		public int CacheTimestamp { get; set; }

		public ISMStageData RemoteData { get; set; }

		public int RemoteTimestamp { get; set; }
	}

	public delegate void CreateFinishHandler(int stage, EventArgs e);

	private ISMStageData mStageData;

	private ISMStageData mFallbackStageData;

	private static ManualResetEvent mNotfiyLoadStep = new ManualResetEvent(false);

	private static volatile bool mFinishLoadStep = false;

	private static bool IsRemoteAvailable
	{
		get
		{
			return true;
		}
	}

	private ISMStageData Original
	{
		get
		{
			return mStageData;
		}
	}

	private ISMStageData Current
	{
		get
		{
			if (mFallbackStageData != null)
			{
				return mFallbackStageData;
			}
			return mStageData;
		}
	}

	private bool IsUseFallback
	{
		get
		{
			return mFallbackStageData != null;
		}
	}

	public int FormatVersion
	{
		get
		{
			return Current.FormatVersion;
		}
	}

	public int OriginalFormatVersion
	{
		get
		{
			return Current.OriginalFormatVersion;
		}
	}

	public int Revision
	{
		get
		{
			return Current.Revision;
		}
	}

	public Def.SERIES Series
	{
		get
		{
			return Current.Series;
		}
		set
		{
			Current.Series = value;
		}
	}

	public int StageNumber
	{
		get
		{
			return Current.StageNumber;
		}
		set
		{
			Current.StageNumber = value;
		}
	}

	public int EventID
	{
		get
		{
			return Current.EventID;
		}
		set
		{
			Current.EventID = value;
		}
	}

	public string DisplayStageNumber
	{
		get
		{
			return Current.DisplayStageNumber;
		}
		set
		{
			Current.DisplayStageNumber = value;
		}
	}

	public int MainStageNumber
	{
		get
		{
			return Current.MainStageNumber;
		}
		set
		{
			Current.MainStageNumber = value;
		}
	}

	public int SubStageNumber
	{
		get
		{
			return Current.SubStageNumber;
		}
		set
		{
			Current.SubStageNumber = value;
		}
	}

	public int CandyKindNum
	{
		get
		{
			return Current.CandyKindNum;
		}
	}

	public bool UseColor0
	{
		get
		{
			return Current.UseColor0;
		}
	}

	public bool UseColor1
	{
		get
		{
			return Current.UseColor1;
		}
	}

	public bool UseColor2
	{
		get
		{
			return Current.UseColor2;
		}
	}

	public bool UseColor3
	{
		get
		{
			return Current.UseColor3;
		}
	}

	public bool UseColor4
	{
		get
		{
			return Current.UseColor4;
		}
	}

	public bool UseColor5
	{
		get
		{
			return Current.UseColor5;
		}
	}

	public bool UseColor6
	{
		get
		{
			return Current.UseColor6;
		}
	}

	public Def.STAGE_TYPE StageType
	{
		get
		{
			return Current.StageType;
		}
	}

	public Def.STAGE_WIN_CONDITION WinType
	{
		get
		{
			return Current.WinType;
		}
	}

	public Def.STAGE_LOSE_CONDITION LoseType
	{
		get
		{
			return Current.LoseType;
		}
	}

	public int LimitMoves
	{
		get
		{
			return Current.LimitMoves;
		}
	}

	public int LimitTimeSec
	{
		get
		{
			return Current.LimitTimeSec;
		}
	}

	public int OverwriteLimitMoves
	{
		get
		{
			return Current.OverwriteLimitMoves;
		}
		set
		{
			Current.OverwriteLimitMoves = value;
		}
	}

	public int OverwriteLimitTimeSec
	{
		get
		{
			return Current.OverwriteLimitTimeSec;
		}
		set
		{
			Current.OverwriteLimitTimeSec = value;
		}
	}

	public int BronzeScore
	{
		get
		{
			return Current.BronzeScore;
		}
	}

	public int SilverScore
	{
		get
		{
			return Current.SilverScore;
		}
	}

	public int GoldScore
	{
		get
		{
			return Current.GoldScore;
		}
	}

	public int Color0
	{
		get
		{
			return Current.Color0;
		}
	}

	public int Color1
	{
		get
		{
			return Current.Color1;
		}
	}

	public int Color2
	{
		get
		{
			return Current.Color2;
		}
	}

	public int Color3
	{
		get
		{
			return Current.Color3;
		}
	}

	public int Color4
	{
		get
		{
			return Current.Color4;
		}
	}

	public int Color5
	{
		get
		{
			return Current.Color5;
		}
	}

	public int Color6
	{
		get
		{
			return Current.Color6;
		}
	}

	public int Ingredient0
	{
		get
		{
			return Current.Ingredient0;
		}
	}

	public int Ingredient1
	{
		get
		{
			return Current.Ingredient1;
		}
	}

	public int Ingredient2
	{
		get
		{
			return Current.Ingredient2;
		}
	}

	public int Ingredient3
	{
		get
		{
			return Current.Ingredient3;
		}
	}

	public int Paper0
	{
		get
		{
			return Current.Paper0;
		}
	}

	public int Paper1
	{
		get
		{
			return Current.Paper1;
		}
	}

	public int Paper2
	{
		get
		{
			return Current.Paper2;
		}
	}

	public Def.BOOSTER_KIND BoosterSlot1
	{
		get
		{
			return Current.BoosterSlot1;
		}
	}

	public Def.BOOSTER_KIND BoosterSlot2
	{
		get
		{
			return Current.BoosterSlot2;
		}
	}

	public Def.BOOSTER_KIND BoosterSlot3
	{
		get
		{
			return Current.BoosterSlot3;
		}
	}

	public Def.BOOSTER_KIND BoosterSlot4
	{
		get
		{
			return Current.BoosterSlot4;
		}
	}

	public Def.BOOSTER_KIND BoosterSlot5
	{
		get
		{
			return Current.BoosterSlot5;
		}
	}

	public Def.BOOSTER_KIND BoosterSlot6
	{
		get
		{
			return Current.BoosterSlot6;
		}
	}

	public Def.BOOSTER_KIND InPuzzleBoosterSlot1
	{
		get
		{
			return Current.InPuzzleBoosterSlot1;
		}
	}

	public Def.BOOSTER_KIND InPuzzleBoosterSlot2
	{
		get
		{
			return Current.InPuzzleBoosterSlot2;
		}
	}

	public Def.BOOSTER_KIND InPuzzleBoosterSlot3
	{
		get
		{
			return Current.InPuzzleBoosterSlot3;
		}
	}

	public Def.BOOSTER_KIND InPuzzleBoosterSlot4
	{
		get
		{
			return Current.InPuzzleBoosterSlot4;
		}
	}

	public float BonusFreq1
	{
		get
		{
			return Current.BonusFreq1;
		}
	}

	public float BonusMove1
	{
		get
		{
			return Current.BonusMove1;
		}
	}

	public int BonusLimit1
	{
		get
		{
			return Current.BonusLimit1;
		}
	}

	public int BonusMoveLimit1
	{
		get
		{
			return Current.BonusMoveLimit1;
		}
	}

	public int Wave
	{
		get
		{
			return Current.Wave;
		}
	}

	public int Bomb
	{
		get
		{
			return Current.Bomb;
		}
	}

	public int Rainbow
	{
		get
		{
			return Current.Rainbow;
		}
	}

	public int WaveWave
	{
		get
		{
			return Current.WaveWave;
		}
	}

	public int BombBomb
	{
		get
		{
			return Current.BombBomb;
		}
	}

	public int RainbowRainbow
	{
		get
		{
			return Current.RainbowRainbow;
		}
	}

	public int WaveBomb
	{
		get
		{
			return Current.WaveBomb;
		}
	}

	public int BombRainbow
	{
		get
		{
			return Current.BombRainbow;
		}
	}

	public int WaveRainbow
	{
		get
		{
			return Current.WaveRainbow;
		}
	}

	public string BGM
	{
		get
		{
			return Current.BGM;
		}
	}

	public string EventCode
	{
		get
		{
			return Current.EventCode;
		}
	}

	public string SegmentCode
	{
		get
		{
			return Current.SegmentCode;
		}
	}

	public string TestCode
	{
		get
		{
			return Current.TestCode;
		}
	}

	public int Timer0
	{
		get
		{
			return Current.Timer0;
		}
	}

	public int Timer1
	{
		get
		{
			return Current.Timer1;
		}
	}

	public int Timer2
	{
		get
		{
			return Current.Timer2;
		}
	}

	public int Timer3
	{
		get
		{
			return Current.Timer3;
		}
	}

	public int Timer4
	{
		get
		{
			return Current.Timer4;
		}
	}

	public int Timer5
	{
		get
		{
			return Current.Timer5;
		}
	}

	public float TimerFreq
	{
		get
		{
			return Current.TimerFreq;
		}
	}

	public int TimerLimit
	{
		get
		{
			return Current.TimerLimit;
		}
	}

	public int TimerRemainMin
	{
		get
		{
			return Current.TimerRemainMin;
		}
	}

	public int TimerRemainMax
	{
		get
		{
			return Current.TimerRemainMax;
		}
	}

	public int Pair0
	{
		get
		{
			return Current.Pair0;
		}
	}

	public int PairLimit0
	{
		get
		{
			return Current.PairLimit0;
		}
	}

	public float JewelFreq
	{
		get
		{
			return Current.JewelFreq;
		}
	}

	public int JewelLimit
	{
		get
		{
			return Current.JewelLimit;
		}
	}

	public float FCookie1Freq
	{
		get
		{
			return Current.FCookie1Freq;
		}
	}

	public int FCookie1Limit
	{
		get
		{
			return Current.FCookie1Limit;
		}
	}

	public float FCookie2Freq
	{
		get
		{
			return Current.FCookie2Freq;
		}
	}

	public int FCookie2Limit
	{
		get
		{
			return Current.FCookie2Limit;
		}
	}

	public float FCookie3Freq
	{
		get
		{
			return Current.FCookie3Freq;
		}
	}

	public int FCookie3Limit
	{
		get
		{
			return Current.FCookie3Limit;
		}
	}

	public int BossHP
	{
		get
		{
			return Current.BossHP;
		}
	}

	public Def.TILE_KIND BossType
	{
		get
		{
			return Current.BossType;
		}
	}

	public int BossPositionX
	{
		get
		{
			return Current.BossPositionX;
		}
	}

	public int BossPositionY
	{
		get
		{
			return Current.BossPositionY;
		}
	}

	public int AttackCandyNum
	{
		get
		{
			return Current.AttackCandyNum;
		}
	}

	public bool AttackUseCandy0
	{
		get
		{
			return Current.AttackUseCandy0;
		}
	}

	public bool AttackUseCandy1
	{
		get
		{
			return Current.AttackUseCandy1;
		}
	}

	public bool AttackUseCandy2
	{
		get
		{
			return Current.AttackUseCandy2;
		}
	}

	public bool AttackUseCandy3
	{
		get
		{
			return Current.AttackUseCandy3;
		}
	}

	public bool AttackUseCandy4
	{
		get
		{
			return Current.AttackUseCandy4;
		}
	}

	public bool AttackUseCandy5
	{
		get
		{
			return Current.AttackUseCandy5;
		}
	}

	public bool AttackUseCandy6
	{
		get
		{
			return Current.AttackUseCandy6;
		}
	}

	public int BossFirstAttack
	{
		get
		{
			return Current.BossFirstAttack;
		}
	}

	public int BossAttackInterval
	{
		get
		{
			return Current.BossAttackInterval;
		}
	}

	public int SilverCrystal
	{
		get
		{
			return Current.SilverCrystal;
		}
	}

	public int EnemyGhost
	{
		get
		{
			return Current.EnemyGhost;
		}
	}

	public int BossID
	{
		get
		{
			return Current.BossID;
		}
	}

	public Def.BOSS_ATTACK BossAttackPattern
	{
		get
		{
			return Current.BossAttackPattern;
		}
	}

	public int BossAttackParam
	{
		get
		{
			return Current.BossAttackParam;
		}
	}

	public string BossWeakChara
	{
		get
		{
			return Current.BossWeakChara;
		}
	}

	public int FindStencil0
	{
		get
		{
			return Current.FindStencil0;
		}
	}

	public int FindStencil1
	{
		get
		{
			return Current.FindStencil1;
		}
	}

	public int FindStencil2
	{
		get
		{
			return Current.FindStencil2;
		}
	}

	public int FindStencil3
	{
		get
		{
			return Current.FindStencil3;
		}
	}

	public List<StencilData> StencilDataList
	{
		get
		{
			return Current.StencilDataList;
		}
	}

	public int BossAttackParam2
	{
		get
		{
			return Current.BossAttackParam2;
		}
	}

	public int BossAttackParam3
	{
		get
		{
			return Current.BossAttackParam3;
		}
	}

	public float FOrb1Freq
	{
		get
		{
			return Current.FOrb1Freq;
		}
	}

	public int FOrb1Limit
	{
		get
		{
			return Current.FOrb1Limit;
		}
	}

	public float FOrb2Freq
	{
		get
		{
			return Current.FOrb2Freq;
		}
	}

	public int FOrb2Limit
	{
		get
		{
			return Current.FOrb2Limit;
		}
	}

	public float FOrb3Freq
	{
		get
		{
			return Current.FOrb3Freq;
		}
	}

	public int FOrb3Limit
	{
		get
		{
			return Current.FOrb3Limit;
		}
	}

	public float FOrb4Freq
	{
		get
		{
			return Current.FOrb4Freq;
		}
	}

	public int FOrb4Limit
	{
		get
		{
			return Current.FOrb4Limit;
		}
	}

	public float FootPieceFreq1
	{
		get
		{
			return Current.FootPieceFreq1;
		}
	}

	public int FootPieceLimit1
	{
		get
		{
			return Current.FootPieceLimit1;
		}
	}

	public float FJewelFreq1
	{
		get
		{
			return Current.FJewelFreq1;
		}
		set
		{
			Current.FJewelFreq1 = value;
		}
	}

	public int FJewel1Limit1
	{
		get
		{
			return Current.FJewel1Limit1;
		}
		set
		{
			Current.FJewel1Limit1 = value;
		}
	}

	public int FJewel3Limit1
	{
		get
		{
			return Current.FJewel3Limit1;
		}
		set
		{
			Current.FJewel3Limit1 = value;
		}
	}

	public int FJewel5Limit1
	{
		get
		{
			return Current.FJewel5Limit1;
		}
		set
		{
			Current.FJewel5Limit1 = value;
		}
	}

	public int FJewel9Limit1
	{
		get
		{
			return Current.FJewel9Limit1;
		}
		set
		{
			Current.FJewel9Limit1 = value;
		}
	}

	public float FPerl00Freq1
	{
		get
		{
			return Current.FPerl00Freq1;
		}
	}

	public int FPerl00Limit1
	{
		get
		{
			return Current.FPerl00Limit1;
		}
	}

	public float FPerl01Freq1
	{
		get
		{
			return Current.FPerl01Freq1;
		}
	}

	public int FPerl01Limit1
	{
		get
		{
			return Current.FPerl01Limit1;
		}
	}

	public float FPerl02Freq1
	{
		get
		{
			return Current.FPerl02Freq1;
		}
	}

	public int FPerl02Limit1
	{
		get
		{
			return Current.FPerl02Limit1;
		}
	}

	public List<IntVector2> DianaWayPosList
	{
		get
		{
			return Current.DianaWayPosList;
		}
	}

	public int Width
	{
		get
		{
			return Original.Width;
		}
	}

	public int Height
	{
		get
		{
			return Original.Height;
		}
	}

	public int PlayAreaX
	{
		get
		{
			return Original.PlayAreaX;
		}
	}

	public int PlayAreaY
	{
		get
		{
			return Original.PlayAreaY;
		}
	}

	public int PlayAreaWidth
	{
		get
		{
			return Original.PlayAreaWidth;
		}
	}

	public int PlayAreaHeight
	{
		get
		{
			return Original.PlayAreaHeight;
		}
	}

	private static float MAX_TIMEOUT
	{
		get
		{
			return (float)Server.Timeout + 3f;
		}
	}

	private SMStageData(ISMStageData stageData, ISMStageData fallbackStageData = null)
	{
		mStageData = stageData;
		mFallbackStageData = fallbackStageData;
	}

	public Def.TILE_KIND[] GetNormaKindArray()
	{
		if (WinType == Def.STAGE_WIN_CONDITION.COLLECT)
		{
			return new Def.TILE_KIND[4]
			{
				Def.TILE_KIND.INGREDIENT0,
				Def.TILE_KIND.INGREDIENT1,
				Def.TILE_KIND.INGREDIENT2,
				Def.TILE_KIND.INGREDIENT3
			};
		}
		if (WinType == Def.STAGE_WIN_CONDITION.EATEN)
		{
			return new Def.TILE_KIND[7]
			{
				Def.TILE_KIND.COLOR0,
				Def.TILE_KIND.COLOR1,
				Def.TILE_KIND.COLOR2,
				Def.TILE_KIND.COLOR3,
				Def.TILE_KIND.COLOR4,
				Def.TILE_KIND.COLOR5,
				Def.TILE_KIND.COLOR6
			};
		}
		if (WinType == Def.STAGE_WIN_CONDITION.CLEAN)
		{
			return new Def.TILE_KIND[3]
			{
				Def.TILE_KIND.THROUGH_WALL0,
				Def.TILE_KIND.THROUGH_WALL1,
				Def.TILE_KIND.THROUGH_WALL2
			};
		}
		if (WinType == Def.STAGE_WIN_CONDITION.GROWUP)
		{
			return new Def.TILE_KIND[1] { Def.TILE_KIND.GROWN_UP0 };
		}
		if (WinType == Def.STAGE_WIN_CONDITION.FIND)
		{
			return new Def.TILE_KIND[4]
			{
				Def.TILE_KIND.FIND0,
				Def.TILE_KIND.FIND1,
				Def.TILE_KIND.FIND2,
				Def.TILE_KIND.FIND3
			};
		}
		if (WinType == Def.STAGE_WIN_CONDITION.SPECIAL_EATEN)
		{
			return new Def.TILE_KIND[9]
			{
				Def.TILE_KIND.WAVE,
				Def.TILE_KIND.BOMB,
				Def.TILE_KIND.RAINBOW,
				Def.TILE_KIND.WAVE_WAVE,
				Def.TILE_KIND.BOMB_BOMB,
				Def.TILE_KIND.RAINBOW_RAINBOW,
				Def.TILE_KIND.WAVE_BOMB,
				Def.TILE_KIND.BOMB_RAINBOW,
				Def.TILE_KIND.WAVE_RAINBOW
			};
		}
		if (WinType == Def.STAGE_WIN_CONDITION.ENEMY)
		{
			return new Def.TILE_KIND[1] { Def.TILE_KIND.INCREASE };
		}
		if (WinType == Def.STAGE_WIN_CONDITION.PAIR)
		{
			return new Def.TILE_KIND[1] { Def.TILE_KIND.PAIR0_COMPLETE };
		}
		if (WinType == Def.STAGE_WIN_CONDITION.TALISMAN)
		{
			return new Def.TILE_KIND[3]
			{
				Def.TILE_KIND.TALISMAN0_0,
				Def.TILE_KIND.TALISMAN1_0,
				Def.TILE_KIND.TALISMAN2_0
			};
		}
		if (WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			return new Def.TILE_KIND[1] { Def.TILE_KIND.STAR };
		}
		return null;
	}

	public int GetTargetNum(Def.TILE_KIND a_kind)
	{
		int result = 0;
		switch (a_kind)
		{
		case Def.TILE_KIND.COLOR0:
			result = Color0;
			break;
		case Def.TILE_KIND.COLOR1:
			result = Color1;
			break;
		case Def.TILE_KIND.COLOR2:
			result = Color2;
			break;
		case Def.TILE_KIND.COLOR3:
			result = Color3;
			break;
		case Def.TILE_KIND.COLOR4:
			result = Color4;
			break;
		case Def.TILE_KIND.COLOR5:
			result = Color5;
			break;
		case Def.TILE_KIND.COLOR6:
			result = Color6;
			break;
		case Def.TILE_KIND.INGREDIENT0:
			result = Ingredient0;
			break;
		case Def.TILE_KIND.INGREDIENT1:
			result = Ingredient1;
			break;
		case Def.TILE_KIND.INGREDIENT2:
			result = Ingredient2;
			break;
		case Def.TILE_KIND.INGREDIENT3:
			result = Ingredient3;
			break;
		case Def.TILE_KIND.THROUGH_WALL0:
			result = Paper0 + Paper1 + Paper2;
			break;
		case Def.TILE_KIND.THROUGH_WALL1:
		case Def.TILE_KIND.THROUGH_WALL2:
			result = 0;
			break;
		case Def.TILE_KIND.GROWN_UP0:
			result = SilverCrystal;
			break;
		case Def.TILE_KIND.GROWN_UP1:
		case Def.TILE_KIND.GROWN_UP2:
			result = 0;
			break;
		case Def.TILE_KIND.FIND0:
			result = FindStencil0;
			break;
		case Def.TILE_KIND.FIND1:
			result = FindStencil1;
			break;
		case Def.TILE_KIND.FIND2:
			result = FindStencil2;
			break;
		case Def.TILE_KIND.FIND3:
			result = FindStencil3;
			break;
		case Def.TILE_KIND.INCREASE:
			result = EnemyGhost;
			break;
		case Def.TILE_KIND.PAIR0_COMPLETE:
			result = Pair0;
			break;
		case Def.TILE_KIND.WAVE:
			result = Wave;
			break;
		case Def.TILE_KIND.BOMB:
			result = Bomb;
			break;
		case Def.TILE_KIND.RAINBOW:
			result = Rainbow;
			break;
		case Def.TILE_KIND.WAVE_WAVE:
			result = WaveWave;
			break;
		case Def.TILE_KIND.BOMB_BOMB:
			result = BombBomb;
			break;
		case Def.TILE_KIND.RAINBOW_RAINBOW:
			result = RainbowRainbow;
			break;
		case Def.TILE_KIND.WAVE_BOMB:
			result = WaveBomb;
			break;
		case Def.TILE_KIND.BOMB_RAINBOW:
			result = BombRainbow;
			break;
		case Def.TILE_KIND.WAVE_RAINBOW:
			result = WaveRainbow;
			break;
		case Def.TILE_KIND.TALISMAN0_0:
		case Def.TILE_KIND.TALISMAN1_0:
		case Def.TILE_KIND.TALISMAN2_0:
			result = 1;
			break;
		case Def.TILE_KIND.STAR:
			result = 1;
			break;
		}
		return result;
	}

	public virtual void SetJewelSetting(float a_freq, int a_limit1, int a_limit3, int a_limit5, int a_limit9)
	{
		FJewelFreq1 = a_freq;
		FJewel1Limit1 = a_limit1;
		FJewel3Limit1 = a_limit3;
		FJewel5Limit1 = a_limit5;
		FJewel9Limit1 = a_limit9;
	}

	public ISMTile GetTile(int x, int y)
	{
		return Original.GetTile(x, y);
	}

	public ISMAttributeTile GetAttributeTile(int x, int y)
	{
		return Original.GetAttributeTile(x, y);
	}

	public ISMTileInfo GetTileInfo(int x, int y)
	{
		return Original.GetTileInfo(x, y);
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			object value = propertyInfo.GetValue(this, null);
			if (value == null)
			{
				stringBuilder.Append(value);
			}
			else
			{
				stringBuilder.Append(Convert.ToString(value));
			}
		}
		return string.Format("[StageData: {0}]", stringBuilder.ToString());
	}

	private static string GetAssetFileName(Def.SERIES a_series, int stage, int eventID)
	{
		string stageFilename = GetStageFilename(a_series, stage, eventID);
		return string.Format("Data/Stages/Stage_{0}/lvl_{0}.bin", stageFilename);
	}

	private static string GetUpdateFileName(Def.SERIES a_series, int stage, int eventID)
	{
		string stageFilename = GetStageFilename(a_series, stage, eventID);
		return string.Format("lvl_{0}.bin.bytes", stageFilename);
	}

	private static string GetCachePath()
	{
		string tempPath = BIJUnity.getTempPath();
		return Path.Combine(tempPath, "clientdata");
	}

	public static void ClearCache()
	{
		try
		{
			string cachePath = GetCachePath();
			if (Directory.Exists(cachePath))
			{
				Directory.Delete(cachePath, true);
			}
		}
		catch (Exception)
		{
		}
	}

	public static string GetStageFilename(Def.SERIES a_series, int stage, int eventID = -1)
	{
		string arg = Def.SeriesPrefix[a_series];
		int a_main;
		int a_sub;
		Def.SplitStageNo(stage, out a_main, out a_sub);
		if ((a_series == Def.SERIES.SM_EV && eventID != -1) || (a_series == Def.SERIES.SM_ADV && eventID != -1))
		{
			arg = Def.GetSeriesString(a_series, eventID);
		}
		if (a_series == Def.SERIES.SM_ADV)
		{
			return string.Format(Def.StageNameFormatAdv, arg, a_main, a_sub);
		}
		return string.Format(Def.StageNameFormat, arg, a_main, a_sub);
	}

	public static IEnumerator CreateAsync(Def.SERIES a_series, int stage, string userSegment, CreateFinishHandler fn, object userData = null)
	{
		return CreateAsync(a_series, -1, stage, userSegment, fn, userData);
	}

	public static IEnumerator CreateAsync(Def.SERIES a_series, int eventID, int stage, string userSegment, CreateFinishHandler fn, object userData = null)
	{
		int WAIT_TIME_MS = 1000 / ((Application.targetFrameRate != -1) ? Application.targetFrameRate : Def.TARGET_FRAMERATE);
		float _start = Time.realtimeSinceStartup;
		string client_ver = Def.ClientDataVersionStr;
		EventArgs args = new EventArgs
		{
			StageNumber = stage,
			UserSegment = ((!string.IsNullOrEmpty(userSegment)) ? userSegment : "1"),
			UserData = userData
		};
		if (GameMain.USE_RESOURCEDL)
		{
			ResourceDownloadManager dLManager = SingletonMonoBehaviour<GameMain>.Instance.mDLManager;
			string _updateName = GetUpdateFileName(a_series, stage, eventID);
			if (dLManager.IsInDownloadFileList(_updateName))
			{
				ResourceDownloadManager.KIND kind = ResourceDownloadManager.getKindWithFileName(_updateName);
				if (!dLManager.CheckDLFileIntegrity(kind, _updateName, true))
				{
					bool isDlSuccess = false;
					yield return SingletonMonoBehaviour<GameMain>.Instance.StartCoroutine(DownloadStageData(kind, _updateName, delegate(bool i)
					{
						isDlSuccess = i;
					}));
					if (!isDlSuccess)
					{
						if (fn != null)
						{
							fn(stage, null);
						}
						yield break;
					}
				}
			}
		}
		bool ret = CreateAsync_00(a_series, client_ver, eventID, stage, ref args);
		yield return 0;
		args.StageData = new SMStageData(args.AssetData);
		if (fn != null)
		{
			fn(stage, args);
		}
	}

	public static IEnumerator DownloadStageData(ResourceDownloadManager.KIND aKind, string aFileName, Action<bool> aCallback)
	{
		bool ret = false;
		bool complete = false;
		GameMain game = SingletonMonoBehaviour<GameMain>.Instance;
		ResourceDownloadManager dLManager = game.mDLManager;
		bool connecting = Server.ManualConnecting;
		Server.ManualConnecting = true;
		string url = dLManager.GetDownloadFileURL(aKind, aFileName);
		string savePath = dLManager.GetDownloadFilePath(aKind, aFileName);
		game.RetryCount = 0;
		while (!complete)
		{
			WWW www2 = new WWW(url);
			float progress = www2.progress;
			DateTime startTime = DateTime.Now;
			bool timeout = false;
			yield return null;
			while (!www2.isDone && string.IsNullOrEmpty(www2.error) && !timeout)
			{
				if (progress == www2.progress)
				{
					long span = DateTimeUtil.BetweenSeconds(DateTime.Now, startTime);
					if (span > 30)
					{
						timeout = true;
						break;
					}
				}
				else
				{
					startTime = DateTime.Now;
					progress = www2.progress;
				}
				yield return null;
			}
			if (string.IsNullOrEmpty(www2.error) && !timeout)
			{
				try
				{
					using (BIJFileBinaryWriter writer = new BIJFileBinaryWriter(savePath))
					{
						writer.WriteAll(www2.bytes);
					}
				}
				catch (Exception ex)
				{
					Exception e = ex;
				}
				if (dLManager.CheckDLFileIntegrity(aKind, aFileName, true))
				{
					ret = true;
					complete = true;
				}
			}
			if (!complete)
			{
				bool closed = false;
				game.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_CLOSE, 0, delegate
				{
					closed = true;
				}, delegate
				{
					complete = true;
					closed = true;
				});
				while (!closed)
				{
					yield return null;
				}
			}
			www2.Dispose();
			www2 = null;
		}
		Server.ManualConnecting = connecting;
		aCallback(ret);
	}

	private static bool ChackSaveRequirement(string client_ver, int stage, ref EventArgs args)
	{
		if (args.CacheData != null && !IsValidStageData(client_ver, stage, args.CacheData, args.CacheTimestamp))
		{
			args.CacheTimestamp = 0;
			args.CacheData = null;
		}
		if (!IsValidStageData(client_ver, stage, args.RemoteData, args.RemoteTimestamp))
		{
			args.RemoteTimestamp = 0;
			args.RemoteData = null;
			return false;
		}
		bool result = false;
		if (args.RemoteData != null && args.RemoteTimestamp > 0)
		{
			if (args.CacheData == null)
			{
				result = true;
			}
			else if (args.RemoteTimestamp > args.CacheTimestamp)
			{
				result = true;
			}
		}
		return result;
	}

	private static bool IsValidStageData(string req_client_ver, int req_stage, ISMStageData stageData, int timestamp)
	{
		if (stageData == null)
		{
			return false;
		}
		if (stageData.StageNumber != req_stage)
		{
			return false;
		}
		if (stageData.FormatVersion > 10150)
		{
			return false;
		}
		return true;
	}

	private static bool CheckTimeout(int stage, float starttime, CreateFinishHandler fn, ref EventArgs args)
	{
		float num = Time.realtimeSinceStartup - starttime;
		if (num > MAX_TIMEOUT)
		{
			args.StageData = args.AssetData;
			if (fn != null)
			{
				fn(stage, args);
			}
			return true;
		}
		return false;
	}

	private static string GetCacheFilePath(Def.SERIES a_series, int stage, string client_ver)
	{
		string cachePath = GetCachePath();
		string stageFilename = GetStageFilename(a_series, stage);
		return string.Format("{0}/Stages/Stage_{2}/lvl_{2}.bin", cachePath, client_ver, stageFilename);
	}

	private static string GetRemoteFileURL(int stage, string client_ver)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.ClientData_URL);
		stringBuilder.Append(string.Format("{0}/getStageData.php", client_ver, stage));
		return stringBuilder.ToString();
	}

	private static int HttpDateTimestamp(int stage, HttpWebResponse _res)
	{
		try
		{
			string responseHeader = _res.GetResponseHeader("Last-Modified");
			DateTime dateTime = DateTimeUtil.HttpDateToDateTime(responseHeader);
			return (int)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime);
		}
		catch (Exception)
		{
		}
		return 0;
	}

	private static bool CreateAsync_00(Def.SERIES a_series, string client_ver, int eventID, int stage, ref EventArgs args)
	{
		try
		{
			SMStageDataImpl sMStageDataImpl = new SMStageDataImpl(a_series, eventID, stage);
			string assetFileName = GetAssetFileName(a_series, stage, eventID);
			string updateFileName = GetUpdateFileName(a_series, stage, eventID);
			BIJBinaryReader downloadFileReadStream = SingletonMonoBehaviour<GameMain>.Instance.mDLManager.GetDownloadFileReadStream(updateFileName);
			if (downloadFileReadStream == null)
			{
				using (BIJBinaryReader data = new BIJResourceBinaryReader(assetFileName))
				{
					sMStageDataImpl.Deserialize(data, 10150);
				}
			}
			else
			{
				sMStageDataImpl.Deserialize(downloadFileReadStream, 10150);
				downloadFileReadStream.Close();
			}
			sMStageDataImpl.SegmentCode = args.UserSegment;
			args.AssetData = sMStageDataImpl;
			return true;
		}
		catch (Exception ex)
		{
			args.StageData = null;
			args.Error = ex.ToString();
		}
		return false;
	}

	private static bool CreateAsync_01(Def.SERIES a_series, string client_ver, int stage, ref EventArgs args)
	{
		return CreateAsync_01(a_series, client_ver, -1, stage, ref args);
	}

	private static bool CreateAsync_01(Def.SERIES a_series, string client_ver, int eventID, int stage, ref EventArgs args)
	{
		try
		{
			SMStageDataImpl sMStageDataImpl = new SMStageDataImpl(a_series, eventID, stage);
			string cacheFilePath = GetCacheFilePath(a_series, stage, client_ver);
			if (!File.Exists(cacheFilePath))
			{
				args.CacheTimestamp = 0;
				args.CacheData = null;
				return false;
			}
			using (BIJBinaryReader data = new BIJFileBinaryReader(cacheFilePath))
			{
				sMStageDataImpl.Deserialize(data, 10150);
			}
			args.CacheTimestamp = (int)DateTimeUtil.DateTimeToUnixTimeStamp(File.GetLastWriteTime(cacheFilePath));
			args.CacheData = sMStageDataImpl;
			return true;
		}
		catch (Exception)
		{
			args.CacheTimestamp = 0;
			args.CacheData = null;
		}
		return false;
	}

	private static bool CreateAsync_02(string client_ver, int stage, ref EventArgs args)
	{
		try
		{
			mNotfiyLoadStep.Reset();
			string remoteFileURL = GetRemoteFileURL(stage, client_ver);
			Dictionary<string, object> p = new Dictionary<string, object>();
			Network.GetCommonParams(ref p);
			p["stg"] = stage;
			p["useg"] = args.UserSegment;
			HttpAsync httpAsync = Network.CreateHttpHead(remoteFileURL, ref p, Server.Timeout);
			httpAsync.GetResponse(delegate(HttpAsync _httpAsync)
			{
				HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
				EventArgs eventArgs = _httpAsync.UserData as EventArgs;
				try
				{
					if (_httpAsync.Result == HttpAsync.RESULT.SUCCESS && httpWebResponse != null)
					{
						eventArgs.RemoteTimestamp = HttpDateTimestamp(eventArgs.StageNumber, httpWebResponse);
					}
				}
				catch (Exception)
				{
					if (eventArgs != null)
					{
						eventArgs.RemoteTimestamp = 0;
					}
				}
				finally
				{
					if (httpWebResponse != null)
					{
						httpWebResponse.Close();
					}
				}
				mNotfiyLoadStep.Set();
			}, args);
			return true;
		}
		catch (Exception)
		{
		}
		return false;
	}

	private static bool CreateAsync_03(string client_ver, Def.SERIES a_series, int stage, ref EventArgs args)
	{
		return CreateAsync_03(client_ver, a_series, -1, stage, ref args);
	}

	private static bool CreateAsync_03(string client_ver, Def.SERIES a_series, int eventID, int stage, ref EventArgs args)
	{
		try
		{
			mFinishLoadStep = false;
			string remoteFileURL = GetRemoteFileURL(stage, client_ver);
			Dictionary<string, object> p = new Dictionary<string, object>();
			Network.GetCommonParams(ref p);
			p["stg"] = stage;
			p["useg"] = args.UserSegment;
			HttpAsync httpAsync = Network.CreateHttpPost(remoteFileURL, ref p, Server.Timeout);
			httpAsync.GetResponse(delegate(HttpAsync _httpAsync)
			{
				HttpWebResponse httpWebResponse = _httpAsync.Response as HttpWebResponse;
				EventArgs eventArgs = _httpAsync.UserData as EventArgs;
				try
				{
					if (_httpAsync.Result == HttpAsync.RESULT.SUCCESS && httpWebResponse != null)
					{
						using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
						{
							string json = streamReader.ReadToEnd();
							SMStageDataImpl sMStageDataImpl = new SMStageDataImpl(a_series, eventID, eventArgs.StageNumber);
							sMStageDataImpl.LoadFromJson(json, 10150);
							sMStageDataImpl.SegmentCode = eventArgs.UserSegment;
							eventArgs.RemoteTimestamp = HttpDateTimestamp(eventArgs.StageNumber, httpWebResponse);
							eventArgs.RemoteData = sMStageDataImpl;
						}
					}
				}
				catch (Exception)
				{
					eventArgs.RemoteTimestamp = 0;
					eventArgs.RemoteData = null;
				}
				finally
				{
					if (httpWebResponse != null)
					{
						httpWebResponse.Close();
					}
				}
				mFinishLoadStep = true;
			}, args);
			return true;
		}
		catch (Exception)
		{
			args.RemoteTimestamp = 0;
			args.RemoteData = null;
		}
		return false;
	}

	private static bool CreateAsync_04(string client_ver, Def.SERIES a_series, int stage, ref EventArgs args)
	{
		try
		{
			SMStageDataImpl sMStageDataImpl = args.RemoteData as SMStageDataImpl;
			if (sMStageDataImpl == null)
			{
				return false;
			}
			string cacheFilePath = GetCacheFilePath(a_series, stage, client_ver);
			string text = null;
			using (BIJBinaryWriter data = new BIJFileBinaryWriter(cacheFilePath))
			{
				sMStageDataImpl.Serialize(data, 10150);
			}
			File.SetLastWriteTime(cacheFilePath, DateTimeUtil.UnixTimeStampToDateTime(args.RemoteTimestamp, true));
			return true;
		}
		catch (Exception)
		{
		}
		return false;
	}

	public static bool CreateAsync_00_Editor(Def.SERIES a_series, string client_ver, int eventID, int stage, ref EventArgs args)
	{
		try
		{
			SMStageDataImpl sMStageDataImpl = new SMStageDataImpl(a_series, eventID, stage);
			string assetFileName = GetAssetFileName(a_series, stage, eventID);
			using (BIJBinaryReader data = new BIJResourceBinaryReader(assetFileName))
			{
				sMStageDataImpl.Deserialize(data, 10150);
			}
			sMStageDataImpl.SegmentCode = args.UserSegment;
			args.AssetData = sMStageDataImpl;
			return true;
		}
		catch (Exception ex)
		{
			args.StageData = null;
			args.Error = ex.ToString();
		}
		return false;
	}

	public void SetBronzeScore(int a_score)
	{
		SMStageDataImpl sMStageDataImpl = mStageData as SMStageDataImpl;
		if (sMStageDataImpl != null)
		{
			sMStageDataImpl.BronzeScore = a_score;
		}
	}

	public void SetSilverScore(int a_score)
	{
		SMStageDataImpl sMStageDataImpl = mStageData as SMStageDataImpl;
		if (sMStageDataImpl != null)
		{
			sMStageDataImpl.SilverScore = a_score;
		}
	}

	public void SetGoldScore(int a_score)
	{
		SMStageDataImpl sMStageDataImpl = mStageData as SMStageDataImpl;
		if (sMStageDataImpl != null)
		{
			sMStageDataImpl.GoldScore = a_score;
		}
	}
}
