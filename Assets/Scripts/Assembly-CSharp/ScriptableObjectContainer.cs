using UnityEngine;

public class ScriptableObjectContainer : MonoBehaviour
{
	[SerializeField]
	private ScriptableObject mData;

	public ScriptableObject Data
	{
		get
		{
			return mData;
		}
		set
		{
			mData = value;
		}
	}
}
