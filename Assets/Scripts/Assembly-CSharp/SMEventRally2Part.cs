using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEventRally2Part : SMMapPart
{
	protected const string EVENT_IMAGE_NAME = "event_map_{0:00}";

	private short mCourseID;

	public void SetCourseID(short a_couseID)
	{
		mCourseID = a_couseID;
	}

	public override void Init(Def.SERIES a_series, int a_index, Vector3 a_mapScale, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadblockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB, bool a_isAvailable, bool useBg = true)
	{
		mSeries = a_series;
		mMapScale = a_mapScale;
		mMapIndex = a_index;
		if (useBg)
		{
			BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
			mBGImage = Util.CreateGameObject("BG" + (a_index + 1), base.gameObject).AddComponent<SMEventBG>();
			mBGImage.Active(image, "null", Vector3.zero, mMapScale);
		}
		else
		{
			mBGImage = null;
		}
		mMapAccessoryCallback = a_accessoryCB;
		mMapRoadBlockCallback = a_roadblockCB;
		mMapStrayUserCallback = a_snsCB;
		mMapChangeCallback = a_mapChangeCB;
		mIsAvailable = a_isAvailable;
	}

	protected override void Update()
	{
		if (mMovableObjectSpawnList.Count <= 0)
		{
			return;
		}
		mMovableObjectSpawnTime += Time.deltaTime;
		if (mMovableObjectSpawnTime > 5f)
		{
			mMovableObjectSpawnTime = 0f;
			int num = Random.Range(0, mMovableObjectSpawnList.Count);
			MovableObjectSpawn movableObjectSpawn = mMovableObjectSpawnList[num];
			MapMovableObject mapMovableObject = Util.CreateGameObject("MovableObject", mGameState.MapPageRoot).AddComponent<MapMovableObject>();
			float a_deg = 0f;
			bool a_noAngle = true;
			if (movableObjectSpawn.AngleRange > 0f)
			{
				a_deg = Random.Range((0f - movableObjectSpawn.AngleRange) / 2f, movableObjectSpawn.AngleRange / 2f);
				a_noAngle = false;
			}
			string movableAnimeKey = GetMovableAnimeKey(mSeries, mMapIndex, movableObjectSpawn.AnimeKey);
			mapMovableObject.Init(0, movableObjectSpawn.AnimeKey, movableObjectSpawn.Atlas, movableObjectSpawn.Position.x, movableObjectSpawn.Position.y, Vector3.one, a_deg, num, mMapIndex, mSeries, a_noAngle, movableAnimeKey);
			mapMovableObject.SetDisappearedCallback(base.OnMovableObjectDisappearedCallback);
			mMapMovableObjectList.Add(mapMovableObject);
		}
	}

	public override IEnumerator Active(SMMapSsData a_mapSsData, int a_mapIndex, bool a_inPlayer)
	{
		List<SsMapEntity> accessoryList = a_mapSsData.GetAccessoryListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapEntityList = a_mapSsData.GetMapEntityListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapRBList = a_mapSsData.GetMapRBListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapERBList = a_mapSsData.GetMapERBListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapEmitterList = a_mapSsData.GetMapEmitterListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapMovableList = a_mapSsData.GetMapMoveObjectListInMapIndex(a_mapIndex);
		List<SsMapEntity> strayUserList = a_mapSsData.GetMapStrayUserListInMapIndex(a_mapIndex);
		List<SsMapEntity> silhouetteList = a_mapSsData.GetMapSilhouetteListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapSignList = a_mapSsData.GetMapSignListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapMaskList = a_mapSsData.GetMapMaskListInMapIndex(a_mapIndex);
		List<SsMapEntity> esilhouetteList = a_mapSsData.GetMapEpisodeListInMapIndex(a_mapIndex);
		IsActiveLoading = true;
		mStrayUserEntityList = strayUserList;
		string map_key = GetMapImageKey();
		NGUITools.SetActive(base.gameObject, true);
		ResImage mapRes = ResourceManager.LoadImageAsync(map_key);
		ResSsAnimation endAnime = ResourceManager.LoadSsAnimationAsync("MAP_END", true);
		for (int i11 = 0; i11 < mapEmitterList.Count; i11++)
		{
			SsMapEntity entity = mapEmitterList[i11];
			EnvParticle accesory = CreateMapEmitter(entity.MapOffsetCounter, entity.AnimeKey, entity.Position, entity.Scale);
			mMapEmitterList.Add(accesory);
		}
		while (mapRes.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		BIJImage atlasMap = mapRes.Image;
		if (mBGImage != null)
		{
			mBGImage.Active(atlasMap, string.Format("event_map_{0:00}", mMapIndex + 1), new Vector3(0f, 0f, Def.MAP_BASE_Z), mMapScale);
		}
		if (accessoryList.Count > 0)
		{
			string accessoryAnimeKey = GetMapAccessoryAnimeKey(mSeries, mMapIndex);
			ResSsAnimation accessoryAnime = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey, true);
			ResSsAnimation accessoryAnimeTap = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey + "_TAP", true);
			ResSsAnimation accessoryAnimeTapLock = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey + "_TAPLOCK", true);
			ResSsAnimation accessoryAnimeLock = ResourceManager.LoadSsAnimationAsync(accessoryAnimeKey + "_LOCK", true);
			while (accessoryAnime.LoadState != ResourceInstance.LOADSTATE.DONE || accessoryAnimeTap.LoadState != ResourceInstance.LOADSTATE.DONE || accessoryAnimeTapLock.LoadState != ResourceInstance.LOADSTATE.DONE || accessoryAnimeLock.LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
			for (int i10 = 0; i10 < accessoryList.Count; i10++)
			{
				SsMapEntity entity9 = accessoryList[i10];
				if (entity9.EnableDisplay)
				{
					EventAccessory accessory3 = CreateMapAccessory(data: mGame.GetAccessoryData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, mCourseID, entity9.AnimeKey), counter: entity9.MapOffsetCounter, animeName: string.Empty, atlas: atlasMap, pos: entity9.Position, scale: entity9.Scale);
					if (accessory3 != null)
					{
						mMapAccessoryList.Add(accessory3);
					}
				}
			}
		}
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		for (int i9 = 0; i9 < mapEntityList.Count; i9++)
		{
			string animeKey = mapEntityList[i9].AnimeKey;
			ResSsAnimation entityAnime = ResourceManager.LoadSsAnimationAsync(animeKey, true);
			asyncList.Add(entityAnime);
		}
		for (int i8 = 0; i8 < asyncList.Count; i8++)
		{
			while (asyncList[i8].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		for (int i7 = 0; i7 < mapEntityList.Count; i7++)
		{
			SsMapEntity entity2 = mapEntityList[i7];
			MapEntity accesory2 = CreateMapEntity(entity2.MapOffsetCounter, entity2.AnimeKey, atlasMap, entity2.Position, entity2.Scale);
			mMapEntityList.Add(accesory2);
		}
		BIJImage atlasCommon = mapRes.Image;
		bool isMaskLock = true;
		if (mapRBList.Count > 0)
		{
			asyncList.Clear();
			string animeKey = GetEventRBAnimeKey(mSeries, mMapIndex);
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_UNLOCK", true));
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_UNLOCKED", true));
			for (int i6 = 0; i6 < asyncList.Count; i6++)
			{
				while (asyncList[i6].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
			}
			SMMapPageData mapData2 = mSMMapPageData;
			SMEventPageData eventPageData2 = mapData2 as SMEventPageData;
			SMEventMapSetting eventMapSetting2 = eventPageData2.GetMap(mCourseID) as SMEventMapSetting;
			int nextRoadBlockLevel2 = mGame.mPlayer.NextRoadBlockLevel;
			for (int i5 = 0; i5 < mapRBList.Count; i5++)
			{
				SsMapEntity entity8 = mapRBList[i5];
				float order = float.Parse(entity8.AnimeKey);
				bool isUnlocked = true;
				int roadLevel = Def.GetStageNoByRouteOrder(order);
				int mainStage;
				int subStage;
				Def.SplitStageNo(roadLevel, out mainStage, out subStage);
				if (nextRoadBlockLevel2 <= roadLevel)
				{
					isUnlocked = false;
				}
				SMEventRoadBlockSetting setting = null;
				List<SMRoadBlockSetting> roadblocks = eventPageData2.GetRoadBlocks(mCourseID);
				foreach (SMRoadBlockSetting s2 in roadblocks)
				{
					if (s2.mStageNo == mainStage)
					{
						setting = s2 as SMEventRoadBlockSetting;
						break;
					}
				}
				if (setting != null)
				{
					MapRoadBlock accessory2 = CreateRoadBlock(entity8.MapOffsetCounter, entity8.AnimeKey, setting, atlasCommon, entity8.Position, entity8.Scale, isUnlocked, entity8.Angle);
					mMapRBList.Add(accessory2);
				}
			}
		}
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out eventData);
		if (false)
		{
			mGame.mPlayer.Data.SetMapData(eventData.EventID, eventData);
		}
		if (mapMaskList.Count > 0)
		{
			asyncList.Clear();
			string animeKey = GetMaskAnimeKey(mSeries, mMapIndex);
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
			for (int i4 = 0; i4 < asyncList.Count; i4++)
			{
				while (asyncList[i4].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
			}
			SMMapPageData mapData = mSMMapPageData;
			SMEventPageData eventPageData = mapData as SMEventPageData;
			SMEventMapSetting eventMapSetting = eventPageData.GetMap(mCourseID) as SMEventMapSetting;
			int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
			List<short> rbNos = eventMapSetting.CharaGetRoadblock;
			for (short i3 = 0; i3 < rbNos.Count; i3++)
			{
				short no = rbNos[i3];
				SMRoadBlockSetting s = eventPageData.GetRoadBlockByRBNo(no, mCourseID);
				if (s != null)
				{
					int mapIndex = a_mapSsData.GetMapIndex(s.RoadBlockLevel);
					if (mapIndex == mMapIndex && nextRoadBlockLevel > s.RoadBlockLevel)
					{
						isMaskLock = false;
					}
				}
			}
			for (int i2 = 0; i2 < mapMaskList.Count; i2++)
			{
				SsMapEntity entity7 = mapMaskList[i2];
				MapMask accessory = CreateMask(entity7.MapOffsetCounter, entity7.AnimeKey, atlasMap, entity7.Position, entity7.Scale, isMaskLock);
				mMapMaskList.Add(accessory);
			}
		}
		asyncList.Clear();
		for (int n = 0; n < mapMovableList.Count; n++)
		{
			SsMapEntity entity3 = mapMovableList[n];
			string animeKey = GetMovableAnimeKey(mSeries, mMapIndex, entity3.AnimeKey);
			ResSsAnimation entityAnime2 = ResourceManager.LoadSsAnimationAsync(animeKey, true);
			asyncList.Add(entityAnime2);
		}
		for (int m = 0; m < asyncList.Count; m++)
		{
			while (asyncList[m].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		for (int l = 0; l < mapMovableList.Count; l++)
		{
			SsMapEntity entity4 = mapMovableList[l];
			MovableObjectSpawn spawn = CreateMovableObjectSpawn(entity4.MapOffsetCounter, entity4.AnimeKey, atlasMap, entity4.Position, entity4.Angle);
			mMovableObjectSpawnList.Add(spawn);
			yield return 0;
		}
		MakeStrayUser(a_inPlayer);
		yield return 0;
		asyncList.Clear();
		if (esilhouetteList.Count > 0)
		{
			for (int k = 0; k < esilhouetteList.Count; k++)
			{
				SsMapEntity entity5 = esilhouetteList[k];
				if (entity5.EnableDisplay)
				{
					string animName = GetEpisodeSilhouetteAnimeKey(mSeries, entity5.MapAtlasIndex);
					ResSsAnimation entityAnime3 = ResourceManager.LoadSsAnimationAsync(animName, true);
					asyncList.Add(entityAnime3);
				}
			}
			for (int j = 0; j < asyncList.Count; j++)
			{
				while (asyncList[j].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					yield return 0;
				}
			}
			for (int i = 0; i < esilhouetteList.Count; i++)
			{
				SsMapEntity entity6 = esilhouetteList[i];
				if (entity6.EnableDisplay)
				{
					string animName2 = GetEpisodeSilhouetteAnimeKey(mSeries, entity6.MapAtlasIndex);
					Vector3 pos = entity6.Position;
					pos.z = Def.MAP_SILHOUETTE_Z;
					MapEntity accesory3 = CreateEpisodeSilhouette(entity6.MapOffsetCounter, animName2, atlasMap, pos, entity6.Scale, entity6.AnimeKey);
					mMapEntityList.Add(accesory3);
				}
			}
		}
		IsActiveLoading = false;
		yield return 0;
	}

	public override void Deactive()
	{
		for (int i = 0; i < mMapMaskList.Count; i++)
		{
			MapMask mono = mMapMaskList[i];
			GameMain.SafeDestroy(ref mono);
		}
		mMapMaskList.Clear();
		base.Deactive();
	}

	public new EventSign CreateSign(int counter, string animeName, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		EventSign eventSign = Util.CreateGameObject("Sign" + mMapIndex, base.gameObject).AddComponent<EventSign>();
		eventSign.SetCourseID(mCourseID);
		eventSign.Init(counter, animeName, atlas, pos.x, pos.y, scale, mMapIndex, GetSignAnimeKey(mSeries, mMapIndex));
		return eventSign;
	}

	public override MapRoadBlock CreateRoadBlock(int counter, string animeName, SMRoadBlockSetting a_data, BIJImage atlas, Vector3 pos, Vector3 scale, bool a_isUnlocked, float a_angle)
	{
		EventStarRoadBlock eventStarRoadBlock = Util.CreateGameObject("RB" + counter, base.gameObject).AddComponent<EventStarRoadBlock>();
		eventStarRoadBlock.SetCourseID(mCourseID);
		eventStarRoadBlock.Init(counter, animeName, a_data, atlas, pos.x, pos.y, scale, a_isUnlocked, a_angle, GetEventRBAnimeKey(mSeries, mMapIndex));
		eventStarRoadBlock.SetPushedCallback(mMapRoadBlockCallback);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			eventStarRoadBlock.SetDebugInfo(a_data);
		}
		return eventStarRoadBlock;
	}

	public EventRoadBlock GetEventRoadBlock(int a_stage)
	{
		for (int i = 0; i < mEventRBList.Count; i++)
		{
			int stageNo = Def.GetStageNo(mEventRBList[i].mData.mStageNo, mEventRBList[i].mData.mSubNo);
			if (stageNo == a_stage)
			{
				return mEventRBList[i];
			}
		}
		return null;
	}

	public override bool UnlockRoadBlock(int a_roadStage)
	{
		return base.UnlockRoadBlock(a_roadStage);
	}

	public EventRoadBlock CreateEventRoadBlock(int counter, string animeName, SMRoadBlockSetting a_data, BIJImage atlas, Vector3 pos, Vector3 scale, bool a_isUnlocked, float a_angle, string a_animeKey)
	{
		EventRoadBlock eventRoadBlock = Util.CreateGameObject("RB" + a_data.RoadBlockID, base.gameObject).AddComponent<EventRoadBlock>();
		eventRoadBlock.Init(counter, animeName, a_data, atlas, pos.x, pos.y, scale, a_isUnlocked, a_angle, a_animeKey);
		eventRoadBlock.SetPushedCallback(null);
		return eventRoadBlock;
	}

	public new EventAccessory CreateMapAccessory(int counter, string animeName, AccessoryData data, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		if (data == null)
		{
			return null;
		}
		bool a_alreadyHas = false;
		if (mGame.mPlayer.IsAccessoryUnlock(data.Index))
		{
			a_alreadyHas = true;
		}
		EventAccessory eventAccessory = Util.CreateGameObject("Accessory" + data.Index, base.gameObject).AddComponent<EventAccessory>();
		eventAccessory.SetCourseID(mCourseID);
		string mapAccessoryAnimeKey = GetMapAccessoryAnimeKey(mSeries, mMapIndex);
		eventAccessory.Init(counter, animeName, data, atlas, pos.x, pos.y, scale, a_alreadyHas, mMapIndex, mSeries, mapAccessoryAnimeKey);
		eventAccessory.SetPushedCallback(mMapAccessoryCallback);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			eventAccessory.SetDebugInfo(data);
		}
		return eventAccessory;
	}

	protected int GetEventID()
	{
		SMEventPageData sMEventPageData = mSMMapPageData as SMEventPageData;
		return sMEventPageData.EventID;
	}

	public override string GetMapImageKey()
	{
		string text = Def.SeriesPrefix[mSeries];
		SMEventPageData sMEventPageData = mSMMapPageData as SMEventPageData;
		return sMEventPageData.EventSetting.MapAtlasKey;
	}

	public override string GetMapAccessoryAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_ACCESSORY_{1}", seriesString, 1);
	}

	public override string GetButtonAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_STAGEBUTTON", seriesString, a_mapIndex + 1);
	}

	public override string GetLineAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_LINE", seriesString, a_mapIndex + 1);
	}

	public string GetEventRBAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_ROADBLOCK", seriesString, a_mapIndex + 1);
	}

	public string GetEventRBAnimeKey2(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_ROADBLOCK2", seriesString, a_mapIndex + 1);
	}

	public override string GetSignAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		SMEventPageData sMEventPageData = mSMMapPageData as SMEventPageData;
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		int key = mCourseID;
		SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[key];
		return sMEventCourseSetting.SignAnimeKey;
	}

	public override string GetMaskAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_{0}_MASK_{1}", seriesString, a_mapIndex + 1);
	}

	public override string GetMovableAnimeKey(Def.SERIES a_series, int a_mapIndex, string a_name)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("MAP_MOVE_{0}_{1}_", seriesString, a_mapIndex + 1) + a_name;
	}

	public override string GetEpisodeSilhouetteAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string seriesString = Def.GetSeriesString(a_series, GetEventID());
		return string.Format("SILHOUETTE_{0}", seriesString);
	}

	public override void OpenNewArea()
	{
		OpenNewAreaHide();
		if (mMapMaskList.Count > 0)
		{
			Util.CreateOneShotAnime("NewArea", GetMaskAnimeKey(mSeries, mMapIndex) + "_OPEN", base.gameObject, new Vector3(0f, 0f, Def.MAP_LENSFLARE_Z), Vector3.one, 0f, 0f);
			mMapMaskList[0].UnlockAnime();
		}
	}

	public override void OpenNewAreaHide()
	{
		mIsAvailable = true;
	}
}
