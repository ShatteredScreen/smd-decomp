using UnityEngine;

public class TweenUtil
{
	public static void MoveTo(GameObject _go, Vector3 _topos, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time)
	{
		iTween.Stop(_go);
		iTween.MoveTo(_go, iTween.Hash("islocal", true, "position", _topos, "easeType", _easetype, "loopType", _looptype, "time", _time));
	}

	public static void MoveTo(GameObject _go, Vector3 _topos, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time, string callback_OnComplete)
	{
		iTween.Stop(_go);
		iTween.MoveTo(_go, iTween.Hash("islocal", true, "position", _topos, "easeType", _easetype, "loopType", _looptype, "time", _time, "oncomplete", callback_OnComplete));
	}

	public static void MoveTo(GameObject _go, Vector3 _topos, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time, float _delay, string callback_OnComplete)
	{
		iTween.Stop(_go);
		iTween.MoveTo(_go, iTween.Hash("islocal", true, "position", _topos, "easeType", _easetype, "loopType", _looptype, "time", _time, "delay", _delay, "oncomplete", callback_OnComplete));
	}

	public static void MoveBy(GameObject _go, Vector3 _addpos, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time)
	{
		iTween.Stop(_go);
		iTween.MoveTo(_go, iTween.Hash("islocal", true, "position", _go.transform.localPosition + _addpos, "easeType", _easetype, "loopType", _looptype, "time", _time));
	}

	public static void MoveBy(GameObject _go, Vector3 _addpos, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time, float _delay)
	{
		iTween.Stop(_go);
		iTween.MoveTo(_go, iTween.Hash("islocal", true, "position", _go.transform.localPosition + _addpos, "easeType", _easetype, "loopType", _looptype, "time", _time, "delay", _delay));
	}

	public static void MoveBy(GameObject _go, Vector3 _addpos, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time, string callback_OnComplete)
	{
		iTween.Stop(_go);
		iTween.MoveTo(_go, iTween.Hash("islocal", true, "position", _go.transform.localPosition + _addpos, "easeType", _easetype, "loopType", _looptype, "time", _time, "oncomplete", callback_OnComplete));
	}

	public static void MoveBy(GameObject _go, Vector3 _addpos, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time, float _delay, string callback_OnComplete)
	{
		iTween.Stop(_go);
		iTween.MoveTo(_go, iTween.Hash("islocal", true, "position", _go.transform.localPosition + _addpos, "easeType", _easetype, "loopType", _looptype, "time", _time, "delay", _delay, "oncomplete", callback_OnComplete));
	}

	public static void ScaleTo(GameObject _go, Vector3 _toscl, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time)
	{
		iTween.Stop(_go);
		iTween.ScaleTo(_go, iTween.Hash("islocal", true, "scale", _toscl, "easeType", _easetype, "loopType", _looptype, "time", _time));
	}

	public static void ScaleTo(GameObject _go, Vector3 _toscl, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time, float _delay)
	{
		iTween.Stop(_go);
		iTween.ScaleTo(_go, iTween.Hash("islocal", true, "scale", _toscl, "easeType", _easetype, "loopType", _looptype, "time", _time, "delay", _delay));
	}

	public static void ScaleTo(GameObject _go, Vector3 _toscl, iTween.EaseType _easetype, iTween.LoopType _looptype, float _time, string callback_OnComplete)
	{
		iTween.Stop(_go);
		iTween.ScaleTo(_go, iTween.Hash("islocal", true, "scale", _toscl, "easeType", _easetype, "loopType", _looptype, "time", _time, "oncomplete", callback_OnComplete));
	}

	public static void Stop(GameObject _go)
	{
		iTween.Stop(_go);
	}
}
