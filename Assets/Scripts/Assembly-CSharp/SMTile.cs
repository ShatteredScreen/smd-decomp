using System;

public class SMTile : ISMTile, ICloneable
{
	protected Def.TILE_KIND mKind;

	protected Def.TILE_FORM mForm;

	public Def.TILE_KIND Kind
	{
		get
		{
			return mKind;
		}
	}

	public Def.TILE_FORM Form
	{
		get
		{
			return mForm;
		}
	}

	public SMTile()
	{
		mKind = Def.TILE_KIND.NONE;
		mForm = Def.TILE_FORM.NORMAL;
	}

	public SMTile(Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		mKind = kind;
		mForm = form;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public object Clone()
	{
		return MemberwiseClone() as SMTile;
	}

	public override bool Equals(object obj)
	{
		SMTile sMTile = obj as SMTile;
		if (sMTile != null)
		{
			return sMTile.Kind == Kind && sMTile.Form == Form;
		}
		return false;
	}

	public override string ToString()
	{
		return string.Format("Kind={0} Form={1}", Kind, Form);
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}

	internal void Serialize(ref BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt((int)mKind);
		data.WriteInt((int)mForm);
	}

	internal void Deserialize(ref BIJBinaryReader data, int savedVersion)
	{
		mKind = (Def.TILE_KIND)data.ReadInt();
		mForm = (Def.TILE_FORM)data.ReadInt();
	}

	public bool IsKind(Def.TILE_KIND a_kind)
	{
		return a_kind == Kind;
	}
}
