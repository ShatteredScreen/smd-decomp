using System.Collections.Generic;
using System.Text;

public class CompanionData
{
	public enum MOTION
	{
		SPECIAL = 0,
		STAY0 = 1,
		STAY1 = 2,
		WIN = 3,
		LOSE = 4,
		HIT0 = 5,
		HIT1 = 6,
		HIT2 = 7,
		HURRY = 8,
		JUMP = 9,
		LANDING = 10,
		SKILL0 = 11,
		SKILL1 = 12,
		SKILL2 = 13,
		MAXNUM = 14
	}

	public enum PERSONAL_ID
	{
		USAGI = 100,
		AMI = 200,
		REI = 300,
		MAKOTO = 400,
		MINAKO = 500,
		P_SERENITY = 1000,
		MAMORU = 1100,
		RUNA = 1200,
		ARTEMIS = 1300,
		CHIBIUSA = 1400,
		SETSUNA = 1500,
		MOTOKI = 1600,
		MICHIRU = 1700,
		HARUKA = 1800,
		RUNA_HUMAN = 1900,
		HOTARU = 2000,
		NARU = 2100,
		JADEITE = 2200,
		NEPHRITE = 2300,
		ZOISITE = 2400,
		KUNZITE = 2500,
		SEIIYA = 2600,
		TAIKI = 2700,
		YATEN = 2800,
		HELIOS = 2900,
		PAIR = 20000,
		MAMORU_USAGI = 20111
	}

	public static int SKILL00_LEVEL = 2;

	public static int SKILL01_LEVEL = 5;

	public static int HIT00_LEVEL = 1;

	public static int HIT01_LEVEL = 1;

	public static int HIT02_LEVEL = 4;

	public static int HURRY00_LEVEL = 1;

	public static int WAIT00_LEVEL = 1;

	public static int WAIT01_LEVEL = 3;

	public static int WIN00_LEVEL = 1;

	public static int LOSE00_LEVEL = 1;

	public short index;

	public string name;

	public string iconName;

	public short maxLevel;

	public bool transform;

	private string[] modelKey;

	private float[] scale;

	private float[] shadowOfsY;

	private float[] modelPosition;

	private string[] specialPose;

	private string[] idle0;

	private string[] idle1;

	private string[] win;

	private string[] lose;

	private string[] hit0;

	private string[] hit1;

	private string[] hit2;

	private string[] hurry;

	private string[] jump;

	private string[] landing;

	private string[] skill0;

	private float[] skill0_jumpDownTime;

	private float[] skill0_actionTime;

	private float[] skill0_jumpUpTime;

	private string[] skill1;

	private float[] skill1_jumpDownTime;

	private float[] skill1_actionTime;

	private float[] skill1_jumpUpTime;

	private string[] skill2;

	private float[] skill2_jumpDownTime;

	private float[] skill2_actionTime;

	private float[] skill2_jumpUpTime;

	private byte[][] zpos = new byte[14][];

	public short idle0ratio;

	public short idle1ratio;

	public short hit0ratio;

	public short hit1ratio;

	public short hit2ratio;

	public float hit0ViewSec;

	public float hit1ViewSec;

	public float hit2ViewSec;

	public float skill0Ratio;

	public float skill1Ratio;

	public byte motionType;

	public short personalId;

	public short skill0Id;

	public short skill1Id;

	public short skill2Id;

	public byte[][] skill0FadeTypes;

	public byte[][] skill1FadeTypes;

	public byte[][] skill2FadeTypes;

	public short backCharacterIndex;

	public short baseCharacterIndex;

	public string puzzleBgm;

	public string puzzleBgmChance;

	public byte sortCategory;

	public short sortOder;

	public int ModelCount
	{
		get
		{
			return modelKey.Length;
		}
	}

	public CompanionData()
	{
		index = 0;
		name = string.Empty;
		iconName = "0";
		maxLevel = 1;
		transform = false;
		modelKey = null;
		scale = null;
		shadowOfsY = null;
		modelPosition = null;
		specialPose = null;
		idle0 = null;
		idle1 = null;
		win = null;
		lose = null;
		hit0 = null;
		hit1 = null;
		hit2 = null;
		hurry = null;
		skill0 = null;
		skill1 = null;
		skill2 = null;
		skill0_jumpDownTime = null;
		skill0_actionTime = null;
		skill0_jumpUpTime = null;
		skill1_jumpDownTime = null;
		skill1_actionTime = null;
		skill1_jumpUpTime = null;
		skill2_jumpDownTime = null;
		skill2_actionTime = null;
		skill2_jumpUpTime = null;
		idle0ratio = 0;
		idle1ratio = 0;
		hit0ratio = 0;
		hit1ratio = 0;
		hit2ratio = 0;
		hit0ViewSec = 0f;
		hit1ViewSec = 0f;
		hit2ViewSec = 0f;
		skill0Ratio = 1f;
		skill1Ratio = 1f;
		motionType = 0;
		personalId = 0;
		skill0Id = 0;
		skill1Id = 0;
		skill2Id = 0;
		skill0FadeTypes = null;
		skill1FadeTypes = null;
		skill2FadeTypes = null;
		backCharacterIndex = 0;
		baseCharacterIndex = 0;
		puzzleBgm = string.Empty;
		puzzleBgmChance = string.Empty;
		sortCategory = 0;
		sortOder = 0;
	}

	public void deserialize(BIJBinaryReader stream)
	{
		index = stream.ReadShort();
		name = stream.ReadUTF();
		iconName = stream.ReadUTF();
		maxLevel = stream.ReadShort();
		transform = stream.ReadBool();
		string empty = string.Empty;
		modelKey = ReadStringAndSplit(stream, ';');
		string[] array = ReadStringAndSplit(stream, ';');
		scale = new float[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			scale[i] = float.Parse(array[i]);
		}
		array = ReadStringAndSplit(stream, ';');
		shadowOfsY = new float[array.Length];
		for (int j = 0; j < array.Length; j++)
		{
			shadowOfsY[j] = float.Parse(array[j]);
		}
		array = ReadStringAndSplit(stream, ';');
		modelPosition = new float[array.Length];
		for (int k = 0; k < array.Length; k++)
		{
			modelPosition[k] = float.Parse(array[k]);
		}
		specialPose = ReadStringAndSplit(stream, ';');
		idle0 = ReadStringAndSplit(stream, ';');
		idle1 = ReadStringAndSplit(stream, ';');
		win = ReadStringAndSplit(stream, ';');
		lose = ReadStringAndSplit(stream, ';');
		hit0 = ReadStringAndSplit(stream, ';');
		hit1 = ReadStringAndSplit(stream, ';');
		hit2 = ReadStringAndSplit(stream, ';');
		hurry = ReadStringAndSplit(stream, ';');
		jump = ReadStringAndSplit(stream, ';');
		landing = ReadStringAndSplit(stream, ';');
		skill0 = ReadStringAndSplit(stream, ';');
		empty = stream.ReadUTF();
		array = empty.Split(';');
		skill0_jumpDownTime = new float[array.Length];
		skill0_actionTime = new float[array.Length];
		skill0_jumpUpTime = new float[array.Length];
		for (int l = 0; l < array.Length; l++)
		{
			string[] array2 = array[l].Split(',');
			skill0_jumpDownTime[l] = float.Parse(array2[0]);
			skill0_actionTime[l] = float.Parse(array2[1]);
			skill0_jumpUpTime[l] = float.Parse(array2[2]);
		}
		skill1 = ReadStringAndSplit(stream, ';');
		empty = stream.ReadUTF();
		array = empty.Split(';');
		skill1_jumpDownTime = new float[array.Length];
		skill1_actionTime = new float[array.Length];
		skill1_jumpUpTime = new float[array.Length];
		for (int m = 0; m < array.Length; m++)
		{
			string[] array2 = array[m].Split(',');
			skill1_jumpDownTime[m] = float.Parse(array2[0]);
			skill1_actionTime[m] = float.Parse(array2[1]);
			skill1_jumpUpTime[m] = float.Parse(array2[2]);
		}
		skill2 = ReadStringAndSplit(stream, ';');
		empty = stream.ReadUTF();
		array = empty.Split(';');
		skill2_jumpDownTime = new float[array.Length];
		skill2_actionTime = new float[array.Length];
		skill2_jumpUpTime = new float[array.Length];
		for (int n = 0; n < array.Length; n++)
		{
			string[] array2 = array[n].Split(',');
			skill2_jumpDownTime[n] = float.Parse(array2[0]);
			skill2_actionTime[n] = float.Parse(array2[1]);
			skill2_jumpUpTime[n] = float.Parse(array2[2]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[0] = new byte[array.Length];
		for (int num = 0; num < array.Length; num++)
		{
			zpos[0][num] = byte.Parse(array[num]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[1] = new byte[array.Length];
		for (int num2 = 0; num2 < array.Length; num2++)
		{
			zpos[1][num2] = byte.Parse(array[num2]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[2] = new byte[array.Length];
		for (int num3 = 0; num3 < array.Length; num3++)
		{
			zpos[2][num3] = byte.Parse(array[num3]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[3] = new byte[array.Length];
		for (int num4 = 0; num4 < array.Length; num4++)
		{
			zpos[3][num4] = byte.Parse(array[num4]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[4] = new byte[array.Length];
		for (int num5 = 0; num5 < array.Length; num5++)
		{
			zpos[4][num5] = byte.Parse(array[num5]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[5] = new byte[array.Length];
		for (int num6 = 0; num6 < array.Length; num6++)
		{
			zpos[5][num6] = byte.Parse(array[num6]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[6] = new byte[array.Length];
		for (int num7 = 0; num7 < array.Length; num7++)
		{
			zpos[6][num7] = byte.Parse(array[num7]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[7] = new byte[array.Length];
		for (int num8 = 0; num8 < array.Length; num8++)
		{
			zpos[7][num8] = byte.Parse(array[num8]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[8] = new byte[array.Length];
		for (int num9 = 0; num9 < array.Length; num9++)
		{
			zpos[8][num9] = byte.Parse(array[num9]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[9] = new byte[array.Length];
		for (int num10 = 0; num10 < array.Length; num10++)
		{
			zpos[9][num10] = byte.Parse(array[num10]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[10] = new byte[array.Length];
		for (int num11 = 0; num11 < array.Length; num11++)
		{
			zpos[10][num11] = byte.Parse(array[num11]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[11] = new byte[array.Length];
		for (int num12 = 0; num12 < array.Length; num12++)
		{
			zpos[11][num12] = byte.Parse(array[num12]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[12] = new byte[array.Length];
		for (int num13 = 0; num13 < array.Length; num13++)
		{
			zpos[12][num13] = byte.Parse(array[num13]);
		}
		array = ReadStringAndSplit(stream, ';');
		zpos[13] = new byte[array.Length];
		for (int num14 = 0; num14 < array.Length; num14++)
		{
			zpos[13][num14] = byte.Parse(array[num14]);
		}
		idle0ratio = stream.ReadShort();
		idle1ratio = stream.ReadShort();
		hit0ratio = stream.ReadShort();
		hit1ratio = stream.ReadShort();
		hit2ratio = stream.ReadShort();
		hit0ViewSec = stream.ReadFloat();
		hit1ViewSec = stream.ReadFloat();
		hit2ViewSec = stream.ReadFloat();
		skill0Ratio = stream.ReadFloat();
		skill1Ratio = stream.ReadFloat();
		motionType = stream.ReadByte();
		personalId = stream.ReadShort();
		skill0Id = stream.ReadShort();
		skill1Id = stream.ReadShort();
		skill2Id = stream.ReadShort();
		array = ReadStringAndSplit(stream, ';');
		skill0FadeTypes = new byte[array.Length][];
		for (int num15 = 0; num15 < array.Length; num15++)
		{
			string[] array2 = array[num15].Split(',');
			skill0FadeTypes[num15] = new byte[array2.Length];
			for (int num16 = 0; num16 < array2.Length; num16++)
			{
				skill0FadeTypes[num15][num16] = byte.Parse(array2[num16]);
			}
		}
		array = ReadStringAndSplit(stream, ';');
		skill1FadeTypes = new byte[array.Length][];
		for (int num17 = 0; num17 < array.Length; num17++)
		{
			string[] array2 = array[num17].Split(',');
			skill1FadeTypes[num17] = new byte[array2.Length];
			for (int num18 = 0; num18 < array2.Length; num18++)
			{
				skill1FadeTypes[num17][num18] = byte.Parse(array2[num18]);
			}
		}
		array = ReadStringAndSplit(stream, ';');
		skill2FadeTypes = new byte[array.Length][];
		for (int num19 = 0; num19 < array.Length; num19++)
		{
			string[] array2 = array[num19].Split(',');
			skill2FadeTypes[num19] = new byte[array2.Length];
			for (int num20 = 0; num20 < array2.Length; num20++)
			{
				skill2FadeTypes[num19][num20] = byte.Parse(array2[num20]);
			}
		}
		backCharacterIndex = stream.ReadShort();
		baseCharacterIndex = stream.ReadShort();
		puzzleBgm = stream.ReadUTF();
		puzzleBgmChance = stream.ReadUTF();
		sortCategory = stream.ReadByte();
		sortOder = stream.ReadShort();
	}

	private string[] ReadStringAndSplit(BIJBinaryReader stream, char separator)
	{
		string text = stream.ReadUTF();
		return text.Split(separator);
	}

	public bool CheckParsonalID(PERSONAL_ID id)
	{
		bool flag = personalId == (short)id;
		if (!flag && personalId >= 20000)
		{
			short num = (short)(personalId - 20000);
			if (num % 100 == (short)id / 100)
			{
				flag = true;
			}
			else if (num / 100 % 100 == (short)id / 100)
			{
				flag = true;
			}
		}
		return flag;
	}

	public bool IsHalloweenHaruka()
	{
		if (index == 45 || index == 46)
		{
			return true;
		}
		return false;
	}

	public bool IsHalloweenMichiru()
	{
		if (index == 47 || index == 48)
		{
			return true;
		}
		return false;
	}

	public bool IsHalloweenUsagi()
	{
		if (index == 107 || index == 108)
		{
			return true;
		}
		return false;
	}

	public bool IsHalloweenHotaru()
	{
		if (index == 109 || index == 110)
		{
			return true;
		}
		return false;
	}

	public bool IsHalloweenUsagi2()
	{
		if (index == 163 || index == 164)
		{
			return true;
		}
		return false;
	}

	public bool IsSailorChibiMoon()
	{
		if (index == 70 || index == 71)
		{
			return true;
		}
		return false;
	}

	public bool IsTall()
	{
		if (index == 68 || index == 69 || index == 74 || index == 75 || index == 123 || index == 124 || index == 129 || index == 130)
		{
			return true;
		}
		return false;
	}

	public bool HaveExpand()
	{
		if (baseCharacterIndex != -1)
		{
			return true;
		}
		return false;
	}

	public bool IsExpandBase()
	{
		if (baseCharacterIndex == index && backCharacterIndex == -1)
		{
			return true;
		}
		return false;
	}

	public bool IsExpanded()
	{
		if (baseCharacterIndex != -1 && backCharacterIndex != -1)
		{
			return true;
		}
		return false;
	}

	public string GetModelKey(int index)
	{
		if (index >= modelKey.Length)
		{
			return "NONE";
		}
		return modelKey[index];
	}

	public float GetScale(int index)
	{
		if (index >= scale.Length)
		{
			return 0f;
		}
		return scale[index];
	}

	public float GetShadowOfs(int index)
	{
		if (index >= shadowOfsY.Length)
		{
			return 0f;
		}
		return shadowOfsY[index];
	}

	public float GetPosition(int index)
	{
		if (index >= modelPosition.Length)
		{
			return 0f;
		}
		return modelPosition[index];
	}

	public string GetMotionPath(MOTION motion, int index)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("motions/");
		string text = "null";
		switch (motion)
		{
		case MOTION.SPECIAL:
			text = specialPose[index];
			break;
		case MOTION.STAY0:
			text = idle0[index];
			break;
		case MOTION.STAY1:
			text = idle1[index];
			break;
		case MOTION.WIN:
			text = win[index];
			break;
		case MOTION.LOSE:
			text = lose[index];
			break;
		case MOTION.HIT0:
			text = hit0[index];
			break;
		case MOTION.HIT1:
			text = hit1[index];
			break;
		case MOTION.HIT2:
			text = hit2[index];
			break;
		case MOTION.HURRY:
			text = hurry[index];
			break;
		case MOTION.JUMP:
			text = jump[index];
			break;
		case MOTION.LANDING:
			text = landing[index];
			break;
		case MOTION.SKILL0:
			text = skill0[index];
			break;
		case MOTION.SKILL1:
			text = skill1[index];
			break;
		case MOTION.SKILL2:
			text = skill2[index];
			break;
		}
		if (string.Compare(text, "null") == 0)
		{
			return "null";
		}
		stringBuilder.Append(text);
		stringBuilder.Append(".mtn");
		return stringBuilder.ToString();
	}

	public byte GetZPosition(MOTION motion, int index)
	{
		if (zpos[(int)motion].Length <= index)
		{
			return 0;
		}
		return zpos[(int)motion][index];
	}

	public string GetSkillName(int rank)
	{
		SkillEffectData skillEffectData = null;
		switch (rank)
		{
		case 1:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill0Id];
			break;
		case 2:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill1Id];
			break;
		case 3:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill2Id];
			break;
		}
		if (skillEffectData == null)
		{
			return string.Empty;
		}
		return Localization.Get(skillEffectData.name);
	}

	public string GetSkillDesc(int rank)
	{
		SkillEffectData skillEffectData = null;
		switch (rank)
		{
		case 1:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill0Id];
			break;
		case 2:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill1Id];
			break;
		case 3:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill2Id];
			break;
		}
		if (skillEffectData == null)
		{
			return string.Empty;
		}
		if (skillEffectData.descriptionParamArray == null)
		{
			return Localization.Get(skillEffectData.description);
		}
		return Util.MakeLText(skillEffectData.description, skillEffectData.descriptionParamArray);
	}

	public string GetSkillDescLineFeed(int rank)
	{
		SkillEffectData skillEffectData = null;
		switch (rank)
		{
		case 1:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill0Id];
			break;
		case 2:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill1Id];
			break;
		case 3:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill2Id];
			break;
		}
		if (skillEffectData == null)
		{
			return string.Empty;
		}
		if (skillEffectData.descriptionParamArray == null)
		{
			return Localization.Get(skillEffectData.descriptionLB);
		}
		return Util.MakeLText(skillEffectData.descriptionLB, skillEffectData.descriptionParamArray);
	}

	public string GetSkillSubDesc(int rank)
	{
		SkillEffectData skillEffectData = null;
		switch (rank)
		{
		case 1:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill0Id];
			break;
		case 2:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill1Id];
			break;
		case 3:
			skillEffectData = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill2Id];
			break;
		}
		if (skillEffectData == null)
		{
			return string.Empty;
		}
		return Localization.Get(skillEffectData.descriptionUp);
	}

	public List<ByteVector2> GetSkillArea(int rank)
	{
		List<ByteVector2> list = null;
		switch (rank)
		{
		case 1:
			list = Def.SKILL_FUNC_DATA[skill0Id].area;
			break;
		case 2:
			list = Def.SKILL_FUNC_DATA[skill1Id].area;
			break;
		case 3:
			list = Def.SKILL_FUNC_DATA[skill2Id].area;
			break;
		}
		if (list == null)
		{
			list = new List<ByteVector2>();
			list.Add(new ByteVector2(0, 0));
		}
		return list;
	}

	public SkillEffectData GetSkillEffectData(int rank)
	{
		SkillEffectData result = null;
		switch (rank)
		{
		case 1:
			result = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill0Id];
			break;
		case 2:
			result = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill1Id];
			break;
		case 3:
			result = SingletonMonoBehaviour<GameMain>.Instance.mSkillEffectData[skill2Id];
			break;
		}
		return result;
	}

	public Def.SkillFuncData GetSkillFuncData(int rank)
	{
		Def.SkillFuncData result = null;
		switch (rank)
		{
		case 1:
			result = Def.SKILL_FUNC_DATA[skill0Id];
			break;
		case 2:
			result = Def.SKILL_FUNC_DATA[skill1Id];
			break;
		case 3:
			result = Def.SKILL_FUNC_DATA[skill2Id];
			break;
		}
		return result;
	}

	public string GetSkillMotionName(int rank, int index)
	{
		string result = string.Empty;
		switch (rank)
		{
		case 1:
			result = skill0[index];
			break;
		case 2:
			result = skill1[index];
			break;
		case 3:
			result = skill2[index];
			break;
		}
		return result;
	}

	public float GetSkillActionTime(int rank, int index)
	{
		float num = 1f;
		switch (rank)
		{
		case 1:
			num = skill0_actionTime[index];
			break;
		case 2:
			num = skill1_actionTime[index];
			break;
		case 3:
			num = skill2_actionTime[index];
			break;
		}
		return num / (float)Def.TARGET_FRAMERATE;
	}

	public float GetSkillJumpUpTime(int rank, int index)
	{
		float num = 1f;
		switch (rank)
		{
		case 1:
			num = skill0_jumpUpTime[index];
			break;
		case 2:
			num = skill1_jumpUpTime[index];
			break;
		case 3:
			num = skill2_jumpUpTime[index];
			break;
		}
		return num / (float)Def.TARGET_FRAMERATE;
	}

	public float GetSkillJumpDownTime(int rank, int index)
	{
		float num = 1f;
		switch (rank)
		{
		case 1:
			num = skill0_jumpDownTime[index];
			break;
		case 2:
			num = skill1_jumpDownTime[index];
			break;
		case 3:
			num = skill2_jumpDownTime[index];
			break;
		}
		return num / (float)Def.TARGET_FRAMERATE;
	}

	public byte GetSkillFadeType(int type, int rank, int index)
	{
		byte result = 0;
		switch (rank)
		{
		case 1:
			if (index < skill0FadeTypes.Length)
			{
				result = skill0FadeTypes[index][type];
			}
			break;
		case 2:
			if (index < skill1FadeTypes.Length)
			{
				result = skill1FadeTypes[index][type];
			}
			break;
		case 3:
			if (index < skill2FadeTypes.Length)
			{
				result = skill2FadeTypes[index][type];
			}
			break;
		}
		return result;
	}

	public CompanionData Clone()
	{
		return (CompanionData)MemberwiseClone();
	}

	public void Dump()
	{
	}
}
