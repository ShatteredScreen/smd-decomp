using System;
using System.Collections;
using System.Collections.Specialized;
using UnityEngine;

[ExecuteInEditMode]
public class BIJPackImage : BIJImage
{
	private class BlockComparer : IComparer
	{
		private Rect[] mBlocks;

		public BlockComparer(Rect[] blocks)
		{
			mBlocks = blocks;
		}

		public int Compare(object x, object y)
		{
			Rect rect = mBlocks[(int)x];
			Rect rect2 = mBlocks[(int)y];
			float f = Mathf.Sqrt(Mathf.Pow(rect.width, 2f) + Mathf.Pow(rect.height, 2f)) - Mathf.Sqrt(Mathf.Pow(rect2.width, 2f) + Mathf.Pow(rect2.height, 2f));
			return -Mathf.FloorToInt(f);
		}
	}

	private object mTextureSync = new object();

	private Texture2D mTexture;

	private Texture2D mRearrangeTexture;

	private BIJPackOffset mOffset;

	private BIJSubPackSheet mSubPackSheet;

	private Vector2 mCountOfPack = new Vector2(1f, 1f);

	private Rect mPackRect = new Rect(0f, 0f, 0f, 0f);

	private Rect[] mBlocksPerPack;

	private StringCollection mPackNames;

	[SerializeField]
	public string ImageName;

	[SerializeField]
	public string OffsetName;

	[SerializeField]
	public string SubPackSheetName;

	public Texture MainTexture;

	public override Texture Texture
	{
		get
		{
			lock (mTextureSync)
			{
				return mTexture;
			}
		}
	}

	public override int Count
	{
		get
		{
			if (mOffset != null)
			{
				return mOffset.Count;
			}
			return 0;
		}
	}

	public virtual void Awake()
	{
	}

	public virtual void Start()
	{
	}

	public virtual void OnDestroy()
	{
		Uninit();
		if (mRearrangeTexture != null)
		{
			UnityEngine.Object.Destroy(mRearrangeTexture);
			mRearrangeTexture = null;
		}
	}

	public virtual void Init()
	{
		try
		{
			Uninit();
			if (!string.IsNullOrEmpty(OffsetName))
			{
				mOffset = new BIJPackOffset(OffsetName);
			}
			if (!string.IsNullOrEmpty(SubPackSheetName))
			{
				mSubPackSheet = new BIJSubPackSheet(SubPackSheetName);
			}
			lock (mTextureSync)
			{
				if (MainTexture != null)
				{
					mTexture = MainTexture as Texture2D;
				}
				else
				{
					mTexture = BIJGraphics2D.LoadTextureFromResource(ImageName, TextureFormat.ARGB4444, false);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	public virtual void Uninit()
	{
		if (mTexture != null && mTexture != MainTexture && mTexture != mRearrangeTexture)
		{
			if (Application.isEditor)
			{
				UnityEngine.Object.DestroyImmediate(mTexture);
			}
			else
			{
				UnityEngine.Object.Destroy(mTexture);
			}
		}
		mTexture = null;
		mOffset = null;
		mSubPackSheet = null;
		mCountOfPack.Set(1f, 1f);
		mPackRect.Set(0f, 0f, 0f, 0f);
		mBlocksPerPack = null;
		mPackNames = null;
	}

	public int Rearrange(string[] targets)
	{
		return Rearrange(targets, 2, 2, true, 2048, 2048);
	}

	public int Rearrange(string[] targets, int onePackWidth, int onePackHeight, bool canGrow)
	{
		return Rearrange(targets, onePackWidth, onePackHeight, canGrow, 2048, 2048);
	}

	public int Rearrange(string[] targets, int onePackWidth, int onePackHeight, bool canGrow, int maxTextureWidth, int maxTextureHeight)
	{
		if (targets == null)
		{
			return -1;
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		Init();
		float num = Time.realtimeSinceStartup - realtimeSinceStartup;
		if (mSubPackSheet == null || mSubPackSheet.CountPerPack < 1)
		{
			return 0;
		}
		Rect[] blocks = CalcPackMaxSizes(targets);
		realtimeSinceStartup = Time.realtimeSinceStartup;
		mBlocksPerPack = FitPack(blocks, onePackWidth, onePackHeight, canGrow, out mPackRect);
		num = Time.realtimeSinceStartup - realtimeSinceStartup;
		if (mBlocksPerPack == null)
		{
			return -1;
		}
		Rect rect = new Rect(0f, 0f, maxTextureWidth, maxTextureHeight);
		mCountOfPack = new Vector2(rect.width / mPackRect.width, rect.height / mPackRect.height);
		if (targets.Length >= (int)(mCountOfPack.x * mCountOfPack.y))
		{
			return -1;
		}
		return -1;
	}

	public BIJImageOffset[] GetTargetOffsets(string _targetname)
	{
		int num = mSubPackSheet.IndexOf(_targetname);
		if (num == -1)
		{
			return null;
		}
		num *= mSubPackSheet.CountPerPack;
		BIJImageOffset[] array = new BIJImageOffset[mSubPackSheet.CountPerPack];
		for (int i = 0; i < mSubPackSheet.CountPerPack; i++)
		{
			array[i] = mOffset[num + i];
		}
		return array;
	}

	private Rect[] CalcPackMaxSizes(string[] targets)
	{
		Rect[] array = new Rect[mSubPackSheet.CountPerPack];
		for (int i = 0; i < targets.Length; i++)
		{
			int num = mSubPackSheet.IndexOf(targets[i]);
			if (num != -1)
			{
				num *= mSubPackSheet.CountPerPack;
				for (int j = 0; j < mSubPackSheet.CountPerPack; j++)
				{
					BIJImageOffset bIJImageOffset = mOffset[num + j];
					float val = bIJImageOffset.width + bIJImageOffset.paddingLeft + bIJImageOffset.paddingRight;
					float val2 = bIJImageOffset.height + bIJImageOffset.paddingTop + bIJImageOffset.paddingBottom;
					array[j].Set(0f, 0f, Math.Max(array[j].width, val), Math.Max(array[j].height, val2));
				}
			}
		}
		return array;
	}

	private Rect[] FitPack(Rect[] blocks, int width, int height, bool canGrow, out Rect packRect)
	{
		packRect = new Rect(0f, 0f, 0f, 0f);
		if (blocks == null)
		{
			return null;
		}
		Rect[] array = new Rect[blocks.Length];
		int[] array2 = new int[blocks.Length];
		for (int i = 0; i < array2.Length; i++)
		{
			array2[i] = i;
		}
		Array.Sort(array2, new BlockComparer(blocks));
		int num = BIJGraphics2D.NextPowerOfTwo(width);
		int num2 = BIJGraphics2D.NextPowerOfTwo(height);
		BIJMaxRectsBinPack bIJMaxRectsBinPack = new BIJMaxRectsBinPack(num, num2, false);
		for (int j = 0; j < blocks.Length; j++)
		{
			int num3 = array2[j];
			Rect rect = blocks[num3];
			Rect rect2 = bIJMaxRectsBinPack.Insert((int)rect.width, (int)rect.height, BIJMaxRectsBinPack.FreeRectChoiceHeuristic.RectBestAreaFit);
			if (rect2.width == 0f || rect2.height == 0f)
			{
				if (canGrow)
				{
					if (num <= num2)
					{
						num *= 2;
					}
					else
					{
						num2 *= 2;
					}
					return FitPack(blocks, num, num2, canGrow, out packRect);
				}
				return null;
			}
			array[num3].Set(rect2.x, rect2.y, rect.width, rect.height);
		}
		packRect = new Rect(0f, 0f, num, num2);
		return array;
	}

	private BIJImageOffset OffsetOf(int index, string packName)
	{
		if (mPackNames != null)
		{
			int num;
			if ((num = mPackNames.IndexOf(packName)) != -1)
			{
				int index2 = mSubPackSheet[packName] * mSubPackSheet.CountPerPack + index;
				string text = mOffset[index2].name;
				short x = (short)((float)(num % (int)mCountOfPack.x) * mPackRect.width + mBlocksPerPack[index].x);
				short y = (short)((float)(num / (int)mCountOfPack.x) * mPackRect.height + mBlocksPerPack[index].y);
				short width = (short)mBlocksPerPack[index].width;
				short height = (short)mBlocksPerPack[index].height;
				return new BIJImageOffset(text, x, y, width, height, 0, 0, 0, 0);
			}
			return null;
		}
		int num2 = index;
		if (mOffset == null)
		{
			return null;
		}
		if (mSubPackSheet != null && !string.IsNullOrEmpty(packName))
		{
			num2 += mSubPackSheet[packName] * mSubPackSheet.CountPerPack;
		}
		return mOffset.OffsetOf(num2);
	}

	private BIJImageOffset OffsetOf(string name, string packName)
	{
		BIJImageOffset bIJImageOffset = null;
		if (mPackNames != null)
		{
			int num;
			if ((num = mPackNames.IndexOf(packName)) != -1)
			{
				int num2 = mSubPackSheet[packName] * mSubPackSheet.CountPerPack;
				int num3 = -1;
				for (int i = 0; i < mSubPackSheet.CountPerPack; i++)
				{
					bIJImageOffset = mOffset[num2 + i];
					if (name.CompareTo(bIJImageOffset.name) == 0)
					{
						num3 = i;
						break;
					}
				}
				if (num3 == -1)
				{
					return null;
				}
				short x = (short)((float)(num % (int)mCountOfPack.x) * mPackRect.width + mBlocksPerPack[num3].x);
				short y = (short)((float)(num / (int)mCountOfPack.x) * mPackRect.height + mBlocksPerPack[num3].y);
				short width = (short)mBlocksPerPack[num3].width;
				short height = (short)mBlocksPerPack[num3].height;
				return new BIJImageOffset(name, x, y, width, height, 0, 0, 0, 0);
			}
		}
		else
		{
			if (mOffset == null)
			{
				return null;
			}
			if (mSubPackSheet != null && !string.IsNullOrEmpty(packName))
			{
				int num4 = mSubPackSheet[packName];
				for (int j = 0; j < mSubPackSheet.CountPerPack; j++)
				{
					bIJImageOffset = mOffset[num4 + j];
					if (name.CompareTo(bIJImageOffset.name) == 0)
					{
						return bIJImageOffset;
					}
				}
			}
			else
			{
				for (int k = 0; k < mOffset.Count; k++)
				{
					bIJImageOffset = mOffset[k];
					if (name.CompareTo(bIJImageOffset.name) == 0)
					{
						return bIJImageOffset;
					}
				}
			}
		}
		return null;
	}

	public override bool Contains(int index, string name)
	{
		return OffsetOf(index, name) != null;
	}

	public override string GetName(int index, string name)
	{
		BIJImageOffset bIJImageOffset = OffsetOf(index, name);
		if (bIJImageOffset != null)
		{
			return bIJImageOffset.name;
		}
		return null;
	}

	public override Vector2 GetOffset(int index, string name)
	{
		BIJImageOffset bIJImageOffset = OffsetOf(index, name);
		if (bIJImageOffset != null)
		{
			float x = (float)bIJImageOffset.x / (float)Texture.width;
			float y = 1f - ((float)bIJImageOffset.y + (float)bIJImageOffset.height) / (float)Texture.height;
			return new Vector2(x, y);
		}
		return new Vector2(0f, 0f);
	}

	public override Vector2 GetSize(int index, string name)
	{
		BIJImageOffset bIJImageOffset = OffsetOf(index, name);
		if (bIJImageOffset != null)
		{
			float x = (float)bIJImageOffset.width / (float)Texture.width;
			float y = (float)bIJImageOffset.height / (float)Texture.height;
			return new Vector2(x, y);
		}
		return new Vector2(1f, 1f);
	}

	public override Vector2 GetPixelOffset(int index, string name)
	{
		BIJImageOffset bIJImageOffset = OffsetOf(index, name);
		if (bIJImageOffset != null)
		{
			return new Vector2(bIJImageOffset.x, bIJImageOffset.y);
		}
		return new Vector2(0f, 0f);
	}

	public override Vector2 GetPixelSize(int index, string name)
	{
		BIJImageOffset bIJImageOffset = OffsetOf(index, name);
		if (bIJImageOffset != null)
		{
			return new Vector2(bIJImageOffset.width, bIJImageOffset.height);
		}
		if (Texture != null)
		{
			return new Vector2(Texture.width, Texture.height);
		}
		return new Vector2(0f, 0f);
	}

	public override Vector4 GetPixelPadding(int index, string name)
	{
		BIJImageOffset bIJImageOffset = OffsetOf(index, name);
		if (bIJImageOffset != null)
		{
			return new Vector4(bIJImageOffset.paddingLeft, bIJImageOffset.paddingTop, bIJImageOffset.paddingRight, bIJImageOffset.paddingBottom);
		}
		return new Vector4(0f, 0f, 0f, 0f);
	}

	public override Vector4 GetPadding(int index, string name)
	{
		BIJLog.E("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		BIJLog.E("GetPadding is not Exist!!!! (作ってない!)");
		BIJLog.E("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		return new Vector4(0f, 0f, 0f, 0f);
	}

	public override Vector4 GetPixelBorder(int index, string name)
	{
		BIJLog.E("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		BIJLog.E("GetPadding is not Exist!!!! (作ってない!)");
		BIJLog.E("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		return new Vector4(0f, 0f, 0f, 0f);
	}

	public override Vector4 GetBorder(int index, string name)
	{
		BIJLog.E("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		BIJLog.E("GetPadding is not Exist!!!! (作ってない!)");
		BIJLog.E("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		return new Vector4(0f, 0f, 0f, 0f);
	}
}
