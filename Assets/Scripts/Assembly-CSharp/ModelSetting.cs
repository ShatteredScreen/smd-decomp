using System.Collections.Generic;

internal interface ModelSetting
{
	string GetModelName();

	string GetModelFile();

	int GetTextureNum();

	string GetTextureFile(int n);

	string[] GetTextureFiles();

	int GetHitAreasNum();

	string GetHitAreaID(int n);

	string GetHitAreaName(int n);

	string GetPhysicsFile();

	string GetPoseFile();

	int GetExpressionNum();

	string GetExpressionFile(int n);

	string[] GetExpressionFiles();

	string GetExpressionName(int n);

	string[] GetExpressionNames();

	string GetExpressionExpandParam(int n, string expand);

	bool IsExpressionExpandParam(int n, string expand);

	string[] GetMotionGroupNames();

	int GetMotionNum(string name);

	string GetMotionFile(string name, int n);

	string GetMotionExpandParam(string name, int n, string expand);

	bool IsMotionExpandParam(string name, int n, string expand);

	string GetMotionSound(string name, int n);

	int GetMotionFadeIn(string name, int n);

	int GetMotionFadeOut(string name, int n);

	bool GetLayout(Dictionary<string, float> layout);

	int GetInitParamNum();

	float GetInitParamValue(int n);

	string GetInitParamID(int n);

	int GetInitPartsVisibleNum();

	float GetInitPartsVisibleValue(int n);

	string GetInitPartsVisibleID(int n);
}
