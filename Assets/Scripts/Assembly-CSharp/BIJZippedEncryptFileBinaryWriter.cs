using System;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;

public class BIJZippedEncryptFileBinaryWriter : BIJBinaryWriter
{
	private const string CONTENT = "._CONTENT_";

	private MemoryStream mStream;

	private ZipOutputStream mZipStream;

	public BIJZippedEncryptFileBinaryWriter(string path)
		: this(path, string.Empty, Encoding.UTF8, true)
	{
	}

	public BIJZippedEncryptFileBinaryWriter(string path, string password)
		: this(path, password, Encoding.UTF8, true)
	{
	}

	public BIJZippedEncryptFileBinaryWriter(string path, string password, Encoding encoding)
		: this(path, password, encoding, true)
	{
	}

	public BIJZippedEncryptFileBinaryWriter(string path, string password, Encoding encoding, bool mkdir)
	{
		mStream = null;
		Writer = null;
		try
		{
			mPath = path;
			string directoryName = System.IO.Path.GetDirectoryName(mPath);
			if (mkdir && !string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			mStream = new MemoryStream();
			mZipStream = new ZipOutputStream(mStream);
			mZipStream.SetLevel(9);
			if (!string.IsNullOrEmpty(password))
			{
				mZipStream.Password = password;
			}
			ZipNameTransform zipNameTransform = new ZipNameTransform(string.Empty);
			string name = zipNameTransform.TransformFile("._CONTENT_");
			ZipEntry entry = new ZipEntry(name)
			{
				IsUnicodeText = true,
				DateTime = DateTime.Now
			};
			mZipStream.PutNextEntry(entry);
			Writer = new BinaryWriter(mZipStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	public override void Close()
	{
		base.Close();
		if (mZipStream != null)
		{
			try
			{
				mZipStream.Flush();
				mZipStream.Finish();
				mZipStream.Close();
			}
			catch (Exception)
			{
			}
			mZipStream = null;
		}
		if (mStream != null)
		{
			try
			{
				byte[] bytes = CryptUtils.EncryptRJ128(mStream.ToArray());
				File.WriteAllBytes(mPath, bytes);
				mStream.Close();
			}
			catch (Exception)
			{
			}
			mStream = null;
		}
	}
}
