using UnityEngine;

public class WebViewDialog : DialogBase
{
	public enum STATE
	{
		OPEN = 0,
		MAIN = 1,
		WAIT = 2
	}

	public enum WEBVIEW_STATE
	{
		NONE = 0,
		PROGRESS = 1,
		ERROR = 2
	}

	public enum WEBVIEW_TYPE
	{
		FULL = 0,
		HALF_TOP = 1,
		HALF_BOTTOM = 2,
		HALF_LEFT = 3,
		HALF_RIGHT = 4,
		VERTICAL_RATE = 5,
		ADJUST_MIN_WIDTH_VERTICAL_RATE = 6
	}

	public enum EXTERNAL_SPAWN_MODE
	{
		NONE = 0,
		SPAWN_AT_OUT_OF_PAGE = 1,
		SPAWN_AT_OUT_OF_DOMAIN = 2
	}

	public delegate void OnDialogClosed();

	public delegate void OnPageMoved(bool first, bool error);

	public delegate void OnWebViewMessage(string message);

	private const string II_BACKGROUND = "instruction_panel";

	private const string CB_Webview_onError = "Webview_onError";

	private const string CB_Webview_onClose = "Webview_onClose";

	private const string CB_Webview_onPageStarted = "Webview_onPageStarted";

	private const string CB_Webview_onPageFinished = "Webview_onPageFinished";

	private const string CB_Webview_onProgress = "Webview_onProgress";

	private const string CB_Webview_onJson = "Webview_onJson";

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.OPEN);

	private WEBVIEW_TYPE mWebViewType;

	private float mWebViewRate = 1f;

	private bool IsCustomBackGroundColor;

	private OnDialogClosed mCallback;

	private OnPageMoved mPageMovedCallback;

	private OnWebViewMessage mWebViewMessageCallback;

	private string mEvaluateJS = string.Empty;

	private WEBVIEW_STATE mWebViewState;

	protected Vector2 mAnchorTopPos;

	protected Vector2 mAnchorBottomPos;

	protected Vector2 mAnchorLeftPos;

	protected Vector2 mAnchorRightPos;

	protected UISprite mTitleBarR;

	private UISprite mTitleBarL;

	private UISprite mBackground;

	private UILabel mTitle;

	private int mTitleSize = 38;

	private WebViewObject mWebViewObject;

	private bool mLoadURLCall;

	private string mLoadURL;

	private string mFallbackURL;

	private ScreenOrientation o;

	private UIButton mCancelButton;

	private bool mLayout = true;

	protected Color mWebViewBackGroundColor = Color.clear;

	protected Color mBackGroundColor = Color.white;

	private EXTERNAL_SPAWN_MODE mTmpSpawnMode;

	private float mAnchorPosY = -1f;

	private bool mIsPauseState;

	private STATE mPrePauseState = STATE.WAIT;

	protected bool mIsClickDeepLink;

	public string Title { get; set; }

	public bool IsOpend { get; private set; }

	protected bool IsInvisibleCancel { private get; set; }

	protected bool RemarginInLayout { private get; set; }

	protected bool IsBusy
	{
		get
		{
			return GetBusy() || mState.GetStatus() != STATE.MAIN;
		}
	}

	public void Init()
	{
		IsCustomBackGroundColor = false;
		mBackGroundColor = Color.white;
	}

	public void Init(Color back_ground_color)
	{
		IsCustomBackGroundColor = true;
		mBackGroundColor = back_ground_color;
	}

	public override void Start()
	{
		base.Start();
		IsOpend = false;
		mIsClickDeepLink = false;
		RemarginInLayout = true;
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		mWebViewObject = null;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.OPEN:
			mState.Change(STATE.MAIN);
			break;
		}
		mState.Update();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		SetLayout(o);
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		UISpriteData uISpriteData = null;
		Vector2 log_screen = Util.LogScreenSize();
		SetJellyTargetScale(1f);
		string title = Title;
		mBackground = Util.CreateSprite("BackGround", base.gameObject, image);
		string imageName = "frame_bg00";
		if (IsCustomBackGroundColor)
		{
			imageName = "bg00w";
			mBackground.color = mBackGroundColor;
		}
		Util.SetSpriteInfo(mBackground, imageName, mBaseDepth, Vector3.zero, Vector3.one, false);
		mBackground.type = UIBasicSprite.Type.Sliced;
		mBackground.SetDimensions(1386, 1386);
		float num = log_screen.y / 2f;
		float num2 = log_screen.x / 2f;
		CalculateAnchorPos();
		float num3 = num + mAnchorTopPos.y;
		float y = num3 + 50f - 123f;
		mTitleBarR = Util.CreateSprite("TitleBarR", base.gameObject, image);
		Util.SetSpriteInfo(mTitleBarR, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, y, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBarL = Util.CreateSprite("TitleBarL", base.gameObject, image);
		Util.SetSpriteInfo(mTitleBarL, "menubar_frame", mBaseDepth + 2, new Vector3(2f, y, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		y = num3 - 28f;
		mTitle = Util.CreateLabel("Title", base.gameObject, atlasFont);
		Util.SetLabelInfo(mTitle, string.Empty, mBaseDepth + 3, Vector3.zero, mTitleSize, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(mTitle, title);
		mTitle.gameObject.transform.localPosition = new Vector3(0f, y, 0f);
		if (!IsInvisibleCancel)
		{
			float x = num2 - 40f;
			y = num - 40f;
			imageName = "button_return";
			mCancelButton = Util.CreateJellyImageButton("ButtonCancel", base.gameObject, image);
			Util.SetImageButtonInfo(mCancelButton, imageName, imageName, imageName, mBaseDepth + 3, Vector3.zero, Vector3.one);
			Util.AddImageButtonMessage(mCancelButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
			mCancelButton.gameObject.transform.localPosition = new Vector3(x, y, 0f);
		}
		RemarginInLayout = false;
		SetLayout(Util.ScreenOrientation);
		GameObject gameObject = Util.CreateGameObject("WebView", base.gameObject);
		float num4 = (float)(-mTitleBarR.height) - 50f - 123f - mAnchorTopPos.y;
		mWebViewObject = gameObject.AddComponent<WebViewObject>();
		mWebViewObject.SetBackGroundColor(mWebViewBackGroundColor);
		mWebViewObject.Init(OnWebViewObjectCallback);
		mWebViewObject.SetVisibility(false);
		int num5 = (int)Util.ToDevY(num4);
		SetWebViewMargin(num4, log_screen);
		SetExternalSpawnMode(mTmpSpawnMode);
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		boxCollider.center = new Vector3(0f, 0f, 0f);
		boxCollider.size = new Vector3(log_screen.x, log_screen.y, 1f);
	}

	protected virtual void SetLayout(ScreenOrientation o)
	{
		CalculateAnchorPos();
		mAnchorPosY = mAnchorTopPos.y;
		Vector2 log_screen = Util.LogScreenSize();
		float num = log_screen.y / 2f;
		float num2 = num + mAnchorTopPos.y;
		switch (o)
		{
		}
		float y = num2 + 50f + 123f;
		mTitleBarR.gameObject.transform.localPosition = new Vector3(-2f, y, 0f);
		mTitleBarL.gameObject.transform.localPosition = new Vector3(2f, y, 0f);
		y = num2 - 28f;
		mTitle.gameObject.transform.localPosition = new Vector3(0f, y, 0f);
		float num3 = log_screen.x / 2f;
		float num4 = num3 + mAnchorRightPos.x;
		float x = num4 - 40f;
		y = num2 - 40f;
		if (mCancelButton != null)
		{
			mCancelButton.gameObject.transform.localPosition = new Vector3(x, y, 0f);
		}
		if (RemarginInLayout)
		{
			float header_height = (float)(-mTitleBarR.height) - 50f - 123f - mAnchorTopPos.y;
			float x2 = mAnchorLeftPos.x;
			SetWebViewMargin(header_height, log_screen);
		}
		RemarginInLayout = true;
	}

	protected void CalculateAnchorPos()
	{
		Vector2 vector = Util.LogScreenSize();
		float num = vector.y / 2f;
		float num2 = vector.x / 2f;
		mAnchorTopPos = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, Util.ScreenOrientation);
		mAnchorTopPos.y -= num;
		mAnchorBottomPos = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, Util.ScreenOrientation);
		mAnchorBottomPos.y += num;
		mAnchorLeftPos = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Left, Util.ScreenOrientation);
		mAnchorLeftPos.x += num2;
		mAnchorRightPos = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Right, Util.ScreenOrientation);
		mAnchorRightPos.x -= num2;
	}

	private void OnWebViewObjectCallback(string message)
	{
		if (mWebViewMessageCallback != null)
		{
			mWebViewMessageCallback(message);
		}
		if (string.Compare("Webview_onPageStarted", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.PROGRESS;
			}
		}
		else if (string.Compare("Webview_onPageFinished", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS || mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.NONE;
				if (mPageMovedCallback != null)
				{
					mPageMovedCallback(mLoadURLCall, false);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onProgress", message) == 0)
		{
			if (mWebViewState != WEBVIEW_STATE.PROGRESS)
			{
			}
		}
		else if (string.Compare("Webview_onError", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS)
			{
				mWebViewState = WEBVIEW_STATE.ERROR;
				if (mPageMovedCallback != null)
				{
					mPageMovedCallback(mLoadURLCall, true);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onClose", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.NONE)
			{
				mState.Change(STATE.WAIT);
				Close();
			}
		}
		else if (message.StartsWith("Webview_onJson") && mWebViewState == WEBVIEW_STATE.NONE)
		{
			string a_json = message.Remove(0, "Webview_onJson".Length);
			SingletonMonoBehaviour<GameMain>.Instance.OnDeeplinkAction(a_json);
			mIsClickDeepLink = true;
			mState.Change(STATE.WAIT);
			Close();
		}
	}

	public void SetExternalSpawnMode(EXTERNAL_SPAWN_MODE mode)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetExternalSpawnMode((int)mode);
		}
		mTmpSpawnMode = mode;
	}

	public void LoadURL(string url)
	{
		LoadURL(url, null);
	}

	public void LoadURL(string url, string fallback)
	{
		if (mWebViewObject != null)
		{
			mLoadURLCall = true;
			mWebViewState = WEBVIEW_STATE.NONE;
			mLoadURL = url;
			mFallbackURL = fallback;
			mWebViewObject.LoadURL(url);
		}
	}

	public void Reload()
	{
		if (mWebViewObject != null)
		{
			mLoadURLCall = true;
			mWebViewState = WEBVIEW_STATE.NONE;
			mWebViewObject.Reload();
		}
	}

	public void ShowWebview(bool show)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(show);
		}
	}

	public void SetWebViewType(WEBVIEW_TYPE type, float rate = 1f)
	{
		mWebViewType = type;
		mWebViewRate = rate;
	}

	protected void SetWebViewMargin(float header_height, Vector2 log_screen)
	{
		float num = Mathf.Min(log_screen.x, log_screen.y);
		int left;
		int top;
		int right;
		int bottom;
		switch (mWebViewType)
		{
		default:
			left = 0;
			top = (int)Util.ToDevY(header_height);
			right = 0;
			bottom = 0;
			break;
		case WEBVIEW_TYPE.HALF_TOP:
			left = 0;
			top = (int)Util.ToDevY(header_height);
			right = 0;
			bottom = (int)Util.ToDevY((log_screen.y - header_height) / 2f);
			break;
		case WEBVIEW_TYPE.HALF_LEFT:
			left = 0;
			top = (int)Util.ToDevY(header_height);
			right = (int)Util.ToDevY(log_screen.x / 2f);
			bottom = 0;
			break;
		case WEBVIEW_TYPE.HALF_RIGHT:
			left = (int)Util.ToDevY(log_screen.x / 2f);
			top = (int)Util.ToDevY(header_height);
			right = 0;
			bottom = 0;
			break;
		case WEBVIEW_TYPE.HALF_BOTTOM:
			left = 0;
			top = (int)Util.ToDevY(header_height + (log_screen.y - header_height) / 2f);
			right = 0;
			bottom = 0;
			break;
		case WEBVIEW_TYPE.VERTICAL_RATE:
			left = 0;
			top = (int)Util.ToDevY(header_height);
			right = 0;
			bottom = (int)Util.ToDevY(log_screen.y * (1f - mWebViewRate));
			break;
		case WEBVIEW_TYPE.ADJUST_MIN_WIDTH_VERTICAL_RATE:
			left = (int)Util.ToDevY((log_screen.x - num) / 4f);
			top = (int)Util.ToDevY(header_height);
			right = (int)Util.ToDevY((log_screen.x - num) / 4f);
			bottom = (int)Util.ToDevY(log_screen.y * (1f - mWebViewRate));
			break;
		}
		if (mWebViewObject != null)
		{
			mWebViewObject.SetMargins(left, top, right, bottom);
		}
	}

	public virtual void PauseState()
	{
		if (!mIsPauseState)
		{
			mIsPauseState = true;
			mPrePauseState = mState.GetStatus();
			mState.Reset(STATE.WAIT, true);
		}
	}

	public virtual void ResumeState()
	{
		if (mIsPauseState)
		{
			mState.Change(mPrePauseState);
		}
	}

	public override void OnOpenFinished()
	{
		IsOpend = true;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
	}

	public override void Close()
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(false);
		}
		mGame.FinishConnecting();
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		if (mCancelButton != null)
		{
			OnCancelPushed(null);
		}
	}

	public virtual void OnCancelPushed(GameObject go)
	{
		if (!IsBusy)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetPageMovedCallback(OnPageMoved callback)
	{
		mPageMovedCallback = callback;
	}

	public void SetWebViewMessageCallback(OnWebViewMessage callback)
	{
		mWebViewMessageCallback = callback;
	}

	public void SetEvaluateJS(string js)
	{
		mEvaluateJS = js;
	}

	public void SetTitleLabelSize(int _size)
	{
		mTitleSize = _size;
		if (mTitle != null)
		{
			mTitle.fontSize = mTitleSize;
		}
	}

	public void SetWebViewBackGroundColor(Color aColor)
	{
		mWebViewBackGroundColor = aColor;
	}

	public void SetInvisibleCancelButton(bool aInvisible)
	{
		IsInvisibleCancel = aInvisible;
	}

	protected bool IsEqualStatus(STATE state)
	{
		return state == mState.GetStatus();
	}

	protected bool IsEqualWebViewStatus(WEBVIEW_STATE state)
	{
		return state == mWebViewState;
	}
}
