public class RatioChange1Info
{
	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("event")]
	public int EventID { get; set; }

	[MiniJSONAlias("stageMin")]
	public int StageMin { get; set; }

	[MiniJSONAlias("stageMax")]
	public int StageMax { get; set; }

	[MiniJSONAlias("type")]
	public int Type { get; set; }

	[MiniJSONAlias("trigger")]
	public int Trigger { get; set; }

	[MiniJSONAlias("interval")]
	public int Interval { get; set; }

	[MiniJSONAlias("addition")]
	public int Addition { get; set; }

	[MiniJSONAlias("type2Info")]
	public RatioChangeType2 Type2Info { get; set; }

	public RatioChange1Info()
	{
		Series = 0;
		StageMin = -1;
		StageMax = -1;
		Type = 1;
		Trigger = 5;
		Interval = 480;
		Addition = 0;
		Type2Info = new RatioChangeType2();
	}
}
