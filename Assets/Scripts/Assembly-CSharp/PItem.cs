using System;

public class PItem : ListItem, ICloneable, ITierData
{
	public enum KIND
	{
		SD0 = 0,
		SD1 = 1,
		SD2 = 2,
		SD3 = 3,
		SD4 = 4,
		SD5 = 5,
		SD6 = 6,
		CA0 = 7,
		CA1 = 8,
		DUMMY0 = 9,
		CA2 = 10,
		CA3 = 11,
		CA4 = 12,
		CA5 = 13,
		CA6 = 14,
		CA7 = 15,
		CA8 = 16,
		CA9 = 17,
		CA10 = 18,
		CA11 = 19,
		CA12 = 20,
		CA13 = 21,
		CA14 = 22,
		CA15 = 23
	}

	public int originalAmount;

	public int campaignAmount;

	public KIND kind;

	public float cost;

	public string itemID = string.Empty;

	public string currencyCode = string.Empty;

	public string formattedPrice = string.Empty;

	public string orderID = string.Empty;

	public int iconID;

	public int campaignIconID;

	public string signature = string.Empty;

	public bool storeInfo;

	public int amount
	{
		get
		{
			if (campaignAmount > 0)
			{
				return campaignAmount;
			}
			return originalAmount;
		}
		set
		{
			originalAmount = value;
		}
	}

	public int ItemNo
	{
		get
		{
			return (int)kind;
		}
	}

	public string ItemID
	{
		get
		{
			return itemID;
		}
	}

	public int Amount
	{
		get
		{
			return amount;
		}
	}

	public double Cost
	{
		get
		{
			return cost;
		}
	}

	public string CurrencyCode
	{
		get
		{
			return currencyCode;
		}
	}

	public string FormattedPrice
	{
		get
		{
			return formattedPrice;
		}
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public static bool IsSDUp(KIND _kind)
	{
		return true;
	}

	public override string ToString()
	{
		return string.Format("<{0}><{1}> {2} {3}x{4} {5} {6}", itemID, orderID, kind, cost, amount, currencyCode, formattedPrice);
	}

	public PItem Clone()
	{
		return MemberwiseClone() as PItem;
	}
}
