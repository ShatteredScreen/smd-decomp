using UnityEngine;

public class OneShotParticle : MonoBehaviour
{
	private SMParticle mParticle;

	private float mLifeTime;

	private float mDelayTime;

	private void Awake()
	{
		mParticle = GetComponent<SMParticle>();
	}

	private void Start()
	{
	}

	private void Update()
	{
		if (mDelayTime > 0f)
		{
			mDelayTime -= Time.deltaTime;
			if (mDelayTime <= 0f)
			{
				mParticle.mParticle.Play();
			}
		}
		mLifeTime -= Time.deltaTime;
		if (mLifeTime <= 0f)
		{
			Object.Destroy(base.gameObject);
		}
	}

	public void Init(float lifeTime, float delayTime)
	{
		mDelayTime = delayTime;
		mLifeTime = lifeTime;
		if (delayTime == 0f)
		{
			mParticle.mParticle.Play();
		}
	}
}
