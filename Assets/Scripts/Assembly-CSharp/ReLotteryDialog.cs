using System.Collections.Generic;
using UnityEngine;

public class ReLotteryDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		AUTHERROR_RETURN_TITLE = 2
	}

	public enum SELECT_ITEM
	{
		OK = 0,
		NG = 1
	}

	public enum MODE
	{
		TICKET = 1,
		HEART = 2,
		MUGEN_HEART = 99,
		HEART_LESS = 100
	}

	private class RewardSpriteData
	{
		public string mImgName;

		public int mNum;

		public RewardSpriteData()
		{
			mImgName = string.Empty;
			mNum = 0;
		}
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private SELECT_ITEM mSelectItem = SELECT_ITEM.NG;

	private MODE mCreateMode = MODE.MUGEN_HEART;

	private MODE mDialogMode = MODE.MUGEN_HEART;

	private string mNekoImg = string.Empty;

	private int mItemCount;

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private BIJImage atlas;

	private Vector2 mLogScreen = Util.LogScreenSize();

	private List<AccessoryData> mAccessoryDataList;

	private bool mNoGetFlg;

	private UILabel mNankoLabel;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		mLogScreen = Util.LogScreenSize();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.AUTHERROR_RETURN_TITLE:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	public void Init(MODE _CreateMode, int _ItemCount, string _NekoImg)
	{
		mCreateMode = _CreateMode;
		mItemCount = _ItemCount;
		mNekoImg = _NekoImg;
		mDialogMode = _CreateMode;
	}

	public void SetDialogMode(MODE a_mode)
	{
		mDialogMode = a_mode;
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("EVENTBINGO").Image;
		UIFont atlasFont = GameMain.LoadFont();
		Vector3 position = new Vector3(0f, 4f, 0f);
		string titleDescKey = Localization.Get("BINGO_ReLot_Title");
		string text = Localization.Get("BINGO_ReLot_Desc00_A") + "\n" + Localization.Get("BINGO_ReLot_Desc01");
		string imageName = "icon_heart";
		string format = Localization.Get("BINGO_ReLot_Desc03_A");
		if (mCreateMode == MODE.MUGEN_HEART)
		{
			text = Localization.Get("BINGO_ReLot_Desc00_C") + "\n" + Localization.Get("BINGO_ReLot_Desc01");
			imageName = "icon_heartinfinite";
			position = new Vector3(0f, 13f, 0f);
		}
		else if (mCreateMode == MODE.TICKET)
		{
			text = Localization.Get("BINGO_ReLot_Desc00_B") + "\n" + Localization.Get("BINGO_ReLot_Desc01");
			imageName = "icon_event_ticket00";
			format = Localization.Get("BINGO_ReLot_Desc03_B");
		}
		format = string.Format(format, mItemCount);
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UILabel uILabel = Util.CreateLabel("Text", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(0f, 180f, 0f), 25, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(530, 80);
		uILabel.spacingY = 8;
		UISprite sprite = Util.CreateSprite("Neko", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, mNekoImg, mBaseDepth + 3, new Vector3(-119f, -10f, 0f), Vector3.one, false);
		sprite = Util.CreateSprite("Fukidashi", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "chara_bng02", mBaseDepth + 3, new Vector3(115f, 75f, 0f), Vector3.one, false);
		if (mCreateMode != MODE.MUGEN_HEART)
		{
			int num = 28;
			num = 16;
			Vector3 position2 = new Vector3(-11f, 114f, 0f);
			uILabel = Util.CreateLabel("Ato", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("BINGO_ReLot_Desc02"), mBaseDepth + 4, position2, num, 0, 0, UIWidget.Pivot.Left);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			position2 = new Vector3(158f, 75f, 0f);
			uILabel = Util.CreateLabel("Nanko", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, format, mBaseDepth + 4, position2, 28, 0, 0, UIWidget.Pivot.Left);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			mNankoLabel = uILabel;
		}
		sprite = Util.CreateSprite("Item", sprite.gameObject, image);
		Util.SetSpriteInfo(sprite, imageName, mBaseDepth + 4, position, Vector3.one, false);
		UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 3, new Vector3(-130f, -190f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnOKPushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonNG", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 3, new Vector3(130f, -190f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void OnOKPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			if (mDialogMode == MODE.HEART_LESS)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.BINGO_RELOTTERY_BUY_HEART;
				BuyHeartDialog buyHeartDialog = Util.CreateGameObject("HeartDialog", base.ParentGameObject).AddComponent<BuyHeartDialog>();
				buyHeartDialog.Place = 3;
				buyHeartDialog.SetClosedCallback(OnHeartDialogDialogClosed);
				buyHeartDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mSelectItem = SELECT_ITEM.OK;
				OnClosePushed();
			}
		}
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		mState.Reset(STATE.WAIT, true);
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void OnHeartDialogDialogClosed(BuyHeartDialog.SELECT_ITEM i)
	{
		if (mDialogMode == MODE.HEART_LESS && mNankoLabel != null && i == BuyHeartDialog.SELECT_ITEM.BUY_COMPLETE)
		{
			string format = Localization.Get("BINGO_ReLot_Desc03_A");
			int itemCount = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1);
			itemCount += mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 2);
			format = string.Format(format, itemCount);
			mNankoLabel.text = format;
			mDialogMode = MODE.HEART;
		}
		mState.Reset(STATE.MAIN, true);
	}

	public void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}
}
