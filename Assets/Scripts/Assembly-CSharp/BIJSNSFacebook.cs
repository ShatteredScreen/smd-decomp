using System;
using System.Collections;
using UnityEngine;

public class BIJSNSFacebook : BIJSNSAppSwitcher
{
	private const float ATTACHEMENT_RESIZE_SCALE = 1f;

	public override SNS Kind
	{
		get
		{
			return SNS.Facebook;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		OP_TYPE oP_TYPE = OP_TYPE.NONE;
		try
		{
			oP_TYPE = (OP_TYPE)(int)Enum.ToObject(typeof(OP_TYPE), type);
		}
		catch (Exception)
		{
			return false;
		}
		switch (oP_TYPE)
		{
		case OP_TYPE.POSTMESSAGE:
		case OP_TYPE.INVITE:
		case OP_TYPE.UPLOADIMAGE:
			return true;
		default:
			return false;
		}
	}

	private IEnumerator BIJSNSFacebook_UploadImage_Coroutine(int seqno, IBIJSNSPostData _data, Handler handler, object userdata, float scale)
	{
		yield return new WaitForSeconds(0.5f);
		string title = BIJSNS.ToSS(_data.Title);
		string message = BIJSNS.ToSS(_data.Message) + ((!string.IsNullOrEmpty(_data.LinkURL)) ? ("\n" + BIJSNS.ToSS(_data.LinkURL)) : string.Empty);
		UnityEngine.Debug.Log("@@@ SNS @@@ " + message);
		string url = string.Empty;
		if (spawn)
		{
			CallCallback(handler, new Response(1, "Facebook already invoked.", null), userdata);
			yield break;
		}
		if (IsEnableSwitching())
		{
			SetSpawn(seqno);
		}
		bool ret = BIJUnity.BIJUnity_invokeFacebookWithImage(url);
		UnityEngine.Debug.Log("SNSInvokeFacebookWithImage: " + ret);
		if (!ret)
		{
			if (base.CurrentSeqNo == seqno)
			{
				base.CurrentRes = new Response(1, "Facebook did not upload image: " + url, null);
				ChangeStep(STEP.COMM_RESULT);
			}
			SetSpawn(0);
		}
	}

	private void DoPostMessage(OP_TYPE type, IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		try
		{
			int num = PushReq((int)type, handler, userdata);
			if (num != 0)
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSFacebook_UploadImage_Coroutine(num, postdata, handler, userdata, 1f));
			}
			else
			{
				CallCallback(handler, new Response(1, "Facebook not requested", null), userdata);
			}
		}
		catch (Exception ex)
		{
			CallCallback(handler, new Response(1, "Facebook unknown error: " + ex.ToString(), null), userdata);
		}
	}

	public override void PostMessage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		DoPostMessage(OP_TYPE.POSTMESSAGE, postdata, handler, userdata);
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		DoPostMessage(OP_TYPE.UPLOADIMAGE, postdata, handler, userdata);
	}

	private IEnumerator BIJSNSFacebook_PluginFailed(int seqno, string message, object result)
	{
		yield return new WaitForSeconds(0.5f);
		SetSpawn(0);
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSFacebook_PluginFailed failed: seqno=" + seqno + " current=" + base.CurrentSeqNo + " message=" + message);
		if (base.CurrentSeqNo == seqno)
		{
			base.CurrentRes = new Response(1, message, result);
			ChangeStep(STEP.COMM_RESULT);
		}
	}
}
