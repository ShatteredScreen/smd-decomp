public class SeriesCampaignSetting
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("series")]
	public short SeriesOriginal { get; set; }

	[MiniJSONAlias("eventID")]
	public int EventID { get; set; }

	[MiniJSONAlias("move")]
	public int Moves { get; set; }

	[MiniJSONAlias("banner")]
	public string BannerFileName { get; set; }

	public Def.SERIES Series
	{
		get
		{
			return (Def.SERIES)SeriesOriginal;
		}
	}
}
