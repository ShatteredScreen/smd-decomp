public class SMDImageUtil
{
	public static string GetLoginBonusIconKey(int a_category, int a_subcategory, int a_quantity)
	{
		string empty = string.Empty;
		switch (a_category)
		{
		case 25:
			if (a_subcategory == 1)
			{
				int num = 0;
				if (a_quantity >= 10000)
				{
					num = 2;
				}
				else if (a_quantity >= 5000)
				{
					num = 1;
				}
				return string.Format("LC_login_{0}_{1}_{2}", a_category, a_subcategory, num);
			}
			return string.Format("LC_login_{0}_{1}", a_category, a_subcategory);
		case 7:
		case 8:
			return string.Format("LC_login_{0}_{1}", a_category, 0);
		case 32:
			return string.Format("LC_login_{0}_0", a_category);
		default:
			return string.Format("LC_login_{0}_{1}", a_category, a_subcategory);
		}
	}

	public static string GetHUDIconKey(int a_category, int a_subcategory, int a_quantity)
	{
		string empty = string.Empty;
		switch (a_category)
		{
		case 25:
			switch (a_subcategory)
			{
			case 2:
				return "adv_icon_skill";
			case 3:
				return "adv_item_tiket00";
			case 4:
				return "adv_item_tiket01";
			case 5:
				return "adv_item_tiket02";
			case 1:
			{
				int num = 0;
				if (a_quantity >= 10000)
				{
					num = 2;
				}
				else if (a_quantity >= 5000)
				{
					num = 1;
				}
				return string.Format("LC_item_xp{0:D2}", num);
			}
			default:
				return "null";
			}
		case 26:
			return "adv_item_coin";
		case 24:
			return string.Empty;
		case 32:
			return string.Format("LC_login_{0}_0", a_category);
		case 1:
			if (a_subcategory == 20)
			{
				return "icon_old_event_key";
			}
			return string.Format("LC_login_{0}_{1}", a_category, a_subcategory);
		default:
			return string.Format("LC_login_{0}_{1}", a_category, a_subcategory);
		}
	}
}
