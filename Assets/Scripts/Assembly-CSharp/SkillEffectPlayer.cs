using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillEffectPlayer : MonoBehaviour
{
	public enum STATE
	{
		INIT = 0,
		PLAY = 1,
		END = 2,
		WAIT = 3
	}

	public enum MODE
	{
		PUZZLE = 0,
		COLLECTION = 1,
		APPEAL = 2,
		REWARD = 3
	}

	private struct EFFECT_SETTING
	{
		public bool releaseAnime;

		public bool playStartSe;

		public Vector3 backBasePosP;

		public Vector3 backBasePosL;

		public Vector3 foreBasePos;

		public bool enableEnterJump;

		public bool enableExitLanding;

		public bool enableText;

		public EFFECT_SETTING(bool _releaseAnime, bool _playStartSe, Vector3 _backBasePosP, Vector3 _backBasePosL, Vector3 _foreBasePos, bool _enableEnterJump, bool _enableExitLanding, bool _enableText)
		{
			releaseAnime = _releaseAnime;
			playStartSe = _playStartSe;
			backBasePosP = _backBasePosP;
			backBasePosL = _backBasePosL;
			foreBasePos = _foreBasePos;
			enableEnterJump = _enableEnterJump;
			enableExitLanding = _enableExitLanding;
			enableText = _enableText;
		}
	}

	public delegate void OnFinished();

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private EFFECT_SETTING[] EffectSettings = new EFFECT_SETTING[4]
	{
		new EFFECT_SETTING(false, true, new Vector3(0f, 118f, Def.PUZZLE_BG_Z + 1f), new Vector3(-360f, -260f, Def.PUZZLE_BG_Z + 1f), new Vector3(0f, 0f, -20f), true, true, true),
		new EFFECT_SETTING(true, false, new Vector3(0f, 0f, -100f), new Vector3(-300f, -260f, -100f), new Vector3(0f, 0f, -20f), true, true, true),
		new EFFECT_SETTING(true, false, Vector3.zero, Vector3.zero, new Vector3(0f, 0f, -20f), false, false, false),
		new EFFECT_SETTING(true, false, Vector3.zero, Vector3.zero, new Vector3(0f, 0f, -20f), false, false, false)
	};

	private GameMain mGame;

	private MODE mMode;

	private Partner mPartner;

	private int mRank;

	private GameObject mParentObject;

	private bool mPartnerForeground;

	private bool[] mSkillMotionFinished;

	private OnFinished OnFinishCallback = delegate
	{
	};

	private bool mCallbackCalled;

	private FixedSprite mBrindTop;

	private FixedSprite mBrindDown;

	private FixedSprite mBrindLeft;

	private FixedSprite mBrindRight;

	private void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			StartCoroutine(StartSkillEffect());
			break;
		case STATE.END:
			OnFinishCallback();
			mCallbackCalled = true;
			mState.Reset(STATE.WAIT);
			break;
		}
		if (mPartner != null)
		{
			mPartner.SetPartnerScreenPos(GetPartnerScreenBasePos(Util.ScreenOrientation));
		}
		mState.Update();
	}

	private void OnDestroy()
	{
		if (EffectSettings[(int)mMode].releaseAnime)
		{
			UnloadSkillAnimation();
		}
		if (!mCallbackCalled)
		{
			OnFinishCallback();
		}
	}

	public void Init(Partner partner, int rank, GameObject parent, MODE mode, OnFinished callback)
	{
		mPartner = partner;
		mRank = rank;
		mParentObject = parent;
		mMode = mode;
		OnFinishCallback = callback;
		mCallbackCalled = false;
		if (!EffectSettings[(int)mMode].enableEnterJump)
		{
			mPartner.SetPartnerScreenEnable(false);
		}
	}

	private Vector3 GetPartnerScreenBasePos(ScreenOrientation o)
	{
		if (mPartnerForeground)
		{
			return EffectSettings[(int)mMode].foreBasePos;
		}
		Vector3 result = Vector3.zero;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			result = EffectSettings[(int)mMode].backBasePosP;
			if (mMode == MODE.PUZZLE && mPartner.mData.ModelCount > 1)
			{
				result += Partner.PUZZLE_PAIRPARTNER_OFS;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			result = EffectSettings[(int)mMode].backBasePosL;
			break;
		}
		return result;
	}

	private IEnumerator CoLoadSkillAnimation(bool waitForFinished)
	{
		List<ResourceInstance> resList = new List<ResourceInstance>();
		SkillEffectData effectData = mPartner.mData.GetSkillEffectData(mRank);
		if (!string.IsNullOrEmpty(effectData.efcText))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(effectData.efcText));
		}
		if (!string.IsNullOrEmpty(effectData.efcA))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(effectData.efcA));
		}
		if (!string.IsNullOrEmpty(effectData.efcB))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(effectData.efcB));
		}
		if (!string.IsNullOrEmpty(effectData.efcC))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(effectData.efcC));
		}
		if (!string.IsNullOrEmpty(effectData.efcD))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(effectData.efcD));
		}
		if (!string.IsNullOrEmpty(effectData.efcBack))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(effectData.efcBack));
		}
		if (!string.IsNullOrEmpty(effectData.soundName))
		{
			resList.Add(ResourceManager.LoadSoundAsync(effectData.soundName));
		}
		if (!waitForFinished)
		{
			yield break;
		}
		while (true)
		{
			bool wait = false;
			foreach (ResourceInstance res in resList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
	}

	private void UnloadSkillAnimation()
	{
		SkillEffectData skillEffectData = mPartner.mData.GetSkillEffectData(mRank);
		if (!string.IsNullOrEmpty(skillEffectData.efcText))
		{
			ResourceManager.UnloadSsAnimation(skillEffectData.efcText);
		}
		if (!string.IsNullOrEmpty(skillEffectData.efcA))
		{
			ResourceManager.UnloadSsAnimation(skillEffectData.efcA);
		}
		if (!string.IsNullOrEmpty(skillEffectData.efcB))
		{
			ResourceManager.UnloadSsAnimation(skillEffectData.efcB);
		}
		if (!string.IsNullOrEmpty(skillEffectData.efcC))
		{
			ResourceManager.UnloadSsAnimation(skillEffectData.efcC);
		}
		if (!string.IsNullOrEmpty(skillEffectData.efcD))
		{
			ResourceManager.UnloadSsAnimation(skillEffectData.efcD);
		}
		if (!string.IsNullOrEmpty(skillEffectData.efcBack))
		{
			ResourceManager.UnloadSsAnimation(skillEffectData.efcBack);
		}
		if (!string.IsNullOrEmpty(skillEffectData.soundName))
		{
			ResourceManager.UnloadSound(skillEffectData.soundName);
		}
	}

	public static GameObject CreateDemoCamera(bool makeUIPanel = false)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.DEMO_CAMERA));
		gameObject.transform.parent = SingletonMonoBehaviour<GameMain>.Instance.mCamera.transform.parent;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		if (makeUIPanel)
		{
			UIPanel uIPanel = gameObject.AddComponent<UIPanel>();
			uIPanel.depth = 10;
			uIPanel.sortingOrder = 10;
		}
		return gameObject;
	}

	private IEnumerator StartSkillEffect()
	{
		mState.Reset(STATE.PLAY);
		int rank = mRank;
		yield return StartCoroutine(CoLoadSkillAnimation(true));
		if (EffectSettings[(int)mMode].playStartSe)
		{
			mGame.PlaySe("VOICE_011", 2);
			mGame.PlaySe("SE_SKILL_START", 0);
		}
		Def.SkillFuncData skillFuncData = mPartner.mData.GetSkillFuncData(rank);
		SkillEffectData effectData = mPartner.mData.GetSkillEffectData(rank);
		StartCoroutine(CoMakeBrind());
		float addDelay = 0f;
		if (EffectSettings[(int)mMode].enableEnterJump)
		{
			addDelay = 1f;
		}
		if (effectData != null)
		{
			if (!string.IsNullOrEmpty(effectData.soundName))
			{
				float delay3 = addDelay;
				mGame.PlaySe(effectData.soundName, 3, 1f, delay3);
				mGame.SetMusicVolume(0.25f, 0);
			}
			if (EffectSettings[(int)mMode].enableText && !string.IsNullOrEmpty(effectData.efcText))
			{
				float delay3 = 0f;
				Util.CreateOneShotAnime("SkillText", effectData.efcText, mParentObject, new Vector3(0f, -150f, Def.HUD_Z - 40f), Vector3.one, 0f, delay3);
			}
			if (!string.IsNullOrEmpty(effectData.efcBack))
			{
				float delay3 = addDelay;
				Util.CreateOneShotAnime("SkillBack", effectData.efcBack, mParentObject, new Vector3(0f, 0f, Def.HUD_Z - 1f), Vector3.one, 0f, delay3);
			}
			if (!string.IsNullOrEmpty(effectData.efcA))
			{
				Util.CreateOneShotAnime(delay: effectData.efcDelayA / 30f + addDelay, name: "SkillEffect", key: effectData.efcA, parent: mParentObject, pos: new Vector3(0f, -284f, Def.HUD_Z - 50f), scale: new Vector3(0.6f, 0.6f, 1f), rotateDeg: 0f);
			}
			if (!string.IsNullOrEmpty(effectData.efcB))
			{
				Util.CreateOneShotAnime(delay: effectData.efcDelayB / 30f + addDelay, name: "SkillEffect", key: effectData.efcB, parent: mParentObject, pos: new Vector3(0f, -284f, Def.HUD_Z - 50f), scale: new Vector3(0.6f, 0.6f, 1f), rotateDeg: 0f);
			}
			if (!string.IsNullOrEmpty(effectData.efcC))
			{
				Util.CreateOneShotAnime(delay: effectData.efcDelayC / 30f + addDelay, name: "SkillEffect", key: effectData.efcC, parent: mParentObject, pos: new Vector3(0f, -284f, Def.HUD_Z - 50f), scale: new Vector3(0.6f, 0.6f, 1f), rotateDeg: 0f);
			}
			if (!string.IsNullOrEmpty(effectData.efcD))
			{
				Util.CreateOneShotAnime(delay: effectData.efcDelayD / 30f + addDelay, name: "SkillEffect", key: effectData.efcD, parent: mParentObject, pos: new Vector3(0f, -284f, Def.HUD_Z - 50f), scale: new Vector3(0.6f, 0.6f, 1f), rotateDeg: 0f);
			}
		}
		mSkillMotionFinished = new bool[mPartner.mData.ModelCount];
		for (int j = 0; j < mPartner.mData.ModelCount; j++)
		{
			StartCoroutine(CoSkillMotion(rank, j));
		}
		while (true)
		{
			bool wait = false;
			for (int i = 0; i < mPartner.mData.ModelCount; i++)
			{
				if (!mSkillMotionFinished[i])
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
		StartCoroutine(CoEraseBrind());
		mGame.SetMusicVolume(1f, 0);
		mState.Reset(STATE.END);
	}

	private IEnumerator CoMakeBrind()
	{
		float gap = (float)mGame.mRoot.manualHeight - 1136f;
		if (!(gap > 0f))
		{
			yield break;
		}
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		float startPos = 568f + gap;
		float endPos = 568f + gap / 2f;
		float distance = endPos - startPos;
		Vector2 scale2 = new Vector2(1136f, gap);
		Vector3 pos4 = new Vector3(0f, startPos, -101f);
		mBrindTop = Util.CreateGameObject("BrindUp", mParentObject).AddComponent<FixedSprite>();
		mBrindTop.Init("HUD_FIXED_SPRITE", "skill_blind", pos4, atlas);
		mBrindTop.ChangeSpriteSize(scale2, Vector2.one);
		pos4 = new Vector3(0f, 0f - startPos, -101f);
		mBrindDown = Util.CreateGameObject("BrindDown", mParentObject).AddComponent<FixedSprite>();
		mBrindDown.Init("HUD_FIXED_SPRITE", "skill_blind", pos4, atlas);
		mBrindDown.ChangeSpriteSize(scale2, Vector2.one);
		scale2 = new Vector3(gap, 1136f);
		pos4 = new Vector3(startPos, 0f, -101f);
		mBrindRight = Util.CreateGameObject("BrindRight", mParentObject).AddComponent<FixedSprite>();
		mBrindRight.Init("HUD_FIXED_SPRITE", "skill_blind", pos4, atlas);
		mBrindRight.ChangeSpriteSize(scale2, Vector2.one);
		pos4 = new Vector3(0f - startPos, 0f, -101f);
		mBrindLeft = Util.CreateGameObject("BrindLeft", mParentObject).AddComponent<FixedSprite>();
		mBrindLeft.Init("HUD_FIXED_SPRITE", "skill_blind", pos4, atlas);
		mBrindLeft.ChangeSpriteSize(scale2, Vector2.one);
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 360f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mBrindTop.transform.localPosition = new Vector3(0f, startPos + distance * ratio, -101f);
			mBrindDown.transform.localPosition = new Vector3(0f, 0f - startPos - distance * ratio, -101f);
			mBrindRight.transform.localPosition = new Vector3(startPos + distance * ratio, 0f, -101f);
			mBrindLeft.transform.localPosition = new Vector3(0f - startPos - distance * ratio, 0f, -101f);
			yield return 0;
		}
	}

	private IEnumerator CoEraseBrind()
	{
		float gap = (float)mGame.mRoot.manualHeight - 1136f;
		if (!(gap > 0f))
		{
			yield break;
		}
		float endPos = 568f + gap;
		float startPos = 568f + gap / 2f;
		float distance = endPos - startPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 360f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mBrindTop.transform.localPosition = new Vector3(0f, startPos + distance * ratio, -101f);
			mBrindDown.transform.localPosition = new Vector3(0f, 0f - startPos - distance * ratio, -101f);
			mBrindRight.transform.localPosition = new Vector3(startPos + distance * ratio, 0f, -101f);
			mBrindLeft.transform.localPosition = new Vector3(0f - startPos - distance * ratio, 0f, -101f);
			yield return 0;
		}
		UnityEngine.Object.Destroy(mBrindTop.gameObject);
		UnityEngine.Object.Destroy(mBrindDown.gameObject);
		UnityEngine.Object.Destroy(mBrindRight.gameObject);
		UnityEngine.Object.Destroy(mBrindLeft.gameObject);
		mBrindTop = null;
		mBrindDown = null;
		mBrindRight = null;
		mBrindLeft = null;
	}

	private IEnumerator CoSkillMotion(int rank, int characterIndex)
	{
		mSkillMotionFinished[characterIndex] = false;
		SimpleTexture partnerScreen = mPartner.GetPartnerScreen(characterIndex);
		GameObject parentBuf = partnerScreen.transform.parent.gameObject;
		Vector3 posLow = new Vector3(0f, -284f, 0f);
		Vector3 posHigh = new Vector3(0f, 700f, 0f);
		Vector3 posHighHigh = new Vector3(0f, 956f, 0f);
		CompanionData data = mPartner.mData;
		if (EffectSettings[(int)mMode].enableEnterJump)
		{
			mPartner.Jump(characterIndex);
			switch (data.GetSkillFadeType(0, rank, characterIndex))
			{
			case 1:
				yield return StartCoroutine(CoFadeOutPartnerScreen(Vector3.zero, 4f / 15f, characterIndex));
				break;
			case 2:
				yield return StartCoroutine(CoJumpUpFadeOutPartnerScreen(Vector3.zero, posHigh / 2f, 4f / 15f, characterIndex));
				break;
			default:
				yield return StartCoroutine(CoJumpUpPartnerScreen(Vector3.zero, posHigh, 4f / 15f, characterIndex));
				break;
			}
			yield return new WaitForSeconds(11f / 15f);
		}
		else
		{
			mPartner.SetPartnerScreenOffset(posHigh, characterIndex);
			yield return new WaitForSeconds(1f / 6f);
			mPartner.SetPartnerScreenEnable(true, characterIndex);
		}
		mPartnerForeground = true;
		mPartner.EnableUIShadow(false);
		mPartner.Skill(rank, characterIndex);
		float scaleBuf = mPartner.GetPartnerScreenScale(characterIndex);
		mPartner.SetPartnerScreenParent(mParentObject, characterIndex);
		mPartner.SetPartnerScreenScale(1.2f, characterIndex);
		mPartner.SetPartnerScreenLayer(mParentObject.gameObject.layer, characterIndex);
		switch (data.GetSkillFadeType(1, rank, characterIndex))
		{
		case 1:
			yield return StartCoroutine(CoFadeInPartnerScreen(posLow, data.GetSkillJumpDownTime(rank, characterIndex), characterIndex));
			break;
		case 2:
			yield return StartCoroutine(CoJumpDownFadeInPartnerScreen(posHighHigh / 2f, posLow, data.GetSkillJumpDownTime(rank, characterIndex), characterIndex));
			break;
		default:
			yield return StartCoroutine(CoJumpDownPartnerScreen(posHighHigh, posLow, data.GetSkillJumpDownTime(rank, characterIndex), characterIndex));
			break;
		}
		yield return new WaitForSeconds(data.GetSkillActionTime(rank, characterIndex));
		switch (data.GetSkillFadeType(2, rank, characterIndex))
		{
		case 1:
			yield return StartCoroutine(CoFadeOutPartnerScreen(posLow, data.GetSkillJumpUpTime(rank, characterIndex), characterIndex));
			break;
		case 2:
			yield return StartCoroutine(CoJumpUpFadeOutPartnerScreen(posLow, posHighHigh / 2f, data.GetSkillJumpUpTime(rank, characterIndex), characterIndex));
			break;
		default:
			yield return StartCoroutine(CoJumpUpPartnerScreen(posLow, posHighHigh, data.GetSkillJumpUpTime(rank, characterIndex), characterIndex));
			break;
		}
		while (!mPartner.IsMotionFinished(characterIndex))
		{
			yield return 0;
		}
		mPartnerForeground = false;
		mPartner.EnableUIShadow(true);
		mPartner.SetPartnerScreenParent(parentBuf, characterIndex);
		mPartner.SetPartnerScreenLayer(parentBuf.layer, characterIndex);
		mPartner.SetPartnerScreenScale(scaleBuf, characterIndex);
		if (EffectSettings[(int)mMode].enableExitLanding)
		{
			mPartner.Landing(characterIndex);
			switch (data.GetSkillFadeType(3, rank, characterIndex))
			{
			case 1:
				yield return StartCoroutine(CoFadeInPartnerScreen(Vector3.zero, 4f / 15f, characterIndex));
				break;
			case 2:
				yield return StartCoroutine(CoJumpDownFadeInPartnerScreen(posHigh / 2f, Vector3.zero, 4f / 15f, characterIndex));
				break;
			default:
				yield return StartCoroutine(CoJumpDownPartnerScreen(posHigh, Vector3.zero, 4f / 15f, characterIndex));
				break;
			}
		}
		else
		{
			mPartner.SetPartnerScreenEnable(false, characterIndex);
		}
		mSkillMotionFinished[characterIndex] = true;
	}

	private IEnumerator CoJumpDownPartnerScreen(Vector3 fromPos, Vector3 toPos, float time, int characterIndex)
	{
		mPartner.SetPartnerScreenColor(Color.white, characterIndex);
		Vector3 distance = toPos - fromPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = 1f - Mathf.Sin((angle + 90f) * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenOffset(fromPos + distance * ratio, characterIndex);
			yield return 0;
		}
	}

	private IEnumerator CoJumpUpPartnerScreen(Vector3 fromPos, Vector3 toPos, float time, int characterIndex)
	{
		yield return new WaitForSeconds(1f / 6f);
		Vector3 distance = toPos - fromPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenOffset(fromPos + distance * ratio, characterIndex);
			yield return 0;
		}
	}

	private IEnumerator CoFadeOutPartnerScreen(Vector3 pos, float time, int characterIndex)
	{
		mPartner.SetPartnerScreenOffset(pos, characterIndex);
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Cos(angle * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenColor(new Color(1f, 1f, 1f, alpha), characterIndex);
			yield return 0;
		}
	}

	private IEnumerator CoFadeInPartnerScreen(Vector3 pos, float time, int characterIndex)
	{
		yield return new WaitForSeconds(1f / 6f);
		mPartner.SetPartnerScreenOffset(pos, characterIndex);
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenColor(new Color(1f, 1f, 1f, alpha), characterIndex);
			yield return 0;
		}
	}

	private IEnumerator CoJumpUpFadeOutPartnerScreen(Vector3 fromPos, Vector3 toPos, float time, int characterIndex)
	{
		Vector3 distance = toPos - fromPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Cos(angle * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenColor(new Color(1f, 1f, 1f, alpha), characterIndex);
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenOffset(fromPos + distance * ratio, characterIndex);
			yield return 0;
		}
	}

	private IEnumerator CoJumpDownFadeInPartnerScreen(Vector3 fromPos, Vector3 toPos, float time, int characterIndex)
	{
		yield return new WaitForSeconds(1f / 6f);
		Vector3 distance = toPos - fromPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenColor(new Color(1f, 1f, 1f, alpha), characterIndex);
			float ratio = 1f - Mathf.Sin((angle + 90f) * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenOffset(fromPos + distance * ratio, characterIndex);
			yield return 0;
		}
	}
}
