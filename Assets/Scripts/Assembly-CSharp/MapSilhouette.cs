using System.Collections.Generic;
using UnityEngine;

public class MapSilhouette : MapEntity
{
	public string UniqueID = string.Empty;

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public virtual void Init(int _counter, string _name, BIJImage atlas, float x, float y, float z, Vector3 scale, string _pngname)
	{
		DefaultAnimeKey = _name;
		AnimeKey = _name;
		mAtlas = atlas;
		mScale = scale;
		UniqueID = _pngname;
		mChangeParts = new Dictionary<string, string>();
		mChangeParts.Add("char", "char_" + _pngname);
		PlayAnime(AnimeKey, true);
		base._pos = new Vector3(x, y, z);
	}

	public virtual void InitEpisode(int _counter, string _name, BIJImage atlas, float x, float y, float z, Vector3 scale, string episodeNo)
	{
		DefaultAnimeKey = _name;
		AnimeKey = _name;
		mAtlas = atlas;
		mScale = scale;
		UniqueID = episodeNo;
		mChangeParts = new Dictionary<string, string>();
		mChangeParts.Add("char", "episode_icon00");
		PlayAnime(AnimeKey, true);
		base._pos = new Vector3(x, y, z);
	}
}
