using System;
using System.Collections.Generic;
using UnityEngine;

public class JellyTextureButton : MonoBehaviour
{
	public enum STATE
	{
		NONE = 0,
		ON = 1,
		OFF = 2,
		DISABLE_ON = 3,
		DISABLE_OFF = 4,
		OPEN = 5,
		CLOSE = 6
	}

	public UITexture mTexture;

	private STATE mEffect;

	protected float mEffectAngle;

	private bool mEnable = true;

	private float mDelaySec;

	private float mBaseScale;

	private List<EventDelegate> mOnClick;

	private GameMain mGame;

	protected float mInitEffectAngle;

	protected float mAngleScale = 1f;

	public STATE EffectState
	{
		get
		{
			return mEffect;
		}
	}

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mTexture = GetComponent<UITexture>();
		Util.SetTextureButtonCallback(mTexture, this, "OnButtonPress", UIButtonMessage.Trigger.OnPress);
		Util.SetTextureButtonCallback(mTexture, this, "OnButtonRelease", UIButtonMessage.Trigger.OnRelease);
		mBaseScale = 1f;
	}

	protected virtual void Start()
	{
		mEffect = STATE.NONE;
		mEffectAngle = 0f;
		mInitEffectAngle = 0f;
		mDelaySec = 0f;
		UIEventTrigger component = mTexture.gameObject.GetComponent<UIEventTrigger>();
		mOnClick = new List<EventDelegate>(component.onClick);
		component.onClick.Clear();
		Util.SetTextureButtonCallback(mTexture, this, "OnButtonClick", UIButtonMessage.Trigger.OnClick);
	}

	protected virtual void Update()
	{
		switch (mEffect)
		{
		case STATE.NONE:
			break;
		case STATE.ON:
		{
			mEffectAngle += Time.deltaTime * 720f * mAngleScale;
			if (mEffectAngle >= 90f)
			{
				mEffectAngle = 90f;
				mEffect = STATE.NONE;
			}
			float num2 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			mTexture.gameObject.transform.localScale = new Vector3(mBaseScale - num2 * 0.1f, mBaseScale - num3 * 0.1f, 1f);
			break;
		}
		case STATE.OFF:
		{
			mEffectAngle += Time.deltaTime * 1440f;
			if (mEffectAngle >= 90f)
			{
				mEffectAngle = 90f;
				mEffect = STATE.NONE;
			}
			float num2 = Mathf.Cos(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = Mathf.Cos(mEffectAngle * ((float)Math.PI / 180f));
			mTexture.gameObject.transform.localScale = new Vector3(mBaseScale - num2 * 0.1f, mBaseScale - num3 * 0.1f, 1f);
			break;
		}
		case STATE.DISABLE_ON:
		{
			mEffectAngle += Time.deltaTime * 1440f * mAngleScale;
			if (mEffectAngle >= 90f)
			{
				mEffectAngle = 90f;
				mEffect = STATE.NONE;
			}
			float num2 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			mTexture.gameObject.transform.localScale = new Vector3(mBaseScale - num2 * 0.1f, mBaseScale - num3 * 0.1f, 1f);
			break;
		}
		case STATE.DISABLE_OFF:
		{
			mEffectAngle += Time.deltaTime * 1440f;
			if (mEffectAngle >= 90f)
			{
				mEffectAngle = 90f;
				mEffect = STATE.NONE;
			}
			float num2 = 1f - Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = 1f - Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			mTexture.gameObject.transform.localScale = new Vector3(mBaseScale - num2 * 0.1f, mBaseScale - num3 * 0.1f, 1f);
			break;
		}
		case STATE.OPEN:
		{
			if (mDelaySec > 0f)
			{
				mDelaySec -= Time.deltaTime;
				if (!(mDelaySec <= 0f))
				{
					break;
				}
				SetVisible(true);
			}
			mEffectAngle += Time.deltaTime * 1800f;
			if (mEffectAngle >= 540f)
			{
				mEffectAngle = 540f;
				mEffect = STATE.NONE;
			}
			float num = 0.2f - 0.00037037037f * mEffectAngle;
			float num2 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			mTexture.transform.localScale = new Vector3(mBaseScale + num2 * num, mBaseScale + num3 * num, 1f);
			break;
		}
		case STATE.CLOSE:
		{
			if (mDelaySec > 0f)
			{
				mDelaySec -= Time.deltaTime;
				break;
			}
			mEffectAngle -= Time.deltaTime * 1800f;
			if (mEffectAngle <= 0f)
			{
				mEffectAngle = 0f;
				SetVisible(false);
				mEffect = STATE.NONE;
			}
			float num = 0.2f - 0.00037037037f * mEffectAngle;
			float num2 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			mTexture.transform.localScale = new Vector3(mBaseScale + num2 * num, mBaseScale + num3 * num, 1f);
			break;
		}
		}
	}

	public void Init()
	{
	}

	public void Shake()
	{
		mEffect = STATE.OFF;
		mEffectAngle = mInitEffectAngle;
	}

	public void OnButtonPress()
	{
		if ((!mGame.mTutorialManager.IsTutorialPlaying() || mGame.mTutorialManager.IsAllowed(base.gameObject)) && !IsOpenClose())
		{
			if (mEnable)
			{
				mEffect = STATE.ON;
			}
			else
			{
				mEffect = STATE.DISABLE_ON;
			}
			mEffectAngle = mInitEffectAngle;
		}
	}

	public void OnButtonRelease()
	{
		if ((!mGame.mTutorialManager.IsTutorialPlaying() || mGame.mTutorialManager.IsAllowed(base.gameObject)) && !IsOpenClose())
		{
			if (mEnable)
			{
				mEffect = STATE.OFF;
			}
			else
			{
				mEffect = STATE.DISABLE_OFF;
			}
			mEffectAngle = 0f;
		}
	}

	public void OnButtonClick()
	{
		if (mGame.mTutorialManager.IsTutorialPlaying())
		{
			if (mGame.mTutorialManager.IsAllowed(base.gameObject))
			{
				EventDelegate.Execute(mOnClick);
			}
		}
		else
		{
			EventDelegate.Execute(mOnClick);
		}
	}

	public void SetButtonEnable(bool enable)
	{
		mEnable = enable;
		if (enable)
		{
			mTexture.color = new Color(1f, 1f, 1f, 1f);
		}
		else
		{
			mTexture.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		}
	}

	public bool IsEnable()
	{
		return mEnable;
	}

	public void SetEnable(bool enable)
	{
		mEnable = enable;
	}

	public void Open(float delaySec)
	{
		mEffect = STATE.OPEN;
		mEffectAngle = mInitEffectAngle;
		mDelaySec = delaySec;
	}

	public void Close(float delaySec)
	{
		mEffect = STATE.CLOSE;
		mEffectAngle = 540f;
		mDelaySec = delaySec;
	}

	public bool IsOpenClose()
	{
		if (mEffect == STATE.OPEN || mEffect == STATE.CLOSE)
		{
			return true;
		}
		return false;
	}

	public bool IsNone()
	{
		if (mEffect == STATE.NONE)
		{
			return true;
		}
		return false;
	}

	public void SetVisible(bool visible)
	{
		mTexture.enabled = visible;
	}

	public void SetBaseScale(float scale)
	{
		mBaseScale = scale;
	}
}
