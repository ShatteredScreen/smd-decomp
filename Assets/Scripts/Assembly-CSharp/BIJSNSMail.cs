using System.Collections;
using System.IO;
using UnityEngine;

public class BIJSNSMail : BIJSNSAppSwitcher
{
	private const float ATTACHEMENT_RESIZE_SCALE = 0.5f;

	public override SNS Kind
	{
		get
		{
			return SNS.Mail;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		switch (type)
		{
		case 7:
		case 8:
		case 9:
			return true;
		default:
			return false;
		}
	}

	private IEnumerator BIJSNSMail_CommonProcess_Coroutine(int seqno, IBIJSNSPostData _data, Handler handler, object userdata, float scale)
	{
		yield return new WaitForSeconds(0.5f);
		string title = BIJSNS.ToSS(_data.Title);
		string message = BIJSNS.ToSS(_data.Message);
		string imageurl = BIJSNS.ToSS(_data.ImageURL);
		string filename = ((!string.IsNullOrEmpty(_data.FileName)) ? _data.FileName : "attachment.png");
		BIJSNS.dbg("@@@ SNS @@@ " + message);
		bool needDownload2 = false;
		string url = imageurl;
		if (string.IsNullOrEmpty(url))
		{
			needDownload2 = false;
		}
		else
		{
			if (!url.Contains("://"))
			{
				url = "file://" + url;
			}
			needDownload2 = ((!url.StartsWith("file:")) ? true : false);
		}
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSMail.CommonProcess ::: attachment=" + url + " scale=" + scale.ToString("0.000"));
		if (needDownload2)
		{
			string localPath = Path.Combine(Application.temporaryCachePath, filename);
			if (File.Exists(localPath))
			{
				File.Delete(localPath);
			}
			WWW www = new WWW(url);
			yield return www;
			Texture2D tex = www.texture;
			if (!IsValidTexture(ref tex))
			{
				url = string.Empty;
			}
			else
			{
				if (scale > 0f)
				{
					BIJTextureScale.Bilinear(newWidth: Mathf.FloorToInt((float)tex.width * scale), newHeight: Mathf.FloorToInt((float)tex.height * scale), tex: tex);
				}
				File.WriteAllBytes(localPath, tex.EncodeToPNG());
				url = ("file://" + localPath).Replace('\\', '/');
			}
		}
		bool ret = BIJUnity.invokeMailWithMessageAndImage(title, message, url);
		BIJSNS.log("SNSInvokeMailWithMessageAndImage: " + ret);
		if (!ret)
		{
			if (base.CurrentSeqNo == seqno)
			{
				base.CurrentRes = new Response(1, "Mail did not invoked", null);
				ChangeStep(STEP.COMM_RESULT);
			}
			SetSpawn(0);
		}
	}

	private void DoPostMessage(OP_TYPE type, IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSMail.DoPostMessage: " + type);
		if (spawn)
		{
			CallCallback(handler, new Response(1, "Mail already invoked.", null), userdata);
			return;
		}
		int num = PushReq((int)type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				SetSpawn(num);
			}
			base.surrogateMonoBehaviour.StartCoroutine(BIJSNSMail_CommonProcess_Coroutine(num, postdata, handler, userdata, 0.5f));
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "Mail not requested", null), userdata);
		}
	}

	public override void PostMessage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSMail.PostMessage");
		DoPostMessage(OP_TYPE.POSTMESSAGE, postdata, handler, userdata);
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSMail.UploadImage");
		DoPostMessage(OP_TYPE.UPLOADIMAGE, postdata, handler, userdata);
	}

	public override void Invite(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSMail.Invite");
		DoPostMessage(OP_TYPE.INVITE, postdata, handler, userdata);
	}
}
