using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMVerticalList : ListBase<SMVerticalListItem>
{
	public delegate void OnCreateItemDelegate(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem);

	public delegate void OnCreateFinishDelegate();

	protected const int STATE_INIT = 0;

	protected const int STATE_INIT_WAIT = 256;

	protected const int STATE_MAIN = 512;

	protected const int STATE_WAIT = 268435455;

	public OnCreateItemDelegate OnCreateItem = delegate
	{
	};

	public OnCreateFinishDelegate OnCreateFinish = delegate
	{
	};

	public Action OnBeforeCreate;

	public bool initilized;

	protected int gamestate;

	protected MonoBehaviour mMessageTaget;

	public List<SMVerticalListItem> items = new List<SMVerticalListItem>();

	protected float posX;

	protected float posY;

	protected GameMain mGame = SingletonMonoBehaviour<GameMain>.Instance;

	public SMVerticalListInfo listInfo;

	protected float LIST_WIDTH = 100f;

	protected float LIST_HEIGHT = 100f;

	public GameObject mTween;

	private UISprite mWallAttention;

	protected bool mIsScrollBarCreated;

	public bool ScroolBarHide;

	public bool portrait { protected get; set; }

	public bool EnableListUpdate { get; protected set; }

	public Vector3 ScrollBarPositionOffsetPortrait { private get; set; }

	public Vector3 ScrollBarPositionOffsetLandscape { private get; set; }

	public UIWidget.Pivot ScrollListPivot { private get; set; }

	public static SMVerticalList Make(GameObject _parent, MonoBehaviour _messageTaget, SMVerticalListInfo listInfo, List<SMVerticalListItem> _items, float _posX, float _posY, int _baseDepth, bool _portrait, UIScrollView.Movement _movement, bool _listupdate = false, bool ScrollHide = false, UIWidget.Pivot a_pivot = UIWidget.Pivot.Top)
	{
		GameObject gameObject = Util.CreateGameObject("SmVerticalList", _parent);
		SMVerticalList sMVerticalList = gameObject.AddComponent<SMVerticalList>();
		sMVerticalList.listInfo = listInfo;
		sMVerticalList.items = _items;
		sMVerticalList.posX = _posX;
		sMVerticalList.posY = _posY;
		sMVerticalList.mBaseDepth = _baseDepth;
		sMVerticalList.mMessageTaget = _messageTaget;
		sMVerticalList.portrait = _portrait;
		sMVerticalList.EnableListUpdate = _listupdate;
		sMVerticalList.movement = _movement;
		sMVerticalList.ScroolBarHide = ScrollHide;
		sMVerticalList.mGridPivot = a_pivot;
		if (sMVerticalList.portrait)
		{
			sMVerticalList.mMaxPerLine = listInfo.portrait_maxPerLine;
		}
		else
		{
			sMVerticalList.mMaxPerLine = listInfo.landscape_maxPerLine;
		}
		sMVerticalList.mMaxDisplayCount = listInfo.maxDisplayCount;
		return sMVerticalList;
	}

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	protected override int GetScrollingDeltaY()
	{
		return listInfo.scrollSpeed;
	}

	protected override int GetCellHeight()
	{
		return listInfo.itemHeight;
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void SetEnableAttention(bool flag)
	{
		if (!(mWallAttention == null))
		{
			NGUITools.SetActive(mWallAttention.gameObject, flag);
		}
	}

	protected void CreateWindow(GameObject parent, string name)
	{
		GameObject gameObject = Util.CreateGameObject("ListParentObject", parent);
		gameObject.transform.localPosition = new Vector3(posX, posY, 0f);
		mTween = gameObject;
		if (portrait)
		{
			LIST_WIDTH = listInfo.portrait_width;
			LIST_HEIGHT = listInfo.portrait_height;
		}
		else
		{
			LIST_WIDTH = listInfo.landscape_width;
			LIST_HEIGHT = listInfo.landscape_height;
		}
		mWindowRect = new Rect(posX - LIST_WIDTH / 2f, posY - LIST_HEIGHT / 2f, LIST_WIDTH, LIST_HEIGHT);
	}

	protected override void CreateContents(GameObject parent, string name)
	{
		CreateList(parent, "List");
		mListScrollView.dragEffect = UIScrollView.DragEffect.MomentumAndSpring;
		mListScrollView.restrictWithinPanel = true;
		mCenterOnChild.enabled = false;
	}

	public void ScrollCustomStop()
	{
		mListScrollView.enabled = false;
	}

	public void ScrollCustomMove()
	{
		mListScrollView.enabled = true;
	}

	protected override void CreateScrollBar(GameObject parent, string name)
	{
		GameObject gameObject = Util.CreateGameObject("Scroll", parent);
		mScroll = gameObject.AddComponent<UIScrollBar>();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		int num = 0;
		if (ScroolBarHide)
		{
			return;
		}
		Vector3 localPosition;
		if (movement == UIScrollView.Movement.Vertical)
		{
			float num2 = 14f;
			localPosition = new Vector3(LIST_WIDTH / 2f + num2, 0f, 0f);
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				localPosition += ScrollBarPositionOffsetLandscape;
			}
			else if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				localPosition += ScrollBarPositionOffsetPortrait;
			}
			num = (int)LIST_HEIGHT;
		}
		else
		{
			float num3 = 62f;
			localPosition = new Vector3(0f, (0f - LIST_HEIGHT) / 2f - num3, 0f);
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				localPosition += ScrollBarPositionOffsetLandscape;
			}
			else if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				localPosition += ScrollBarPositionOffsetPortrait;
			}
			num = (int)LIST_WIDTH;
		}
		mScroll.gameObject.transform.localPosition = localPosition;
		UISprite uISprite = Util.CreateSprite("scrollCursor", gameObject, image);
		Util.SetSpriteInfo(uISprite, "scrollbar01", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 20;
		uISprite.height = num;
		UISprite uISprite2 = Util.CreateSprite("scrollBack", gameObject, image);
		Util.SetSpriteInfo(uISprite2, "scrollbar00", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.width = 20;
		uISprite2.height = num;
		if (movement == UIScrollView.Movement.Horizontal)
		{
			uISprite.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			uISprite2.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
		}
		mScroll.fillDirection = UIProgressBar.FillDirection.TopToBottom;
		mScroll.foregroundWidget = uISprite;
		mScroll.backgroundWidget = uISprite2;
		mIsScrollBarCreated = true;
	}

	public float Scroll()
	{
		if (mScroll != null)
		{
			return mScroll.value;
		}
		return 0f;
	}

	public void ScrollSet(float value)
	{
		if (mScroll != null)
		{
			mScroll.value = value;
		}
	}

	public void HideScrollBar()
	{
		ScroolBarHide = true;
		if (mIsScrollBarCreated && mScroll != null)
		{
			NGUITools.SetActive(mScroll.gameObject, false);
		}
	}

	public void CreateScrollBarForce()
	{
		ScroolBarHide = false;
		if (mIsScrollBarCreated)
		{
			NGUITools.SetActive(mScroll.gameObject, true);
			return;
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		int num = 0;
		Vector3 localPosition;
		if (movement == UIScrollView.Movement.Vertical)
		{
			float num2 = 14f;
			localPosition = new Vector3(LIST_WIDTH / 2f + num2, 0f, 0f);
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				localPosition += ScrollBarPositionOffsetLandscape;
			}
			else if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				localPosition += ScrollBarPositionOffsetPortrait;
			}
			num = (int)LIST_HEIGHT;
		}
		else
		{
			float num3 = 62f;
			localPosition = new Vector3(0f, (0f - LIST_HEIGHT) / 2f - num3, 0f);
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				localPosition += ScrollBarPositionOffsetLandscape;
			}
			else if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				localPosition += ScrollBarPositionOffsetPortrait;
			}
			num = (int)LIST_WIDTH;
		}
		mScroll.gameObject.transform.localPosition = localPosition;
		UISprite uISprite = Util.CreateSprite("scrollCursor", mScroll.gameObject, image);
		Util.SetSpriteInfo(uISprite, "scrollbar01", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 20;
		uISprite.height = num;
		UISprite uISprite2 = Util.CreateSprite("scrollBack", mScroll.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "scrollbar00", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.width = 20;
		uISprite2.height = num;
		if (movement == UIScrollView.Movement.Horizontal)
		{
			uISprite.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			uISprite2.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
		}
		mScroll.fillDirection = UIProgressBar.FillDirection.TopToBottom;
		mScroll.foregroundWidget = uISprite;
		mScroll.backgroundWidget = uISprite2;
		mIsScrollBarCreated = true;
	}

	protected override void CreateList(GameObject parent, string name)
	{
		base.CreateList(parent, name);
		mListScrollView.contentPivot = ScrollListPivot;
	}

	protected override void CreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		OnCreateItem(itemGO, index, item, lastItem);
	}

	protected IEnumerator UpdateMenuAttention(UISprite MenuAttention)
	{
		float counter = 0f;
		float basePosY = MenuAttention.transform.localPosition.y;
		while (!(MenuAttention == null))
		{
			counter += Time.deltaTime * 360f;
			if (counter > 720f)
			{
				counter -= 720f;
			}
			Vector3 pos = MenuAttention.transform.localPosition;
			if (counter < 180f)
			{
				pos.y = basePosY + Mathf.Sin(counter * ((float)Math.PI / 180f)) * 30f;
			}
			else
			{
				pos.y = basePosY;
			}
			MenuAttention.transform.localPosition = pos;
			yield return null;
		}
	}

	public override void Update()
	{
		switch (gamestate)
		{
		case 0:
			ResetContents();
			CreateWindow(base.gameObject, "Window");
			CreateContents(mTween, "Contents");
			UpdateList();
			if (OnBeforeCreate != null)
			{
				OnBeforeCreate();
			}
			initilized = true;
			gamestate = 256;
			break;
		case 256:
			OnScrollAdjust();
			gamestate = 512;
			if (OnCreateFinish != null)
			{
				OnCreateFinish();
			}
			break;
		}
		if (EnableListUpdate && items.Count > 0)
		{
			for (int i = 0; i < items.Count; i++)
			{
				items[i].Update();
			}
		}
	}

	public void OnScreenChanged(bool a_portrait)
	{
		if (a_portrait)
		{
			mMaxPerLine = listInfo.portrait_maxPerLine;
			LIST_WIDTH = listInfo.portrait_width;
			LIST_HEIGHT = listInfo.portrait_height;
		}
		else
		{
			mMaxPerLine = listInfo.landscape_maxPerLine;
			LIST_WIDTH = listInfo.landscape_width;
			LIST_HEIGHT = listInfo.landscape_height;
		}
		mWindowRect = new Rect(posX - LIST_WIDTH / 2f, posY - LIST_HEIGHT / 2f, LIST_WIDTH, LIST_HEIGHT);
		float width = mWindowRect.width;
		float height = mWindowRect.height;
		mListPanel.baseClipRegion = new Vector4(0f, 0f, width, height);
		if (IsHorizontal())
		{
			mScrollPage = width;
		}
		else
		{
			mScrollPage = height;
		}
		mList.maxPerLine = mMaxPerLine;
		if (IsHorizontal())
		{
			mList.arrangement = UIGrid.Arrangement.Vertical;
			mList.cellWidth = GetCellHeight();
			mList.cellHeight = mListPanel.finalClipRegion.w / (float)mMaxPerLine;
		}
		else
		{
			mList.arrangement = UIGrid.Arrangement.Horizontal;
			mList.cellWidth = mListPanel.finalClipRegion.z / (float)mMaxPerLine;
			mList.cellHeight = GetCellHeight();
		}
		mList.keepWithinPanel = true;
		mList.Reposition();
		if (!(mScroll != null))
		{
			return;
		}
		int num = 0;
		Vector3 localPosition;
		if (movement == UIScrollView.Movement.Vertical)
		{
			float num2 = 14f;
			localPosition = new Vector3(LIST_WIDTH / 2f + num2, 0f, 0f);
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				localPosition += ScrollBarPositionOffsetLandscape;
			}
			else if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				localPosition += ScrollBarPositionOffsetPortrait;
			}
			num = (int)LIST_HEIGHT;
		}
		else
		{
			float num3 = 62f;
			localPosition = new Vector3(0f, (0f - LIST_HEIGHT) / 2f - num3, 0f);
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				localPosition += ScrollBarPositionOffsetLandscape;
			}
			else if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				localPosition += ScrollBarPositionOffsetPortrait;
			}
			num = (int)LIST_WIDTH;
		}
		mScroll.gameObject.transform.localPosition = localPosition;
		if (mScroll.foregroundWidget != null)
		{
			mScroll.foregroundWidget.height = num;
			if (movement == UIScrollView.Movement.Horizontal)
			{
				mScroll.foregroundWidget.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			}
			else
			{
				mScroll.foregroundWidget.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			}
		}
		if (mScroll.backgroundWidget != null)
		{
			mScroll.backgroundWidget.height = num;
			if (movement == UIScrollView.Movement.Horizontal)
			{
				mScroll.backgroundWidget.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			}
			else
			{
				mScroll.backgroundWidget.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			}
		}
		if (IsHorizontal())
		{
			mListScrollView.horizontalScrollBar = mScroll;
		}
		else
		{
			mListScrollView.verticalScrollBar = mScroll;
		}
	}

	public void OnScreenChanged(bool a_portrait, float a_x, float a_y)
	{
		mTween.transform.localPosition = new Vector3(a_x, a_y, 0f);
		OnScreenChanged(a_portrait);
	}

	protected void ResetContents()
	{
		if (mTween != null && mTween.gameObject != null)
		{
			GameMain.SafeDestroy(mTween.gameObject);
			mTween = null;
		}
	}

	public void Reconstruct()
	{
		gamestate = 0;
	}

	public bool CanOperation()
	{
		return gamestate == 512;
	}

	public void CenterOn(GameObject a_go)
	{
		if (!(mCenterOnChild == null) && !(a_go == null))
		{
			mCenterOnChild.enabled = true;
			mCenterOnChild.CenterOn(a_go.transform);
		}
	}

	public void CenterOffEnabled()
	{
		if (!(mCenterOnChild == null))
		{
			mCenterOnChild.enabled = false;
		}
	}

	public void SetCenterOnCallBack(SpringPanel.OnFinished callback)
	{
		if (!(mCenterOnChild == null))
		{
			mCenterOnChild.onFinished = callback;
		}
	}

	public SMVerticalListItem FindListItem(GameObject go)
	{
		int num = FindListIndexOf(go);
		if (num < 0 || items.Count <= num)
		{
			return null;
		}
		return items[num];
	}

	public void RemoveListItem(SMVerticalListItem item)
	{
		items.Remove(item);
		Reconstruct();
	}

	public void ClearListItem()
	{
		items.Clear();
	}

	protected override List<SMVerticalListItem> GetListItem()
	{
		return items;
	}

	public void RemoveListItem(int index)
	{
		foreach (KeyValuePair<GameObject, int> mMapItem in mMapItems)
		{
			if (mMapItem.Value == index)
			{
				mList.RemoveChild(mMapItem.Key.transform);
				mList.enabled = true;
				mList.Reposition();
				UnityEngine.Object.Destroy(mMapItem.Key);
				mMapItems.Remove(mMapItem.Key);
				break;
			}
		}
	}

	public int GetListItemCount()
	{
		return (mMapItems != null) ? mMapItems.Count : 0;
	}

	public void SetScrollBarPosition(Vector3 a_pos)
	{
		if (mScroll != null)
		{
			mScroll.gameObject.transform.localPosition = a_pos;
		}
	}

	public void SetScrollBarDisplay(bool active = true)
	{
		if (mScroll != null)
		{
			NGUITools.SetActive(mScroll.gameObject, active);
		}
	}
}
