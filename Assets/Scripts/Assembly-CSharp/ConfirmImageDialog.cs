using System.Collections;
using UnityEngine;

public class ConfirmImageDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum CONFIRM_IMAGE_DIALOG_STYLE
	{
		OK_ONLY = 0,
		CLOSE_ONLY = 1,
		YES_NO = 2,
		TROPHY = 3,
		CONSUME = 4
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	protected SELECT_ITEM mSelectItem;

	protected CONFIRM_IMAGE_DIALOG_STYLE mConfirmStyle;

	protected DIALOG_SIZE mDialogSize;

	protected string mTitle;

	protected string mDescriptionA;

	protected string mDescriptionB;

	protected bool mDescBoff;

	protected string mImage;

	public string mImageAtlasKey = "HUD";

	protected UISprite mBoardSprite;

	protected UILabel LabelTop;

	protected UILabel LabelBot;

	protected int mSpecing = 6;

	protected UISprite mImagesprite;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(mDialogSize);
		InitDialogTitle(mTitle);
		mBoardSprite = Util.CreateSprite("BoosterBoard", base.gameObject, image);
		Util.SetSpriteInfo(mBoardSprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 27f, 0f), Vector3.one, false);
		mBoardSprite.SetDimensions(510, 104);
		mBoardSprite.type = UIBasicSprite.Type.Sliced;
		float y = 0f;
		float y2 = -280f;
		float y3 = 0f;
		switch (mDialogSize)
		{
		case DIALOG_SIZE.SMALL:
			y = -10f;
			y2 = -130f;
			break;
		case DIALOG_SIZE.MEDIUM:
			y = 90f;
			y3 = -55f;
			y2 = -117f;
			break;
		case DIALOG_SIZE.LARGE:
			y = -180f;
			y2 = -205f;
			break;
		}
		LabelTop = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(LabelTop, mDescriptionA, mBaseDepth + 4, new Vector3(0f, y, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(LabelTop);
		LabelTop.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		LabelTop.overflowMethod = UILabel.Overflow.ResizeHeight;
		LabelTop.spacingY = mSpecing;
		BIJImage image2 = ResourceManager.LoadImage(mImageAtlasKey).Image;
		mImagesprite = Util.CreateSprite("IconImage", mBoardSprite.gameObject, image2);
		Util.SetSpriteInfo(mImagesprite, mImage, mBaseDepth + 4, new Vector3(0f, 1f, 0f), Vector3.one, false);
		if (!mDescBoff)
		{
			LabelBot = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(LabelBot, mDescriptionB, mBaseDepth + 4, new Vector3(0f, y3, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect(LabelBot);
			LabelBot.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
			LabelBot.overflowMethod = UILabel.Overflow.ResizeHeight;
			LabelBot.spacingY = mSpecing;
			LabelBot.color = Color.red;
		}
		switch (mConfirmStyle)
		{
		case CONFIRM_IMAGE_DIALOG_STYLE.OK_ONLY:
		case CONFIRM_IMAGE_DIALOG_STYLE.CLOSE_ONLY:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			string text3 = "LC_button_yes";
			if (mConfirmStyle == CONFIRM_IMAGE_DIALOG_STYLE.CLOSE_ONLY)
			{
				text3 = "LC_button_close";
			}
			Util.SetImageButtonInfo(button, text3, text3, text3, mBaseDepth + 4, new Vector3(0f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_IMAGE_DIALOG_STYLE.TROPHY:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			string text2 = "LC_button_yes";
			mBoardSprite.transform.localPosition = new Vector3(0f, 70f, 0f);
			LabelTop.transform.localPosition = new Vector3(0f, -16f, 0f);
			LabelBot.transform.localPosition = new Vector3(0f, -55f, 0f);
			Util.SetImageButtonInfo(button, text2, text2, text2, mBaseDepth + 4, new Vector3(0f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_IMAGE_DIALOG_STYLE.CONSUME:
		{
			LabelTop.overflowMethod = UILabel.Overflow.ShrinkContent;
			LabelTop.SetDimensions(510, 215);
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			string text = "LC_button_close";
			mBoardSprite.transform.localPosition = new Vector3(0f, 145f, 0f);
			LabelTop.transform.localPosition = new Vector3(0f, -47f, 0f);
			UISprite uISprite = Util.CreateSprite("BoardSubSprite", LabelTop.gameObject, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite.SetDimensions(510, 220);
			uISprite.type = UIBasicSprite.Type.Sliced;
			Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(0f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		default:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		}
	}

	public void Init(string title, string descA, string descB, string image, CONFIRM_IMAGE_DIALOG_STYLE style, bool openSeEnable = true)
	{
		Init(title, descA, descB, image, style, DIALOG_SIZE.MEDIUM);
		SetOpenSeEnable(openSeEnable);
	}

	public void Init(string title, string descA, string descB, string image, CONFIRM_IMAGE_DIALOG_STYLE style, DIALOG_SIZE size)
	{
		mTitle = title;
		mDescriptionA = descA;
		mDescriptionB = descB;
		mConfirmStyle = style;
		mDialogSize = size;
		mImage = image;
	}

	public void ConsumeInit(string title, string descA, string image, CONFIRM_IMAGE_DIALOG_STYLE style, DIALOG_SIZE size, int Specing = 6)
	{
		mTitle = title;
		mDescriptionA = descA;
		mConfirmStyle = style;
		mDialogSize = size;
		mImage = image;
		mSpecing = Specing;
		mDescBoff = true;
	}

	public virtual IEnumerator CloseProcess()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
		yield return null;
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetAtlasImage(string a_atlasKey, string a_imageName)
	{
		mImageAtlasKey = a_atlasKey;
		mImage = a_imageName;
	}
}
