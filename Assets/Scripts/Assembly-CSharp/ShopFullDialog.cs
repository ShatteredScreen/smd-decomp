using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class ShopFullDialog : DialogBase
{
	private enum LOAD_ATLAS
	{
		SHOP = 0,
		MAIL_BOX = 1,
		HUD = 2
	}

	private enum TRANSFORM_CACHE
	{
		HEADER = 0,
		FOOTER = 1,
		BODY = 2,
		BACKGROUND = 3,
		TITLEBAR_R = 4,
		TITLEBAR_L = 5,
		TITLE = 6,
		BACKBUTTON = 7,
		TAB_SALE = 8,
		TAB_BOOSTER = 9,
		LIST_SALE = 10,
		LIST_BOOSTER = 11,
		BUYGEM = 12,
		NOWGEM = 13,
		AoSCT = 14,
		GEMNUM = 15
	}

	private enum INIT_TYPE
	{
		SELECT_TAB = 0,
		OPENED_DIALOG = 1
	}

	public enum LIST_TYPE
	{
		BOOSTER = 0,
		SALE = 1
	}

	public enum EVENT_STATE
	{
		AUTH_ERROR = 0
	}

	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		CLOSE = 2,
		WAIT = 3,
		WAIT_OPENED_DIALOG = 4,
		NETWORK_00 = 5,
		NETWORK_00_1 = 6,
		NETWORK_01 = 7,
		NETWORK_01_1 = 8,
		NETWORK_02 = 9,
		NETWORK_02_1 = 10,
		NETWORK_03 = 11,
		AUTHERROR_WAIT = 12
	}

	private struct Size
	{
		public int Width;

		public int Height;

		public static Size Zero
		{
			get
			{
				return new Size(0, 0);
			}
		}

		public Size(int width, int height)
		{
			Width = width;
			Height = height;
		}
	}

	private sealed class AtlasInfo : IDisposable
	{
		private bool mInit;

		private bool mForceNotUnload;

		private bool mDisposed;

		public ResImage Resource { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Resource == null) ? null : Resource.Image;
			}
		}

		public string Key { get; private set; }

		public AtlasInfo(string key)
		{
			Key = key;
		}

		~AtlasInfo()
		{
			Dispose(false);
		}

		public IEnumerator Load(bool not_unload = false)
		{
			if (Resource != null)
			{
				UnLoad();
			}
			if (Res.ResImageDict.ContainsKey(Key) && ResourceManager.IsLoaded(Key))
			{
				Resource = ResourceManager.LoadImage(Key);
				if (Resource != null)
				{
					yield break;
				}
			}
			Resource = ResourceManager.LoadImageAsync(Key);
			if (Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.INIT)
			{
				mInit = true;
				mForceNotUnload = not_unload;
			}
		}

		public void UnLoad()
		{
			if (mInit && !mForceNotUnload)
			{
				ResourceManager.UnloadImage(Key);
			}
			mInit = false;
		}

		public bool IsDone()
		{
			return !mInit || (mInit && Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.DONE);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	private sealed class ShopItemListItem : SMVerticalListItem
	{
		public ShopItemData Data { get; private set; }

		public int ResaleCount { get; private set; }

		public int ShopCount { get; private set; }

		private long StartTime { get; set; }

		private long EndTime { get; set; }

		public ShopItemListItem(ShopLimitedTime shop)
		{
			Data = SingletonMonoBehaviour<GameMain>.Instance.mShopItemData[shop.ShopItemID];
			ResaleCount = shop.ResaleCount;
			ShopCount = shop.ShopCount;
			StartTime = shop.StartTime;
			EndTime = shop.EndTime;
		}

		public bool Equals(ShopLimitedTime shop)
		{
			if (shop == null || Data == null)
			{
				return false;
			}
			if (shop.ShopItemID != Data.Index)
			{
				return false;
			}
			if (shop.ResaleCount != ResaleCount)
			{
				return false;
			}
			if (shop.ShopCount != ShopCount)
			{
				return false;
			}
			if (shop.StartTime != StartTime)
			{
				return false;
			}
			if (shop.EndTime != EndTime)
			{
				return false;
			}
			return true;
		}
	}

	private sealed class SaleListItemInfo
	{
		public UILabel RemainBuyName { get; set; }

		public UIButton BuyButton { get; set; }
	}

	private sealed class TransFormCacheComparer : IEqualityComparer<TRANSFORM_CACHE>
	{
		public bool Equals(TRANSFORM_CACHE x, TRANSFORM_CACHE y)
		{
			return x == y;
		}

		public int GetHashCode(TRANSFORM_CACHE arg)
		{
			return (int)arg;
		}
	}

	private sealed class LoadAtlasComparer : IEqualityComparer<LOAD_ATLAS>
	{
		public bool Equals(LOAD_ATLAS x, LOAD_ATLAS y)
		{
			return x == y;
		}

		public int GetHashCode(LOAD_ATLAS arg)
		{
			return (int)arg;
		}
	}

	private sealed class EventStateComparer : IEqualityComparer<EVENT_STATE>
	{
		public bool Equals(EVENT_STATE x, EVENT_STATE y)
		{
			return x == y;
		}

		public int GetHashCode(EVENT_STATE arg)
		{
			return (int)arg;
		}
	}

	private sealed class ListTypeComparer : IEqualityComparer<LIST_TYPE>
	{
		public bool Equals(LIST_TYPE x, LIST_TYPE y)
		{
			return x == y;
		}

		public int GetHashCode(LIST_TYPE arg)
		{
			return (int)arg;
		}
	}

	private sealed class InitTypeComparer : IEqualityComparer<INIT_TYPE>
	{
		public bool Equals(INIT_TYPE x, INIT_TYPE y)
		{
			return x == y;
		}

		public int GetHashCode(INIT_TYPE arg)
		{
			return (int)arg;
		}
	}

	private const float INNER_FRAME_FLUCT_HEIGHT = 192f;

	private const int RESALE_PARAM_OFFSET = 10000;

	private const float WIDE_SCREEN_BODY_PORTRAIT_OFFSET = -12.5f;

	private const int WIDE_SCREEN_BODY_HEIGHT_OFFSET = 100;

	private static readonly KeyValuePair<ScreenOrientation, float>[] LIST_ITEM_SCALE = new KeyValuePair<ScreenOrientation, float>[7]
	{
		new KeyValuePair<ScreenOrientation, float>(ScreenOrientation.Unknown, 1f),
		new KeyValuePair<ScreenOrientation, float>(ScreenOrientation.Portrait, 1f),
		new KeyValuePair<ScreenOrientation, float>(ScreenOrientation.PortraitUpsideDown, 1f),
		new KeyValuePair<ScreenOrientation, float>(ScreenOrientation.LandscapeLeft, 0.9125f),
		new KeyValuePair<ScreenOrientation, float>(ScreenOrientation.LandscapeRight, 0.9125f),
		new KeyValuePair<ScreenOrientation, float>(ScreenOrientation.LandscapeLeft, 0.9125f),
		new KeyValuePair<ScreenOrientation, float>(ScreenOrientation.AutoRotation, 1f)
	};

	private static readonly KeyValuePair<LIST_TYPE, Size>[] LIST_ITEM_SIZE_DELTA = new KeyValuePair<LIST_TYPE, Size>[2]
	{
		new KeyValuePair<LIST_TYPE, Size>(LIST_TYPE.BOOSTER, new Size(545, 240)),
		new KeyValuePair<LIST_TYPE, Size>(LIST_TYPE.SALE, new Size(540, 380))
	};

	private GameObject mGameObject;

	private ScreenOrientation mLayoutOrientation;

	private Dictionary<TRANSFORM_CACHE, Transform> mTransformCache = new Dictionary<TRANSFORM_CACHE, Transform>(new TransFormCacheComparer());

	private Dictionary<LOAD_ATLAS, AtlasInfo> mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>(new LoadAtlasComparer());

	private Dictionary<EVENT_STATE, UnityEvent> mEvent = new Dictionary<EVENT_STATE, UnityEvent>(new EventStateComparer());

	private Dictionary<LIST_TYPE, SMVerticalList> mShopItemList = new Dictionary<LIST_TYPE, SMVerticalList>(new ListTypeComparer());

	private Dictionary<LIST_TYPE, UIButton> mTab = new Dictionary<LIST_TYPE, UIButton>(new ListTypeComparer());

	private Dictionary<INIT_TYPE, bool> mInitFlags = new Dictionary<INIT_TYPE, bool>(new InitTypeComparer());

	private LIST_TYPE mSelectedTab = LIST_TYPE.SALE;

	private LinkBannerRequest mLinkBannerReq = new LinkBannerRequest();

	private List<LinkBannerInfo> mLinkBanner = new List<LinkBannerInfo>();

	private int mWebSaleDialogIndex = -1;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.NETWORK_00);

	private LIST_TYPE mDefaultSelectList;

	private List<Transform> mListItems = new List<Transform>();

	private Coroutine mRoutineRotateList;

	private List<Coroutine> mRoutineRotateLaceList = new List<Coroutine>();

	private Dictionary<LIST_TYPE, List<Collider>> mScrollItemCollider = new Dictionary<LIST_TYPE, List<Collider>>(new ListTypeComparer());

	private UISsSprite mLoadingAnimation;

	private int mDispGemCount = -1;

	private Dictionary<int, SaleListItemInfo> mSaleListItemDict = new Dictionary<int, SaleListItemInfo>();

	private float mWideScreenBodyPortraitOffset;

	private int mWideScreenBodyHeightOffset;

	private bool IsBusy
	{
		get
		{
			return GetBusy() || mState.GetStatus() != STATE.MAIN;
		}
	}

	protected GameObject GameObject
	{
		get
		{
			if (mGameObject == null)
			{
				mGameObject = base.gameObject;
			}
			return mGameObject;
		}
	}

	protected float Aspect
	{
		get
		{
			Vector2 vector = Util.LogScreenSize();
			Vector2 vector2 = ((!(vector.x > vector.y)) ? new Vector2(vector.y, vector.x) : vector);
			return vector2.y / (vector2.x / 16f * 9f);
		}
	}

	private float WideScreenBodyPoratraitOffset
	{
		get
		{
			if (mWideScreenBodyPortraitOffset == 0f && Util.IsWideScreen())
			{
				float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num2 = 0.5625f;
				float num3 = 0.4617052f;
				mWideScreenBodyPortraitOffset = (num2 - num) / (num2 - num3) * -12.5f;
			}
			return mWideScreenBodyPortraitOffset;
		}
	}

	private int WideScreenBodyHeightOffset
	{
		get
		{
			if (mWideScreenBodyHeightOffset == 0 && Util.IsWideScreen())
			{
				float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num2 = 0.5625f;
				float num3 = 0.4617052f;
				mWideScreenBodyHeightOffset = (int)((num2 - num) / (num2 - num3) * 100f);
			}
			return mWideScreenBodyHeightOffset;
		}
	}

	public override void Awake()
	{
		base.Awake();
		foreach (int value in Enum.GetValues(typeof(INIT_TYPE)))
		{
			mInitFlags.Add((INIT_TYPE)value, false);
		}
	}

	public override void Update()
	{
		base.Update();
		if (mState.IsChanged() && mTransformCache != null)
		{
			Transform transform = ((!mTransformCache.ContainsKey(TRANSFORM_CACHE.AoSCT)) ? null : mTransformCache[TRANSFORM_CACHE.AoSCT]);
			Transform transform2 = ((!mTransformCache.ContainsKey(TRANSFORM_CACHE.BACKBUTTON)) ? null : mTransformCache[TRANSFORM_CACHE.BACKBUTTON]);
			Transform transform3 = ((!mTransformCache.ContainsKey(TRANSFORM_CACHE.BUYGEM)) ? null : mTransformCache[TRANSFORM_CACHE.BUYGEM]);
			Collider collider = ((!(transform != null)) ? null : transform.GetComponent<Collider>());
			Collider collider2 = ((!(transform2 != null)) ? null : transform2.GetComponent<Collider>());
			Collider collider3 = ((!(transform3 != null)) ? null : transform3.GetComponent<Collider>());
			bool flag = mState.GetStatus() == STATE.MAIN;
			if (collider != null)
			{
				collider.enabled = flag;
			}
			if (collider2 != null)
			{
				collider2.enabled = flag;
			}
			if (collider3 != null)
			{
				collider3.enabled = flag;
			}
		}
		if (!GetBusy())
		{
			switch (mState.GetStatus())
			{
			case STATE.INIT:
				CreateGemWindow();
				mState.Change(STATE.WAIT_OPENED_DIALOG);
				break;
			case STATE.WAIT_OPENED_DIALOG:
				if (mInitFlags[INIT_TYPE.OPENED_DIALOG])
				{
					CreateList(true);
					mState.Change(STATE.MAIN);
				}
				break;
			case STATE.NETWORK_00:
				if (mGame.Network_UserAuth())
				{
					mState.Change(STATE.NETWORK_00_1);
				}
				else if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_00);
				}
				break;
			case STATE.NETWORK_00_1:
			{
				if (!GameMain.mUserAuthCheckDone)
				{
					break;
				}
				if (GameMain.mUserAuthSucceed)
				{
					if (!GameMain.mUserAuthTransfered)
					{
						mState.Change(STATE.NETWORK_01);
					}
					else
					{
						mState.Change(STATE.AUTHERROR_WAIT);
					}
					break;
				}
				Server.ErrorCode mSegmentProfileErrorCode = GameMain.mUserAuthErrorCode;
				if (mSegmentProfileErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 5, OnConnectRetry, OnConnectAbandon);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 5, OnConnectRetry, null);
				}
				mState.Change(STATE.WAIT);
				break;
			}
			case STATE.AUTHERROR_WAIT:
				ClearCallback(CALLBACK_STATE.OnCloseFinished);
				InvokeCallback(EVENT_STATE.AUTH_ERROR);
				Close();
				mState.Change(STATE.WAIT);
				break;
			case STATE.NETWORK_01:
				if (mGame.Network_GetGemCount())
				{
					mState.Change(STATE.NETWORK_01_1);
				}
				else if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_02);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
				break;
			case STATE.NETWORK_01_1:
				if (GameMain.mGetGemCountCheckDone)
				{
					if (GameMain.mGetGemCountSucceed)
					{
						mState.Change(STATE.NETWORK_02);
					}
					else
					{
						ShowNetworkErrorDialog(STATE.NETWORK_01);
					}
				}
				break;
			case STATE.NETWORK_02:
				if (mGame.Network_GetSegmentProfile(string.Empty, string.Empty))
				{
					mState.Change(STATE.NETWORK_02_1);
				}
				else if (GameMain.mSegmentProfileCheckDone)
				{
					mState.Change(STATE.NETWORK_03);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_02);
				}
				break;
			case STATE.NETWORK_02_1:
			{
				if (!GameMain.mSegmentProfileCheckDone)
				{
					break;
				}
				if (GameMain.mSegmentProfileSucceed)
				{
					mState.Change(STATE.NETWORK_03);
					break;
				}
				Server.ErrorCode mSegmentProfileErrorCode = GameMain.mSegmentProfileErrorCode;
				if (mSegmentProfileErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 9, OnConnectRetry, OnConnectAbandon);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_CLOSE, 9, OnConnectRetry, OnConnectAbandon);
				}
				mState.Change(STATE.WAIT);
				break;
			}
			case STATE.NETWORK_03:
			{
				if (!InternetReachability.IsEnable)
				{
					ShowNetworkErrorDialog(STATE.NETWORK_03);
					break;
				}
				mLinkBanner = null;
				LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerReq);
				mLinkBannerReq.SetHighPriority(true);
				List<LinkBannerDetail> header = new List<LinkBannerDetail>();
				List<LinkBannerDetail> unsorted = new List<LinkBannerDetail>();
				List<LinkBannerDetail> tail = new List<LinkBannerDetail>();
				Action<ShopLimitedTime[]> action = delegate(ShopLimitedTime[] fn_shop)
				{
					if (fn_shop != null)
					{
						foreach (ShopLimitedTime s2 in fn_shop)
						{
							if (!s2.InvisibleStoreList)
							{
								StringBuilder stringBuilder = new StringBuilder("sale_{0}_set");
								if (s2.ResaleCount != 0)
								{
									stringBuilder.Append("_").Append(s2.ResaleCount);
								}
								stringBuilder.Append(".png");
								LinkBannerDetail item = new LinkBannerDetail
								{
									Action = LinkBannerInfo.ACTION.NOP.ToString(),
									Param = s2.ShopItemID + s2.ResaleCount * 10000,
									FileName = (string.IsNullOrEmpty(s2.SaleBannerURL) ? string.Format(stringBuilder.ToString(), s2.ShopItemID.ToString("00")) : s2.SaleBannerURL)
								};
								if (!mGame.mPlayer.IsEnablePurchaseItem(s2.ShopItemID, s2.ShopItemLimit, s2.ShopCount))
								{
									tail.Add(item);
								}
								else if (mGame.SegmentProfile.ShopListOrder[1].Any((int n) => n == s2.ShopItemID))
								{
									header.Add(item);
								}
								else
								{
									unsorted.Add(item);
								}
							}
						}
					}
				};
				List<ShopLimitedTime> list = new List<ShopLimitedTime>();
				ShopLimitedTime s;
				foreach (ShopLimitedTime shopItem in mGame.SegmentProfile.ShopItemList)
				{
					s = shopItem;
					if (mGame.SegmentProfile.ShopSetItemList.Any((ShopLimitedTime n) => n.ShopItemID == s.ShopItemID) || mGame.SegmentProfile.ShopAllCrushItemList.Any((ShopLimitedTime n) => n.ShopItemID == s.ShopItemID) || mGame.SegmentProfile.ShopMugenHeartItemList.Any((ShopLimitedTime n) => n.ShopItemID == s.ShopItemID))
					{
						list.Add(s);
					}
				}
				action(list.ToArray());
				LinkBannerSetting linkBannerSetting = new LinkBannerSetting();
				linkBannerSetting.BannerList = new List<LinkBannerDetail>();
				if (mGame.SegmentProfile.ShopListOrder != null && mGame.SegmentProfile.ShopListOrder.Count > 1 && mGame.SegmentProfile.ShopListOrder[1] != null && mGame.SegmentProfile.ShopListOrder[1].Count > 0)
				{
					foreach (int item2 in mGame.SegmentProfile.ShopListOrder[1])
					{
						foreach (LinkBannerDetail item3 in header)
						{
							if (item3.Param != item2)
							{
								continue;
							}
							linkBannerSetting.BannerList.Add(item3);
							break;
						}
					}
				}
				else
				{
					foreach (LinkBannerDetail item4 in header)
					{
						linkBannerSetting.BannerList.Add(item4);
					}
				}
				linkBannerSetting.BannerList = linkBannerSetting.BannerList.Concat(unsorted.Concat(tail)).ToList();
				if (linkBannerSetting.BannerList.Count > 0)
				{
					if (mLinkBannerReq.mReceiveEveryTimeCallback == null)
					{
						mLinkBannerReq.mReceiveEveryTimeCallback = new LinkBannerRequest.OnReceiveEveryTimeCallback();
						mLinkBannerReq.mReceiveEveryTimeCallback.AddListener(delegate(LinkBannerInfo fn_info)
						{
							if (mLinkBanner == null)
							{
								mLinkBanner = new List<LinkBannerInfo>();
							}
							mLinkBanner.Add(fn_info);
						});
					}
					linkBannerSetting.BaseURL = mGame.SegmentProfile.ShopSaleBannerBaseURL;
					LinkBannerManager.Instance.GetLinkBannerRequest(mLinkBannerReq, linkBannerSetting, delegate
					{
					});
				}
				mState.Change(STATE.INIT);
				break;
			}
			}
		}
		mState.Update();
		UpdatePossessGem();
	}

	public void Init(LIST_TYPE default_list_type)
	{
		mDefaultSelectList = default_list_type;
	}

	public override IEnumerator BuildResource()
	{
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>(new LoadAtlasComparer());
		}
		foreach (KeyValuePair<LOAD_ATLAS, AtlasInfo> mAtla in mAtlas)
		{
			mAtla.Value.UnLoad();
		}
		mAtlas.Clear();
		foreach (int type in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			AtlasInfo info2 = new AtlasInfo(((LOAD_ATLAS)type).ToString());
			mAtlas.Add((LOAD_ATLAS)type, info2);
			StartCoroutine(info2.Load());
		}
		foreach (AtlasInfo info in mAtlas.Values)
		{
			while (!info.IsDone())
			{
				yield return null;
			}
		}
	}

	public override void BuildDialog()
	{
		string empty = string.Empty;
		Vector2 vector = Util.LogScreenSize();
		float num = vector.y * 0.5f;
		float num2 = vector.x * 0.5f;
		float speed = 15f;
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		UIFont atlasFont = GameMain.LoadFont();
		SetJellyTargetScale(1f);
		GameObject gameObject = Util.CreateGameObject("Header", GameObject);
		GameObject gameObject2 = Util.CreateGameObject("Footer", GameObject);
		GameObject gameObject3 = Util.CreateGameObject("Body", GameObject);
		mTransformCache.Add(TRANSFORM_CACHE.HEADER, gameObject.transform);
		mTransformCache.Add(TRANSFORM_CACHE.FOOTER, gameObject2.transform);
		mTransformCache.Add(TRANSFORM_CACHE.BODY, gameObject3.transform);
		UISprite uISprite = Util.CreateSprite("BackGround", GameObject, mAtlas[LOAD_ATLAS.MAIL_BOX].Image);
		Util.SetSpriteInfo(uISprite, "event_back", mBaseDepth, new Vector3(0f, 0f, 200f), Vector3.one, false, UIWidget.Pivot.Top);
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions(1386, 910);
		uISprite.SetLocalScale(1f, 1.5230769f, 1f);
		uISprite.SetLocalEulerAnglesZ(180f);
		mTransformCache.Add(TRANSFORM_CACHE.BACKGROUND, uISprite.transform);
		GameObject anchor = Util.CreateAnchorWithChild("CollectionAnchorBottomLeft", GameObject, UIAnchor.Side.BottomLeft, component).gameObject;
		mRoutineRotateLaceList.Add(StartCoroutine(Routine_RotateLace(anchor, mBaseDepth + 1, new Vector3(190f, -50f, 0f), new Vector3(0.8f, 0.8f, 1f), speed)));
		mRoutineRotateLaceList.Add(StartCoroutine(Routine_RotateLace(anchor, mBaseDepth + 1, new Vector3(-20f, 60f, 0f), Vector3.one, speed)));
		anchor = Util.CreateAnchorWithChild("CollectionAnchorTopRight", GameObject, UIAnchor.Side.TopRight, component).gameObject;
		mRoutineRotateLaceList.Add(StartCoroutine(Routine_RotateLace(anchor, mBaseDepth + 1, new Vector3(30f, -110f, 0f), Vector3.one, speed)));
		mRoutineRotateLaceList.Add(StartCoroutine(Routine_RotateLace(anchor, mBaseDepth + 1, new Vector3(-140f, -10f, 0f), new Vector3(0.8f, 0.8f, 1f), speed)));
		uISprite = Util.CreateSprite("TitleBarR", gameObject, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, num + 50f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTransformCache.Add(TRANSFORM_CACHE.TITLEBAR_R, uISprite.transform);
		uISprite = Util.CreateSprite("TitleBarL", gameObject, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "menubar_frame", mBaseDepth + 2, new Vector3(2f, num + 50f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTransformCache.Add(TRANSFORM_CACHE.TITLEBAR_L, uISprite.transform);
		UILabel uILabel = Util.CreateLabel("Title", gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 3, Vector3.zero, 38, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(uILabel, Localization.Get("CockpitIconStore"));
		mTransformCache.Add(TRANSFORM_CACHE.TITLE, uILabel.transform);
		empty = "button_back";
		UIButton uIButton = Util.CreateJellyImageButton("BackButton", gameObject2, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth + 3, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnBackButton_Click", UIButtonMessage.Trigger.OnClick);
		mTransformCache.Add(TRANSFORM_CACHE.BACKBUTTON, uIButton.transform);
		InitDialogFrame(DIALOG_SIZE.XLARGE, null, null, 623, 840);
		mDialogFrame.transform.SetParent(mTransformCache[TRANSFORM_CACHE.BODY]);
		empty = "LC_button_buy_jem";
		uIButton = Util.CreateJellyImageButton("GemButton", gameObject2, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth + 1, new Vector3(3f, -455f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnGemButton_Click", UIButtonMessage.Trigger.OnClick);
		mTransformCache.Add(TRANSFORM_CACHE.BUYGEM, uIButton.transform);
		empty = "LC_sale_tab00";
		uIButton = Util.CreateImageButton("TabSale", gameObject3, mAtlas[LOAD_ATLAS.MAIL_BOX].Image);
		Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth - 1, new Vector3(-175f, 440f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnTabSale_Click", UIButtonMessage.Trigger.OnClick);
		Util.SetImageButtonColor(uIButton, Color.gray);
		mTransformCache.Add(TRANSFORM_CACHE.TAB_SALE, uIButton.transform);
		mTab.Add(LIST_TYPE.SALE, uIButton);
		empty = "LC_sale_tab01";
		uIButton = Util.CreateImageButton("TabBooster", gameObject3, mAtlas[LOAD_ATLAS.MAIL_BOX].Image);
		Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth - 1, new Vector3(-10f, 440f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnTabBooster_Click", UIButtonMessage.Trigger.OnClick);
		Util.SetImageButtonColor(uIButton, Color.gray);
		mTransformCache.Add(TRANSFORM_CACHE.TAB_BOOSTER, uIButton.transform);
		mTab.Add(LIST_TYPE.BOOSTER, uIButton);
		mLoadingAnimation = Util.CreateUISsSprite("LoadingAnime", mDialogFrame.gameObject, "ADV_FIGURE_LOADING", Vector3.zero, Vector3.one, mBaseDepth + 2);
		UpdateLayout(Util.ScreenOrientation);
	}

	public override void CreateGemWindow(bool isEula = false)
	{
		UIFont atlasFont = GameMain.LoadFont();
		Vector2 vector = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, mLayoutOrientation);
		Vector3 position = new Vector3(200f, -458f);
		Vector3 vector2 = new Vector3(183f, -515f);
		if (Util.IsWideScreen())
		{
			float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
			float num2 = 0.5625f;
			float num3 = 0.4617052f;
			float num4 = (num2 - num) / (num2 - num3) * 50f;
			position.y = vector.y + num4 + 57f;
			vector2.y = vector.y + num4;
			if (mOrientation == ScreenOrientation.LandscapeLeft || mOrientation == ScreenOrientation.LandscapeRight)
			{
				position = new Vector3(440f, vector.y + 70f);
				vector2 = new Vector3(200f, vector.y + 80f);
			}
		}
		else if (mOrientation == ScreenOrientation.LandscapeRight || mOrientation == ScreenOrientation.LandscapeLeft)
		{
			Vector2 vector3 = Util.LogScreenSize();
			position = new Vector3(440f, (0f - vector3.y) * 0.5f + 70f);
			vector2 = new Vector3(200f, (0f - vector3.y) * 0.5f + 80f);
		}
		UISprite uISprite = Util.CreateSprite("GemFrame", mTransformCache[TRANSFORM_CACHE.FOOTER].gameObject, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel8", mBaseDepth + 1, position, Vector3.one, false);
		uISprite.SetDimensions(185, 70);
		GameObject parent = uISprite.gameObject;
		mTransformCache.Add(TRANSFORM_CACHE.NOWGEM, uISprite.transform);
		uISprite = Util.CreateSprite("GemIcon", parent, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "jem_panel", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("GemNum", parent, atlasFont);
		Util.SetLabelInfo(uILabel, mGame.mPlayer.Dollar.ToString(), mBaseDepth + 3, new Vector3(12f, -1f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mTransformCache.Add(TRANSFORM_CACHE.GEMNUM, uILabel.transform);
	}

	private void CreateList(bool is_select_list)
	{
		bool flag = CreateItemList(LIST_TYPE.SALE);
		bool flag2 = CreateItemList(LIST_TYPE.BOOSTER);
		if (!flag && !flag2)
		{
			mState.Reset(STATE.WAIT, true);
			ConfirmDialog confirmDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("Store_Title_Error"), Localization.Get("Store_Desc_Error00"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			confirmDialog.SetClosedCallback(delegate
			{
				UpdatePossessGem();
				mGame.PlaySe("SE_NEGATIVE", -1);
				Close();
			});
			confirmDialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
			{
				UpdateLayout(Util.ScreenOrientation);
			});
		}
		else
		{
			if (is_select_list)
			{
				if ((flag && mDefaultSelectList == LIST_TYPE.SALE) || !flag2)
				{
					SelectList(LIST_TYPE.SALE);
				}
				else if (flag2 && mDefaultSelectList == LIST_TYPE.BOOSTER)
				{
					SelectList(LIST_TYPE.BOOSTER);
				}
			}
			mGame.mPlayer.ShowSaleIconTime = DateTimeUtil.Now();
		}
		if (mLoadingAnimation != null)
		{
			GameMain.SafeDestroy(mLoadingAnimation.gameObject);
			mLoadingAnimation = null;
		}
	}

	private bool CreateItemList(LIST_TYPE list_type)
	{
		Func<int, bool> fnUniqueSale = delegate(int fn_kind)
		{
			switch ((Def.BOOSTER_KIND)fn_kind)
			{
			case Def.BOOSTER_KIND.WAVE_BOMB:
			case Def.BOOSTER_KIND.TAP_BOMB2:
			case Def.BOOSTER_KIND.RAINBOW:
			case Def.BOOSTER_KIND.ADD_SCORE:
			case Def.BOOSTER_KIND.SKILL_CHARGE:
			case Def.BOOSTER_KIND.SELECT_ONE:
			case Def.BOOSTER_KIND.SELECT_COLLECT:
			case Def.BOOSTER_KIND.COLOR_CRUSH:
			case Def.BOOSTER_KIND.BLOCK_CRUSH:
			case Def.BOOSTER_KIND.PAIR_MAKER:
				return false;
			default:
				return true;
			}
		};
		Func<ShopLimitedTime, bool> fnIsTarget = null;
		Func<ShopLimitedTime, bool> func = null;
		string empty = string.Empty;
		SMVerticalList.OnCreateItemDelegate onCreateItemDelegate = null;
		SMVerticalList.OnCreateFinishDelegate onCreateFinishDelegate = null;
		Size value = LIST_ITEM_SIZE_DELTA.FirstOrDefault((KeyValuePair<LIST_TYPE, Size> n) => n.Key == list_type).Value;
		TRANSFORM_CACHE key;
		switch (list_type)
		{
		case LIST_TYPE.BOOSTER:
			empty = "BoosterList";
			key = TRANSFORM_CACHE.LIST_BOOSTER;
			onCreateItemDelegate = OnBoosterList_CreateItem;
			onCreateFinishDelegate = delegate
			{
				UpdateScreenChangedList(LIST_TYPE.BOOSTER);
			};
			fnIsTarget = (ShopLimitedTime s) => s != null && !s.InvisibleStoreList && mGame.mShopItemData[s.ShopItemID].BoosterID < mGame.mBoosterData.Count && !fnUniqueSale(mGame.mShopItemData[s.ShopItemID].BoosterID) && s.MugenHeartItem == 0 && s.AllCrushItem == 0;
			break;
		case LIST_TYPE.SALE:
			empty = "SaleList";
			key = TRANSFORM_CACHE.LIST_SALE;
			onCreateItemDelegate = OnSaleList_CreateItem;
			onCreateFinishDelegate = delegate
			{
				UpdateScreenChangedList(LIST_TYPE.SALE);
			};
			fnIsTarget = (ShopLimitedTime s) => s != null && !s.InvisibleStoreList && (mGame.mShopItemData[s.ShopItemID].BoosterID >= mGame.mBoosterData.Count || fnUniqueSale(mGame.mShopItemData[s.ShopItemID].BoosterID) || s.MugenHeartItem != 0 || s.AllCrushItem != 0);
			func = (ShopLimitedTime fn_shop) => mGame.mPlayer.IsEnablePurchaseItem(fn_shop.ShopItemID, fn_shop.ShopItemLimit, fn_shop.ShopCount);
			break;
		default:
			return false;
		}
		SMVerticalListInfo listInfo = new SMVerticalListInfo(2, 1006f, 330f + 192f * (Aspect - 1f), 1, value.Width, 800 + WideScreenBodyHeightOffset, 1, value.Height);
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		IEnumerable<ShopItemListItem> enumerable = from s in mGame.SegmentProfile.ShopItemList
			where mGame.mShopItemData.ContainsKey(s.ShopItemID) && fnIsTarget(s)
			select new ShopItemListItem(s) into s
			where s != null
			select s;
		List<ShopItemListItem> list2 = new List<ShopItemListItem>();
		List<ShopItemListItem> list3 = new List<ShopItemListItem>();
		int idx = (int)list_type;
		if (idx < mGame.SegmentProfile.ShopListOrder.Count)
		{
			for (int i = 0; i < mGame.SegmentProfile.ShopListOrder[idx].Count; i++)
			{
				list2.Add(enumerable.FirstOrDefault((ShopItemListItem n) => n.Data.Index == mGame.SegmentProfile.ShopListOrder[idx][i]));
			}
			ShopItemListItem s2;
			foreach (ShopItemListItem item in enumerable)
			{
				s2 = item;
				if (!mGame.SegmentProfile.ShopListOrder[idx].Any((int n) => n == s2.Data.Index))
				{
					list3.Add(s2);
				}
			}
		}
		enumerable = list2.Concat(list3);
		DateTime dateTime = DateTimeUtil.Now();
		Queue<ShopItemListItem> queue = new Queue<ShopItemListItem>();
		ShopItemListItem data;
		foreach (ShopItemListItem item2 in enumerable)
		{
			data = item2;
			if (data == null)
			{
				continue;
			}
			ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => data.Equals(n));
			if (shopLimitedTime == null)
			{
				continue;
			}
			DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(shopLimitedTime.StartTime, true);
			DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(shopLimitedTime.EndTime, true);
			if ((shopLimitedTime.StartTime == shopLimitedTime.EndTime && shopLimitedTime.StartTime == 0L) || (dateTime2 < dateTime && dateTime < dateTime3))
			{
				if (func != null && !func(shopLimitedTime))
				{
					queue.Enqueue(data);
				}
				else
				{
					list.Add(data);
				}
			}
		}
		while (queue.Count > 0)
		{
			list.Add(queue.Dequeue());
		}
		if (list.Count == 0)
		{
			return false;
		}
		GameObject gameObject = Util.CreateGameObject(empty, mTransformCache[TRANSFORM_CACHE.BODY].gameObject);
		mTransformCache.Add(key, gameObject.transform);
		SMVerticalList body = SMVerticalList.Make(gameObject, this, listInfo, list, 0f, 8f, mBaseDepth + 2, mLayoutOrientation != ScreenOrientation.LandscapeLeft && mLayoutOrientation != ScreenOrientation.LandscapeRight, UIScrollView.Movement.Vertical);
		body.OnCreateItem = onCreateItemDelegate;
		body.OnCreateFinish = onCreateFinishDelegate;
		body.ScrollBarPositionOffsetPortrait = new Vector3(10f, 0f);
		body.ScrollBarPositionOffsetLandscape = new Vector3(10f, 0f);
		body.transform.SetLocalPosition(-12f, -10f, 0f);
		mShopItemList.Add(list_type, body);
		body.OnCreateFinish = delegate
		{
			Collider[] componentsInChildren = body.GetComponentsInChildren<Collider>(true);
			if (componentsInChildren != null)
			{
				Collider[] array = componentsInChildren;
				foreach (Collider collider in array)
				{
					RegisterListItemCollider(list_type, collider);
				}
			}
		};
		return true;
	}

	private bool UpdateLayout(ScreenOrientation orientation)
	{
		if (mLayoutOrientation == orientation)
		{
			return false;
		}
		mLayoutOrientation = orientation;
		Vector2 vector = Util.LogScreenSize();
		float num = vector.y * 0.5f;
		float num2 = 70f;
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, orientation);
		Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomLeft, orientation);
		float y = -10f;
		if (Util.IsWideScreen())
		{
			num = vector2.y;
			y = 0f;
		}
		if (mTransformCache.ContainsKey(TRANSFORM_CACHE.BACKBUTTON))
		{
			mTransformCache[TRANSFORM_CACHE.BACKBUTTON].SetLocalPosition(vector3.x + num2, vector3.y + 80f + (float)((orientation != ScreenOrientation.Portrait && orientation != ScreenOrientation.PortraitUpsideDown) ? (-10) : 10), 0f);
		}
		if (mTransformCache.ContainsKey(TRANSFORM_CACHE.BACKGROUND))
		{
			mTransformCache[TRANSFORM_CACHE.BACKGROUND].SetLocalScaleY((Util.LogScreenSize().y + 2f) / 910f);
			mTransformCache[TRANSFORM_CACHE.BACKGROUND].SetLocalPositionY((0f - vector.y) * 0.5f - 2f);
		}
		if (mRoutineRotateList != null)
		{
			StopCoroutine(mRoutineRotateList);
		}
		mRoutineRotateList = StartCoroutine(Routine_OnScreenChangedListAsync(orientation));
		if (mListItems != null)
		{
			foreach (Transform mListItem in mListItems)
			{
				if (mListItem != null)
				{
					mListItem.SetLocalScale(LIST_ITEM_SCALE.FirstOrDefault((KeyValuePair<ScreenOrientation, float> n) => n.Key == orientation).Value);
				}
			}
		}
		switch (orientation)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.BODY))
			{
				mTransformCache[TRANSFORM_CACHE.BODY].SetLocalPositionY(WideScreenBodyPoratraitOffset);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TITLEBAR_R))
			{
				mTransformCache[TRANSFORM_CACHE.TITLEBAR_R].SetLocalPosition(-2f, num + 50f + 123f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TITLEBAR_L))
			{
				mTransformCache[TRANSFORM_CACHE.TITLEBAR_L].SetLocalPosition(2f, num + 50f + 123f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TITLE))
			{
				mTransformCache[TRANSFORM_CACHE.TITLE].SetLocalPosition(0f, num - 28f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TAB_SALE))
			{
				mTransformCache[TRANSFORM_CACHE.TAB_SALE].SetLocalPosition(-175f, 440f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TAB_BOOSTER))
			{
				mTransformCache[TRANSFORM_CACHE.TAB_BOOSTER].SetLocalPosition(-10f, 440f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
			}
			if (mDialogFrame != null)
			{
				mDialogFrame.SetDimensions(623, 840 + WideScreenBodyHeightOffset);
				mDialogFrame.transform.SetLocalPosition(2f, -3f, 0f);
			}
			float y3 = -455f;
			float y4 = -458f;
			float y5 = -515f;
			if (Util.IsWideScreen())
			{
				float num3 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num4 = 0.5625f;
				float num5 = 0.4617052f;
				float num6 = (num4 - num3) / (num4 - num5) * 50f;
				y3 = vector3.y + num6 + 60f;
				y4 = vector3.y + num6 + 57f;
				y5 = vector3.y + num6;
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.BUYGEM))
			{
				mTransformCache[TRANSFORM_CACHE.BUYGEM].SetLocalPosition(3f, y3, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.NOWGEM))
			{
				mTransformCache[TRANSFORM_CACHE.NOWGEM].SetLocalPosition(200f, y4, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.AoSCT))
			{
				mTransformCache[TRANSFORM_CACHE.AoSCT].SetLocalPosition(183f, y5, 0f);
			}
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			float aspect = Aspect;
			float y2 = 210f + 192f * (aspect - 1f);
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.BODY))
			{
				mTransformCache[TRANSFORM_CACHE.BODY].SetLocalPositionY(y);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TITLEBAR_R))
			{
				mTransformCache[TRANSFORM_CACHE.TITLEBAR_R].SetLocalPosition(-2f, num + 50f + 123f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TITLEBAR_L))
			{
				mTransformCache[TRANSFORM_CACHE.TITLEBAR_L].SetLocalPosition(2f, num + 50f + 123f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TITLE))
			{
				mTransformCache[TRANSFORM_CACHE.TITLE].SetLocalPosition(0f, num - 28f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TAB_SALE))
			{
				mTransformCache[TRANSFORM_CACHE.TAB_SALE].SetLocalPosition(-90f, y2, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.TAB_BOOSTER))
			{
				mTransformCache[TRANSFORM_CACHE.TAB_BOOSTER].SetLocalPosition(90f, y2, 0f);
			}
			if ((bool)mDialogFrame)
			{
				mDialogFrame.SetDimensions(1110, (int)(aspect * 375f));
				mDialogFrame.transform.SetLocalPosition(0f, 0f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.BUYGEM))
			{
				mTransformCache[TRANSFORM_CACHE.BUYGEM].SetLocalPosition(-30f, vector3.y + 80f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.NOWGEM))
			{
				mTransformCache[TRANSFORM_CACHE.NOWGEM].SetLocalPosition(440f, vector3.y + 70f, 0f);
			}
			if (mTransformCache.ContainsKey(TRANSFORM_CACHE.AoSCT))
			{
				mTransformCache[TRANSFORM_CACHE.AoSCT].SetLocalPosition(200f, vector3.y + 80f, 0f);
			}
			break;
		}
		}
		return true;
	}

	private void UpdateScreenChangedList(LIST_TYPE type)
	{
		if (mShopItemList == null)
		{
			return;
		}
		IEnumerable<SMVerticalList> enumerable = from n in mShopItemList
			where n.Key == type
			select n.Value;
		foreach (SMVerticalList item in enumerable)
		{
			if (item.gameObject.activeSelf)
			{
				switch (mOrientation)
				{
				case ScreenOrientation.Portrait:
				case ScreenOrientation.PortraitUpsideDown:
					item.OnScreenChanged(true);
					break;
				case ScreenOrientation.LandscapeLeft:
				case ScreenOrientation.LandscapeRight:
					item.OnScreenChanged(false);
					break;
				}
				item.ScrollSet(0f);
			}
		}
	}

	private void UpdatePossessGem()
	{
		if (mDispGemCount != mGame.mPlayer.Dollar && mTransformCache.ContainsKey(TRANSFORM_CACHE.GEMNUM) && (bool)mTransformCache[TRANSFORM_CACHE.GEMNUM])
		{
			UILabel component = mTransformCache[TRANSFORM_CACHE.GEMNUM].GetComponent<UILabel>();
			if (component != null)
			{
				Util.SetLabelText(component, mGame.mPlayer.Dollar.ToString());
				mDispGemCount = mGame.mPlayer.Dollar;
			}
		}
	}

	private void ShowNetworkErrorDialog(STATE next_state)
	{
		mState.Reset(STATE.WAIT, true);
		ConfirmDialog confirmDialog = Util.CreateGameObject("StoreErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(delegate
		{
			mState.Change(next_state);
		});
		confirmDialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
		{
			UpdateLayout(Util.ScreenOrientation);
		});
	}

	private void CallBuyBoosterDialog(SMVerticalComponent cmp, bool is_invisible)
	{
		if (IsBusy)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		ShopItemListItem item = ((!cmp) ? null : (cmp.ListItem as ShopItemListItem));
		if (item == null)
		{
			return;
		}
		GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.ITEM_SHOP;
		if (mGame.SegmentProfile.WebSale.DetailList.Contains(item.Data.Index))
		{
			CreateWebSaleDialog(WebSaleDialog.MODE.TOP);
			return;
		}
		EnableListItemCollider(false);
		BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BoosterDialog", base.ParentGameObject).AddComponent<BuyBoosterDialog>();
		buyBoosterDialog.Init(mGame.mShopItemData[item.Data.Index], is_invisible, false, item.ShopCount);
		buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.IN_SHOP;
		buyBoosterDialog.SetAuthErrorClosedCallback(delegate
		{
			mState.Change(STATE.AUTHERROR_WAIT);
		});
		buyBoosterDialog.SetClosedCallback(delegate(ShopItemData item_data, BuyBoosterDialog.SELECT_ITEM i)
		{
			if (mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15) != 0 || mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30) != 0 || mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60) != 0)
			{
				base.Close();
			}
			else
			{
				if (i != BuyBoosterDialog.SELECT_ITEM.CANCEL)
				{
					UpdateSaleListItem(item, cmp.Index);
				}
				EnableListItemCollider(true);
				UpdatePossessGem();
				mState.Change(STATE.MAIN);
			}
		});
		buyBoosterDialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
		{
			UpdateLayout(Util.ScreenOrientation);
		});
	}

	private void CreateWebSaleDialog(WebSaleDialog.MODE mode, int index = -1)
	{
		WebSaleDialog webSaleDialog = Util.CreateGameObject("WebSaleDialog", base.ParentGameObject).AddComponent<WebSaleDialog>();
		webSaleDialog.Init(mode, index);
		webSaleDialog.mPlace = WebSaleDialog.PLACE.IN_SHOP;
		webSaleDialog.SetClosedCallback(OnWebSaleDialog_Closed);
		webSaleDialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
		{
			UpdateLayout(Util.ScreenOrientation);
		});
	}

	private void EnableListItemCollider(LIST_TYPE type, bool enable)
	{
		if (mScrollItemCollider == null || !mScrollItemCollider.ContainsKey(type))
		{
			return;
		}
		foreach (Collider item in mScrollItemCollider[type])
		{
			if ((bool)item)
			{
				item.enabled = enable;
			}
		}
	}

	private void EnableListItemCollider(bool enable)
	{
		foreach (int value in Enum.GetValues(typeof(LIST_TYPE)))
		{
			EnableListItemCollider((LIST_TYPE)value, enable);
		}
	}

	private void UpdateSaleListItem(ShopItemListItem shop_item, int index)
	{
		if (shop_item == null || mSaleListItemDict == null || !mSaleListItemDict.ContainsKey(index))
		{
			return;
		}
		string empty = string.Empty;
		UIFont uIFont = GameMain.LoadFont();
		LIST_TYPE listType = LIST_TYPE.SALE;
		Vector2 vector = new Vector2(mShopItemList[listType].mList.cellWidth, mShopItemList[listType].mList.cellHeight);
		Size value = LIST_ITEM_SIZE_DELTA.FirstOrDefault((KeyValuePair<LIST_TYPE, Size> n) => n.Key == listType).Value;
		ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => n.ShopItemID == shop_item.Data.Index && n.ShopCount == shop_item.ShopCount);
		bool flag = mGame.SegmentProfile.WebSale.DetailList.Contains(shop_item.Data.Index);
		bool flag2 = false;
		if (!flag && mSaleListItemDict[index].RemainBuyName != null && shopLimitedTime.ShopItemLimit != 0)
		{
			int num = shopLimitedTime.ShopItemLimit;
			if (mGame.mPlayer.PurchaseShop.ContainsKey(shopLimitedTime.ShopItemID))
			{
				num -= mGame.mPlayer.PurchaseShop[shopLimitedTime.ShopItemID].ShopPurchaseItemNum;
				if (mGame.mPlayer.PurchaseShop[shopLimitedTime.ShopItemID].ShopCount == shopLimitedTime.ShopCount && mGame.mPlayer.PurchaseShop[shopLimitedTime.ShopItemID].ShopPurchaseItemNum >= shopLimitedTime.ShopItemLimit)
				{
					flag2 = true;
					num = 0;
				}
			}
			if (num < 0)
			{
				num = 0;
			}
			string text = string.Format(Localization.Get("Store_BuyCount02"), num);
			if (flag2)
			{
				text = Localization.Get("Store_BuyCount_Max");
			}
			Util.SetLabelInfo(mSaleListItemDict[index].RemainBuyName, text, mBaseDepth + 3, new Vector3(145f, -92f), 19, 0, 0, UIWidget.Pivot.Center);
			mSaleListItemDict[index].RemainBuyName.color = new Color32(195, 22, 47, byte.MaxValue);
			mSaleListItemDict[index].RemainBuyName.overflowMethod = UILabel.Overflow.ShrinkContent;
			mSaleListItemDict[index].RemainBuyName.SetDimensions(175, 27);
		}
		if (!(mSaleListItemDict[index].BuyButton != null))
		{
			return;
		}
		float y = ((!flag) ? (-140) : (-115));
		empty = ((!flag2) ? "LC_button_buy3" : "LC_button_soldout2");
		Util.SetImageButtonInfo(mSaleListItemDict[index].BuyButton, empty, empty, empty, mBaseDepth + 4, new Vector3(145f, y), Vector3.one);
		if (flag2)
		{
			JellyImageButton component = mSaleListItemDict[index].BuyButton.GetComponent<JellyImageButton>();
			if (component != null)
			{
				component.SetButtonEnable(false);
			}
			UIEventTrigger component2 = mSaleListItemDict[index].BuyButton.GetComponent<UIEventTrigger>();
			if (component2 != null)
			{
				component2.onClick.Clear();
			}
		}
		else
		{
			Util.SetImageButtonMessage(mSaleListItemDict[index].BuyButton, this, "OnSalePurchaseButton_Click", UIButtonMessage.Trigger.OnClick);
		}
	}

	private void SelectList(LIST_TYPE type)
	{
		if (mSelectedTab == type && mInitFlags[INIT_TYPE.SELECT_TAB])
		{
			return;
		}
		if (mInitFlags[INIT_TYPE.SELECT_TAB])
		{
			mGame.PlaySe("SE_POSITIVE", -1);
		}
		foreach (int value in Enum.GetValues(typeof(LIST_TYPE)))
		{
			if (mTab.ContainsKey((LIST_TYPE)value) && mTab[(LIST_TYPE)value] != null)
			{
				Util.SetImageButtonColor(mTab[(LIST_TYPE)value], (value != (int)type) ? Color.gray : Color.white);
			}
		}
		TRANSFORM_CACHE tRANSFORM_CACHE = TRANSFORM_CACHE.LIST_BOOSTER;
		switch (type)
		{
		case LIST_TYPE.BOOSTER:
			tRANSFORM_CACHE = TRANSFORM_CACHE.LIST_BOOSTER;
			break;
		case LIST_TYPE.SALE:
			tRANSFORM_CACHE = TRANSFORM_CACHE.LIST_SALE;
			break;
		default:
			tRANSFORM_CACHE = TRANSFORM_CACHE.LIST_BOOSTER;
			break;
		}
		foreach (int value2 in Enum.GetValues(typeof(TRANSFORM_CACHE)))
		{
			if ((value2 == 11 || value2 == 10) && mTransformCache.ContainsKey((TRANSFORM_CACHE)value2) && mTransformCache[(TRANSFORM_CACHE)value2].gameObject.activeSelf != (value2 == (int)tRANSFORM_CACHE))
			{
				mTransformCache[(TRANSFORM_CACHE)value2].gameObject.SetActive(value2 == (int)tRANSFORM_CACHE);
			}
		}
		mSelectedTab = type;
		mInitFlags[INIT_TYPE.SELECT_TAB] = true;
	}

	private void RegisterListItemCollider(LIST_TYPE type, Collider collider)
	{
		if (mScrollItemCollider == null)
		{
			mScrollItemCollider = new Dictionary<LIST_TYPE, List<Collider>>(new ListTypeComparer());
		}
		if (!mScrollItemCollider.ContainsKey(type))
		{
			mScrollItemCollider.Add(type, new List<Collider>());
		}
		if (!mScrollItemCollider[type].Contains(collider))
		{
			mScrollItemCollider[type].Add(collider);
		}
	}

	private IEnumerator Routine_RotateLace(GameObject anchor, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		if (anchor == null)
		{
			yield break;
		}
		UISprite sprite = Util.CreateSprite("Lace", anchor, mAtlas[LOAD_ATLAS.MAIL_BOX].Image);
		Util.SetSpriteInfo(sprite, "characolle_back_lace02", depth, pos, scale, false);
		Transform trans = sprite.transform;
		while (true)
		{
			trans.AddLocalEulerAnglesZ(Time.deltaTime * speed);
			yield return null;
		}
	}

	private IEnumerator Routine_OnScreenChangedListAsync(ScreenOrientation orientation)
	{
		Dictionary<LIST_TYPE, bool> flags = new Dictionary<LIST_TYPE, bool>(new ListTypeComparer());
		foreach (int type2 in Enum.GetValues(typeof(LIST_TYPE)))
		{
			flags[(LIST_TYPE)type2] = false;
		}
		while (true)
		{
			foreach (int type in Enum.GetValues(typeof(LIST_TYPE)))
			{
				if (!flags[(LIST_TYPE)type] && mShopItemList.ContainsKey((LIST_TYPE)type) && !(mShopItemList[(LIST_TYPE)type] == null) && !(mShopItemList[(LIST_TYPE)type].ListPanel == null) && mShopItemList[(LIST_TYPE)type].gameObject.activeInHierarchy)
				{
					float scrollValue = mShopItemList[(LIST_TYPE)type].Scroll();
					mShopItemList[(LIST_TYPE)type].OnScreenChanged(orientation == ScreenOrientation.Portrait || orientation == ScreenOrientation.PortraitUpsideDown);
					if (mShopItemList[(LIST_TYPE)type].GetListItemCount() <= 2)
					{
						scrollValue = 0f;
					}
					mShopItemList[(LIST_TYPE)type].ScrollSet(scrollValue);
					flags[(LIST_TYPE)type] = true;
				}
			}
			if (flags.Any((KeyValuePair<LIST_TYPE, bool> n) => !n.Value))
			{
				yield return null;
				continue;
			}
			break;
		}
	}

	private IEnumerator Routine_OnAttachBanner(GameObject go, ShopItemListItem item)
	{
		if (go == null)
		{
			yield break;
		}
		UISprite bg2 = Util.CreateSprite("BackGround", go, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(bg2, "bg00w", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		Util.SetSpriteSize(bg2, 500, 194);
		bg2.color = Color.gray;
		bg2.transform.SetLocalPositionY(65f);
		UISsSprite loading2 = Util.CreateUISsSprite("LoadingAnime", go, "ADV_FIGURE_LOADING", Vector3.zero, Vector3.one, mBaseDepth + 4);
		loading2.transform.SetLocalPositionY(65f);
		ShopItemListItem item2 = default(ShopItemListItem);
		while (true)
		{
			if (mLinkBanner == null)
			{
				yield return null;
				continue;
			}
			LinkBannerInfo info = mLinkBanner.FirstOrDefault((LinkBannerInfo n) => n.param == item2.Data.Index + item2.ResaleCount * 10000);
			if (info != null && info.banner != null)
			{
				UITexture texture = go.AddComponent<UITexture>();
				texture.depth = mBaseDepth + 3;
				texture.mainTexture = info.banner;
				texture.SetDimensions(info.banner.width, info.banner.height);
				texture.SetLocalPositionY(65f);
				if ((bool)loading2)
				{
					GameMain.SafeDestroy(loading2.gameObject);
				}
				if ((bool)bg2)
				{
					GameMain.SafeDestroy(bg2.gameObject);
				}
				loading2 = null;
				bg2 = null;
				break;
			}
			if (info != null && info.banner == null)
			{
				break;
			}
			yield return null;
		}
	}

	private IEnumerator CloseAsync()
	{
		if (mAtlas != null)
		{
			foreach (AtlasInfo info in mAtlas.Values)
			{
				info.Dispose();
			}
			mAtlas.Clear();
		}
		if (mLinkBanner != null)
		{
			mLinkBanner.Clear();
		}
		mLinkBanner = null;
		LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerReq);
		mLinkBannerReq = null;
		if (mRoutineRotateLaceList != null)
		{
			foreach (Coroutine routine in mRoutineRotateLaceList)
			{
				StopCoroutine(routine);
			}
			mRoutineRotateLaceList.Clear();
		}
		mRoutineRotateLaceList = null;
		if (mRoutineRotateList != null)
		{
			StopCoroutine(mRoutineRotateList);
		}
		mRoutineRotateList = null;
		mTransformCache.Clear();
		mTransformCache = null;
		mAtlas = null;
		mEvent.Clear();
		mEvent = null;
		mShopItemList.Clear();
		mShopItemList = null;
		mTab.Clear();
		mTab = null;
		mInitFlags.Clear();
		mInitFlags = null;
		mListItems.Clear();
		mListItems = null;
		mScrollItemCollider.Clear();
		mScrollItemCollider = null;
		mSaleListItemDict.Clear();
		mSaleListItemDict = null;
		mGame.mPlayer.ShowSaleIconTime = null;
		yield return null;
		yield return UnloadUnusedAssets();
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private void OnConnectRetry(int state)
	{
		mState.Change((STATE)state);
	}

	private void OnConnectAbandon()
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseAsync());
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		UpdateLayout(o);
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
		mInitFlags[INIT_TYPE.OPENED_DIALOG] = true;
	}

	public override void OnBackKeyPress()
	{
		OnBackButton_Click(null);
	}

	private void OnBackButton_Click(GameObject go)
	{
		if (!IsBusy)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void OnWebSaleDialog_Closed(WebSaleDialog.SELECT_ITEM i, int index)
	{
		Action fnReOpenWebSale = delegate
		{
			if (mWebSaleDialogIndex == -1)
			{
				CreateWebSaleDialog(WebSaleDialog.MODE.TOP);
			}
			else
			{
				CreateWebSaleDialog(WebSaleDialog.MODE.DETAIL, mWebSaleDialogIndex);
			}
		};
		switch (i)
		{
		case WebSaleDialog.SELECT_ITEM.PURCHASE:
		{
			mWebSaleDialogIndex = index;
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SHOP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
			purchaseDialog.SetBaseDepth(mBaseDepth + 10);
			purchaseDialog.SetClosedCallback(delegate
			{
				fnReOpenWebSale();
			});
			purchaseDialog.SetAuthErrorClosedCallback(delegate
			{
				mState.Change(STATE.AUTHERROR_WAIT);
				UpdatePossessGem();
			});
			purchaseDialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
			{
				UpdateLayout(Util.ScreenOrientation);
			});
			break;
		}
		case WebSaleDialog.SELECT_ITEM.EULA:
		{
			EULADialog eULADialog = Util.CreateGameObject("EULADialog", GameObject).AddComponent<EULADialog>();
			eULADialog.Init(EULADialog.MODE.ASCT);
			eULADialog.SetCallback(delegate
			{
				fnReOpenWebSale();
				UpdatePossessGem();
			});
			eULADialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
			{
				UpdateLayout(Util.ScreenOrientation);
			});
			break;
		}
		}
		if (index <= 0)
		{
			mState.Change(STATE.MAIN);
			return;
		}
		switch (i)
		{
		case WebSaleDialog.SELECT_ITEM.BUY:
		{
			int key = mGame.SegmentProfile.WebSale.DetailList[index - 1];
			ShopItemData shopItemData = mGame.mShopItemData[key];
			TuxedoMaskDilog tuxedoMaskDilog = Util.CreateGameObject("TuxedDialog", base.ParentGameObject).AddComponent<TuxedoMaskDilog>();
			tuxedoMaskDilog.Init(mGame.mShopItemData[shopItemData.Index]);
			tuxedoMaskDilog.SetClosedCallback(delegate
			{
				UpdatePossessGem();
				CreateWebSaleDialog(WebSaleDialog.MODE.TOP);
			});
			tuxedoMaskDilog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
			{
				UpdateLayout(Util.ScreenOrientation);
			});
			break;
		}
		case WebSaleDialog.SELECT_ITEM.DETAIL:
			CreateWebSaleDialog(WebSaleDialog.MODE.DETAIL, index);
			break;
		case WebSaleDialog.SELECT_ITEM.NEGATIVE:
			if (!GameMain.MaintenanceMode)
			{
				CreateWebSaleDialog(WebSaleDialog.MODE.TOP);
				break;
			}
			mWebSaleDialogIndex = -1;
			mState.Change(STATE.MAIN);
			break;
		case WebSaleDialog.SELECT_ITEM.PURCHASE:
		case WebSaleDialog.SELECT_ITEM.EULA:
			break;
		}
	}

	private void OnTabSale_Click(GameObject go)
	{
		if (!IsBusy)
		{
			mState.Reset(STATE.WAIT, true);
			SelectList(LIST_TYPE.SALE);
			mState.Reset(STATE.MAIN, true);
		}
	}

	private void OnTabBooster_Click(GameObject go)
	{
		if (!IsBusy)
		{
			mState.Reset(STATE.WAIT, true);
			SelectList(LIST_TYPE.BOOSTER);
			mState.Reset(STATE.MAIN, true);
		}
	}

	private void OnBoosterPurchaseButton_Click(GameObject go)
	{
		if (!IsBusy)
		{
			CallBuyBoosterDialog((!go) ? null : go.GetComponent<SMVerticalComponent>(), true);
		}
	}

	private void OnSalePurchaseButton_Click(GameObject go)
	{
		if (!IsBusy)
		{
			CallBuyBoosterDialog((!go) ? null : go.GetComponent<SMVerticalComponent>(), false);
		}
	}

	private void OnSCTLButton_Click(GameObject go)
	{
		if (!IsBusy)
		{
			mState.Reset(STATE.WAIT, true);
			EnableListItemCollider(false);
			EULADialog dialog = Util.CreateGameObject("EULADialog", GameObject).AddComponent<EULADialog>();
			dialog.Init(EULADialog.MODE.ASCT);
			dialog.SetCallback(delegate
			{
				EnableListItemCollider(true);
				mState.Change(STATE.MAIN);
				dialog = null;
			});
			dialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
			{
				UpdateLayout(Util.ScreenOrientation);
			});
		}
	}

	private void OnGemButton_Click(GameObject go)
	{
		if (!IsBusy)
		{
			mState.Reset(STATE.WAIT, true);
			EnableListItemCollider(false);
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SHOP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
			purchaseDialog.SetBaseDepth(mBaseDepth + 10);
			purchaseDialog.SetClosedCallback(delegate
			{
				EnableListItemCollider(true);
				UpdatePossessGem();
				mState.Change(STATE.MAIN);
			});
			purchaseDialog.SetAuthErrorClosedCallback(delegate
			{
				UpdatePossessGem();
				mState.Change(STATE.AUTHERROR_WAIT);
			});
			purchaseDialog.SetCallback(CALLBACK_STATE.OnScreenChanged, delegate
			{
				UpdateLayout(Util.ScreenOrientation);
			});
		}
	}

	private void OnSaleList_CreateItem(GameObject go, int index, SMVerticalListItem item, SMVerticalListItem last_item)
	{
		string empty = string.Empty;
		UIFont atlasFont = GameMain.LoadFont();
		ShopItemListItem shopItem = item as ShopItemListItem;
		LIST_TYPE listType = LIST_TYPE.SALE;
		Vector2 vector = new Vector2(mShopItemList[listType].mList.cellWidth, mShopItemList[listType].mList.cellHeight);
		Size value = LIST_ITEM_SIZE_DELTA.FirstOrDefault((KeyValuePair<LIST_TYPE, Size> n) => n.Key == listType).Value;
		UISprite uISprite = Util.CreateSprite("Collision", go, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, new Vector3(0f, 0f), Vector3.one, false);
		uISprite.SetDimensions((int)vector.x, (int)vector.y);
		if (shopItem == null)
		{
			return;
		}
		ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => n.ShopItemID == shopItem.Data.Index && n.ShopCount == shopItem.ShopCount);
		bool flag = mGame.SegmentProfile.WebSale.DetailList.Contains(shopItem.Data.Index);
		SaleListItemInfo saleListItemInfo = new SaleListItemInfo();
		LOAD_ATLAS key = LOAD_ATLAS.MAIL_BOX;
		empty = "instruction_panel3";
		if (shopLimitedTime.SaleBannerID == 0)
		{
			key = LOAD_ATLAS.HUD;
			empty = "instruction_panel";
		}
		uISprite = Util.CreateSprite("Panel", go, mAtlas[key].Image);
		Util.SetSpriteInfo(uISprite, empty, mBaseDepth + 2, new Vector3(0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(value.Width, value.Height - 20);
		GameObject parent = uISprite.gameObject;
		Transform transform = uISprite.transform;
		transform.SetLocalScale(LIST_ITEM_SCALE.FirstOrDefault((KeyValuePair<ScreenOrientation, float> n) => n.Key == mLayoutOrientation).Value);
		mListItems.Add(transform);
		StartCoroutine(Routine_OnAttachBanner(Util.CreateGameObject("Banner", parent), shopItem));
		if (shopLimitedTime.SaleBannerID != 0)
		{
			uISprite = Util.CreateSprite("Recommend", uISprite.gameObject, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetSpriteInfo(uISprite, "LC_sale" + shopLimitedTime.SaleBannerID.ToString("00"), mBaseDepth + 4, new Vector3(-155f, 172f), new Vector3(0.7f, 0.7f), false);
		}
		uISprite = Util.CreateSprite("RemainTimeFrame", parent, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "instruction_accessory11", mBaseDepth + 3, new Vector3(0f, -52f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(470, 36);
		DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(shopLimitedTime.EndTime);
		UILabel uILabel = Util.CreateLabel("RemainTimeLabel", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Format(Localization.Get("ShopItemDesc_Store_SET_Head"), dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute.ToString("00")), mBaseDepth + 4, new Vector3(0f, 3f), 22, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color32(195, 22, 47, byte.MaxValue);
		if (!flag || !string.IsNullOrEmpty(shopItem.Data.SupplementKey))
		{
			Vector3 position = Vector3.zero;
			Size zero = Size.Zero;
			if (!string.IsNullOrEmpty(shopItem.Data.SupplementKey))
			{
				uISprite = Util.CreateSprite("DefaultGemFrame", parent, mAtlas[LOAD_ATLAS.HUD].Image);
				Util.SetSpriteInfo(uISprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(-90f, -100f), Vector3.one, false);
				uISprite.type = UIBasicSprite.Type.Sliced;
				uISprite.SetDimensions(290, 42);
				uILabel = Util.CreateLabel("DefaultGemName", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, Localization.Get(shopItem.Data.SupplementKey), mBaseDepth + 4, new Vector3(0f, 6f), 20, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = Color.white;
				uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
				uILabel.SetDimensions(285, 20);
				position = new Vector3(-90f, -137f);
				zero = new Size(265, 45);
			}
			else if (shopItem.Data.GemAmount < shopItem.Data.GemDefaultAmount)
			{
				bool flag2 = false;
				if (!flag2)
				{
					uISprite = Util.CreateSprite("DefaultGemFrame", parent, mAtlas[LOAD_ATLAS.HUD].Image);
					Util.SetSpriteInfo(uISprite, "instruction_accessory12", mBaseDepth + 3, new Vector3(-90f, -100f), Vector3.one, false);
					uISprite.type = UIBasicSprite.Type.Sliced;
					uISprite.SetDimensions(290, 42);
					uILabel = Util.CreateLabel("DefaultGemName", uISprite.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, Localization.Get("ShopItemDesc_Normal_Price") + string.Format(Localization.Get("ShopItemDesc_Normal_Price_Gem"), string.Format(Localization.Get("Store_Gem"), shopItem.Data.GemDefaultAmount)), mBaseDepth + 4, new Vector3(0f, 6f), 20, 0, 0, UIWidget.Pivot.Center);
					uILabel.color = Color.white;
					uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
					uILabel.SetDimensions(285, 20);
				}
				position = new Vector3(-90f, (!flag2) ? (-137) : (-117));
				zero = new Size(265, 45);
			}
			else if (shopLimitedTime.ShopItemLimit != 0)
			{
				position = new Vector3(-90f, -117f);
				zero = new Size(265, 45);
			}
			else
			{
				position = new Vector3(0f, -90f);
				zero = new Size(485, 45);
			}
			uISprite = Util.CreateSprite("GemFrame", parent, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 3, position, Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(zero.Width, zero.Height);
			uILabel = Util.CreateLabel("GemName", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Format(Localization.Get("Store_Gem"), shopItem.Data.GemAmount), mBaseDepth + 4, new Vector3(0f, -2f), 25, 0, 0, UIWidget.Pivot.Center);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(zero.Width, zero.Height);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		bool flag3 = false;
		if (!flag && shopLimitedTime.ShopItemLimit != 0)
		{
			int num = shopLimitedTime.ShopItemLimit;
			if (mGame.mPlayer.PurchaseShop.ContainsKey(shopLimitedTime.ShopItemID))
			{
				num -= mGame.mPlayer.PurchaseShop[shopLimitedTime.ShopItemID].ShopPurchaseItemNum;
				if (mGame.mPlayer.PurchaseShop[shopLimitedTime.ShopItemID].ShopCount == shopLimitedTime.ShopCount && mGame.mPlayer.PurchaseShop[shopLimitedTime.ShopItemID].ShopPurchaseItemNum >= shopLimitedTime.ShopItemLimit)
				{
					flag3 = true;
					num = 0;
				}
			}
			if (num < 0)
			{
				num = 0;
			}
			string text = string.Format(Localization.Get("Store_BuyCount02"), num);
			if (flag3)
			{
				text = Localization.Get("Store_BuyCount_Max");
			}
			uILabel = Util.CreateLabel("RemainBuyName", parent, atlasFont);
			Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(145f, -92f), 19, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = new Color32(195, 22, 47, byte.MaxValue);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(175, 27);
			saleListItemInfo.RemainBuyName = uILabel;
		}
		float y = ((!flag) ? (-140) : (-115));
		empty = ((!flag3) ? "LC_button_buy3" : "LC_button_soldout2");
		UIButton uIButton = Util.CreateJellyImageButton("PurchaseButton", parent, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth + 4, new Vector3(145f, y), Vector3.one);
		SMVerticalComponent sMVerticalComponent = uIButton.gameObject.AddComponent<SMVerticalComponent>();
		sMVerticalComponent.ListItem = shopItem;
		sMVerticalComponent.Index = index;
		if (flag3)
		{
			JellyImageButton component = uIButton.GetComponent<JellyImageButton>();
			if (component != null)
			{
				component.SetButtonEnable(false);
			}
		}
		else
		{
			Util.SetImageButtonMessage(uIButton, this, "OnSalePurchaseButton_Click", UIButtonMessage.Trigger.OnClick);
		}
		saleListItemInfo.BuyButton = uIButton;
		if (!mSaleListItemDict.ContainsKey(index))
		{
			mSaleListItemDict.Add(index, saleListItemInfo);
		}
	}

	private void OnBoosterList_CreateItem(GameObject go, int index, SMVerticalListItem item, SMVerticalListItem last_item)
	{
		string empty = string.Empty;
		UIFont atlasFont = GameMain.LoadFont();
		ShopItemListItem shopItem = item as ShopItemListItem;
		LIST_TYPE listType = LIST_TYPE.BOOSTER;
		Size value = LIST_ITEM_SIZE_DELTA.FirstOrDefault((KeyValuePair<LIST_TYPE, Size> n) => n.Key == listType).Value;
		UISprite uISprite = Util.CreateSprite("Collision", go, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, new Vector3(0f, 0f), Vector3.one, false);
		uISprite.SetDimensions((int)mShopItemList[listType].mList.cellWidth, (int)mShopItemList[listType].mList.cellHeight);
		if (shopItem != null)
		{
			ShopLimitedTime shopLimitedTime = mGame.SegmentProfile.ShopItemList.FirstOrDefault((ShopLimitedTime n) => n.ShopItemID == shopItem.Data.Index);
			LOAD_ATLAS key = LOAD_ATLAS.MAIL_BOX;
			empty = "instruction_panel3";
			if (shopLimitedTime.SaleBannerID == 0)
			{
				key = LOAD_ATLAS.HUD;
				empty = "instruction_panel";
			}
			uISprite = Util.CreateSprite("Panel", go, mAtlas[key].Image);
			Util.SetSpriteInfo(uISprite, empty, mBaseDepth + 3, new Vector3(0f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(value.Width, value.Height - 14);
			GameObject parent = uISprite.gameObject;
			Transform transform = uISprite.transform;
			transform.SetLocalScale(LIST_ITEM_SCALE.FirstOrDefault((KeyValuePair<ScreenOrientation, float> n) => n.Key == mLayoutOrientation).Value);
			mListItems.Add(transform);
			UILabel uILabel = Util.CreateLabel("BoosterName", parent, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get(shopItem.Data.Name), mBaseDepth + 4, new Vector3(78f, 70f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uISprite = Util.CreateSprite("Separator", parent, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetSpriteInfo(uISprite, "line00", mBaseDepth + 4, new Vector3(79f, 52f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.width = 338;
			uILabel = Util.CreateLabel("BoosterDesc", parent, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get(shopItem.Data.Desc), mBaseDepth + 4, new Vector3(-81f, 9f), 21, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(345, 64);
			uISprite = Util.CreateSprite("PriceGemFrame", parent, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 5, new Vector3(6f, -68f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(170, 50);
			uILabel = Util.CreateLabel("PriceNum", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Format(Localization.Get("Store_Gem"), shopItem.Data.GemAmount), mBaseDepth + 6, new Vector3(0f, -2f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			empty = "LC_button_buy3";
			UIButton uIButton = Util.CreateJellyImageButton("PurchaseButton", parent, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth + 4, new Vector3(174f, -67f), Vector3.one);
			Util.SetImageButtonMessage(uIButton, this, "OnBoosterPurchaseButton_Click", UIButtonMessage.Trigger.OnClick);
			SMVerticalComponent sMVerticalComponent = uIButton.gameObject.AddComponent<SMVerticalComponent>();
			sMVerticalComponent.ListItem = shopItem;
			sMVerticalComponent.Index = index;
			uISprite = Util.CreateSprite("BoosterIcon", parent, mAtlas[LOAD_ATLAS.SHOP].Image);
			Util.SetSpriteInfo(uISprite, shopItem.Data.IconName, mBaseDepth + 4, new Vector3(-176f, 12f), Vector3.one, false);
			uILabel = Util.CreateLabel("NumCross", parent, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Xtext") + shopItem.Data.DefaultNum, mBaseDepth + 4, new Vector3(-176f, -80f), 33, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = new Color(1f, 0.1f, 0.1f);
			if (shopLimitedTime.SaleBannerID != 0)
			{
				uISprite = Util.CreateSprite("Recommend", parent, mAtlas[LOAD_ATLAS.HUD].Image);
				Util.SetSpriteInfo(uISprite, "LC_sale" + shopLimitedTime.SaleBannerID.ToString("00"), mBaseDepth + 4, new Vector3(-155f, 105f), new Vector3(0.7f, 0.7f), false);
			}
		}
	}

	private void ExecCallbackMethod(UnityAction<UnityAction> method, params UnityAction[] calls)
	{
		if (method == null || calls == null)
		{
			return;
		}
		foreach (UnityAction unityAction in calls)
		{
			if (unityAction != null)
			{
				method(unityAction);
			}
		}
	}

	private UnityEvent GetCallback(EVENT_STATE type, bool force_create)
	{
		if (mEvent == null && force_create)
		{
			mEvent = new Dictionary<EVENT_STATE, UnityEvent>(new EventStateComparer());
		}
		if (mEvent != null && !mEvent.ContainsKey(type) && force_create)
		{
			mEvent.Add(type, new UnityEvent());
		}
		return (mEvent == null || !mEvent.ContainsKey(type)) ? null : mEvent[type];
	}

	private void InvokeCallback(EVENT_STATE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.Invoke();
		}
	}

	public void AddCallback(EVENT_STATE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.AddListener, calls);
			}
		}
	}

	public void RemoveCallback(EVENT_STATE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.RemoveListener, calls);
			}
		}
	}

	public void ClearCallback(EVENT_STATE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.RemoveAllListeners();
		}
	}

	public new void ClearCallback()
	{
		foreach (int value in Enum.GetValues(typeof(EVENT_STATE)))
		{
			ClearCallback((EVENT_STATE)value);
		}
	}

	public void SetCallback(EVENT_STATE type, params UnityAction[] calls)
	{
		ClearCallback(type);
		AddCallback(type, calls);
	}
}
