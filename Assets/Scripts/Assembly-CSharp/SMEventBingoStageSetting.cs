public class SMEventBingoStageSetting : SMMapPageSettingBase
{
	public short courseNum;

	public short stageNum;

	public short difficulty;

	public string keyname
	{
		get
		{
			return string.Format("{0:D4}", stageNum + 1000 * courseNum);
		}
	}

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteShort(courseNum);
		a_writer.WriteShort(stageNum);
		a_writer.WriteShort(difficulty);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		courseNum = a_reader.ReadShort();
		stageNum = a_reader.ReadShort();
		difficulty = a_reader.ReadShort();
	}
}
