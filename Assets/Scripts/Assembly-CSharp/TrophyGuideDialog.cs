using UnityEngine;

public class TrophyGuideDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		MAP = 0,
		TROPHYPAGE = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private bool mOKEnable = true;

	private bool mIsRBUnlockByOther;

	private int mRBStage = -1;

	private int mMaxRBKeyNum;

	private float mCounter;

	private bool mFirstlose = true;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mCounter += Time.deltaTime;
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = null;
		text = ((!mFirstlose) ? Localization.Get("TrophyGuid01_Title02") : Localization.Get("TrophyGuid01_Title"));
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(text);
		UISprite uISprite = Util.CreateSprite("Frame", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 123f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(510, 139);
		UISprite sprite = Util.CreateSprite("IconImage", uISprite.gameObject, image);
		Util.SetSpriteInfo(sprite, "button_trophy", mBaseDepth + 2, new Vector3(0f, 0f, 0f), new Vector3(0.8f, 0.8f, 1f), false);
		uISprite = Util.CreateSprite("Frame", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, -66f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(510, 182);
		text = ((!mFirstlose) ? Localization.Get("TrophyGuid01_Desc02") : Localization.Get("TrophyGuid01_Desc"));
		UILabel uILabel = Util.CreateLabel("Text", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(0f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
		uILabel.spacingY = 6;
		string text2 = "LC_button_ok2";
		UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, image);
		Util.SetImageButtonInfo(button, text2, text2, text2, mBaseDepth + 3, new Vector3(0f, -218f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(bool firstlose = true)
	{
		mFirstlose = firstlose;
	}

	public void OnTrophyPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = SELECT_ITEM.TROPHYPAGE;
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = SELECT_ITEM.MAP;
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
