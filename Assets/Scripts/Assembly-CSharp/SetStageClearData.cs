public class SetStageClearData : ParameterObject<SetStageClearData>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("clr_score")]
	public int ClearScore { get; set; }

	[MiniJSONAlias("clr_time")]
	public int ClearTime { get; set; }
}
