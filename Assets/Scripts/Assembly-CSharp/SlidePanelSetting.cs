using System.Collections;
using UnityEngine;

public class SlidePanelSetting
{
	public delegate IEnumerator OnLoad(SlidePanelObject a_panel);

	public delegate void OnUnload(SlidePanelObject a_panel);

	public delegate void OnLayout(SlidePanelObject a_panel, ScreenOrientation o);

	public int mIndex;

	public int mWidth;

	public int mHeight;

	public bool mIsLoadFinished;

	public OnLoad mOnLoadCallback;

	public OnUnload mOnUnloadCallback;

	public OnLayout mOnSetLayoutCallback;

	public SlidePanelSetting(int _width, int _height, int _index = -1)
	{
		mWidth = _width;
		mHeight = _height;
		mIndex = _index;
	}

	public void SetLoadCallback(OnLoad a_callback)
	{
		mOnLoadCallback = a_callback;
	}

	public void SetUnloadCallback(OnUnload a_callback)
	{
		mOnUnloadCallback = a_callback;
	}

	public void SetLayoutCallback(OnLayout a_callback)
	{
		mOnSetLayoutCallback = a_callback;
	}
}
