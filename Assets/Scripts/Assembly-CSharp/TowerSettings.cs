using System;

public class TowerSettings : ICloneable
{
	[MiniJSONAlias("event_id")]
	public short EventID { get; set; }

	[MiniJSONAlias("help_url")]
	public string HelpURL { get; set; }

	[MiniJSONAlias("always_open")]
	public short AlwaysOpen { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public TowerSettings Clone()
	{
		return MemberwiseClone() as TowerSettings;
	}
}
