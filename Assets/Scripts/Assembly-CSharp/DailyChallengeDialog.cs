using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class DailyChallengeDialog : DialogBase
{
	public enum LOAD_ATLAS
	{
		HUD = 0,
		DIALOG_BASE = 1,
		DAILY_CHALLENGE_DIALOG = 2
	}

	public enum LOAD_SOUND
	{
		SE_OPEN_ITEMBOX = 0,
		SE_BINGO_SWIPE = 1
	}

	public enum EVENT_TYPE
	{
		PLAY = 0,
		MIGHT_MAINTENANCE = 1
	}

	public enum REWARD_TYPE : byte
	{
		STAGE = 0,
		COMPLETE = 1
	}

	private enum MOVE_VECTOR
	{
		PREV = 0,
		NEXT = 1
	}

	private enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		CLEAR_EFFECT = 3,
		UNLOCK_STAGE = 4,
		COMPLETE_SHEET = 5
	}

	public sealed class AtlasInfo : IDisposable
	{
		private bool mForceNotUnload;

		private bool mDisposed;

		public ResImage Resource { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Resource == null) ? null : Resource.Image;
			}
		}

		public string Key { get; private set; }

		public bool IsInit { get; private set; }

		public AtlasInfo(string key)
		{
			Key = key;
		}

		~AtlasInfo()
		{
			Dispose(false);
		}

		public void Load(bool not_unload = false)
		{
			if (!not_unload)
			{
				Resource = ResourceManager.LoadImageAsync(Key);
			}
			else
			{
				Resource = ResourceManager.LoadImage(Key);
			}
			IsInit = true;
			mForceNotUnload = not_unload;
		}

		private void UnLoad()
		{
			if (IsInit && !mForceNotUnload)
			{
				Resource = null;
				ResourceManager.UnloadImage(Key);
			}
			IsInit = false;
		}

		public bool IsDone()
		{
			return Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.DONE;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	private const int SCROLLVIEW_CONTENT_ITEM_COUNT = 3;

	private Dictionary<LOAD_ATLAS, AtlasInfo> mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();

	private Dictionary<LOAD_SOUND, ResSound> mSound = new Dictionary<LOAD_SOUND, ResSound>();

	private UILabel mLabelSheetNo;

	private Dictionary<int, UIButton> mSpriteDot = new Dictionary<int, UIButton>();

	private List<DailyChallengeSheetItem> mSheetList = new List<DailyChallengeSheetItem>();

	private UICenterOnChild mCenterOnChild;

	private int mNowSheetPage = 1;

	private Transform mSheetPageTarget;

	private STATE mSheetMovedState = STATE.MAIN;

	private GameObject mRoot;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private Dictionary<EVENT_TYPE, UnityEvent> mCallback;

	private bool mCanOpenNewSheetDialog;

	private STATE mOpenedState = STATE.MAIN;

	private GameStateSMMapBase mGameState;

	private UIScrollView mScrollView;

	private bool mIsClearStageEffectMode;

	private int mDefaultSheetNoOffset;

	private DailyChallengeSheetItem mClearStageSheet;

	private bool mCanScrollView = true;

	private List<Collider> mSheetMoveCollider = new List<Collider>();

	private bool mIsOpenStageInfoDialog;

	private bool mIsFirstPermissionState;

	private DailyChallengeSheetItem ClearStageSheet
	{
		get
		{
			if (mClearStageSheet == null)
			{
				mClearStageSheet = mSheetList.FirstOrDefault((DailyChallengeSheetItem n) => n.No == mGame.SelectedDailyChallengeSheetNo);
			}
			return mClearStageSheet;
		}
	}

	public bool IsSelectStage { get; private set; }

	public bool IsPlayStage { get; private set; }

	public override void Update()
	{
		base.Update();
		if (mState.IsChanged())
		{
			switch (mState.GetStatus())
			{
			case STATE.MAIN:
				if (mCanOpenNewSheetDialog)
				{
					mCanOpenNewSheetDialog = false;
					ImageTextDescDialog imageTextDescDialog = Util.CreateGameObject("FindNewSheetDialog", mRoot).AddComponent<ImageTextDescDialog>();
					imageTextDescDialog.Init(new ImageTextDescDialog.ConfigAtlas
					{
						Atlas = LOAD_ATLAS.DAILY_CHALLENGE_DIALOG.ToString(),
						Sprite = "daily_howto00",
						Desc = Localization.Get("DC_SeatPlus_Desc"),
						ImageHeight = 112
					});
					imageTextDescDialog.SetCallback(CALLBACK_STATE.OnCloseFinished, delegate
					{
						mState.Change(STATE.MAIN);
					});
					mState.Reset(STATE.WAIT, true);
				}
				if ((bool)mScrollView)
				{
					mScrollView.enabled = mCanScrollView;
				}
				if (mSheetMoveCollider != null)
				{
					foreach (Collider item in mSheetMoveCollider)
					{
						if (item != null)
						{
							item.enabled = true;
						}
					}
				}
				mIsFirstPermissionState = true;
				break;
			case STATE.CLEAR_EFFECT:
				if (!ClearStageSheet)
				{
					mState.Change(STATE.MAIN);
					mIsClearStageEffectMode = false;
				}
				else
				{
					ClearStageSheet.PlayClearStage(GlobalVariables.Instance.SelectedDailyChallengeStageNo, mGameState, 0f, delegate
					{
						if (mGame.SelectedDailyChallengeStageNo >= 5)
						{
							mState.Change(STATE.COMPLETE_SHEET);
						}
						else
						{
							mState.Change(STATE.UNLOCK_STAGE);
						}
					});
				}
				goto default;
			case STATE.COMPLETE_SHEET:
				if (!ClearStageSheet)
				{
					mState.Change(STATE.MAIN);
					mIsClearStageEffectMode = false;
				}
				else
				{
					ClearStageSheet.PlayOpenRewardBox(mGameState, 0f, delegate
					{
						int num = -1;
						foreach (DailyChallengeSheetItem mSheet in mSheetList)
						{
							if (mSheet != null && mGame.mPlayer.Data.DailyChallengeStayStage.ContainsKey(mSheet.No) && mGame.mPlayer.Data.DailyChallengeStayStage[mSheet.No] != 999 && (num == -1 || mSheet.No < num))
							{
								num = mSheet.No;
							}
						}
						if (num == -1)
						{
							string sheetSetName = GlobalVariables.Instance.SelectedDailyChallengeSetting.SheetName;
							if (mGame.mDebugDailyChallengeSheetSetName != string.Empty)
							{
								sheetSetName = mGame.mDebugDailyChallengeSheetSetName;
							}
							DailyChallengeSheet dailyChallengeSheet = mGame.mDailyChallengeData.FirstOrDefault((DailyChallengeSheet n) => n.SheetName == sheetSetName);
							if (dailyChallengeSheet == null)
							{
								if (mSheetList != null)
								{
									foreach (DailyChallengeSheetItem mSheet2 in mSheetList)
									{
										if (mSheet2 != null)
										{
											mSheet2.SetInfo(mSheet2.No, false);
										}
									}
								}
								mState.Change(STATE.MAIN);
								mIsClearStageEffectMode = false;
							}
							else if (mGame.mPlayer.Data.DailyChallengeStayStage.Keys.Count() < dailyChallengeSheet.StageCount)
							{
								StartCoroutine(mGameState.GrayOut());
								ImageTextDescDialog imageTextDescDialog2 = Util.CreateGameObject("FindNewSheetDialog", mRoot).AddComponent<ImageTextDescDialog>();
								imageTextDescDialog2.Init(new ImageTextDescDialog.ConfigAtlas
								{
									Atlas = LOAD_ATLAS.DAILY_CHALLENGE_DIALOG.ToString(),
									Sprite = "daily_howto01",
									Desc = Localization.Get("DC_SeatPriorNotice_Desc"),
									ImageHeight = 112
								});
								imageTextDescDialog2.SetCallback(CALLBACK_STATE.OnCloseFinished, delegate
								{
									if (mSheetList != null)
									{
										foreach (DailyChallengeSheetItem mSheet3 in mSheetList)
										{
											if (mSheet3 != null)
											{
												mSheet3.SetInfo(mSheet3.No, false);
											}
										}
									}
									mState.Change(STATE.MAIN);
									mIsClearStageEffectMode = false;
								});
							}
							else
							{
								mGame.PlaySe("VOICE_029", 2);
								AccessoryData accessoryData = mGame.GetAccessoryData(1446);
								EventCompDialog dialog = Util.CreateGameObject("SheetCompDialog", mRoot).AddComponent<EventCompDialog>();
								dialog.Init(accessoryData.GetCompanionID(), "EVENTHUD");
								dialog.SetCallback(CALLBACK_STATE.OnCloseFinished, delegate
								{
									if (mSheetList != null)
									{
										foreach (DailyChallengeSheetItem mSheet4 in mSheetList)
										{
											if (mSheet4 != null)
											{
												mSheet4.SetInfo(mSheet4.No, false);
											}
										}
									}
									mState.Change(STATE.MAIN);
									UnityEngine.Object.Destroy(dialog.gameObject);
									mIsClearStageEffectMode = false;
								});
							}
						}
						else
						{
							if (mSheetList != null)
							{
								foreach (DailyChallengeSheetItem mSheet5 in mSheetList)
								{
									if (mSheet5 != null)
									{
										mSheet5.SetInfo(mSheet5.No, false);
									}
								}
							}
							mState.Change(STATE.MAIN);
							mIsClearStageEffectMode = false;
							SetSheetPage(num);
						}
					});
				}
				goto default;
			case STATE.UNLOCK_STAGE:
				if (ClearStageSheet == null)
				{
					mState.Change(STATE.MAIN);
					mIsClearStageEffectMode = false;
				}
				else
				{
					ClearStageSheet.PlayOpenNewStage(mGame.mPlayer.Data.DailyChallengeStayStage[mGame.SelectedDailyChallengeSheetNo], 0f, delegate
					{
						if (mSheetList != null)
						{
							foreach (DailyChallengeSheetItem mSheet6 in mSheetList)
							{
								if (mSheet6 != null)
								{
									mSheet6.SetInfo(mSheet6.No, false);
								}
							}
						}
						mState.Change(STATE.MAIN);
						mIsClearStageEffectMode = false;
					});
				}
				goto default;
			default:
				if ((bool)mScrollView)
				{
					mScrollView.enabled = false;
				}
				if (mSheetMoveCollider == null)
				{
					break;
				}
				foreach (Collider item2 in mSheetMoveCollider)
				{
					if (item2 != null)
					{
						item2.enabled = false;
					}
				}
				break;
			}
		}
		STATE status = mState.GetStatus();
		if (status == STATE.MAIN && mGameState != null && !mIsOpenStageInfoDialog && (mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.TIME_OVER || mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.TIME_OVER_PUZZLE || mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.LAST_CHECK))
		{
			mGameState.UpdateMugenHeartStatus();
		}
		mState.Update();
	}

	public bool Init(GameObject dialog_root, GameStateSMMapBase game_state, bool can_open_new_sheet_dialog)
	{
		mRoot = dialog_root;
		mCanOpenNewSheetDialog = can_open_new_sheet_dialog;
		mGameState = game_state;
		mIsClearStageEffectMode = false;
		mOpenedState = STATE.MAIN;
		mState.Change(STATE.WAIT);
		mOpenImmediate = false;
		mDefaultSheetNoOffset = mGame.mPlayer.Data.DailyChallengeLastSelectSheetNo - 1;
		mGame.IsDailyChallengePuzzle = true;
		IsPlayStage = false;
		return true;
	}

	public bool InitComeBackFromPuzzle(GameObject dialog_root, GameStateSMMapBase game_state)
	{
		if (GlobalVariables.Instance.SelectedDailyChallengeSheetNo == -1 || GlobalVariables.Instance.SelectedDailyChallengeStageNo == -1)
		{
			return false;
		}
		mRoot = dialog_root;
		mCanOpenNewSheetDialog = false;
		mGameState = game_state;
		mIsClearStageEffectMode = mGame.LastWon == 1;
		mOpenedState = ((!mIsClearStageEffectMode) ? STATE.MAIN : STATE.CLEAR_EFFECT);
		mState.Change(STATE.WAIT);
		mOpenImmediate = true;
		mDefaultSheetNoOffset = mGame.mPlayer.Data.DailyChallengeLastSelectSheetNo - 1;
		mGame.IsDailyChallengePuzzle = true;
		IsPlayStage = false;
		return true;
	}

	public override void BuildDialog()
	{
		string empty = string.Empty;
		UISprite uISprite = null;
		UILabel uILabel = null;
		UIFont atlasFont = GameMain.LoadFont();
		GameObject gameObject = null;
		UIDragScrollView uIDragScrollView = null;
		UIGrid uIGrid = null;
		UIPanel uIPanel = null;
		UIButton uIButton = null;
		InitDialogFrame(DIALOG_SIZE.LARGE, null, "daily_instruction_panel", 575, 575);
		mDialogFrame.type = UIBasicSprite.Type.Advanced;
		mDialogFrame.topType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.bottomType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.leftType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.rightType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.centerType = UIBasicSprite.AdvancedType.Tiled;
		InitDialogTitle(string.Empty, null, null, 430);
		uISprite = Util.CreateSprite("Image", mTitleBoard.gameObject, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
		Util.SetSpriteInfo(uISprite, "LC_daily_title", mBaseDepth + mDialogUsedDepth + 1, new Vector3(12f, 0f), Vector3.one, false);
		uISprite = Util.CreateSprite("Character", mTitleBoard.gameObject, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
		Util.SetSpriteInfo(uISprite, "daily_chara00", mBaseDepth + 3, new Vector3(-177f, -43f), Vector3.one, false);
		CreateCancelButton("OnCancelButton_Clicked");
		GameObject parent = Util.CreateGameObject("Sheet", base.gameObject);
		int num = ((mGame.mPlayer.Data.DailyChallengeStayStage.Count == 1) ? 1 : ((mGame.mPlayer.Data.DailyChallengeStayStage.Count <= 3) ? 3 : mGame.mPlayer.Data.DailyChallengeStayStage.Count));
		mCanScrollView = num > 1;
		if (mCanScrollView)
		{
			uIPanel = Util.CreatePanel("Arrow", parent, mBaseDepth + 2, mBaseDepth + 2);
			uIPanel.transform.SetLocalPosition(0f, -1f);
			gameObject = uIPanel.gameObject;
			empty = "btn_banner02";
			uIButton = Util.CreateImageButton("Left", gameObject, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth, new Vector3(-228f, 0f), Vector3.one);
			if (num > 1)
			{
				Util.SetImageButtonMessage(uIButton, this, "OnArrowPrev_Clicked", UIButtonMessage.Trigger.OnClick);
			}
			else
			{
				Util.SetImageButtonColor(uIButton, Color.gray);
			}
			mSheetMoveCollider.Add(uIButton.GetComponent<Collider>());
			StartCoroutine(AnimationCursorScaleAsync(uIButton, false));
			uIButton = Util.CreateImageButton("Right", gameObject, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth, new Vector3(223f, 0f), Vector3.one);
			uIButton.SetLocalScaleX(-1f);
			if (num > 1)
			{
				Util.SetImageButtonMessage(uIButton, this, "OnArrowNext_Clicked", UIButtonMessage.Trigger.OnClick);
			}
			else
			{
				Util.SetImageButtonColor(uIButton, Color.gray);
			}
			mSheetMoveCollider.Add(uIButton.GetComponent<Collider>());
			StartCoroutine(AnimationCursorScaleAsync(uIButton, true));
		}
		gameObject = Util.CreateGameObject("ScrollViewArea", parent);
		gameObject.transform.SetLocalPosition(0f, 1f);
		UIPanel uIPanel2 = Util.CreatePanel("ScrollView", gameObject, mBaseDepth + 1, mBaseDepth + 1);
		uIPanel2.transform.SetLocalPositionY(-12f);
		gameObject = uIPanel2.gameObject;
		mScrollView = gameObject.AddComponent<UIScrollView>();
		uIPanel2.clipping = UIDrawCall.Clipping.SoftClip;
		uIPanel2.clipSoftness = new Vector2(10f, 0f);
		uIPanel2.SetRect(0f, 0f, 561f, 530f);
		mScrollView.contentPivot = UIWidget.Pivot.TopLeft;
		mScrollView.movement = UIScrollView.Movement.Horizontal;
		mScrollView.dragEffect = UIScrollView.DragEffect.None;
		GameObject gameObject2 = Util.CreateGameObject("Contents", gameObject);
		UIWrapContent uIWrapContent = gameObject2.AddComponent<UIWrapContent>();
		mCenterOnChild = gameObject2.AddComponent<UICenterOnChild>();
		uIWrapContent.itemSize = 482;
		uIWrapContent.onInitializeItem = OnWrapItem_Initialize;
		mCenterOnChild.onCenter = OnWrapItem_Center;
		for (int i = 0; i < num; i++)
		{
			uISprite = Util.CreateSprite("SheetItem" + i, gameObject2, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			GameObject gameObject3 = uISprite.gameObject;
			BoxCollider boxCollider = gameObject3.AddComponent<BoxCollider>();
			uIDragScrollView = gameObject3.AddComponent<UIDragScrollView>();
			Util.SetSpriteInfo(uISprite, "daily_panel" + (i % 3).ToString("00"), mBaseDepth, Vector3.zero, Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(475, 370);
			uIDragScrollView.scrollView = mScrollView;
			GameObject parent2 = Util.CreateGameObject("Route", gameObject3);
			uISprite = Util.CreateSprite("Image1_3", parent2, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, "daily_route", mBaseDepth + 1, new Vector3(0f, 93f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(275, 10);
			GameObject parent3 = Util.CreateGameObject("Route3_4", parent2);
			uISprite = Util.CreateSprite("Image_0", parent3, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, "daily_route", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(306, 10);
			uISprite = Util.CreateSprite("Image_1", parent3, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, "daily_route", mBaseDepth + 1, new Vector3(148f, 41f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(10, 85);
			uISprite = Util.CreateSprite("Image_2", parent3, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, "daily_route", mBaseDepth + 1, new Vector3(-148f, -41f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(10, 85);
			uISprite = Util.CreateSprite("Image4_Box", parent2, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, "daily_route", mBaseDepth + 1, new Vector3(-30f, -87f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(255, 10);
			DailyChallengeSheetItem dailyChallengeSheetItem = gameObject3.AddComponent<DailyChallengeSheetItem>();
			dailyChallengeSheetItem.Init(this, delegate
			{
				IsSelectStage = true;
			}, mGame, mRoot, mAtlas.ToArray(), mBaseDepth + 2, mScrollView, delegate(EVENT_TYPE _event)
			{
				switch (_event)
				{
				case EVENT_TYPE.MIGHT_MAINTENANCE:
					InvokeCallback(_event);
					break;
				case EVENT_TYPE.PLAY:
					IsPlayStage = true;
					Close();
					break;
				}
			});
			dailyChallengeSheetItem.SetCallback(DailyChallengeSheetItem.EVENT_TYPE.OPEN_STAGE_DIALOG, delegate
			{
				mIsOpenStageInfoDialog = true;
			});
			dailyChallengeSheetItem.SetCallback(DailyChallengeSheetItem.EVENT_TYPE.CLOSE_STAGE_DIALOG, delegate
			{
				mIsOpenStageInfoDialog = false;
				IsSelectStage = false;
			});
			mSheetList.Add(dailyChallengeSheetItem);
		}
		gameObject = Util.CreateGameObject("Dot", parent);
		gameObject.transform.SetLocalPosition(0f, -212f);
		uIGrid = gameObject.AddComponent<UIGrid>();
		uIGrid.pivot = UIWidget.Pivot.Center;
		uIGrid.arrangement = UIGrid.Arrangement.Horizontal;
		uIGrid.cellWidth = 23f;
		uIGrid.cellHeight = 14f;
		uIGrid.maxPerLine = 0;
		for (int j = 0; j < mGame.mPlayer.Data.DailyChallengeStayStage.Count; j++)
		{
			empty = "event_load_dot";
			uIButton = Util.CreateImageButton((j + 1).ToString(), gameObject, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
			Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth + 3, Vector3.zero, Vector3.one);
			Util.SetImageButtonMessage(uIButton, this, "OnDot_Clicked", UIButtonMessage.Trigger.OnClick);
			mSheetMoveCollider.Add(uIButton.GetComponent<Collider>());
			mSpriteDot.Add(j, uIButton);
		}
		uIPanel = Util.CreatePanel("SheetDetail", parent, mBaseDepth + 4, mBaseDepth + 3);
		uIPanel.transform.SetLocalPosition(-170f, 180f);
		gameObject = uIPanel.gameObject;
		uISprite = Util.CreateSprite("Band", gameObject, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
		Util.SetSpriteInfo(uISprite, "daily_no", mBaseDepth + 4, Vector3.zero, Vector3.one, false);
		mLabelSheetNo = Util.CreateLabel("Number", gameObject, atlasFont);
		Util.SetLabelInfo(mLabelSheetNo, string.Format(Localization.Get("DC_SeatNo"), 0), mBaseDepth + 5, new Vector3(0f, -4f), 26, 0, 0, UIWidget.Pivot.Center);
		mLabelSheetNo.overflowMethod = UILabel.Overflow.ShrinkContent;
		mLabelSheetNo.SetDimensions(100, 26);
		empty = ((!mCanScrollView) ? "DC_Desc_B" : "DC_Desc");
		uILabel = Util.CreateLabel("Desc", gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get(empty), mBaseDepth + 4, new Vector3(77f, 24f), 18, 0, 0, UIWidget.Pivot.Left);
		Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(365, 36);
		uISprite = Util.CreateSprite("RemainFrame", base.gameObject, mAtlas[LOAD_ATLAS.DAILY_CHALLENGE_DIALOG].Image);
		Util.SetSpriteInfo(uISprite, "daily_total", mBaseDepth + 3, new Vector3(0f, -245f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(400, 31);
		long num2 = GlobalVariables.Instance.SelectedDailyChallengeSetting.EndTime;
		if (mGame.mDebugDailyChallengeEndTime != -1)
		{
			num2 = mGame.mDebugDailyChallengeEndTime;
		}
		DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(num2);
		uILabel = Util.CreateLabel("Label", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Format(Localization.Get("DC_TimeLim_Desc"), dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute.ToString("00")), mBaseDepth + 4, Vector3.zero, 19, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
	}

	private void SetSheetPage(int sheet_no, MOVE_VECTOR vector = MOVE_VECTOR.NEXT)
	{
		if (!mCenterOnChild || mSheetList == null)
		{
			return;
		}
		mSheetMovedState = mState.GetNextStatus();
		mState.Reset(STATE.WAIT);
		sheet_no = (int)Mathf.Repeat(sheet_no - 1, mGame.mPlayer.Data.DailyChallengeStayStage.Count) + 1;
		DailyChallengeSheetItem[] array = mSheetList.OrderBy((DailyChallengeSheetItem _) => _.transform.localPosition.x).ToArray();
		if (vector == MOVE_VECTOR.PREV)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if ((bool)array[i] && array[i].No == sheet_no)
				{
					mSheetPageTarget = array[i].transform;
					mCenterOnChild.CenterOn(mSheetPageTarget);
					break;
				}
			}
			return;
		}
		for (int num = array.Length - 1; num >= 0; num--)
		{
			if ((bool)array[num] && array[num].No == sheet_no)
			{
				mSheetPageTarget = array[num].transform;
				mCenterOnChild.CenterOn(mSheetPageTarget);
				break;
			}
		}
	}

	private bool IsBusy()
	{
		return GetBusy() || mState.GetStatus() != STATE.MAIN;
	}

	public override IEnumerator BuildResource()
	{
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();
		}
		if (mSound == null)
		{
			mSound = new Dictionary<LOAD_SOUND, ResSound>();
		}
		foreach (int type2 in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			AtlasInfo info3 = new AtlasInfo(((LOAD_ATLAS)type2).ToString());
			mAtlas.Add((LOAD_ATLAS)type2, info3);
			bool isNotUnload = false;
			LOAD_ATLAS lOAD_ATLAS = (LOAD_ATLAS)type2;
			if (lOAD_ATLAS == LOAD_ATLAS.HUD || lOAD_ATLAS == LOAD_ATLAS.DIALOG_BASE)
			{
				isNotUnload = true;
			}
			info3.Load(isNotUnload);
		}
		foreach (int type in Enum.GetValues(typeof(LOAD_SOUND)))
		{
			mSound.Add((LOAD_SOUND)type, ResourceManager.LoadSoundAsync(((LOAD_SOUND)type).ToString()));
		}
		foreach (AtlasInfo info2 in mAtlas.Values)
		{
			while (!info2.IsDone())
			{
				yield return null;
			}
		}
		foreach (ResSound info in mSound.Values)
		{
			while (info.LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return null;
			}
		}
		yield return null;
	}

	private IEnumerator AnimationCursorScaleAsync(UIButton btn, bool is_flip)
	{
		if (btn == null)
		{
			yield break;
		}
		AnimationCurve animScale = AnimationCurve.Linear(0f, 0f, 0.5f, 1f);
		animScale.postWrapMode = WrapMode.PingPong;
		animScale.preWrapMode = WrapMode.PingPong;
		float time = 0f;
		while (true)
		{
			float value = animScale.Evaluate(time) * 90f * ((float)Math.PI / 180f) * 0.15f + 1f;
			btn.transform.SetLocalScale((!is_flip) ? value : (0f - value), value);
			yield return null;
			time += Time.deltaTime;
			if (time > 1f)
			{
				time -= 1f;
			}
		}
	}

	private IEnumerator CloseProcess()
	{
		if (!IsPlayStage)
		{
			mGame.mPlayer.Data.DailyChallengeLastSelectSheetNo = mNowSheetPage;
			mGame.Save();
			mGame.SelectedDailyChallengeSheetNo = -1;
			mGame.SelectedDailyChallengeStageNo = -1;
		}
		if (mAtlas != null)
		{
			foreach (AtlasInfo info2 in mAtlas.Values)
			{
				info2.Dispose();
			}
			mAtlas.Clear();
		}
		if (mSound != null)
		{
			foreach (LOAD_SOUND info in mSound.Keys)
			{
				ResourceManager.UnloadSound(info.ToString());
			}
			mSound.Clear();
		}
		yield return null;
		yield return UnloadUnusedAssets();
		if (IsPlayStage)
		{
			InvokeCallback(EVENT_TYPE.PLAY);
		}
		else
		{
			mGame.IsDailyChallengePuzzle = false;
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
		mState.Change(mOpenedState);
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public override void OnBackKeyPress()
	{
		OnCancelButton_Clicked(null);
	}

	private void OnCancelButton_Clicked(GameObject go)
	{
		if (!IsBusy() && !mIsOpenStageInfoDialog)
		{
			mState.Change(STATE.WAIT);
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void OnWrapItem_Initialize(GameObject go, int wrap_index, int real_index)
	{
		DailyChallengeSheetItem component = go.GetComponent<DailyChallengeSheetItem>();
		if (component == null)
		{
			return;
		}
		string sheetSetName = GlobalVariables.Instance.SelectedDailyChallengeSetting.SheetName;
		if (mGame.mDebugDailyChallengeSheetSetName != string.Empty)
		{
			sheetSetName = mGame.mDebugDailyChallengeSheetSetName;
		}
		DailyChallengeSheet dailyChallengeSheet = mGame.mDailyChallengeData.FirstOrDefault((DailyChallengeSheet n) => n.SheetName == sheetSetName);
		if (!(dailyChallengeSheet == null))
		{
			int num = mGame.mPlayer.Data.DailyChallengeStayStage.Count();
			int num2 = (int)Mathf.Repeat(real_index % num + mDefaultSheetNoOffset, num) + 1;
			if (num2 <= 0)
			{
				num2 = mGame.mPlayer.Data.DailyChallengeStayStage.Count() - -num2;
			}
			UISprite component2 = go.GetComponent<UISprite>();
			if (component2 != null)
			{
				Util.SetSpriteImageName(component2, "daily_panel" + ((num2 - 1) % 3).ToString("00"), Vector3.one, false);
				component2.SetDimensions(475, 370);
			}
			component.SetInfo(num2, mIsClearStageEffectMode);
		}
	}

	private void OnWrapItem_Center(GameObject go)
	{
		DailyChallengeSheetItem component = go.GetComponent<DailyChallengeSheetItem>();
		if (component == null)
		{
			return;
		}
		if (mNowSheetPage != component.No && mIsFirstPermissionState)
		{
			mGame.PlaySe("SE_BINGO_SWIPE", -1);
		}
		mNowSheetPage = component.No;
		if ((bool)mLabelSheetNo)
		{
			Util.SetLabelText(mLabelSheetNo, string.Format(Localization.Get("DC_SeatNo"), component.No));
		}
		if (mSpriteDot != null)
		{
			foreach (KeyValuePair<int, UIButton> item in mSpriteDot)
			{
				if ((bool)item.Value)
				{
					Util.SetImageButtonColor(item.Value, (item.Key != component.No - 1) ? Color.gray : Color.white);
				}
			}
		}
		if (mSheetPageTarget != null && go.transform == mSheetPageTarget)
		{
			mSheetPageTarget = null;
			mState.Change(mSheetMovedState);
		}
	}

	private void OnDot_Clicked(GameObject go)
	{
		int result;
		if (int.TryParse(go.name, out result))
		{
			MOVE_VECTOR vector = MOVE_VECTOR.NEXT;
			if (result < mNowSheetPage)
			{
				vector = MOVE_VECTOR.PREV;
			}
			SetSheetPage(result, vector);
		}
	}

	private void OnArrowNext_Clicked(GameObject go)
	{
		SetSheetPage(mNowSheetPage + 1);
	}

	private void OnArrowPrev_Clicked(GameObject go)
	{
		SetSheetPage(mNowSheetPage - 1, MOVE_VECTOR.PREV);
	}

	private void ExecCallbackMethod(UnityAction<UnityAction> method, params UnityAction[] calls)
	{
		if (method == null || calls == null)
		{
			return;
		}
		foreach (UnityAction unityAction in calls)
		{
			if (unityAction != null)
			{
				method(unityAction);
			}
		}
	}

	private UnityEvent GetCallback(EVENT_TYPE type, bool force_create)
	{
		if (mCallback == null && force_create)
		{
			mCallback = new Dictionary<EVENT_TYPE, UnityEvent>();
		}
		if (mCallback != null && !mCallback.ContainsKey(type) && force_create)
		{
			mCallback.Add(type, new UnityEvent());
		}
		return (mCallback == null || !mCallback.ContainsKey(type)) ? null : mCallback[type];
	}

	private void InvokeCallback(EVENT_TYPE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.Invoke();
		}
	}

	public void AddCallback(EVENT_TYPE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.AddListener, calls);
			}
		}
	}

	public void RemoveCallback(EVENT_TYPE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.RemoveListener, calls);
			}
		}
	}

	public void ClearCallback(EVENT_TYPE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.RemoveAllListeners();
		}
	}

	public new void ClearCallback()
	{
		foreach (int value in Enum.GetValues(typeof(EVENT_TYPE)))
		{
			ClearCallback((EVENT_TYPE)value);
		}
	}

	public void SetCallback(EVENT_TYPE type, params UnityAction[] calls)
	{
		ClearCallback(type);
		AddCallback(type, calls);
	}
}
