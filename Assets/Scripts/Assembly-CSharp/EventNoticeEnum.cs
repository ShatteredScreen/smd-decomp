public class EventNoticeEnum
{
	public enum EventStarNotice
	{
		FIRST_ROADBLOCK_BEFORE = 0,
		FIRST_ROADBLOCK_RELEASE = 1,
		FIRST_ROADBLOCK_ADV = 2,
		COMPLETE_EVENT = 99
	}

	public enum EventLabyrinthNotice
	{
		NONE = 100,
		FIRST_COURSE_IN = 101,
		OPEN_NEWCOURSE = 102,
		REPEAT_PLAY_COURSE = 103,
		COMPLETE_EVENT = 104,
		LABYRINTH_NOTICE_MAX = 199
	}
}
