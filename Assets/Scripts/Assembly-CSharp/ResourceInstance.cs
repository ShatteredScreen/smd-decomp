public class ResourceInstance
{
	public enum TYPE
	{
		INVALID = -1,
		TEXTURE = 0,
		IMAGE = 1,
		SS_ANIMATION = 2,
		L2D_ANIMATION = 3,
		SOUND = 4,
		SCRIPTABLE_OBJECT = 5
	}

	public enum LOADSTATE
	{
		INIT = 0,
		LOADING = 1,
		DONE = 2
	}

	public string Name;

	public TYPE ResourceType;

	public LOADSTATE LoadState;

	public bool Preload;

	public ResourceInstance(string name)
	{
		Name = name;
		ResourceType = TYPE.INVALID;
		LoadState = LOADSTATE.INIT;
		Preload = false;
	}
}
