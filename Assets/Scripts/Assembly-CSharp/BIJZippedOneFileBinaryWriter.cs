using System;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;

public class BIJZippedOneFileBinaryWriter : BIJBinaryWriter
{
	private const string CONTENT = "._CONTENT_";

	private FileStream mStream;

	private ZipOutputStream mZipStream;

	public BIJZippedOneFileBinaryWriter(string path)
		: this(path, string.Empty, Encoding.UTF8, true)
	{
	}

	public BIJZippedOneFileBinaryWriter(string path, string password)
		: this(path, password, Encoding.UTF8, true)
	{
	}

	public BIJZippedOneFileBinaryWriter(string path, string password, Encoding encoding)
		: this(path, password, encoding, true)
	{
	}

	public BIJZippedOneFileBinaryWriter(string path, string password, Encoding encoding, bool mkdir)
	{
		mStream = null;
		Writer = null;
		try
		{
			mPath = path;
			string directoryName = System.IO.Path.GetDirectoryName(mPath);
			if (mkdir && !string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			mStream = new FileStream(mPath, FileMode.Create, FileAccess.Write);
			mZipStream = new ZipOutputStream(mStream);
			mZipStream.SetLevel(9);
			if (!string.IsNullOrEmpty(password))
			{
				mZipStream.Password = password;
			}
			ZipNameTransform zipNameTransform = new ZipNameTransform(string.Empty);
			string name = zipNameTransform.TransformFile("._CONTENT_");
			ZipEntry entry = new ZipEntry(name)
			{
				IsUnicodeText = true,
				DateTime = DateTime.Now
			};
			mZipStream.PutNextEntry(entry);
			Writer = new BinaryWriter(mZipStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	public override void Close()
	{
		base.Close();
		if (mZipStream != null)
		{
			try
			{
				mZipStream.Flush();
				mZipStream.Finish();
				mZipStream.Close();
			}
			catch (Exception)
			{
			}
			mZipStream = null;
		}
		if (mStream != null)
		{
			try
			{
				mStream.Close();
			}
			catch (Exception)
			{
			}
			mStream = null;
		}
	}
}
