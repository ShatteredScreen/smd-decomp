public class MCChargeRequest : ParameterObject<MCChargeRequest>
{
	public string api_version { get; set; }

	public MCReceipt receipt { get; set; }

	public int bhiveid { get; set; }

	public string udid { get; set; }

	public string uniq_id { get; set; }

	public int uuid { get; set; }

	public int iapid { get; set; }

	public string price_currency_code { get; set; }

	public float price { get; set; }

	public string formatted_price { get; set; }

	public int series { get; set; }

	public int lvl { get; set; }

	public int mc { get; set; }

	public int sc { get; set; }

	public int tgt_series { get; set; }

	public int tgt_stage { get; set; }

	public int place { get; set; }
}
