public class UserAuth : ParameterObject<UserAuth>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("unity_id")]
	public string UnityID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("uuid")]
	public int UUID { get; set; }

	[MiniJSONAlias("mac_addr")]
	public string MacAddress { get; set; }

	[MiniJSONAlias("bhiveid")]
	public int HiveId { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("adid")]
	public string AdvertisingID { get; set; }
}
