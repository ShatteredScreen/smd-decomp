using UnityEngine;

public struct MapSNSSetting
{
	public Texture2D Icon;

	public int IconID;

	public string Gender;

	public int IconLv;

	public int UUID;
}
