using UnityEngine;

public class FriendHelpSuccessDialog : DialogBase
{
	public enum CONFIRM_IMAGE_DIALOG_STYLE
	{
		OK_ONLY = 0,
		CLOSE_ONLY = 1,
		YES_NO = 2,
		TROPHY = 3
	}

	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		NETWORK_00 = 3,
		NETWORK_00_1 = 4,
		NETWORK_00_2 = 5
	}

	public enum SELECT_ITEM
	{
		SUCCESS = 0,
		ERROR = 1,
		NO = 2
	}

	public enum SendFriendHelpResResult
	{
		SENT_OK = 0,
		SENT_NG = 1,
		NO_FRIEND = 2,
		NO_FRIENDHELP = 3
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private CONFIRM_IMAGE_DIALOG_STYLE mConfirmStyle;

	private DIALOG_SIZE mDialogSize;

	private string mTitle;

	private string mDescriptionA;

	private string mDescriptionB;

	private string mImage;

	private UISprite mBoardSprite;

	private UILabel LabelTop;

	private UILabel LabelBot;

	private UISprite mImagesprite;

	private bool mOKEnable = true;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	public STATE CurrentState;

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private ConfirmDialog mConfirmDialog;

	private string mImg = "LC_button_ok2";

	private float mCounter;

	private bool mSendFriendHelpResSucceed;

	private bool mSendFriendHelpResCheckDone;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		CurrentState = mState.GetStatus();
		switch (CurrentState)
		{
		case STATE.INIT:
			Server.GiveGiftEvent += Server_SendFriendHelpRes;
			mState.Reset(STATE.NETWORK_00, true);
			break;
		case STATE.NETWORK_00:
			Network_SendFriendHelpRes();
			mState.Change(STATE.NETWORK_00_1);
			break;
		case STATE.NETWORK_00_1:
			if (mSendFriendHelpResCheckDone)
			{
				mState.Change(STATE.NETWORK_00_2);
			}
			break;
		case STATE.NETWORK_00_2:
		{
			if (mSendFriendHelpResSucceed)
			{
				mSelectItem = SELECT_ITEM.SUCCESS;
				mState.Change(STATE.MAIN);
				break;
			}
			mSelectItem = SELECT_ITEM.ERROR;
			SendFriendHelpFail();
			mConfirmDialog = Util.CreateGameObject("FriendHelpNGDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			string desc = Localization.Get("FriendHelp_SendError_Desc");
			mConfirmDialog.Init(Localization.Get("FriendHelp_SendError_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			mConfirmDialog.SetClosedCallback(OnFriendHelpResErrorDialogClosed);
			mState.Change(STATE.MAIN);
			break;
		}
		}
		mCounter += Time.deltaTime;
		if (mOKEnable && mCounter > 2.5f)
		{
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, mImg, mImg, mImg, mBaseDepth + 4, new Vector3(0f, -117f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			mOKEnable = false;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(mDialogSize);
		InitDialogTitle(mTitle);
		mBoardSprite = Util.CreateSprite("BoosterBoard", base.gameObject, image);
		Util.SetSpriteInfo(mBoardSprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 27f, 0f), Vector3.one, false);
		mBoardSprite.SetDimensions(510, 104);
		mBoardSprite.type = UIBasicSprite.Type.Sliced;
		float y = 0f;
		float y2 = -280f;
		float y3 = 0f;
		switch (mDialogSize)
		{
		case DIALOG_SIZE.SMALL:
			y = -10f;
			y2 = -130f;
			break;
		case DIALOG_SIZE.MEDIUM:
			y = 90f;
			y3 = -55f;
			y2 = -117f;
			break;
		case DIALOG_SIZE.LARGE:
			y = -180f;
			y2 = -490f;
			break;
		case DIALOG_SIZE.XLARGE:
			y = 60f;
			y2 = -370f;
			break;
		}
		LabelTop = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(LabelTop, mDescriptionA, mBaseDepth + 4, new Vector3(0f, y, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(LabelTop);
		LabelTop.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		LabelTop.overflowMethod = UILabel.Overflow.ResizeHeight;
		LabelTop.spacingY = 6;
		mImagesprite = Util.CreateSprite("IconImage", mBoardSprite.gameObject, image);
		Util.SetSpriteInfo(mImagesprite, mImage, mBaseDepth + 2, new Vector3(0f, 1f, 0f), Vector3.one, false);
		LabelBot = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(LabelBot, mDescriptionB, mBaseDepth + 4, new Vector3(0f, y3, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(LabelBot);
		LabelBot.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		LabelBot.overflowMethod = UILabel.Overflow.ResizeHeight;
		LabelBot.spacingY = 6;
		LabelBot.color = Color.red;
		switch (mConfirmStyle)
		{
		case CONFIRM_IMAGE_DIALOG_STYLE.OK_ONLY:
		case CONFIRM_IMAGE_DIALOG_STYLE.CLOSE_ONLY:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			string text = "LC_button_yes";
			if (mConfirmStyle == CONFIRM_IMAGE_DIALOG_STYLE.CLOSE_ONLY)
			{
				text = "LC_button_close";
			}
			Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(0f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_IMAGE_DIALOG_STYLE.TROPHY:
			mBoardSprite.transform.localPosition = new Vector3(0f, 70f, 0f);
			LabelTop.transform.localPosition = new Vector3(0f, -16f, 0f);
			LabelBot.transform.localPosition = new Vector3(0f, -55f, 0f);
			break;
		default:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(130f, y2, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		}
	}

	public void Init(string title, string descA, string descB, string image, CONFIRM_IMAGE_DIALOG_STYLE style, bool openSeEnable = true)
	{
		Init(title, descA, descB, image, style, DIALOG_SIZE.MEDIUM);
		SetOpenSeEnable(openSeEnable);
	}

	public void Init(string title, string descA, string descB, string image, CONFIRM_IMAGE_DIALOG_STYLE style, DIALOG_SIZE size)
	{
		mTitle = title;
		mDescriptionA = descA;
		mDescriptionB = descB;
		mConfirmStyle = style;
		mDialogSize = size;
		mImage = image;
	}

	public override void OnOpenFinished()
	{
		mGame.PlaySe("SE_DIALY_COMPLETE_STAMP", -1);
		UISsSprite uISsSprite = null;
		uISsSprite = Util.CreateGameObject("GrowHalo", base.gameObject).AddComponent<UISsSprite>();
		uISsSprite.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
		uISsSprite.depth = mBaseDepth + 5;
		uISsSprite.transform.localPosition = new Vector3(0f, 0f, 0f);
		uISsSprite.transform.localScale = new Vector3(1f, 1f, 1f);
		uISsSprite.PlayCount = 1;
		uISsSprite.Play();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Server.GiveGiftEvent -= Server_SendFriendHelpRes;
	}

	public override void OnBackKeyPress()
	{
		OnPositivePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	private void OnFriendHelpResErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Reset(STATE.NETWORK_00, true);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void Network_SendFriendHelpRes()
	{
		mSendFriendHelpResSucceed = false;
		mSendFriendHelpResCheckDone = false;
		Player mPlayer = mGame.mPlayer;
		FriendHelpPlay playingFriendHelp = mGame.PlayingFriendHelp;
		if (playingFriendHelp == null || !playingFriendHelp.IsStageCleared)
		{
			mSendFriendHelpResCheckDone = true;
			return;
		}
		int fromID = playingFriendHelp.Gift.Data.FromID;
		GiftItem giftItem = new GiftItem();
		giftItem.Data.FromID = mGame.mPlayer.UUID;
		giftItem.Data.Message = mGame.mPlayer.NickName;
		giftItem.Data.ItemCategory = 19;
		giftItem.Data.ItemKind = playingFriendHelp.Gift.Data.ItemKind;
		giftItem.Data.ItemID = playingFriendHelp.Gift.Data.ItemID;
		giftItem.Data.Quantity = 1;
		if (!Server.GiveGift(giftItem.Data, fromID))
		{
			mSendFriendHelpResCheckDone = true;
		}
	}

	private void Server_SendFriendHelpRes(int result, int code)
	{
		if (result == 0)
		{
			mSendFriendHelpResSucceed = true;
			SendFriendHelpSuccess();
		}
		mSendFriendHelpResCheckDone = true;
	}

	private void SendFriendHelpSuccess()
	{
		if (mGame.PlayingFriendHelp != null && !mGame.PlayingFriendHelp.IsStageCleared)
		{
		}
	}

	private void SendFriendHelpFail()
	{
	}
}
