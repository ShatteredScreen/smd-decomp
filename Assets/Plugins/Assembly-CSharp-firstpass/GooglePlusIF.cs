using System;
using System.Collections;
using UnityEngine;

public abstract class GooglePlusIF : MonoBehaviour
{
	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	public static Action<string> authenticationSucceededEvent;

	public static Action<string> authenticationFailedEvent;

	public static Action<object> loadPersonSucceededEvent;

	public static Action<string> loadPersonFailedEvent;

	public static Action<object> loadVisiblePeopleSucceededEvent;

	public static Action<string> loadVisiblePeopleFailedEvent;

	public static Action<string, string> getUserImageURLEvent;

	public static Action<string> interactivePostSucceededEvent;

	public static Action<string> interactivePostFailedEvent;

	public void enableLogging(bool enable)
	{
		enableLogging(enable, enable);
	}

	public abstract void enableLogging(bool enable, bool verbose);

	public void setup(string clientid)
	{
		setup(clientid, false);
	}

	public abstract void setup(string clientid, bool autoConnect);

	public abstract void onPause();

	public abstract void onResume();

	public abstract bool isCanceled();

	public abstract bool isSignedIn();

	public abstract void authenticate();

	public abstract void signOut();

	public abstract bool loadPerson(string userid);

	public abstract bool loadVisiblePeople(string userid);

	public abstract bool getUserImageURL(string userid);

	public abstract bool interactivePost(string title, string message, string link, string attachUrl, string attachMime, string actionLabel, string actionUri, string deepLinkID, string description, string thumbnailUri);

	protected static string ToSS(object o)
	{
		return (o != null) ? o.ToString() : string.Empty;
	}

	protected static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	protected static void log(string format, params object[] args)
	{
		try
		{
			long num = time();
			string arg = string.Format(format, args);
			UnityEngine.Debug.Log(string.Format("@@@ {0}({1}): {2}", "GooglePlusPlugin", num, arg));
		}
		catch (Exception)
		{
		}
	}

	protected static void dbg(string format, params object[] args)
	{
		try
		{
			long num = time();
			string arg = string.Format(format, args);
			UnityEngine.Debug.Log(string.Format("@@@ {0}({1}): {2}", "GooglePlusPlugin", num, arg));
		}
		catch (Exception)
		{
		}
	}

	protected void _PluginCB_onSignInSucceeded(string result)
	{
		if (authenticationSucceededEvent != null)
		{
			authenticationSucceededEvent(result);
		}
	}

	protected void _PluginCB_onSignInFailed(string result)
	{
		if (authenticationFailedEvent != null)
		{
			authenticationFailedEvent(result);
		}
	}

	protected void _PluginCB_onLoadPersonSucceeded(string result)
	{
		try
		{
			if (loadPersonSucceededEvent != null)
			{
				loadPersonSucceededEvent(MiniJSON.jsonDecode(result));
			}
		}
		catch (Exception ex)
		{
			_PluginCB_onLoadPersonFailed(ex.ToString());
		}
	}

	protected void _PluginCB_onLoadPersonFailed(string result)
	{
		if (loadPersonFailedEvent != null)
		{
			loadPersonFailedEvent(result);
		}
	}

	protected void _PluginCB_onLoadVisiblePeopleSucceeded(string result)
	{
		try
		{
			if (loadVisiblePeopleSucceededEvent != null)
			{
				loadVisiblePeopleSucceededEvent(MiniJSON.jsonDecode(result));
			}
		}
		catch (Exception ex)
		{
			_PluginCB_onLoadVisiblePeopleFailed(ex.ToString());
		}
	}

	protected void _PluginCB_onLoadVisiblePeopleFailed(string result)
	{
		if (loadVisiblePeopleFailedEvent != null)
		{
			loadVisiblePeopleFailedEvent(result);
		}
	}

	protected void _PluginCB_onGetUserImageURL(string result)
	{
		dbg("_PluginCB_onGetUserImageURL ::: " + result);
		if (getUserImageURLEvent != null)
		{
			string empty = string.Empty;
			string empty2 = string.Empty;
			try
			{
				IDictionary dictionary = MiniJSON.jsonDecode(result) as IDictionary;
				empty = ToSS(dictionary["id"]);
				empty2 = ToSS(dictionary["url"]);
			}
			catch (Exception)
			{
				log("_PluginCB_onGetUserImageURL illegal format of json: " + result);
				empty = string.Empty;
				empty2 = string.Empty;
			}
			getUserImageURLEvent(empty, empty2);
		}
	}

	protected void _PluginCB_onInteractivePostSucceeded(string result)
	{
		if (interactivePostSucceededEvent != null)
		{
			interactivePostSucceededEvent(result);
		}
	}

	protected void _PluginCB_onInteractivePostFailed(string result)
	{
		if (interactivePostFailedEvent != null)
		{
			interactivePostFailedEvent(result);
		}
	}
}
