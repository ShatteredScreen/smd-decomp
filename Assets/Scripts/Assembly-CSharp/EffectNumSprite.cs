using System;
using System.Collections;
using UnityEngine;

public class EffectNumSprite : MonoBehaviour
{
	public enum STEP
	{
		NONE = 0,
		ON = 1,
		OFF = 2,
		SCALE_UP = 3,
		SCALE_DOWN = 4
	}

	public UISprite mSprite;

	private STEP mStep;

	private float mEffectAngle;

	private int mNum;

	private float mBaseScale;

	private bool mEffectBusy;

	private static readonly string[] NumNameTbl = new string[10] { "num0", "num1", "num2", "num3", "num4", "num5", "num6", "num7", "num8", "num9" };

	private string[] mImageTbl;

	private float mMaxScale = 1f;

	private bool mIsShake;

	private void Awake()
	{
		mSprite = GetComponent<UISprite>();
		mBaseScale = 1f;
		mImageTbl = NumNameTbl;
	}

	private void Start()
	{
		mStep = STEP.NONE;
		mEffectAngle = 0f;
	}

	private void Update()
	{
		switch (mStep)
		{
		case STEP.NONE:
			break;
		case STEP.ON:
		{
			mEffectAngle += Time.deltaTime * 360f;
			if (mEffectAngle >= 90f)
			{
				mEffectAngle = 90f;
				mStep = STEP.NONE;
			}
			float num2 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = 0f - Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			Util.SetSpriteScale(mSprite, new Vector3(mBaseScale + num2 * 0.4f, mBaseScale + num3 * 0.4f, 1f));
			break;
		}
		case STEP.OFF:
			break;
		case STEP.SCALE_UP:
		{
			mEffectAngle += Time.deltaTime * 180f;
			if (mEffectAngle >= 90f)
			{
				mEffectAngle = 90f;
				mStep = STEP.SCALE_DOWN;
			}
			float num = mMaxScale - mBaseScale;
			float num2 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = Mathf.Sin(mEffectAngle * ((float)Math.PI / 180f));
			num2 = mBaseScale + num2 * num;
			num3 = mBaseScale + num3 * num;
			Util.SetSpriteScale(mSprite, new Vector3(num2, num3, 1f));
			break;
		}
		case STEP.SCALE_DOWN:
		{
			mEffectAngle += Time.deltaTime * 180f;
			if (mEffectAngle >= 90f)
			{
				mEffectAngle = 90f;
				mStep = STEP.OFF;
			}
			float num = mMaxScale - mBaseScale;
			float num2 = Mathf.Cos(mEffectAngle * ((float)Math.PI / 180f));
			float num3 = Mathf.Cos(mEffectAngle * ((float)Math.PI / 180f));
			num2 = mBaseScale + num2 * num;
			num3 = mBaseScale + num3 * num;
			Util.SetSpriteScale(mSprite, new Vector3(num2, num3, 1f));
			break;
		}
		}
	}

	public void SetNum(int num)
	{
		mSprite.spriteName = mImageTbl[num];
		mNum = num;
	}

	public void ChangeNum(int num)
	{
		if (mNum != num)
		{
			mSprite.spriteName = mImageTbl[num];
			mNum = num;
			StartCoroutine(ChangeEffect());
		}
	}

	public void SetBaseScale(float scale)
	{
		mBaseScale = scale;
		Util.SetSpriteScale(mSprite, new Vector3(mBaseScale, mBaseScale, 1f));
	}

	public void SetImageNameTable(string[] a_table)
	{
		mImageTbl = a_table;
		SetNum(mNum);
	}

	public void ChangeNum(int num, float a_max, bool a_isSkipUp, bool a_isShake)
	{
		if (mNum != num)
		{
			mSprite.spriteName = mImageTbl[num];
			mNum = num;
		}
		if (a_isSkipUp)
		{
			mStep = STEP.SCALE_DOWN;
		}
		else
		{
			mStep = STEP.SCALE_UP;
		}
		mEffectAngle = 0f;
		mMaxScale = a_max;
		mIsShake = a_isShake;
	}

	private IEnumerator ChangeEffect()
	{
		if (!mEffectBusy)
		{
			mEffectBusy = true;
			Util.TriFunc triFunc = Util.CreateTriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
			bool loop = true;
			while (loop)
			{
				loop = !triFunc.AddDegree(Time.deltaTime * 360f);
				float xscale = mBaseScale;
				Util.SetSpriteScale(scale: new Vector3(xscale, mBaseScale + 1f - triFunc.Sin(), 1f), sprite: mSprite);
				yield return 0;
			}
			mEffectBusy = false;
		}
	}

	private void OnEnable()
	{
		if (mEffectBusy)
		{
			mEffectBusy = false;
			if (mSprite != null)
			{
				Util.SetSpriteScale(mSprite, new Vector3(mBaseScale, mBaseScale, 1f));
			}
		}
	}
}
