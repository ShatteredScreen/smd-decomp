using UnityEngine;

public class RemainTimeDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed();

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private GameStateManager mGameState;

	private OnDialogClosed mCallback;

	private UILabel mRemainFriendHelpTime;

	private UILabel mRemainDailyEventTime;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			UpdateTime();
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		float num = 0f;
		float num2 = 0f;
		int num3 = 0;
		UISpriteData uISpriteData = null;
		UIFont uIFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("LimitTimer_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		int remainFriendHelpTime = GetRemainFriendHelpTime();
		int remainDailyEventTime = GetRemainDailyEventTime();
		num2 = ((remainFriendHelpTime < 0 || remainDailyEventTime < 0) ? 0f : 40f);
		num3 = remainFriendHelpTime;
		if (num3 >= 0)
		{
			GameObject gameObject = Util.CreateGameObject("FriendHelp", base.gameObject);
			gameObject.transform.localPosition = new Vector3(0f, num2, 0f);
			UISprite uISprite = Util.CreateSprite("Instruction", gameObject, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(510, 120);
			UILabel uILabel = Util.CreateLabel("Title", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("LimitTimer_FriendHelp"), mBaseDepth + 3, new Vector3(40f, 20f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uISprite = Util.CreateSprite("Icon", gameObject, image);
			Util.SetSpriteInfo(uISprite, "icon_friendHelp", mBaseDepth + 3, new Vector3(-140f, 0f, 0f), Vector3.one, false);
			uISprite = Util.CreateSprite("LimitBase", gameObject, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel9", mBaseDepth + 3, new Vector3(40f, -20f, 0f), Vector3.one, false);
			uILabel = Util.CreateLabel("LimitTime", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 4, new Vector3(40f, -20f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.SetDimensions(170, 36);
			UpdateTimeLabel(uILabel, num3);
			mRemainFriendHelpTime = uILabel;
			num2 += -130f;
		}
		num3 = remainDailyEventTime;
		if (num3 >= 0)
		{
			GameObject gameObject2 = Util.CreateGameObject("DailyEvent", base.gameObject);
			gameObject2.transform.localPosition = new Vector3(0f, num2, 0f);
			UISprite uISprite = Util.CreateSprite("Instruction", gameObject2, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(510, 120);
			UILabel uILabel = Util.CreateLabel("Title", gameObject2, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("LimitTimer_DairyEvent"), mBaseDepth + 3, new Vector3(40f, 20f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uISprite = Util.CreateSprite("Icon0", gameObject2, image);
			Util.SetSpriteInfo(uISprite, "icon_item00", mBaseDepth + 3, new Vector3(-158f, 20f, 0f), Vector3.one, false);
			uISprite.SetDimensions(56, 56);
			uISprite = Util.CreateSprite("Icon1", gameObject2, image);
			Util.SetSpriteInfo(uISprite, "icon_item02", mBaseDepth + 3, new Vector3(-122f, -20f, 0f), Vector3.one, false);
			uISprite.SetDimensions(56, 56);
			uISprite = Util.CreateSprite("LimitBase", gameObject2, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel9", mBaseDepth + 3, new Vector3(40f, -20f, 0f), Vector3.one, false);
			uILabel = Util.CreateLabel("LimitTime", gameObject2, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 4, new Vector3(40f, -20f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.SetDimensions(170, 36);
			UpdateTimeLabel(uILabel, num3);
			mRemainDailyEventTime = uILabel;
			num2 += -130f;
		}
		CreateCancelButton("OnBackKeyPress");
	}

	private int GetRemainFriendHelpTime()
	{
		return mGame.GetRemainFriendHelpTime();
	}

	private int GetRemainDailyEventTime()
	{
		return mGame.GetRemainDailyEventTime();
	}

	private void UpdateTime()
	{
		int num = 0;
		if (mRemainFriendHelpTime != null)
		{
			num = GetRemainFriendHelpTime();
			if (num < 0)
			{
				num = 0;
			}
			UpdateTimeLabel(mRemainFriendHelpTime, num);
		}
		if (mRemainDailyEventTime != null)
		{
			num = GetRemainDailyEventTime();
			if (num < 0)
			{
				num = 0;
			}
			UpdateTimeLabel(mRemainDailyEventTime, num);
		}
	}

	private void UpdateTimeLabel(UILabel label, int timeSeconds)
	{
		int num = timeSeconds / 3600;
		timeSeconds %= 3600;
		int num2 = timeSeconds / 60;
		int num3 = timeSeconds % 60;
		string text = ((num >= 1) ? string.Format("{0:00}:{1:00}", Mathf.Min(99, num), num2, num3) : string.Format("{1:00}:{2:00}", Mathf.Min(99, num), num2, num3));
		label.text = text;
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
