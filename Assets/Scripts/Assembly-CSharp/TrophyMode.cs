public enum TrophyMode
{
	NONE = -1,
	MISSION = 0,
	CLEARED_MISSION = 1,
	EXCHANGE = 2
}
