using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextSeasonNoticeDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0
	}

	public enum STATE
	{
		INIT = 0,
		ANIME_WAIT = 1,
		MAIN = 2,
		WAIT = 3
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private OnDialogClosed mCallback;

	private UITexture mTexture;

	private Live2DRender mL2DRender;

	private List<CompanionData> mCompanionDataList = new List<CompanionData>();

	private UISprite[] mShadow;

	private UIButton mButton;

	private Live2DInstance[] l2dInst;

	private float mStateTime;

	private bool[] mHitAnimationFinished;

	private bool[] mLoadedL2D;

	private int mTextureHeight = 568;

	private int mTextureWidth = 568;

	private new string mLoadImageAtlas;

	private ResImage mResImage;

	private Def.SERIES mNowSeries;

	private float mCharaPos2;

	private Def.TUTORIAL_INDEX mNextIndex;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(int chara = 16, int chara2 = -1, int chara3 = -1)
	{
		CompanionData item = mGame.mCompanionData[chara];
		mCompanionDataList.Add(item);
		if (chara2 != -1)
		{
			mCharaPos2 = 250f;
			item = mGame.mCompanionData[chara2];
			mCompanionDataList.Add(item);
		}
		if (chara3 != -1)
		{
			item = mGame.mCompanionData[chara3];
			mCompanionDataList.Add(item);
		}
		mLoadImageAtlas = "HUDNOTICE";
		mOpenSeEnable = false;
		mNowSeries = mGame.mPlayer.Data.CurrentSeries;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (mL2DRender != null)
			{
				mState.Change(STATE.ANIME_WAIT);
			}
			break;
		case STATE.ANIME_WAIT:
		{
			if (l2dInst == null)
			{
				break;
			}
			bool flag = true;
			for (int i = 0; i < l2dInst.Length; i++)
			{
				if (!mLoadedL2D[i])
				{
					flag = false;
					continue;
				}
				mHitAnimationFinished[i] = l2dInst[i].IsAnimationFinished();
				if (!mHitAnimationFinished[i])
				{
					flag = false;
				}
			}
			if (flag)
			{
				mState.Change(STATE.MAIN);
			}
			break;
		}
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		mResImage = ResourceManager.LoadImageAsync(mLoadImageAtlas);
		List<ResourceInstance> list = new List<ResourceInstance>();
		for (int j = 0; j < mCompanionDataList.Count; j++)
		{
			ResLive2DAnimation anim = ResourceManager.LoadLive2DAnimationAsync(mCompanionDataList[j].GetModelKey(0));
			list.Add(anim);
		}
		if (mNowSeries == Def.SERIES.SM_S)
		{
			ResLive2DAnimation anim2 = ResourceManager.LoadLive2DAnimationAsync("PEGASUS");
			list.Add(anim2);
		}
		while (mResImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		for (int i = 0; i < mCompanionDataList.Count; i++)
		{
			while (list[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return null;
			}
		}
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.TutorialSkip(index);
			Close();
		}
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		mNextIndex = mGame.mTutorialManager.TutorialComplete(index);
		if (mNextIndex != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(mNextIndex);
		}
		else
		{
			Close();
		}
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		string empty = string.Empty;
		InitDialogFrame(DIALOG_SIZE.LARGE, "HUDNOTICE", "window_nextmap", 501, 501);
		UISprite sprite = Util.CreateSprite("TitleSprite", base.gameObject, mResImage.Image);
		Util.SetSpriteInfo(sprite, "LC_mess_nextmap", mBaseDepth + 2, new Vector3(0f, 172f, 0f), Vector3.one, false);
		if (mNowSeries == Def.SERIES.SM_FIRST)
		{
			mTextureWidth = (mTextureHeight = 568);
			mL2DRender = mGame.CreateLive2DRender(mTextureWidth, mTextureHeight);
			UIFont uIFont = GameMain.LoadFont();
			mHitAnimationFinished = new bool[1];
			l2dInst = new Live2DInstance[1];
			l2dInst[0] = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			l2dInst[0].Init(mCompanionDataList[0].GetModelKey(0), Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
			l2dInst[0].StartMotion("motions/win00.mtn", true);
			l2dInst[0].Position = new Vector3(0f, 0f, -50f);
			mL2DRender.ResisterInstance(l2dInst[0]);
			mShadow = new UISprite[1];
			if (mCompanionDataList[0].index != 10)
			{
				mShadow[0] = Util.CreateSprite("Shadow", base.gameObject, image);
				Util.SetSpriteInfo(mShadow[0], "character_foot2", mBaseDepth + 3, new Vector3(0f, -175f, 0f), new Vector3(1.2f, 1.2f, 1f), false);
			}
			mLoadedL2D = new bool[1] { true };
		}
		else if (mNowSeries == Def.SERIES.SM_R)
		{
			mTextureWidth = (mTextureHeight = 568);
			mL2DRender = mGame.CreateLive2DRender(mTextureWidth, mTextureHeight);
			UIFont uIFont2 = GameMain.LoadFont();
			mHitAnimationFinished = new bool[2];
			l2dInst = new Live2DInstance[2];
			l2dInst[0] = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			l2dInst[0].Init(mCompanionDataList[0].GetModelKey(0), Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
			l2dInst[0].StartMotion("motions/wait01.mtn", true);
			l2dInst[0].Position = new Vector3(0f - mCharaPos2, 0f, -50f);
			mL2DRender.ResisterInstance(l2dInst[0]);
			l2dInst[1] = Util.CreateGameObject("DummyPartner2", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			l2dInst[1].Init(mCompanionDataList[1].GetModelKey(0), Vector3.zero, 0.95f, Live2DInstance.PIVOT.CENTER);
			l2dInst[1].StartMotion("motions/wait01.mtn", true);
			l2dInst[1].Position = new Vector3(mCharaPos2, -26f, -50f);
			mL2DRender.ResisterInstance(l2dInst[1]);
			mShadow = new UISprite[2];
			if (mCompanionDataList[0].index != 10)
			{
				mShadow[0] = Util.CreateSprite("Shadow", base.gameObject, image);
				Util.SetSpriteInfo(mShadow[0], "character_foot2", mBaseDepth + 3, new Vector3(-94f, -198f, 0f), new Vector3(1.2f, 1.2f, 1f), false);
			}
			if (mCompanionDataList[1].index != 10)
			{
				mShadow[1] = Util.CreateSprite("Shadow2", base.gameObject, image);
				Util.SetSpriteInfo(mShadow[1], "character_foot2", mBaseDepth + 3, new Vector3(94f, -198f, 0f), new Vector3(1.2f, 1.2f, 1f), false);
			}
			mLoadedL2D = new bool[2] { true, true };
		}
		else if (mNowSeries == Def.SERIES.SM_S)
		{
			mTextureWidth = (mTextureHeight = 568);
			mL2DRender = mGame.CreateLive2DRender(mTextureWidth, mTextureHeight);
			l2dInst = new Live2DInstance[2];
			mShadow = new UISprite[2];
			mLoadedL2D = new bool[2];
			l2dInst[0] = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			l2dInst[1] = Util.CreateGameObject("DummyPartner2", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			mHitAnimationFinished = new bool[2];
			StartCoroutine(Routine_CreateL2D(0, "PEGASUS", "motions/talk00.mtn", new Vector3(-150f, 80f, -50f), 1f, new Vector3(-44f, -178f, 0f), image));
			StartCoroutine(Routine_CreateL2D(1, mCompanionDataList[0].GetModelKey(0), "motions/wait00.mtn", new Vector3(250f, -56f, -55f), 0.8f, new Vector3(95f, -178f, 0f), image, mCompanionDataList[0].index));
		}
		else if (mNowSeries == Def.SERIES.SM_SS)
		{
			mTextureWidth = (mTextureHeight = 568);
			mL2DRender = mGame.CreateLive2DRender(mTextureWidth, mTextureHeight);
			l2dInst = new Live2DInstance[mCompanionDataList.Count];
			mShadow = new UISprite[mCompanionDataList.Count];
			mLoadedL2D = new bool[3];
			mHitAnimationFinished = new bool[mCompanionDataList.Count];
			float[] array = new float[3] { 64f, 62f, 60f };
			for (int i = 0; i < mCompanionDataList.Count; i++)
			{
				l2dInst[i] = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
				StartCoroutine(Routine_CreateL2D(i, mCompanionDataList[i].GetModelKey(0), "motions/wait01.mtn", new Vector3(-350 + i * 350, 1136f * (array[i] / array[1] - 1f) / 2f, -55f), 0.91f * (array[i] / array[1]), new Vector3(-132 + i * 132, -185f, -50f), image, mCompanionDataList[i].index, 1f * (array[i] / array[1])));
			}
		}
		TutorialStart(Def.TUTORIAL_INDEX.R_SEASON_NOTICE);
		mGame.PlaySe("VOICE_006", -1, 1f, 0.6f);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		if (mTexture != null)
		{
			Object.Destroy(mTexture.gameObject);
			mTexture = null;
		}
		if (mL2DRender != null)
		{
			Object.Destroy(mL2DRender.gameObject);
			mL2DRender = null;
		}
		if (mShadow != null)
		{
			for (int i = 0; i < mShadow.Length; i++)
			{
				Object.Destroy(mShadow[i].gameObject);
				mShadow[i] = null;
			}
			mShadow = null;
		}
		base.Close();
	}

	public override void OnOpening()
	{
		base.OnOpening();
		mGame.PlaySe("SE_PARTNER_GET", -1);
		mTexture = Util.CreateGameObject("Texture", base.gameObject).AddComponent<UITexture>();
		mTexture.mainTexture = mL2DRender.RenderTexture;
		mTexture.SetDimensions(mTextureWidth, mTextureHeight);
		mTexture.depth = mBaseDepth + 5;
		mTexture.transform.localScale = new Vector3(0.75f, 0.75f, 1f);
		if (mNowSeries == Def.SERIES.SM_FIRST)
		{
			mTexture.transform.localPosition = new Vector3(0f, 28f, 0f);
		}
		else if (mNowSeries == Def.SERIES.SM_R)
		{
			mTexture.transform.localPosition = new Vector3(0f, 5f, 0f);
		}
	}

	public override void OnOpenFinished()
	{
		mGame.Network_NicknameEdit(string.Empty);
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private IEnumerator Routine_CreateL2D(int index, string model_path, string motion_key, Vector3 position, float scale, Vector3 shadow_position, BIJImage shadow_atlas, int companion_index = -1, float shadow_scale = 1.2f)
	{
		l2dInst[index].Init(model_path, Vector3.zero, scale, Live2DInstance.PIVOT.CENTER);
		l2dInst[index].Position = position;
		while (!l2dInst[index].IsLoadFinished())
		{
			yield return null;
		}
		l2dInst[index].StartMotion(motion_key, true);
		mL2DRender.ResisterInstance(l2dInst[index]);
		mShadow[index] = null;
		if (companion_index != 10)
		{
			mShadow[index] = Util.CreateSprite("Shadow", base.gameObject, shadow_atlas);
			Util.SetSpriteInfo(mShadow[index], "character_foot2", mBaseDepth + 3, shadow_position, new Vector3(shadow_scale, shadow_scale, 1f), false);
		}
		mLoadedL2D[index] = true;
	}
}
