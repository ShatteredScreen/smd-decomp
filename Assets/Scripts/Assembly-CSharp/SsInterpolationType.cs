public enum SsInterpolationType
{
	None = 0,
	Linear = 1,
	Hermite = 2,
	Bezier = 3,
	Num = 4
}
