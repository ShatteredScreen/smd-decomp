using UnityEngine;

public class DailyEventTimer : PartsChangedSprite
{
	private enum STATE
	{
		INIT = 0,
		MAIN = 1
	}

	public enum MODE
	{
		HHMMSS = 0,
		HHMM = 1,
		MMSS = 2
	}

	private GameMain mGame;

	private STATE mState;

	private MODE mMode = MODE.MMSS;

	private int[] mTime;

	private float mBlinkTime;

	private string mColon;

	private static readonly string[] DIGITS = new string[10] { "sb_stage_num00", "sb_stage_num01", "sb_stage_num02", "sb_stage_num03", "sb_stage_num04", "sb_stage_num05", "sb_stage_num06", "sb_stage_num07", "sb_stage_num08", "sb_stage_num09" };

	public void Init(MODE mode, int playCount, Vector3 pos, int sortingOrder)
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mMode = mode;
		if (mMode == MODE.HHMMSS)
		{
			_animationName = "DAILY_TIMER2";
		}
		else
		{
			_animationName = "DAILY_TIMER";
		}
		BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
		Atlas = image;
		mPlayCount = playCount;
		base.transform.localPosition = pos;
		if (sortingOrder > 0)
		{
			MeshRenderer component = GetComponent<MeshRenderer>();
			component.sortingOrder = sortingOrder;
		}
		mBlinkTime = 0f;
		mColon = "null";
		mState = STATE.INIT;
	}

	public void InitHHMM(int playCount, Vector3 pos)
	{
		Init(MODE.HHMM, playCount, pos, 0);
	}

	public void InitMMDD(int playCount, Vector3 pos)
	{
		Init(MODE.MMSS, playCount, pos, 0);
	}

	public void InitHHMMDD(int playCount, Vector3 pos, int sortingOrder)
	{
		Init(MODE.HHMMSS, playCount, pos, sortingOrder);
	}

	private new void Update()
	{
		switch (mState)
		{
		case STATE.INIT:
			OnStateInit();
			mState = STATE.MAIN;
			break;
		case STATE.MAIN:
			OnStateMain();
			break;
		}
	}

	private void OnStateInit()
	{
		Atlas = ResourceManager.LoadImage("MAPCOMMON").Image;
		ChangeAnime(_animationName, new CHANGE_PART_INFO[0], false, mPlayCount, mAnimationFinished);
	}

	private void OnStateMain()
	{
		int num = mGame.GetRemainDailyEventTime();
		if (num < 0)
		{
			num = 0;
		}
		int[] array = UpdateTime(num);
		string text = mColon;
		mBlinkTime += Time.deltaTime;
		if (Mathf.FloorToInt(mBlinkTime) % 2 == 0)
		{
			mColon = "null";
		}
		else
		{
			mColon = "dtime_colon";
		}
		if (mTime != null && string.Compare(text, mColon) == 0)
		{
			if (mMode == MODE.HHMMSS)
			{
				if (array[0] == mTime[0] && array[1] == mTime[1] && array[2] == mTime[2] && array[3] == mTime[3] && array[4] == mTime[4] && array[5] == mTime[5])
				{
					return;
				}
			}
			else if (mMode == MODE.HHMM)
			{
				if (array[0] == mTime[0] && array[1] == mTime[1] && array[2] == mTime[2] && array[3] == mTime[3])
				{
					return;
				}
			}
			else if (mMode == MODE.MMSS && array[2] == mTime[2] && array[3] == mTime[3] && array[4] == mTime[4] && array[5] == mTime[5])
			{
				return;
			}
		}
		mTime = array;
		CHANGE_PART_INFO[] changePartsArray = new CHANGE_PART_INFO[0];
		MODE mODE = mMode;
		if (mODE == MODE.HHMM && mTime[0] == 0 && mTime[1] == 0)
		{
			mODE = MODE.MMSS;
		}
		switch (mODE)
		{
		case MODE.HHMMSS:
			changePartsArray = new CHANGE_PART_INFO[8]
			{
				new CHANGE_PART_INFO
				{
					partName = "change_000",
					spriteName = DigitImageName(mTime[0]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_001",
					spriteName = DigitImageName(mTime[1]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_002",
					spriteName = text,
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_003",
					spriteName = DigitImageName(mTime[2]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_004",
					spriteName = DigitImageName(mTime[3]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_005",
					spriteName = text,
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_006",
					spriteName = DigitImageName(mTime[4]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_007",
					spriteName = DigitImageName(mTime[5]),
					centerOffset = Vector2.zero,
					enableOffset = false
				}
			};
			break;
		case MODE.HHMM:
			changePartsArray = new CHANGE_PART_INFO[5]
			{
				new CHANGE_PART_INFO
				{
					partName = "change_000",
					spriteName = DigitImageName(mTime[0]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_001",
					spriteName = DigitImageName(mTime[1]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_002",
					spriteName = text,
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_003",
					spriteName = DigitImageName(mTime[2]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_004",
					spriteName = DigitImageName(mTime[3]),
					centerOffset = Vector2.zero,
					enableOffset = false
				}
			};
			break;
		case MODE.MMSS:
			changePartsArray = new CHANGE_PART_INFO[5]
			{
				new CHANGE_PART_INFO
				{
					partName = "change_000",
					spriteName = DigitImageName(mTime[2]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_001",
					spriteName = DigitImageName(mTime[3]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_002",
					spriteName = text,
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_003",
					spriteName = DigitImageName(mTime[4]),
					centerOffset = Vector2.zero,
					enableOffset = false
				},
				new CHANGE_PART_INFO
				{
					partName = "change_004",
					spriteName = DigitImageName(mTime[5]),
					centerOffset = Vector2.zero,
					enableOffset = false
				}
			};
			break;
		}
		ChangeAnime(_animationName, changePartsArray, true, mPlayCount, mAnimationFinished);
	}

	private string DigitImageName(int n)
	{
		n %= 10;
		return DIGITS[n];
	}

	private int[] UpdateTime(int timeSeconds)
	{
		int b = timeSeconds / 3600;
		timeSeconds %= 3600;
		int num = timeSeconds / 60;
		int num2 = timeSeconds % 60;
		return new int[6]
		{
			Mathf.Min(99, b) / 10,
			Mathf.Min(99, b) % 10,
			num / 10,
			num % 10,
			num2 / 10,
			num2 % 10
		};
	}
}
