using System.Collections;
using UnityEngine;

public class GetCharacterAppeal : MonoBehaviour
{
	public enum STATE
	{
		INIT = 0,
		INIT_WAIT = 1,
		START_WAIT = 2,
		MAIN = 3
	}

	public delegate IEnumerator AfterCoroutine();

	private GameMain mGame;

	private CompanionData mCompanionData;

	private int mBaseDepth;

	private bool mLvOpenFlg;

	private AfterCoroutine mAfterCoroutine;

	private bool mIsLoadCompleted;

	private int mRank = 1;

	private bool mIsUseJumpUp = true;

	private bool mIsShowJumpUp;

	private bool mIsAppealRequest;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private Partner mPartner;

	public void SetAppealStart()
	{
		mIsAppealRequest = true;
	}

	public void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	public void Start()
	{
	}

	public void Init(CompanionData a_data, int a_depth, bool a_flg, AfterCoroutine a_coroutine)
	{
		mCompanionData = a_data;
		mBaseDepth = a_depth;
		mLvOpenFlg = a_flg;
		mAfterCoroutine = a_coroutine;
		if (!mLvOpenFlg)
		{
			mRank = 1;
		}
		else
		{
			mRank = 2;
		}
	}

	public void SetRank(int a_rank)
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			mRank = a_rank;
		}
	}

	public void SetJumpUp(bool a_flg, bool a_show)
	{
		mIsUseJumpUp = a_flg;
		mIsShowJumpUp = a_show;
	}

	public IEnumerator BuildResource()
	{
		mIsLoadCompleted = true;
		yield break;
	}

	public void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mIsLoadCompleted = false;
			StartCoroutine(BuildResource());
			mState.Change(STATE.INIT_WAIT);
			break;
		case STATE.INIT_WAIT:
			if (mIsLoadCompleted)
			{
				mState.Change(STATE.START_WAIT);
			}
			break;
		case STATE.START_WAIT:
			if (mIsAppealRequest)
			{
				mIsAppealRequest = false;
				StartCoroutine(Live2DAction());
				mState.Change(STATE.MAIN);
			}
			break;
		}
		mState.Update();
	}

	public IEnumerator Live2DAction()
	{
		yield return StartCoroutine(StartSkillEffect(mRank));
		if (mAfterCoroutine != null)
		{
			yield return StartCoroutine(mAfterCoroutine());
		}
		Object.Destroy(base.gameObject);
	}

	private IEnumerator StartSkillEffect(int rank)
	{
		GameObject skillParent = SkillEffectPlayer.CreateDemoCamera();
		mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		mPartner.Init(mCompanionData.index, new Vector3(0f, 10f, 0f), 1f, 3, skillParent, false, 0);
		while (!mPartner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
		bool skillWait = true;
		SkillEffectPlayer skillEffect = Util.CreateGameObject("SkillManager", skillParent).AddComponent<SkillEffectPlayer>();
		skillEffect.Init(mPartner, rank, skillParent, SkillEffectPlayer.MODE.REWARD, delegate
		{
			skillWait = false;
		});
		while (skillWait)
		{
			yield return 0;
		}
		Object.Destroy(skillEffect.gameObject);
		Object.Destroy(mPartner.gameObject);
		Object.Destroy(skillParent);
		yield return new WaitForSeconds(0.5f);
	}
}
