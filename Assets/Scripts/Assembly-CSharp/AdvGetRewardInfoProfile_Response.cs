using System;
using System.Collections.Generic;

public class AdvGetRewardInfoProfile_Response : ICloneable
{
	[MiniJSONAlias("rewards")]
	public List<AdvRewardData> RewardDataList { get; set; }

	[MiniJSONAlias("received_rewards")]
	public List<AdvRewardData> Received_List { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGetRewardInfoProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGetRewardInfoProfile_Response;
	}
}
