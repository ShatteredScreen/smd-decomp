using System;

public class ConnectUserAuth : ConnectBase
{
	protected override void InitServerFlg()
	{
		GameMain.mUserAuthCheckDone = false;
		GameMain.mUserAuthSucceed = false;
		GameMain.mUserAuthChanged = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mUserAuthCheckDone = true;
		GameMain.mUserAuthSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mUserAuthCheckDone = true;
		GameMain.mUserAuthSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			mGame.mUserUniqueID = new UserAuthResponse();
			mGame.mUserUniqueID.UUID = 100;
			mGame.mUserUniqueID.HiveID = 83;
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetAuthTime, DateTime.Now);
		long gET_AUTHUUID_FREQ = GameMain.GET_AUTHUUID_FREQ;
		if (gET_AUTHUUID_FREQ > 0 && num < gET_AUTHUUID_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		InitServerFlg();
		if (!Server.UserAuthCheck())
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetUserAuthFromResponse();
		bool flag = false;
		bool flag2 = true;
		if (mParam != null)
		{
			ConnectAuthParam connectAuthParam = mParam as ConnectAuthParam;
			if (connectAuthParam != null && !connectAuthParam.IsUserTransferedCheck)
			{
				flag2 = false;
			}
		}
		if (flag2 && GameMain.mUserAuthTransfered)
		{
			flag = true;
		}
		if (!flag)
		{
			base.StateConnectFinish();
			return;
		}
		ServerFlgFailed();
		OnTransferedNoticeDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM.GIVEUP, 0);
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
			if (a_code == 90)
			{
				UserDataInfo.Instance.ReCreateAuthKey();
				mState.Change(STATE.START);
				return false;
			}
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}

	private void OnTransferedNoticeDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		mConnectResultKind = 13;
		mState.Change(STATE.FINISH);
	}
}
