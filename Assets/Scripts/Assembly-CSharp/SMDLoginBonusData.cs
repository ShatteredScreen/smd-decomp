using System.Collections.Generic;

public class SMDLoginBonusData
{
	public List<GetLoginBonusResponse> login_bonus { get; set; }

	public long server_time { get; set; }

	public bool HasLoginData
	{
		get
		{
			return login_bonus != null && login_bonus.Count > 0;
		}
	}

	public void FinishedConnect()
	{
		if (login_bonus != null)
		{
			for (int i = 0; i < login_bonus.Count; i++)
			{
				login_bonus[i].Convert();
			}
		}
	}

	public void Reset()
	{
		if (login_bonus != null)
		{
			login_bonus.Clear();
		}
		server_time = 0L;
	}

	public List<int> GetLoginBonusCategoryList()
	{
		List<int> list = null;
		if (login_bonus != null)
		{
			list = new List<int>();
			for (int i = 0; i < login_bonus.Count; i++)
			{
				list.Add(login_bonus[i].LoginID);
			}
		}
		return list;
	}

	public void RemoveLoginBonusData(int a_loginID)
	{
		GetLoginBonusResponse loginBonus = GetLoginBonus(a_loginID);
		if (loginBonus != null)
		{
			login_bonus.Remove(loginBonus);
		}
	}

	public void RemoveLoginBonusDataByIndex(int a_index)
	{
		if (login_bonus != null && login_bonus.Count > a_index)
		{
			login_bonus.RemoveAt(a_index);
		}
	}

	public GetLoginBonusResponse GetLoginBonusByIndex(int a_index)
	{
		if (login_bonus == null)
		{
			return null;
		}
		if (login_bonus.Count <= a_index)
		{
			return null;
		}
		return login_bonus[a_index];
	}

	public GetLoginBonusResponse GetLoginBonus(int a_loginID)
	{
		if (login_bonus == null)
		{
			return null;
		}
		return login_bonus.Find((GetLoginBonusResponse a) => a.LoginID.CompareTo(a_loginID) == 0);
	}

	public List<LoginBonusRewardData> GetLoginBonusRewardList(int a_loginID)
	{
		GetLoginBonusResponse loginBonus = GetLoginBonus(a_loginID);
		if (loginBonus == null)
		{
			return null;
		}
		return loginBonus.RewardList;
	}

	public string GetLoginBonusImageURL(int a_loginID)
	{
		GetLoginBonusResponse loginBonus = GetLoginBonus(a_loginID);
		if (loginBonus == null)
		{
			return string.Empty;
		}
		return loginBonus.ImageURL;
	}
}
