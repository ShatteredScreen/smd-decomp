using System.Collections.Generic;
using UnityEngine;

public class ItemPossessManager : MonoBehaviour
{
	private static object mInstanceLock = new object();

	private static ItemPossessManager mInstance = null;

	private bool mIsTerminating;

	public static ItemPossessManager Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					if ((bool)gameObject)
					{
						mInstance = (ItemPossessManager)gameObject.GetComponent(typeof(ItemPossessManager));
					}
				}
				return mInstance;
			}
		}
	}

	private void OnApplicationPause(bool pause)
	{
		if (pause)
		{
			SendItemPossessionCount();
		}
	}

	private void OnDestroy()
	{
		if (!mIsTerminating)
		{
			mIsTerminating = true;
			SendItemPossessionCount();
		}
	}

	private void OnApplicationQuit()
	{
		if (!mIsTerminating)
		{
			mIsTerminating = true;
			SendItemPossessionCount();
		}
	}

	public void SendItemPossessionCount()
	{
		List<string> list = new List<string>();
		List<string> list2 = new List<string>();
		List<string> list3 = new List<string>();
		string lvl_array = string.Empty;
		Def.CONSUME_ID[] array = new Def.CONSUME_ID[13]
		{
			Def.CONSUME_ID.HEART,
			Def.CONSUME_ID.ROADBLOCK_TICKET,
			Def.CONSUME_ID.ROADBLOCK_KEY,
			Def.CONSUME_ID.HEART_PIECE,
			Def.CONSUME_ID.GROWUP,
			Def.CONSUME_ID.GROWUP_PIECE,
			Def.CONSUME_ID.TROPHY,
			Def.CONSUME_ID.ROADBLOCK_KEY_R,
			Def.CONSUME_ID.ROADBLOCK_KEY_S,
			Def.CONSUME_ID.ROADBLOCK_KEY_SS,
			Def.CONSUME_ID.ROADBLOCK_KEY_STARS,
			Def.CONSUME_ID.OLDEVENT_KEY,
			Def.CONSUME_ID.ROADBLOCK_KEY_GF00
		};
		Def.ADV_CONSUME_ID[] array2 = new Def.ADV_CONSUME_ID[6]
		{
			Def.ADV_CONSUME_ID.NONE,
			Def.ADV_CONSUME_ID.POWER_P,
			Def.ADV_CONSUME_ID.SKILL_LEVELUP,
			Def.ADV_CONSUME_ID.GASYA_TICKET_NORMAL,
			Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM,
			Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM_SUB
		};
		Def.BOOSTER_KIND[] array3 = new Def.BOOSTER_KIND[14]
		{
			Def.BOOSTER_KIND.WAVE_BOMB,
			Def.BOOSTER_KIND.TAP_BOMB2,
			Def.BOOSTER_KIND.RAINBOW,
			Def.BOOSTER_KIND.ADD_SCORE,
			Def.BOOSTER_KIND.SKILL_CHARGE,
			Def.BOOSTER_KIND.SELECT_ONE,
			Def.BOOSTER_KIND.SELECT_COLLECT,
			Def.BOOSTER_KIND.COLOR_CRUSH,
			Def.BOOSTER_KIND.BLOCK_CRUSH,
			Def.BOOSTER_KIND.MUGEN_HEART15,
			Def.BOOSTER_KIND.MUGEN_HEART30,
			Def.BOOSTER_KIND.MUGEN_HEART60,
			Def.BOOSTER_KIND.PAIR_MAKER,
			Def.BOOSTER_KIND.ALL_CRUSH
		};
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		Def.CONSUME_ID[] array4 = array;
		foreach (Def.CONSUME_ID cONSUME_ID in array4)
		{
			list.Add((cONSUME_ID != Def.CONSUME_ID.HEART) ? instance.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, (int)cONSUME_ID).ToString() : instance.mPlayer.LifeCount.ToString());
		}
		Def.ADV_CONSUME_ID[] array5 = array2;
		for (int j = 0; j < array5.Length; j++)
		{
			switch (array5[j])
			{
			case Def.ADV_CONSUME_ID.NONE:
				list2.Add(instance.mPlayer.AdvStamina.ToString());
				break;
			case Def.ADV_CONSUME_ID.GASYA_TICKET_NORMAL:
				list2.Add(instance.mPlayer.AdvSaveData.mGachaTicket_Normal.ToString());
				break;
			case Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM:
				list2.Add(instance.mPlayer.AdvSaveData.mGachaTicket_Premium.ToString());
				break;
			case Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM_SUB:
				list2.Add(instance.mPlayer.AdvSaveData.mGachaTicket_PremiumAuxiliary.ToString());
				break;
			case Def.ADV_CONSUME_ID.POWER_P:
				list2.Add(instance.mPlayer.AdvSaveData.mChargeExp.ToString());
				break;
			case Def.ADV_CONSUME_ID.SKILL_LEVELUP:
				list2.Add(instance.mPlayer.AdvSaveData.mSkillLvUpItemNum.ToString());
				break;
			}
		}
		Def.BOOSTER_KIND[] array6 = array3;
		foreach (Def.BOOSTER_KIND bOOSTER_KIND in array6)
		{
			int num = 0;
			switch (bOOSTER_KIND)
			{
			default:
				num = instance.mPlayer.GetBoosterNum(bOOSTER_KIND);
				break;
			case Def.BOOSTER_KIND.MUGEN_HEART15:
				if (instance.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START && instance.mPlayer.Data.mUseMugenHeartSetTime == Def.MugenHeartLimitTimeData[Def.BOOSTER_KIND.MUGEN_HEART15])
				{
					num = 1;
				}
				break;
			case Def.BOOSTER_KIND.MUGEN_HEART30:
				if (instance.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START && instance.mPlayer.Data.mUseMugenHeartSetTime == Def.MugenHeartLimitTimeData[Def.BOOSTER_KIND.MUGEN_HEART30])
				{
					num = 1;
				}
				break;
			case Def.BOOSTER_KIND.MUGEN_HEART60:
				if (instance.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START && instance.mPlayer.Data.mUseMugenHeartSetTime == Def.MugenHeartLimitTimeData[Def.BOOSTER_KIND.MUGEN_HEART60])
				{
					num = 1;
				}
				break;
			}
			list3.Add(num.ToString());
		}
		int ver = 0;
		int xp = 0;
		int cur_series = 0;
		int cur_stage = 0;
		ServerCram.GetAdvSaveDataInfo(instance.mPlayer, instance.mPlayer.AdvSaveData, ref ver, ref lvl_array, ref xp, ref cur_series, ref cur_stage);
		ServerCram.ItemPossessionNumber(string.Join(",", list.ToArray()), string.Join(",", list2.ToArray()), string.Join(",", list3.ToArray()), ServerCram.GetLevelArrayString(instance.mPlayer), lvl_array);
	}
}
