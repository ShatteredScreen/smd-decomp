public interface SsKeyFrameInterface
{
	SsKeyValueType ValueType { get; set; }

	int Time { get; set; }

	object ObjectValue { get; set; }

	SsCurveParams Curve { get; set; }

	bool EqualsValue(SsKeyFrameInterface rhs);
}
