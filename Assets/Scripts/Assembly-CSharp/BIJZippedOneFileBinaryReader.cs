using System;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;

public class BIJZippedOneFileBinaryReader : BIJBinaryReader
{
	private const string CONTENT = "._CONTENT_";

	private FileStream mStream;

	private bool mCloseOnDelete;

	private ZipInputStream mZipStream;

	public BIJZippedOneFileBinaryReader(string path)
		: this(path, string.Empty, Encoding.UTF8, false)
	{
	}

	public BIJZippedOneFileBinaryReader(string path, string password)
		: this(path, password, Encoding.UTF8, false)
	{
	}

	public BIJZippedOneFileBinaryReader(string path, string password, Encoding encoding)
		: this(path, password, encoding, false)
	{
	}

	public BIJZippedOneFileBinaryReader(string path, string password, Encoding encoding, bool closeOnDelete)
	{
		mStream = null;
		mCloseOnDelete = closeOnDelete;
		Reader = null;
		try
		{
			mPath = path;
			mStream = new FileStream(mPath, FileMode.Open, FileAccess.Read);
			mZipStream = new ZipInputStream(mStream);
			if (!string.IsNullOrEmpty(password))
			{
				mZipStream.Password = password;
			}
			ZipNameTransform zipNameTransform = new ZipNameTransform(string.Empty);
			string text = zipNameTransform.TransformFile("._CONTENT_");
			ZipEntry zipEntry = null;
			ZipEntry nextEntry;
			while ((nextEntry = mZipStream.GetNextEntry()) != null)
			{
				if (nextEntry.IsDirectory || string.Compare(text, nextEntry.Name, StringComparison.OrdinalIgnoreCase) != 0)
				{
					continue;
				}
				zipEntry = nextEntry;
				break;
			}
			if (zipEntry == null)
			{
				throw new FileNotFoundException("Content not found: " + text);
			}
			Reader = new BinaryReader(mZipStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	public override void Close()
	{
		base.Close();
		if (mZipStream != null)
		{
			try
			{
				mZipStream.Close();
			}
			catch (Exception)
			{
			}
			mZipStream = null;
		}
		if (mStream != null)
		{
			try
			{
				mStream.Close();
			}
			catch (Exception)
			{
			}
			mStream = null;
		}
		if (!mCloseOnDelete)
		{
			return;
		}
		try
		{
			if (!string.IsNullOrEmpty(mPath) && File.Exists(mPath))
			{
				File.Delete(mPath);
			}
		}
		catch (Exception)
		{
		}
	}
}
