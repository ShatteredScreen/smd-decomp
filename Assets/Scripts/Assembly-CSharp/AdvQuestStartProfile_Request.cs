public class AdvQuestStartProfile_Request : RequestBase
{
	[MiniJSONAlias("first_clear_reward_ids")]
	public int[] FirstRewardIDs { get; set; }

	[MiniJSONAlias("enemy_drop_reward_ids")]
	public int[] EnemyRewardIDs { get; set; }
}
