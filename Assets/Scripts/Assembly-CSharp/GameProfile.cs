using System;
using System.Collections.Generic;

public class GameProfile : ICloneable
{
	public enum UpdateKind
	{
		NONE = 0,
		CONFIRM = 1,
		FORCE = 99
	}

	public Dictionary<Def.SERIES, SeriesMaxLevel> MaxLevelList = new Dictionary<Def.SERIES, SeriesMaxLevel>();

	private long mPurchaseCountStartTime;

	private long mPurchaseCountEndTime;

	[MiniJSONAlias("log")]
	public int LogLevelNum { get; set; }

	[MiniJSONAlias("appver")]
	public int AppVer { get; set; }

	[MiniJSONAlias("update")]
	public int Update { get; set; }

	[MiniJSONAlias("timeout")]
	public int ServerTimeout { get; set; }

	[MiniJSONAlias("reqtimeout")]
	public int ReqTimeout { get; set; }

	[MiniJSONAlias("iotimeout")]
	public int IOTimeout { get; set; }

	[MiniJSONAlias("mail")]
	public int MailGetCount { get; set; }

	[MiniJSONAlias("gift_receive_max")]
	public int GiftReceiveMax { get; set; }

	[MiniJSONAlias("rfriend")]
	public int RandomFriendGetCount { get; set; }

	[MiniJSONAlias("applyrepeat")]
	public int ApplyRepeatNum { get; set; }

	[MiniJSONAlias("friendhelp")]
	public int FriendHelp { get; set; }

	[MiniJSONAlias("oldpurchasedialog")]
	public bool mOldPurchaseDialog { get; set; }

	[MiniJSONAlias("campaigncheck")]
	public long mCampaignCheckFreq { get; set; }

	[MiniJSONAlias("dllisturl")]
	public string DlListURL { get; set; }

	[MiniJSONAlias("dldataver")]
	public string DlDataVer { get; set; }

	[MiniJSONAlias("dlstageurl")]
	public string DlStageURL { get; set; }

	[MiniJSONAlias("dlstagever")]
	public string DlStageVer { get; set; }

	[MiniJSONAlias("dlhash")]
	public string DlListHash { get; set; }

	[MiniJSONAlias("dlhashStage")]
	public string DlListHashStage { get; set; }

	[MiniJSONAlias("dlhashAdv")]
	public string DlListHashAdv { get; set; }

	[MiniJSONAlias("dlhashAdvFigure")]
	public string DlListHashAdvFigure { get; set; }

	[MiniJSONAlias("dlhashAdvStage")]
	public string DlListHashAdvStage { get; set; }

	[MiniJSONAlias("dlParallelNum")]
	public int DlParallelNum { get; set; }

	[MiniJSONAlias("linkbanner")]
	public LinkBannerSetting LinkBanner { get; set; }

	[MiniJSONAlias("lbcachedelete")]
	public int LBCacheDelete { get; set; }

	[MiniJSONAlias("promo_url")]
	public string PromoBaseURL { get; set; }

	[MiniJSONAlias("help_url")]
	public string HelpUrl { get; set; }

	[MiniJSONAlias("aos_url")]
	public string AoSURL { get; set; }

	[MiniJSONAlias("aosct_url")]
	public string AoSCTURL { get; set; }

	[MiniJSONAlias("tos_checktime")]
	public int ToSCheckTime { get; set; }

	[MiniJSONAlias("tos_ver")]
	public int ToSVer { get; set; }

	[MiniJSONAlias("tos_top_url")]
	public string ToSTopURL { get; set; }

	[MiniJSONAlias("tos_url")]
	public string ToSURL { get; set; }

	[MiniJSONAlias("privacy_checktime")]
	public int PrivacyCheckTime { get; set; }

	[MiniJSONAlias("privacy_ver")]
	public int PrivacyVer { get; set; }

	[MiniJSONAlias("privacy_url")]
	public string PrivacyURL { get; set; }

	[MiniJSONAlias("gdpr")]
	public GDPRSetting mGDPRSetting { get; set; }

	[MiniJSONAlias("info_cnt")]
	public int InfoCount { get; set; }

	[MiniJSONAlias("info_url")]
	public List<string> InfoURL { get; set; }

	[MiniJSONAlias("info_data_url")]
	public string InfoDataURL { get; set; }

	[MiniJSONAlias("tw_ex_url")]
	public string TwitterExternalPostURL { get; set; }

	[MiniJSONAlias("fb_ex_url")]
	public string FacebookExternalPostURL { get; set; }

	[MiniJSONAlias("tuxedword")]
	public List<int> TuxedWordList { get; set; }

	[MiniJSONAlias("maxlvl")]
	public int MaxLevel { get; set; }

	[MiniJSONAlias("maxlvls")]
	public List<SeriesMaxLevel> MaxLevels { get; set; }

	[MiniJSONAlias("gc_settings")]
	public AdvGameInfoSettings AdvSettings { get; set; }

	[MiniJSONDictionary(typeof(string), typeof(bool))]
	[MiniJSONAlias("sns_available")]
	public Dictionary<string, bool> SNSFunctions { get; set; }

	[MiniJSONDictionary(typeof(string), typeof(int))]
	[MiniJSONAlias("sns_flg")]
	public Dictionary<string, int> SNSFlags { get; set; }

	[MiniJSONAlias("purchasecount_starttime")]
	public long PurchaseCountStartTime
	{
		get
		{
			if (mPurchaseCountStartTime == 0L)
			{
				return (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Today.AddDays(-30.0));
			}
			return mPurchaseCountStartTime;
		}
		set
		{
			mPurchaseCountStartTime = value;
		}
	}

	[MiniJSONAlias("purchasecount_endtime")]
	public long PurchaseCountEndTime
	{
		get
		{
			if (mPurchaseCountEndTime == 0L)
			{
				return (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
			}
			return mPurchaseCountEndTime;
		}
		set
		{
			mPurchaseCountEndTime = value;
		}
	}

	[MiniJSONAlias("datekey")]
	public long DateKey { get; set; }

	public bool IsToSCheckTime
	{
		get
		{
			return ToSCheckTime > 0;
		}
	}

	public bool IsPrivacyCheckTime
	{
		get
		{
			return PrivacyCheckTime > 0;
		}
	}

	public int Timeout
	{
		get
		{
			if (ServerTimeout > 0)
			{
				return ServerTimeout;
			}
			return 5000;
		}
	}

	public bool EnableFriendHelp
	{
		get
		{
			return FriendHelp == 1;
		}
	}

	public UpdateKind UpdateVersionFlag
	{
		get
		{
			UpdateKind result = UpdateKind.NONE;
			try
			{
				if (Enum.IsDefined(typeof(UpdateKind), Update))
				{
					result = (UpdateKind)(int)Enum.ToObject(typeof(UpdateKind), Update);
				}
			}
			catch (Exception)
			{
			}
			return result;
		}
	}

	public string LinkBannerBaseURL
	{
		get
		{
			return PromoBaseURL + "linkbanner/images/en/";
		}
	}

	public GameProfile()
	{
		MaxLevels = new List<SeriesMaxLevel>();
		mPurchaseCountEndTime = 0L;
		mPurchaseCountStartTime = 0L;
		mOldPurchaseDialog = false;
		mCampaignCheckFreq = 10L;
		GiftReceiveMax = 10;
		PromoBaseURL = "https://smd-promo.s3.amazonaws.com/";
		InfoDataURL = null;
		mGDPRSetting = new GDPRSetting();
		DlParallelNum = 1;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public GameProfile Clone()
	{
		return MemberwiseClone() as GameProfile;
	}

	public bool IsSeriesAvailable(Def.SERIES a_series)
	{
		int maxLevel = GetMaxLevel(a_series);
		if (maxLevel > 100)
		{
			return true;
		}
		return false;
	}

	private void MakeMaxLevelList()
	{
		if (MaxLevelList.Count != 0)
		{
			return;
		}
		for (int i = 0; i < MaxLevels.Count; i++)
		{
			SeriesMaxLevel seriesMaxLevel = MaxLevels[i];
			if (seriesMaxLevel.Available == 1)
			{
				MaxLevelList.Add((Def.SERIES)seriesMaxLevel.SeriesInt, seriesMaxLevel);
			}
		}
	}

	public int GetMaxLevel(Def.SERIES a_series)
	{
		MakeMaxLevelList();
		SeriesMaxLevel value;
		if (MaxLevelList.TryGetValue(a_series, out value) && value.Available == 1)
		{
			if (value.Schedule != null)
			{
				DateTime dateTime = DateTimeUtil.Now();
				SeriesMaxLevelSchedule seriesMaxLevelSchedule = null;
				foreach (SeriesMaxLevelSchedule item in value.Schedule)
				{
					if (item != null && !(item.StartTime > dateTime) && (seriesMaxLevelSchedule == null || item.StartUnixTime >= seriesMaxLevelSchedule.StartUnixTime))
					{
						seriesMaxLevelSchedule = item;
					}
				}
				return (seriesMaxLevelSchedule == null) ? value.MaxLevel : seriesMaxLevelSchedule.MaxLevel;
			}
			return value.MaxLevel;
		}
		return 100;
	}

	public bool IsSeriesSelectAvailable()
	{
		MakeMaxLevelList();
		if (MaxLevelList.Count > 1)
		{
			return true;
		}
		return false;
	}

	public bool IsSeriesUnlocked(Def.SERIES a_series)
	{
		bool result = false;
		SeriesMaxLevel value;
		if (MaxLevelList.TryGetValue(a_series, out value) && value.Available == 1)
		{
			result = true;
		}
		return result;
	}
}
