using System;

namespace live2d.framework
{
	public class L2DTargetPoint
	{
		public const int FRAME_RATE = 30;

		private float faceTargetX;

		private float faceTargetY;

		private float faceX;

		private float faceY;

		private float faceVX;

		private float faceVY;

		private long lastTimeSec;

		public void Set(float x, float y)
		{
			faceTargetX = x;
			faceTargetY = y;
		}

		public float getX()
		{
			return faceX;
		}

		public float getY()
		{
			return faceY;
		}

		public void setX(float x)
		{
			faceX = x;
		}

		public void setY(float y)
		{
			faceY = y;
		}

		public void update()
		{
			if (lastTimeSec == 0L)
			{
				lastTimeSec = UtSystem.getUserTimeMSec();
				return;
			}
			long userTimeMSec = UtSystem.getUserTimeMSec();
			float num = (float)(userTimeMSec - lastTimeSec) * 30f / 1000f;
			lastTimeSec = userTimeMSec;
			float num2 = num * (8f / 45f) / 4.5f;
			float num3 = faceTargetX - faceX;
			float num4 = faceTargetY - faceY;
			if (num3 != 0f || num4 != 0f)
			{
				float num5 = (float)Math.Sqrt(num3 * num3 + num4 * num4);
				float num6 = 8f / 45f * num3 / num5;
				float num7 = 8f / 45f * num4 / num5;
				float num8 = num6 - faceVX;
				float num9 = num7 - faceVY;
				float num10 = (float)Math.Sqrt(num8 * num8 + num9 * num9);
				if (num10 < 0f - num2 || num10 > num2)
				{
					num8 *= num2 / num10;
					num9 *= num2 / num10;
					num10 = num2;
				}
				faceVX += num8;
				faceVY += num9;
				float num11 = 0.5f * ((float)Math.Sqrt(num2 * num2 + 16f * num2 * num5 - 8f * num2 * num5) - num2);
				float num12 = (float)Math.Sqrt(faceVX * faceVX + faceVY * faceVY);
				if (num12 > num11)
				{
					faceVX *= num11 / num12;
					faceVY *= num11 / num12;
				}
				faceX += faceVX;
				faceY += faceVY;
			}
		}
	}
}
