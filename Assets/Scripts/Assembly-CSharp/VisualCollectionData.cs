public class VisualCollectionData
{
	public short index;

	public string nameKey;

	public int accessoryId;

	public string atlasKey;

	public string imageKey;

	public int[] piceIds;

	private void Update()
	{
	}

	public void deserialize(BIJBinaryReader stream)
	{
		index = stream.ReadShort();
		nameKey = stream.ReadUTF();
		string s = stream.ReadUTF();
		accessoryId = int.Parse(s);
		atlasKey = stream.ReadUTF();
		imageKey = stream.ReadUTF();
		s = stream.ReadUTF();
		string[] array = s.Split(',');
		piceIds = new int[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			piceIds[i] = int.Parse(array[i]);
		}
	}

	public VisualCollectionData Clone()
	{
		return (VisualCollectionData)MemberwiseClone();
	}

	public void Dump()
	{
	}
}
