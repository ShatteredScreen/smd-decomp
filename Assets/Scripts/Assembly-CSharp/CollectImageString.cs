using UnityEngine;

public class CollectImageString : MonoBehaviour
{
	private UISprite[] mCollectNum;

	private UISprite[] mTargetNum;

	private UISprite mSlash;

	private float mScale;

	private static float ImageWidth = 34f;

	private static int Keta = 3;

	private static string[] numTbl = new string[10] { "35", "26", "27", "28", "29", "30", "31", "32", "33", "34" };

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void Init(int targetNum, int collectNum, int depth, float scale)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mScale = scale;
		mCollectNum = new UISprite[Keta];
		mTargetNum = new UISprite[Keta];
		for (int i = 0; i < Keta; i++)
		{
			mCollectNum[i] = Util.CreateSprite("Num", base.gameObject, image);
			Util.SetSpriteInfo(mCollectNum[i], "35", depth, Vector3.zero, new Vector3(scale, scale, 1f), false);
			mTargetNum[i] = Util.CreateSprite("Num", base.gameObject, image);
			Util.SetSpriteInfo(mTargetNum[i], "35", depth, Vector3.zero, new Vector3(scale, scale, 1f), false);
		}
		mSlash = Util.CreateSprite("Slash", base.gameObject, image);
		Util.SetSpriteInfo(mSlash, "24", depth, Vector3.zero, new Vector3(scale, scale, 1f), false);
		SetNum(targetNum, collectNum);
	}

	public void SetNum(int targetNum, int collectNum)
	{
		int num = targetNum;
		int num2 = 0;
		for (int i = 0; i < Keta; i++)
		{
			if (i != 0 && num == 0)
			{
				mTargetNum[i].enabled = false;
			}
			else
			{
				mTargetNum[i].enabled = true;
				num2++;
			}
			Util.SetSpriteImageName(mTargetNum[i], numTbl[num % 10], new Vector3(mScale, mScale, 1f), false);
			num /= 10;
		}
		float num3 = (float)num2 * ImageWidth * mScale - 5f * mScale;
		for (int j = 0; j < Keta; j++)
		{
			mTargetNum[j].transform.localPosition = new Vector3(num3, 0f, 0f);
			num3 -= ImageWidth * mScale;
		}
		num = collectNum;
		num2 = 0;
		for (int k = 0; k < Keta; k++)
		{
			if (k != 0 && num == 0)
			{
				mCollectNum[k].enabled = false;
			}
			else
			{
				mCollectNum[k].enabled = true;
				num2++;
			}
			Util.SetSpriteImageName(mCollectNum[k], numTbl[num % 10], new Vector3(mScale, mScale, 1f), false);
			num /= 10;
		}
		num3 = (0f - ImageWidth) * mScale + 5f * mScale;
		for (int l = 0; l < Keta; l++)
		{
			mCollectNum[l].transform.localPosition = new Vector3(num3, 0f, 0f);
			num3 -= ImageWidth * mScale;
		}
	}
}
