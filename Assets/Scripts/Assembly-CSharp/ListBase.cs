using System.Collections.Generic;
using UnityEngine;

public abstract class ListBase<T> : LtBs where T : ListItem, new()
{
	protected UIScrollView.Movement movement = UIScrollView.Movement.Vertical;

	public UIScrollView mListScrollView;

	public UIGrid mList;

	protected UIScrollBar mScroll;

	protected ScSpriteObject mArrowT;

	protected ScSpriteObject mArrowB;

	protected float mScrollT;

	protected float mScrollB;

	protected float mScrollPage;

	protected GameObject mScrollGuardTL;

	protected GameObject mScrollGuardBR;

	protected int mBaseDepth = 60;

	protected int mMaxPerLine = 1;

	protected Rect mWindowRect = default(Rect);

	protected Vector2 mClipSoftness = new Vector2(1f, 10f);

	protected Dictionary<GameObject, int> mMapItems = new Dictionary<GameObject, int>();

	protected List<GameObject> mItems = new List<GameObject>();

	protected UICenterOnChild mCenterOnChild;

	public UIDrawCall.Clipping mClipping = UIDrawCall.Clipping.SoftClip;

	protected UIPanel mListPanel;

	protected bool mIsContentsCreated;

	protected int mMaxDisplayCount;

	protected UIWidget.Pivot mGridPivot = UIWidget.Pivot.Top;

	public int ListBaseDepth
	{
		get
		{
			return mBaseDepth;
		}
	}

	public List<GameObject> ListItems
	{
		get
		{
			return mItems;
		}
	}

	public Rect WindowRect
	{
		set
		{
			mWindowRect = value;
		}
	}

	public UIPanel ListPanel
	{
		get
		{
			return mListPanel;
		}
	}

	public bool IsContentsCreated
	{
		get
		{
			return mIsContentsCreated;
		}
	}

	public virtual void Awake()
	{
	}

	public virtual void Start()
	{
	}

	protected abstract void CreateContents(GameObject parent, string name);

	protected virtual void CreateScrollBar(GameObject parent, string name)
	{
		GameObject gameObject = Util.CreateGameObject("Scroll", parent);
		mScroll = gameObject.AddComponent<UIScrollBar>();
	}

	protected bool IsHorizontal()
	{
		return movement == UIScrollView.Movement.Horizontal;
	}

	protected bool IsVertical()
	{
		return !IsHorizontal();
	}

	protected virtual void CreateList(GameObject parent, string name)
	{
		CreateScrollBar(parent, name);
		GameObject gameObject = Util.CreateGameObject("List", parent);
		gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
		mListPanel = gameObject.AddComponent<UIPanel>();
		mListPanel.sortingOrder = mBaseDepth;
		mListPanel.depth = mBaseDepth;
		float width = mWindowRect.width;
		float height = mWindowRect.height;
		mListPanel.clipping = mClipping;
		mListPanel.baseClipRegion = new Vector4(0f, 0f, width, height);
		mListPanel.clipSoftness = mClipSoftness;
		if (IsHorizontal())
		{
			mScrollPage = width;
			mScrollT = mWindowRect.center.x + mListPanel.finalClipRegion.x + width / 2f;
			mScrollB = mWindowRect.center.x + mListPanel.finalClipRegion.x - width / 2f;
		}
		else
		{
			mScrollPage = height;
			mScrollT = mWindowRect.center.y + mListPanel.finalClipRegion.y + height / 2f;
			mScrollB = mWindowRect.center.y + mListPanel.finalClipRegion.y - height / 2f;
		}
		mListScrollView = gameObject.AddComponent<UIScrollView>();
		mListScrollView.movement = movement;
		mListScrollView.dragEffect = UIScrollView.DragEffect.None;
		mListScrollView.restrictWithinPanel = false;
		mListScrollView.disableDragIfFits = true;
		mListScrollView.momentumAmount = 35f;
		mListScrollView.scrollWheelFactor = 1f;
		mListScrollView.showScrollBars = UIScrollView.ShowCondition.Always;
		if (IsHorizontal())
		{
			mListScrollView.horizontalScrollBar = mScroll;
		}
		else
		{
			mListScrollView.verticalScrollBar = mScroll;
		}
		GameObject gameObject2 = Util.CreateGameObject("ItemList", gameObject);
		mList = gameObject2.AddComponent<UIGrid>();
		mList.maxPerLine = mMaxPerLine;
		mList.pivot = mGridPivot;
		if (IsHorizontal())
		{
			mList.arrangement = UIGrid.Arrangement.Vertical;
			mList.cellWidth = GetCellHeight();
			mList.cellHeight = mListPanel.finalClipRegion.w / (float)mMaxPerLine;
		}
		else
		{
			mList.arrangement = UIGrid.Arrangement.Horizontal;
			mList.cellWidth = mListPanel.finalClipRegion.z / (float)mMaxPerLine;
			mList.cellHeight = GetCellHeight();
		}
		mCenterOnChild = gameObject2.AddComponent<UICenterOnChild>();
		mIsContentsCreated = true;
	}

	protected virtual int GetScrollingDeltaY()
	{
		return 1;
	}

	protected virtual int GetCellHeight()
	{
		return 150;
	}

	protected virtual void OnScrollBarChange()
	{
		bool t = mScroll.value > 0.05f;
		bool b = mScroll.value < 0.95f;
		if (mScroll.value == 0f && mMapItems != null)
		{
			float num = mScrollT - mScrollB;
			int num2 = ((!IsHorizontal()) ? ((int)(num / mList.cellHeight)) : ((int)(num / mList.cellWidth)));
			int num3 = mMapItems.Count / mMaxPerLine;
			if (mMapItems.Count % mMaxPerLine > 0)
			{
				num3++;
			}
			if (num3 <= num2)
			{
				b = false;
			}
		}
		CreateScrollArrow(t, b);
	}

	protected virtual int GetArrowOffsetY()
	{
		return 40;
	}

	protected virtual void CreateScrollArrow(bool t, bool b)
	{
	}

	protected virtual void OnScrollAdjust()
	{
		mListScrollView.UpdateScrollbars();
	}

	protected virtual void OnScrollTop(GameObject go)
	{
		float num = mScrollPage;
		if (IsHorizontal())
		{
			mListScrollView.MoveRelative(new Vector3(num, 0f, 0f));
		}
		else
		{
			mListScrollView.MoveRelative(new Vector3(0f, 0f - num, 0f));
		}
		mListScrollView.RestrictWithinBounds(true);
	}

	protected virtual void OnScrollBottom(GameObject go)
	{
		float num = mScrollPage;
		if (IsHorizontal())
		{
			mListScrollView.MoveRelative(new Vector3(0f - num, 0f, 0f));
		}
		else
		{
			mListScrollView.MoveRelative(new Vector3(0f, num, 0f));
		}
		mListScrollView.RestrictWithinBounds(true);
	}

	public virtual void ClearList()
	{
		mMapItems.Clear();
		mItems.Clear();
		UIDragScrollView[] componentsInChildren = mList.GetComponentsInChildren<UIDragScrollView>();
		UIDragScrollView[] array = componentsInChildren;
		foreach (UIDragScrollView uIDragScrollView in array)
		{
			GameMain.SafeDestroy(uIDragScrollView.gameObject);
		}
	}

	public virtual void UpdateList()
	{
		ClearList();
		T[] array = GetListItem().ToArray();
		int num = 0;
		T lastItem = (T)null;
		T[] array2 = array;
		foreach (T val in array2)
		{
			GameObject gameObject = Util.CreateGameObject("Item" + num, mList.gameObject);
			UIDragScrollView uIDragScrollView = gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView.scrollView = mListScrollView;
			mItems.Add(gameObject);
			mMapItems.Add(gameObject, num);
			CreateItem(gameObject, num, val, lastItem);
			lastItem = val;
			NGUITools.AddWidgetCollider(gameObject);
			BoxCollider component = gameObject.GetComponent<BoxCollider>();
			component.size = new Vector3(component.size.x, component.size.y, 1f);
			component.center = new Vector3(component.center.x, component.center.y, -1f);
			num++;
			if (mMaxDisplayCount > 0 && num >= mMaxDisplayCount)
			{
				break;
			}
		}
		mListScrollView.ResetPosition();
		mList.Reposition();
		mScroll.ForceUpdate();
		mListScrollView.UpdatePosition();
		OnScrollAdjust();
	}

	protected virtual List<T> GetListItem()
	{
		return new List<T>();
	}

	protected virtual void CreateItem(GameObject go, int index, T item, T lastItem)
	{
	}

	public virtual void OnDestroy()
	{
	}

	public virtual void Update()
	{
	}

	protected void SetClipSoftness(Vector2 vec)
	{
		mClipSoftness = vec;
	}

	public virtual int ListIndexOf(GameObject go)
	{
		if (mMapItems == null || mMapItems.Count < 1)
		{
			return -1;
		}
		int value = -1;
		if (!mMapItems.TryGetValue(go, out value))
		{
			return -1;
		}
		return value;
	}

	public virtual int FindListIndexOf(GameObject go)
	{
		UIDragScrollView uIDragScrollView = NGUITools.FindInParents<UIDragScrollView>(go);
		if (uIDragScrollView == null)
		{
			return -1;
		}
		return ListIndexOf(uIDragScrollView.gameObject);
	}
}
