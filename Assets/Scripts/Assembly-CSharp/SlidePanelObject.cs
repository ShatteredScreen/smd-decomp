using System.Collections.Generic;
using UnityEngine;

public class SlidePanelObject : MonoBehaviour
{
	public int mPanelIndex;

	public SlidePanelSetting mPanelSetting;

	public GameObject mParent;

	public Dictionary<string, MonoBehaviour> mChildren = new Dictionary<string, MonoBehaviour>();

	public static SlidePanelObject Make(SlidePanelSetting a_setting, int i, GameObject a_parent)
	{
		SlidePanelObject slidePanelObject = Util.CreateGameObject("SlidePanel" + i, a_parent).AddComponent<SlidePanelObject>();
		slidePanelObject.mPanelIndex = i;
		slidePanelObject.mParent = a_parent;
		slidePanelObject.mPanelSetting = a_setting;
		return slidePanelObject;
	}

	public void Load()
	{
		if (mPanelSetting.mOnLoadCallback != null)
		{
			mPanelSetting.mIsLoadFinished = false;
			StartCoroutine(mPanelSetting.mOnLoadCallback(this));
		}
		else
		{
			mPanelSetting.mIsLoadFinished = true;
		}
	}

	public void Unload()
	{
		List<string> list = new List<string>(mChildren.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			Object.Destroy(mChildren[list[i]].gameObject);
			mChildren[list[i]] = null;
		}
		mChildren.Clear();
		if (mPanelSetting.mOnUnloadCallback != null)
		{
			mPanelSetting.mOnUnloadCallback(this);
		}
		Object.Destroy(base.gameObject);
	}
}
