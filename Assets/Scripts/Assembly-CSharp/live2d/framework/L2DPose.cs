using System.Collections.Generic;
using System.Text;

namespace live2d.framework
{
	public class L2DPose
	{
		protected List<L2DPartsParam[]> partsGroupList;

		private long lastTime;

		private ALive2DModel lastModel;

		public L2DPose()
		{
			partsGroupList = new List<L2DPartsParam[]>();
		}

		public void addPartsGroup(L2DPartsParam[] partsGroup)
		{
			partsGroupList.Add(partsGroup);
		}

		public void addPartsGroup(string[] idGroup)
		{
			L2DPartsParam[] array = new L2DPartsParam[idGroup.Length];
			for (int i = 0; i < idGroup.Length; i++)
			{
				array[i] = new L2DPartsParam(idGroup[i]);
			}
			partsGroupList.Add(array);
		}

		public void updateParam(ALive2DModel model)
		{
			if (model != null)
			{
				if (model != lastModel)
				{
					initParam(model);
				}
				lastModel = model;
				long userTimeMSec = UtSystem.getUserTimeMSec();
				float num = ((lastTime != 0L) ? ((float)(userTimeMSec - lastTime) / 1000f) : 0f);
				lastTime = userTimeMSec;
				if (num < 0f)
				{
					num = 0f;
				}
				for (int i = 0; i < partsGroupList.Count; i++)
				{
					normalizePartsOpacityGroup(model, partsGroupList[i], num);
					copyOpacityOtherParts(model, partsGroupList[i]);
				}
			}
		}

		public void initParam(ALive2DModel model)
		{
			if (model == null)
			{
				return;
			}
			for (int i = 0; i < partsGroupList.Count; i++)
			{
				L2DPartsParam[] array = partsGroupList[i];
				for (int j = 0; j < array.Length; j++)
				{
					array[j].initIndex(model);
					int partsIndex = array[j].partsIndex;
					int paramIndex = array[j].paramIndex;
					if (partsIndex >= 0)
					{
						bool flag = model.getParamFloat(paramIndex) != 0f;
						model.setPartsOpacity(partsIndex, (!flag) ? 0f : 1f);
						model.setParamFloat(paramIndex, (!flag) ? 0f : 1f);
					}
				}
			}
		}

		public void normalizePartsOpacityGroup(ALive2DModel model, L2DPartsParam[] partsGroup, float deltaTimeSec)
		{
			int num = -1;
			float num2 = 1f;
			float num3 = 0.5f;
			float num4 = 0.5f;
			float num5 = 0.15f;
			for (int i = 0; i < partsGroup.Length; i++)
			{
				int partsIndex = partsGroup[i].partsIndex;
				int paramIndex = partsGroup[i].paramIndex;
				if (partsIndex >= 0 && model.getParamFloat(paramIndex) != 0f)
				{
					if (num >= 0)
					{
						break;
					}
					num = i;
					num2 = model.getPartsOpacity(partsIndex);
					num2 += deltaTimeSec / num3;
					if (num2 > 1f)
					{
						num2 = 1f;
					}
				}
			}
			if (num < 0)
			{
				num = 0;
				num2 = 1f;
			}
			for (int j = 0; j < partsGroup.Length; j++)
			{
				int partsIndex2 = partsGroup[j].partsIndex;
				if (partsIndex2 < 0)
				{
					continue;
				}
				if (num == j)
				{
					model.setPartsOpacity(partsIndex2, num2);
					continue;
				}
				float num6 = model.getPartsOpacity(partsIndex2);
				float num7 = ((!(num2 < num4)) ? ((1f - num2) * num4 / (1f - num4)) : (num2 * (num4 - 1f) / num4 + 1f));
				float num8 = (1f - num7) * (1f - num2);
				if (num8 > num5)
				{
					num7 = 1f - num5 / (1f - num2);
				}
				if (num6 > num7)
				{
					num6 = num7;
				}
				model.setPartsOpacity(partsIndex2, num6);
			}
		}

		public void copyOpacityOtherParts(ALive2DModel model, L2DPartsParam[] partsGroup)
		{
			foreach (L2DPartsParam l2DPartsParam in partsGroup)
			{
				if (l2DPartsParam.link == null || l2DPartsParam.partsIndex < 0)
				{
					continue;
				}
				float partsOpacity = model.getPartsOpacity(l2DPartsParam.partsIndex);
				for (int j = 0; j < l2DPartsParam.link.Count; j++)
				{
					L2DPartsParam l2DPartsParam2 = l2DPartsParam.link[j];
					if (l2DPartsParam2.partsIndex < 0)
					{
						l2DPartsParam2.initIndex(model);
					}
					if (l2DPartsParam2.partsIndex >= 0)
					{
						model.setPartsOpacity(l2DPartsParam2.partsIndex, partsOpacity);
					}
				}
			}
		}

		public static L2DPose load(byte[] buf)
		{
			return load(Encoding.GetEncoding("UTF-8").GetString(buf));
		}

		public static L2DPose load(string buf)
		{
			return load(buf.ToCharArray());
		}

		public static L2DPose load(char[] buf)
		{
			L2DPose l2DPose = new L2DPose();
			Value value = Json.parseFromBytes(buf);
			List<Value> vector = value.get("parts_visible").getVector(null);
			int count = vector.Count;
			for (int i = 0; i < count; i++)
			{
				Value value2 = vector[i];
				List<Value> vector2 = value2.get("group").getVector(null);
				int count2 = vector2.Count;
				L2DPartsParam[] array = new L2DPartsParam[count2];
				for (int j = 0; j < count2; j++)
				{
					Value value3 = vector2[j];
					L2DPartsParam l2DPartsParam = (array[j] = new L2DPartsParam(value3.get("id").toString()));
					if (value3.getMap(null).ContainsKey("link"))
					{
						List<Value> vector3 = value3.get("link").getVector(null);
						int count3 = vector3.Count;
						l2DPartsParam.link = new List<L2DPartsParam>();
						for (int k = 0; k < count3; k++)
						{
							L2DPartsParam item = new L2DPartsParam(vector3[k].toString());
							l2DPartsParam.link.Add(item);
						}
					}
				}
				l2DPose.addPartsGroup(array);
			}
			return l2DPose;
		}
	}
}
