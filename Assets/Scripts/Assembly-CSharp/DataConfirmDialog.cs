using System.Collections.Generic;
using UnityEngine;

public class DataConfirmDialog : DialogBase
{
	public enum STYLE
	{
		NONE = 0,
		BACK_UP = 1,
		BACK_UP_NEWDATA = 2,
		HAND_OFF = 3
	}

	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private STYLE mStyle;

	private SELECT_ITEM mSelectItem;

	private Player mData;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public void Init(Player data, STYLE style, OnDialogClosed callback)
	{
		mData = data;
		mStyle = style;
		mCallback = callback;
	}

	public override void BuildDialog()
	{
		base.BuildDialog();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont font = GameMain.LoadFont();
		switch (mStyle)
		{
		case STYLE.NONE:
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle("NONE");
			break;
		case STYLE.BACK_UP:
		case STYLE.BACK_UP_NEWDATA:
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle(Localization.Get("BackupCheck_Title"));
			CreateBoard(base.gameObject, image, font, "BackupDataBoard", Localization.Get("BackupCheck_Subject"), Localization.Get("BackupCheck_Speak"), new Vector3(0f, 120f, 0f), mBaseDepth + 1);
			CreateDesc(base.gameObject, font, Localization.Get("BackupCheck_Desc"), new Vector3(0f, -160f, 0f), mBaseDepth + 1);
			break;
		case STYLE.HAND_OFF:
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle(Localization.Get("HandoffCheck_Title"));
			CreateBoard(base.gameObject, image, font, "HandOffDataBoard", Localization.Get("HandoffCheck_Subject"), Localization.Get("HandoffCheck_Speak"), new Vector3(0f, 0f, 0f), mBaseDepth + 1);
			CreateDesc(base.gameObject, font, Localization.Get("HandoffCheck_Desc"), new Vector3(0f, -135f, 0f), mBaseDepth + 1);
			break;
		}
		float y = -225f;
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		UIButton button2 = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
		string text;
		switch (mStyle)
		{
		default:
			text = "9";
			break;
		case STYLE.BACK_UP:
			text = "LC_button_no";
			break;
		case STYLE.BACK_UP_NEWDATA:
			text = "LC_button_first";
			break;
		}
		Util.SetImageButtonInfo(button2, text, text, text, mBaseDepth + 4, new Vector3(130f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(button2, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
	}

	private void CreateBoard(GameObject parent, BIJImage atlas, UIFont font, string ObjName, string LocName, string Speak, Vector3 pos, int depth)
	{
		UISprite uISprite = Util.CreateSprite(ObjName, parent, atlas);
		Util.SetSpriteInfo(uISprite, "149", depth, pos, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(500, 165);
		UILabel uILabel = Util.CreateLabel("Title", uISprite.gameObject, font);
		Util.SetLabelInfo(uILabel, LocName, depth + 1, new Vector3(0f, 95f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.SetDimensions(450, 30);
		float posY = 60f;
		string[] array = new string[5] { "SeasonTitle_00", "SeasonTitle_01", "SeasonTitle_02", "SeasonTitle_03", "SeasonTitle_04" };
		for (int i = 0; i <= 4; i++)
		{
			int num = 0;
			List<PlayStageParam> playStage = mData.PlayStage;
			for (int j = 0; j < playStage.Count; j++)
			{
				PlayStageParam playStageParam = playStage[j];
				if (i == playStageParam.Series)
				{
					num = playStageParam.Level / 100;
					break;
				}
			}
			if (num != 0)
			{
				string titleLocName = Localization.Get(array[i]);
				string text = Localization.Get("DataCheck_LevelTitle");
				string text2 = " ";
				if (num < 10)
				{
					text2 = " \u3000 ";
				}
				else if (num < 100)
				{
					text2 = "\u3000";
				}
				string text3 = string.Format(Localization.Get("DataCheck_LevelFormat"), num);
				PlayDataLabel playDataLabel = new PlayDataLabel(uISprite.gameObject, font, "Level", titleLocName, text + text2 + text3, 0L, 0L, 0L, ref posY, 28f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
			}
		}
		posY = -93f;
		PlayDataLabel playDataLabel2 = new PlayDataLabel(uISprite.gameObject, font, "Star", "DataCheck_StarTitle", "DataCheck_StarFormat", mData.GetRankingTotalStar(), 0L, 0L, ref posY, 28f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		PlayDataLabel playDataLabel3 = new PlayDataLabel(uISprite.gameObject, font, "Count", "DataCheck_CountTitle", "DataCheck_CountFormat", mData.Data.PlayCount, 0L, 0L, ref posY, 28f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		int num2 = (int)mData.Data.TotalPlayTime;
		int num3 = num2 / 3600;
		num2 %= 3600;
		int num4 = num2 / 60;
		int num5 = num2 % 60;
		PlayDataLabel playDataLabel4 = new PlayDataLabel(uISprite.gameObject, font, "Time", "DataCheck_TimeTitle", "DataCheck_TimeFormat", num3, num4, num5, ref posY, 28f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		posY -= 10f;
		if (mStyle != STYLE.BACK_UP)
		{
			UILabel uILabel2 = Util.CreateLabel("Speak", uISprite.gameObject, font);
			Util.SetLabelInfo(uILabel2, Speak, depth + 1, new Vector3(0f, -245f, 0f), 18, 0, 0, UIWidget.Pivot.Center);
			uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel2.color = Color.red;
			uILabel2.SetDimensions(500, 75);
		}
	}

	private void CreateDesc(GameObject parent, UIFont font, string desc, Vector3 pos, int depth)
	{
		UILabel uILabel = Util.CreateLabel("Speak", parent, font);
		Util.SetLabelInfo(uILabel, desc, depth + 1, pos, 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.SetDimensions(500, 100);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
