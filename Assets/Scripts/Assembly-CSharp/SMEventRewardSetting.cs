public class SMEventRewardSetting : SMMapPageSettingBase
{
	public int TotalNum;

	public int RewardID;

	public int IsGCItem;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteInt(TotalNum);
		a_writer.WriteInt(RewardID);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		TotalNum = a_reader.ReadInt();
		RewardID = a_reader.ReadInt();
	}
}
