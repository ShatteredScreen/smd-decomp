using System;
using UnityEngine;

public class SsVersion
{
	public static int ToInt(string versionStr)
	{
		string[] array = versionStr.Split('.');
		if (array.Length != 3)
		{
			UnityEngine.Debug.LogError("The number of version digits are invalid: " + array.Length);
			return -1;
		}
		int num = _HexToInt(array[0]);
		int num2 = _HexToInt(array[1]);
		int num3 = _HexToInt(array[2]);
		return (num << 16) | (num2 << 8) | num3;
	}

	public static string ToString(int n)
	{
		int num = (n >> 16) & 0xFF;
		if (num == 0)
		{
			return null;
		}
		int num2 = (n >> 8) & 0xFF;
		int num3 = n & 0xFF;
		return string.Format("{0:X}.{1:X2}.{2:X2}", num, num2, num3);
	}

	private static int _HexToInt(string src)
	{
		return Convert.ToInt32(src, 16);
	}
}
