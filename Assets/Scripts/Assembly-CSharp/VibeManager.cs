using System.Collections;
using UnityEngine;

public class VibeManager : MonoBehaviour
{
	public const float TIME_WAIT = 0.25f;

	public const float TIME_VERY_SHORT = 0.05f;

	public const float TIME_SHORT = 0.25f;

	public const float TIME_LONG = 0.5f;

	public static VibeManager instance;

	public static bool IsVibeEnabled = true;

	protected bool waittimeflag;

	protected float waittime;

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		Object.DontDestroyOnLoad(base.gameObject);
	}

	public void Play(float _time)
	{
		if (IsVibeEnabled)
		{
			if (_time < 0.25f)
			{
				long num = Mathf.FloorToInt(_time * 1000f);
				return;
			}
			Handheld.Vibrate();
			waittimeflag = true;
			waittime = _time;
			StartCoroutine(updateWait());
		}
	}

	public bool isWaiting()
	{
		return waittimeflag;
	}

	public void setWait(float _time)
	{
		waittime = _time;
	}

	public IEnumerator updateWait()
	{
		waittimeflag = true;
		float duration = 0f;
		do
		{
			Handheld.Vibrate();
			float time = 0.25f;
			yield return new WaitForSeconds(time);
			duration += time;
		}
		while (!(duration >= waittime));
		waittimeflag = false;
	}
}
