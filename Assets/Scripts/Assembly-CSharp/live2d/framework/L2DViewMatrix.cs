namespace live2d.framework
{
	public class L2DViewMatrix : L2DMatrix44
	{
		private float max;

		private float min;

		private float screenLeft;

		private float screenRight;

		private float screenTop;

		private float screenBottom;

		private float maxLeft;

		private float maxRight;

		private float maxTop;

		private float maxBottom;

		public L2DViewMatrix()
		{
			max = 1f;
			min = 1f;
		}

		public float getMaxScale()
		{
			return max;
		}

		public float getMinScale()
		{
			return min;
		}

		public void setMaxScale(float v)
		{
			max = v;
		}

		public void setMinScale(float v)
		{
			min = v;
		}

		public bool isMaxScale()
		{
			return getScaleX() == max;
		}

		public bool isMinScale()
		{
			return getScaleX() == min;
		}

		public void adjustTranslate(float shiftX, float shiftY)
		{
			if (tr[0] * maxLeft + (tr[12] + shiftX) > screenLeft)
			{
				shiftX = screenLeft - tr[0] * maxLeft - tr[12];
			}
			if (tr[0] * maxRight + (tr[12] + shiftX) < screenRight)
			{
				shiftX = screenRight - tr[0] * maxRight - tr[12];
			}
			if (tr[5] * maxTop + (tr[13] + shiftY) < screenTop)
			{
				shiftY = screenTop - tr[5] * maxTop - tr[13];
			}
			if (tr[5] * maxBottom + (tr[13] + shiftY) > screenBottom)
			{
				shiftY = screenBottom - tr[5] * maxBottom - tr[13];
			}
			float[] a = new float[16]
			{
				1f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f,
				1f, 0f, shiftX, shiftY, 0f, 1f
			};
			L2DMatrix44.mul(a, tr, tr);
		}

		public void adjustScale(float cx, float cy, float scale)
		{
			float num = scale * tr[0];
			if (num < min)
			{
				if (tr[0] > 0f)
				{
					scale = min / tr[0];
				}
			}
			else if (num > max && tr[0] > 0f)
			{
				scale = max / tr[0];
			}
			float[] a = new float[16]
			{
				1f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f,
				1f, 0f, cx, cy, 0f, 1f
			};
			float[] a2 = new float[16]
			{
				scale, 0f, 0f, 0f, 0f, scale, 0f, 0f, 0f, 0f,
				1f, 0f, 0f, 0f, 0f, 1f
			};
			float[] a3 = new float[16]
			{
				1f,
				0f,
				0f,
				0f,
				0f,
				1f,
				0f,
				0f,
				0f,
				0f,
				1f,
				0f,
				0f - cx,
				0f - cy,
				0f,
				1f
			};
			L2DMatrix44.mul(a3, tr, tr);
			L2DMatrix44.mul(a2, tr, tr);
			L2DMatrix44.mul(a, tr, tr);
		}

		public void setScreenRect(float left, float right, float bottom, float top)
		{
			screenLeft = left;
			screenRight = right;
			screenTop = top;
			screenBottom = bottom;
		}

		public void setMaxScreenRect(float left, float right, float bottom, float top)
		{
			maxLeft = left;
			maxRight = right;
			maxTop = top;
			maxBottom = bottom;
		}

		public float getScreenLeft()
		{
			return screenLeft;
		}

		public float getScreenRight()
		{
			return screenRight;
		}

		public float getScreenBottom()
		{
			return screenBottom;
		}

		public float getScreenTop()
		{
			return screenTop;
		}

		public float getMaxLeft()
		{
			return maxLeft;
		}

		public float getMaxRight()
		{
			return maxRight;
		}

		public float getMaxBottom()
		{
			return maxBottom;
		}

		public float getMaxTop()
		{
			return maxTop;
		}
	}
}
