using System.Collections.Generic;
using UnityEngine;

public static class SsShaderManager
{
	private static Dictionary<SsShaderType, Shader> _shaderList = new Dictionary<SsShaderType, Shader>();

	private static Shader[] _unifiedShaderList = new Shader[5];

	private static bool _initialized = false;

	public static void Initialize()
	{
		if (_initialized)
		{
			return;
		}
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				for (int k = 0; k < 2; k++)
				{
					SsColorBlendOperation ssColorBlendOperation = (SsColorBlendOperation)i;
					SsAlphaBlendOperation ssAlphaBlendOperation = (SsAlphaBlendOperation)j;
					SsMaterialColorBlendOperation ssMaterialColorBlendOperation = (SsMaterialColorBlendOperation)k;
					SsShaderType key = EnumToType(ssColorBlendOperation, ssAlphaBlendOperation, ssMaterialColorBlendOperation);
					string text = string.Concat("Ss/", ssColorBlendOperation, "Color", ssAlphaBlendOperation, "Alpha");
					if (ssMaterialColorBlendOperation != 0)
					{
						text = string.Concat(text, ssMaterialColorBlendOperation, "MatCol");
					}
					_shaderList[key] = Shader.Find(text);
					if (_shaderList[key] == null)
					{
						UnityEngine.Debug.LogError("not found shader!!: " + text);
					}
				}
			}
		}
		_initialized = true;
	}

	public static Shader Get(SsShaderType t, bool unified)
	{
		if (!_initialized)
		{
			Initialize();
		}
		if (unified)
		{
			return _unifiedShaderList[(int)t >> 4];
		}
		Shader value;
		if (_shaderList.TryGetValue(t, out value))
		{
			return value;
		}
		return null;
	}

	public static SsShaderType EnumToType(SsColorBlendOperation color, SsAlphaBlendOperation alpha, SsMaterialColorBlendOperation matColor)
	{
		return (SsShaderType)((int)color | ((int)alpha << 4) | ((int)matColor << 8));
	}

	public static int ToSerial(SsShaderType t)
	{
		return (int)(((int)t >> 8) * 25 + ((int)t >> 4) * 5 + (t & SsShaderType.ColorMask));
	}
}
