using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class PurchaseItemList : ListBase<PLItem>
{
	public const float DEFAULT_LIST_WIDTH = 520f;

	public const float DEFAULT_LIST_HEIGHT = 430f;

	public MonoBehaviour messageTaget;

	public List<PLItem> items = new List<PLItem>();

	private float posX;

	private float posY;

	public bool initilized;

	private int gamestate;

	public GameObject mListParent;

	private float LIST_WIDTH { get; set; }

	private float LIST_HEIGHT { get; set; }

	public static PurchaseItemList Make(GameObject _parent, MonoBehaviour _messageTaget, List<PLItem> _items, float _posX, float _posY, int _baseDepth, float _w, float _h)
	{
		GameObject gameObject = Util.CreateGameObject("PurchaseItemList", _parent);
		PurchaseItemList purchaseItemList = gameObject.AddComponent<PurchaseItemList>();
		purchaseItemList.items = _items;
		purchaseItemList.posX = _posX;
		purchaseItemList.posY = _posY;
		purchaseItemList.mBaseDepth = _baseDepth;
		purchaseItemList.messageTaget = _messageTaget;
		purchaseItemList.LIST_WIDTH = ((!(_w > 0f)) ? 520f : _w);
		purchaseItemList.LIST_HEIGHT = ((!(_h > 0f)) ? 430f : _h);
		return purchaseItemList;
	}

	public static void MakeAddItem(List<PLItem> _tgList, PItem _pitem)
	{
		PLItem pLItem = new PLItem();
		if (_pitem != null)
		{
			pLItem.pitem = _pitem;
			pLItem.kind = PLItem.KIND.SD;
		}
		else
		{
			pLItem.kind = PLItem.KIND.DUMMY;
		}
		_tgList.Add(pLItem);
	}

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	protected override int GetScrollingDeltaY()
	{
		return 1;
	}

	protected override int GetCellHeight()
	{
		return 117;
	}

	public new virtual void OnDestroy()
	{
	}

	protected void CreateWindow(GameObject parent, string name)
	{
		mListParent = Util.CreateGameObject("ListParent", parent);
		mListParent.transform.localPosition = new Vector3(posX, posY, 0f);
		mWindowRect = new Rect((0f - LIST_WIDTH) / 2f, (0f - LIST_HEIGHT) / 2f, LIST_WIDTH, LIST_HEIGHT);
	}

	protected override void CreateContents(GameObject parent, string name)
	{
		CreateList(parent, "List");
		mListScrollView.dragEffect = UIScrollView.DragEffect.MomentumAndSpring;
		mListScrollView.restrictWithinPanel = true;
		mCenterOnChild.enabled = false;
	}

	protected override void CreateScrollBar(GameObject parent, string name)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mScroll = Util.CreateGameObject("Scroll", parent).AddComponent<UIScrollBar>();
		mScroll.fillDirection = UIProgressBar.FillDirection.TopToBottom;
		UISprite uISprite = Util.CreateSprite("Cursor", mScroll.gameObject, image);
		Util.SetSpriteInfo(uISprite, "scrollbar01", mBaseDepth + 3, new Vector3(263f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 26;
		uISprite.height = (int)LIST_HEIGHT;
		UISprite uISprite2 = Util.CreateSprite("BackGround", mScroll.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "scrollbar00", mBaseDepth + 2, new Vector3(263f, 0f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.width = 26;
		uISprite2.height = (int)LIST_HEIGHT;
		mScroll.foregroundWidget = uISprite;
		mScroll.backgroundWidget = uISprite2;
	}

	protected override void CreateItem(GameObject itemGO, int index, PLItem item, PLItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		string empty = string.Empty;
		float cellWidth = mList.cellWidth;
		float cellHeight = mList.cellHeight;
		StringBuilder stringBuilder = new StringBuilder();
		float num = 0f;
		float num2 = 0f;
		float num3 = mList.cellWidth - 15f;
		float num4 = mList.cellHeight - 4f;
		PLItem.KIND kind = item.kind;
		if (kind == PLItem.KIND.DUMMY || kind != PLItem.KIND.SD)
		{
			uISprite = Util.CreateSprite("BlankBack", itemGO, image);
			Util.SetSpriteInfo(uISprite, "0", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite.width = (int)cellWidth;
			uISprite.height = (int)cellHeight;
			return;
		}
		if (index != 0)
		{
			uISprite = Util.CreateSprite("Line", itemGO, image);
			Util.SetSpriteInfo(uISprite, "171", mBaseDepth + 2, new Vector3(0f, cellHeight / 2f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.width = (int)cellWidth - 50;
			uISprite.height = 10;
		}
		uISprite = Util.CreateSprite("BlankBack", itemGO, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(490, 104);
		string imageName = ((index != 0) ? ("icon_currency" + (index + 1)) : "icon_currency");
		uISprite = Util.CreateSprite("Gem", itemGO, image);
		Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 2, new Vector3(-187f, 2f, 0f), Vector3.one, false);
		if (index == 3)
		{
			uISprite.transform.localPosition = new Vector3(-187f, -1f, 0f);
		}
		uISprite = Util.CreateSprite("CostBack", itemGO, image);
		Util.SetSpriteInfo(uISprite, "buy_panel", mBaseDepth + 2, new Vector3(19f, 3f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 130;
		uISprite.height = 40;
		UILabel uILabel = Util.CreateLabel("Yen", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Purchase_yen"), mBaseDepth + 4, new Vector3(-15f, -3f, 0f), 22, 0, 0, UIWidget.Pivot.Right);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel = Util.CreateLabel("Cost", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty + item.pitem.cost, mBaseDepth + 4, new Vector3(72f, -3f, 0f), 22, 0, 0, UIWidget.Pivot.Right);
		DialogBase.SetDialogLabelEffect2(uILabel);
		string text = Localization.Get("Xtext");
		uILabel = Util.CreateLabel("X", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 4, new Vector3(-141f, -3f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel = Util.CreateLabel("Amount", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty + item.pitem.amount, mBaseDepth + 4, new Vector3(-112f, -3f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		DialogBase.SetDialogLabelEffect2(uILabel);
		UIButton button = Util.CreateJellyImageButton("BuyButton", itemGO, image);
		Util.SetImageButtonInfo(button, "LC_button_buy2", "LC_button_buy2", "LC_button_buy2", mBaseDepth + 3, new Vector3(162f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, messageTaget, "OnSDPushedFromList_" + item.pitem.kind, UIButtonMessage.Trigger.OnClick);
	}

	public override void Update()
	{
		switch (gamestate)
		{
		case 0:
			ResetContents();
			CreateWindow(base.gameObject, "Window");
			CreateContents(mListParent, "Contents");
			UpdateList();
			initilized = true;
			gamestate = 4096;
			break;
		case 4096:
			OnScrollAdjust();
			gamestate = 8192;
			break;
		}
	}

	private void ResetContents()
	{
		if (mListParent != null && mListParent.gameObject != null)
		{
			GameMain.SafeDestroy(mListParent.gameObject);
			mListParent = null;
		}
	}

	public void Reconstruct()
	{
		gamestate = 0;
	}

	protected new int ListIndexOf(GameObject go)
	{
		if (mMapItems == null || mMapItems.Count < 1)
		{
			return -1;
		}
		int value = -1;
		if (!mMapItems.TryGetValue(go, out value))
		{
			return -1;
		}
		return value;
	}

	protected new int FindListIndexOf(GameObject go)
	{
		UIDragScrollView uIDragScrollView = NGUITools.FindInParents<UIDragScrollView>(go);
		if (uIDragScrollView == null)
		{
			return -1;
		}
		return ListIndexOf(uIDragScrollView.gameObject);
	}

	public void ClearListItem()
	{
		items.Clear();
	}

	protected override List<PLItem> GetListItem()
	{
		return items;
	}
}
