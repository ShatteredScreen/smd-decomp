using System;
using System.IO;
using System.Text;

public class BIJMemoryBinaryWriter : BIJBinaryWriter
{
	private MemoryStream mStream;

	public byte[] Bytes
	{
		get
		{
			return mStream.ToArray();
		}
	}

	public BIJMemoryBinaryWriter()
		: this(4096, Encoding.UTF8, string.Empty)
	{
	}

	public BIJMemoryBinaryWriter(string name)
		: this(4096, Encoding.UTF8, name)
	{
	}

	public BIJMemoryBinaryWriter(int capacity, Encoding encoding, string name)
	{
		mStream = null;
		Writer = null;
		try
		{
			mPath = name;
			mStream = new MemoryStream(capacity);
			Writer = new BinaryWriter(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	public override void Close()
	{
		base.Close();
		if (mStream != null)
		{
			try
			{
				mStream.Close();
			}
			catch (Exception)
			{
			}
			mStream = null;
		}
	}
}
