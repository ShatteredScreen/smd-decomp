using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMessageDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		NEXT_OPEN = 2,
		NEXT_CLOSE = 3,
		NEXT_CLOSE_WAIT = 4,
		FAKE_OPEN = 5,
		FAKE_CLOSE = 6,
		FAKE_CLOSE_WAIT = 7
	}

	public enum SELECT_ITEM
	{
		TAP = 0,
		SKIP = 1
	}

	public delegate void OnDialogClosed(Def.TUTORIAL_INDEX index, SELECT_ITEM selectItem);

	public delegate void OnFakeClosed();

	private const char PAGENEXT = '@';

	private const int MAX_ROW = 3;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private OnDialogClosed mOnClosed;

	private OnDialogClosed mOnMessageFinished;

	private OnFakeClosed mOnFakeClosed = delegate
	{
	};

	private float mDialogOfsY;

	private float mDescOfsY;

	private Def.TUTORIAL_INDEX mIndex;

	private Def.TutorialStartData mData;

	private float mTimer;

	private bool mTapWait;

	private bool mShowSkip;

	private bool mNextPushed;

	private UISprite mFrame;

	private UIButton mSkipButton;

	private UIButton mNextButton;

	private UISprite mNextAttention;

	private UILabel mLabel;

	private UISprite mMaskSprite;

	private string mL2DKey;

	private string mL2DMotionKey;

	private string mL2DExpressionKey;

	private float mL2DScale;

	private float mL2DOffsetPosY;

	private Live2DRender mL2DRender;

	private UITexture mL2DTexture;

	private Live2DInstance mL2DInst;

	private string DEFAULT_L2D_KEY = "LUNA";

	private string DEFAULT_L2D_MOTION_KEY = "motions/talk00.mtn";

	private string DEFAULT_L2D_EXPRESSION_KEY = string.Empty;

	private List<string> textList = new List<string>();

	private int textCounter;

	public override void Awake()
	{
		base.Awake();
		mGame.mDialogManager.Unresister(this);
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (!GetBusy() && mTimer > 0f)
			{
				mTimer -= Time.deltaTime;
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.MAIN:
			if (!mNextPushed)
			{
				break;
			}
			if (textCounter >= textList.Count - 1)
			{
				if (mTapWait)
				{
					mOnMessageFinished(mIndex, SELECT_ITEM.TAP);
				}
			}
			else
			{
				textCounter++;
				mLabel.text = textList[textCounter];
			}
			mNextPushed = false;
			break;
		case STATE.NEXT_CLOSE:
			if (mJelly.IsMoveFinished())
			{
				textCounter++;
				mLabel.text = textList[textCounter];
				mJelly.Open();
				mState.Change(STATE.NEXT_OPEN);
			}
			break;
		case STATE.NEXT_OPEN:
			if (mJelly.IsMoveFinished())
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.FAKE_OPEN:
			if (mJelly.IsMoveFinished())
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.FAKE_CLOSE:
			if (mJelly.IsMoveFinished())
			{
				mFrame.gameObject.SetActive(false);
				mState.Change(STATE.FAKE_CLOSE_WAIT);
				if (mOnFakeClosed != null)
				{
					mOnFakeClosed();
				}
			}
			break;
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		mL2DRender = mGame.CreateLive2DRender(568, 568);
		mL2DInst = Util.CreateGameObject("Char", mL2DRender.gameObject).AddComponent<Live2DInstance>();
		mL2DInst.Init(mL2DKey, Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER, new Color(1f, 1f, 1f, 1f), true);
		while (!mL2DInst.IsLoadFinished())
		{
			yield return 0;
		}
		if (!string.IsNullOrEmpty(mL2DMotionKey))
		{
			SetMotion(mL2DMotionKey);
		}
		if (!string.IsNullOrEmpty(mL2DExpressionKey))
		{
			SetExpression(mL2DExpressionKey);
		}
		mL2DInst.Position = new Vector3(0f, 0f, -50f);
		mL2DInst.StartLipSync();
		mL2DRender.ResisterInstance(mL2DInst);
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("DIALOG_BASE").Image;
		UIFont atlasFont = GameMain.LoadFont();
		mFrame = Util.CreateSprite("Frame", base.gameObject, image2);
		Util.SetSpriteInfo(mFrame, "instruction_panel01", mBaseDepth + 2, new Vector3(0f, 0f, 0f), Vector3.one, false);
		mFrame.type = UIBasicSprite.Type.Sliced;
		mFrame.SetDimensions(428, 140);
		mMaskSprite = Util.CreateSprite("Mask", base.gameObject, image2);
		Util.SetSpriteInfo(mMaskSprite, "panel_mask", mBaseDepth - 2, new Vector3(0f, 10f, 0f), Vector3.one, false);
		mMaskSprite.type = UIBasicSprite.Type.Sliced;
		mMaskSprite.SetDimensions(720, 288);
		mLabel = Util.CreateLabel("Desc", mFrame.gameObject, atlasFont);
		Util.SetLabelInfo(mLabel, string.Empty, mBaseDepth + 3, new Vector3(-160f, mDescOfsY, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		mLabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		mLabel.SetDimensions(365, 120);
		mLabel.spacingY = 8;
		mL2DTexture = Util.CreateGameObject("CharacterScreen", mFrame.gameObject).AddComponent<UITexture>();
		mL2DTexture.mainTexture = mL2DRender.RenderTexture;
		mL2DTexture.SetDimensions((int)(568f * mL2DScale), (int)(568f * mL2DScale));
		mL2DTexture.transform.localPosition = new Vector3(-240f, 216f + mL2DOffsetPosY, 0f);
		mL2DTexture.depth = mBaseDepth + 5;
		mTimer = 1f;
		mNextPushed = false;
		mSkipButton = Util.CreateJellyImageButton("ButtonSkip", mFrame.gameObject, image);
		Util.SetImageButtonInfo(mSkipButton, "LC_button_skip2", "LC_button_skip2", "LC_button_skip2", mBaseDepth + 120, new Vector3(0f, 96f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSkipButton, this, "OnSkipPushed", UIButtonMessage.Trigger.OnClick);
		mNextButton = Util.CreateJellyImageButton("ButtonNext", mFrame.gameObject, image);
		Util.SetImageButtonInfo(mNextButton, "LC_button_next3", "LC_button_next3", "LC_button_next3", mBaseDepth + 120, new Vector3(154f, 104f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mNextButton, this, "OnNextPushed", UIButtonMessage.Trigger.OnClick);
		mGame.mTutorialManager.AddAllowedButton(mNextButton.gameObject);
		mNextAttention = Util.CreateSprite("Attention", mNextButton.gameObject, image);
		Util.SetSpriteInfo(mNextAttention, "story_arrow2", mBaseDepth + 121, new Vector3(50f, 0f, 0f), Vector3.one, false);
		StartCoroutine(NextAttentionUpdate());
		SetMessage();
		SetLayout(Util.ScreenOrientation);
	}

	private IEnumerator NextAttentionUpdate()
	{
		float angle = 0f;
		while (mNextAttention != null)
		{
			angle += Time.deltaTime * 360f;
			mNextAttention.transform.localScale = new Vector3(Mathf.Cos((float)Math.PI / 180f * angle), 1f, 1f);
			yield return 0;
		}
	}

	private void SetMessage()
	{
		if (mData.messageDisp)
		{
			mFrame.gameObject.SetActive(true);
			mMaskSprite.gameObject.SetActive(true);
		}
		else
		{
			mFrame.gameObject.SetActive(false);
			mMaskSprite.gameObject.SetActive(false);
		}
		textCounter = 0;
		string[] array = Localization.Get(mData.messageKey).Split('@');
		textList.Clear();
		int i = 0;
		for (int num = array.Length; i < num; i++)
		{
			textList.Add(array[i]);
		}
		mLabel.text = textList[textCounter];
		if (!mGame.mTutorialManager.IsAllowed(mSkipButton.gameObject))
		{
			mGame.mTutorialManager.AddAllowedButton(mSkipButton.gameObject);
		}
		if (!mGame.mTutorialManager.IsAllowed(mNextButton.gameObject))
		{
			mGame.mTutorialManager.AddAllowedButton(mNextButton.gameObject);
		}
		if (mShowSkip)
		{
			mSkipButton.gameObject.SetActive(true);
		}
		else
		{
			mSkipButton.gameObject.SetActive(false);
		}
		if (mTapWait)
		{
			mNextButton.gameObject.SetActive(true);
		}
		else
		{
			mNextButton.gameObject.SetActive(false);
		}
		if (mTapWait)
		{
			mMaskSprite.SetDimensions(700, 250);
			mMaskSprite.transform.localPosition = new Vector3(-30f, 34f, 0f);
		}
		else if (mShowSkip)
		{
			mMaskSprite.SetDimensions(700, 230);
			mMaskSprite.transform.localPosition = new Vector3(-30f, 25f, 0f);
		}
		else
		{
			mMaskSprite.SetDimensions(700, 180);
			mMaskSprite.transform.localPosition = new Vector3(-30f, 0f, 0f);
		}
	}

	public void SetMotion(string key)
	{
		if (!(mL2DInst == null))
		{
			mL2DInst.StartMotion(key, true, 0);
		}
	}

	public void SetExpression(string key)
	{
		if (!(mL2DInst == null))
		{
			mL2DInst.SetExpression(key);
		}
	}

	public void Init(Def.TUTORIAL_INDEX index, Def.TutorialStartData data)
	{
		mIndex = index;
		mData = data;
		mShowSkip = data.showSkip;
		mTapWait = data.tapWait;
		mL2DKey = data.l2dKey;
		if (string.IsNullOrEmpty(mL2DKey))
		{
			mL2DKey = DEFAULT_L2D_KEY;
		}
		mL2DMotionKey = data.l2dMotionKey;
		if (string.IsNullOrEmpty(mL2DMotionKey))
		{
			mL2DMotionKey = DEFAULT_L2D_MOTION_KEY;
		}
		mL2DExpressionKey = data.l2dExpressionKey;
		if (string.IsNullOrEmpty(mL2DExpressionKey))
		{
			mL2DExpressionKey = DEFAULT_L2D_EXPRESSION_KEY;
		}
		mL2DScale = data.l2dScale;
		mL2DOffsetPosY = data.l2dOffsetPosY;
	}

	public void Change(Def.TUTORIAL_INDEX index, Def.TutorialStartData data)
	{
		mIndex = index;
		mData = data;
		mShowSkip = data.showSkip;
		mTapWait = data.tapWait;
		mL2DMotionKey = data.l2dMotionKey;
		if (string.IsNullOrEmpty(mL2DMotionKey))
		{
			mL2DMotionKey = DEFAULT_L2D_MOTION_KEY;
		}
		mL2DExpressionKey = data.l2dExpressionKey;
		if (string.IsNullOrEmpty(mL2DExpressionKey))
		{
			mL2DExpressionKey = DEFAULT_L2D_EXPRESSION_KEY;
		}
		mL2DScale = data.l2dScale;
		mL2DOffsetPosY = data.l2dOffsetPosY;
		SetMessage();
		SetLayout(Util.ScreenOrientation);
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		SetLayout(o);
	}

	private void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			base.gameObject.transform.localPosition = new Vector3(mData.messagePosPortrait.x, mData.messagePosPortrait.y, 0f);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			base.gameObject.transform.localPosition = new Vector3(mData.messagePosLandscape.x, mData.messagePosLandscape.y, 0f);
			break;
		}
	}

	public override void OnDestroy()
	{
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void Close()
	{
		if (mBaseState.GetStatus() != BASE_STATE.CLOSE && mBaseState.GetStatus() != BASE_STATE.AFTER_CLOSE && mBaseState.GetStatus() != BASE_STATE.WAIT)
		{
			base.Close();
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mOnClosed != null)
		{
			mOnClosed(mIndex, SELECT_ITEM.TAP);
		}
		mNextAttention = null;
		if (mL2DTexture != null)
		{
			UnityEngine.Object.Destroy(mL2DTexture.gameObject);
		}
		if (mL2DRender != null)
		{
			UnityEngine.Object.Destroy(mL2DRender.gameObject);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
	}

	public void OnNextPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mNextPushed = true;
			mGame.PlaySe("SE_POSITIVE", -1);
		}
	}

	public void OnSkipPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			if (mOnClosed != null)
			{
				mOnClosed(mIndex, SELECT_ITEM.SKIP);
			}
			UnityEngine.Object.Destroy(mL2DTexture.gameObject);
			UnityEngine.Object.Destroy(mL2DRender.gameObject);
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void FakeClose(OnFakeClosed onFakeClosed)
	{
		mOnFakeClosed = onFakeClosed;
		mJelly.Close();
		mMaskSprite.gameObject.SetActive(false);
		mState.Change(STATE.FAKE_CLOSE);
	}

	public void FakeOpen()
	{
		mJelly.Open();
		mFrame.gameObject.SetActive(true);
		mMaskSprite.gameObject.SetActive(true);
		mState.Change(STATE.FAKE_OPEN);
	}

	public bool IsFakeClosed()
	{
		if (mState.GetStatus() == STATE.FAKE_CLOSE_WAIT)
		{
			return true;
		}
		return false;
	}

	public void SetClosedCallback(OnDialogClosed onClosed, OnDialogClosed onMessageFinished)
	{
		mOnClosed = onClosed;
		mOnMessageFinished = onMessageFinished;
	}
}
