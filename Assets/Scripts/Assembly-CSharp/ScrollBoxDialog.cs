using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScrollBoxDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum CONFIRM_DIALOG_STYLE
	{
		OK_ONLY = 0,
		YES_NO = 1,
		CLOSEBUTTON = 2,
		OK_CANCEL = 3,
		CLOSE_ONLY = 4,
		RETRY_CLOSE = 5,
		RETRY_ONLY = 6,
		CONFIRM_CLOSE = 7,
		CONFIRM_ONLY = 8,
		TIMER = 9,
		IMAGE_CHOICE = 10
	}

	public enum FRAME_STYLE
	{
		PINK_0 = 0,
		PINK_1 = 1,
		YELLOW = 2,
		GREEN = 3,
		EVENT = 4,
		MENU = 5
	}

	public enum MODE
	{
		NORMAL = 0,
		BLANK = 1,
		SEPARATOR = 2
	}

	private sealed class ClosedCallback : UnityEvent<SELECT_ITEM>
	{
	}

	private sealed class ScrollLabelItem : SMVerticalListItem
	{
		private UIFont mFont;

		private CreateConfig.DescriptionInfo mInfo;

		private int mFontSize;

		private int mDepth;

		private MODE mMode;

		public static ScrollLabelItem Make(CreateConfig.DescriptionInfo info, UIFont font, int font_size, int depth)
		{
			ScrollLabelItem scrollLabelItem = new ScrollLabelItem();
			scrollLabelItem.mFont = font;
			scrollLabelItem.mInfo = info;
			scrollLabelItem.mFontSize = font_size;
			scrollLabelItem.mDepth = depth;
			scrollLabelItem.mMode = MODE.NORMAL;
			return scrollLabelItem;
		}

		public static ScrollLabelItem MakeSeparator(int depth)
		{
			ScrollLabelItem scrollLabelItem = new ScrollLabelItem();
			scrollLabelItem.mDepth = depth;
			scrollLabelItem.mMode = MODE.SEPARATOR;
			return scrollLabelItem;
		}

		public static ScrollLabelItem MakeBlank(int depth)
		{
			ScrollLabelItem scrollLabelItem = new ScrollLabelItem();
			scrollLabelItem.mDepth = depth;
			scrollLabelItem.mMode = MODE.BLANK;
			return scrollLabelItem;
		}

		public void Set(GameObject parent, Vector2 item_size)
		{
			UISprite uISprite = Util.CreateSprite("RaycastTarget", parent, ResourceManager.LoadImage("HUD").Image);
			Util.SetSpriteInfo(uISprite, "null", mDepth + 1, Vector3.zero, Vector3.one, false, UIWidget.Pivot.TopLeft);
			uISprite.width = Mathf.CeilToInt(item_size.x);
			uISprite.height = Mathf.CeilToInt(item_size.y + 5f);
			Vector2 vector = uISprite.transform.localPosition;
			vector.x -= item_size.x * 0.5f;
			uISprite.transform.localPosition = vector;
			switch (mMode)
			{
			case MODE.SEPARATOR:
			{
				UISprite uISprite2 = Util.CreateSprite("Separator", parent, ResourceManager.LoadImage("MAIL_BOX").Image);
				Util.SetSpriteInfo(uISprite2, "line01", mDepth + 1, Vector3.zero, Vector3.one, false);
				uISprite2.width = Mathf.CeilToInt(item_size.x * 0.95f);
				uISprite2.height = 6;
				uISprite2.type = UIBasicSprite.Type.Sliced;
				switch (mInfo.Pivot)
				{
				case UIWidget.Pivot.TopLeft:
				case UIWidget.Pivot.Left:
				case UIWidget.Pivot.BottomLeft:
					vector = uISprite2.transform.localPosition;
					vector.y -= item_size.y * 0.5f;
					uISprite2.transform.localPosition = vector;
					break;
				}
				break;
			}
			case MODE.NORMAL:
			{
				string message = mInfo.Message;
				UILabel uILabel = Util.CreateLabel("Label", parent, mFont);
				Util.SetLabelInfo(uILabel, message, mDepth + 2, Vector3.zero, mFontSize, 0, 0, mInfo.Pivot);
				uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
				uILabel.SetDimensions((int)item_size.x, (int)item_size.y);
				uILabel.color = mInfo.Color;
				switch (mInfo.Pivot)
				{
				case UIWidget.Pivot.TopLeft:
				case UIWidget.Pivot.Left:
				case UIWidget.Pivot.BottomLeft:
					vector = uILabel.transform.localPosition;
					vector.x -= item_size.x * 0.5f;
					uILabel.transform.localPosition = vector;
					break;
				}
				break;
			}
			case MODE.BLANK:
				break;
			}
		}
	}

	public struct CreateConfig
	{
		public struct DescriptionInfo
		{
			public string Message;

			public Color Color;

			public bool IgnoreNextSeparator;

			public MODE Mode;

			public int CustomFontSize;

			public UIWidget.Pivot Pivot;
		}

		public string Title;

		public DescriptionInfo[] Description;

		public int DescriptionSize;

		public CONFIRM_DIALOG_STYLE Style;

		public DIALOG_SIZE Size;

		public FRAME_STYLE FrameStyle;

		public string PositiveButtonImageName;

		public string NegativeButtonImageName;

		public float Timer;

		public bool DisableOpneSE;

		public bool DisableButtonSE;

		public bool VisibleSeparator;

		public bool HideInnerFrame;

		public bool HideScrollBar;

		public Vector2 CustomInnerSize;

		public UIWidget.Pivot Pivot;

		private ClosedCallback mOnClosedCallback;

		private ClosedCallback OnClosedCallback
		{
			get
			{
				if (mOnClosedCallback == null)
				{
					mOnClosedCallback = new ClosedCallback();
				}
				return mOnClosedCallback;
			}
		}

		private void ExecCallbackFunction(UnityAction<UnityAction<SELECT_ITEM>> fnc, params UnityAction<SELECT_ITEM>[] args)
		{
			if (fnc == null || args == null)
			{
				return;
			}
			foreach (UnityAction<SELECT_ITEM> unityAction in args)
			{
				if (unityAction != null)
				{
					fnc(unityAction);
				}
			}
		}

		public void AddCallback(params UnityAction<SELECT_ITEM>[] call)
		{
			if (call != null && OnClosedCallback != null)
			{
				ExecCallbackFunction(OnClosedCallback.AddListener, call);
			}
		}

		public void RemoveCallback(params UnityAction<SELECT_ITEM>[] call)
		{
			if (call != null && OnClosedCallback != null)
			{
				ExecCallbackFunction(OnClosedCallback.RemoveListener, call);
			}
		}

		public void ClearCallback()
		{
			if (OnClosedCallback != null)
			{
				OnClosedCallback.RemoveAllListeners();
			}
		}

		public void SetCallback(params UnityAction<SELECT_ITEM>[] call)
		{
			ClearCallback();
			AddCallback(call);
		}

		public void InvokeCallback(SELECT_ITEM i)
		{
			if (OnClosedCallback != null)
			{
				OnClosedCallback.Invoke(i);
			}
		}
	}

	private CreateConfig mConfig;

	private int mReservedSortingOrder;

	public SELECT_ITEM SelectItem { get; private set; }

	public override void BuildDialog()
	{
		if (mReservedSortingOrder > 0)
		{
			mPanel.sortingOrder = mReservedSortingOrder;
			mPanel.depth = mReservedSortingOrder;
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont font = GameMain.LoadFont();
		GameObject parent = base.gameObject;
		string spriteKey = null;
		switch (mConfig.FrameStyle)
		{
		case FRAME_STYLE.PINK_0:
			spriteKey = "instruction_panel";
			break;
		case FRAME_STYLE.PINK_1:
			spriteKey = "instruction_panel01";
			break;
		case FRAME_STYLE.YELLOW:
			spriteKey = "instruction_panel11";
			break;
		case FRAME_STYLE.GREEN:
			spriteKey = "instruction_panel4";
			break;
		case FRAME_STYLE.EVENT:
			spriteKey = "event_instruction_panel";
			break;
		case FRAME_STYLE.MENU:
			spriteKey = "menu_instruction_panel";
			break;
		}
		InitDialogFrame(mConfig.Size, null, spriteKey);
		if (!string.IsNullOrEmpty(mConfig.Title))
		{
			InitDialogTitle(mConfig.Title);
		}
		if (!mConfig.HideInnerFrame)
		{
			UISprite uISprite = Util.CreateSprite("Inner", parent, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions((mConfig.CustomInnerSize.x == 0f) ? 500 : ((int)mConfig.CustomInnerSize.x), (mConfig.CustomInnerSize.y == 0f) ? 200 : ((int)mConfig.CustomInnerSize.y));
			uISprite.transform.localPosition = new Vector3(0f, 30f);
		}
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		Vector2 itemSize = new Vector2((mConfig.CustomInnerSize.x == 0f) ? 460 : ((int)mConfig.CustomInnerSize.x - 40), 0f);
		if (mConfig.Description != null)
		{
			for (int i = 0; i < mConfig.Description.Length; i++)
			{
				int num = mConfig.DescriptionSize;
				if (mConfig.Description[i].CustomFontSize > 0)
				{
					num = mConfig.Description[i].CustomFontSize;
				}
				ScrollLabelItem scrollLabelItem = ScrollLabelItem.Make(mConfig.Description[i], font, num, mBaseDepth + 2);
				if (scrollLabelItem != null)
				{
					int num2 = 0;
					if (!string.IsNullOrEmpty(mConfig.Description[i].Message))
					{
						num2 = mConfig.Description[i].Message.Split('\n').Length * num;
					}
					if ((float)num2 > itemSize.y)
					{
						itemSize.y = num2;
					}
					list.Add(scrollLabelItem);
					if (mConfig.VisibleSeparator && i + 1 < mConfig.Description.Length && !mConfig.Description[i].IgnoreNextSeparator)
					{
						ScrollLabelItem item = ScrollLabelItem.MakeSeparator(mBaseDepth + 2);
						list.Add(item);
					}
				}
			}
		}
		float num3 = 180f;
		if (mConfig.CustomInnerSize.y != 0f)
		{
			num3 = mConfig.CustomInnerSize.y - 20f;
		}
		SMVerticalListInfo listInfo = new SMVerticalListInfo(1, itemSize.x, num3, 1, itemSize.x, num3, 1, (int)itemSize.y + 5);
		SMVerticalList sMVerticalList = SMVerticalList.Make(parent, this, listInfo, list, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		sMVerticalList.ScrollBarPositionOffsetPortrait = new Vector3(20f, 0f);
		sMVerticalList.ScrollBarPositionOffsetLandscape = new Vector3(20f, 0f);
		sMVerticalList.ScrollListPivot = mConfig.Pivot;
		if (sMVerticalList != null)
		{
			Vector3 localPosition = sMVerticalList.transform.localPosition;
			localPosition.y = 30f;
			sMVerticalList.transform.localPosition = localPosition;
		}
		sMVerticalList.OnCreateItem = delegate(GameObject fn_item_go, int fn_index, SMVerticalListItem fn_item, SMVerticalListItem fn_last_item)
		{
			ScrollLabelItem scrollLabelItem2 = fn_item as ScrollLabelItem;
			if (fn_item_go != null)
			{
				scrollLabelItem2.Set(fn_item_go, itemSize);
			}
		};
		if (mConfig.HideScrollBar)
		{
			sMVerticalList.HideScrollBar();
		}
		string[] array = null;
		switch (mConfig.Style)
		{
		case CONFIRM_DIALOG_STYLE.OK_ONLY:
			array = new string[1] { "LC_button_ok2" };
			break;
		case CONFIRM_DIALOG_STYLE.CLOSEBUTTON:
		case CONFIRM_DIALOG_STYLE.CLOSE_ONLY:
			array = new string[1] { "LC_button_close" };
			break;
		case CONFIRM_DIALOG_STYLE.RETRY_CLOSE:
			array = new string[2] { "LC_button_retry", "LC_button_close" };
			break;
		case CONFIRM_DIALOG_STYLE.RETRY_ONLY:
			array = new string[1] { "LC_button_retry" };
			break;
		case CONFIRM_DIALOG_STYLE.CONFIRM_CLOSE:
			array = new string[2] { "LC_button_check", "LC_button_close" };
			break;
		case CONFIRM_DIALOG_STYLE.CONFIRM_ONLY:
			array = new string[1] { "LC_button_check" };
			break;
		case CONFIRM_DIALOG_STYLE.IMAGE_CHOICE:
			array = new string[2] { mConfig.PositiveButtonImageName, mConfig.NegativeButtonImageName };
			break;
		default:
			array = new string[2] { "LC_button_yes", "LC_button_no" };
			break;
		case CONFIRM_DIALOG_STYLE.TIMER:
			break;
		}
		if (array == null)
		{
			return;
		}
		float y = -370f;
		switch (mConfig.Size)
		{
		case DIALOG_SIZE.SMALL:
			y = -40f;
			break;
		case DIALOG_SIZE.MEDIUM:
			y = -125f;
			break;
		case DIALOG_SIZE.LARGE:
			y = -210f;
			break;
		case DIALOG_SIZE.XLARGE:
			y = -370f;
			break;
		}
		string[] array2 = new string[2] { "Positive", "Negative" };
		float num4 = 260f;
		for (int j = 0; j < array.Length; j++)
		{
			if (!string.IsNullOrEmpty(array[j]))
			{
				UIButton button = Util.CreateJellyImageButton("Button" + ((j >= array2.Length) ? string.Empty : array2[j]), parent, image);
				Util.SetImageButtonInfo(button, array[j], array[j], array[j], mBaseDepth + 4, new Vector3(num4 * (float)(array.Length - 1) * 0.5f * -1f + (float)j * num4, y, 0f), Vector3.one);
				Util.AddImageButtonMessage(button, this, "On" + ((j >= array2.Length) ? string.Empty : array2[j]) + "Pushed", UIButtonMessage.Trigger.OnClick);
			}
		}
	}

	public void ClearClosedCallback()
	{
		mConfig.ClearCallback();
	}

	public void Init(CreateConfig config)
	{
		mConfig = config;
		SetOpenSeEnable(!config.DisableOpneSE);
		if (config.Style == CONFIRM_DIALOG_STYLE.TIMER)
		{
			StartCoroutine(RoutineTimerUpdate());
		}
	}

	public void ReserveSortingOrder(int sorting_order)
	{
		mReservedSortingOrder = sorting_order;
	}

	public void SetCloseTimer(float timer)
	{
		mConfig.Timer = timer;
	}

	public void EnableButtonSe(bool enable)
	{
		mConfig.DisableButtonSE = !enable;
	}

	public void SetClosedCallback(params UnityAction<SELECT_ITEM>[] call)
	{
		mConfig.SetCallback(call);
	}

	public void AddClosedCallback(params UnityAction<SELECT_ITEM>[] call)
	{
		mConfig.AddCallback(call);
	}

	public void RemoveClosedCallback(params UnityAction<SELECT_ITEM>[] call)
	{
		mConfig.RemoveCallback(call);
	}

	private IEnumerator RoutineTimerUpdate()
	{
		if (mConfig.Style != CONFIRM_DIALOG_STYLE.TIMER)
		{
			yield break;
		}
		while (true)
		{
			if (!GetBusy())
			{
				mConfig.Timer -= Time.deltaTime;
				if (mConfig.Timer <= 0f || mGame.mBackKeyTrg)
				{
					break;
				}
			}
			yield return null;
		}
		Close();
	}

	public override void OnCloseFinished()
	{
		mConfig.InvokeCallback(SelectItem);
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	private void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (!mConfig.DisableButtonSE)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
			}
			SelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	private void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (!mConfig.DisableButtonSE)
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
			SelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}
}
