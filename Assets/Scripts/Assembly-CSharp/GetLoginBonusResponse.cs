using System;
using System.Collections.Generic;

public class GetLoginBonusResponse : ICloneable
{
	private string[] mCompanionData;

	public int LoginDayCount;

	private bool TimeLocalize
	{
		get
		{
			return false;
		}
	}

	public DateTime StartTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(StartTimeRaw, TimeLocalize);
		}
	}

	public DateTime EndTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(EndTimeRaw, TimeLocalize);
		}
	}

	public string CompanionName
	{
		get
		{
			if (mCompanionData != null)
			{
				return mCompanionData[0];
			}
			return string.Empty;
		}
	}

	public string CompanionBeforeMotionName
	{
		get
		{
			if (mCompanionData != null)
			{
				return mCompanionData[1];
			}
			return string.Empty;
		}
	}

	public string CompanionAfterMotionName
	{
		get
		{
			if (mCompanionData != null)
			{
				return mCompanionData[2];
			}
			return string.Empty;
		}
	}

	public float CompanionScale
	{
		get
		{
			if (mCompanionData != null)
			{
				return float.Parse(mCompanionData[3]);
			}
			return 0f;
		}
	}

	public bool IsDislpay
	{
		get
		{
			return IsResults == 1;
		}
	}

	[MiniJSONAlias("receive_bonus_id")]
	public int LoginID { get; set; }

	[MiniJSONAlias("category")]
	public string Category { get; set; }

	[MiniJSONAlias("sheet")]
	public int Sheet { get; set; }

	[MiniJSONAlias("image")]
	public string ImageURL { get; set; }

	[MiniJSONAlias("chara")]
	public string CompanionRaw { get; set; }

	[MiniJSONAlias("is_results")]
	public int IsResults { get; set; }

	[MiniJSONAlias("starttime")]
	public long StartTimeRaw { get; set; }

	[MiniJSONAlias("endtime")]
	public long EndTimeRaw { get; set; }

	[MiniJSONAlias("rewards")]
	public List<LoginBonusRewardData> RewardList { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public GetLoginBonusResponse Clone()
	{
		return MemberwiseClone() as GetLoginBonusResponse;
	}

	public void Convert()
	{
		mCompanionData = CompanionRaw.Split(',');
		LoginDayCount = 0;
		foreach (LoginBonusRewardData reward in RewardList)
		{
			if (reward.IsReceived)
			{
				LoginDayCount++;
			}
		}
	}
}
