using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class BIJBinaryReader : IDisposable
{
	protected string mPath;

	private BinaryReader mReader;

	public string Path
	{
		get
		{
			return mPath;
		}
	}

	public long RemainSize
	{
		get
		{
			if (mReader == null)
			{
				return 0L;
			}
			return mReader.BaseStream.Length - mReader.BaseStream.Position;
		}
	}

	protected virtual BinaryReader Reader
	{
		get
		{
			return mReader;
		}
		set
		{
			mReader = value;
		}
	}

	public BIJBinaryReader()
	{
		mReader = null;
	}

	public BIJBinaryReader(BinaryReader reader)
	{
		mReader = reader;
	}

	public void Dispose()
	{
		Close();
	}

	public virtual void Close()
	{
		if (mReader != null)
		{
			try
			{
				mReader.Close();
			}
			catch (Exception)
			{
			}
			mReader = null;
		}
	}

	public long Seek(long offset, SeekOrigin origin)
	{
		long result = 0L;
		Stream baseStream = Reader.BaseStream;
		if (baseStream != null)
		{
			result = baseStream.Seek(offset, origin);
		}
		return result;
	}

	public byte[] ReadAll()
	{
		using (MemoryStream memoryStream = new MemoryStream())
		{
			byte[] array = new byte[4096];
			int count;
			while ((count = Reader.Read(array, 0, array.Length)) != 0)
			{
				memoryStream.Write(array, 0, count);
			}
			return memoryStream.ToArray();
		}
	}

	public int ReadRaw(byte[] buffer, int index, int count)
	{
		return Reader.Read(buffer, index, count);
	}

	public bool ReadBool()
	{
		return 0 != ReadByte();
	}

	public bool[] ReadBoolArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		bool[] array = new bool[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadBool();
		}
		return array;
	}

	public bool[] ReadBoolArray_32L()
	{
		return ReadBoolArray(true);
	}

	public byte ReadByte()
	{
		return Reader.ReadByte();
	}

	public byte[] ReadByteArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		byte[] array = new byte[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadByte();
		}
		return array;
	}

	public byte[] ReadByteArray_32L()
	{
		return ReadByteArray(true);
	}

	public short ReadShort()
	{
		ushort[] array = new ushort[2]
		{
			Reader.ReadByte(),
			Reader.ReadByte()
		};
		return (short)(((array[0] << 8) & 0xFF00) | (array[1] & 0xFF));
	}

	public ushort ReadUShort()
	{
		ushort[] array = new ushort[2]
		{
			Reader.ReadByte(),
			Reader.ReadByte()
		};
		return (ushort)(((uint)(array[0] << 8) & 0xFF00u) | (array[1] & 0xFFu));
	}

	public short[] ReadShortArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		short[] array = new short[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadShort();
		}
		return array;
	}

	public short[] ReadShortArray_32L()
	{
		return ReadShortArray(true);
	}

	public List<int> ReadShortArrayToIntList(bool isIntLength = false)
	{
		List<int> list = new List<int>();
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		for (int i = 0; i < num; i++)
		{
			short item = ReadShort();
			list.Add(item);
		}
		return list;
	}

	public List<int> ReadShortArrayToIntList_32L()
	{
		return ReadShortArrayToIntList(true);
	}

	public int ReadInt()
	{
		uint[] array = new uint[2]
		{
			(uint)ReadShort(),
			(uint)ReadShort()
		};
		return ((int)(array[0] << 16) & -65536) | (int)(array[1] & 0xFFFF);
	}

	public int[] ReadIntArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		int[] array = new int[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadInt();
		}
		return array;
	}

	public int[] ReadIntArray_32L()
	{
		return ReadIntArray(true);
	}

	public long ReadLong()
	{
		ulong[] array = new ulong[2]
		{
			(ulong)ReadInt(),
			(ulong)ReadInt()
		};
		return ((long)(array[0] << 32) & -4294967296L) | (long)(array[1] & 0xFFFFFFFFu);
	}

	public long[] ReadLongArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		long[] array = new long[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadLong();
		}
		return array;
	}

	public long[] ReadLongArray_32L()
	{
		return ReadLongArray(true);
	}

	public float ReadFloat()
	{
		byte[] array = new byte[4];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadByte();
		}
		if (BitConverter.IsLittleEndian)
		{
			Array.Reverse(array);
		}
		return BitConverter.ToSingle(array, 0);
	}

	public float[] ReadFloatArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		float[] array = new float[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadFloat();
		}
		return array;
	}

	public float[] ReadFloatArray_32L()
	{
		return ReadFloatArray(true);
	}

	public double ReadDouble()
	{
		byte[] array = new byte[8];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadByte();
		}
		if (BitConverter.IsLittleEndian)
		{
			Array.Reverse(array);
		}
		return BitConverter.ToDouble(array, 0);
	}

	public double[] ReadDoubleArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		double[] array = new double[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadDouble();
		}
		return array;
	}

	public double[] ReadDoubleArray_32L()
	{
		return ReadDoubleArray(true);
	}

	public string ReadUTF(bool isIntLength = false)
	{
		byte[] array = ReadByteArray(isIntLength);
		if (array == null || array.Length == 0)
		{
			return null;
		}
		return Encoding.UTF8.GetString(array);
	}

	public string ReadUTF_32L()
	{
		return ReadUTF(true);
	}

	public string[] ReadUTFArray(bool isIntLength = false)
	{
		int num = 0;
		num = ((!isIntLength) ? ReadUShort() : ReadInt());
		string[] array = new string[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = ReadUTF(isIntLength);
		}
		return array;
	}

	public string[] ReadUTFArray_32L()
	{
		return ReadUTFArray(true);
	}

	public DateTime ReadDateTime()
	{
		try
		{
			string s = ReadUTF();
			return DateTime.Parse(s).ToLocalTime();
		}
		catch (Exception)
		{
			return new DateTime(0L);
		}
	}
}
