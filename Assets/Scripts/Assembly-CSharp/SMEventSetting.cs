public class SMEventSetting : SMMapPageSettingBase
{
	public int EventID;

	public Def.EVENT_TYPE EventType;

	public string Title = string.Empty;

	public string HudAtlasKey = string.Empty;

	public string MapAtlasKey = string.Empty;

	public string MapBackName = string.Empty;

	public string MapEventTitle = string.Empty;

	public string MapEventBGM = string.Empty;

	public short AdvButtonFlg;

	public SMEventSetting(int eventNo)
	{
		EventID = eventNo;
	}

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteInt(EventID);
		a_writer.WriteInt((int)EventType);
		a_writer.WriteUTF(Title);
		a_writer.WriteUTF(HudAtlasKey);
		a_writer.WriteUTF(MapAtlasKey);
		a_writer.WriteUTF(MapBackName);
		a_writer.WriteUTF(MapEventTitle);
		a_writer.WriteUTF(MapEventBGM);
		a_writer.WriteShort(AdvButtonFlg);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		EventID = a_reader.ReadInt();
		EventType = (Def.EVENT_TYPE)a_reader.ReadInt();
		Title = a_reader.ReadUTF();
		HudAtlasKey = a_reader.ReadUTF();
		MapAtlasKey = a_reader.ReadUTF();
		MapBackName = a_reader.ReadUTF();
		MapEventTitle = a_reader.ReadUTF();
		MapEventBGM = a_reader.ReadUTF();
		AdvButtonFlg = a_reader.ReadShort();
	}
}
