using System.IO;
using System.Text;

internal static class ReadOp
{
	public static byte ReadByte(BinaryReader reader)
	{
		return reader.ReadByte();
	}

	public static short ReadShort(BinaryReader reader)
	{
		byte b = reader.ReadByte();
		byte b2 = reader.ReadByte();
		return (short)((b << 8) | b2);
	}

	public static int ReadInt(BinaryReader reader)
	{
		byte b = reader.ReadByte();
		byte b2 = reader.ReadByte();
		byte b3 = reader.ReadByte();
		byte b4 = reader.ReadByte();
		return (b << 24) | (b2 << 16) | (b3 << 8) | b4;
	}

	public static string ReadUTF(BinaryReader reader)
	{
		int num = ReadShort(reader);
		if (num <= 0)
		{
			return null;
		}
		StringBuilder stringBuilder = new StringBuilder(num);
		for (int i = 0; i < num; i++)
		{
			stringBuilder.Append(reader.ReadChar());
		}
		return stringBuilder.ToString();
	}
}
