using System;

[Flags]
public enum SNS_FLAG
{
	None = 0,
	Startup = 1,
	Auto = 2,
	Option = 4,
	Invite = 8,
	Post = 0x10,
	Screenshot = 0x20,
	MyPage = 0x40,
	FriendSearch = 0x80,
	Nickname = 0x100,
	MyIcon = 0x200,
	Achievement = 0x400,
	All = 0xFFFF
}
