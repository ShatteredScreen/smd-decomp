using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class NotificationManager : MonoBehaviour
{
	public delegate void OnScheduleHandler();

	public const int DAY_SECONDS = 86400;

	public const int HOUR_SECONDS = 3600;

	private static object mInstanceLock = new object();

	private static NotificationManager mInstance = null;

	private bool mIsPaused;

	private bool isTerminating;

	public static NotificationManager Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					mInstance = (NotificationManager)gameObject.GetComponent(typeof(NotificationManager));
				}
				return mInstance;
			}
		}
	}

	public static bool IsNotificationEnabled
	{
		get
		{
			return BIJUnity.isEnableRemoteNotification();
		}
		set
		{
			BIJUnity.enableRemoteNotification(value);
		}
	}

	[method: MethodImpl(32)]
	public static event OnScheduleHandler OnLoginSchedule;

	[method: MethodImpl(32)]
	public static event OnScheduleHandler OnStatusSchedule;

	private static void log(string format, params object[] args)
	{
		try
		{
		}
		catch (Exception)
		{
		}
	}

	private static void dbg(string format, params object[] args)
	{
		try
		{
		}
		catch (Exception)
		{
		}
	}

	public static void scheduleNotification(string message, string title, int afterSeconds)
	{
		if (IsNotificationEnabled)
		{
			BIJUnity.scheduleLocalNotification(message, title, afterSeconds);
		}
	}

	public static void scheduleNotification(int nid, string message, string title, int afterSeconds)
	{
		if (IsNotificationEnabled)
		{
			BIJUnity.scheduleLocalNotification(nid, message, title, afterSeconds);
		}
	}

	private void scheduleLogin()
	{
		if (IsNotificationEnabled && NotificationManager.OnLoginSchedule != null)
		{
			NotificationManager.OnLoginSchedule();
		}
	}

	private void scheduleNotifications()
	{
		if (IsNotificationEnabled && NotificationManager.OnStatusSchedule != null)
		{
			NotificationManager.OnStatusSchedule();
		}
	}

	private void Awake()
	{
	}

	private void Start()
	{
		startGame();
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			pause();
		}
		else
		{
			resume();
		}
	}

	private void OnDestroy()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			quitGame();
		}
	}

	private void OnApplicationQuit()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			quitGame();
		}
	}

	public void playGame()
	{
		dbg("NotificationManager: playGame");
	}

	private void pause()
	{
		dbg("NotificationManager: pause");
		mIsPaused = true;
		scheduleLogin();
		scheduleNotifications();
	}

	private void resume()
	{
		dbg("NotificationManager: resume");
		mIsPaused = false;
		BIJUnity.cancelAllLocalNotification();
	}

	private void startGame()
	{
		dbg("NotificationManager: startGame");
		BIJUnity.cancelAllLocalNotification();
	}

	private void quitGame()
	{
		dbg("NotificationManager: quitGame");
		if (!mIsPaused)
		{
			scheduleLogin();
			scheduleNotifications();
		}
	}
}
