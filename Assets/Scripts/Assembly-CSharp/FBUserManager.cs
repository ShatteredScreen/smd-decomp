using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FBUserManager
{
	private enum STATE
	{
		IDLE = 0,
		GET_ICON = 1,
		WAIT_ICON = 2
	}

	public delegate void OnIconGetCallback(bool ret, int uuid, string fbid, Texture2D icon);

	private OnIconGetCallback mCallback;

	private long mIconGetSpan = 10080L;

	private STATE mState;

	private Dictionary<int, FBUserData> mDict = new Dictionary<int, FBUserData>();

	private int mUnloadedNum;

	private FBIconManager mIconMng;

	private FBIconRequest mIconRequest;

	private FBUserData mCurrentData;

	private bool mPriority;

	public static FBUserManager CreateInstance()
	{
		FBUserManager fBUserManager = new FBUserManager();
		fBUserManager.Awake();
		SingletonMonoBehaviour<FBIconManager>.Instance.AddFBUserManager(fBUserManager);
		return fBUserManager;
	}

	public void SetPriority(bool aPriority)
	{
		mPriority = aPriority;
	}

	public void SetIconGetSpan(long aIconGetSpan)
	{
		mIconGetSpan = aIconGetSpan;
	}

	public bool IsAllIconComplete()
	{
		if (mUnloadedNum == 0)
		{
			return true;
		}
		return false;
	}

	public Texture2D GetIcon(int uuid)
	{
		FBUserData value;
		if (mDict.TryGetValue(uuid, out value))
		{
			return value.icon;
		}
		return null;
	}

	public FBUserData.STATE GetIconStatus(int uuid)
	{
		FBUserData value;
		if (mDict.TryGetValue(uuid, out value))
		{
			return value.status;
		}
		return FBUserData.STATE.FAILURE;
	}

	public bool IsIconCanged(int uuid)
	{
		FBUserData value;
		if (mDict.TryGetValue(uuid, out value))
		{
			return value.changed;
		}
		return false;
	}

	public void SetFBUserList(List<SearchUserData> aList)
	{
		if (!GameMain.IsFacebookIconEnable())
		{
			return;
		}
		Init();
		FBUserData.STATE dataState = FBUserData.STATE.DEFAULT;
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		if (GameMain.IsSNSLogined(sns))
		{
			dataState = FBUserData.STATE.LOADING;
		}
		for (int i = 0; i < aList.Count; i++)
		{
			SearchUserData searchUserData = aList[i];
			if (searchUserData.iconid >= 10000 && !string.IsNullOrEmpty(searchUserData.fbid))
			{
				AddDictionary(searchUserData.uuid, searchUserData.fbid, dataState);
			}
		}
	}

	public void SetFBUserList(List<GiftItemData> aList)
	{
		if (!GameMain.IsFacebookIconEnable())
		{
			return;
		}
		Init();
		FBUserData.STATE dataState = FBUserData.STATE.DEFAULT;
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		if (GameMain.IsSNSLogined(sns))
		{
			dataState = FBUserData.STATE.LOADING;
		}
		for (int i = 0; i < aList.Count; i++)
		{
			GiftItemData giftItemData = aList[i];
			if (giftItemData.IconID >= 10000 && !string.IsNullOrEmpty(giftItemData.Fbid))
			{
				AddDictionary(giftItemData.FromID, giftItemData.Fbid, dataState);
			}
		}
	}

	public void SetFBUserList(List<MyFaceBookFriend> aList)
	{
		if (!GameMain.IsFacebookIconEnable())
		{
			return;
		}
		Init();
		FBUserData.STATE dataState = FBUserData.STATE.DEFAULT;
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		if (GameMain.IsSNSLogined(sns))
		{
			dataState = FBUserData.STATE.LOADING;
		}
		for (int i = 0; i < aList.Count; i++)
		{
			MyFaceBookFriend myFaceBookFriend = aList[i];
			if (myFaceBookFriend.IconID >= 10000 && !string.IsNullOrEmpty(myFaceBookFriend.Fbid))
			{
				AddDictionary(myFaceBookFriend.UUID, myFaceBookFriend.Fbid, dataState);
			}
		}
	}

	public void SetFBUserList(List<MyFriend> aList, OnIconGetCallback aCallback)
	{
		if (!GameMain.IsFacebookIconEnable())
		{
			return;
		}
		Init();
		mCallback = aCallback;
		FBUserData.STATE dataState = FBUserData.STATE.DEFAULT;
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		if (GameMain.IsSNSLogined(sns))
		{
			dataState = FBUserData.STATE.LOADING;
		}
		for (int i = 0; i < aList.Count; i++)
		{
			MyFriend myFriend = aList[i];
			if (myFriend.Data.IconID >= 10000 && !string.IsNullOrEmpty(myFriend.Data.Fbid))
			{
				AddDictionary(myFriend.Data.UUID, myFriend.Data.Fbid, dataState);
			}
		}
	}

	private void AddDictionary(int uuid, string fbid, FBUserData.STATE dataState)
	{
		Texture2D texture2D = null;
		FileInfo iconFileInfo = mIconMng.GetIconFileInfo(uuid);
		if (iconFileInfo != null)
		{
			long num = DateTimeUtil.BetweenMinutes(iconFileInfo.LastWriteTime, DateTime.Now);
			if (num < mIconGetSpan)
			{
				texture2D = mIconMng.GetIcon(uuid);
				if (texture2D != null)
				{
					dataState = FBUserData.STATE.SUCCESS;
				}
			}
		}
		FBUserData fBUserData = new FBUserData(uuid, fbid, texture2D);
		fBUserData.status = dataState;
		if (!mDict.ContainsKey(uuid))
		{
			mDict.Add(uuid, fBUserData);
			if (dataState == FBUserData.STATE.LOADING)
			{
				mUnloadedNum++;
			}
		}
	}

	private void Init()
	{
		mState = STATE.IDLE;
		mIconMng = SingletonMonoBehaviour<FBIconManager>.Instance;
		mUnloadedNum = 0;
		mDict.Clear();
		mIconRequest = null;
		mCurrentData = null;
	}

	private void Awake()
	{
		Init();
	}

	public void Update()
	{
		switch (mState)
		{
		case STATE.IDLE:
			if (mUnloadedNum > 0)
			{
				mState = STATE.GET_ICON;
			}
			break;
		case STATE.GET_ICON:
		{
			foreach (FBUserData value in mDict.Values)
			{
				if (value.status == FBUserData.STATE.LOADING)
				{
					mIconRequest = mIconMng.GetIconRequest(value.uuid, value.fbid, mPriority);
					mCurrentData = value;
					mState = STATE.WAIT_ICON;
					break;
				}
			}
			break;
		}
		case STATE.WAIT_ICON:
			if (mIconRequest.isDone)
			{
				bool ret = false;
				if (mIconRequest.icon != null)
				{
					mCurrentData.icon = mIconRequest.icon;
					mCurrentData.status = FBUserData.STATE.SUCCESS;
					ret = true;
				}
				else
				{
					mCurrentData.status = FBUserData.STATE.FAILURE;
				}
				mCurrentData.changed = true;
				if (mCallback != null)
				{
					mCallback(ret, mCurrentData.uuid, mCurrentData.fbid, mCurrentData.icon);
				}
				mUnloadedNum--;
				mIconRequest = null;
				mState = STATE.IDLE;
			}
			break;
		}
	}
}
