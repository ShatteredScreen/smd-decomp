using System;
using System.Collections;
using UnityEngine;

public class EventLabyrinthButton : MapStageButton
{
	public enum StatusChangeType
	{
		NONE = 0,
		TO_UNLOCK = 1,
		TO_CLEAR = 2
	}

	public const int FRAME_LOCK = 10;

	public const int FRAME_CLEAR = 0;

	public const int FRAME_CLEAR_STOP = 9;

	public const int FRAME_UNLOCK = 10;

	public const int FRAME_UNLOCK_LOOP = 44;

	public const int FRAME_JEWEL_LOCK = 0;

	public const int FRAME_JEWEL_UNLOCK = 0;

	public const int FRAME_JEWEL_UNLOCK_LOOP = 50;

	private Color mColor;

	private OneShotAnime mBox;

	private OneShotAnime mChara;

	private OneShotAnime mGoalJewel;

	protected short mCourseID;

	private StatusChangeType mButtonChangeType;

	private bool mIsStage = true;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public void SetStatusTypeReserved(StatusChangeType a_flg)
	{
		mButtonChangeType = a_flg;
	}

	public override void Init(Vector3 pos, float scale, int stageNo, byte starNum, Player.STAGE_STATUS mode, BIJImage atlas, string a_animeKey, bool a_partnerZ = false, int displayNo = -1)
	{
		mUseColliderReciprocal = true;
		base.DEFAULT_SCALE = 1f;
		mAtlas = atlas;
		mBaseScale = scale;
		mStarNum = starNum;
		mStageNo = stageNo;
		mHasItem = a_partnerZ;
		DefaultAnimeKey = a_animeKey;
		DisplayNumber = displayNo;
		if (stageNo == 10000 || stageNo == 20000)
		{
			mIsStage = false;
		}
		bool force = false;
		if (mMode != mode)
		{
			force = true;
		}
		mMode = mode;
		mColliderCenter = new Vector3(0f, 60f, 0f);
		mColliderSize = 120;
		string defaultAnimeKey = DefaultAnimeKey;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
		SMMapPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		SMEventPageData sMEventPageData = currentEventPageData as SMEventPageData;
		mColor = Color.gray;
		Vector3 vector = new Vector3(0f, -25f, -5f);
		int num = -1;
		int num2 = -1;
		if (mIsStage)
		{
			mChangeImageDict["change_gate"] = "null";
			mChangeImageDict["change_door"] = "null";
			mChangeImageDict["change_level"] = "null";
			mChangeImageDict["change_level_v2"] = "null";
			mChangeImageDict["change_num"] = "null";
			mChangeImageDict["change_num_v2"] = "null";
			mChangeImageDict["change_star1"] = "null";
			mChangeImageDict["change_star2"] = "null";
			mChangeImageDict["change_star3"] = "null";
			int a_main;
			int a_sub;
			Def.SplitStageNo(mStageNo, out a_main, out a_sub);
			SMEventLabyrinthStageSetting labyrinthStageSetting = sMEventPageData.GetLabyrinthStageSetting(a_main, a_sub, mCourseID);
			SMMapStageSetting mapStage = sMEventPageData.GetMapStage(a_main, a_sub, mCourseID);
			if (base.UseCurrentAnime)
			{
				force = true;
				defaultAnimeKey = DefaultAnimeKey;
			}
			bool flag = false;
			bool flag2 = false;
			mChangeImageDict["change_num"] = "event_btn_num" + a_main;
			mChangeImageDict["change_num_v2"] = string.Format("event_btn_num{0}_v2", a_main);
			mChangeImageDict["change_level"] = string.Format("LC_event_btn_lv{0:00}", labyrinthStageSetting.Difficulty);
			mChangeImageDict["change_level_v2"] = string.Format("LC_event_btn_lv{0:00}_v2", labyrinthStageSetting.Difficulty);
			mChangeImageDict["change_gate"] = string.Format("event_btn{0:00}", labyrinthStageSetting.Difficulty);
			switch (mMode)
			{
			case Player.STAGE_STATUS.LOCK:
				flag = true;
				flag2 = true;
				num = 10;
				break;
			case Player.STAGE_STATUS.UNLOCK:
				flag = true;
				flag2 = true;
				num = ((mButtonChangeType != StatusChangeType.TO_UNLOCK) ? 44 : 10);
				break;
			case Player.STAGE_STATUS.CLEAR:
				flag = true;
				num = ((mButtonChangeType != StatusChangeType.TO_CLEAR) ? 9 : 0);
				mChangeImageDict["change_door"] = string.Format("event_btn{0:00}_clear", labyrinthStageSetting.Difficulty);
				break;
			}
			mChangeImageDict["change_star1"] = "scorestar_back";
			mChangeImageDict["change_star2"] = "scorestar_back";
			mChangeImageDict["change_star3"] = "scorestar_back";
			if (starNum >= 1)
			{
				mChangeImageDict["change_star1"] = "scorestar";
			}
			if (starNum >= 2)
			{
				mChangeImageDict["change_star2"] = "scorestar";
			}
			if (starNum >= 3)
			{
				mChangeImageDict["change_star3"] = "scorestar";
			}
			if (flag && mBox == null)
			{
				short boxKind = labyrinthStageSetting.BoxKind;
				bool flag3 = false;
				if (data.LabyrinthData.ClearedStages.ContainsKey(mCourseID))
				{
					flag3 = data.LabyrinthData.ClearedStages[mCourseID].Contains((short)mStageNo);
				}
				BIJImage image = ResourceManager.LoadImage("EVENTLABYRINTH_REWARD").Image;
				string empty = string.Empty;
				CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[1];
				if (!flag3)
				{
					empty = GameStateEventLabyrinth.RewardBoxAnimationFormat;
					array[0] = new CHANGE_PART_INFO
					{
						partName = "change_000",
						spriteName = string.Format("adv_TBox{0}_getA", boxKind)
					};
					mBox = Util.CreateOneShotAnime(pos: new Vector3(50f, 10f, -5f), name: "Box" + mStageNo, key: empty, parent: base.gameObject, scale: new Vector3(0.75f, 0.75f, 1f), rotateDeg: 0f, delay: 0f, changePartsArray: array, atlas: image, autoDestroy: false, onFinished: delegate(SsSprite sprite)
					{
						sprite.AnimFrame = 0f;
						sprite.Play();
					});
					mBox.Play();
				}
				else
				{
					empty = GameStateEventLabyrinth.RewardFixedSpriteAnimationFormat;
					array[0] = new CHANGE_PART_INFO
					{
						partName = "change000",
						spriteName = string.Format("adv_TBox{0}_getB", boxKind)
					};
					mBox = Util.CreateOneShotAnime(pos: new Vector3(53f, 24f, -5f), name: "Box" + mStageNo, key: empty, parent: base.gameObject, scale: new Vector3(0.75f, 0.75f, 1f), rotateDeg: 0f, delay: 0f, changePartsArray: array, atlas: image, autoDestroy: false, onFinished: (SsSprite.AnimationCallback)delegate
					{
					});
					mBox.Pause();
				}
			}
			if (flag2 && mChara == null && mHasItem)
			{
				BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
				string key = "EFFECT_ICON_FIXEDSPRITE";
				int index = mapStage.EnterPartners[0];
				CompanionData companionData = SingletonMonoBehaviour<GameMain>.Instance.mCompanionData[index];
				string iconName = companionData.iconName;
				mChara = Util.CreateOneShotAnime("Partner" + mStageNo, key, base.gameObject, new Vector3(-53f, 16f, -5f), new Vector3(0.4f, 0.4f, 1f), new CHANGE_PART_INFO[1]
				{
					new CHANGE_PART_INFO
					{
						partName = "change000",
						spriteName = iconName
					}
				}, image2, false);
				mChara.Pause();
			}
		}
		else if (mStageNo == 10000)
		{
			mChangeImageDict["change000"] = "event_btn_start";
		}
		else if (mStageNo == 20000)
		{
			mChangeImageDict["change000"] = "event_btn_goal00";
			if (mGoalJewel == null)
			{
				pos += new Vector3(0f, 0f, 0f);
				mGoalJewel = Util.CreateOneShotAnime("JewelCup", GameStateEventLabyrinth.GoalAnimationNameFormat, base.gameObject, new Vector3(0f, 60f, -1f), Vector3.one, 0f, 0f, false, (SsSprite.AnimationCallback)delegate
				{
				});
			}
			num2 = ((mMode != Player.STAGE_STATUS.LOCK && (mMode != Player.STAGE_STATUS.UNLOCK || mButtonChangeType != StatusChangeType.TO_UNLOCK)) ? 50 : 0);
		}
		ChangeAnime(defaultAnimeKey, mChangeImageDict, force, 1);
		_sprite.transform.localScale = new Vector3(mBaseScale, mBaseScale, 1f);
		_sprite.transform.localPosition = pos;
		if (mIsStage)
		{
			_sprite.AnimFrame = num;
			_sprite.PlayAtStart = false;
		}
		else
		{
			num = 0;
			if (mGoalJewel != null)
			{
				mGoalJewel._sprite.AnimFrame = num2;
				if (mMode == Player.STAGE_STATUS.UNLOCK && mButtonChangeType == StatusChangeType.NONE)
				{
					mGoalJewel._sprite.AnimationFinished = delegate(SsSprite sprite)
					{
						sprite.AnimFrame = 50f;
						sprite.Play();
					};
					mGoalJewel.Play();
				}
				else
				{
					mGoalJewel.Pause();
				}
			}
		}
		if (mIsStage && mMode == Player.STAGE_STATUS.UNLOCK && mButtonChangeType == StatusChangeType.NONE)
		{
			_sprite.AnimationFinished = delegate(SsSprite sprite)
			{
				sprite.AnimFrame = 44f;
				sprite.Play();
			};
			_sprite.Play();
		}
		else
		{
			_sprite.AnimationFinished = null;
			_sprite.Pause();
		}
		if (mIsStage || mStageNo == 20000)
		{
			mBoxCollider.center = mColliderCenter;
			mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
			if (!mFirstInitialized)
			{
				EventDelegate item = new EventDelegate(this, "OnButtonPushed");
				mEventTrigger.onPress.Add(item);
				EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
				mEventTrigger.onRelease.Add(item2);
				EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
				mEventTrigger.onClick.Add(item3);
			}
		}
		else
		{
			mBoxCollider.size = new Vector3(0f, 0f, 0f);
		}
		mFirstInitialized = true;
	}

	public override void SetDebugInfo(SMMapStageSetting a_setting)
	{
		if (a_setting == null)
		{
		}
	}

	public override void SetColor(Color col)
	{
	}

	public IEnumerator DisappearBox()
	{
		if (mBox == null)
		{
			yield break;
		}
		float scale = 1f;
		float time = 0f;
		while (scale > 0f)
		{
			time += Time.deltaTime * 540f;
			scale = 1f - Mathf.Sin(time * ((float)Math.PI / 180f));
			float y = Mathf.Sin(time * ((float)Math.PI / 180f)) * 30f;
			if (time > 90f)
			{
				scale = 0f;
				NGUITools.SetActive(mBox.gameObject, false);
			}
			mBox.transform.localScale = new Vector3(scale, mBox.transform.localScale.y, mBox.transform.localScale.z);
			mBox.transform.localPosition = new Vector3(mBox.transform.localPosition.x, mBox.transform.localPosition.y + y, mBox.transform.localPosition.z);
			yield return null;
		}
		UnityEngine.Object.Destroy(mBox.gameObject);
		mBox = null;
	}

	public IEnumerator DisappearGoal()
	{
		if (mGoalJewel == null)
		{
			yield break;
		}
		float scale = 1f;
		float time = 0f;
		while (scale > 0f)
		{
			time += Time.deltaTime * 540f;
			scale = 1f - Mathf.Sin(time * ((float)Math.PI / 180f));
			float y = Mathf.Sin(time * ((float)Math.PI / 180f)) * 30f;
			if (time > 90f)
			{
				scale = 0f;
				NGUITools.SetActive(mGoalJewel.gameObject, false);
			}
			mGoalJewel.transform.localScale = new Vector3(scale, mGoalJewel.transform.localScale.y, mGoalJewel.transform.localScale.z);
			mGoalJewel.transform.localPosition = new Vector3(mGoalJewel.transform.localPosition.x, mGoalJewel.transform.localPosition.y + y, mGoalJewel.transform.localPosition.z);
			yield return null;
		}
		UnityEngine.Object.Destroy(mGoalJewel.gameObject);
		mGoalJewel = null;
	}

	public IEnumerator ClearProcess()
	{
		SetStatusTypeReserved(StatusChangeType.NONE);
		_sprite.AnimFrame = 0f;
		_sprite.Pause();
		yield return null;
		SsPart part = _sprite.GetPart("change_btnXX_Mroot");
		if (part == null)
		{
			yield break;
		}
		bool isClearFinished = false;
		SsPart.KeyframeCallback handler2 = null;
		handler2 = delegate(SsPart a_part, SsAttrValueInterface a_val)
		{
			SsUserDataKeyValue ssUserDataKeyValue = a_val as SsUserDataKeyValue;
			if (ssUserDataKeyValue.String.CompareTo("CLEAR_FINISH") == 0)
			{
				a_part.OnUserDataKey -= handler2;
				isClearFinished = true;
			}
		};
		part.OnUserDataKey += handler2;
		_sprite.Play();
		mGame.PlaySe("SE_DAILY_PRESS_STAMP", 2);
		float _start = Time.realtimeSinceStartup;
		while (!isClearFinished)
		{
			float timeSpan = Time.realtimeSinceStartup;
			if (timeSpan - _start > 3f)
			{
				part.OnUserDataKey -= handler2;
				isClearFinished = true;
			}
			yield return null;
		}
		_sprite.AnimFrame = 9f;
		_sprite.Pause();
	}

	public IEnumerator UnlockProcess()
	{
		SetStatusTypeReserved(StatusChangeType.NONE);
		bool isAnimeFinished = false;
		if (mStageNo == 20000)
		{
			if (mGoalJewel != null)
			{
				mGoalJewel._sprite.AnimFrame = 0f;
				mGoalJewel._sprite.AnimationFinished = delegate(SsSprite sprite)
				{
					sprite.AnimFrame = 50f;
					sprite.Play();
					isAnimeFinished = true;
				};
				mGame.PlaySe("SE_BOMB_BOMB", 2);
				mGoalJewel.Play();
			}
		}
		else
		{
			_sprite.AnimFrame = 10f;
			_sprite.AnimationFinished = delegate(SsSprite sprite)
			{
				sprite.AnimFrame = 44f;
				sprite.Play();
				isAnimeFinished = true;
			};
			_sprite.Play();
		}
		float _start = Time.realtimeSinceStartup;
		while (!isAnimeFinished)
		{
			float timeSpan = Time.realtimeSinceStartup;
			if (timeSpan - _start > 3f)
			{
				isAnimeFinished = true;
			}
			yield return null;
		}
	}
}
