using System.Collections.Generic;
using UnityEngine;

public class IdResultDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_00 = 2,
		NETWORK_00_1 = 3,
		NETWORK_00_2 = 4,
		NETWORK_00_3 = 5
	}

	public enum ID_RESULT
	{
		HIT = 0,
		ALREADY = 1,
		ALREADY_APPLY = 2,
		NOTFOUND = 3,
		MYSELF = 4,
		ERROR = 5
	}

	public delegate void OnDialogClosed(bool ResultFlg = false);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	public string mFriendID;

	public ID_RESULT mItem = ID_RESULT.NOTFOUND;

	private OnDialogClosed mCallback;

	private ConfirmDialog mConfirmDialog;

	private UIInput mInputs;

	private SearchUserData mSearchData;

	public bool mResultOK;

	private FBIconManager mIconMng;

	private FBIconRequest mIconRequest;

	private FBUserData mCurrentData;

	private List<IdResultIconCheckData> mIdResultIconCheckData = new List<IdResultIconCheckData>();

	private bool mApplySearchUserSucceed;

	private bool mApplySearchUserCheckDone;

	public override void Start()
	{
		mIconMng = SingletonMonoBehaviour<FBIconManager>.Instance;
		base.Start();
		Server.GiveApplyEvent += Server_OnGiveApply;
	}

	public override void OnDestroy()
	{
		Server.GiveApplyEvent -= Server_OnGiveApply;
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
		{
			if (mIdResultIconCheckData.Count <= 0 || !mIconRequest.isDone)
			{
				break;
			}
			IdResultIconCheckData idResultIconCheckData = mIdResultIconCheckData[0];
			BIJImage image = ResourceManager.LoadImage("ICON").Image;
			if (idResultIconCheckData.loadingIcon != null)
			{
				Object.Destroy(idResultIconCheckData.loadingIcon);
			}
			if (mIconRequest.icon != null)
			{
				UITexture uITexture = Util.CreateGameObject("Icon", idResultIconCheckData.boardImage.gameObject).AddComponent<UITexture>();
				uITexture.depth = mBaseDepth + 3;
				uITexture.transform.localPosition = new Vector3(-166f, -11f, 0f);
				uITexture.mainTexture = mIconRequest.icon;
				uITexture.SetDimensions(65, 65);
				int num = mSearchData.iconid;
				if (num > 9999)
				{
					num -= 10000;
				}
				if (num >= mGame.mCompanionData.Count)
				{
					num = 0;
				}
				string iconName = mGame.mCompanionData[num].iconName;
				UISprite uISprite = Util.CreateSprite("CharaIcon", uITexture.gameObject, image);
				Util.SetSpriteInfo(uISprite, iconName, mBaseDepth + 4, new Vector3(32f, 28f, 0f), Vector3.one, false);
				uISprite.SetDimensions(50, 50);
			}
			else
			{
				UISprite uISprite2 = Util.CreateSprite("Icon", idResultIconCheckData.boardImage.gameObject, image);
				Util.SetSpriteInfo(uISprite2, "icon_chara_facebook", mBaseDepth + 3, new Vector3(-166f, -11f, 0f), Vector3.one, false);
				uISprite2.SetDimensions(65, 65);
				int num2 = mSearchData.iconid;
				if (num2 > 9999)
				{
					num2 -= 10000;
				}
				if (num2 >= mGame.mCompanionData.Count)
				{
					num2 = 0;
				}
				string iconName2 = mGame.mCompanionData[num2].iconName;
				UISprite uISprite3 = Util.CreateSprite("CharaIcon", uISprite2.gameObject, image);
				Util.SetSpriteInfo(uISprite3, iconName2, mBaseDepth + 4, new Vector3(32f, 28f, 0f), Vector3.one, false);
				uISprite3.SetDimensions(50, 50);
			}
			mIdResultIconCheckData.RemoveAt(0);
			break;
		}
		case STATE.NETWORK_00:
			mState.Change(STATE.MAIN);
			break;
		case STATE.NETWORK_00_1:
			if (mApplySearchUserCheckDone)
			{
				mState.Change(STATE.NETWORK_00_2);
			}
			break;
		case STATE.NETWORK_00_2:
			ApplyFinished();
			mState.Change(STATE.MAIN);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		string titleDescKey = Localization.Get("ID_Search_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		switch (mItem)
		{
		case ID_RESULT.HIT:
			ID_HitDialog();
			break;
		case ID_RESULT.ALREADY:
			ID_AlreadyDialog(false);
			break;
		case ID_RESULT.ALREADY_APPLY:
			ID_AlreadyDialog(true);
			break;
		case ID_RESULT.NOTFOUND:
			ID_NotFoundDialog();
			break;
		case ID_RESULT.MYSELF:
			ID_MySelfDialog();
			break;
		default:
			ID_ErrorDialog();
			break;
		}
	}

	public void ID_HitDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = "icon_chara00";
		string text2 = "lv_1";
		int index = 0;
		int num = mSearchData.iconid;
		if (num >= 10000)
		{
			num -= 10000;
		}
		if (mGame.mCompanionData.Count > num)
		{
			index = num;
		}
		CompanionData companionData = mGame.mCompanionData[index];
		text = companionData.iconName;
		int iconlvl = mSearchData.iconlvl;
		text2 = "lv_" + iconlvl;
		int a_main;
		int a_sub;
		Def.SplitStageNo(mSearchData.lvl, out a_main, out a_sub);
		string text3 = string.Format(Localization.Get("Stage_Data"), a_main);
		string nickname = mSearchData.nickname;
		string text4 = string.Format(Localization.Get("ID_Search_Hit"));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text4, mBaseDepth + 4, new Vector3(0f, 95f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		UISprite uISprite = Util.CreateSprite("Instruction", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 22f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(424, 108);
		UISprite uISprite2;
		if (GameMain.IsFacebookIconEnable() && mSearchData.iconid > 9999)
		{
			string imageName = "icon_chara_facebook02";
			uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, imageName, mBaseDepth + 3, new Vector3(-155f, 3f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(80, 80);
			IdResultIconCheckData idResultIconCheckData = new IdResultIconCheckData();
			idResultIconCheckData.boardImage = uISprite;
			idResultIconCheckData.loadingIcon = uISprite2;
			idResultIconCheckData.uuid = mSearchData.uuid;
			mIdResultIconCheckData.Add(idResultIconCheckData);
			mIconRequest = mIconMng.GetIconRequest(mSearchData.uuid, mSearchData.fbid, true);
		}
		else
		{
			uISprite2 = Util.CreateSprite("CharaIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, text, mBaseDepth + 3, new Vector3(-155f, 3f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(80, 80);
			uISprite2 = Util.CreateSprite("CharaLv", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, text2, mBaseDepth + 4, new Vector3(-155f, -37f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(mSearchData.fbid))
			{
				string imageName2 = "icon_sns00_mini";
				uISprite2 = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite2, imageName2, mBaseDepth + 4, new Vector3(25f, 63f, 0f), Vector3.one, false);
			}
		}
		uISprite2 = Util.CreateSprite("Linetop", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "line00", mBaseDepth + 3, new Vector3(45f, 7f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(292, 6);
		uILabel = Util.CreateLabel("userNameDesk", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, nickname, mBaseDepth + 3, new Vector3(45f, 24f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(310, 26);
		int num2 = mSearchData.series;
		int stageNumber = 1;
		List<PlayStageParam> playStage = mSearchData.PlayStage;
		if (playStage != null && playStage.Count != 0)
		{
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == num2)
				{
					stageNumber = playStage[i].Level / 100;
					break;
				}
			}
		}
		else
		{
			num2 = 0;
			Def.SplitStageNo(mSearchData.lvl, out a_main, out a_sub);
			stageNumber = a_main;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(uISprite.gameObject, mBaseDepth, num2, stageNumber, -11f, -8f);
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_application", "LC_button_application", "LC_button_application", mBaseDepth + 3, new Vector3(0f, -123f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnFriendPushed", UIButtonMessage.Trigger.OnClick);
		text4 = string.Format(Localization.Get("Com_Notice"));
		uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text4, mBaseDepth + 1, new Vector3(0f, -60f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.54509807f, 0.38431373f, 0.18039216f, 1f);
		CreateCancelButton("OnCancelPushed");
	}

	public void ID_AlreadyDialog(bool isApply)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = "icon_chara00";
		string text2 = "lv_1";
		int index = 0;
		int num = mSearchData.iconid;
		if (num >= 10000)
		{
			num -= 10000;
		}
		if (mGame.mCompanionData.Count > num)
		{
			index = num;
		}
		CompanionData companionData = mGame.mCompanionData[index];
		text = companionData.iconName;
		int iconlvl = mSearchData.iconlvl;
		text2 = "lv_" + iconlvl;
		int a_main;
		int a_sub;
		Def.SplitStageNo(mSearchData.lvl, out a_main, out a_sub);
		string text3 = string.Format(Localization.Get("Stage_Data"), a_main);
		string nickname = mSearchData.nickname;
		string text4 = (isApply ? string.Format(Localization.Get("ID_Search_AlreadyApply")) : string.Format(Localization.Get("ID_Search_Already")));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text4, mBaseDepth + 4, new Vector3(0f, 90f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		UISprite uISprite = Util.CreateSprite("Instruction", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 4f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(424, 108);
		UISprite uISprite2;
		if (GameMain.IsFacebookIconEnable() && mSearchData.iconid > 9999)
		{
			string imageName = "icon_chara_facebook02";
			uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, imageName, mBaseDepth + 3, new Vector3(-155f, 3f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(80, 80);
			IdResultIconCheckData idResultIconCheckData = new IdResultIconCheckData();
			idResultIconCheckData.boardImage = uISprite;
			idResultIconCheckData.loadingIcon = uISprite2;
			idResultIconCheckData.uuid = mSearchData.uuid;
			mIdResultIconCheckData.Add(idResultIconCheckData);
			mIconRequest = mIconMng.GetIconRequest(mSearchData.uuid, mSearchData.fbid, true);
		}
		else
		{
			uISprite2 = Util.CreateSprite("CharaIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, text, mBaseDepth + 3, new Vector3(-155f, 3f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(80, 80);
			uISprite2 = Util.CreateSprite("CharaLv", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, text2, mBaseDepth + 4, new Vector3(-155f, -37f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(mSearchData.fbid))
			{
				string imageName2 = "icon_sns00_mini";
				uISprite2 = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite2, imageName2, mBaseDepth + 4, new Vector3(25f, 63f, 0f), Vector3.one, false);
			}
		}
		uISprite2 = Util.CreateSprite("Linetop", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "line00", mBaseDepth + 3, new Vector3(45f, 7f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(292, 6);
		uILabel = Util.CreateLabel("userNameDesk", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, nickname, mBaseDepth + 3, new Vector3(45f, 24f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(310, 26);
		int num2 = mSearchData.series;
		int stageNumber = 1;
		List<PlayStageParam> playStage = mSearchData.PlayStage;
		if (playStage != null && playStage.Count != 0)
		{
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == num2)
				{
					stageNumber = playStage[i].Level / 100;
					break;
				}
			}
		}
		else
		{
			num2 = 0;
			Def.SplitStageNo(mSearchData.lvl, out a_main, out a_sub);
			stageNumber = a_main;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(uISprite.gameObject, mBaseDepth, num2, stageNumber, -11f, -8f);
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 3, new Vector3(0f, -110f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void ID_NotFoundDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = string.Format(Localization.Get("ID_Search_NotFound"));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 4, new Vector3(0f, 30f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 3, new Vector3(0f, -97f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void ID_MySelfDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = string.Format(Localization.Get("ID_Search_Myself"));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 4, new Vector3(0f, 30f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 3, new Vector3(0f, -97f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void ID_ErrorDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = string.Format(Localization.Get("ID_Search_Error"));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 4, new Vector3(0f, 30f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 3, new Vector3(0f, -97f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(ID_RESULT Item, string FriendID, SearchUserData a_userData)
	{
		mItem = Item;
		mFriendID = FriendID;
		mSearchData = a_userData;
		switch (mItem)
		{
		case ID_RESULT.HIT:
		case ID_RESULT.ALREADY:
		case ID_RESULT.ALREADY_APPLY:
			if (mSearchData == null)
			{
				mItem = ID_RESULT.ERROR;
			}
			break;
		}
	}

	public void OnFriendPushed()
	{
		if (mGame.mPlayer.Friends.Count >= Constants.GAME_FRIEND_MAX)
		{
			mConfirmDialog = Util.CreateGameObject("LimitFriendDialog", base.gameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Approval_Friend_Title"), Localization.Get("Friend_Request_Result_Mine_Full"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
			mConfirmDialog.SetClosedCallback(OnFriendOKRequestClosed);
			mState.Reset(STATE.WAIT);
		}
		else
		{
			Network_GiveApply(mSearchData.uuid);
			mState.Change(STATE.NETWORK_00_1);
		}
	}

	private void ApplyFinished()
	{
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", base.gameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("ID_Search_Title"), Localization.Get("Friend_Request_Result_OK"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
		mConfirmDialog.SetClosedCallback(OnFriendOKRequestClosed);
	}

	private void ApplyFailed()
	{
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", base.gameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("ID_Search_Title"), Localization.Get("Friend_Request_Result_NG"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
		mConfirmDialog.SetClosedCallback(OnFriendNGRequestClosed);
	}

	public void OnFriendOKRequestClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mResultOK = true;
		if (mCallback != null)
		{
			mCallback(mResultOK);
		}
		Object.Destroy(base.gameObject);
	}

	public void OnFriendNGRequestClosed(ConfirmDialog.SELECT_ITEM i)
	{
		if (mCallback != null)
		{
			mCallback(mResultOK);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mResultOK);
		}
		Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void Network_GiveApply(int a_target)
	{
		mApplySearchUserCheckDone = false;
		mApplySearchUserSucceed = false;
		GiftItemData giftItemData = new GiftItemData();
		giftItemData.FromID = mGame.mPlayer.UUID;
		giftItemData.Message = mGame.mPlayer.NickName;
		giftItemData.ItemCategory = 16;
		giftItemData.ItemKind = 0;
		giftItemData.ItemID = 0;
		giftItemData.Quantity = 1;
		giftItemData.PlayStage = mGame.mPlayer.PlayStage;
		if (!Server.GiveApply(giftItemData, a_target))
		{
			mApplySearchUserCheckDone = true;
		}
	}

	private void Server_OnGiveApply(int a_result, int a_code)
	{
		if (a_result == 0)
		{
			MyFriend myFriend = new MyFriend();
			myFriend.Data.UUID = mSearchData.uuid;
			myFriend.Data.Name = mSearchData.nickname;
			myFriend.Data.Level = mSearchData.lvl;
			myFriend.Data.IconID = mSearchData.iconid;
			myFriend.Data.IconLevel = mSearchData.iconlvl;
			myFriend.Data.Series = mSearchData.series;
			if (!mGame.mPlayer.ApplyFriends.ContainsKey(myFriend.Data.UUID))
			{
				mGame.mPlayer.ApplyFriends.Add(myFriend.Data.UUID, myFriend);
			}
			mState.Reset(STATE.NETWORK_00_2, true);
		}
		else
		{
			mState.Reset(STATE.NETWORK_00_3, true);
		}
	}
}
