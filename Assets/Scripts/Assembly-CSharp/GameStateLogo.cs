using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateLogo : GameState
{
	public enum STATE
	{
		LOAD_WAIT = 0,
		PERMISSON_CHECK = 1,
		PERMISSON_WAIT = 2,
		START_LOGO = 3,
		UNLOAD_WAIT = 4,
		MAIN = 5,
		WAIT = 6,
		DLLISTHASH_CHECK = 7,
		DLLISTHASH_WAIT = 8
	}

	public enum CONNECT_STATE
	{
		INIT = 0,
		WAIT = 1,
		AUTH_CHECK = 2,
		AUTH_WAIT = 3,
		NICKNAME_CHECK = 4,
		NICKNAME_WAIT = 5,
		GAMEINFO_CHECK = 6,
		GAMEINFO_WAIT = 7,
		MARKETING_INIT = 8,
		DEVICE_TOKEN = 9,
		SEGMENTINFO_CHECK = 10,
		SEGMENTINFO_WAIT = 11,
		GETGAMEDATA_CHECK = 12,
		GETGAMEDATA_WAIT = 13,
		GETGEM_CHECK = 14,
		GETGEM_WAIT = 15,
		AUTOLOGIN_CHECK = 16,
		AUTOLOGIN_WAIT = 17,
		GET_ADV_GAMEDATA = 18,
		GET_ADV_GAMEDATA_WAIT = 19,
		CHECKMASTERTABLE = 20,
		CHECKMASTERTABLE_WAIT = 21,
		GETMASTERTABLE = 22,
		GETMASTERTABLE_WAIT = 23,
		GETALLITEM = 24,
		GETALLITEM_WAIT = 25,
		COMPLETE = 26
	}

	public class PermissionResultJSON
	{
		public bool result { get; set; }
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private StatusManager<CONNECT_STATE> mConnectState = new StatusManager<CONNECT_STATE>(CONNECT_STATE.INIT);

	private GameStateManager mGameState;

	private ConfirmDialog mConfirmDialog;

	private UISprite mBgSprite;

	private bool mPermissionCheckFinished;

	private bool mPermissionAllowed;

	private List<SNS> mAutoLoginList;

	private int mLoginWaitCnt;

	private bool mOptionSNSUpdate;

	private Camera mBeelineLogoCamera;

	private static int init_step;

	private static bool first_loading_anime = true;

	private static bool isShowAuthErrorDialog;

	private bool isBIJLogoFinished;

	private static float first_device_token;

	private static string device_token;

	public static bool isRestartAfterTransfer;

	private bool mIsInitGameStepFinished;

	private static float first_connect_establish = -1f;

	private float first_debugdialog_open_checktime = -1f;

	private bool debugdialog_open_flag;

	private float debugdialog_open_pushtime;

	private bool debugdialog_opening_flag;

	private bool debugdialog_open_finished;

	private AdInfo mAdInfo;

	private volatile bool mAdInfoAcquiring;

	private ConnectInstance mConnectInstance;

	private bool mIsOpenApplicationDetailsSettings;

	private bool mMaintenanceRetryFlg;

	private bool mIsPlayerReset;

	private Player mOldPlayer;

	private PlayerData mOldSaveData;

	private AdvSaveData mOldAdvSaveData;

	private string mOldAuthKey;

	private int mResetReason = -1;

	private bool mIsBootMaintenanceCheckSuccess;

	public override void Start()
	{
		base.Start();
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public override void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mState.Change(STATE.PERMISSON_CHECK);
			}
			break;
		case STATE.PERMISSON_CHECK:
		{
			int num = BIJUnity.checkPermission();
			if (num != 0)
			{
				List<string> list = new List<string>();
				if (((uint)num & (true ? 1u : 0u)) != 0)
				{
					list.Add("PermissionText_2");
				}
				if (((uint)num & 2u) != 0)
				{
					list.Add("PermissionText_3");
				}
				if (((uint)num & 4u) != 0)
				{
					list.Add("PermissionText_4");
				}
				PermissionADListDialog permissionADListDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<PermissionADListDialog>();
				permissionADListDialog.HEADER_PERMISSION_KEY = "PermissionText_1";
				permissionADListDialog.PermissionKeyArray = list.ToArray();
				permissionADListDialog.SetClosedCallback(delegate
				{
					BIJUnity.requestPermission(base.gameObject.name, "OnRequestPermissionResult");
					mState.Change(STATE.PERMISSON_WAIT);
				});
				mState.Change(STATE.WAIT);
			}
			else
			{
				mState.Change(STATE.START_LOGO);
			}
			break;
		}
		case STATE.PERMISSON_WAIT:
		{
			if (!mPermissionCheckFinished)
			{
				break;
			}
			int num2 = BIJUnity.checkPermission();
			string empty = string.Empty;
			string empty2 = string.Empty;
			if (num2 != 0)
			{
				List<string> list2 = new List<string>();
				if (((uint)num2 & (true ? 1u : 0u)) != 0)
				{
					list2.Add("PermissionText_2");
				}
				if (((uint)num2 & 2u) != 0)
				{
					list2.Add("PermissionText_3");
				}
				if (((uint)num2 & 4u) != 0)
				{
					list2.Add("PermissionText_4");
				}
				int num3 = BIJUnity.shouldShowRequestPermissionRationale();
				if (num3 != 0 && (num2 & num3) != 0)
				{
					PermissionADListDialog permissionADListDialog2 = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<PermissionADListDialog>();
					permissionADListDialog2.HEADER_PERMISSION_KEY = "PermissionText_1";
					permissionADListDialog2.PermissionKeyArray = list2.ToArray();
					permissionADListDialog2.SetClosedCallback(delegate
					{
						mIsOpenApplicationDetailsSettings = true;
						BIJUnity.openApplicationDetailsSettings();
					});
					mState.Reset(STATE.WAIT);
					break;
				}
				mPermissionCheckFinished = false;
				PermissionADListDialog permissionADListDialog3 = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<PermissionADListDialog>();
				permissionADListDialog3.HEADER_PERMISSION_KEY = "PermissionText_1";
				permissionADListDialog3.PermissionKeyArray = list2.ToArray();
				permissionADListDialog3.SetClosedCallback(delegate
				{
					BIJUnity.requestPermission(base.gameObject.name, "OnRequestPermissionResult");
					mState.Change(STATE.PERMISSON_WAIT);
				});
				mState.Reset(STATE.WAIT);
			}
			else
			{
				empty = Localization.Get("PermissionAllowedTitle");
				empty2 = Localization.Get("PermissionAllowedText");
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				confirmDialog.Init(empty, empty2, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, false);
				confirmDialog.SetButtonSeEnable(false);
				confirmDialog.SetClosedCallback(delegate
				{
					GameMain.ApplicationQuit();
				});
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.START_LOGO:
			StartCoroutine(Process());
			StartCoroutine(Render());
			mState.Change(STATE.MAIN);
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.MAIN:
			if (GameMain.IsLoaded)
			{
				if (!first_loading_anime)
				{
					mGame.FinishLoading();
				}
				if (!GameMain.IsLogoFinished)
				{
					break;
				}
				FinishBeelineLogo();
				if (GameMain.USE_DEBUG_STATE)
				{
					if (GameMain.USE_RESOURCEDL)
					{
						mGame.mDLManager.LoadLocalDLData();
					}
					mGameState.SetNextGameState(GameStateManager.GAME_STATE.DEBUG);
				}
				else if (mGame.mMaintenanceProfile.IsServiceFinished)
				{
					mGameState.SetNextGameState(GameStateManager.GAME_STATE.SERVICE_FINISH);
				}
				else if (GameMain.USE_RESOURCEDL)
				{
					bool flag = mGame.mDLManager.DLFilelistHashCheck();
					mGame.mDLManager.LoadLocalDLData(true);
					if (flag & mGame.mDLManager.CheckDLFileIntegrity(true, true))
					{
						mGame.mDLManager.LoadDownloadFiles();
						mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
					}
					else
					{
						mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
					}
				}
				else
				{
					mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
				}
				mState.Change(STATE.UNLOAD_WAIT);
			}
			else
			{
				if (!GameMain.IsLogoFinished)
				{
					break;
				}
				if (isShowAuthErrorDialog)
				{
					if (!first_loading_anime)
					{
						first_loading_anime = true;
						mGame.FinishLoading();
					}
				}
				else if (first_loading_anime)
				{
					first_loading_anime = false;
					mGame.StartLoading();
				}
			}
			break;
		}
		mState.Update();
	}

	private IEnumerator Process()
	{
		float _start2 = Time.realtimeSinceStartup;
		bool loaded4 = false;
		if (!isRestartAfterTransfer)
		{
			init_step = 0;
			GameMain.load_step_wait = false;
			do
			{
				loaded4 = InitGameStep(init_step);
				if (!GameMain.load_step_wait)
				{
					init_step++;
				}
				yield return null;
			}
			while (!loaded4);
		}
		else
		{
			first_device_token = 0f;
			isRestartAfterTransfer = false;
		}
		_start2 = Time.realtimeSinceStartup;
		if (!mGame.mMaintenanceProfile.IsServiceFinished)
		{
			loaded4 = false;
			init_step = 0;
			GameMain.load_step_wait = false;
			do
			{
				loaded4 = InitServerCommuStep();
				yield return null;
			}
			while (!loaded4);
		}
		yield return null;
		GameMain.IsLoaded = true;
	}

	private IEnumerator Render()
	{
		GameMain.IsLogoFinished = false;
		ResImage resImageLogo = ResourceManager.LoadImage("LOGO");
		mGame.mCamera.backgroundColor = Color.black;
		mBgSprite = Util.CreateSprite("Bg", mRoot, resImageLogo.Image);
		Util.SetSpriteInfo(mBgSprite, "back", 1, Vector3.zero, Vector3.one, false);
		mBgSprite.type = UIBasicSprite.Type.Tiled;
		SetLayout(Util.ScreenOrientation);
		UISprite attentionBase = Util.CreateSprite("Attention", mRoot, resImageLogo.Image);
		Util.SetSpriteInfo(attentionBase, "caution", 1, Vector3.zero, Vector3.one, false);
		UISprite sprite4 = Util.CreateSprite("Attention2", attentionBase.gameObject, resImageLogo.Image);
		Util.SetSpriteInfo(sprite4, "caution2", 1, new Vector3(0f, -230f, 0f), Vector3.one, false);
		UISprite tapArrow = Util.CreateSprite("Attention3", sprite4.gameObject, resImageLogo.Image);
		float position2 = 116f;
		position2 = 130f;
		Util.SetSpriteInfo(tapArrow, "caution3", 1, new Vector3(position2, 0f, 0f), Vector3.one, false);
		StartCoroutine("TapWaitArrow", tapArrow);
		StartCoroutine(GrayIn(0.5f));
		yield return new WaitForSeconds(0.5f);
		while (!mGame.mTouchUpTrg)
		{
			yield return 0;
		}
		StopCoroutine("TapWaitArrow");
		StartCoroutine(GrayOut(1f, Color.black, 0.5f));
		yield return new WaitForSeconds(0.5f);
		UnityEngine.Object.Destroy(mBgSprite.gameObject);
		mBgSprite = null;
		UnityEngine.Object.Destroy(attentionBase.gameObject);
		bool skip4 = false;
		StartCoroutine(GrayOut(1f, Color.black, 1f / 30f));
		yield return 0;
		mGame.mCamera.backgroundColor = Color.white;
		sprite4 = Util.CreateSprite("Logo", mRoot, resImageLogo.Image);
		Util.SetSpriteInfo(sprite4, "splash_BNE00", 1, Vector3.zero, Vector3.one, false);
		StartCoroutine(GrayIn(0.5f));
		yield return new WaitForSeconds(0.5f);
		float startTime3 = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup - startTime3 < 1f)
		{
			if (mGame.mTouchUpTrg)
			{
				skip4 = true;
				break;
			}
			yield return 0;
		}
		if (!skip4)
		{
			StartCoroutine(GrayOut(1f, Color.black, 0.5f));
			yield return new WaitForSeconds(0.5f);
		}
		UnityEngine.Object.Destroy(sprite4.gameObject);
		skip4 = false;
		StartCoroutine(GrayOut(1f, Color.black, 1f / 30f));
		yield return 0;
		mGame.mCamera.backgroundColor = new Color(81f / 85f, 23f / 51f, 11f / 85f, 1f);
		sprite4 = Util.CreateSprite("Logo", mRoot, resImageLogo.Image);
		Util.SetSpriteInfo(sprite4, "splash", 1, Vector3.zero, Vector3.one, false);
		StartCoroutine(GrayIn(0.5f));
		yield return new WaitForSeconds(0.5f);
		startTime3 = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup - startTime3 < 1f)
		{
			if (mGame.mTouchUpTrg)
			{
				skip4 = true;
				break;
			}
			yield return 0;
		}
		if (!skip4)
		{
			StartCoroutine(GrayOut(1f, Color.black, 0.5f));
			yield return new WaitForSeconds(0.5f);
		}
		UnityEngine.Object.Destroy(sprite4.gameObject);
		isBIJLogoFinished = true;
		mGame.mCamera.backgroundColor = Color.black;
		while (!GameMain.mMaintenanceProfileCheckDone || !GameMain.mMaintenanceProfileSucceed)
		{
			yield return 0;
		}
		if (!mGame.mMaintenanceProfile.IsServiceFinished)
		{
			mBgSprite = Util.CreateSprite("Bg", mRoot, resImageLogo.Image);
			Util.SetSpriteInfo(mBgSprite, "back", 1, Vector3.zero, Vector3.one, false);
			mBgSprite.type = UIBasicSprite.Type.Tiled;
			SetLayout(Util.ScreenOrientation);
			sprite4 = Util.CreateSprite("AnniversaryLogo", mRoot, resImageLogo.Image);
			Util.SetSpriteInfo(sprite4, "20anniversarylogo", 1, Vector3.zero, Vector3.one, false);
			if (!skip4)
			{
				StartCoroutine(GrayIn(0.5f));
				yield return new WaitForSeconds(0.5f);
			}
			skip4 = false;
			startTime3 = Time.realtimeSinceStartup;
			while (Time.realtimeSinceStartup - startTime3 < 1f)
			{
				if (mGame.mTouchUpTrg)
				{
					skip4 = true;
					break;
				}
				yield return 0;
			}
			StartCoroutine(GrayOut(1f, Color.black, 0.5f));
			yield return new WaitForSeconds(0.5f);
		}
		GameMain.IsLogoFinished = true;
		yield return null;
	}

	private IEnumerator TapWaitArrow(UISprite sprite)
	{
		float angle = 0f;
		while (true)
		{
			angle += Time.deltaTime * 360f;
			sprite.transform.localScale = new Vector3(Mathf.Cos((float)Math.PI / 180f * angle), 1f, 1f);
			yield return 0;
		}
	}

	public override IEnumerator LoadGameState()
	{
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		yield return 0;
		LoadGameStateFinished();
		yield return 0;
	}

	public override IEnumerator UnloadGameState()
	{
		ResourceManager.UnloadSsAnimationAll();
		yield return 0;
		ResourceManager.UnloadImageAll();
		mGame.UnloadNotPreloadSoundResources(true);
		yield return 0;
		UnloadGameStateFinished();
		yield return 0;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		if (mBgSprite != null)
		{
			Vector2 vector = Util.LogScreenSize();
			float num = vector.y / 1022f;
			mBgSprite.transform.localScale = new Vector3(num, num, 1f);
			mBgSprite.SetDimensions((int)(vector.x / num), (int)(vector.y / num));
		}
	}

	public void OnBackKeyPress()
	{
	}

	public void OnQuitDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			Application.Quit();
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	private void OnApplicationPause(bool pauseState)
	{
		if (!pauseState && mIsOpenApplicationDetailsSettings)
		{
			mIsOpenApplicationDetailsSettings = false;
			mPermissionCheckFinished = true;
			mState.Change(STATE.PERMISSON_WAIT);
		}
	}

	private bool MaintenanceErrorInLogoDisplay(ConnectBase a_target)
	{
		bool result = false;
		mMaintenanceRetryFlg = false;
		if (GameMain.MaintenanceMode)
		{
			a_target.SetResultWaitCallback(MaintenanceModeDialogCheck);
			if (!GameMain.IsLogoFinished)
			{
				StartCoroutine(MaintenanceErrorLogoDisplayWait());
			}
			else
			{
				mGame.CreateMaintenanceDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY, OnMaintenanceDialogClosedRetry);
			}
			result = true;
		}
		return result;
	}

	private IEnumerator MaintenanceErrorLogoDisplayWait()
	{
		while (!GameMain.IsLogoFinished)
		{
			yield return null;
		}
		mGame.CreateMaintenanceDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY, OnMaintenanceDialogClosedRetry);
	}

	private void OnMaintenanceDialogClosedRetry(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		mMaintenanceRetryFlg = true;
	}

	private void MaintenanceModeDialogCheck(ConnectBase a_target)
	{
		if (mMaintenanceRetryFlg)
		{
			if (mGame.mPlayer != null)
			{
				mGame.mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			}
			a_target.StartConnect();
		}
	}

	private void ResetPlayer(InheritingConfirmDialog.REASON aReason)
	{
		UserDataInfo instance = UserDataInfo.Instance;
		short mAdvLoaclMTVersion = mGame.mPlayer.AdvSaveData.mAdvLoaclMTVersion;
		if (instance.mDataState == UserDataInfo.DATA_STATE.LOADED)
		{
			mOldSaveData = mGame.mPlayer.Data;
			ServerCramVariableData.mLocalUUID = mOldSaveData.UUID;
			ServerCramVariableData.mLocalHiveId = mOldSaveData.HiveId;
			ServerCramVariableData.mLocalSearchID = mOldSaveData.SearchID;
		}
		if (instance.mAuthKeyState == UserDataInfo.DATA_STATE.LOADED)
		{
			mOldAuthKey = instance.AuthKey;
		}
		mOldPlayer = mGame.mPlayer;
		mGame.mPlayer = Player.CreateForNewPlayer();
		mGame.mOptions = Options.CreateForNewPlayer();
		mGame.mPlayer.AdvSaveData.mAdvLoaclMTVersion = mAdvLoaclMTVersion;
		instance.ReCreateAuthKey(false);
		mResetReason = -1;
		switch (aReason)
		{
		case InheritingConfirmDialog.REASON.LOAD_ERROR:
			mResetReason = 0;
			if (instance.mDataState != UserDataInfo.DATA_STATE.LOADED)
			{
				mResetReason++;
			}
			if (instance.mAuthKeyState != UserDataInfo.DATA_STATE.LOADED)
			{
				mResetReason += 4;
			}
			break;
		case InheritingConfirmDialog.REASON.AUTHKEY_ERROR:
			mResetReason = 101;
			break;
		}
		mGame.mConcierge.ResetGameConcierge();
		mIsPlayerReset = true;
	}

	private void SaveResetPlayer()
	{
		UserDataInfo instance = UserDataInfo.Instance;
		ServerCram.SendDataTransferDialog(mResetReason, mOldPlayer, mOldSaveData, mOldAdvSaveData, mOldAuthKey);
		instance.mResetReason = mResetReason;
		instance.Save(true);
		mGame.Save();
		mGame.SaveAdvData();
		mGame.SaveOptions();
		mIsPlayerReset = false;
		mResetReason = -1;
		mOldPlayer = null;
		mOldSaveData = null;
		mOldAdvSaveData = null;
		mOldAuthKey = null;
	}

	private bool InitGameStep(int step)
	{
		bool result = false;
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		switch (step)
		{
		case 0:
			Constants.Load();
			break;
		case 2:
			mGame.LoadOptions();
			break;
		case 3:
			mGame.LoadBoosterData("Data/Booster.bin");
			mGame.LoadTileData("Data/Tile.bin");
			mGame.LoadShopData("Data/Shop.bin");
			mGame.LoadEventData("Data/Event/EventData.bin");
			mGame.LoadTrophyExchangeData("Data/TrophyExchange.bin");
			mGame.LoadSpecialDailyData("Data/Event/SpecialDailyData.bin");
			mGame.LoadAlternateRewardData("Data/AlternateRewardData.bin");
			mGame.LoadDailyChallenge("ScriptableObject/DailyChallenge/DailyChallenge");
			MissionManager.LoadMissionData();
			mGame.SwitchTileData(false);
			break;
		case 4:
			mGame.InitNetwork();
			mGame.InitPurchase();
			break;
		case 5:
		{
			for (int j = 0; j < Res.LOAD_PATICLES.Length; j++)
			{
				GameMain.LoadParticlePrefab(Res.LOAD_PATICLES[j]);
			}
			break;
		}
		case 6:
		{
			mGame.LoadPlayerData();
			mGame.LoadMetadata();
			UserBehavior.Load();
			if (mGame.mPlayer.Data.AppVersion < 1230)
			{
				mGame.mConcierge.DeserializeFromPlayerPrefs();
			}
			UserDataInfo instance = UserDataInfo.Instance;
			instance.Load();
			if (instance.IsInstalled())
			{
				instance.mShowInheriting = true;
				instance.Save();
			}
			else if (instance.IsDataDameged())
			{
				ResetPlayer(InheritingConfirmDialog.REASON.LOAD_ERROR);
			}
			break;
		}
		case 7:
			if (!GameMain.OPEN_DEBUGDIALOG_BOOT)
			{
				break;
			}
			if (!debugdialog_opening_flag)
			{
				if (first_debugdialog_open_checktime < 0f)
				{
					first_debugdialog_open_checktime = Time.realtimeSinceStartup;
					GameMain.load_step_wait = true;
					debugdialog_open_flag = false;
					debugdialog_open_pushtime = 0f;
				}
				if (mGame.mTouchCnd)
				{
					debugdialog_open_pushtime += Time.deltaTime;
				}
				if (debugdialog_open_pushtime > 3f)
				{
					debugdialog_open_flag = true;
				}
				float num = Time.realtimeSinceStartup - first_debugdialog_open_checktime;
				if (!(num > 5f) && !debugdialog_open_flag)
				{
					break;
				}
				if (debugdialog_open_flag)
				{
					debugdialog_opening_flag = true;
					debugdialog_open_finished = false;
					debugdialog_open_flag = false;
					SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
					sMDDebugDialog.SetGameState(this);
					sMDDebugDialog.SetInitializePage(SMDDebugDialog.PAGE.BOOK);
					sMDDebugDialog.SetClosedCallback(delegate
					{
						debugdialog_open_finished = true;
					});
				}
				else
				{
					GameMain.load_step_wait = false;
				}
			}
			else if (debugdialog_open_finished)
			{
				GameMain.load_step_wait = false;
			}
			break;
		case 9:
		{
			string a_debugkey = string.Empty;
			if (GameMain.DEBUG_DEBUGSERVERINFO)
			{
				a_debugkey = "debug";
				GameMain.GET_MAINTENANCE_INFO_FREQ = 240L;
			}
			mGame.Network_GetMaintenanceProfile(string.Empty, a_debugkey);
			GameMain.load_step_wait = false;
			break;
		}
		case 10:
			GameMain.load_step_wait = true;
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (mGame.mMaintenanceProfile.IsServiceFinished)
				{
					GameMain.load_step_wait = false;
				}
				else if (GameMain.MaintenanceMode)
				{
					if (GameMain.IsLogoFinished)
					{
						isShowAuthErrorDialog = true;
						mGame.CreateMaintenanceDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY, OnMaintenanceDialogClosed);
						init_step = 11;
					}
				}
				else
				{
					GameMain.load_step_wait = false;
				}
			}
			else if (isBIJLogoFinished)
			{
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenanceCheckErrorDialogClosed, 9);
				init_step = 11;
			}
			break;
		case 12:
		{
			PurchaseManager.LoadPurchaseData();
			PurchaseManager.LoadCampaignLogData();
			mGame.LoadServerCramVariableData();
			int playspan;
			if (!mGame.CountPlayerBoot(out playspan))
			{
			}
			break;
		}
		case 13:
			GameMain.load_step_wait = !PurchaseManager.instance.IsInitialized;
			break;
		case 14:
			mAdInfoAcquiring = true;
			mAdInfo = new AdInfo();
			if (!BIJUnity.getAdvertisingInfoAsync(base.gameObject))
			{
				mAdInfoAcquiring = false;
			}
			break;
		case 15:
			if (mAdInfoAcquiring)
			{
				GameMain.load_step_wait = true;
				break;
			}
			Network.AdInfo = mAdInfo;
			GameMain.load_step_wait = false;
			break;
		case 16:
			mGame.mPlayer.Data.StartupCount++;
			break;
		default:
			if (step > 16)
			{
				mIsInitGameStepFinished = true;
				result = true;
			}
			break;
		case 1:
		case 8:
		case 11:
			break;
		}
		float num2 = Time.realtimeSinceStartup - realtimeSinceStartup;
		return result;
	}

	private void OnConnectErrorRetry(int a_state)
	{
		isShowAuthErrorDialog = false;
		mConnectState.Change((CONNECT_STATE)a_state);
	}

	private void OnConnectErrorAbandon()
	{
		isShowAuthErrorDialog = false;
		mConnectState.Change(CONNECT_STATE.GAMEINFO_CHECK);
	}

	private void OnConnectErrorAbandonToComplete()
	{
		isShowAuthErrorDialog = false;
		mConnectState.Change(CONNECT_STATE.COMPLETE);
	}

	private void MaintenanceConnectFinishedCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		mIsBootMaintenanceCheckSuccess = true;
		mConnectInstance = null;
	}

	private void AuthConnectCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (!a_error)
		{
			if (mIsPlayerReset)
			{
				SaveResetPlayer();
			}
			mGame.RetryCount = 0;
			if (mGame.mPlayer.UUID != 0 && !GameMain.MaintenanceMode)
			{
				mConnectState.Change(CONNECT_STATE.NICKNAME_CHECK);
			}
			else
			{
				mConnectState.Change(CONNECT_STATE.COMPLETE);
			}
		}
		else
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mConnectState.Change(CONNECT_STATE.NICKNAME_CHECK);
			}
			else
			{
				BIJLog.E("Connection Error : First Connect");
			}
		}
		mConnectInstance = null;
	}

	private void FirstConnectCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (!a_error)
		{
			mGame.RetryCount = 0;
			if (mGame.mPlayer.UUID != 0 && !GameMain.MaintenanceMode)
			{
				mConnectState.Change(CONNECT_STATE.MARKETING_INIT);
			}
			else
			{
				mConnectState.Change(CONNECT_STATE.COMPLETE);
			}
		}
		else
		{
			BIJLog.E("Connection Error : First Connect");
		}
		mConnectInstance = null;
	}

	private void SecondConnectCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (!a_error)
		{
			mGame.RetryCount = 0;
			mConnectState.Change(CONNECT_STATE.AUTOLOGIN_CHECK);
			ServerCram.SendStart();
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_7000);
				mGame.Save();
			}
		}
		else
		{
			BIJLog.E("Connection Error : Second Connect");
		}
		mConnectInstance = null;
	}

	private void AdvDataConnectCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (!a_error)
		{
			mGame.RetryCount = 0;
		}
		mConnectState.Change(CONNECT_STATE.GETMASTERTABLE);
		mConnectInstance = null;
	}

	private void AdvMasterTableConnectCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (!a_error)
		{
			mGame.RetryCount = 0;
			mGame.SaveMTData();
			mConnectState.Change(CONNECT_STATE.GETALLITEM);
		}
		mConnectInstance = null;
	}

	private void AdvGetAllItemConnectCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (!a_error)
		{
			mGame.RetryCount = 0;
			mGame.mPlayer.AdvSaveData.UpdateMyCharaDict();
			mConnectState.Change(CONNECT_STATE.COMPLETE);
		}
		mConnectInstance = null;
	}

	private bool InitServerCommuStep()
	{
		bool result = false;
		if (!mIsInitGameStepFinished)
		{
			return false;
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		bool flag = true;
		switch (mConnectState.GetStatus())
		{
		case CONNECT_STATE.INIT:
			mConnectState.Change(CONNECT_STATE.AUTH_CHECK);
			break;
		case CONNECT_STATE.WAIT:
			flag = mConnectState.IsChanged();
			break;
		case CONNECT_STATE.AUTH_CHECK:
			mGame.Network_UserAuth();
			mConnectState.Change(CONNECT_STATE.AUTH_WAIT);
			break;
		case CONNECT_STATE.AUTH_WAIT:
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (mIsPlayerReset)
				{
					SaveResetPlayer();
				}
				mGame.RetryCount = 0;
				mConnectState.Change(CONNECT_STATE.NICKNAME_CHECK);
			}
			else if (GameMain.mUserAuthErrorCode == Server.ErrorCode.AUTHKEY_REGISTERED)
			{
				UserDataInfo.Instance.ReCreateAuthKey();
				mConnectState.Change(CONNECT_STATE.AUTH_CHECK);
			}
			else if (GameMain.mUserAuthErrorCode == Server.ErrorCode.AUTHKEY_DIFFERENT)
			{
				ResetPlayer(InheritingConfirmDialog.REASON.AUTHKEY_ERROR);
				mConnectState.Change(CONNECT_STATE.AUTH_CHECK);
			}
			else if (GameMain.IsLogoFinished)
			{
				isShowAuthErrorDialog = true;
				Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mUserAuthErrorCode;
				if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 2, OnConnectErrorRetry, OnConnectErrorAbandon);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 2, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
			}
			break;
		case CONNECT_STATE.NICKNAME_CHECK:
			mGame.Network_NicknameCheck();
			mConnectState.Change(CONNECT_STATE.NICKNAME_WAIT);
			break;
		case CONNECT_STATE.NICKNAME_WAIT:
			if (GameMain.mNickNameCheckDone)
			{
				if (GameMain.mNickNameSucceed)
				{
					mGame.RetryCount = 0;
					mConnectState.Change(CONNECT_STATE.GAMEINFO_CHECK);
				}
				else if (GameMain.IsLogoFinished)
				{
					isShowAuthErrorDialog = true;
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 4, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
			}
			break;
		case CONNECT_STATE.GAMEINFO_CHECK:
		{
			string a_debugkey = string.Empty;
			if (GameMain.DEBUG_DEBUGSERVERINFO)
			{
				a_debugkey = "debug";
				GameMain.GET_GAME_INFO_FREQ = 240L;
			}
			mGame.Network_GetGameProfile(string.Empty, a_debugkey);
			mConnectState.Change(CONNECT_STATE.GAMEINFO_WAIT);
			break;
		}
		case CONNECT_STATE.GAMEINFO_WAIT:
			if (!GameMain.mGameProfileCheckDone)
			{
				break;
			}
			if (GameMain.mGameProfileSucceed)
			{
				if (mGame.mPlayer.UUID != 0 && !mGame.mMaintenanceProfile.IsMaintenanceMode)
				{
					mConnectState.Change(CONNECT_STATE.MARKETING_INIT);
				}
				else
				{
					mConnectState.Change(CONNECT_STATE.COMPLETE);
				}
			}
			else
			{
				mGame.FinishLoading();
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 6, OnConnectErrorRetry, OnConnectErrorAbandon);
				mConnectState.Change(CONNECT_STATE.WAIT);
			}
			break;
		case CONNECT_STATE.MARKETING_INIT:
			mAutoLoginList = mGame.InitSocialPlugin();
			mLoginWaitCnt = 0;
			mOptionSNSUpdate = false;
			mGame.InitMarketing();
			mConnectState.Change(CONNECT_STATE.DEVICE_TOKEN);
			break;
		case CONNECT_STATE.DEVICE_TOKEN:
		{
			if (first_device_token == 0f)
			{
				first_device_token = Time.realtimeSinceStartup;
			}
			device_token = BIJUnity.getRemoteNotificationToken();
			float num = Time.realtimeSinceStartup - first_device_token;
			if (!string.IsNullOrEmpty(device_token) || !(num < 1f))
			{
				if (!string.IsNullOrEmpty(device_token))
				{
					Server.SendDeviceToken(device_token);
				}
				mConnectState.Change(CONNECT_STATE.SEGMENTINFO_CHECK);
			}
			break;
		}
		case CONNECT_STATE.SEGMENTINFO_CHECK:
		{
			string a_debugkey2 = string.Empty;
			if (GameMain.DEBUG_DEBUGSERVERINFO)
			{
				a_debugkey2 = "debug";
				GameMain.GET_SEGMENT_INFO_FREQ = 240L;
			}
			mGame.Network_GetSegmentProfile(string.Empty, a_debugkey2);
			mConnectState.Change(CONNECT_STATE.SEGMENTINFO_WAIT);
			break;
		}
		case CONNECT_STATE.SEGMENTINFO_WAIT:
		{
			if (!GameMain.mSegmentProfileCheckDone)
			{
				break;
			}
			if (GameMain.mSegmentProfileSucceed)
			{
				mConnectState.Change(CONNECT_STATE.GETGAMEDATA_CHECK);
				break;
			}
			Server.ErrorCode mSegmentProfileErrorCode = GameMain.mSegmentProfileErrorCode;
			Server.ErrorCode mAdvGetAllItemErrorCode = mSegmentProfileErrorCode;
			if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 10, OnConnectErrorRetry, OnConnectErrorAbandonToComplete);
				mGame.FinishLoading();
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 10, OnConnectErrorRetry, OnConnectErrorAbandon);
				mGame.FinishLoading();
			}
			mConnectState.Change(CONNECT_STATE.WAIT);
			break;
		}
		case CONNECT_STATE.GETGAMEDATA_CHECK:
			mGame.Network_GetGameData();
			mConnectState.Change(CONNECT_STATE.GETGAMEDATA_WAIT);
			break;
		case CONNECT_STATE.GETGAMEDATA_WAIT:
			if (GameMain.mGetGameDataCheckDone)
			{
				if (GameMain.mGetGameDataSucceed)
				{
					mGame.RetryCount = 0;
					mConnectState.Change(CONNECT_STATE.GETGEM_CHECK);
				}
				else if (GameMain.IsLogoFinished)
				{
					isShowAuthErrorDialog = true;
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 12, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
			}
			break;
		case CONNECT_STATE.GETGEM_CHECK:
			mGame.Network_GetGemCount();
			mConnectState.Change(CONNECT_STATE.GETGEM_WAIT);
			break;
		case CONNECT_STATE.GETGEM_WAIT:
			flag = mConnectState.IsChanged();
			if (!GameMain.mGetGemCountCheckDone)
			{
				break;
			}
			if (GameMain.mGetGemCountSucceed)
			{
				mConnectState.Change(CONNECT_STATE.AUTOLOGIN_CHECK);
				ServerCram.SendStart();
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
				{
					mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_7000);
					mGame.Save();
				}
			}
			else if (GameMain.IsLogoFinished)
			{
				isShowAuthErrorDialog = true;
				Server.ErrorCode mGetGemCountErrorCode = GameMain.mGetGemCountErrorCode;
				Server.ErrorCode mAdvGetAllItemErrorCode = mGetGemCountErrorCode;
				if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 14, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 14, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
			}
			break;
		case CONNECT_STATE.AUTOLOGIN_CHECK:
			if (mAutoLoginList.Count > 0)
			{
				SNS kind = mAutoLoginList[0];
				BIJSNS _sns = SocialManager.Instance[kind];
				string id = null;
				string text = null;
				GameMain.TryGetSNSUserInfo(_sns, out id, out text);
				if (string.IsNullOrEmpty(id))
				{
					if (_sns.IsLogined())
					{
						mConnectState.Change(CONNECT_STATE.AUTOLOGIN_WAIT);
						_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
						{
							try
							{
								if (res != null && res.result == 0)
								{
									IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
									if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
									{
										mOptionSNSUpdate = true;
									}
								}
							}
							catch (Exception)
							{
							}
							mLoginWaitCnt = 0;
							mAutoLoginList.RemoveAt(0);
							mConnectState.Change(CONNECT_STATE.AUTOLOGIN_CHECK);
						}, null);
					}
					else
					{
						mLoginWaitCnt++;
						if (mLoginWaitCnt >= 10)
						{
							mLoginWaitCnt = 0;
							mAutoLoginList.RemoveAt(0);
						}
					}
				}
				else
				{
					mAutoLoginList.RemoveAt(0);
				}
			}
			else
			{
				if (mOptionSNSUpdate)
				{
					mGame.SaveOptions();
				}
				mConnectState.Change(CONNECT_STATE.COMPLETE);
			}
			break;
		case CONNECT_STATE.GET_ADV_GAMEDATA:
			mGame.Network_AdvGetSaveData();
			mConnectState.Change(CONNECT_STATE.GET_ADV_GAMEDATA_WAIT);
			break;
		case CONNECT_STATE.GET_ADV_GAMEDATA_WAIT:
			if (!GameMain.mAdvGetSaveDataCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetSaveDataSucceed)
			{
				mConnectState.Change(CONNECT_STATE.CHECKMASTERTABLE);
			}
			else if (GameMain.IsLogoFinished)
			{
				Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mAdvGetSaveDataErrorCode;
				if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 18, OnConnectErrorRetry, null);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 18, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
				mConnectState.Change(CONNECT_STATE.WAIT);
			}
			break;
		case CONNECT_STATE.CHECKMASTERTABLE:
			mGame.Network_AdvCheckMasterTableVer();
			mConnectState.Change(CONNECT_STATE.CHECKMASTERTABLE_WAIT);
			break;
		case CONNECT_STATE.CHECKMASTERTABLE_WAIT:
			if (!GameMain.mAdvCheckMasterTableVerCheckDone)
			{
				break;
			}
			if (GameMain.mAdvCheckMasterTableVerSucceed)
			{
				mConnectState.Change(CONNECT_STATE.GETMASTERTABLE);
			}
			else if (GameMain.IsLogoFinished)
			{
				isShowAuthErrorDialog = true;
				Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mAdvCheckMasterTableVerErrorCode;
				if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 20, OnConnectErrorRetry, null);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 20, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
				mConnectState.Change(CONNECT_STATE.WAIT);
			}
			break;
		case CONNECT_STATE.GETMASTERTABLE:
			mConnectState.Change(CONNECT_STATE.GETALLITEM);
			break;
		case CONNECT_STATE.GETMASTERTABLE_WAIT:
			if (!GameMain.mAdvGetMasterTableDataCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetMasterTableDataSucceed)
			{
				mGame.SaveMTData();
				mConnectState.Change(CONNECT_STATE.GETALLITEM);
			}
			else if (GameMain.IsLogoFinished)
			{
				isShowAuthErrorDialog = true;
				Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mAdvGetMasterTableDataErrorCode;
				if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 22, OnConnectErrorRetry, null);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 22, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
				mConnectState.Change(CONNECT_STATE.WAIT);
			}
			break;
		case CONNECT_STATE.GETALLITEM:
			mGame.Network_AdvGetAllItem();
			mConnectState.Change(CONNECT_STATE.GETALLITEM_WAIT);
			break;
		case CONNECT_STATE.GETALLITEM_WAIT:
			if (!GameMain.mAdvGetAllItemCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetAllItemSucceed)
			{
				mGame.mPlayer.AdvSaveData.UpdateMyCharaDict();
				mConnectState.Change(CONNECT_STATE.COMPLETE);
			}
			else if (GameMain.IsLogoFinished)
			{
				isShowAuthErrorDialog = true;
				Server.ErrorCode mAdvGetAllItemErrorCode = GameMain.mAdvGetAllItemErrorCode;
				if (mAdvGetAllItemErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 24, OnConnectErrorRetry, null);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 24, OnConnectErrorRetry, null);
					mConnectState.Change(CONNECT_STATE.WAIT);
				}
				mConnectState.Change(CONNECT_STATE.WAIT);
			}
			break;
		case CONNECT_STATE.COMPLETE:
			result = true;
			break;
		default:
			result = true;
			break;
		case CONNECT_STATE.AUTOLOGIN_WAIT:
			break;
		}
		float num2 = Time.realtimeSinceStartup - realtimeSinceStartup;
		if (flag)
		{
		}
		mConnectState.Update();
		return result;
	}

	private void StartBeelineLogo()
	{
	}

	private void FinishBeelineLogo()
	{
	}

	public void OnAcquireAdvertisingInfo(string result)
	{
		if (!string.IsNullOrEmpty(result))
		{
			try
			{
				mAdInfo = new MiniJSONSerializer().Deserialize<AdInfo>(result);
			}
			catch (Exception)
			{
				mAdInfo = new AdInfo();
			}
		}
		mAdInfoAcquiring = false;
	}

	public void OnRequestPermissionResult(string result)
	{
		mPermissionCheckFinished = true;
		PermissionResultJSON permissionResultJSON = new MiniJSONSerializer().Deserialize<PermissionResultJSON>(result);
		mPermissionAllowed = permissionResultJSON.result;
	}

	private void OnMaintenanceDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		isShowAuthErrorDialog = false;
		init_step = 9;
	}

	private void OnMaintenanceCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		isShowAuthErrorDialog = false;
		init_step = 9;
	}
}
