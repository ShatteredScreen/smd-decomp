using System;

public class SpecialDailyEventSettings : ICloneable, IComparable
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("id")]
	public int SpecialEventID { get; set; }

	[MiniJSONAlias("howto_url")]
	public string HowToUrl { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object obj)
	{
		return CompareTo(obj as SpecialDailyEventSettings);
	}

	public SpecialDailyEventSettings Clone()
	{
		return MemberwiseClone() as SpecialDailyEventSettings;
	}

	public int CompareTo(SpecialDailyEventSettings obj)
	{
		if (obj == null)
		{
			return int.MaxValue;
		}
		return (int)(StartTime - obj.StartTime);
	}
}
