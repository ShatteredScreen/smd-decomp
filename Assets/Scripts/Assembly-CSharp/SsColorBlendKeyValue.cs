using System;
using UnityEngine;

[Serializable]
public class SsColorBlendKeyValue : SsInterpolatable, SsAttrValueInterface
{
	public SsColorBlendTarget Target;

	public SsColorBlendOperation Operation;

	public SsColorRef[] Colors;

	public SsColorBlendKeyValue(int colorsNum)
	{
		Target = SsColorBlendTarget.None;
		Operation = SsColorBlendOperation.Mix;
		if (colorsNum > 0)
		{
			Colors = SsColorRef.CreateArray(colorsNum);
		}
	}

	public SsColorBlendKeyValue(SsColorBlendKeyValue r)
	{
		Target = r.Target;
		Operation = r.Operation;
		Colors = (SsColorRef[])r.Colors.Clone();
	}

	public SsAttrValueInterface Clone()
	{
		return new SsColorBlendKeyValue(this);
	}

	public SsInterpolatable GetInterpolated(SsCurveParams curve, float time, SsInterpolatable start, SsInterpolatable end_, int startTime, int endTime)
	{
		SsColorBlendKeyValue ssColorBlendKeyValue = (SsColorBlendKeyValue)end_;
		SsColorBlendKeyValue ssColorBlendKeyValue2 = new SsColorBlendKeyValue(ssColorBlendKeyValue.Colors.Length);
		return ssColorBlendKeyValue2.Interpolate(curve, time, start, ssColorBlendKeyValue, startTime, endTime);
	}

	public SsInterpolatable Interpolate(SsCurveParams curve, float time, SsInterpolatable start_, SsInterpolatable end_, int startTime, int endTime)
	{
		SsColorBlendKeyValue ssColorBlendKeyValue = (SsColorBlendKeyValue)start_;
		SsColorBlendKeyValue ssColorBlendKeyValue2 = (SsColorBlendKeyValue)end_;
		if (ssColorBlendKeyValue.Target == SsColorBlendTarget.None && ssColorBlendKeyValue2.Target == SsColorBlendTarget.None)
		{
			UnityEngine.Debug.LogError("Must not come here.");
		}
		else
		{
			for (int i = 0; i < Colors.Length; i++)
			{
				Colors[i].Interpolate(curve, time, ssColorBlendKeyValue.Colors[i], ssColorBlendKeyValue2.Colors[i], startTime, endTime);
			}
			bool flag = time < 1f;
			if (ssColorBlendKeyValue.Target == SsColorBlendTarget.None)
			{
				for (int j = 0; j < Colors.Length; j++)
				{
					Colors[j].R = ssColorBlendKeyValue2.Colors[j].R;
					Colors[j].G = ssColorBlendKeyValue2.Colors[j].G;
					Colors[j].B = ssColorBlendKeyValue2.Colors[j].B;
				}
				flag = false;
			}
			else if (ssColorBlendKeyValue2.Target == SsColorBlendTarget.None)
			{
				for (int k = 0; k < Colors.Length; k++)
				{
					Colors[k].R = ssColorBlendKeyValue.Colors[k].R;
					Colors[k].G = ssColorBlendKeyValue.Colors[k].G;
					Colors[k].B = ssColorBlendKeyValue.Colors[k].B;
				}
			}
			if (flag)
			{
				Target = ssColorBlendKeyValue.Target;
				Operation = ssColorBlendKeyValue.Operation;
			}
			else
			{
				Target = ssColorBlendKeyValue2.Target;
				Operation = ssColorBlendKeyValue2.Operation;
			}
		}
		return this;
	}

	public override string ToString()
	{
		string text = string.Concat("Target: ", Target, ", Operation: ", Operation);
		if (Colors != null)
		{
			int num = 0;
			SsColorRef[] colors = Colors;
			foreach (SsColorRef ssColorRef in colors)
			{
				string text2 = text;
				text = text2 + ", Color[" + num + "]: " + ssColorRef;
				num++;
			}
		}
		return text;
	}
}
