using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageInfoDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		PLAY = 0,
		CANCEL = 1,
		BOOSTER0 = 2,
		BOOSTER1 = 3,
		BOOSTER2 = 4,
		GOTOADV = 5
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		EFFECT = 2,
		AUTHERROR_RETURN_TITLE = 3
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	public delegate void OnPartnerSelectEvent();

	private const float SCROLL_TIME = 0.2f;

	private const float SCROLL_Y = -80f;

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private OnPartnerSelectEvent mPartnerSelectPushed;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private float mStateTime;

	private bool mIsContinue;

	private bool mCanGetStar = true;

	private bool mTutorialEnd;

	private SMMapStageSetting mStageSetting;

	private PlayerClearData mClearData;

	private int mSelectedPartnerNo = -1;

	private int mSelectedPartnerZLv = -1;

	private TutorialMessageDialog mTutorialMessageDialog;

	private EventCharaDescDialog mEventCharaDescDialog;

	private ShopItemData mSaleItem;

	private ShopLimitedTime mSaleShop;

	private UISprite mSaleInnerFrame;

	private UIButton mSelectedPartnerButton;

	private UISprite mSelectedPartnerSprite;

	private UISprite mSelectedPartnerLvl;

	private GameObject mBoosterSelectGO;

	private UILabel mBoosterNum0;

	private UILabel mBoosterNum1;

	private UILabel mBoosterNum2;

	private UILabel mBoosterNum3;

	private UILabel mBoosterNum4;

	private UILabel mBoosterNum5;

	private int mNum0;

	private int mNum1;

	private int mNum2;

	private int mNum3;

	private int mNum4;

	private int mNum5;

	private JellyImageButton mBoosterButton0;

	private JellyImageButton mBoosterButton1;

	private JellyImageButton mBoosterButton2;

	private JellyImageButton mBoosterButton3;

	private JellyImageButton mBoosterButton4;

	private JellyImageButton mBoosterButton5;

	private UIButton mUIBoosterButton0;

	private UIButton mUIBoosterButton1;

	private UIButton mUIBoosterButton2;

	private UIButton mUIBoosterButton3;

	private UIButton mUIBoosterButton4;

	private UISprite mUIBoosterCheck0;

	private UISprite mUIBoosterCheck1;

	private UISprite mUIBoosterCheck2;

	private UISprite mUIBoosterCheck3;

	private UISprite mUIBoosterCheck4;

	private UISprite mUIBoosterCheck5;

	private UISprite sprite;

	private UIFont font = GameMain.LoadFont();

	private UIButton mPlayButton;

	private UILabel label;

	private string str;

	private string Imagetitle;

	private int starNum;

	private int hiScore;

	private float mCounter = 0.9f;

	private bool mAnimeStart;

	private bool mAnimeStart2;

	private UISprite ButtonColor;

	private bool mEffectEnd;

	private GameObject mObjeSkillCharge;

	private SMDailyEventSetting mDailyEvent;

	private List<int> mDailyEventBoxID;

	public int Debug_DEIndex;

	public int[] Debug_DEAccessories;

	public AccessoryData.ACCESSORY_TYPE[] Debug_AccessoryType;

	public string[] Debug_AccessoryNum;

	public int[] Debug_StageAccessories;

	public AccessoryData.ACCESSORY_TYPE[] Debug_StageAccessoryType;

	public string[] Debug_StageAccessoryNum;

	public bool[] Debug_StageAccessoryHas;

	public void SetEnableNoButton()
	{
		if (mPlayButton != null)
		{
			ButtonColor = mPlayButton.GetComponent<UISprite>();
			ButtonColor.color = Color.gray;
		}
		if (mSelectedPartnerButton != null)
		{
			ButtonColor = mSelectedPartnerButton.GetComponent<UISprite>();
			ButtonColor.color = Color.gray;
		}
		if (mUIBoosterButton0 != null && mTutorialEnd)
		{
			ButtonColor = mUIBoosterButton0.GetComponent<UISprite>();
			ButtonColor.color = Color.gray;
		}
		if (mUIBoosterButton1 != null && mTutorialEnd)
		{
			ButtonColor = mUIBoosterButton1.GetComponent<UISprite>();
			ButtonColor.color = Color.gray;
		}
		if (mUIBoosterButton2 != null && mTutorialEnd)
		{
			ButtonColor = mUIBoosterButton2.GetComponent<UISprite>();
			ButtonColor.color = Color.gray;
		}
		if (mUIBoosterButton3 != null && mTutorialEnd)
		{
			ButtonColor = mUIBoosterButton3.GetComponent<UISprite>();
			ButtonColor.color = Color.gray;
		}
		int companionLevel = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
		if (mSelectedPartnerZLv != -1)
		{
			companionLevel = mSelectedPartnerZLv;
		}
		if (mUIBoosterButton4 != null && companionLevel > 1 && mTutorialEnd && IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot5) && mBoosterButton4 != null)
		{
			ButtonColor = mUIBoosterButton4.GetComponent<UISprite>();
			ButtonColor.color = Color.gray;
		}
	}

	public void SetEnableYesButton()
	{
		if (mPlayButton != null)
		{
			ButtonColor = mPlayButton.GetComponent<UISprite>();
			ButtonColor.color = Color.white;
			NGUITools.SetActive(mPlayButton.gameObject, true);
			SsAnimation(mPlayButton.gameObject);
		}
		if (mSelectedPartnerButton != null)
		{
			ButtonColor = mSelectedPartnerButton.GetComponent<UISprite>();
			ButtonColor.color = Color.white;
			mSelectedPartnerButton.transform.localScale = new Vector3(mCounter, mCounter);
			SsAnimation(mSelectedPartnerButton.gameObject);
		}
		if (mUIBoosterButton0 != null && mTutorialEnd && !IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot1) && mBoosterButton0 != null)
		{
			ButtonColor = mUIBoosterButton0.GetComponent<UISprite>();
			ButtonColor.color = Color.white;
			mUIBoosterButton0.transform.localScale = new Vector3(mCounter, mCounter);
			SsAnimation(mUIBoosterButton0.gameObject);
		}
		if (mUIBoosterButton1 != null && mTutorialEnd && !IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot2) && mBoosterButton1 != null)
		{
			ButtonColor = mUIBoosterButton1.GetComponent<UISprite>();
			ButtonColor.color = Color.white;
			mUIBoosterButton1.transform.localScale = new Vector3(mCounter, mCounter);
			SsAnimation(mUIBoosterButton1.gameObject);
		}
		if (mUIBoosterButton2 != null && mTutorialEnd && !IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot3) && mBoosterButton2 != null)
		{
			ButtonColor = mUIBoosterButton2.GetComponent<UISprite>();
			ButtonColor.color = Color.white;
			mUIBoosterButton2.transform.localScale = new Vector3(mCounter, mCounter);
			SsAnimation(mUIBoosterButton2.gameObject);
		}
		if (mUIBoosterButton3 != null && mTutorialEnd && !IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot4) && mBoosterButton3 != null)
		{
			ButtonColor = mUIBoosterButton3.GetComponent<UISprite>();
			ButtonColor.color = Color.white;
			mUIBoosterButton3.transform.localScale = new Vector3(mCounter, mCounter);
			SsAnimation(mUIBoosterButton3.gameObject);
		}
		int companionLevel = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
		if (mSelectedPartnerZLv != -1)
		{
			companionLevel = mSelectedPartnerZLv;
		}
		if (mUIBoosterButton4 != null && companionLevel > 1 && mTutorialEnd && IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot5) && mBoosterButton4 != null)
		{
			ButtonColor = mUIBoosterButton4.GetComponent<UISprite>();
			ButtonColor.color = Color.white;
			mUIBoosterButton4.transform.localScale = new Vector3(mCounter, mCounter);
			SsAnimation(mUIBoosterButton4.gameObject);
		}
	}

	public void SsAnimation(GameObject parent)
	{
		UISsSprite uISsSprite = null;
		uISsSprite = Util.CreateGameObject("Shine", parent).AddComponent<UISsSprite>();
		uISsSprite.Animation = ResourceManager.LoadSsAnimation("EFFECT_SHINE").SsAnime;
		uISsSprite.depth = mBaseDepth + 10;
		uISsSprite.transform.localScale = new Vector3(1f, 1f, 1f);
		uISsSprite.DestroyAtEnd = true;
		uISsSprite.PlayCount = 1;
		uISsSprite.Play();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void Start()
	{
		MaintenanceCheckOnStart(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(bool a_isContinue = false, bool a_canGetStar = true)
	{
		mIsContinue = a_isContinue;
		mCanGetStar = a_canGetStar;
		if (!a_isContinue)
		{
			if (mGame.mPlayer.Data.LastUsedIcon >= 10000)
			{
				mSelectedPartnerNo = mGame.mPlayer.Data.LastUsedIcon - 10000;
			}
			else
			{
				mSelectedPartnerNo = mGame.mPlayer.Data.LastUsedIcon;
			}
			return;
		}
		mSelectedPartnerNo = mGame.mCompanionIndex;
		mSaleItem = null;
		DateTime dateTime = DateTimeUtil.Now();
		mGame.mPlayer.ShowSaleIconTime = dateTime;
		foreach (ShopLimitedTime shopSetItem in mGame.SegmentProfile.ShopSetItemList)
		{
			if (!(dateTime < DateTimeUtil.UnixTimeStampToDateTime(shopSetItem.StartTime, true)) && !(DateTimeUtil.UnixTimeStampToDateTime(shopSetItem.EndTime, true) < dateTime) && mGame.mShopItemData.ContainsKey(shopSetItem.ShopItemID) && mGame.mShopItemData[shopSetItem.ShopItemID] != null && mGame.mShopItemData[shopSetItem.ShopItemID].IsSale && !mGame.SegmentProfile.WebSale.DetailList.Contains(shopSetItem.ShopItemID) && shopSetItem.MugenHeartItem <= 0 && mGame.mPlayer.IsEnablePurchaseItem(shopSetItem.ShopItemID, shopSetItem.ShopItemLimit, shopSetItem.ShopCount) && (mSaleItem == null || (mGame.mShopItemData[shopSetItem.ShopItemID].GemAmount >= mSaleItem.GemAmount && (mGame.mShopItemData[shopSetItem.ShopItemID].GemAmount != mSaleItem.GemAmount || shopSetItem.ShopItemID <= mSaleShop.ShopItemID))))
			{
				mSaleShop = shopSetItem;
				mSaleItem = mGame.mShopItemData[shopSetItem.ShopItemID];
			}
		}
	}

	public void SetCompanion(int a_companionID)
	{
		mGame.mLastCompanionSetting = true;
		mState.Change(STATE.MAIN);
		mSelectedPartnerNo = a_companionID;
		string iconName = mGame.mCompanionData[mSelectedPartnerNo].iconName;
		if (mSelectedPartnerSprite != null)
		{
			Util.SetSpriteImageName(mSelectedPartnerSprite, iconName, Vector3.one, false);
		}
		else
		{
			Util.SetImageButtonGraphic(mSelectedPartnerButton, iconName, iconName, iconName);
		}
		int companionLevel = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
		CompanionData companionData = mGame.mCompanionData[a_companionID];
		int companionLevel2 = mGame.mPlayer.GetCompanionLevel(a_companionID);
		Util.SetSpriteImageName(imageName: (companionLevel2 >= companionData.maxLevel) ? Def.CharacterIconLevelMaxImageTbl[companionLevel2] : Def.CharacterIconLevelImageTbl[companionLevel2], sprite: mSelectedPartnerLvl, scale: Vector3.one, flip: false);
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (!mEffectEnd)
			{
				mState.Change(STATE.EFFECT);
			}
			if (mGame.mDebugRepeatPlayMode)
			{
				OnPlayPushed(null);
			}
			break;
		case STATE.EFFECT:
			if (!mAnimeStart)
			{
				mCounter += Time.deltaTime;
				if (mCounter > 0.95f)
				{
					mCounter = 0.9f;
					SetEnableYesButton();
					mAnimeStart = true;
					mAnimeStart2 = true;
				}
			}
			if (mAnimeStart && mAnimeStart2)
			{
				float num = Time.deltaTime - 0.02f;
				if (num < 0f)
				{
					num = 0.02f;
				}
				mCounter += num;
				if (mCounter <= 1f)
				{
					if (mSelectedPartnerButton != null)
					{
						mSelectedPartnerButton.transform.localScale = new Vector3(mCounter, mCounter);
					}
					if (mTutorialEnd)
					{
						if (mUIBoosterButton0 != null)
						{
							mUIBoosterButton0.transform.localScale = new Vector3(mCounter, mCounter);
						}
						if (mUIBoosterButton1 != null)
						{
							mUIBoosterButton1.transform.localScale = new Vector3(mCounter, mCounter);
						}
						if (mUIBoosterButton2 != null)
						{
							mUIBoosterButton2.transform.localScale = new Vector3(mCounter, mCounter);
						}
						if (mUIBoosterButton3 != null)
						{
							mUIBoosterButton3.transform.localScale = new Vector3(mCounter, mCounter);
						}
					}
					int companionLevel = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
					if (mSelectedPartnerZLv != -1)
					{
						companionLevel = mSelectedPartnerZLv;
					}
					if (companionLevel > 1 && mUIBoosterButton4 != null)
					{
						mUIBoosterButton4.transform.localScale = new Vector3(mCounter, mCounter);
					}
				}
				if (mCounter > 1f)
				{
					mCounter = 1f;
					mEffectEnd = true;
					mState.Change(STATE.MAIN);
				}
			}
			else
			{
				if (!mAnimeStart || mAnimeStart2)
				{
					break;
				}
				mCounter -= Time.deltaTime - 0.02f;
				if (mCounter > 0.9f)
				{
					if (mSelectedPartnerButton != null)
					{
						mSelectedPartnerButton.transform.localScale = new Vector3(mCounter, mCounter);
					}
					if (mTutorialEnd)
					{
						if (mUIBoosterButton0 != null)
						{
							mUIBoosterButton0.transform.localScale = new Vector3(mCounter, mCounter);
						}
						if (mUIBoosterButton1 != null)
						{
							mUIBoosterButton1.transform.localScale = new Vector3(mCounter, mCounter);
						}
						if (mUIBoosterButton2 != null)
						{
							mUIBoosterButton2.transform.localScale = new Vector3(mCounter, mCounter);
						}
						if (mUIBoosterButton3 != null)
						{
							mUIBoosterButton3.transform.localScale = new Vector3(mCounter, mCounter);
						}
					}
					int companionLevel2 = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
					if (mSelectedPartnerZLv != -1)
					{
						companionLevel2 = mSelectedPartnerZLv;
					}
					if (companionLevel2 > 1 && mUIBoosterButton4 != null)
					{
						mUIBoosterButton4.transform.localScale = new Vector3(mCounter, mCounter);
					}
				}
				if (mCounter < 0.9f)
				{
					mAnimeStart2 = true;
				}
			}
			break;
		case STATE.AUTHERROR_RETURN_TITLE:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		UpdateBoosterNum();
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		float _start = Time.realtimeSinceStartup;
		ResImage image = ResourceManager.LoadImageAsync("HUD2");
		ResSsAnimation anim = ResourceManager.LoadSsAnimationAsync("EFFECT_SHINE");
		yield return 0;
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (anim.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		int stageNumber = mGame.mCurrentStage.StageNumber;
		int a_main;
		int a_sub;
		if (mGame.IsDailyChallengePuzzle)
		{
			Def.SplitStageNo(stageNumber, out a_main, out a_sub);
			SMMapPageData seriesMapPageData = GameMain.GetSeriesMapPageData(mGame.mCurrentStage.Series);
			mStageSetting = seriesMapPageData.GetMapStage(a_main, a_sub);
			if (!mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, stageNumber, out mClearData))
			{
				mClearData = null;
			}
		}
		else if (mGame.mEventMode)
		{
			short a_course;
			Def.SplitEventStageNo(stageNumber, out a_course, out a_main, out a_sub);
			SMMapPageData currentEventPageData = GameMain.GetCurrentEventPageData();
			mStageSetting = currentEventPageData.GetMapStage(a_main, a_sub, a_course);
			int stageNo = Def.GetStageNo(a_main, a_sub);
			if (!mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, a_course, stageNo, out mClearData))
			{
				mClearData = null;
			}
		}
		else
		{
			Def.SplitStageNo(stageNumber, out a_main, out a_sub);
			SMMapPageData seriesMapPageData2 = GameMain.GetSeriesMapPageData(mGame.mPlayer.Data.CurrentSeries);
			mStageSetting = seriesMapPageData2.GetMapStage(a_main, a_sub);
			if (!mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, stageNumber, out mClearData))
			{
				mClearData = null;
			}
		}
		mGame.mUseBooster0 = false;
		mGame.mUseBooster1 = false;
		mGame.mUseBooster2 = false;
		mGame.mUseBooster3 = false;
		mGame.mUseBooster4 = false;
		mGame.mUseBooster5 = false;
		bool flag = false;
		if (mClearData != null)
		{
			hiScore = mClearData.HightScore;
			starNum = mClearData.GotStars;
			flag = true;
		}
		string empty = string.Empty;
		string text = empty;
		empty = string.Concat(text, "USE1:", mGame.mCurrentStage.BoosterSlot1, "\n");
		text = empty;
		empty = string.Concat(text, "USE2:", mGame.mCurrentStage.BoosterSlot2, "\n");
		text = empty;
		empty = string.Concat(text, "USE3:", mGame.mCurrentStage.BoosterSlot3, "\n");
		text = empty;
		empty = string.Concat(text, "USE4:", mGame.mCurrentStage.BoosterSlot4, "\n");
		text = empty;
		empty = string.Concat(text, "USE5:", mGame.mCurrentStage.BoosterSlot5, "\n");
		text = empty;
		empty = string.Concat(text, "USE6:", mGame.mCurrentStage.BoosterSlot6, "\n");
		mTutorialEnd = false;
		if (mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL8_3))
		{
			mTutorialEnd = true;
		}
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.EATEN)
		{
			Imagetitle = "LC_ClearingConditions04";
			str = Localization.Get("GetEaten_Title");
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.COLLECT)
		{
			Imagetitle = "LC_ClearingConditions03";
			str = Localization.Get("GetCollect_Title");
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.CLEAN && mGame.mCurrentStage.Paper0 + mGame.mCurrentStage.Paper1 + mGame.mCurrentStage.Paper2 > 0)
		{
			Imagetitle = "LC_ClearingConditions02";
			str = Localization.Get("GetClean_Title");
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.TIMER || mGame.mCurrentStage.LimitTimeSec > 0)
		{
			Imagetitle = "LC_ClearingConditions01";
			str = Localization.Get("GetScoreTime_Title");
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.GROWUP)
		{
			Imagetitle = "LC_ClearingConditions05";
			str = Localization.Get("GetCrystal_Title");
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY)
		{
			Imagetitle = "LC_ClearingConditions09";
			str = Localization.Get("GetFootprint_Title");
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.DEFEAT)
		{
			if (mGame.mCurrentStage.BossID == 0)
			{
				Imagetitle = "LC_ClearingConditions06";
				str = Localization.Get("GetEnemy001_Title");
			}
			else if (mGame.mCurrentStage.BossID == 1)
			{
				Imagetitle = "LC_ClearingConditions08";
				str = Localization.Get("GetEnemy001_Title");
			}
			else if (mGame.mCurrentStage.BossID == 2)
			{
				Imagetitle = "LC_ClearingConditions10";
				str = Localization.Get("GetEnemy001_Title");
			}
			else
			{
				Imagetitle = "LC_ClearingConditions12";
				str = Localization.Get("GetEnemy001_Title");
			}
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.FIND)
		{
			Imagetitle = "LC_ClearingConditions07";
			str = Localization.Get("GetSearch_Title");
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.PAIR)
		{
			Imagetitle = "LC_ClearingConditions11";
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.GOAL)
		{
			Imagetitle = "LC_ClearingConditions13";
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.TALISMAN)
		{
			Imagetitle = "LC_ClearingConditions14";
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			Imagetitle = "LC_ClearingConditions15";
		}
		else
		{
			Imagetitle = "LC_ClearingConditions00";
			str = Localization.Get("GetScore_Title");
		}
		if (!mIsContinue)
		{
			GoStageDialog();
		}
		else
		{
			string text2 = mGame.mCurrentStage.DisplayStageNumber;
			if (mGame.IsDailyChallengePuzzle)
			{
				text2 = mGame.SelectedDailyChallengeStageNo.ToString();
				if (mGame.mDebugDailyChallengeVisibleBaseStage)
				{
					text = text2;
					text2 = string.Concat(text, "[sup](", mGame.mCurrentStage.Series, mGame.mCurrentStage.DisplayStageNumber, ")[/sup]");
				}
			}
			str = Util.MakeLText("LevelStart_Title", text2);
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle(str);
		}
		if (mDailyEvent != null && mDailyEventBoxID != null && mDailyEventBoxID.Count > 0)
		{
			AccessoryData.ACCESSORY_TYPE aCCESSORY_TYPE = mGame.GuessDailyEventPresentBoxType(mDailyEventBoxID);
			if (aCCESSORY_TYPE != 0)
			{
				string dailyEventPresentBoxAnimeKey = mGame.GetDailyEventPresentBoxAnimeKey(aCCESSORY_TYPE);
				UISsSprite uISsSprite = Util.CreateGameObject("DailyEventBox", base.gameObject).AddComponent<UISsSprite>();
				uISsSprite.transform.localPosition = new Vector3(0f, 0f, 0f);
				ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(dailyEventPresentBoxAnimeKey);
				SsAnimation ssAnime = resSsAnimation.SsAnime;
				uISsSprite.Animation = ssAnime;
				uISsSprite.depth = mBaseDepth + 5;
				uISsSprite.ResetAnimationStatus();
				uISsSprite.Pause();
				uISsSprite.PlayCount = 0;
				Vector3 localPosition = mTitleBoard.transform.localPosition;
				Vector2 vector = new Vector2(mTitleBoard.width, mTitleBoard.height);
				Vector2 vector2 = new Vector2(20f, -20f);
				uISsSprite.transform.localPosition = new Vector3(localPosition.x - vector.x / 2f + vector2.x, localPosition.y + vector2.y, 0f);
				DailyEventTimer dailyEventTimer = Util.CreateGameObject("Timer", uISsSprite.gameObject).AddComponent<DailyEventTimer>();
				UIPanel component = GetComponent<UIPanel>();
				dailyEventTimer.InitHHMMDD(0, new Vector3(0f, 0f, -0.2f), component.sortingOrder + 10);
			}
		}
		BIJImage image2 = ResourceManager.LoadImage("HUD2").Image;
		sprite = Util.CreateSprite("title", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, Imagetitle, mBaseDepth + 3, new Vector3(0f, 183f, 0f), Vector3.one, false);
		Vector3 zero = Vector3.zero;
		zero = ((!mCanGetStar) ? new Vector3(0f, 100f) : new Vector3(-98f, 82f));
		str = string.Empty;
		str += Util.MakeLText("Target_score_Desc", mGame.mCurrentStage.BronzeScore);
		label = Util.CreateLabel("Desc", base.gameObject, font);
		Util.SetLabelInfo(label, str, mBaseDepth + 3, zero, 20, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(label);
		label.SetDimensions(mDialogFrame.width - 30, 60);
		label.color = Def.DEFAULT_MESSAGE_COLOR;
		label.overflowMethod = UILabel.Overflow.ShrinkContent;
		sprite = Util.CreateSprite("sen", label.gameObject, image);
		Util.SetSpriteInfo(sprite, "line00", mBaseDepth + 3, new Vector3(0f, -11f, 0f), Vector3.one, false);
		sprite.type = UIBasicSprite.Type.Sliced;
		sprite.SetDimensions(296, 6);
		UISprite[] array = null;
		if (mCanGetStar)
		{
			UISprite uISprite = Util.CreateSprite("StarBoard", base.gameObject, image);
			Util.SetSpriteInfo(uISprite, "score_banner_stars", mBaseDepth + 2, new Vector3(160f, 117f, 0f), Vector3.one, false);
			sprite = Util.CreateSprite("sen", uISprite.gameObject, image);
			Util.SetSpriteInfo(sprite, "line00", mBaseDepth + 3, new Vector3(0f, -46f, 0f), Vector3.one, false);
			sprite.type = UIBasicSprite.Type.Sliced;
			sprite.SetDimensions(200, 6);
			UISprite uISprite2 = Util.CreateSprite("Star1_Empty", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, "star_gold_empty", mBaseDepth + 3, new Vector3(-67f, -19f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(55, 55);
			UISprite uISprite3 = Util.CreateSprite("Star2_Empty", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite3, "star_gold_empty", mBaseDepth + 3, new Vector3(0f, -19f, 0f), Vector3.one, false);
			uISprite3.SetDimensions(55, 55);
			UISprite uISprite4 = Util.CreateSprite("Star3_Empty", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite4, "star_gold_empty", mBaseDepth + 3, new Vector3(67f, -19f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(55, 55);
			array = new UISprite[3];
			sprite = Util.CreateSprite("Star1", uISprite2.gameObject, image);
			if (starNum > 0)
			{
				Util.SetSpriteInfo(sprite, "star_gold", mBaseDepth + 4, new Vector3(0f, 0f, 0f), Vector3.one, false);
				array[0] = sprite;
				sprite.SetDimensions(55, 55);
			}
			else
			{
				array[0] = uISprite2;
			}
			sprite = Util.CreateSprite("Star2", uISprite3.gameObject, image);
			if (starNum > 1)
			{
				Util.SetSpriteInfo(sprite, "star_gold", mBaseDepth + 4, new Vector3(0f, 0f, 0f), Vector3.one, false);
				array[1] = sprite;
				sprite.SetDimensions(55, 55);
			}
			else
			{
				array[1] = uISprite3;
			}
			sprite = Util.CreateSprite("Star3", uISprite4.gameObject, image);
			if (starNum > 2)
			{
				Util.SetSpriteInfo(sprite, "star_gold", mBaseDepth + 4, new Vector3(0f, 0f, 0f), Vector3.one, false);
				array[2] = sprite;
				sprite.SetDimensions(55, 55);
			}
			else
			{
				array[2] = uISprite4;
			}
		}
		else
		{
			label = Util.CreateLabel("NoGetStarLabel", base.gameObject, font);
			Util.SetLabelInfo(label, Localization.Get("NoStars"), mBaseDepth + 4, new Vector3(0f, 75f), 17, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(label, new Color32(byte.MaxValue, 65, 110, byte.MaxValue));
		}
		if (mStageSetting != null && mGame.PlayingFriendHelp == null)
		{
			for (int i = 0; i < mStageSetting.BoxKind.Count; i++)
			{
				Def.ITEM_CATEGORY iTEM_CATEGORY = mStageSetting.BoxKind[i];
				if (iTEM_CATEGORY != Def.ITEM_CATEGORY.ACCESSORY || mStageSetting.BoxSpawnCondition[i] != Def.UNLOCK_TYPE.STAR)
				{
					continue;
				}
				int num = mStageSetting.BoxID[i];
				if (mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.ACCESSORY, num) != 0)
				{
					continue;
				}
				AccessoryData accessoryData = mGame.mAccessoryData[num];
				int num2 = mStageSetting.BoxSpawnConditionID[i] - 1;
				int index = (int)mStageSetting.BoxSpawnTileKind[i];
				string imageName = mGame.mTileData[index].DropImageNames[0][0];
				sprite = null;
				if (array != null)
				{
					if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.RB_KEY || accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.RB_KEY_R || accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.RB_KEY_S || accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.RB_KEY_SS || accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.RB_KEY_STARS || accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.RB_KEY_GF00)
					{
						sprite = Util.CreateSprite("InfoItem_" + num, array[num2].gameObject, image);
						Util.SetSpriteInfo(sprite, mGame.GetRBKeyImageName(accessoryData.Series), mBaseDepth + 5, Vector3.zero, Vector3.one, false);
					}
					else if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.ERB_KEY)
					{
						BIJImage image3 = ResourceManager.LoadImage("MAP_PAGE_F_ATLAS_1").Image;
						sprite = Util.CreateSprite("InfoItem_" + num, array[num2].gameObject, image3);
						Util.SetSpriteInfo(sprite, "lock_key", mBaseDepth + 5, Vector3.zero, Vector3.one, false);
					}
					else
					{
						sprite = Util.CreateSprite("InfoItem_" + num, array[num2].gameObject, image);
						Util.SetSpriteInfo(sprite, imageName, mBaseDepth + 5, Vector3.zero, Vector3.one, false);
					}
				}
				if (sprite != null)
				{
					sprite.SetDimensions(45, 45);
				}
			}
			if (!mIsContinue)
			{
				if ((mStageSetting.EnterPartners.Contains(mSelectedPartnerNo) && mGame.mPlayer.IsCompanionUnlock(mSelectedPartnerNo)) || flag || mGame.IsDailyChallengePuzzle)
				{
					if (!mGame.mPlayer.IsCompanionUnlock(mSelectedPartnerNo))
					{
						mSelectedPartnerNo = 0;
					}
					if (!flag && mStageSetting.EnterPartners.Count > 0 && mStageSetting.StageEnterCondition == "PARTNER_Z")
					{
						mSelectedPartnerNo = mStageSetting.EnterPartners[0];
						string[] array2 = mStageSetting.StageEnterConditionID.Split(',');
						mSelectedPartnerZLv = int.Parse(array2[1]);
						mGame.mLastCompanionSetting = false;
					}
					else
					{
						mGame.mLastCompanionSetting = true;
					}
				}
				else if (mStageSetting.EnterPartners.Count > 0)
				{
					mGame.mLastCompanionSetting = false;
					int num3 = -1;
					foreach (int enterPartner in mStageSetting.EnterPartners)
					{
						if (mGame.mPlayer.IsCompanionUnlock(enterPartner))
						{
							num3 = enterPartner;
							break;
						}
					}
					if (num3 != -1)
					{
						mSelectedPartnerNo = num3;
					}
					else
					{
						mSelectedPartnerNo = mStageSetting.EnterPartners[0];
					}
					if (mStageSetting.StageEnterCondition == "PARTNER_Z")
					{
						string[] array3 = mStageSetting.StageEnterConditionID.Split(',');
						mSelectedPartnerZLv = int.Parse(array3[1]);
					}
				}
				else
				{
					mGame.mLastCompanionSetting = true;
					if (!mGame.mPlayer.IsCompanionUnlock(mSelectedPartnerNo))
					{
						mSelectedPartnerNo = 0;
					}
				}
			}
			else if (mGame.mPlayer.mCompanionXPTemp.Count > 0)
			{
				mSelectedPartnerZLv = mGame.mPlayer.GetCompanionLevelTemp(mSelectedPartnerNo);
			}
			if (GameMain.USE_DEBUG_DIALOG)
			{
				SetDebugInfo(mStageSetting);
			}
		}
		MakeBoosterSelect(mTutorialEnd);
		UpdateBoosterNum();
		CreateCancelButton("OnCancelPushed");
	}

	public void GoStageDialog()
	{
		string text = mGame.mCurrentStage.DisplayStageNumber;
		if (mGame.IsDailyChallengePuzzle)
		{
			text = mGame.SelectedDailyChallengeStageNo.ToString();
			if (mGame.mDebugDailyChallengeVisibleBaseStage)
			{
				string text2 = text;
				text = string.Concat(text2, "[sup](", mGame.mCurrentStage.Series, mGame.mCurrentStage.DisplayStageNumber, ")[/sup]");
			}
		}
		if (mGame.mEventMode && mGame != null && !string.IsNullOrEmpty(mGame.mEventName))
		{
			str = mGame.mEventName + text;
		}
		else
		{
			str = Localization.Get("LevelInfo_Level") + text;
		}
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(str);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		ResourceManager.UnloadImage("HUD2");
		yield return null;
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mSelectItem != 0)
		{
			mGame.mPlayer.SetCompanionLevelTempClear();
			GlobalVariables.Instance.CanAcquireAccessoryIdListForWin.Clear();
		}
		mGame.mPlayer.ShowSaleIconTime = null;
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnPlayPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		if (mGame.mPlayer.LifeCount < 1)
		{
			if (mIsContinue)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_STAGE;
			}
			else if (mGame.mEventMode)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_EVENTMAP;
			}
			else
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_MAP;
			}
			BuyHeartDialog buyHeartDialog = Util.CreateGameObject("HeartDialog", base.transform.parent.gameObject).AddComponent<BuyHeartDialog>();
			buyHeartDialog.SetBaseDepth(mBaseDepth + 10);
			buyHeartDialog.Place = 2;
			buyHeartDialog.SetClosedCallback(OnHeartDialogClosed);
			buyHeartDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			mState.Reset(STATE.WAIT, true);
			return;
		}
		mGame.mCompanionIndex = mSelectedPartnerNo;
		if (mGame.mLastCompanionSetting)
		{
			mGame.mPlayer.Data.SelectedCompanion = (short)mGame.mCompanionIndex;
		}
		mGame.mUseBoosters.Clear();
		if (mGame.mUseBooster0)
		{
			mGame.mUseBoosters.Add(mGame.mCurrentStage.BoosterSlot1);
		}
		if (mGame.mUseBooster1)
		{
			mGame.mUseBoosters.Add(mGame.mCurrentStage.BoosterSlot2);
		}
		if (mGame.mUseBooster2)
		{
			mGame.mUseBoosters.Add(mGame.mCurrentStage.BoosterSlot3);
		}
		if (mGame.mUseBooster3)
		{
			mGame.mUseBoosters.Add(mGame.mCurrentStage.BoosterSlot4);
		}
		if (mGame.mUseBooster4)
		{
			mGame.mUseBoosters.Add(mGame.mCurrentStage.BoosterSlot5);
		}
		if (mGame.mUseBooster5)
		{
			mGame.mUseBoosters.Add(mGame.mCurrentStage.BoosterSlot6);
		}
		mGame.mPlayer.Data.UsePlusChanceBoosterCount = mGame.mUseBoosters.Count;
		mGame.mPlayer.Data.UseBeforeBooster.Clear();
		mGame.mPlayer.Data.UseBeforeBooster.AddRange(mGame.mUseBoosters.ToArray());
		mGame.Save();
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL1)
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL1);
		}
		mGame.PlaySe("SE_PLAY_BUTTON", 1);
		mGame.PlaySe("VOICE_003", 2);
		if (!mIsContinue)
		{
			int mode = ((mGame.mDeviceOrientaion == ScreenOrientation.LandscapeLeft || mGame.mDeviceOrientaion == ScreenOrientation.LandscapeRight) ? 1 : 0);
			ServerCram.DisPlayMode(mode, Network.DeviceModel, 0);
		}
		int seriesCampaignMoves = GetSeriesCampaignMoves();
		if (seriesCampaignMoves > -1)
		{
			GlobalVariables.Instance.AddMovesCampaignAmount = (byte)seriesCampaignMoves;
		}
		mSelectItem = SELECT_ITEM.PLAY;
		Close();
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			if (mGame.PlayingFriendHelp != null)
			{
				mGame.SetFriendHelp(null);
			}
			Close();
		}
	}

	public void OnHeartDialogClosed(BuyHeartDialog.SELECT_ITEM i)
	{
		mState.Reset(STATE.MAIN, true);
	}

	public void OnBackToCompanionPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mPartnerSelectPushed != null)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.mCompanionIndex = mSelectedPartnerNo;
			mPartnerSelectPushed();
		}
	}

	public void OnEventCharaDescPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		mEventCharaDescDialog = Util.CreateGameObject("EventCharaDescDialog", base.gameObject).AddComponent<EventCharaDescDialog>();
		SMMapPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		SMEventPageData sMEventPageData = currentEventPageData as SMEventPageData;
		bool flag = false;
		if (mStageSetting.StageEnterCondition.CompareTo("PARTNER_Z") == 0 && mStageSetting.PartnerZ_Level == 5)
		{
			flag = true;
		}
		if (flag)
		{
			if (sMEventPageData.EventSetting.EventType != Def.EVENT_TYPE.SM_STAR)
			{
				mEventCharaDescDialog.Init(Localization.Get("DemoEV_TriCharaLv5_Title"), Localization.Get("DemoEV_TriCharaLv5_Desc00"), Localization.Get("DemoEV_TriCharaLv5_Desc01"), mStageSetting.EnterPartners[0], mStageSetting.PartnerZ_Level);
			}
			else
			{
				mEventCharaDescDialog.Init(Localization.Get("DemoEV_TriCharaLv5_Title"), Localization.Get("DemoEV_TriCharaLv5_Desc00C"), Localization.Get("DemoEV_TriCharaLv5_Desc01"), mStageSetting.EnterPartners[0], mStageSetting.PartnerZ_Level);
			}
		}
		else if (sMEventPageData.EventSetting.EventType != Def.EVENT_TYPE.SM_STAR)
		{
			mEventCharaDescDialog.Init(Localization.Get("DemoEV_TriChara_Title"), Localization.Get("DemoEV_TriChara_Desc00"), Localization.Get("DemoEV_TriChara_Desc01"));
		}
		else
		{
			mEventCharaDescDialog.Init(Localization.Get("DemoEV_TriChara_Title"), Localization.Get("DemoEV_TriChara_Desc00C"), Localization.Get("DemoEV_TriChara_Desc01"));
		}
		mEventCharaDescDialog.SetClosedCallback(OnEventCharaDescClosed);
		mState.Reset(STATE.WAIT, true);
	}

	public void OnEventCharaDescClosed()
	{
		mState.Reset(STATE.MAIN, true);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetPartnerSelectCallback(OnPartnerSelectEvent callback)
	{
		mPartnerSelectPushed = callback;
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}

	private bool IsUpdateBoosterKind(Def.BOOSTER_KIND a_kind)
	{
		bool result = false;
		if (a_kind == Def.BOOSTER_KIND.SKILL_CHARGE && mGame.mCurrentStage != null && mGame.mCurrentStage.UseColor5)
		{
			result = true;
		}
		return result;
	}

	private void UpdateBoosterNum()
	{
		if (mBoosterNum0 != null)
		{
			int num = mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot1);
			if (mGame.mUseBooster0)
			{
				num--;
			}
			if (num > 99)
			{
				num = 99;
			}
			if (mNum0 != num)
			{
				mNum0 = num;
				Util.SetLabelText(mBoosterNum0, string.Empty + num);
			}
		}
		if (mBoosterNum1 != null)
		{
			int num = mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot2);
			if (mGame.mUseBooster1)
			{
				num--;
			}
			if (num > 99)
			{
				num = 99;
			}
			if (mNum1 != num)
			{
				mNum1 = num;
				Util.SetLabelText(mBoosterNum1, string.Empty + num);
			}
		}
		if (mBoosterNum2 != null)
		{
			int num = mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot3);
			if (mGame.mUseBooster2)
			{
				num--;
			}
			if (num > 99)
			{
				num = 99;
			}
			if (mNum2 != num)
			{
				mNum2 = num;
				Util.SetLabelText(mBoosterNum2, string.Empty + num);
			}
		}
		if (mBoosterNum3 != null)
		{
			int num = mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot4);
			if (mGame.mUseBooster3)
			{
				num--;
			}
			if (num > 99)
			{
				num = 99;
			}
			if (mNum3 != num)
			{
				mNum3 = num;
				Util.SetLabelText(mBoosterNum3, string.Empty + num);
			}
		}
		if (mBoosterNum4 != null)
		{
			int num = mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot5);
			if (mGame.mUseBooster4)
			{
				num--;
			}
			if (num > 99)
			{
				num = 99;
			}
			if (mNum4 != num)
			{
				mNum4 = num;
				Util.SetLabelText(mBoosterNum4, string.Empty + num);
			}
		}
		if (mBoosterNum5 != null)
		{
			int num = mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot6);
			if (mGame.mUseBooster5)
			{
				num--;
			}
			if (num > 99)
			{
				num = 99;
			}
			if (mNum5 != num)
			{
				mNum5 = num;
				Util.SetLabelText(mBoosterNum5, string.Empty + num);
			}
		}
		int companionLevel = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
		if (mSelectedPartnerZLv != -1)
		{
			companionLevel = mSelectedPartnerZLv;
		}
		if (mBoosterButton0 != null && IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot1))
		{
			if (companionLevel < 2 && mBoosterButton0.IsEnable())
			{
				mBoosterButton0.SetButtonEnable(false);
			}
			else if (companionLevel >= 2 && !mBoosterButton0.IsEnable())
			{
				mBoosterButton0.SetButtonEnable(true);
			}
		}
		if (mBoosterButton1 != null && IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot2))
		{
			if (companionLevel < 2 && mBoosterButton1.IsEnable())
			{
				mBoosterButton1.SetButtonEnable(false);
			}
			else if (companionLevel >= 2 && !mBoosterButton1.IsEnable())
			{
				mBoosterButton1.SetButtonEnable(true);
			}
		}
		if (mBoosterButton2 != null && IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot3))
		{
			if (companionLevel < 2 && mBoosterButton2.IsEnable())
			{
				mBoosterButton2.SetButtonEnable(false);
			}
			else if (companionLevel >= 2 && !mBoosterButton2.IsEnable())
			{
				mBoosterButton2.SetButtonEnable(true);
			}
		}
		if (mBoosterButton3 != null && IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot4))
		{
			if (companionLevel < 2 && mBoosterButton3.IsEnable())
			{
				mBoosterButton3.SetButtonEnable(false);
			}
			else if (companionLevel >= 2 && !mBoosterButton3.IsEnable())
			{
				mBoosterButton3.SetButtonEnable(true);
			}
		}
		if (mBoosterButton4 != null)
		{
			if (IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot5))
			{
				if (companionLevel < 2 && mBoosterButton4.IsEnable())
				{
					mBoosterButton4.SetButtonEnable(false);
					SkillChargeUndo();
				}
				else if (companionLevel >= 2 && !mBoosterButton4.IsEnable())
				{
					mBoosterButton4.SetButtonEnable(true);
				}
			}
			else
			{
				mBoosterButton4.SetButtonEnable(false);
				SkillChargeUndo();
			}
		}
		if (IsUpdateBoosterKind(mGame.mCurrentStage.BoosterSlot6) && mBoosterButton5 != null)
		{
			if (companionLevel < 2 && mBoosterButton5.IsEnable())
			{
				mBoosterButton5.SetButtonEnable(false);
			}
			else if (companionLevel >= 2 && !mBoosterButton5.IsEnable())
			{
				mBoosterButton5.SetButtonEnable(true);
			}
		}
	}

	private void SkillChargeUndo()
	{
		string text = null;
		BoosterData boosterData = mGame.mBoosterData[(int)mGame.mCurrentStage.BoosterSlot5];
		if (mGame.mUseBooster4)
		{
			mGame.mUseBooster4 = false;
			text = boosterData.iconNameOff;
			mUIBoosterCheck4.enabled = false;
			mGame.PlaySe("SE_CHECK_BOOSTER_OFF", -1);
		}
		if (mObjeSkillCharge != null && text != null)
		{
			Util.SetImageButtonGraphic(mObjeSkillCharge.GetComponent<UIButton>(), text, text, text);
		}
	}

	private void MakeBoosterSelect(bool a_tutorialEnd)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int w = 520;
		int h = 245;
		mBoosterSelectGO = Util.CreateGameObject("BoosterSelectGO", base.gameObject);
		mBoosterSelectGO.transform.localPosition = new Vector3(0f, -98f, 0f);
		UISprite uISprite = Util.CreateSprite("BoosterSelectBoard", mBoosterSelectGO, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 40f, 0f), Vector3.one, false);
		uISprite.SetDimensions(w, h);
		uISprite.type = UIBasicSprite.Type.Sliced;
		UILabel uILabel = Util.CreateLabel("SelectBoosters", mBoosterSelectGO.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 3, new Vector3(0f, 136f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		string text = ((!a_tutorialEnd) ? Util.MakeLText("LevelInfo_SelectBoostersNG") : Util.MakeLText("LevelInfo_SelectBoosters"));
		Util.SetLabelText(uILabel, text);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, 30);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		if (!mIsContinue)
		{
			UISprite uISprite2 = Util.CreateSprite("Border", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, "line01", mBaseDepth + 3, new Vector3(95f, -17f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(18, 195);
			CompanionData companionData = mGame.mCompanionData[mSelectedPartnerNo];
			bool flag = false;
			if (mGame.mPlayer.IsCompanionUnlock(companionData.index))
			{
				flag = true;
			}
			if (mStageSetting == null || mStageSetting.IsForcePartner || mClearData != null)
			{
				flag = true;
			}
			bool flag2 = false;
			int num = 1;
			mGame.mPlayer.SetCompanionLevelTempClear();
			if ((!mGame.mPlayer.IsCompanionUnlock(mSelectedPartnerNo) || mClearData == null) && mStageSetting.EnterPartners.Count != 0 && mStageSetting.StageEnterCondition == "PARTNER_Z")
			{
				string[] array = mStageSetting.StageEnterConditionID.Split(',');
				flag = true;
				mGame.mPlayer.SetCompanionLevelTemp(mSelectedPartnerNo, int.Parse(array[1]));
				flag2 = true;
				num = int.Parse(array[1]);
			}
			GameObject gameObject = null;
			string iconName = companionData.iconName;
			int num2 = 0;
			for (int i = 0; i < mGame.mCompanionData.Count; i++)
			{
				if (mGame.mPlayer.IsCompanionUnlock(i))
				{
					num2++;
				}
			}
			if ((mClearData == null || num2 == 1) && (mIsContinue || !mGame.mPlayer.IsCompanionUnlock(companionData.index) || (mStageSetting != null && mStageSetting.EnterPartners.Count == 1)) && !mGame.IsDailyChallengePuzzle)
			{
				if (!flag2)
				{
					mSelectedPartnerSprite = Util.CreateSprite("SelectedPartner", uISprite.gameObject, image2);
					Util.SetSpriteInfo(mSelectedPartnerSprite, iconName, mBaseDepth + 3, new Vector3(172f, 13f, 0f), Vector3.one, false);
					string imageName = "LC_button_charachange";
					UISprite uISprite3 = Util.CreateSprite("NoButton", mSelectedPartnerSprite.gameObject, image);
					Util.SetSpriteInfo(uISprite3, imageName, mBaseDepth + 3, new Vector3(0f, -90f, 0f), Vector3.one, false);
					gameObject = mSelectedPartnerSprite.gameObject;
					if (mSelectedPartnerButton != null)
					{
						UnityEngine.Object.Destroy(mSelectedPartnerButton);
						mSelectedPartnerButton = null;
					}
					uISprite3.color = Color.gray;
					mSelectedPartnerLvl = Util.CreateSprite("SelectedPartnerLvl", gameObject, image);
					int companionLevel = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
					CompanionData companionData2 = mGame.mCompanionData[mSelectedPartnerNo];
					Util.SetSpriteInfo(imageName: (companionLevel >= companionData2.maxLevel) ? Def.CharacterIconLevelMaxImageTbl[companionLevel] : Def.CharacterIconLevelImageTbl[companionLevel], sprite: mSelectedPartnerLvl, depth: mBaseDepth + 4, position: new Vector3(0f, -55f, 0f), scale: Vector3.one, flip: false);
				}
				else
				{
					mSelectedPartnerSprite = Util.CreateSprite("SelectedPartner", uISprite.gameObject, image2);
					Util.SetSpriteInfo(mSelectedPartnerSprite, iconName, mBaseDepth + 3, new Vector3(172f, 13f, 0f), Vector3.one, false);
					string text2 = "LC_button_charachange2";
					mSelectedPartnerButton = Util.CreateJellyImageButton("ButtonPartner", mSelectedPartnerSprite.gameObject, image);
					Util.SetImageButtonInfo(mSelectedPartnerButton, text2, text2, text2, mBaseDepth + 3, new Vector3(0f, -90f, 0f), Vector3.one);
					Util.AddImageButtonMessage(mSelectedPartnerButton, this, "OnEventCharaDescPushed", UIButtonMessage.Trigger.OnClick);
					gameObject = mSelectedPartnerSprite.gameObject;
					mSelectedPartnerLvl = Util.CreateSprite("SelectedPartnerLvl", gameObject, image);
					CompanionData companionData3 = mGame.mCompanionData[mSelectedPartnerNo];
					Util.SetSpriteInfo(imageName: (num >= companionData3.maxLevel) ? Def.CharacterIconLevelMaxImageTbl[num] : Def.CharacterIconLevelImageTbl[num], sprite: mSelectedPartnerLvl, depth: mBaseDepth + 4, position: new Vector3(0f, -55f, 0f), scale: Vector3.one, flip: false);
				}
			}
			else
			{
				mSelectedPartnerSprite = Util.CreateSprite("SelectedPartner", uISprite.gameObject, image2);
				Util.SetSpriteInfo(mSelectedPartnerSprite, iconName, mBaseDepth + 3, new Vector3(172f, 13f, 0f), Vector3.one, false);
				string text3 = "LC_button_charachange";
				mSelectedPartnerButton = Util.CreateJellyImageButton("ButtonPartner", mSelectedPartnerSprite.gameObject, image);
				Util.SetImageButtonInfo(mSelectedPartnerButton, text3, text3, text3, mBaseDepth + 3, new Vector3(0f, -90f, 0f), Vector3.one);
				Util.AddImageButtonMessage(mSelectedPartnerButton, this, "OnBackToCompanionPushed", UIButtonMessage.Trigger.OnClick);
				gameObject = mSelectedPartnerButton.gameObject;
				mSelectedPartnerLvl = Util.CreateSprite("SelectedPartnerLvl", mSelectedPartnerSprite.gameObject, image);
				int companionLevel2 = mGame.mPlayer.GetCompanionLevel(mSelectedPartnerNo);
				CompanionData companionData4 = mGame.mCompanionData[mSelectedPartnerNo];
				Util.SetSpriteInfo(imageName: (companionLevel2 >= companionData4.maxLevel) ? Def.CharacterIconLevelMaxImageTbl[companionLevel2] : Def.CharacterIconLevelImageTbl[companionLevel2], sprite: mSelectedPartnerLvl, depth: mBaseDepth + 4, position: new Vector3(0f, -55f, 0f), scale: Vector3.one, flip: false);
				if (num2 > 1 && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL13_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL13_0, base.ParentGameObject, OnTutorialMessageFinished, OnTutorialMessageClosed);
					mGame.mTutorialManager.SetTutorialPoint1Arrow(base.gameObject, mSelectedPartnerButton.gameObject, false, 80, Def.DIR.NONE, 0f);
				}
			}
			if (flag)
			{
				mPlayButton = Util.CreateJellyImageButton("ButtonPlay", mBoosterSelectGO.gameObject, image);
				Util.SetImageButtonInfo(mPlayButton, "LC_button_play", "LC_button_play", "LC_button_play", mBaseDepth + 3, new Vector3(0f, -136f, 0f), Vector3.one);
				Util.AddImageButtonMessage(mPlayButton, this, "OnPlayPushed", UIButtonMessage.Trigger.OnClick);
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1, mGame.mPlayer.IsTutorialSkipped()))
				{
					mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL1, base.ParentGameObject, delegate
					{
					}, delegate
					{
					});
					mGame.mTutorialManager.SetTutorialFinger(mPlayButton.gameObject, mPlayButton.gameObject, Def.DIR.NONE);
					mGame.mTutorialManager.AddAllowedButton(mPlayButton.gameObject);
				}
				UISprite uISprite4 = Util.CreateSprite("runa", base.gameObject, image);
				Util.SetSpriteInfo(uISprite4, "chara_play_runa", mBaseDepth + 3, new Vector3(-138f, -242f), Vector3.one, false);
				UISprite uISprite5 = Util.CreateSprite("alt", base.gameObject, image);
				Util.SetSpriteInfo(uISprite5, "chara_play_alt", mBaseDepth + 3, new Vector3(138f, -242f), Vector3.one, false);
			}
			else
			{
				mSelectedPartnerLvl.color = Color.gray;
				uILabel = Util.CreateLabel("NoPlayLabel", mBoosterSelectGO.gameObject, atlasFont);
				text = Util.MakeLText("LevelInfo_NoPlayablePartners");
				Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(0f, -130f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel);
				uILabel.SetDimensions(mDialogFrame.width - 30, 30);
				uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
				uILabel.color = Color.red;
			}
			mNum0 = -1;
			mNum1 = -1;
			mNum2 = -1;
			mNum3 = -1;
			mNum4 = -1;
			mNum5 = -1;
			MakeBoosterButton(uISprite.gameObject, 0, mGame.mCurrentStage.BoosterSlot1, new Vector3(-176f, 43f, 0f), mGame.mUseBooster0, a_tutorialEnd, flag, out mBoosterButton0, out mBoosterNum0);
			MakeBoosterButton(uISprite.gameObject, 1, mGame.mCurrentStage.BoosterSlot2, new Vector3(-70f, 43f, 0f), mGame.mUseBooster1, a_tutorialEnd, flag, out mBoosterButton1, out mBoosterNum1);
			MakeBoosterButton(uISprite.gameObject, 2, mGame.mCurrentStage.BoosterSlot3, new Vector3(36f, 43f, 0f), mGame.mUseBooster2, a_tutorialEnd, flag, out mBoosterButton2, out mBoosterNum2);
			MakeBoosterButton(uISprite.gameObject, 3, mGame.mCurrentStage.BoosterSlot4, new Vector3(-176f, -64f, 0f), mGame.mUseBooster3, a_tutorialEnd, flag, out mBoosterButton3, out mBoosterNum3);
			MakeBoosterButton(uISprite.gameObject, 4, mGame.mCurrentStage.BoosterSlot5, new Vector3(-70f, -64f, 0f), mGame.mUseBooster4, a_tutorialEnd, flag, out mBoosterButton4, out mBoosterNum4);
		}
		else
		{
			UISprite uISprite6 = Util.CreateSprite("runa", base.gameObject, image);
			Util.SetSpriteInfo(uISprite6, "chara_play_runa", mBaseDepth + 3, new Vector3(-138f, -242f), Vector3.one, false);
			UISprite uISprite7 = Util.CreateSprite("alt", base.gameObject, image);
			Util.SetSpriteInfo(uISprite7, "chara_play_alt", mBaseDepth + 3, new Vector3(138f, -242f), Vector3.one, false);
			mPlayButton = Util.CreateJellyImageButton("ButtonPlay", mBoosterSelectGO.gameObject, image);
			Util.SetImageButtonInfo(mPlayButton, "LC_button_play", "LC_button_play", "LC_button_play", mBaseDepth + 3, new Vector3(0f, -136f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mPlayButton, this, "OnPlayPushed", UIButtonMessage.Trigger.OnClick);
			mNum0 = -1;
			mNum1 = -1;
			mNum2 = -1;
			mNum3 = -1;
			mNum4 = -1;
			mNum5 = -1;
			MakeBoosterButton(uISprite.gameObject, 0, mGame.mCurrentStage.BoosterSlot1, new Vector3(-106f, 43f, 0f), mGame.mUseBooster0, a_tutorialEnd, true, out mBoosterButton0, out mBoosterNum0);
			MakeBoosterButton(uISprite.gameObject, 1, mGame.mCurrentStage.BoosterSlot2, new Vector3(0f, 43f, 0f), mGame.mUseBooster1, a_tutorialEnd, true, out mBoosterButton1, out mBoosterNum1);
			MakeBoosterButton(uISprite.gameObject, 2, mGame.mCurrentStage.BoosterSlot3, new Vector3(106f, 43f, 0f), mGame.mUseBooster2, a_tutorialEnd, true, out mBoosterButton2, out mBoosterNum2);
			MakeBoosterButton(uISprite.gameObject, 3, mGame.mCurrentStage.BoosterSlot4, new Vector3(-106f, -64f, 0f), mGame.mUseBooster3, a_tutorialEnd, true, out mBoosterButton3, out mBoosterNum3);
			MakeBoosterButton(uISprite.gameObject, 4, mGame.mCurrentStage.BoosterSlot5, new Vector3(0f, -64f, 0f), mGame.mUseBooster4, a_tutorialEnd, true, out mBoosterButton4, out mBoosterNum4);
			if (mSaleItem != null && a_tutorialEnd)
			{
				mSaleInnerFrame = Util.CreateSprite("inner", uISprite.gameObject, image);
				Util.SetSpriteInfo(mSaleInnerFrame, "instruction_panel17", mBaseDepth + 2, new Vector3(106f, -64f), Vector3.one, false);
				mSaleInnerFrame.type = UIBasicSprite.Type.Sliced;
				mSaleInnerFrame.SetDimensions(105, 105);
				string text4 = mSaleItem.SaleIconName;
				if (string.IsNullOrEmpty(text4))
				{
					text4 = "button_boosterdialog_itemset";
				}
				UIButton button = Util.CreateJellyImageButton("SaleIcon_" + mSaleItem.Index, mSaleInnerFrame.gameObject, image);
				Util.SetImageButtonInfo(button, text4, text4, text4, mBaseDepth + 3, new Vector3(0f, 2f, 0f), new Vector3(0.9f, 0.9f));
				Util.AddImageButtonMessage(button, this, "OnSaleIconPushed", UIButtonMessage.Trigger.OnClick);
				UISprite sale_now = Util.CreateSprite("shopBugSale", mSaleInnerFrame.gameObject, image);
				Util.SetSpriteInfo(sale_now, "LC_sale_mini00", mBaseDepth + 4, new Vector3(0f, -37.2f, 0f), new Vector3(0.9f, 0.9f), false);
				StartCoroutine(UpdateSaleBound(sale_now));
			}
		}
		int num3 = GetSeriesCampaignMoves();
		if (num3 == -1)
		{
			num3 = GlobalVariables.Instance.AddMovesCampaignAmount;
		}
		if (mPlayButton != null && num3 > 0)
		{
			string text5 = string.Empty;
			int w2 = 350;
			switch (mGame.mCurrentStage.LoseType)
			{
			case Def.STAGE_LOSE_CONDITION.TIME:
				text5 = string.Format(Localization.Get("BonusTime"), num3 * 3);
				w2 = 350;
				break;
			case Def.STAGE_LOSE_CONDITION.MOVES:
				text5 = string.Format(Localization.Get("BonusMove"), num3);
				w2 = 280;
				break;
			}
			if (!string.IsNullOrEmpty(text5))
			{
				Vector3 localPosition = mPlayButton.transform.localPosition;
				UISprite uISprite8 = Util.CreateSprite("SeriesCampaignBanner", mPlayButton.transform.parent.gameObject, image);
				Util.SetSpriteInfo(uISprite8, "instruction_accessory12", mBaseDepth + 5, localPosition + new Vector3(0f, 45f, 0f), Vector3.one, false);
				uISprite8.type = UIBasicSprite.Type.Sliced;
				uISprite8.SetDimensions(w2, 42);
				UILabel uILabel2 = Util.CreateLabel("SeriesCampaignText", uISprite8.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel2, text5, mBaseDepth + 6, new Vector3(0f, 5f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
				uILabel2.SetDimensions(260, 26);
				uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
				UpdateSeriesCampaignBanner(uISprite8);
			}
		}
		SetEnableNoButton();
	}

	private int GetSeriesCampaignMoves()
	{
		int num = -1;
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0))
		{
			return 0;
		}
		if (!mIsContinue)
		{
			num = 0;
			if (!mGame.IsDailyChallengePuzzle)
			{
				num = ((!mGame.mEventMode) ? mGame.GetSeriesCampaignMoves(mGame.mCurrentStage.Series) : mGame.GetSeriesCampaignMovesEvent(mGame.mCurrentStage.EventID));
			}
		}
		else
		{
			num = -1;
		}
		return num;
	}

	private IEnumerator UpdateSeriesCampaignBanner(UISprite a_sprite)
	{
		yield break;
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE && selectItem != TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			TutorialStart(tUTORIAL_INDEX);
			return;
		}
		mGame.mTutorialManager.DeleteTutorialPointArrow();
		mGame.mTutorialManager.TutorialSkip(index);
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(tUTORIAL_INDEX);
		}
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.DeleteTutorialPointArrow();
			mGame.mTutorialManager.TutorialSkip(index);
		}
	}

	private void MakeBoosterButton(GameObject a_parent, int a_slot, Def.BOOSTER_KIND a_kind, Vector3 a_pos, bool a_use, bool a_tutorialEnd, bool a_enable, out JellyImageButton a_button, out UILabel a_label)
	{
		UIButton uIButton = null;
		UISprite uISprite = null;
		a_button = null;
		a_label = null;
		string empty = string.Empty;
		UIFont atlasFont = GameMain.LoadFont();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		if (a_kind != 0 && (int)a_kind < mGame.mBoosterData.Count)
		{
			BoosterData boosterData = mGame.mBoosterData[(int)a_kind];
			empty = boosterData.iconNameOff;
			GameObject parent;
			UISprite uISprite2;
			if (a_enable)
			{
				uIButton = Util.CreateJellyImageButton("ButtonBooster" + a_slot, a_parent, image);
				Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth + 3, a_pos, Vector3.one);
				a_button = uIButton.gameObject.GetComponent<JellyImageButton>();
				if (!a_tutorialEnd)
				{
					a_button.SetButtonEnable(false);
				}
				Util.AddImageButtonMessage(uIButton, this, string.Format("OnBooster{0}Pushed", a_slot), UIButtonMessage.Trigger.OnClick);
				parent = uIButton.gameObject;
			}
			else
			{
				uISprite2 = Util.CreateSprite("NumBall", a_parent, image);
				Util.SetSpriteInfo(uISprite2, empty, mBaseDepth + 3, a_pos, Vector3.one, false);
				uISprite2.color = Color.gray;
				parent = uISprite2.gameObject;
			}
			uISprite = Util.CreateSprite("check", parent, image);
			Util.SetSpriteInfo(uISprite, "button_check", mBaseDepth + 4, new Vector3(0f, -10f, 0f), new Vector3(1.2f, 1.2f, 0f), false);
			if (!a_use)
			{
				uISprite.enabled = false;
			}
			uISprite2 = Util.CreateSprite("NumBall", parent, image);
			Util.SetSpriteInfo(uISprite2, "button_booster_num", mBaseDepth + 5, new Vector3(29f, -27f, 0f), Vector3.one, false);
			int num = mGame.mPlayer.GetBoosterNum(a_kind);
			if (num > 99)
			{
				num = 99;
			}
			a_label = Util.CreateLabel("Num", parent, atlasFont);
			Util.SetLabelInfo(a_label, string.Empty, mBaseDepth + 6, new Vector3(29f, -31f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelText(a_label, string.Empty + num);
			a_label.color = Color.white;
			a_label.fontSize = 18;
			a_label.spacingY = 6;
			if (a_tutorialEnd && a_enable)
			{
				uISprite2.color = Color.white;
				a_label.color = Color.white;
			}
			else
			{
				uISprite2.color = Color.gray;
				a_label.color = Color.gray;
			}
		}
		else
		{
			empty = "button_boosterdialog05";
			UISprite uISprite3 = Util.CreateSprite("ButtonBooster" + a_slot, a_parent, image);
			Util.SetSpriteInfo(uISprite3, empty, mBaseDepth + 3, a_pos, Vector3.one, false);
			uISprite3.color = Color.gray;
		}
		switch (a_slot)
		{
		case 0:
			mUIBoosterButton0 = uIButton;
			mUIBoosterCheck0 = uISprite;
			break;
		case 1:
			mUIBoosterButton1 = uIButton;
			mUIBoosterCheck1 = uISprite;
			break;
		case 2:
			mUIBoosterButton2 = uIButton;
			mUIBoosterCheck2 = uISprite;
			break;
		case 3:
			mUIBoosterButton3 = uIButton;
			mUIBoosterCheck3 = uISprite;
			break;
		case 4:
			mUIBoosterButton4 = uIButton;
			mUIBoosterCheck4 = uISprite;
			break;
		}
		if (a_kind != Def.BOOSTER_KIND.SKILL_CHARGE)
		{
		}
	}

	public void OnBooster0Pushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (!go.GetComponent<JellyImageButton>().IsEnable())
			{
				mGame.PlaySe("SE_PUSH", -1);
			}
			else if (mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot1) == 0)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.STAGEINFO;
				BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BuyBoosuterDialog", base.transform.parent.gameObject).AddComponent<BuyBoosterDialog>();
				buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.PRE_STAGE;
				buyBoosterDialog.SetBaseDepth(mBaseDepth + 10);
				buyBoosterDialog.Init(mGame.mShopItemData[(int)mGame.mCurrentStage.BoosterSlot1]);
				buyBoosterDialog.SetClosedCallback(OnBuyBoosterClosed);
				buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else if (!mGame.mUseBooster0)
			{
				mGame.mUseBooster0 = true;
				mUIBoosterCheck0.enabled = true;
				mGame.PlaySe("SE_CHECK_BOOSTER_ON", -1);
			}
			else
			{
				mGame.mUseBooster0 = false;
				mUIBoosterCheck0.enabled = false;
				mGame.PlaySe("SE_CHECK_BOOSTER_OFF", -1);
			}
		}
	}

	public void OnBooster1Pushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (!go.GetComponent<JellyImageButton>().IsEnable())
			{
				mGame.PlaySe("SE_PUSH", -1);
			}
			else if (mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot2) == 0)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.STAGEINFO;
				BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BuyBoosuterDialog", base.transform.parent.gameObject).AddComponent<BuyBoosterDialog>();
				buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.PRE_STAGE;
				buyBoosterDialog.SetBaseDepth(mBaseDepth + 10);
				buyBoosterDialog.Init(mGame.mShopItemData[(int)mGame.mCurrentStage.BoosterSlot2]);
				buyBoosterDialog.SetClosedCallback(OnBuyBoosterClosed);
				buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else if (!mGame.mUseBooster1)
			{
				mGame.mUseBooster1 = true;
				mUIBoosterCheck1.enabled = true;
				mGame.PlaySe("SE_CHECK_BOOSTER_ON", -1);
			}
			else
			{
				mGame.mUseBooster1 = false;
				mUIBoosterCheck1.enabled = false;
				mGame.PlaySe("SE_CHECK_BOOSTER_OFF", -1);
			}
		}
	}

	public void OnBooster2Pushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (!go.GetComponent<JellyImageButton>().IsEnable())
			{
				mGame.PlaySe("SE_PUSH", -1);
			}
			else if (mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot3) == 0)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.STAGEINFO;
				BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BuyBoosuterDialog", base.transform.parent.gameObject).AddComponent<BuyBoosterDialog>();
				buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.PRE_STAGE;
				buyBoosterDialog.SetBaseDepth(mBaseDepth + 10);
				buyBoosterDialog.Init(mGame.mShopItemData[(int)mGame.mCurrentStage.BoosterSlot3]);
				buyBoosterDialog.SetClosedCallback(OnBuyBoosterClosed);
				buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else if (!mGame.mUseBooster2)
			{
				mGame.mUseBooster2 = true;
				mUIBoosterCheck2.enabled = true;
				mGame.PlaySe("SE_CHECK_BOOSTER_ON", -1);
			}
			else
			{
				mGame.mUseBooster2 = false;
				mUIBoosterCheck2.enabled = false;
				mGame.PlaySe("SE_CHECK_BOOSTER_OFF", -1);
			}
		}
	}

	public void OnBooster3Pushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (!go.GetComponent<JellyImageButton>().IsEnable())
			{
				mGame.PlaySe("SE_PUSH", -1);
			}
			else if (mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot4) == 0)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.STAGEINFO;
				BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BuyBoosuterDialog", base.transform.parent.gameObject).AddComponent<BuyBoosterDialog>();
				buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.PRE_STAGE;
				buyBoosterDialog.SetBaseDepth(mBaseDepth + 10);
				buyBoosterDialog.Init(mGame.mShopItemData[(int)mGame.mCurrentStage.BoosterSlot4]);
				buyBoosterDialog.SetClosedCallback(OnBuyBoosterClosed);
				buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else if (!mGame.mUseBooster3)
			{
				mGame.mUseBooster3 = true;
				mUIBoosterCheck3.enabled = true;
				mGame.PlaySe("SE_CHECK_BOOSTER_ON", -1);
			}
			else
			{
				mGame.mUseBooster3 = false;
				mUIBoosterCheck3.enabled = false;
				mGame.PlaySe("SE_CHECK_BOOSTER_OFF", -1);
			}
		}
	}

	public void OnBooster4Pushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		if (!go.GetComponent<JellyImageButton>().IsEnable())
		{
			mGame.PlaySe("SE_PUSH", -1);
		}
		else if (mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot5) == 0)
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.STAGEINFO;
			BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BuyBoosuterDialog", base.transform.parent.gameObject).AddComponent<BuyBoosterDialog>();
			buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.PRE_STAGE;
			buyBoosterDialog.SetBaseDepth(mBaseDepth + 10);
			buyBoosterDialog.Init(mGame.mShopItemData[(int)mGame.mCurrentStage.BoosterSlot5]);
			buyBoosterDialog.SetClosedCallback(OnBuyBoosterClosed);
			buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			mObjeSkillCharge = go;
			if (!mGame.mUseBooster4)
			{
				mGame.mUseBooster4 = true;
				mUIBoosterCheck4.enabled = true;
				mGame.PlaySe("SE_CHECK_BOOSTER_ON", -1);
			}
			else
			{
				mGame.mUseBooster4 = false;
				mUIBoosterCheck4.enabled = false;
				mGame.PlaySe("SE_CHECK_BOOSTER_OFF", -1);
			}
		}
	}

	public void OnBooster5Pushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (!go.GetComponent<JellyImageButton>().IsEnable())
			{
				mGame.PlaySe("SE_PUSH", -1);
			}
			else if (mGame.mPlayer.GetBoosterNum(mGame.mCurrentStage.BoosterSlot6) == 0)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.STAGEINFO;
				BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BuyBoosuterDialog", base.transform.parent.gameObject).AddComponent<BuyBoosterDialog>();
				buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.PRE_STAGE;
				buyBoosterDialog.SetBaseDepth(mBaseDepth + 10);
				buyBoosterDialog.Init(mGame.mShopItemData[(int)mGame.mCurrentStage.BoosterSlot6]);
				buyBoosterDialog.SetClosedCallback(OnBuyBoosterClosed);
				buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else if (!mGame.mUseBooster5)
			{
				mGame.mUseBooster5 = true;
				mUIBoosterCheck5.enabled = true;
				mGame.PlaySe("SE_CHECK_BOOSTER_ON", -1);
			}
			else
			{
				mGame.mUseBooster5 = false;
				mUIBoosterCheck5.enabled = false;
				mGame.PlaySe("SE_CHECK_BOOSTER_OFF", -1);
			}
		}
	}

	public void OnSaleIconPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		if (!go.GetComponent<JellyImageButton>().IsEnable())
		{
			mGame.PlaySe("SE_PUSH", -1);
			return;
		}
		GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.STAGEINFO;
		BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BuyBoosterDialog", base.transform.parent.gameObject).AddComponent<BuyBoosterDialog>();
		buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.PRE_STAGE;
		buyBoosterDialog.SetBaseDepth(mBaseDepth + 10);
		buyBoosterDialog.Init(mSaleItem, false, false, mSaleShop.ShopCount);
		buyBoosterDialog.SetClosedCallback(delegate(ShopItemData data, BuyBoosterDialog.SELECT_ITEM i)
		{
			mState.Reset(STATE.MAIN, true);
			if (i == BuyBoosterDialog.SELECT_ITEM.BUY_COMPLETE && !mGame.mPlayer.IsEnablePurchaseItem(mSaleShop.ShopItemID, mSaleShop.ShopItemLimit, mSaleShop.ShopCount) && mSaleInnerFrame.gameObject.activeSelf)
			{
				mSaleInnerFrame.gameObject.SetActive(false);
			}
		});
		buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		mState.Reset(STATE.WAIT, true);
	}

	public void OnBuyBoosterClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		mState.Reset(STATE.MAIN, true);
		if (item == BuyBoosterDialog.SELECT_ITEM.BUY_COMPLETE)
		{
			switch (ItemData.BoosterID)
			{
			case 1:
				OnBooster0Pushed(GameObject.Find("ButtonBooster0"));
				break;
			case 2:
				OnBooster1Pushed(GameObject.Find("ButtonBooster1"));
				break;
			case 3:
				OnBooster2Pushed(GameObject.Find("ButtonBooster2"));
				break;
			case 4:
				OnBooster3Pushed(GameObject.Find("ButtonBooster3"));
				break;
			case 5:
				OnBooster4Pushed(GameObject.Find("ButtonBooster4"));
				break;
			case 6:
				OnBooster5Pushed(GameObject.Find("ButtonBooster5"));
				break;
			}
		}
	}

	public void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void SetDailyEvent(SMDailyEventSetting settings, List<int> boxIds)
	{
		mDailyEvent = settings;
		mDailyEventBoxID = boxIds;
		if (GameMain.USE_DEBUG_DIALOG)
		{
			SetDebugInfo(settings);
		}
	}

	private IEnumerator UpdateSaleBound(UISprite sale_now, float delay = 5f)
	{
		if (!sale_now)
		{
			yield break;
		}
		Transform trans = sale_now.transform;
		float counter = 0f;
		Vector3 defaultPos = trans.localPosition;
		yield return new WaitForSeconds(delay);
		while ((bool)trans)
		{
			counter += Time.deltaTime * 540f;
			if (counter > 360f)
			{
				counter = 0f;
				trans.SetLocalPosition(defaultPos);
				yield return new WaitForSeconds(3f);
			}
			trans.SetLocalPositionY(defaultPos.y + Mathf.Abs(Mathf.Sin(counter * ((float)Math.PI / 180f))) * 25f);
			yield return null;
		}
	}

	public virtual void SetDebugInfo(SMDailyEventSetting a_setting)
	{
		if (a_setting == null)
		{
			return;
		}
		Debug_DEIndex = a_setting.DailyEventNo;
		Debug_DEAccessories = new int[a_setting.BoxID.Count];
		Debug_AccessoryType = new AccessoryData.ACCESSORY_TYPE[a_setting.BoxID.Count];
		Debug_AccessoryNum = new string[a_setting.BoxID.Count];
		for (int i = 0; i < Debug_DEAccessories.Length; i++)
		{
			Debug_DEAccessories[i] = a_setting.BoxID[i];
			AccessoryData accessoryData = mGame.GetAccessoryData(Debug_DEAccessories[i]);
			if (accessoryData != null)
			{
				Debug_AccessoryType[i] = accessoryData.AccessoryType;
				Debug_AccessoryNum[i] = accessoryData.GotID;
			}
		}
	}

	public virtual void SetDebugInfo(SMMapStageSetting a_setting)
	{
		if (a_setting == null)
		{
			return;
		}
		Debug_StageAccessories = new int[a_setting.BoxID.Count];
		Debug_StageAccessoryType = new AccessoryData.ACCESSORY_TYPE[a_setting.BoxID.Count];
		Debug_StageAccessoryNum = new string[a_setting.BoxID.Count];
		Debug_StageAccessoryHas = new bool[a_setting.BoxID.Count];
		for (int i = 0; i < Debug_StageAccessories.Length; i++)
		{
			Debug_StageAccessories[i] = a_setting.BoxID[i];
			AccessoryData accessoryData = mGame.GetAccessoryData(Debug_StageAccessories[i]);
			if (accessoryData != null)
			{
				Debug_StageAccessoryType[i] = accessoryData.AccessoryType;
				Debug_StageAccessoryNum[i] = accessoryData.GotID;
				Debug_StageAccessoryHas[i] = mGame.mPlayer.IsAccessoryUnlock(Debug_StageAccessories[i]);
			}
		}
	}
}
