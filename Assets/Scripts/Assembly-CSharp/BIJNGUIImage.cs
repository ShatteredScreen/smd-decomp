using UnityEngine;

public class BIJNGUIImage : BIJImage
{
	private UIAtlas mAtlas;

	public UIAtlas Atlas
	{
		get
		{
			return mAtlas;
		}
		set
		{
			if (mAtlas != null)
			{
				mAtlas.spriteMaterial = null;
			}
			mAtlas = value;
		}
	}

	public override Texture Texture
	{
		get
		{
			if (Atlas != null)
			{
				return Atlas.texture;
			}
			return null;
		}
	}

	public override int Count
	{
		get
		{
			if (Atlas != null)
			{
				return Atlas.spriteList.Count;
			}
			return 0;
		}
	}

	public void Awake()
	{
	}

	public void Start()
	{
	}

	public void OnDestroy()
	{
		if (mAtlas != null)
		{
			mAtlas.spriteMaterial = null;
		}
	}

	public override bool Contains(int index, string name)
	{
		if (Atlas == null)
		{
			return false;
		}
		if (name == null)
		{
			name = GetName(index);
		}
		return Atlas.GetListOfSprites().Contains(name);
	}

	public override string GetName(int index, string name)
	{
		if (Atlas == null)
		{
			return string.Empty;
		}
		return string.Empty + index;
	}

	public override Vector2 GetOffset(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null && Texture != null)
		{
			float x = (float)(uISpriteData.x + uISpriteData.paddingLeft) / (float)Texture.width;
			float y = 1f - (float)(uISpriteData.y + uISpriteData.paddingTop + uISpriteData.height - uISpriteData.paddingTop - uISpriteData.paddingBottom) / (float)Texture.height;
			return new Vector2(x, y);
		}
		return new Vector2(0f, 0f);
	}

	public override Vector2 GetSize(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null && Texture != null)
		{
			float x = (float)(uISpriteData.width - uISpriteData.paddingLeft - uISpriteData.paddingRight) / (float)Texture.width;
			float y = (float)(uISpriteData.height - uISpriteData.paddingTop - uISpriteData.paddingBottom) / (float)Texture.height;
			return new Vector2(x, y);
		}
		return new Vector2(1f, 1f);
	}

	public override Vector2 GetPixelOffset(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null)
		{
			return new Vector2((float)uISpriteData.x * Atlas.pixelSize, (float)uISpriteData.y * Atlas.pixelSize);
		}
		return new Vector2(0f, 0f);
	}

	public override Vector2 GetPixelSize(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null)
		{
			return new Vector2((float)uISpriteData.width * Atlas.pixelSize, (float)uISpriteData.height * Atlas.pixelSize);
		}
		if (Texture != null)
		{
			return new Vector2(Texture.width, Texture.height);
		}
		return new Vector2(0f, 0f);
	}

	public override Vector4 GetPixelPadding(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null)
		{
			return new Vector4((float)(uISpriteData.paddingLeft / uISpriteData.width) * Atlas.pixelSize, (float)(uISpriteData.paddingTop / uISpriteData.height) * Atlas.pixelSize, (float)(uISpriteData.paddingRight / uISpriteData.width) * Atlas.pixelSize, (float)(uISpriteData.paddingBottom / uISpriteData.height) * Atlas.pixelSize);
		}
		return new Vector4(0f, 0f, 0f, 0f);
	}

	public override Vector4 GetPadding(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null)
		{
			return new Vector4(uISpriteData.paddingLeft, uISpriteData.paddingTop, uISpriteData.paddingRight, uISpriteData.paddingBottom);
		}
		return new Vector4(0f, 0f, 0f, 0f);
	}

	public override Vector4 GetPixelBorder(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null)
		{
			return new Vector4((float)uISpriteData.borderLeft * Atlas.pixelSize, (float)uISpriteData.borderTop * Atlas.pixelSize, (float)uISpriteData.borderRight * Atlas.pixelSize, (float)uISpriteData.borderBottom * Atlas.pixelSize);
		}
		return new Vector4(0f, 0f, 0f, 0f);
	}

	public override Vector4 GetBorder(int index, string name)
	{
		if (name == null)
		{
			name = GetName(index);
		}
		UISpriteData uISpriteData = ((!(Atlas == null)) ? Atlas.GetSprite(name) : null);
		if (uISpriteData != null)
		{
			return new Vector4((float)uISpriteData.borderLeft / (float)Texture.width, (float)uISpriteData.borderTop / (float)Texture.height, (float)uISpriteData.borderRight / (float)Texture.width, (float)uISpriteData.borderBottom / (float)Texture.height);
		}
		return new Vector4(0f, 0f, 0f, 0f);
	}
}
