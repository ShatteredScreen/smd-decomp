using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankUpDialog : DialogBase
{
	public enum MODE
	{
		STAGE = 0,
		TROPHY = 1,
		STAR = 2,
		SCORE = 3
	}

	public enum STATE
	{
		EVENT_READY = 0,
		EVENT = 1,
		MAIN = 2,
		WAIT = 3
	}

	public delegate void OnDialogClosed();

	private MODE mMode;

	private int mStageNumber;

	private RankUpItem mMyselfItem;

	private RankUpItem mFriendItem;

	private RankUpEventObj mMyselfObj;

	private RankUpEventObj mFriendObj;

	private UISprite mSpriteA;

	private UISprite mSpriteB;

	private UISprite mSpriteC;

	private UISprite mSpriteD;

	private UISprite mSpriteE;

	private UISprite mSpriteF;

	private UISprite mSpriteG;

	private UIButton mSkipbutton;

	private bool mSkipflg;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.EVENT_READY);

	private float mTimer;

	private OnDialogClosed mCloseDialogCallback;

	private static Vector3 UPPER_LEVEL_POS = new Vector3(0f, 68f, 0f);

	private static Vector3 LOW_LEVEL_POS = new Vector3(0f, -82f, 0f);

	private static Vector3 CROSS_POS = new Vector3(0f, UPPER_LEVEL_POS.y + (LOW_LEVEL_POS.y - UPPER_LEVEL_POS.y) / 2f, 0f);

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.EVENT_READY:
			if (!mJelly.IsMoveFinished())
			{
				return;
			}
			StartCoroutine(RankUpEvent());
			mState.Change(STATE.EVENT);
			break;
		case STATE.EVENT:
			mTimer += Time.deltaTime;
			if (!(mTimer < 30f))
			{
				RankUpEffect();
				mState.Change(STATE.MAIN);
			}
			break;
		}
		mState.Update();
	}

	public void Init()
	{
		mMyselfItem = new RankUpItem();
		mMyselfItem.Icon = mGame.mPlayer.Icon;
		mMyselfItem.Gender = "male";
		mMyselfItem.Name = "MYSELF";
		mMyselfItem.RankOld = 10;
		mMyselfItem.RankNew = 1;
		mMyselfItem.DisplayValue = 1;
		mMyselfItem.Mode = MODE.STAGE;
		mFriendItem = new RankUpItem();
		mFriendItem.Icon = null;
		mFriendItem.Gender = "female";
		mFriendItem.Name = "FRIEND";
		mFriendItem.RankOld = 10;
		mFriendItem.RankNew = 18;
		mFriendItem.DisplayValue = 1;
		mFriendItem.Mode = MODE.STAGE;
	}

	public void Init(int newRank, int oldRank, int displayStageNumber, int ownScore, StageRankingData targetData)
	{
		mStageNumber = displayStageNumber;
		MyFriend value;
		if (mGame.mPlayer.Friends.TryGetValue(targetData.uuid, out value))
		{
			_Init(newRank, oldRank, ownScore, mGame.mPlayer.Friends[targetData.uuid], targetData, MODE.SCORE);
		}
	}

	public void Init(int newRank, int oldRank, MyFriend targetFriend, MODE mode)
	{
		switch (mode)
		{
		case MODE.STAGE:
			_Init(newRank, oldRank, mGame.mPlayer.LevelToS, targetFriend, null, mode);
			break;
		case MODE.TROPHY:
			_Init(newRank, oldRank, (int)mGame.mPlayer.TotalTrophy, targetFriend, null, mode);
			break;
		default:
			_Init(newRank, oldRank, mGame.mPlayer.TotalStars, targetFriend, null, mode);
			break;
		}
	}

	public void _Init(int newRank, int oldRank, int dislpayValue, MyFriend targetFriend, StageRankingData targetData, MODE mode)
	{
		mMode = mode;
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		bool flag = GameMain.IsSNSLogined(sns);
		Player mPlayer = mGame.mPlayer;
		mMyselfItem = new RankUpItem();
		if (mGame.mPlayer.Icon != null)
		{
			mMyselfItem.Icon = mGame.mPlayer.Icon;
		}
		mMyselfItem.FbID = mGame.mOptions.Fbid;
		mMyselfItem.Name = mGame.mPlayer.Data.NickName;
		mMyselfItem.IconID = mGame.mPlayer.Data.PlayerIcon;
		if (mMode == MODE.SCORE)
		{
			mMyselfItem.RankOld = oldRank;
			mMyselfItem.RankNew = newRank;
		}
		else if (mMode == MODE.STAR)
		{
			mMyselfItem.RankOld = oldRank + 1;
			mMyselfItem.RankNew = newRank - 1;
		}
		else
		{
			mMyselfItem.RankOld = oldRank + 1;
			mMyselfItem.RankNew = newRank - 1;
		}
		mMyselfItem.Mode = mMode;
		mMyselfItem.DisplayValue = dislpayValue;
		mMyselfItem.Star = mGame.mPlayer.TotalStars;
		mMyselfItem.Trophy = mGame.mPlayer.TotalTrophy;
		List<PlayStageParam> playStage = mGame.Player.PlayStage;
		int stage = 1;
		for (int i = 0; i < playStage.Count; i++)
		{
			if (playStage[i].Series == (int)mGame.mPlayer.Data.CurrentSeries)
			{
				stage = playStage[i].Level;
				stage /= 100;
				break;
			}
		}
		mMyselfItem.Stage = stage;
		int num = mGame.mPlayer.Data.PlayerIcon;
		if (num >= 10000)
		{
			num -= 10000;
		}
		mMyselfItem.MyLevel = mGame.mPlayer.GetCompanionLevel(num);
		mFriendItem = new RankUpItem();
		if (targetFriend.Icon != null)
		{
			mFriendItem.Icon = targetFriend.Icon;
		}
		mFriendItem.IconID = targetFriend.Data.IconID;
		mFriendItem.Gender = targetFriend.Gender;
		mFriendItem.Name = targetFriend.Data.Name;
		if (mMode == MODE.SCORE)
		{
			mFriendItem.RankOld = newRank;
			mFriendItem.RankNew = newRank + 1;
		}
		else if (mMode == MODE.STAR)
		{
			mFriendItem.RankOld = newRank - 1;
			mFriendItem.RankNew = newRank;
		}
		else
		{
			mFriendItem.RankOld = newRank - 1;
			mFriendItem.RankNew = newRank;
		}
		mFriendItem.Mode = mMode;
		mFriendItem.Star = targetFriend.Data.TotalStars;
		mFriendItem.Trophy = targetFriend.Data.TotalTrophy;
		mFriendItem.StageIcon = targetFriend.Data.Series;
		stage = 1;
		playStage = targetFriend.Data.PlayStage;
		stage = ((targetFriend.Data.PlayStage.Count != 0) ? mGame.HavePlayStageLevel(targetFriend.Data.PlayStage) : mGame.NoHavePlayStageLevel(targetFriend.Data.Level));
		mFriendItem.MyLevel = targetFriend.Data.IconLevel;
		mFriendItem.Stage = stage;
		mFriendItem.FbID = targetFriend.Data.Fbid;
		switch (mode)
		{
		case MODE.STAGE:
			mFriendItem.DisplayValue = stage;
			break;
		case MODE.STAR:
			mFriendItem.DisplayValue = (int)targetFriend.Data.TotalStars;
			break;
		case MODE.TROPHY:
			mFriendItem.DisplayValue = (int)targetFriend.Data.TotalTrophy;
			break;
		case MODE.SCORE:
			mFriendItem.DisplayValue = targetData.clr_score;
			break;
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		base.BuildDialog();
		string titleDescKey = string.Empty;
		UIFont uIFont = GameMain.LoadFont();
		switch (mMode)
		{
		case MODE.STAGE:
			titleDescKey = Localization.Get("Ranking_StageTitle");
			break;
		case MODE.TROPHY:
			titleDescKey = Localization.Get("Ranking_TrophyTitle");
			break;
		case MODE.STAR:
			titleDescKey = Localization.Get("Ranking_StarTitle");
			break;
		case MODE.SCORE:
			titleDescKey = Localization.Get("Ranking_ScoreTitle");
			break;
		}
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		mSkipbutton = Util.CreateJellyImageButton("NullSkip", base.gameObject, image);
		Util.SetImageButtonInfo(mSkipbutton, "null", "null", "null", mBaseDepth + 100, new Vector3(0f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSkipbutton, this, "OnSkipPushed", UIButtonMessage.Trigger.OnClick);
		BoxCollider component = mSkipbutton.GetComponent<BoxCollider>();
		component.size = new Vector3(560f, 560f, 1f);
	}

	public void OnSkipPushed()
	{
		mTimer = 31f;
		if (mSkipbutton != null)
		{
			UnityEngine.Object.Destroy(mSkipbutton.gameObject);
			mSkipbutton = null;
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCloseDialogCallback = callback;
	}

	public override void OnCloseFinished()
	{
		if (mCloseDialogCallback != null)
		{
			mCloseDialogCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			PlaySE("SE_NEGATIVE");
			Close();
		}
	}

	private void PlaySE(string key, Res.SE_CHANNEL cannel = Res.SE_CHANNEL.AUTO, float vol = 1f)
	{
		mGame.PlaySe(key, (int)cannel, vol);
	}

	private void RankUpEffect()
	{
		UIFont font = GameMain.LoadFont();
		mSkipflg = true;
		CreateButton();
		if (mFriendObj == null)
		{
			mFriendObj = new RankUpEventObj(base.gameObject, font, "Friend", mBaseDepth + 5, UPPER_LEVEL_POS, mFriendItem, false);
			StartCoroutine(MoveEventArrow(mFriendObj, true));
		}
		if (mMyselfObj == null)
		{
			mMyselfObj = new RankUpEventObj(base.gameObject, font, "Myself", mBaseDepth + 8, LOW_LEVEL_POS, mMyselfItem, true);
			StartCoroutine(MoveEventArrow(mMyselfObj, false));
		}
		mFriendObj.ChangeCurrent();
		mMyselfObj.ChangeCurrent();
		mMyselfObj.Base.transform.localPosition = new Vector3(0f, 68f, 0f);
		mFriendObj.Base.transform.localPosition = new Vector3(0f, -82f, 0f);
		float num = -180f;
		float num2 = 60f;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		if (mSpriteA == null)
		{
			mSpriteA = Util.CreateSprite("RankUpLight", base.gameObject, image);
			Util.SetSpriteInfo(mSpriteA, "rankup00", mBaseDepth + 1, new Vector3(num, 180f, 0f), Vector3.one, false);
			StartCoroutine(MoveRankup(mSpriteA));
		}
		num += num2;
		if (mSpriteB == null)
		{
			mSpriteB = Util.CreateSprite("RankUpLight", base.gameObject, image);
			Util.SetSpriteInfo(mSpriteB, "rankup01", mBaseDepth + 1, new Vector3(num, 180f, 0f), Vector3.one, false);
			StartCoroutine(MoveRankup(mSpriteB));
		}
		num += num2;
		if (mSpriteC == null)
		{
			mSpriteC = Util.CreateSprite("RankUpLight", base.gameObject, image);
			Util.SetSpriteInfo(mSpriteC, "rankup02", mBaseDepth + 1, new Vector3(num, 180f, 0f), Vector3.one, false);
			StartCoroutine(MoveRankup(mSpriteC));
		}
		num += num2;
		if (mSpriteD == null)
		{
			mSpriteD = Util.CreateSprite("RankUpLight", base.gameObject, image);
			Util.SetSpriteInfo(mSpriteD, "rankup03", mBaseDepth + 1, new Vector3(num, 180f, 0f), Vector3.one, false);
			StartCoroutine(MoveRankup(mSpriteD));
		}
		num += num2 + 15f;
		if (mSpriteE == null)
		{
			mSpriteE = Util.CreateSprite("RankUpLight", base.gameObject, image);
			Util.SetSpriteInfo(mSpriteE, "rankup04", mBaseDepth + 1, new Vector3(num, 180f, 0f), Vector3.one, false);
			StartCoroutine(MoveRankup(mSpriteE));
		}
		num += num2;
		if (mSpriteF == null)
		{
			mSpriteF = Util.CreateSprite("RankUpLight", base.gameObject, image);
			Util.SetSpriteInfo(mSpriteF, "rankup05", mBaseDepth + 1, new Vector3(num, 180f, 0f), Vector3.one, false);
			StartCoroutine(MoveRankup(mSpriteF));
		}
		num += num2;
		if (mSpriteG == null)
		{
			mSpriteG = Util.CreateSprite("RankUpLight", base.gameObject, image);
			Util.SetSpriteInfo(mSpriteG, "rankup06", mBaseDepth + 1, new Vector3(num, 180f, 0f), Vector3.one, false);
			StartCoroutine(MoveRankup(mSpriteG));
		}
		mFriendObj.newArrow();
		mMyselfObj.newArrow(false);
	}

	private IEnumerator RankUpEvent()
	{
		UIFont font = GameMain.LoadFont();
		yield return new WaitForSeconds(0.2f);
		float a2 = -180f;
		float b = 60f;
		BIJImage hudImage = ResourceManager.LoadImage("HUD").Image;
		if (mSpriteA == null)
		{
			mSpriteA = Util.CreateSprite("RankUpLight", base.gameObject, hudImage);
			Util.SetSpriteInfo(mSpriteA, "rankup00", mBaseDepth + 1, new Vector3(a2, 180f, 0f), Vector3.one, false);
			a2 += b;
			StartCoroutine(AppearEvent(mSpriteA, 0.2f));
			StartCoroutine(MoveRankup(mSpriteA));
			yield return new WaitForSeconds(0.02f);
		}
		if (mSpriteB == null)
		{
			mSpriteB = Util.CreateSprite("RankUpLight", base.gameObject, hudImage);
			Util.SetSpriteInfo(mSpriteB, "rankup01", mBaseDepth + 1, new Vector3(a2, 180f, 0f), Vector3.one, false);
			a2 += b;
			StartCoroutine(AppearEvent(mSpriteB, 0.2f));
			StartCoroutine(MoveRankup(mSpriteB));
			yield return new WaitForSeconds(0.02f);
		}
		if (mSpriteC == null)
		{
			mSpriteC = Util.CreateSprite("RankUpLight", base.gameObject, hudImage);
			Util.SetSpriteInfo(mSpriteC, "rankup02", mBaseDepth + 1, new Vector3(a2, 180f, 0f), Vector3.one, false);
			a2 += b;
			mGame.PlaySe("SE_RANKUP2", -1);
			StartCoroutine(AppearEvent(mSpriteC, 0.2f));
			StartCoroutine(MoveRankup(mSpriteC));
			yield return new WaitForSeconds(0.02f);
		}
		if (mSpriteD == null)
		{
			mSpriteD = Util.CreateSprite("RankUpLight", base.gameObject, hudImage);
			Util.SetSpriteInfo(mSpriteD, "rankup03", mBaseDepth + 1, new Vector3(a2, 180f, 0f), Vector3.one, false);
			a2 += b + 15f;
			StartCoroutine(AppearEvent(mSpriteD, 0.2f));
			StartCoroutine(MoveRankup(mSpriteD));
			yield return new WaitForSeconds(0.02f);
		}
		if (mSpriteE == null)
		{
			mSpriteE = Util.CreateSprite("RankUpLight", base.gameObject, hudImage);
			Util.SetSpriteInfo(mSpriteE, "rankup04", mBaseDepth + 1, new Vector3(a2, 180f, 0f), Vector3.one, false);
			a2 += b;
			StartCoroutine(AppearEvent(mSpriteE, 0.2f));
			StartCoroutine(MoveRankup(mSpriteE));
			yield return new WaitForSeconds(0.02f);
		}
		if (mSpriteF == null)
		{
			mSpriteF = Util.CreateSprite("RankUpLight", base.gameObject, hudImage);
			Util.SetSpriteInfo(mSpriteF, "rankup05", mBaseDepth + 1, new Vector3(a2, 180f, 0f), Vector3.one, false);
			a2 += b;
			StartCoroutine(AppearEvent(mSpriteF, 0.2f));
			StartCoroutine(MoveRankup(mSpriteF));
			yield return new WaitForSeconds(0.02f);
		}
		if (mSpriteG == null)
		{
			mSpriteG = Util.CreateSprite("RankUpLight", base.gameObject, hudImage);
			Util.SetSpriteInfo(mSpriteG, "rankup06", mBaseDepth + 1, new Vector3(a2, 180f, 0f), Vector3.one, false);
			a2 += b;
			StartCoroutine(AppearEvent(mSpriteG, 0.2f));
			StartCoroutine(MoveRankup(mSpriteG));
			yield return new WaitForSeconds(0.1f);
		}
		if (mFriendObj == null)
		{
			mFriendObj = new RankUpEventObj(base.gameObject, font, "Friend", mBaseDepth + 5, UPPER_LEVEL_POS, mFriendItem, false);
			StartCoroutine(AppearEvent(mFriendObj.Base, 0.3f));
			StartCoroutine(MoveEventArrow(mFriendObj, true));
			mGame.PlaySe("SE_RANKUP1", -1, 1f, 0.3f);
			yield return new WaitForSeconds(0.3f);
		}
		if (mMyselfObj == null)
		{
			mMyselfObj = new RankUpEventObj(base.gameObject, font, "Myself", mBaseDepth + 8, LOW_LEVEL_POS, mMyselfItem, true);
			StartCoroutine(AppearEvent(mMyselfObj.Base, 0.3f));
			StartCoroutine(MoveEventArrow(mMyselfObj, false));
			mGame.PlaySe("SE_RANKUP1", -1, 1f, 0.3f);
			yield return new WaitForSeconds(0.5f);
		}
		if (!mSkipflg)
		{
			mGame.PlaySe("SE_RANKUP3", -1);
			StartCoroutine(MoveEvent(mFriendObj.Base, UPPER_LEVEL_POS, CROSS_POS, 0.15f));
			StartCoroutine(MoveEvent(mMyselfObj.Base, LOW_LEVEL_POS, CROSS_POS, 0.15f));
			yield return new WaitForSeconds(0.05f);
			mFriendObj.ChangeCurrent();
			mMyselfObj.ChangeCurrent();
			StartCoroutine(MoveEvent(mFriendObj.Base, CROSS_POS, LOW_LEVEL_POS, 0.15f));
			StartCoroutine(MoveEvent(mMyselfObj.Base, CROSS_POS, UPPER_LEVEL_POS, 0.15f));
			yield return new WaitForSeconds(0.4f);
		}
		if (!mSkipflg)
		{
			mFriendObj.newArrow();
			mMyselfObj.newArrow(false);
		}
		if (mSkipbutton != null)
		{
			UnityEngine.Object.Destroy(mSkipbutton.gameObject);
			mSkipbutton = null;
		}
		if (!mSkipflg)
		{
			CreateButton();
		}
		mState.Reset(STATE.MAIN, true);
	}

	private IEnumerator AppearEvent(UISprite sprite, float time)
	{
		for (float timer = 0f; timer < time; timer += Time.deltaTime)
		{
			float scale_angle = 105f + 180f * timer;
			if (scale_angle > 180f)
			{
				scale_angle = 180f;
			}
			sprite.transform.localScale = Vector3.one * (1f + 1.2f * Mathf.Sin(scale_angle * ((float)Math.PI / 180f)));
			float alpha_angle = 180f * timer;
			if (alpha_angle > 90f)
			{
				alpha_angle = 90f;
			}
			sprite.color = new Color(1f, 1f, 1f, Mathf.Sin(alpha_angle * ((float)Math.PI / 180f)));
			yield return null;
		}
		sprite.transform.localScale = Vector3.one;
		sprite.color = Color.white;
	}

	private IEnumerator MoveEvent(UISprite sprite, Vector3 start_pos, Vector3 end_pos, float time)
	{
		float timer = 0f;
		Vector3 move_vec = end_pos - start_pos;
		for (; timer < time; timer += Time.deltaTime)
		{
			sprite.transform.localPosition = start_pos + move_vec * (timer / time);
			yield return null;
		}
		sprite.transform.localPosition = end_pos;
	}

	private IEnumerator MoveEventArrow(RankUpEventObj eventObj, bool flip)
	{
		while (eventObj.Arrow == null)
		{
			yield return 0;
		}
		Vector3 pos = eventObj.Arrow.gameObject.transform.localPosition;
		float angle = 0f;
		while (true)
		{
			angle += Time.deltaTime * 180f;
			if (angle > 180f)
			{
				angle -= 180f;
			}
			float y = Mathf.Sin(angle * ((float)Math.PI / 180f)) * 20f;
			if (flip)
			{
				y *= -1f;
			}
			eventObj.Arrow.gameObject.transform.localPosition = new Vector3(pos.x, pos.y + y, pos.z);
			yield return 0;
		}
	}

	private IEnumerator MoveRankup(UISprite RankUpDesc)
	{
		float counter = 0f;
		float wait = 500f;
		float basePosY = RankUpDesc.transform.localPosition.y;
		while (true)
		{
			if (wait > 0f)
			{
				wait -= Time.deltaTime * 360f;
				yield return null;
			}
			if (RankUpDesc == null)
			{
				break;
			}
			if (wait < 0f)
			{
				counter += Time.deltaTime * 360f;
				if (counter > 720f)
				{
					counter -= 720f;
				}
				Vector3 pos = RankUpDesc.transform.localPosition;
				if (counter < 180f)
				{
					pos.y = basePosY + Mathf.Sin(counter * ((float)Math.PI / 180f)) * 30f;
				}
				else
				{
					pos.y = basePosY;
				}
				RankUpDesc.transform.localPosition = pos;
			}
			yield return null;
		}
	}

	private void CreateButton()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIButton button = Util.CreateJellyImageButton("ButtonCancel", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 1, new Vector3(0f, -212f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}
}
