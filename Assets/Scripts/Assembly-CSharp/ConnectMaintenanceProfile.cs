using System;

public class ConnectMaintenanceProfile : ConnectBase
{
	protected override void InitServerFlg()
	{
		GameMain.mMaintenanceProfileCheckDone = false;
		GameMain.mMaintenanceProfileSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mMaintenanceProfileCheckDone = true;
		GameMain.mMaintenanceProfileSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mMaintenanceProfileCheckDone = true;
		GameMain.mMaintenanceProfileSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		if (mPlayer != null)
		{
			if (mGame.Debug_MaintenanceModeFlg && mGame.Debug_MaintenanceModeCount == 1)
			{
				mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			}
			if (DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0) <= mPlayer.Data.LastGetMaintenanceInfoTime)
			{
				mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			}
			long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetMaintenanceInfoTime, DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0));
			long gET_MAINTENANCE_INFO_FREQ = GameMain.GET_MAINTENANCE_INFO_FREQ;
			if (gET_MAINTENANCE_INFO_FREQ > 0 && num < gET_MAINTENANCE_INFO_FREQ)
			{
				if (mGame.Debug_MaintenanceModeFlg && mGame.Debug_MaintenanceModeCount == 0)
				{
					mGame.Debug_MaintenanceModeCount = 1;
				}
				ServerFlgSucceeded();
				return false;
			}
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		GameMain.ResetMaintenance();
		string text = string.Empty;
		string a_datekey = string.Empty;
		if (mParam != null)
		{
			ConnectGetProfileParam connectGetProfileParam = mParam as ConnectGetProfileParam;
			if (connectGetProfileParam != null)
			{
				a_datekey = connectGetProfileParam.DebugDate;
				text = connectGetProfileParam.DebugKey;
			}
		}
		if (GameMain.DEBUG_DEBUGSERVERINFO && string.IsNullOrEmpty(text))
		{
			text = "debug";
			GameMain.GET_MAINTENANCE_INFO_FREQ = 240L;
		}
		InitServerFlg();
		if (!Server.GetMaintenanceInfo(a_datekey, text))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void OnConnectErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		bool flag = true;
		if (i == ConnectCommonErrorDialog.SELECT_ITEM.RETRY)
		{
			flag = true;
		}
		if (flag && mPlayer != null)
		{
			mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		base.OnConnectErrorDialogClosed(i, a_state);
	}

	protected override void StateConnectFinish()
	{
		mGame.SetMaintenanceProfileFromResponse();
		if (GameMain.MaintenanceMode)
		{
			ShowErrorDialog(mMaintenanceDialogStyle);
		}
		else
		{
			base.StateConnectFinish();
		}
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (mPlayer != null)
			{
				mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			}
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
