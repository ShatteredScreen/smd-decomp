public interface IPurchaseInfo
{
	int IAPNo { get; }

	string OrderName { get; }

	string OrderID { get; }

	ITierData TierData { get; }

	int ItemKind { get; }

	int PurchasedTimeStamp { get; }
}
