using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBlockEVStarDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		MAINTENANCE_CHECK_START = 3,
		MAINTENANCE_CHECK_WAIT = 4,
		NETWORK_00 = 5,
		NETWORK_00_1 = 6,
		NETWORK_00_2 = 7,
		NETWORK_01 = 8,
		NETWORK_01_1 = 9,
		NETWORK_01_2 = 10,
		MAINTENANCE_CHECK_START02 = 11,
		MAINTENANCE_CHECK_WAIT02 = 12,
		NETWORK_02 = 13,
		NETWORK_02_1 = 14,
		NETWORK_02_2 = 15,
		AUTHERROR_WAIT = 16
	}

	public enum SELECT_ITEM
	{
		CLEAR = 0,
		UNLOCK_SD = 1,
		UNLOCK_KEY = 2,
		UNLOCK_FRIEND = 3,
		UNLOCK_TICKET = 4,
		CANCEL = 5
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	public STATE StateStatus;

	private STATE mStateAfterNetworkError = STATE.MAIN;

	private List<string> mPreLoadAnimationKeys = new List<string>();

	private SELECT_ITEM mSelectItem;

	private SMEventRoadBlockSetting mSMEventRoadBlockSetting;

	private int mStarNum;

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mSNSCallCount;

	private RoadBlockHelpDialog mRoadBlockHelpDialog;

	private SNSFriendManager mFriendManager;

	private Live2DRender mL2DRender;

	private UITexture mL2DTexture;

	private Live2DInstance mL2DInstLuna;

	private Live2DInstance mL2DInstArtemis;

	private ShopItemData mBoughtItem;

	private ConfirmDialog mErrorDialog;

	private new string mLoadImageAtlas = string.Empty;

	private bool mCreateGemflg;

	private bool mFirstRoadblock;

	public SMRoadBlockSetting CurrentRoadBlockSetting
	{
		get
		{
			return mSMEventRoadBlockSetting;
		}
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mState.Change(STATE.MAIN);
			break;
		case STATE.MAIN:
			if (!mState.IsChanged())
			{
			}
			break;
		case STATE.MAINTENANCE_CHECK_START:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT:
			if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
			{
				mState.Change(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00:
			if (!mGame.Network_UserAuth())
			{
				if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_00);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		case STATE.NETWORK_00_1:
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00_2);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
			}
			else
			{
				ShowNetworkErrorDialog(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00_2:
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			if (!mGame.Network_GetGemCount())
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.INIT);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_01_1);
			}
			break;
		case STATE.NETWORK_01_1:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_01_2);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			break;
		case STATE.NETWORK_01_2:
			mState.Change(STATE.INIT);
			break;
		case STATE.MAINTENANCE_CHECK_START02:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.CLOSE);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT02);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT02:
			switch (MaintenanceCheckResult())
			{
			case MAINTENANCE_RESULT.OK:
				mState.Change(STATE.NETWORK_02);
				break;
			case MAINTENANCE_RESULT.ERROR:
			case MAINTENANCE_RESULT.IN_MAINTENANCE:
				if (!GetBusy())
				{
					mState.Change(STATE.MAIN);
				}
				break;
			}
			break;
		case STATE.NETWORK_02:
			if (!mGame.Network_BuyItem(mBoughtItem.Index))
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_1);
			}
			break;
		case STATE.NETWORK_02_1:
			if (GameMain.mBuyItemCheckDone)
			{
				if (GameMain.mBuyItemSucceed)
				{
					mState.Change(STATE.NETWORK_02_2);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.MAIN);
				}
			}
			break;
		case STATE.NETWORK_02_2:
		{
			mBoughtItem = null;
			int stageNo = mSMEventRoadBlockSetting.StageNo;
			int unlock_by = 0;
			int cur_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
			int reach_time = 0;
			if (mGame.mPlayer.RoadBlockReachLevel == mSMEventRoadBlockSetting.StageNo)
			{
				reach_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mGame.mPlayer.RoadBlockReachTime);
			}
			int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
			ServerCram.UnlockRoadblock(stageNo, unlock_by, cur_time, reach_time, currentSeries);
			mGame.mOptions.BuyUnlockRBCount++;
			mGame.SaveOptions();
			mSelectItem = SELECT_ITEM.UNLOCK_SD;
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	private void OnEventExplainDialogClosed()
	{
		mGame.Save();
		mState.Change(STATE.MAIN);
	}

	public IEnumerator CloseProcess()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		UnityEngine.Object.Destroy(base.gameObject);
		yield return null;
	}

	public void Init(SMEventRoadBlockSetting data, string atlas, int star, bool firstRoadblock = false)
	{
		mSMEventRoadBlockSetting = data;
		mLoadImageAtlas = atlas;
		mStarNum = star;
		mFirstRoadblock = firstRoadblock;
	}

	public override void BuildDialog()
	{
		if (mLoadImageAtlas == string.Empty)
		{
			mLoadImageAtlas = "EventMap03";
		}
		if (mSMEventRoadBlockSetting == null)
		{
			mSMEventRoadBlockSetting = new SMEventRoadBlockSetting();
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage(mLoadImageAtlas).Image;
		UIFont atlasFont = GameMain.LoadFont();
		float r = 66f / 85f;
		mState.Change(STATE.INIT);
		bool flag = false;
		InitDialogFrame(DIALOG_SIZE.MEDIUM, "DIALOG_BASE", "event_instruction_panel", 0, 400);
		InitDialogTitle(string.Empty, "DIALOG_BASE", "popup_panel_title");
		UISprite sprite = Util.CreateSprite("TitleIcon", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "panel_" + mSMEventRoadBlockSetting.ImageName, mBaseDepth + 30, new Vector3(-140f, 206f, 0f), Vector3.one, false);
		string text = Localization.Get(mSMEventRoadBlockSetting.TitleKey);
		UILabel uILabel = Util.CreateLabel("Title", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 30, new Vector3(36f, 188f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		text = Localization.Get("ORev_RB_Desc00");
		uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(0f, 95f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		float y = 0f;
		UIButton uIButton = Util.CreateJellyImageButton("StarUnlock", base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, "button_roadblock", "button_roadblock", "button_roadblock", mBaseDepth + 3, new Vector3(0f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnStarUnlockPushed", UIButtonMessage.Trigger.OnClick);
		UISprite uISprite = Util.CreateSprite("StarImage", uIButton.gameObject, image);
		Util.SetSpriteInfo(uISprite, "event_star", mBaseDepth + 4, new Vector3(-169f, 0f, 0f), Vector3.one, false);
		uISprite.SetDimensions(54, 54);
		text = Util.MakeLText("ORev_Open00");
		uILabel = Util.CreateLabel("UseRBText", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 5, new Vector3(-45f, 0f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(200, 64);
		UISprite uISprite2 = Util.CreateSprite("LabelFrame", uIButton.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "button02", mBaseDepth + 4, new Vector3(126f, 0f, 0f), Vector3.one, false);
		y = -54f;
		uISprite2.SetDimensions(141, 51);
		uILabel = Util.CreateLabel("StarNum", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty + mStarNum + "/" + mSMEventRoadBlockSetting.UnlockStars, mBaseDepth + 5, new Vector3(0f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		if (mSMEventRoadBlockSetting.UnlockStars > mStarNum)
		{
			uILabel.color = new Color(r, 0.1f, 0.1f);
		}
		else
		{
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		text = Localization.Get("ORev_RB_Desc01");
		uILabel = Util.CreateLabel("Descbot", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(0f, -125f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(1f, 77f / 85f, 0f, 1f);
		uILabel.spacingY = 5;
		if (mSMEventRoadBlockSetting.UnlockSD > 0)
		{
		}
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnPurchaseDialogClosed()
	{
		UILabel component = GameObject.Find("Price").GetComponent<UILabel>();
		if (mSMEventRoadBlockSetting.UnlockSD > mGame.mPlayer.Dollar)
		{
			component.color = new Color(1f, 0.1f, 0.1f);
		}
		else
		{
			component.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		mState.Change(STATE.MAIN);
	}

	private void PurchaseAuthError()
	{
		mState.Change(STATE.AUTHERROR_WAIT);
	}

	public void OnSDUnlockPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (mSMEventRoadBlockSetting.UnlockSD > mGame.mPlayer.Dollar)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				confirmDialog.SetClosedCallback(OnSDConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mBoughtItem = mGame.mShopItemData[mGame.mSegmentProfile.RoadBlockItem];
				GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
				gemUseConfirmDialog.Init(mSMEventRoadBlockSetting.UnlockSD, 11);
				gemUseConfirmDialog.SetClosedCallback(OnGemUseConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
		}
	}

	public void OnGemUseConfirmClosed(GemUseConfirmDialog.SELECT_ITEM item, int aValue)
	{
		if (item == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Change((STATE)aValue);
			return;
		}
		mBoughtItem = null;
		mState.Change(STATE.MAIN);
	}

	public void OnSDConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.RB_MAP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
			purchaseDialog.SetBaseDepth(mBaseDepth + 10);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(PurchaseAuthError);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	public void OnRoadBlockHelpDialogClosed()
	{
		mState.Change(STATE.MAIN);
	}

	public void OnStarUnlockPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (mSMEventRoadBlockSetting.UnlockStars > mStarNum)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("ORev_RB_StarError_Title"), Localization.Get("ORev_RB_StarError_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
				confirmDialog.SetClosedCallback(OnStarConfirmClosed);
				confirmDialog.SetBaseDepth(mBaseDepth + 10);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mSelectItem = SELECT_ITEM.UNLOCK_KEY;
				Close();
			}
		}
	}

	public void OnStarConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mState.Change(STATE.MAIN);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAINTENANCE_CHECK_WAIT))
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void ShowNetworkErrorDialog(STATE aNextState)
	{
		mStateAfterNetworkError = aNextState;
		mState.Reset(STATE.WAIT, true);
		mErrorDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mErrorDialog.SetClosedCallback(OnNetworkErrorDialogClosed);
	}

	private void OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(mStateAfterNetworkError);
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}
}
