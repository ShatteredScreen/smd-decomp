using System.IO;

public class ConnectGetGameData : ConnectBase
{
	private Stream mResStream;

	private int mDataVersion = -1;

	private bool mIsSetResponseData;

	public void SetResponseData(Stream a_stream, int a_version)
	{
		mResStream = a_stream;
		mDataVersion = a_version;
		mIsSetResponseData = true;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (mParam == null)
		{
			BIJLog.E("Must Parameter:" + GetType().Name);
			SetServerResponse(1, -1);
			return;
		}
		ConnectGetGameDataParam connectGetGameDataParam = mParam as ConnectGetGameDataParam;
		if (connectGetGameDataParam == null)
		{
			BIJLog.E("Type Error Parameter:" + GetType().Name + "<=" + mParam.GetType().Name);
			SetServerResponse(1, -1);
			return;
		}
		mGame.SetBackupPath();
		if (!Server.GetServerGameData(connectGetGameDataParam.UseHive, connectGetGameDataParam.HiveID, connectGetGameDataParam.UUID))
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetGetGameDataFromResponse(mResStream, mDataVersion);
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result == 0)
		{
			if (!mIsSetResponseData)
			{
				BIJLog.E("No set response Data:" + GetType().Name);
				base.StateConnectFinish();
			}
		}
		else if (!flag)
		{
			ShowErrorDialog(mErrorDialogStyle);
		}
		return false;
	}
}
