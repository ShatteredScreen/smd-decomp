public class SkillEffectData
{
	public short index;

	public string name;

	public string description;

	public string descriptionLB;

	public string[] descriptionParamArray;

	public string descriptionUp;

	public string soundName;

	public string efcText;

	public string efcA;

	public float efcDelayA;

	public string efcB;

	public float efcDelayB;

	public string efcC;

	public float efcDelayC;

	public string efcD;

	public float efcDelayD;

	public string efcBack;

	public SkillEffectData()
	{
		index = 0;
		name = string.Empty;
		description = string.Empty;
		descriptionLB = string.Empty;
		descriptionParamArray = null;
		descriptionUp = string.Empty;
		soundName = string.Empty;
		efcText = string.Empty;
		efcA = string.Empty;
		efcDelayA = 0f;
		efcB = string.Empty;
		efcDelayB = 0f;
		efcC = string.Empty;
		efcDelayC = 0f;
		efcD = string.Empty;
		efcDelayD = 0f;
		efcBack = string.Empty;
	}

	public void deserialize(BIJBinaryReader stream)
	{
		index = stream.ReadShort();
		name = stream.ReadUTF();
		description = stream.ReadUTF();
		descriptionLB = stream.ReadUTF();
		string text = stream.ReadUTF();
		descriptionUp = stream.ReadUTF();
		soundName = stream.ReadUTF();
		efcText = stream.ReadUTF();
		efcA = stream.ReadUTF();
		efcDelayA = stream.ReadFloat();
		efcB = stream.ReadUTF();
		efcDelayB = stream.ReadFloat();
		efcC = stream.ReadUTF();
		efcDelayC = stream.ReadFloat();
		efcD = stream.ReadUTF();
		efcDelayD = stream.ReadFloat();
		efcBack = stream.ReadUTF();
		if (text == "NONE")
		{
			descriptionParamArray = null;
		}
		else
		{
			descriptionParamArray = text.Split(',');
		}
		if (soundName == "NONE")
		{
			soundName = string.Empty;
		}
		if (efcText == "NONE")
		{
			efcText = string.Empty;
		}
		if (efcA == "NONE")
		{
			efcA = string.Empty;
		}
		if (efcB == "NONE")
		{
			efcB = string.Empty;
		}
		if (efcC == "NONE")
		{
			efcC = string.Empty;
		}
		if (efcD == "NONE")
		{
			efcD = string.Empty;
		}
		if (efcBack == "NONE")
		{
			efcBack = string.Empty;
		}
	}

	public SkillEffectData Clone()
	{
		return (SkillEffectData)MemberwiseClone();
	}

	public void Dump()
	{
	}
}
