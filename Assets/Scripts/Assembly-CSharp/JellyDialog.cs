using System;
using UnityEngine;

public class JellyDialog : MonoBehaviour
{
	private enum STATE
	{
		INIT = 0,
		OPEN = 1,
		CLOSE_WAIT = 2,
		CLOSE = 3,
		REDUCE = 4,
		RESET_REDUCE = 5,
		WAIT = 6
	}

	private STATE mState;

	public static readonly float DIALOG_SCALE = 0.9f;

	protected GameObject mTarget;

	protected float mDelay;

	protected float mAngle;

	protected float mCloseDelay;

	protected bool mMoveFinished;

	protected bool mOpenImmediate;

	protected bool mCloseImmediate;

	private float mOpenSpeed = 1f;

	private float mCloseSpeed = 1f;

	private float mDefaultScale = DIALOG_SCALE;

	protected float mTargetScale = DIALOG_SCALE;

	private Action<float> mCustomOpenEvent;

	private float mCustomOpenSpeed = -1f;

	private Action<float> mCustomCloseEvent;

	private float mCustomCloseSpeed = -1f;

	protected static float ADD_SCALE_MAX = 0.3f;

	protected virtual void Awake()
	{
		mTarget = base.gameObject;
		mDelay = 0f;
	}

	protected virtual void Start()
	{
		Reset();
	}

	protected virtual void Update()
	{
		if (mDelay > 0f)
		{
			mDelay -= Time.deltaTime;
			return;
		}
		Action<float> action = null;
		float num = 640f;
		switch (mState)
		{
		case STATE.INIT:
			mAngle = 0f;
			mTarget.transform.localScale = new Vector3(1f + ADD_SCALE_MAX, 1f + ADD_SCALE_MAX, 1f);
			mMoveFinished = false;
			mState = STATE.WAIT;
			break;
		case STATE.OPEN:
			if (mOpenImmediate)
			{
				mAngle = 90f;
			}
			else if (mCustomOpenSpeed != -1f)
			{
				num = mCustomOpenSpeed;
			}
			mAngle += Time.deltaTime * num;
			if (mAngle >= 90f)
			{
				mAngle = 90f;
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			action = mCustomOpenEvent;
			break;
		case STATE.CLOSE_WAIT:
			mCloseDelay -= Time.deltaTime;
			if (mCloseDelay <= 0f)
			{
				mAngle = 90f;
				mState = STATE.CLOSE;
			}
			break;
		case STATE.CLOSE:
			if (mCloseImmediate)
			{
				mAngle = 0f;
			}
			else if (mCustomCloseSpeed != -1f)
			{
				num = mCustomCloseSpeed;
			}
			mAngle -= Time.deltaTime * num;
			if (mAngle <= 0f)
			{
				mAngle = 0f;
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			action = mCustomCloseEvent;
			break;
		case STATE.REDUCE:
		{
			int num3 = 0;
			mAngle -= Time.deltaTime * 640f / 2f;
			if (mAngle <= 0f)
			{
				mAngle = 0f;
				num3++;
			}
			mDefaultScale -= Time.deltaTime * 640f / 2f / 90f;
			if (mDefaultScale <= mTargetScale)
			{
				mDefaultScale = mTargetScale;
				num3++;
			}
			if (num3 >= 2)
			{
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			break;
		}
		case STATE.RESET_REDUCE:
		{
			int num2 = 0;
			mAngle += Time.deltaTime * 640f;
			if (mAngle >= 90f)
			{
				mAngle = 90f;
				num2++;
			}
			mDefaultScale += Time.deltaTime * 640f / 90f;
			if (mDefaultScale >= mTargetScale)
			{
				mDefaultScale = mTargetScale;
				num2++;
			}
			if (num2 >= 2)
			{
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			break;
		}
		}
		float num4 = Mathf.Cos(mAngle * ((float)Math.PI / 180f));
		if (action != null)
		{
			action(num4);
			return;
		}
		float num5 = mDefaultScale + num4 * ADD_SCALE_MAX;
		mTarget.transform.localScale = new Vector3(num5, num5, 1f);
	}

	public virtual void Reset()
	{
		mState = STATE.INIT;
	}

	public virtual void Open(bool immediate = false)
	{
		mOpenImmediate = immediate;
		mAngle = 0f;
		mMoveFinished = false;
		mState = STATE.OPEN;
	}

	public virtual void Close(bool immediate = false)
	{
		mCloseImmediate = immediate;
		mCloseDelay = 0.1f;
		mMoveFinished = false;
		mState = STATE.CLOSE_WAIT;
	}

	public virtual void Reduce(float aReduceScale)
	{
		mTargetScale = aReduceScale;
		mMoveFinished = false;
		mState = STATE.REDUCE;
	}

	public virtual void ResetReduce()
	{
		mTargetScale = DIALOG_SCALE;
		mMoveFinished = false;
		mState = STATE.RESET_REDUCE;
	}

	public virtual bool IsMoveFinished()
	{
		return mMoveFinished;
	}

	public virtual void SetMoveFinished(bool flag)
	{
		mMoveFinished = flag;
	}

	public virtual void SetTarget(GameObject target)
	{
		mTarget = target;
	}

	public virtual void SetDelay(float delayTime)
	{
		mDelay = delayTime;
	}

	public void SetTargetScale(float scale)
	{
		mDefaultScale = scale;
		mTargetScale = scale;
	}

	public float GetDefaultScale()
	{
		return mDefaultScale;
	}

	public void SetCustomOpenEvent(Action<float> callback, float speed = -1f)
	{
		mCustomOpenEvent = callback;
		mCustomOpenSpeed = speed;
	}

	public void SetCustomCloseEvent(Action<float> callback, float speed = 1f)
	{
		mCustomCloseEvent = callback;
		mCustomCloseSpeed = speed;
	}
}
