using System.Collections.Generic;
using UnityEngine;
using live2d;
using live2d.framework;

public class ResLive2DAnimation : ResourceInstance
{
	public string BasePath;

	public ModelSettingJson ModelSetting;

	public byte[] ModelData;

	public Dictionary<string, byte[]> MotionDataDict;

	public Dictionary<string, byte[]> ExpressionDataDict;

	public byte[] PhysicsData;

	public L2DPose Pose;

	public Texture2D[] Textures;

	public ResLive2DAnimation(string name)
		: base(name)
	{
		ResourceType = TYPE.L2D_ANIMATION;
	}

	public int GetMotionIndex(string motionName)
	{
		string[] motionGroupNames = ModelSetting.GetMotionGroupNames();
		for (int i = 0; i < ModelSetting.GetMotionNum(motionGroupNames[0]); i++)
		{
			if (string.Compare(ModelSetting.GetMotionFile(motionGroupNames[0], i), motionName) == 0)
			{
				return i;
			}
		}
		return -1;
	}

	public int GetExpressionIndex(string expressionName)
	{
		for (int i = 0; i < ModelSetting.GetExpressionNum(); i++)
		{
			if (string.Compare(ModelSetting.GetExpressionFile(i), expressionName) == 0)
			{
				return i;
			}
		}
		return -1;
	}

	public Live2DModelUnity CreateModelInstance()
	{
		if (ModelData == null)
		{
			return null;
		}
		return Live2DModelUnity.loadModel(ModelData);
	}

	public Live2DMotion CreateMotionInstance(string key)
	{
		byte[] value = null;
		MotionDataDict.TryGetValue(key, out value);
		if (value == null)
		{
			return null;
		}
		return Live2DMotion.loadMotion(value);
	}

	public L2DExpressionMotion CreateExpressionInstance(string key)
	{
		byte[] value = null;
		ExpressionDataDict.TryGetValue(key, out value);
		if (value == null)
		{
			return null;
		}
		return L2DExpressionMotion.loadJson(value);
	}

	public L2DPhysics CreatePhysicsInstance()
	{
		if (PhysicsData == null)
		{
			return null;
		}
		return L2DPhysics.load(PhysicsData);
	}
}
