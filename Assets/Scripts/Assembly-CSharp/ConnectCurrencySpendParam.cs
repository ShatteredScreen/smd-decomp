public class ConnectCurrencySpendParam : ConnectParameterBase
{
	public int BuyItemID { get; private set; }

	public ConnectCurrencySpendParam(int a_itemID)
	{
		BuyItemID = a_itemID;
	}
}
