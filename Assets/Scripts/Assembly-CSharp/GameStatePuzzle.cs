using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using live2d;

public class GameStatePuzzle : GameState
{
	public enum STATE
	{
		LOAD_WAIT = 0,
		UNLOAD_WAIT = 1,
		STAGE_ERROR = 2,
		TRANSFORM = 3,
		READY = 4,
		START = 5,
		MAIN = 6,
		CONTINUE = 7,
		REPLAY = 8,
		NEXTSTAGE = 9,
		EXIT = 10,
		END = 11,
		WAIT = 12,
		RANKUP = 13,
		RANKUP_00 = 14,
		RANKUP_01 = 15,
		RANKUP_END = 16,
		NETWORK_WON = 17,
		NETWORK_LOST = 18,
		NETWORK_MAINTENANCE = 19,
		NETWORK_MAINTENANCE_WAIT = 20,
		AUTHERROR_RETURN_TITLE = 21,
		AUTHERROR_RETURN_WAIT = 22,
		PLUSCHANCE = 23,
		PLUSCHANCE_WAIT = 24,
		SPECIALCHANCE = 25,
		SPECIALCHANCE_WAIT = 26,
		PRE_NETWORK_WON = 27,
		BUY_STARGET = 28
	}

	private class _StageWonInfo : IStageInfo
	{
		public int ClearScore { get; set; }

		public int ClearTime { get; set; }

		public byte[] Metadata { get; set; }

		public byte[] GetMetaData()
		{
			return Metadata;
		}
	}

	public delegate void OnPuzzleFinishedCallback(bool a_win, bool a_stageskip);

	public delegate void CutInEffectFinishedDelegate();

	protected OnPuzzleFinishedCallback mPuzzleFinishCallback;

	private GameObject mSkillParent;

	private SkillEffectPlayer mSkillEffect;

	private bool continueDemoFinished;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	public static bool STAGE_DATA_REQUIRE = true;

	public static string STAGE_DISPLAY_NUMBER = string.Empty;

	public static int STAGE_LIMIT_TIMES_OR_MOVES = 0;

	private static readonly int STAGE_DATA_LOAD_WAIT_TIME_MS = 1000 / ((Application.targetFrameRate != -1) ? Application.targetFrameRate : Def.TARGET_FRAMERATE);

	private static readonly float STAGE_DATA_LOAD_WAIT_TIME = (float)STAGE_DATA_LOAD_WAIT_TIME_MS / 1000f;

	private GameStateManager mGameState;

	private PuzzleManager mPuzzleManager;

	private PuzzleHud mPuzzleHud;

	private int mPartnerIndex;

	private Partner mPartner;

	private float mPartnerBaseScale = 1f;

	private bool mPartnerForeground;

	private bool mSkillEffectPlaying;

	private SsSprite mBg;

	private ParticleEmitterControl mBgParticle;

	private OneShotAnime mBgMask;

	private GameObject mAnchorTop;

	private GameObject mAnchorLeft;

	private GameObject mAnchorBottom;

	private GameObject mAnchorRight;

	private GameObject mPuzzleRoot;

	private FixedSprite mFrameBG;

	private FixedSprite mFrameTopL;

	private FixedSprite mFrameTopR;

	private FixedSprite mFrameTopC;

	private bool mChanceAnnounceDone;

	private LazerRenderer mLazerEffects;

	private string mLoadBgmKey0 = string.Empty;

	private string mLoadBgmKey1 = string.Empty;

	private string mBGMKey = string.Empty;

	private bool mBGMLoadError;

	private LoseDialog mLoseDialog;

	private WinDialog mWinDialog;

	private LabyrinthResultDialog mLabyrinthResultDialog;

	private FinishDialog mFinishDialog;

	private StageInfoDialog mStageInfoDialog;

	private StageStartDialog mStartDialog;

	private ConfirmDialog mConfirmDialog;

	private FriendHelpSuccessDialog mFriendHelpSuccessDialog;

	private RankUpDialog mRankUpDialog;

	private MissionGaugeUpDialog mMissionGaugeUpDialog;

	private STATE mRankUpNextState;

	private int mExtraContinueAddMove;

	private bool mSuspend;

	private bool mCollectPresent;

	private bool mReplayExit;

	private Def.STAGE_LOSE_REASON mLoseReason;

	private int mContinueCount;

	private bool mSkillBallExist;

	private bool mStageSkipDisplayed;

	private bool mStageSkipSelected;

	private STATE mNetworkLostNext = STATE.END;

	private BoosterMessageDialog mBoosterMessageDialog;

	private SkillMessageDialog mSkillMessageDialog;

	private object[] mAdditionalSkillArgs;

	private SocialRankingWindow mSocialWinRanking;

	private SocialRankingWindow mSocialLoseRanking;

	private List<StageRankingData> mWinStageRankinglist = new List<StageRankingData>();

	private List<StageRankingData> mStageRankinglist;

	private bool mRankingClosing;

	private bool mEnablePlusChance;

	private bool mWaitPlusChance;

	private Action mChancePlayEvent;

	private Action mChancePrevOpen;

	private float mExpandScoreUpRatio = 1f;

	private Coroutine mPreNetworkOnWonRoutine;

	private bool mIsJewelGetChance;

	private bool mIsJewelGetChancePlay;

	private short mJewel1Num;

	private short mJewel3Num;

	private short mJewel5Num;

	private short mJewel9Num;

	private int mUBAMoves;

	private float mUBATime;

	private int mUBAContinue;

	private int mUBASkill;

	private Dictionary<Def.BOOSTER_KIND, int> mUBAUseBooster;

	private Dictionary<Def.BOOSTER_KIND, int> mUBAUsePaidBooster;

	private Dictionary<Def.TILE_KIND, int> mUBADropPresentBox;

	private int mUBAWinScore;

	private Dictionary<Def.TILE_FORM, int> mUBACreatedSpecialTileCount;

	private Dictionary<Def.TILE_FORM, int> mUBACrushedSpecialTileCount;

	private Dictionary<Def.TILE_KIND, int> mUBACrushedSpecialCrossCount;

	private ParticleSystem mPSforAdreno;

	private static readonly Def.BOOSTER_KIND[] UBA_BOOSTERS = new Def.BOOSTER_KIND[8]
	{
		Def.BOOSTER_KIND.WAVE_BOMB,
		Def.BOOSTER_KIND.TAP_BOMB2,
		Def.BOOSTER_KIND.RAINBOW,
		Def.BOOSTER_KIND.ADD_SCORE,
		Def.BOOSTER_KIND.SKILL_CHARGE,
		Def.BOOSTER_KIND.SELECT_ONE,
		Def.BOOSTER_KIND.SELECT_COLLECT,
		Def.BOOSTER_KIND.COLOR_CRUSH
	};

	private static readonly Def.TILE_KIND[] UBA_PRESENTS = new Def.TILE_KIND[24]
	{
		Def.TILE_KIND.PRESENTBOX0,
		Def.TILE_KIND.PRESENTBOX1,
		Def.TILE_KIND.PRESENTBOX2,
		Def.TILE_KIND.PRESENTBOX3,
		Def.TILE_KIND.PRESENTBOX4,
		Def.TILE_KIND.PRESENTBOX5,
		Def.TILE_KIND.PRESENTBOX6,
		Def.TILE_KIND.PRESENTBOX7,
		Def.TILE_KIND.PRESENTBOX8,
		Def.TILE_KIND.PRESENTBOX9,
		Def.TILE_KIND.PRESENTBOX10,
		Def.TILE_KIND.PRESENTBOX11,
		Def.TILE_KIND.PRESENTBOX12,
		Def.TILE_KIND.PRESENTBOX13,
		Def.TILE_KIND.PRESENTBOX14,
		Def.TILE_KIND.PRESENTBOX15,
		Def.TILE_KIND.PRESENTBOX16,
		Def.TILE_KIND.PRESENTBOX17,
		Def.TILE_KIND.PRESENTBOX18,
		Def.TILE_KIND.PRESENTBOX19,
		Def.TILE_KIND.PRESENTBOX20,
		Def.TILE_KIND.PRESENTBOX21,
		Def.TILE_KIND.PRESENTBOX22,
		Def.TILE_KIND.PRESENTBOX23
	};

	private bool mTutorialBoosterUse;

	private bool mTutorialContinue;

	private bool _stargetstage_load_finished;

	private bool _stargetstage_load_success;

	public bool StageDataLoadFinished { get; private set; }

	public bool RankingClosing
	{
		set
		{
			mRankingClosing = value;
		}
	}

	public short JewelCount { get; private set; }

	public short DispJewelCount { get; private set; }

	private OnPuzzleFinishedCallback GetFinishedCallback()
	{
		OnPuzzleFinishedCallback result = null;
		if (mGame.mCurrentStage.Series == Def.SERIES.SM_EV)
		{
			SMEventPageData sMEventPageData = mGame.mCurrentMapPageData as SMEventPageData;
			if (sMEventPageData != null)
			{
				Def.EVENT_TYPE eventType = sMEventPageData.EventSetting.EventType;
				if (eventType == Def.EVENT_TYPE.SM_LABYRINTH)
				{
					result = PuzzleFinishedProcess_Labyrinth;
				}
			}
		}
		return result;
	}

	private void PuzzleFinishedProcess_Labyrinth(bool a_win, bool a_stageskip)
	{
		int series = (int)mGame.mCurrentStage.Series;
		int stageNumber = mGame.mCurrentStage.StageNumber;
		int formatVersion = mGame.mCurrentStage.FormatVersion;
		int revision = mGame.mCurrentStage.Revision;
		string segmentCode = mGame.mCurrentStage.SegmentCode;
		string text = string.Empty + (int)mPuzzleManager.GetDifficultyMode() * 100;
		int mScore = mPuzzleManager.mScore;
		int num = mPuzzleManager.CheckGotStars();
		int mMaxCombo = mPuzzleManager.mMaxCombo;
		int mMoves = mPuzzleManager.mMoves;
		float mTime = mPuzzleManager.mTime;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, out data);
		PlayerEventLabyrinthData labyrinthData = data.LabyrinthData;
		if (!a_win)
		{
			bool noNextLottery = false;
			if (mContinueCount == 0)
			{
				if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
				{
					if (mTime > 5f)
					{
						noNextLottery = true;
					}
				}
				else if (mMoves > 5)
				{
					noNextLottery = true;
				}
			}
			labyrinthData.NoNextLottery = noNextLottery;
			return;
		}
		labyrinthData.NoNextLottery = false;
		int jewelCount = JewelCount;
		if (a_stageskip)
		{
			mScore = 0;
			num = 0;
		}
		short a_course;
		int a_main;
		int a_sub;
		Def.SplitEventStageNo(stageNumber, out a_course, out a_main, out a_sub);
		int stageNo = Def.GetStageNo(a_main, a_sub);
		SMEventPageData sMEventPageData = mGame.mCurrentMapPageData as SMEventPageData;
		GlobalLabyrinthEventData labyrinthData2 = GlobalVariables.Instance.GetLabyrinthData(mGame.mCurrentStage.EventID);
		labyrinthData2.LastClearedStage = stageNumber;
		if (labyrinthData.ClearStageProcess(a_course, stageNo))
		{
			SMEventLabyrinthStageSetting labyrinthStageSetting = sMEventPageData.GetLabyrinthStageSetting(a_main, a_sub, a_course);
		}
		if (jewelCount > 0)
		{
			mGame.AddEventPoint(mGame.mCurrentStage.EventID, jewelCount);
		}
		GlobalVariables.Instance.SetLabyrinthData(mGame.mCurrentStage.EventID, labyrinthData2);
	}

	private IEnumerator CoEmitterDelete(SsSprite sp)
	{
		yield return 0;
		UnityEngine.Object.Destroy(sp.gameObject);
		yield return 0;
	}

	private IEnumerator CoJumpDown(UITexture uiTexture, SimpleTexture simpleTexture, Vector3 fromPos, Vector3 toPos, float time)
	{
		Vector3 distance = toPos - fromPos;
		float angle = 90f;
		while (angle < 180f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 180f)
			{
				angle = 180f;
			}
			float ratio = 1f - Mathf.Sin(angle * ((float)Math.PI / 180f));
			if (uiTexture != null)
			{
				uiTexture.transform.localPosition = fromPos + distance * ratio;
			}
			if (simpleTexture != null)
			{
				simpleTexture.transform.localPosition = fromPos + distance * ratio;
			}
			yield return 0;
		}
	}

	private IEnumerator CoJumpUp(UITexture uiTexture, SimpleTexture simpleTexture, Vector3 fromPos, Vector3 toPos, float time)
	{
		yield return new WaitForSeconds(1f / 6f);
		Vector3 distance = toPos - fromPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			if (uiTexture != null)
			{
				uiTexture.transform.localPosition = fromPos + distance * ratio;
			}
			if (simpleTexture != null)
			{
				simpleTexture.transform.localPosition = fromPos + distance * ratio;
			}
			yield return 0;
		}
	}

	private IEnumerator CoJumpDownPartnerScreen(Vector3 fromPos, Vector3 toPos, float time, int characterIndex)
	{
		Vector3 distance = toPos - fromPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = 1f - Mathf.Sin((angle + 90f) * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenOffset(fromPos + distance * ratio, characterIndex);
			yield return 0;
		}
	}

	private IEnumerator CoJumpUpPartnerScreen(Vector3 fromPos, Vector3 toPos, float time, int characterIndex)
	{
		yield return new WaitForSeconds(1f / 6f);
		Vector3 distance = toPos - fromPos;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mPartner.SetPartnerScreenOffset(fromPos + distance * ratio, characterIndex);
			yield return 0;
		}
	}

	private IEnumerator PartnerTransform()
	{
		mPuzzleHud.HudOff();
		float deviceRatio = (float)mGame.mRoot.manualHeight / 1136f;
		OneShotAnime anime2 = Util.CreateOneShotAnime(scale: new Vector3(deviceRatio, deviceRatio, 1f), name: "TransformBg", key: "EFFECT_TRANSFORMATION", parent: mRoot, pos: new Vector3(0f, 0f, Def.HUD_Z - 20f), rotateDeg: 0f, delay: 0f, autoDestroy: false);
		anime2._sprite.PlayCount = 0;
		SMParticle particle2 = Util.CreateGameObject("TransformParticle", mRoot).AddComponent<SMParticle>();
		particle2.LoadFromPrefab("Particles/TransformParticle");
		particle2.gameObject.transform.localScale = Vector3.one;
		particle2.gameObject.transform.localPosition = new Vector3(0f, -284f, -40f);
		particle2.mParticle.Play();
		while (!isLoadFinished())
		{
			yield return 0;
		}
		mGame.PlaySe("SE_PARTNER_TRANSFORM", 1);
		while (!mGameState.IsFadeFinished)
		{
			yield return 0;
		}
		mPartner.StartMotion(CompanionData.MOTION.LANDING, false);
		mPartnerForeground = true;
		mPartner.SetPartnerScreenScale(1.2f);
		for (int i = 0; i < mPartner.mData.ModelCount; i++)
		{
			StartCoroutine(CoJumpDownPartnerScreen(new Vector3(0f, 568f, -50f), new Vector3(0f, -284f, -50f), 4f / 15f, i));
		}
		yield return new WaitForSeconds(0.40000004f);
		mPartner.StartMotion(CompanionData.MOTION.SPECIAL, false);
		yield return new WaitForSeconds(1.6666667f);
		StartCoroutine(GrayOut(1f, Color.white, 0.5f));
		yield return 0;
		while (mGrayOutFuncEnable)
		{
			yield return 0;
		}
		mPartner.SetPartnerScreenScale(mPartnerBaseScale);
		mPartner.SetPartnerScreenOffset(Vector3.zero);
		mPartnerForeground = false;
		UnityEngine.Object.Destroy(anime2.gameObject);
		anime2 = null;
		UnityEngine.Object.Destroy(particle2.gameObject);
		particle2 = null;
		yield return 0;
		mPuzzleHud.HudOn();
		StartCoroutine(GrayIn(0.5f));
		while (mGrayInFuncEnable)
		{
			yield return 0;
		}
		ResourceManager.UnloadSsAnimation("EFFECT_TRANSFORMATION");
		mState.Change(STATE.READY);
	}

	private IEnumerator PartnerCutinEffect(string animeKey, string emitterName, string soundKey, CompanionData.MOTION motion, bool motionLoop, string voiceKey, string voiceCutinSpriteKey)
	{
		GameObject go = SkillEffectPlayer.CreateDemoCamera();
		yield return 0;
		Partner partner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		partner.transform.localPosition = Vector3.zero;
		partner.Init((short)mGame.mCompanionIndex, Vector3.zero, 1f, 5, go, false, 0);
		while (!partner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
		partner.StartMotion(motion, motionLoop);
		partner.SetPartnerScreenPos(new Vector3(0f, -284f, -40f));
		partner.SetPartnerScreenScale(1.2f);
		yield return 0;
		partner.SetPartnerScreenEnable(true);
		if (!string.IsNullOrEmpty(animeKey))
		{
			OneShotAnime anime = Util.CreateOneShotAnime("TextEffect", animeKey, go, new Vector3(0f, -160f, -45f), Vector3.one, 0f, 0f, false, delegate(SsSprite sp)
			{
				StartCoroutine(CoComboEffectDelete(sp));
			});
			if (!string.IsNullOrEmpty(emitterName))
			{
				ParticleEmitterControl ec = Util.CreateGameObject("EmitControl", go).AddComponent<ParticleEmitterControl>();
				ec.Init(anime._sprite, emitterName);
			}
		}
		if (!string.IsNullOrEmpty(soundKey))
		{
			mGame.PlaySe(soundKey, 1);
		}
		if (!string.IsNullOrEmpty(voiceCutinSpriteKey) && !mGame.mCompanionData[mPartnerIndex].CheckParsonalID(CompanionData.PERSONAL_ID.USAGI))
		{
			GameObject anchorBottomRight = Util.CreateAnchorWithChild("PuzzleAnchorBottomLeft", go, UIAnchor.Side.BottomRight, mGame.mCamera).gameObject;
			ResImage resImagePuzzleHud = ResourceManager.LoadImage("PUZZLE_HUD");
			CHANGE_PART_INFO[] changePartInfo = new CHANGE_PART_INFO[1];
			changePartInfo[0].partName = "change_000";
			changePartInfo[0].spriteName = voiceCutinSpriteKey;
			Util.CreateOneShotAnime("CutinSailorMoon", "CUTIN_SAILOR_MOON", anchorBottomRight.gameObject, new Vector3(-132f, 132f, -46f), Vector3.one, 0f, 0f, changePartInfo, resImagePuzzleHud.Image);
		}
		float alpha = 0f;
		while (alpha < 1f)
		{
			alpha += Time.deltaTime * 5f;
			if (alpha > 1f)
			{
				alpha = 1f;
			}
			partner.SetPartnerScreenColor(new Color(1f, 1f, 1f, alpha));
			yield return 0;
		}
		if (!string.IsNullOrEmpty(voiceKey))
		{
			mGame.PlaySe(voiceKey, 2);
		}
		yield return new WaitForSeconds(1.6666667f);
		while (alpha > 0f)
		{
			alpha -= Time.deltaTime * 5f;
			if (alpha < 0f)
			{
				alpha = 0f;
			}
			partner.SetPartnerScreenColor(new Color(1f, 1f, 1f, alpha));
			yield return 0;
		}
		UnityEngine.Object.Destroy(partner.gameObject);
		UnityEngine.Object.Destroy(go);
	}

	private IEnumerator ComboCutinEffect(string animeKey, string emitterName, string soundKey, CompanionData.MOTION motion, string voiceKey, string voiceCutinSpriteKey)
	{
		if (mGame.mOptions.ComboCutIn)
		{
			StartCoroutine(GrayOut(0.5f, Color.black, 0.5f));
			yield return StartCoroutine(PartnerCutinEffect(animeKey, emitterName, soundKey, motion, false, voiceKey, voiceCutinSpriteKey));
			StartCoroutine(GrayIn());
		}
		else
		{
			yield return 0;
		}
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator AfterPlayCutinEffect(string animeKey, string soundKey, CompanionData.MOTION motion, string voiceKey, string voiceCutinSpriteKey, CutInEffectFinishedDelegate onFinished)
	{
		StartCoroutine(GrayOut(0.8f, Color.black, 0.5f));
		yield return StartCoroutine(PartnerCutinEffect(animeKey, string.Empty, soundKey, motion, true, voiceKey, voiceCutinSpriteKey));
		onFinished();
	}

	private IEnumerator CreateSkillBall(PuzzleTile targetTile)
	{
		mGame.PlaySe("SE_SKILL_BALL", 3);
		mPuzzleHud.TransformSkillBall(targetTile.Sprite.gameObject);
		yield return new WaitForSeconds(1.9f);
		mPuzzleManager.SkillBallTransform(targetTile);
		yield return new WaitForSeconds(1.2f);
		mPuzzleManager.WaitStateFinished();
		mSkillBallExist = true;
	}

	private IEnumerator ChanceBgChange()
	{
		StartCoroutine(GrayOut());
		ResSsAnimation resAnime = ResourceManager.LoadSsAnimationAsync("PARTNER_BG_CHANCE");
		PlayBGM();
		yield return StartCoroutine(PartnerCutinEffect("TEXT_CHANCE", string.Empty, string.Empty, CompanionData.MOTION.HIT1, false, "VOICE_020", "efc_cutin_c00"));
		StartCoroutine(GrayIn());
		while (resAnime.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mBgMask = Util.CreateOneShotAnime("MaskBg", "PARTNER_BG_MASK", mPuzzleRoot.gameObject, Vector3.zero, Vector3.one, 0f, 0f, false, (SsSprite.AnimationCallback)delegate
		{
			UnityEngine.Object.Destroy(mBgMask.gameObject);
			mBgMask = null;
		});
		SetLayoutPartnerBg(Util.ScreenOrientation);
		yield return new WaitForSeconds(0.5f);
		UnityEngine.Object.Destroy(mBg.gameObject);
		mBg = Util.CreateGameObject("PartnerBg", mAnchorTop).AddComponent<SsSprite>();
		mBg.Animation = resAnime.SsAnime;
		SetLayoutPartnerBg(Util.ScreenOrientation);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator ContinueEffect(int continueCount, int additionalMoves, bool animationSkip)
	{
		mGame.PlaySe("SE_CONTINUE_EFFECT", 2);
		StartCoroutine(GrayIn(1f));
		if (!animationSkip)
		{
			Live2DRender l2dRender = mGame.CreateLive2DRender(568, 568);
			GameObject go = SkillEffectPlayer.CreateDemoCamera();
			float deviceRatio = (float)mGame.mRoot.manualHeight / 1136f;
			Util.CreateOneShotAnime(scale: new Vector3(deviceRatio, deviceRatio, 1f), name: "Effect", key: "EFFECT_CONTINUE_BACK", parent: go, pos: new Vector3(0f, 0f, Def.HUD_Z - 1f), rotateDeg: 0f, delay: 0f);
			yield return new WaitForSeconds(1f / 3f);
			OneShotAnime anime = Util.CreateOneShotAnime("Effect", "EFFECT_CONTINUE", go, new Vector3(0f, 0f, Def.HUD_Z - 40f), Vector3.one, 0f, 0f, false, delegate(SsSprite sp)
			{
				StartCoroutine(CoEmitterDelete(sp));
			});
			ParticleEmitterControl ec = Util.CreateGameObject("EmitControl", go).AddComponent<ParticleEmitterControl>();
			ec.Init(anime._sprite, "Particles/ContinueParticle", 1f);
			yield return new WaitForSeconds(2f / 3f);
			Live2DInstance l2dInst = Util.CreateGameObject("TUXEDO", l2dRender.gameObject).AddComponent<Live2DInstance>();
			l2dInst.Init("TUXEDO", Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER, Color.white, true);
			while (!l2dInst.IsLoadFinished())
			{
				yield return 0;
			}
			l2dInst.StartMotion("motions/continue00.mtn", false);
			l2dInst.Position = Vector3.zero;
			l2dRender.ResisterInstance(l2dInst);
			yield return 0;
			SimpleTexture partnerScreen = Util.CreateGameObject("Live2DScreen", go).AddComponent<SimpleTexture>();
			partnerScreen.Init(l2dRender.RenderTexture, new Vector2(568f, 568f), new Rect(0f, 0f, 1f, 1f), SimpleTexture.PIVOT.BOTTOM);
			partnerScreen.transform.localPosition = new Vector3(0f, -284f, Def.HUD_Z - 20f);
			StartCoroutine(CoJumpDown(null, partnerScreen, new Vector3(0f, 568f, Def.HUD_Z - 20f), new Vector3(0f, -284f, -50f), 1f / 6f));
			yield return new WaitForSeconds(1f / 6f);
			yield return new WaitForSeconds(3.0666668f);
			StartCoroutine(CoJumpUp(null, partnerScreen, new Vector3(0f, -284f, Def.HUD_Z - 20f), new Vector3(0f, 824f, -30f), 1f / 6f));
			yield return new WaitForSeconds(4f / 15f);
			UnityEngine.Object.Destroy(go);
			UnityEngine.Object.Destroy(l2dRender.gameObject);
		}
		mPuzzleHud.StartContinueEffect(continueCount, additionalMoves);
		mPartner.Continue();
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.LEVEL7_2)
		{
			mPuzzleManager.TutorialMessageFinish();
			yield break;
		}
		PlayBGM();
		mGame.SetMusicVolume(1f, 0);
	}

	private IEnumerator Booster_Common(Def.BOOSTER_KIND boosterKind, PuzzleTile targetTile, string seKey, CHANGE_PART_INFO[] changePartInfo)
	{
		BoosterData data = mGame.mBoosterData[(int)boosterKind];
		List<ResourceInstance> resList2 = new List<ResourceInstance>();
		for (int n = 0; n < data.preLoadSound.Length; n++)
		{
			if (!string.IsNullOrEmpty(data.preLoadSound[n]))
			{
				resList2.Add(ResourceManager.LoadSoundAsync(data.preLoadSound[n]));
			}
		}
		for (int m = 0; m < data.preLoadAtlas.Length; m++)
		{
			if (!string.IsNullOrEmpty(data.preLoadAtlas[m]))
			{
				resList2.Add(ResourceManager.LoadImageAsync(data.preLoadAtlas[m]));
			}
		}
		for (int l = 0; l < data.preLoadAnimation.Length; l++)
		{
			if (!string.IsNullOrEmpty(data.preLoadAnimation[l]))
			{
				resList2.Add(ResourceManager.LoadSsAnimationAsync(data.preLoadAnimation[l]));
			}
		}
		while (true)
		{
			bool wait = false;
			foreach (ResourceInstance res in resList2)
			{
				if (res != null && res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
		resList2.Clear();
		resList2 = null;
		GameObject cameraGo = SkillEffectPlayer.CreateDemoCamera();
		Util.CreateOneShotAnime(atlas: ResourceManager.LoadImage(data.preLoadAtlas[0]).Image, name: "BoosterEffect", key: data.anime, parent: cameraGo, pos: Vector3.zero, scale: Vector3.one, rotateDeg: 0f, delay: 0f, changePartsArray: changePartInfo);
		mGame.PlaySe("VOICE_023", 2);
		if (!string.IsNullOrEmpty(data.sound))
		{
			mGame.PlaySe(data.sound, 3);
		}
		switch (boosterKind)
		{
		case Def.BOOSTER_KIND.WAVE_BOMB:
			yield return StartCoroutine(Booster_WaveBomb());
			break;
		case Def.BOOSTER_KIND.TAP_BOMB2:
			yield return StartCoroutine(Booster_TapBomb2());
			break;
		case Def.BOOSTER_KIND.RAINBOW:
			yield return StartCoroutine(Booster_Rainbow());
			break;
		case Def.BOOSTER_KIND.ADD_SCORE:
			yield return StartCoroutine(Booster_AddScore(0.1f * mExpandScoreUpRatio));
			break;
		case Def.BOOSTER_KIND.SKILL_CHARGE:
			yield return StartCoroutine(Booster_SkillCharge());
			break;
		case Def.BOOSTER_KIND.SELECT_ONE:
			yield return StartCoroutine(Booster_SelectOne(targetTile));
			break;
		case Def.BOOSTER_KIND.SELECT_COLLECT:
			yield return StartCoroutine(Booster_SelectCollect(targetTile));
			break;
		case Def.BOOSTER_KIND.COLOR_CRUSH:
			yield return StartCoroutine(Booster_ColorCrush(targetTile));
			break;
		case Def.BOOSTER_KIND.BLOCK_CRUSH:
			yield return StartCoroutine(Booster_BlockCrush());
			break;
		case Def.BOOSTER_KIND.PAIR_MAKER:
			yield return StartCoroutine(Booster_PairMaker(targetTile));
			break;
		case Def.BOOSTER_KIND.ALL_CRUSH:
			yield return StartCoroutine(Booster_CrushAll(targetTile, cameraGo));
			break;
		}
		if (cameraGo != null)
		{
			UnityEngine.Object.Destroy(cameraGo);
		}
		while (SoundManager.instance.isSePlaying(3))
		{
			yield return 0;
		}
		for (int k = 0; k < data.preLoadSound.Length; k++)
		{
			if (!string.IsNullOrEmpty(data.preLoadSound[k]))
			{
				ResourceManager.UnloadSound(data.preLoadSound[k]);
			}
		}
		for (int j = 0; j < data.preLoadAtlas.Length; j++)
		{
			if (!string.IsNullOrEmpty(data.preLoadAtlas[j]))
			{
				ResourceManager.UnloadImage(data.preLoadAtlas[j]);
			}
		}
		for (int i = 0; i < data.preLoadAnimation.Length; i++)
		{
			if (!string.IsNullOrEmpty(data.preLoadAnimation[i]))
			{
				ResourceManager.UnloadSsAnimation(data.preLoadAnimation[i]);
			}
		}
	}

	private IEnumerator Booster_WaveBomb()
	{
		yield return new WaitForSeconds(1.6666667f);
		PuzzleTile[] targets = mPuzzleManager.Booster_GetTargetTiles(2);
		if (targets == null)
		{
			mPuzzleManager.WaitStateFinished();
			yield break;
		}
		Vector2 logScreenSize = Util.LogScreenSize();
		Vector3 orignPos = new Vector3(0f, 0f, 0f);
		for (int i = 0; i < targets.Length; i++)
		{
			Vector3 targetPos = targets[i].Position * logScreenSize.y / 2f;
			HomingLazer lazer = new HomingLazer(new Vector2(orignPos.x, orignPos.y), new Vector2(targetPos.x, targetPos.y), 10, 20f, "efc_cristal_ball", 0f)
			{
				OnFinished = delegate(LazerInstanceBase self)
				{
					HomingLazer homingLazer = (HomingLazer)self;
					mPuzzleManager.Booster_WaveBomb(targets[homingLazer.IntParam0], homingLazer.IntParam0);
				},
				IntParam0 = i
			};
			mLazerEffects.AddLazer(lazer);
		}
		yield return new WaitForSeconds(2.5f);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator Booster_TapBomb2()
	{
		yield return new WaitForSeconds(1.6666667f);
		PuzzleTile[] targets = mPuzzleManager.Booster_GetTargetTiles(2);
		if (targets == null)
		{
			mPuzzleManager.WaitStateFinished();
			yield break;
		}
		Vector2 logScreenSize = Util.LogScreenSize();
		Vector3 orignPos = new Vector3(0f, 0f, 0f);
		for (int i = 0; i < targets.Length; i++)
		{
			Vector3 targetPos = targets[i].Position * logScreenSize.y / 2f;
			HomingLazer lazer = new HomingLazer(new Vector2(orignPos.x, orignPos.y), new Vector2(targetPos.x, targetPos.y), 10, 20f, "efc_cristal_ball", 0f)
			{
				OnFinished = delegate(LazerInstanceBase self)
				{
					HomingLazer homingLazer = (HomingLazer)self;
					mPuzzleManager.Booster_TapBomb2(targets[homingLazer.IntParam0]);
				},
				IntParam0 = i
			};
			mLazerEffects.AddLazer(lazer);
		}
		yield return new WaitForSeconds(2.5f);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator Booster_Rainbow()
	{
		yield return new WaitForSeconds(1.6666667f);
		PuzzleTile[] targets = mPuzzleManager.Booster_GetTargetTiles(1);
		if (targets == null)
		{
			mPuzzleManager.WaitStateFinished();
			yield break;
		}
		Vector2 logScreenSize = Util.LogScreenSize();
		Vector3 orignPos = new Vector3(0f, 0f, 0f);
		for (int i = 0; i < targets.Length; i++)
		{
			Vector3 targetPos = targets[i].Position * logScreenSize.y / 2f;
			HomingLazer lazer = new HomingLazer(new Vector2(orignPos.x, orignPos.y), new Vector2(targetPos.x, targetPos.y), 10, 20f, "efc_cristal_ball", 0f)
			{
				OnFinished = delegate(LazerInstanceBase self)
				{
					HomingLazer homingLazer = (HomingLazer)self;
					mPuzzleManager.Booster_Rainbow(targets[homingLazer.IntParam0]);
				},
				IntParam0 = i
			};
			mLazerEffects.AddLazer(lazer);
		}
		yield return new WaitForSeconds(2.5f);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator Booster_AddScore(float rate)
	{
		yield return new WaitForSeconds(1f);
		Vector3 orignPos = new Vector3(0f, 0f, 0f);
		Vector3 targetPos = mPuzzleHud.GetScoreBoardPos();
		OneShotParticle particle0 = Util.CreateOneShotParticle("Change", "Particles/PuzzleBooster/ScoreUpA", mRoot, new Vector3(0f, 0f, -30f), new Vector3(1f, 1f, 0.1f), 3f, 0f);
		OneShotParticle particle1 = Util.CreateOneShotParticle("Change", "Particles/PuzzleBooster/ScoreUpB", mRoot, new Vector3(0f, 0f, -30f), new Vector3(1f, 1f, 0.1f), 3f, 0f);
		OneShotParticle particle2 = Util.CreateOneShotParticle("Change", "Particles/PuzzleBooster/ScoreUpC", mRoot, new Vector3(0f, 0f, -30f), new Vector3(1f, 1f, 0.1f), 3f, 0f);
		float zBuf = particle0.transform.position.z;
		float angle = 0f;
		Vector3 distance = targetPos - orignPos;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			particle0.transform.position = new Vector3(orignPos.x + distance.x * ratio, orignPos.y + distance.y * ratio, zBuf);
			particle1.transform.position = new Vector3(orignPos.x + distance.x * ratio, orignPos.y + distance.y * ratio, zBuf);
			particle2.transform.position = new Vector3(orignPos.x + distance.x * ratio, orignPos.y + distance.y * ratio, zBuf);
			yield return 0;
		}
		mPuzzleHud.StartScoreUpEffect(rate);
		mPuzzleManager.Booster_AddScore(rate);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator Booster_SkillCharge()
	{
		yield return new WaitForSeconds(1f);
		Vector2 logScreenSize = Util.LogScreenSize();
		Vector3 orignPos = new Vector3(0f, 0f, 0f);
		Vector3 targetPos = mPuzzleHud.GetSkillButtonPos() * logScreenSize.y / 2f;
		HomingLazer lazer;
		for (int i = 0; i < 9; i++)
		{
			lazer = new HomingLazer(new Vector2(orignPos.x, orignPos.y), new Vector2(targetPos.x, targetPos.y), 8, 16f, "efc_homing_purple", (float)i * 0.05f);
			mLazerEffects.AddLazer(lazer);
		}
		lazer = new HomingLazer(new Vector2(orignPos.x, orignPos.y), new Vector2(targetPos.x, targetPos.y), 8, 16f, "efc_homing_purple", 0.5f)
		{
			OnFinished = delegate
			{
				mPuzzleHud.SkillGaugeChargeUpStart();
				mPuzzleManager.WaitStateFinished();
			}
		};
		mLazerEffects.AddLazer(lazer);
	}

	private IEnumerator Booster_SelectOne(PuzzleTile targetTile)
	{
		bool finishFlg = false;
		yield return new WaitForSeconds(1.6666667f);
		PuzzleTile targetTile2 = default(PuzzleTile);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_SELECT_ONE0", mRoot.gameObject, new Vector3(0f, 0f, Def.HUD_Z - 20f), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			mPuzzleManager.Booster_SelectOne(targetTile2);
			mPuzzleManager.WaitStateFinished();
			finishFlg = true;
		});
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_SELECT_ONE1", mRoot.gameObject, new Vector3(0f, 0f, Def.HUD_Z - 19f), Vector3.one, 0f, 0f);
		while (!finishFlg)
		{
			yield return 0;
		}
	}

	private IEnumerator Booster_SelectCollect(PuzzleTile targetTile)
	{
		bool finishFlg = false;
		yield return new WaitForSeconds(1.6666667f);
		PuzzleTile targetTile2 = default(PuzzleTile);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_SELECT_COLLECT", mRoot.gameObject, new Vector3(0f, 0f, Def.HUD_Z - 20f), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			mPuzzleManager.Booster_SelectCollect(targetTile2);
			mPuzzleManager.WaitStateFinished();
			finishFlg = true;
		});
		while (!finishFlg)
		{
			yield return 0;
		}
	}

	private IEnumerator Booster_ColorCrush(PuzzleTile targetTile)
	{
		bool finishFlg = false;
		yield return new WaitForSeconds(1.6666667f);
		PuzzleTile targetTile2 = default(PuzzleTile);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_COLOR_CRUSH", mRoot.gameObject, new Vector3(0f, 0f, Def.HUD_Z - 20f), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			mPuzzleManager.Booster_ColorCrush(targetTile2);
			mPuzzleManager.WaitStateFinished();
			finishFlg = true;
		});
		while (!finishFlg)
		{
			yield return 0;
		}
	}

	private IEnumerator Booster_BlockCrush()
	{
		bool finishFlg = false;
		yield return new WaitForSeconds(1.6666667f);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_BLOCK_CRUSH", mRoot.gameObject, new Vector3(0f, 0f, Def.HUD_Z - 20f), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			PuzzleTile[] array = mPuzzleManager.Booster_GetBlockCrushTargetTiles(3);
			for (int i = 0; i < array.Length; i++)
			{
				mPuzzleManager.Booster_BlockCrush(array[i]);
			}
			mPuzzleManager.WaitStateFinished();
			finishFlg = true;
		});
		while (!finishFlg)
		{
			yield return 0;
		}
	}

	private IEnumerator Booster_PairMaker(PuzzleTile targetTile)
	{
		bool finishFlg = false;
		yield return new WaitForSeconds(1.6666667f);
		PuzzleTile targetTile2 = default(PuzzleTile);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_PAIR_MAKER", mRoot.gameObject, new Vector3(0f, 0f, Def.HUD_Z - 20f), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			PuzzleTile target = mPuzzleManager.Booster_GetPairMakerTargetTile(targetTile2);
			CollectPresent collectPresent = Util.CreateGameObject("Collect", mRoot).AddComponent<CollectPresent>();
			Vector2 vector = Util.LogScreenSize();
			Vector3 vector2 = target.Position * vector.y / 2f;
			Vector3 targetPos = targetTile2.Position * vector.y / 2f;
			collectPresent.Init(new Vector3(vector2.x, vector2.y, -2f), targetPos, target, false, true, 0f, null, string.Empty);
			mGame.PlaySe("SE_BOOSTER_PAIR_MAKER01", 2);
			collectPresent.SetFinishedCallback(delegate
			{
				mPuzzleManager.BeginCollectPair(targetTile2, target);
				if (targetTile2.Kind == Def.TILE_KIND.PAIR0_PARTS)
				{
					mPuzzleManager.AddExitCount(Def.TILE_KIND.PAIR0_COMPLETE, 1);
				}
				else if (targetTile2.Kind == Def.TILE_KIND.PAIR1_PARTS)
				{
					mPuzzleManager.AddExitCount(Def.TILE_KIND.PAIR1_COMPLETE, 1);
				}
				mGame.PlaySe("SE_BOOSTER_PAIR_MAKER02", 2);
				mPuzzleManager.WaitStateFinished();
				finishFlg = true;
			});
		});
		while (!finishFlg)
		{
			yield return 0;
		}
	}

	private IEnumerator Booster_CrushAll(PuzzleTile targetTile, GameObject parent)
	{
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_ALL_CRUSH", parent, new Vector3(0f, 0f, 1f), Vector3.one, 0f, 0f);
		yield return new WaitForSeconds(4f);
		mPuzzleManager.Booster_CrushAll(targetTile);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator ShakeScreen(float time)
	{
		Vector3 posBuf = mPuzzleRoot.transform.localPosition;
		while (time >= 0f)
		{
			time -= Time.deltaTime;
			Vector2 shakeOfs = new Vector2(UnityEngine.Random.Range(-5f, 5f), UnityEngine.Random.Range(-5f, 5f));
			mPuzzleRoot.transform.localPosition = posBuf + new Vector3(shakeOfs.x, shakeOfs.y, 0f);
			yield return 0;
		}
		mPuzzleRoot.transform.localPosition = posBuf;
	}

	private IEnumerator BossGimmickChangeTarget(Vector3 orignPos, Def.TILE_KIND[] newTargets)
	{
		StartCoroutine(ShakeScreen(0.5f));
		GameObject target = mPuzzleHud.GetNormaBoard();
		yield return StartCoroutine(BossGimmickPlaceEffect(orignPos, target.transform.position));
		Util.CreateOneShotParticle("Change", "Particles/PuzzleGimmick/Boss_TargetChangeB", target, new Vector3(0f, -10f, -30f), new Vector3(1f, 1f, 0.1f), 2.5f, 0f);
		yield return new WaitForSeconds(1f);
		mPuzzleHud.SetAttackTarget(newTargets);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator BossGimmickPlaceBrokenWall(Vector3 orignPos, int num1, int num2, int num3)
	{
		StartCoroutine(ShakeScreen(0.5f));
		PuzzleTile[] tiles = mPuzzleManager.GetRandomColorTiles(true);
		int index = 0;
		for (int k = 0; k < num1; k++)
		{
			while (tiles.Length > index)
			{
				index++;
				if (BossGimmickPlaceWallCheck(tiles[index - 1], Def.TILE_KIND.BROKEN_WALL0))
				{
					StartCoroutine(BossGimmickPlaceBrokenWall(orignPos, tiles[index - 1], Def.TILE_KIND.BROKEN_WALL0));
					break;
				}
			}
		}
		for (int j = 0; j < num2; j++)
		{
			while (tiles.Length > index)
			{
				index++;
				if (BossGimmickPlaceWallCheck(tiles[index - 1], Def.TILE_KIND.BROKEN_WALL1))
				{
					StartCoroutine(BossGimmickPlaceBrokenWall(orignPos, tiles[index - 1], Def.TILE_KIND.BROKEN_WALL1));
					break;
				}
			}
		}
		for (int i = 0; i < num3; i++)
		{
			while (tiles.Length > index)
			{
				index++;
				if (BossGimmickPlaceWallCheck(tiles[index - 1], Def.TILE_KIND.BROKEN_WALL2))
				{
					StartCoroutine(BossGimmickPlaceBrokenWall(orignPos, tiles[index - 1], Def.TILE_KIND.BROKEN_WALL2));
					break;
				}
			}
		}
		yield return new WaitForSeconds(1.5f);
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator BossGimmickPlaceMovableWall(Vector3 orignPos, int num1, int num2, int num3)
	{
		StartCoroutine(ShakeScreen(0.5f));
		PuzzleTile[] tiles = mPuzzleManager.GetRandomColorTiles(true);
		int movableWallCount = mPuzzleManager.GetMovableWallTiles().Length;
		int index = 0;
		for (int k = 0; k < num1; k++)
		{
			if (movableWallCount >= 30)
			{
				break;
			}
			while (tiles.Length > index)
			{
				index++;
				if (BossGimmickPlaceWallCheck(tiles[index - 1], Def.TILE_KIND.MOVABLE_WALL0))
				{
					StartCoroutine(BossGimmickPlaceMovableWall(orignPos, tiles[index - 1], Def.TILE_KIND.MOVABLE_WALL0));
					movableWallCount++;
					break;
				}
			}
		}
		for (int j = 0; j < num2; j++)
		{
			if (movableWallCount >= 30)
			{
				break;
			}
			while (tiles.Length > index)
			{
				index++;
				if (BossGimmickPlaceWallCheck(tiles[index - 1], Def.TILE_KIND.MOVABLE_WALL1))
				{
					StartCoroutine(BossGimmickPlaceMovableWall(orignPos, tiles[index - 1], Def.TILE_KIND.MOVABLE_WALL1));
					movableWallCount++;
					break;
				}
			}
		}
		for (int i = 0; i < num3; i++)
		{
			if (movableWallCount >= 30)
			{
				break;
			}
			while (tiles.Length > index)
			{
				index++;
				if (BossGimmickPlaceWallCheck(tiles[index - 1], Def.TILE_KIND.MOVABLE_WALL2))
				{
					StartCoroutine(BossGimmickPlaceMovableWall(orignPos, tiles[index - 1], Def.TILE_KIND.MOVABLE_WALL2));
					movableWallCount++;
					break;
				}
			}
		}
		yield return new WaitForSeconds(1.5f);
	}

	private bool BossGimmickPlaceWallCheck(PuzzleTile targetTile, Def.TILE_KIND kind)
	{
		PuzzleTile attribute = mPuzzleManager.GetAttribute(targetTile.TilePos);
		if (attribute != null)
		{
			return false;
		}
		ISMTileInfo tileInfo = mGame.mCurrentStage.GetTileInfo(targetTile.TilePos.x, targetTile.TilePos.y);
		if (tileInfo == null || !tileInfo.MakeObstacleArea)
		{
			return false;
		}
		return true;
	}

	private IEnumerator BossGimmickPlaceEffect(Vector3 orignPos, Vector3 targetPos)
	{
		OneShotParticle particle = Util.CreateOneShotParticle("Change", "Particles/PuzzleGimmick/Boss_TargetChangeA", mRoot, new Vector3(0f, 0f, -30f), new Vector3(1f, 1f, 0.1f), 3f, 0f);
		float zBuf = particle.transform.position.z;
		float angle = 0f;
		Vector3 distance = targetPos - orignPos;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			particle.transform.position = new Vector3(orignPos.x + distance.x * ratio, orignPos.y + distance.y * ratio, zBuf);
			yield return 0;
		}
	}

	private IEnumerator BossGimmickPlaceBrokenWall(Vector3 orignPos, PuzzleTile targetTile, Def.TILE_KIND kind)
	{
		yield return StartCoroutine(BossGimmickPlaceEffect(orignPos, targetTile.Sprite.transform.position));
		mPuzzleManager.BossGimmick_ChangeBrokenWall(targetTile, kind);
	}

	private IEnumerator BossGimmickPlaceMovableWall(Vector3 orignPos, PuzzleTile targetTile, Def.TILE_KIND kind)
	{
		yield return StartCoroutine(BossGimmickPlaceEffect(orignPos, targetTile.Sprite.transform.position));
		mPuzzleManager.BossGimmick_ChangeMovableWall(targetTile, kind);
	}

	private void TalismanGimmickPlaceMovableWall(PuzzleTile orignTile, int num1, int num2, int num3)
	{
		int num4 = mPuzzleManager.GetMovableWallTiles().Length;
		Vector3 position = orignTile.Position;
		IntVector2 tilePos = orignTile.TilePos;
		PuzzleTile[] randomColorTiles = mPuzzleManager.GetRandomColorTiles(true);
		int num5 = 0;
		for (int i = 0; i < num1; i++)
		{
			if (num4 >= 30)
			{
				break;
			}
			while (randomColorTiles.Length > num5)
			{
				num5++;
				if (randomColorTiles[num5 - 1].TilePos.x < tilePos.x - 2 || randomColorTiles[num5 - 1].TilePos.x > tilePos.x + 2 || randomColorTiles[num5 - 1].TilePos.y < tilePos.y - 2 || randomColorTiles[num5 - 1].TilePos.y > tilePos.y + 2 || !BossGimmickPlaceWallCheck(randomColorTiles[num5 - 1], Def.TILE_KIND.MOVABLE_WALL0))
				{
					continue;
				}
				TalismanGimmickPlaceMovableWallSub(position, randomColorTiles[num5 - 1], Def.TILE_KIND.MOVABLE_WALL0);
				num4++;
				break;
			}
		}
		for (int j = 0; j < num2; j++)
		{
			if (num4 >= 30)
			{
				break;
			}
			while (randomColorTiles.Length > num5)
			{
				num5++;
				if (randomColorTiles[num5 - 1].TilePos.x < tilePos.x - 2 || randomColorTiles[num5 - 1].TilePos.x > tilePos.x + 2 || randomColorTiles[num5 - 1].TilePos.y < tilePos.y - 2 || randomColorTiles[num5 - 1].TilePos.y > tilePos.y + 2 || !BossGimmickPlaceWallCheck(randomColorTiles[num5 - 1], Def.TILE_KIND.MOVABLE_WALL1))
				{
					continue;
				}
				TalismanGimmickPlaceMovableWallSub(position, randomColorTiles[num5 - 1], Def.TILE_KIND.MOVABLE_WALL1);
				num4++;
				break;
			}
		}
		for (int k = 0; k < num3; k++)
		{
			if (num4 >= 30)
			{
				break;
			}
			while (randomColorTiles.Length > num5)
			{
				num5++;
				if (randomColorTiles[num5 - 1].TilePos.x < tilePos.x - 2 || randomColorTiles[num5 - 1].TilePos.x > tilePos.x + 2 || randomColorTiles[num5 - 1].TilePos.y < tilePos.y - 2 || randomColorTiles[num5 - 1].TilePos.y > tilePos.y + 2 || !BossGimmickPlaceWallCheck(randomColorTiles[num5 - 1], Def.TILE_KIND.MOVABLE_WALL2))
				{
					continue;
				}
				TalismanGimmickPlaceMovableWallSub(position, randomColorTiles[num5 - 1], Def.TILE_KIND.MOVABLE_WALL2);
				num4++;
				break;
			}
		}
	}

	private void TalismanGimmickPlaceMovableWallSub(Vector3 orignPos, PuzzleTile targetTile, Def.TILE_KIND kind)
	{
		Vector2 vector = Util.LogScreenSize();
		Vector3 targetPos = targetTile.Position * vector.y / 2f;
		orignPos = orignPos * vector.y / 2f;
		PuzzleTile tile = new PuzzleTile(Def.TILE_KIND.MOVABLE_WALL0, Def.TILE_FORM.NORMAL, new IntVector2(0, 0), Vector3.zero, null, null, Def.ITEM_CATEGORY.NONE, 0);
		CollectPresent collectPresent = Util.CreateGameObject("Effect", mRoot).AddComponent<CollectPresent>();
		collectPresent.Init(new Vector3(orignPos.x, orignPos.y, -2f), targetPos, tile, false, false, 0f, null, string.Empty);
		collectPresent.SetMoveTime(0.3f);
		targetTile.ChangeForm(Def.TILE_KIND.MOVABLE_WALL0, Def.TILE_FORM.NONE, 0f, 0.3f, false, true, 0, false);
	}

	private IEnumerator CoLoadSkillAnimation(int partnerIndex, bool waitForFinished)
	{
		List<ResourceInstance> resList = new List<ResourceInstance>();
		int skill0id = mGame.mCompanionData[partnerIndex].skill0Id;
		int skill1id = mGame.mCompanionData[partnerIndex].skill1Id;
		int skill2id = mGame.mCompanionData[partnerIndex].skill2Id;
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcText))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcText));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcA))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcA));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcB))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcB));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcC))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcC));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcD))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcD));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcBack))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcBack));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].soundName))
		{
			resList.Add(ResourceManager.LoadSoundAsync(mGame.mSkillEffectData[skill0id].soundName));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcText))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcText));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcA))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcA));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcB))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcB));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcC))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcC));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcD))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcD));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcBack))
		{
			resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcBack));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].soundName))
		{
			resList.Add(ResourceManager.LoadSoundAsync(mGame.mSkillEffectData[skill1id].soundName));
		}
		if (skill2id != -1)
		{
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcText))
			{
				resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcText));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcA))
			{
				resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcA));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcB))
			{
				resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcB));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcC))
			{
				resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcC));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcD))
			{
				resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcD));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcBack))
			{
				resList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcBack));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].soundName))
			{
				resList.Add(ResourceManager.LoadSoundAsync(mGame.mSkillEffectData[skill2id].soundName));
			}
		}
		if (!waitForFinished)
		{
			yield break;
		}
		while (true)
		{
			bool wait = false;
			foreach (ResourceInstance res in resList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
	}

	private IEnumerator StartSkillEffect(int rank, object[] targets)
	{
		bool skillWait = true;
		mPuzzleManager.TimeScale = 0f;
		mSkillParent = SkillEffectPlayer.CreateDemoCamera();
		Def.SkillFuncData skillFuncData2 = null;
		int rankBuf = rank;
		if (mPartner.mData.IsHalloweenHaruka() || mPartner.mData.IsHalloweenMichiru())
		{
			rankBuf = ((UnityEngine.Random.Range(0, 100) < 50) ? 1 : 2);
		}
		if (mPartner.mData.IsHalloweenUsagi())
		{
			rankBuf = ((UnityEngine.Random.Range(0, 100) < 20) ? 1 : 2);
		}
		if (mPartner.mData.IsHalloweenHotaru())
		{
			rankBuf = ((UnityEngine.Random.Range(0, 100) < 80) ? 1 : 2);
		}
		if (mPartner.mData.IsHalloweenUsagi2())
		{
			rankBuf = ((UnityEngine.Random.Range(0, 100) < 20) ? 1 : 2);
		}
		if (mPartner.mData.IsSailorChibiMoon())
		{
			switch (rank)
			{
			case 1:
			{
				int[] ratioTbl2 = new int[3] { 55, 40, 5 };
				rankBuf = SkillEffectLottery(ref ratioTbl2) + 1;
				break;
			}
			case 2:
			{
				int[] ratioTbl = new int[3] { 65, 25, 10 };
				rankBuf = SkillEffectLottery(ref ratioTbl) + 1;
				break;
			}
			}
		}
		mSkillEffectPlaying = true;
		mSkillEffect = Util.CreateGameObject("SkillManager", mRoot).AddComponent<SkillEffectPlayer>();
		mSkillEffect.Init(mPartner, rankBuf, mSkillParent, SkillEffectPlayer.MODE.PUZZLE, delegate
		{
			skillWait = false;
		});
		while (skillWait)
		{
			yield return 0;
		}
		mSkillEffectPlaying = false;
		switch (rankBuf)
		{
		case 1:
			skillFuncData2 = Def.SKILL_FUNC_DATA[mPartner.mData.skill0Id];
			break;
		case 2:
			skillFuncData2 = Def.SKILL_FUNC_DATA[mPartner.mData.skill1Id];
			break;
		default:
			skillFuncData2 = Def.SKILL_FUNC_DATA[mPartner.mData.skill2Id];
			break;
		}
		object[] parameters = new object[targets.Length + 2];
		Array.Copy(targets, parameters, targets.Length);
		parameters[targets.Length] = skillFuncData2;
		parameters[targets.Length + 1] = rank;
		if (skillFuncData2.addMoves > 0)
		{
			yield return StartCoroutine(SkillRecoverMoves(skillFuncData2.addMoves));
		}
		yield return StartCoroutine(skillFuncData2.coroutineName, parameters);
		UnityEngine.Object.Destroy(mSkillEffect.gameObject);
		UnityEngine.Object.Destroy(mSkillParent);
		mSkillEffect = null;
		mPuzzleManager.TimeScale = 1f;
		mPuzzleManager.WaitStateFinished();
		mSkillBallExist = false;
	}

	private IEnumerator SkillRecoverMoves(int amount)
	{
		yield return new WaitForSeconds(0.5f);
		mPuzzleHud.StartRecoveryEffect(amount, amount * 3);
		yield return new WaitForSeconds(1.5f);
		Def.STAGE_LOSE_CONDITION loseType = mGame.mCurrentStage.LoseType;
		if (loseType != 0 && loseType == Def.STAGE_LOSE_CONDITION.TIME)
		{
			mPuzzleManager.AddTime(amount * 3);
		}
		else
		{
			mPuzzleManager.AddMoves(amount);
		}
	}

	private IEnumerator SkillNone(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	public IEnumerator SkillGimmickBreak(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_GimmickBreak(targetTile, ref data.area);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	public IEnumerator SkillGimmickBreak2(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		int param = data.param0;
		if (param == 129 || param == 130)
		{
			mPuzzleManager.Skill_GimmickBreak2(targetTile, ref data.area);
			Vector2 effectPos = mPuzzleManager.GetDefaultCoords(targetTile.TilePos);
			Util.CreateOneShotAnime("SkillHIT", "EFFECT_STRIPE_EXPLOSION_H", mPuzzleRoot, new Vector3(effectPos.x, effectPos.y, -3f), Vector3.one, 45f, 0f);
			Util.CreateOneShotAnime("SkillHIT", "EFFECT_STRIPE_EXPLOSION_H", mPuzzleRoot, new Vector3(effectPos.x, effectPos.y, -3f), Vector3.one, 135f, 0f);
			mGame.PlaySe("SE_WAVE", -1);
		}
		else
		{
			mPuzzleManager.Skill_GimmickBreak2(targetTile, ref data.area);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillTransformBomb(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformBomb(targetTile, data.param0);
	}

	private IEnumerator SkillTransformWave(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformWave(targetTile, data.param0);
	}

	private IEnumerator SkillTransformRainbow(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformRainbow(targetTile, data.param0);
	}

	private IEnumerator SkillTransformPaint(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.4f);
		mPuzzleManager.Skill_TransformPaint(targetTile, data.param0);
	}

	private IEnumerator SkillRandomCrush(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_RandomCrush(targetTile, data.param0, data.param1);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillTryLuckBreakLineH(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.4f);
		bool success = UnityEngine.Random.Range(0, 100) < data.param0;
		if (success)
		{
			Util.CreateOneShotAnime("SkillHIT", "TEXT_SKILL_HIT", mSkillParent, new Vector3(0f, -150f, Def.HUD_Z - 40f), Vector3.one, 0f, 0f);
			mGame.PlaySe("SE_SKILL_SUCCESS", 2);
		}
		else
		{
			Util.CreateOneShotAnime("SkillMISS", "TEXT_SKILL_MISS", mSkillParent, new Vector3(0f, -150f, Def.HUD_Z - 40f), Vector3.one, 0f, 0f);
			mGame.PlaySe("SE_NEGATIVE", 2);
		}
		yield return new WaitForSeconds(1.3333334f);
		if (success)
		{
			mPuzzleManager.Skill_BreakLineH(targetTile);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillTryLuckBreakLineV(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.4f);
		bool success = UnityEngine.Random.Range(0, 100) < data.param0;
		if (success)
		{
			Util.CreateOneShotAnime("SkillHIT", "TEXT_SKILL_HIT", mSkillParent, new Vector3(0f, -150f, Def.HUD_Z - 40f), Vector3.one, 0f, 0f);
			mGame.PlaySe("SE_SKILL_SUCCESS", 2);
		}
		else
		{
			Util.CreateOneShotAnime("SkillMISS", "TEXT_SKILL_MISS", mSkillParent, new Vector3(0f, -150f, Def.HUD_Z - 40f), Vector3.one, 0f, 0f);
			mGame.PlaySe("SE_NEGATIVE", 2);
		}
		yield return new WaitForSeconds(1.3333334f);
		if (success)
		{
			mPuzzleManager.Skill_BreakLineV(targetTile);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillRandomRandomBreakLineH(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		int total = data.param2 + data.param3;
		if (UnityEngine.Random.Range(0, total) < data.param2)
		{
			mPuzzleManager.Skill_RandomBreakLineH(data.param0);
		}
		else
		{
			mPuzzleManager.Skill_RandomBreakLineH(data.param1);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillRandomBreakLineH(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_RandomBreakLineH(data.param0);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillRandomBreakLineV(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_RandomBreakLineV(data.param0);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillBreakLineVH(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_BreakLineVH(targetTile);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillBreakLineWaveBomb(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_BreakLineWaveBomb(targetTile);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillShuffle(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_Shuffle(targetTile);
		mPuzzleManager.ReserveSkillBallCrush(targetTile);
	}

	private IEnumerator SkillTransformWaveOrBomb(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformWaveOrBomb(targetTile, data.param0);
	}

	private IEnumerator SkillTransformBombAndWave(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformBombAndWave(targetTile, data.param0, data.param1);
	}

	private IEnumerator SkillTransformPaintAndWave(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformPaintAndWave(targetTile, data.param0, data.param1);
	}

	private IEnumerator SkillTransformPaintAndBomb(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformPaintAndBomb(targetTile, data.param0, data.param1);
	}

	private IEnumerator SkillRandomColorCrush(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mGame.PlaySe("SE_RAINBOW", 2);
		mPuzzleManager.SpecialRainbow(targetTile.TilePos, Def.TILE_KIND.NONE);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillRandomColorPaint(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mGame.PlaySe("SE_RAINBOW", 2);
		mPuzzleManager.SpecialPaint(targetTile.TilePos, Def.TILE_KIND.NONE, Def.TILE_KIND.NONE);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	public IEnumerator SkillSwap(object[] parameters)
	{
		PuzzleTile targetTile1 = (PuzzleTile)parameters[0];
		PuzzleTile targetTile2 = (PuzzleTile)parameters[1];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[2];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_Swap(targetTile1, targetTile2);
		yield return new WaitForSeconds(1.5f);
		mPuzzleManager.BeginTapCrush(targetTile2);
	}

	private IEnumerator SkillTransformAreaColor(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.4f);
		mPuzzleManager.Skill_TransformAreaColor(targetTile, ref data.area, (Def.TILE_KIND)data.param0);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillGimmickBreak2SailorV0(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_GimmickBreak2(targetTile, ref data.area, 0.04f);
		yield return new WaitForSeconds(0.12f);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillGimmickBreak2SailorV1(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		Util.CreateOneShotAnime("SkillHIT", "EFFECT_MINAKO2_V_SLASH", mSkillParent, new Vector3(0f, 0f, Def.HUD_Z - 40f), Vector3.one, 0f, 0f);
		yield return new WaitForSeconds(1.6666666f);
		mPuzzleManager.Skill_GimmickBreak2(targetTile, ref data.area, 0.04f);
		yield return new WaitForSeconds(0.36f);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillHalloweenHaruka0(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank2 = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		int[][] ratioTbl = new int[2][]
		{
			new int[5] { 50, 10, 18, 20, 2 },
			new int[5] { 5, 50, 15, 20, 10 }
		};
		rank2--;
		switch (SkillEffectLottery(ref ratioTbl[rank2]))
		{
		default:
			mPuzzleManager.Skill_GimmickBreak(targetTile, ref Def.SKILL_SAILOR_URANUS_00_AREA);
			break;
		case 1:
			mPuzzleManager.Skill_GimmickBreak(targetTile, ref Def.SKILL_SAILOR_URANUS_01_AREA);
			break;
		case 2:
			mPuzzleManager.Skill_RandomCrush(targetTile, 7, 2);
			break;
		case 3:
			mPuzzleManager.Skill_Shuffle(targetTile);
			break;
		case 4:
			mGame.PlaySe("SE_RAINBOW", 2);
			mPuzzleManager.SpecialRainbow(targetTile.TilePos, Def.TILE_KIND.NONE);
			break;
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillHalloweenHaruka1(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank2 = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		int[][] ratioTbl = new int[2][]
		{
			new int[4] { 75, 15, 8, 2 },
			new int[4] { 5, 75, 10, 10 }
		};
		rank2--;
		switch (SkillEffectLottery(ref ratioTbl[rank2]))
		{
		default:
			mPuzzleManager.Skill_TransformBomb(targetTile, 1);
			break;
		case 1:
			mPuzzleManager.Skill_TransformBomb(targetTile, 2);
			break;
		case 2:
			mPuzzleManager.Skill_TransformBomb(targetTile, 3);
			break;
		case 3:
			mPuzzleManager.Skill_TransformRainbow(targetTile, 1);
			break;
		}
	}

	private IEnumerator SkillHalloweenMichiru0(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank2 = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		int[][] ratioTbl = new int[2][]
		{
			new int[5] { 50, 10, 18, 20, 2 },
			new int[5] { 10, 45, 20, 15, 10 }
		};
		rank2--;
		switch (SkillEffectLottery(ref ratioTbl[rank2]))
		{
		default:
			mPuzzleManager.Skill_GimmickBreak(targetTile, ref Def.SKILL_SAILOR_NEPTUNE_00_AREA);
			break;
		case 1:
			mPuzzleManager.Skill_GimmickBreak(targetTile, ref Def.SKILL_SAILOR_NEPTUNE_01_AREA);
			break;
		case 2:
			mPuzzleManager.Skill_RandomBreakLineH(1);
			break;
		case 3:
			mPuzzleManager.Skill_Shuffle(targetTile);
			break;
		case 4:
			mGame.PlaySe("SE_RAINBOW", 2);
			mPuzzleManager.SpecialPaint(targetTile.TilePos, Def.TILE_KIND.NONE, Def.TILE_KIND.NONE);
			break;
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillHalloweenMichiru1(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank2 = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		int[][] ratioTbl = new int[2][]
		{
			new int[4] { 75, 15, 8, 2 },
			new int[4] { 5, 75, 10, 10 }
		};
		rank2--;
		switch (SkillEffectLottery(ref ratioTbl[rank2]))
		{
		default:
			mPuzzleManager.Skill_TransformWave(targetTile, 1);
			break;
		case 1:
			mPuzzleManager.Skill_TransformWave(targetTile, 2);
			break;
		case 2:
			mPuzzleManager.Skill_TransformWave(targetTile, 3);
			break;
		case 3:
			mPuzzleManager.Skill_TransformPaint(targetTile, 1);
			break;
		}
	}

	private IEnumerator SkillHalloweenUsagi0(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		if (rank == 1)
		{
			mPuzzleManager.Skill_RandomColorChange(18, 0);
		}
		else
		{
			mPuzzleManager.Skill_RandomColorChange(21, 0);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillHalloweenUsagi1(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		if (rank == 1)
		{
			mPuzzleManager.Skill_RandomColorChange(8, 2);
		}
		else
		{
			mPuzzleManager.Skill_RandomColorChange(9, 3);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillHalloweenHotaru0(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_TransformWaveOrBomb(targetTile, 2);
	}

	private IEnumerator SkillHalloweenHotaru1(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		if (rank == 1)
		{
			mPuzzleManager.Skill_TransformWaveOrBomb(targetTile, 3);
		}
		else
		{
			mPuzzleManager.Skill_TransformWaveOrBomb(targetTile, 4);
		}
	}

	private IEnumerator SkillHalloweenUsagi2_0(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		if (rank == 1)
		{
			mPuzzleManager.Skill_RandomCrush(targetTile, 18, 0);
		}
		else
		{
			mPuzzleManager.Skill_RandomCrush(targetTile, 21, 0);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillHalloweenUsagi2_1(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		int rank = (int)parameters[2];
		yield return new WaitForSeconds(0.5f);
		if (rank == 1)
		{
			mPuzzleManager.Skill_RandomCrush(targetTile, 7, 5);
		}
		else
		{
			mPuzzleManager.Skill_RandomCrush(targetTile, 7, 5);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	public IEnumerator SkillGimmickBreakFailed(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		List<ByteVector2> area = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(0, 1),
			new ByteVector2(0, -1)
		};
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_GimmickBreak(targetTile, ref area);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private IEnumerator SkillRandomWave(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		int total = data.param2 + data.param3;
		if (UnityEngine.Random.Range(0, total) < data.param2)
		{
			mPuzzleManager.Skill_TransformWave(targetTile, data.param0);
		}
		else
		{
			mPuzzleManager.Skill_TransformWave(targetTile, data.param1);
		}
	}

	private IEnumerator SkillGimmickBreakAndTransformBomb(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return StartCoroutine(SkillTransformBomb(parameters));
		yield return new WaitForSeconds(1f);
		mPuzzleManager.Skill_GimmickBreak(targetTile, ref data.area);
	}

	private IEnumerator SkillGimmickBreakAndTransformWave(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return StartCoroutine(SkillTransformWave(parameters));
		yield return new WaitForSeconds(1f);
		mPuzzleManager.Skill_GimmickBreak(targetTile, ref data.area);
	}

	private IEnumerator SkillRandomCrossBreak(object[] parameters)
	{
		ByteVector2[][] StartPosTblA = new ByteVector2[4][]
		{
			new ByteVector2[2]
			{
				new ByteVector2(0, 10),
				new ByteVector2(2, 10)
			},
			new ByteVector2[2]
			{
				new ByteVector2(0, 10),
				new ByteVector2(0, 8)
			},
			new ByteVector2[2]
			{
				new ByteVector2(0, 9),
				new ByteVector2(2, 10)
			},
			new ByteVector2[2]
			{
				new ByteVector2(1, 10),
				new ByteVector2(0, 8)
			}
		};
		ByteVector2[] StartPosTblB = new ByteVector2[5]
		{
			new ByteVector2(6, 10),
			new ByteVector2(8, 10),
			new ByteVector2(10, 10),
			new ByteVector2(10, 8),
			new ByteVector2(10, 6)
		};
		List<ByteVector2> targets = new List<ByteVector2>();
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		int lineNum = data.param0;
		for (int k = 0; k < lineNum; k++)
		{
			targets.Clear();
			float rotate;
			if (k % 2 == 0)
			{
				ByteVector2[] startPosSubTbl = StartPosTblA[UnityEngine.Random.Range(0, StartPosTblA.Length)];
				ByteVector2 startPos2 = startPosSubTbl[k / 2];
				for (int j = 0; j < 11; j++)
				{
					targets.Add(startPos2 + new ByteVector2((sbyte)j, (sbyte)(-j)));
				}
				rotate = -45f;
			}
			else
			{
				ByteVector2 startPos2 = StartPosTblB[UnityEngine.Random.Range(0, StartPosTblB.Length)];
				for (int i = 0; i < 11; i++)
				{
					targets.Add(startPos2 + new ByteVector2((sbyte)(-i), (sbyte)(-i)));
				}
				rotate = 45f;
			}
			mPuzzleManager.Skill_GimmickBreak2(null, ref targets, 0.04f, (float)k * 0.44f);
			Vector2 effectPos = mPuzzleManager.GetDefaultCoords(new IntVector2(targets[0].x, targets[0].y));
			Util.CreateOneShotAnime("SkillHIT", "EFFECT_STRIPE_EXPLOSION_H", mPuzzleRoot, new Vector3(effectPos.x, effectPos.y, -3f), Vector3.one, rotate, (float)k * 0.44f);
			mGame.PlaySe("SE_WAVE", -1, 1f, (float)k * 0.44f);
		}
		mPuzzleManager.BeginTapCrush(targetTile);
		yield break;
	}

	private IEnumerator SkillRandomColorChange(object[] parameters)
	{
		PuzzleTile targetTile = (PuzzleTile)parameters[0];
		Def.SkillFuncData data = (Def.SkillFuncData)parameters[1];
		yield return new WaitForSeconds(0.5f);
		mPuzzleManager.Skill_RandomColorChange(data.param0, data.param1);
		mPuzzleManager.BeginTapCrush(targetTile);
	}

	private int SkillEffectLottery(ref int[] ratioTbl)
	{
		int num = 0;
		for (int i = 0; i < ratioTbl.Length; i++)
		{
			num += ratioTbl[i];
		}
		int num2 = UnityEngine.Random.Range(0, num);
		int result = 0;
		num = 0;
		for (int j = 0; j < ratioTbl.Length; j++)
		{
			num += ratioTbl[j];
			if (num2 < num)
			{
				result = j;
				break;
			}
		}
		return result;
	}

	private IEnumerator ContinueTutorial00()
	{
		StartCoroutine(GrayOut());
		StopBGM();
		GameObject go = SkillEffectPlayer.CreateDemoCamera();
		Util.CreateOneShotAnime("TutorialContinue", "EFFECT_TUTORIAL_CONTINUE", go, new Vector3(0f, 0f, Def.HUD_Z - 50f), Vector3.one, 0f, 0f);
		yield return new WaitForSeconds(2f);
		UnityEngine.Object.Destroy(go);
		StartCoroutine(GrayOut(0.8f, Color.black, 1f));
		continueDemoFinished = false;
		StoryDemoManager storyDemo = Util.CreateGameObject("StoryDemo", mRoot).AddComponent<StoryDemoManager>();
		storyDemo.ChangeStoryDemo("Data/Story/TUTORIAL_000.bin", delegate
		{
			continueDemoFinished = true;
		}, false, false);
		while (!continueDemoFinished)
		{
			yield return 0;
		}
		mPuzzleManager.WaitStateFinished();
	}

	private IEnumerator ContinueTutorial01()
	{
		yield return 0;
		StartCoroutine(GrayOut(0.8f, Color.black, 1f));
		continueDemoFinished = false;
		StoryDemoManager storyDemo = Util.CreateGameObject("StoryDemo", mRoot).AddComponent<StoryDemoManager>();
		storyDemo.ChangeStoryDemo("Data/Story/TUTORIAL_001.bin", delegate
		{
			continueDemoFinished = true;
		}, false, false);
		while (!continueDemoFinished)
		{
			yield return 0;
		}
		StartCoroutine(GrayIn());
		mPuzzleManager.TutorialMessageFinish();
		mPuzzleManager.WaitStateFinished();
		PlayBGM();
		mGame.SetMusicVolume(1f, 0);
	}

	public override void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		StageDataLoadFinished = false;
		if (!string.IsNullOrEmpty(STAGE_DISPLAY_NUMBER))
		{
			mGame.mCurrentStage.DisplayStageNumber = STAGE_DISPLAY_NUMBER;
		}
		if (STAGE_LIMIT_TIMES_OR_MOVES > 0)
		{
			Def.STAGE_LOSE_CONDITION loseType = mGame.mCurrentStage.LoseType;
			if (loseType != 0 && loseType == Def.STAGE_LOSE_CONDITION.TIME)
			{
				mGame.mCurrentStage.OverwriteLimitTimeSec = STAGE_LIMIT_TIMES_OR_MOVES;
			}
			else
			{
				mGame.mCurrentStage.OverwriteLimitMoves = STAGE_LIMIT_TIMES_OR_MOVES;
			}
		}
		StageDataLoadFinished = true;
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
		base.Start();
		mLoseDialog = null;
		mWinDialog = null;
		mFinishDialog = null;
		mStartDialog = null;
		mConfirmDialog = null;
		mLabyrinthResultDialog = null;
		mSuspend = false;
		mCollectPresent = false;
		mReplayExit = false;
		mContinueCount = 0;
		mSkillBallExist = false;
		mMissionGaugeUpDialog = null;
		GameObject particle = GameMain.GetParticle("Particles/DummyVerts");
		particle.transform.parent = mRoot.transform;
		particle.layer = mRoot.layer;
		particle.transform.localScale = Vector3.one;
		mPSforAdreno = particle.GetComponent<ParticleSystem>();
		mPSforAdreno.loop = false;
		mPSforAdreno.transform.localPosition = new Vector3(0f, 0f, 0f);
		mPSforAdreno.Stop();
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
	}

	private void OnStageError()
	{
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("StageData_Error_Desc"));
		mConfirmDialog.Init(Localization.Get("StageData_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mConfirmDialog.SetClosedCallback(OnStageErrorDialogClosed);
		mState.Change(STATE.WAIT);
	}

	private void OnStageErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.END);
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished() && mGameState.IsFadeFinished)
			{
				if (mGame.mCurrentStage == null)
				{
					mState.Change(STATE.STAGE_ERROR);
				}
				else
				{
					mState.Change(STATE.TRANSFORM);
				}
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.STAGE_ERROR:
			OnStageError();
			break;
		case STATE.TRANSFORM:
			if (mGame.mReplay || !mPartner.mData.transform || mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
			{
				mState.Change(STATE.READY);
			}
			else
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.READY:
			if (mGame.mPlayer.Data.mMugenHeart != Def.MUGEN_HEART_STATUS.LAST_CHECK)
			{
				PlayBGM();
				mGame.SetMusicVolume(1f, 0);
				if (mGame.mReplay)
				{
					mStageInfoDialog = Util.CreateGameObject("StageInfoDialog", mRoot).AddComponent<StageInfoDialog>();
					mStageInfoDialog.Init(true);
					mStageInfoDialog.SetClosedCallback(OnStageInfoDialogClosed);
					mStageInfoDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
					StartCoroutine(GrayOut());
				}
				else
				{
					mStartDialog = Util.CreateGameObject("StartDialog", mRoot).AddComponent<StageStartDialog>();
					mStartDialog.SetClosedCallback(OnStartDialogClosed);
				}
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.LAST_CHECK_WAIT;
				mState.Change(STATE.NETWORK_MAINTENANCE);
			}
			break;
		case STATE.START:
			mPuzzleManager.SetPlayStart();
			if (mWaitPlusChance)
			{
				mState.Change(STATE.PLUSCHANCE);
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.PLUSCHANCE:
		{
			if (!mPuzzleManager.mEnablePlusChance)
			{
				break;
			}
			DateTime dateTime2 = DateTimeUtil.Now();
			int num3 = -1;
			for (int k = 0; k < mGame.SegmentProfile.ChanceList.Count; k++)
			{
				if (mGame.SegmentProfile.ChanceList != null && k < mGame.SegmentProfile.ChanceList.Count)
				{
					ChanceCommInfo common2 = mGame.SegmentProfile.ChanceList[k].Common;
					if (!(dateTime2 < common2.StartTime) && !(common2.EndTime < dateTime2) && mGame.SegmentProfile.ChanceList[k].Data != null && 1 < mGame.SegmentProfile.ChanceList[k].Data.Count && mGame.SegmentProfile.ChanceList[k].Data[1].IsEnable)
					{
						num3 = k;
						break;
					}
				}
			}
			if (num3 == -1)
			{
				StartCoroutine(GrayIn());
				ResourceManager.UnloadScriptableObject("SPECIAL_CHANCE");
				mWaitPlusChance = false;
				mPuzzleManager.WaitStateFinished();
				mState.Change(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.PLUSCHANCE_WAIT);
			}
			break;
		}
		case STATE.PLUSCHANCE_WAIT:
		{
			if (!mWaitPlusChance)
			{
				break;
			}
			if (mGame.mPlayer.Data.ChanceCount == null)
			{
				mGame.mPlayer.Data.ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
			}
			if (!mGame.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS))
			{
				mGame.mPlayer.Data.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.PLUS, 0);
			}
			Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> chanceCount;
			Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> dictionary = (chanceCount = mGame.mPlayer.Data.ChanceCount);
			SpecialChanceConditionSet.CHANCE_MODE key;
			SpecialChanceConditionSet.CHANCE_MODE key2 = (key = SpecialChanceConditionSet.CHANCE_MODE.PLUS);
			short num = chanceCount[key];
			dictionary[key2] = (short)(num + 1);
			mGame.Save();
			mWaitPlusChance = false;
			mPuzzleManager.mEnablePlusChance = false;
			BoosterChanceDialog dialog = null;
			if (BoosterChanceDialog.TryMake(SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP, mRoot, out dialog, default(SpecialChanceConditionItem.Option)) || BoosterChanceDialog.TryMake(SpecialChanceConditionSet.CHANCE_MODE.PLUS, mRoot, out dialog, default(SpecialChanceConditionItem.Option)))
			{
				mState.Change(STATE.WAIT);
				StartCoroutine(GrayOut());
				dialog.SetClosedEvent(delegate(BoosterChanceDialog.SELECT_ITEM i)
				{
					StartCoroutine(GrayIn());
					mPuzzleManager.WaitStateFinished();
					mState.Change(STATE.MAIN);
					BoosterChanceDialog.SELECT_ITEM sELECT_ITEM = i;
					if (sELECT_ITEM == BoosterChanceDialog.SELECT_ITEM.FIRST_CONSUME)
					{
						SingletonMonoBehaviour<PuzzleManager>.Instance.SetColorModifyMode(true);
					}
					sELECT_ITEM = i;
					if (sELECT_ITEM == BoosterChanceDialog.SELECT_ITEM.CONSUME || sELECT_ITEM == BoosterChanceDialog.SELECT_ITEM.FIRST_CONSUME)
					{
						mExpandScoreUpRatio = 1f;
						if (dialog.ShopItemDetail != null)
						{
							if (dialog.ShopItemDetail.SCORE_UP_RATIO_1_5 > 0)
							{
								mExpandScoreUpRatio = 1.5f;
							}
							else if (dialog.ShopItemDetail.SCORE_UP_RATIO_2_0 > 0)
							{
								mExpandScoreUpRatio = 2f;
							}
							else if (dialog.ShopItemDetail.SCORE_UP_RATIO_2_5 > 0)
							{
								mExpandScoreUpRatio = 2.5f;
							}
							else if (dialog.ShopItemDetail.SCORE_UP_RATIO_3_0 > 0)
							{
								mExpandScoreUpRatio = 3f;
							}
						}
						if (mExpandScoreUpRatio > 1f)
						{
							SingletonMonoBehaviour<PuzzleManager>.Instance.mUseScoreUpChance = true;
						}
					}
				});
			}
			else
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.MAIN);
				mPuzzleManager.WaitStateFinished();
			}
			ResourceManager.UnloadScriptableObject("SPECIAL_CHANCE");
			break;
		}
		case STATE.SPECIALCHANCE:
		{
			mGame.mPlayer.Data.UseSpecialChance = false;
			mGame.mPlayer.Data.UsePlusChance = false;
			mGame.mPlayer.Data.UseScoreUpChance = false;
			mGame.mPlayer.Data.UseContinueChance = false;
			mGame.mPlayer.Data.UseStarGetChallenge = false;
			mEnablePlusChance = false;
			DateTime dateTime = DateTimeUtil.Now();
			int num2 = -1;
			for (int j = 0; j < mGame.SegmentProfile.ChanceList.Count; j++)
			{
				if (mGame.SegmentProfile.ChanceList == null || j >= mGame.SegmentProfile.ChanceList.Count)
				{
					continue;
				}
				ChanceCommInfo common = mGame.SegmentProfile.ChanceList[j].Common;
				if (!(dateTime < common.StartTime) && !(common.EndTime < dateTime) && mGame.SegmentProfile.ChanceList[j].Data != null)
				{
					if (1 < mGame.SegmentProfile.ChanceList[j].Data.Count || mGame.SegmentProfile.ChanceList[j].Data[1].IsEnable)
					{
						mEnablePlusChance = true;
					}
					if (0 < mGame.SegmentProfile.ChanceList[j].Data.Count && mGame.SegmentProfile.ChanceList[j].Data[0].IsEnable)
					{
						num2 = j;
						break;
					}
				}
			}
			if (num2 == -1)
			{
				mWaitPlusChance = mGame.mUseBoosters.Count != 0;
				if (!mWaitPlusChance)
				{
					if (mChancePlayEvent != null)
					{
						mChancePlayEvent();
					}
					else
					{
						StartCoroutine(GrayIn());
						mState.Change(STATE.START);
					}
					ResourceManager.UnloadScriptableObject("SPECIAL_CHANCE");
					mPuzzleManager.WaitStateFinished();
				}
				else
				{
					mState.Change(STATE.START);
				}
			}
			else if (!GameMain.mChargePriceCheckSucceed)
			{
				mWaitPlusChance = false;
				mGame.Network_ChargePriceCheck(mGame.SegmentProfile.ChanceList[num2].Common.Period);
				if (mChancePlayEvent != null)
				{
					mChancePlayEvent();
				}
				else
				{
					mState.Change(STATE.START);
				}
			}
			else
			{
				mState.Change(STATE.SPECIALCHANCE_WAIT);
			}
			break;
		}
		case STATE.SPECIALCHANCE_WAIT:
		{
			if (mGame.mPlayer.Data.ChanceCount == null)
			{
				mGame.mPlayer.Data.ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
			}
			if (!mGame.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL))
			{
				mGame.mPlayer.Data.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL, 0);
			}
			Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> chanceCount2;
			Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> dictionary2 = (chanceCount2 = mGame.mPlayer.Data.ChanceCount);
			SpecialChanceConditionSet.CHANCE_MODE key;
			SpecialChanceConditionSet.CHANCE_MODE key3 = (key = SpecialChanceConditionSet.CHANCE_MODE.SPECIAL);
			short num = chanceCount2[key];
			dictionary2[key3] = (short)(num + 1);
			if (mGame.mUseBoosters.Count == 0)
			{
				if (mEnablePlusChance)
				{
					if (mGame.mPlayer.Data.ChanceCount == null)
					{
						mGame.mPlayer.Data.ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
					}
					if (!mGame.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS))
					{
						mGame.mPlayer.Data.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.PLUS, 0);
					}
					Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> chanceCount3;
					Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> dictionary3 = (chanceCount3 = mGame.mPlayer.Data.ChanceCount);
					SpecialChanceConditionSet.CHANCE_MODE key4 = (key = SpecialChanceConditionSet.CHANCE_MODE.PLUS);
					num = chanceCount3[key];
					dictionary3[key4] = (short)(num + 1);
				}
				mGame.Save();
				mWaitPlusChance = false;
				BoosterChanceDialog result = null;
				if (!BoosterChanceDialog.TryMake(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL, mRoot, out result, default(SpecialChanceConditionItem.Option)) || result == null)
				{
					if (mChancePlayEvent != null)
					{
						mChancePlayEvent();
					}
					else
					{
						mState.Change(STATE.START);
					}
				}
				else
				{
					if (mChancePrevOpen != null)
					{
						mChancePrevOpen();
					}
					mState.Change(STATE.WAIT);
					result.SetClosedEvent(delegate(BoosterChanceDialog.SELECT_ITEM i)
					{
						StartCoroutine(GrayIn());
						if (mChancePlayEvent != null)
						{
							mChancePlayEvent();
						}
						else
						{
							mState.Change(STATE.START);
						}
						if (i == BoosterChanceDialog.SELECT_ITEM.FIRST_CONSUME)
						{
							mPuzzleManager.SetColorModifyMode(true);
						}
					});
				}
				ResourceManager.UnloadScriptableObject("SPECIAL_CHANCE");
			}
			else
			{
				mGame.Save();
				mWaitPlusChance = true;
				if (mChancePlayEvent != null)
				{
					mChancePlayEvent();
				}
				else
				{
					mState.Change(STATE.START);
				}
			}
			break;
		}
		case STATE.MAIN:
			if (!mGame.adrenoParticled)
			{
				if (!mPSforAdreno.isPlaying)
				{
					mPSforAdreno.Emit(25000);
				}
				mGame.adrenoParticled = true;
			}
			if (mGame.mBackKeyTrg)
			{
				if (GameMain.USE_DEBUG_DIALOG && GameMain.HIDDEN_DEBUG_BUTTON)
				{
					OnDebugPushed();
				}
				else
				{
					OnPausePushed();
				}
			}
			break;
		case STATE.REPLAY:
			mGame.mReplay = true;
			StopBGM();
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.PUZZLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		case STATE.BUY_STARGET:
			if (mState.IsChanged())
			{
				StartCoroutine(LoadStarGetStage());
			}
			else if (_stargetstage_load_finished)
			{
				mGame.mReplay = false;
				StopBGM();
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.PUZZLE);
				mState.Change(STATE.UNLOAD_WAIT);
			}
			break;
		case STATE.END:
		{
			StopBGM();
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
			{
				ISMStageData aPoppedStage;
				SMMapStageSetting aPoppedStageSetting;
				mGame.PopCurrentStage(out aPoppedStage, out aPoppedStageSetting);
			}
			if (Def.IsEventSeries(mGame.mCurrentStage.Series))
			{
				GameStateManager.GAME_STATE nextGameState = mGame.GetNextGameState();
				mGameState.SetNextGameState(nextGameState);
			}
			else
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
			}
			int mode = ((mGame.mDeviceOrientaion == ScreenOrientation.LandscapeLeft || mGame.mDeviceOrientaion == ScreenOrientation.LandscapeRight) ? 1 : 0);
			ServerCram.DisPlayMode(mode, Network.DeviceModel, 1);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		}
		case STATE.RANKUP:
			mState.Change(STATE.RANKUP_00);
			break;
		case STATE.RANKUP_00:
			if (!RankUpStageScoreStartCheck())
			{
				mState.Change(STATE.RANKUP_END);
			}
			break;
		case STATE.RANKUP_END:
			mState.Change(STATE.END);
			break;
		case STATE.PRE_NETWORK_WON:
			mState.Reset(STATE.WAIT, true);
			StartCoroutine(PreNetwork_OnWon());
			break;
		case STATE.NETWORK_WON:
			Network_OnWon();
			break;
		case STATE.NETWORK_LOST:
			Network_OnLost();
			break;
		case STATE.AUTHERROR_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Transfer_CertError_Title"), Localization.Get("Transfered_TitleBack_Desc2"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.AUTHERROR_RETURN_WAIT);
			break;
		case STATE.NETWORK_MAINTENANCE:
			mGame.mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_MAINTENANCE_WAIT);
			break;
		case STATE.NETWORK_MAINTENANCE_WAIT:
			if (GameMain.mMaintenanceProfileCheckDone)
			{
				if (GameMain.mMaintenanceProfileSucceed)
				{
					mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.LAST_CHECK_END;
					mState.Change(STATE.READY);
				}
				else
				{
					mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 19);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		}
		if (mPartner != null && !mSkillEffectPlaying)
		{
			mPartner.SetPartnerScreenPos(GetPartnerScreenBasePos(Util.ScreenOrientation));
		}
		if (mSuspend && mGame.mTouchUpTrg)
		{
			Time.timeScale = 1f;
			mSuspend = false;
			mGame.FinishPause();
		}
		if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugSkillCharge)
		{
			mPuzzleHud.SetSkillPower(100f);
			mGame.mDebugSkillCharge = false;
		}
		mState.Update();
	}

	public override IEnumerator LoadGameState()
	{
		SingletonMonoBehaviour<GameMain>.Instance.mAdvMode = false;
		Live2D.dispose();
		Live2D.init();
		mGame.StartLoading();
		if (GameMain.USE_RESOURCEDL)
		{
		}
		UIDrawCall.ReleaseInactive();
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		mGame.SwitchTileData(false);
		mGame.TileSetting_Normal();
		string bgKey2 = mGame.mCurrentStageInfo.GetStageBGKey();
		if (mGame.IsDailyChallengePuzzle)
		{
			bgKey2 = "PARTNER_BG0";
		}
		ResSsAnimation resAnime = ResourceManager.LoadSsAnimationAsync(bgKey2);
		mAnchorTop = Util.CreateAnchorWithChild("PuzzleAnchorTop", mGame.mCamera.gameObject, UIAnchor.Side.Top, mGame.mCamera).gameObject;
		mAnchorLeft = Util.CreateAnchorWithChild("PuzzleAnchorLeft", mGame.mCamera.gameObject, UIAnchor.Side.Left, mGame.mCamera).gameObject;
		mAnchorBottom = Util.CreateAnchorWithChild("PuzzleAnchorBottom", mGame.mCamera.gameObject, UIAnchor.Side.Bottom, mGame.mCamera).gameObject;
		mAnchorRight = Util.CreateAnchorWithChild("PuzzleAnchorRight", mGame.mCamera.gameObject, UIAnchor.Side.Right, mGame.mCamera).gameObject;
		yield return 0;
		mChanceAnnounceDone = false;
		mGame.mDebugForceWin = false;
		mGame.mDebugForceLose = false;
		mGame.mDebugForceNoMoreMoves = false;
		mGame.mDebugSkillCharge = false;
		mPuzzleManager = Util.CreateGameObject("PuzzleManager", base.gameObject).AddComponent<PuzzleManager>();
		mPuzzleManager.OnPuzzleStart = OnPuzzleStart;
		mPuzzleManager.OnBeforePlay = OnBeforePlay;
		mPuzzleManager.OnSkillBallTransform = OnSkillBallTransfer;
		mPuzzleManager.OnBeforePuzzleSpecialOfferCheck = OnBeforePuzzleSpecialOfferCheck;
		mPuzzleManager.OnPuzzleHudAdjust = OnPuzzleHudAdjust;
		mPuzzleManager.OnBoosterUse = OnBoosterUse;
		mPuzzleManager.OnSkillTargetSelect = OnSkillTargetSelect;
		mPuzzleManager.OnBoosterEffectOnly = OnBoosterEffectOnly;
		mPuzzleManager.OnBeginCrush = OnBeginCrush;
		mPuzzleManager.OnBossDamageOrbCrush = OnBossDamageOrbCrush;
		mPuzzleManager.OnSpecialCreateCountUp = OnSpecialCreateCountUp;
		mPuzzleManager.OnSpecialCrushCountUp = OnSpecialCrushCountUp;
		mPuzzleManager.OnSpecialCrossCountUp = OnSpecialCrossCountUp;
		mPuzzleManager.OnShuffle = OnShuffle;
		mPuzzleManager.OnCollect = OnCollect;
		mPuzzleManager.OnCombo = OnCombo;
		mPuzzleManager.OnComboFinished = OnComboFinished;
		mPuzzleManager.OnChangeMoves = OnChangeMoves;
		mPuzzleManager.OnChangeTimer = OnChangeTimer;
		mPuzzleManager.OnChangeScore = OnChangeScore;
		mPuzzleManager.OnWin = OnWin;
		mPuzzleManager.OnLose = OnLose;
		mPuzzleManager.OnClearBonusSpecialTileCrushed = OnCrearBonusSpecialTileCrushed;
		mPuzzleManager.OnClearBonusStart = OnClearBonusStart;
		mPuzzleManager.OnClearBonusStepUp = OnClearBonusStepUp;
		mPuzzleManager.OnClearBonusFinished = OnClearBonusFinished;
		mPuzzleManager.OnPuzzleFinished = OnPuzzleFinished;
		mPuzzleManager.OnSkillPushed = OnSkillPushed;
		mPuzzleManager.OnGetTutorialArrowTarget = OnGetTutorialArrowTarget;
		mPuzzleManager.OnTutorialTrigger = OnTutorialTrigger;
		mPuzzleManager.OnShakeScreen = OnShakeScreen;
		mPuzzleManager.OnAttackTargetsChanged = OnAttackTargetsChanged;
		mPuzzleManager.OnPlaceBrokenWall = OnPlaceBrokenWall;
		mPuzzleManager.OnTalismanPlaceMovableWall = OnTalismanPlaceMovableWall;
		mPuzzleManager.OnDianaStepRemainChanged = OnDianaStepRemainChanged;
		yield return 0;
		BIJImage atlas = ResourceManager.LoadImage("PUZZLE_BG").Image;
		if (mGame.mCurrentStage != null)
		{
			if (mGame.mCompanionIndex != -1)
			{
				mGame.mPlayer.AddUseCompanionCount(mGame.mCompanionIndex);
			}
			int _tgt_stage2 = 0;
			_tgt_stage2 = ((!mGame.mEventMode) ? mGame.mCurrentStage.StageNumber : Def.GetStageNoForServer(mGame.mPlayer.Data.CurrentEventID, mGame.mCurrentStage.StageNumber));
			UBAResetData(mGame.mCurrentStage.Series, _tgt_stage2);
			Def.SERIES _last_series = mGame.mPlayer.Data.LastPlaySeries;
			DateTime _play_time = DateTime.Now;
			int _last_stage = mGame.mPlayer.Data.LastPlayLevel;
			DateTime _last_time2 = mGame.mPlayer.Data.LastPlayLevelTime;
			int _sts_count2 = mGame.mPlayer.Data.LastPlayLevelCount;
			DateTime _sts_time = mGame.mPlayer.Data.LastPlayLevelStartTime;
			if (_last_series == mGame.mCurrentStage.Series && _last_stage == _tgt_stage2)
			{
				_sts_count2++;
			}
			else
			{
				_last_series = mGame.mCurrentStage.Series;
				_last_stage = _tgt_stage2;
				_last_time2 = _play_time;
				_sts_count2 = 0;
				_sts_time = _play_time;
				if (mGame.mPlayer.Data.ChanceCount == null)
				{
					mGame.mPlayer.Data.ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
				}
				if (mGame.mPlayer.Data.ChanceCount.Keys != null)
				{
					SpecialChanceConditionSet.CHANCE_MODE[] keys2 = mGame.mPlayer.Data.ChanceCount.Keys.ToArray();
					SpecialChanceConditionSet.CHANCE_MODE[] array = keys2;
					foreach (SpecialChanceConditionSet.CHANCE_MODE key2 in array)
					{
						if (mGame.mPlayer.Data.ChanceCount.ContainsKey(key2))
						{
							mGame.mPlayer.Data.ChanceCount[key2] = 0;
						}
						else
						{
							mGame.mPlayer.Data.ChanceCount.Add(key2, 0);
						}
					}
				}
				if (mGame.mPlayer.Data.ChanceDispCount == null)
				{
					mGame.mPlayer.Data.ChanceDispCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
				}
				if (mGame.mPlayer.Data.ChanceDispCount.Keys != null)
				{
					SpecialChanceConditionSet.CHANCE_MODE[] keys = mGame.mPlayer.Data.ChanceDispCount.Keys.ToArray();
					SpecialChanceConditionSet.CHANCE_MODE[] array2 = keys;
					foreach (SpecialChanceConditionSet.CHANCE_MODE key in array2)
					{
						if (mGame.mPlayer.Data.ChanceDispCount.ContainsKey(key))
						{
							mGame.mPlayer.Data.ChanceDispCount[key] = 0;
						}
						else
						{
							mGame.mPlayer.Data.ChanceDispCount.Add(key, 0);
						}
					}
				}
			}
			if (!mGame.IsDailyChallengePuzzle)
			{
				mGame.mPlayer.Data.LastPlaySeries = _last_series;
				mGame.mPlayer.Data.LastPlayLevel = _last_stage;
			}
			mGame.mPlayer.Data.LastPlayLevelTime = _play_time;
			mGame.mPlayer.Data.LastPlayLevelCount = _sts_count2;
			mGame.mPlayer.Data.LastPlayLevelStartTime = _sts_time;
			mGame.mPlayer.SetTargetStage(mGame.mCurrentStage.StageNumber);
			mGame.mConcierge.UpdateUserSegment();
			Def.DIFFICULTY_MODE difficultyMode = mGame.mConcierge.GetDifficultyMode();
			object[] param = mGame.mConcierge.GetDifficultyModeParam(difficultyMode);
			mPuzzleManager.SetDifficultyMode(difficultyMode, param);
			mGame.mConcierge.PuzzleStart(difficultyMode);
			while (resAnime.LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
			mBg = Util.CreateGameObject("PartnerBg", mAnchorTop).AddComponent<SsSprite>();
			mBg.Animation = resAnime.SsAnime;
			mBg.transform.localPosition = new Vector3(0f, -100f, Def.PARTNER_BG_Z);
			bgKey2 = mGame.mCurrentStageInfo.GetStageBGKey();
			if (mGame.IsDailyChallengePuzzle)
			{
				bgKey2 = "PARTNER_BG0";
			}
			if (string.Compare(bgKey2, "PARTNER_BG4") == 0)
			{
				mBgParticle = Util.CreateGameObject("EmitControl", mRoot).AddComponent<ParticleEmitterControl>();
				mBgParticle.Init(mBg, "Particles/PuzzleGimmick/PartnerBg4");
			}
			mPartnerIndex = mGame.mCompanionIndex;
			yield return StartCoroutine(CoLoadSkillAnimation(mPartnerIndex, false));
			mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
			mPartner.transform.localPosition = Vector3.zero;
			int level = mGame.mPlayer.GetCompanionLevel(mPartnerIndex);
			if (mGame.mPlayer.mCompanionXPTemp.Count != 0)
			{
				level = mGame.mPlayer.GetCompanionLevelTemp(mGame.mCompanionIndex);
			}
			mPartner.Init((short)mGame.mCompanionIndex, Vector3.zero, 1f, level, mRoot, false, 0);
			while (!mPartner.IsPartnerLoadFinished())
			{
				yield return 0;
			}
			mPartnerBaseScale = 1f;
			if (mGame.mCompanionData[mGame.mCompanionIndex].ModelCount > 1)
			{
				mPartnerBaseScale = 0.8f;
				mPartner.SetPartnerScreenScale(mPartnerBaseScale);
			}
			mPartner.SetPartnerScreenEnable(true);
			mPuzzleHud = Util.CreateGameObject("HudManager", base.gameObject).AddComponent<PuzzleHud>();
			bool skillEnable = ((mGame.mPlayer.mCompanionXPTemp.Count == 0) ? (mGame.mPlayer.GetCompanionLevel(mPartnerIndex) > 1) : (mGame.mPlayer.GetCompanionLevelTemp(mPartnerIndex) > 1));
			mPuzzleHud.Init(mPuzzleManager, mPartner, skillEnable, GlobalVariables.Instance.IsDailyChallengePuzzle);
			mPuzzleHud.OnBoosterPushed = OnBoosterSelect;
			mPuzzleHud.OnPausePushed = OnPausePushed;
			mPuzzleHud.OnContinueEffectFinished = OnContinueEffectFinished;
			mPuzzleHud.OnCollect = OnCollect;
			yield return 0;
			mPuzzleRoot = Util.CreateGameObject("PuzzleRoot", mAnchorTop);
			mPuzzleManager.SetRoot(mPuzzleRoot);
			mPuzzleManager.SetCamera(GameObject.Find("Camera").GetComponent<Camera>());
			mPuzzleManager.LoadSpecialCrossData("Data/SpecialCross.bin");
			mPuzzleManager.InitStage(mGame.mCurrentStageInfo);
			mPuzzleFinishCallback = GetFinishedCallback();
		}
		yield return 0;
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.DEFEAT || mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY || mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.TALISMAN)
		{
			mLoadBgmKey0 = "BGM_PUZZLE_ENEMY";
			mLoadBgmKey1 = "BGM_PUZZLE_ENEMY_CHANCE";
			if (mGame.mEventMode && mGame.mCurrentStage.EventID == 53)
			{
				mLoadBgmKey1 = "BGM_PUZZLE_ENEMY_MOONLIGHT";
			}
		}
		else
		{
			mLoadBgmKey0 = mGame.mCompanionData[mPartnerIndex].puzzleBgm;
			mLoadBgmKey1 = mGame.mCompanionData[mPartnerIndex].puzzleBgmChance;
			if (mGame.mEventMode && mGame.mCurrentStage.EventID == 53)
			{
				mLoadBgmKey1 = "BGM_PUZZLE_MOONLIGHT";
			}
		}
		ResSound resSound0 = ResourceManager.LoadSoundAsync(mLoadBgmKey0);
		ResSound resSound1 = ResourceManager.LoadSoundAsync(mLoadBgmKey1);
		FixedSprite fixedSprite4 = Util.CreateGameObject("FrameBg", mPuzzleRoot.gameObject).AddComponent<FixedSprite>();
		fixedSprite4.Init("BG_FIXED_SPRITE", "puzzle_back", Vector3.zero, atlas);
		mFrameBG = fixedSprite4;
		fixedSprite4 = Util.CreateGameObject("FrameTopL", mPuzzleRoot.gameObject).AddComponent<FixedSprite>();
		fixedSprite4.Init("BG_FIXED_SPRITE", "puzzle_frame02", Vector3.zero, atlas);
		mFrameTopL = fixedSprite4;
		fixedSprite4 = Util.CreateGameObject("FrameTopR", mPuzzleRoot.gameObject).AddComponent<FixedSprite>();
		fixedSprite4.Init("BG_FIXED_SPRITE", "puzzle_frame02", Vector3.zero, atlas);
		fixedSprite4.transform.localScale = new Vector3(-1f, 1f, 1f);
		mFrameTopR = fixedSprite4;
		fixedSprite4 = Util.CreateGameObject("FrameTopC", mPuzzleRoot.gameObject).AddComponent<FixedSprite>();
		fixedSprite4.Init("BG_FIXED_SPRITE", "puzzle_frame00", Vector3.zero, atlas);
		mFrameTopC = fixedSprite4;
		yield return 0;
		mPartnerForeground = false;
		mSkillEffectPlaying = false;
		JewelCount = 0;
		if (mGame.mCurrentMapPageData != null)
		{
			SMEventPageData eventPageData = mGame.mCurrentMapPageData as SMEventPageData;
			if (eventPageData != null)
			{
				short course;
				int mainStage;
				int subStage;
				Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out course, out mainStage, out subStage);
				PlayerEventData eventData;
				mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, eventPageData.EventID, out eventData);
				mPuzzleHud.InitEvent(eventPageData.EventSetting.EventType);
				Def.EVENT_TYPE eventType = eventPageData.EventSetting.EventType;
				if (eventType == Def.EVENT_TYPE.SM_LABYRINTH)
				{
					SMEventLabyrinthStageSetting labyrinthStageSetting = eventPageData.GetLabyrinthStageSetting(mainStage, subStage, course);
					int add_value = eventData.LabyrinthData.JewelGetChanceAddCount;
					if (GameMain.USE_DEBUG_DIALOG && mGame.mIsForceJewelGetChance)
					{
						add_value = 100;
						eventData.LabyrinthData.NoNextLottery = false;
					}
					bool isForceJewelGetChance = false;
					if (!eventData.LabyrinthData.DoneForceJewelGetChance)
					{
						int clearedCount = eventData.LabyrinthData.GetClearedStageCount();
						if (clearedCount >= 1)
						{
							add_value = 100;
							isForceJewelGetChance = true;
							eventData.LabyrinthData.DoneForceJewelGetChance = true;
						}
					}
					bool doLottery = true;
					if (!isForceJewelGetChance && eventData.LabyrinthData.NoNextLottery)
					{
						doLottery = false;
					}
					mIsJewelGetChancePlay = false;
					if (doLottery)
					{
						int jewelGetRate = labyrinthStageSetting.ChanceFreq;
						if (Util.CommonLottery(jewelGetRate + add_value, 100))
						{
							mIsJewelGetChancePlay = true;
							mIsJewelGetChance = true;
							mJewel1Num = labyrinthStageSetting.ChanceItemLimit_1;
							mJewel3Num = labyrinthStageSetting.ChanceItemLimit_3;
							mJewel5Num = labyrinthStageSetting.ChanceItemLimit_5;
							mJewel9Num = labyrinthStageSetting.ChanceItemLimit_9;
							if (!isForceJewelGetChance)
							{
								eventData.LabyrinthData.ResetJewelGetChanceCount();
							}
						}
						else
						{
							eventData.LabyrinthData.AddJewelGetChanceCount();
						}
					}
				}
			}
		}
		MissionManager.InitCountPuzzle();
		while (resSound0.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (resSound1.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		if (resSound0.mAudioClip == null || resSound1.mAudioClip == null)
		{
			mBGMLoadError = true;
		}
		if (GameMain.USE_RESOURCEDL)
		{
			ResourceManager.UnloadAssetBundles();
		}
		mLazerEffects = Util.CreateGameObject("LazerRenderer", mRoot).AddComponent<LazerRenderer>();
		mLazerEffects.transform.localPosition = new Vector3(0f, 0f, -10f);
		yield return 0;
		ResScriptableObject resSpChance = ResourceManager.LoadScriptableObjectAsync("SPECIAL_CHANCE");
		while (resSpChance.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		if (GameMain.USE_DEBUG_DIALOG && !GameMain.HIDDEN_DEBUG_BUTTON)
		{
			UIButton button = Util.CreateJellyImageButton(atlas: ResourceManager.LoadImage("HUD").Image, name: "DebugButton", parent: mAnchorTop);
			Util.SetImageButtonInfo(button, "button_debug", "button_debug", "button_debug", 50, new Vector3(-140f, -40f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnDebugPushed", UIButtonMessage.Trigger.OnClick);
			Color col = Color.white;
			if (mPuzzleManager.GetDifficultyMode() == Def.DIFFICULTY_MODE.COLOR_MODIFY_POSI)
			{
				col = Color.red;
			}
			else if (mPuzzleManager.GetDifficultyMode() == Def.DIFFICULTY_MODE.FILL_MATCH)
			{
				col = Color.blue;
			}
			if (ResourceManager.mABLoadError)
			{
				col = ResourceManager.mDebugButtonColor;
			}
			button.disabledColor = col;
			button.defaultColor = col;
			button.hover = col;
			button.pressed = col;
			button.UpdateColor(true);
		}
		SetLayout(Util.ScreenOrientation);
		if (mPartner.mData.transform && !mGame.mReplay && mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
		{
			StartCoroutine(PartnerTransform());
		}
		mGame.FinishLoading();
		LoadGameStateFinished();
		yield return 0;
	}

	public void OnDebugPushed()
	{
		ResourceManager.mABLoadError = false;
		ResourceManager.mDebugButtonColor = Color.white;
		Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
	}

	public override IEnumerator UnloadGameState()
	{
		mGame.mPlayer.SetTargetStage(0);
		ResourceManager.UnloadSound(mLoadBgmKey0);
		ResourceManager.UnloadSound(mLoadBgmKey1);
		UnityEngine.Object.Destroy(mPartner.gameObject);
		UnityEngine.Object.Destroy(mAnchorTop);
		UnityEngine.Object.Destroy(mAnchorLeft);
		UnityEngine.Object.Destroy(mAnchorBottom);
		UnityEngine.Object.Destroy(mAnchorRight);
		mBg = null;
		mAnchorTop = null;
		mAnchorLeft = null;
		mAnchorBottom = null;
		mAnchorRight = null;
		if (mBgParticle != null)
		{
			UnityEngine.Object.Destroy(mBgParticle.gameObject);
		}
		yield return 0;
		UnityEngine.Object.Destroy(mPuzzleHud.gameObject);
		yield return 0;
		yield return 0;
		mGame.StopSeAll();
		ResourceManager.UnloadSsAnimationAll();
		ResourceManager.UnloadImageAll();
		ResourceManager.UnloadSoundAll();
		ResourceManager.UnloadScriptableObjectAll();
		mGame.UnloadNotPreloadSoundResources(true);
		yield return 0;
		UnloadGameStateFinished();
		yield return 0;
	}

	private void PlayBGM()
	{
		if (!mBGMLoadError)
		{
			bool flag = mPuzzleHud.IsChance();
			if (mPuzzleManager.IsGameFinished())
			{
				flag = true;
			}
			string text = ((!flag) ? mLoadBgmKey0 : mLoadBgmKey1);
			if (string.Compare(mBGMKey, text) != 0)
			{
				mGame.PlayMusic(text, 0, true, false, true);
				mBGMKey = text;
			}
		}
	}

	private void StopBGM()
	{
		mGame.StopMusic(0, 0.2f);
		mBGMKey = string.Empty;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		float num = (float)mGame.mRoot.manualHeight / 1136f;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			float num5 = 0f - (Def.PUZZLE_PORTRAIT_ORIGN_Y * num - Def.PUZZLE_PORTRAIT_ORIGN_Y);
			mPuzzleRoot.transform.parent = mAnchorTop.transform;
			mPuzzleRoot.transform.localPosition = new Vector3(0f, Def.PUZZLE_PORTRAIT_ORIGN_Y - num5 / 2f, 0f);
			float num6 = 8f;
			mFrameBG.ChangeSpriteSize(new Vector2(vector.x + num6, Def.PUZZLE_PORTRAIT_FRAME_SIZE.y * num + num6), new Vector2(760f, 760f));
			mFrameBG.transform.localPosition = new Vector3(0f, 0f - num6, Def.PUZZLE_BG_Z);
			mFrameTopL.transform.localPosition = new Vector3(-250f, (Def.PUZZLE_PORTRAIT_FRAME_SIZE.y + num5) / 2f, Def.PUZZLE_FRAME_Z);
			mFrameTopR.transform.localPosition = new Vector3(250f, (Def.PUZZLE_PORTRAIT_FRAME_SIZE.y + num5) / 2f, Def.PUZZLE_FRAME_Z);
			mFrameTopC.transform.localPosition = new Vector3(0f, (Def.PUZZLE_PORTRAIT_FRAME_SIZE.y + num5) / 2f, Def.PUZZLE_FRAME_Z - 0.1f);
			mFrameTopL.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			mFrameTopR.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			mFrameTopC.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			mFrameTopC.ChangeSprite("puzzle_frame00");
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			float num2 = 0f - (Def.PUZZLE_LANDSCAPE_ORIGN_X * num - Def.PUZZLE_LANDSCAPE_ORIGN_X);
			float num3 = 100f * num - 100f;
			mPuzzleRoot.transform.parent = mAnchorLeft.transform;
			mPuzzleRoot.transform.localPosition = new Vector3(Def.PUZZLE_LANDSCAPE_ORIGN_X - num2 / 2f, num3, 0f);
			float num4 = 8f;
			mFrameBG.ChangeSpriteSize(new Vector2(Def.PUZZLE_LANDSCAPE_FRAME_SIZE.x * num + num4, vector.y + num4), new Vector2(760f, 760f));
			mFrameBG.transform.localPosition = new Vector3(0f, 0f - num3, Def.PUZZLE_BG_Z);
			mFrameTopL.transform.localPosition = new Vector3((0f - (Def.PUZZLE_LANDSCAPE_FRAME_SIZE.x - num2)) / 2f, -250f, Def.PUZZLE_FRAME_Z);
			mFrameTopR.transform.localPosition = new Vector3((0f - (Def.PUZZLE_LANDSCAPE_FRAME_SIZE.x - num2)) / 2f, 250f, Def.PUZZLE_FRAME_Z);
			mFrameTopC.transform.localPosition = new Vector3((0f - (Def.PUZZLE_LANDSCAPE_FRAME_SIZE.x - num2)) / 2f, 0f, Def.PUZZLE_FRAME_Z - 0.1f);
			mFrameTopL.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			mFrameTopR.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			mFrameTopC.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
			mFrameTopC.ChangeSprite("puzzle_frame01");
			break;
		}
		}
		if (!mRankingClosing)
		{
			if (mSocialWinRanking != null)
			{
				UnityEngine.Object.Destroy(mSocialWinRanking.gameObject);
				mSocialWinRanking = null;
				SocialRanking();
			}
			if (mSocialLoseRanking != null)
			{
				UnityEngine.Object.Destroy(mSocialLoseRanking.gameObject);
				mSocialLoseRanking = null;
				SocialLoseRanking();
			}
		}
		SetLayoutPartnerBg(o);
		mPuzzleHud.SetLayout(o);
	}

	private Vector3 GetPartnerScreenBasePos(ScreenOrientation o)
	{
		if (mPartnerForeground)
		{
			return new Vector3(0f, 0f, -20f);
		}
		Vector3 result = Vector3.zero;
		float num = (float)mGame.mRoot.manualHeight / 1136f;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			float num2 = (0f - (Def.PUZZLE_PORTRAIT_ORIGN_Y * num - Def.PUZZLE_PORTRAIT_ORIGN_Y)) / 2f;
			result = new Vector3(0f, 118f + num2 / 2f, Def.PUZZLE_BG_Z + 1f);
			if (mPartner.mData.ModelCount > 1)
			{
				result += Partner.PUZZLE_PAIRPARTNER_OFS;
			}
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			result = new Vector3(-360f, -260f, Def.PUZZLE_BG_Z + 1f);
			break;
		}
		return result;
	}

	private void SetLayoutPartnerBg(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mBg.transform.parent = mAnchorTop.transform;
			mBg.transform.localPosition = new Vector3(0f, -100f, Def.PARTNER_BG_Z);
			if (mBgMask != null)
			{
				mBgMask.transform.parent = mAnchorTop.transform;
				mBgMask.transform.localPosition = new Vector3(0f, -100f, Def.PARTNER_BG_Z - 0.1f);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mBg.transform.parent = mAnchorLeft.transform;
			mBg.transform.localPosition = new Vector3(100f, 0f, Def.PARTNER_BG_Z);
			if (mBgMask != null)
			{
				mBgMask.transform.parent = mAnchorLeft.transform;
				mBgMask.transform.localPosition = new Vector3(100f, 0f, Def.PARTNER_BG_Z - 0.1f);
			}
			break;
		}
	}

	private void UBAResetData(Def.SERIES a_series, int _stage)
	{
		mUBAMoves = 0;
		mUBATime = 0f;
		mUBAContinue = 0;
		mUBAUseBooster = new Dictionary<Def.BOOSTER_KIND, int>();
		mUBAUsePaidBooster = new Dictionary<Def.BOOSTER_KIND, int>();
		mUBADropPresentBox = new Dictionary<Def.TILE_KIND, int>();
		mUBAWinScore = 0;
		mUBASkill = 0;
		mUBACreatedSpecialTileCount = new Dictionary<Def.TILE_FORM, int>();
		mUBACrushedSpecialTileCount = new Dictionary<Def.TILE_FORM, int>();
		mUBACrushedSpecialCrossCount = new Dictionary<Def.TILE_KIND, int>();
		mGame.mPlayerMetaData.Init(a_series, _stage, mGame.mPlayer);
	}

	private void UBACollectData(Def.SERIES a_series, int _stage, bool isWon)
	{
		mUBAMoves = mPuzzleManager.mTotalMoves;
		mUBATime = mPuzzleManager.mPlayTime;
		mUBAContinue = mContinueCount;
		Def.TILE_KIND[] uBA_PRESENTS = UBA_PRESENTS;
		foreach (Def.TILE_KIND tILE_KIND in uBA_PRESENTS)
		{
			mUBADropPresentBox[tILE_KIND] = mPuzzleManager.GetExitCount(tILE_KIND);
		}
		mGame.mPlayerMetaData.Update(a_series, _stage, mUBAMoves, mUBATime, mUBAContinue, mUBAUseBooster, mUBADropPresentBox, mGame.mPlayer, isWon);
	}

	public void OnWin()
	{
		mUBAWinScore = mPuzzleManager.mScore;
		StartCoroutine(CoOnWin());
		if (mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
		{
			mGame.mPlayer.AddLifeCountAtPuzzleEnd();
		}
		mPuzzleHud.StageClear();
		if (mGame.mCurrentStage.Series == Def.SERIES.SM_FIRST && mGame.mCurrentStage.StageNumber == 100)
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.LEVEL1_WIN1);
		}
		mState.Change(STATE.WAIT);
	}

	private IEnumerator CoOnWin()
	{
		bool animeFinished2 = false;
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.TALISMAN)
		{
			GameObject cameraGo = SkillEffectPlayer.CreateDemoCamera();
			OneShotAnime anime = Util.CreateOneShotAnime("TalismanEnd", "GIMMICK_TALISMAN_END", cameraGo, new Vector3(0f, 0f, -4f), new Vector3(0.75f, 0.75f, 1f), 0f, 0f, true, (SsSprite.AnimationCallback)delegate
			{
				animeFinished2 = true;
			});
			mGame.PlaySe("SE_TALISMAN_CLEAR", 3);
			do
			{
				yield return 0;
			}
			while (!animeFinished2);
			UnityEngine.Object.Destroy(cameraGo);
		}
		mPartner.Win();
		mGame.PlaySe("SE_WIN", 3);
		StopBGM();
		animeFinished2 = false;
		Util.CreateOneShotAnime("LevelClear", "TEXT_LEVELCLEAR", mRoot.gameObject, new Vector3(0f, 0f, -3f), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			animeFinished2 = true;
		});
		do
		{
			yield return 0;
		}
		while (!animeFinished2);
		mPuzzleManager.WaitStateFinished();
		PlayBGM();
	}

	private IEnumerator PlayWinEffect()
	{
		yield break;
	}

	public void OnCrearBonusSpecialTileCrushed()
	{
	}

	public void OnClearBonusStart()
	{
		Util.CreateOneShotAnime("ClearBonusText", "TEXT_LUNA_BONUS", mRoot, new Vector3(0f, 0f, -4f), Vector3.one, 0f, 1f, true, (SsSprite.AnimationCallback)delegate
		{
			mGame.SetMusicVolume(1f, 0);
			mPuzzleManager.WaitStateFinished();
		});
		mGame.SetMusicVolume(0.5f, 0);
		mGame.PlaySe("SE_CLEAR_BONUS", 3, 1f, 1f);
		mGame.PlaySe("VOICE_021", 2, 1f, 1f);
	}

	public void OnClearBonusStepUp(int step)
	{
		if (step % 2 == 0)
		{
			OneShotAnime oneShotAnime = Util.CreateOneShotAnime("ClearBonus", "EFFECT_LUNA_BONUS", mRoot, new Vector3(0f, 0f, -10f), Vector3.one, 0f, 0f, false, delegate(SsSprite sp)
			{
				StartCoroutine(CoComboEffectDelete(sp));
			});
			ParticleEmitterControl particleEmitterControl = Util.CreateGameObject("EmitControl", mRoot).AddComponent<ParticleEmitterControl>();
			particleEmitterControl.Init(oneShotAnime._sprite, "Particles/PuzzleGimmick/LunaBonus", 5f);
			mGame.PlaySe("SE_CLEAR_BONUS_JUMP", 3);
		}
		else
		{
			StartCoroutine(ClearBonusStepUpOdd());
		}
	}

	private IEnumerator ClearBonusStepUpOdd()
	{
		Live2DRender l2dRender = mGame.CreateLive2DRender(568, 568);
		yield return 0;
		Live2DInstance l2dInst = Util.CreateGameObject("Luna", l2dRender.gameObject).AddComponent<Live2DInstance>();
		l2dInst.Init("LUNA", Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER, Color.white, true);
		while (!l2dInst.IsLoadFinished())
		{
			yield return 0;
		}
		l2dInst.StartMotion("motions/bonus00.mtn", false);
		l2dRender.ResisterInstance(l2dInst);
		SimpleTexture partnerScreen = Util.CreateGameObject("L2DScreen", mAnchorBottom).AddComponent<SimpleTexture>();
		partnerScreen.Init(l2dRender.RenderTexture, new Vector2(568f, 568f), new Rect(0f, 0f, 1f, 1f), SimpleTexture.PIVOT.BOTTOM);
		partnerScreen.transform.localPosition = new Vector3(0f, -280f, -50f);
		partnerScreen.transform.localScale = new Vector3(1.8f, 1.8f, 1f);
		mGame.PlaySe("SE_CLEAR_BONUS_LOOK", 3);
		float wait = 3f;
		while (wait > 0f)
		{
			wait -= Time.deltaTime;
			yield return 0;
		}
		l2dRender.UnresisterInstance(l2dInst);
		UnityEngine.Object.Destroy(l2dInst.gameObject);
		UnityEngine.Object.Destroy(partnerScreen.gameObject);
		UnityEngine.Object.Destroy(l2dRender.gameObject);
	}

	public void OnClearBonusFinished()
	{
		PlayBGM();
		string animeKey = string.Empty;
		string voiceKey = string.Empty;
		if (mGame.IsDailyChallengePuzzle)
		{
			animeKey = "TEXT_CLEAR_02";
			voiceKey = "VOICE_018";
		}
		else
		{
			switch (mPuzzleManager.CheckGotStars())
			{
			case 1:
				animeKey = "TEXT_CLEAR_00";
				voiceKey = "VOICE_016";
				break;
			case 2:
				animeKey = "TEXT_CLEAR_01";
				voiceKey = "VOICE_017";
				break;
			case 3:
				animeKey = "TEXT_CLEAR_02";
				voiceKey = "VOICE_018";
				break;
			}
		}
		StartCoroutine(AfterPlayCutinEffect(animeKey, "SE_WIN_MESSAGE", CompanionData.MOTION.WIN, voiceKey, "efc_cutin_win", delegate
		{
			mState.Change(STATE.PRE_NETWORK_WON);
		}));
		if (mGame.mCurrentStage.Series == Def.SERIES.SM_FIRST && mGame.mCurrentStage.StageNumber == 100)
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.LEVEL1_WIN2);
		}
	}

	public void OnPuzzleFinished()
	{
	}

	private IEnumerator PreNetwork_OnWon()
	{
		bool isWaitRoutine2 = false;
		if (mGame.mCurrentMapPageData is SMEventPageData && (mGame.mCurrentMapPageData as SMEventPageData).EventSetting != null)
		{
			Def.EVENT_TYPE eventType = (mGame.mCurrentMapPageData as SMEventPageData).EventSetting.EventType;
			if (eventType == Def.EVENT_TYPE.SM_LABYRINTH)
			{
				PlayerEventData playerEvData;
				mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out playerEvData);
				SMEventPageData eventPage = GameMain.GetEventPageData(mGame.mPlayer.Data.CurrentEventID);
				isWaitRoutine2 = true;
				mLabyrinthResultDialog = Util.CreateGameObject("LabyrinthResultDialog", mRoot).AddComponent<LabyrinthResultDialog>();
				mLabyrinthResultDialog.SetCallback(LabyrinthResultDialog.SELECT_ITEM.OnClosed, delegate(int calc_jewel, KeyValuePair<LabyrinthResultDialog.CALLBACK_BREAKDOWN, int>[] break_down)
				{
					isWaitRoutine2 = false;
					JewelCount = (short)calc_jewel;
					short a_course;
					int a_main;
					int a_sub;
					Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
					int evt_id = ((!mGame.mEventMode) ? 9999 : mGame.mPlayer.Data.CurrentEventID);
					int num = 0;
					int num2 = 0;
					int num3 = 0;
					int num4 = 0;
					if (break_down != null)
					{
						for (int i = 0; i < break_down.Length; i++)
						{
							KeyValuePair<LabyrinthResultDialog.CALLBACK_BREAKDOWN, int> keyValuePair = break_down[i];
							switch (keyValuePair.Key)
							{
							case LabyrinthResultDialog.CALLBACK_BREAKDOWN.IN_PUZZLE:
								num += keyValuePair.Value;
								break;
							case LabyrinthResultDialog.CALLBACK_BREAKDOWN.COURSE_BONUS:
								num2 += keyValuePair.Value;
								break;
							case LabyrinthResultDialog.CALLBACK_BREAKDOWN.DOUBLE_UP_CHANCE:
								num3 += keyValuePair.Value;
								break;
							case LabyrinthResultDialog.CALLBACK_BREAKDOWN.FIRST_CLEAR:
								num4 += keyValuePair.Value;
								break;
							}
						}
					}
					int num5 = num + num2 + num3 + num4;
					int total_jewel_num = playerEvData.LabyrinthData.JewelAmount + num5;
					ServerCram.EventLabyrinthGetJewel(a_course, (int)mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.LastPlayLevel, evt_id, 0, num5, num, num2, num3, num4, 0, 0, 0, total_jewel_num);
				});
				mLabyrinthResultDialog.Init(JewelCount, playerEvData.LabyrinthData.JewelAmount, mContinueCount, mIsJewelGetChancePlay, this, LabyrinthResultDialog.DISPLAY_TYPE.RESULT);
				while (isWaitRoutine2)
				{
					yield return null;
				}
				if (mLabyrinthResultDialog != null)
				{
					UnityEngine.Object.Destroy(mLabyrinthResultDialog.gameObject);
				}
				mLabyrinthResultDialog = null;
			}
		}
		mState.Change(STATE.NETWORK_WON);
	}

	private void Network_OnWon()
	{
		int series = (int)mGame.mCurrentStage.Series;
		int num = mGame.mCurrentStage.StageNumber;
		int formatVersion = mGame.mCurrentStage.FormatVersion;
		int revision = mGame.mCurrentStage.Revision;
		string segmentCode = mGame.mCurrentStage.SegmentCode;
		string puzzleMode = string.Empty + (int)mPuzzleManager.GetDifficultyMode() * 100;
		int uUID = mGame.mPlayer.UUID;
		int hiveId = mGame.mPlayer.HiveId;
		string fbName = mGame.mOptions.FbName;
		int num2 = mPuzzleManager.mScore;
		int num3 = mPuzzleManager.CheckGotStars();
		int num4 = 0;
		int mMaxCombo = mPuzzleManager.mMaxCombo;
		if (mStageSkipSelected)
		{
			num2 = 0;
			num3 = 0;
		}
		mState.Reset(STATE.WAIT, true);
		if (mGame.IsDailyChallengePuzzle)
		{
			if (mGame.CanAcquireAccessoryIdListForWin != null)
			{
				foreach (int item in mGame.CanAcquireAccessoryIdListForWin)
				{
					AccessoryData accessoryData = mGame.GetAccessoryData(item);
					mGame.mPlayer.AddAccessory(accessoryData, true, false, 18, GiftMailType.LOCAL_EVENT);
				}
			}
			mGame.CanAcquireAccessoryIdListForWin.Clear();
		}
		int num5 = -1;
		int num6 = -1;
		int num7 = 0;
		int num8 = 0;
		PlayerStageData _psd = null;
		if (!mGame.IsDailyChallengePuzzle)
		{
			PlayerClearData _psd2;
			if (mGame.mEventMode)
			{
				short a_course;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(num, out a_course, out a_main, out a_sub);
				int stageNo = Def.GetStageNo(a_main, a_sub);
				if (mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
				{
					PlayerEventData data;
					mGame.mPlayer.Data.GetMapData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, out data);
					if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, a_course, stageNo, out _psd2))
					{
						num8 = _psd2.HightScore;
					}
					PlayerEventClearData playerEventClearData = new PlayerEventClearData();
					playerEventClearData.Series = mGame.mCurrentStage.Series;
					playerEventClearData.EventID = mGame.mCurrentStage.EventID;
					playerEventClearData.CourseID = data.CourseID;
					playerEventClearData.StageNo = stageNo;
					playerEventClearData.HightScore = num2;
					playerEventClearData.GotStars = num3;
					mGame.mPlayer.AddStageClearData(playerEventClearData);
					num5 = mGame.mCurrentStage.EventID;
					num6 = data.CourseID;
					num7 = Def.GetStageNoForServer(num5, num);
					mGame.mPlayer.UpdateStageChallegeData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, data.CourseID, stageNo, true);
					mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, data.CourseID, stageNo, out _psd);
					mGame.mPlayer.SetClearEventStageInfo(stageNo);
				}
				else
				{
					ISMStageData aStage;
					SMMapStageSetting aStageSetting;
					mGame.GetPreStage(out aStage, out aStageSetting);
					Def.SplitEventStageNo(aStage.StageNumber, out a_course, out a_main, out a_sub);
					int stageNo2 = Def.GetStageNo(a_main, a_sub);
					PlayerEventClearData playerEventClearData2 = null;
					if (mGame.mPlayer.GetStageClearData(aStage.Series, aStage.EventID, a_course, stageNo2, out _psd2))
					{
						playerEventClearData2 = (PlayerEventClearData)_psd2;
					}
					else
					{
						PlayerEventData data2;
						mGame.mPlayer.Data.GetMapData(aStage.Series, aStage.EventID, out data2);
						playerEventClearData2 = new PlayerEventClearData();
						playerEventClearData2.Series = aStage.Series;
						playerEventClearData2.EventID = aStage.EventID;
						playerEventClearData2.CourseID = data2.CourseID;
						playerEventClearData2.StageNo = stageNo2;
						playerEventClearData2.HightScore = 0;
					}
					playerEventClearData2.GotStars = 3;
					mGame.mPlayer.AddStageClearData(playerEventClearData2);
					num7 = Def.GetStageNoForServer(mGame.mCurrentStage.EventID, num);
				}
				num = stageNo;
			}
			else
			{
				if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, num, out _psd2))
				{
					num8 = _psd2.HightScore;
				}
				PlayerClearData playerClearData = new PlayerClearData();
				playerClearData.Series = mGame.mCurrentStage.Series;
				playerClearData.StageNo = num;
				playerClearData.HightScore = num2;
				playerClearData.GotStars = num3;
				mGame.mPlayer.AddStageClearData(playerClearData);
				num7 = num;
				mGame.mPlayer.UpdateStageChallengeData(mGame.mCurrentStage.Series, num, true);
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, num, out _psd);
				if (mGame.PlayingFriendHelp == null)
				{
					mGame.mPlayer.SetClearStageInfo(mGame.mCurrentStage.StageNumber);
				}
				else
				{
					mGame.PlayingFriendHelp.IsStageCleared = true;
				}
			}
		}
		SpecialSpawnData[] array;
		if (!mStageSkipSelected)
		{
			array = mPuzzleManager.GetCollectedItemList();
			for (int i = 0; i < array.Length; i++)
			{
				mGame.AddAccessoryFromPresentBox(array[i].ItemIndex);
				int itemIndex = array[i].ItemIndex;
				AccessoryData accessoryData2 = mGame.GetAccessoryData(itemIndex);
				if (accessoryData2.AccessoryType == AccessoryData.ACCESSORY_TYPE.TROPHY)
				{
					int gotIDByNum = accessoryData2.GetGotIDByNum();
					int get_type = 3;
					if (mGame.mCurrentStageInfo.DailyEventBoxID.Contains(itemIndex))
					{
						get_type = 4;
					}
					ServerCram.GetTrophy(gotIDByNum, mGame.mPlayer.GetTrophy(), get_type, itemIndex, mGame.mOptions.PurchaseCount);
					mGame.mTrophyProductionFlg = GameMain.TROPHY_PRODUCTION.STOCK;
				}
			}
		}
		else
		{
			List<SpecialSpawnData> list = new List<SpecialSpawnData>();
			array = list.ToArray();
		}
		UBACollectData(mGame.mCurrentStage.Series, num7, true);
		mGame.mPlayer.Data.TotalWin++;
		MissionManager.ResultCountPuzzle(true, mUBASkill);
		int num9 = 0;
		if (mUBACreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_H))
		{
			num9 += mUBACreatedSpecialTileCount[Def.TILE_FORM.WAVE_H];
		}
		if (mUBACreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_V))
		{
			num9 += mUBACreatedSpecialTileCount[Def.TILE_FORM.WAVE_V];
		}
		mGame.mConcierge.SetPuzzleEndStripeCount(num9, Def.STAGE_LOSE_REASON.NONE);
		mGame.mConcierge.PuzzleEnd(mPuzzleManager.GetDifficultyMode(), true);
		if (mGame.IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
		{
			DailyChallengeEventSettings selectedDailyChallengeSetting = GlobalVariables.Instance.SelectedDailyChallengeSetting;
			int num10 = selectedDailyChallengeSetting.DailyChallengeID;
			string text = selectedDailyChallengeSetting.SheetName;
			if (mGame.mDebugDailyChallengeID > 0)
			{
				num10 = mGame.mDebugDailyChallengeID;
			}
			if (mGame.mDebugDailyChallengeSheetSetName != string.Empty)
			{
				text = mGame.mDebugDailyChallengeSheetSetName;
			}
			if (num10 == mGame.mPlayer.Data.DailyChallengeID)
			{
				DailyChallengeSheet dailyChallengeSheet = null;
				foreach (DailyChallengeSheet mDailyChallengeDatum in mGame.mDailyChallengeData)
				{
					if (mDailyChallengeDatum.SheetName == text)
					{
						dailyChallengeSheet = mDailyChallengeDatum;
						break;
					}
				}
				if (dailyChallengeSheet != null && mGame.SelectedDailyChallengeSheetNo > 0 && mGame.SelectedDailyChallengeSheetNo <= dailyChallengeSheet.StageCount)
				{
					int nextStageID = dailyChallengeSheet[mGame.SelectedDailyChallengeSheetNo - 1].GetNextStageID(mGame.SelectedDailyChallengeStageNo);
					if (mGame.mPlayer.Data.DailyChallengeStayStage != null && mGame.mPlayer.Data.DailyChallengeStayStage.ContainsKey(mGame.SelectedDailyChallengeSheetNo))
					{
						mGame.mPlayer.Data.DailyChallengeStayStage[mGame.SelectedDailyChallengeSheetNo] = nextStageID;
					}
				}
			}
		}
		if (mPuzzleFinishCallback != null)
		{
			mPuzzleFinishCallback(true, mStageSkipSelected);
		}
		if (mStageSkipSelected)
		{
			List<int> boxID = mGame.mCurrentStageInfo.BoxID;
			int j = 0;
			for (int count = boxID.Count; j < count; j++)
			{
				if (mGame.GetAccessoryData(boxID[j]).AccessoryType == AccessoryData.ACCESSORY_TYPE.ERB_KEY)
				{
					mGame.mPlayer.UnlockAccessory(boxID[j]);
				}
			}
			mGame.Save(true, true);
			BIJSNS bIJSNS = SocialManager.Instance[SNS.Facebook];
			num4 = (int)mUBATime;
			_StageWonInfo stageWonInfo = new _StageWonInfo();
			stageWonInfo.ClearScore = num2;
			stageWonInfo.ClearTime = num4;
			using (BIJMemoryBinaryWriter bIJMemoryBinaryWriter = new BIJMemoryBinaryWriter(Constants.METADATA_FILE))
			{
				SingletonMonoBehaviour<GameMain>.Instance.mPlayerMetaData.Serialize(bIJMemoryBinaryWriter, 1290);
				stageWonInfo.Metadata = bIJMemoryBinaryWriter.Bytes;
			}
			if (num8 < stageWonInfo.ClearScore)
			{
				mGame.Network_SetStageClearData(series, num, hiveId, uUID, fbName, stageWonInfo, num5, (short)num6);
			}
			SingletonMonoBehaviour<GameMain>.Instance.WinRate(num7, formatVersion, revision, segmentCode, puzzleMode, false, num2, num3, mUBAMoves, mUBATime, mUBAContinue, mMaxCombo, mUBASkill, mUBAUseBooster, mUBAUsePaidBooster, mUBADropPresentBox, array, mUBAWinScore, mUBACreatedSpecialTileCount, mUBACrushedSpecialTileCount, mUBACrushedSpecialCrossCount, mChanceAnnounceDone);
			int num11 = mGame.SegmentProfile.StageSkipShopItemIndex;
			if (num11 >= 34)
			{
				num11 = 33;
			}
			ShopItemData shopItemData = mGame.mShopItemData[num11];
			int gemAmount = shopItemData.GemAmount;
			int loseType = (int)mGame.mCurrentStage.LoseType;
			int num12 = mContinueCount;
			int stage_win = 0;
			int num13 = 0;
			float stage_win_rate = 0f;
			if (_psd != null)
			{
				stage_win = _psd.WinCount;
				num13 = _psd.LoseCount;
				stage_win_rate = _psd.WinRate;
			}
			ServerCram.SkipStage(gemAmount, loseType, num13, 1, series, num, stage_win, num13, stage_win_rate);
			UserBehavior.Instance.BuySkip((short)gemAmount);
			UserBehavior.Save();
		}
		else
		{
			SaveGameData(true);
			if (mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
			{
				BIJSNS bIJSNS2 = SocialManager.Instance[SNS.Facebook];
				num4 = (int)mUBATime;
				_StageWonInfo stageWonInfo2 = new _StageWonInfo();
				stageWonInfo2.ClearScore = num2;
				stageWonInfo2.ClearTime = num4;
				using (BIJMemoryBinaryWriter bIJMemoryBinaryWriter2 = new BIJMemoryBinaryWriter(Constants.METADATA_FILE))
				{
					SingletonMonoBehaviour<GameMain>.Instance.mPlayerMetaData.Serialize(bIJMemoryBinaryWriter2, 1290);
					stageWonInfo2.Metadata = bIJMemoryBinaryWriter2.Bytes;
				}
				if (num8 < stageWonInfo2.ClearScore)
				{
					mGame.Network_SetStageClearData(series, num, hiveId, uUID, fbName, stageWonInfo2, num5, (short)num6);
				}
				mGame.AchievementStageClear(num);
				mGame.AchievementCombo((int)mGame.mPlayer.Data.MaxCombo);
			}
			SingletonMonoBehaviour<GameMain>.Instance.WinRate(num7, formatVersion, revision, segmentCode, puzzleMode, true, num2, num3, mUBAMoves, mUBATime, mUBAContinue, mMaxCombo, mUBASkill, mUBAUseBooster, mUBAUsePaidBooster, mUBADropPresentBox, array, mUBAWinScore, mUBACreatedSpecialTileCount, mUBACrushedSpecialTileCount, mUBACrushedSpecialCrossCount, mChanceAnnounceDone);
		}
		if (mStageSkipSelected)
		{
			List<int> list2 = new List<int>();
			list2 = MissionManager.GetShowGuageArray(MissionData.SHOWGUAGE.PUZZLE_END);
			if (list2.Count != 0)
			{
				MissionGaugeUpDialog.CLEARDATA creardata = ((MissionManager.PlayerCrearData() == null) ? MissionGaugeUpDialog.CLEARDATA.FIRSTWIN : MissionGaugeUpDialog.CLEARDATA.NONE);
				mMissionGaugeUpDialog = Util.CreateGameObject("MissionGaugeUpDialog", mRoot).AddComponent<MissionGaugeUpDialog>();
				mMissionGaugeUpDialog.Init(list2, creardata);
				mMissionGaugeUpDialog.SetClosedCallback(OnMissionGaugeUpStageSkipDialogClosed);
			}
			else
			{
				mState.Change(STATE.END);
			}
			return;
		}
		List<int> list3 = new List<int>();
		list3 = MissionManager.GetShowGuageArray(MissionData.SHOWGUAGE.PUZZLE_END);
		if (list3.Count != 0)
		{
			MissionGaugeUpDialog.CLEARDATA creardata2 = ((MissionManager.PlayerCrearData() == null) ? MissionGaugeUpDialog.CLEARDATA.FIRSTWIN : MissionGaugeUpDialog.CLEARDATA.NONE);
			mMissionGaugeUpDialog = Util.CreateGameObject("MissionGaugeUpDialog", mRoot).AddComponent<MissionGaugeUpDialog>();
			mMissionGaugeUpDialog.Init(list3, creardata2);
			mMissionGaugeUpDialog.SetClosedCallback(OnMissionGaugeUpWinDialogClosed);
		}
		else
		{
			mWinDialog = Util.CreateGameObject("WinDialog", mRoot).AddComponent<WinDialog>();
			mWinDialog.SetClosedCallback(OnWinDialogClosed);
			mWinDialog.Init(mPuzzleManager.CheckGotStars(), array);
		}
		foreach (StageRankingData lastCheckRankingStageDatum in mGame.LastCheckRankingStageData)
		{
			mWinStageRankinglist.Add(lastCheckRankingStageDatum.Clone());
		}
		bool flag = false;
		for (int k = 0; k < mWinStageRankinglist.Count; k++)
		{
			StageRankingData stageRankingData = mWinStageRankinglist[k];
			if (stageRankingData.uuid == mGame.mPlayer.UUID)
			{
				if (num2 > stageRankingData.clr_score)
				{
					stageRankingData.clr_score = num2;
					mWinStageRankinglist[k] = stageRankingData;
				}
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			StageRankingData stageRankingData2 = new StageRankingData();
			stageRankingData2.uuid = mGame.mPlayer.UUID;
			stageRankingData2.clr_score = num2;
			stageRankingData2.iconid = mGame.mPlayer.Data.PlayerIcon;
			stageRankingData2.nickname = mGame.mPlayer.Data.NickName;
			mWinStageRankinglist.Add(stageRankingData2);
		}
		mWinStageRankinglist.Sort();
		SocialRanking();
		DebugWinRate(true);
	}

	public void OnMissionGaugeUpWinDialogClosed()
	{
		if (mMissionGaugeUpDialog != null)
		{
			UnityEngine.Object.Destroy(mMissionGaugeUpDialog.gameObject);
			mMissionGaugeUpDialog = null;
		}
		if (mGame.mCurrentStage.Series == Def.SERIES.SM_FIRST && mGame.mCurrentStage.StageNumber == 100)
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.LEVEL1_WIN3);
		}
		SpecialSpawnData[] collectedItemList = mPuzzleManager.GetCollectedItemList();
		mWinDialog = Util.CreateGameObject("WinDialog", mRoot).AddComponent<WinDialog>();
		mWinDialog.SetClosedCallback(OnWinDialogClosed);
		mWinDialog.Init(mPuzzleManager.CheckGotStars(), collectedItemList);
	}

	public IEnumerator CloseSocialRanking(Action<bool> on_closed = null)
	{
		mRankingClosing = true;
		bool result = false;
		if (mSocialWinRanking != null)
		{
			bool isClosed = false;
			mSocialWinRanking.SetCallback(SocialRankingWindow.CALLBACK.OnClosed, delegate
			{
				isClosed = true;
			});
			mSocialWinRanking.Close();
			while (!isClosed)
			{
				yield return null;
			}
			result = true;
		}
		if (on_closed != null)
		{
			on_closed(result);
		}
	}

	public void SocialRanking()
	{
		if (!mGame.IsDailyChallengePuzzle && mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET && !mRankingClosing)
		{
			int series = (int)mGame.mCurrentStage.Series;
			int stageNumber = mGame.mCurrentStage.StageNumber;
			if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				mSocialWinRanking = Util.CreateGameObject("SocialRanking", mAnchorBottom).AddComponent<SocialRankingWindow>();
				mSocialWinRanking.SetSortingOder(SingletonMonoBehaviour<GameMain>.Instance.mDialogManager.mDepth + 103);
				mSocialWinRanking.Init(series, stageNumber, mWinStageRankinglist, true);
			}
			else
			{
				mSocialWinRanking = Util.CreateGameObject("SocialRanking", mAnchorRight).AddComponent<SocialRankingWindow>();
				mSocialWinRanking.SetSortingOder(SingletonMonoBehaviour<GameMain>.Instance.mDialogManager.mDepth + 103);
				mSocialWinRanking.Init(series, stageNumber, mWinStageRankinglist, true, false);
			}
		}
	}

	public void SocialLoseRanking()
	{
		int mScore = mPuzzleManager.mScore;
		int series = (int)mGame.mCurrentStage.Series;
		int stageNumber = mGame.mCurrentStage.StageNumber;
		mStageRankinglist = mGame.LastCheckRankingStageData;
		if (!mGame.IsDailyChallengePuzzle && mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET && !mRankingClosing)
		{
			if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
			{
				mSocialLoseRanking = Util.CreateGameObject("SocialRanking", mAnchorBottom).AddComponent<SocialRankingWindow>();
				mSocialLoseRanking.SetSortingOder(SingletonMonoBehaviour<GameMain>.Instance.mDialogManager.mDepth + 23);
				mSocialLoseRanking.Init(series, stageNumber, mStageRankinglist, true, true, false);
			}
			else
			{
				mSocialLoseRanking = Util.CreateGameObject("SocialRanking", mAnchorRight).AddComponent<SocialRankingWindow>();
				mSocialLoseRanking.SetSortingOder(SingletonMonoBehaviour<GameMain>.Instance.mDialogManager.mDepth + 23);
				mSocialLoseRanking.Init(series, stageNumber, mStageRankinglist, true, false, false);
			}
		}
	}

	public void OnLose(Def.STAGE_LOSE_REASON reason)
	{
		mPartner.Lose();
		mLoseReason = reason;
		if (reason == Def.STAGE_LOSE_REASON.CANCEL)
		{
			if (mReplayExit)
			{
				mNetworkLostNext = STATE.REPLAY;
			}
			else
			{
				mNetworkLostNext = STATE.END;
			}
			mState.Change(STATE.NETWORK_LOST);
			return;
		}
		mState.Reset(STATE.WAIT);
		bool stageSkipMode = false;
		PlayerStageData _psd = null;
		int stageNumber = mGame.mCurrentStage.StageNumber;
		int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
		int series = (int)mGame.mCurrentStage.Series;
		if (mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			if (mGame.mEventMode)
			{
				if (mGame.mSegmentProfile.IsStageSkipEnable_EventStage())
				{
					int eventID = mGame.mCurrentStage.EventID;
					short a_course = 0;
					int a_main;
					int a_sub;
					Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
					mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, eventID, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
				}
			}
			else if ((mGame.mCurrentStage.SubStageNumber > 0 && mGame.mSegmentProfile.IsStageSkipEnable_SubStage()) || (mGame.mCurrentStage.SubStageNumber == 0 && mGame.mSegmentProfile.IsStageSkipEnable_NormalStage()))
			{
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, stageNumber, out _psd);
			}
			if (_psd != null && _psd.WinCount == 0 && _psd.LoseCount >= mGame.SegmentProfile.StageSkipLoseCount)
			{
				stageSkipMode = true;
			}
		}
		mStageSkipDisplayed = stageSkipMode;
		StartCoroutine(CoOnLose(reason, stageSkipMode));
	}

	private IEnumerator CoOnLose(Def.STAGE_LOSE_REASON reason, bool stageSkipMode)
	{
		mExtraContinueAddMove = 0;
		bool isLabyrinth = false;
		if (mGame.mCurrentMapPageData is SMEventPageData && (mGame.mCurrentMapPageData as SMEventPageData).EventSetting != null && (mGame.mCurrentMapPageData as SMEventPageData).EventSetting.EventType == Def.EVENT_TYPE.SM_LABYRINTH)
		{
			isLabyrinth = true;
		}
		bool isContunueChanceEnable = mChanceAnnounceDone;
		if (reason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
		{
			isContunueChanceEnable = false;
		}
		bool isDialogCreate = false;
		string messageKey = null;
		if (reason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
		{
			messageKey = "Message_ShuffleFail";
			if (stageSkipMode)
			{
				isDialogCreate = true;
			}
		}
		else if (mPuzzleHud.IsNormaCompleted() && !mPuzzleHud.IsBronzeScoreCompleted())
		{
			messageKey = "Message_Score";
			isDialogCreate = true;
		}
		else
		{
			isDialogCreate = true;
		}
		if (messageKey != null)
		{
			MessageBar messageBar = Util.CreateGameObject("Message", mRoot).AddComponent<MessageBar>();
			messageBar.Init(Localization.Get(messageKey), string.Empty, null);
			if (!isDialogCreate)
			{
				mGame.PlaySe("SE_LOSE", 3);
				StopBGM();
			}
			yield return new WaitForSeconds(1.9f);
		}
		if (isDialogCreate)
		{
			if (isLabyrinth)
			{
				mLoseDialog = Util.CreateGameObject("LoseDialog", mRoot).AddComponent<LabyrinthLoseDialog>();
				(mLoseDialog as LabyrinthLoseDialog).Init(JewelCount, mIsJewelGetChancePlay, mContinueCount, reason, mGame.mCompanionData[mPartnerIndex].CheckParsonalID(CompanionData.PERSONAL_ID.MAMORU), stageSkipMode, mGame.SegmentProfile.StageSkipShopItemIndex, isContunueChanceEnable);
			}
			else
			{
				mLoseDialog = Util.CreateGameObject("LoseDialog", mRoot).AddComponent<LoseDialog>();
				mLoseDialog.Init(mContinueCount, reason, mGame.mCompanionData[mPartnerIndex].CheckParsonalID(CompanionData.PERSONAL_ID.MAMORU), stageSkipMode, mGame.SegmentProfile.StageSkipShopItemIndex, isContunueChanceEnable);
			}
			mLoseDialog.SetClosedCallback(OnLoseDialogClosed);
			mLoseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			if (!mGame.mTutorialManager.IsTutorialPlaying() || mGame.mTutorialManager.GetCurrentTutorial() != Def.TUTORIAL_INDEX.LEVEL7_2)
			{
				StartCoroutine(GrayOut(0.8f, Color.black, 0.5f));
				mGame.PlaySe("SE_LOSE", 3);
				StopBGM();
			}
		}
		else
		{
			StartCoroutine(GrayOut(0.8f, Color.black, 0.5f));
			OnLoseDialogClosed(LoseDialog.SELECT_ITEM.GIVEUP);
		}
	}

	private void Network_OnLost()
	{
		int series = (int)mGame.mCurrentStage.Series;
		int num = mGame.mCurrentStage.StageNumber;
		int formatVersion = mGame.mCurrentStage.FormatVersion;
		int revision = mGame.mCurrentStage.Revision;
		string segmentCode = mGame.mCurrentStage.SegmentCode;
		string puzzleMode = string.Empty + (int)mPuzzleManager.GetDifficultyMode() * 100;
		int mScore = mPuzzleManager.mScore;
		int stageGotStars = mPuzzleManager.CheckGotStars();
		int mMaxCombo = mPuzzleManager.mMaxCombo;
		SpecialSpawnData[] collectedItemList = mPuzzleManager.GetCollectedItemList();
		int num2 = -1;
		int num3 = -1;
		int num4 = 0;
		PlayerStageData _psd = null;
		if (!mGame.IsDailyChallengePuzzle)
		{
			if (mGame.mEventMode)
			{
				short a_course;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(num, out a_course, out a_main, out a_sub);
				int stageNo = Def.GetStageNo(a_main, a_sub);
				if (mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
				{
					PlayerEventData data;
					mGame.mPlayer.Data.GetMapData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, out data);
					num2 = mGame.mCurrentStage.EventID;
					num3 = data.CourseID;
					num4 = Def.GetStageNoForServer(num2, num);
					mGame.mPlayer.UpdateStageChallegeData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, data.CourseID, stageNo, false);
					mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, data.CourseID, stageNo, out _psd);
				}
				num = stageNo;
			}
			else
			{
				num4 = num;
				mGame.mPlayer.UpdateStageChallengeData(mGame.mCurrentStage.Series, num, false);
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, num, out _psd);
			}
		}
		UBACollectData(mGame.mCurrentStage.Series, num4, false);
		mGame.mPlayer.Data.TotalLose++;
		int num5 = 0;
		if (mUBACreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_H))
		{
			num5 += mUBACreatedSpecialTileCount[Def.TILE_FORM.WAVE_H];
		}
		if (mUBACreatedSpecialTileCount.ContainsKey(Def.TILE_FORM.WAVE_V))
		{
			num5 += mUBACreatedSpecialTileCount[Def.TILE_FORM.WAVE_V];
		}
		mGame.mConcierge.SetPuzzleEndStripeCount(num5, mLoseReason);
		mGame.mConcierge.PuzzleEnd(mPuzzleManager.GetDifficultyMode(), false);
		if (mPuzzleFinishCallback != null)
		{
			mPuzzleFinishCallback(false, mStageSkipSelected);
		}
		SaveGameData(false);
		SingletonMonoBehaviour<GameMain>.Instance.WinRate(num4, formatVersion, revision, segmentCode, puzzleMode, false, mScore, stageGotStars, mUBAMoves, mUBATime, mUBAContinue, mMaxCombo, mUBASkill, mUBAUseBooster, mUBAUsePaidBooster, mUBADropPresentBox, collectedItemList, mUBAWinScore, mUBACreatedSpecialTileCount, mUBACrushedSpecialTileCount, mUBACrushedSpecialCrossCount, mChanceAnnounceDone);
		if (mStageSkipDisplayed)
		{
			int num6 = mGame.SegmentProfile.StageSkipShopItemIndex;
			if (num6 >= 34)
			{
				num6 = 33;
			}
			ShopItemData shopItemData = mGame.mShopItemData[num6];
			int gemAmount = shopItemData.GemAmount;
			int loseType = (int)mGame.mCurrentStage.LoseType;
			int num7 = mContinueCount;
			int stage_win = 0;
			int num8 = 0;
			float stage_win_rate = 0f;
			if (_psd != null)
			{
				stage_win = _psd.WinCount;
				num8 = _psd.LoseCount;
				stage_win_rate = _psd.WinRate;
			}
			ServerCram.SkipStage(gemAmount, loseType, num8, 0, series, num, stage_win, num8, stage_win_rate);
		}
		DebugWinRate(false);
		mState.Change(mNetworkLostNext);
	}

	private void DebugWinRate(bool isWon)
	{
		if (GameMain.DEBUG_WINRATE && mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET && !mGame.IsDailyChallengePuzzle)
		{
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot.gameObject).AddComponent<ConfirmDialog>();
			int a_main;
			int a_sub;
			Def.SplitStageNo(mGame.mCurrentStage.StageNumber, out a_main, out a_sub);
			PlayerStageData _psd;
			if (mGame.mEventMode)
			{
				int eventID = mGame.mCurrentStage.EventID;
				short a_course = 0;
				Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, eventID, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
			}
			else
			{
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, out _psd);
			}
			int num = 0;
			int num2 = 0;
			float num3 = 0f;
			if (_psd != null)
			{
				num = _psd.WinCount;
				num2 = _psd.LoseCount;
				num3 = _psd.WinRate;
			}
			string empty = string.Empty;
			empty = ((a_sub != 0) ? string.Format(" {0}-{1}", a_main, a_sub) : string.Format(" {0}", a_main));
			string text = string.Format(Localization.Get("WinRate_Desc"), empty, (!isWon) ? Localization.Get("WinRate_Lost") : Localization.Get("WinRate_Won"), mPuzzleManager.mStageClearMoves, string.Format("  ({0} / {1})", mPuzzleManager.mTotalMoves, mGame.mCurrentStage.LimitMoves), mContinueCount, mPuzzleManager.mScore, mGame.mCurrentStage.BronzeScore, mPuzzleManager.CheckGotStars(), mPuzzleManager.mStageClearScore, mPuzzleManager.mStageClearStars);
			text += string.Format("\n\nWin / Lose : {0} / {1}\nWinRate : {2:00.00}%", num, num2, num3);
			confirmDialog.Init(Localization.Get("WinRate_Title"), text, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, DialogBase.DIALOG_SIZE.XLARGE, string.Empty, string.Empty);
			int series = (int)mGame.mCurrentStage.Series;
			int stageNumber = mGame.mCurrentStage.StageNumber;
			int formatVersion = mGame.mCurrentStage.FormatVersion;
			int revision = mGame.mCurrentStage.Revision;
			string segmentCode = mGame.mCurrentStage.SegmentCode;
			string puzzleMode = string.Empty + (int)mPuzzleManager.GetDifficultyMode() * 100;
			int uUID = mGame.mPlayer.UUID;
			int hiveId = mGame.mPlayer.HiveId;
			string fbName = mGame.mOptions.FbName;
			int mScore = mPuzzleManager.mScore;
			int stageGotStars = mPuzzleManager.CheckGotStars();
			int mMaxCombo = mPuzzleManager.mMaxCombo;
			SingletonMonoBehaviour<GameMain>.Instance.DebugWinRate(stageNumber, formatVersion, revision, segmentCode, puzzleMode, isWon, mScore, stageGotStars, mUBAMoves, mUBATime, mUBAContinue, mMaxCombo, mUBASkill, mUBAUseBooster, mUBADropPresentBox, mUBAWinScore, mPuzzleManager.mStageClearScore, mPuzzleManager.mStageClearStars, mGame.mCurrentStage.LimitMoves);
		}
	}

	public void OnPurchaseDialogClosed()
	{
		StartCoroutine(GrayIn());
	}

	public bool OnPuzzleStart()
	{
		StartCoroutine(CoOnPuzzleStart());
		return true;
	}

	private IEnumerator CoOnPuzzleStart()
	{
		if (GlobalVariables.Instance.AddMovesCampaignAmount > 0)
		{
			byte amount = GlobalVariables.Instance.AddMovesCampaignAmount;
			mPuzzleHud.StartRecoveryEffect(amount, amount * 3);
			yield return new WaitForSeconds(1.5f);
			mPartner.Hit();
			Def.STAGE_LOSE_CONDITION loseType = mGame.mCurrentStage.LoseType;
			if (loseType != 0 && loseType == Def.STAGE_LOSE_CONDITION.TIME)
			{
				mPuzzleManager.AddTime(amount * 3);
			}
			else
			{
				mPuzzleManager.AddMoves(amount);
			}
		}
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.TALISMAN)
		{
			if (!mGame.mReplay)
			{
				GameObject cameraGo = SkillEffectPlayer.CreateDemoCamera();
				bool animeFinished = false;
				OneShotAnime anime = Util.CreateOneShotAnime("TalismanStart", "GIMMICK_TALISMAN_START", cameraGo, new Vector3(0f, 0f, -4f), new Vector3(0.75f, 0.75f, 1f), 0f, 0f, true, (SsSprite.AnimationCallback)delegate
				{
					animeFinished = true;
				});
				mGame.PlaySe("SE_TALISMAN_BEGIN", 3);
				do
				{
					yield return 0;
				}
				while (!animeFinished);
				UnityEngine.Object.Destroy(cameraGo);
			}
			yield return StartCoroutine(mPuzzleManager.PuzzkeStartTalismanTransform());
		}
		yield return 0;
		mPuzzleManager.WaitStateFinished();
	}

	public bool OnBeforePlay()
	{
		if (OnSkillBallTransfer())
		{
			return true;
		}
		bool flag = mPuzzleHud.IsChance();
		if (!mChanceAnnounceDone && flag)
		{
			mChanceAnnounceDone = true;
			StartCoroutine(ChanceBgChange());
			return true;
		}
		return false;
	}

	public bool OnSkillBallTransfer()
	{
		if (mPuzzleHud.IsSkillCharge())
		{
			PuzzleTile skillBallTransformTarget = mPuzzleManager.GetSkillBallTransformTarget();
			if (skillBallTransformTarget != null)
			{
				mPuzzleHud.ClearSkillPower();
				StartCoroutine(CreateSkillBall(skillBallTransformTarget));
				return true;
			}
		}
		return false;
	}

	public bool OnBeforePuzzleSpecialOfferCheck()
	{
		if (mIsJewelGetChance)
		{
			mIsJewelGetChance = false;
			StartCoroutine(JewelGetChanceProcess());
			return true;
		}
		return false;
	}

	public void OnPuzzleHudAdjust()
	{
		mPuzzleHud.AdjustNormaCount();
	}

	public void OnBoosterSelect(Def.BOOSTER_KIND b)
	{
		if (mBoosterMessageDialog != null)
		{
			return;
		}
		bool flag = mPuzzleManager.IsItemUsable();
		bool tutorialMode = false;
		if (mGame.mTutorialManager.IsTutorialPlaying())
		{
			if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.SELECT_ONE_3)
			{
				mTutorialBoosterUse = true;
				flag = true;
				tutorialMode = true;
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.SELECT_ONE_3);
			}
			else if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.SELECT_COLLECT_3)
			{
				mTutorialBoosterUse = true;
				flag = true;
				tutorialMode = true;
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.SELECT_COLLECT_3);
			}
			else if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.COLOR_CRUSH_3)
			{
				mTutorialBoosterUse = true;
				flag = true;
				tutorialMode = true;
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.COLOR_CRUSH_3);
			}
		}
		if (!flag)
		{
			return;
		}
		if (mGame.mPlayer.GetBoosterNum(b) < 1)
		{
			mPuzzleManager.SetWaitState();
			int[] includeSaleList;
			bool isSale = mGame.TryGetBoosterIncludedSaleList(b, out includeSaleList);
			if (includeSaleList == null)
			{
				isSale = false;
			}
			ShopItemData shop = null;
			int? saleCount = null;
			DateTime item = DateTimeUtil.Now();
			DateTime now = DateTimeUtil.Now();
			int highestGem = -1;
			int youngerID = -1;
			bool isSelectSingleAllCrush = false;
			Func<ShopLimitedTime, ShopItemData> fnCheckSaleItem = delegate(ShopLimitedTime fn_shop)
			{
				if (fn_shop == null)
				{
					return null;
				}
				if (now < DateTimeUtil.UnixTimeStampToDateTime(fn_shop.StartTime, true) || DateTimeUtil.UnixTimeStampToDateTime(fn_shop.EndTime, true) < now)
				{
					return null;
				}
				if (!mGame.mShopItemData.ContainsKey(fn_shop.ShopItemID) || mGame.mShopItemData[fn_shop.ShopItemID] == null || !mGame.mShopItemData[fn_shop.ShopItemID].IsSale)
				{
					return null;
				}
				if (mGame.SegmentProfile.WebSale.DetailList.Contains(fn_shop.ShopItemID))
				{
					return null;
				}
				if (fn_shop.MugenHeartItem > 0)
				{
					return null;
				}
				bool flag3 = false;
				if (includeSaleList != null)
				{
					int[] array = includeSaleList;
					foreach (int num3 in array)
					{
						if (num3 == fn_shop.ShopItemID)
						{
							flag3 = true;
							break;
						}
					}
				}
				if (!flag3)
				{
					return null;
				}
				ShopItemData result = null;
				if (fn_shop.ShopItemID >= 0 || fn_shop.ShopItemID < mGame.mShopItemData.Count)
				{
					result = mGame.mShopItemData[fn_shop.ShopItemID];
				}
				return result;
			};
			Func<ShopLimitedTime, ShopItemData> fnCheckSaleItemWithGem = delegate(ShopLimitedTime fn_shop)
			{
				ShopItemData shopItemData3 = fnCheckSaleItem(fn_shop);
				if (shopItemData3 == null)
				{
					return null;
				}
				return (highestGem == -1 || shopItemData3.GemAmount > highestGem || (shopItemData3.GemAmount == highestGem && (youngerID == -1 || shopItemData3.Index < youngerID))) ? shopItemData3 : null;
			};
			foreach (ShopLimitedTime shopItem in mGame.SegmentProfile.ShopItemList)
			{
				if (shopItem == null)
				{
					continue;
				}
				DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(shopItem.EndTime, true);
				bool flag2 = shopItem.AllCrushItem == 0 && mGame.SegmentProfile.ShopAllCrushItemList != null && mGame.SegmentProfile.ShopAllCrushItemList.Count > 0;
				if (flag2)
				{
					foreach (ShopLimitedTime shopAllCrushItem in mGame.SegmentProfile.ShopAllCrushItemList)
					{
						if (fnCheckSaleItem(shopAllCrushItem) != null)
						{
							flag2 = true;
							break;
						}
						flag2 = false;
					}
				}
				if ((b != Def.BOOSTER_KIND.ALL_CRUSH || flag2) && mGame.mPlayer.Data.AlreadyOpenBoosterSetDict.ContainsKey(shopItem.ShopItemID) && mGame.mPlayer.Data.AlreadyOpenBoosterSetDict[shopItem.ShopItemID].Contains(dateTime))
				{
					continue;
				}
				ShopItemData shopItemData = fnCheckSaleItemWithGem(shopItem);
				if (shopItemData != null)
				{
					item = dateTime;
					shop = shopItemData;
					saleCount = shopItem.ShopCount;
					highestGem = shopItemData.GemAmount;
					youngerID = shopItemData.Index;
					Def.BOOSTER_KIND bOOSTER_KIND = b;
					if (bOOSTER_KIND == Def.BOOSTER_KIND.ALL_CRUSH)
					{
						isSelectSingleAllCrush = !flag2;
					}
					else
					{
						isSelectSingleAllCrush = false;
					}
				}
			}
			isSale = shop != null;
			if (!isSale)
			{
				saleCount = null;
				int num = mGame.BoosterID_ChangeShopID((int)b);
				if (num >= 0 && num < mGame.mShopItemData.Count)
				{
					shop = mGame.mShopItemData[num];
				}
			}
			else
			{
				Def.BOOSTER_KIND bOOSTER_KIND = b;
				if (bOOSTER_KIND != Def.BOOSTER_KIND.ALL_CRUSH || !isSelectSingleAllCrush)
				{
					if (!mGame.mPlayer.Data.AlreadyOpenBoosterSetDict.ContainsKey(shop.Index))
					{
						mGame.mPlayer.Data.AlreadyOpenBoosterSetDict.Add(shop.Index, new List<DateTime> { item });
					}
					else if (!mGame.mPlayer.Data.AlreadyOpenBoosterSetDict[shop.Index].Contains(item))
					{
						mGame.mPlayer.Data.AlreadyOpenBoosterSetDict[shop.Index].Add(item);
					}
					mGame.Save();
				}
			}
			Func<ShopItemData, BuyBoosterDialog.OnDialogClosed, bool> fnMakeBuyBoosterDialog = delegate(ShopItemData fn_shop, BuyBoosterDialog.OnDialogClosed fn_close_callback)
			{
				if (fn_shop == null)
				{
					return false;
				}
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.ITEM_STAGE;
				BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("ShopDialog", mRoot).AddComponent<BuyBoosterDialog>();
				buyBoosterDialog.Init(fn_shop, false, false, saleCount);
				buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.IN_STAGE;
				buyBoosterDialog.SetClosedCallback(fn_close_callback);
				buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				return true;
			};
			if (!fnMakeBuyBoosterDialog(shop, delegate(ShopItemData item_data, BuyBoosterDialog.SELECT_ITEM i)
			{
				if (i == BuyBoosterDialog.SELECT_ITEM.CANCEL)
				{
					if (!isSale || isSelectSingleAllCrush)
					{
						mPuzzleManager.WaitStateFinished();
					}
					else
					{
						shop = null;
						Def.BOOSTER_KIND bOOSTER_KIND2 = b;
						if (bOOSTER_KIND2 == Def.BOOSTER_KIND.ALL_CRUSH)
						{
							highestGem = -1;
							youngerID = -1;
							foreach (ShopLimitedTime shopAllCrushItem2 in mGame.SegmentProfile.ShopAllCrushItemList)
							{
								if (shopAllCrushItem2 != null)
								{
									DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(shopAllCrushItem2.EndTime, true);
									ShopItemData shopItemData2 = fnCheckSaleItemWithGem(shopAllCrushItem2);
									if (shopItemData2 != null)
									{
										shop = shopItemData2;
										saleCount = shopAllCrushItem2.ShopCount;
										highestGem = shop.GemAmount;
										youngerID = shop.Index;
									}
								}
							}
						}
						else
						{
							int num2 = mGame.BoosterID_ChangeShopID((int)b);
							if (num2 >= 0 && num2 < mGame.mShopItemData.Count)
							{
								shop = mGame.mShopItemData[num2];
							}
						}
						if (!fnMakeBuyBoosterDialog(shop, delegate
						{
							mPuzzleManager.WaitStateFinished();
						}))
						{
							mPuzzleManager.WaitStateFinished();
						}
					}
				}
				else
				{
					mPuzzleManager.WaitStateFinished();
				}
			}))
			{
				mPuzzleManager.WaitStateFinished();
			}
		}
		else
		{
			mBoosterMessageDialog = Util.CreateGameObject("BoosterMessageDialog", mRoot).AddComponent<BoosterMessageDialog>();
			mBoosterMessageDialog.Init(b, tutorialMode, delegate
			{
				mPuzzleManager.TargetSelectEnd();
			}, mPuzzleManager.BoosterUsableCheck(b));
			mBoosterMessageDialog.SetClosedCallback(BoosterMessageDialogClosed);
			mPuzzleManager.BeginBoosterTargetSelect(b);
		}
	}

	public void BoosterMessageDialogClosed(Def.BOOSTER_KIND b, bool cancel)
	{
		UnityEngine.Object.Destroy(mBoosterMessageDialog.gameObject);
		mBoosterMessageDialog = null;
		mPuzzleManager.TargetSelectEffectEnd();
		if (b == Def.BOOSTER_KIND.BLOCK_CRUSH && !cancel)
		{
			OnBoosterUse(b, null, true);
			mPuzzleManager.SetWaitState(PuzzleManager.STATE.CHAIN);
		}
		if (b == Def.BOOSTER_KIND.ALL_CRUSH && !cancel)
		{
			OnBoosterUse(b, null, true);
			mPuzzleManager.SetWaitState(PuzzleManager.STATE.CHAIN);
		}
	}

	public bool OnBoosterUse(Def.BOOSTER_KIND b, PuzzleTile targetTile, bool add_use_count)
	{
		if (mBoosterMessageDialog != null)
		{
			mBoosterMessageDialog.Close();
		}
		bool flag = false;
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[2];
		array[0].partName = "change_000";
		array[1].partName = "change_001";
		string seKey = string.Empty;
		switch (b)
		{
		case Def.BOOSTER_KIND.WAVE_BOMB:
			array[0].spriteName = "efc_mess_booster_WAVE_BOMB";
			array[1].spriteName = "efc_booster_WAVE_BOMB";
			seKey = "SE_BOOSTER_WAVE_BOMB";
			flag = true;
			break;
		case Def.BOOSTER_KIND.TAP_BOMB2:
			array[0].spriteName = "efc_mess_booster_TAP_BOMB2";
			array[1].spriteName = "efc_booster_TAP_BOMB2";
			seKey = "SE_BOOSTER_TAP_BOMB2";
			flag = true;
			break;
		case Def.BOOSTER_KIND.RAINBOW:
			array[0].spriteName = "efc_mess_booster_RAINBOW";
			array[1].spriteName = "efc_booster_RAINBOW";
			seKey = "SE_BOOSTER_RAINBOW";
			flag = true;
			break;
		case Def.BOOSTER_KIND.ADD_SCORE:
			array[0].spriteName = "efc_mess_booster_ADD_SCORE";
			array[1].spriteName = "efc_booster_ADD_SCORE";
			seKey = "SE_BOOSTER_ADD_SCORE";
			flag = true;
			break;
		case Def.BOOSTER_KIND.SKILL_CHARGE:
			array[0].spriteName = "efc_mess_booster_SKILL_CHARGE";
			array[1].spriteName = "efc_booster_SKILL_CHARGE";
			seKey = "SE_BOOSTER_SKILL_CHARGE";
			flag = true;
			break;
		case Def.BOOSTER_KIND.SELECT_ONE:
			array[0].spriteName = "efc_mess_booster_SELECT_ONE";
			array[1].spriteName = "efc_booster_SELECT_ONE";
			seKey = "SE_BOOSTER_SELECT_ONE";
			flag = true;
			break;
		case Def.BOOSTER_KIND.SELECT_COLLECT:
			array[0].spriteName = "efc_mess_booster_SELECT_COLLECT";
			array[1].spriteName = "efc_booster_SELECT_COLLECT";
			seKey = "SE_BOOSTER_SELECT_COLLECT";
			flag = true;
			break;
		case Def.BOOSTER_KIND.COLOR_CRUSH:
			array[0].spriteName = "efc_mess_booster_COLOR_CRUSH";
			array[1].spriteName = "efc_booster_COLOR_CRUSH";
			seKey = "SE_BOOSTER_COLOR_CRUSH";
			flag = true;
			break;
		case Def.BOOSTER_KIND.BLOCK_CRUSH:
			array[0].spriteName = "efc_mess_booster_BLOCK_CRUSH";
			array[1].spriteName = "efc_booster_GIMMICK_CRUSH";
			seKey = "SE_BOOSTER_COLOR_CRUSH";
			flag = true;
			break;
		case Def.BOOSTER_KIND.PAIR_MAKER:
			array[0].spriteName = "efc_mess_booster_PAIR_MAKER";
			array[1].spriteName = "efc_booster_PAIR_MAKER";
			seKey = "SE_BOOSTER_PAIR_MAKER00";
			flag = true;
			break;
		case Def.BOOSTER_KIND.ALL_CRUSH:
			array[0].spriteName = "efc_mess_booster_ALL_CRUSH";
			array[1].spriteName = "efc_booster_ALL_CRUSH";
			seKey = "SE_BOOSTER_ALL_CRUSH";
			flag = true;
			break;
		}
		if (flag)
		{
			Def.ITEMGET_TYPE a_type;
			mGame.mPlayer.SubBoosterNum(b, 1, out a_type);
			mGame.Save();
			StartCoroutine(Booster_Common(b, targetTile, seKey, array));
			if (!mTutorialBoosterUse && add_use_count)
			{
				if (mUBAUseBooster.ContainsKey(b))
				{
					Dictionary<Def.BOOSTER_KIND, int> dictionary;
					Dictionary<Def.BOOSTER_KIND, int> dictionary2 = (dictionary = mUBAUseBooster);
					Def.BOOSTER_KIND key;
					Def.BOOSTER_KIND key2 = (key = b);
					int num = dictionary[key];
					dictionary2[key2] = num + 1;
				}
				else
				{
					mUBAUseBooster.Add(b, 1);
				}
				if (a_type == Def.ITEMGET_TYPE.PAID)
				{
					if (mUBAUsePaidBooster.ContainsKey(b))
					{
						Dictionary<Def.BOOSTER_KIND, int> dictionary3;
						Dictionary<Def.BOOSTER_KIND, int> dictionary4 = (dictionary3 = mUBAUsePaidBooster);
						Def.BOOSTER_KIND key;
						Def.BOOSTER_KIND key3 = (key = b);
						int num = dictionary3[key];
						dictionary4[key3] = num + 1;
					}
					else
					{
						mUBAUsePaidBooster.Add(b, 1);
					}
				}
			}
		}
		return flag;
	}

	public bool OnBoosterEffectOnly(Def.BOOSTER_KIND b, PuzzleTile targetTile)
	{
		if (mBoosterMessageDialog != null)
		{
			mBoosterMessageDialog.Close();
		}
		bool flag = false;
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[2];
		array[0].partName = "change_000";
		array[1].partName = "change_001";
		string seKey = string.Empty;
		switch (b)
		{
		case Def.BOOSTER_KIND.WAVE_BOMB:
			array[0].spriteName = "efc_mess_booster_WAVE_BOMB";
			array[1].spriteName = "efc_booster_WAVE_BOMB";
			seKey = "SE_BOOSTER_WAVE_BOMB";
			flag = true;
			break;
		case Def.BOOSTER_KIND.TAP_BOMB2:
			array[0].spriteName = "efc_mess_booster_TAP_BOMB2";
			array[1].spriteName = "efc_booster_TAP_BOMB2";
			seKey = "SE_BOOSTER_TAP_BOMB2";
			flag = true;
			break;
		case Def.BOOSTER_KIND.RAINBOW:
			array[0].spriteName = "efc_mess_booster_RAINBOW";
			array[1].spriteName = "efc_booster_RAINBOW";
			seKey = "SE_BOOSTER_RAINBOW";
			flag = true;
			break;
		case Def.BOOSTER_KIND.ADD_SCORE:
			array[0].spriteName = "efc_mess_booster_ADD_SCORE";
			array[1].spriteName = "efc_booster_ADD_SCORE";
			seKey = "SE_BOOSTER_ADD_SCORE";
			flag = true;
			break;
		case Def.BOOSTER_KIND.SKILL_CHARGE:
			array[0].spriteName = "efc_mess_booster_SKILL_CHARGE";
			array[1].spriteName = "efc_booster_SKILL_CHARGE";
			seKey = "SE_BOOSTER_SKILL_CHARGE";
			flag = true;
			break;
		case Def.BOOSTER_KIND.SELECT_ONE:
			array[0].spriteName = "efc_mess_booster_SELECT_ONE";
			array[1].spriteName = "efc_booster_SELECT_ONE";
			seKey = "SE_BOOSTER_SELECT_ONE";
			flag = true;
			break;
		case Def.BOOSTER_KIND.SELECT_COLLECT:
			array[0].spriteName = "efc_mess_booster_SELECT_COLLECT";
			array[1].spriteName = "efc_booster_SELECT_COLLECT";
			seKey = "SE_BOOSTER_SELECT_COLLECT";
			flag = true;
			break;
		case Def.BOOSTER_KIND.COLOR_CRUSH:
			array[0].spriteName = "efc_mess_booster_COLOR_CRUSH";
			array[1].spriteName = "efc_booster_COLOR_CRUSH";
			seKey = "SE_BOOSTER_COLOR_CRUSH";
			flag = true;
			break;
		case Def.BOOSTER_KIND.BLOCK_CRUSH:
			array[0].spriteName = "efc_mess_booster_BLOCK_CRUSH";
			array[1].spriteName = "efc_booster_GIMMICK_CRUSH";
			seKey = "SE_BOOSTER_COLOR_CRUSH";
			flag = true;
			break;
		case Def.BOOSTER_KIND.PAIR_MAKER:
			array[0].spriteName = "efc_mess_booster_PAIR_MAKER";
			array[1].spriteName = "efc_booster_PAIR_MAKER";
			seKey = "SE_BOOSTER_PAIR_MAKER00";
			flag = true;
			break;
		case Def.BOOSTER_KIND.ALL_CRUSH:
			array[0].spriteName = "efc_mess_booster_ALL_CRUSH";
			array[1].spriteName = "efc_booster_ALL_CRUSH";
			seKey = "SE_BOOSTER_ALL_CRUSH";
			flag = true;
			break;
		}
		if (flag)
		{
			StartCoroutine(Booster_Common(b, targetTile, seKey, array));
		}
		return flag;
	}

	public void OnBeginCrush(PuzzleTile tile, float delayTime)
	{
		MissionManager.CountPuzzleTile(tile.Kind);
		if (tile.isJewel())
		{
			short num = 0;
			switch (tile.Form)
			{
			case Def.TILE_FORM.JEWEL:
				num = 1;
				break;
			case Def.TILE_FORM.JEWEL3:
				num = 3;
				break;
			case Def.TILE_FORM.JEWEL5:
				num = 5;
				break;
			case Def.TILE_FORM.JEWEL9:
				num = 9;
				break;
			}
			JewelCount += num;
			DispJewelCount = JewelCount;
		}
		if (mPuzzleManager.IsGameFinished())
		{
			return;
		}
		Vector2 vector = Util.LogScreenSize();
		string spriteName = string.Empty;
		if (tile.isColor())
		{
			switch (tile.Kind)
			{
			case Def.TILE_KIND.COLOR0:
				spriteName = "efc_homing_red";
				break;
			case Def.TILE_KIND.COLOR1:
				spriteName = "efc_homing_blue";
				break;
			case Def.TILE_KIND.COLOR2:
				spriteName = "efc_homing_green";
				break;
			case Def.TILE_KIND.COLOR3:
				spriteName = "efc_homing_yellow";
				break;
			case Def.TILE_KIND.COLOR4:
				spriteName = "efc_homing_orange";
				break;
			case Def.TILE_KIND.COLOR5:
				spriteName = "efc_homing_purple";
				break;
			}
			if (tile.Kind == Def.TILE_KIND.COLOR5 && mPuzzleHud.GetSkillButtonEnable() && !mSkillBallExist)
			{
				mPuzzleManager.SetCollectEffectBusy(true);
				Vector3 vector2 = tile.Position * vector.y / 2f;
				Vector3 vector3 = mPuzzleHud.GetSkillButtonPos() * vector.y / 2f;
				HomingLazer homingLazer = new HomingLazer(new Vector2(vector2.x, vector2.y), new Vector2(vector3.x, vector3.y), 8, 10f, spriteName, delayTime);
				homingLazer.OnFinished = delegate
				{
					mPuzzleManager.SetCollectEffectBusy(false);
					mPuzzleHud.AddSkillPower(1f);
				};
				mLazerEffects.AddLazer(homingLazer);
			}
		}
		if (mPuzzleHud.IsNormaTarget(tile.Kind) && tile.isColor())
		{
			mPuzzleManager.SetCollectEffectBusy(true);
			Vector3 vector4 = tile.Position * vector.y / 2f;
			Vector3 targetPos = mPuzzleHud.GetCollectTargetPos(tile.Kind) * vector.y / 2f;
			CollectPresent collectPresent = Util.CreateGameObject("Collect", mRoot).AddComponent<CollectPresent>();
			collectPresent.Init(new Vector3(vector4.x, vector4.y, -2f), targetPos, tile, false, false, delayTime, null, string.Empty);
			collectPresent.SetFinishedCallback(delegate
			{
				mPuzzleManager.SetCollectEffectBusy(false);
				mPuzzleHud.CrushTile(tile.Kind);
			});
		}
		else if (mPuzzleHud.IsAttackTarget(tile.Kind) && tile.isColor())
		{
			float num2 = UnityEngine.Random.Range(0f, 0.2f);
			Vector3 vector5 = tile.Position * vector.y / 2f;
			if (!mPuzzleManager.IsBossGimmickDead())
			{
				mPuzzleManager.SetCollectEffectBusy(true);
				Vector3 vector6 = mPuzzleManager.GetBossGimmickPos() * vector.y / 2f;
				HomingLazer homingLazer2 = new HomingLazer(new Vector2(vector5.x, vector5.y), new Vector2(vector6.x, vector6.y), 8, 20f, spriteName, delayTime + num2);
				homingLazer2.OnFinished = delegate
				{
					mPuzzleManager.AddBossDamage(tile.Kind);
					mPuzzleManager.SetCollectEffectBusy(false);
				};
				mLazerEffects.AddLazer(homingLazer2);
			}
		}
		else if (tile.Form == Def.TILE_FORM.FOOT_STAMP)
		{
			mPuzzleManager.SetCollectEffectBusy(true);
			Vector3 vector7 = tile.Position * vector.y / 2f;
			Vector3 targetPos2 = mPuzzleManager.GetDianaGimmickPos() * vector.y / 2f;
			CollectPresent collectPresent2 = Util.CreateGameObject("Collect", mRoot).AddComponent<CollectPresent>();
			collectPresent2.Init(new Vector3(vector7.x, vector7.y, -2f), targetPos2, tile, false, false, delayTime, null, string.Empty);
			collectPresent2.SetFinishedCallback(delegate
			{
				mPuzzleManager.AddDianaStep(1);
				mPuzzleManager.SetCollectEffectBusy(false);
			});
		}
		else
		{
			mPuzzleHud.CrushTile(tile.Kind);
		}
	}

	public void OnBossDamageOrbCrush(PuzzleTile tile, float delayTime)
	{
		if (mPuzzleManager.IsGameFinished())
		{
			return;
		}
		Vector2 vector = Util.LogScreenSize();
		string spriteName = "efc_homing_rainbow";
		for (int i = 0; i < 10; i++)
		{
			float num = UnityEngine.Random.Range(0f, 0.2f);
			Vector3 vector2 = tile.Position * vector.y / 2f;
			if (!mPuzzleManager.IsBossGimmickDead())
			{
				mPuzzleManager.SetCollectEffectBusy(true);
				Vector3 vector3 = mPuzzleManager.GetBossGimmickPos() * vector.y / 2f;
				HomingLazer homingLazer = new HomingLazer(new Vector2(vector2.x, vector2.y), new Vector2(vector3.x, vector3.y), 8, 20f, spriteName, delayTime + num);
				homingLazer.OnFinished = delegate
				{
					mPuzzleManager.AddBossDamage(tile.Kind);
					mPuzzleManager.SetCollectEffectBusy(false);
				};
				mLazerEffects.AddLazer(homingLazer);
			}
		}
	}

	public void OnSpecialCreateCountUp(Def.TILE_FORM form)
	{
		if (!mPuzzleManager.IsGameFinished())
		{
			int value;
			if (mUBACreatedSpecialTileCount.TryGetValue(form, out value))
			{
				mUBACreatedSpecialTileCount[form] = value + 1;
			}
			else
			{
				mUBACreatedSpecialTileCount[form] = 1;
			}
		}
	}

	public void OnSpecialCrushCountUp(Def.TILE_FORM form)
	{
		if (!mPuzzleManager.IsGameFinished())
		{
			int value;
			if (mUBACrushedSpecialTileCount.TryGetValue(form, out value))
			{
				mUBACrushedSpecialTileCount[form] = value + 1;
			}
			else
			{
				mUBACrushedSpecialTileCount[form] = 1;
			}
		}
	}

	public void OnSpecialCrossCountUp(Def.TILE_KIND crossKind)
	{
		if (!mPuzzleManager.IsGameFinished())
		{
			int value;
			if (mUBACrushedSpecialCrossCount.TryGetValue(crossKind, out value))
			{
				mUBACrushedSpecialCrossCount[crossKind] = value + 1;
			}
			else
			{
				mUBACrushedSpecialCrossCount[crossKind] = 1;
			}
		}
	}

	public void OnCollect(PuzzleTile tile, Vector3 worldPosition, string overWriteImage)
	{
		mPuzzleManager.SetCollectEffectBusy(true);
		Def.TILE_KIND tILE_KIND = tile.Kind;
		switch (tILE_KIND)
		{
		case Def.TILE_KIND.TALISMAN0_1:
		case Def.TILE_KIND.TALISMAN0_2:
			tILE_KIND = Def.TILE_KIND.TALISMAN0_0;
			break;
		case Def.TILE_KIND.TALISMAN1_1:
		case Def.TILE_KIND.TALISMAN1_2:
			tILE_KIND = Def.TILE_KIND.TALISMAN1_0;
			break;
		case Def.TILE_KIND.TALISMAN2_1:
		case Def.TILE_KIND.TALISMAN2_2:
			tILE_KIND = Def.TILE_KIND.TALISMAN2_0;
			break;
		}
		CollectPresent collectPresent = Util.CreateGameObject("Collect", mRoot).AddComponent<CollectPresent>();
		collectPresent.SetFinishedCallback(delegate(Def.TILE_KIND collectKind)
		{
			mPuzzleManager.SetCollectEffectBusy(false);
			mPuzzleHud.CrushTile(collectKind);
		});
		Vector2 vector = Util.LogScreenSize();
		Vector3 vector2 = worldPosition * vector.y / 2f;
		if (tile.isFind())
		{
			Vector3 targetPos = mPuzzleHud.GetCollectTargetPos(tILE_KIND) * vector.y / 2f;
			collectPresent.Init(new Vector3(vector2.x, vector2.y, -2f), targetPos, tile, false, true, 0f, null, overWriteImage);
			mGame.PlaySe("SE_INGREDIENT", 2);
		}
		else if (tile.isPairComplete())
		{
			Vector3 targetPos2 = mPuzzleHud.GetCollectTargetPos(tILE_KIND) * vector.y / 2f;
			collectPresent.Init(new Vector3(vector2.x, vector2.y, -2f), targetPos2, tile, false, true, 0f, null, overWriteImage, true);
			mGame.PlaySe("SE_BOOSTER_PAIR_MAKER02", 2);
		}
		else if (tile.isIngredient() || tile.isTalisman())
		{
			Vector3 targetPos3 = mPuzzleHud.GetCollectTargetPos(tILE_KIND) * vector.y / 2f;
			collectPresent.Init(new Vector3(vector2.x, vector2.y, -2f), targetPos3, tile, false, false, 0f, null, string.Empty);
			mGame.PlaySe("SE_INGREDIENT", 2);
		}
		else if (tile.isPresentBox() || tile.isKey())
		{
			mPuzzleHud.AddBoxCollectCount();
			GameObject collectBoxParent = mPuzzleHud.GetCollectBoxParent();
			Vector3 targetPos4 = collectBoxParent.transform.position * vector.y / 2f;
			collectPresent.Init(new Vector3(vector2.x, vector2.y, -2f), targetPos4, tile, false, true, 0f, collectBoxParent, string.Empty);
			mGame.PlaySe("SE_PRESENTBOX", 2);
			mPuzzleManager.AddCollectItemList(tILE_KIND, tile.ItemCategory, tile.ItemIndex);
		}
		MissionManager.CountPuzzleTile(tILE_KIND);
	}

	public void OnShuffle(bool forceShuffle)
	{
		if (!forceShuffle)
		{
			MessageBar messageBar = Util.CreateGameObject("Message", mRoot).AddComponent<MessageBar>();
			messageBar.Init(Localization.Get("Message_Shuffle"), "shuffle_icon", null);
		}
		mGame.PlaySe("SE_SHUFFLE", 3);
	}

	public void OnChangeMoves(int moves)
	{
		if (moves <= 5)
		{
			mPartner.Hurry();
		}
		mPuzzleHud.LimitTargetNum = moves;
	}

	public void OnChangeTimer(float time)
	{
		if (time < 10f)
		{
			mPartner.Hurry();
		}
		mPuzzleHud.LimitTargetNum = (int)time;
	}

	public void OnChangeScore(int score)
	{
		if (score > mGame.mPlayer.Data.MaxScore)
		{
			mGame.mPlayer.Data.MaxScore = score;
		}
		mPuzzleHud.ScoreTargetNum = score;
	}

	public void OnCombo(int comboNum)
	{
		if (comboNum > mGame.mPlayer.Data.MaxCombo)
		{
			mGame.mPlayer.Data.MaxCombo = comboNum;
		}
		mPartner.Hit();
		mPuzzleHud.Combo(comboNum);
	}

	public bool OnComboFinished(int comboNum)
	{
		mPuzzleHud.ComboFinished(comboNum);
		string text = "NONE";
		string soundKey = string.Empty;
		string voiceKey = string.Empty;
		string emitterName = string.Empty;
		string voiceCutinSpriteKey = string.Empty;
		if (comboNum >= 20)
		{
			text = "TEXT_MIRACLE";
			emitterName = "Particles/HappinessParticle";
			soundKey = "SE_MIRACLE";
			voiceKey = "VOICE_015";
			voiceCutinSpriteKey = "efc_cutin_c02";
		}
		else if (comboNum >= 15)
		{
			text = "TEXT_AMAZING";
			emitterName = "Particles/ExcellentParticle";
			soundKey = "SE_AMAZING";
			voiceKey = "VOICE_014";
			voiceCutinSpriteKey = "efc_cutin_c02";
		}
		else if (comboNum >= 10)
		{
			text = "TEXT_FANTASTIC";
			emitterName = "Particles/GoodjobParticle";
			soundKey = "SE_FANTASTIC";
			voiceKey = "VOICE_013";
			voiceCutinSpriteKey = "efc_cutin_c01";
		}
		else if (comboNum >= 5)
		{
			text = "TEXT_GOOD";
			emitterName = "Particles/CoolParticle";
			soundKey = "SE_GOOD";
			voiceKey = "VOICE_012";
			voiceCutinSpriteKey = "efc_cutin_c00";
		}
		if (string.Compare(text, "NONE") != 0)
		{
			StartCoroutine(ComboCutinEffect(text, emitterName, soundKey, CompanionData.MOTION.HIT0, voiceKey, voiceCutinSpriteKey));
			return true;
		}
		return false;
	}

	public IEnumerator CoComboEffectDelete(SsSprite sp)
	{
		yield return 0;
		UnityEngine.Object.Destroy(sp.gameObject);
		yield return 0;
	}

	public void OnLoseDialogClosed(LoseDialog.SELECT_ITEM i)
	{
		switch (i)
		{
		case LoseDialog.SELECT_ITEM.CONTINUE:
		{
			int num = Math.Max(0, Math.Min(mContinueCount, Def.CONTINUE_STEP.Length - 1));
			Def.ContinueStepData continueStepData = Def.CONTINUE_STEP[num];
			mTutorialContinue = false;
			if (mGame.mTutorialManager.IsTutorialPlaying())
			{
				mTutorialContinue = true;
			}
			mExtraContinueAddMove = mLoseDialog.ExtraAddMove;
			StartCoroutine(ContinueEffect(num, mLoseDialog.ExtraAddMove, mGame.mCompanionData[mPartnerIndex].CheckParsonalID(CompanionData.PERSONAL_ID.MAMORU)));
			if (mTutorialContinue)
			{
				break;
			}
			int num2 = (int)mGame.mCurrentStage.LoseType;
			int cur_series = (int)mGame.mCurrentStage.Series;
			int cur_stage = mGame.mCurrentStage.StageNumber;
			int sts_cnt = mContinueCount;
			int add_move = 0;
			float add_time = 0f;
			switch (num2)
			{
			case 0:
				add_move = continueStepData.addMove + mExtraContinueAddMove;
				break;
			case 1:
				add_time = continueStepData.addTime + (float)(mExtraContinueAddMove * 3);
				break;
			}
			if (mChanceAnnounceDone)
			{
				num2 += 100;
			}
			int stage_win = 0;
			int num3 = 0;
			float stage_win_rate = 0f;
			PlayerStageData _psd = null;
			if (mGame.IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
			{
				int num4 = ((mGame.mDebugDailyChallengeID <= 0) ? GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID : mGame.mDebugDailyChallengeID);
				cur_series = 200;
				cur_stage = int.Parse(num4 + (mGame.SelectedDailyChallengeSheetNo - 1).ToString("00") + mGame.SelectedDailyChallengeStageNo.ToString("0"));
			}
			else if (mGame.mEventMode)
			{
				int eventID = mGame.mCurrentStage.EventID;
				short a_course = 0;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, eventID, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
				int stageNo = Def.GetStageNo(a_course, a_main, a_sub);
				cur_stage = Def.GetStageNoForServer(mGame.mPlayer.Data.CurrentEventID, stageNo);
			}
			else
			{
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, out _psd);
			}
			if (_psd != null)
			{
				stage_win = _psd.WinCount;
				num3 = _psd.LoseCount;
				stage_win_rate = _psd.WinRate;
			}
			long purchaseCount = mGame.mOptions.PurchaseCount;
			ServerCram.BuyContinue(continueStepData.price, add_move, add_time, num2, (int)continueStepData.item, continueStepData.num, sts_cnt, cur_series, cur_stage, stage_win, num3, stage_win_rate, purchaseCount);
			mGame.mOptions.BuyContinueCount++;
			mGame.SaveOptions();
			UserBehavior.Instance.BuyContinue((short)(mContinueCount + 1), (short)continueStepData.price, mGame.mCurrentStage.Series, mGame.mCurrentStage.WinType);
			UserBehavior.Save();
			if (mStageSkipDisplayed)
			{
				int num5 = mGame.SegmentProfile.StageSkipShopItemIndex;
				if (num5 >= 34)
				{
					num5 = 33;
				}
				ShopItemData shopItemData = mGame.mShopItemData[num5];
				int gemAmount = shopItemData.GemAmount;
				ServerCram.SkipStage(gemAmount, num2, num3, 2, cur_series, cur_stage, stage_win, num3, stage_win_rate);
			}
			break;
		}
		case LoseDialog.SELECT_ITEM.GIVEUP:
			StartCoroutine(AfterPlayCutinEffect("TEXT_LOSE_00", "SE_LOSE_MESSAGE", CompanionData.MOTION.LOSE, "VOICE_019", "efc_cutin_lose", delegate
			{
				SocialLoseRanking();
				MissionManager.ResultCountPuzzle(false, mUBASkill);
				List<int> list = new List<int>();
				list = MissionManager.GetShowGuageArray(MissionData.SHOWGUAGE.PUZZLE_END);
				if (list.Count != 0)
				{
					mMissionGaugeUpDialog = Util.CreateGameObject("MissionGaugeUpDialog", mRoot).AddComponent<MissionGaugeUpDialog>();
					mMissionGaugeUpDialog.Init(list, MissionGaugeUpDialog.CLEARDATA.LOSE);
					mMissionGaugeUpDialog.SetClosedCallback(OnMissionGaugeUpDialogClosed);
					mGame.TrophyGuideLoseFlg = true;
				}
				else
				{
					ShowFinishDialog();
				}
			}));
			mGame.AchievementCombo((int)mGame.mPlayer.Data.MaxCombo);
			break;
		case LoseDialog.SELECT_ITEM.STAGESKIP:
			mStageSkipSelected = true;
			mGame.AchievementCombo((int)mGame.mPlayer.Data.MaxCombo);
			mState.Change(STATE.PRE_NETWORK_WON);
			break;
		}
		if (mLoseDialog != null)
		{
			UnityEngine.Object.Destroy(mLoseDialog.gameObject);
		}
	}

	public void OnMissionGaugeUpStageSkipDialogClosed()
	{
		if (mMissionGaugeUpDialog != null)
		{
			UnityEngine.Object.Destroy(mMissionGaugeUpDialog.gameObject);
			mMissionGaugeUpDialog = null;
		}
		mState.Change(STATE.END);
	}

	private void OnContinueEffectFinished()
	{
		mPuzzleManager.Continue(mContinueCount, mExtraContinueAddMove);
		if (!mTutorialContinue)
		{
			mContinueCount++;
		}
		mGame.Save();
		mState.Change(STATE.MAIN);
	}

	public void OnWinDialogClosed(WinDialog.SELECT_ITEM i)
	{
		if (mSocialWinRanking != null)
		{
			mSocialWinRanking.Close();
			mSocialWinRanking = null;
		}
		if (mGame.mCurrentStage.Series == Def.SERIES.SM_FIRST && mGame.mCurrentStage.StageNumber == 100)
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.LEVEL1_WIN4);
		}
		switch (i)
		{
		case WinDialog.SELECT_ITEM.MAP:
			if (mGame.PlayingFriendHelp == null)
			{
				mState.Change(STATE.RANKUP);
			}
			else if (mGame.PlayingFriendHelp.IsStageCleared)
			{
				string image2 = "icon_ranking_trophy";
				mFriendHelpSuccessDialog = Util.CreateGameObject("FriendHelpSuccessDialog", mRoot).AddComponent<FriendHelpSuccessDialog>();
				mFriendHelpSuccessDialog.Init(Localization.Get("FriendHelp_Trophy_Get_Title"), Localization.Get("FriendHelp_Trophy_Get_Desc00"), Localization.Get("FriendHelp_Trophy_Get_Desc01"), image2, FriendHelpSuccessDialog.CONFIRM_IMAGE_DIALOG_STYLE.TROPHY);
				mFriendHelpSuccessDialog.SetClosedCallback(OnFriendHelpSuccessMapDialogClosed);
			}
			break;
		case WinDialog.SELECT_ITEM.REPLAY:
			mState.Change(STATE.REPLAY);
			break;
		case WinDialog.SELECT_ITEM.NEXT:
			if (mGame.PlayingFriendHelp == null)
			{
				mState.Change(STATE.NEXTSTAGE);
			}
			else if (mGame.PlayingFriendHelp.IsStageCleared)
			{
				string image = "icon_ranking_trophy";
				mFriendHelpSuccessDialog = Util.CreateGameObject("FriendHelpSuccessDialog", mRoot).AddComponent<FriendHelpSuccessDialog>();
				mFriendHelpSuccessDialog.Init(Localization.Get("FriendHelp_Trophy_Get_Title"), Localization.Get("FriendHelp_Trophy_Get_Desc00"), Localization.Get("FriendHelp_Trophy_Get_Desc01"), image, FriendHelpSuccessDialog.CONFIRM_IMAGE_DIALOG_STYLE.TROPHY);
				mFriendHelpSuccessDialog.SetClosedCallback(OnFriendHelpSuccessNextDialogClosed);
			}
			break;
		case WinDialog.SELECT_ITEM.STARGET:
			mState.Change(STATE.BUY_STARGET);
			break;
		}
		UnityEngine.Object.Destroy(mWinDialog.gameObject);
	}

	private void OnFriendHelpSuccessNextDialogClosed(FriendHelpSuccessDialog.SELECT_ITEM i)
	{
		mFriendHelpSuccessDialog = null;
		mState.Change(STATE.NEXTSTAGE);
	}

	private void OnFriendHelpSuccessMapDialogClosed(FriendHelpSuccessDialog.SELECT_ITEM i)
	{
		if (mGame.PlayingFriendHelp.IsStageCleared)
		{
			mGame.mPlayer.AddTrophy(1);
			mGame.mPlayer.RemoveGift(mGame.PlayingFriendHelp.Gift);
			mGame.SetFriendHelp(null);
			mGame.Save();
		}
		mFriendHelpSuccessDialog = null;
		mState.Change(STATE.RANKUP);
	}

	public void OnMissionGaugeUpDialogClosed()
	{
		ShowFinishDialog();
	}

	private void ShowFinishDialog()
	{
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			OnFinishDialogClosed(FinishDialog.SELECT_ITEM.NO);
			return;
		}
		mFinishDialog = Util.CreateGameObject("FinishDialog", mRoot).AddComponent<FinishDialog>();
		mFinishDialog.SetClosedCallback(OnFinishDialogClosed);
		mFinishDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
	}

	public void OnFinishDialogClosed(FinishDialog.SELECT_ITEM i)
	{
		if (mSocialWinRanking != null)
		{
			mSocialWinRanking.Close();
			mSocialWinRanking = null;
		}
		if (mSocialLoseRanking != null)
		{
			mSocialLoseRanking.Close();
			mSocialLoseRanking = null;
		}
		if (i == FinishDialog.SELECT_ITEM.RETRY)
		{
			mNetworkLostNext = STATE.REPLAY;
		}
		else
		{
			mNetworkLostNext = STATE.END;
		}
		if (mFinishDialog != null)
		{
			UnityEngine.Object.Destroy(mFinishDialog.gameObject);
		}
		mState.Change(STATE.NETWORK_LOST);
	}

	public void OnStartDialogClosed(StageStartDialog.SELECT_ITEM i)
	{
		if (mGame.mTutorialManager.IsInitialTutorialCompleted() && mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
		{
			mChancePlayEvent = delegate
			{
				mState.Change(STATE.START);
			};
			mChancePrevOpen = delegate
			{
				StartCoroutine(GrayOut());
			};
			mState.Change(STATE.SPECIALCHANCE);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.START);
		}
		UnityEngine.Object.Destroy(mStartDialog.gameObject);
	}

	public void OnStageInfoDialogClosed(StageInfoDialog.SELECT_ITEM i)
	{
		switch (i)
		{
		case StageInfoDialog.SELECT_ITEM.PLAY:
		{
			Action fnPlayGame = delegate
			{
				mState.Change(STATE.START);
				if (Def.HEART_BREAK_RETURN)
				{
					mPuzzleManager.PlayStartHeartBreak();
				}
			};
			if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				mChancePlayEvent = delegate
				{
					StartCoroutine(GrayIn());
					fnPlayGame();
				};
				mChancePrevOpen = null;
				mState.Change(STATE.SPECIALCHANCE);
			}
			else
			{
				StartCoroutine(GrayIn());
				fnPlayGame();
			}
			break;
		}
		case StageInfoDialog.SELECT_ITEM.CANCEL:
			mState.Change(STATE.END);
			break;
		}
		UnityEngine.Object.Destroy(mStageInfoDialog.gameObject);
	}

	public void OnPauseDialogClosed(StageStartDialog.SELECT_ITEM item)
	{
		switch (item)
		{
		case StageStartDialog.SELECT_ITEM.GIVEUP:
		{
			mPuzzleManager.CancelRequest();
			mPuzzleManager.PauseEnd(PuzzleManager.STATE.PLAY);
			mState.Reset(STATE.EXIT, true);
			MissionManager.ResultCountPuzzle(false, mUBASkill);
			List<int> list2 = new List<int>();
			list2 = MissionManager.GetShowGuageArray(MissionData.SHOWGUAGE.PUZZLE_END);
			for (int j = 0; j < list2.Count; j++)
			{
				MissionManager.GetMissionInfo(list2[j]);
			}
			break;
		}
		case StageStartDialog.SELECT_ITEM.RETRY:
		{
			mReplayExit = true;
			mPuzzleManager.CancelRequest();
			mPuzzleManager.PauseEnd(PuzzleManager.STATE.PLAY);
			mState.Reset(STATE.EXIT, true);
			MissionManager.ResultCountPuzzle(false, mUBASkill);
			List<int> list = new List<int>();
			list = MissionManager.GetShowGuageArray(MissionData.SHOWGUAGE.PUZZLE_END);
			for (int i = 0; i < list.Count; i++)
			{
				MissionManager.GetMissionInfo(list[i]);
			}
			break;
		}
		case StageStartDialog.SELECT_ITEM.RETURN:
			mPuzzleManager.PauseEnd(PuzzleManager.STATE.PLAY);
			StartCoroutine(GrayIn());
			mStartDialog = null;
			break;
		}
	}

	public void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public GameObject OnGetTutorialArrowTarget(Def.TUTORIAL_INDEX tutIndex)
	{
		return mPuzzleHud.GetTutorialArrowTarget(tutIndex);
	}

	public void OnTutorialTrigger(Def.TUTORIAL_INDEX tutIndex)
	{
		switch (tutIndex)
		{
		case Def.TUTORIAL_INDEX.LEVEL7_2:
			StartCoroutine(ContinueTutorial00());
			break;
		case Def.TUTORIAL_INDEX.LEVEL7_3:
			StartCoroutine(ContinueTutorial01());
			break;
		case Def.TUTORIAL_INDEX.LEVEL9_1:
			mPuzzleHud.SetSkillPower(99f);
			break;
		}
	}

	public void OnShakeScreen(float time)
	{
		StartCoroutine(ShakeScreen(time));
	}

	public void OnAttackTargetsChanged(Vector3 orignPos, Def.TILE_KIND[] newTargets, bool immediate)
	{
		if (immediate)
		{
			mPuzzleHud.SetAttackTarget(newTargets);
		}
		else
		{
			StartCoroutine(BossGimmickChangeTarget(orignPos, newTargets));
		}
	}

	public void OnPlaceBrokenWall(Vector3 orignPos, int num1, int num2, int num3)
	{
		StartCoroutine(BossGimmickPlaceBrokenWall(orignPos, num1, num2, num3));
	}

	public void OnDianaStepRemainChanged(int remain)
	{
		mPuzzleHud.SetDianaStepRemain(remain);
	}

	public void OnTalismanPlaceMovableWall(PuzzleTile orignTile, int num1, int num2, int num3)
	{
		TalismanGimmickPlaceMovableWall(orignTile, num1, num2, num3);
		mPuzzleManager.WaitStateFinished();
	}

	public void OnPausePushed()
	{
		if (mSuspend)
		{
			return;
		}
		if (mPuzzleManager.IsPausing())
		{
			if (!(mConfirmDialog != null) && !(mStartDialog != null))
			{
				mPuzzleManager.PauseEnd(PuzzleManager.STATE.PLAY);
				StartCoroutine(GrayIn());
				mStartDialog.Close();
				mStartDialog = null;
			}
		}
		else if (mPuzzleManager.IsItemUsable() && mState.GetStatus() != STATE.EXIT && !mGame.mTutorialManager.IsTutorialPlaying())
		{
			mPuzzleManager.Pause();
			StartCoroutine(GrayOut());
			mStartDialog = Util.CreateGameObject("PauseDialog", mRoot).AddComponent<StageStartDialog>();
			mStartDialog.SetClosedCallback(OnPauseDialogClosed);
			mStartDialog.Init(JewelCount, StageStartDialog.MODE.MOVE_PAUSE);
		}
	}

	public void OnSkillPushed(PuzzleTile targetTile)
	{
		int rank = mPartner.GetSkillRank();
		if (rank == 0)
		{
			return;
		}
		List<ByteVector2> list = new List<ByteVector2>(mPartner.GetSkillArea());
		IntVector2 tilePos = targetTile.TilePos;
		int count = list.Count;
		for (int num = count - 1; num >= 0; num--)
		{
			IntVector2 pos = new IntVector2(tilePos.x + list[num].x, tilePos.y + list[num].y);
			if (!mPuzzleManager.GetBackground(pos))
			{
				list.RemoveAt(num);
			}
			else
			{
				PuzzleTile attribute = mPuzzleManager.GetAttribute(pos);
				if (attribute != null && (attribute.Kind == Def.TILE_KIND.DIANA_GOAL_L || attribute.Kind == Def.TILE_KIND.DIANA_GOAL_R))
				{
					list.RemoveAt(num);
				}
			}
		}
		Def.SkillFuncData skillFuncData = mPartner.GetSkillFuncData();
		if (skillFuncData.skillType == Def.SKILL_TYPE.SELECT_TARGET)
		{
			mPuzzleManager.BeginSkillTargetSelect(skillFuncData.skillType);
		}
		mAdditionalSkillArgs = null;
		mSkillMessageDialog = Util.CreateGameObject("MessageDialog", mRoot).AddComponent<SkillMessageDialog>();
		mSkillMessageDialog.Init(mPartner.GetSkillName(), mPartner.GetSkillDescLineFeed(), targetTile, list.ToArray(), skillFuncData.skillType);
		mSkillMessageDialog.SetClosedCallback(delegate(bool canceled)
		{
			mSkillMessageDialog = null;
			if (!canceled)
			{
				object[] array = new object[1] { targetTile };
				if (mAdditionalSkillArgs != null)
				{
					array = new object[mAdditionalSkillArgs.Length + 1];
					array[0] = targetTile;
					Array.Copy(mAdditionalSkillArgs, 0, array, 1, mAdditionalSkillArgs.Length);
				}
				StartCoroutine(StartSkillEffect(rank, array));
				mUBASkill++;
			}
			else if (skillFuncData.skillType == Def.SKILL_TYPE.SELECT_TARGET)
			{
				mPuzzleManager.TargetSelectEnd();
			}
			else
			{
				mPuzzleManager.WaitStateFinished();
			}
			mPuzzleManager.TargetSelectEffectEnd();
		});
	}

	public bool OnSkillTargetSelect(PuzzleTile selectTile)
	{
		mAdditionalSkillArgs = new object[1] { selectTile };
		if (mSkillMessageDialog != null)
		{
			mSkillMessageDialog.OnUsePushed(null);
		}
		return true;
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus || mSuspend || !(mPuzzleManager != null))
		{
			return;
		}
		try
		{
			if (mPuzzleManager.mState.GetStatus() != 0 && mPuzzleManager.mState.GetStatus() != PuzzleManager.STATE.FINISH && mPuzzleManager.mState.GetStatus() != PuzzleManager.STATE.PAUSE)
			{
				Time.timeScale = 0f;
				mSuspend = true;
				mGame.StartPause();
			}
		}
		catch (Exception)
		{
		}
	}

	private bool RankUpStageScoreStartCheck()
	{
		Def.SERIES series = mGame.mCurrentStage.Series;
		int stageNumber = mGame.mCurrentStage.StageNumber;
		int mScore = mPuzzleManager.mScore;
		StageRankingData rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpStageScore(series, stageNumber, mScore, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		mState.Reset(STATE.WAIT, true);
		mRankUpNextState = STATE.RANKUP_END;
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, mGame.mCurrentStage.StageNumber, mScore, rankingData);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	private void OnRankUpDialogClosed()
	{
		mRankUpDialog = null;
		mState.Reset(mRankUpNextState, true);
	}

	public void OnTitleReturnMessageClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGame.StopMusic(0, 0.5f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	private void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		mState.Change((STATE)state);
	}

	private void SaveGameData(bool a_isWon)
	{
		if (mContinueCount > 0)
		{
			mGame.Save(true, false, true);
		}
		else
		{
			mGame.Network_CheckSave();
		}
	}

	private IEnumerator JewelGetChanceProcess()
	{
		List<ResourceInstance> list = new List<ResourceInstance>
		{
			ResourceManager.LoadImageAsync("JEWEL_GET_CHANCE"),
			ResourceManager.LoadSsAnimationAsync("LABYRINTH_JEWELGETCHANCE"),
			ResourceManager.LoadSsAnimationAsync("LABYRINTH_JEWELGETCHANCE_MASK"),
			ResourceManager.LoadSoundAsync("SE_JEWEL_GET_CHANCE00"),
			ResourceManager.LoadSoundAsync("SE_JEWEL_GET_CHANCE01")
		};
		GameObject cameraRoot2 = SkillEffectPlayer.CreateDemoCamera(true);
		Camera camera = cameraRoot2.GetComponent<Camera>();
		if (camera == null)
		{
			yield break;
		}
		camera.depth = 10f;
		while (true)
		{
			bool allDone = true;
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					allDone = false;
					break;
				}
			}
			if (allDone)
			{
				break;
			}
			yield return null;
		}
		PartsChangedSprite maskAnime3 = null;
		CHANGE_PART_INFO[] parts = new CHANGE_PART_INFO[1] { default(CHANGE_PART_INFO) };
		parts[0].partName = "change000";
		parts[0].spriteName = "color";
		maskAnime3 = Util.CreateGameObject("FadeInAnime", cameraRoot2).AddComponent<PartsChangedSprite>();
		maskAnime3.Atlas = ResourceManager.LoadImage("JEWEL_GET_CHANCE").Image;
		maskAnime3.transform.localScale = Vector3.zero;
		maskAnime3.transform.localPosition = new Vector3(10000f, 10000f, -1f);
		maskAnime3.SetBaseColor(new Color(1f, 1f, 1f, 0f));
		maskAnime3.ChangeAnime("LABYRINTH_JEWELGETCHANCE_MASK", parts, true, 0, null);
		maskAnime3.enabled = false;
		yield return null;
		mGame.PlaySe("SE_JEWEL_GET_CHANCE00", 2);
		float angle = 0f;
		maskAnime3.enabled = true;
		maskAnime3.transform.localPosition = new Vector3(0f, 0f, -1f);
		maskAnime3.transform.localScale = new Vector3(30f, 30f, 1f);
		while (true)
		{
			bool f = false;
			angle += Time.deltaTime * 346.15f;
			if (angle >= 90f)
			{
				angle = 90f;
				f = true;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f)) * 0.83f;
			maskAnime3.SetColor(new Color(1f, 1f, 1f, alpha));
			if (f)
			{
				break;
			}
			yield return null;
		}
		float _start = Time.realtimeSinceStartup;
		bool animeFinished = false;
		OneShotAnime anime2 = Util.CreateOneShotAnime("JewelGetChance", "LABYRINTH_JEWELGETCHANCE", cameraRoot2, new Vector3(0f, 0f, -1.1f), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			animeFinished = true;
		});
		while (true)
		{
			float timeSpan = Time.realtimeSinceStartup - _start;
			if (timeSpan > 3f)
			{
				animeFinished = true;
			}
			if (animeFinished)
			{
				break;
			}
			yield return null;
		}
		maskAnime3 = null;
		anime2 = null;
		UnityEngine.Object.Destroy(cameraRoot2);
		cameraRoot2 = null;
		ResourceManager.UnloadResourceByAssetBundleName(ResourceManager.GetAssetBundleNameByResourceKey("JEWEL_GET_CHANCE"));
		yield return Resources.UnloadUnusedAssets();
		mPuzzleManager.WaitStateFinished(PuzzleManager.STATE.INCREASE_WAIT);
		yield return StartCoroutine(mPuzzleManager.JewelGetChanceCoroutine(mJewel1Num, mJewel3Num, mJewel5Num, mJewel9Num));
		mPuzzleManager.SetWaitState(PuzzleManager.STATE.BEFOREPLAY);
		mPuzzleManager.WaitStateFinished();
	}

	protected IEnumerator LoadStarGetStage()
	{
		SMEventPageData eventPage = GameMain.GetCurrentEventPageData();
		List<SMMapStageSetting> stageList = eventPage.GetMapStages(9);
		for (int i = stageList.Count - 1; i >= 0; i--)
		{
			if (stageList[i].mStageNo < 9900)
			{
				stageList.RemoveAt(i);
			}
		}
		int index = UnityEngine.Random.Range(0, stageList.Count);
		SMMapStageSetting stageSetting = stageList[index];
		_stargetstage_load_finished = false;
		_stargetstage_load_success = false;
		StartCoroutine(SMStageData.CreateAsync(userSegment: mGame.GetUserSegment(), a_series: Def.SERIES.SM_EV, eventID: mGame.mPlayer.Data.CurrentEventID, stage: stageSetting.mStageNo * 100, fn: delegate(int _stage, SMStageData.EventArgs _e)
		{
			if (_e != null)
			{
				_stargetstage_load_success = true;
				mGame.PushCurrentStage(_e.StageData, stageSetting);
				STAGE_DISPLAY_NUMBER = mGame.mCurrentStageInfo.DisplayStageNo;
				mGame.mCurrentStage.DisplayStageNumber = mGame.mCurrentStageInfo.DisplayStageNo;
			}
			_stargetstage_load_finished = true;
		}));
		while (!_stargetstage_load_finished)
		{
			yield return 0;
		}
	}
}
