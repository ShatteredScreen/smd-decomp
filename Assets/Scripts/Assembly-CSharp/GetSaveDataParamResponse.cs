using System;

public class GetSaveDataParamResponse : ICloneable
{
	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int Level { get; set; }

	[MiniJSONAlias("xp")]
	public int Experience { get; set; }

	[MiniJSONAlias("total_score")]
	public long TotalScore { get; set; }

	[MiniJSONAlias("total_star")]
	public long TotalStars { get; set; }

	[MiniJSONAlias("total_trophy")]
	public long TotalTrophy { get; set; }

	public GetSaveDataParamResponse()
	{
		Series = 0;
		Level = 0;
		Experience = 0;
		TotalScore = 0L;
		TotalStars = 0L;
		TotalTrophy = 0L;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public GetSaveDataParamResponse Clone()
	{
		return MemberwiseClone() as GetSaveDataParamResponse;
	}
}
