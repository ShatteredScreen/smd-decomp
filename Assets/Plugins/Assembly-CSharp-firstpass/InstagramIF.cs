using System;
using UnityEngine;

public abstract class InstagramIF : MonoBehaviour
{
	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	public static Action<string> previewSucceededEvent;

	public static Action<string> previewFailedEvent;

	public void enableLogging(bool enable)
	{
		enableLogging(enable, enable);
	}

	public abstract void enableLogging(bool enable, bool verbose);

	public void setup(string key, string secrect)
	{
		setup(key, secrect, true);
	}

	public abstract void setup(string key, string secrect, bool autoConnect);

	public abstract void onPause();

	public abstract void onResume();

	public abstract bool isCanceled();

	public abstract bool preview(string title, string message, string link, string attachUrl, string attachMime);

	protected static string ToSS(object o)
	{
		return (o != null) ? o.ToString() : string.Empty;
	}

	protected static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	protected static void log(string format, params object[] args)
	{
		try
		{
			long num = time();
			string arg = string.Format(format, args);
			UnityEngine.Debug.Log(string.Format("@@@ {0}({1}): {2}", "InstagramPlugin", num, arg));
		}
		catch (Exception)
		{
		}
	}

	protected static void dbg(string format, params object[] args)
	{
		try
		{
			long num = time();
			string arg = string.Format(format, args);
			UnityEngine.Debug.Log(string.Format("@@@ {0}({1}): {2}", "InstagramPlugin", num, arg));
		}
		catch (Exception)
		{
		}
	}

	protected void _PluginCB_onPreviewSucceeded(string result)
	{
		if (previewSucceededEvent != null)
		{
			previewSucceededEvent(result);
		}
	}

	protected void _PluginCB_onPreviewFailed(string result)
	{
		if (previewFailedEvent != null)
		{
			previewFailedEvent(result);
		}
	}
}
