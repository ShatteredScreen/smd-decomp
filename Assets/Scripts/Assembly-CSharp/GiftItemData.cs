using System;
using System.Collections.Generic;

public class GiftItemData : SMJsonData, ICloneable, IGiftItem
{
	private enum MESSAGECARD_PARAM
	{
		MESSAGE = 0,
		CARD_ID = 1,
		PRESENT_COUNT = 2,
		DEMO_ID = 3
	}

	private string mMessage;

	public int Version { get; set; }

	public string GiftID { get; set; }

	public GiftMailType GiftSource { get; set; }

	[MiniJSONAlias("id")]
	public int RemoteID { get; set; }

	[MiniJSONAlias("from_uuid")]
	public int FromID { get; set; }

	[MiniJSONAlias("message")]
	public string Message
	{
		get
		{
			if (GiftSource == GiftMailType.GIFT || GiftSource == GiftMailType.FRIEND)
			{
				return StringUtils.ReplaceEmojiToAsterisk(mMessage);
			}
			return mMessage;
		}
		set
		{
			mMessage = value;
		}
	}

	[MiniJSONAlias("category")]
	public int ItemCategory { get; set; }

	[MiniJSONAlias("sub_category")]
	public int ItemKind { get; set; }

	[MiniJSONAlias("itemid")]
	public int ItemID { get; set; }

	[MiniJSONAlias("quantity")]
	public int Quantity { get; set; }

	[MiniJSONAlias("ctime")]
	public int RemoteTimeEpoch { get; set; }

	[MiniJSONAlias("mail_type")]
	public int MailType { get; set; }

	public bool UseIcon { get; set; }

	[MiniJSONAlias("iconid")]
	public int IconID { get; set; }

	[MiniJSONAlias("iconlvl")]
	public int IconLevel { get; set; }

	public bool UsePlayerInfo { get; set; }

	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int SeriesStage { get; set; }

	[MiniJSONAlias("fbid")]
	public string Fbid { get; set; }

	[MiniJSONAlias("play_stage")]
	public List<PlayStageParam> PlayStage { get; set; }

	public string ToID { get; private set; }

	public DateTime CreateTime { get; set; }

	public int MessageCardID
	{
		get
		{
			return GetMessageCardParam(MESSAGECARD_PARAM.CARD_ID);
		}
	}

	public int MessageCardPresentCount
	{
		get
		{
			return GetMessageCardParam(MESSAGECARD_PARAM.PRESENT_COUNT);
		}
	}

	public int MessageCardDemoID
	{
		get
		{
			return GetMessageCardParam(MESSAGECARD_PARAM.DEMO_ID);
		}
	}

	public GiftItemData()
	{
		RemoteTimeEpoch = 0;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override string ToString()
	{
		string empty = string.Empty;
		string text = empty;
		return string.Concat(text, "  RemoteID:", RemoteID, "\n  FromID:", FromID, "\n  Message:", Message, "\n  ItemCategory:", (Def.ITEM_CATEGORY)ItemCategory, "\n  ItemKind:", ItemKind, "\n  ItemID:", ItemID, "\n  Quantity:", Quantity, "\n  MailType:", MailType, "\n  GiftSource:", GiftSource, "\n  IconID:", IconID, "\n  IconLevel:", IconLevel, "\n  Series:", Series, "\n  SeriesStage:", SeriesStage, "\n  Fbid:", Fbid, "\n  RemoteTimeEpoch:", GetRemoteTimeToLocal(), "\n");
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(Version);
		data.WriteUTF(GiftID);
		data.WriteInt((int)GiftSource);
		data.WriteInt(RemoteID);
		data.WriteInt(FromID);
		data.WriteUTF(Message);
		data.WriteInt(ItemCategory);
		data.WriteInt(ItemKind);
		data.WriteInt(ItemID);
		data.WriteInt(Quantity);
		data.WriteDateTime(CreateTime);
		data.WriteInt(RemoteTimeEpoch);
		data.WriteInt(MailType);
		data.WriteBool(UseIcon);
		data.WriteInt(IconID);
		data.WriteInt(IconLevel);
		data.WriteBool(UsePlayerInfo);
		data.WriteInt(Series);
		data.WriteInt(SeriesStage);
		data.WriteUTF(Fbid);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		Version = data.ReadInt();
		GiftID = data.ReadUTF();
		GiftSource = (GiftMailType)data.ReadInt();
		RemoteID = data.ReadInt();
		FromID = data.ReadInt();
		Message = data.ReadUTF();
		ItemCategory = data.ReadInt();
		ItemKind = data.ReadInt();
		ItemID = data.ReadInt();
		Quantity = data.ReadInt();
		CreateTime = data.ReadDateTime();
		RemoteTimeEpoch = data.ReadInt();
		MailType = data.ReadInt();
		UseIcon = data.ReadBool();
		IconID = data.ReadInt();
		IconLevel = data.ReadInt();
		UsePlayerInfo = data.ReadBool();
		Series = data.ReadInt();
		SeriesStage = data.ReadInt();
		if (reqVersion >= 1010)
		{
			Fbid = data.ReadUTF();
		}
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			GiftItemData obj = new GiftItemData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
			Version = obj.Version;
			GiftID = obj.GiftID;
			GiftSource = obj.GiftSource;
			RemoteID = obj.RemoteID;
			FromID = obj.FromID;
			Message = obj.Message;
			ItemCategory = obj.ItemCategory;
			ItemKind = obj.ItemKind;
			ItemID = obj.ItemID;
			Fbid = obj.Fbid;
			Quantity = obj.Quantity;
			CreateTime = obj.CreateTime;
			RemoteTimeEpoch = obj.RemoteTimeEpoch;
		}
		catch (Exception)
		{
		}
	}

	public GiftItemData Clone()
	{
		return MemberwiseClone() as GiftItemData;
	}

	public bool IsValid()
	{
		bool result = true;
		if (ItemCategory == 0)
		{
			result = false;
		}
		return result;
	}

	public void SetGiftSource(Server.MailType _kind)
	{
		GiftSource = EnumHelper.FromObject((int)_kind, GiftMailType.LOCAL);
	}

	public DateTime GetRemoteTimeToLocal()
	{
		return DateTimeUtil.UnixTimeStampToDateTime(RemoteTimeEpoch, true);
	}

	private int GetMessageCardParam(MESSAGECARD_PARAM _param)
	{
		if (string.IsNullOrEmpty(Message))
		{
			return -1;
		}
		string[] array = Message.Split('_');
		if (array.Length <= (int)_param)
		{
			return -1;
		}
		int result;
		if (!int.TryParse(array[(int)_param], out result))
		{
			return -1;
		}
		return result;
	}
}
