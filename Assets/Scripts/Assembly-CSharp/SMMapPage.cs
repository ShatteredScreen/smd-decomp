using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMMapPage : MonoBehaviour
{
	public enum STATE
	{
		WAIT = 0,
		MAIN = 1,
		STAGE_OPEN = 2,
		STAGE_UNLOCK = 3,
		STATE_UNLOCK_WALK = 4,
		STATE_CLEAR = 5,
		ACCESSORY_OPEN = 6,
		LINE_OPEN = 7,
		LINE_UNLOCK = 8,
		SIGN_UNLOCK = 9,
		ROADBLOCK_OPEN = 10,
		ROADBLOCK_UNLOCK = 11,
		NEWAREA_OPEN = 12,
		SERIES_UNLOCK = 13,
		PARTNER_UNLOCK = 14,
		AVATAR_MOVE = 15
	}

	public enum PRESS
	{
		NONE = 0,
		GUESS = 1,
		HIT = 2
	}

	public delegate void OnLevelPushed(int stage);

	public const string MAP_ATLAS_PATH = "Atlas/Map{0:00}/Map{0:00}Atlas";

	public const float DEFAULT_MAP_Y = 0f;

	private const string DAILY_EVENT = "DailyEvent";

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	public STATE CurrentState;

	protected GameMain mGame;

	protected GameStateSMMapBase mGameState;

	protected Camera mCamera;

	protected SMMapPageData mPageData;

	protected Def.SERIES mSeries;

	protected int mMaxPage;

	protected int mMaxAvailablePage;

	protected int mMaxModulePage;

	protected int mMaxModulePageFromMaxLevel;

	public float MapAlpha = 1f;

	public float MapSpriteAlpha = 1f;

	protected GameObject[] mMapGameObject;

	protected SMMapPart[] mMapParts;

	protected MapAccessory mMapAccessory;

	protected List<MapStageButton> mStageButtonList = new List<MapStageButton>();

	protected List<MapAccessory> mMapAccessoryList = new List<MapAccessory>();

	protected List<MapLine> mLineList = new List<MapLine>();

	protected Dictionary<int, MapLine> mLineDict = new Dictionary<int, MapLine>();

	protected SMMapPart mSMMapPart;

	public SMMapSsData NewMapSsDataEntity;

	protected Dictionary<string, GameObject> mMapButtons = new Dictionary<string, GameObject>();

	protected Dictionary<int, List<SMMapRoute<float>.RouteNode>> mMapButtonByMapID = new Dictionary<int, List<SMMapRoute<float>.RouteNode>>();

	public MapAvater mAvater;

	public List<MapSNSIcon> mFriendIcons = new List<MapSNSIcon>();

	protected int mOpenStageNo;

	protected MapStageButton mOpenButton;

	protected float mOpenScale = 1f;

	protected int mUnlockStageNo;

	protected bool mIsUnlockAvatarMove;

	protected bool mIsAvatarNoMove;

	protected MapStageButton mUnlockButton;

	protected float mUnlockScale = 1f;

	protected int mClearStageNo;

	protected MapStageButton mClearButton;

	protected int mOpenLineNo;

	protected MapLine mOpenLine;

	protected int mUnlockLineNo;

	protected MapLine mUnlockLine;

	protected int mUnlockSignNo = -1;

	protected MapSign mUnlockSign;

	protected MapEntity mUnlockPartner;

	protected MapStageButton mPrevButton;

	protected int mNextMoveStageNo;

	public bool IsActiveObject;

	protected string mOpenAccessoryLayer = string.Empty;

	protected int mOpenRBNo;

	protected int mUnlockRBNo;

	protected int mNewAreaNo;

	protected float mStateTime;

	public bool IsAvatorMoving;

	protected OnLevelPushed mLevelPushedCallback;

	protected List<ResourceInstance> mLoadAsyncList = new List<ResourceInstance>();

	public bool IsInitializing;

	private int mStageOpenCount;

	private readonly Dictionary<Def.SERIES, List<int>> mSeriesLensFlare = new Dictionary<Def.SERIES, List<int>> { 
	{
		Def.SERIES.SM_FIRST,
		new List<int> { 1, 3, 10 }
	} };

	public Def.SERIES Series
	{
		get
		{
			return mSeries;
		}
	}

	public int MaxPage
	{
		get
		{
			return mMaxPage;
		}
	}

	public int MaxAvailablePage
	{
		get
		{
			return mMaxAvailablePage;
		}
	}

	public int MaxModulePageFromMaxLevel
	{
		get
		{
			return mMaxModulePageFromMaxLevel;
		}
	}

	public int MaxModulePage
	{
		get
		{
			return mMaxModulePage;
		}
	}

	public GameObject[] MapGameObject
	{
		get
		{
			return mMapGameObject;
		}
	}

	public bool IsOpenAccessory
	{
		get
		{
			return !string.IsNullOrEmpty(mOpenAccessoryLayer);
		}
	}

	public bool IsOpenRoadBlock
	{
		get
		{
			return mOpenRBNo > 0;
		}
	}

	public MapStageButton mTutorialCompButton { get; protected set; }

	protected virtual void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mCamera = Camera.main;
	}

	protected virtual void Start()
	{
	}

	public void SetGameState(GameStateSMMapBase a_map)
	{
		mGameState = a_map;
	}

	protected virtual void OnDestroy()
	{
		mOpenButton = null;
		mUnlockButton = null;
		mClearButton = null;
		mPrevButton = null;
		mOpenLine = null;
		mUnlockLine = null;
		mUnlockSign = null;
		mStageButtonList = null;
		mMapAccessoryList = null;
		mLineList = null;
		mLineDict = null;
		mSMMapPart = null;
		NewMapSsDataEntity = null;
		mMapButtons = null;
		mMapButtonByMapID = null;
	}

	protected virtual void Update()
	{
		CurrentState = mState.GetStatus();
		switch (CurrentState)
		{
		case STATE.MAIN:
			if (mState.IsChanged())
			{
				IsAvatorMoving = false;
			}
			break;
		case STATE.STAGE_OPEN:
			StageOpenProcess();
			break;
		case STATE.STAGE_UNLOCK:
			StageUnlockProcess();
			break;
		case STATE.STATE_UNLOCK_WALK:
			UnlockWalkProcess();
			break;
		case STATE.STATE_CLEAR:
			ClearProcess();
			break;
		case STATE.ACCESSORY_OPEN:
			AccessoryOpenProcess();
			break;
		case STATE.ROADBLOCK_OPEN:
			RoadBlockOpenProcess();
			break;
		case STATE.ROADBLOCK_UNLOCK:
			RoadBlockUnlockProcess();
			break;
		case STATE.AVATAR_MOVE:
			AvatarMoveProcess();
			break;
		case STATE.NEWAREA_OPEN:
			NewAreaOpenProcess();
			break;
		case STATE.LINE_OPEN:
			LineOpenProcess();
			break;
		case STATE.LINE_UNLOCK:
			LineUnlockProcess();
			break;
		case STATE.SIGN_UNLOCK:
			SignUnlockProcess();
			break;
		case STATE.PARTNER_UNLOCK:
			PartnerUnlockProcess();
			break;
		}
		mState.Update();
	}

	protected virtual void StageOpenProcess()
	{
		if (mState.IsChanged())
		{
			mOpenButton = GetStageButton(mOpenStageNo);
			if (mOpenButton == null)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			mOpenScale = mOpenButton.transform.localScale.x;
			mOpenButton.Open();
			mStateTime = 0f;
		}
		float num = 360f;
		if (mStageOpenCount > 5)
		{
			num *= 1.5f;
		}
		mStateTime += Time.deltaTime * num;
		if (mStateTime > 15f)
		{
			mStateTime = 90f;
			mState.Change(STATE.MAIN);
			mOpenButton.ChangeMode(Player.STAGE_STATUS.LOCK);
		}
	}

	protected virtual void StageUnlockProcess()
	{
		if (mState.IsChanged())
		{
			mGame.PlaySe("SE_LEVEL_UNLOCK", -1);
			mUnlockButton = GetStageButton(mUnlockStageNo);
			if (mUnlockButton == null)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			mUnlockScale = mUnlockButton.transform.localScale.x;
			mUnlockButton.Unlock();
			mStateTime = 0f;
		}
		mStateTime += Time.deltaTime * 180f;
		if (mStateTime > 90f)
		{
			mStateTime = 90f;
			mState.Change(STATE.STATE_UNLOCK_WALK);
			mUnlockButton.ChangeMode(Player.STAGE_STATUS.UNLOCK);
		}
	}

	protected virtual void UnlockWalkProcess()
	{
		if (mState.IsChanged())
		{
			if (!mIsUnlockAvatarMove)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			if (mAvater != null)
			{
				int a_main;
				int a_sub;
				Def.SplitStageNo(mUnlockStageNo, out a_main, out a_sub);
				mAvater.MoveToStageButton(mUnlockStageNo, a_sub == 0);
				IsAvatorMoving = true;
			}
		}
		if (mAvater == null || !mAvater.IsMoving())
		{
			mAvater.FindPosition();
			if (mPrevButton != null)
			{
				mPrevButton.ChangeMode();
			}
			if (mUnlockButton != null)
			{
				mUnlockButton.ChangeMode();
			}
			mState.Change(STATE.MAIN);
		}
		else if (IsAvatorMoving && mAvater != null)
		{
			Vector3 position = mAvater.Position;
			float x = base.gameObject.transform.localPosition.x;
			float y = base.gameObject.transform.localPosition.y;
			float z = base.gameObject.transform.localPosition.z;
			float num = position.y + y;
			if (num > 10f)
			{
				num = 10f;
			}
			else if (num < -10f)
			{
				num = -10f;
			}
			float num2 = y + num * -1f;
			if (num2 > mGameState.Map_DeviceOffset)
			{
				num2 = mGameState.Map_DeviceOffset;
			}
			else if (num2 < (float)(mMaxAvailablePage - 1) * 1136f * -1f - mGameState.Map_DeviceOffset)
			{
				num2 = (float)(mMaxAvailablePage - 1) * 1136f * -1f + mGameState.MAP_BOUNDS_RANGE * 0.1f - mGameState.Map_DeviceOffset;
			}
			base.gameObject.transform.localPosition = new Vector3(x, num2, z);
		}
	}

	protected virtual void ClearProcess()
	{
		if (mState.IsChanged())
		{
			mGame.PlaySe("SE_SPECIAL_TILE", -1);
			mClearButton = GetStageButton(mClearStageNo);
			if (mClearButton == null)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			mUnlockScale = mClearButton.transform.localScale.x;
			mClearButton.Cleared();
			mStateTime = 0f;
		}
		mStateTime += Time.deltaTime * 180f;
		if (mStateTime > 90f)
		{
			mStateTime = 90f;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void AccessoryOpenProcess()
	{
		if (mState.IsChanged())
		{
			OpenAccessory(mOpenAccessoryLayer);
			mStateTime = 0f;
			mOpenAccessoryLayer = string.Empty;
			mGame.PlaySe("SE_MAP_ACCESSORY_OPEN", -1);
		}
		mStateTime += Time.deltaTime * 180f;
		if (mStateTime > 180f)
		{
			mStateTime = 180f;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void RoadBlockOpenProcess()
	{
		if (mState.IsChanged())
		{
			OpenRoadBlock(mOpenRBNo);
			mStateTime = 0f;
			mOpenRBNo = 0;
			mGame.PlaySe("SE_LEVEL_UNLOCK", -1);
		}
		mStateTime += Time.deltaTime * 180f;
		if (mStateTime > 180f)
		{
			mStateTime = 180f;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void RoadBlockUnlockProcess()
	{
		if (mState.IsChanged())
		{
			UnlockRoadBlock(mUnlockRBNo);
			mStateTime = 0f;
			mUnlockRBNo = 0;
			mGame.PlaySe("SE_LEVEL_UNLOCK", -1);
		}
		mStateTime += Time.deltaTime * 120f;
		if (mStateTime > 180f)
		{
			mStateTime = 180f;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void AvatarMoveProcess()
	{
		if (mState.IsChanged())
		{
			if (mAvater != null)
			{
				mAvater.MoveToStageButton(mNextMoveStageNo);
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
		}
		if (mAvater == null || !mAvater.IsMoving())
		{
			mAvater.FindPosition();
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void NewAreaOpenProcess()
	{
		if (mState.IsChanged())
		{
			OpenNewArea(mNewAreaNo);
			mStateTime = 0f;
			mOpenRBNo = 0;
			mGame.PlaySe("SE_LEVEL_UNLOCK", -1);
		}
		mStateTime += Time.deltaTime * 180f;
		if (mStateTime > 180f)
		{
			mStateTime = 180f;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void LineOpenProcess()
	{
		if (mState.IsChanged())
		{
			if (mOpenLine == null)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			mOpenLine.LockAnime();
		}
		if (mOpenLine != null && mOpenLine.IsLockAnimeFinished)
		{
			mOpenLineNo = -1;
			mOpenLine = null;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void LineUnlockProcess()
	{
		if (mState.IsChanged())
		{
			if (mUnlockLine == null)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			mUnlockLine.UnlockAnime();
		}
		if (mUnlockLine != null && mUnlockLine.IsUnlockAnimeFinished)
		{
			mUnlockLineNo = -1;
			mUnlockLine = null;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void SignUnlockProcess()
	{
		if (mState.IsChanged())
		{
			if (mUnlockSign == null)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			mUnlockSign.UnlockAnime();
		}
		if (mUnlockSign != null && mUnlockSign.IsUnlockAnimeFinished)
		{
			mUnlockSignNo = -1;
			mUnlockSign = null;
			mState.Change(STATE.MAIN);
		}
	}

	protected virtual void PartnerUnlockProcess()
	{
		if (mState.IsChanged())
		{
			if (mUnlockPartner == null)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			mUnlockPartner.StartDisappear();
		}
		if (mUnlockPartner != null && mUnlockPartner.DisappearFinished())
		{
			mUnlockPartner = null;
			mState.Change(STATE.MAIN);
		}
	}

	public void LoadRouteAnimeAsync(Def.SERIES a_series, SMMapPageData a_data)
	{
		mPageData = a_data;
		ResourceManager.LoadSsAnimationAsync("MAP_F_ACCESSORY", true);
		ResourceManager.LoadSsAnimationAsync("MAP_F_ACCESSORY", true);
		ResImage item = ResourceManager.LoadImageAsync("MAPCOMMON", true);
		mLoadAsyncList.Add(item);
		ResSsAnimation item2 = ResourceManager.LoadSsAnimationAsync("MAP_END", true);
		mLoadAsyncList.Add(item2);
		ResSsAnimation item3 = ResourceManager.LoadSsAnimationAsync("MAP_STRAY_ICON", true);
		mLoadAsyncList.Add(item3);
	}

	public IEnumerator Init(Def.SERIES a_series, int a_maxLevel, OnLevelPushed a_lvlPushedCB, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadBlockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB)
	{
		IsInitializing = true;
		float _start = Time.realtimeSinceStartup;
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		mSeries = a_series;
		if (mSeries != mPageData.Series)
		{
		}
		mMaxModulePage = mPageData.GetMap().ModuleMaxPageNum;
		mLevelPushedCallback = a_lvlPushedCB;
		int asyncframe = 0;
		for (int i = 0; i < mLoadAsyncList.Count; i++)
		{
			while (mLoadAsyncList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				asyncframe++;
				yield return 0;
			}
		}
		_start = Time.realtimeSinceStartup;
		yield return StartCoroutine(LoadRouteData(a_series, 1136f));
		mLoadAsyncList.Clear();
		mMaxPage = NewMapSsDataEntity.GetMapIndex(a_maxLevel) + 1;
		int available = Def.GetStageNo(mPageData.GetMap().AvailableMaxLevel, 0);
		mMaxAvailablePage = NewMapSsDataEntity.GetMapIndex(available) + 1;
		int moduleLevel = Def.GetStageNo(mPageData.GetMap().ModuleMaxLevel, 0);
		mMaxModulePageFromMaxLevel = NewMapSsDataEntity.GetMapIndex(moduleLevel) + 1;
		mMapGameObject = new GameObject[mMaxModulePage];
		mMapParts = new SMMapPart[mMaxModulePage];
		_start = Time.realtimeSinceStartup;
		for (int m = 0; m < mMaxModulePage; m++)
		{
			mMapGameObject[m] = Util.CreateGameObject("MapGO" + (m + 1), base.gameObject);
			mMapParts[m] = mMapGameObject[m].AddComponent<SMMapPart>();
			mMapParts[m].SetGameState(mGameState, this, mPageData);
			Vector3 scale;
			if (!NewMapSsDataEntity.MapScaleList.TryGetValue(m, out scale))
			{
				scale = Vector3.one;
			}
			mMapParts[m].Init(mSeries, m, scale, a_accessoryCB, a_roadBlockCB, a_snsCB, a_mapChangeCB, m < mMaxPage);
			Vector3 pos = new Vector3(0f, 0f + 1136f * (float)m, Def.MAP_BASE_Z - 0.1f * (float)m);
			mMapGameObject[m].gameObject.transform.localPosition = pos;
		}
		_start = Time.realtimeSinceStartup;
		SMMapRoute<float>.RouteNode n2 = NewMapSsDataEntity.MapRoute.Root;
		SMMapRoute<float>.RouteNode p4 = null;
		float tm2 = Time.realtimeSinceStartup;
		float timer0 = 0f;
		float timer1 = 0f;
		CheckStageProgress();
		int count = 0;
		int countbtn = 0;
		while (n2 != null)
		{
			tm2 = Time.realtimeSinceStartup;
			if (n2.mIsStage)
			{
				int stageNo3 = (int)n2.Value;
				stageNo3 = Def.GetStageNo(stageNo3, 0);
				int mapIndex2;
				if (NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(n2, out mapIndex2) && mapIndex2 < mMapGameObject.Length)
				{
					GameObject go2 = mMapGameObject[mapIndex2];
					Vector3 pos3 = n2.Position;
					pos3.z = Def.MAP_BUTTON_Z;
					MapStageButton button2 = CreateStageButton(stageNo3, pos3, go2);
					mStageButtonList.Add(button2);
					mMapButtons.Add(stageNo3.ToString(), button2.gameObject);
					countbtn++;
				}
			}
			timer0 += Time.realtimeSinceStartup - tm2;
			tm2 = Time.realtimeSinceStartup;
			if (n2.NextSub != null)
			{
				for (p4 = n2.NextSub; p4 != null; p4 = p4.NextSub)
				{
					if (p4.mIsStage)
					{
						int stageNo = Def.GetStageNoByRouteOrder(p4.Value);
						int mapIndex;
						if (NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(p4, out mapIndex) && mapIndex < mMapGameObject.Length)
						{
							GameObject go = mMapGameObject[mapIndex];
							Vector3 pos2 = p4.Position;
							pos2.z = Def.MAP_BUTTON_Z;
							MapStageButton button = CreateStageButton(stageNo, pos2, go, true);
							mStageButtonList.Add(button);
							mMapButtons.Add(stageNo.ToString(), button.gameObject);
							countbtn++;
						}
					}
				}
			}
			timer1 += Time.realtimeSinceStartup - tm2;
			count++;
			n2 = n2.NextMain;
		}
		if (NewMapSsDataEntity.MapLineList.Count > 0)
		{
			_start = Time.realtimeSinceStartup;
			asyncList.Clear();
			n2 = NewMapSsDataEntity.MapLineRoute.Root;
			p4 = null;
			while (n2 != null)
			{
				if (n2.Name.ToString().Contains("Line"))
				{
					int stageNo6 = (int)n2.Value;
					stageNo6 = Def.GetStageNo(stageNo6, 0);
					int mapIndex4;
					GameObject go6 = ((!NewMapSsDataEntity.MapLineRouteMapIndex.TryGetValue(n2, out mapIndex4)) ? base.gameObject : mMapGameObject[mapIndex4]);
					SMMapPart part2 = go6.GetComponent<SMMapPart>();
					if (part2 != null)
					{
						string animeKey3 = part2.GetLineAnimeKey(mSeries, mapIndex4) + "_LOCK";
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey3, true));
						animeKey3 = part2.GetLineAnimeKey(mSeries, mapIndex4) + "_UNLOCK";
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey3, true));
						animeKey3 = part2.GetLineAnimeKey(mSeries, mapIndex4) + "_UNLOCK_IDLE";
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey3, true));
						for (int l = 0; l < asyncList.Count; l++)
						{
							while (asyncList[l].LoadState != ResourceInstance.LOADSTATE.DONE)
							{
								yield return 0;
							}
						}
						List<SsMapEntity> list2 = NewMapSsDataEntity.GetMapLineListInMapIndex(mapIndex4);
						SsMapEntity e2 = list2.Find((SsMapEntity x) => x.AnimeKey.CompareTo(n2.Value.ToString()) == 0);
						MapLine button4 = CreateLine(e2.MapOffsetCounter, e2.AnimeKey, e2.Position, e2.Scale, e2.Angle, mapIndex4, go6, part2.GetLineAnimeKey(mSeries, mapIndex4));
						mLineList.Add(button4);
						mLineDict.Add(button4.StageNo, button4);
					}
				}
				if (n2.NextSub != null)
				{
					for (p4 = n2.NextSub; p4 != null; p4 = p4.NextSub)
					{
						if (p4.Name.ToString().Contains("Line"))
						{
							int stageNo4 = Def.GetStageNoByRouteOrder(p4.Value);
							int mapIndex3;
							GameObject go5 = ((!NewMapSsDataEntity.MapLineRouteMapIndex.TryGetValue(p4, out mapIndex3)) ? base.gameObject : mMapGameObject[mapIndex3]);
							SMMapPart part = go5.GetComponent<SMMapPart>();
							if (part != null)
							{
								List<SsMapEntity> list = NewMapSsDataEntity.GetMapLineListInMapIndex(mapIndex3);
								SsMapEntity e = list.Find((SsMapEntity x) => x.AnimeKey.CompareTo(p4.Value.ToString()) == 0);
								MapLine button3 = CreateLine(e.MapOffsetCounter, e.AnimeKey, e.Position, e.Scale, e.Angle, mapIndex3, go5, part.GetLineAnimeKey(mSeries, mapIndex3));
								mLineList.Add(button3);
								mLineDict.Add(button3.StageNo, button3);
							}
						}
					}
				}
				n2 = n2.NextMain;
			}
		}
		_start = Time.realtimeSinceStartup;
		Dictionary<string, List<MapSNSSetting>> mapFriendDict = new Dictionary<string, List<MapSNSSetting>>();
		List<MyFriend> myfriends = new List<MyFriend>(mGame.mPlayer.Friends.Values);
		for (int k = 0; k < myfriends.Count; k++)
		{
			MyFriend f = myfriends[k];
			int series = f.Data.Series;
			int stage = f.Data.Level;
			int Iconlevel = f.Data.IconLevel;
			int UUID = f.Data.UUID;
			if (mGame.mPlayer.Data.CurrentSeries == (Def.SERIES)series)
			{
				MapSNSSetting setting = new MapSNSSetting
				{
					IconID = f.Data.IconID
				};
				if (setting.IconID < 0)
				{
					f.LoadIcon();
					setting.Icon = f.Icon;
				}
				setting.IconLv = Iconlevel;
				setting.UUID = UUID;
				GameObject go3;
				if (!mMapButtons.TryGetValue(stage.ToString(), out go3))
				{
					int serverMax = mGame.mGameProfile.GetMaxLevel(mSeries);
					int localMax = Def.GetStageNo(mPageData.GetMap().MaxLevel, 0);
					stage = ((serverMax <= localMax) ? localMax : serverMax);
				}
				if (!mapFriendDict.ContainsKey(stage.ToString()))
				{
					mapFriendDict.Add(stage.ToString(), new List<MapSNSSetting>());
				}
				mapFriendDict[stage.ToString()].Add(setting);
			}
		}
		int a = 0;
		_start = Time.realtimeSinceStartup;
		foreach (KeyValuePair<string, List<MapSNSSetting>> kv in mapFriendDict)
		{
			GameObject go4;
			if (mMapButtons.TryGetValue(kv.Key, out go4))
			{
				MapSNSIcon icon = Util.CreateGameObject("FriendIcon_" + kv.Key, go4.transform.parent.gameObject).AddComponent<MapSNSIcon>();
				Vector3 pos4 = go4.transform.localPosition;
				icon.UuID = kv.Value[a].UUID;
				icon.Init(kv.Value, new Vector3(pos4.x, pos4.y, Def.MAP_FRIEND_Z));
				mFriendIcons.Add(icon);
			}
		}
		for (int j = 0; j < mMaxModulePage; j++)
		{
			NGUITools.SetActive(mMapGameObject[j].gameObject, false);
		}
		_start = Time.realtimeSinceStartup;
		mAvater = Util.CreateGameObject("Avatar", base.gameObject).AddComponent<MapAvater>();
		mAvater.SetCallback(OnAvaterMoveFinished, OnAvatarSave, OnGetMapData);
		mAvater.Init(this);
		IsInitializing = false;
	}

	protected void CheckStageProgress()
	{
		if (mGame.mDebugSkipCheckStageProcess)
		{
			return;
		}
		SMMapRoute<float>.RouteNode routeNode = NewMapSsDataEntity.MapRoute.Root;
		SMMapRoute<float>.RouteNode routeNode2 = null;
		bool flag = false;
		int num = -1;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(mSeries, out data);
		if (data == null)
		{
			return;
		}
		int playableMaxLevel = data.PlayableMaxLevel;
		bool flag2 = false;
		int num2 = 0;
		while (routeNode != null)
		{
			if (routeNode.mIsStage)
			{
				num2 = (int)routeNode.Value;
				num2 = Def.GetStageNo(num2, 0);
				if (!flag)
				{
					int a_main;
					int a_sub;
					Def.SplitStageNo(num2, out a_main, out a_sub);
					SMRoadBlockSetting roadBlock = mPageData.GetRoadBlock(a_main, a_sub);
					if (roadBlock != null && !data.IsUnlockedRoadBlock(roadBlock.RoadBlockID))
					{
						int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
						int a_main2;
						int a_sub2;
						Def.SplitStageNo(mGame.mPlayer.RoadBlockReachLevel, out a_main2, out a_sub2);
						if (a_main2 == roadBlock.mStageNo && playableMaxLevel >= nextRoadBlockLevel)
						{
							flag2 = true;
						}
					}
				}
				PlayerClearData pcd2;
				if (mGame.mPlayer.GetStageClearData(mSeries, num2, out pcd2))
				{
					if (flag)
					{
						int num3 = data.TempStageClearData.FindIndex((PlayerClearData a) => a.Series == pcd2.Series && a.StageNo == pcd2.StageNo);
						if (num3 == -1)
						{
							data.TempStageClearData.Add(pcd2);
						}
						else
						{
							data.TempStageClearData[num3] = pcd2;
						}
						mGame.mPlayer.RemoveStageClearData(pcd2);
						PlayerStageData psd3;
						if (mGame.mPlayer.GetStageChallengeData(mSeries, num2, out psd3))
						{
							num3 = data.TempStageChallengeData.FindIndex((PlayerStageData a) => a.Series == psd3.Series && a.StageNo == psd3.StageNo);
							if (num3 == -1)
							{
								data.TempStageChallengeData.Add(psd3);
							}
							else
							{
								data.TempStageChallengeData[num3] = psd3;
							}
							mGame.mPlayer.RemoveStageChallengeData(psd3);
						}
						mGame.mPlayer.SetStageStatus(mSeries, num2, Player.STAGE_STATUS.NOOPEN);
					}
				}
				else
				{
					Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mSeries, num2);
					if (!flag)
					{
						if (stageStatus == Player.STAGE_STATUS.UNLOCK || stageStatus == Player.STAGE_STATUS.CLEAR)
						{
							flag = true;
							num = num2;
							mGame.mPlayer.SetPlayableMaxLevel(mSeries, num2);
							mGame.mPlayer.SetOpenNoticeLevel(mSeries, num2);
							playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
							int lastClearedLevel = mGame.mPlayer.LastClearedLevel;
							float avatarPosition = data.AvatarPosition;
							int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(avatarPosition);
							if (lastClearedLevel > 0 && lastClearedLevel > playableMaxLevel)
							{
								data.LastClearedLevel = -1;
								data.AvatarNodeName = routeNode.Name;
								data.AvatarPosition = routeNode.Value;
								data.AvatarStagePosition = playableMaxLevel;
							}
							else if (stageNoByRouteOrder > playableMaxLevel)
							{
								data.AvatarNodeName = routeNode.Name;
								data.AvatarPosition = routeNode.Value;
								data.AvatarStagePosition = playableMaxLevel;
							}
						}
						else
						{
							if (playableMaxLevel == num2)
							{
								flag = true;
								num = num2;
							}
							for (SMMapRoute<float>.RouteNode parent = routeNode.Parent; parent != null; parent = parent.Parent)
							{
								if (parent.mIsStage)
								{
									int stageNo = Def.GetStageNo((int)parent.Value, 0);
									Player.STAGE_STATUS stageStatus2 = mGame.mPlayer.GetStageStatus(mSeries, stageNo);
									if (stageStatus2 == Player.STAGE_STATUS.UNLOCK)
									{
										SMMapRoute<float>.RouteNode routeNode3 = parent;
										if (!flag)
										{
											if (mGame.mPlayer.GetStageClearData(mSeries, stageNo, out pcd2))
											{
												for (SMMapRoute<float>.RouteNode nextMain = parent.NextMain; nextMain != null; nextMain = nextMain.NextMain)
												{
													if (nextMain.mIsStage)
													{
														stageNo = Def.GetStageNo((int)nextMain.Value, 0);
														routeNode3 = nextMain;
														break;
													}
												}
												mGame.mPlayer.SetStageStatus(mSeries, stageNo, Player.STAGE_STATUS.UNLOCK);
												if (parent.NextSub != null)
												{
													for (SMMapRoute<float>.RouteNode nextMain = parent.NextSub; nextMain != null; nextMain = nextMain.NextSub)
													{
														if (nextMain.mIsStage)
														{
															mGame.mPlayer.SetStageStatus(mSeries, Def.GetStageNoByRouteOrder(parent.NextSub.Value), Player.STAGE_STATUS.UNLOCK);
															break;
														}
													}
												}
											}
											flag = true;
											num = stageNo;
											mGame.mPlayer.SetPlayableMaxLevel(mSeries, stageNo);
											mGame.mPlayer.SetOpenNoticeLevel(mSeries, stageNo);
										}
										playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
										int lastClearedLevel2 = mGame.mPlayer.LastClearedLevel;
										float avatarPosition2 = data.AvatarPosition;
										int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(avatarPosition2);
										if (lastClearedLevel2 > 0 && lastClearedLevel2 > playableMaxLevel)
										{
											data.LastClearedLevel = -1;
											data.AvatarNodeName = routeNode3.Name;
											data.AvatarPosition = routeNode3.Value;
											data.AvatarStagePosition = playableMaxLevel;
										}
										else if (stageNoByRouteOrder2 > playableMaxLevel)
										{
											data.AvatarNodeName = routeNode3.Name;
											data.AvatarPosition = routeNode3.Value;
											data.AvatarStagePosition = playableMaxLevel;
										}
										break;
									}
								}
							}
						}
					}
					else
					{
						if (stageStatus == Player.STAGE_STATUS.UNLOCK)
						{
							mGame.mPlayer.SetStageStatus(mSeries, num2, Player.STAGE_STATUS.NOOPEN);
						}
						if (mGame.mPlayer.StageUnlockList.Count > 0 && mGame.mPlayer.StageUnlockList.Contains(num2))
						{
							mGame.mPlayer.StageUnlockList.Remove(num2);
						}
					}
				}
			}
			if (routeNode.NextSub != null)
			{
				for (routeNode2 = routeNode.NextSub; routeNode2 != null; routeNode2 = routeNode2.NextSub)
				{
					if (routeNode2.mIsStage)
					{
						num2 = Def.GetStageNoByRouteOrder(routeNode2.Value);
						PlayerClearData pcd;
						if (mGame.mPlayer.GetStageClearData(mSeries, num2, out pcd))
						{
							if (flag)
							{
								int num4 = data.TempStageClearData.FindIndex((PlayerClearData a) => a.Series == pcd.Series && a.StageNo == pcd.StageNo);
								if (num4 == -1)
								{
									data.TempStageClearData.Add(pcd);
								}
								else
								{
									data.TempStageClearData[num4] = pcd;
								}
								mGame.mPlayer.RemoveStageClearData(pcd);
								PlayerStageData psd2;
								if (mGame.mPlayer.GetStageChallengeData(mSeries, num2, out psd2))
								{
									num4 = data.TempStageChallengeData.FindIndex((PlayerStageData a) => a.Series == psd2.Series && a.StageNo == psd2.StageNo);
									if (num4 == -1)
									{
										data.TempStageChallengeData.Add(psd2);
									}
									else
									{
										data.TempStageChallengeData[num4] = psd2;
									}
									mGame.mPlayer.RemoveStageChallengeData(psd2);
								}
								mGame.mPlayer.SetStageStatus(mSeries, num2, Player.STAGE_STATUS.NOOPEN);
							}
						}
						else
						{
							Player.STAGE_STATUS stageStatus3 = mGame.mPlayer.GetStageStatus(mSeries, num2);
							if (stageStatus3 == Player.STAGE_STATUS.UNLOCK && flag)
							{
								mGame.mPlayer.SetStageStatus(mSeries, num2, Player.STAGE_STATUS.NOOPEN);
							}
						}
					}
				}
			}
			if (!flag && flag2)
			{
				num2 = (int)routeNode.Value;
				num2 = Def.GetStageNo(num2, 0);
				flag = true;
				num = num2;
				int a_main3;
				int a_sub3;
				Def.SplitStageNo(num2, out a_main3, out a_sub3);
				mGame.mPlayer.SetPlayableMaxLevel(mSeries, Def.GetStageNo(a_main3 + 1, 0));
				mGame.mPlayer.SetOpenNoticeLevel(mSeries, num2);
				int lastClearedLevel3 = mGame.mPlayer.LastClearedLevel;
				float avatarPosition3 = data.AvatarPosition;
				int stageNoByRouteOrder3 = Def.GetStageNoByRouteOrder(avatarPosition3);
				if (lastClearedLevel3 > 0 && lastClearedLevel3 > num2)
				{
					data.LastClearedLevel = -1;
					data.AvatarNodeName = routeNode.Name;
					data.AvatarPosition = routeNode.Value;
					data.AvatarStagePosition = num2;
				}
				else if (stageNoByRouteOrder3 > num2)
				{
					data.AvatarNodeName = routeNode.Name;
					data.AvatarPosition = routeNode.Value;
					data.AvatarStagePosition = num2;
				}
			}
			routeNode = routeNode.NextMain;
		}
	}

	public virtual MapStageButton CreateStageButton(int a_stageno, Vector3 a_pos, GameObject a_parent, bool a_isSub = false)
	{
		BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stageno, out a_main, out a_sub);
		string text = (a_isSub ? ("stage_" + a_main + "-" + a_sub) : ("stage_" + a_main));
		Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mSeries, a_stageno);
		byte starNum = 0;
		PlayerClearData _psd;
		if (mGame.mPlayer.GetStageClearData(mSeries, a_stageno, out _psd))
		{
			starNum = (byte)_psd.GotStars;
		}
		bool a_hasItem = HasStageItem(a_main, a_sub);
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.MAPSTAGEBUTTON));
		Util.SetGameObject(gameObject, text, a_parent);
		MapStageButton component = gameObject.GetComponent<MapStageButton>();
		SMMapStageSetting mapStage = mPageData.GetMapStage(a_main, a_sub);
		float scale = component.DEFAULT_SCALE;
		if (stageStatus != Player.STAGE_STATUS.UNLOCK && stageStatus != Player.STAGE_STATUS.CLEAR)
		{
			scale = 0.75f;
		}
		Util.SetGameObject(component.gameObject, text, a_parent);
		component.SetUseCurrentAnime(true);
		if (mSeries == Def.SERIES.SM_FIRST && a_main == 11)
		{
			mTutorialCompButton = component;
		}
		bool flag = false;
		int storyDemoAtStageSelect = mapStage.GetStoryDemoAtStageSelect();
		int storyDemoAtStageClear = mapStage.GetStoryDemoAtStageClear();
		if (storyDemoAtStageSelect > -1 && !mGame.mPlayer.IsStoryCompleted(storyDemoAtStageSelect))
		{
			flag = true;
		}
		if (storyDemoAtStageClear > -1 && !mGame.mPlayer.IsStoryCompleted(storyDemoAtStageClear))
		{
			flag = true;
		}
		if (flag)
		{
			component.SetReservedFukidashiFlg("image", "icon_84epsode");
		}
		component.Init(a_pos, scale, a_stageno, starNum, stageStatus, image, "MAP_STAGEBUTTON", a_hasItem);
		component.SetGameState(mGameState);
		component.SetPushedCallback(OnStageButtonPushed);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			component.SetDebugInfo(mapStage);
		}
		return component;
	}

	public virtual MapLine CreateLine(int counter, string animeName, Vector3 pos, Vector3 scale, float a_angle, int a_mapIndex, GameObject a_parent, string animeKey)
	{
		BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
		MapLine mapLine = Util.CreateGameObject("MapLine" + counter, a_parent).AddComponent<MapLine>();
		mapLine.Init(counter, animeName, image, a_mapIndex, pos.x, pos.y, scale, a_angle, animeKey);
		return mapLine;
	}

	protected bool HasStageItem(int a_main, int a_sub, short a_course = 0)
	{
		bool result = false;
		SMMapStageSetting mapStage = mPageData.GetMapStage(a_main, a_sub, a_course);
		if (mapStage != null && mapStage.BoxID.Count > 0)
		{
			for (int i = 0; i < mapStage.BoxID.Count; i++)
			{
				if (!mGame.mPlayer.IsAccessoryUnlock(mapStage.BoxID[i]))
				{
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public void OnStageButtonPushed(GameObject go)
	{
		string[] array = go.name.Split('_');
		if (mLevelPushedCallback != null)
		{
			string text = array[1];
			string[] array2 = text.Split('-');
			int a_main = int.Parse(array2[0]);
			int a_sub = 0;
			if (array2.Length == 2)
			{
				a_sub = int.Parse(array2[1]);
			}
			int stageNo = Def.GetStageNo(a_main, a_sub);
			mLevelPushedCallback(stageNo);
		}
	}

	public virtual IEnumerator ActiveMapButton(int a_mapIndex)
	{
		yield break;
	}

	public virtual IEnumerator ActiveObjectInCamera(float y)
	{
		List<SMMapPart> atvList = new List<SMMapPart>();
		IsActiveObject = true;
		int mapIndex = NewMapSsDataEntity.GetMapIndex(Def.GetStageNoByRouteOrder(mAvater.GetCurrentNode().Value));
		for (int i = 0; i < mMapGameObject.Length; i++)
		{
			bool isNowActive = NGUITools.GetActive(mMapGameObject[i]);
			float distance = Mathf.Abs(mMapGameObject[i].gameObject.transform.localPosition.y + y);
			if ((double)distance < 1704.0)
			{
				if (!isNowActive && !mMapParts[i].IsActiveLoading)
				{
					StartCoroutine(ActiveMapButton(mapIndex));
					StartCoroutine(mMapParts[i].Active(NewMapSsDataEntity, i, mGame.RandomUserList.Count > 0 && mapIndex == i));
					atvList.Add(mMapParts[i]);
				}
			}
			else if (isNowActive)
			{
				mMapParts[i].Deactive();
			}
		}
		while (true)
		{
			bool endflag = true;
			int j = 0;
			for (int imax = atvList.Count; j < imax; j++)
			{
				if (atvList[j].IsActivating)
				{
					endflag = false;
					break;
				}
			}
			if (endflag)
			{
				break;
			}
			yield return null;
		}
		IsActiveObject = false;
	}

	public GameObject IntroRoadblockID(int cource)
	{
		GameObject result = null;
		if (mMapGameObject.Length < cource)
		{
			return result;
		}
		return mMapParts[cource - 1].IntroRoadblock();
	}

	public void MakeStrayUser()
	{
		if (mGame.RandomUserList.Count >= 0)
		{
			int mapIndex = NewMapSsDataEntity.GetMapIndex(Def.GetStageNoByRouteOrder(mAvater.GetCurrentNode().Value));
			float y = base.gameObject.transform.localPosition.y;
			for (int i = 0; i < mMapGameObject.Length; i++)
			{
				bool active = NGUITools.GetActive(mMapGameObject[i]);
				float num = Mathf.Abs(mMapGameObject[i].gameObject.transform.localPosition.y + y);
				if ((double)num < 1704.0 && active)
				{
					mMapParts[i].MakeStrayUser(mapIndex == i);
				}
			}
			if (mGame.mFbUserMngRandom == null)
			{
				mGame.mFbUserMngRandom = FBUserManager.CreateInstance();
			}
			mGame.mFbUserMngRandom.SetFBUserList(mGame.RandomUserList);
		}
		else if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL10_3))
		{
			MakeTutorialStrayUser();
		}
	}

	public void MakeTutorialStrayUser()
	{
		float y = base.gameObject.transform.localPosition.y;
		int mapIndex = NewMapSsDataEntity.GetMapIndex(Def.GetStageNoByRouteOrder(mAvater.GetCurrentNode().Value));
		for (int i = 0; i < mMapGameObject.Length; i++)
		{
			bool active = NGUITools.GetActive(mMapGameObject[i]);
			float num = Mathf.Abs(mMapGameObject[i].gameObject.transform.localPosition.y + y);
			if ((double)num < 1704.0 && active)
			{
				mMapParts[i].MakeStrayUser(mapIndex == i);
			}
		}
	}

	public void SetStrayUserEnable(bool a_flg)
	{
		float y = base.gameObject.transform.localPosition.y;
		for (int i = 0; i < mMapGameObject.Length; i++)
		{
			bool active = NGUITools.GetActive(mMapGameObject[i]);
			float num = Mathf.Abs(mMapGameObject[i].gameObject.transform.localPosition.y + y);
			if ((double)num < 1704.0 && active && !a_flg)
			{
				mMapParts[i].RemoveStrayUser();
			}
		}
	}

	public bool RemoveSilhouette(string a_layer)
	{
		MapEntity mapEntity = null;
		for (int i = 0; i < mMapGameObject.Length; i++)
		{
			mapEntity = mMapParts[i].RemoveSilhouette(a_layer);
			if (mapEntity != null)
			{
				mUnlockPartner = mapEntity;
				mState.Change(STATE.PARTNER_UNLOCK);
				return true;
			}
		}
		return false;
	}

	public bool RemoveEpisodeSilhouette(string demoID)
	{
		MapEntity mapEntity = null;
		for (int i = 0; i < mMapGameObject.Length; i++)
		{
			mapEntity = mMapParts[i].RemoveEpisodeSilhouette(demoID);
			if (mapEntity != null)
			{
				DisplayEpisodeSilhouette();
				mUnlockPartner = mapEntity;
				mState.Change(STATE.PARTNER_UNLOCK);
				return true;
			}
		}
		return false;
	}

	public virtual IEnumerator DeactiveAllObject()
	{
		for (int j = 0; j < mMapGameObject.Length; j++)
		{
			if (NGUITools.GetActive(mMapGameObject[j]))
			{
				mMapParts[j].Deactive();
			}
		}
		for (int i = 0; i < mStageButtonList.Count; i++)
		{
			UnityEngine.Object.Destroy(mStageButtonList[i].gameObject);
			mStageButtonList[i] = null;
		}
		mStageButtonList.Clear();
		mMapButtons.Clear();
		yield return Resources.UnloadUnusedAssets();
	}

	public virtual IEnumerator UnloadResource()
	{
		yield break;
	}

	public Def.PRESS OnScreenPress(Vector3 worldPos)
	{
		return Def.PRESS.NONE;
	}

	public void StartStageOpenEffect(int a_nextStage, int a_count)
	{
		mOpenStageNo = a_nextStage;
		mStageOpenCount = a_count;
		mState.Change(STATE.STAGE_OPEN);
	}

	public void StartStageClearEffect(int a_nextStage)
	{
		mClearStageNo = a_nextStage;
		mState.Change(STATE.STATE_CLEAR);
	}

	public bool StartStageUnlockEffect(int a_nextStage, bool a_isAvatarMove)
	{
		PlayerMapData currentMapData = mGame.mPlayer.CurrentMapData;
		int avatarStagePosition = currentMapData.AvatarStagePosition;
		mPrevButton = GetStageButton(avatarStagePosition);
		mIsUnlockAvatarMove = a_isAvatarMove;
		if (mIsAvatarNoMove)
		{
			mIsAvatarNoMove = false;
			mIsUnlockAvatarMove = false;
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(avatarStagePosition, out a_main, out a_sub);
		int a_main2;
		int a_sub2;
		Def.SplitStageNo(a_nextStage, out a_main2, out a_sub2);
		bool flag = a_sub != 0;
		bool flag2 = a_sub2 != 0;
		if (!flag && flag2)
		{
			mIsUnlockAvatarMove = false;
		}
		mUnlockStageNo = a_nextStage;
		mState.Change(STATE.STAGE_UNLOCK);
		return mIsUnlockAvatarMove;
	}

	public void UnlockStageButton(int a_stage, bool a_particle = true)
	{
		MapStageButton stageButton = GetStageButton(a_stage);
		stageButton.Unlock(a_particle);
	}

	public void StartAvatarMove(int a_nextStage)
	{
		mNextMoveStageNo = a_nextStage;
		mState.Change(STATE.AVATAR_MOVE);
	}

	public void StartOpenLineEffect(int a_stage)
	{
		MapLine line = GetLine(a_stage);
		if (line != null)
		{
			mOpenLineNo = a_stage;
			mOpenLine = line;
			mState.Change(STATE.LINE_OPEN);
		}
	}

	public void StartUnlockLineEffect(int a_stage)
	{
		MapLine line = GetLine(a_stage);
		if (line != null)
		{
			mUnlockLineNo = a_stage;
			mUnlockLine = line;
			mState.Change(STATE.LINE_UNLOCK);
		}
	}

	public void StartSignEffect()
	{
		MapSign mapSign = mMapParts[0].GetMapSign();
		if (mapSign != null)
		{
			mUnlockSign = mapSign;
			mUnlockLineNo = 0;
			mState.Change(STATE.SIGN_UNLOCK);
		}
	}

	public virtual void StartRoadBlockOpenEffect()
	{
		mState.Change(STATE.ROADBLOCK_OPEN);
	}

	public virtual void StartRoadBlockUnlockEffect(int a_roadStage)
	{
		mUnlockRBNo = a_roadStage;
		mGame.PlaySe("SE_RB_UNLOCK", -1);
		mState.Change(STATE.ROADBLOCK_UNLOCK);
	}

	public virtual void StartAccessoryOpenEffect()
	{
		mState.Change(STATE.ACCESSORY_OPEN);
	}

	public virtual void StartNewAreaOpenEffect(int a_newarea)
	{
		mNewAreaNo = a_newarea;
		mState.Change(STATE.NEWAREA_OPEN);
	}

	public MapStageButton GetStageButton(int a_stage)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stage, out a_main, out a_sub);
		MapStageButton result = null;
		for (int i = 0; i < mStageButtonList.Count; i++)
		{
			MapStageButton mapStageButton = mStageButtonList[i];
			if (mapStageButton.StageNo == a_stage)
			{
				result = mapStageButton;
				break;
			}
		}
		return result;
	}

	public void ChangeStageButton(int a_stage, Player.STAGE_STATUS a_status)
	{
		MapStageButton stageButton = GetStageButton(a_stage);
		stageButton.ChangeMode(a_status);
	}

	public void ChangeStageButton(int a_stage, Player.STAGE_STATUS a_status, byte num, bool a_checkItem = false)
	{
		MapStageButton stageButton = GetStageButton(a_stage);
		if (a_checkItem)
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(a_stage, out a_main, out a_sub);
			bool hasItem = HasStageItem(a_main, a_sub);
			stageButton.ChangeMode(a_status, num, hasItem);
		}
		else
		{
			stageButton.ChangeMode(a_status, num);
		}
	}

	public MapLine GetLine(int a_stage)
	{
		MapLine value = null;
		if (!mLineDict.TryGetValue(a_stage, out value))
		{
			value = null;
		}
		return value;
	}

	public void ChangeLine(int a_stage, Player.STAGE_STATUS a_status, bool a_anime = false)
	{
		MapLine line = GetLine(a_stage);
		if (line == null)
		{
			return;
		}
		switch (a_status)
		{
		case Player.STAGE_STATUS.LOCK:
			if (a_anime)
			{
				line.LockAnime();
			}
			else
			{
				line.ChangeAnime(a_status);
			}
			break;
		case Player.STAGE_STATUS.UNLOCK:
			if (a_anime)
			{
				line.UnlockAnime();
			}
			else
			{
				line.ChangeAnime(a_status);
			}
			break;
		}
	}

	public MapAccessory StageAccessory(int a_stage)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stage, out a_main, out a_sub);
		mSMMapPart = Util.CreateGameObject("Avatar", base.gameObject).AddComponent<SMMapPart>();
		AccessoryData accessoryDataOnMap = mGame.GetAccessoryDataOnMap(a_main, a_sub);
		int mapIndex = NewMapSsDataEntity.GetMapIndex(Def.GetStageNoByRouteOrder(mAvater.GetCurrentNode().Value));
		return mSMMapPart.TutorialData(NewMapSsDataEntity.GetAccessoryListInMapIndex(0), NewMapSsDataEntity.GetMapEntityListInMapIndex(0), NewMapSsDataEntity.GetMapRBListInMapIndex(0), NewMapSsDataEntity.GetMapEmitterListInMapIndex(0), NewMapSsDataEntity.GetMapMoveObjectListInMapIndex(0), NewMapSsDataEntity.GetMapStrayUserListInMapIndex(0), mGame.RandomUserList.Count > 0 && mapIndex == 0, accessoryDataOnMap);
	}

	public GameObject StageRandomFriend()
	{
		SsMapEntity ssMapEntity = NewMapSsDataEntity.GetMapStrayUserListInMapIndex(0)[0];
		GameObject gameObject = Util.CreateGameObject("StrayUser", base.gameObject);
		gameObject.transform.localPosition = ssMapEntity.Position;
		return gameObject;
	}

	public virtual void OpenNextLine(int a_cleared)
	{
	}

	public virtual void UnlockNextLine(int a_cleared)
	{
	}

	public virtual void OpenNewStageUntilNextChapter(int a_current, int a_nextChapter)
	{
		List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(a_current, a_nextChapter, true);
		Player mPlayer = mGame.mPlayer;
		PlayerMapData currentMapData = mGame.mPlayer.CurrentMapData;
		for (int i = 0; i < stages.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stages[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS value;
			if (currentMapData.StageStatus.TryGetValue(stageNoByRouteOrder, out value))
			{
				if (value == Player.STAGE_STATUS.NOOPEN)
				{
					mPlayer.StageOpenList.Add(stageNoByRouteOrder);
				}
			}
			else
			{
				mPlayer.StageOpenList.Add(stageNoByRouteOrder);
			}
		}
	}

	public void OpenNewStageUntilRB(int a_current, int a_nextRB)
	{
		List<SMMapRoute<float>.RouteNode> stageUntilNextRB = NewMapSsDataEntity.GetStageUntilNextRB(a_current, a_nextRB);
		Player mPlayer = mGame.mPlayer;
		PlayerMapData currentMapData = mGame.mPlayer.CurrentMapData;
		for (int i = 0; i < stageUntilNextRB.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stageUntilNextRB[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS value;
			if (currentMapData.StageStatus.TryGetValue(stageNoByRouteOrder, out value))
			{
				if (value == Player.STAGE_STATUS.NOOPEN)
				{
					mPlayer.StageOpenList.Add(stageNoByRouteOrder);
				}
			}
			else
			{
				mPlayer.StageOpenList.Add(stageNoByRouteOrder);
			}
		}
	}

	public virtual float GetStageWorldPos(int a_stageNo)
	{
		SMMapRoute<float>.RouteNode stage = NewMapSsDataEntity.GetStage(a_stageNo, true);
		return stage.mRoutePosition.y;
	}

	public int GetMapIndexByStage(int a_stageno)
	{
		return NewMapSsDataEntity.GetMapIndex(a_stageno);
	}

	public List<SMMapRoute<float>.RouteNode> GetStageList(int a_current, int a_last, bool a_include = true)
	{
		return NewMapSsDataEntity.GetStages(a_current, a_last, a_include);
	}

	public void SetAvatarNode(int a_stageno, bool a_move = false)
	{
		PlayerMapData playerMapData = OnGetMapData();
		if (playerMapData == null)
		{
			return;
		}
		SMMapRoute<float>.RouteNode stage = NewMapSsDataEntity.GetStage(a_stageno, true);
		if (stage != null)
		{
			playerMapData.AvatarNodeName = stage.Name;
			playerMapData.AvatarPosition = stage.Value;
			playerMapData.AvatarStagePosition = a_stageno;
			mGame.mPlayer.Data.SetMapData(mSeries, playerMapData);
			if (a_move)
			{
				mAvater.FindPosition();
				ResImage resImage = ResourceManager.LoadImage("HUD");
				OneShotAnime oneShotAnime = Util.CreateOneShotAnime("AvatarAppear", "MAP_AVATAR_APPEAR", mAvater.gameObject, new Vector3(mAvater.Position.x, mAvater.Position.y + 73f, mAvater.Position.z - 1f), Vector3.one, 0f, 0f, null, resImage.Image);
			}
		}
	}

	public virtual float UnlockNewStageSub(int a_playable, int a_notice)
	{
		float result = 0f;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(mSeries, out data);
		int a_main;
		int a_sub;
		if (a_playable >= a_notice)
		{
			Def.SplitStageNo(a_notice, out a_main, out a_sub);
			int num = a_notice;
			if (a_playable == a_notice)
			{
				Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mSeries, num);
				if (stageStatus == Player.STAGE_STATUS.LOCK && a_main > 1)
				{
					num = Def.GetStageNo(a_main - 1, 0);
				}
			}
			List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(num, a_playable, false);
			foreach (SMMapRoute<float>.RouteNode item in stages)
			{
				if (item == null)
				{
					continue;
				}
				int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(item.Value);
				Def.SplitStageNo(stageNoByRouteOrder, out a_main, out a_sub);
				SMRoadBlockSetting roadBlock = mPageData.GetRoadBlock(a_main, a_sub);
				if (roadBlock != null)
				{
					int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
					int a_main2;
					int a_sub2;
					Def.SplitStageNo(mGame.mPlayer.RoadBlockReachLevel, out a_main2, out a_sub2);
					if (a_playable >= nextRoadBlockLevel && a_main2 == roadBlock.mStageNo)
					{
						if (mGame.mPlayer.StageUnlockList.Count == 0)
						{
							mGame.mPlayer.SetOpenNoticeLevel(mSeries, stageNoByRouteOrder);
						}
						break;
					}
				}
				AccessoryData accessoryDataOnMap = mGame.GetAccessoryDataOnMap(a_main, a_sub);
				if (accessoryDataOnMap != null && accessoryDataOnMap.IsStop == 1 && !mGame.mPlayer.IsAccessoryUnlock(accessoryDataOnMap.Index))
				{
					if (mGame.mPlayer.StageUnlockList.Count == 0)
					{
						mGame.mPlayer.SetOpenNoticeLevel(mSeries, stageNoByRouteOrder);
					}
					break;
				}
				PlayerClearData _psd;
				if (!mGame.mPlayer.GetStageClearData(mSeries, stageNoByRouteOrder, out _psd))
				{
					continue;
				}
				if (item.NextMain != null)
				{
					SMMapRoute<float>.RouteNode routeNode = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextMain, true, false);
					if (routeNode != null)
					{
						int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode.Value);
						switch (mGame.mPlayer.GetStageStatus(mSeries, stageNoByRouteOrder2))
						{
						case Player.STAGE_STATUS.LOCK:
							if (!mGame.mPlayer.StageUnlockList.Contains(stageNoByRouteOrder2))
							{
								mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder2);
							}
							mGame.mPlayer.SetOpenNoticeLevel(mSeries, stageNoByRouteOrder2);
							break;
						case Player.STAGE_STATUS.UNLOCK:
						case Player.STAGE_STATUS.CLEAR:
							if (!mGame.mPlayer.AvatarMoveList.Contains(stageNoByRouteOrder2))
							{
								mGame.mPlayer.AvatarMoveList.Add(stageNoByRouteOrder2);
							}
							mGame.mPlayer.SetOpenNoticeLevel(mSeries, stageNoByRouteOrder2);
							break;
						case Player.STAGE_STATUS.NOOPEN:
							if (mGame.mPlayer.StageOpenList.Contains(stageNoByRouteOrder2))
							{
								if (!mGame.mPlayer.StageUnlockList.Contains(stageNoByRouteOrder2))
								{
									mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder2);
								}
								mGame.mPlayer.SetOpenNoticeLevel(mSeries, stageNoByRouteOrder2);
							}
							break;
						}
					}
				}
				if (item.NextSub == null)
				{
					continue;
				}
				SMMapRoute<float>.RouteNode routeNode2 = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextSub, false, true);
				if (routeNode2 != null)
				{
					int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode2.Value);
					Player.STAGE_STATUS stageStatus2 = mGame.mPlayer.GetStageStatus(mSeries, stageNoByRouteOrder2);
					if (stageStatus2 == Player.STAGE_STATUS.LOCK && !mGame.mPlayer.StageUnlockList.Contains(stageNoByRouteOrder2))
					{
						mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder2);
					}
				}
			}
		}
		int stage;
		foreach (int stageUnlock in mGame.mPlayer.StageUnlockList)
		{
			stage = stageUnlock;
			mGame.mPlayer.AvatarMoveList.RemoveAll((int p) => p < stage);
		}
		Def.SplitStageNo(a_playable, out a_main, out a_sub);
		SMMapRoute<float>.RouteNode routeNode3 = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main, a_sub, a_sub != 0));
		if (routeNode3 != null)
		{
			result = routeNode3.RoutePosition.y;
		}
		return result;
	}

	public virtual float UnlockNewStage(int a_playable, int a_notice, int a_cleared)
	{
		int a_main = -1;
		int a_sub = -1;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(mSeries, out data);
		if (a_cleared > 0)
		{
			Def.SplitStageNo(a_cleared, out a_main, out a_sub);
			AccessoryData accessoryDataOnMap = mGame.GetAccessoryDataOnMap(a_main, a_sub);
			if (accessoryDataOnMap != null)
			{
				mOpenAccessoryLayer = accessoryDataOnMap.AnimeLayerName;
			}
			if (a_sub > 0)
			{
				SMMapRoute<float>.RouteNode routeNode = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main, a_sub, a_sub != 0));
				if (routeNode != null && routeNode.IsSub && routeNode.NextSub != null)
				{
					SMMapRoute<float>.RouteNode routeNode2 = NewMapSsDataEntity.MapRoute.FindContain("Stage", routeNode.NextSub, false, true);
					if (routeNode2 != null)
					{
						int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode2.Value);
						Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mSeries, stageNoByRouteOrder);
						if (stageStatus == Player.STAGE_STATUS.LOCK)
						{
							mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
						}
					}
					return routeNode.RoutePosition.y;
				}
				return mAvater.Position.y;
			}
			int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
			int roadBlockReachLevel = mGame.mPlayer.RoadBlockReachLevel;
			if (a_cleared == nextRoadBlockLevel && roadBlockReachLevel < nextRoadBlockLevel)
			{
				data.RoadBlockReachLevel = nextRoadBlockLevel;
				DateTime now = DateTime.Now;
				data.RoadBlockReachTime = now;
				int roadBlockReachLevel2 = data.RoadBlockReachLevel;
				int cur_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
				int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
				ServerCram.ReachRoadblock(roadBlockReachLevel2, cur_time, currentSeries);
				mOpenRBNo = nextRoadBlockLevel;
			}
		}
		else
		{
			int avatarStagePosition = data.AvatarStagePosition;
			Def.SplitStageNo(avatarStagePosition, out a_main, out a_sub);
			PlayerClearData _psd;
			if (mGame.mPlayer.GetStageClearData(mSeries, avatarStagePosition, out _psd))
			{
				SMMapRoute<float>.RouteNode routeNode3 = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main, a_sub, a_sub != 0));
				if (routeNode3 != null && routeNode3.IsSub && routeNode3.NextSub != null)
				{
					SMMapRoute<float>.RouteNode routeNode4 = NewMapSsDataEntity.MapRoute.FindContain("Stage", routeNode3.NextSub, false, true);
					if (routeNode4 != null)
					{
						int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode4.Value);
						Player.STAGE_STATUS stageStatus2 = mGame.mPlayer.GetStageStatus(mSeries, stageNoByRouteOrder);
						if (stageStatus2 == Player.STAGE_STATUS.LOCK)
						{
							mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
						}
					}
					return routeNode3.RoutePosition.y;
				}
			}
		}
		mGame.mPlayer.Data.SetMapData(mSeries, data);
		return UnlockNewStageSub(a_playable, a_notice);
	}

	public virtual void DisplayAllAccessory()
	{
		Player mPlayer = mGame.mPlayer;
		int stageNo = Def.GetStageNo(mPageData.GetMap().ModuleMaxLevel, 0);
		int a_last = stageNo;
		List<SMMapRoute<float>.RouteNode> stageList = GetStageList(100, a_last);
		for (int i = 0; i < stageList.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stageList[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			foreach (AccessoryData mMapAccessoryDatum in mGame.mMapAccessoryData)
			{
				if (mMapAccessoryDatum.Series == mGame.mPlayer.Data.CurrentSeries && mMapAccessoryDatum.AccessoryUnlockScene.CompareTo("MAP") == 0)
				{
					int stageNo2 = Def.GetStageNo(mMapAccessoryDatum.MainStage, mMapAccessoryDatum.SubStage);
					if (stageNoByRouteOrder == stageNo2)
					{
						NewMapSsDataEntity.UpdateDisplayMapAccessoryDataByLayer(mMapAccessoryDatum.AnimeLayerName, true);
					}
				}
			}
		}
	}

	public virtual void DisplaySilhouette()
	{
		int stageNo = Def.GetStageNo(mPageData.GetMap().AvailableMaxLevel, 0);
		List<SMMapRoute<float>.RouteNode> stageList = GetStageList(100, stageNo);
		for (int i = 0; i < stageList.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stageList[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			foreach (AccessoryData mMapAccessoryDatum in mGame.mMapAccessoryData)
			{
				if (mMapAccessoryDatum.AccessoryUnlockScene.CompareTo("MAP") == 0)
				{
					int stageNo2 = Def.GetStageNo(mMapAccessoryDatum.MainStage, mMapAccessoryDatum.SubStage);
					if (stageNoByRouteOrder == stageNo2)
					{
						bool a_flg = !mGame.mPlayer.IsAccessoryUnlock(mMapAccessoryDatum.Index);
						NewMapSsDataEntity.UpdateDisplaySilhouetteByLayer(mMapAccessoryDatum.AnimeLayerName, a_flg);
					}
				}
			}
		}
	}

	public virtual void DisplayEpisodeSilhouette()
	{
	}

	public virtual void DisplayAccessoryByRB(int a_nextRB)
	{
		List<SMMapRoute<float>.RouteNode> stageUntilNextRB = NewMapSsDataEntity.GetStageUntilNextRB(100, a_nextRB);
		Player mPlayer = mGame.mPlayer;
		for (int i = 0; i < stageUntilNextRB.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stageUntilNextRB[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			foreach (AccessoryData mAccessoryDatum in mGame.mAccessoryData)
			{
				if (mAccessoryDatum.AccessoryUnlockScene.CompareTo("MAP") == 0)
				{
					int stageNo = Def.GetStageNo(mAccessoryDatum.MainStage, mAccessoryDatum.SubStage);
					if (stageNoByRouteOrder == stageNo)
					{
						NewMapSsDataEntity.UpdateDisplayMapAccessoryDataByLayer(mAccessoryDatum.AnimeLayerName, true);
					}
				}
			}
		}
	}

	public void DisplayAccessoryByChapter(int a_nextChapter)
	{
		List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(100, a_nextChapter, true);
		Player mPlayer = mGame.mPlayer;
		for (int i = 0; i < stages.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stages[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			foreach (AccessoryData mAccessoryDatum in mGame.mAccessoryData)
			{
				if (mAccessoryDatum.AccessoryUnlockScene.CompareTo("MAP") == 0)
				{
					int stageNo = Def.GetStageNo(mAccessoryDatum.MainStage, mAccessoryDatum.SubStage);
					if (stageNoByRouteOrder == stageNo)
					{
						NewMapSsDataEntity.UpdateDisplayMapAccessoryDataByLayer(mAccessoryDatum.AnimeLayerName, true);
					}
				}
			}
		}
	}

	public void OpenAccessory(string a_layer)
	{
		for (int i = 0; i < mMapParts.Length && !mMapParts[i].OpenAccessory(a_layer); i++)
		{
		}
	}

	public void OpenRoadBlock(int a_roadstage)
	{
		for (int i = 0; i < mMapParts.Length && !mMapParts[i].OpenRoadBlock(a_roadstage); i++)
		{
		}
	}

	public virtual void UnlockRoadBlock(int a_roadstage)
	{
		for (int i = 0; i < mMapParts.Length && !mMapParts[i].UnlockRoadBlock(a_roadstage); i++)
		{
		}
	}

	public virtual void OpenNewArea(int a_page)
	{
		if (mMapParts.Length > a_page)
		{
			for (int i = 0; i < a_page; i++)
			{
				mMapParts[i].OpenNewAreaHide();
			}
			mMapParts[a_page].OpenNewArea();
		}
	}

	public virtual bool IsEffectFinished()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			return true;
		}
		return false;
	}

	public virtual void OnAvaterMoveFinished(int a_stageno)
	{
		MapStageButton stageButton = GetStageButton(a_stageno);
		if (stageButton != null)
		{
			Util.CreateOneShotAnime(pos: new Vector3(0f, 0f, -0.1f), name: "MapArriveAnime", key: "MAP_STAGEARRIVAL", parent: stageButton.gameObject, scale: Vector3.one, rotateDeg: 0f, delay: 0f);
			stageButton.Arrival();
		}
	}

	public virtual void OnAvatarSave(string a_nodeName, int a_stageno, float a_pos)
	{
		PlayerMapData playerMapData = OnGetMapData();
		playerMapData.AvatarNodeName = a_nodeName;
		playerMapData.AvatarStagePosition = a_stageno;
		playerMapData.AvatarPosition = a_pos;
		mGame.mPlayer.Data.SetMapData(mSeries, playerMapData);
		mGame.Save();
	}

	public virtual PlayerMapData OnGetMapData()
	{
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(mSeries, out data);
		return data;
	}

	public void UpdateMapButtonState()
	{
		for (int i = 0; i < mStageButtonList.Count; i++)
		{
			MapStageButton mapStageButton = mStageButtonList[i];
			int stageNo = mapStageButton.StageNo;
			int a_main;
			int a_sub;
			Def.SplitStageNo(stageNo, out a_main, out a_sub);
			bool a_hasItem = HasStageItem(a_main, a_sub);
			mapStageButton.ChangeMode(a_hasItem);
		}
	}

	public void SetEnableButton(bool a_flg)
	{
		for (int i = 0; i < mStageButtonList.Count; i++)
		{
			MapStageButton mapStageButton = mStageButtonList[i];
			mapStageButton.SetEnable(a_flg);
		}
		for (int j = 0; j < mMapParts.Length; j++)
		{
			SMMapPart sMMapPart = mMapParts[j];
			sMMapPart.SetEnable(a_flg);
		}
	}

	public bool IsPlayableLensFlare(out float y)
	{
		y = 0f;
		SMMapRoute<float>.RouteNode currentNode = mAvater.GetCurrentNode();
		if (currentNode == null)
		{
			return false;
		}
		int value;
		if (!NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(currentNode, out value))
		{
			return false;
		}
		y = currentNode.mRoutePosition.y;
		return true;
	}

	public void SetAvatarPositionOnStage(int a_stageno)
	{
		if (mAvater != null)
		{
			mAvater.FindPosition(a_stageno);
		}
	}

	private GameObject GetDailyEventObject(int a_stageno, out GameObject button)
	{
		button = null;
		if (!mMapButtons.TryGetValue(a_stageno.ToString(), out button) || !button)
		{
			return null;
		}
		SsSprite[] componentsInChildren = button.GetComponentsInChildren<SsSprite>(true);
		if (componentsInChildren != null && componentsInChildren.Length > 0)
		{
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				if (string.Compare(componentsInChildren[i].gameObject.name, "DailyEvent", StringComparison.OrdinalIgnoreCase) == 0)
				{
					return componentsInChildren[i].gameObject;
				}
			}
		}
		return null;
	}

	public void CreateDailyEvent(int a_stageno, string _key, bool _force)
	{
		GameObject button = null;
		GameObject dailyEventObject = GetDailyEventObject(a_stageno, out button);
		if (_force)
		{
			UnityEngine.Object.DestroyImmediate(dailyEventObject);
		}
		else if (dailyEventObject != null)
		{
			return;
		}
		SsSprite ssSprite = Util.CreateGameObject("DailyEvent", button).AddComponent<SsSprite>();
		ssSprite.transform.localPosition = new Vector3(0f, 0f, -0.1f);
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_key);
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		ssSprite.Animation = ssAnime;
		ssSprite.ResetAnimationStatus();
		ssSprite.Play();
		ssSprite.PlayCount = 0;
		DailyEventTimer dailyEventTimer = Util.CreateGameObject("Timer", ssSprite.gameObject).AddComponent<DailyEventTimer>();
		dailyEventTimer.InitHHMM(0, new Vector3(0f, 0f, -0.2f));
	}

	public void RemoveDailyEvent(int a_stageno)
	{
		GameObject button = null;
		GameObject dailyEventObject = GetDailyEventObject(a_stageno, out button);
		if (!(dailyEventObject == null))
		{
			UnityEngine.Object.Destroy(dailyEventObject);
		}
	}

	public virtual void UpdateMapInfomation(int a_maxLevel)
	{
		mMaxPage = NewMapSsDataEntity.GetMapIndex(a_maxLevel) + 1;
		int stageNo = Def.GetStageNo(mPageData.GetMap().AvailableMaxLevel, 0);
		mMaxAvailablePage = NewMapSsDataEntity.GetMapIndex(stageNo) + 1;
		int stageNo2 = Def.GetStageNo(mPageData.GetMap().ModuleMaxLevel, 0);
		mMaxModulePageFromMaxLevel = NewMapSsDataEntity.GetMapIndex(stageNo2) + 1;
	}

	public virtual void SetLayout(bool a_isLandscape)
	{
		for (int i = 0; i < mMapParts.Length; i++)
		{
			mMapParts[i].SetLayout(a_isLandscape);
		}
	}

	protected virtual IEnumerator LoadRouteData(Def.SERIES a_series, float a_mapSize, int a_eventID = -1, short a_courseID = -1)
	{
		bool useBin = true;
		if (Def.SeriesMapRouteData.ContainsKey(a_series))
		{
			useBin = true;
		}
		else if (a_series == Def.SERIES.SM_EV)
		{
			useBin = false;
		}
		if (useBin)
		{
			NewMapSsDataEntity = mGame.GetMapRouteData(a_series, a_eventID, a_courseID, a_mapSize);
			if (NewMapSsDataEntity == null)
			{
				BIJLog.E(string.Concat("ROUTE LOAD ERROR!!!", a_series, ": ", a_eventID, " ", a_courseID));
			}
		}
		if (NewMapSsDataEntity != null)
		{
			yield break;
		}
		string routeKey = string.Empty;
		if (a_courseID > -1)
		{
			SMEventPageData eventPageData = mPageData as SMEventPageData;
			routeKey = eventPageData.EventCourseList[a_courseID].RouteAnimeKey;
			for (int k = 0; k < mMaxModulePage; k++)
			{
				string anime_key2 = routeKey + "_" + (k + 1);
				ResSsAnimation entityAnime2 = ResourceManager.LoadSsAnimationAsync(anime_key2, true);
				mLoadAsyncList.Add(entityAnime2);
			}
		}
		else
		{
			string prefix = Def.SeriesPrefix[mSeries];
			for (int j = 0; j < mMaxModulePage; j++)
			{
				string anime_key = string.Format("MAP_PAGE_{0}_ANIMATION_{1}", prefix, j + 1);
				ResSsAnimation entityAnime = ResourceManager.LoadSsAnimationAsync(anime_key, true);
				mLoadAsyncList.Add(entityAnime);
			}
		}
		for (int i = 0; i < mLoadAsyncList.Count; i++)
		{
			while (mLoadAsyncList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		NewMapSsDataEntity = new SMMapSsData();
		NewMapSsDataEntity.MapSize(a_mapSize);
		if (!string.IsNullOrEmpty(routeKey))
		{
			NewMapSsDataEntity.Load(mMaxModulePage, mSeries, routeKey);
		}
		else
		{
			NewMapSsDataEntity.Load(mMaxModulePage, mSeries);
		}
	}

	public void ShownDemo(int a_stageNo)
	{
		MapStageButton stageButton = GetStageButton(a_stageNo);
		if (stageButton != null)
		{
			stageButton.SetFukidashi(false, "image", "icon_84epsode");
		}
	}
}
