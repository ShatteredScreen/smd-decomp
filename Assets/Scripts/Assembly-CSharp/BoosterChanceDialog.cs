using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class BoosterChanceDialog : DialogBase
{
	private enum LOAD_ATLAS
	{
		SHOP = 0,
		SPECIAL_CHANCE_DIALOG = 1,
		HUD = 2,
		DIALOG_BASE = 3
	}

	public enum SELECT_ITEM
	{
		CANCEL = 0,
		CONSUME = 1,
		FIRST_CONSUME = 2
	}

	public enum PURCHASE_TYPE
	{
		NONE = 0,
		LIGHT = 1,
		HEAVY = 2
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		MAINTENANCE_CHECK_START = 2,
		MAINTENANCE_CHECK_WAIT = 3,
		MAINTENANCE_CHECK_START2 = 4,
		MAINTENANCE_CHECK_WAIT2 = 5,
		NETWORK_00 = 6,
		NETWORK_00_1 = 7,
		NETWORK_00_2 = 8,
		NETWORK_01 = 9,
		NETWORK_01_1 = 10,
		NETWORK_01_2 = 11,
		NETWORK_02 = 12,
		NETWORK_02_1 = 13,
		NETWORK_02_2 = 14,
		AUTHERROR_WAIT = 15
	}

	private enum PROB_TYPE
	{
		NONE = 0,
		FAILURE = 1,
		SUCCESS = 2
	}

	public enum AB_TEST_PLACE : byte
	{
		FIRST = 0,
		R = 1,
		S = 2,
		SS = 3,
		StarS = 4,
		Rally = 5,
		Star = 6,
		Rally2 = 7,
		Bingo = 8,
		Labyrinth = 9,
		DailyChallenge = 10,
		GF00 = 11,
		OldEvent = 12
	}

	public enum AB_TEST_PARAM : byte
	{
		FULL = 0,
		ODD = 1,
		EVEN = 2,
		NONE = 3
	}

	private sealed class ClosedCallback : UnityEvent<SELECT_ITEM>
	{
	}

	private sealed class AtlasInfo : IDisposable
	{
		private bool mForceNotUnload;

		private bool mDisposed;

		public ResImage Resource { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Resource == null) ? null : Resource.Image;
			}
		}

		public string Key { get; private set; }

		public bool IsInit { get; private set; }

		public AtlasInfo(string key)
		{
			Key = key;
		}

		~AtlasInfo()
		{
			Dispose(false);
		}

		public void Load(bool not_unload = false)
		{
			if (!not_unload)
			{
				Resource = ResourceManager.LoadImageAsync(Key);
			}
			else
			{
				Resource = ResourceManager.LoadImage(Key);
			}
			IsInit = true;
			mForceNotUnload = not_unload;
		}

		public void UnLoad()
		{
			if (IsInit && !mForceNotUnload)
			{
				Resource = null;
				ResourceManager.UnloadImage(Key);
			}
			IsInit = false;
		}

		public bool IsDone()
		{
			return Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.DONE;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	private abstract class LayoutInfo
	{
		private SpecialChanceConditionSet.CHANCE_MODE mMode = SpecialChanceConditionSet.CHANCE_MODE.ERROR;

		private bool mIsFirstPurchase;

		private Dictionary<LOAD_ATLAS, BIJImage> mAtlas = new Dictionary<LOAD_ATLAS, BIJImage>();

		protected SpecialChanceConditionSet.CHANCE_MODE Mode
		{
			get
			{
				return mMode;
			}
		}

		protected bool IsFirstPurchase
		{
			get
			{
				return mIsFirstPurchase;
			}
		}

		public string CaptionName
		{
			get
			{
				return "LC_chance";
			}
		}

		public virtual Color UnderDescColor
		{
			get
			{
				return Def.DEFAULT_MESSAGE_COLOR;
			}
		}

		public string CharacterName
		{
			get
			{
				return "chance_chara";
			}
		}

		public virtual Vector3 CharacterPositionOffset
		{
			get
			{
				switch (mMode)
				{
				case SpecialChanceConditionSet.CHANCE_MODE.PLUS:
					return new Vector2(-5f, 0f);
				case SpecialChanceConditionSet.CHANCE_MODE.SPECIAL:
					return new Vector2(6f, 0f);
				default:
					return Vector2.zero;
				}
			}
		}

		public virtual Vector3 CaptionPanelPos
		{
			get
			{
				return new Vector3(0f, 115f);
			}
		}

		public virtual int CaptionPanelDimensionHeight
		{
			get
			{
				return 113;
			}
		}

		public virtual Vector3 CaptionPos
		{
			get
			{
				return new Vector3(0f, 60f);
			}
		}

		public virtual Vector3 InnerPos
		{
			get
			{
				return Vector3.zero;
			}
		}

		public virtual int InnerDimensionHeight
		{
			get
			{
				return 200;
			}
		}

		public virtual bool IsShowUnderDesc
		{
			get
			{
				return true;
			}
		}

		public KeyValuePair<LOAD_ATLAS, BIJImage>[] Atlas
		{
			set
			{
				mAtlas.Clear();
				if (value != null)
				{
					for (int i = 0; i < value.Length; i++)
					{
						KeyValuePair<LOAD_ATLAS, BIJImage> keyValuePair = value[i];
						mAtlas.Add(keyValuePair.Key, keyValuePair.Value);
					}
				}
			}
		}

		public string BaseAtlasName { protected get; set; }

		public virtual LOAD_ATLAS PurchaceButtonAtlasType
		{
			get
			{
				return LOAD_ATLAS.HUD;
			}
		}

		public virtual string PurchaceButtonSpriteName
		{
			get
			{
				return "LC_button_buy";
			}
		}

		public LayoutInfo(SpecialChanceConditionSet.CHANCE_MODE mode, bool is_first_purchase)
		{
			mMode = mode;
			mIsFirstPurchase = is_first_purchase;
		}

		public virtual void CreateTopDescText(GameObject parent, UIFont font, int depth)
		{
		}

		public virtual void CreateFirstPurchaseFrame(ShopItemData shop, GameObject parent, UIFont font, int depth)
		{
			if (IsFirstPurchase && shop != null)
			{
				BIJImage atlas = GetAtlas(LOAD_ATLAS.HUD);
				UISprite uISprite = Util.CreateSprite("DefaultGemFrame", parent, atlas);
				Util.SetSpriteInfo(uISprite, "instruction_accessory12", depth, new Vector3(0f, -123f), Vector3.one, false);
				uISprite.type = UIBasicSprite.Type.Sliced;
				uISprite.SetDimensions(345, 42);
				GameObject gameObject = uISprite.gameObject;
				UILabel label = Util.CreateLabel("Text", gameObject, font);
				Util.SetLabelInfo(label, Localization.Get("ShopItemDesc_Normal_Price"), depth + 1, new Vector3(-148f, 6f), 22, 0, 0, UIWidget.Pivot.Left);
				uISprite = Util.CreateSprite("Gem", gameObject, atlas);
				Util.SetSpriteInfo(uISprite, "icon_currency_jemMini", depth + 1, new Vector3(5f, 6f), Vector3.one, false);
				label = Util.CreateLabel("Price", gameObject, font);
				UIWidget.Pivot pivot = UIWidget.Pivot.Left;
				float num = 24f;
				float num2 = -7f;
				num = 64f;
				num2 = 0f;
				pivot = UIWidget.Pivot.Center;
				Util.SetLabelInfo(label, string.Format(Localization.Get("ShopItemDesc_Normal_Price_Gem"), ((shop != null) ? shop.GemDefaultAmount : 0).ToString()), depth + 4, new Vector3(num, 6f), 22, 0, 0, pivot);
				uISprite = Util.CreateSprite("PriceCancel", label.gameObject, atlas);
				Util.SetSpriteInfo(uISprite, "line_cancellation2", depth + 5, new Vector3(num2, 0f), Vector3.one, false, pivot);
				uISprite = Util.CreateSprite("PriceAllow", gameObject, atlas);
				Util.SetSpriteInfo(uISprite, "instruction_accessory13", depth + 1, new Vector3(151f, 6f), Vector3.one, false, UIWidget.Pivot.Right);
			}
		}

		public virtual void CreateButtonPopImage(GameObject parent, int depth)
		{
			BIJImage atlas = GetAtlas(LOAD_ATLAS.SPECIAL_CHANCE_DIALOG);
			if ((bool)atlas)
			{
				UISprite sprite = Util.CreateSprite("ButtonPop", parent, atlas);
				float num = 0f;
				num = -15f;
				if (IsFirstPurchase)
				{
					Util.SetSpriteInfo(sprite, "LC_chance_sale00", depth, new Vector3(-142f + num, -4f), Vector3.one, false);
				}
				else
				{
					Util.SetSpriteInfo(sprite, "LC_chance_sale01", depth, new Vector3(54f, 40f), Vector3.one, false);
				}
			}
		}

		public abstract void CreateInnerItems(GameObject parent, int base_depth, PURCHASE_TYPE segment, params string[] booster_id);

		protected BIJImage GetAtlas(LOAD_ATLAS key)
		{
			return (mAtlas == null || !mAtlas.ContainsKey(key)) ? null : mAtlas[key];
		}

		public abstract void GetPanelInfo(out string sprite_name, out UIBasicSprite.Type sprite_type);

		public static LayoutInfo Create(SpecialChanceConditionSet.CHANCE_MODE mode, bool is_first_purchase)
		{
			LayoutInfo layoutInfo = null;
			switch (mode)
			{
			case SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP:
				return new LayoutScoreUp(mode, is_first_purchase);
			case SpecialChanceConditionSet.CHANCE_MODE.STAR_GET:
				return new LayoutStarGet(mode, is_first_purchase);
			default:
				return new LayoutDefault(mode, is_first_purchase);
			}
		}
	}

	private class LayoutDefault : LayoutInfo
	{
		public LayoutDefault(SpecialChanceConditionSet.CHANCE_MODE mode, bool is_first_purchase)
			: base(mode, is_first_purchase)
		{
		}

		public override void CreateTopDescText(GameObject parent, UIFont font, int depth)
		{
			string key = string.Empty;
			switch (base.Mode)
			{
			case SpecialChanceConditionSet.CHANCE_MODE.PLUS:
				key = "PlusChance_Desc00";
				break;
			case SpecialChanceConditionSet.CHANCE_MODE.SPECIAL:
				key = "SpecialChance_Desc00";
				break;
			}
			UILabel uILabel = Util.CreateLabel("Text0", parent, font);
			Util.SetLabelInfo(uILabel, Localization.Get(key), depth, new Vector3(0f, 1.5f), 22, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.white;
		}

		public override void CreateInnerItems(GameObject parent, int base_depth, PURCHASE_TYPE segment, params string[] booster_id)
		{
			UISprite uISprite = null;
			UILabel uILabel = null;
			UIFont atlasFont = GameMain.LoadFont();
			BIJImage bIJImage = null;
			bIJImage = GetAtlas(LOAD_ATLAS.SHOP);
			if (bIJImage != null)
			{
				foreach (string text in booster_id)
				{
					uISprite = Util.CreateSprite(text, parent, bIJImage);
					Util.SetSpriteInfo(uISprite, "icon_150booster_" + text, base_depth + 2, Vector3.zero, new Vector2(0.7f, 0.7f), false);
				}
			}
			GameObject parent2 = Util.CreateGameObject("CountArea", parent);
			uILabel = Util.CreateLabel("NumDesc", parent2, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Xtext"), base_depth + 2, new Vector2(-28f, -4f), 26, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(uILabel);
			uILabel.fontSize = 45;
			uILabel.color = new Color32(188, 183, 21, byte.MaxValue);
			bIJImage = GetAtlas(LOAD_ATLAS.HUD);
			if (bIJImage != null)
			{
				NumberImageString numberImageString = Util.CreateGameObject("NumDefault", parent2).AddComponent<NumberImageString>();
				numberImageString.Init(1, 1, base_depth + 2, 0.9f, UIWidget.Pivot.Center, false, bIJImage, false, Res.BuyNumImageString);
				numberImageString.transform.SetLocalPositionX(25f);
			}
			UIGrid uIGrid = parent.AddComponent<UIGrid>();
			uIGrid.cellWidth = 110f;
			uIGrid.pivot = UIWidget.Pivot.Center;
		}

		public override void GetPanelInfo(out string sprite_name, out UIBasicSprite.Type sprite_type)
		{
			sprite_name = "null";
			sprite_type = UIBasicSprite.Type.Simple;
			switch (base.Mode)
			{
			case SpecialChanceConditionSet.CHANCE_MODE.SPECIAL:
				sprite_name = "instruction_panel14";
				sprite_type = UIBasicSprite.Type.Sliced;
				break;
			case SpecialChanceConditionSet.CHANCE_MODE.PLUS:
				sprite_name = "instruction_panel15";
				sprite_type = UIBasicSprite.Type.Tiled;
				break;
			}
		}
	}

	private class LayoutScoreUp : LayoutInfo
	{
		public override Color UnderDescColor
		{
			get
			{
				return new Color(1f, 0.1f, 0.1f);
			}
		}

		public override Vector3 CharacterPositionOffset
		{
			get
			{
				return base.CharacterPositionOffset - new Vector3(0f, 30f);
			}
		}

		public override Vector3 CaptionPanelPos
		{
			get
			{
				return new Vector3(0f, 110f);
			}
		}

		public override int CaptionPanelDimensionHeight
		{
			get
			{
				return 93;
			}
		}

		public override Vector3 CaptionPos
		{
			get
			{
				return new Vector3(0f, 30f);
			}
		}

		public override Vector3 InnerPos
		{
			get
			{
				return new Vector3(0f, 30f);
			}
		}

		public override int InnerDimensionHeight
		{
			get
			{
				return 260;
			}
		}

		public override bool IsShowUnderDesc
		{
			get
			{
				return false;
			}
		}

		public LayoutScoreUp(SpecialChanceConditionSet.CHANCE_MODE mode, bool is_first_purchase)
			: base(mode, is_first_purchase)
		{
		}

		public override void CreateInnerItems(GameObject parent, int base_depth, PURCHASE_TYPE segment, params string[] booster_id)
		{
			UISprite uISprite = null;
			UIGrid uIGrid = null;
			BIJImage atlas = GetAtlas(LOAD_ATLAS.SPECIAL_CHANCE_DIALOG);
			if (!(atlas == null))
			{
				float num = 0f;
				float num2 = 0f;
				PURCHASE_TYPE pURCHASE_TYPE = segment;
				if (pURCHASE_TYPE == PURCHASE_TYPE.HEAVY)
				{
					num = -35f;
					num2 = 35f;
				}
				else
				{
					num = -15f;
					num2 = 70f;
				}
				if (parent != null)
				{
					parent.transform.SetLocalPosition(0f, num, 0f);
				}
				pURCHASE_TYPE = segment;
				if (pURCHASE_TYPE == PURCHASE_TYPE.NONE || pURCHASE_TYPE == PURCHASE_TYPE.LIGHT)
				{
					uISprite = Util.CreateSprite("Line00", parent, atlas);
					Util.SetSpriteInfo(uISprite, "line00", base_depth + 2, Vector3.zero, Vector3.one, false);
					uISprite.type = UIBasicSprite.Type.Sliced;
					uISprite.SetDimensions(425, 6);
				}
				uISprite = Util.CreateSprite("Score", parent, atlas);
				Util.SetSpriteInfo(uISprite, "score_chance00", base_depth + 2, Vector3.zero, Vector3.one, false);
				pURCHASE_TYPE = segment;
				if (pURCHASE_TYPE == PURCHASE_TYPE.HEAVY)
				{
					Util.CreateGameObject("Dammy", parent);
				}
				uISprite = Util.CreateSprite("Line00", parent, atlas);
				Util.SetSpriteInfo(uISprite, "line00", base_depth + 2, Vector3.zero, Vector3.one, false);
				uISprite.type = UIBasicSprite.Type.Sliced;
				uISprite.SetDimensions(425, 6);
				pURCHASE_TYPE = segment;
				if (pURCHASE_TYPE == PURCHASE_TYPE.HEAVY)
				{
					uISprite = Util.CreateSprite("Additional", parent, atlas);
					Util.SetSpriteInfo(uISprite, "score_chance01", base_depth + 2, Vector3.zero, Vector3.one, false);
				}
				uIGrid = parent.AddComponent<UIGrid>();
				uIGrid.arrangement = UIGrid.Arrangement.Vertical;
				uIGrid.cellHeight = num2;
				uIGrid.pivot = UIWidget.Pivot.Center;
			}
		}

		public override void CreateFirstPurchaseFrame(ShopItemData shop, GameObject parent, UIFont font, int depth)
		{
		}

		public override void GetPanelInfo(out string sprite_name, out UIBasicSprite.Type sprite_type)
		{
			sprite_name = "null";
			sprite_type = UIBasicSprite.Type.Simple;
			SpecialChanceConditionSet.CHANCE_MODE mode = base.Mode;
			if (mode == SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP)
			{
				sprite_name = "instruction_panel15";
				sprite_type = UIBasicSprite.Type.Tiled;
			}
		}
	}

	private class LayoutStarGet : LayoutInfo
	{
		public override bool IsShowUnderDesc
		{
			get
			{
				return false;
			}
		}

		public override Vector3 InnerPos
		{
			get
			{
				return new Vector3(0f, 30f);
			}
		}

		public override int InnerDimensionHeight
		{
			get
			{
				return (!base.IsFirstPurchase) ? 280 : 260;
			}
		}

		public override Vector3 CaptionPanelPos
		{
			get
			{
				return new Vector3(0f, 120f);
			}
		}

		public override int CaptionPanelDimensionHeight
		{
			get
			{
				return 80;
			}
		}

		public override Vector3 CaptionPos
		{
			get
			{
				return new Vector3(0f, 25f);
			}
		}

		public override Vector3 CharacterPositionOffset
		{
			get
			{
				return new Vector3(0f, -30f);
			}
		}

		public override LOAD_ATLAS PurchaceButtonAtlasType
		{
			get
			{
				return LOAD_ATLAS.SPECIAL_CHANCE_DIALOG;
			}
		}

		public override string PurchaceButtonSpriteName
		{
			get
			{
				return "LC_button_buy_play";
			}
		}

		public LayoutStarGet(SpecialChanceConditionSet.CHANCE_MODE mode, bool is_first_purchase)
			: base(mode, is_first_purchase)
		{
		}

		public override void CreateFirstPurchaseFrame(ShopItemData shop, GameObject parent, UIFont font, int depth)
		{
			if (base.IsFirstPurchase)
			{
				base.CreateFirstPurchaseFrame(shop, parent, font, depth);
				return;
			}
			BIJImage atlas = GetAtlas(LOAD_ATLAS.SPECIAL_CHANCE_DIALOG);
			string text = null;
			UISprite uISprite = Util.CreateSprite("DefaultGemFrame", parent, atlas);
			Util.SetSpriteInfo(uISprite, "starget_challenge01", depth + 2, new Vector3(0f, -115f), Vector3.one, false);
			GameObject gameObject = uISprite.gameObject;
			Color32 color = new Color32(238, 42, 83, byte.MaxValue);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(365, 70);
			text = Localization.Get("StarGet_Desc01");
			UILabel label = Util.CreateLabel("Label", uISprite.gameObject, font);
			Util.SetLabelInfo(label, text, depth + 3, new Vector3(0f, 10f), 17, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(label, color);
		}

		public override void CreateInnerItems(GameObject parent, int base_depth, PURCHASE_TYPE segment, params string[] booster_id)
		{
			UISprite uISprite = null;
			BIJImage bIJImage = null;
			bIJImage = GetAtlas(LOAD_ATLAS.SPECIAL_CHANCE_DIALOG);
			uISprite = Util.CreateSprite("Label", parent, bIJImage);
			Util.SetSpriteInfo(uISprite, "starget_challenge00", base_depth + 2, Vector3.zero, Vector3.one, false);
			GameObject gameObject = Util.CreateGameObject("GetStar", parent);
			UIGrid uIGrid = gameObject.AddComponent<UIGrid>();
			uIGrid.arrangement = UIGrid.Arrangement.Horizontal;
			uIGrid.cellWidth = 120f;
			uIGrid.pivot = UIWidget.Pivot.Center;
			bIJImage = GetAtlas(LOAD_ATLAS.HUD);
			for (int i = 0; i < 3; i++)
			{
				uISprite = Util.CreateSprite("StarEmpty", gameObject, bIJImage);
				Util.SetSpriteInfo(uISprite, "star_gold_empty", base_depth + 2, Vector3.zero, Vector3.one, false);
				if (i < 2)
				{
					uISprite = Util.CreateSprite("Start", uISprite.gameObject, bIJImage);
					Util.SetSpriteInfo(uISprite, "star_gold", base_depth + 3, Vector3.zero, Vector3.one, false);
				}
			}
			uIGrid = parent.AddComponent<UIGrid>();
			uIGrid.cellHeight = 90f;
			uIGrid.pivot = UIWidget.Pivot.Center;
			uIGrid.arrangement = UIGrid.Arrangement.Vertical;
		}

		public override void CreateButtonPopImage(GameObject parent, int depth)
		{
			BIJImage atlas = GetAtlas(LOAD_ATLAS.SPECIAL_CHANCE_DIALOG);
			if ((bool)atlas && base.IsFirstPurchase)
			{
				UISprite sprite = Util.CreateSprite("ButtonPop", parent, atlas);
				float num = 0f;
				Util.SetSpriteInfo(sprite, "LC_chance_sale00", depth, new Vector3(-152f + num, -4f), Vector3.one, false);
			}
		}

		public override void GetPanelInfo(out string sprite_name, out UIBasicSprite.Type sprite_type)
		{
			sprite_name = "oldevent_instruction_panel";
			sprite_type = UIBasicSprite.Type.Tiled;
		}
	}

	private static readonly Color PRICE_LABEL_LACKING_GEM_COLOR = new Color(1f, 0.1f, 0.1f);

	private SpecialChanceConditionSet.CHANCE_MODE mMode = SpecialChanceConditionSet.CHANCE_MODE.ERROR;

	private PURCHASE_TYPE mUserSegment;

	private SELECT_ITEM mSelectItem;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAINTENANCE_CHECK_START);

	private ClosedCallback mClosedCallback = new ClosedCallback();

	private UnityEvent mAuthErrorCallback = new UnityEvent();

	private ChanceConditionInfo mCondition;

	private bool mIsFirstPurchase;

	private bool mIsFreePrice;

	private ShopItemData mShopItemData;

	private UILabel mPriceLabel;

	private bool mIsCreatedGemWindow;

	private SpecialChanceConditionSet mConditionSet;

	private Dictionary<LOAD_ATLAS, AtlasInfo> mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();

	private LayoutInfo mLayout;

	public SpecialChanceConditionSet.CHANCE_MODE Mode
	{
		get
		{
			return mMode;
		}
	}

	public ShopItemDetail ShopItemDetail
	{
		get
		{
			return (mShopItemData == null) ? null : mShopItemData.ItemDetail;
		}
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy())
		{
			switch (mState.GetStatus())
			{
			case STATE.MAINTENANCE_CHECK_START:
				MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
				mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
				break;
			case STATE.MAINTENANCE_CHECK_WAIT:
				if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
				{
					mState.Change(STATE.NETWORK_00);
				}
				break;
			case STATE.MAINTENANCE_CHECK_START2:
				MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY);
				mState.Change(STATE.MAINTENANCE_CHECK_WAIT2);
				break;
			case STATE.MAINTENANCE_CHECK_WAIT2:
				switch (MaintenanceCheckResult())
				{
				case MAINTENANCE_RESULT.OK:
					mState.Change(STATE.NETWORK_02);
					break;
				case MAINTENANCE_RESULT.ERROR:
				case MAINTENANCE_RESULT.IN_MAINTENANCE:
					if (!GetBusy())
					{
						mState.Change(STATE.MAIN);
					}
					break;
				}
				break;
			case STATE.NETWORK_00:
				if (!mGame.Network_UserAuth())
				{
					if (GameMain.mUserAuthSucceed)
					{
						mState.Change(STATE.NETWORK_01);
					}
					else
					{
						ShowNetworkErrorDialog(STATE.NETWORK_00);
					}
				}
				else
				{
					mState.Change(STATE.NETWORK_00_1);
				}
				break;
			case STATE.NETWORK_00_1:
			{
				if (!GameMain.mUserAuthCheckDone)
				{
					break;
				}
				if (GameMain.mUserAuthSucceed)
				{
					if (!GameMain.mUserAuthTransfered)
					{
						mState.Change(STATE.NETWORK_00_2);
					}
					else
					{
						mState.Change(STATE.AUTHERROR_WAIT);
					}
					break;
				}
				GameMain.OnConnectRetry a_callback2 = delegate(int fn_a_state)
				{
					mState.Change((STATE)fn_a_state);
				};
				GameMain.OnConnectAbandon a_abandon2 = delegate
				{
					mGame.PlaySe("SE_NEGATIVE", -1);
					Close();
				};
				Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
				if (mUserAuthErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 6, a_callback2, a_abandon2);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 6, a_callback2, null);
				}
				mState.Change(STATE.WAIT);
				break;
			}
			case STATE.NETWORK_00_2:
				mState.Change(STATE.NETWORK_01);
				break;
			case STATE.AUTHERROR_WAIT:
				ClearCallback(CALLBACK_STATE.OnCloseFinished);
				ClearClosedEvent();
				if (mAuthErrorCallback != null)
				{
					mAuthErrorCallback.Invoke();
				}
				Close();
				mState.Change(STATE.WAIT);
				break;
			case STATE.NETWORK_01:
				if (!mGame.Network_GetGemCount())
				{
					if (GameMain.mGetGemCountSucceed)
					{
						mState.Change(STATE.MAIN);
					}
					else
					{
						ShowNetworkErrorDialog(STATE.NETWORK_01);
					}
				}
				mState.Change(STATE.NETWORK_01_1);
				break;
			case STATE.NETWORK_01_1:
				if (GameMain.mGetGemCountCheckDone)
				{
					if (GameMain.mGetGemCountSucceed)
					{
						mState.Change(STATE.NETWORK_01_2);
					}
					else
					{
						ShowNetworkErrorDialog(STATE.NETWORK_01);
					}
				}
				break;
			case STATE.NETWORK_01_2:
				mState.Change(STATE.MAIN);
				break;
			case STATE.NETWORK_02:
				mGame.ConnectRetryFlg = false;
				if (mShopItemData != null)
				{
					if (!mGame.Network_BuyItem(mShopItemData.Index))
					{
						ShowNetworkErrorDialog(STATE.MAIN);
						break;
					}
					mState.Change(STATE.NETWORK_02_1);
					goto case STATE.NETWORK_02_1;
				}
				mState.Change(STATE.NETWORK_02_2);
				goto case STATE.NETWORK_02_2;
			case STATE.NETWORK_02_1:
			{
				if (!GameMain.mBuyItemCheckDone)
				{
					break;
				}
				if (GameMain.mBuyItemSucceed)
				{
					mState.Change(STATE.NETWORK_02_2);
					break;
				}
				GameMain.OnConnectRetry a_callback = delegate(int fn_a_state)
				{
					mGame.ConnectRetryFlg = true;
					mState.Change((STATE)fn_a_state);
				};
				GameMain.OnConnectAbandon a_abandon = delegate
				{
					mGame.ConnectRetryFlg = false;
					mGame.PlaySe("SE_NEGATIVE", -1);
					Close();
				};
				switch (GameMain.mBuyItemErrorCode)
				{
				case Server.ErrorCode.MAINTENANCE_MODE:
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 12, a_callback, a_abandon);
					mState.Change(STATE.WAIT);
					break;
				case Server.ErrorCode.DUPLICATE_DATA:
				{
					ConfirmDialog confirmDialog = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
					confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
					confirmDialog.SetClosedCallback(delegate
					{
						mGame.GetGemCountFreqFlag = false;
						mState.Change(STATE.NETWORK_00);
					});
					mState.Change(STATE.WAIT);
					break;
				}
				default:
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 12, a_callback, a_abandon);
					mState.Change(STATE.WAIT);
					break;
				}
				break;
			}
			case STATE.NETWORK_02_2:
			{
				int itemID = 0;
				int countID = 0;
				int num = 0;
				if (mShopItemData != null)
				{
					foreach (KeyValuePair<int, ShopItemData> mShopItemDatum in mGame.mShopItemData)
					{
						int index = mShopItemDatum.Value.Index;
						if (index == mShopItemData.Index)
						{
							itemID = mShopItemDatum.Value.Index;
							countID = mShopItemDatum.Value.DefaultNum;
							num = mShopItemDatum.Value.GemAmount;
						}
					}
					mGame.mPlayer.BoughtItem(mShopItemData.ItemDetail);
					mGame.mPlayer.AddPurchaseItem(itemID, countID);
				}
				if (mGame.mPlayer.Data.ChanceTotalSaleCount.ContainsKey(mCondition.ChanceID))
				{
					Dictionary<int, int> chanceTotalSaleCount;
					Dictionary<int, int> dictionary = (chanceTotalSaleCount = mGame.mPlayer.Data.ChanceTotalSaleCount);
					int chanceID;
					int key = (chanceID = mCondition.ChanceID);
					chanceID = chanceTotalSaleCount[chanceID];
					dictionary[key] = chanceID + 1;
				}
				else
				{
					mGame.mPlayer.Data.ChanceTotalSaleCount.Add(mCondition.ChanceID, 1);
				}
				mGame.Save();
				SpecialChanceConditionSet.CHANCE_MODE cHANCE_MODE = mMode;
				if (cHANCE_MODE == SpecialChanceConditionSet.CHANCE_MODE.SPECIAL || cHANCE_MODE == SpecialChanceConditionSet.CHANCE_MODE.PLUS)
				{
					mGame.mOptions.BuyBoostCount++;
					mGame.SaveOptions();
				}
				if (mGame.mPlayer.Data.ChanceFirstPurchaseFlag == null)
				{
					mGame.mPlayer.Data.ChanceFirstPurchaseFlag = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, bool>();
				}
				if (!mGame.mPlayer.Data.ChanceFirstPurchaseFlag.ContainsKey(mMode))
				{
					mGame.mPlayer.Data.ChanceFirstPurchaseFlag.Add(mMode, false);
				}
				mGame.mPlayer.Data.ChanceFirstPurchaseFlag[mMode] = true;
				mGame.Save();
				if (mShopItemData != null && mShopItemData.ItemDetail != null)
				{
					if (mShopItemData.ItemDetail.WAVE_BOMB > 0)
					{
						mGame.mNoSendUseBoosters.Add(mGame.mCurrentStage.BoosterSlot1);
					}
					if (mShopItemData.ItemDetail.TAP_BOMB2 > 0)
					{
						mGame.mNoSendUseBoosters.Add(mGame.mCurrentStage.BoosterSlot2);
					}
					if (mShopItemData.ItemDetail.RAINBOW > 0)
					{
						mGame.mNoSendUseBoosters.Add(mGame.mCurrentStage.BoosterSlot3);
					}
					if (mShopItemData.ItemDetail.ADD_SCORE > 0)
					{
						mGame.mNoSendUseBoosters.Add(mGame.mCurrentStage.BoosterSlot4);
					}
					if (mShopItemData.ItemDetail.SKILL_CHARGE > 0)
					{
						mGame.mNoSendUseBoosters.Add(mGame.mCurrentStage.BoosterSlot5);
					}
				}
				mGame.mPlayer.Data.UsePlusChanceBoosterCount = mGame.mNoSendUseBoosters.Count;
				mGame.mPlayer.Data.UseBeforeBooster.Clear();
				mGame.mPlayer.Data.UseBeforeBooster.AddRange(mGame.mNoSendUseBoosters.ToArray());
				if (!mIsFirstPurchase)
				{
					mSelectItem = SELECT_ITEM.CONSUME;
				}
				else
				{
					mSelectItem = SELECT_ITEM.FIRST_CONSUME;
				}
				Close();
				mState.Reset(STATE.WAIT);
				break;
			}
			}
		}
		mState.Update();
	}

	public static bool TryMake(SpecialChanceConditionSet.CHANCE_MODE mode, GameObject root, out BoosterChanceDialog result, SpecialChanceConditionItem.Option option)
	{
		result = null;
		GameMain main = SingletonMonoBehaviour<GameMain>.Instance;
		if (main == null || main.SegmentProfile == null || main.SegmentProfile.ChanceList == null)
		{
			return false;
		}
		ChanceConditionInfo condition;
		PURCHASE_TYPE segment;
		if (!Lottery(mode, out condition, out segment, option))
		{
			return false;
		}
		SpecialChanceConditionSet.CHANCE_MODE cHANCE_MODE = mode;
		if (cHANCE_MODE != SpecialChanceConditionSet.CHANCE_MODE.ERROR && cHANCE_MODE != SpecialChanceConditionSet.CHANCE_MODE.CONTINUE)
		{
			if (main.mPlayer.Data.ChanceFirstPurchaseFlag == null)
			{
				main.mPlayer.Data.ChanceFirstPurchaseFlag = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, bool>();
			}
			if (!main.mPlayer.Data.ChanceFirstPurchaseFlag.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS))
			{
				main.mPlayer.Data.ChanceFirstPurchaseFlag.Add(SpecialChanceConditionSet.CHANCE_MODE.PLUS, false);
			}
			if (!main.mPlayer.Data.ChanceFirstPurchaseFlag.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP))
			{
				main.mPlayer.Data.ChanceFirstPurchaseFlag.Add(SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP, false);
			}
			if (!main.mPlayer.Data.ChanceFirstPurchaseFlag.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL))
			{
				main.mPlayer.Data.ChanceFirstPurchaseFlag.Add(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL, false);
			}
			if (!main.mPlayer.Data.ChanceFirstPurchaseFlag.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.STAR_GET))
			{
				main.mPlayer.Data.ChanceFirstPurchaseFlag.Add(SpecialChanceConditionSet.CHANCE_MODE.STAR_GET, false);
			}
			if (!main.mPlayer.Data.ChanceFirstPurchaseFlag.ContainsKey(mode))
			{
				main.mPlayer.Data.ChanceFirstPurchaseFlag.Add(mode, false);
			}
			bool flag = main.mPlayer.Data.ChanceFirstPurchaseFlag[SpecialChanceConditionSet.CHANCE_MODE.PLUS] || main.mPlayer.Data.ChanceFirstPurchaseFlag[SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP];
			bool flag2 = (mode == SpecialChanceConditionSet.CHANCE_MODE.PLUS && !flag) || (mode == SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP && !flag) || (mode == SpecialChanceConditionSet.CHANCE_MODE.SPECIAL && !main.mPlayer.Data.ChanceFirstPurchaseFlag[SpecialChanceConditionSet.CHANCE_MODE.SPECIAL]) || (mode == SpecialChanceConditionSet.CHANCE_MODE.STAR_GET && !main.mPlayer.Data.ChanceFirstPurchaseFlag[SpecialChanceConditionSet.CHANCE_MODE.STAR_GET]);
			int key = condition.ItemID[(!flag2) ? 1 : 0][(int)segment];
			BoosterChanceDialog dialog = Util.CreateGameObject("BoosterChanceDialog", root).AddComponent<BoosterChanceDialog>();
			dialog.mMode = mode;
			dialog.mCondition = condition;
			dialog.mIsFirstPurchase = flag2;
			dialog.mShopItemData = ((!main.mShopItemData.ContainsKey(key)) ? null : main.mShopItemData[key]);
			dialog.mIsFreePrice = main.mShopItemData.ContainsKey(key) && main.mShopItemData[key].GemAmount == 0;
			dialog.ParticleFeather = false;
			dialog.mUserSegment = segment;
			dialog.mLayout = LayoutInfo.Create(mode, dialog.mIsFirstPurchase);
			dialog.mJelly.SetCustomOpenEvent(delegate(float ratio)
			{
				Transform self = dialog.mJelly.transform;
				ratio = 1f - ratio;
				if (ratio <= 0.8f)
				{
					float num2 = ratio / 0.8f;
					self.SetLocalScale(num2 * 1.1f);
					self.SetLocalEulerAnglesZ((0f - num2) * 720f);
				}
				else if (ratio <= 0.9f)
				{
					self.SetLocalScale(0.95f + (1f - (ratio - 0.8f) / 0.1f) * 0.15f);
					self.SetLocalEulerAnglesZ(-720f);
				}
				else
				{
					self.SetLocalScale(0.95f + (ratio - 0.9f) / 0.1f * 0.05f);
					self.SetLocalEulerAnglesZ(-720f);
					if (Mathf.Approximately(ratio, 1f) || ratio >= 1f)
					{
						main.mDialogManager.OpenParticle(dialog.transform.localPosition);
						if (!dialog.mIsCreatedGemWindow)
						{
							dialog.CreateGemWindow();
						}
					}
				}
			}, 120f);
			result = dialog;
		}
		if (main.mPlayer.Data.ChanceDispCount == null)
		{
			main.mPlayer.Data.ChanceDispCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
		}
		if (!main.mPlayer.Data.ChanceDispCount.ContainsKey(mode))
		{
			main.mPlayer.Data.ChanceDispCount.Add(mode, 0);
		}
		Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> chanceDispCount;
		Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> dictionary = (chanceDispCount = main.mPlayer.Data.ChanceDispCount);
		SpecialChanceConditionSet.CHANCE_MODE key2 = (cHANCE_MODE = mode);
		short num = chanceDispCount[cHANCE_MODE];
		dictionary[key2] = (short)(num + 1);
		if (main.mPlayer.Data.ChanceTotalOfferCount.ContainsKey(condition.ChanceID))
		{
			Dictionary<int, int> chanceTotalOfferCount;
			Dictionary<int, int> dictionary2 = (chanceTotalOfferCount = main.mPlayer.Data.ChanceTotalOfferCount);
			int chanceID;
			int key3 = (chanceID = condition.ChanceID);
			chanceID = chanceTotalOfferCount[chanceID];
			dictionary2[key3] = chanceID + 1;
		}
		else
		{
			main.mPlayer.Data.ChanceTotalOfferCount.Add(condition.ChanceID, 1);
		}
		main.Save();
		return true;
	}

	public static bool Lottery(SpecialChanceConditionSet.CHANCE_MODE mode, out ChanceConditionInfo condition, out PURCHASE_TYPE segment, SpecialChanceConditionItem.Option option)
	{
		condition = null;
		segment = PURCHASE_TYPE.NONE;
		GameMain main = SingletonMonoBehaviour<GameMain>.Instance;
		if (main == null || main.SegmentProfile == null || main.SegmentProfile.ChanceList == null)
		{
			return false;
		}
		for (int i = 0; i < main.SegmentProfile.ChanceList.Count; i++)
		{
			PROB_TYPE pROB_TYPE = PROB_TYPE.NONE;
			foreach (int value in Enum.GetValues(typeof(SpecialChanceConditionSet.CHANCE_MODE)))
			{
				if (value == -1 || value != (int)mode || main.SegmentProfile.ChanceList.Count <= i || main.SegmentProfile.ChanceList[i].Data == null || !main.SegmentProfile.ChanceList[i].Data.Any((ChanceDataInfo n) => n.Type - 1 == (int)mode))
				{
					continue;
				}
				ChanceCommInfo common = main.SegmentProfile.ChanceList[i].Common;
				if (common == null || !common.InTime())
				{
					continue;
				}
				long num = ((main.mDebugForceSpecialChanceSegmentPrice == -1) ? main.mChargePriceCheckData.price : main.mDebugForceSpecialChanceSegmentPrice);
				long num2 = main.SegmentProfile.ChanceList[i].Common.Purchase;
				if (num > 0 && num < num2)
				{
					segment = PURCHASE_TYPE.LIGHT;
				}
				else if (num >= num2)
				{
					segment = PURCHASE_TYPE.HEAVY;
				}
				Func<ChanceConditionInfo, bool, ChanceConditionInfo> func = delegate(ChanceConditionInfo fn_cond, bool fn_force_success)
				{
					ChanceConditionInfo result = null;
					ResScriptableObject resScriptableObject = ResourceManager.LoadScriptableObject("SPECIAL_CHANCE");
					if (resScriptableObject != null && resScriptableObject.ScriptableObject != null && resScriptableObject.ScriptableObject is SpecialChanceConditionSet)
					{
						SpecialChanceConditionSet specialChanceConditionSet = resScriptableObject.ScriptableObject as SpecialChanceConditionSet;
						if (specialChanceConditionSet.IsAchieve(fn_cond.ID, option) || fn_force_success)
						{
							result = fn_cond;
							if (main.mPlayer.Data.ChanceCount == null)
							{
								main.mPlayer.Data.ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
							}
							SpecialChanceConditionSet.CHANCE_MODE chanceMode = specialChanceConditionSet.GetChanceMode(fn_cond.ID);
							if (chanceMode == SpecialChanceConditionSet.CHANCE_MODE.PLUS || chanceMode == SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP)
							{
								if (!main.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS))
								{
									main.mPlayer.Data.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.PLUS] = 0;
								}
								if (!main.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP))
								{
									main.mPlayer.Data.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP] = 0;
								}
								main.mPlayer.Data.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.PLUS] = 0;
								main.mPlayer.Data.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP] = 0;
								main.mPlayer.Data.UseBeforeBooster.Clear();
								main.mPlayer.Data.UsePlusChanceBoosterCount = 0;
							}
							else
							{
								SpecialChanceConditionSet.CHANCE_MODE chanceMode2 = specialChanceConditionSet.GetChanceMode(fn_cond.ID);
								if (!main.mPlayer.Data.ChanceCount.ContainsKey(chanceMode2))
								{
									main.mPlayer.Data.ChanceCount.Add(chanceMode2, 0);
								}
								main.mPlayer.Data.ChanceCount[chanceMode2] = 0;
							}
						}
					}
					return result;
				};
				ChanceDataInfo info = main.SegmentProfile.ChanceList[i].Data.FirstOrDefault((ChanceDataInfo n) => n.Type - 1 == (int)mode);
				if (info != null)
				{
					if (main.mDebugForceSpecialChance != -1)
					{
						foreach (ChanceConditionInfo item in info.Condition)
						{
							if (item != null && item.ID == main.mDebugForceSpecialChance)
							{
								condition = func(item, true);
								if (condition != null)
								{
									break;
								}
							}
						}
						if (condition != null)
						{
							main.mDebugForceSpecialChance = -1;
						}
					}
					else if (info.IsEnable)
					{
						switch (pROB_TYPE)
						{
						case PROB_TYPE.NONE:
							if (info.Probability[(int)segment] <= UnityEngine.Random.Range(0, 100))
							{
								pROB_TYPE = PROB_TYPE.FAILURE;
								continue;
							}
							pROB_TYPE = PROB_TYPE.SUCCESS;
							break;
						case PROB_TYPE.FAILURE:
							continue;
						}
						switch (mode)
						{
						default:
							if (!main.mPlayer.Data.ChanceCount.ContainsKey(mode) || info.TryNum > main.mPlayer.Data.ChanceCount[mode])
							{
								continue;
							}
							break;
						case SpecialChanceConditionSet.CHANCE_MODE.PLUS:
						case SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP:
						{
							int num3 = (main.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS) ? main.mPlayer.Data.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.PLUS] : 0);
							int num4 = (main.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP) ? main.mPlayer.Data.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP] : 0);
							if (info.TryNum > num3 + num4)
							{
								continue;
							}
							break;
						}
						case SpecialChanceConditionSet.CHANCE_MODE.CONTINUE:
						{
							if (info.ABtestingPlace == null)
							{
								continue;
							}
							bool isOdd = main.mPlayer.HiveId % 2 != 0;
							Func<Def.SERIES, AB_TEST_PLACE, bool> func2 = delegate(Def.SERIES _series, AB_TEST_PLACE _place)
							{
								if (main.mPlayer.Data.CurrentSeries != _series || main.IsDailyChallengePuzzle)
								{
									return false;
								}
								return ((int)_place >= info.ABtestingPlace.Count || info.ABtestingPlace[(int)_place] == 0 || (isOdd && info.ABtestingPlace[(int)_place] == 2) || (!isOdd && info.ABtestingPlace[(int)_place] == 1)) ? true : false;
							};
							Func<Def.EVENT_TYPE, AB_TEST_PLACE, bool> func3 = delegate(Def.EVENT_TYPE _event, AB_TEST_PLACE _place)
							{
								if (main.mPlayer.Data.CurrentSeries != Def.SERIES.SM_EV || main.IsDailyChallengePuzzle)
								{
									return false;
								}
								SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
								if (currentEventPageData == null || currentEventPageData.EventSetting.EventType != _event)
								{
									return false;
								}
								if (currentEventPageData.EventID >= 200 && currentEventPageData.EventID < 1000)
								{
									_place = AB_TEST_PLACE.OldEvent;
								}
								return ((int)_place >= info.ABtestingPlace.Count || info.ABtestingPlace[(int)_place] == 0 || (isOdd && info.ABtestingPlace[(int)_place] == 2) || (!isOdd && info.ABtestingPlace[(int)_place] == 1)) ? true : false;
							};
							if (func2(Def.SERIES.SM_FIRST, AB_TEST_PLACE.FIRST) || func2(Def.SERIES.SM_R, AB_TEST_PLACE.R) || func2(Def.SERIES.SM_S, AB_TEST_PLACE.S) || func2(Def.SERIES.SM_SS, AB_TEST_PLACE.SS) || func2(Def.SERIES.SM_STARS, AB_TEST_PLACE.StarS) || func2(Def.SERIES.SM_GF00, AB_TEST_PLACE.GF00) || func3(Def.EVENT_TYPE.SM_RALLY, AB_TEST_PLACE.Rally) || func3(Def.EVENT_TYPE.SM_STAR, AB_TEST_PLACE.Star) || func3(Def.EVENT_TYPE.SM_RALLY2, AB_TEST_PLACE.Rally2) || func3(Def.EVENT_TYPE.SM_BINGO, AB_TEST_PLACE.Bingo) || func3(Def.EVENT_TYPE.SM_LABYRINTH, AB_TEST_PLACE.Labyrinth) || (main.IsDailyChallengePuzzle && 10 < info.ABtestingPlace.Count && (info.ABtestingPlace[10] == 0 || (isOdd && info.ABtestingPlace[10] == 1) || (!isOdd && info.ABtestingPlace[10] == 2))))
							{
								continue;
							}
							break;
						}
						}
						foreach (ChanceConditionInfo item2 in info.Condition)
						{
							condition = func(item2, false);
							if (condition != null)
							{
								break;
							}
						}
					}
				}
				if (condition == null)
				{
					continue;
				}
				return true;
			}
		}
		condition = null;
		segment = PURCHASE_TYPE.NONE;
		return false;
	}

	public override IEnumerator BuildResource()
	{
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();
		}
		foreach (KeyValuePair<LOAD_ATLAS, AtlasInfo> pair in mAtlas)
		{
			if (pair.Value != null)
			{
				pair.Value.UnLoad();
			}
		}
		mAtlas.Clear();
		foreach (int type in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			string atlasName = ((LOAD_ATLAS)type).ToString();
			if (type == 1)
			{
				atlasName = atlasName + "_" + ((int)Mode).ToString("00");
			}
			AtlasInfo info2 = new AtlasInfo(atlasName);
			mAtlas.Add((LOAD_ATLAS)type, info2);
			bool isNotUnload = false;
			LOAD_ATLAS lOAD_ATLAS = (LOAD_ATLAS)type;
			if (lOAD_ATLAS == LOAD_ATLAS.HUD || lOAD_ATLAS == LOAD_ATLAS.DIALOG_BASE)
			{
				isNotUnload = true;
			}
			info2.Load(isNotUnload);
		}
		foreach (AtlasInfo info in mAtlas.Values)
		{
			while (!info.IsDone())
			{
				yield return null;
			}
		}
	}

	public override void BuildDialog()
	{
		string empty = string.Empty;
		UIButton uIButton = null;
		UILabel uILabel = null;
		UISprite uISprite = null;
		GameObject gameObject = base.gameObject;
		UIFont uIFont = GameMain.LoadFont();
		if (mLayout != null)
		{
			mLayout.Atlas = mAtlas.Select((KeyValuePair<LOAD_ATLAS, AtlasInfo> k) => new KeyValuePair<LOAD_ATLAS, BIJImage>(k.Key, k.Value.Image)).ToArray();
		}
		if (mLayout != null)
		{
			string sprite_name;
			UIBasicSprite.Type sprite_type;
			mLayout.GetPanelInfo(out sprite_name, out sprite_type);
			InitDialogFrame(DIALOG_SIZE.MEDIUM, "DIALOG_BASE", sprite_name, 575, 470);
			if (sprite_type != UIBasicSprite.Type.Tiled)
			{
				mDialogFrame.type = sprite_type;
			}
			else
			{
				mDialogFrame.type = UIBasicSprite.Type.Advanced;
				mDialogFrame.topType = UIBasicSprite.AdvancedType.Tiled;
				mDialogFrame.bottomType = UIBasicSprite.AdvancedType.Tiled;
				mDialogFrame.leftType = UIBasicSprite.AdvancedType.Tiled;
				mDialogFrame.rightType = UIBasicSprite.AdvancedType.Tiled;
				mDialogFrame.centerType = UIBasicSprite.AdvancedType.Tiled;
			}
			SpecialChanceConditionSet.CHANCE_MODE mode = Mode;
			if (mode == SpecialChanceConditionSet.CHANCE_MODE.STAR_GET)
			{
				gameObject.transform.SetLocalPosition(DialogBase.BUILD_DIALOG_POSITION_OFFSET);
			}
			else
			{
				gameObject.transform.SetLocalPositionY(-80f);
			}
		}
		GameObject parent = null;
		if (mLayout != null)
		{
			empty = "instruction_panel2";
			uISprite = Util.CreateSprite("Inner", gameObject, mAtlas[LOAD_ATLAS.DIALOG_BASE].Image);
			Util.SetSpriteInfo(uISprite, empty, mBaseDepth + 1, mLayout.InnerPos, Vector3.one, false);
			uISprite.SetDimensions(500, mLayout.InnerDimensionHeight);
			uISprite.type = UIBasicSprite.Type.Sliced;
			parent = uISprite.gameObject;
		}
		List<string> list = new List<string>();
		if (mShopItemData != null)
		{
			if (mShopItemData.ItemDetail.WAVE_BOMB > 0)
			{
				list.Add("00");
			}
			if (mShopItemData.ItemDetail.TAP_BOMB2 > 0)
			{
				list.Add("01");
			}
			if (mShopItemData.ItemDetail.RAINBOW > 0)
			{
				list.Add("02");
			}
			if (mShopItemData.ItemDetail.ADD_SCORE > 0)
			{
				list.Add("03");
			}
			if (mShopItemData.ItemDetail.SKILL_CHARGE > 0)
			{
				list.Add("04");
			}
			if (mShopItemData.ItemDetail.SELECT_ONE > 0)
			{
				list.Add("06");
			}
			if (mShopItemData.ItemDetail.SELECT_COLLECT > 0)
			{
				list.Add("07");
			}
			if (mShopItemData.ItemDetail.COLOR_CRUSH > 0)
			{
				list.Add("08");
			}
			if (mShopItemData.ItemDetail.BLOCK_CRUSH > 0)
			{
				list.Add("09");
			}
			if (mShopItemData.ItemDetail.PAIR_MAKER > 0)
			{
				list.Add("10");
			}
		}
		GameObject parent2 = Util.CreateGameObject("BoosterArea", parent);
		if (mLayout != null)
		{
			mLayout.CreateInnerItems(parent2, mBaseDepth, mUserSegment, list.ToArray());
		}
		if (mLayout != null && mLayout.IsShowUnderDesc)
		{
			uILabel = Util.CreateLabel("Text1", gameObject, uIFont);
			Util.SetLabelInfo(uILabel, Localization.Get("SpecialChance_Desc01"), mBaseDepth + 2, new Vector3(0f, -75f), 16, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = mLayout.UnderDescColor;
		}
		if (mLayout != null)
		{
			uISprite = Util.CreateSprite("Character", parent, mAtlas[LOAD_ATLAS.SPECIAL_CHANCE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, mLayout.CharacterName, mBaseDepth + 2, new Vector3(0f, 260f) + mLayout.CharacterPositionOffset, Vector3.one, false);
		}
		GameObject parent3 = null;
		if (mLayout != null)
		{
			uISprite = Util.CreateSprite("CaptionPanel", parent, mAtlas[LOAD_ATLAS.SPECIAL_CHANCE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, "chance_panel", mBaseDepth + 3, mLayout.CaptionPanelPos, Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(545, mLayout.CaptionPanelDimensionHeight);
			parent3 = uISprite.gameObject;
		}
		if (mLayout != null)
		{
			uISprite = Util.CreateSprite("Caption", parent3, mAtlas[LOAD_ATLAS.SPECIAL_CHANCE_DIALOG].Image);
			Util.SetSpriteInfo(uISprite, mLayout.CaptionName, mBaseDepth + 4, mLayout.CaptionPos, Vector3.one, false);
		}
		if (mLayout != null)
		{
			mLayout.CreateTopDescText(parent3, uIFont, mBaseDepth + 4);
		}
		if (mLayout != null)
		{
			mLayout.CreateFirstPurchaseFrame(mShopItemData, gameObject, uIFont, mBaseDepth + 3);
		}
		empty = "LC_button_buy";
		if (mLayout != null)
		{
			empty = mLayout.PurchaceButtonSpriteName;
		}
		uIButton = Util.CreateJellyImageButton("BuyButton", gameObject, (mLayout == null) ? mAtlas[LOAD_ATLAS.HUD].Image : mAtlas[mLayout.PurchaceButtonAtlasType].Image);
		Util.SetImageButtonInfo(uIButton, empty, empty, empty, mBaseDepth + 4, new Vector3(0f, -170f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnBuyButton_Clicked", UIButtonMessage.Trigger.OnClick);
		GameObject parent4 = uIButton.gameObject;
		if (mLayout != null)
		{
			mLayout.CreateButtonPopImage(parent4, mBaseDepth + 6);
		}
		mPriceLabel = Util.CreateLabel("Price", parent4, uIFont);
		Util.SetLabelInfo(mPriceLabel, ((mShopItemData != null && !mIsFreePrice) ? mShopItemData.GemAmount : 0).ToString(), mBaseDepth + 5, new Vector3(60f, -5f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		mPriceLabel.color = Def.DEFAULT_MESSAGE_COLOR;
		if (mShopItemData != null && !mIsFreePrice && mShopItemData.GemAmount > mGame.mPlayer.Dollar)
		{
			mPriceLabel.color = PRICE_LABEL_LACKING_GEM_COLOR;
		}
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			CreateGemWindow();
			mIsCreatedGemWindow = true;
		}
		CreateCancelButton("OnCancelButton_Clicked");
	}

	private void ShowNetworkErrorDialog(STATE aNextState)
	{
		mState.Reset(STATE.WAIT, true);
		ConfirmDialog confirmDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(delegate
		{
			mState.Change(aNextState);
		});
	}

	private void OnBuyButton_Clicked(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		int amount = ((mShopItemData != null && !mIsFreePrice) ? mShopItemData.GemAmount : 0);
		if (amount <= mGame.mPlayer.Dollar)
		{
			GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
			gemUseConfirmDialog.Init(amount, 4);
			gemUseConfirmDialog.SetClosedCallback(delegate(GemUseConfirmDialog.SELECT_ITEM i, int a_value)
			{
				if (i == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					mState.Change((STATE)a_value);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			});
		}
		else
		{
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			confirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
			{
				if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
					purchaseDialog.SetBaseDepth(mBaseDepth + 10);
					purchaseDialog.SetClosedCallback(delegate
					{
						mGemDisplayNum = mGame.mPlayer.Dollar;
						mGemNumLabel.text = mGemDisplayNum.ToString();
						if (amount > mGame.mPlayer.Dollar)
						{
							mPriceLabel.color = PRICE_LABEL_LACKING_GEM_COLOR;
						}
						else
						{
							DialogBase.SetDialogLabelEffect2(mPriceLabel);
						}
						mState.Change(STATE.MAIN);
					});
					purchaseDialog.SetAuthErrorClosedCallback(delegate
					{
						mState.Change(STATE.AUTHERROR_WAIT);
					});
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			});
		}
		mState.Reset(STATE.WAIT, true);
	}

	private void OnCancelButton_Clicked(GameObject go)
	{
		if (!GetBusy() && (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAINTENANCE_CHECK_WAIT))
		{
			mState.Reset(STATE.WAIT);
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public override void OnCloseFinished()
	{
		int offer_num = 0;
		int sale_num = 0;
		Options mOptions = SingletonMonoBehaviour<GameMain>.Instance.mOptions;
		int first_purchase = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.FirstPurchasedTime);
		int num = 0;
		if (mGame.mPlayer.Data.ChanceTotalOfferCount.ContainsKey(mCondition.ChanceID))
		{
			offer_num = mGame.mPlayer.Data.ChanceTotalOfferCount[mCondition.ChanceID];
		}
		if (mGame.mPlayer.Data.ChanceTotalSaleCount.ContainsKey(mCondition.ChanceID))
		{
			sale_num = mGame.mPlayer.Data.ChanceTotalSaleCount[mCondition.ChanceID];
		}
		SpecialChanceConditionSet.CHANCE_MODE cHANCE_MODE = mMode;
		if (cHANCE_MODE == SpecialChanceConditionSet.CHANCE_MODE.PLUS || cHANCE_MODE == SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP)
		{
			int num2 = (mGame.mPlayer.Data.ChanceDispCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS) ? mGame.mPlayer.Data.ChanceDispCount[SpecialChanceConditionSet.CHANCE_MODE.PLUS] : 0);
			int num3 = (mGame.mPlayer.Data.ChanceDispCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP) ? mGame.mPlayer.Data.ChanceDispCount[SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP] : 0);
			num = num2 + num3;
		}
		else
		{
			num = (mGame.mPlayer.Data.ChanceDispCount.ContainsKey(mMode) ? mGame.mPlayer.Data.ChanceDispCount[mMode] : 0);
		}
		int cur_series = (int)mGame.mPlayer.Data.LastPlaySeries;
		int cur_stage = mGame.mPlayer.Data.LastPlayLevel;
		int evt_id = 9999;
		if (mGame.IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
		{
			int num4 = ((mGame.mDebugDailyChallengeID <= 0) ? GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID : mGame.mDebugDailyChallengeID);
			cur_series = 200;
			cur_stage = int.Parse(string.Format("{0}{1}{2}", num4, (mGame.SelectedDailyChallengeSheetNo - 1).ToString("00"), mGame.SelectedDailyChallengeStageNo));
			evt_id = num4;
		}
		else if (mGame.mEventMode)
		{
			evt_id = mGame.mCurrentStage.EventID;
		}
		ServerCram.SaleSpecialChance(mCondition.ChanceID, (int)(mMode + 1), evt_id, cur_series, cur_stage, num, sale_num, offer_num, (int)mUserSegment, (int)mSelectItem, first_purchase, (int)mGame.mChargePriceCheckData.price, mOptions.CurrencyCode);
		short aChanceType = (short)(mMode + 1);
		bool aBuy = false;
		if (mSelectItem != 0)
		{
			aBuy = true;
		}
		short aGem = (short)((mShopItemData != null) ? mShopItemData.GemAmount : 0);
		UserBehavior.Instance.CountSpecialChance(aChanceType, aBuy, aGem);
		UserBehavior.Save();
		if (mSelectItem != 0)
		{
			switch (mMode)
			{
			case SpecialChanceConditionSet.CHANCE_MODE.SPECIAL:
				mGame.mPlayer.Data.UseSpecialChance = true;
				break;
			case SpecialChanceConditionSet.CHANCE_MODE.SCORE_UP:
				mGame.mPlayer.Data.UseScoreUpChance = true;
				mGame.mPlayer.Data.UsePlusChanceBoosterCount = 0;
				mGame.mPlayer.Data.UseBeforeBooster.Clear();
				break;
			case SpecialChanceConditionSet.CHANCE_MODE.PLUS:
				mGame.mPlayer.Data.UsePlusChance = true;
				mGame.mPlayer.Data.UsePlusChanceBoosterCount = 0;
				mGame.mPlayer.Data.UseBeforeBooster.Clear();
				break;
			case SpecialChanceConditionSet.CHANCE_MODE.CONTINUE:
				mGame.mPlayer.Data.UseContinueChance = true;
				break;
			case SpecialChanceConditionSet.CHANCE_MODE.STAR_GET:
				mGame.mPlayer.Data.UseStarGetChallenge = true;
				break;
			}
			if (mGame.mPlayer.Data.ChanceDispCount == null)
			{
				mGame.mPlayer.Data.ChanceDispCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
			}
			if (!mGame.mPlayer.Data.ChanceDispCount.ContainsKey(mMode))
			{
				mGame.mPlayer.Data.ChanceDispCount.Add(mMode, 0);
			}
			mGame.mPlayer.Data.ChanceDispCount[mMode] = 0;
		}
		if (mClosedCallback != null)
		{
			mClosedCallback.Invoke(mSelectItem);
		}
		if (mAtlas != null)
		{
			foreach (AtlasInfo value in mAtlas.Values)
			{
				if (value != null)
				{
					value.UnLoad();
				}
			}
			mAtlas.Clear();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelButton_Clicked(null);
	}

	protected override void SetLayoutGemDisp(ScreenOrientation o)
	{
		base.SetLayoutGemDisp(o);
		if (o != ScreenOrientation.LandscapeLeft && o != ScreenOrientation.LandscapeRight)
		{
			return;
		}
		SpecialChanceConditionSet.CHANCE_MODE mode = Mode;
		if (mode == SpecialChanceConditionSet.CHANCE_MODE.STAR_GET)
		{
			mGemFrame.MulLocalPositionX(-1f);
			Vector3 localPosition = mGemFrame.transform.localPosition;
			localPosition.y += (mGemFrame.localSize.y + 38f) / 2f + 10f;
			if (mEulaButton != null)
			{
				mEulaButton.SetLocalPosition(localPosition);
			}
		}
	}

	public void AddClosedEvent(params UnityAction<SELECT_ITEM>[] calls)
	{
		if (calls != null)
		{
			if (mClosedCallback == null)
			{
				mClosedCallback = new ClosedCallback();
			}
			foreach (UnityAction<SELECT_ITEM> call in calls)
			{
				mClosedCallback.AddListener(call);
			}
		}
	}

	public void RemoveClosedEvent(params UnityAction<SELECT_ITEM>[] calls)
	{
		if (calls != null && mClosedCallback != null)
		{
			foreach (UnityAction<SELECT_ITEM> call in calls)
			{
				mClosedCallback.RemoveListener(call);
			}
		}
	}

	public void ClearClosedEvent()
	{
		if (mClosedCallback != null)
		{
			mClosedCallback.RemoveAllListeners();
		}
	}

	public void SetClosedEvent(params UnityAction<SELECT_ITEM>[] calls)
	{
		ClearClosedEvent();
		AddClosedEvent(calls);
	}

	public void AddAuthErrorEvent(params UnityAction[] calls)
	{
		if (calls != null)
		{
			if (mAuthErrorCallback == null)
			{
				mAuthErrorCallback = new UnityEvent();
			}
			foreach (UnityAction call in calls)
			{
				mAuthErrorCallback.AddListener(call);
			}
		}
	}

	public void RemoveAuthErrorEvent(params UnityAction[] calls)
	{
		if (calls != null && mAuthErrorCallback != null)
		{
			foreach (UnityAction call in calls)
			{
				mAuthErrorCallback.RemoveListener(call);
			}
		}
	}

	public void ClearAuthErrorEvent()
	{
		if (mAuthErrorCallback != null)
		{
			mAuthErrorCallback.RemoveAllListeners();
		}
	}

	public void SetAuthErrorEvent(params UnityAction[] calls)
	{
		ClearAuthErrorEvent();
		AddAuthErrorEvent(calls);
	}
}
