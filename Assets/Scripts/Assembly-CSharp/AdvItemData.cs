public class AdvItemData
{
	[MiniJSONAlias("id")]
	public int id { get; set; }

	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }
}
