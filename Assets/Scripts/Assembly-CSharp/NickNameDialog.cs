using System;
using System.Text;
using UnityEngine;

public class NickNameDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		NETWORK_00 = 3,
		NETWORK_00_1 = 4,
		NETWORK_01 = 5,
		NETWORK_LOGIN00 = 6,
		NETWORK_LOGIN01 = 7
	}

	public enum ID_RESULT
	{
		HIT = 0,
		ALREADY = 1,
		NOTFOUND = 2
	}

	public delegate void OnDialogClosed(bool mHereLogin);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	public int mItem = 2;

	private OnDialogClosed mCallback;

	private UIInput mInputs;

	private string mIDdata;

	private string mBeforeName = string.Empty;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	public bool mHereLogin;

	private ConfirmDialog mErrorDialog;

	public bool mTutorial;

	private Def.TUTORIAL_INDEX mNextIndex;

	private float mAdjustY;

	public bool Modified { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		if (mInputs != null)
		{
			EventDelegate.Remove(mInputs.onChange, OnChangeUIInput);
		}
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_00:
		{
			string inputPassword = GetInputPassword();
			if (string.IsNullOrEmpty(inputPassword))
			{
				mErrorDialog = Util.CreateGameObject("NicknameErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
				mErrorDialog.Init(Localization.Get("MyNicknameEmptyNG_Title"), Localization.Get("MyNicknameEmptyNG_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
				mErrorDialog.SetClosedCallback(OnErrorDialogClosed);
				mState.Change(STATE.WAIT);
			}
			else if (mGame.mPlayer.NickName.CompareTo(inputPassword) == 0)
			{
				mState.Change(STATE.MAIN);
			}
			else
			{
				Network_NicknameEdit(inputPassword);
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		}
		case STATE.NETWORK_00_1:
			if (mGame.NicknameEditCheckDone)
			{
				mState.Change(STATE.NETWORK_01);
			}
			break;
		case STATE.NETWORK_01:
		{
			if (mGame.NicknameEditSuccess)
			{
				mGame.mPlayer.NickName = GetInputPassword();
				mGame.Save();
				Close();
				break;
			}
			string empty = string.Empty;
			string empty2 = string.Empty;
			Server.ErrorCode nicknameEditErrorCode = mGame.NicknameEditErrorCode;
			if (nicknameEditErrorCode == Server.ErrorCode.NICKNAME_IN_NGWORD)
			{
				empty = Localization.Get("MyNicknameNG_Title");
				empty2 = Localization.Get("MyNicknameNG_Desc");
			}
			else
			{
				empty = Localization.Get("Network_Error_Title");
				empty2 = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
			}
			mErrorDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mErrorDialog.Init(empty, empty2, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mErrorDialog.SetClosedCallback(OnErrorDialogClosed);
			mState.Change(STATE.WAIT);
			break;
		}
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							Modified = false;
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
						mHereLogin = true;
						if (mGame.mPlayer.Data.PlayerIcon < 10000)
						{
							mGame.mPlayer.Data.PlayerIcon += 10000;
						}
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns))
				{
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}, null);
			break;
		}
		}
		mState.Update();
	}

	private void OnChangeUIInput()
	{
		mInputs.value = StringUtils.RemoveEmoji(mInputs.value);
	}

	public void CreateUIInput()
	{
		if (mInputs != null)
		{
			EventDelegate.Remove(mInputs.onChange, OnChangeUIInput);
			UnityEngine.Object.Destroy(mInputs.gameObject);
		}
		UIFont font = GameMain.LoadFont();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mInputs = Util.CreateInput("Input", base.gameObject);
		Util.SetInputInfo(mInputs, font, 24, Def.InputName, image, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 60f + mAdjustY, 0f), Vector3.one, 400, 60, UIWidget.Pivot.Center, 0f, string.Empty);
		Util.SetInputType(mInputs);
		EventDelegate.Add(mInputs.onChange, OnChangeUIInput);
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		mBeforeName = mGame.mPlayer.NickName;
		if (!GameMain.IsSNSFunctionEnabled(SNS_FLAG.Nickname) || !GameMain.IsSNSFunctionEnabled(SNS_FLAG.Nickname, SNS.Facebook))
		{
			mAdjustY = -20f;
		}
		string titleDescKey = Localization.Get("ChangeNickname");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		titleDescKey = string.Format(Localization.Get("MyNickname"));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 108f + mAdjustY, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		titleDescKey = string.Format(Localization.Get("MyNickname_Notice"));
		uILabel = Util.CreateLabel("ID_Search_Notice", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 14f + mAdjustY, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.red;
		CreateUIInput();
		UIButton button;
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Nickname) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Nickname, SNS.Facebook))
		{
			button = Util.CreateJellyImageButton("FaceBookButton", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_FacebookName", "LC_button_FacebookName", "LC_button_FacebookName", mBaseDepth + 3, new Vector3(0f, -40f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnFaceBookNamePushed", UIButtonMessage.Trigger.OnClick);
		}
		button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_decision", "LC_button_decision", "LC_button_decision", mBaseDepth + 3, new Vector3(0f, -118f - mAdjustY, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnOKPushed", UIButtonMessage.Trigger.OnClick);
		if (mTutorial)
		{
			UISprite back = Util.CreateSprite("bg", mInputs.gameObject, image);
			Util.SetInputCollider(mInputs, back, 0f);
			mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE0, base.ParentGameObject, OnTutorialMessageFinished, OnTutorialMessageClosed);
		}
		CreateCancelButton("OnCancelPushed");
	}

	public void OnTutorial()
	{
		mGame.mTutorialManager.MessageFakeOpen();
		TutorialStart(mNextIndex);
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE && selectItem != TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			TutorialStart(tUTORIAL_INDEX);
			return;
		}
		CreateUIInput();
		mGame.mTutorialManager.DeleteTutorialPointArrow();
		mGame.mTutorialManager.TutorialSkip(index);
		mGame.Save();
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		mNextIndex = mGame.mTutorialManager.TutorialComplete(index);
		if (mNextIndex != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(mNextIndex);
			if (mNextIndex == Def.TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE1)
			{
				mGame.mTutorialManager.SetTutorialArrow(mInputs.gameObject, mInputs.gameObject, false, 100);
			}
		}
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.DeleteTutorialPointArrow();
			mGame.mTutorialManager.TutorialSkip(index);
		}
	}

	public void Init(bool Tutorial = false)
	{
		mTutorial = Tutorial;
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnOKPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			string inputPassword = GetInputPassword();
			inputPassword = inputPassword.Trim();
			if (!(inputPassword == string.Empty))
			{
				mState.Change(STATE.NETWORK_00);
			}
		}
	}

	private void OnFaceBookNamePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSNS = SocialManager.Instance[SNS.Facebook];
			if (IsFacebookLogined())
			{
				mInputs.value = mGame.mOptions.FbName;
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private string GetInputPassword()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(mInputs.value);
		return stringBuilder.ToString();
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = base.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mHereLogin);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void OnErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mErrorDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void Network_NicknameEdit(string a_name)
	{
		mGame.Network_NicknameEdit(a_name);
	}
}
