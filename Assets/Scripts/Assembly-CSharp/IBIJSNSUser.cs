using System;

public interface IBIJSNSUser : ICloneable, ICopyable
{
	string ID { get; }

	string Name { get; }

	string Gender { get; }
}
