public class MissionProgress
{
	public int mID { get; set; }

	public int mStep { get; set; }

	public int mValue { get; set; }

	public int mChangeValue { get; set; }

	public MissionProgress()
	{
		Init();
	}

	public void Init()
	{
		mID = 0;
		mStep = 1;
		mValue = 0;
		mChangeValue = 0;
	}

	public void SerializeToBinary(BIJBinaryWriter aWriter)
	{
		aWriter.WriteInt(mID);
		aWriter.WriteInt(mStep);
		aWriter.WriteInt(mValue);
		aWriter.WriteInt(mChangeValue);
	}

	public void DeserializeFromBinary(BIJBinaryReader aReader)
	{
		mID = aReader.ReadInt();
		mStep = aReader.ReadInt();
		mValue = aReader.ReadInt();
		mChangeValue = aReader.ReadInt();
	}
}
