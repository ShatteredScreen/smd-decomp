public class TileData
{
	public enum TILE_TYPE
	{
		NORMAL = 0,
		ATTRIBUTE = 1
	}

	public enum ANIMATION_TYPE
	{
		NOTANIMATION = 0,
		ANIMATION = 1
	}

	public enum DROP_ANIME_TYPE
	{
		NORMAL = 0,
		NORMAL_SHINE = 1,
		NORMAL_SHAKE = 2,
		AFTER_CHANGE = 3,
		EXPLOSION = 4,
		GLOW_BOMB_WAIT = 5,
		WAVE_V = 6,
		WAVE_H = 7,
		BOMB = 8,
		BOMB_B = 9,
		RAINBOW = 10,
		ADDTIME = 11,
		TIMER_BOMB = 12,
		JEWEL = 13,
		PAINT = 14,
		TAP_BOMB = 15,
		ADDPLUS = 16,
		ADDHP = 17,
		FOOT_STAMP = 18,
		JEWEL3 = 19,
		JEWEL5 = 20,
		JEWEL9 = 21,
		PEARL = 22,
		PEARL_EXPLOSION = 23,
		SKILL = 24,
		MAXNUM = 25
	}

	public short index;

	public string name;

	public TILE_TYPE tileType;

	public bool isThrough;

	public bool isMove;

	public bool isFall;

	public bool isExit;

	public bool isCaptor;

	public ANIMATION_TYPE animationType;

	public byte attributePriority;

	public float zOffset;

	public bool isColor;

	public bool isIngredient;

	public bool isPresentBox;

	public bool isKey;

	public bool isFind;

	public bool isThroughWall;

	public bool isBrokenWall;

	public bool isMovableWall;

	public bool isCapture;

	public bool isGrownUp;

	public bool isOrb;

	public bool isDiana;

	public bool isFlower;

	public bool isTalisman;

	public bool isTalismanFace;

	public string[] DropAnimeNames = new string[25];

	public string[][] DropImageNames = new string[25][];

	private CHANGE_PART_INFO[] mPrevPartsInfo;

	public void deserialize(BIJBinaryReader stream)
	{
		index = stream.ReadShort();
		name = stream.ReadUTF();
		tileType = (TILE_TYPE)stream.ReadByte();
		isThrough = stream.ReadBool();
		isMove = stream.ReadBool();
		isFall = stream.ReadBool();
		isExit = stream.ReadBool();
		isCaptor = stream.ReadBool();
		animationType = (ANIMATION_TYPE)stream.ReadByte();
		attributePriority = stream.ReadByte();
		zOffset = stream.ReadFloat();
		for (int i = 0; i < 25; i++)
		{
			DropAnimeNames[i] = stream.ReadUTF();
			string text = stream.ReadUTF();
			string[] array = text.Split(',');
			DropImageNames[i] = new string[array.Length];
			for (int j = 0; j < array.Length; j++)
			{
				string text2 = array[j];
				DropImageNames[i][j] = text2;
			}
		}
		stream.ReadByte();
		isColor = false;
		isIngredient = false;
		isPresentBox = false;
		isKey = false;
		isFind = false;
		isThroughWall = false;
		isBrokenWall = false;
		isMovableWall = false;
		isCapture = false;
		isGrownUp = false;
		isOrb = false;
		isDiana = false;
		isFlower = false;
		isTalisman = false;
		isTalismanFace = false;
		switch ((Def.TILE_KIND)index)
		{
		case Def.TILE_KIND.COLOR0:
		case Def.TILE_KIND.COLOR1:
		case Def.TILE_KIND.COLOR2:
		case Def.TILE_KIND.COLOR3:
		case Def.TILE_KIND.COLOR4:
		case Def.TILE_KIND.COLOR5:
		case Def.TILE_KIND.COLOR6:
			isColor = true;
			break;
		case Def.TILE_KIND.INGREDIENT0:
		case Def.TILE_KIND.INGREDIENT1:
		case Def.TILE_KIND.INGREDIENT2:
		case Def.TILE_KIND.INGREDIENT3:
			isIngredient = true;
			break;
		case Def.TILE_KIND.PRESENTBOX0:
		case Def.TILE_KIND.PRESENTBOX1:
		case Def.TILE_KIND.PRESENTBOX2:
		case Def.TILE_KIND.PRESENTBOX3:
		case Def.TILE_KIND.PRESENTBOX4:
		case Def.TILE_KIND.PRESENTBOX5:
		case Def.TILE_KIND.PRESENTBOX6:
		case Def.TILE_KIND.PRESENTBOX7:
		case Def.TILE_KIND.PRESENTBOX8:
		case Def.TILE_KIND.PRESENTBOX9:
		case Def.TILE_KIND.PRESENTBOX10:
		case Def.TILE_KIND.PRESENTBOX11:
		case Def.TILE_KIND.PRESENTBOX12:
		case Def.TILE_KIND.PRESENTBOX13:
		case Def.TILE_KIND.PRESENTBOX14:
		case Def.TILE_KIND.PRESENTBOX15:
		case Def.TILE_KIND.PRESENTBOX16:
		case Def.TILE_KIND.PRESENTBOX17:
		case Def.TILE_KIND.PRESENTBOX18:
		case Def.TILE_KIND.PRESENTBOX19:
		case Def.TILE_KIND.PRESENTBOX20:
		case Def.TILE_KIND.PRESENTBOX21:
		case Def.TILE_KIND.PRESENTBOX22:
		case Def.TILE_KIND.PRESENTBOX23:
			isPresentBox = true;
			break;
		case Def.TILE_KIND.KEY0:
		case Def.TILE_KIND.KEY1:
		case Def.TILE_KIND.KEY2:
		case Def.TILE_KIND.KEY3:
		case Def.TILE_KIND.KEY4:
		case Def.TILE_KIND.KEY5:
		case Def.TILE_KIND.KEY6:
		case Def.TILE_KIND.KEY7:
		case Def.TILE_KIND.KEY8:
			isKey = true;
			break;
		case Def.TILE_KIND.FIND0:
		case Def.TILE_KIND.FIND1:
		case Def.TILE_KIND.FIND2:
		case Def.TILE_KIND.FIND3:
			isFind = true;
			break;
		case Def.TILE_KIND.THROUGH_WALL0:
		case Def.TILE_KIND.THROUGH_WALL1:
		case Def.TILE_KIND.THROUGH_WALL2:
			isThroughWall = true;
			break;
		case Def.TILE_KIND.BROKEN_WALL0:
		case Def.TILE_KIND.BROKEN_WALL1:
		case Def.TILE_KIND.BROKEN_WALL2:
			isBrokenWall = true;
			break;
		case Def.TILE_KIND.MOVABLE_WALL0:
		case Def.TILE_KIND.MOVABLE_WALL1:
		case Def.TILE_KIND.MOVABLE_WALL2:
		case Def.TILE_KIND.STAR:
			isMovableWall = true;
			break;
		case Def.TILE_KIND.CAPTURE0:
		case Def.TILE_KIND.CAPTURE1:
		case Def.TILE_KIND.CAPTURE2:
			isCapture = true;
			break;
		case Def.TILE_KIND.GROWN_UP0:
		case Def.TILE_KIND.GROWN_UP1:
		case Def.TILE_KIND.GROWN_UP2:
			isGrownUp = true;
			break;
		case Def.TILE_KIND.ORB_WAVE_V0:
		case Def.TILE_KIND.ORB_WAVE_V1:
		case Def.TILE_KIND.ORB_WAVE_V2:
		case Def.TILE_KIND.ORB_WAVE_H0:
		case Def.TILE_KIND.ORB_WAVE_H1:
		case Def.TILE_KIND.ORB_WAVE_H2:
		case Def.TILE_KIND.ORB_BOMB0:
		case Def.TILE_KIND.ORB_BOMB1:
		case Def.TILE_KIND.ORB_BOMB2:
		case Def.TILE_KIND.ORB_BOSSDAMAGE0:
		case Def.TILE_KIND.ORB_BOSSDAMAGE1:
		case Def.TILE_KIND.ORB_BOSSDAMAGE2:
			isOrb = true;
			break;
		case Def.TILE_KIND.DIANA:
			isDiana = true;
			break;
		case Def.TILE_KIND.FIXED_FLOWER0:
		case Def.TILE_KIND.FIXED_FLOWER1:
		case Def.TILE_KIND.MOVABLE_FLOWER0:
		case Def.TILE_KIND.MOVABLE_FLOWER1:
			isFlower = true;
			break;
		case Def.TILE_KIND.TALISMAN0_0:
		case Def.TILE_KIND.TALISMAN1_0:
		case Def.TILE_KIND.TALISMAN2_0:
		case Def.TILE_KIND.TALISMAN0_1:
		case Def.TILE_KIND.TALISMAN1_1:
		case Def.TILE_KIND.TALISMAN2_1:
		case Def.TILE_KIND.TALISMAN0_2:
		case Def.TILE_KIND.TALISMAN1_2:
		case Def.TILE_KIND.TALISMAN2_2:
			isTalisman = true;
			break;
		case Def.TILE_KIND.TALISMAN0_3:
		case Def.TILE_KIND.TALISMAN1_3:
		case Def.TILE_KIND.TALISMAN2_3:
			isTalismanFace = true;
			break;
		case Def.TILE_KIND.SPECIAL:
		case Def.TILE_KIND.NORMAL:
		case Def.TILE_KIND.EDGE:
		case Def.TILE_KIND.THROUGH:
		case Def.TILE_KIND.WALL:
		case Def.TILE_KIND.SPAWN:
		case Def.TILE_KIND.EXIT:
		case Def.TILE_KIND.INCREASE:
		case Def.TILE_KIND.PAIR0_PARTS:
		case Def.TILE_KIND.PAIR0_COMPLETE:
		case Def.TILE_KIND.PAIR1_PARTS:
		case Def.TILE_KIND.PAIR1_COMPLETE:
		case Def.TILE_KIND.BOSS0:
		case Def.TILE_KIND.WARP_IN:
		case Def.TILE_KIND.WARP_OUT:
		case Def.TILE_KIND.SKILL_BALL:
		case Def.TILE_KIND.DIANA_GOAL_L:
		case Def.TILE_KIND.DIANA_GOAL_R:
		case Def.TILE_KIND.DIANA_ROUTE:
			break;
		}
	}

	public void GetChangePartsInfo(DROP_ANIME_TYPE a_anim_type, Def.TILE_FORM form, out CHANGE_PART_INFO[] a_ret, Def.TILE_KIND prevKind = Def.TILE_KIND.NONE)
	{
		switch (a_anim_type)
		{
		case DROP_ANIME_TYPE.AFTER_CHANGE:
			switch (form)
			{
			case Def.TILE_FORM.WAVE_V:
				a_anim_type = DROP_ANIME_TYPE.WAVE_V;
				break;
			case Def.TILE_FORM.WAVE_H:
				a_anim_type = DROP_ANIME_TYPE.WAVE_H;
				break;
			case Def.TILE_FORM.BOMB:
				a_anim_type = DROP_ANIME_TYPE.BOMB;
				break;
			case Def.TILE_FORM.BOMB_B:
				a_anim_type = DROP_ANIME_TYPE.BOMB_B;
				break;
			case Def.TILE_FORM.RAINBOW:
				a_anim_type = DROP_ANIME_TYPE.RAINBOW;
				break;
			case Def.TILE_FORM.ADDTIME:
				a_anim_type = DROP_ANIME_TYPE.ADDTIME;
				break;
			case Def.TILE_FORM.ADDPLUS:
				a_anim_type = DROP_ANIME_TYPE.ADDPLUS;
				break;
			case Def.TILE_FORM.TIMER_BOMB:
				a_anim_type = DROP_ANIME_TYPE.TIMER_BOMB;
				break;
			case Def.TILE_FORM.JEWEL:
				a_anim_type = DROP_ANIME_TYPE.JEWEL;
				break;
			case Def.TILE_FORM.PAINT:
				a_anim_type = DROP_ANIME_TYPE.PAINT;
				break;
			case Def.TILE_FORM.TAP_BOMB:
				a_anim_type = DROP_ANIME_TYPE.TAP_BOMB;
				break;
			case Def.TILE_FORM.ADDHP:
				a_anim_type = DROP_ANIME_TYPE.ADDHP;
				break;
			case Def.TILE_FORM.FOOT_STAMP:
				a_anim_type = DROP_ANIME_TYPE.FOOT_STAMP;
				break;
			case Def.TILE_FORM.JEWEL3:
				a_anim_type = DROP_ANIME_TYPE.JEWEL3;
				break;
			case Def.TILE_FORM.JEWEL5:
				a_anim_type = DROP_ANIME_TYPE.JEWEL5;
				break;
			case Def.TILE_FORM.JEWEL9:
				a_anim_type = DROP_ANIME_TYPE.JEWEL9;
				break;
			case Def.TILE_FORM.PEARL:
				a_anim_type = DROP_ANIME_TYPE.PEARL;
				break;
			case Def.TILE_FORM.SKILL:
				a_anim_type = DROP_ANIME_TYPE.SKILL;
				break;
			}
			break;
		case DROP_ANIME_TYPE.EXPLOSION:
			switch (form)
			{
			case Def.TILE_FORM.TAP_BOMB:
				a_anim_type = DROP_ANIME_TYPE.TAP_BOMB;
				break;
			case Def.TILE_FORM.FOOT_STAMP:
				a_anim_type = DROP_ANIME_TYPE.FOOT_STAMP;
				break;
			case Def.TILE_FORM.JEWEL:
				a_anim_type = DROP_ANIME_TYPE.JEWEL;
				break;
			case Def.TILE_FORM.JEWEL3:
				a_anim_type = DROP_ANIME_TYPE.JEWEL3;
				break;
			case Def.TILE_FORM.JEWEL5:
				a_anim_type = DROP_ANIME_TYPE.JEWEL5;
				break;
			case Def.TILE_FORM.JEWEL9:
				a_anim_type = DROP_ANIME_TYPE.JEWEL9;
				break;
			case Def.TILE_FORM.PEARL:
				a_anim_type = DROP_ANIME_TYPE.PEARL;
				break;
			}
			break;
		}
		string[] array = DropImageNames[(int)a_anim_type];
		a_ret = new CHANGE_PART_INFO[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			a_ret[i].partName = string.Format("change_{0:000}", i);
			a_ret[i].spriteName = array[i];
			a_ret[i].enableOffset = false;
		}
		if (a_anim_type == DROP_ANIME_TYPE.PEARL_EXPLOSION && prevKind != Def.TILE_KIND.FIXED_FLOWER1 && a_ret.Length >= 4)
		{
			a_ret[2].spriteName = "null";
			a_ret[3].spriteName = "null";
		}
		mPrevPartsInfo = a_ret;
	}

	public DROP_ANIME_TYPE GetAnimeType(ref DROP_ANIME_TYPE defaultAnimeType, Def.TILE_FORM form, Def.TILE_KIND prevKind = Def.TILE_KIND.NONE)
	{
		if (defaultAnimeType == DROP_ANIME_TYPE.AFTER_CHANGE)
		{
			if (form == Def.TILE_FORM.PEARL && (prevKind == Def.TILE_KIND.MOVABLE_FLOWER1 || prevKind == Def.TILE_KIND.FIXED_FLOWER1))
			{
				defaultAnimeType = DROP_ANIME_TYPE.PEARL_EXPLOSION;
			}
		}
		if (defaultAnimeType == DROP_ANIME_TYPE.NORMAL)
		{
			switch (form)
			{
			case Def.TILE_FORM.WAVE_V:
				defaultAnimeType = DROP_ANIME_TYPE.WAVE_V;
				break;
			case Def.TILE_FORM.WAVE_H:
				defaultAnimeType = DROP_ANIME_TYPE.WAVE_H;
				break;
			case Def.TILE_FORM.BOMB:
				defaultAnimeType = DROP_ANIME_TYPE.BOMB;
				break;
			case Def.TILE_FORM.BOMB_B:
				defaultAnimeType = DROP_ANIME_TYPE.BOMB_B;
				break;
			case Def.TILE_FORM.RAINBOW:
				defaultAnimeType = DROP_ANIME_TYPE.RAINBOW;
				break;
			case Def.TILE_FORM.ADDTIME:
				defaultAnimeType = DROP_ANIME_TYPE.ADDTIME;
				break;
			case Def.TILE_FORM.ADDPLUS:
				defaultAnimeType = DROP_ANIME_TYPE.ADDPLUS;
				break;
			case Def.TILE_FORM.TIMER_BOMB:
				defaultAnimeType = DROP_ANIME_TYPE.TIMER_BOMB;
				break;
			case Def.TILE_FORM.JEWEL:
				defaultAnimeType = DROP_ANIME_TYPE.JEWEL;
				break;
			case Def.TILE_FORM.PAINT:
				defaultAnimeType = DROP_ANIME_TYPE.PAINT;
				break;
			case Def.TILE_FORM.TAP_BOMB:
				defaultAnimeType = DROP_ANIME_TYPE.TAP_BOMB;
				break;
			case Def.TILE_FORM.ADDHP:
				defaultAnimeType = DROP_ANIME_TYPE.ADDHP;
				break;
			case Def.TILE_FORM.FOOT_STAMP:
				defaultAnimeType = DROP_ANIME_TYPE.FOOT_STAMP;
				break;
			case Def.TILE_FORM.JEWEL3:
				defaultAnimeType = DROP_ANIME_TYPE.JEWEL3;
				break;
			case Def.TILE_FORM.JEWEL5:
				defaultAnimeType = DROP_ANIME_TYPE.JEWEL5;
				break;
			case Def.TILE_FORM.JEWEL9:
				defaultAnimeType = DROP_ANIME_TYPE.JEWEL9;
				break;
			case Def.TILE_FORM.PEARL:
				defaultAnimeType = DROP_ANIME_TYPE.PEARL;
				break;
			case Def.TILE_FORM.SKILL:
				defaultAnimeType = DROP_ANIME_TYPE.SKILL;
				break;
			}
		}
		return defaultAnimeType;
	}

	public TileData Clone()
	{
		return (TileData)MemberwiseClone();
	}
}
