using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public static class CryptUtils
{
	public static byte[] EncryptRJ128(byte[] bytes)
	{
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		rijndaelManaged.BlockSize = 128;
		rijndaelManaged.KeySize = 128;
		rijndaelManaged.Padding = PaddingMode.Zeros;
		rijndaelManaged.Mode = CipherMode.CBC;
		rijndaelManaged.Key = Encoding.UTF8.GetBytes(Constants.CRYPT_AES_KEY);
		rijndaelManaged.IV = Encoding.UTF8.GetBytes(Constants.CRYPT_AES_IV);
		ICryptoTransform transform = rijndaelManaged.CreateEncryptor();
		MemoryStream memoryStream = new MemoryStream();
		CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
		byte b = (byte)(16 - (bytes.Length & 0xF));
		byte[] array = new byte[bytes.Length + b];
		Array.Copy(bytes, 0, array, 0, bytes.Length);
		for (int i = bytes.Length; i < array.Length; i++)
		{
			array[i] = b;
		}
		cryptoStream.Write(array, 0, array.Length);
		cryptoStream.FlushFinalBlock();
		return memoryStream.ToArray();
	}

	public static byte[] DecryptRJ128(byte[] bytes)
	{
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		rijndaelManaged.BlockSize = 128;
		rijndaelManaged.KeySize = 128;
		rijndaelManaged.Padding = PaddingMode.Zeros;
		rijndaelManaged.Mode = CipherMode.CBC;
		rijndaelManaged.Key = Encoding.UTF8.GetBytes(Constants.CRYPT_AES_KEY);
		rijndaelManaged.IV = Encoding.UTF8.GetBytes(Constants.CRYPT_AES_IV);
		ICryptoTransform transform = rijndaelManaged.CreateDecryptor();
		byte[] array = new byte[bytes.Length];
		MemoryStream stream = new MemoryStream(bytes);
		CryptoStream cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read);
		cryptoStream.Read(array, 0, array.Length);
		int num = array[array.Length - 1];
		byte[] array2 = new byte[array.Length - num];
		Array.Copy(array, 0, array2, 0, array2.Length);
		return array2;
	}
}
