public class BIJDLData
{
	public enum STATS
	{
		NON = 0,
		DL = 1,
		UNZIP = 2,
		DL_WAIT = 4096,
		ALL_COMP = 3,
		ERROR = 256
	}

	public bool IsAssetBundle;

	public string Extension = string.Empty;

	public string BaseName;

	public int BaseNameHash;

	public STATS DLDataStats;

	public string URL { get; set; }

	public string CRC { get; set; }

	public bool NeedUnZip { get; set; }

	public string Query { get; set; }

	public int DataSize { get; set; }

	public string AssetBundleName { get; set; }

	public BIJDLData()
	{
		URL = string.Empty;
		CRC = string.Empty;
		DataSize = 0;
		BaseName = string.Empty;
		Query = string.Empty;
		NeedUnZip = false;
		DLDataStats = STATS.NON;
	}

	public void SetData(BIJDLData a_data)
	{
		URL = a_data.URL;
		CRC = a_data.CRC;
		NeedUnZip = a_data.NeedUnZip;
		Query = a_data.Query;
		DataSize = a_data.DataSize;
		DLDataStats = a_data.DLDataStats;
		SetData();
	}

	public void SetData()
	{
		BaseName = URL.Substring(URL.LastIndexOf('/') + 1);
		if (BaseName != string.Empty)
		{
			BaseNameHash = BaseName.GetHashCode();
		}
		else
		{
			BaseNameHash = 65535;
		}
	}

	public bool CompareData(BIJDLData a_data)
	{
		if (DataSize != a_data.DataSize)
		{
			return false;
		}
		if (CRC != a_data.CRC)
		{
			return false;
		}
		return true;
	}

	public void SerializeToLocalFile(BIJBinaryWriter a_writer, int a_reqVersion)
	{
		a_writer.WriteUTF(URL);
		a_writer.WriteUTF(CRC);
		a_writer.WriteInt(DataSize);
	}

	public static BIJDLData DeserializeOldFormatFromLocalFile(BIJBinaryReader a_reader, int a_reqVersion)
	{
		BIJDLData bIJDLData = new BIJDLData();
		int num = a_reader.ReadInt();
		bIJDLData.URL = a_reader.ReadUTF();
		bIJDLData.CRC = a_reader.ReadUTF();
		bIJDLData.DataSize = a_reader.ReadInt();
		bIJDLData.SetData();
		return bIJDLData;
	}

	public static BIJDLData DeserializeFromLocalFile(BIJBinaryReader a_reader, int a_savedVersion)
	{
		BIJDLData bIJDLData = new BIJDLData();
		bIJDLData.URL = a_reader.ReadUTF();
		bIJDLData.CRC = a_reader.ReadUTF();
		bIJDLData.DataSize = a_reader.ReadInt();
		bIJDLData.SetData();
		return bIJDLData;
	}

	public void DeserializeFromServerFile(BIJBinaryReader a_reader, int a_version)
	{
		URL = a_reader.ReadUTF();
		CRC = a_reader.ReadUTF();
		DataSize = a_reader.ReadInt();
		AssetBundleName = a_reader.ReadUTF();
		SetData();
	}

	public string GetURLFilename()
	{
		if (BaseName == string.Empty)
		{
			BaseName = URL.Substring(URL.LastIndexOf('/') + 1);
		}
		return BaseName;
	}

	public string GetURLPath()
	{
		string uRL = URL;
		int length = uRL.LastIndexOf('/');
		return uRL.Substring(0, length);
	}

	public bool HasFilename(string a_name)
	{
		bool result = false;
		int hashCode = a_name.GetHashCode();
		if (BaseNameHash == hashCode)
		{
			result = true;
		}
		return result;
	}

	public void Dump()
	{
		string empty = string.Empty;
		empty = string.Format("URL:{0}\nCRC:{1}\nUnZip:{2}\nQuery:{3}\nBaseName:{4}\nBaseNameHash:{5}\nDataSize:{6}", URL, CRC, NeedUnZip, Query, BaseName, BaseNameHash, DataSize);
	}
}
