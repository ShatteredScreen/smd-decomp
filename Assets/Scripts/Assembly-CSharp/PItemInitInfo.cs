public class PItemInitInfo
{
	public PItem.KIND mKind;

	public int mIconID;

	public int mAmount;

	public float mCost;

	public float mCostEn;

	public string mFormattedPrice;

	public string mFormattedPriceEn;

	public string mItemID;

	public string mItemID_Debug;

	public string mItemID_DebugOld;

	public string mItemIDEnIOS;

	public string mItemIDEnAD;

	public string mItemIDEn_Debug;

	public string mItemIDEn_DebugOld;

	public PItemInitInfo(PItem.KIND aKind, int aIconID, int aAmount, float aCost, float aCostEn, string aFormattedPrice, string aFormattedPriceEn, string aItemID, string aItemID_Debug, string aItemID_DebugOld, string aItemIDEnIOS, string aItemIDEnAD, string aItemIDEn_Debug, string aItemIDEn_DebugOld)
	{
		mKind = aKind;
		mIconID = aIconID;
		mAmount = aAmount;
		mCost = aCost;
		mCostEn = aCostEn;
		mFormattedPrice = aFormattedPrice;
		mFormattedPriceEn = aFormattedPriceEn;
		mItemID = aItemID;
		mItemID_Debug = aItemID_Debug;
		mItemIDEnIOS = aItemIDEnIOS;
		mItemIDEnAD = aItemIDEnAD;
		mItemIDEn_Debug = aItemIDEn_Debug;
		mItemID_DebugOld = aItemID_DebugOld;
		mItemIDEn_DebugOld = aItemIDEn_DebugOld;
	}

	public float GetCost()
	{
		return mCostEn;
	}

	public string GetFormattedPrice()
	{
		return mFormattedPriceEn;
	}

	public string GetItemID()
	{
		if (GameMain.USE_DEBUG_TIER)
		{
			return mItemIDEn_Debug;
		}
		return mItemIDEnAD;
	}
}
