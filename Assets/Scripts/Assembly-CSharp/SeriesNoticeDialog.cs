using System.Collections.Generic;
using UnityEngine;

public class SeriesNoticeDialog : DialogBase
{
	public enum STATE
	{
		OPEN = 0,
		CLOSE = 1,
		WAIT = 2
	}

	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i, Def.SERIES a_series);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.OPEN);

	private SELECT_ITEM mSelectItem;

	private Def.SERIES mSeries;

	private LinkBannerRequest mLinkBannerReq;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.OPEN:
			if (!GetBusy())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.WAIT:
			if (mGame.mBackKeyTrg)
			{
				OnBackKeyPress();
			}
			break;
		}
		mState.Update();
	}

	public void Init(Def.SERIES a_series)
	{
		mSeries = a_series;
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mLinkBannerReq = new LinkBannerRequest();
		LinkBannerDetail linkBannerDetail = new LinkBannerDetail();
		linkBannerDetail.FileName = string.Format("series_{0}.png", Def.SeriesPrefix[mSeries]);
		LinkBannerSetting linkBannerSetting = new LinkBannerSetting();
		linkBannerSetting.BaseURL = mGame.mGameProfile.LinkBannerBaseURL;
		linkBannerSetting.BannerList.Add(linkBannerDetail);
		bool linkBannerRequest = LinkBannerManager.Instance.GetLinkBannerRequest(mLinkBannerReq, linkBannerSetting, OnGetComplete_LinkBanner);
		InitDialogFrame(DIALOG_SIZE.LARGE);
		CreateCancelButton("OnPositivePushed");
		SetLayout(Util.ScreenOrientation);
	}

	public void OnGetComplete_LinkBanner(List<LinkBannerInfo> aBannerList)
	{
		if (aBannerList != null && aBannerList.Count != 0)
		{
			Texture2D banner = aBannerList[0].banner;
			if (banner != null)
			{
				UITexture uITexture = Util.CreateGameObject("Banner", base.gameObject).AddComponent<UITexture>();
				uITexture.depth = mBaseDepth + 2;
				uITexture.mainTexture = banner;
				uITexture.SetDimensions(banner.width, banner.height);
			}
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem, mSeries);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
