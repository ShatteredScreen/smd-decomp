using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ScClippingCamera : MonoBehaviour
{
	private Camera mCamera;

	private Vector3 mPosition;

	private Vector4 mClipping = new Vector4(0f, 0f, 0f, 0f);

	private bool mClippingUpdate;

	public static ScClippingCamera Instance
	{
		get
		{
			return UnityEngine.Object.FindObjectOfType(typeof(ScClippingCamera)) as ScClippingCamera;
		}
	}

	public Vector4 ClippingRect
	{
		get
		{
			return mClipping;
		}
	}

	[method: MethodImpl(32)]
	public static event Action<Camera, Vector4> UpdateClippingEvent;

	private void Start()
	{
		mCamera = base.gameObject.GetComponent<Camera>();
		mPosition = base.gameObject.transform.position;
	}

	public void UpdateClippingRect()
	{
		mClippingUpdate = true;
	}

	public void CallUpdateClippingRect()
	{
		if (ScClippingCamera.UpdateClippingEvent != null)
		{
			ScClippingCamera.UpdateClippingEvent(mCamera, mClipping);
		}
	}

	private void OnPreCull()
	{
		try
		{
			if (mClippingUpdate)
			{
				Vector3 vector = mCamera.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
				Vector3 vector2 = mCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f));
				mClipping.x = vector.x;
				mClipping.y = vector.y;
				mClipping.z = vector2.x;
				mClipping.w = vector2.y;
				Shader.SetGlobalVector("_Clip", mClipping);
				CallUpdateClippingRect();
				mClippingUpdate = false;
			}
		}
		catch (Exception)
		{
		}
	}

	private void OnPreRender()
	{
		GL.SetRevertBackfacing(true);
	}

	private void OnPostRender()
	{
		GL.SetRevertBackfacing(false);
	}
}
