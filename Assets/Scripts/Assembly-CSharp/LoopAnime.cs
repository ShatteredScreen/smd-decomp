using UnityEngine;

public class LoopAnime : OneShotAnime
{
	public GameObject ParentGO { get; set; }

	public Vector3 Position
	{
		get
		{
			return base.gameObject.transform.localPosition;
		}
		set
		{
			base.gameObject.transform.localPosition = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
	}

	protected new virtual void Update()
	{
		base.Update();
	}

	public override void OnDestroy()
	{
		ParentGO = null;
		base.OnDestroy();
	}

	public new void Init(string index, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, AnimationCallback onFinished)
	{
		Init(index, null, null, pos, scale, rotateDeg, delay, autoDestroy, onFinished);
	}

	public void Init(string index, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, int playCount = 0, AnimationCallback onFinished = null)
	{
		mPlayCount = playCount;
		Init(index, changePartsArray, atlas, pos, scale, rotateDeg, delay, autoDestroy, onFinished);
	}

	protected override void AnimeStart(CHANGE_PART_INFO[] changePartsArray)
	{
		if (changePartsArray == null)
		{
			ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_animationName);
			_sprite.Animation = resSsAnimation.SsAnime;
			_sprite.DestroyAtEnd = mAutoDestroy;
			_sprite.PlayCount = mPlayCount;
			_sprite.Play();
		}
		else
		{
			ChangeAnime(_animationName, changePartsArray, false, 0, _sprite.AnimationFinished);
		}
	}

	public void SetEnable(bool a_flg)
	{
		if (ParentGO != null)
		{
			ParentGO.SetActive(a_flg);
		}
	}

	public bool IsEnable()
	{
		if (ParentGO != null)
		{
			return ParentGO.activeSelf;
		}
		return false;
	}
}
