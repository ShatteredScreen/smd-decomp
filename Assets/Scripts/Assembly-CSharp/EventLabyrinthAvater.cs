using System.Collections.Generic;
using UnityEngine;

public class EventLabyrinthAvater : MapAvater
{
	private float mOffset;

	private int mInitStageNo;

	public void Init(int a_stageno, SMMapPage a_map, bool a_active, float a_offset)
	{
		mInitStageNo = a_stageno;
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mMapPage = a_map;
		mOffset = a_offset;
		SetIcon();
		NGUITools.SetActive(mPlayerIcon, a_active);
	}

	public void Init(int a_stageno)
	{
		Init(a_stageno, mMapPage, true, mOffset);
	}

	public override void FindPosition(int a_stageno = 0)
	{
		int num = a_stageno;
		if (num == 0)
		{
			num = mInitStageNo;
		}
		Vector3 vector = Vector3.zero;
		List<SsMapEntity> list = new List<SsMapEntity>(mMapPage.NewMapSsDataEntity.MapStageList.Values);
		float num2 = 0f;
		if (num == 10000)
		{
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].Start.CompareTo("start") == 0)
				{
					vector = list[i].Position;
				}
			}
		}
		else
		{
			for (int j = 0; j < list.Count; j++)
			{
				int a_main = (int)float.Parse(list[j].AnimeKey);
				int stageNo = Def.GetStageNo(a_main, 0);
				if (stageNo == num)
				{
					vector = list[j].Position;
				}
			}
			num2 = mOffset;
		}
		mPlayerIcon.transform.localPosition = new Vector3(vector.x, vector.y + num2, Def.MAP_AVATAR_Z);
	}
}
