using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using UnityEngine;

public class Network : SingletonMonoBehaviour<Network>
{
	private class RequestAsync
	{
		public ManualResetEvent Sync { get; set; }

		public int SeqNo { get; set; }

		public HttpWebRequest Request { get; set; }

		public BIJBinaryReader RequestData { get; set; }
	}

	public delegate void RequestHandler(HttpAsync _httpAsync);

	private const string DEBUG_DIR = "debug";

	private const string TEMP_DIR = "temp";

	private const UnityEngine.ThreadPriority ACTIVE_THREAD_PRIORITY = UnityEngine.ThreadPriority.High;

	private static AdInfo mAdInfo;

	private static readonly IDictionary<string, string> Certificates = new Dictionary<string, string>();

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private bool isTerminating;

	private static string mUnityID = null;

	private static string mUniqueDeviceIdentifierCache = null;

	private static int mRequestSeqNoCounter = 0;

	public static string RequestID
	{
		get
		{
			return Guid.NewGuid().ToString();
		}
	}

	public static string BNIDAuthID
	{
		get
		{
			return BIJUnity.getAndroidID();
		}
	}

	public static string BNIDPublishID
	{
		get
		{
			if (GameMain.NO_NETWORK)
			{
				return "53CE2F77-3448-45EE-A339-FAA0A11B3C43";
			}
			string text = BIJUnity.getIMEI();
			if (string.IsNullOrEmpty(text))
			{
				text = BIJUnity.getMacAddress();
			}
			return text;
		}
	}

	public static string MacAddress
	{
		get
		{
			string text = BIJUnity.getMacAddress();
			if (text == null)
			{
				text = string.Empty;
			}
			return text;
		}
	}

	public static string OpenUDID
	{
		get
		{
			return string.Empty;
		}
	}

	public static string IMSI
	{
		get
		{
			return BIJUnity.getIMSI();
		}
	}

	public static string Serial
	{
		get
		{
			return BIJUnity.getSerial();
		}
	}

	public static string DeviceSerial
	{
		get
		{
			return BIJUnity.getDeviceSerial();
		}
	}

	public static AdInfo AdInfo
	{
		set
		{
			mAdInfo = value;
		}
	}

	public static string AdvertisingID
	{
		get
		{
			if (mAdInfo == null || string.IsNullOrEmpty(mAdInfo.Id))
			{
				return string.Empty;
			}
			if (!mAdInfo.Enabled)
			{
				return "android";
			}
			return mAdInfo.Id;
		}
	}

	public static INetworkConstants NetworkConstants { get; set; }

	public static NetworkInterface NetworkInterface { get; set; }

	public static Server Server { get; private set; }

	public static ServerCram ServerCram { get; private set; }

	public static ServerIAP ServerIAP { get; private set; }

	public static PlaySessionManager PlaySessionManager { get; private set; }

	public static SocialManager SocialManager { get; private set; }

	public static NotificationManager NotificationManager { get; private set; }

	public static GameConnectManager GameConnectManager { get; private set; }

	public static ItemPossessManager ItemPossessManager { get; private set; }

	private static string DebugPath { get; set; }

	private static string TemporaryPath { get; set; }

	public string COUNTRY_CODE
	{
		get
		{
			string text = null;
			try
			{
				text = BIJUnity.getCountryCode();
			}
			catch (Exception)
			{
				text = null;
			}
			if (string.IsNullOrEmpty(text) || string.Compare("unknown", text, StringComparison.OrdinalIgnoreCase) == 0)
			{
				text = NetworkConstants.COUNTRY_CODE;
			}
			return text;
		}
	}

	public string LANGUAGE_CODE
	{
		get
		{
			string text = null;
			try
			{
				return BIJUnity.getLanguageCode();
			}
			catch (Exception)
			{
				return null;
			}
		}
	}

	public string CURRENCY_CODE
	{
		get
		{
			string text = null;
			try
			{
				return BIJUnity.getCurrencyCode();
			}
			catch (Exception)
			{
				return null;
			}
		}
	}

	public string NETWORK_SALT
	{
		get
		{
			return NetworkConstants.NETWORK_SALT;
		}
	}

	public string AppName
	{
		get
		{
			return NetworkConstants.APPNAME;
		}
	}

	public string AppBase_URL
	{
		get
		{
			return NetworkConstants.AppBase_URL;
		}
	}

	public string AppEventBase_URL
	{
		get
		{
			return NetworkConstants.AppEventBase_URL;
		}
	}

	public string ClientData_URL
	{
		get
		{
			return NetworkConstants.ClientData_URL;
		}
	}

	public string Social_URL
	{
		get
		{
			return NetworkConstants.Social_URL;
		}
	}

	public string NICKNAME_URL
	{
		get
		{
			return NetworkConstants.NICKNAME_URL;
		}
	}

	public string VIP_URL
	{
		get
		{
			return NetworkConstants.VIP_URL;
		}
	}

	public string IAP_URL
	{
		get
		{
			return NetworkConstants.IAP_URL;
		}
	}

	public string CRAM_URL
	{
		get
		{
			return NetworkConstants.CRAM_URL;
		}
	}

	public string CRAM_KPI_URL
	{
		get
		{
			return NetworkConstants.CRAM_KPI_URL;
		}
	}

	public string Campaign_URL
	{
		get
		{
			return NetworkConstants.Campaign_URL;
		}
	}

	public string Help_URL
	{
		get
		{
			return NetworkConstants.HELP_URL;
		}
	}

	public string MoreGames_URL
	{
		get
		{
			return NetworkConstants.MoreGames_URL;
		}
	}

	public string Store_URL
	{
		get
		{
			return NetworkConstants.STORE_URL;
		}
	}

	public string Reward_URL
	{
		get
		{
			return NetworkConstants.Reward_URL;
		}
	}

	public string TransferBase_URL
	{
		get
		{
			return NetworkConstants.TransferBase_URL;
		}
	}

	public string EULA_URL
	{
		get
		{
			return NetworkConstants.EULA_URL;
		}
	}

	public string DLFILELIST_URL
	{
		get
		{
			return NetworkConstants.DLFILELIST_URL;
		}
	}

	public string HEADER_PREFIX
	{
		get
		{
			return NetworkConstants.HEADER_PREFIX;
		}
	}

	public string GameInfo_URL
	{
		get
		{
			return NetworkConstants.GameInfo_URL;
		}
	}

	public string Help_FileURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("file:///android_asset/");
			stringBuilder.Append("help_manualEN.html");
			return stringBuilder.ToString();
		}
	}

	public string EULA_FileURL
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("file:///android_asset/");
			stringBuilder.Append("EULA/eula.html");
			return stringBuilder.ToString();
		}
	}

	public string Connection_URL
	{
		get
		{
			return NetworkConstants.Connection_URL;
		}
	}

	public string AppCode
	{
		get
		{
			return "sm";
		}
	}

	public string AppVer
	{
		get
		{
			return Def.AppVersionStr;
		}
	}

	public string SrvVer
	{
		get
		{
			return Def.SrvVersionStr;
		}
	}

	public string DatVer
	{
		get
		{
			return Def.VersionStr;
		}
	}

	public int HiveId
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.HiveId : 0;
		}
	}

	public int UUID
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.UUID : 0;
		}
	}

	public string NickName
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? string.Empty : ToSS(NetworkInterface.Player.NickName);
		}
	}

	public string SearchID
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? string.Empty : ToSS(NetworkInterface.Player.SearchID);
		}
	}

	public int Series
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.SeriesToS : 0;
		}
	}

	public int Level
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.LevelToS : 0;
		}
	}

	public int MasterCurrency
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.MasterCurrency : 0;
		}
	}

	public int SecondaryCurrency
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.SecondaryCurrency : 0;
		}
	}

	public int Experience
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.Experience : 0;
		}
	}

	public int CompanionID
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.CompanionIDToS : 0;
		}
	}

	public int CompanionLevel
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.CompanionLevelToS : 0;
		}
	}

	public int ClearSeries
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.ClearSeriesToS : 0;
		}
	}

	public int ClearStageNo
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.ClearStageNoToS : 0;
		}
	}

	public int LastSeries
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.LastSeriesToS : 0;
		}
	}

	public int LastStageNo
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.LastStageNoToS : 0;
		}
	}

	public long TotalScore
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? 0 : NetworkInterface.Player.TotalScoreToS;
		}
	}

	public long TotalStars
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? 0 : NetworkInterface.Player.TotalStarToS;
		}
	}

	public long TotalTrophy
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? 0 : NetworkInterface.Player.TotalTrophyToS;
		}
	}

	public long ServerTime
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? 0 : NetworkInterface.Player.ServerTimeToS;
		}
	}

	public long PlayDays
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? 0 : NetworkInterface.Player.PlayDays;
		}
	}

	public long IntervalDays
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? 0 : NetworkInterface.Player.IntervalDays;
		}
	}

	public long ContinuousDays
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? 0 : NetworkInterface.Player.ContinuousDays;
		}
	}

	public List<PlayStageParam> PlayStage
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Player == null) ? new List<PlayStageParam>() : NetworkInterface.Player.PlayStage;
		}
	}

	public string Fbid
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Options == null) ? string.Empty : ToSS(NetworkInterface.Options.Fbid);
		}
	}

	public string Gcid
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Options == null) ? string.Empty : ToSS(NetworkInterface.Options.Gcid);
		}
	}

	public string Gpid
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Options == null) ? string.Empty : ToSS(NetworkInterface.Options.Gpid);
		}
	}

	public string Gender
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.Options == null) ? string.Empty : ToSS(NetworkInterface.Options.Gender);
		}
	}

	public string UserSegment
	{
		get
		{
			return (NetworkInterface == null || NetworkInterface.NetworkProfile == null) ? "0" : ToSS(NetworkInterface.Concierge.GetUserSegmentNumber());
		}
	}

	public int AdvLastStage
	{
		get
		{
			return (NetworkInterface != null && NetworkInterface.Player != null) ? NetworkInterface.Player.AdvLastStage : 0;
		}
	}

	public string AdvAppBase_URL
	{
		get
		{
			return NetworkConstants.AdvAppBase_URL;
		}
	}

	public static string PlayerAdvertiseGUID
	{
		get
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(UniqueDeviceIdentifier);
			stringBuilder.Append("_");
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.HiveId);
			stringBuilder.Append("_");
			stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.AppName);
			log("PlayerAdvertiseGUID ::: " + stringBuilder.ToString());
			return stringBuilder.ToString();
		}
	}

	public static string UnityID
	{
		get
		{
			if (string.IsNullOrEmpty(mUnityID))
			{
				mUnityID = BIJUnity.getUnityID();
			}
			return mUnityID;
		}
	}

	public static string UniqueDeviceIdentifier
	{
		get
		{
			if (string.IsNullOrEmpty(mUniqueDeviceIdentifierCache))
			{
				mUniqueDeviceIdentifierCache = BIJUnity.getDeviceID();
			}
			return mUniqueDeviceIdentifierCache;
		}
	}

	public static string DeviceModel
	{
		get
		{
			string empty = string.Empty;
			return SystemInfo.deviceModel;
		}
	}

	public static string GraphicsDeviceSpec
	{
		get
		{
			return SystemInfo.graphicsDeviceVersion;
		}
	}

	public static int InternetConnectionMode
	{
		get
		{
			int result = 0;
			if (InternetReachability.Status == NetworkReachability.ReachableViaLocalAreaNetwork)
			{
				result = 1;
			}
			else if (InternetReachability.Status == NetworkReachability.ReachableViaCarrierDataNetwork)
			{
				result = 2;
			}
			return result;
		}
	}

	public static int Timeout { get; set; }

	public static int IOTimeout { get; set; }

	public Network()
	{
		SingletonMonoBehaviour<Network>.SetSingletonType(typeof(Network));
	}

	private static HttpAsync DoCreateHttpHeadJson(string url, ref Dictionary<string, object> p, int timeout, bool a_encryption = false)
	{
		throw new NotImplementedException("No Implementation Head Method in Json");
	}

	private static HttpAsync DoCreateHttpGetJson(string url, ref Dictionary<string, object> p, int timeout, bool a_encryption = false)
	{
		throw new NotImplementedException("No Implementation Get Method in Json");
	}

	public static HttpAsync CreateHttpPostJson(string url, string p, int timeout, bool a_encryption = false)
	{
		string text = p;
		if (a_encryption)
		{
			StringBuilder stringBuilder = new StringBuilder(text);
			stringBuilder.Append(NetworkConstants.NETWORK_SALT);
			BIJMD5 bIJMD = new BIJMD5(stringBuilder.ToString());
			string hash = bIJMD.ToString();
			string originalJson = text;
			JsonEncryption jsonEncryption = new JsonEncryption();
			jsonEncryption.Hash = hash;
			jsonEncryption.OriginalJson = originalJson;
			text = new MiniJSONSerializer().Serialize(jsonEncryption);
		}
		byte[] bytes = Encoding.UTF8.GetBytes(text);
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		Dictionary<string, string> p2 = new Dictionary<string, string>();
		GetCommonHeader(ref p2);
		dbg("CommonHeader =" + p2.Count);
		foreach (KeyValuePair<string, string> item in p2)
		{
			httpWebRequest.Headers.Add(item.Key, item.Value);
			dbg(string.Format("Custom Header key={0} value={1}", item.Key, item.Value));
		}
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = "application/json";
		httpWebRequest.ContentLength = bytes.LongLength;
		Stream requestStream = httpWebRequest.GetRequestStream();
		requestStream.Write(bytes, 0, bytes.Length);
		requestStream.Close();
		HttpAsync httpAsync = new HttpAsync(NextRequestSeqNo(), httpWebRequest, timeout);
		log(string.Format("URL[POST]({0}) {1}?{2}", httpAsync.SeqNo, url, text));
		return httpAsync;
	}

	private static HttpAsync DoCreateHttpPostJson(string url, ref Dictionary<string, object> p, int timeout, bool a_encryption = false)
	{
		throw new NotImplementedException("No Implementation Post Method to use Dictionary in Json");
	}

	public static bool CreateHttpPostAsyncJson(string url, string p, int timeout, RequestHandler handler, bool a_encryption = false, bool UseAdvSalt = false, bool UseEncoding = false)
	{
		int num = NextRequestSeqNo();
		dbg(string.Format("DoCreateHttpPostAsyncJson(#{0}) - Begin", num));
		string text = p;
		log("PostAsync JSON =" + text);
		if (a_encryption)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			StringBuilder stringBuilder = new StringBuilder(text);
			if (UseAdvSalt)
			{
				stringBuilder.Append(NetworkConstants.NETWORK_ADV_SALT);
			}
			else
			{
				stringBuilder.Append(NetworkConstants.NETWORK_SALT);
			}
			BIJMD5 bIJMD = new BIJMD5(stringBuilder.ToString());
			string hash = bIJMD.ToString();
			string originalJson = text;
			JsonEncryption jsonEncryption = new JsonEncryption();
			jsonEncryption.Hash = hash;
			jsonEncryption.OriginalJson = originalJson;
			text = new MiniJSONSerializer().Serialize(jsonEncryption);
			log("PostAsync encryption JSON =" + text);
		}
		byte[] bytes = Encoding.UTF8.GetBytes(text);
		log(string.Format("ASYNC REQUEST[POST](#{0}) : {1}?{2}", num, url, text));
		dbg(string.Format("DoCreateHttpPostAsyncJson(#{0}) - 1", num));
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		Dictionary<string, string> p2 = new Dictionary<string, string>();
		GetCommonHeader(ref p2);
		dbg("CommonHeader =" + p2.Count);
		foreach (KeyValuePair<string, string> item in p2)
		{
			httpWebRequest.Headers.Add(item.Key, item.Value);
		}
		if (UseEncoding)
		{
			httpWebRequest.Headers.Add("X-BIJ-ENCODING", "gzip");
		}
		string text2 = string.Empty;
		WebHeaderCollection headers = httpWebRequest.Headers;
		for (int i = 0; i < headers.Count; i++)
		{
			text2 += string.Format("Custom Header key={0} value={1}\n", headers.GetKey(i), headers[i]);
		}
		dbg(text2);
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = "application/json";
		httpWebRequest.ContentLength = bytes.LongLength;
		dbg(string.Format("DoCreateHttpPostAsyncJson(#{0}) - 2", num));
		Coroutine arg = SingletonMonoBehaviour<Network>.Instance.StartCoroutine(CreatePostRequestStreamAsync(num, httpWebRequest, timeout, bytes, handler));
		dbg(string.Format("DoCreateHttpPostAsyncJson(#{0}) - End : {1}", num, arg));
		return true;
	}

	private static bool DoCreateHttpPostAsyncJson(string url, ref Dictionary<string, object> p, int timeout, RequestHandler handler, bool a_encryption = false)
	{
		string p2 = MiniJSON.jsonEncode(p);
		return CreateHttpPostAsyncJson(url, p2, timeout, handler, a_encryption);
	}

	public static HttpAsync CreateHttpMultiPartJson(string url, string p, ref Dictionary<string, BIJBinaryReader> octets, int timeout, bool a_encryption = false)
	{
		string text = "utf-8";
		Encoding encoding = Encoding.GetEncoding(text);
		string arg = Guid.NewGuid().ToString().Replace("-", string.Empty);
		string text2 = p;
		if (a_encryption)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			StringBuilder stringBuilder = new StringBuilder(text2);
			stringBuilder.Append(NetworkConstants.NETWORK_SALT);
			BIJMD5 bIJMD = new BIJMD5(stringBuilder.ToString());
			string hash = bIJMD.ToString();
			string originalJson = text2;
			JsonEncryption jsonEncryption = new JsonEncryption();
			jsonEncryption.Hash = hash;
			jsonEncryption.OriginalJson = originalJson;
			text2 = new MiniJSONSerializer().Serialize(jsonEncryption);
		}
		long num = 0L;
		byte[] array = null;
		byte[] bytes;
		using (MemoryStream memoryStream = new MemoryStream())
		{
			string s = string.Format("--{0}\r\nContent-Type: application/json\r\nContent-disposition: form-data; name=\"{1}\"\r\n\r\n", arg, "json");
			bytes = encoding.GetBytes(s);
			memoryStream.Write(bytes, 0, bytes.Length);
			bytes = encoding.GetBytes(text2);
			memoryStream.Write(bytes, 0, bytes.Length);
			bytes = encoding.GetBytes("\r\n");
			memoryStream.Write(bytes, 0, bytes.Length);
			array = memoryStream.ToArray();
		}
		if (array != null)
		{
			num += array.LongLength;
		}
		if (octets.Count > 0)
		{
			foreach (KeyValuePair<string, BIJBinaryReader> octet in octets)
			{
				BIJBinaryReader value = octet.Value;
				string s2 = string.Format("--{0}\r\nContent-Type: application/octet-stream\r\nContent-disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\n\r\n", arg, octet.Key, Path.GetFileName(value.Path));
				bytes = encoding.GetBytes(s2);
				num += bytes.LongLength;
				value.Seek(0L, SeekOrigin.Begin);
				num += value.Seek(0L, SeekOrigin.End);
				value.Seek(0L, SeekOrigin.Begin);
				bytes = encoding.GetBytes("\r\n");
				num += bytes.LongLength;
			}
		}
		bytes = encoding.GetBytes(string.Format("--{0}--\r\n", arg));
		num += bytes.LongLength;
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		Dictionary<string, string> p2 = new Dictionary<string, string>();
		GetCommonHeader(ref p2);
		dbg("CommonHeader =" + p2.Count);
		foreach (KeyValuePair<string, string> item in p2)
		{
			httpWebRequest.Headers.Add(item.Key, item.Value);
			dbg(string.Format("Custom Header key={0} value={1}", item.Key, item.Value));
		}
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = string.Format("multipart/form-data; boundary=\"{0}\"", arg);
		httpWebRequest.ContentLength = num;
		Stream requestStream = httpWebRequest.GetRequestStream();
		BIJTempFileBinaryWriter bIJTempFileBinaryWriter = null;
		try
		{
			if (array != null)
			{
				requestStream.Write(array, 0, array.Length);
				if (bIJTempFileBinaryWriter != null)
				{
					bIJTempFileBinaryWriter.WriteRaw(array, 0, array.Length);
				}
			}
			if (octets.Count > 0)
			{
				byte[] array2 = new byte[4096];
				int num2 = 0;
				foreach (KeyValuePair<string, BIJBinaryReader> octet2 in octets)
				{
					BIJBinaryReader value2 = octet2.Value;
					string s3 = string.Format("--{0}\r\nContent-Type: application/octet-stream\r\nContent-disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\n\r\n", arg, octet2.Key, Path.GetFileName(value2.Path));
					bytes = encoding.GetBytes(s3);
					requestStream.Write(bytes, 0, bytes.Length);
					if (bIJTempFileBinaryWriter != null)
					{
						bIJTempFileBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					}
					while ((num2 = value2.ReadRaw(array2, 0, array2.Length)) != 0)
					{
						requestStream.Write(array2, 0, num2);
						if (bIJTempFileBinaryWriter != null)
						{
							bIJTempFileBinaryWriter.WriteRaw(array2, 0, num2);
						}
					}
					bytes = encoding.GetBytes("\r\n");
					requestStream.Write(bytes, 0, bytes.Length);
					if (bIJTempFileBinaryWriter != null)
					{
						bIJTempFileBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					}
				}
			}
			bytes = encoding.GetBytes(string.Format("--{0}--\r\n", arg));
			requestStream.Write(bytes, 0, bytes.Length);
			if (bIJTempFileBinaryWriter != null)
			{
				bIJTempFileBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
			}
		}
		finally
		{
			if (bIJTempFileBinaryWriter != null)
			{
				bIJTempFileBinaryWriter.Flush();
				bIJTempFileBinaryWriter.Close();
			}
		}
		requestStream.Close();
		HttpAsync httpAsync = new HttpAsync(NextRequestSeqNo(), httpWebRequest, timeout);
		log(string.Format("URL[MULTI]({0}) {1} path={2} size={3}", httpAsync.SeqNo, url, (bIJTempFileBinaryWriter == null) ? string.Empty : bIJTempFileBinaryWriter.Path, num));
		return httpAsync;
	}

	private static HttpAsync DoCreateHttpMultiPartJson(string url, ref Dictionary<string, object> p, int timeout, bool a_encryption = false)
	{
		Dictionary<string, BIJBinaryReader> dictionary = new Dictionary<string, BIJBinaryReader>();
		Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
		if (p != null && p.Count > 0)
		{
			foreach (KeyValuePair<string, object> item in p)
			{
				BIJBinaryReader bIJBinaryReader = item.Value as BIJBinaryReader;
				if (bIJBinaryReader != null)
				{
					dictionary[item.Key] = bIJBinaryReader;
				}
				else
				{
					dictionary2[item.Key] = item.Value;
				}
			}
		}
		string empty = string.Empty;
		if (dictionary2.Count > 0)
		{
			empty = new MiniJSONSerializer().Serialize(dictionary2);
		}
		throw new NotImplementedException("No Implementation Multi Method to use Dictionary in Json");
	}

	public static bool CreateHttpMultiPartAsyncJson(string url, string p, ref Dictionary<string, BIJBinaryReader> octets, int timeout, RequestHandler handler, bool a_encryption = false)
	{
		int num = NextRequestSeqNo();
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - Begin", num));
		log(string.Format("ASYNC REQUEST[MULTI](#{0}) : {1}?{2}", num, url, p));
		string arg = Guid.NewGuid().ToString().Replace("-", string.Empty);
		string path = Path.Combine(TemporaryPath, Path.GetRandomFileName());
		BIJBinaryWriter bIJBinaryWriter = null;
		string text = "utf-8";
		Encoding encoding = Encoding.GetEncoding(text);
		bool flag = true;
		try
		{
			dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 1", num));
			bIJBinaryWriter = new BIJFileBinaryWriter(path, encoding, false, true);
			dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 2 path=", num, bIJBinaryWriter.Path));
			byte[] bytes;
			if (p.Length > 0)
			{
				string s = string.Format("--{0}\r\nContent-Type: application/json\r\nContent-disposition: form-data; name=\"{1}\"\r\n\r\n", arg, "json");
				bytes = encoding.GetBytes(s);
				bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
				bytes = encoding.GetBytes(p);
				bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
				bytes = encoding.GetBytes("\r\n");
				bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
			}
			if (octets.Count > 0)
			{
				byte[] array = new byte[4096];
				int num2 = 0;
				foreach (KeyValuePair<string, BIJBinaryReader> octet in octets)
				{
					BIJBinaryReader value = octet.Value;
					string s2 = string.Format("--{0}\r\nContent-Type: application/octet-stream\r\nContent-disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\n\r\n", arg, octet.Key, Path.GetFileName(value.Path));
					bytes = encoding.GetBytes(s2);
					bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					while ((num2 = value.ReadRaw(array, 0, array.Length)) != 0)
					{
						bIJBinaryWriter.WriteRaw(array, 0, num2);
					}
					bytes = encoding.GetBytes("\r\n");
					bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
				}
			}
			bytes = encoding.GetBytes(string.Format("--{0}--\r\n", arg));
			bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
		}
		catch (Exception innerException)
		{
			flag = false;
			throw new Exception("temporary request failed", innerException);
		}
		finally
		{
			try
			{
				bIJBinaryWriter.Flush();
				bIJBinaryWriter.Close();
			}
			catch (Exception)
			{
			}
			if (!flag && bIJBinaryWriter != null)
			{
				try
				{
					if (File.Exists(bIJBinaryWriter.Path))
					{
						File.Delete(bIJBinaryWriter.Path);
					}
				}
				catch (Exception)
				{
				}
			}
		}
		bool closeOnDelete = true;
		BIJBinaryReader bIJBinaryReader = new BIJFileBinaryReader(path, encoding, closeOnDelete);
		long num3 = 0L;
		try
		{
			bIJBinaryReader.Seek(0L, SeekOrigin.Begin);
			num3 = bIJBinaryReader.Seek(0L, SeekOrigin.End);
			bIJBinaryReader.Seek(0L, SeekOrigin.Begin);
		}
		catch (Exception)
		{
		}
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 3 contentsLength={1}", num, num3));
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		Dictionary<string, string> p2 = new Dictionary<string, string>();
		GetCommonHeader(ref p2);
		dbg("CommonHeader =" + p2.Count);
		foreach (KeyValuePair<string, string> item in p2)
		{
			httpWebRequest.Headers.Add(item.Key, item.Value);
			dbg(string.Format("Custom Header key={0} value={1}", item.Key, item.Value));
		}
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = string.Format("multipart/form-data; boundary=\"{0}\"", arg);
		httpWebRequest.ContentLength = num3;
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 4", num));
		Coroutine arg2 = SingletonMonoBehaviour<Network>.Instance.StartCoroutine(CreatePostRequestStreamAsync(num, httpWebRequest, timeout, bIJBinaryReader, handler));
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - End : {1}", num, arg2));
		return true;
	}

	private static bool DoCreateHttpMultiPartAsyncJson(string url, ref Dictionary<string, object> p, int timeout, RequestHandler handler, bool a_encryption = false)
	{
		Dictionary<string, BIJBinaryReader> dictionary = new Dictionary<string, BIJBinaryReader>();
		Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
		if (p != null && p.Count > 0)
		{
			foreach (KeyValuePair<string, object> item in p)
			{
				BIJBinaryReader bIJBinaryReader = item.Value as BIJBinaryReader;
				if (bIJBinaryReader != null)
				{
					dictionary[item.Key] = bIJBinaryReader;
				}
				else
				{
					dictionary2[item.Key] = item.Value;
				}
			}
		}
		string empty = string.Empty;
		if (dictionary2.Count > 0)
		{
			empty = new MiniJSONSerializer().Serialize(dictionary2);
			if (a_encryption)
			{
				Dictionary<string, string> dictionary3 = new Dictionary<string, string>();
				StringBuilder stringBuilder = new StringBuilder(empty);
				stringBuilder.Append(NetworkConstants.NETWORK_SALT);
				BIJMD5 bIJMD = new BIJMD5(stringBuilder.ToString());
				string hash = bIJMD.ToString();
				string originalJson = empty;
				JsonEncryption jsonEncryption = new JsonEncryption();
				jsonEncryption.Hash = hash;
				jsonEncryption.OriginalJson = originalJson;
				empty = new MiniJSONSerializer().Serialize(jsonEncryption);
			}
		}
		throw new NotImplementedException("No Implementation Multi Async Method to use Dictionary in Json");
	}

	public static void GetCommonHeader(ref Dictionary<string, string> p)
	{
		p[SingletonMonoBehaviour<Network>.Instance.HEADER_PREFIX + "APPVER"] = SingletonMonoBehaviour<Network>.Instance.AppVer;
		p[SingletonMonoBehaviour<Network>.Instance.HEADER_PREFIX + "CC"] = SingletonMonoBehaviour<Network>.Instance.COUNTRY_CODE;
		p[SingletonMonoBehaviour<Network>.Instance.HEADER_PREFIX + "DEVICE"] = DeviceModel;
		p[SingletonMonoBehaviour<Network>.Instance.HEADER_PREFIX + "LAC"] = SingletonMonoBehaviour<Network>.Instance.LANGUAGE_CODE;
		p[SingletonMonoBehaviour<Network>.Instance.HEADER_PREFIX + "CUC"] = SingletonMonoBehaviour<Network>.Instance.CURRENCY_CODE;
	}

	public static void GetCommonParams(ref Dictionary<string, object> p)
	{
		p["udid"] = BNIDPublishID;
	}

	public static string GetCRAMCommonParams()
	{
		string text = SingletonMonoBehaviour<Network>.Instance.UUID.ToString();
		int num = SingletonMonoBehaviour<Network>.Instance.HiveId;
		if (GameMain.USE_DEBUG_DIALOG)
		{
			GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
			instance.CheckDevelopmentServer();
			if (instance.mDebugUseDevelopmentServer)
			{
				text = "1";
				num = 1;
			}
		}
		int series = SingletonMonoBehaviour<Network>.Instance.Series;
		int level = SingletonMonoBehaviour<Network>.Instance.Level;
		int masterCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		int secondaryCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		int experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		string userSegment = SingletonMonoBehaviour<Network>.Instance.UserSegment;
		long num2 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		long totalStars = SingletonMonoBehaviour<Network>.Instance.TotalStars;
		long totalTrophy = SingletonMonoBehaviour<Network>.Instance.TotalTrophy;
		long totalScore = SingletonMonoBehaviour<Network>.Instance.TotalScore;
		long playDays = SingletonMonoBehaviour<Network>.Instance.PlayDays;
		long intervalDays = SingletonMonoBehaviour<Network>.Instance.IntervalDays;
		long continuousDays = SingletonMonoBehaviour<Network>.Instance.ContinuousDays;
		string empty = string.Empty;
		empty = empty + "\"udid\":\"" + text + "\"";
		empty = empty + ", \"bhiveid\":" + num;
		empty = empty + ", \"appver\":\"" + SingletonMonoBehaviour<Network>.Instance.AppVer + "\"";
		empty = empty + ", \"series\":" + series;
		empty = empty + ", \"lvl\":" + level;
		empty = empty + ", \"mc\":" + masterCurrency;
		empty = empty + ", \"sc\":" + secondaryCurrency;
		empty = empty + ", \"xp\":" + experience;
		empty = empty + ", \"cc\":\"" + SingletonMonoBehaviour<Network>.Instance.COUNTRY_CODE + "\"";
		empty = empty + ", \"action_uts\":" + num2;
		empty = empty + ", \"u_seg\":\"" + userSegment + "\"";
		empty = empty + ", \"stars\":" + totalStars;
		empty = empty + ", \"trophy\":" + totalTrophy;
		empty = empty + ", \"total_score\":" + totalScore;
		empty = empty + ", \"playd\":" + playDays;
		empty = empty + ", \"intervald\":" + intervalDays;
		return empty + ", \"continuousd\":" + continuousDays;
	}

	public static string GetSupportParams(ref Dictionary<string, object> p)
	{
		return string.Empty;
	}

	private static HttpAsync DoCreateHttpHead(string url, ref Dictionary<string, object> p, int timeout)
	{
		string value = URLEncode(ref p);
		StringBuilder stringBuilder = new StringBuilder(url);
		if (!string.IsNullOrEmpty(value))
		{
			if (url.IndexOf('?') > 0)
			{
				stringBuilder.Append('&');
			}
			else
			{
				stringBuilder.Append('?');
			}
			stringBuilder.Append(value);
		}
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(stringBuilder.ToString());
		httpWebRequest.Method = "HEAD";
		HttpAsync httpAsync = new HttpAsync(NextRequestSeqNo(), httpWebRequest, timeout);
		log(string.Format("URL[HEAD]({0}) {1}", httpAsync.SeqNo, stringBuilder.ToString()));
		return httpAsync;
	}

	private static HttpAsync DoCreateHttpGet(string url, ref Dictionary<string, object> p, int timeout)
	{
		string value = URLEncode(ref p);
		StringBuilder stringBuilder = new StringBuilder(url);
		if (!string.IsNullOrEmpty(value))
		{
			if (url.IndexOf('?') > 0)
			{
				stringBuilder.Append('&');
			}
			else
			{
				stringBuilder.Append('?');
			}
			stringBuilder.Append(value);
		}
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(stringBuilder.ToString());
		httpWebRequest.Method = "GET";
		HttpAsync httpAsync = new HttpAsync(NextRequestSeqNo(), httpWebRequest, timeout);
		log(string.Format("URL[GET]({0}) {1}", httpAsync.SeqNo, stringBuilder.ToString()));
		return httpAsync;
	}

	private static HttpAsync DoCreateHttpPost(string url, ref Dictionary<string, object> p, int timeout)
	{
		string text = URLEncode(ref p);
		byte[] bytes = Encoding.UTF8.GetBytes(text);
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
		httpWebRequest.ContentLength = bytes.LongLength;
		Stream requestStream = httpWebRequest.GetRequestStream();
		requestStream.Write(bytes, 0, bytes.Length);
		requestStream.Close();
		HttpAsync httpAsync = new HttpAsync(NextRequestSeqNo(), httpWebRequest, timeout);
		log(string.Format("URL[POST]({0}) {1}?{2}", httpAsync.SeqNo, url, text));
		return httpAsync;
	}

	private static bool DoCreateHttpPostAsync(string url, ref Dictionary<string, object> p, int timeout, RequestHandler handler)
	{
		int num = NextRequestSeqNo();
		dbg(string.Format("DoCreateHttpPostAsync(#{0}) - Begin", num));
		string text = URLEncode(ref p);
		byte[] bytes = Encoding.UTF8.GetBytes(text);
		log(string.Format("ASYNC REQUEST[POST](#{0}) : {1}?{2}", num, url, text));
		dbg(string.Format("DoCreateHttpPostAsync(#{0}) - 1", num));
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
		httpWebRequest.ContentLength = bytes.LongLength;
		dbg(string.Format("DoCreateHttpPostAsync(#{0}) - 2", num));
		Coroutine arg = SingletonMonoBehaviour<Network>.Instance.StartCoroutine(CreatePostRequestStreamAsync(num, httpWebRequest, timeout, bytes, handler));
		dbg(string.Format("DoCreateHttpPostAsync(#{0}) - End : {1}", num, arg));
		return true;
	}

	public static HttpAsync DoCreateHttpMultiPart(string url, ref Dictionary<string, object> p, int timeout)
	{
		string arg = "utf-8";
		Encoding encoding = Encoding.GetEncoding(arg);
		string arg2 = Guid.NewGuid().ToString().Replace("-", string.Empty);
		Dictionary<string, BIJBinaryReader> dictionary = new Dictionary<string, BIJBinaryReader>();
		Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
		if (p != null && p.Count > 0)
		{
			foreach (KeyValuePair<string, object> item in p)
			{
				BIJBinaryReader bIJBinaryReader = item.Value as BIJBinaryReader;
				if (bIJBinaryReader != null)
				{
					dictionary[item.Key] = bIJBinaryReader;
				}
				else
				{
					dictionary2[item.Key] = item.Value;
				}
			}
		}
		long num = 0L;
		byte[] array = null;
		byte[] bytes;
		using (MemoryStream memoryStream = new MemoryStream())
		{
			if (dictionary2.Count > 0)
			{
				foreach (KeyValuePair<string, object> item2 in dictionary2)
				{
					string s = string.Format("--{0}\r\nContent-Type: text/plain; charset=\"{1}\"\r\nContent-disposition: form-data; name=\"{2}\"\r\n\r\n", arg2, arg, item2.Key);
					bytes = encoding.GetBytes(s);
					memoryStream.Write(bytes, 0, bytes.Length);
					bytes = encoding.GetBytes(ToSS(item2.Value));
					memoryStream.Write(bytes, 0, bytes.Length);
					bytes = encoding.GetBytes("\r\n");
					memoryStream.Write(bytes, 0, bytes.Length);
				}
				array = memoryStream.ToArray();
			}
		}
		if (array != null)
		{
			num += array.LongLength;
		}
		if (dictionary.Count > 0)
		{
			foreach (KeyValuePair<string, BIJBinaryReader> item3 in dictionary)
			{
				BIJBinaryReader value = item3.Value;
				string s2 = string.Format("--{0}\r\nContent-Type: application/octet-stream\r\nContent-disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\n\r\n", arg2, item3.Key, Path.GetFileName(value.Path));
				bytes = encoding.GetBytes(s2);
				num += bytes.LongLength;
				value.Seek(0L, SeekOrigin.Begin);
				num += value.Seek(0L, SeekOrigin.End);
				value.Seek(0L, SeekOrigin.Begin);
				bytes = encoding.GetBytes("\r\n");
				num += bytes.LongLength;
			}
		}
		bytes = encoding.GetBytes(string.Format("--{0}--\r\n", arg2));
		num += bytes.LongLength;
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = string.Format("multipart/form-data; boundary=\"{0}\"", arg2);
		httpWebRequest.ContentLength = num;
		Stream requestStream = httpWebRequest.GetRequestStream();
		BIJTempFileBinaryWriter bIJTempFileBinaryWriter = null;
		try
		{
			if (array != null)
			{
				requestStream.Write(array, 0, array.Length);
				if (bIJTempFileBinaryWriter != null)
				{
					bIJTempFileBinaryWriter.WriteRaw(array, 0, array.Length);
				}
			}
			if (dictionary.Count > 0)
			{
				byte[] array2 = new byte[4096];
				int num2 = 0;
				foreach (KeyValuePair<string, BIJBinaryReader> item4 in dictionary)
				{
					BIJBinaryReader value2 = item4.Value;
					string s3 = string.Format("--{0}\r\nContent-Type: application/octet-stream\r\nContent-disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\n\r\n", arg2, item4.Key, Path.GetFileName(value2.Path));
					bytes = encoding.GetBytes(s3);
					requestStream.Write(bytes, 0, bytes.Length);
					if (bIJTempFileBinaryWriter != null)
					{
						bIJTempFileBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					}
					while ((num2 = value2.ReadRaw(array2, 0, array2.Length)) != 0)
					{
						requestStream.Write(array2, 0, num2);
						if (bIJTempFileBinaryWriter != null)
						{
							bIJTempFileBinaryWriter.WriteRaw(array2, 0, num2);
						}
					}
					bytes = encoding.GetBytes("\r\n");
					requestStream.Write(bytes, 0, bytes.Length);
					if (bIJTempFileBinaryWriter != null)
					{
						bIJTempFileBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					}
				}
			}
			bytes = encoding.GetBytes(string.Format("--{0}--\r\n", arg2));
			requestStream.Write(bytes, 0, bytes.Length);
			if (bIJTempFileBinaryWriter != null)
			{
				bIJTempFileBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
			}
		}
		finally
		{
			if (bIJTempFileBinaryWriter != null)
			{
				bIJTempFileBinaryWriter.Flush();
				bIJTempFileBinaryWriter.Close();
			}
		}
		requestStream.Close();
		HttpAsync httpAsync = new HttpAsync(NextRequestSeqNo(), httpWebRequest, timeout);
		log(string.Format("URL[MULTI]({0}) {1} path={2} size={3}", httpAsync.SeqNo, url, (bIJTempFileBinaryWriter == null) ? string.Empty : bIJTempFileBinaryWriter.Path, num));
		return httpAsync;
	}

	private static bool DoCreateHttpMultiPartAsync(string url, ref Dictionary<string, object> p, int timeout, RequestHandler handler)
	{
		int num = NextRequestSeqNo();
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - Begin", num));
		string arg = "<hidden>";
		log(string.Format("ASYNC REQUEST[MULTI](#{0}) : {1}?{2}", num, url, arg));
		string arg2 = Guid.NewGuid().ToString().Replace("-", string.Empty);
		string path = Path.Combine(TemporaryPath, Path.GetRandomFileName());
		BIJBinaryWriter bIJBinaryWriter = null;
		string arg3 = "utf-8";
		Encoding encoding = Encoding.GetEncoding(arg3);
		bool flag = true;
		try
		{
			dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 1", num));
			bIJBinaryWriter = new BIJFileBinaryWriter(path, encoding, false, true);
			dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 2 path=", num, bIJBinaryWriter.Path));
			Dictionary<string, BIJBinaryReader> dictionary = new Dictionary<string, BIJBinaryReader>();
			Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
			if (p != null && p.Count > 0)
			{
				foreach (KeyValuePair<string, object> item in p)
				{
					BIJBinaryReader bIJBinaryReader = item.Value as BIJBinaryReader;
					if (bIJBinaryReader != null)
					{
						dictionary[item.Key] = bIJBinaryReader;
					}
					else
					{
						dictionary2[item.Key] = item.Value;
					}
				}
			}
			byte[] bytes;
			if (dictionary2.Count > 0)
			{
				foreach (KeyValuePair<string, object> item2 in dictionary2)
				{
					string s = string.Format("--{0}\r\nContent-Type: text/plain; charset=\"{1}\"\r\nContent-disposition: form-data; name=\"{2}\"\r\n\r\n", arg2, arg3, item2.Key);
					bytes = encoding.GetBytes(s);
					bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					bytes = encoding.GetBytes(ToSS(item2.Value));
					bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					bytes = encoding.GetBytes("\r\n");
					bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
				}
			}
			if (dictionary.Count > 0)
			{
				byte[] array = new byte[4096];
				int num2 = 0;
				foreach (KeyValuePair<string, BIJBinaryReader> item3 in dictionary)
				{
					BIJBinaryReader value = item3.Value;
					string s2 = string.Format("--{0}\r\nContent-Type: application/octet-stream\r\nContent-disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\n\r\n", arg2, item3.Key, Path.GetFileName(value.Path));
					bytes = encoding.GetBytes(s2);
					bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
					while ((num2 = value.ReadRaw(array, 0, array.Length)) != 0)
					{
						bIJBinaryWriter.WriteRaw(array, 0, num2);
					}
					bytes = encoding.GetBytes("\r\n");
					bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
				}
			}
			bytes = encoding.GetBytes(string.Format("--{0}--\r\n", arg2));
			bIJBinaryWriter.WriteRaw(bytes, 0, bytes.Length);
		}
		catch (Exception innerException)
		{
			flag = false;
			throw new Exception("temporary request failed", innerException);
		}
		finally
		{
			try
			{
				bIJBinaryWriter.Flush();
				bIJBinaryWriter.Close();
			}
			catch (Exception)
			{
			}
			if (!flag && bIJBinaryWriter != null)
			{
				try
				{
					if (File.Exists(bIJBinaryWriter.Path))
					{
						File.Delete(bIJBinaryWriter.Path);
					}
				}
				catch (Exception)
				{
				}
			}
		}
		bool closeOnDelete = true;
		BIJBinaryReader bIJBinaryReader2 = new BIJFileBinaryReader(path, encoding, closeOnDelete);
		long num3 = 0L;
		try
		{
			bIJBinaryReader2.Seek(0L, SeekOrigin.Begin);
			num3 = bIJBinaryReader2.Seek(0L, SeekOrigin.End);
			bIJBinaryReader2.Seek(0L, SeekOrigin.Begin);
		}
		catch (Exception)
		{
		}
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 3 contentsLength={1}", num, num3));
		HttpWebRequest httpWebRequest = CreateNewHttpRequest(url);
		httpWebRequest.Method = "POST";
		httpWebRequest.ContentType = string.Format("multipart/form-data; boundary=\"{0}\"", arg2);
		httpWebRequest.ContentLength = num3;
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - 4", num));
		Coroutine arg4 = SingletonMonoBehaviour<Network>.Instance.StartCoroutine(CreatePostRequestStreamAsync(num, httpWebRequest, timeout, bIJBinaryReader2, handler));
		dbg(string.Format("DoCreateHttpMultiPartAsync(#{0}) - End : {1}", num, arg4));
		return true;
	}

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
		try
		{
			long num = time();
		}
		catch (Exception)
		{
		}
	}

	private static void dbg(string msg)
	{
	}

	private static void assert(bool exp, string msg)
	{
	}

	protected override void Awake()
	{
		base.Awake();
	}

	private void AwakeSingleton()
	{
		log("@@@ Network AwakeSingleton");
		if (NetworkConstants == null)
		{
		}
		try
		{
			for (int i = 0; i < NetworkConstants.SSL_ACCEPT_CERTS.Length; i++)
			{
				try
				{
					string text = NetworkConstants.SSL_ACCEPT_CERTS[i];
					if (!string.IsNullOrEmpty(text))
					{
						text = text.Trim('\r', '\n', ' ').ToUpper();
						if (!string.IsNullOrEmpty(text))
						{
							Certificates[text] = text;
						}
					}
				}
				catch (Exception)
				{
				}
			}
		}
		catch (Exception)
		{
		}
		try
		{
			ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)Delegate.Combine(ServicePointManager.ServerCertificateValidationCallback, new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback));
		}
		catch (Exception)
		{
		}
		try
		{
			TemporaryPath = Path.Combine(BIJUnity.getTempPath(), "temp");
			if (Directory.Exists(TemporaryPath))
			{
				Directory.Delete(TemporaryPath, true);
			}
		}
		catch (Exception)
		{
		}
		Server.Timeout = 5000;
		ServerCram.Timeout = 5000;
		Server = base.gameObject.AddComponent<Server>();
		ServerCram = base.gameObject.AddComponent<ServerCram>();
		ServerIAP = base.gameObject.AddComponent<ServerIAP>();
		SocialManager = base.gameObject.AddComponent<SocialManager>();
		PlaySessionManager = base.gameObject.AddComponent<PlaySessionManager>();
		NotificationManager = base.gameObject.AddComponent<NotificationManager>();
		ItemPossessManager = base.gameObject.AddComponent<ItemPossessManager>();
		GameConnectManager = base.gameObject.AddComponent<GameConnectManager>();
	}

	private bool OnRemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
	{
		dbg("OnRemoteCertificateValidationCallback: ALWAYS ACCEPT");
		return true;
	}

	private GameObject CreateGameObject(string name)
	{
		GameObject gameObject = new GameObject(name);
		gameObject.transform.parent = base.gameObject.transform;
		gameObject.layer = base.gameObject.layer;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localRotation = Quaternion.identity;
		return gameObject;
	}

	private new void OnDestroy()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			Term();
		}
	}

	public void OnApplicationQuit()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			SendMessage("OnApplicationQuit", SendMessageOptions.DontRequireReceiver);
			Term();
		}
	}

	public void Term()
	{
		ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)Delegate.Remove(ServicePointManager.ServerCertificateValidationCallback, new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback));
	}

	private void OnApplicationPause(bool pauseStatus)
	{
	}

	public bool CheckHiveId(int _hiveId)
	{
		if (_hiveId == 0)
		{
			return false;
		}
		return true;
	}

	public static string GetCampaignURL()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(SingletonMonoBehaviour<Network>.Instance.Campaign_URL);
		stringBuilder.Append("campaign_top.php");
		Dictionary<string, object> p = new Dictionary<string, object>();
		string uniqueDeviceIdentifier = UniqueDeviceIdentifier;
		int hiveId = SingletonMonoBehaviour<Network>.Instance.HiveId;
		int uUID = SingletonMonoBehaviour<Network>.Instance.UUID;
		string appName = SingletonMonoBehaviour<Network>.Instance.AppName;
		string appVersionStr = Def.AppVersionStr;
		int level = SingletonMonoBehaviour<Network>.Instance.Level;
		int masterCurrency = SingletonMonoBehaviour<Network>.Instance.MasterCurrency;
		int secondaryCurrency = SingletonMonoBehaviour<Network>.Instance.SecondaryCurrency;
		int experience = SingletonMonoBehaviour<Network>.Instance.Experience;
		string fbid = SingletonMonoBehaviour<Network>.Instance.Fbid;
		string gcid = SingletonMonoBehaviour<Network>.Instance.Gcid;
		string gpid = SingletonMonoBehaviour<Network>.Instance.Gpid;
		p["udid"] = uniqueDeviceIdentifier;
		p["ou"] = "-";
		p["oid"] = "-";
		p["vid"] = "-";
		p["adid"] = "-";
		p["im"] = BIJUnity.getIMEI();
		p["aid"] = BIJUnity.getAndroidID();
		p["uuid"] = uUID;
		p["hiveid"] = hiveId;
		p["appname"] = appName;
		p["appcode"] = appName;
		p["appver"] = appVersionStr;
		p["lvl"] = string.Empty + level;
		p["mc"] = string.Empty + masterCurrency;
		p["sc"] = string.Empty + secondaryCurrency;
		p["xp"] = string.Empty + experience;
		p["fbid"] = fbid;
		p["gcid"] = gcid;
		p["gpid"] = gpid;
		if (p.Count > 0)
		{
			stringBuilder.Append("?");
			stringBuilder.Append(URLEncode(ref p));
		}
		return stringBuilder.ToString();
	}

	public static int NextRequestSeqNo()
	{
		mRequestSeqNoCounter++;
		return mRequestSeqNoCounter;
	}

	public static string ToSS(object _obj)
	{
		return (_obj != null) ? _obj.ToString() : string.Empty;
	}

	public static string ToASS(object _obj, string _alt)
	{
		return (!string.IsNullOrEmpty(ToSS(_obj))) ? _obj.ToString() : _alt;
	}

	public static string URLEncode(ref Dictionary<string, object> p)
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (p != null && p.Count > 0)
		{
			int num = 0;
			foreach (KeyValuePair<string, object> item in p)
			{
				if (num > 0)
				{
					stringBuilder.Append('&');
				}
				stringBuilder.Append(Uri.EscapeDataString(item.Key).Replace("%20", "+"));
				stringBuilder.Append('=');
				stringBuilder.Append(Uri.EscapeDataString(ToSS(item.Value)).Replace("%20", "+"));
				num++;
			}
		}
		return stringBuilder.ToString();
	}

	public static int URLDecode(ref Dictionary<string, object> p, string url)
	{
		if (string.IsNullOrEmpty(url))
		{
			return 0;
		}
		int num = 0;
		string text = url.Substring(url.IndexOf('?') + 1);
		string[] array = text.Split(new char[1] { '&' }, StringSplitOptions.None);
		foreach (string text2 in array)
		{
			if (!string.IsNullOrEmpty(text2))
			{
				int num2 = text2.IndexOf('=');
				string empty = string.Empty;
				string empty2 = string.Empty;
				if (num2 > 0)
				{
					empty = Uri.UnescapeDataString(text2.Substring(0, num2 - 1).Replace("+", "%20"));
					empty2 = Uri.UnescapeDataString(text2.Substring(num2 + 1).Replace("+", "%20"));
				}
				else
				{
					empty = Uri.UnescapeDataString(text2.Replace("+", "%20"));
					empty2 = string.Empty;
				}
				if (!string.IsNullOrEmpty(empty))
				{
					p[empty] = empty2;
					num++;
				}
			}
		}
		return num;
	}

	public static HttpWebRequest CreateNewHttpRequest(string url)
	{
		HttpWebRequest httpWebRequest = null;
		try
		{
			httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
			httpWebRequest.KeepAlive = true;
			if (Timeout > 0)
			{
				httpWebRequest.Timeout = Timeout;
			}
			if (IOTimeout > 0)
			{
				httpWebRequest.ReadWriteTimeout = IOTimeout;
			}
		}
		catch (Exception)
		{
			httpWebRequest = null;
		}
		return httpWebRequest;
	}

	private static int GetRequestWaitTimeMillis()
	{
		return 10;
	}

	private static IEnumerator CreatePostRequestStreamAsync(int seqNo, HttpWebRequest req, int timeout, byte[] data, RequestHandler handler)
	{
		return CreatePostRequestStreamAsync(seqNo, req, timeout, new BIJMemoryBinaryReader(data), handler);
	}

	private static IEnumerator CreatePostRequestStreamAsync(int seqNo, HttpWebRequest req, int timeout, BIJBinaryReader data, RequestHandler handler)
	{
		dbg(string.Format("CreatePostRequestStreamAsync(#{0}) - th={1} Begin", seqNo, Thread.CurrentThread.GetHashCode().ToString()));
		RequestAsync reqAsync = new RequestAsync
		{
			Sync = new ManualResetEvent(false),
			SeqNo = seqNo,
			Request = req,
			RequestData = data
		};
		dbg(string.Format("CreatePostRequestStreamAsync(#{0}) - 1", seqNo));
		reqAsync.Sync.Reset();
		dbg(string.Format("CreatePostRequestStreamAsync(#{0}) - 2", seqNo));
		req.BeginGetRequestStream(GetPostRequestStreamCallback, reqAsync);
		float _start = Time.realtimeSinceStartup;
		while (!reqAsync.Sync.WaitOne(GetRequestWaitTimeMillis()))
		{
			float temp_start = Time.realtimeSinceStartup;
			float span_time = Time.realtimeSinceStartup - _start;
			dbg(string.Format("start:{0} temp:{1} span:{2}", _start, temp_start, span_time));
			if (span_time > (float)(timeout / 1000 * 2))
			{
				HttpAsync httpAsyncTimeout = new HttpAsync(seqNo, req, timeout, true);
				if (handler != null)
				{
					handler(httpAsyncTimeout);
				}
				yield break;
			}
			yield return 0;
		}
		dbg(string.Format("CreatePostRequestStreamAsync(#{0}) - 3", seqNo));
		HttpAsync httpAsync = new HttpAsync(seqNo, req, timeout);
		dbg(string.Format("URL[POST](#{0}) {1} len={2}", httpAsync.SeqNo, req.RequestUri, req.ContentLength));
		dbg(string.Format("CreatePostRequestStreamAsync(#{0}) - 4", seqNo));
		if (handler != null)
		{
			handler(httpAsync);
		}
		dbg(string.Format("CreatePostRequestStreamAsync(#{0}) - th={1} End", seqNo, Thread.CurrentThread.GetHashCode().ToString()));
	}

	private static void GetPostRequestStreamCallback(IAsyncResult asyncResult)
	{
		RequestAsync requestAsync = asyncResult.AsyncState as RequestAsync;
		assert(requestAsync == null, "FATAL GetPostRequestStreamCallback reqAsync is nothing!!");
		ManualResetEvent sync = requestAsync.Sync;
		int seqNo = requestAsync.SeqNo;
		BIJBinaryReader bIJBinaryReader = null;
		Stream stream = null;
		BIJBinaryWriter temp = null;
		dbg(string.Format("GetRequestStreamCallback(#{0}) - th={1} Begin", seqNo, Thread.CurrentThread.GetHashCode().ToString()));
		try
		{
			HttpWebRequest request = requestAsync.Request;
			bIJBinaryReader = requestAsync.RequestData;
			dbg(string.Format("GetPostRequestStreamCallback(#{0}) - 1", seqNo));
			stream = request.EndGetRequestStream(asyncResult);
			temp = Debug_PostDataInit();
			dbg(string.Format("GetPostRequestStreamCallback(#{0}) - 2", seqNo));
			if (bIJBinaryReader != null)
			{
				byte[] array = new byte[4096];
				int num = 0;
				int num2 = 0;
				while ((num = bIJBinaryReader.ReadRaw(array, 0, array.Length)) != 0)
				{
					stream.Write(array, 0, num);
					Debug_PostDataOut(temp, array, num);
					dbg(string.Format("GetRequestStreamCallback(#{0}) - 3-{1} : thunk {2} bytes write", seqNo, num2, num));
					num2++;
				}
			}
		}
		catch (Exception)
		{
		}
		finally
		{
			Debug_PostDataTerm(temp);
			dbg(string.Format("GetPostRequestStreamCallback(#{0}) - 4", seqNo));
			if (stream != null)
			{
				try
				{
					stream.Close();
				}
				catch (Exception)
				{
				}
			}
			dbg(string.Format("GetPostRequestStreamCallback(#{0}) - 5", seqNo));
			if (bIJBinaryReader != null)
			{
				try
				{
					bIJBinaryReader.Close();
				}
				catch (Exception)
				{
				}
			}
			dbg(string.Format("GetPostRequestStreamCallback(#{0}) - 6", seqNo));
			sync.Set();
			dbg(string.Format("GetRequestStreamCallback(#{0}) - th={1} End", seqNo, Thread.CurrentThread.GetHashCode().ToString()));
		}
	}

	private static BIJBinaryWriter Debug_PostDataInit()
	{
		return null;
	}

	private static void Debug_PostDataOut(BIJBinaryWriter temp, byte[] data, int length = 0)
	{
	}

	private static void Debug_PostDataTerm(BIJBinaryWriter temp)
	{
	}

	public static HttpAsync CreateHttpHead(string url, ref Dictionary<string, object> p, int timeout, bool a_useJson = true, bool a_encryption = false)
	{
		dbg("CreateHttpHead - Begin");
		HttpAsync result = ((!a_useJson) ? DoCreateHttpHead(url, ref p, timeout) : DoCreateHttpGetJson(url, ref p, timeout, a_encryption));
		dbg("CreateHttpHead - End");
		return result;
	}

	public static HttpAsync CreateHttpGet(string url, ref Dictionary<string, object> p, int timeout, bool a_useJson = true, bool a_encryption = false)
	{
		dbg("CreateHttpGet - Begin");
		HttpAsync result = ((!a_useJson) ? DoCreateHttpGet(url, ref p, timeout) : DoCreateHttpGetJson(url, ref p, timeout, a_encryption));
		dbg("CreateHttpGet - End");
		return result;
	}

	public static HttpAsync CreateHttpPost(string url, ref Dictionary<string, object> p, int timeout, bool a_useJson = true, bool a_encryption = false)
	{
		dbg("CreateHttpPost - Begin");
		HttpAsync result = ((!a_useJson) ? DoCreateHttpPost(url, ref p, timeout) : DoCreateHttpPostJson(url, ref p, timeout, a_encryption));
		dbg("CreateHttpPost - End");
		return result;
	}

	public static bool CreateHttpPostAsync(string url, ref Dictionary<string, object> p, int timeout, RequestHandler handler, bool a_useJson = true, bool a_encryption = false)
	{
		dbg("CreateHttpPostAsync - Begin");
		bool flag = false;
		try
		{
			flag = ((!a_useJson) ? DoCreateHttpPostAsync(url, ref p, timeout, handler) : DoCreateHttpPostAsyncJson(url, ref p, timeout, handler, a_encryption));
		}
		catch (Exception)
		{
			flag = false;
		}
		dbg("CreateHttpPostAsync - End => " + flag);
		return flag;
	}

	public static HttpAsync CreateHttpMultiPart(string url, ref Dictionary<string, object> p, int timeout, bool a_useJson = true, bool a_encryption = false)
	{
		dbg("CreateHttpMultiPart - Begin");
		HttpAsync result = ((!a_useJson) ? DoCreateHttpMultiPart(url, ref p, timeout) : DoCreateHttpMultiPartJson(url, ref p, timeout, a_encryption));
		dbg("CreateHttpMultiPart - End");
		return result;
	}

	public static bool CreateHttpMultiPartAsync(string url, ref Dictionary<string, object> p, int timeout, RequestHandler handler, bool a_useJson = true, bool a_encryption = false)
	{
		dbg("CreateHttpMultiPartAsync - Begin");
		bool flag = false;
		try
		{
			flag = ((!a_useJson) ? DoCreateHttpMultiPartAsync(url, ref p, timeout, handler) : DoCreateHttpMultiPartAsyncJson(url, ref p, timeout, handler, a_encryption));
		}
		catch (Exception)
		{
			flag = false;
		}
		dbg("CreateHttpMultiPartAsync - End => " + flag);
		return flag;
	}
}
