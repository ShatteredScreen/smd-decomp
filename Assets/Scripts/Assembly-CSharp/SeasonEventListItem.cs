public class SeasonEventListItem : SMVerticalListItem
{
	public SeasonEventSettings serverEventSetting;

	public SMSeasonEventSetting eventData;

	public int[] companionRewardID;

	public int maxStageNum;

	public int clearStageNum;

	public int maxStarNum;

	public int getStarNum;

	public bool nowOpen;

	public UISprite timerFrameSprite;

	public UILabel timerLabel;

	public UIButton enterButton;

	public UILabel enterLabel;

	public UISprite activeFrame;

	public UISprite lockIcon;
}
