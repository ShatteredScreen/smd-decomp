using System;

[Serializable]
public class SsPaletteKeyValue : SsInterpolatable, SsAttrValueInterface
{
	public bool Use;

	public int Page;

	public byte Block;

	public SsPaletteKeyValue()
	{
	}

	public SsPaletteKeyValue(SsPaletteKeyValue r)
	{
		Use = r.Use;
		Page = r.Page;
		Block = r.Block;
	}

	public override string ToString()
	{
		return "Use: " + Use + ", Page: " + Page + ", Block: " + Block;
	}

	public SsAttrValueInterface Clone()
	{
		return new SsPaletteKeyValue(this);
	}

	public SsInterpolatable GetInterpolated(SsCurveParams curve, float time, SsInterpolatable start, SsInterpolatable end, int startTime, int endTime)
	{
		SsPaletteKeyValue ssPaletteKeyValue = new SsPaletteKeyValue();
		return ssPaletteKeyValue.Interpolate(curve, time, start, end, startTime, endTime);
	}

	public SsInterpolatable Interpolate(SsCurveParams curve, float time, SsInterpolatable start_, SsInterpolatable end_, int startTime, int endTime)
	{
		SsPaletteKeyValue ssPaletteKeyValue = (SsPaletteKeyValue)start_;
		SsPaletteKeyValue ssPaletteKeyValue2 = (SsPaletteKeyValue)end_;
		Use = SsInterpolation.Interpolate(curve, time, (!ssPaletteKeyValue.Use) ? 0f : 1f, (!ssPaletteKeyValue2.Use) ? 0f : 1f, startTime, endTime) >= 0.5f;
		Page = SsInterpolation.Interpolate(curve, time, ssPaletteKeyValue.Page, ssPaletteKeyValue2.Page, startTime, endTime);
		Block = (byte)SsInterpolation.Interpolate(curve, time, ssPaletteKeyValue.Block, ssPaletteKeyValue2.Block, startTime, endTime);
		return this;
	}
}
