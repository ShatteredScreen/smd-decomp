using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class GameConnectManager : MonoBehaviour
{
	private static object mInstanceLock = new object();

	private static GameConnectManager mInstance = null;

	private List<ConnectInstance> mItemStack = new List<ConnectInstance>();

	private ConnectInstance mConnectInstance;

	private int mConnectID;

	private bool mIsInitialized;

	public static GameConnectManager Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					mInstance = (GameConnectManager)gameObject.GetComponent(typeof(GameConnectManager));
				}
				return mInstance;
			}
		}
	}

	public bool IsNetworking
	{
		get
		{
			if (mConnectInstance != null || mItemStack.Count > 0)
			{
				return true;
			}
			return false;
		}
	}

	private void RegisterCallbackToServer()
	{
		Server.UserAuthEvent += Server_OnUserAuth;
		Server.NicknameCheckEvent += Server_OnNicknameCheck;
		Server.GetMaintenanceInfoEvent += Server_OnMaintenanceProfile;
		Server.GetGameInfoEvent += Server_OnGameProfile;
		Server.GetServerGameDataEvent += Server_OnGetGameData;
		Server.GetSegmentInfoEvent += Server_OnSegmentProfile;
		Server.GetGiftEvent += Server_OnGetGift;
		Server.GetApplyEvent += Server_OnGetApply;
		Server.GetSupportEvent += Server_OnGetSupport;
		Server.LoginBonusEvent += Server_OnLoginBonus;
		Server.GetRandomUserEvent += Server_OnGetRandomUser;
		ServerIAP.CurrencyCheckEvent += Server_OnGetGemCount;
		ServerIAP.CurrencySpendEvent += Server_OnBuyItem;
		ServerIAP.SubcurrencyCheckEvent += Server_OnGetSupportPurchase;
		Server.AdvGetAllItemEvent += Server_OnAdvGetAllItem;
		Server.AdvGetSaveDataEvent += Server_OnAdvGetGameData;
		Server.AdvCheckMasterTableVerEvent += Server_OnAdvCheckMasterTableVer;
		Server.AdvGetMasterTableDataEvent += Server_OnAdvGetMasterTableData;
		Server.AdvGetMailEvent += Server_OnAdvGetMail;
	}

	private void RemoveCallbackFromServer()
	{
		Server.UserAuthEvent -= Server_OnUserAuth;
		Server.NicknameCheckEvent -= Server_OnNicknameCheck;
		Server.GetMaintenanceInfoEvent -= Server_OnMaintenanceProfile;
		Server.GetGameInfoEvent -= Server_OnGameProfile;
		Server.GetServerGameDataEvent -= Server_OnGetGameData;
		Server.GetSegmentInfoEvent -= Server_OnSegmentProfile;
		Server.GetGiftEvent -= Server_OnGetGift;
		Server.GetApplyEvent -= Server_OnGetApply;
		Server.GetSupportEvent -= Server_OnGetSupport;
		Server.LoginBonusEvent -= Server_OnLoginBonus;
		Server.GetRandomUserEvent -= Server_OnGetRandomUser;
		ServerIAP.CurrencyCheckEvent -= Server_OnGetGemCount;
		ServerIAP.CurrencySpendEvent -= Server_OnBuyItem;
		ServerIAP.SubcurrencyCheckEvent -= Server_OnGetSupportPurchase;
		Server.AdvGetAllItemEvent -= Server_OnAdvGetAllItem;
		Server.AdvGetSaveDataEvent -= Server_OnAdvGetGameData;
		Server.AdvCheckMasterTableVerEvent -= Server_OnAdvCheckMasterTableVer;
		Server.AdvGetMasterTableDataEvent -= Server_OnAdvGetMasterTableData;
		Server.AdvGetMailEvent -= Server_OnAdvGetMail;
	}

	public void Server_OnUserAuth(int a_result, int a_code)
	{
		SimpleResponse(1001, a_result, a_code);
	}

	public void Server_OnNicknameCheck(int a_result, int a_code)
	{
		SimpleResponse(1003, a_result, a_code);
	}

	public void Server_OnMaintenanceProfile(int a_result, int a_code)
	{
		SimpleResponse(1044, a_result, a_code);
	}

	public void Server_OnGameProfile(int a_result, int a_code)
	{
		SimpleResponse(1031, a_result, a_code);
	}

	public void Server_OnSegmentProfile(int a_result, int a_code)
	{
		SimpleResponse(1005, a_result, a_code);
	}

	public void Server_OnSendDeviceToken(int a_result, int a_code)
	{
		SimpleResponse(1006, a_result, a_code);
	}

	public void Server_OnLoginBonus(int a_result, int a_code)
	{
		SimpleResponse(1045, a_result, a_code);
	}

	public void Server_OnGetGameData(int a_result, int a_code, Stream a_stream, int a_version)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(1023);
		if (connectItemFromAPI != null)
		{
			ConnectGetGameData connectGetGameData = connectItemFromAPI as ConnectGetGameData;
			if (connectGetGameData != null)
			{
				connectGetGameData.SetResponseData(a_stream, a_version);
				SetResponse(connectItemFromAPI, a_result, a_code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnGetGift(int a_result, int a_code, Server.MailType a_kind, string a_json)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(1015);
		if (connectItemFromAPI != null)
		{
			ConnectGetGift connectGetGift = connectItemFromAPI as ConnectGetGift;
			if (connectGetGift != null)
			{
				connectGetGift.SetResponseData(a_kind, a_json);
				SetResponse(connectItemFromAPI, a_result, a_code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnGetApply(int a_result, int a_code, Server.MailType a_kind, Stream resStream)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(1016);
		if (!(connectItemFromAPI != null))
		{
			return;
		}
		ConnectGetApply connectGetApply = connectItemFromAPI as ConnectGetApply;
		if (connectGetApply != null)
		{
			string a_json = string.Empty;
			using (StreamReader streamReader = new StreamReader(resStream, Encoding.UTF8))
			{
				a_json = streamReader.ReadToEnd();
			}
			connectGetApply.SetResponseData(a_kind, a_json);
			SetResponse(connectItemFromAPI, a_result, a_code);
		}
		else
		{
			BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
		}
	}

	public void Server_OnGetSupport(int a_result, int a_code, Server.MailType a_kind, Stream resStream)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(1017);
		if (!(connectItemFromAPI != null))
		{
			return;
		}
		ConnectGetSupport connectGetSupport = connectItemFromAPI as ConnectGetSupport;
		if (connectGetSupport != null)
		{
			string a_json = string.Empty;
			using (StreamReader streamReader = new StreamReader(resStream, Encoding.UTF8))
			{
				a_json = streamReader.ReadToEnd();
			}
			connectGetSupport.SetResponseData(a_kind, a_json);
			SetResponse(connectItemFromAPI, a_result, a_code);
		}
		else
		{
			BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
		}
	}

	public void Server_OnGetRandomUser(int a_result, int a_code, string a_json)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(1008);
		if (connectItemFromAPI != null)
		{
			ConnectGetRandomUser connectGetRandomUser = connectItemFromAPI as ConnectGetRandomUser;
			if (connectGetRandomUser != null)
			{
				connectGetRandomUser.SetResponseData(a_json);
				SetResponse(connectItemFromAPI, a_result, a_code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnGetSupportPurchase(int a_result, object ret_obj)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(3);
		if (connectItemFromAPI != null)
		{
			ConnectGetSupportPurchase connectGetSupportPurchase = connectItemFromAPI as ConnectGetSupportPurchase;
			if (connectGetSupportPurchase != null)
			{
				SCCheckResponse sCCheckResponse = ret_obj as SCCheckResponse;
				connectGetSupportPurchase.SetResponseData(sCCheckResponse);
				SetResponse(connectItemFromAPI, a_result, sCCheckResponse.code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnGetGemCount(int a_result, object ret_obj)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(2);
		if (connectItemFromAPI != null)
		{
			ConnectChargeCheck connectChargeCheck = connectItemFromAPI as ConnectChargeCheck;
			if (connectChargeCheck != null)
			{
				MCCheckResponse mCCheckResponse = ret_obj as MCCheckResponse;
				connectChargeCheck.SetResponseData(mCCheckResponse);
				SetResponse(connectItemFromAPI, a_result, mCCheckResponse.code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnBuyItem(int a_result, object ret_obj)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(1);
		if (connectItemFromAPI != null)
		{
			ConnectCurrencySpend connectCurrencySpend = connectItemFromAPI as ConnectCurrencySpend;
			if (connectCurrencySpend != null)
			{
				MCSpendResponse mCSpendResponse = ret_obj as MCSpendResponse;
				connectCurrencySpend.SetResponseData(mCSpendResponse);
				SetResponse(connectItemFromAPI, a_result, mCSpendResponse.code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnAdvGetAllItem(int a_result, int a_code)
	{
		SimpleResponse(2000, a_result, a_code);
	}

	public void Server_OnAdvGetGameData(int a_result, int a_code, Stream a_stream)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(2011);
		if (connectItemFromAPI != null)
		{
			ConnectAdvGetGameData connectAdvGetGameData = connectItemFromAPI as ConnectAdvGetGameData;
			if (connectAdvGetGameData != null)
			{
				connectAdvGetGameData.SetResponseData(a_stream);
				SetResponse(connectItemFromAPI, a_result, a_code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnAdvCheckMasterTableVer(int a_result, int a_code)
	{
		SimpleResponse(2006, a_result, a_code);
	}

	public void Server_OnAdvGetMasterTableData(int a_result, int a_code)
	{
		SimpleResponse(2007, a_result, a_code);
	}

	public void Server_OnAdvGetMail(int a_result, int a_code, Stream a_stream)
	{
		if (!(mConnectInstance != null))
		{
			return;
		}
		ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(2012);
		if (connectItemFromAPI != null)
		{
			ConnectAdvGetMail connectAdvGetMail = connectItemFromAPI as ConnectAdvGetMail;
			if (connectAdvGetMail != null)
			{
				connectAdvGetMail.SetResponseData(a_stream);
				SetResponse(connectItemFromAPI, a_result, a_code);
			}
			else
			{
				BIJLog.E("Error:item type is invalid " + connectItemFromAPI.GetType());
			}
		}
	}

	public void Server_OnAdvGetRewardInfo(int a_result, int a_code)
	{
		SimpleResponse(2008, a_result, a_code);
	}

	public void Server_OnAdvSetReward(int a_result, int a_code)
	{
		SimpleResponse(2009, a_result, a_code);
	}

	private ConnectBase GetConnect(int a_api, GameObject a_root, ConnectParameterBase a_param)
	{
		ConnectBase connectBase = null;
		bool flag = false;
		switch (a_api)
		{
		case 1001:
			connectBase = Util.CreateGameObject("Auth", a_root).AddComponent<ConnectUserAuth>();
			break;
		case 1003:
			connectBase = Util.CreateGameObject("NicknameCheck", a_root).AddComponent<ConnectNicknameCheck>();
			break;
		case 1044:
			connectBase = Util.CreateGameObject("GetMaintenanceInfo", a_root).AddComponent<ConnectMaintenanceProfile>();
			break;
		case 1031:
			connectBase = Util.CreateGameObject("GetGameInfo", a_root).AddComponent<ConnectGameProfile>();
			break;
		case 1023:
			connectBase = Util.CreateGameObject("GetGameData", a_root).AddComponent<ConnectGetGameData>();
			break;
		case 1005:
			connectBase = Util.CreateGameObject("GetSegmentInfo", a_root).AddComponent<ConnectSegmentProfile>();
			break;
		case 1045:
			connectBase = Util.CreateGameObject("LoginBonus", a_root).AddComponent<ConnectLoginBonus>();
			break;
		case 1015:
			connectBase = Util.CreateGameObject("GetGift", a_root).AddComponent<ConnectGetGift>();
			break;
		case 1016:
			connectBase = Util.CreateGameObject("GetApply", a_root).AddComponent<ConnectGetApply>();
			break;
		case 1017:
			connectBase = Util.CreateGameObject("GetSupport", a_root).AddComponent<ConnectGetSupport>();
			break;
		case 1008:
			connectBase = Util.CreateGameObject("GetRandomUser", a_root).AddComponent<ConnectGetRandomUser>();
			break;
		case 2:
			connectBase = Util.CreateGameObject("ChargeCheck", a_root).AddComponent<ConnectChargeCheck>();
			break;
		case 1:
			connectBase = Util.CreateGameObject("CurrencySpend", a_root).AddComponent<ConnectCurrencySpend>();
			break;
		case 3:
			connectBase = Util.CreateGameObject("SubcurrencyCheck", a_root).AddComponent<ConnectGetSupportPurchase>();
			break;
		case 2000:
			connectBase = Util.CreateGameObject("AdvGetAllItem", a_root).AddComponent<ConnectAdvGetAllItem>();
			break;
		case 2011:
			connectBase = Util.CreateGameObject("AdvGetSaveData", a_root).AddComponent<ConnectAdvGetGameData>();
			break;
		case 2006:
			connectBase = Util.CreateGameObject("AdvCheckMasterTableVer", a_root).AddComponent<ConnectAdvCheckMasterTableVersion>();
			break;
		case 2007:
			connectBase = Util.CreateGameObject("AdvGetMasterTableData", a_root).AddComponent<ConnectAdvGetMasterTableData>();
			break;
		case 2012:
			connectBase = Util.CreateGameObject("AdvGetMail", a_root).AddComponent<ConnectAdvGetMail>();
			break;
		case 2008:
			connectBase = Util.CreateGameObject("AdvGetRewardInfo", a_root).AddComponent<ConnectAdvGetRewardInfo>();
			break;
		case 2009:
			connectBase = Util.CreateGameObject("AdvSetReward", a_root).AddComponent<ConnectAdvSetReward>();
			break;
		}
		if (connectBase == null)
		{
			BIJLog.E("Not Implemented:");
		}
		else
		{
			if (!flag)
			{
				connectBase.Init(a_api);
			}
			if (a_param != null)
			{
				connectBase.SetConnectParam(a_param);
			}
		}
		return connectBase;
	}

	private void Awake()
	{
	}

	private void Start()
	{
		OnSetup();
		mIsInitialized = true;
	}

	private void OnDestroy()
	{
		Stop();
	}

	private void OnApplicationQuit()
	{
		Term();
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			OnSuspned();
		}
		else
		{
			OnResume();
		}
	}

	public ConnectInstance StartConnect(int a_api, GameObject a_root)
	{
		return StartConnect(new List<int> { a_api }, a_root, null, false, false, null);
	}

	public ConnectInstance StartConnect(int a_api, GameObject a_root, ConnectInstance.OnFinishedHandler a_callback)
	{
		return StartConnect(new List<int> { a_api }, a_root, null, false, false, a_callback);
	}

	public ConnectInstance StartConnect(int a_api, GameObject a_root, ConnectParameterBase a_param = null, bool a_showAnime = false, bool a_errorDialogWait = false, ConnectInstance.OnFinishedHandler a_callback = null)
	{
		Dictionary<int, ConnectParameterBase> a_params = null;
		if (a_param != null)
		{
			Dictionary<int, ConnectParameterBase> dictionary = new Dictionary<int, ConnectParameterBase>();
			dictionary.Add(a_api, a_param);
			a_params = dictionary;
		}
		return StartConnect(new List<int> { a_api }, a_root, a_params, false, a_errorDialogWait, a_callback);
	}

	public ConnectInstance StartConnect(List<int> a_connectList, GameObject a_root)
	{
		return StartConnect(a_connectList, a_root, null, false, false, null);
	}

	public ConnectInstance StartConnect(List<int> a_connectList, GameObject a_root, ConnectInstance.OnFinishedHandler a_callback)
	{
		return StartConnect(a_connectList, a_root, null, false, false, a_callback);
	}

	public ConnectInstance StartConnect(List<int> a_connectList, GameObject a_root, Dictionary<int, ConnectParameterBase> a_params = null, bool a_showAnime = false, bool a_errorDialogWait = false, ConnectInstance.OnFinishedHandler a_callback = null)
	{
		ConnectInstance connectInstance = null;
		connectInstance = Util.CreateGameObject("ConnectInstance" + mConnectID, base.gameObject).AddComponent<ConnectInstance>();
		List<ConnectBase> list = new List<ConnectBase>();
		for (int i = 0; i < a_connectList.Count; i++)
		{
			int num = a_connectList[i];
			ConnectParameterBase value;
			if (a_params == null || !a_params.TryGetValue(num, out value))
			{
				value = null;
			}
			ConnectBase connect = GetConnect(num, connectInstance.gameObject, value);
			if (connect != null)
			{
				if (connect.IsValidConnectInstance())
				{
					list.Add(connect);
					continue;
				}
				Object.Destroy(connect.gameObject);
				connect = null;
			}
			else
			{
				BIJLog.E("Not Implement!!!:" + num);
			}
		}
		if (list.Count > 0)
		{
			connectInstance.Init(list, a_root, a_errorDialogWait, a_showAnime);
			connectInstance.SetFinishHandler(a_callback);
			mItemStack.Add(connectInstance);
		}
		else
		{
			Object.Destroy(connectInstance.gameObject);
			connectInstance = null;
			a_callback(new List<ConnectResult>(), false, false);
		}
		mConnectID++;
		if (mConnectID > 10000)
		{
			mConnectID = 0;
		}
		return connectInstance;
	}

	public void OnSetup()
	{
		RegisterCallbackToServer();
	}

	public void Term()
	{
		if (mItemStack.Count > 0)
		{
			for (int i = 0; i < mItemStack.Count; i++)
			{
				Object.Destroy(mItemStack[i].gameObject);
				mItemStack[i] = null;
			}
			mItemStack.Clear();
		}
		RemoveCallbackFromServer();
		mIsInitialized = false;
	}

	private void SimpleResponse(int a_api, int a_result, int a_code)
	{
		if (mConnectInstance != null)
		{
			ConnectBase connectItemFromAPI = mConnectInstance.GetConnectItemFromAPI(a_api);
			if (connectItemFromAPI != null)
			{
				SetResponse(connectItemFromAPI, a_result, a_code);
			}
		}
	}

	public void SetResponse(ConnectBase a_item, int a_result, int a_code)
	{
		a_item.SetServerResponse(a_result, a_code);
	}

	public void Update()
	{
		if (!mIsInitialized)
		{
			return;
		}
		if (mConnectInstance != null)
		{
			if (mConnectInstance.EnableDelete)
			{
				mItemStack.Remove(mConnectInstance);
				Object.Destroy(mConnectInstance.gameObject);
				mConnectInstance = null;
			}
		}
		else if (mItemStack.Count > 0)
		{
			mConnectInstance = mItemStack[0];
			mConnectInstance.StartConnect();
		}
	}

	public void Stop()
	{
	}

	public void OnResume()
	{
	}

	public void OnSuspned()
	{
	}
}
