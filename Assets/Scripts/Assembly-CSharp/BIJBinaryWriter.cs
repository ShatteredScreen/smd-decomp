using System;
using System.IO;
using System.Text;

public class BIJBinaryWriter : IDisposable
{
	protected string mPath;

	private BinaryWriter mWriter;

	public string Path
	{
		get
		{
			return mPath;
		}
	}

	protected virtual BinaryWriter Writer
	{
		get
		{
			return mWriter;
		}
		set
		{
			mWriter = value;
		}
	}

	public BIJBinaryWriter()
	{
		mWriter = null;
	}

	public BIJBinaryWriter(BinaryWriter writer)
	{
		mWriter = writer;
	}

	public void Dispose()
	{
		Close();
	}

	public virtual void Close()
	{
		try
		{
			if (mWriter != null)
			{
				mWriter.Close();
			}
		}
		catch (Exception)
		{
		}
		mWriter = null;
	}

	public virtual void Flush()
	{
		if (mWriter != null)
		{
			mWriter.Flush();
		}
	}

	public long Seek(long offset, SeekOrigin origin)
	{
		long result = 0L;
		Stream baseStream = Writer.BaseStream;
		if (baseStream != null)
		{
			result = baseStream.Seek(offset, origin);
		}
		return result;
	}

	public void WriteAll(byte[] bytes)
	{
		Writer.Write(bytes);
		Writer.Flush();
	}

	public void WriteRaw(byte[] bytes, int index, int count)
	{
		Writer.Write(bytes, index, count);
	}

	public void WriteBool(bool v)
	{
		WriteByte((byte)(v ? 1u : 0u));
	}

	public void WriteBoolArray(bool[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteBool(v[i]);
		}
	}

	public void WriteBoolArray_32L(bool[] v)
	{
		WriteBoolArray(v, true);
	}

	public void WriteByte(byte v)
	{
		Writer.Write(v);
	}

	public void WriteByteArray(byte[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteByte(v[i]);
		}
	}

	public void WriteByteArray_32L(byte[] v)
	{
		WriteByteArray(v, true);
	}

	public void WriteShort(short v)
	{
		byte[] array = new byte[2]
		{
			(byte)((uint)(v >> 8) & 0xFFu),
			(byte)((uint)v & 0xFFu)
		};
		WriteByte(array[0]);
		WriteByte(array[1]);
	}

	public void WriteUShort(ushort v)
	{
		byte[] array = new byte[2]
		{
			(byte)((uint)(v >> 8) & 0xFFu),
			(byte)(v & 0xFFu)
		};
		WriteByte(array[0]);
		WriteByte(array[1]);
	}

	public void WriteShortArray(short[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteShort(v[i]);
		}
	}

	public void WriteShortArray_32L(short[] v)
	{
		WriteShortArray(v, true);
	}

	public void WriteInt(int v)
	{
		short[] array = new short[2]
		{
			(short)((v >> 16) & 0xFFFF),
			(short)(v & 0xFFFF)
		};
		WriteShort(array[0]);
		WriteShort(array[1]);
	}

	public void WriteIntArray(int[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteInt(v[i]);
		}
	}

	public void WriteIntArray_32L(int[] v)
	{
		WriteIntArray(v, true);
	}

	public void WriteLong(long v)
	{
		int[] array = new int[2]
		{
			(int)((v >> 32) & 0xFFFFFFFFu),
			(int)(v & 0xFFFFFFFFu)
		};
		WriteInt(array[0]);
		WriteInt(array[1]);
	}

	public void WriteLongArray(long[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteLong(v[i]);
		}
	}

	public void WriteLongArray_32L(long[] v)
	{
		WriteLongArray(v, true);
	}

	public void WriteFloat(float v)
	{
		byte[] bytes = BitConverter.GetBytes(v);
		if (BitConverter.IsLittleEndian)
		{
			Array.Reverse(bytes);
		}
		byte[] array = bytes;
		foreach (byte v2 in array)
		{
			WriteByte(v2);
		}
	}

	public void WriteFloatArray(float[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteFloat(v[i]);
		}
	}

	public void WriteFloatArray_32L(float[] v)
	{
		WriteFloatArray(v, true);
	}

	public void WriteDouble(double v)
	{
		byte[] bytes = BitConverter.GetBytes(v);
		if (BitConverter.IsLittleEndian)
		{
			Array.Reverse(bytes);
		}
		byte[] array = bytes;
		foreach (byte v2 in array)
		{
			WriteByte(v2);
		}
	}

	public void WriteDoubleArray(double[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteDouble(v[i]);
		}
	}

	public void WriteDoubleArray_32L(double[] v)
	{
		WriteDoubleArray(v, true);
	}

	public void WriteUTF(string v, bool isIntLength = false)
	{
		if (string.IsNullOrEmpty(v))
		{
			v = string.Empty;
		}
		byte[] bytes = Encoding.UTF8.GetBytes(v);
		WriteByteArray(bytes, isIntLength);
	}

	public void WriteUTF_32L(string v)
	{
		WriteUTF(v, true);
	}

	public void WriteUTFArray(string[] v, bool isIntLength = false)
	{
		if (isIntLength)
		{
			WriteInt(v.Length);
		}
		else
		{
			WriteUShort((ushort)v.Length);
		}
		for (int i = 0; i < v.Length; i++)
		{
			WriteUTF(v[i], isIntLength);
		}
	}

	public void WriteUTFArray_32L(string[] v)
	{
		WriteUTFArray(v, true);
	}

	public void WriteDateTime(DateTime v)
	{
		string v2 = v.ToUniversalTime().ToString();
		WriteUTF(v2);
	}
}
