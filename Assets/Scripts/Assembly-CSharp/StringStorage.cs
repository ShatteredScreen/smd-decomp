using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class StringStorage
{
	private string[] mStorage;

	private string mOriginal = string.Empty;

	private int mIndex;

	private char mDelimiter = '\n';

	private int mMaxRow;

	private int mMaxPerRow;

	public bool HasCurrent
	{
		get
		{
			return mStorage != null && 0 <= mIndex && mIndex < mStorage.Length;
		}
	}

	public bool HasNext
	{
		get
		{
			return mStorage != null && mIndex < mStorage.Length;
		}
	}

	public bool HasPrev
	{
		get
		{
			return mStorage != null && 0 <= mIndex;
		}
	}

	public bool IsBegin
	{
		get
		{
			return mIndex < 0;
		}
	}

	public bool IsEnd
	{
		get
		{
			return mStorage != null && mStorage.Length <= mIndex;
		}
	}

	public StringStorage(string text, int maxRow = 0, int maxPerRow = 0, char delimiter = '\n')
	{
		Reset(text, maxRow, maxPerRow, delimiter);
	}

	public void Reset(string text, int maxRow = 0, int maxPerRow = 0, char delimiter = '\n')
	{
		mOriginal = text;
		mDelimiter = delimiter;
		mMaxRow = maxRow;
		mMaxPerRow = maxPerRow;
		SetupStorage(text);
	}

	public string Current()
	{
		if (!HasCurrent)
		{
			return string.Empty;
		}
		int num = mIndex;
		int num2 = Mathf.Min(mIndex + mMaxRow, mStorage.Length);
		string text = string.Empty;
		for (int i = num; i < num2; i++)
		{
			text += mStorage[i];
			if (i != num2 - 1)
			{
				text += "\n";
			}
		}
		return text;
	}

	public string Next()
	{
		if (IsBegin)
		{
			First();
		}
		string result = Current();
		Step(mMaxRow);
		return result;
	}

	public string Prev()
	{
		if (IsEnd)
		{
			Last();
		}
		string result = Current();
		Step(-mMaxRow);
		return result;
	}

	public void Step(int num)
	{
		if (mStorage != null)
		{
			mIndex += num;
			mIndex = Mathf.Max(mIndex, -1);
			mIndex = Mathf.Min(mIndex, mStorage.Length);
		}
	}

	public void First()
	{
		mIndex = 0;
	}

	public void Last()
	{
		if (mStorage != null)
		{
			if (mStorage.Length % mMaxRow == 0)
			{
				mIndex = mStorage.Length - mMaxRow;
			}
			else
			{
				mIndex = mMaxRow * (mStorage.Length / mMaxRow);
			}
		}
	}

	private void SetupStorage(string text)
	{
		string[] array = text.Split(mDelimiter);
		if (mMaxPerRow <= 0)
		{
			mStorage = array;
			mIndex = 0;
			return;
		}
		List<string> list = new List<string>();
		for (int i = 0; i < array.Length; i++)
		{
			int num = Mathf.Max(1, Mathf.CeilToInt((float)array[i].Length / (float)mMaxPerRow));
			for (int j = 0; j < num; j++)
			{
				int num2 = j * mMaxPerRow;
				int length = Mathf.Min(mMaxPerRow, array[i].Length - num2);
				list.Add(array[i].Substring(num2, length));
			}
		}
		mStorage = list.ToArray();
		if (mMaxRow <= 0)
		{
			mMaxRow = mStorage.Length;
		}
		First();
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("Original:\n" + mOriginal + "\n");
		stringBuilder.Append("Length:" + mStorage.Length + "\n");
		stringBuilder.Append("Index:" + mIndex + "\n");
		stringBuilder.Append("HasNext:" + HasNext + "\n");
		stringBuilder.Append("HasPrev:" + HasPrev + "\n");
		stringBuilder.Append("HasCurrent:" + HasCurrent + "\n");
		stringBuilder.Append("Delimiter:" + mDelimiter + "\n");
		stringBuilder.Append("MaxRow:" + mMaxRow + "\n");
		stringBuilder.Append("MaxPerRow:" + mMaxPerRow + "\n");
		return stringBuilder.ToString();
	}
}
