using System.Collections.Generic;

namespace live2d.framework
{
	public class L2DBaseModel
	{
		protected ALive2DModel live2DModel;

		protected L2DModelMatrix modelMatrix;

		protected Dictionary<string, AMotion> expressions;

		protected Dictionary<string, AMotion> motions;

		protected L2DMotionManager mainMotionManager;

		protected L2DMotionManager expressionManager;

		protected L2DEyeBlink eyeBlink;

		protected L2DPhysics physics;

		protected L2DPose pose;

		protected bool debugMode;

		protected bool initialized;

		protected bool updating;

		protected bool lipSync;

		protected float lipSyncValue;

		protected float accelX;

		protected float accelY;

		protected float accelZ;

		protected float dragX;

		protected float dragY;

		protected long startTimeMSec;

		public L2DBaseModel()
		{
			mainMotionManager = new L2DMotionManager();
			expressionManager = new L2DMotionManager();
			motions = new Dictionary<string, AMotion>();
			expressions = new Dictionary<string, AMotion>();
		}

		public L2DModelMatrix getModelMatrix()
		{
			return modelMatrix;
		}

		public bool isInitialized()
		{
			return initialized;
		}

		public void setInitialized(bool v)
		{
			initialized = v;
		}

		public bool isUpdating()
		{
			return updating;
		}

		public void setUpdating(bool v)
		{
			updating = v;
		}

		public ALive2DModel getLive2DModel()
		{
			return live2DModel;
		}

		public void setLipSync(bool v)
		{
			lipSync = v;
		}

		public void setLipSyncValue(float v)
		{
			lipSyncValue = v;
		}

		public void setAccel(float x, float y, float z)
		{
			accelX = x;
			accelY = y;
			accelZ = z;
		}

		public void setDrag(float x, float y)
		{
			dragX = x;
			dragY = y;
		}

		public MotionQueueManager getMainMotionManager()
		{
			return mainMotionManager;
		}

		public MotionQueueManager getExpressionManager()
		{
			return expressionManager;
		}

		public void loadModelData(string path)
		{
			IPlatformManager platformManager = Live2DFramework.getPlatformManager();
			if (debugMode)
			{
				platformManager.log("Load model : " + path);
			}
			live2DModel = platformManager.loadLive2DModel(path);
			live2DModel.saveParam();
			if (Live2D.getError() != 0)
			{
				platformManager.log("Error : Failed to loadModelData().");
				return;
			}
			float canvasWidth = live2DModel.getCanvasWidth();
			float canvasHeight = live2DModel.getCanvasHeight();
			modelMatrix = new L2DModelMatrix(canvasWidth, canvasHeight);
			if (canvasWidth > canvasHeight)
			{
				modelMatrix.setWidth(2f);
			}
			else
			{
				modelMatrix.setHeight(2f);
			}
			modelMatrix.setCenterPosition(0f, 0f);
		}

		public void loadTexture(int no, string path)
		{
			IPlatformManager platformManager = Live2DFramework.getPlatformManager();
			if (debugMode)
			{
				platformManager.log("Load Texture : " + path);
			}
			platformManager.loadTexture(live2DModel, no, path);
		}

		public AMotion loadMotion(string name, string path)
		{
			IPlatformManager platformManager = Live2DFramework.getPlatformManager();
			if (debugMode)
			{
				platformManager.log("Load Motion : " + path);
			}
			Live2DMotion live2DMotion = null;
			byte[] str = platformManager.loadBytes(path);
			live2DMotion = Live2DMotion.loadMotion(str);
			if (name != null)
			{
				motions.Add(name, live2DMotion);
			}
			return live2DMotion;
		}

		public void loadExpression(string name, string path)
		{
			IPlatformManager platformManager = Live2DFramework.getPlatformManager();
			if (debugMode)
			{
				platformManager.log("Load Expression : " + path);
			}
			expressions.Add(name, L2DExpressionMotion.loadJson(platformManager.loadBytes(path)));
		}

		public void loadPose(string path)
		{
			IPlatformManager platformManager = Live2DFramework.getPlatformManager();
			if (debugMode)
			{
				platformManager.log("Load Pose : " + path);
			}
			pose = L2DPose.load(platformManager.loadBytes(path));
		}

		public void loadPhysics(string path)
		{
			IPlatformManager platformManager = Live2DFramework.getPlatformManager();
			if (debugMode)
			{
				platformManager.log("Load Physics : " + path);
			}
			physics = L2DPhysics.load(platformManager.loadBytes(path));
		}

		public bool getSimpleRect(string drawID, out float left, out float right, out float top, out float bottom)
		{
			int drawDataIndex = live2DModel.getDrawDataIndex(drawID);
			if (drawDataIndex < 0)
			{
				left = 0f;
				right = 0f;
				top = 0f;
				bottom = 0f;
				return false;
			}
			float[] transformedPoints = live2DModel.getTransformedPoints(drawDataIndex);
			float num = live2DModel.getCanvasWidth();
			float num2 = 0f;
			float num3 = live2DModel.getCanvasHeight();
			float num4 = 0f;
			for (int i = 0; i < transformedPoints.Length; i += 2)
			{
				float num5 = transformedPoints[i];
				float num6 = transformedPoints[i + 1];
				if (num5 < num)
				{
					num = num5;
				}
				if (num5 > num2)
				{
					num2 = num5;
				}
				if (num6 < num3)
				{
					num3 = num6;
				}
				if (num6 > num4)
				{
					num4 = num6;
				}
			}
			left = num;
			right = num2;
			top = num3;
			bottom = num4;
			return true;
		}

		public bool hitTestSimple(string drawID, float testX, float testY)
		{
			float left = 0f;
			float right = 0f;
			float top = 0f;
			float bottom = 0f;
			if (!getSimpleRect(drawID, out left, out right, out top, out bottom))
			{
				return false;
			}
			float num = modelMatrix.invertTransformX(testX);
			float num2 = modelMatrix.invertTransformY(testY);
			return left <= num && num <= right && top <= num2 && num2 <= bottom;
		}
	}
}
