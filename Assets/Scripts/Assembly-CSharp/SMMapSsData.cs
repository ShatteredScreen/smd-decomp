using System.Collections.Generic;
using UnityEngine;

public class SMMapSsData
{
	public const float CHANGEMAP_ORDER = 1000f;

	public Dictionary<string, SsMapEntity> MapAccessoryPosList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapEntityPosList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapRBPosList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapEmitterPosList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapMoveObjectPosList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapStrayUserList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapSilhouetteList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapLineList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapSignList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapERBPosList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapMaskList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapChangeList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapEpisodeList = new Dictionary<string, SsMapEntity>();

	public SMMapRoute<float> MapRoute = new SMMapRoute<float>();

	public Dictionary<SMMapRoute<float>.RouteNode, int> MapRouteMapIndex = new Dictionary<SMMapRoute<float>.RouteNode, int>();

	public Dictionary<int, Vector3> MapScaleList = new Dictionary<int, Vector3>();

	public Dictionary<SMMapRoute<float>.RouteNode, int> MapLineRouteMapIndex = new Dictionary<SMMapRoute<float>.RouteNode, int>();

	public SMMapRoute<float> MapLineRoute = new SMMapRoute<float>();

	private int StageNum;

	private int RBNum;

	private int LineNum;

	public Dictionary<string, SsMapEntity> MapStageList = new Dictionary<string, SsMapEntity>();

	public Dictionary<string, SsMapEntity> MapPointList = new Dictionary<string, SsMapEntity>();

	private float mMapSize = 1136f;

	public void UpdateDisplayMapAccessoryDataByLayer(string a_layerName, bool a_flg)
	{
		string text = string.Empty;
		foreach (KeyValuePair<string, SsMapEntity> mapAccessoryPos in MapAccessoryPosList)
		{
			if (mapAccessoryPos.Value.AnimeKey.CompareTo(a_layerName) == 0)
			{
				text = mapAccessoryPos.Key;
				break;
			}
		}
		if (!string.IsNullOrEmpty(text))
		{
			SsMapEntity value = MapAccessoryPosList[text];
			value.EnableDisplay = a_flg;
			MapAccessoryPosList[text] = value;
		}
	}

	public void UpdateDisplaySilhouetteByLayer(string a_layerName, bool a_flg)
	{
		string text = string.Empty;
		foreach (KeyValuePair<string, SsMapEntity> mapSilhouette in MapSilhouetteList)
		{
			if (mapSilhouette.Value.AnimeKey.CompareTo(a_layerName) == 0)
			{
				text = mapSilhouette.Key;
				break;
			}
		}
		if (!string.IsNullOrEmpty(text))
		{
			SsMapEntity value = MapSilhouetteList[text];
			value.EnableDisplay = a_flg;
			MapSilhouetteList[text] = value;
		}
	}

	public void UpdateDisplayEpisodeSilhouetteByIndex(int a_index, bool a_flg)
	{
		string text = string.Empty;
		foreach (KeyValuePair<string, SsMapEntity> mapEpisode in MapEpisodeList)
		{
			if (mapEpisode.Value.AnimeKey.CompareTo(a_index.ToString()) == 0)
			{
				text = mapEpisode.Key;
				break;
			}
		}
		if (!string.IsNullOrEmpty(text))
		{
			SsMapEntity value = MapEpisodeList[text];
			value.EnableDisplay = a_flg;
			MapEpisodeList[text] = value;
		}
	}

	public List<SsMapEntity> GetAccessoryListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapAccessoryPos in MapAccessoryPosList)
		{
			if (mapAccessoryPos.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapAccessoryPos.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapEntityListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapEntityPos in MapEntityPosList)
		{
			if (mapEntityPos.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapEntityPos.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapRBListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapRBPos in MapRBPosList)
		{
			if (mapRBPos.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapRBPos.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapERBListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapERBPos in MapERBPosList)
		{
			if (mapERBPos.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapERBPos.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapChangeListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapChange in MapChangeList)
		{
			if (mapChange.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapChange.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapEmitterListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapEmitterPos in MapEmitterPosList)
		{
			if (mapEmitterPos.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapEmitterPos.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapMoveObjectListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapMoveObjectPos in MapMoveObjectPosList)
		{
			if (mapMoveObjectPos.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapMoveObjectPos.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapStrayUserListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapStrayUser in MapStrayUserList)
		{
			if (mapStrayUser.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapStrayUser.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapSilhouetteListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapSilhouette in MapSilhouetteList)
		{
			if (mapSilhouette.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapSilhouette.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapLineListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapLine in MapLineList)
		{
			if (mapLine.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapLine.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapSignListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapSign in MapSignList)
		{
			if (mapSign.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapSign.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapMaskListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapMask in MapMaskList)
		{
			if (mapMask.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapMask.Value);
			}
		}
		return list;
	}

	public List<SsMapEntity> GetMapEpisodeListInMapIndex(int a_mapIndex)
	{
		List<SsMapEntity> list = new List<SsMapEntity>();
		foreach (KeyValuePair<string, SsMapEntity> mapEpisode in MapEpisodeList)
		{
			if (mapEpisode.Value.MapAtlasIndex == a_mapIndex)
			{
				list.Add(mapEpisode.Value);
			}
		}
		return list;
	}

	public int GetMapIndex(int a_stageno)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stageno, out a_main, out a_sub);
		int value = 0;
		float routeOrder = Def.GetRouteOrder(a_main, a_sub, a_sub != 0);
		SMMapRoute<float>.RouteNode a_node = null;
		bool flag = a_sub == 0;
		if (!flag)
		{
			float routeOrder2 = Def.GetRouteOrder(a_main, 0f);
			a_node = MapRoute.FindOrder(routeOrder2);
		}
		SMMapRoute<float>.RouteNode routeNode = MapRoute.FindOrder(routeOrder, a_node, flag);
		if (routeNode != null && MapRouteMapIndex.TryGetValue(routeNode, out value))
		{
			return value;
		}
		return 0;
	}

	public List<SMMapRoute<float>.RouteNode> GetStageUntilNextRB(int a_currentStage, int a_nextRB)
	{
		List<SMMapRoute<float>.RouteNode> list = new List<SMMapRoute<float>.RouteNode>();
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_currentStage, out a_main, out a_sub);
		int a_main2;
		int a_sub2;
		Def.SplitStageNo(a_nextRB, out a_main2, out a_sub2);
		SMMapRoute<float>.RouteNode routeNode = MapRoute.FindOrder(Def.GetRouteOrder(a_main, a_sub));
		if (routeNode == null)
		{
			return list;
		}
		SMMapRoute<float>.RouteNode routeNode2 = MapRoute.FindContain("RBP" + a_main2, routeNode, true, false);
		if (routeNode2 == null)
		{
			return list;
		}
		SMMapRoute<float>.RouteNode routeNode3 = null;
		SMMapRoute<float>.RouteNode routeNode4 = null;
		SMMapRoute<float>.RouteNode routeNode5 = null;
		SMMapRoute<float>.RouteNode routeNode6 = null;
		routeNode3 = routeNode;
		routeNode4 = null;
		while (routeNode3 != null)
		{
			routeNode4 = routeNode3;
			if (routeNode4.mIsStage && !list.Contains(routeNode4))
			{
				list.Add(routeNode4);
			}
			if (routeNode3.NextSub != null)
			{
				routeNode5 = routeNode3.NextSub;
				routeNode6 = null;
				while (routeNode5 != null)
				{
					routeNode6 = routeNode5;
					if (routeNode6.mIsStage && !list.Contains(routeNode6))
					{
						list.Add(routeNode6);
					}
					routeNode5 = routeNode6.NextSub;
				}
			}
			if (routeNode4.NextMain == routeNode2)
			{
				break;
			}
			routeNode3 = routeNode4.NextMain;
		}
		return list;
	}

	public SMMapRoute<float>.RouteNode GetStage(int a_stage, bool a_enableSub = false)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stage, out a_main, out a_sub);
		bool flag = true;
		if (a_enableSub)
		{
			flag = a_sub == 0;
		}
		SMMapRoute<float>.RouteNode routeNode = MapRoute.FindOrder(Def.GetRouteOrder(a_main, 0f));
		SMMapRoute<float>.RouteNode routeNode2 = null;
		routeNode2 = ((!flag) ? MapRoute.FindOrder(Def.GetRouteOrder(a_main, a_sub, true), routeNode, false) : routeNode);
		if (routeNode2 == null)
		{
			return null;
		}
		return routeNode2;
	}

	public List<SMMapRoute<float>.RouteNode> GetStages(int a_currentStage, int a_lastStage, bool a_include)
	{
		List<SMMapRoute<float>.RouteNode> list = new List<SMMapRoute<float>.RouteNode>();
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_currentStage, out a_main, out a_sub);
		int a_main2;
		int a_sub2;
		Def.SplitStageNo(a_lastStage, out a_main2, out a_sub2);
		SMMapRoute<float>.RouteNode routeNode = MapRoute.FindOrder(Def.GetRouteOrder(a_main, a_sub));
		if (routeNode == null)
		{
			return list;
		}
		SMMapRoute<float>.RouteNode routeNode2 = MapRoute.FindOrder(Def.GetRouteOrder(a_main2, a_sub2));
		if (routeNode2 == null)
		{
			return list;
		}
		if (routeNode == routeNode2)
		{
			return list;
		}
		SMMapRoute<float>.RouteNode routeNode3 = null;
		SMMapRoute<float>.RouteNode routeNode4 = null;
		SMMapRoute<float>.RouteNode routeNode5 = null;
		SMMapRoute<float>.RouteNode routeNode6 = null;
		routeNode3 = routeNode;
		routeNode4 = null;
		while (routeNode3 != null)
		{
			routeNode4 = routeNode3;
			if (routeNode4.mIsStage && !list.Contains(routeNode4))
			{
				list.Add(routeNode4);
			}
			if (routeNode3.NextSub != null)
			{
				routeNode5 = routeNode3.NextSub;
				routeNode6 = null;
				while (routeNode5 != null)
				{
					routeNode6 = routeNode5;
					if (routeNode6.mIsStage && !list.Contains(routeNode6))
					{
						list.Add(routeNode6);
					}
					routeNode5 = routeNode6.NextSub;
				}
			}
			if (routeNode4.NextMain == routeNode2)
			{
				if (a_include)
				{
					list.Add(routeNode4.NextMain);
				}
				break;
			}
			routeNode3 = routeNode4.NextMain;
		}
		return list;
	}

	public SMMapRoute<float>.RouteNode GetLine(int a_stage, bool a_enableSub = false)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stage, out a_main, out a_sub);
		bool flag = true;
		if (a_enableSub)
		{
			flag = a_sub == 0;
		}
		SMMapRoute<float>.RouteNode routeNode = MapLineRoute.FindOrder(Def.GetRouteOrder(a_main, a_sub));
		SMMapRoute<float>.RouteNode routeNode2 = null;
		routeNode2 = ((!flag) ? MapLineRoute.FindOrder(Def.GetRouteOrder(a_main, a_sub, !flag), routeNode, flag) : routeNode);
		if (routeNode2 == null)
		{
			return null;
		}
		return routeNode2;
	}

	public List<SMMapRoute<float>.RouteNode> GetLines(int a_currentStage, int a_lastStage, bool a_include)
	{
		List<SMMapRoute<float>.RouteNode> list = new List<SMMapRoute<float>.RouteNode>();
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_currentStage, out a_main, out a_sub);
		int a_main2;
		int a_sub2;
		Def.SplitStageNo(a_lastStage, out a_main2, out a_sub2);
		SMMapRoute<float>.RouteNode routeNode = MapLineRoute.FindOrder(Def.GetRouteOrder(a_main, a_sub));
		if (routeNode == null)
		{
			return list;
		}
		SMMapRoute<float>.RouteNode routeNode2 = MapLineRoute.FindOrder(Def.GetRouteOrder(a_main2, a_sub2));
		if (routeNode2 == null)
		{
			return list;
		}
		if (routeNode == routeNode2)
		{
			return list;
		}
		SMMapRoute<float>.RouteNode routeNode3 = null;
		SMMapRoute<float>.RouteNode routeNode4 = null;
		SMMapRoute<float>.RouteNode routeNode5 = null;
		SMMapRoute<float>.RouteNode routeNode6 = null;
		routeNode3 = routeNode;
		routeNode4 = null;
		while (routeNode3 != null)
		{
			routeNode4 = routeNode3;
			if (routeNode4.Name.Contains("Line") && !list.Contains(routeNode4))
			{
				list.Add(routeNode4);
			}
			if (routeNode3.NextSub != null)
			{
				routeNode5 = routeNode3.NextSub;
				routeNode6 = null;
				while (routeNode5 != null)
				{
					routeNode6 = routeNode5;
					if (routeNode6.Name.Contains("Line") && !list.Contains(routeNode6))
					{
						list.Add(routeNode6);
					}
					routeNode5 = routeNode6.NextSub;
				}
			}
			if (routeNode4.NextMain == routeNode2)
			{
				if (a_include)
				{
					list.Add(routeNode4.NextMain);
				}
				break;
			}
			routeNode3 = routeNode4.NextMain;
		}
		return list;
	}

	public void MapSize(float mapsize)
	{
		mMapSize = mapsize;
	}

	public void Init()
	{
		MapRoute = new SMMapRoute<float>();
		MapLineRoute = new SMMapRoute<float>();
		RBNum = 0;
		StageNum = 0;
		LineNum = 0;
		MapRoute.InsertMain("Stage1", 1f, true);
		MapLineRoute.InsertMain("Line1", 1f, false);
	}

	public void Load(int map_num, Def.SERIES a_series)
	{
		Init();
		string arg = Def.SeriesPrefix[a_series];
		for (int i = 0; i < map_num; i++)
		{
			string text = string.Format("MAP_PAGE_{0}_ANIMATION_{1}", arg, i + 1);
			LoadAnimation(text, i, a_series);
			ResourceManager.UnloadSsAnimation(text);
		}
		if (MapRoute != null)
		{
			MapRoute.Print("MapRoute");
		}
		if (MapLineRoute != null)
		{
			MapLineRoute.Print("MapLine");
		}
	}

	public void Load(int map_num, Def.SERIES a_series, string a_animeKey)
	{
		Init();
		for (int i = 0; i < map_num; i++)
		{
			string text = a_animeKey + "_" + (i + 1);
			LoadAnimation(text, i, a_series);
			ResourceManager.UnloadSsAnimation(text);
		}
		if (MapRoute != null)
		{
			MapRoute.Print("MapRoute");
		}
		if (MapLineRoute != null)
		{
			MapLineRoute.Print("MapLine");
		}
	}

	public void LoadPostProcess()
	{
		SMMapRoute<float>.RouteNode routeNode = MapRoute.Root;
		SMMapRoute<float>.RouteNode routeNode2 = null;
		while (routeNode != null)
		{
			routeNode2 = routeNode;
			if (routeNode.mIsStage)
			{
				int num = (int)routeNode.Value;
				if (StageNum < num)
				{
					StageNum = num;
				}
			}
			if (routeNode.Name.Contains("RBP"))
			{
				int num2 = (int)routeNode.Value;
				if (RBNum < num2)
				{
					RBNum = num2;
				}
			}
			routeNode = routeNode.NextMain;
		}
		routeNode = MapLineRoute.Root;
		routeNode2 = null;
		while (routeNode != null)
		{
			routeNode2 = routeNode;
			if (routeNode.Name.Contains("Line"))
			{
				int num3 = (int)routeNode.Value;
				if (LineNum < num3)
				{
					LineNum = num3;
				}
			}
			routeNode = routeNode.NextMain;
		}
	}

	public void LoadAnimation(string a_key, int a_mapIndex, Def.SERIES a_series)
	{
		SsAnimation ssAnime = ResourceManager.LoadSsAnimation(a_key).SsAnime;
		SsPartRes[] partList = ssAnime.PartList;
		ParseAnimation(partList, a_mapIndex);
		LoadPostProcess();
	}

	public void ParseAnimation(SsPartRes[] parts, int a_mapIndex)
	{
		float mAP_BUTTON_Z = Def.MAP_BUTTON_Z;
		int num = 0;
		float num2 = 0f;
		int num3 = 0;
		for (int i = 0; i < parts.Length; i++)
		{
			if (parts[i].IsRoot)
			{
				continue;
			}
			if (parts[i].Name == "map")
			{
				Vector3 value = new Vector3(parts[i].ScaleX(0), parts[i].ScaleY(0), 1f);
				MapScaleList.Add(a_mapIndex, value);
				continue;
			}
			num2 = mAP_BUTTON_Z - 0.001f * (float)num;
			string[] array = parts[i].Name.Split('_');
			switch (array[0])
			{
			case "cp":
			case "cpoint":
			{
				float num26 = (float)StageNum + float.Parse(array[1]);
				float a_x2 = parts[i].PosX(0);
				float a_y2 = 0f - parts[i].PosY(0);
				MapRoute.InsertMain("CPM" + num26, num26, false, null, a_x2, a_y2, num2, (float)a_mapIndex * mMapSize);
				break;
			}
			case "cps":
			case "cpoints":
			{
				string[] array4 = array[1].Split('-');
				float num17 = (float)StageNum + float.Parse(array4[0]) + 0.01f * float.Parse(array4[1]);
				float a_x = parts[i].PosX(0);
				float a_y = 0f - parts[i].PosY(0);
				MapRoute.InsertMain("CPS" + num17, num17, false, null, a_x, a_y, num2, (float)a_mapIndex * mMapSize);
				break;
			}
			case "rbp":
			{
				string[] array2 = array[1].Split('-');
				float num6 = float.Parse(array2[0]);
				float num7 = 0f;
				float num8 = parts[i].PosX(0);
				float num9 = 0f - parts[i].PosY(0);
				float x4 = parts[i].ScaleX(0);
				float y4 = parts[i].ScaleY(0);
				float angle = parts[i].Angle(0);
				float num10 = 0f;
				if (array2.Length == 2)
				{
					num7 = float.Parse(array2[1]);
					num10 = Def.GetRouteOrder((float)StageNum + num6, num7, true);
					MapRoute.InsertSub("RBP" + num10, num10, false, null, num8, num9, num2, (float)a_mapIndex * mMapSize);
				}
				else
				{
					num10 = (float)StageNum + float.Parse(array[1]);
					MapRoute.InsertMain("RBP" + num10, num10, false, null, num8, num9, num2, (float)a_mapIndex * mMapSize);
				}
				SsMapEntity value4 = default(SsMapEntity);
				value4.Name = parts[i].Name;
				value4.AnimeKey = num10.ToString();
				value4.Position = new Vector3(num8, num9, num2);
				value4.Scale = new Vector3(x4, y4, 1f);
				value4.MapAtlasIndex = a_mapIndex;
				value4.MapOffsetCounter = num;
				value4.Angle = angle;
				value4.EnableDisplay = false;
				MapRBPosList.Add(value4.Name + "_" + a_mapIndex, value4);
				num++;
				break;
			}
			case "erbp":
			{
				string[] array8 = array[1].Split('-');
				float num31 = float.Parse(array8[0]);
				float num32 = 0f;
				float x20 = parts[i].PosX(0);
				float y20 = 0f - parts[i].PosY(0);
				float x21 = parts[i].ScaleX(0);
				float y21 = parts[i].ScaleY(0);
				float angle5 = parts[i].Angle(0);
				float num33 = 0f;
				if (array8.Length == 2)
				{
					num32 = float.Parse(array8[1]);
					num33 = Def.GetRouteOrder((float)StageNum + num31, num32, true);
				}
				else
				{
					num33 = (float)StageNum + float.Parse(array[1]);
				}
				SsMapEntity value16 = default(SsMapEntity);
				value16.Name = parts[i].Name;
				value16.AnimeKey = num33.ToString();
				value16.Position = new Vector3(x20, y20, num2);
				value16.Scale = new Vector3(x21, y21, 1f);
				value16.MapAtlasIndex = a_mapIndex;
				value16.MapOffsetCounter = num;
				value16.Angle = angle5;
				value16.EnableDisplay = false;
				MapERBPosList.Add(value16.Name + "_" + a_mapIndex, value16);
				num++;
				break;
			}
			case "changemap":
			{
				float num35 = parts[i].PosX(0);
				float num36 = 0f - parts[i].PosY(0);
				float x24 = parts[i].ScaleX(0);
				float y24 = parts[i].ScaleY(0);
				float angle6 = parts[i].Angle(0);
				float num37 = 1000f;
				MapRoute.InsertMain("RBP" + num37, num37, false, null, num35, num36, num2, (float)a_mapIndex * mMapSize);
				SsMapEntity value18 = default(SsMapEntity);
				value18.Name = parts[i].Name;
				value18.AnimeKey = num37.ToString();
				value18.Position = new Vector3(num35, num36, num2);
				value18.Scale = new Vector3(x24, y24, 1f);
				value18.MapAtlasIndex = a_mapIndex;
				value18.MapOffsetCounter = num;
				value18.Angle = angle6;
				value18.EnableDisplay = false;
				MapChangeList.Add(value18.Name + "_" + a_mapIndex, value18);
				num++;
				break;
			}
			case "stage":
			{
				string[] array3 = array[1].Split('-');
				float num11 = float.Parse(array3[0]);
				float num12 = 0f;
				if (array3.Length == 2)
				{
					num12 = float.Parse(array3[1]);
				}
				float routeOrder = Def.GetRouteOrder((float)StageNum + num11, num12);
				float num13 = parts[i].PosX(0);
				float num14 = 0f - parts[i].PosY(0);
				SMMapRoute<float>.RouteNode routeNode;
				if ((int)routeOrder != 1)
				{
					routeNode = MapRoute.InsertMain("Stage" + routeOrder, routeOrder, true, null, num13, num14, num2, (float)a_mapIndex * mMapSize);
				}
				else
				{
					routeNode = MapRoute.Find("Stage1");
					routeNode.SetPosition(new Vector3(num13, num14, num2));
					routeNode.SetRoutePosition(new Vector3(num13, num14 + (float)a_mapIndex * mMapSize, num2));
				}
				if (!MapRouteMapIndex.ContainsKey(routeNode))
				{
					MapRouteMapIndex.Add(routeNode, a_mapIndex);
				}
				num++;
				SsMapEntity value6 = default(SsMapEntity);
				value6.Name = parts[i].Name;
				value6.AnimeKey = routeOrder.ToString();
				value6.Position = new Vector3(num13, num14, num2);
				value6.MapAtlasIndex = a_mapIndex;
				value6.MapOffsetCounter = num;
				value6.Start = ((int)num11).ToString();
				value6.End = ((int)num12).ToString();
				MapStageList.Add(value6.Name + "_" + a_mapIndex, value6);
				break;
			}
			case "sub":
			{
				string[] array7 = array[1].Split('-');
				float routeOrder3 = Def.GetRouteOrder((float)StageNum + float.Parse(array7[0]), float.Parse(array7[1]), true);
				float num28 = parts[i].PosX(0);
				float num29 = 0f - parts[i].PosY(0);
				float routeOrder4 = Def.GetRouteOrder((float)StageNum + float.Parse(array7[0]), 0f);
				SMMapRoute<float>.RouteNode routeNode4 = MapRoute.Find("Stage" + routeOrder4);
				if (routeNode4 != null)
				{
					SMMapRoute<float>.RouteNode key = MapRoute.InsertSub("Stage" + routeOrder3, routeOrder3, true, routeNode4, num28, num29, num2, (float)a_mapIndex * mMapSize);
					if (!MapRouteMapIndex.ContainsKey(key))
					{
						MapRouteMapIndex.Add(key, a_mapIndex);
					}
					num++;
				}
				SsMapEntity value14 = default(SsMapEntity);
				value14.Name = parts[i].Name;
				value14.AnimeKey = routeOrder3.ToString();
				value14.Position = new Vector3(num28, num29, num2);
				value14.MapAtlasIndex = a_mapIndex;
				value14.MapOffsetCounter = num;
				MapStageList.Add(value14.Name + "_" + a_mapIndex, value14);
				break;
			}
			case "accessory":
			{
				string text11 = string.Empty;
				for (int num38 = 1; num38 < array.Length; num38++)
				{
					text11 = text11 + array[num38] + "_";
				}
				text11 = text11.TrimEnd('_');
				float x25 = parts[i].PosX(0);
				float y25 = 0f - parts[i].PosY(0);
				float x26 = parts[i].ScaleX(0);
				float y26 = parts[i].ScaleY(0);
				AccessoryData accessoryData = SingletonMonoBehaviour<GameMain>.Instance.GetAccessoryData(text11);
				if (accessoryData == null)
				{
					BIJLog.E("NO ACCESSORY DATA!! : " + text11 + " map : " + (a_mapIndex + 1));
					break;
				}
				SsMapEntity value19 = default(SsMapEntity);
				value19.Name = parts[i].Name;
				value19.AnimeKey = text11;
				value19.Position = new Vector3(x25, y25, 0f);
				value19.Scale = new Vector3(x26, y26, 1f);
				value19.MapAtlasIndex = a_mapIndex;
				value19.MapOffsetCounter = num;
				value19.EnableDisplay = false;
				MapAccessoryPosList.Add(value19.Name + "_" + a_mapIndex, value19);
				num++;
				break;
			}
			case "obj":
			{
				string text9 = string.Empty;
				for (int num30 = 1; num30 < array.Length; num30++)
				{
					text9 = text9 + array[num30] + "_";
				}
				text9 = text9.TrimEnd('_');
				float x18 = parts[i].PosX(0);
				float y18 = 0f - parts[i].PosY(0);
				float x19 = parts[i].ScaleX(0);
				float y19 = parts[i].ScaleY(0);
				SsMapEntity value15 = default(SsMapEntity);
				value15.Name = parts[i].Name;
				value15.AnimeKey = text9;
				value15.Position = new Vector3(x18, y18, Def.MAP_ENTITY_Z);
				value15.Scale = new Vector3(x19, y19, 1f);
				value15.MapAtlasIndex = a_mapIndex;
				value15.MapOffsetCounter = num;
				value15.EnableDisplay = false;
				MapEntityPosList.Add(value15.Name + "_" + a_mapIndex, value15);
				num++;
				break;
			}
			case "ep":
			{
				string text6 = string.Empty;
				for (int num18 = 1; num18 < array.Length; num18++)
				{
					text6 = text6 + array[num18] + "_";
				}
				text6 = text6.TrimEnd('_');
				float x9 = parts[i].PosX(0);
				float y9 = 0f - parts[i].PosY(0);
				float x10 = parts[i].ScaleX(0);
				float y10 = parts[i].ScaleY(0);
				SsMapEntity value9 = default(SsMapEntity);
				value9.Name = parts[i].Name;
				value9.AnimeKey = "MapParticle_" + text6;
				value9.Position = new Vector3(x9, y9, 0f);
				value9.Scale = new Vector3(x10, y10, 1f);
				value9.MapAtlasIndex = a_mapIndex;
				value9.MapOffsetCounter = num;
				value9.EnableDisplay = false;
				MapEmitterPosList.Add(value9.Name + "_" + a_mapIndex, value9);
				num++;
				break;
			}
			case "eps":
			{
				string text12 = string.Empty;
				for (int num39 = 1; num39 < array.Length; num39++)
				{
					text12 = text12 + array[num39] + "_";
				}
				text12 = text12.TrimEnd('_');
				float num40 = parts[i].PosX(0);
				float y27 = 0f - parts[i].PosY(0);
				float x27 = parts[i].ScaleX(0);
				float y28 = parts[i].ScaleY(0);
				SsMapEntity value20 = default(SsMapEntity);
				value20.Name = parts[i].Name;
				value20.AnimeKey = "MapParticle_" + text12;
				Vector2 vector = Util.LogScreenSize();
				num40 = ((!(num40 > 0f)) ? ((0f - vector.x) / 2f) : (vector.x / 2f));
				value20.Position = new Vector3(num40, y27, 0f);
				value20.Scale = new Vector3(x27, y28, 1f);
				value20.MapAtlasIndex = a_mapIndex;
				value20.MapOffsetCounter = num;
				value20.EnableDisplay = false;
				MapEmitterPosList.Add(value20.Name + "_" + a_mapIndex, value20);
				num++;
				break;
			}
			case "op":
			{
				string text8 = string.Empty;
				for (int num27 = 1; num27 < array.Length; num27++)
				{
					text8 = text8 + array[num27] + "_";
				}
				text8 = text8.TrimEnd('_');
				float x16 = parts[i].PosX(0);
				float y16 = 0f - parts[i].PosY(0);
				float x17 = parts[i].ScaleX(0);
				float y17 = parts[i].ScaleY(0);
				SsMapEntity value13 = default(SsMapEntity);
				value13.Name = parts[i].Name;
				value13.AnimeKey = text8;
				value13.Position = new Vector3(x16, y16, 0f);
				value13.Scale = new Vector3(x17, y17, 1f);
				value13.MapAtlasIndex = a_mapIndex;
				value13.MapOffsetCounter = num;
				value13.EnableDisplay = false;
				value13.Angle = 30f;
				MapMoveObjectPosList.Add(value13.Name + "_" + a_mapIndex, value13);
				num++;
				break;
			}
			case "op0":
			{
				string text10 = string.Empty;
				for (int num34 = 1; num34 < array.Length; num34++)
				{
					text10 = text10 + array[num34] + "_";
				}
				text10 = text10.TrimEnd('_');
				float x22 = parts[i].PosX(0);
				float y22 = 0f - parts[i].PosY(0);
				float x23 = parts[i].ScaleX(0);
				float y23 = parts[i].ScaleY(0);
				SsMapEntity value17 = default(SsMapEntity);
				value17.Name = parts[i].Name;
				value17.AnimeKey = text10;
				value17.Position = new Vector3(x22, y22, 0f);
				value17.Scale = new Vector3(x23, y23, 1f);
				value17.MapAtlasIndex = a_mapIndex;
				value17.MapOffsetCounter = num;
				value17.EnableDisplay = false;
				value17.Angle = 0f;
				MapMoveObjectPosList.Add(value17.Name + "_" + a_mapIndex, value17);
				num++;
				break;
			}
			case "nora":
			{
				string text4 = string.Empty;
				for (int m = 1; m < array.Length; m++)
				{
					text4 = text4 + array[m] + "_";
				}
				text4 = text4.TrimEnd('_');
				float x6 = parts[i].PosX(0);
				float y6 = 0f - parts[i].PosY(0);
				float num15 = parts[i].ScaleX(0);
				float num16 = parts[i].ScaleY(0);
				SsMapEntity value7 = default(SsMapEntity);
				value7.Name = parts[i].Name;
				value7.AnimeKey = text4;
				value7.Position = new Vector3(x6, y6, 0f);
				value7.Scale = Vector3.one;
				value7.MapAtlasIndex = a_mapIndex;
				value7.MapOffsetCounter = num;
				value7.EnableDisplay = false;
				MapStrayUserList.Add(value7.Name + "_" + a_mapIndex, value7);
				num++;
				break;
			}
			case "char":
			{
				string text2 = string.Empty;
				for (int k = 1; k < array.Length; k++)
				{
					text2 = text2 + array[k] + "_";
				}
				text2 = text2.TrimEnd('_');
				float x3 = parts[i].PosX(0);
				float y3 = 0f - parts[i].PosY(0);
				float num4 = parts[i].ScaleX(0);
				float num5 = parts[i].ScaleY(0);
				SsMapEntity value3 = default(SsMapEntity);
				value3.Name = parts[i].Name;
				value3.AnimeKey = text2;
				value3.Position = new Vector3(x3, y3, 0f);
				value3.Scale = Vector3.one;
				value3.MapAtlasIndex = a_mapIndex;
				value3.MapOffsetCounter = num;
				value3.EnableDisplay = false;
				MapSilhouetteList.Add(value3.Name + "_" + a_mapIndex, value3);
				num++;
				break;
			}
			case "line":
			{
				string[] array6 = array[1].Split('-');
				float num21 = float.Parse(array6[0]);
				float num22 = 0f;
				float num23 = parts[i].PosX(0);
				float num24 = 0f - parts[i].PosY(0);
				float x15 = parts[i].ScaleX(0);
				float y15 = parts[i].ScaleY(0);
				float angle4 = parts[i].Angle(0);
				float num25 = 0f;
				SMMapRoute<float>.RouteNode routeNode2 = null;
				if (array6.Length == 2)
				{
					num22 = float.Parse(array6[1]);
					num25 = Def.GetRouteOrder((float)StageNum + num21, num22, true);
					float routeOrder2 = Def.GetRouteOrder((float)StageNum + num21, 0f);
					SMMapRoute<float>.RouteNode routeNode3 = MapLineRoute.Find("Line" + routeOrder2);
					if (routeNode3 != null)
					{
						routeNode2 = MapLineRoute.InsertSub("Line" + num25, num25, false, routeNode3, num23, num24, num2, (float)a_mapIndex * mMapSize, false);
					}
				}
				else
				{
					num25 = (float)StageNum + float.Parse(array[1]);
					if ((int)num25 != 1)
					{
						routeNode2 = MapLineRoute.InsertMain("Line" + num25, num25, false, null, num23, num24, num2, (float)a_mapIndex * mMapSize, false);
					}
					else
					{
						routeNode2 = MapLineRoute.Find("Line1");
						routeNode2.SetPosition(new Vector3(num23, num24, num2));
						routeNode2.SetRoutePosition(new Vector3(num23, num24 + (float)a_mapIndex * mMapSize, num2));
					}
				}
				if (routeNode2 != null && !MapLineRouteMapIndex.ContainsKey(routeNode2))
				{
					MapLineRouteMapIndex.Add(routeNode2, a_mapIndex);
				}
				SsMapEntity value12 = default(SsMapEntity);
				value12.Name = parts[i].Name;
				value12.AnimeKey = num25.ToString();
				value12.Position = new Vector3(num23, num24, 0f);
				value12.Scale = new Vector3(x15, y15, 1f);
				value12.MapAtlasIndex = a_mapIndex;
				value12.MapOffsetCounter = num;
				value12.Angle = angle4;
				value12.EnableDisplay = false;
				MapLineList.Add(value12.Name + "_" + a_mapIndex, value12);
				num++;
				break;
			}
			case "Lline":
			{
				string[] array5 = array[1].Split('-');
				float x13 = parts[i].PosX(0);
				float y13 = 0f - parts[i].PosY(0);
				float x14 = parts[i].ScaleX(0);
				float y14 = parts[i].ScaleY(0);
				float angle3 = parts[i].Angle(0);
				float num20 = 0f;
				string start2 = array5[0];
				string end2 = array5[1];
				SsMapEntity value11 = default(SsMapEntity);
				value11.Name = parts[i].Name;
				value11.AnimeKey = num20.ToString();
				value11.Position = new Vector3(x13, y13, 0f);
				value11.Scale = new Vector3(x14, y14, 1f);
				value11.MapAtlasIndex = a_mapIndex;
				value11.MapOffsetCounter = num;
				value11.Angle = angle3;
				value11.EnableDisplay = false;
				value11.Start = start2;
				value11.End = end2;
				MapLineList.Add(value11.Name + "_" + a_mapIndex, value11);
				num++;
				break;
			}
			case "L":
			{
				string text7 = array[1];
				float x11 = parts[i].PosX(0);
				float y11 = 0f - parts[i].PosY(0);
				float x12 = parts[i].ScaleX(0);
				float y12 = parts[i].ScaleY(0);
				float angle2 = parts[i].Angle(0);
				float num19 = 0f;
				string start = text7;
				string end = text7;
				SsMapEntity value10 = default(SsMapEntity);
				value10.Name = parts[i].Name;
				value10.AnimeKey = num19.ToString();
				value10.Position = new Vector3(x11, y11, 0f);
				value10.Scale = new Vector3(x12, y12, 1f);
				value10.MapAtlasIndex = a_mapIndex;
				value10.MapOffsetCounter = num;
				value10.Angle = angle2;
				value10.EnableDisplay = false;
				value10.Start = start;
				value10.End = end;
				MapStageList.Add(value10.Name + "_" + a_mapIndex, value10);
				num++;
				break;
			}
			case "sign":
			{
				string text5 = string.Empty;
				for (int n = 1; n < array.Length; n++)
				{
					text5 = text5 + array[n] + "_";
				}
				text5 = text5.TrimEnd('_');
				float x7 = parts[i].PosX(0);
				float y7 = 0f - parts[i].PosY(0);
				float x8 = parts[i].ScaleX(0);
				float y8 = parts[i].ScaleY(0);
				SsMapEntity value8 = default(SsMapEntity);
				value8.Name = parts[i].Name;
				value8.AnimeKey = text5;
				value8.Position = new Vector3(x7, y7, 0f);
				value8.Scale = new Vector3(x8, y8, 1f);
				value8.MapAtlasIndex = a_mapIndex;
				value8.MapOffsetCounter = num;
				value8.EnableDisplay = false;
				MapSignList.Add(value8.Name + "_" + text5, value8);
				num++;
				break;
			}
			case "episode":
			{
				string text3 = string.Empty;
				for (int l = 1; l < array.Length; l++)
				{
					text3 = text3 + array[l] + "_";
				}
				text3 = text3.TrimEnd('_');
				float x5 = parts[i].PosX(0);
				float y5 = 0f - parts[i].PosY(0);
				SsMapEntity value5 = default(SsMapEntity);
				value5.Name = parts[i].Name;
				value5.AnimeKey = text3;
				value5.Position = new Vector3(x5, y5, 0f);
				value5.Scale = Vector3.one;
				value5.MapAtlasIndex = a_mapIndex;
				value5.MapOffsetCounter = num;
				value5.EnableDisplay = false;
				value5.Angle = num3;
				MapEpisodeList.Add(value5.Name + "_" + text3, value5);
				num++;
				num3++;
				break;
			}
			case "mask":
			{
				string text = string.Empty;
				for (int j = 1; j < array.Length; j++)
				{
					text = text + array[j] + "_";
				}
				text = text.TrimEnd('_');
				float x = parts[i].PosX(0);
				float y = 0f - parts[i].PosY(0);
				float x2 = parts[i].ScaleX(0);
				float y2 = parts[i].ScaleY(0);
				SsMapEntity value2 = default(SsMapEntity);
				value2.Name = parts[i].Name;
				value2.AnimeKey = text;
				value2.Position = new Vector3(x, y, 0f);
				value2.Scale = new Vector3(x2, y2, 1f);
				value2.MapAtlasIndex = a_mapIndex;
				value2.MapOffsetCounter = num;
				value2.EnableDisplay = false;
				MapMaskList.Add(value2.Name + "_" + text, value2);
				num++;
				break;
			}
			}
		}
	}

	public void Serialize(BIJBinaryWriter a_data)
	{
	}

	public void Deserialize(BIJBinaryReader a_data)
	{
		MapScaleList.Clear();
		int num = a_data.ReadInt();
		for (int i = 0; i < num; i++)
		{
			int key = a_data.ReadInt();
			float x = a_data.ReadFloat();
			float y = a_data.ReadFloat();
			float z = 1f;
			if (!MapScaleList.ContainsKey(key))
			{
				MapScaleList.Add(key, new Vector3(x, y, z));
			}
		}
		Init();
		num = a_data.ReadInt();
		for (int j = 0; j < num; j++)
		{
			string text = a_data.ReadUTF();
			string s = a_data.ReadUTF();
			float a_x = a_data.ReadFloat();
			float a_y = a_data.ReadFloat();
			float a_z = a_data.ReadFloat();
			short num2 = a_data.ReadShort();
			int num3 = a_data.ReadInt();
			float num4 = float.Parse(s);
			string[] array = text.Split('_');
			switch (array[0])
			{
			case "cp":
			case "cpoint":
				MapRoute.InsertMain("CPM" + num4, num4, false, null, a_x, a_y, a_z, (float)num2 * mMapSize);
				break;
			case "cps":
			case "cpoints":
				MapRoute.InsertMain("CPS" + num4, num4, false, null, a_x, a_y, a_z, (float)num2 * mMapSize);
				break;
			}
		}
		num = a_data.ReadInt();
		for (int k = 0; k < num; k++)
		{
			string text2 = a_data.ReadUTF();
			string s2 = a_data.ReadUTF();
			float num5 = a_data.ReadFloat();
			float num6 = a_data.ReadFloat();
			float num7 = a_data.ReadFloat();
			short num8 = a_data.ReadShort();
			int num9 = a_data.ReadInt();
			float num10 = float.Parse(s2);
			string[] array2 = text2.Split('_');
			switch (array2[0])
			{
			case "stage":
			{
				SMMapRoute<float>.RouteNode routeNode2;
				if ((int)num10 != 1)
				{
					routeNode2 = MapRoute.InsertMain("Stage" + num10, num10, true, null, num5, num6, num7, (float)num8 * mMapSize);
				}
				else
				{
					routeNode2 = MapRoute.Find("Stage1");
					routeNode2.SetPosition(new Vector3(num5, num6, num7));
					routeNode2.SetRoutePosition(new Vector3(num5, num6 + (float)num8 * mMapSize, num7));
				}
				if (!MapRouteMapIndex.ContainsKey(routeNode2))
				{
					MapRouteMapIndex.Add(routeNode2, num8);
				}
				break;
			}
			case "sub":
			{
				float routeOrder = Def.GetRouteOrder((int)num10, 0f);
				SMMapRoute<float>.RouteNode routeNode = MapRoute.Find("Stage" + routeOrder);
				if (routeNode != null)
				{
					SMMapRoute<float>.RouteNode key2 = MapRoute.InsertSub("Stage" + num10, num10, true, routeNode, num5, num6, num7, (float)num8 * mMapSize);
					if (!MapRouteMapIndex.ContainsKey(key2))
					{
						MapRouteMapIndex.Add(key2, num8);
					}
				}
				break;
			}
			}
		}
		MapAccessoryPosList.Clear();
		num = a_data.ReadInt();
		for (int l = 0; l < num; l++)
		{
			string name = a_data.ReadUTF();
			string text3 = a_data.ReadUTF();
			float x2 = a_data.ReadFloat();
			float y2 = a_data.ReadFloat();
			float z2 = 0f;
			short num11 = a_data.ReadShort();
			int mapOffsetCounter = a_data.ReadInt();
			float x3 = a_data.ReadFloat();
			float y3 = a_data.ReadFloat();
			float z3 = 1f;
			AccessoryData accessoryData = SingletonMonoBehaviour<GameMain>.Instance.GetAccessoryData(text3);
			if (accessoryData != null)
			{
				SsMapEntity value = default(SsMapEntity);
				value.Name = name;
				value.AnimeKey = text3;
				value.Position = new Vector3(x2, y2, z2);
				value.Scale = new Vector3(x3, y3, z3);
				value.MapAtlasIndex = num11;
				value.MapOffsetCounter = mapOffsetCounter;
				value.EnableDisplay = false;
				MapAccessoryPosList.Add(value.Name + "_" + num11, value);
			}
		}
		MapRBPosList.Clear();
		num = a_data.ReadInt();
		for (int m = 0; m < num; m++)
		{
			string text4 = a_data.ReadUTF();
			string text5 = a_data.ReadUTF();
			float num12 = a_data.ReadFloat();
			float num13 = a_data.ReadFloat();
			float num14 = a_data.ReadFloat();
			short num15 = a_data.ReadShort();
			int mapOffsetCounter2 = a_data.ReadInt();
			float x4 = a_data.ReadFloat();
			float y4 = a_data.ReadFloat();
			float z4 = 1f;
			float num16 = a_data.ReadFloat();
			string[] array3 = text4.Split('_');
			string[] array4 = array3[1].Split('-');
			float num17 = float.Parse(array4[0]);
			float num18 = float.Parse(text5);
			if (array4.Length == 2)
			{
				MapRoute.InsertSub("RBP" + num18, num18, false, null, num12, num13, num14, (float)num15 * mMapSize);
			}
			else
			{
				MapRoute.InsertMain("RBP" + num18, num18, false, null, num12, num13, num14, (float)num15 * mMapSize);
			}
			SsMapEntity value2 = default(SsMapEntity);
			value2.Name = text4;
			value2.AnimeKey = text5;
			value2.Position = new Vector3(num12, num13, num14);
			value2.Scale = new Vector3(x4, y4, z4);
			value2.MapAtlasIndex = num15;
			value2.MapOffsetCounter = mapOffsetCounter2;
			value2.EnableDisplay = false;
			MapRBPosList.Add(value2.Name + "_" + num15, value2);
		}
		MapERBPosList.Clear();
		num = a_data.ReadInt();
		for (int n = 0; n < num; n++)
		{
			string name2 = a_data.ReadUTF();
			string animeKey = a_data.ReadUTF();
			float x5 = a_data.ReadFloat();
			float y5 = a_data.ReadFloat();
			float z5 = a_data.ReadFloat();
			short num19 = a_data.ReadShort();
			int mapOffsetCounter3 = a_data.ReadInt();
			float x6 = a_data.ReadFloat();
			float y6 = a_data.ReadFloat();
			float z6 = 1f;
			float num20 = a_data.ReadFloat();
			SsMapEntity value3 = default(SsMapEntity);
			value3.Name = name2;
			value3.AnimeKey = animeKey;
			value3.Position = new Vector3(x5, y5, z5);
			value3.Scale = new Vector3(x6, y6, z6);
			value3.MapAtlasIndex = num19;
			value3.MapOffsetCounter = mapOffsetCounter3;
			value3.EnableDisplay = false;
			MapERBPosList.Add(value3.Name + "_" + num19, value3);
		}
		MapChangeList.Clear();
		num = a_data.ReadInt();
		for (int num21 = 0; num21 < num; num21++)
		{
			string name3 = a_data.ReadUTF();
			string animeKey2 = a_data.ReadUTF();
			float num22 = a_data.ReadFloat();
			float num23 = a_data.ReadFloat();
			float num24 = a_data.ReadFloat();
			short num25 = a_data.ReadShort();
			int mapOffsetCounter4 = a_data.ReadInt();
			float x7 = a_data.ReadFloat();
			float y7 = a_data.ReadFloat();
			float z7 = 1f;
			float num26 = a_data.ReadFloat();
			MapRoute.InsertMain("RBPLast", 1000f, false, null, num22, num23, num24, (float)num25 * mMapSize);
			SsMapEntity value4 = default(SsMapEntity);
			value4.Name = name3;
			value4.AnimeKey = animeKey2;
			value4.Position = new Vector3(num22, num23, num24);
			value4.Scale = new Vector3(x7, y7, z7);
			value4.MapAtlasIndex = num25;
			value4.MapOffsetCounter = mapOffsetCounter4;
			value4.EnableDisplay = false;
			MapChangeList.Add(value4.Name + "_" + num25, value4);
		}
		MapEntityPosList.Clear();
		num = a_data.ReadInt();
		for (int num27 = 0; num27 < num; num27++)
		{
			string name4 = a_data.ReadUTF();
			string animeKey3 = a_data.ReadUTF();
			float x8 = a_data.ReadFloat();
			float y8 = a_data.ReadFloat();
			float mAP_ENTITY_Z = Def.MAP_ENTITY_Z;
			short num28 = a_data.ReadShort();
			int mapOffsetCounter5 = a_data.ReadInt();
			float x9 = a_data.ReadFloat();
			float y9 = a_data.ReadFloat();
			float z8 = 1f;
			SsMapEntity value5 = default(SsMapEntity);
			value5.Name = name4;
			value5.AnimeKey = animeKey3;
			value5.Position = new Vector3(x8, y8, mAP_ENTITY_Z);
			value5.Scale = new Vector3(x9, y9, z8);
			value5.MapAtlasIndex = num28;
			value5.MapOffsetCounter = mapOffsetCounter5;
			value5.EnableDisplay = false;
			MapEntityPosList.Add(value5.Name + "_" + num28, value5);
		}
		MapEmitterPosList.Clear();
		num = a_data.ReadInt();
		for (int num29 = 0; num29 < num; num29++)
		{
			string text6 = a_data.ReadUTF();
			string animeKey4 = a_data.ReadUTF();
			float num30 = a_data.ReadFloat();
			float y10 = a_data.ReadFloat();
			float z9 = 0f;
			short num31 = a_data.ReadShort();
			int mapOffsetCounter6 = a_data.ReadInt();
			float x10 = a_data.ReadFloat();
			float y11 = a_data.ReadFloat();
			float z10 = 1f;
			string[] array5 = text6.Split('_');
			switch (array5[0])
			{
			case "eps":
			{
				Vector2 vector = Util.LogScreenSize();
				num30 = ((!(num30 > 0f)) ? ((0f - vector.x) / 2f) : (vector.x / 2f));
				break;
			}
			}
			SsMapEntity value6 = default(SsMapEntity);
			value6.Name = text6;
			value6.AnimeKey = animeKey4;
			value6.Position = new Vector3(num30, y10, z9);
			value6.Scale = new Vector3(x10, y11, z10);
			value6.MapAtlasIndex = num31;
			value6.MapOffsetCounter = mapOffsetCounter6;
			value6.EnableDisplay = false;
			MapEmitterPosList.Add(value6.Name + "_" + num31, value6);
		}
		MapMoveObjectPosList.Clear();
		num = a_data.ReadInt();
		for (int num32 = 0; num32 < num; num32++)
		{
			string name5 = a_data.ReadUTF();
			string animeKey5 = a_data.ReadUTF();
			float x11 = a_data.ReadFloat();
			float y12 = a_data.ReadFloat();
			float z11 = 0f;
			short num33 = a_data.ReadShort();
			int mapOffsetCounter7 = a_data.ReadInt();
			float x12 = a_data.ReadFloat();
			float y13 = a_data.ReadFloat();
			float z12 = 1f;
			float angle = a_data.ReadFloat();
			SsMapEntity value7 = default(SsMapEntity);
			value7.Name = name5;
			value7.AnimeKey = animeKey5;
			value7.Position = new Vector3(x11, y12, z11);
			value7.Scale = new Vector3(x12, y13, z12);
			value7.MapAtlasIndex = num33;
			value7.MapOffsetCounter = mapOffsetCounter7;
			value7.EnableDisplay = false;
			value7.Angle = angle;
			MapMoveObjectPosList.Add(value7.Name + "_" + num33, value7);
		}
		MapStrayUserList.Clear();
		num = a_data.ReadInt();
		for (int num34 = 0; num34 < num; num34++)
		{
			string name6 = a_data.ReadUTF();
			string animeKey6 = a_data.ReadUTF();
			float x13 = a_data.ReadFloat();
			float y14 = a_data.ReadFloat();
			float z13 = 0f;
			short num35 = a_data.ReadShort();
			int mapOffsetCounter8 = a_data.ReadInt();
			SsMapEntity value8 = default(SsMapEntity);
			value8.Name = name6;
			value8.AnimeKey = animeKey6;
			value8.Position = new Vector3(x13, y14, z13);
			value8.Scale = Vector3.one;
			value8.MapAtlasIndex = num35;
			value8.MapOffsetCounter = mapOffsetCounter8;
			value8.EnableDisplay = false;
			MapStrayUserList.Add(value8.Name + "_" + num35, value8);
		}
		MapSilhouetteList.Clear();
		num = a_data.ReadInt();
		for (int num36 = 0; num36 < num; num36++)
		{
			string name7 = a_data.ReadUTF();
			string animeKey7 = a_data.ReadUTF();
			float x14 = a_data.ReadFloat();
			float y15 = a_data.ReadFloat();
			float z14 = 0f;
			short num37 = a_data.ReadShort();
			int mapOffsetCounter9 = a_data.ReadInt();
			SsMapEntity value9 = default(SsMapEntity);
			value9.Name = name7;
			value9.AnimeKey = animeKey7;
			value9.Position = new Vector3(x14, y15, z14);
			value9.Scale = Vector3.one;
			value9.MapAtlasIndex = num37;
			value9.MapOffsetCounter = mapOffsetCounter9;
			value9.EnableDisplay = false;
			MapSilhouetteList.Add(value9.Name + "_" + num37, value9);
		}
		MapSignList.Clear();
		num = a_data.ReadInt();
		for (int num38 = 0; num38 < num; num38++)
		{
			string name8 = a_data.ReadUTF();
			string animeKey8 = a_data.ReadUTF();
			float x15 = a_data.ReadFloat();
			float y16 = a_data.ReadFloat();
			float z15 = 0f;
			short num39 = a_data.ReadShort();
			int mapOffsetCounter10 = a_data.ReadInt();
			float x16 = a_data.ReadFloat();
			float y17 = a_data.ReadFloat();
			float z16 = 1f;
			SsMapEntity value10 = default(SsMapEntity);
			value10.Name = name8;
			value10.AnimeKey = animeKey8;
			value10.Position = new Vector3(x15, y16, z15);
			value10.Scale = new Vector3(x16, y17, z16);
			value10.MapAtlasIndex = num39;
			value10.MapOffsetCounter = mapOffsetCounter10;
			value10.EnableDisplay = false;
			MapSignList.Add(value10.Name + "_" + num39, value10);
		}
		MapEpisodeList.Clear();
		num = a_data.ReadInt();
		for (int num40 = 0; num40 < num; num40++)
		{
			string name9 = a_data.ReadUTF();
			string animeKey9 = a_data.ReadUTF();
			float x17 = a_data.ReadFloat();
			float y18 = a_data.ReadFloat();
			float z17 = 0f;
			short num41 = a_data.ReadShort();
			int mapOffsetCounter11 = a_data.ReadInt();
			int num42 = a_data.ReadShort();
			SsMapEntity value11 = default(SsMapEntity);
			value11.Name = name9;
			value11.AnimeKey = animeKey9;
			value11.Position = new Vector3(x17, y18, z17);
			value11.Scale = Vector3.one;
			value11.MapAtlasIndex = num41;
			value11.MapOffsetCounter = mapOffsetCounter11;
			value11.Angle = num42;
			value11.EnableDisplay = false;
			MapEpisodeList.Add(value11.Name + "_" + num41, value11);
		}
		MapLineList.Clear();
		num = a_data.ReadInt();
		for (int num43 = 0; num43 < num; num43++)
		{
			string text7 = a_data.ReadUTF();
			string text8 = a_data.ReadUTF();
			float num44 = a_data.ReadFloat();
			float num45 = a_data.ReadFloat();
			float num46 = a_data.ReadFloat();
			short num47 = a_data.ReadShort();
			int mapOffsetCounter12 = a_data.ReadInt();
			float x18 = a_data.ReadFloat();
			float y19 = a_data.ReadFloat();
			float z18 = 1f;
			float angle2 = a_data.ReadFloat();
			string text9 = a_data.ReadUTF();
			string text10 = a_data.ReadUTF();
			if (string.IsNullOrEmpty(text9))
			{
				text9 = string.Empty;
			}
			if (string.IsNullOrEmpty(text10))
			{
				text10 = string.Empty;
			}
			float num48 = float.Parse(text8);
			string[] array6 = text7.Split('_');
			string[] array7 = array6[1].Split('-');
			switch (array6[0])
			{
			case "line":
			{
				float routeOrder2 = Def.GetRouteOrder((int)num48, 0f);
				SMMapRoute<float>.RouteNode routeNode3 = null;
				if (array7.Length == 2)
				{
					SMMapRoute<float>.RouteNode routeNode4 = MapLineRoute.Find("Line" + routeOrder2);
					if (routeNode4 != null)
					{
						routeNode3 = MapLineRoute.InsertSub("Line" + num48, num48, false, routeNode4, num44, num45, num46, (float)num47 * mMapSize, false);
					}
				}
				else if ((int)num48 != 1)
				{
					routeNode3 = MapLineRoute.InsertMain("Line" + num48, num48, false, null, num44, num45, num46, (float)num47 * mMapSize, false);
				}
				else
				{
					routeNode3 = MapLineRoute.Find("Line1");
					routeNode3.SetPosition(new Vector3(num44, num45, num46));
					routeNode3.SetRoutePosition(new Vector3(num44, num45 + (float)num47 * mMapSize, num46));
				}
				if (routeNode3 != null && !MapLineRouteMapIndex.ContainsKey(routeNode3))
				{
					MapLineRouteMapIndex.Add(routeNode3, num47);
				}
				break;
			}
			}
			SsMapEntity value12 = default(SsMapEntity);
			value12.Name = text7;
			value12.AnimeKey = text8;
			value12.Position = new Vector3(num44, num45, num46);
			value12.Scale = new Vector3(x18, y19, z18);
			value12.MapAtlasIndex = num47;
			value12.MapOffsetCounter = mapOffsetCounter12;
			value12.EnableDisplay = false;
			value12.Angle = angle2;
			value12.Start = text9;
			value12.End = text10;
			MapLineList.Add(value12.Name + "_" + num47, value12);
		}
	}

	public void Dump()
	{
	}
}
