using System;

public class MiniJSONArrayAttribute : Attribute
{
	public Type EntryType { get; set; }

	public MiniJSONArrayAttribute(Type entryType)
	{
		EntryType = entryType;
	}
}
