using System.Collections.Generic;
using UnityEngine;

public class SlidePanel : MonoBehaviour
{
	protected enum SW_STATE
	{
		INIT = 0,
		FIRST_LOAD = 1,
		MAIN = 2,
		CURSOR_MOVE_INIT = 3,
		ORB_MOVE_INIT = 4,
		FLICK_MOVE_INIT = 5,
		MOVE_INIT = 6,
		MOVE = 7,
		WAIT = 8
	}

	public enum DIRECTION
	{
		LEFT = 0,
		RIGHT = 1
	}

	private class OrbButtonInstance : MonoBehaviour
	{
		public delegate void OnOrbButtonPushed(int i);

		private OnOrbButtonPushed mCallback;

		public int index;

		public UISprite sprite;

		public UIButton button;

		public static OrbButtonInstance Make(GameObject _parent, int _index, float _posX, BIJImage _Atlas, int _baseDepth, string _ImgName, bool _enable)
		{
			OrbButtonInstance orbButtonInstance = null;
			if (_enable)
			{
				UIButton uIButton = Util.CreateJellyImageButton("orb" + _index, _parent, _Atlas);
				orbButtonInstance = uIButton.gameObject.AddComponent<OrbButtonInstance>();
				orbButtonInstance.button = uIButton;
				Util.SetImageButtonInfo(orbButtonInstance.button, _ImgName, _ImgName, _ImgName, _baseDepth + 3, new Vector3(_posX, 0f, 0f), Vector3.one);
				Util.AddImageButtonMessage(orbButtonInstance.button, orbButtonInstance, "OnOrbButtonCallBack", UIButtonMessage.Trigger.OnClick);
				orbButtonInstance.sprite = orbButtonInstance.button.gameObject.GetComponent<UISprite>();
			}
			else
			{
				UISprite uISprite = Util.CreateSprite("orb" + _index, _parent, _Atlas);
				orbButtonInstance = uISprite.gameObject.AddComponent<OrbButtonInstance>();
				Util.SetSpriteInfo(uISprite, _ImgName, _baseDepth + 3, new Vector3(_posX, 0f, 0f), Vector3.one, false);
				orbButtonInstance.sprite = uISprite;
			}
			orbButtonInstance.index = _index;
			return orbButtonInstance;
		}

		public void setCallBack(OnOrbButtonPushed _mCallback)
		{
			mCallback = _mCallback;
		}

		private void OnOrbButtonCallBack()
		{
			mCallback(index);
		}
	}

	public delegate void OnIndexButtonPushed(int i);

	public delegate void OnMoveInit(int currentIndex, int targetIndex, int startPosX, int targetPosX);

	public delegate void OnMoveUpdate(int currentIndex, int targetIndex, int moveX, int startX, int targetX, int slidedirecttion);

	public delegate void OnMoveFinished(int currentIndex);

	public const int MAX_PANEL_NUM = 5;

	protected StatusManager<SW_STATE> mState = new StatusManager<SW_STATE>(SW_STATE.INIT);

	private int mBaseDepth;

	private float mClipX;

	private float mClipY;

	public DIRECTION mSlideDirection = DIRECTION.RIGHT;

	private string mCursorImgName;

	private BIJImage mCursorAtlas;

	public float mCursorPos = 21.5f;

	private bool mUseCursor;

	private UIButton mLeftCursor;

	private UIButton mRightCursor;

	private GameObject mCursorParentGO;

	private Util.TriFunc mCursorTriFunc;

	private string mOrbImgName;

	private BIJImage mOrbAtlas;

	private bool mUseOrb;

	private GameObject mOrbParentGO;

	private GameObject mOrbRoot;

	private float mOrbPosX;

	private float mOrbRootPosX;

	private float mOrbRootPosY;

	private float mOrbRootPosZ;

	private float mOrbRootPosXOverride;

	private float mOrbRootPosYOverride;

	private float mOrbRootPosZOverride;

	public float mOrbPos = -10f;

	private SlidePanelObject mFirstPanel;

	private bool mIsFirstPanelLoaded;

	private List<JellyImageButton> mOrbButtonList = new List<JellyImageButton>();

	private List<UISprite> mOrbSpriteList = new List<UISprite>();

	protected List<SlidePanelSetting> mSlideSettingList = new List<SlidePanelSetting>();

	protected Dictionary<int, SlidePanelObject> mSlideObjectList = new Dictionary<int, SlidePanelObject>();

	public List<SlidePanelObject> CacheSlideObjectList;

	public bool ForceSlideUnable;

	public bool IsPanelAllLoadFinished;

	public bool mLoopEnable;

	public bool mAutoMoveEnable;

	public float mAutoMoveSec = 4f;

	private float mAutoMoveStartTime;

	private OnIndexButtonPushed mCallback;

	private OnMoveInit mMoveInitCallback;

	private OnMoveUpdate mMoveUpdateCallback;

	private OnMoveFinished mMoveFinishedCallback;

	private GameObject mListRoot;

	private float mPosX;

	private int mSlideCount;

	private bool mSlideEnable = true;

	private int mNowViewPanelId;

	private int mMovedSlidePosX;

	private int mMoveSlideStartPosX;

	private int mMoveValue;

	private int mMoveDirection = 1;

	private int mMovedPanelId;

	private List<BoxCollider> mColliderList = new List<BoxCollider>();

	private bool mFlickNow;

	private float mTheLastNowPosX;

	private Vector3 mNowGoPos;

	private bool mEnableButtonNow;

	private bool mEnableOrbButton = true;

	private bool mIsOrbMove;

	private int mPanelDiff;

	private int mConstDirection
	{
		get
		{
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				return 1;
			}
			return -1;
		}
	}

	public SlidePanelObject FirstPanel
	{
		get
		{
			return mFirstPanel;
		}
	}

	public List<SlidePanelSetting> SettingList
	{
		get
		{
			return mSlideSettingList;
		}
	}

	public static void log(string a_str)
	{
		SingletonMonoBehaviour<GameMain>.Instance.LogDebug(a_str, "TIPS");
	}

	public SlidePanelObject GetCurrentPanelObject()
	{
		if (mSlideObjectList.ContainsKey(mNowViewPanelId))
		{
			return mSlideObjectList[mNowViewPanelId];
		}
		return null;
	}

	public List<SlidePanelObject> GetSlideObjectList()
	{
		if (CacheSlideObjectList == null)
		{
			CacheSlideObjectList = new List<SlidePanelObject>();
			List<int> list = new List<int>(mSlideObjectList.Keys);
			list.Sort();
			for (int i = 0; i < list.Count; i++)
			{
				CacheSlideObjectList.Add(mSlideObjectList[list[i]]);
			}
		}
		return CacheSlideObjectList;
	}

	public void SetMoveInitCallback(OnMoveInit a_callback)
	{
		mMoveInitCallback = a_callback;
	}

	public void SetMoveUpdateCallback(OnMoveUpdate a_callback)
	{
		mMoveUpdateCallback = a_callback;
	}

	public void SetMoveFinishCallback(OnMoveFinished a_callback)
	{
		mMoveFinishedCallback = a_callback;
	}

	public void SetOrbButtonEnable(bool _flg)
	{
		mEnableOrbButton = _flg;
	}

	public static SlidePanel Make(string a_name, GameObject a_parent, int a_depth, int a_clipx, int a_clipy)
	{
		SlidePanel slidePanel = Util.CreateGameObject(a_name, a_parent).AddComponent<SlidePanel>();
		slidePanel.Init(a_depth, a_clipx, a_clipy);
		return slidePanel;
	}

	public void Start()
	{
	}

	public virtual void Update()
	{
		switch (mState.GetStatus())
		{
		case SW_STATE.INIT:
			CreateSlideWindow();
			log("slide window created.");
			mState.Change(SW_STATE.FIRST_LOAD);
			break;
		case SW_STATE.FIRST_LOAD:
		{
			if (!mFirstPanel.mPanelSetting.mIsLoadFinished)
			{
				break;
			}
			mIsFirstPanelLoaded = true;
			List<SlidePanelObject> slideObjectList = GetSlideObjectList();
			for (int i = 0; i < slideObjectList.Count; i++)
			{
				if (!slideObjectList[i].mPanelSetting.mIsLoadFinished)
				{
					slideObjectList[i].Load();
				}
			}
			mState.Change(SW_STATE.MAIN);
			break;
		}
		case SW_STATE.MAIN:
			SetFlickInitPos();
			if (!ForceSlideUnable)
			{
				CheckPanelLoad();
				if (IsPanelAllLoadFinished)
				{
					CheckAutoMove();
				}
			}
			break;
		case SW_STATE.CURSOR_MOVE_INIT:
			if (ForceSlideUnable)
			{
				break;
			}
			mMovedSlidePosX = (int)(mListRoot.transform.localPosition.x + mClipX * (float)mMoveDirection);
			mMoveValue = (int)mClipX / 8;
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				if (mMoveDirection == -1)
				{
					mMovedPanelId = mNowViewPanelId + 1;
				}
				else
				{
					mMovedPanelId = mNowViewPanelId - 1;
				}
			}
			else if (mMoveDirection == -1)
			{
				mMovedPanelId = mNowViewPanelId - 1;
			}
			else
			{
				mMovedPanelId = mNowViewPanelId + 1;
			}
			mState.Reset(SW_STATE.MOVE_INIT, true);
			break;
		case SW_STATE.ORB_MOVE_INIT:
			if (ForceSlideUnable)
			{
				break;
			}
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				if (mMovedPanelId > mNowViewPanelId)
				{
					mPanelDiff = mMovedPanelId - mNowViewPanelId;
					mMoveDirection = -1;
				}
				else
				{
					mPanelDiff = mNowViewPanelId - mMovedPanelId;
					mMoveDirection = 1;
				}
			}
			else if (mMovedPanelId > mNowViewPanelId)
			{
				mPanelDiff = mMovedPanelId - mNowViewPanelId;
				mMoveDirection = 1;
			}
			else
			{
				mPanelDiff = mNowViewPanelId - mMovedPanelId;
				mMoveDirection = -1;
			}
			if (mPanelDiff > 5)
			{
				mPanelDiff = 5;
			}
			mMovedSlidePosX = (int)(mClipX * (float)(mMoveDirection * -1)) * mMovedPanelId;
			mMoveValue = (int)(mClipX * (float)mPanelDiff) / (8 + (mPanelDiff - 1));
			mIsOrbMove = true;
			mState.Reset(SW_STATE.MOVE_INIT, true);
			break;
		case SW_STATE.FLICK_MOVE_INIT:
		{
			if (ForceSlideUnable)
			{
				break;
			}
			float num = mListRoot.transform.localPosition.x - mTheLastNowPosX;
			if (num >= 0f)
			{
				if (num > mClipX / 4f)
				{
					mMoveDirection = 1;
					if (mSlideDirection == DIRECTION.RIGHT)
					{
						if (mMoveDirection == -1)
						{
							mMovedPanelId = mNowViewPanelId + 1;
						}
						else
						{
							mMovedPanelId = mNowViewPanelId - 1;
						}
					}
					else if (mMoveDirection == -1)
					{
						mMovedPanelId = mNowViewPanelId - 1;
					}
					else
					{
						mMovedPanelId = mNowViewPanelId + 1;
					}
				}
				else
				{
					mMoveDirection = -1;
					mMovedPanelId = mNowViewPanelId;
				}
			}
			else if (num < mClipX / 4f * -1f)
			{
				mMoveDirection = -1;
				if (mSlideDirection == DIRECTION.RIGHT)
				{
					if (mMoveDirection == -1)
					{
						mMovedPanelId = mNowViewPanelId + 1;
					}
					else
					{
						mMovedPanelId = mNowViewPanelId - 1;
					}
				}
				else if (mMoveDirection == -1)
				{
					mMovedPanelId = mNowViewPanelId - 1;
				}
				else
				{
					mMovedPanelId = mNowViewPanelId + 1;
				}
			}
			else
			{
				mMoveDirection = 1;
				mMovedPanelId = mNowViewPanelId;
			}
			if (mMovedPanelId == mNowViewPanelId)
			{
				mMovedSlidePosX = 0;
			}
			else
			{
				mMovedSlidePosX = (int)(mClipX * (float)mMoveDirection);
			}
			mMoveValue = (int)mClipX / 8;
			mState.Reset(SW_STATE.MOVE_INIT, true);
			break;
		}
		case SW_STATE.MOVE_INIT:
			if (mState.IsChanged())
			{
				if (mMovedPanelId < 0)
				{
					mMovedPanelId = mSlideObjectList.Count - 1;
				}
				else if (mMovedPanelId >= mSlideObjectList.Count)
				{
					mMovedPanelId = 0;
				}
				log("to=" + mMovedSlidePosX);
				AdjustPanelPos();
				if (mMovedPanelId == mNowViewPanelId)
				{
					mMoveSlideStartPosX = 0;
				}
				else
				{
					mMoveSlideStartPosX = (int)mListRoot.transform.localPosition.x;
				}
				if (mMoveInitCallback != null)
				{
					mMoveInitCallback(mNowViewPanelId, mMovedPanelId, mMoveSlideStartPosX, mMovedSlidePosX);
				}
			}
			if (IsMoveEnable())
			{
				mState.Reset(SW_STATE.MOVE);
				goto case SW_STATE.MOVE;
			}
			break;
		case SW_STATE.MOVE:
			if (mListRoot.transform.localPosition.x == (float)mMovedSlidePosX)
			{
				mNowViewPanelId = mMovedPanelId;
				ChangeCursorStatus();
				ChangeOrbStatus();
				if (mLoopEnable)
				{
					mAutoMoveStartTime = mAutoMoveSec;
				}
				mSlideEnable = true;
				MoveFinished();
				if (mMoveFinishedCallback != null)
				{
					mMoveFinishedCallback(mNowViewPanelId);
				}
				mState.Change(SW_STATE.MAIN);
			}
			else
			{
				int num2 = (int)mListRoot.transform.localPosition.x + mMoveValue * mMoveDirection;
				if (num2 * mMoveDirection <= mMovedSlidePosX * mMoveDirection)
				{
					mListRoot.transform.localPosition = new Vector3(num2, 0f, 0f);
				}
				else
				{
					mListRoot.transform.localPosition = new Vector3(mMovedSlidePosX, 0f, 0f);
				}
				if (mMoveUpdateCallback != null)
				{
					mMoveUpdateCallback(mNowViewPanelId, mMovedPanelId, num2, mMoveSlideStartPosX, mMovedSlidePosX, mMoveDirection);
				}
			}
			break;
		}
		if (mSlideEnable)
		{
			if (mUseCursor && mCursorTriFunc != null)
			{
				mCursorTriFunc.AddDegree(Time.deltaTime * 180f);
				float num3 = 0.15f * mCursorTriFunc.Sin();
				if (mRightCursor != null)
				{
					mRightCursor.transform.localScale = new Vector3(1f + num3, 1f + num3, 1f);
				}
				if (mLeftCursor != null)
				{
					mLeftCursor.transform.localScale = new Vector3(1f + num3, 1f + num3, 1f);
				}
			}
		}
		else if (mUseCursor)
		{
			if (mCursorTriFunc != null)
			{
				mCursorTriFunc.SetDegree(0f);
			}
			if (mRightCursor != null)
			{
				mRightCursor.transform.localScale = Vector3.one;
			}
			if (mLeftCursor != null)
			{
				mLeftCursor.transform.localScale = Vector3.one;
			}
		}
		mState.Update();
	}

	public void Init(int _baseDepth, float _clipX, float _clipY)
	{
		mBaseDepth = _baseDepth;
		mClipX = _clipX;
		mClipY = _clipY;
	}

	public void SetCursorSettings(string _CursorImgName, BIJImage _CursorAtlas, GameObject a_parent = null)
	{
		mCursorImgName = _CursorImgName;
		mCursorAtlas = _CursorAtlas;
		mUseCursor = true;
		mCursorParentGO = a_parent;
	}

	public void SetOrbSettings(string _OrbImgName, BIJImage _OrbAtlas, GameObject a_parent = null)
	{
		mOrbImgName = _OrbImgName;
		mOrbAtlas = _OrbAtlas;
		mUseOrb = true;
		mOrbParentGO = a_parent;
	}

	public void setCallBack(OnIndexButtonPushed _mCallback)
	{
		mCallback = _mCallback;
	}

	public virtual int AddSlideWindowPanel(SlidePanelSetting a_setting)
	{
		mSlideSettingList.Add(a_setting);
		return mSlideSettingList.Count - 1;
	}

	public void Delete()
	{
		for (int i = 0; i < mSlideObjectList.Count; i++)
		{
			mSlideObjectList[i].Unload();
		}
		mSlideObjectList.Clear();
		Object.Destroy(base.gameObject);
	}

	public void AdjustPanelPos()
	{
		log("slide panel adjust panel pos. obj=" + mSlideObjectList.Count);
		int num = mMovedPanelId;
		if (num < 0)
		{
			num = mSlideObjectList.Count - 1;
		}
		if (num >= mSlideObjectList.Count)
		{
			num = 0;
		}
		log("slide panel adjust panel pos target=" + num);
		List<int> list = new List<int>(mSlideObjectList.Keys);
		List<int> list2 = new List<int>();
		if (mIsOrbMove)
		{
			for (int i = 0; i < list.Count; i++)
			{
				int num2 = list[i];
				if (num2 != mNowViewPanelId && num2 != num)
				{
					list2.Add(num2);
				}
			}
			if (list2.Count < mPanelDiff)
			{
				int[] array = new int[mSlideSettingList.Count];
				for (int j = 0; j < mSlideSettingList.Count; j++)
				{
					array[j] = j;
				}
				int num3 = array.Length;
				while (num3 > 1)
				{
					num3--;
					int num4 = Random.Range(0, num3);
					int num5 = array[num4];
					array[num4] = array[num3];
					array[num3] = num5;
				}
				int num6 = 0;
				while (list2.Count < mPanelDiff)
				{
					if (num != array[num6] && !mSlideObjectList.ContainsKey(array[num6]))
					{
						SlidePanelSetting a_setting = mSlideSettingList[array[num6]];
						mSlideObjectList[array[num6]] = SlidePanelObject.Make(a_setting, array[num6], mListRoot);
						mSlideObjectList[array[num6]].Load();
						list2.Add(array[num6]);
					}
					num6++;
				}
			}
		}
		if (!mSlideObjectList.ContainsKey(num))
		{
			SlidePanelSetting a_setting2 = mSlideSettingList[num];
			mSlideObjectList[num] = SlidePanelObject.Make(a_setting2, num, mListRoot);
			mSlideObjectList[num].Load();
		}
		List<int> list3 = new List<int>();
		list3.Add(mNowViewPanelId);
		SlidePanelObject slidePanelObject = null;
		float num7 = 0f;
		if (num != mNowViewPanelId)
		{
			slidePanelObject = mSlideObjectList[num];
			num7 = (mIsOrbMove ? (mClipX * (float)mMoveDirection * -1f * (float)mPanelDiff) : (mClipX * (float)mMoveDirection * -1f));
			slidePanelObject.transform.localPosition = new Vector3(num7, slidePanelObject.transform.localPosition.y, slidePanelObject.transform.localPosition.z);
			list3.Add(num);
		}
		if (mIsOrbMove)
		{
			for (int k = 0; k < list2.Count; k++)
			{
				slidePanelObject = mSlideObjectList[list2[k]];
				num7 = mClipX * (float)mMoveDirection * -1f * (float)k;
				slidePanelObject.transform.localPosition = new Vector3(num7, slidePanelObject.transform.localPosition.y, slidePanelObject.transform.localPosition.z);
				list3.Add(num);
			}
		}
		else
		{
			int num8 = mNowViewPanelId + 1;
			if (num8 >= mSlideObjectList.Count)
			{
				num8 = 0;
			}
			if (num != num8)
			{
				slidePanelObject = mSlideObjectList[num8];
				num7 = mClipX;
				slidePanelObject.transform.localPosition = new Vector3(num7, slidePanelObject.transform.localPosition.y, slidePanelObject.transform.localPosition.z);
				list3.Add(num8);
			}
			num8 = mNowViewPanelId - 1;
			if (num8 < 0)
			{
				num8 = mSlideObjectList.Count - 1;
			}
			if (num != num8)
			{
				slidePanelObject = mSlideObjectList[num8];
				num7 = mClipX * -1f;
				slidePanelObject.transform.localPosition = new Vector3(num7, slidePanelObject.transform.localPosition.y, slidePanelObject.transform.localPosition.z);
				list3.Add(num8);
			}
		}
		list = new List<int>(mSlideObjectList.Keys);
		for (int l = 0; l < list.Count; l++)
		{
			int num9 = list[l];
			if (!list3.Contains(num9))
			{
				mSlideObjectList[num9].transform.localScale = Vector3.zero;
				continue;
			}
			mSlideObjectList[num9].transform.localScale = Vector3.one;
			NGUITools.SetActive(mSlideObjectList[num9].gameObject, true);
		}
		log("slide panel adjust panel pos end");
		mIsOrbMove = false;
	}

	private bool IsMoveEnable()
	{
		bool result = true;
		List<SlidePanelObject> slideObjectList = GetSlideObjectList();
		for (int i = 0; i < slideObjectList.Count; i++)
		{
			if (!slideObjectList[i].mPanelSetting.mIsLoadFinished)
			{
				result = false;
				break;
			}
		}
		return result;
	}

	private void MoveFinished()
	{
		List<int> list = new List<int>(mSlideObjectList.Keys);
		for (int i = -2; i < 3; i++)
		{
			int num = mNowViewPanelId + i;
			if (num < 0)
			{
				num += mSlideSettingList.Count;
			}
			else if (num >= mSlideSettingList.Count)
			{
				num -= mSlideSettingList.Count;
			}
			if (list.Contains(num))
			{
				list.Remove(num);
			}
		}
		for (int j = 0; j < list.Count; j++)
		{
			int key = list[j];
			if (mSlideObjectList.ContainsKey(key))
			{
				mSlideObjectList[key].Unload();
				mSlideObjectList.Remove(key);
			}
		}
		mListRoot.transform.localPosition = new Vector3(0f, mListRoot.transform.localPosition.y, mListRoot.transform.localPosition.z);
		mSlideObjectList[mNowViewPanelId].transform.localPosition = new Vector3(0f, mSlideObjectList[mNowViewPanelId].transform.localPosition.y, mSlideObjectList[mNowViewPanelId].transform.localPosition.z);
		AdjustPanelPos();
	}

	protected virtual void CreateSlideWindow()
	{
		GameObject gameObject = Util.CreateGameObject("PanelListGo", base.gameObject);
		gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
		UIPanel uIPanel = gameObject.AddComponent<UIPanel>();
		uIPanel.sortingOrder = mBaseDepth;
		uIPanel.depth = mBaseDepth;
		uIPanel.clipping = UIDrawCall.Clipping.SoftClip;
		uIPanel.baseClipRegion = new Vector4(0f, 0f, mClipX, mClipY);
		uIPanel.clipSoftness = new Vector2(10f, 10f);
		mListRoot = Util.CreateGameObject("List", uIPanel.gameObject);
		mListRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
		if (CacheSlideObjectList != null)
		{
			CacheSlideObjectList = null;
		}
		float num = 0f;
		for (int i = 0; i < 5 && mSlideSettingList.Count > i; i++)
		{
			SlidePanelSetting slidePanelSetting = mSlideSettingList[i];
			mSlideObjectList[i] = SlidePanelObject.Make(slidePanelSetting, i, mListRoot);
			mSlideObjectList[i].transform.localPosition = new Vector3(num, 0f, 0f);
			num += (float)slidePanelSetting.mWidth;
		}
		mSlideCount = mSlideObjectList.Count;
		if (mSlideCount <= 1)
		{
			mLoopEnable = false;
			mUseCursor = false;
			mAutoMoveEnable = false;
			mUseOrb = false;
		}
		else
		{
			FlickWindow flickWindow = base.gameObject.AddComponent<FlickWindow>();
			mNowGoPos = Camera.main.WorldToScreenPoint(base.gameObject.transform.position);
			float _posX;
			float _posY;
			GetObjectScreenSize(new Vector3(mClipX, mClipY), out _posX, out _posY);
			flickWindow.Init(mNowGoPos.x - _posX / 2f, mNowGoPos.y - _posY / 2f, _posX, _posY);
			flickWindow.SetFlickStartCallBack(GetFlickPos);
			flickWindow.SetFlickEndCallBack(EndFlick);
		}
		if (mLoopEnable)
		{
			int num2 = mSlideSettingList.Count - 1;
			if (!mSlideObjectList.ContainsKey(num2))
			{
				SlidePanelSetting slidePanelSetting2 = mSlideSettingList[num2];
				mSlideObjectList[num2] = SlidePanelObject.Make(slidePanelSetting2, num2, mListRoot);
				mSlideObjectList[num2].transform.localPosition = new Vector3(-slidePanelSetting2.mWidth, 0f, 0f);
			}
		}
		else
		{
			mAutoMoveEnable = false;
		}
		if (mSlideCount >= 1 && mUseCursor)
		{
			mLeftCursor = Util.CreateJellyImageButton("LeftCursor", mCursorParentGO, mCursorAtlas);
			Util.SetImageButtonInfo(mLeftCursor, mCursorImgName, mCursorImgName, mCursorImgName, mBaseDepth + 30, new Vector3(0f - mCursorPos, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mLeftCursor, this, "OnLeft_Callback", UIButtonMessage.Trigger.OnClick);
			mLeftCursor.transform.localPosition = new Vector3(mLeftCursor.transform.localPosition.x - mClipX / 2f, mLeftCursor.transform.localPosition.y, mLeftCursor.transform.localPosition.z);
			mColliderList.Add(mLeftCursor.gameObject.GetComponent<BoxCollider>());
			mRightCursor = Util.CreateJellyImageButton("RightCursor", mCursorParentGO, mCursorAtlas);
			Util.SetImageButtonInfo(mRightCursor, mCursorImgName, mCursorImgName, mCursorImgName, mBaseDepth + 30, new Vector3(mCursorPos, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mRightCursor, this, "OnRight_Callback", UIButtonMessage.Trigger.OnClick);
			mRightCursor.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
			mRightCursor.transform.localPosition = new Vector3(mRightCursor.transform.localPosition.x + mClipX / 2f, mRightCursor.transform.localPosition.y, mRightCursor.transform.localPosition.z);
			mColliderList.Add(mRightCursor.gameObject.GetComponent<BoxCollider>());
			mCursorTriFunc = Util.CreateTriFunc(0f, 180f, Util.TriFunc.MAXFUNC.LOOP);
			ChangeCursorStatus();
		}
		else
		{
			mUseCursor = false;
		}
		if (mSlideCount >= 1 && mUseOrb)
		{
			mOrbRootPosY = mOrbPos - mClipY / 2f;
			mOrbRootPosZ = -10f;
			mOrbRoot = Util.CreateGameObject("OrbList", mOrbParentGO);
			mOrbRoot.transform.localPosition = new Vector3(0f, mOrbRootPosY, mOrbRootPosZ);
			float x = 0f;
			for (int j = 0; j < mSlideCount; j++)
			{
				OrbButtonInstance orbButtonInstance = OrbButtonInstance.Make(mOrbRoot, j, mOrbPosX, mOrbAtlas, mBaseDepth, mOrbImgName, mEnableOrbButton);
				if (mEnableOrbButton)
				{
					orbButtonInstance.setCallBack(OnOrbButtonPushedCallBack);
					mOrbButtonList.Add(orbButtonInstance.button.GetComponent<JellyImageButton>());
					mColliderList.Add(orbButtonInstance.button.gameObject.GetComponent<BoxCollider>());
				}
				else
				{
					mOrbSpriteList.Add(orbButtonInstance.sprite);
				}
				mOrbPosX += (float)mConstDirection * ((float)orbButtonInstance.sprite.width * 1.5f);
				mOrbRoot.transform.localPosition = new Vector3(x, mOrbRoot.transform.localPosition.y, mOrbRootPosZ);
				x = mOrbRoot.transform.localPosition.x - (float)mConstDirection * ((float)orbButtonInstance.sprite.width * 0.75f);
			}
			mOrbRootPosX = x;
			ChangeOrbStatus();
		}
		else
		{
			mUseOrb = false;
		}
		if (mLoopEnable)
		{
			mAutoMoveStartTime = mAutoMoveSec;
		}
		if (mSlideObjectList.Count > 0)
		{
			List<SlidePanelObject> slideObjectList = GetSlideObjectList();
			mFirstPanel = slideObjectList[0];
			mFirstPanel.Load();
			SetEnableButtonAction(false);
			ChangeCursorStatus();
		}
		CheckPanelLoad();
		AdjustPanelPos();
		if ((int)mOrbRootPosXOverride == 0)
		{
			mOrbRootPosXOverride = mOrbRootPosX;
		}
		if ((int)mOrbRootPosYOverride == 0)
		{
			mOrbRootPosYOverride = mOrbRootPosY;
		}
		if ((int)mOrbRootPosZOverride == 0)
		{
			mOrbRootPosZOverride = mOrbRootPosZ;
		}
		SetLayout(Util.ScreenOrientation);
	}

	public void SetLayout(ScreenOrientation o)
	{
		List<int> list = new List<int>(mSlideObjectList.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			if (mSlideObjectList[list[i]].mPanelSetting.mOnSetLayoutCallback != null)
			{
				mSlideObjectList[list[i]].mPanelSetting.mOnSetLayoutCallback(mSlideObjectList[list[i]], o);
			}
		}
		UpdateOrbPosition();
	}

	private void CheckPanelLoad()
	{
		bool flag = true;
		if (!mFirstPanel.mPanelSetting.mIsLoadFinished)
		{
			flag = false;
		}
		else if (!IsMoveEnable())
		{
			flag = false;
		}
		if (flag)
		{
			IsPanelAllLoadFinished = true;
			if (!mEnableButtonNow)
			{
				SetEnableButtonAction(true);
				ChangeCursorStatus();
			}
		}
		else
		{
			IsPanelAllLoadFinished = false;
			if (mEnableButtonNow)
			{
				SetEnableButtonAction(false);
				ChangeCursorStatus();
			}
		}
	}

	public void SetOrbPositionX(float a_posx)
	{
		mOrbRootPosXOverride = a_posx;
	}

	public void SetOrbPositionY(float a_posy)
	{
		mOrbRootPosYOverride = a_posy;
	}

	public void SetOrbPositionZ(float a_posz)
	{
		mOrbRootPosZOverride = a_posz;
	}

	public void UpdateOrbPosition()
	{
		bool flag = false;
		if ((int)mOrbRootPosXOverride != (int)mOrbRootPosX)
		{
			mOrbRootPosX = mOrbRootPosXOverride;
			flag = true;
		}
		if ((int)mOrbRootPosYOverride != (int)mOrbRootPosY)
		{
			mOrbRootPosY = mOrbRootPosYOverride;
			flag = true;
		}
		if ((int)mOrbRootPosZOverride != (int)mOrbRootPosZ)
		{
			mOrbRootPosZ = mOrbRootPosZOverride;
			flag = true;
		}
		if (flag && mOrbRoot != null)
		{
			mOrbRoot.transform.localPosition = new Vector3(mOrbRootPosX, mOrbRootPosY, mOrbRootPosZ);
		}
	}

	private void ChangeCursorStatus()
	{
		if (ForceSlideUnable || mLeftCursor == null || mRightCursor == null)
		{
			return;
		}
		if (!mEnableButtonNow)
		{
			NGUITools.SetActive(mLeftCursor.gameObject, false);
			NGUITools.SetActive(mRightCursor.gameObject, false);
		}
		else
		{
			if (!mUseCursor)
			{
				return;
			}
			if (mLoopEnable)
			{
				NGUITools.SetActive(mLeftCursor.gameObject, true);
				NGUITools.SetActive(mRightCursor.gameObject, true);
				return;
			}
			NGUITools.SetActive(mLeftCursor.gameObject, true);
			NGUITools.SetActive(mRightCursor.gameObject, true);
			if (mNowViewPanelId == 0)
			{
				if (mSlideDirection == DIRECTION.RIGHT)
				{
					NGUITools.SetActive(mLeftCursor.gameObject, false);
				}
				else
				{
					NGUITools.SetActive(mRightCursor.gameObject, false);
				}
			}
			else if (mNowViewPanelId == mSlideCount - 1)
			{
				if (mSlideDirection == DIRECTION.RIGHT)
				{
					NGUITools.SetActive(mRightCursor.gameObject, false);
				}
				else
				{
					NGUITools.SetActive(mLeftCursor.gameObject, false);
				}
			}
		}
	}

	private void ChangeOrbStatus()
	{
		if (!mUseOrb)
		{
			return;
		}
		if (mOrbButtonList.Count > 0)
		{
			for (int i = 0; i < mOrbButtonList.Count; i++)
			{
				if (i == mNowViewPanelId)
				{
					mOrbButtonList[i].SetButtonEnable(true);
				}
				else
				{
					mOrbButtonList[i].SetButtonEnable(false);
				}
			}
		}
		else
		{
			if (mOrbSpriteList.Count <= 0)
			{
				return;
			}
			for (int j = 0; j < mOrbSpriteList.Count; j++)
			{
				if (j == mNowViewPanelId)
				{
					mOrbSpriteList[j].color = new Color(1f, 1f, 1f, 1f);
				}
				else
				{
					mOrbSpriteList[j].color = new Color(0.5f, 0.5f, 0.5f, 1f);
				}
			}
		}
	}

	private void CheckAutoMove()
	{
		if (ForceSlideUnable || !mAutoMoveEnable)
		{
			return;
		}
		if (!mEnableButtonNow)
		{
			mAutoMoveStartTime = mAutoMoveSec;
			return;
		}
		mAutoMoveStartTime -= Time.deltaTime;
		if (!(mAutoMoveStartTime > 0f))
		{
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				OnRight_Callback();
			}
			else
			{
				OnLeft_Callback();
			}
		}
	}

	private void OnIndexButtonPushedCallBack(int index)
	{
		if (!ForceSlideUnable && mEnableButtonNow && mCallback != null)
		{
			mCallback(index);
		}
	}

	private void OnOrbButtonPushedCallBack(int index)
	{
		if (!ForceSlideUnable && mEnableButtonNow && mSlideEnable && mNowViewPanelId != index)
		{
			mSlideEnable = false;
			mMovedPanelId = index;
			mState.Reset(SW_STATE.ORB_MOVE_INIT);
		}
	}

	private void OnRight_Callback()
	{
		if (!ForceSlideUnable && mEnableButtonNow && mSlideEnable)
		{
			mSlideEnable = false;
			mMoveDirection = -1;
			mState.Reset(SW_STATE.CURSOR_MOVE_INIT);
		}
	}

	private void OnLeft_Callback()
	{
		if (!ForceSlideUnable && mEnableButtonNow && mSlideEnable)
		{
			mSlideEnable = false;
			mMoveDirection = 1;
			mState.Reset(SW_STATE.CURSOR_MOVE_INIT);
		}
	}

	public void SetEnableButtonAction(bool buttonflg)
	{
		for (int i = 0; i < mColliderList.Count; i++)
		{
			mColliderList[i].enabled = buttonflg;
		}
		mEnableButtonNow = buttonflg;
	}

	private void GetFlickPos(float _directionX, float _directionY)
	{
		if (mEnableButtonNow && (mSlideEnable || mFlickNow))
		{
			if (!mFlickNow)
			{
				mFlickNow = true;
				mSlideEnable = false;
				mTheLastNowPosX = mListRoot.transform.localPosition.x;
			}
			if (_directionX <= mClipX - 1f && _directionX >= (mClipX - 1f) * -1f)
			{
				float x = mTheLastNowPosX + _directionX;
				mListRoot.transform.localPosition = new Vector3(x, 0f, 0f);
			}
		}
	}

	private void EndFlick()
	{
		if (mEnableButtonNow)
		{
			mFlickNow = false;
			mState.Reset(SW_STATE.FLICK_MOVE_INIT);
		}
	}

	private void SetFlickInitPos()
	{
		FlickWindow component = base.gameObject.GetComponent<FlickWindow>();
		if (component != null && !mFlickNow)
		{
			Vector3 vector = Camera.main.WorldToScreenPoint(base.gameObject.transform.position);
			if (mNowGoPos != vector)
			{
				float _posX;
				float _posY;
				GetObjectScreenSize(new Vector3(mClipX, mClipY), out _posX, out _posY);
				component.UpdateInitPos(vector.x - _posX / 2f, vector.y - _posY / 2f, _posX, _posY);
				mNowGoPos = vector;
			}
		}
	}

	private void GetObjectScreenSize(Vector3 pos, out float _posX, out float _posY)
	{
		GameObject gameObject = Util.CreateGameObject("getsize", base.gameObject);
		gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f));
		gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x + pos.x, gameObject.transform.localPosition.y + pos.y);
		_posX = Camera.main.WorldToScreenPoint(gameObject.transform.position).x;
		_posY = Camera.main.WorldToScreenPoint(gameObject.transform.position).y;
		GameMain.SafeDestroy(gameObject);
	}
}
