using System.Collections.Generic;
using System.Linq;

public class SMMapPageData
{
	public string Filename = string.Empty;

	public Def.SERIES Series;

	public int MaxMainStageNum;

	public int MaxSubStageNum;

	public SortedDictionary<int, SMMapSetting> MapDataList = new SortedDictionary<int, SMMapSetting>();

	public SortedDictionary<string, SMRoadBlockSetting> RoadBlockDataList = new SortedDictionary<string, SMRoadBlockSetting>();

	public SortedDictionary<string, SMMapStageSetting> MapStageDataList = new SortedDictionary<string, SMMapStageSetting>();

	public SortedDictionary<string, SMChapterSetting> ChapterDataList = new SortedDictionary<string, SMChapterSetting>();

	public SortedDictionary<int, SMDailyEventSetting> MapDailyEventList = new SortedDictionary<int, SMDailyEventSetting>();

	public SortedDictionary<int, SMEventStarRewardSetting> MapStarRewardList = new SortedDictionary<int, SMEventStarRewardSetting>();

	public SortedDictionary<int, SMEventRewardSetting> EventRewardList = new SortedDictionary<int, SMEventRewardSetting>();

	public SortedDictionary<string, SMEventBingoStageSetting> BingoStageList = new SortedDictionary<string, SMEventBingoStageSetting>();

	public SMMapPageData()
	{
	}

	public SMMapPageData(string a_filename)
	{
		Filename = a_filename;
	}

	public virtual SMMapSetting GetMap(int a_courseNo = 0)
	{
		SMMapSetting value;
		if (MapDataList.TryGetValue(a_courseNo, out value))
		{
			return value;
		}
		return null;
	}

	public virtual List<SMRoadBlockSetting> GetRoadBlockInCourse(int a_course = 0)
	{
		List<SMRoadBlockSetting> list = new List<SMRoadBlockSetting>();
		List<SMRoadBlockSetting> list2 = new List<SMRoadBlockSetting>(RoadBlockDataList.Values);
		list2.Sort((SMRoadBlockSetting a, SMRoadBlockSetting b) => a.RoadBlockID - b.RoadBlockID);
		for (int i = 0; i < list2.Count; i++)
		{
			if (list2[i].mCourseNo == a_course)
			{
				list.Add(list2[i]);
			}
		}
		return list;
	}

	public virtual SMRoadBlockSetting GetRoadBlock(int a_main, int a_sub, int a_course = 0)
	{
		string key = SMMapPageSettingBase.MakeKey(a_main, a_sub);
		SMRoadBlockSetting value;
		if (RoadBlockDataList.TryGetValue(key, out value))
		{
			return value;
		}
		return null;
	}

	public virtual SMRoadBlockSetting GetRoadBlockByRBNo(int a_rbNo, int a_course = 0)
	{
		List<SMRoadBlockSetting> roadBlockInCourse = GetRoadBlockInCourse(a_course);
		for (int i = 0; i < roadBlockInCourse.Count; i++)
		{
			if (roadBlockInCourse[i].RoadBlockID == a_rbNo)
			{
				return roadBlockInCourse[i];
			}
		}
		return null;
	}

	public virtual SMMapStageSetting GetMapStage(int a_main, int a_sub, int a_course = 0)
	{
		string key = SMMapPageSettingBase.MakeKey(a_main, a_sub);
		SMMapStageSetting value;
		if (MapStageDataList.TryGetValue(key, out value))
		{
			return value;
		}
		return null;
	}

	public virtual SMChapterSetting GetChapter(int a_main, int a_sub, int a_course = 0)
	{
		string key = SMMapPageSettingBase.MakeKey(a_main, a_sub);
		SMChapterSetting value;
		if (ChapterDataList.TryGetValue(key, out value))
		{
			return value;
		}
		return null;
	}

	public virtual SMChapterSetting GetChapterSearch(int a_main, int a_course = 0)
	{
		foreach (KeyValuePair<string, SMChapterSetting> chapterData in ChapterDataList)
		{
			if (chapterData.Value.mStageNo <= a_main && a_main <= chapterData.Value.mSubNo)
			{
				return chapterData.Value;
			}
		}
		return null;
	}

	public virtual SMDailyEventSetting GetDailyEvent(int a_id)
	{
		SMDailyEventSetting value;
		if (MapDailyEventList.TryGetValue(a_id, out value))
		{
			return value;
		}
		return null;
	}

	public virtual SMEventStarRewardSetting GetStarRewardEvent(int a_id)
	{
		SMEventStarRewardSetting value;
		if (MapStarRewardList.TryGetValue(a_id, out value))
		{
			return value;
		}
		return null;
	}

	public virtual void Serialize(BIJFileBinaryWriter sw)
	{
		sw.WriteInt((int)Series);
		sw.WriteInt(MaxMainStageNum);
		sw.WriteInt(MaxSubStageNum);
		sw.WriteInt(MapDataList.Count);
		List<int> list = MapDataList.Keys.ToList();
		list.Sort();
		foreach (int item in list)
		{
			SMMapSetting sMMapSetting = MapDataList[item];
			sMMapSetting.Serialize(sw);
		}
		sw.WriteInt(RoadBlockDataList.Count);
		List<string> list2 = RoadBlockDataList.Keys.ToList();
		list2.Sort();
		foreach (string item2 in list2)
		{
			SMRoadBlockSetting sMRoadBlockSetting = RoadBlockDataList[item2];
			sMRoadBlockSetting.Serialize(sw);
		}
		sw.WriteInt(ChapterDataList.Count);
		list2 = ChapterDataList.Keys.ToList();
		list2.Sort();
		foreach (string item3 in list2)
		{
			SMChapterSetting sMChapterSetting = ChapterDataList[item3];
			sMChapterSetting.Serialize(sw);
		}
		sw.WriteInt(MapStageDataList.Count);
		list2 = MapStageDataList.Keys.ToList();
		list2.Sort();
		foreach (string item4 in list2)
		{
			SMMapStageSetting sMMapStageSetting = MapStageDataList[item4];
			sMMapStageSetting.Serialize(sw);
		}
		sw.WriteInt(MapDailyEventList.Count);
		list = MapDailyEventList.Keys.ToList();
		list.Sort();
		foreach (int item5 in list)
		{
			SMDailyEventSetting sMDailyEventSetting = MapDailyEventList[item5];
			sMDailyEventSetting.Serialize(sw);
		}
	}

	public virtual void Deserialize(BIJBinaryReader a_reader)
	{
		Series = (Def.SERIES)a_reader.ReadInt();
		MaxMainStageNum = a_reader.ReadInt();
		MaxSubStageNum = a_reader.ReadInt();
		int num = a_reader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			SMMapSetting sMMapSetting = new SMMapSetting();
			sMMapSetting.SetSeries(Series);
			sMMapSetting.Deserialize(a_reader);
			AddMapData(sMMapSetting);
		}
		num = a_reader.ReadInt();
		for (int j = 0; j < num; j++)
		{
			SMRoadBlockSetting sMRoadBlockSetting = new SMRoadBlockSetting();
			sMRoadBlockSetting.SetSeries(Series);
			sMRoadBlockSetting.Deserialize(a_reader);
			AddRoadBlockData(sMRoadBlockSetting);
		}
		num = a_reader.ReadInt();
		for (int k = 0; k < num; k++)
		{
			SMChapterSetting sMChapterSetting = new SMChapterSetting();
			sMChapterSetting.SetSeries(Series);
			sMChapterSetting.Deserialize(a_reader);
			AddChapterData(sMChapterSetting);
		}
		num = a_reader.ReadInt();
		for (int l = 0; l < num; l++)
		{
			SMMapStageSetting sMMapStageSetting = new SMMapStageSetting();
			sMMapStageSetting.SetSeries(Series);
			sMMapStageSetting.Deserialize(a_reader);
			AddMapStageData(sMMapStageSetting);
		}
		num = a_reader.ReadInt();
		for (int m = 0; m < num; m++)
		{
			SMDailyEventSetting sMDailyEventSetting = new SMDailyEventSetting();
			sMDailyEventSetting.SetSeries(Series);
			sMDailyEventSetting.Deserialize(a_reader);
			AddDailyEventData(sMDailyEventSetting);
		}
	}

	public void AddChapterData(SMChapterSetting a_data)
	{
		if (!ChapterDataList.ContainsKey(a_data.Key))
		{
			ChapterDataList.Add(a_data.Key, a_data);
		}
		else
		{
			ChapterDataList[a_data.Key] = a_data;
		}
	}

	public void AddMapData(SMMapSetting a_data)
	{
		if (!MapDataList.ContainsKey(a_data.mCourseNo))
		{
			MapDataList.Add(a_data.mCourseNo, a_data);
		}
		else
		{
			MapDataList[a_data.mCourseNo] = a_data;
		}
	}

	public void AddRoadBlockData(SMRoadBlockSetting a_data)
	{
		if (!RoadBlockDataList.ContainsKey(a_data.Key))
		{
			RoadBlockDataList.Add(a_data.Key, a_data);
		}
		else
		{
			RoadBlockDataList[a_data.Key] = a_data;
		}
	}

	public void AddStarRewardData(SMEventStarRewardSetting a_data)
	{
		if (!MapStarRewardList.ContainsKey(a_data.StarNum))
		{
			MapStarRewardList.Add(a_data.StarNum, a_data);
		}
		else
		{
			MapStarRewardList[a_data.StarNum] = a_data;
		}
	}

	public void AddEventRewardData(SMEventRewardSetting a_data)
	{
		if (!EventRewardList.ContainsKey(a_data.TotalNum))
		{
			EventRewardList.Add(a_data.TotalNum, a_data);
		}
		else
		{
			EventRewardList[a_data.TotalNum] = a_data;
		}
	}

	public void AddBingoStageData(SMEventBingoStageSetting a_data)
	{
		if (!BingoStageList.ContainsKey(a_data.keyname))
		{
			BingoStageList.Add(a_data.keyname, a_data);
		}
		else
		{
			BingoStageList[a_data.keyname] = a_data;
		}
	}

	public void AddMapStageData(SMMapStageSetting a_data)
	{
		if (!MapStageDataList.ContainsKey(a_data.Key))
		{
			MapStageDataList.Add(a_data.Key, a_data);
			return;
		}
		SMMapStageSetting sMMapStageSetting = MapStageDataList[a_data.Key];
		if (a_data.BoxID.Count > 0)
		{
			sMMapStageSetting.BoxID.Add(a_data.BoxID[0]);
			sMMapStageSetting.BoxKind.Add(a_data.BoxKind[0]);
			sMMapStageSetting.BoxSpawnCondition.Add(a_data.BoxSpawnCondition[0]);
			sMMapStageSetting.BoxSpawnConditionID.Add(a_data.BoxSpawnConditionID[0]);
			sMMapStageSetting.BoxSpawnTileKind.Add(a_data.BoxSpawnTileKind[0]);
			MapStageDataList[a_data.Key] = sMMapStageSetting;
		}
	}

	public void AddDailyEventData(SMDailyEventSetting a_data)
	{
		if (!MapDailyEventList.ContainsKey(a_data.DailyEventNo))
		{
			MapDailyEventList.Add(a_data.DailyEventNo, a_data);
		}
		else
		{
			MapDailyEventList[a_data.DailyEventNo] = a_data;
		}
	}

	public void SwapMapStageData(List<SMMapStageSetting> a_list)
	{
		if (a_list.Count != 0)
		{
			SMMapStageSetting sMMapStageSetting = a_list[0];
			if (MapStageDataList.ContainsKey(sMMapStageSetting.Key))
			{
				MapStageDataList.Remove(sMMapStageSetting.Key);
			}
			AddMapStageData(sMMapStageSetting);
			for (int i = 1; i < a_list.Count; i++)
			{
				AddMapStageData(a_list[i]);
			}
		}
	}

	public static void UpdateMapPageData(SMMapPageData aCurrent, SMMapPageData aUpdate)
	{
		aCurrent.MaxMainStageNum = aUpdate.MaxMainStageNum;
		aCurrent.MaxSubStageNum = aUpdate.MaxSubStageNum;
		foreach (SMMapSetting value in aUpdate.MapDataList.Values)
		{
			aCurrent.AddMapData(value);
		}
		foreach (SMRoadBlockSetting value2 in aUpdate.RoadBlockDataList.Values)
		{
			aCurrent.AddRoadBlockData(value2);
		}
		foreach (SMMapStageSetting value3 in aUpdate.MapStageDataList.Values)
		{
			aCurrent.AddMapStageData(value3);
		}
		foreach (SMChapterSetting value4 in aUpdate.ChapterDataList.Values)
		{
			aCurrent.AddChapterData(value4);
		}
		foreach (SMDailyEventSetting value5 in aUpdate.MapDailyEventList.Values)
		{
			aCurrent.AddDailyEventData(value5);
		}
		foreach (SMEventStarRewardSetting value6 in aUpdate.MapStarRewardList.Values)
		{
			aCurrent.AddStarRewardData(value6);
		}
	}

	public int GetValidCourseCount()
	{
		int num = 0;
		if (MapDataList != null && num < MapDataList.Count)
		{
			num = MapDataList.Count;
		}
		return num;
	}
}
