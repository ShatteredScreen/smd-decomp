using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionGaugeUpDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_00 = 2,
		NETWORK_00_01 = 3,
		NETWORK_01_00 = 4,
		NETWORK_01_01 = 5
	}

	public enum CLEARDATA
	{
		NONE = 0,
		LOSE = 1,
		FIRSTWIN = 2
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private SMVerticalListInfo mListInfo;

	private SMVerticalList mItemList;

	private FriendItem mSelectedFriend;

	private UISprite mGauge;

	private List<int> mMissionNum = new List<int>();

	private ConfirmDialog mConfirmDialog;

	private bool scrollBarPosition = true;

	private UISsSprite mUISsAnime;

	private bool mShineEffect;

	private List<bool> mSkipBool = new List<bool>();

	private int mSkipBoolCount;

	private float mScrollBarPos;

	private List<UIButton> mSkipButtonList = new List<UIButton>();

	public CLEARDATA mCrearData;

	private UIButton mSkipbutton;

	private bool mSkipflg;

	private static bool mCloseFlg = true;

	private float mTap;

	private float mRelease;

	private float mWait = 0.2f;

	private bool mDonePlaySE_Gauge;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (mItemList != null && mItemList.IsContentsCreated && scrollBarPosition)
			{
				mItemList.SetScrollBarPosition(new Vector3(265f, 0f, 0f));
				scrollBarPosition = false;
			}
			if (mItemList != null)
			{
				mScrollBarPos = mItemList.Scroll();
			}
			break;
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		ResImage resImage = ResourceManager.LoadImageAsync("TROPHY");
		while (resImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("TROPHY").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = Localization.Get("FriendList");
		InitDialogFrame(DIALOG_SIZE.LARGE, "DIALOG_BASE", "instruction_panel11");
		UISprite uISprite = Util.CreateSprite("TitleBoard", base.gameObject, image2);
		Util.SetSpriteInfo(uISprite, "panel_mission", mBaseDepth + 7, new Vector3(0f, 289f, 0f), Vector3.one, false);
		uISprite.SetDimensions(407, 70);
		uISprite.type = UIBasicSprite.Type.Sliced;
		UISprite uISprite2 = Util.CreateSprite("TitleIcon", base.gameObject, image2);
		Util.SetSpriteInfo(uISprite2, "icon_trophy", mBaseDepth + 135, new Vector3(-157f, 292f, 0f), Vector3.one, false);
		uISprite2.SetDimensions(95, 95);
		UISprite sprite = Util.CreateSprite("titleName", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "LC_text_mission", mBaseDepth + 135, new Vector3(0f, 290f, 0f), Vector3.one, false);
		SetLayout(Util.ScreenOrientation);
		text = string.Format(Localization.Get("TrophyPage_Desc00"));
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 1, new Vector3(0f, 212f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.54509807f, 0.38431373f, 0.18039216f, 1f);
		mSkipbutton = Util.CreateJellyImageButton("NullSkip", base.gameObject, image);
		Util.SetImageButtonInfo(mSkipbutton, "null", "null", "null", mBaseDepth + 100, new Vector3(0f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSkipbutton, this, "OnSkipPushed", UIButtonMessage.Trigger.OnClick);
		BoxCollider component = mSkipbutton.GetComponent<BoxCollider>();
		component.size = new Vector3(560f, 560f, 1f);
		CreateCancelButton("OnCancelPushed");
		mCloseFlg = false;
	}

	public void OnSkipPushed()
	{
		mSkipflg = true;
		if (mSkipbutton != null)
		{
			UnityEngine.Object.Destroy(mSkipbutton.gameObject);
			mSkipbutton = null;
		}
		for (int i = 0; i < mSkipBool.Count; i++)
		{
			if (mSkipButtonList != null && mSkipButtonList.Count > i && mSkipButtonList[i] != null)
			{
				UnityEngine.Object.Destroy(mSkipButtonList[i].gameObject);
				mSkipButtonList[i] = null;
			}
		}
		mSkipButtonList.Clear();
	}

	public void OnTapPushed()
	{
		OnSkipPushed();
	}

	public override void OnOpenFinished()
	{
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		List<TrophyMissionGuageUpListItem> list2 = new List<TrophyMissionGuageUpListItem>();
		for (int i = 0; i < mMissionNum.Count; i++)
		{
			MissionInfo missionInfo = MissionManager.GetMissionInfo(mMissionNum[i]);
			TrophyMissionGuageUpListItem trophyMissionGuageUpListItem = new TrophyMissionGuageUpListItem();
			List<ShowGuageInfo> showGuageInfo = MissionManager.GetShowGuageInfo(missionInfo);
			trophyMissionGuageUpListItem.data = showGuageInfo;
			trophyMissionGuageUpListItem.id = mMissionNum[i];
			trophyMissionGuageUpListItem.name = missionInfo.mData.mName;
			trophyMissionGuageUpListItem.maxMission = missionInfo.mData.mMaxStep;
			list2.Add(trophyMissionGuageUpListItem);
		}
		for (int j = 0; j < list2.Count; j++)
		{
			list.Add(list2[j]);
		}
		mListInfo = new SMVerticalListInfo(1, 6000f, 412f, 1, 6000f, 455f, 1, 156);
		mItemList = SMVerticalList.Make(base.gameObject, this, mListInfo, list, 0f, -37f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mItemList.OnCreateItem = OnCreateItem;
	}

	private void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("TROPHY").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		TrophyMissionGuageUpListItemData trophyMissionGuageUpListItemData = new TrophyMissionGuageUpListItemData();
		float cellWidth = mItemList.mList.cellWidth;
		float cellHeight = mItemList.mList.cellHeight;
		UIButton uIButton = Util.CreateImageButton("ListSkipButton", itemGO, image);
		Util.SetImageButtonInfo(uIButton, "null", "null", "null", mBaseDepth + 100, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnTapPushed", UIButtonMessage.Trigger.OnPress);
		BoxCollider component = uIButton.GetComponent<BoxCollider>();
		component.size = new Vector3(cellWidth, cellHeight, 1f);
		mSkipButtonList.Add(uIButton);
		TrophyMissionGuageUpListItem trophyMissionGuageUpListItem = (trophyMissionGuageUpListItemData.data = item as TrophyMissionGuageUpListItem);
		trophyMissionGuageUpListItemData.itemGo = itemGO;
		uISprite = Util.CreateSprite("FriendBoard", itemGO, image2);
		Util.SetSpriteInfo(uISprite, "instruction_panel2_lace", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		trophyMissionGuageUpListItemData.boardImage = uISprite.gameObject;
		string text = string.Format(Localization.Get(trophyMissionGuageUpListItem.name), 0);
		UILabel uILabel = Util.CreateLabel("Mission_Title", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 8, new Vector3(-180f, 30f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(253, 50);
		UISprite uISprite2 = Util.CreateSprite("trophyNumBack", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite2, "trophy_panel", mBaseDepth + 8, new Vector3(140f, 33f, 0f), Vector3.one, false);
		UILabel uILabel2 = Util.CreateLabel("Price", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel2, string.Empty + trophyMissionGuageUpListItem.data[0].mReward, mBaseDepth + 9, new Vector3(21f, -7f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel2);
		uISprite2 = Util.CreateSprite("Line", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "line00", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(375, 6);
		UISprite uISprite3 = Util.CreateSprite("redbar", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite3, "instruction_accessory30", mBaseDepth + 3, new Vector3(-222f, 47f, 0f), Vector3.one, false);
		uISprite3.type = UIBasicSprite.Type.Sliced;
		uISprite3.SetDimensions(102, 34);
		uISprite3.transform.localRotation = Quaternion.Euler(0f, 0f, 30f);
		text = ((trophyMissionGuageUpListItem.maxMission == 1) ? string.Format(Localization.Get("TrophyMission_Lv_nothing")) : string.Format(Localization.Get("TrophyMission_Lv"), trophyMissionGuageUpListItem.data[0].mStep));
		uILabel = Util.CreateLabel("LvNum", uISprite3.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 5, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		uILabel.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
		mGauge = Util.CreateSprite("GaugeBase", uISprite.gameObject, image2);
		Util.SetSpriteInfo(mGauge, "gauge_trophy", mBaseDepth + 3, new Vector3(-50.3f, -28f, 0f), Vector3.one, false);
		mGauge.type = UIBasicSprite.Type.Sliced;
		mGauge.SetDimensions(250, 53);
		UISprite uISprite4 = Util.CreateSprite("Fill", mGauge.gameObject, image2);
		Util.SetSpriteInfo(uISprite4, "gauge_trophy_bar", mBaseDepth + 4, new Vector3(1f, -1f, 0f), Vector3.one, false);
		uISprite4.SetDimensions(232, 27);
		uISprite4.type = UIBasicSprite.Type.Filled;
		uISprite4.fillDirection = UIBasicSprite.FillDirection.Horizontal;
		int num2 = trophyMissionGuageUpListItem.data[0].mCurrentValue + trophyMissionGuageUpListItem.data[0].mChangeValue;
		uISprite4.fillAmount = 0f;
		int num3 = num2;
		if (num3 > trophyMissionGuageUpListItem.data[0].mStep)
		{
			num3 = trophyMissionGuageUpListItem.data[0].mStep;
		}
		if (trophyMissionGuageUpListItem.data[0].mStep <= 1)
		{
			StartCoroutine(MoveGauge(trophyMissionGuageUpListItem.data, uISprite4, itemGO, uISprite3, uILabel, uISprite.gameObject));
		}
		else
		{
			float num4 = (float)trophyMissionGuageUpListItem.data[0].mCurrentValue - (float)trophyMissionGuageUpListItem.data[0].mGuageSize;
			float num5 = trophyMissionGuageUpListItem.data[0].mGuageSize;
			StartCoroutine(MoveGauge(trophyMissionGuageUpListItem.data, uISprite4, itemGO, uISprite3, uILabel, uISprite.gameObject));
		}
		uISprite2 = Util.CreateSprite("ato", uISprite.gameObject, image2);
		float num6 = -2f;
		int num7 = 119;
		num6 = -27f;
		num7 = 169;
		Util.SetSpriteInfo(uISprite2, "LC_gauge_trophy2", mBaseDepth + 8, new Vector3(num6, -49f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(num7, 21);
		NumberImageString numberImageString = Util.CreateGameObject("Number", uISprite.gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(6, trophyMissionGuageUpListItem.data[0].mGuageSize - num3, mBaseDepth + 15, 0.5f, UIWidget.Pivot.Right, false, image);
		numberImageString.transform.localPosition = new Vector3(53f, -47f, 0f);
		numberImageString.SetPitch(34f);
		StartCoroutine(MoveNum(trophyMissionGuageUpListItem.data, numberImageString, uILabel2));
	}

	private void OnGetAnimeFinished(UISsSprite sprite)
	{
	}

	private IEnumerator MoveNum(List<ShowGuageInfo> showguage, NumberImageString num, UILabel changeEXNum)
	{
		float angle = 0f;
		int changeNum = 0;
		float value2 = showguage[0].mCurrentValue;
		float changeValue3 = 0f;
		changeValue3 = showguage[0].mChangeValue;
		float maxNum2 = showguage[0].mGuageSize;
		int next2 = 0;
		if (maxNum2 > value2)
		{
			num.SetNum((int)(maxNum2 - value2));
		}
		else
		{
			num.SetNum(0);
		}
		yield return new WaitForSeconds(mWait);
		while (!mCloseFlg)
		{
			if (mSkipflg)
			{
				next2 = showguage.Count - 1;
				value2 = showguage[next2].mCurrentValue;
				maxNum2 = showguage[next2].mGuageSize;
				changeValue3 = showguage[next2].mChangeValue;
				changeEXNum.text = string.Empty + showguage[next2].mReward;
				if (maxNum2 > value2 + changeValue3)
				{
					num.SetNum((int)(maxNum2 - (value2 + changeValue3)));
				}
				else
				{
					num.SetNum(0);
				}
				break;
			}
			if (value2 >= maxNum2)
			{
				next2++;
				if (showguage.Count > next2)
				{
					value2 = showguage[next2].mCurrentValue;
					maxNum2 = showguage[next2].mGuageSize;
					changeValue3 = showguage[next2].mChangeValue;
					changeEXNum.text = string.Empty + showguage[next2].mReward;
					if (changeValue3 == 0f)
					{
						num.SetNum((int)(maxNum2 - value2));
					}
				}
			}
			if (!(changeValue3 > 0f))
			{
				break;
			}
			if (changeValue3 > 1f)
			{
				changeValue3 -= 2f;
				changeNum = 2;
			}
			else if (changeValue3 > 0f)
			{
				changeValue3 -= 1f;
				changeNum = 1;
			}
			value2 += (float)changeNum;
			if (maxNum2 > value2)
			{
				if (num != null)
				{
					num.SetNum((int)(maxNum2 - value2));
				}
			}
			else if (num != null)
			{
				num.SetNum(0);
			}
			yield return null;
		}
	}

	private IEnumerator MoveGauge(List<ShowGuageInfo> showguage, UISprite gauge, GameObject itemGo, UISprite redSprite, UILabel ribbonLabel, GameObject boardGameobject)
	{
		mSkipBool.Add(false);
		int SkipBoolCount = mSkipBoolCount;
		mSkipBoolCount++;
		float angle = 0f;
		float value2 = showguage[0].mCurrentValue;
		float changeValue3 = 0f;
		changeValue3 = showguage[0].mChangeValue;
		float maxNum2 = showguage[0].mGuageSize;
		int next2 = 0;
		gauge.fillAmount = value2 / maxNum2;
		int changeNum = 0;
		bool effect = true;
		UISsSprite uiSsAnime2 = null;
		uiSsAnime2 = Util.CreateGameObject("Charge", itemGo).AddComponent<UISsSprite>();
		uiSsAnime2.Animation = ResourceManager.LoadSsAnimation("EFFECT_CHARGE_TROPHY").SsAnime;
		uiSsAnime2.depth = mBaseDepth + 10;
		uiSsAnime2.transform.localScale = new Vector3(1f, 1f, 1f);
		uiSsAnime2.Play();
		float posX2 = 200f * (value2 / maxNum2);
		if (posX2 >= 200f)
		{
			posX2 = 200f;
		}
		uiSsAnime2.transform.localPosition = new Vector3(-166f + posX2, -28f, 1f);
		yield return new WaitForSeconds(mWait);
		if (uiSsAnime2 != null)
		{
			NGUITools.SetActive(uiSsAnime2.gameObject, true);
		}
		UISsSprite uiRibbonAnime = null;
		if (!mDonePlaySE_Gauge)
		{
			mGame.PlaySe("SE_CLEAR_BONUS_JUMP", -1);
			mDonePlaySE_Gauge = true;
		}
		UILabel label = null;
		string str3 = null;
		UISsSprite uiSsAnimeLvUp = null;
		while (true)
		{
			if (mSkipflg)
			{
				next2 = showguage.Count - 1;
				value2 = showguage[next2].mCurrentValue;
				maxNum2 = showguage[next2].mGuageSize;
				changeValue3 = showguage[next2].mChangeValue;
				gauge.fillAmount = (value2 + changeValue3) / maxNum2;
				if (next2 > 0)
				{
					str3 = string.Format(Localization.Get("TrophyMission_Lv"), showguage[next2].mStep);
					ribbonLabel.text = string.Empty + str3;
					if (label != null)
					{
						label.text = string.Empty + str3;
					}
					if (uiRibbonAnime == null)
					{
						BIJImage trophyAtlas = ResourceManager.LoadImage("TROPHY").Image;
						if (boardGameobject != null)
						{
							UISprite sprite = Util.CreateSprite("trophy_lvup", boardGameobject, trophyAtlas);
							Util.SetSpriteInfo(sprite, "efc_Lvup_font", mBaseDepth + 8, new Vector3(155f, -36f, 0f), Vector3.one, false);
						}
					}
				}
				if (uiSsAnime2 != null)
				{
					NGUITools.SetActive(uiSsAnime2.gameObject, false);
				}
				if (next2 > 0)
				{
					mGame.TrophyGuideWinCompFlg = true;
				}
				yield break;
			}
			if (value2 >= maxNum2)
			{
				mGame.TrophyGuideWinCompFlg = true;
				next2++;
				if (showguage.Count > next2)
				{
					value2 = showguage[next2].mCurrentValue;
					maxNum2 = showguage[next2].mGuageSize;
					changeValue3 = showguage[next2].mChangeValue;
					if (uiRibbonAnime != null)
					{
						UnityEngine.Object.Destroy(uiRibbonAnime.gameObject);
						uiRibbonAnime = null;
					}
					if (uiSsAnimeLvUp != null)
					{
						UnityEngine.Object.Destroy(uiSsAnimeLvUp.gameObject);
						uiSsAnimeLvUp = null;
					}
					if (boardGameobject != null)
					{
						uiSsAnimeLvUp = Util.CreateGameObject("LVUP", boardGameobject).AddComponent<UISsSprite>();
						uiSsAnimeLvUp.Animation = ResourceManager.LoadSsAnimation("EFFECT_TROPHY_GAUGE_LVUP").SsAnime;
						uiSsAnimeLvUp.depth = mBaseDepth + 10;
						uiSsAnimeLvUp.transform.localScale = new Vector3(1f, 1f, 1f);
						uiSsAnimeLvUp.transform.localPosition = new Vector3(155f, -36f, 0f);
						uiSsAnimeLvUp.PlayCount = 1;
						uiSsAnimeLvUp.Play();
					}
					if (redSprite != null)
					{
						Transform child2 = redSprite.transform.FindChild("LvNum");
						if (child2 != null)
						{
							UnityEngine.Object.Destroy(child2.gameObject);
						}
						child2 = null;
						uiRibbonAnime = Util.CreateGameObject("Ribbon", redSprite.gameObject).AddComponent<UISsSprite>();
						uiRibbonAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_TROPHY_RIBBON").SsAnime;
						uiRibbonAnime.depth = mBaseDepth + 20;
						uiRibbonAnime.transform.localScale = new Vector3(1f, 1f, 1f);
						uiRibbonAnime.Play();
						uiRibbonAnime.PlayCount = 1;
					}
					UIFont font = GameMain.LoadFont();
					str3 = string.Format(Localization.Get("TrophyMission_Lv"), showguage[next2].mStep);
					if (uiRibbonAnime != null)
					{
						label = Util.CreateLabel("LvNum", uiRibbonAnime.gameObject, font);
						Util.SetLabelInfo(label, string.Empty + str3, mBaseDepth + 5, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
						label.color = Color.white;
						label.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
						label.depth = mBaseDepth + 21;
						label.transform.localRotation = Quaternion.Euler(0f, 0f, 30f);
						StartCoroutine(UpdateLabelJump(label));
					}
					mGame.PlaySe("SE_GAUGE_MAX", 1);
					if (changeValue3 == 0f)
					{
						gauge.fillAmount = value2 / maxNum2;
					}
				}
			}
			if (!(changeValue3 > 0f))
			{
				break;
			}
			if (changeValue3 > 1f)
			{
				changeValue3 -= 2f;
				changeNum = 2;
			}
			else if (changeValue3 > 0f)
			{
				changeValue3 -= 1f;
				changeNum = 1;
			}
			value2 += (float)changeNum;
			gauge.fillAmount = value2 / maxNum2;
			if (effect)
			{
				effect = false;
			}
			posX2 = 232f * (value2 / maxNum2);
			if (posX2 >= 232f)
			{
				posX2 = 232f;
			}
			if (uiSsAnime2 != null)
			{
				uiSsAnime2.transform.localPosition = new Vector3(-166f + posX2, -28f, 1f);
			}
			mShineEffect = true;
			yield return null;
		}
		if (uiSsAnime2 != null)
		{
			NGUITools.SetActive(uiSsAnime2.gameObject, false);
		}
		mSkipBool[SkipBoolCount] = true;
		bool mSkipButtonDelete = true;
		for (int ab = 0; ab < mSkipBool.Count; ab++)
		{
			if (!mSkipBool[ab])
			{
				mSkipButtonDelete = false;
				break;
			}
		}
		if (mSkipButtonDelete)
		{
			mSkipflg = true;
			if (mSkipbutton != null)
			{
				UnityEngine.Object.Destroy(mSkipbutton.gameObject);
				mSkipbutton = null;
			}
		}
		for (int abc = 0; abc < mSkipButtonList.Count; abc++)
		{
			if (mSkipButtonList.Count > abc && mSkipButtonList[abc] != null)
			{
				UnityEngine.Object.Destroy(mSkipButtonList[abc].gameObject);
				mSkipButtonList[abc] = null;
			}
		}
		mSkipButtonList.Clear();
	}

	private IEnumerator UpdateLabelJump(UILabel Jump)
	{
		float counter2 = 0f;
		bool is_draw = true;
		Vector3 BaseScale = Jump.transform.localScale;
		float delay_timer = 0.1f;
		while (true)
		{
			if (Jump == null)
			{
				yield break;
			}
			if (is_draw)
			{
				if (delay_timer <= 0f)
				{
					counter2 += Time.deltaTime * 540f;
					if (counter2 > 360f)
					{
						break;
					}
					if (Jump != null)
					{
						Vector3 pos = Jump.transform.localScale;
						pos.x = BaseScale.x + Mathf.Abs(Mathf.Sin(counter2 * ((float)Math.PI / 180f))) * 1.3f;
						pos.y = BaseScale.y + Mathf.Abs(Mathf.Sin(counter2 * ((float)Math.PI / 180f))) * 1.3f;
						Jump.transform.localScale = pos;
					}
				}
				else
				{
					delay_timer -= Time.deltaTime;
				}
			}
			else
			{
				delay_timer = 5f;
				counter2 = 0f;
				Jump.transform.localPosition = BaseScale;
			}
			yield return null;
		}
		counter2 = 0f;
		Jump.transform.localScale = BaseScale;
	}

	public void Init(List<int> MissionNum, CLEARDATA creardata)
	{
		mMissionNum = MissionNum;
		mCrearData = creardata;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		}
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
		mCloseFlg = true;
		if (mItemList != null)
		{
			GameMain.SafeDestroy(mItemList.gameObject);
			mItemList = null;
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
