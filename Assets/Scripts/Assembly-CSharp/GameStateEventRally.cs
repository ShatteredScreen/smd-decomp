using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameStateEventRally : GameStateSMMapBase
{
	public enum STATE
	{
		NONE = 0,
		LOAD_WAIT = 1,
		UNLOAD_WAIT = 2,
		INIT = 3,
		UNLOCK_ACTING = 4,
		MAIN = 5,
		DLLISTHASH_WAIT = 6,
		STAGEOPEN_WAIT = 7,
		STAGEUNLOCK_WAIT = 8,
		STAGECLEAR_WAIT = 9,
		ACCESSORIEOPEN_WAIT = 10,
		LINEOPEN_WAIT = 11,
		LINEUNLOCK_WAIT = 12,
		SIGNUNLOCK_WAIT = 13,
		COURSECLEAR_WAIT = 14,
		COURSECOMPLETE_WAIT = 15,
		RBOPEN_WAIT = 16,
		RBUNLOCK_WAIT = 17,
		RBKEYBROKEN_WAIT = 18,
		SERIESOPEN_WAIT = 19,
		AVATARMOVE_WAIT = 20,
		ERBUNLOCK_WAIT = 21,
		NEWAREA_OPEN = 22,
		NEWAREA_OPEN_ANIMEWAIT = 23,
		NEWAREA_OPEN_WAIT = 24,
		NEWAREA_RETURN = 25,
		WAIT = 26,
		PLAY = 27,
		PAUSE = 28,
		STORY_DEMO_FADE = 29,
		STORY_DEMO = 30,
		WEBVIEW = 31,
		WEBVIEW_WAIT = 32,
		TUTORIAL = 33,
		NETWORK_CONNECT_WAIT = 34,
		NETWORK_PROLOGUE = 35,
		NETWORK_MAINTENANCE = 36,
		NETWORK_MAINTENANCE_00 = 37,
		NETWORK_AUTH = 38,
		NETWORK_AUTH_00 = 39,
		NETWORK_00 = 40,
		NETWORK_00_00 = 41,
		NETWORK_01 = 42,
		NETWORK_01_00 = 43,
		NETWORK_02 = 44,
		NETWORK_02_00 = 45,
		NETWORK_03 = 46,
		NETWORK_03_00 = 47,
		NETWORK_04 = 48,
		NETWORK_ADV_GET_MAIL = 49,
		NETWORK_ADV_GET_MAIL_WAIT = 50,
		NETWORK_04_00 = 51,
		NETWORK_04_00_1 = 52,
		NETWORK_04_00_2 = 53,
		NETWORK_04_01 = 54,
		NETWORK_04_01_1 = 55,
		NETWORK_04_01_2 = 56,
		NETWORK_04_02 = 57,
		NETWORK_04_02_1 = 58,
		NETWORK_04_02_2 = 59,
		NETWORK_04_02_3 = 60,
		NETWORK_04_03 = 61,
		NETWORK_04_03_1 = 62,
		NETWORK_04_04 = 63,
		NETWORK_04_99 = 64,
		NETWORK_GETFRIENDINFO = 65,
		NETWORK_GETFRIENDINFO_00 = 66,
		NETWORK_GETCAMPAIGNINFO = 67,
		NETWORK_GETCAMPAIGNINFO_00 = 68,
		NETWORK_EPILOGUE = 69,
		NETWORK_REWARD_CHECK = 70,
		NETWORK_REWARD_CHECK_00 = 71,
		NETWORK_REWARD_MAINTENACE = 72,
		NETWORK_REWARD_MAINTENACE_00 = 73,
		NETWORK_REWARD_SET = 74,
		NETWORK_REWARD_SET_00 = 75,
		NETWORK_RESUME_MAINTENACE = 76,
		NETWORK_RESUME_MAINTENACE_00 = 77,
		NETWORK_RESUME_GAMEINFO = 78,
		NETWORK_RESUME_GAMEINFO_00 = 79,
		NETWORK_RESUME_CAMPAIGN = 80,
		NETWORK_RESUME_CAMPAIGN_00 = 81,
		NETWORK_RESUME_LOGINBONUS = 82,
		NETWORK_RESUME_LOGINBONUS_00 = 83,
		NETWORK_STAGE = 84,
		NETWORK_STAGE_WAIT = 85,
		NETWORK_NOTICE_WAIT = 86,
		AUTHERROR_RETURN_TITLE = 87,
		AUTHERROR_RETURN_WAIT = 88,
		MAINTENANCE_RETURN_TITLE = 89,
		MAINTENANCE_RETURN_WAIT = 90,
		GOTO_REENTER = 91,
		OLD_EVENT_EFFECT_WAIT = 92
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private STATE mRewardErrorPrevState = STATE.MAIN;

	private string mGuID;

	protected int mBundleAccessoryCount;

	protected AccessoryData mOpenPuzzleAccessory;

	protected AccessoryData mOtherOpenPuzzleAccessory;

	protected AccessoryData mAccessory_GrowStamp;

	protected SMMapBG mBGImage;

	protected OneShotAnime mSignAnimation;

	protected OneShotAnime mClearAnimation;

	protected OneShotAnime mCompleteAnimation;

	protected bool mIsNextCourseDialogFlg;

	private EventMiniDialog mEventMiniDialog;

	protected bool mIsFinishedCourseMove;

	protected bool[] mNewCourseNotifyList;

	private EventImageDialog mEventImageDialog;

	private string DEFAULT_EVENT_BGM = "BGM_MAP_F";

	private List<SMEventRallyPage> mCourseList = new List<SMEventRallyPage>();

	private SMEventPageData mEventPageData;

	protected short CurrentCourseID = -1;

	private bool mEffectedOldEvent;

	protected bool mIntroFlg;

	protected int mIntroCourse;

	protected int mIntroAccessory;

	protected bool mPartnerGetInrtoflg;

	private STATE mStateAfterConnect = STATE.NETWORK_EPILOGUE;

	private STATE mBootConnectErrorState;

	public STATE StateStatus = STATE.WAIT;

	public STATE OverwriteState = STATE.WAIT;

	protected bool mStartWebview;

	public float Event_DeviceOffset;

	protected override void RegisterServerCallback()
	{
		Server.GetEventStageRankingDataEvent += Server_OnGetStageRankingData;
	}

	protected override void UnregisterServerCallback()
	{
		Server.GetEventStageRankingDataEvent -= Server_OnGetStageRankingData;
	}

	protected override void Network_OnStage()
	{
		STATE aStatus = STATE.WAIT;
		int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		int currentLevel = playerMapData.CurrentLevel;
		base.RankingData = null;
		mState.Change(STATE.NETWORK_STAGE_WAIT);
		bool flag = false;
		if (!Server.GetEventStageRankingData(currentSeries, currentLevel, CurrentEventID, CurrentCourseID))
		{
			mState.Change(aStatus);
		}
	}

	protected override void Network_OnStageWait()
	{
		_GetRankingData rankingData = base.RankingData;
		if (rankingData != null && rankingData.results.Count > 0)
		{
			mState.Change(STATE.WAIT);
			if (EnableSocialRankingDialog())
			{
				ShowSocialRankingDialog(rankingData.series, rankingData.stage, rankingData.results);
			}
		}
	}

	protected virtual void Server_OnGetStageRankingData(int result, int code, string json)
	{
		if (mState.GetStatus() != STATE.NETWORK_STAGE_WAIT || result != 0)
		{
			return;
		}
		try
		{
			_GetRankingData obj = new _GetRankingData();
			new MiniJSONSerializer().Populate(ref obj, json);
			if (obj != null && obj.results != null)
			{
				for (int num = obj.results.Count - 1; num >= 0; num--)
				{
					if (obj.results[num].uuid == mGame.mPlayer.UUID)
					{
						int num2 = 0;
						PlayerEventData data;
						mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
						PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
						PlayerClearData _psd;
						if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, CurrentCourseID, playerMapData.CurrentLevel, out _psd))
						{
							num2 = _psd.HightScore;
						}
						if (num2 <= 0)
						{
							obj.results.RemoveAt(num);
						}
					}
					else if (!mGame.mPlayer.Friends.ContainsKey(obj.results[num].uuid))
					{
						obj.results.RemoveAt(num);
					}
				}
				obj.results.Sort();
			}
			base.RankingData = obj;
		}
		catch (Exception)
		{
		}
	}

	public override void Start()
	{
		base.Start();
		RegisterServerCallback();
		CurrentEventID = mGame.mPlayer.Data.CurrentEventID;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		CurrentCourseID = data.CourseID;
	}

	public void OnDestroy()
	{
		UnregisterServerCallback();
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public override void Update()
	{
		DragPower = Vector2.zero;
		base.Update();
		if (OverwriteState != STATE.WAIT)
		{
			mState.Change(OverwriteState);
			OverwriteState = STATE.WAIT;
		}
		StateStatus = mState.GetStatus();
		if (mState.IsChanged())
		{
			if (StateStatus == STATE.MAIN)
			{
				if (mCockpit != null)
				{
					mCockpit.SetEnableButtonAction(true);
				}
				if (mCurrentPage != null)
				{
					mCurrentPage.SetEnableButton(true);
				}
			}
			else
			{
				if (mCockpit != null)
				{
					mCockpit.SetEnableButtonAction(false);
				}
				if (mCurrentPage != null)
				{
					mCurrentPage.SetEnableButton(false);
				}
			}
		}
		switch (StateStatus)
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mState.Change(STATE.NETWORK_PROLOGUE);
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_STAGE:
			Network_OnStage();
			break;
		case STATE.NETWORK_STAGE_WAIT:
			Network_OnStageWait();
			break;
		case STATE.NETWORK_CONNECT_WAIT:
			if (!mIsConnecting)
			{
				if (!mHasBootConnectingError)
				{
					mConnectionTime = Time.realtimeSinceStartup;
					mState.Change(STATE.NETWORK_PROLOGUE);
				}
				else if (mBootConnectErrorState != 0)
				{
					mState.Reset(mBootConnectErrorState, true);
				}
			}
			break;
		case STATE.GOTO_REENTER:
			if (GameMain.USE_DEBUG_DIALOG)
			{
				mGameState.SetNextGameState(mGame.GetNextGameState());
				mState.Change(STATE.UNLOAD_WAIT);
			}
			else
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
				mState.Change(STATE.UNLOAD_WAIT);
			}
			break;
		case STATE.INIT:
			if (mState.IsChanged() && MaintenanceModeStartCheck())
			{
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				Update_INIT();
			}
			break;
		case STATE.UNLOCK_ACTING:
			if (mState.IsChanged() && MaintenanceModeStartCheck())
			{
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				Update_UNLOCK_ACTING();
			}
			break;
		case STATE.MAIN:
			if (mState.IsChanged())
			{
				if (MaintenanceModeStartCheck())
				{
					mState.Reset(STATE.WAIT, true);
					break;
				}
				switch (DLFilelistHashCheck())
				{
				case DLFILE_CHECK_RESULT.FILE_DESTROY:
					mState.Reset(STATE.WAIT, true);
					break;
				case DLFILE_CHECK_RESULT.HASH_NG:
					mState.Reset(STATE.WAIT, true);
					break;
				default:
					if (!CheaterStartCheck() && !WebviewStartCheck() && !GiftStartCheck() && !RankingStartCheck() && !RankingRankUpStarStartCheck() && !OldEventEffectCheck())
					{
						UpdateCockpit(false);
					}
					break;
				}
			}
			else
			{
				if (mSocialRanking != null)
				{
					mSocialRanking.Close();
					mSocialRanking = null;
				}
				Update_MAIN();
				UpdateMugenHeartStatus();
			}
			break;
		case STATE.SERIESOPEN_WAIT:
			Update_CourseOpen();
			break;
		case STATE.STAGEOPEN_WAIT:
		case STATE.STAGEUNLOCK_WAIT:
		case STATE.STAGECLEAR_WAIT:
		case STATE.ACCESSORIEOPEN_WAIT:
		case STATE.LINEOPEN_WAIT:
		case STATE.LINEUNLOCK_WAIT:
		case STATE.SIGNUNLOCK_WAIT:
		case STATE.RBOPEN_WAIT:
		case STATE.AVATARMOVE_WAIT:
		case STATE.ERBUNLOCK_WAIT:
			if (mCurrentPage.IsEffectFinished())
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case STATE.RBUNLOCK_WAIT:
		{
			if (!mCurrentPage.IsEffectFinished())
			{
				break;
			}
			int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
			int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
			int a_main;
			int a_sub;
			Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
			SMChapterSetting chapterSearch = mMapPageData.GetChapterSearch(a_main + 1);
			if (chapterSearch != null)
			{
				int stageNo = Def.GetStageNo(chapterSearch.mSubNo, 0);
				mCurrentPage.OpenNewStageUntilNextChapter(playableMaxLevel, stageNo);
				if (mGame.mPlayer.StageOpenList.Count > 0)
				{
					mIsOpenNewChapter = true;
				}
			}
			mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, -1);
			if (mDemoNoAfterRBUnlock != -1 && !mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, mDemoNoAfterRBUnlock))
			{
				StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
				storyDemoTemp.DemoIndex = mDemoNoAfterRBUnlock;
				storyDemoTemp.SelectStage = -1;
				storyDemoTemp.ClearStage = -1;
				mShowStoryDemoList.Add(storyDemoTemp);
				mDemoNoAfterRBUnlock = -1;
			}
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		}
		case STATE.PLAY:
		{
			mGame.mReplay = false;
			mGame.ClearPresentBox();
			if (mGame.mEventMode)
			{
				mGame.mSkinIndex = mGame.mEventSkinIndex;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mPlayer.Data.SelectedCompanion;
				}
			}
			else
			{
				mGame.mSkinIndex = mGame.mPlayer.Data.SelectedSkin;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mPlayer.Data.SelectedCompanion;
				}
			}
			int playerIcon = mGame.mPlayer.Data.PlayerIcon;
			if (mGame.mPlayer.IsCompanionUnlock(mGame.mPlayer.Data.SelectedCompanion) && mGame.mLastCompanionSetting)
			{
				mGame.mPlayer.Data.LastUsedIcon = mGame.mPlayer.Data.SelectedCompanion;
				if (!mGame.mPlayer.Data.LastUsedIconCancelFlg)
				{
					if (mGame.mPlayer.Data.PlayerIcon >= 10000)
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion + 10000;
					}
					else
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion;
					}
				}
				if (playerIcon != mGame.mPlayer.Data.PlayerIcon)
				{
					mGame.Network_NicknameEdit(string.Empty);
				}
			}
			if (playerIcon != mGame.mPlayer.Data.PlayerIcon)
			{
				mGame.Network_NicknameEdit(string.Empty);
			}
			Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
			int stageNumber = mGame.mCurrentStage.StageNumber;
			short a_course;
			int a_main2;
			int a_sub2;
			Def.SplitEventStageNo(stageNumber, out a_course, out a_main2, out a_sub2);
			mCurrentPage.SetAvatarNode(Def.GetStageNo(a_main2, a_sub2));
			mGame.AlreadyConnectAccessoryList.Clear();
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.PUZZLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		}
		case STATE.STORY_DEMO_FADE:
			if (mGameState.IsFadeFinished)
			{
				mGameState.FadeIn();
				if (mCurrentDemoIndex != -1)
				{
					StoryDemoManager storyDemoManager = Util.CreateGameObject("DemoManager", base.gameObject).AddComponent<StoryDemoManager>();
					storyDemoManager.ChangeStoryDemo(mCurrentDemoIndex, OnStoryDemoFinished);
					mCurrentDemoIndex = -1;
					StartCoroutine(GrayIn(1f, 0.8f));
					mState.Change(STATE.STORY_DEMO);
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			break;
		case STATE.WEBVIEW:
			OnHowToDialog();
			break;
		case STATE.TUTORIAL:
			if (mCurrentPage != null)
			{
				mCurrentPage.SetEnableButton(true);
			}
			break;
		case STATE.NETWORK_PROLOGUE:
			if (!GameMain.CanCommunication())
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
				break;
			}
			if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				Server.ManualConnecting = true;
			}
			else if (mTipsScreenDisplayed)
			{
				mGameState.SetLoadingUISsAnimation(2);
			}
			mState.Change(STATE.NETWORK_MAINTENANCE);
			break;
		case STATE.NETWORK_MAINTENANCE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_MAINTENANCE_00);
			break;
		case STATE.NETWORK_MAINTENANCE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_AUTH);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 36);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_AUTH:
			mGame.Network_UserAuth();
			mState.Change(STATE.NETWORK_AUTH_00);
			break;
		case STATE.NETWORK_AUTH_00:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00);
					break;
				}
				mGame.FinishConnecting();
				mState.Change(STATE.AUTHERROR_RETURN_TITLE);
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode mAdvGetMailErrorCode = mUserAuthErrorCode;
			if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 38, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 38, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_00:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_00_00);
			break;
		case STATE.NETWORK_00_00:
			if (GameMain.mGameProfileCheckDone)
			{
				if (GameMain.mGameProfileSucceed)
				{
					mState.Change(STATE.NETWORK_01);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 40, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_01:
			mGame.Network_GetSegmentProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_01_00);
			break;
		case STATE.NETWORK_01_00:
		{
			if (!GameMain.mSegmentProfileCheckDone)
			{
				break;
			}
			if (GameMain.mSegmentProfileSucceed)
			{
				mState.Change(STATE.NETWORK_02);
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode2 = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode mAdvGetMailErrorCode = mUserAuthErrorCode2;
			if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 42, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 42, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_02:
			mState.Change(STATE.NETWORK_03);
			break;
		case STATE.NETWORK_03:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_03_00);
			break;
		case STATE.NETWORK_03_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				mGame.DoLoginBonusServerCheck = false;
				mState.Change(STATE.NETWORK_04);
			}
			else
			{
				Server.ErrorCode mLoginBonusErrorCode = GameMain.mLoginBonusErrorCode;
				Server.ErrorCode mAdvGetMailErrorCode = mLoginBonusErrorCode;
				if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 46, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 46, OnConnectErrorAuthRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.NETWORK_04:
			mGame.mGotGiftIds = new List<int>();
			mGame.mGotApplyIds = new List<int>();
			mGame.mGotSupportIds = new List<int>();
			mGame.mGotSupportPurchaseIds = new List<int>();
			if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				mState.Change(STATE.NETWORK_04_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_02);
			}
			break;
		case STATE.NETWORK_ADV_GET_MAIL:
		{
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			mGame.Network_AdvGetMail(mGuID, is_retry);
			mState.Change(STATE.NETWORK_ADV_GET_MAIL_WAIT);
			break;
		}
		case STATE.NETWORK_ADV_GET_MAIL_WAIT:
		{
			if (!GameMain.mAdvGetMailCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetMailSucceed)
			{
				mGuID = null;
				mState.Change(STATE.NETWORK_04_00);
				break;
			}
			StartCoroutine(GrayOut());
			Server.ErrorCode mAdvGetMailErrorCode = GameMain.mAdvGetMailErrorCode;
			if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 49, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 49, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_04_00:
		{
			STATE aStatus2 = STATE.NETWORK_04_00_1;
			if (mGame.MapNetwork_SupportCheck())
			{
				aStatus2 = STATE.NETWORK_04_01;
			}
			mState.Change(aStatus2);
			break;
		}
		case STATE.NETWORK_04_00_1:
			if (GameMain.mMapSupportCheckDone)
			{
				mState.Change(STATE.NETWORK_04_00_2);
			}
			break;
		case STATE.NETWORK_04_00_2:
			mGame.MapNetwork_SupportGot();
			mState.Change(STATE.NETWORK_04_01);
			break;
		case STATE.NETWORK_04_01:
		{
			STATE aStatus = STATE.NETWORK_04_01_1;
			if (mGame.MapNetwork_GiftCheck())
			{
				aStatus = STATE.NETWORK_04_02;
			}
			mState.Change(aStatus);
			break;
		}
		case STATE.NETWORK_04_01_1:
			if (GameMain.mMapGiftCheckDone)
			{
				mState.Change(STATE.NETWORK_04_01_2);
			}
			break;
		case STATE.NETWORK_04_01_2:
			mGame.MapNetwork_GiftGot();
			mState.Change(STATE.NETWORK_04_02);
			break;
		case STATE.NETWORK_04_02:
		{
			STATE aStatus4 = STATE.NETWORK_04_02_1;
			if (mGame.MapNetwork_ApplyCheck())
			{
				aStatus4 = STATE.NETWORK_04_03;
			}
			mState.Change(aStatus4);
			break;
		}
		case STATE.NETWORK_04_02_1:
			if (GameMain.mMapApplyCheckDone)
			{
				mState.Change(STATE.NETWORK_04_02_2);
			}
			break;
		case STATE.NETWORK_04_02_2:
			mGame.MapNetwork_ApplyGot();
			if (GameMain.mMapApplyRepeat)
			{
				mState.Change(STATE.NETWORK_04_02_3);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_03);
			}
			break;
		case STATE.NETWORK_04_02_3:
		{
			long num = DateTimeUtil.BetweenMilliseconds(mGame.mPlayer.Data.LastGetApplyTime, DateTime.Now);
			if (num > GameMain.mMapApplyRepeatWait)
			{
				mGame.mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
				mState.Change(STATE.NETWORK_04_02);
			}
			break;
		}
		case STATE.NETWORK_04_03:
			if (GameMain.NO_NETWORK)
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			else if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				STATE aStatus3 = STATE.NETWORK_04_03_1;
				if (mGame.MapNetwork_SupportPurchaseCheck())
				{
					aStatus3 = STATE.NETWORK_04_04;
				}
				mState.Change(aStatus3);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_03_1:
			if (GameMain.mMapSupportPurchaseCheckDone)
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_04:
			mState.Reset(STATE.NETWORK_04_99, true);
			break;
		case STATE.NETWORK_04_99:
		{
			mState.Change(STATE.NETWORK_GETFRIENDINFO);
			if (mGame.mFbUserMngGift == null)
			{
				mGame.mFbUserMngGift = FBUserManager.CreateInstance();
			}
			List<GiftItemData> list2 = new List<GiftItemData>();
			if (mGame.mPlayer.Gifts.Count <= 0)
			{
				break;
			}
			foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
			{
				GiftItem value = gift.Value;
				list2.Add(value.Data);
			}
			mGame.mFbUserMngGift.SetFBUserList(list2);
			break;
		}
		case STATE.NETWORK_GETFRIENDINFO:
			mGame.mFriendManager = new GameFriendManager(mGame);
			if (!mGame.mFriendManager.Start())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mState.Change(STATE.NETWORK_GETFRIENDINFO_00);
			}
			break;
		case STATE.NETWORK_GETFRIENDINFO_00:
			if (mGame.mFriendManager == null || mGame.mFriendManager.IsFinished())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mGame.mFriendManager.Update();
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO:
			if (mGame.Network_CampaignCheck())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_EPILOGUE:
			if (mGame.mNetworkDataModified)
			{
				mGame.Save();
			}
			if (mGame.mGotGiftIds != null)
			{
				mGame.mGotGiftIds.Clear();
			}
			if (mGame.mGotSupportIds != null)
			{
				mGame.mGotSupportIds.Clear();
			}
			if (mGame.mGotApplyIds != null)
			{
				mGame.mGotApplyIds.Clear();
			}
			if (mGame.mGotSupportPurchaseIds != null)
			{
				mGame.mGotSupportPurchaseIds.Clear();
			}
			if (mTipsScreenDisplayed)
			{
				mGameState.TipsScreenOff();
			}
			else
			{
				Server.ManualConnecting = false;
			}
			mState.Change(STATE.INIT);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_REWARD_MAINTENACE_00);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_REWARD_SET);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 72);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_REWARD_SET:
			Server.ManualConnecting = true;
			mGame.Network_RewardSet(AccessoryConnectIndex);
			mState.Reset(STATE.NETWORK_REWARD_SET_00, true);
			break;
		case STATE.NETWORK_REWARD_SET_00:
		{
			if (!GameMain.mRewardSetCheckDone)
			{
				break;
			}
			Server.ManualConnecting = false;
			string empty = string.Empty;
			string empty2 = string.Empty;
			if (GameMain.mRewardSetSucceed)
			{
				AccessoryData accessoryData = mGame.GetAccessoryData(AccessoryConnectIndex);
				bool flag = mGame.mPlayer.SubAccessoryConnect(accessoryData);
				AccessoryConnectIndex = 0;
				mGame.Save();
				if (GameMain.mRewardSetStatus == 0)
				{
					int index = accessoryData.Index;
					if (index != 0)
					{
						int cur_series = (int)mGame.mPlayer.Data.CurrentSeries;
						int get_type = 4;
						if (mGame.IsDailyChallengePuzzle)
						{
							cur_series = 200;
						}
						ServerCram.GetAccessory(index, cur_series, get_type);
					}
					if (flag)
					{
						mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
						mState.Reset(STATE.NETWORK_04_03, true);
					}
					else
					{
						mState.Reset(STATE.UNLOCK_ACTING, true);
					}
					break;
				}
				empty = Localization.Get("GotError_Title");
				empty2 = Localization.Get("GotError_Desc");
				if (mAutoUnlockAccessoryList.Contains(accessoryData))
				{
					mAutoUnlockAccessoryList.Remove(accessoryData);
				}
				if (GameStateSMMapBase.mIsShowErrorDialog)
				{
					StartCoroutine(GrayOut());
					GetRewardDialog getRewardDialog = Util.CreateGameObject("NoGetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
					List<AccessoryData> list = new List<AccessoryData>();
					list.Add(accessoryData);
					getRewardDialog.Init(list, true);
					getRewardDialog.SetClosedCallback(OnGetRewardDialogErrorClosed);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					mState.Reset(STATE.UNLOCK_ACTING, true);
				}
			}
			else
			{
				empty = Localization.Get("Network_Error_Title");
				empty2 = Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b");
				if (GameStateSMMapBase.mIsShowErrorDialog)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 74, OnConnectErrorAccessoryRetry, OnConnectErrorAccessoryAbandon);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					OnConnectErrorAccessoryAbandon();
				}
			}
			break;
		}
		case STATE.NETWORK_RESUME_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_MAINTENACE_00);
			break;
		case STATE.NETWORK_RESUME_MAINTENACE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_RESUME_GAMEINFO);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 76);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_GAMEINFO:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_GAMEINFO_00);
			break;
		case STATE.NETWORK_RESUME_GAMEINFO_00:
			if (GameMain.mGameProfileCheckDone)
			{
				if (GameMain.mGameProfileSucceed)
				{
					mState.Change(STATE.NETWORK_RESUME_CAMPAIGN);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 78, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN:
			if (PurchaseFullDialog.IsShow())
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			else if (mGame.Network_CampaignCheck())
			{
				mState.Change(STATE.NETWORK_RESUME_CAMPAIGN_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_RESUME_LOGINBONUS_00);
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				mGame.DoLoginBonusServerCheck = false;
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
					mState.Change(STATE.NETWORK_04);
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 82, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.AUTHERROR_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Transfer_CertError_Title"), Localization.Get("Transfered_TitleBack_Desc2"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.AUTHERROR_RETURN_WAIT);
			break;
		case STATE.MAINTENANCE_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("MaintenanceMode_ReturnTitle_Title"), Localization.Get("MaintenanceMode_ReturnTitle_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.MAINTENANCE_RETURN_WAIT);
			mGame.IsTitleReturnMaintenance = true;
			break;
		case STATE.OLD_EVENT_EFFECT_WAIT:
		{
			EventCockpit eventCockpit = mCockpit as EventCockpit;
			if (!(eventCockpit != null) || !eventCockpit.IsOldEventPlutoEffecting)
			{
				mEffectedOldEvent = true;
				mState.Change(STATE.MAIN);
			}
			break;
		}
		}
		UpdateMapPages();
		mState.Update();
	}

	public override IEnumerator LoadGameState()
	{
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		mTipsScreenDisplayed = false;
		if (mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			mTipsScreenDisplayed = true;
			yield return StartCoroutine(mGameState.CoTipsScreenOn(1, false));
			float temp_start = Time.realtimeSinceStartup;
			float span_time = Time.realtimeSinceStartup - _start;
			while (span_time < 0.5f)
			{
				span_time = Time.realtimeSinceStartup - _start;
				yield return 0;
			}
		}
		yield return StartCoroutine(LoadCommonResources());
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		GameObject parent = mRoot;
		mAnchorBottom = Util.CreateAnchorWithChild("MapAnchorBottom", parent, UIAnchor.Side.Bottom, camera).gameObject;
		mAnchorRight = Util.CreateAnchorWithChild("MapAnchorRight", parent, UIAnchor.Side.Right, camera).gameObject;
		mAnchorCenter = Util.CreateAnchorWithChild("MapAnchorCenter", parent, UIAnchor.Side.Center, camera).gameObject;
		mMapPageRoot = Util.CreateGameObject("MapPageRoot", mAnchorCenter.gameObject);
		ResourceManager.LoadImage("FADE");
		mMapPageData = GameMain.GetCurrentEventPageData();
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		DEFAULT_EVENT_BGM = eventPageData.EventSetting.MapEventBGM;
		if (DEFAULT_EVENT_BGM == string.Empty)
		{
			DEFAULT_EVENT_BGM = "BGM_MAP";
		}
		ResSound resSound = ResourceManager.LoadSoundAsync(DEFAULT_EVENT_BGM);
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mNewCourseNotifyList = new bool[eventPageData.EventCourseList.Count];
		for (int j = 0; j < mNewCourseNotifyList.Length; j++)
		{
			mNewCourseNotifyList[j] = false;
		}
		Player p = mGame.mPlayer;
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		PlayerEventData eventData;
		p.Data.GetMapData(currentSeries, CurrentEventID, out eventData);
		PlayerMapData data = eventData.CourseData[CurrentCourseID];
		if (data.NextRoadBlockLevel == 0)
		{
			foreach (KeyValuePair<string, SMRoadBlockSetting> roadBlockData in eventPageData.RoadBlockDataList)
			{
				SMEventRoadBlockSetting s = roadBlockData.Value as SMEventRoadBlockSetting;
				if (s == null || s.mCourseNo != CurrentCourseID || s.mSubNo != 0)
				{
					continue;
				}
				int nextRB = s.RoadBlockLevel;
				data.NextRoadBlockLevel = nextRB;
				break;
			}
			data.MaxLevel = Def.GetStageNo(mMapPageData.GetMap(CurrentCourseID).MaxLevel, 0);
			eventData.CourseData[CurrentCourseID] = data;
			mGame.mPlayer.Data.SetMapData(CurrentEventID, eventData);
		}
		mPageMoveLock = Def.PRESS.NONE;
		mPageMoved = false;
		SetScrollPower(0f);
		ResImage resImage = ResourceManager.LoadImageAsync(eventPageData.EventSetting.MapAtlasKey);
		while (resImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		int eventID = mGame.mPlayer.Data.CurrentEventID;
		SMSeasonEventSetting eventsetting = mGame.mEventData.InSessionEventList[eventID];
		ResImage bannerAtlas = ResourceManager.LoadImageAsync(eventsetting.BannerAtlas);
		while (bannerAtlas.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		BIJImage atlasMap = ResourceManager.LoadImage(eventPageData.EventSetting.MapAtlasKey).Image;
		mBGImage = Util.CreateGameObject("EventBG", mMapPageRoot).AddComponent<SMEventBG>();
		string backname = eventPageData.EventSetting.MapBackName;
		mBGImage.Active(atlasMap, backname, new Vector3(0f, 0f, Def.MAP_BASE_Z), new Vector2(2f, 2f));
		List<int> courseList = new List<int>(eventPageData.EventCourseList.Keys);
		courseList.Sort();
		foreach (int course in courseList)
		{
			int courseID = course;
			SMEventCourseSetting setting = eventPageData.EventCourseList[courseID];
			SMEventRallyPage page = Util.CreateGameObject("NewMapPage_" + courseID, mMapPageRoot).AddComponent<SMEventRallyPage>();
			page.SetGameState(this);
			yield return StartCoroutine(page.Init(currentSeries, CurrentEventID, (short)setting.mCourseNo, data.MaxLevel, OnStageButtonPushed, OnMapAccessoryTapped, OnMapRoadBlockTapped, null, null, eventPageData));
			CheckAccessoryDisplay(page);
			yield return StartCoroutine(page.ActiveObjectInCamera(0f));
			InitOpenNewStage(page);
			page.transform.localPosition = new Vector3((float)courseID * 1136f, page.transform.localPosition.y, page.transform.localPosition.z);
			page.SetEnableButton(false);
			mCourseList.Add(page);
			NGUITools.SetActive(page.gameObject, false);
		}
		for (int i = 0; i < mCourseList.Count; i++)
		{
			float x = mCourseList[i].gameObject.transform.localPosition.x - 1136f * (float)CurrentCourseID;
			float y = mCourseList[i].gameObject.transform.localPosition.y;
			float z = mCourseList[i].gameObject.transform.localPosition.z;
			mCourseList[i].gameObject.transform.localPosition = new Vector3(x, y, z);
		}
		yield return null;
		mCurrentPage = mCourseList[CurrentCourseID];
		NGUITools.SetActive(mCurrentPage.gameObject, true);
		mMaxPage = mCurrentPage.MaxPage;
		mMaxAvailablePage = mCurrentPage.MaxAvailablePage;
		mMaxModulePage = mCurrentPage.MaxModulePageFromMaxLevel;
		CheckOpenNewStage();
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		ResImage image = ResourceManager.LoadImageAsync("HUD2", true);
		ResSsAnimation anim = ResourceManager.LoadSsAnimationAsync("EFFECT_SHINE", true);
		UIFont font = GameMain.LoadFont();
		mGame.mUseBooster0 = false;
		mGame.mUseBooster1 = false;
		mGame.mUseBooster2 = false;
		mGame.mUseBooster3 = false;
		mGame.mUseBooster4 = false;
		mGame.mUseBooster5 = false;
		SetDeviceScreen(Util.ScreenOrientation);
		float cameraY = ClearedStagePosition();
		yield return StartCoroutine(DirectMapMove(0f - cameraY));
		UpdateMapPages();
		float y2 = mCurrentPage.transform.localPosition.y;
		SetLayout(Util.ScreenOrientation);
		UpdateMapPages();
		CheckRewardOnEnterMap();
		if (!mIsPlayingBGM && mShowStoryDemoList.Count == 0 && resSound.mAudioClip != null)
		{
			mGame.PlayMusic(DEFAULT_EVENT_BGM, 0, true, false, true);
			mGame.SetMusicVolume(1f, 0);
			mIsPlayingBGM = true;
		}
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		yield return Resources.UnloadUnusedAssets();
		ResourceManager.EnableLoadAllResourceFromAssetBundle();
		LoadGameStateFinished();
	}

	public override IEnumerator UnloadGameState()
	{
		if (mTipsScreenDisplayed)
		{
			mGameState.TipsScreenOff();
		}
		else
		{
			Server.ManualConnecting = false;
		}
		ResourceManager.UnloadSound(DEFAULT_EVENT_BGM);
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		if (mCockpit != null)
		{
			GameMain.SafeDestroy(mCockpit.gameObject);
			mCockpit = null;
		}
		yield return StartCoroutine(mCurrentPage.DeactiveAllObject());
		UnityEngine.Object.Destroy(mAnchorBottom.gameObject);
		mAnchorBottom = null;
		UnityEngine.Object.Destroy(mAnchorRight.gameObject);
		mAnchorRight = null;
		UnityEngine.Object.Destroy(mAnchorCenter.gameObject);
		mAnchorCenter = null;
		UnityEngine.Object.Destroy(mCurrentPage.gameObject);
		UnityEngine.Object.Destroy(mRoot);
		ResourceManager.UnloadSsAnimationAll();
		ResourceManager.UnloadImageAll();
		mGame.UnloadNotPreloadSoundResources(true);
		UnloadGameStateFinished();
		yield return 0;
	}

	protected override void Update_INIT()
	{
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		int lastClearedLevel = playerMapData.LastClearedLevel;
		if (lastClearedLevel > 0)
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(lastClearedLevel, out a_main, out a_sub);
			SMEventStageSetting sMEventStageSetting = mMapPageData.GetMapStage(a_main, a_sub, CurrentCourseID) as SMEventStageSetting;
			int num = -1;
			if (sMEventStageSetting != null)
			{
				int storyDemoClear = sMEventStageSetting.GetStoryDemoClear();
				if (storyDemoClear != -1 && !mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, storyDemoClear))
				{
					num = storyDemoClear;
				}
				if (sMEventStageSetting.StageEnterCondition == "PARTNER_F" && sMEventStageSetting.EnterPartners.Count == 1)
				{
					int num2 = sMEventStageSetting.EnterPartners[0];
					if (!mGame.mPlayer.IsCompanionUnlock(num2))
					{
						AccessoryData accessoryData = null;
						List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
						for (int i = 0; i < accessoryByType.Count; i++)
						{
							AccessoryData accessoryData2 = accessoryByType[i];
							if (accessoryData2.GetCompanionID() == num2)
							{
								accessoryData = accessoryData2;
								break;
							}
						}
						if (accessoryData != null)
						{
							mAutoUnlockPartner = accessoryData;
						}
					}
				}
			}
			if (num != -1 && !mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, num))
			{
				StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
				storyDemoTemp.DemoIndex = num;
				storyDemoTemp.SelectStage = -1;
				storyDemoTemp.ClearStage = lastClearedLevel;
				mShowStoryDemoList.Add(storyDemoTemp);
			}
			mGame.mPlayer.StageClearList.Add(lastClearedLevel);
			playerMapData.LastClearedLevel = -1;
		}
		UpdateCockpit(true, false);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void Update_UNLOCK_ACTING()
	{
		if (!isLoadFinished() || !mGameState.IsFadeFinished)
		{
			return;
		}
		Player mPlayer = mGame.mPlayer;
		if (mIntroFlg)
		{
			mIntroFlg = false;
			OnIntroAnimationFinished();
		}
		else if (CurrentCourseID < mNewCourseNotifyList.Length && mNewCourseNotifyList[CurrentCourseID])
		{
			mNewCourseNotifyList[CurrentCourseID] = false;
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			string courseClearDescKey = sMEventPageData.EventCourseList[CurrentCourseID].CourseClearDescKey;
			StartCoroutine(GrayOut());
			mEventMiniDialog = Util.CreateGameObject("NextCourseDialog", mRoot).AddComponent<EventMiniDialog>();
			mEventMiniDialog.Init("LC_event_instruction_text01", Localization.Get("SeasonEvent_CourseClearAF_Desc"));
			mEventMiniDialog.SetClosedCallback(OnCourseNotifyDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else if (mGame.DoLoginBonusServerCheck)
		{
			mState.Reset(STATE.NETWORK_RESUME_MAINTENACE, true);
		}
		else if (AccessoryConnectCheck())
		{
			mState.Reset(STATE.NETWORK_REWARD_MAINTENACE, true);
		}
		else if (StoryDemoStartCheck())
		{
			mCurrentStoryDemo = mShowStoryDemoList[0];
			mShowStoryDemoList.RemoveAt(0);
			if (mCurrentStoryDemo.DemoIndex != -1)
			{
				StartStoryDemo(mCurrentStoryDemo.DemoIndex);
			}
		}
		else if (mGame.mPlayer.StageClearList.Count > 0)
		{
			mGame.mPlayer.StageClearList.Sort();
			int num = mGame.mPlayer.StageClearList[0];
			mGame.mPlayer.StageClearList.RemoveAt(0);
			int a_main;
			int a_sub;
			Def.SplitStageNo(num, out a_main, out a_sub);
			mCurrentPage.StartStageClearEffect(num);
			mGame.mPlayer.SetStageStatus(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, num, Player.STAGE_STATUS.CLEAR);
			mState.Change(STATE.STAGECLEAR_WAIT);
		}
		else if (mGame.GetPresentBoxNum() > 0)
		{
			StartCoroutine(GrayOut());
			mOpenPuzzleAccessory = mGame.GetPresentBox(0);
			int num2 = mGame.GetPresentBoxNum();
			if (mOpenPuzzleAccessory != null)
			{
				if (num2 > 1)
				{
					while (num2 > 1)
					{
						mOtherOpenPuzzleAccessory = mGame.GetPresentBox(num2 - 1);
						if (mOpenPuzzleAccessory.AccessoryType == mOtherOpenPuzzleAccessory.AccessoryType)
						{
							mBundleAccessoryCount++;
							mGame.RemovePresentBox(mOtherOpenPuzzleAccessory);
						}
						num2--;
					}
					mGame.RemovePresentBox(mOpenPuzzleAccessory);
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.Init(mOpenPuzzleAccessory, mBundleAccessoryCount, false);
					mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
					mBundleAccessoryCount = 0;
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.RemovePresentBox(mOpenPuzzleAccessory);
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.Init(mOpenPuzzleAccessory, mBundleAccessoryCount, false);
					mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
					mState.Change(STATE.WAIT);
				}
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
		}
		else if (mGame.mPlayer.HeartUpPiece >= 15)
		{
			StartCoroutine(GrayOut());
			mGame.mPlayer.SubHeartPiece(15);
			GiftItem giftItem = new GiftItem();
			giftItem.Data.Message = Localization.Get("Gift_RatingReward");
			giftItem.Data.ItemCategory = 1;
			giftItem.Data.ItemKind = 1;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = 1;
			mGame.mPlayer.AddGift(giftItem);
			ServerCram.ArcadeGetItem(1, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 2, 1, 30, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
			mGame.Save();
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init("icon_heart", Localization.Get("GetAccessory_Item_Title"), string.Format(Localization.Get("GetAccessory_HEART_Desc"), 1));
			mGetAccessoryDialog.SetClosedCallback(OnGetHeartPieaceDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else if (mCurrentPage.IsOpenRoadBlock)
		{
			mCurrentPage.StartRoadBlockOpenEffect();
			mState.Change(STATE.RBOPEN_WAIT);
		}
		else if (mGame.mPlayer.UnlockedRBList.Count > 0)
		{
			mGame.mPlayer.UnlockedRBList.Sort();
			int num3 = mGame.mPlayer.UnlockedRBList[0];
			mGame.mPlayer.UnlockedRBList.RemoveAt(0);
			int a_main2;
			int a_sub2;
			Def.SplitStageNo(num3, out a_main2, out a_sub2);
			SMEventRallyPage sMEventRallyPage = mCurrentPage as SMEventRallyPage;
			EventRoadBlock eventRoadBlock = sMEventRallyPage.GetEventRoadBlock(num3);
			if (eventRoadBlock != null)
			{
				sMEventRallyPage.StartEventRoadBlockUnlockEffect(num3);
				short num4 = 0;
				SMEventPageData sMEventPageData2 = mMapPageData as SMEventPageData;
				SMEventRoadBlockSetting sMEventRoadBlockSetting = sMEventPageData2.GetRoadBlock(a_main2, a_sub2, CurrentCourseID) as SMEventRoadBlockSetting;
				if (sMEventRoadBlockSetting != null)
				{
					num4 = sMEventRoadBlockSetting.RoadBlockID;
					mGame.mPlayer.UnlockEventRoadBlock(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, num4);
				}
				mGame.Save();
				mState.Change(STATE.ERBUNLOCK_WAIT);
			}
		}
		else if (mGame.mPlayer.LineUnlockList.Count > 0)
		{
			mGame.mPlayer.LineUnlockList.Sort();
			int num5 = mGame.mPlayer.LineUnlockList[0];
			mGame.mPlayer.LineUnlockList.RemoveAt(0);
			int a_main3;
			int a_sub3;
			Def.SplitStageNo(num5, out a_main3, out a_sub3);
			MapLine line = mCurrentPage.GetLine(num5);
			if (line != null)
			{
				mCurrentPage.StartUnlockLineEffect(num5);
				mGame.mPlayer.SetLineStatus(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, num5, Player.STAGE_STATUS.UNLOCK);
				mState.Change(STATE.LINEUNLOCK_WAIT);
			}
		}
		else if (mPlayer.StageOpenList.Count > 0)
		{
			if (mIsOpenNewChapter)
			{
				mIsOpenNewChapter = false;
				mGame.PlaySe("SE_OPEN_NEWCHAPTER", -1);
			}
			mPlayer.StageOpenList.Sort();
			int num6 = mPlayer.StageOpenList[0];
			mPlayer.StageOpenList.RemoveAt(0);
			int a_main4;
			int a_sub4;
			Def.SplitStageNo(num6, out a_main4, out a_sub4);
			mCurrentPage.StartStageOpenEffect(num6, mPlayer.StageOpenList.Count);
			mPlayer.SetStageStatus(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, num6, Player.STAGE_STATUS.LOCK);
			if (mPlayer.StageOpenList.Count == 0)
			{
				int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
				int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
				mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, -1);
			}
			mState.Change(STATE.STAGEOPEN_WAIT);
			if (mGame.mPlayer.StageOpenList.Count == 0)
			{
				mGame.Save();
			}
		}
		else if (mPlayer.StageUnlockList.Count > 0)
		{
			mPlayer.StageUnlockList.Sort();
			int num7 = 0;
			int count = mPlayer.StageUnlockList.Count;
			if (count > 2)
			{
				for (int j = 0; j < count; j++)
				{
					num7 = mPlayer.StageUnlockList[j];
					mCurrentPage.UnlockStageButton(num7);
				}
				num7 = mPlayer.StageUnlockList[count - 1];
				mPlayer.StageUnlockList.Clear();
			}
			else
			{
				num7 = mPlayer.StageUnlockList[0];
				mPlayer.StageUnlockList.RemoveAt(0);
			}
			mGame.mPlayer.AvatarMoveList.Clear();
			int a_main5;
			int a_sub5;
			Def.SplitStageNo(num7, out a_main5, out a_sub5);
			bool flag = mCurrentPage.StartStageUnlockEffect(num7, true);
			if (mGame.mPlayer.StageUnlockList.Count == 0)
			{
				mGame.Save();
			}
			mState.Change(STATE.STAGEUNLOCK_WAIT);
		}
		else if (mGame.mPlayer.LineOpenList.Count > 0)
		{
			mGame.mPlayer.LineOpenList.Sort();
			int num8 = mGame.mPlayer.LineOpenList[0];
			mGame.mPlayer.LineOpenList.RemoveAt(0);
			int a_main6;
			int a_sub6;
			Def.SplitStageNo(num8, out a_main6, out a_sub6);
			MapLine line2 = mCurrentPage.GetLine(num8);
			if (line2 != null)
			{
				mCurrentPage.StartOpenLineEffect(num8);
				mGame.mPlayer.SetLineStatus(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, num8, Player.STAGE_STATUS.LOCK);
				mState.Change(STATE.LINEOPEN_WAIT);
			}
			mGame.Save();
		}
		else if (TutorialStartCheckOnInit())
		{
			mState.Change(STATE.TUTORIAL);
		}
		else if (mGame.mPlayer.AvatarMoveList.Count > 0)
		{
			mGame.mPlayer.AvatarMoveList.Clear();
		}
		else if (mGame.mPlayer.ShowSignAnimation)
		{
			mGame.mPlayer.ShowSignAnimation = false;
			mCurrentPage.StartSignEffect();
			mGame.PlaySe("SE_MAP_OPEN_AREA", -1);
			mState.Change(STATE.SIGNUNLOCK_WAIT);
		}
		else if (mGame.mPlayer.ShowClearAnimation)
		{
			mGame.mPlayer.ShowClearAnimation = false;
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
			SMEventPageData sMEventPageData3 = mMapPageData as SMEventPageData;
			data.UpdateCourseClear(CurrentCourseID, true);
			mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
			if (CurrentCourseID < sMEventPageData3.EventCourseList.Count - 1)
			{
				mIsNextCourseDialogFlg = true;
			}
			string courseClearAnimeKey = sMEventPageData3.EventCourseList[CurrentCourseID].CourseClearAnimeKey;
			mClearAnimation = Util.CreateOneShotAnime("CClear", courseClearAnimeKey, mMapPageRoot, new Vector3(0f, 0f, Def.MAP_LENSFLARE_Z), Vector3.one, 0f, 0f, true, OnClearAnimationFinished);
			mGame.PlaySe("SE_DEMO_CHAPTER", -1);
			mState.Change(STATE.COURSECLEAR_WAIT);
		}
		else if (mAutoUnlockPartner != null)
		{
			mAutoUnlockAccessory = mAutoUnlockPartner;
			if (mCurrentPage.RemoveSilhouette(mAutoUnlockPartner.AnimeLayerName))
			{
				mAutoUnlockPartner = null;
				mState.Reset(STATE.ACCESSORIEOPEN_WAIT);
			}
			else
			{
				mState.Reset(STATE.UNLOCK_ACTING);
			}
		}
		else if (mAutoUnlockAccessory != null)
		{
			StartCoroutine(GrayOut());
			if (mAutoUnlockAccessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
			{
				mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
				mGetAccessoryDialog.Init(mAutoUnlockAccessory, 0, false);
				mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else if (mAutoUnlockAccessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
			{
				GetAccessoryProcess(mAutoUnlockAccessory, false);
				if (mGame.SetPartnerCollectionAttention(mAutoUnlockAccessory))
				{
					mCockpit.SetEnableMenuButtonAttention(true);
				}
			}
			else
			{
				GetAccessoryProcess(mAutoUnlockAccessory, false);
				mGame.mPlayer.SetCollectionNotify(mAutoUnlockAccessory.Index);
				mCockpit.SetEnableMenuButtonAttention(true);
			}
			mAutoUnlockAccessory = null;
		}
		else if (mAlterUnlockAccessory.Count > 0)
		{
			StartCoroutine(GrayOut());
			mAlternateRewardDialog = Util.CreateGameObject("AlterRewardDialog", mRoot).AddComponent<AlternateRewardDialog>();
			mAlternateRewardDialog.Init(mAlterUnlockAccessory);
			mAlternateRewardDialog.SetClosedCallback(delegate
			{
				UnityEngine.Object.Destroy(mAlternateRewardDialog.gameObject);
				mAlternateRewardDialog = null;
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			});
			mAlterUnlockAccessory.Clear();
			mState.Reset(STATE.WAIT, true);
		}
		else if (mAutoUnlockAccessoryList.Count > 0)
		{
			mAutoUnlockAccessory = mAutoUnlockAccessoryList[0];
			mAutoUnlockAccessoryList.RemoveAt(0);
		}
		else if (mAlterUnlockAccessoryList.Count > 0)
		{
			mAlterUnlockAccessory = new List<AccessoryData>(mAlterUnlockAccessoryList[0]);
			mAlterUnlockAccessoryList.RemoveAt(0);
		}
		else if (mGame.mPlayer.ShowCompleteAnimation)
		{
			mGame.mPlayer.ShowCompleteAnimation = false;
			PlayerEventData data2;
			mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data2);
			GetCourseReward(false);
			if (mAutoUnlockAccessory != null)
			{
				mAutoUnlockAccessoryList.Add(mAutoUnlockAccessory);
				mAutoUnlockAccessory = null;
			}
			else if (mAlterUnlockAccessory.Count > 0)
			{
				mAlterUnlockAccessoryList.Add(new List<AccessoryData>(mAlterUnlockAccessory));
				mAlterUnlockAccessory.Clear();
			}
			data2.UpdateCourseComplete(CurrentCourseID, true);
			mGame.mPlayer.Data.SetMapData(CurrentEventID, data2);
			if (mAlterUnlockAccessoryList.Count > 0 || mAutoUnlockAccessoryList.Count > 0)
			{
				mGame.Save();
			}
			SMEventPageData sMEventPageData4 = mMapPageData as SMEventPageData;
			string courseCompleteAnimeKey = sMEventPageData4.EventCourseList[CurrentCourseID].CourseCompleteAnimeKey;
			mCompleteAnimation = Util.CreateOneShotAnime("CComplete", courseCompleteAnimeKey, mMapPageRoot, new Vector3(0f, 0f, Def.MAP_LENSFLARE_Z), Vector3.one, 0f, 0f, true, OnCompleteAnimationFinished);
			mGame.PlaySe("VOICE_029", 2);
			mState.Change(STATE.COURSECOMPLETE_WAIT);
		}
		else if (mIsNextCourseDialogFlg)
		{
			mState.Reset(STATE.WAIT, true);
			SMEventPageData sMEventPageData5 = mMapPageData as SMEventPageData;
			string courseClearDescKey2 = sMEventPageData5.EventCourseList[CurrentCourseID].CourseClearDescKey;
			mIsNextCourseDialogFlg = false;
			StartCoroutine(GrayOut());
			mEventMiniDialog = Util.CreateGameObject("NextCourseDialog", mRoot).AddComponent<EventMiniDialog>();
			mEventMiniDialog.Init("LC_event_instruction_text00", Localization.Get(courseClearDescKey2));
			mEventMiniDialog.SetClosedCallback(OnNextCourseOpenDialogClosed);
		}
		else if (mGame.AutoPopupStageNo > 0)
		{
			OnStageButtonPushed(mGame.AutoPopupStageNo);
			mGame.ResetAutoPopupStage();
		}
		else if (mLoginBonusScreen == null && mGame.mLoginBonusData != null && mGame.mLoginBonusData.HasLoginData)
		{
			GetLoginBonusResponse loginBonusByIndex = mGame.mLoginBonusData.GetLoginBonusByIndex(0);
			if (loginBonusByIndex.IsDislpay)
			{
				mLoginBonusScreen = Util.CreateGameObject("LoginBonusScreen", mRoot).AddComponent<LoginBonusScreen>();
				mLoginBonusScreen.init(loginBonusByIndex, delegate
				{
					mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
					mState.Reset(STATE.UNLOCK_ACTING, true);
					mLoginBonusScreen = null;
				});
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	protected override void Update_MAIN()
	{
		MapMove();
		if (mGame.mTouchDownTrg)
		{
			if (mCurrentPage != null)
			{
				Vector3 position = new Vector3(mGame.mTouchPos.x, mGame.mTouchPos.y, 0f);
				mPageMoveLock = mCurrentPage.OnScreenPress(mCamera.ScreenToWorldPoint(position));
			}
		}
		else if (mGame.mBackKeyTrg)
		{
			if (GameMain.USE_DEBUG_DIALOG && GameMain.HIDDEN_DEBUG_BUTTON)
			{
				OnDebugPushed();
			}
			else
			{
				OnBackKeyPushed();
			}
		}
	}

	protected override void MapMove()
	{
		if (mGame.mTouchDownTrg)
		{
			mDownPos = mGame.mTouchPos;
		}
		if (mPageMoveLock != Def.PRESS.HIT)
		{
			if (mGame.mTouchWasDrag && !mPageMoved)
			{
				Vector3 vector = mCamera.ScreenToWorldPoint(new Vector3(0f, mGame.mTouchPos.y));
				Vector3 vector2 = mCamera.ScreenToWorldPoint(new Vector3(0f, mGame.mTouchPosOld.y));
				MoveVector = (vector - vector2) * Map_DeviceScreenSizeY / 2f;
				mScrollPowerYTemp += MoveVector.y;
				mPageMoveBacking = false;
				if (Mathf.Abs(mScrollPowerYTemp) > GameStateSMMapBase.s_ScrollThresholdY)
				{
					float num = Vector2.Dot(MoveVector, MoveVectorOld);
					if (num < 0f)
					{
						mScrollPowerYTemp = mScrollPowerY / 5f;
					}
					if (Mathf.Abs(mScrollPowerYTemp) > 75f)
					{
						mScrollPowerYTemp = ((!(mScrollPowerYTemp < 0f)) ? 75f : (-75f));
					}
					DragPower = new Vector2(0f, mScrollPowerYTemp);
					SetScrollPower(mScrollPowerYTemp);
					mScrollPowerYTemp = 0f;
					MoveVectorOld = MoveVector;
				}
			}
			else
			{
				MoveVectorOld = Vector2.zero;
			}
		}
		if (!mGame.mTouchCnd)
		{
			mPageMoved = false;
			mPageMoveLock = Def.PRESS.NONE;
		}
	}

	public override void UpdateMapPages()
	{
		base.UpdateMapPages();
		if (mBGImage != null && mCurrentPage != null)
		{
			mBGImage.transform.localPosition = new Vector3(mBGImage.transform.localPosition.x, mCurrentPage.gameObject.transform.localPosition.y, mBGImage.transform.localPosition.z);
		}
	}

	protected void Update_CourseOpen()
	{
		if (mIsFinishedCourseMove)
		{
			for (int i = 0; i < mCourseList.Count; i++)
			{
				NGUITools.SetActive(mCourseList[i].gameObject, false);
			}
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
			CurrentCourseID = data.CourseID;
			mCurrentPage = mCourseList[CurrentCourseID];
			NGUITools.SetActive(mCurrentPage.gameObject, true);
			mState.Change(STATE.INIT);
		}
	}

	protected float AvatarPosition(short a_courseID)
	{
		float num = 0f;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[a_courseID];
		int lastClearedLevel = playerMapData.LastClearedLevel;
		int playableMaxLevel = playerMapData.PlayableMaxLevel;
		SMEventRallyPage sMEventRallyPage = mCourseList[a_courseID];
		if (lastClearedLevel > 0)
		{
			num = sMEventRallyPage.GetStageWorldPos(lastClearedLevel);
			sMEventRallyPage.SetAvatarPositionOnStage(lastClearedLevel);
			CheckUnlockNewStage();
		}
		else
		{
			CheckUnlockNewStage();
			float mValue = sMEventRallyPage.mAvater.GetCurrentNode().mValue;
			lastClearedLevel = Def.GetStageNoByRouteOrder(mValue);
			num = sMEventRallyPage.GetStageWorldPos(lastClearedLevel);
		}
		float y = sMEventRallyPage.transform.localPosition.y;
		if (SetDeviceScreen(Util.ScreenOrientation) && num > y)
		{
			num += 80f;
		}
		float map_DeviceOffset = Map_DeviceOffset;
		float num2 = 0f - (1136f * (float)(mMaxModulePage - 1) + map_DeviceOffset);
		if (num > map_DeviceOffset)
		{
			num = map_DeviceOffset;
		}
		else if (num < num2)
		{
			num = num2;
		}
		return 0f - num;
	}

	protected IEnumerator MoveNextCourse()
	{
		mIsFinishedCourseMove = false;
		yield return 0;
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		int courseMax = eventPageData.EventCourseList.Count;
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out eventData);
		if (CurrentCourseID >= courseMax - 1)
		{
			eventData.CourseID = CurrentCourseID;
			mIsFinishedCourseMove = true;
			mGame.mPlayer.Data.SetMapData(CurrentEventID, eventData);
			yield break;
		}
		SMMapPage current = mCurrentPage;
		SMMapPage next = mCourseList[CurrentCourseID + 1];
		NGUITools.SetActive(next.gameObject, true);
		int moveAmount = 0;
		float deg = 0f;
		float[] defaultX = new float[mCourseList.Count];
		float[] defaultY = new float[mCourseList.Count];
		float[] defaultZ = new float[mCourseList.Count];
		for (int l = 0; l < mCourseList.Count; l++)
		{
			NGUITools.SetActive(mCourseList[l].gameObject, true);
			defaultX[l] = mCourseList[l].gameObject.transform.localPosition.x;
			if (l != CurrentCourseID)
			{
				defaultY[l] = AvatarPosition((short)l);
			}
			else
			{
				defaultY[l] = mCourseList[l].gameObject.transform.localPosition.y;
			}
			defaultZ[l] = mCourseList[l].gameObject.transform.localPosition.z;
		}
		while (true)
		{
			float time = 1f;
			deg += Time.deltaTime * 90f / time;
			moveAmount = (int)(Mathf.Sin(deg * ((float)Math.PI / 180f)) * 1136f);
			for (int k = 0; k < mCourseList.Count; k++)
			{
				mCourseList[k].gameObject.transform.localPosition = new Vector3(defaultX[k] - (float)moveAmount, defaultY[k], defaultZ[k]);
			}
			if (deg < 90f)
			{
				yield return 0;
				continue;
			}
			break;
		}
		for (int j = 0; j < mCourseList.Count; j++)
		{
			mCourseList[j].gameObject.transform.localPosition = new Vector3(defaultX[j] - 1136f, defaultY[j], defaultZ[j]);
		}
		for (int i = 0; i < mCourseList.Count; i++)
		{
			NGUITools.SetActive(mCourseList[i].gameObject, false);
		}
		NGUITools.SetActive(next.gameObject, true);
		NGUITools.SetActive(current.gameObject, false);
		CurrentCourseID++;
		eventData.CourseID = CurrentCourseID;
		mCurrentPage = mCourseList[CurrentCourseID];
		bool islandscape = SetDeviceScreen(Util.ScreenOrientation);
		UpdateCurrentMapPos(islandscape);
		mGame.mPlayer.Data.SetMapData(CurrentEventID, eventData);
		mIsFinishedCourseMove = true;
		yield return 0;
	}

	protected IEnumerator MoveBackCourse()
	{
		mIsFinishedCourseMove = false;
		yield return 0;
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		int courseMax = eventPageData.EventCourseList.Count;
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out eventData);
		if (CurrentCourseID <= 0)
		{
			eventData.CourseID = CurrentCourseID;
			mIsFinishedCourseMove = true;
			mGame.mPlayer.Data.SetMapData(CurrentEventID, eventData);
			yield break;
		}
		SMMapPage current = mCurrentPage;
		SMMapPage next = mCourseList[CurrentCourseID - 1];
		NGUITools.SetActive(next.gameObject, true);
		int moveAmount = 0;
		float deg = 0f;
		float[] defaultX = new float[mCourseList.Count];
		float[] defaultY = new float[mCourseList.Count];
		float[] defaultZ = new float[mCourseList.Count];
		for (int l = 0; l < mCourseList.Count; l++)
		{
			NGUITools.SetActive(mCourseList[l].gameObject, true);
			defaultX[l] = mCourseList[l].gameObject.transform.localPosition.x;
			if (l != CurrentCourseID)
			{
				defaultY[l] = AvatarPosition((short)l);
			}
			else
			{
				defaultY[l] = mCourseList[l].gameObject.transform.localPosition.y;
			}
			defaultZ[l] = mCourseList[l].gameObject.transform.localPosition.z;
		}
		while (true)
		{
			float time = 1f;
			deg += Time.deltaTime * 90f / time;
			moveAmount = (int)(Mathf.Sin(deg * ((float)Math.PI / 180f)) * 1136f);
			for (int k = 0; k < mCourseList.Count; k++)
			{
				mCourseList[k].gameObject.transform.localPosition = new Vector3(defaultX[k] + (float)moveAmount, defaultY[k], defaultZ[k]);
			}
			if (deg < 90f)
			{
				yield return 0;
				continue;
			}
			break;
		}
		for (int j = 0; j < mCourseList.Count; j++)
		{
			mCourseList[j].gameObject.transform.localPosition = new Vector3(defaultX[j] + 1136f, defaultY[j], defaultZ[j]);
		}
		for (int i = 0; i < mCourseList.Count; i++)
		{
			NGUITools.SetActive(mCourseList[i].gameObject, false);
		}
		NGUITools.SetActive(next.gameObject, true);
		NGUITools.SetActive(current.gameObject, false);
		CurrentCourseID--;
		eventData.CourseID = CurrentCourseID;
		mCurrentPage = mCourseList[CurrentCourseID];
		bool islandscape = SetDeviceScreen(Util.ScreenOrientation);
		UpdateCurrentMapPos(islandscape);
		mGame.mPlayer.Data.SetMapData(CurrentEventID, eventData);
		mIsFinishedCourseMove = true;
		yield return 0;
	}

	public void InitOpenNewStage(SMMapPage a_page)
	{
		SMEventRallyPage sMEventRallyPage = a_page as SMEventRallyPage;
		short courseID = sMEventRallyPage.CourseID;
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[courseID];
		int nextRoadBlockLevel = playerMapData.NextRoadBlockLevel;
		int playableMaxLevel = playerMapData.PlayableMaxLevel;
		int openNoticeLevel = playerMapData.OpenNoticeLevel;
		int num = -1;
		int a_main;
		int a_sub;
		Def.SplitStageNo(openNoticeLevel, out a_main, out a_sub);
		SMChapterSetting chapterSearch = mMapPageData.GetChapterSearch(a_main, courseID);
		if (chapterSearch != null)
		{
			num = Def.GetStageNo(chapterSearch.mSubNo, 0);
		}
		if (playableMaxLevel != 100)
		{
			return;
		}
		Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
		chapterSearch = mMapPageData.GetChapterSearch(a_main, courseID);
		num = Def.GetStageNo(chapterSearch.mSubNo, 0);
		List<int> stagesUntilNextChapter = sMEventRallyPage.GetStagesUntilNextChapter(playableMaxLevel, num);
		foreach (int item in stagesUntilNextChapter)
		{
			if (mPlayer.GetStageStatus(a_page.Series, sMEventRallyPage.EventID, courseID, item) == Player.STAGE_STATUS.NOOPEN)
			{
				mGame.mPlayer.SetStageStatus(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, courseID, item, Player.STAGE_STATUS.LOCK);
				sMEventRallyPage.ChangeStageButton(item, Player.STAGE_STATUS.LOCK);
			}
		}
		mGame.mPlayer.SetStageStatus(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, courseID, 100, Player.STAGE_STATUS.UNLOCK);
		sMEventRallyPage.ChangeStageButton(100, Player.STAGE_STATUS.UNLOCK);
		mGame.mPlayer.SetOpenNoticeLevel(mPlayer.Data.CurrentSeries, CurrentEventID, courseID, Def.GetStageNo(1, 0));
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		List<SMRoadBlockSetting> roadBlocks = sMEventPageData.GetRoadBlocks(courseID);
		for (int i = 0; i < roadBlocks.Count; i++)
		{
			int stageNo = Def.GetStageNo(roadBlocks[i].mStageNo, roadBlocks[i].mSubNo);
			mGame.mPlayer.SetLineStatus(mPlayer.Data.CurrentSeries, CurrentEventID, courseID, stageNo, Player.STAGE_STATUS.LOCK);
			sMEventRallyPage.ChangeLine(stageNo, Player.STAGE_STATUS.LOCK);
		}
	}

	public void CheckOpenNewStage(bool a_lookNotice = true)
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		int nextRoadBlockLevel = playerMapData.NextRoadBlockLevel;
		int playableMaxLevel = playerMapData.PlayableMaxLevel;
		int openNoticeLevel = playerMapData.OpenNoticeLevel;
		int a_nextChapter = -1;
		int a_main;
		int a_sub;
		if (a_lookNotice)
		{
			Def.SplitStageNo(openNoticeLevel, out a_main, out a_sub);
		}
		else
		{
			Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
		}
		SMChapterSetting chapterSearch = mMapPageData.GetChapterSearch(a_main, CurrentCourseID);
		if (chapterSearch != null)
		{
			a_nextChapter = Def.GetStageNo(chapterSearch.mSubNo, 0);
		}
		if (playableMaxLevel == 100)
		{
			Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
			chapterSearch = mMapPageData.GetChapterSearch(a_main, CurrentCourseID);
			a_nextChapter = Def.GetStageNo(chapterSearch.mSubNo, 0);
			mCurrentPage.OpenNewStageUntilNextChapter(playableMaxLevel, a_nextChapter);
			foreach (int stageOpen in mGame.mPlayer.StageOpenList)
			{
				mGame.mPlayer.SetStageStatus(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, stageOpen, Player.STAGE_STATUS.LOCK);
				mCurrentPage.ChangeStageButton(stageOpen, Player.STAGE_STATUS.LOCK);
			}
			mGame.mPlayer.StageOpenList.Clear();
			mGame.mPlayer.SetStageStatus(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, 100, Player.STAGE_STATUS.UNLOCK);
			mCurrentPage.ChangeStageButton(100, Player.STAGE_STATUS.UNLOCK);
			mGame.mPlayer.SetOpenNoticeLevel(mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, Def.GetStageNo(1, 0));
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			List<SMRoadBlockSetting> roadBlocks = sMEventPageData.GetRoadBlocks(CurrentCourseID);
			for (int i = 0; i < roadBlocks.Count; i++)
			{
				int stageNo = Def.GetStageNo(roadBlocks[i].mStageNo, roadBlocks[i].mSubNo);
				mGame.mPlayer.SetLineStatus(mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, stageNo, Player.STAGE_STATUS.LOCK);
				mCurrentPage.ChangeLine(stageNo, Player.STAGE_STATUS.LOCK);
			}
		}
		else
		{
			mCurrentPage.OpenNewStageUntilNextChapter(100, a_nextChapter);
			if (mGame.mPlayer.StageOpenList.Count > 0)
			{
				mIsOpenNewChapter = true;
			}
		}
	}

	public float CheckUnlockNewStage()
	{
		float result = 0f;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		int playableMaxLevel = playerMapData.PlayableMaxLevel;
		int openNoticeLevel = playerMapData.OpenNoticeLevel;
		int lastClearedLevel = playerMapData.LastClearedLevel;
		if (playableMaxLevel != 100)
		{
			result = mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, lastClearedLevel);
		}
		return result;
	}

	public override float ClearedStagePosition(bool a_newcheck = true)
	{
		float num = 0f;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		int lastClearedLevel = playerMapData.LastClearedLevel;
		int playableMaxLevel = playerMapData.PlayableMaxLevel;
		if (lastClearedLevel > 0)
		{
			num = mCurrentPage.GetStageWorldPos(lastClearedLevel);
			mCurrentPage.SetAvatarPositionOnStage(lastClearedLevel);
			if (a_newcheck)
			{
				CheckUnlockNewStage();
			}
		}
		else
		{
			if (a_newcheck)
			{
				CheckUnlockNewStage();
			}
			float mValue = mCurrentPage.mAvater.GetCurrentNode().mValue;
			lastClearedLevel = Def.GetStageNoByRouteOrder(mValue);
			num = mCurrentPage.GetStageWorldPos(lastClearedLevel);
		}
		return num;
	}

	protected void CheckAccessoryDisplay(SMMapPage a_page)
	{
		a_page.DisplayAllAccessory();
	}

	public void OnMapAccessoryTapped(AccessoryData data, MapAccessory a_accessory)
	{
		if (data == null || (mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.TUTORIAL))
		{
			a_accessory.RequestOpenAnime();
		}
		else if (!string.IsNullOrEmpty(data.DemoName) && data.DemoName != "NONE")
		{
			if (mState.GetStatus() != STATE.MAIN)
			{
				a_accessory.RequestOpenAnime();
				return;
			}
			SetScrollPower(0f);
			UpdateMapPages();
			StartCoroutine(GrayOut());
		}
		else
		{
			GetAccessoryProcess(data);
			mPushedAccessory = a_accessory;
		}
	}

	public void GetAccessoryProcess(AccessoryData data, bool a_add = true)
	{
		switch (data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.PARTNER:
			StartCoroutine(GrayOut());
			mGetPartnerDialog = Util.CreateGameObject("GetPartnerDialog", mRoot).AddComponent<GetPartnerDialog>();
			mGetPartnerDialog.Init(data, GetPartnerDialog.PLACE.MAIN, a_add);
			mGetPartnerDialog.SetClosedCallback(OnGetPartnerDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART:
		case AccessoryData.ACCESSORY_TYPE.GEM:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		}
	}

	public void OnGetPartnerDialogClosed(GetPartnerDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGetPartnerDialog.Data;
		UnityEngine.Object.Destroy(mGetPartnerDialog.gameObject);
		mGetPartnerDialog = null;
		if (data == null || data.HasDemo)
		{
		}
		mPushedAccessory = null;
		CheckUnlockNewStage();
		mState.Change(STATE.UNLOCK_ACTING);
		StartCoroutine(GrayIn());
	}

	public void OnGrowupSelectDialogClosed(GrowupPartnerSelectDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupDialog.SelectedPartnerNo;
		bool isCloseButton = mGrowupDialog.IsCloseButton;
		mGrowupDialog.Init(false);
		AccessoryData data = mGrowupDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupDialog.gameObject);
		mGrowupDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerSelectDialog.SELECT_ITEM.PLAY:
			mGrowupPartnerConfirmtDialog = Util.CreateGameObject("GrowupPartnerConfirmDialog", mRoot).AddComponent<GrowupPartnerConfirmDialog>();
			mGrowupPartnerConfirmtDialog.Init(data, selectedPartnerNo, isCloseButton, accessoryType);
			mGrowupPartnerConfirmtDialog.SetClosedCallback(OnGrowupPartnerConfirmDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerSelectDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			CheckUnlockNewStage();
			if (mPushedAccessory != null)
			{
				mPushedAccessory.OpenAnime();
			}
			mState.Change(STATE.UNLOCK_ACTING);
			if (isCloseButton && mGame.mPlayer.GrowUpPiece > 0)
			{
				StartCoroutine(GrayOut());
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mState.Reset(STATE.WAIT, true);
			}
			break;
		}
	}

	public void OnGrowupPartnerConfirmDialogClosed(GrowupPartnerConfirmDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupPartnerConfirmtDialog.SelectedPartnerNo;
		bool isReplayStamp = mGrowupPartnerConfirmtDialog.IsReplayStamp;
		mGrowupPartnerConfirmtDialog.Init(false);
		AccessoryData data = mGrowupPartnerConfirmtDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupPartnerConfirmtDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupPartnerConfirmtDialog.gameObject);
		mGrowupPartnerConfirmtDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerConfirmDialog.SELECT_ITEM.PLAY:
			mGrowupResultDialog = Util.CreateGameObject("GrowupResultDialog", mRoot).AddComponent<GrowupResultDialog>();
			mGrowupResultDialog.Init(data, selectedPartnerNo, true, accessoryType, isReplayStamp);
			mGrowupResultDialog.MapUpdateInit(mCurrentPage.mAvater);
			mGrowupResultDialog.SetClosedCallback(OnGrowupResultDialogClosed);
			mPushedAccessory = null;
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			CheckUnlockNewStage();
			if (mPushedAccessory != null)
			{
				mPushedAccessory.OpenAnime();
			}
			if (isReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.BACK:
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(data, isReplayStamp, accessoryType);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		}
	}

	public void OnGrowupResultDialogClosed(GrowupResultDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGrowupResultDialog.Data;
		StartCoroutine(GrayIn());
		if (data == null || data.HasDemo)
		{
		}
		if (mGrowupResultDialog != null)
		{
			if (mGrowupResultDialog.IsReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				StartCoroutine(GrayOut());
				mGrowupResultDialog.Init(false);
				mGrowupResultDialog.MapUpdateInit(mCurrentPage.mAvater);
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
		UnityEngine.Object.Destroy(mGrowupResultDialog.gameObject);
		mGrowupResultDialog = null;
		CheckUnlockNewStage();
	}

	public void OnGetHeartPieaceDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetAccessoryDialog != null)
		{
			UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
			mGetAccessoryDialog = null;
		}
		if (mGame.mPlayer.HeartUpPiece > 0)
		{
			StartCoroutine(GrayOut());
			mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
			mGetStampDialog.Init(null, 0, false, true, false);
			mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			mBundleAccessoryCount = 0;
			mState.Change(STATE.WAIT);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
			mPushedAccessory = null;
		}
	}

	public void OnGetStampDialogClosed(GetStampDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetStampDialog != null)
		{
			UnityEngine.Object.Destroy(mGetStampDialog.gameObject);
		}
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
		if (a_selectItem == GetStampDialog.SELECT_ITEM.GROWUP)
		{
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(null, true, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			StartCoroutine(GrayIn());
		}
	}

	public void OnGetAccessoryDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		StartCoroutine(GrayIn());
		AccessoryData data = mGetAccessoryDialog.Data;
		if (data == null || data.HasDemo)
		{
		}
		if (data != null)
		{
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER)
			{
				List<AccessoryData> wallPaperPieces = mGame.GetWallPaperPieces(data.GetWallPaperIndex());
				mGame.mPlayer.SetCollectionNotify(wallPaperPieces[0].Index);
				mCockpit.SetEnableMenuButtonAttention(true);
			}
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE)
			{
				VisualCollectionData visualCollectionData = mGame.mVisualCollectionData[data.IsFlip];
				int num = 0;
				for (int i = 0; i < visualCollectionData.piceIds.Length; i++)
				{
					if (mGame.mPlayer.IsAccessoryUnlock(visualCollectionData.piceIds[i]))
					{
						num++;
					}
				}
				if (visualCollectionData.piceIds.Length == num)
				{
					mGame.mPlayer.SetCollectionNotify(visualCollectionData.accessoryId);
					mCockpit.SetEnableMenuButtonAttention(true);
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.Init(true);
					mGetAccessoryDialog.SetClosedCallback(OnWallPeperCompClosed);
					mState.Reset(STATE.WAIT, true);
					return;
				}
			}
		}
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
	}

	protected void OnWallPeperCompClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
	}

	public void OnMapRoadBlockTapped(SMRoadBlockSetting a_setting, MapRoadBlock a_roadblock)
	{
	}

	public void OnRoadBlockDialogClosed(RoadBlockDialog.SELECT_ITEM i)
	{
	}

	public void CompactionRoadBlockResponses()
	{
	}

	public void OnBrokenRBKeyClosed(GetAccessoryDialog.SELECT_ITEM a_electItem)
	{
	}

	protected bool CheaterStartCheck()
	{
		if (!mGame.CheckTimeCheater())
		{
			return false;
		}
		mState.Change(STATE.WAIT);
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("Cheater_TimeCheat_Desc"));
		mConfirmDialog.Init(Localization.Get("Cheater_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnTimeCheatErrorDialogClosed);
		return true;
	}

	protected void OnTimeCheatErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		OnExitToTitle(true);
	}

	protected void OnEventExit(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			if (!IsEventExpired(true, OnMoveToMap, delegate
			{
				mState.Change(STATE.MAIN);
			}))
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("ReturnMap_Desc"));
				mConfirmDialog.Init(Localization.Get("ReturnMap_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnMoveToMap);
			}
		}
	}

	protected void OnMoveToMapForce(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		MoveToMap();
	}

	protected void OnMoveToMap(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			MoveToMap();
			return;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void MoveToMap()
	{
		mGame.StopMusic(0, 0.5f);
		mGame.mPlayer.SetNextSeries(mGame.mPlayer.Data.PreviousSeries);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override bool WebviewStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return false;
		}
		if (mGame.mEventProfile == null)
		{
			return false;
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		if (seasonEvent == null)
		{
			return false;
		}
		int helpID = seasonEvent.HelpID;
		string helpURL = seasonEvent.HelpURL;
		if (helpID == 0 || string.IsNullOrEmpty(helpURL))
		{
			return false;
		}
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		if (data.FirstHowToPlayFlg >= seasonEvent.HelpID)
		{
			return false;
		}
		mStartWebview = true;
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		OnEventHelp(null);
		return true;
	}

	protected void OnDebugEventHowToClosed(ConfirmDialog.SELECT_ITEM i)
	{
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
		if (mConfirmDialog != null)
		{
			UnityEngine.Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		if (mStartWebview)
		{
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			int num = 0;
			for (int j = 0; j < sMEventPageData.EventCourseList.Count; j++)
			{
				SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[j];
				num = sMEventCourseSetting.ClearAccessoryID;
				if (num != 0)
				{
					AccessoryData accessoryData = mGame.GetAccessoryData(num);
					if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
					{
						mIntroAccessory = accessoryData.Index;
						mIntroCourse = j;
						StartCoroutine(GrayIn());
						StartCoroutine(MoveNextIntroCourse(j));
						break;
					}
				}
			}
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	protected IEnumerator MoveNextIntroCourse(int courseID, bool nextCurrentCourseIDFlg = false)
	{
		float delay_timer = 0f;
		while (true)
		{
			delay_timer += Time.deltaTime;
			if (delay_timer <= 0.3f)
			{
				yield return null;
				continue;
			}
			break;
		}
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		int courseMax = eventPageData.EventCourseList.Count;
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out eventData);
		mState.Reset(STATE.WAIT, true);
		float[] defaultX = new float[mCourseList.Count];
		float[] defaultY = new float[mCourseList.Count];
		float[] defaultZ = new float[mCourseList.Count];
		for (int a = 0; a < mCourseList.Count; a++)
		{
			NGUITools.SetActive(mCourseList[a].gameObject, true);
			defaultX[a] = mCourseList[a].gameObject.transform.localPosition.x;
			if (a != CurrentCourseID)
			{
				defaultY[a] = 0f;
			}
			else
			{
				defaultY[a] = mCourseList[a].gameObject.transform.localPosition.y;
			}
			defaultZ[a] = mCourseList[a].gameObject.transform.localPosition.z;
		}
		int moveAmount = 0;
		float deg = 0f;
		NGUITools.SetActive(mCourseList[courseID - 1].mAvater.gameObject, false);
		NGUITools.SetActive(mCourseList[courseID].mAvater.gameObject, false);
		while (true)
		{
			float time = 1f;
			deg += Time.deltaTime * 45f / time;
			moveAmount = (int)(Mathf.Sin(deg * ((float)Math.PI / 180f)) * (1136f * (float)(courseID - CurrentCourseID)));
			for (int a3 = 0; a3 < mCourseList.Count; a3++)
			{
				mCourseList[a3].gameObject.transform.localPosition = new Vector3(defaultX[a3] - (float)moveAmount, defaultY[a3], defaultZ[a3]);
			}
			if (deg < 90f)
			{
				yield return 0;
				continue;
			}
			break;
		}
		for (int a2 = 0; a2 < mCourseList.Count; a2++)
		{
			if (courseID != a2)
			{
				NGUITools.SetActive(mCourseList[a2].gameObject, false);
			}
		}
		if (nextCurrentCourseIDFlg)
		{
			CurrentCourseID++;
			eventData.CourseID = CurrentCourseID;
			mGame.mPlayer.Data.SetMapData(CurrentEventID, eventData);
		}
		mCurrentPage = mCourseList[courseID];
		mCurrentPage.StartSignEffect();
		mIntroFlg = true;
		mGame.PlaySe("SE_MAP_OPEN_AREA", -1);
		mState.Change(STATE.SIGNUNLOCK_WAIT);
	}

	protected IEnumerator MoveBackIntroCourse()
	{
		mState.Reset(STATE.WAIT, true);
		float[] defaultX = new float[mCourseList.Count];
		float[] defaultY = new float[mCourseList.Count];
		float[] defaultZ = new float[mCourseList.Count];
		for (int i = 0; i < mCourseList.Count; i++)
		{
			NGUITools.SetActive(mCourseList[i].gameObject, true);
			defaultX[i] = mCourseList[i].gameObject.transform.localPosition.x;
			if (i != CurrentCourseID)
			{
				defaultY[i] = 0f;
			}
			else
			{
				defaultY[i] = mCourseList[i].gameObject.transform.localPosition.y;
			}
			defaultZ[i] = mCourseList[i].gameObject.transform.localPosition.z;
			if (CurrentCourseID == i)
			{
				NGUITools.SetActive(mCourseList[i].mAvater.gameObject, true);
			}
		}
		int moveAmount = 0;
		float deg = 0f;
		while (true)
		{
			float time = 1f;
			deg += Time.deltaTime * 45f / time;
			moveAmount = (int)(Mathf.Sin(deg * ((float)Math.PI / 180f)) * (1136f * (float)(mIntroCourse - CurrentCourseID)));
			for (int j = 0; j < mCourseList.Count; j++)
			{
				mCourseList[j].gameObject.transform.localPosition = new Vector3(defaultX[j] + (float)moveAmount, defaultY[j], defaultZ[j]);
			}
			if (deg < 90f)
			{
				yield return 0;
				continue;
			}
			break;
		}
		for (int k = 0; k < mCourseList.Count; k++)
		{
			NGUITools.SetActive(mCourseList[k].mAvater.gameObject, true);
		}
		for (int a = 0; a < mCourseList.Count; a++)
		{
			if (CurrentCourseID != a)
			{
				NGUITools.SetActive(mCourseList[a].gameObject, false);
			}
		}
		if (mStartWebview)
		{
			mStartWebview = false;
			mEventImageDialog = Util.CreateGameObject("EventExplainDialog", mRoot).AddComponent<EventImageDialog>();
			mEventImageDialog.Init("label_howto", Localization.Get("SeasonEvent_Rule_Title"), Localization.Get("SeasonEvent_Rule_Desc"));
			mEventImageDialog.SetClosedCallback(OnEventExplainDialogClosed);
			mState.Reset(STATE.WAIT, true);
			StartCoroutine(GrayOut());
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	protected void OnEventIntroCharactertDialogClosed()
	{
		mCurrentPage = mCourseList[CurrentCourseID];
		StartCoroutine(GrayIn());
		StartCoroutine(MoveBackIntroCourse());
	}

	protected void OnIntroAnimationFinished()
	{
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
		mEventIntroCharacterDialog.Init(mIntroAccessory, Def.EVENT_TYPE.SM_RALLY, mIntroCourse + 1, mPartnerGetInrtoflg);
		mEventIntroCharacterDialog.SetClosedCallback(OnEventIntroCharactertDialogClosed);
		StartCoroutine(GrayOut());
		mState.Reset(STATE.WAIT, true);
	}

	protected void OnEventExplainDialogClosed()
	{
		StartCoroutine(GrayIn());
		mGame.Save();
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnEventHelp(GameObject go)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
			mHelpDialog = null;
		}
		if (IsEventHelpExpired(delegate
		{
			mConfirmDialog = null;
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}))
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			return;
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		if (seasonEvent != null)
		{
			int helpID = seasonEvent.HelpID;
			string helpURL = seasonEvent.HelpURL;
			if (helpID != 0 && !string.IsNullOrEmpty(helpURL))
			{
				StartCoroutine(GrayOut());
				mGame.PlaySe("SE_POSITIVE", -1);
				mHelpDialog = Util.CreateGameObject("HowToDialog", mRoot).AddComponent<WebViewDialog>();
				mHelpDialog.Title = Localization.Get("EventPage");
				mHelpDialog.SetClosedCallback(OnHowToDialogClosed);
				mHelpDialog.SetPageMovedCallback(OnHowToDialogPageMoved);
				mState.Reset(STATE.WEBVIEW, true);
			}
		}
	}

	public void OnCourseNext(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
			if (data.CourseID < data.PlayableMaxCourseID)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				EventRallyCockpit eventRallyCockpit = mCockpit as EventRallyCockpit;
				eventRallyCockpit.DisplayCourseBackButton();
				eventRallyCockpit.DisplayCourseNextButton();
				data.CourseID++;
				mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
				StartCoroutine(MoveNextCourse());
				mState.Reset(STATE.SERIESOPEN_WAIT, true);
			}
		}
	}

	public void OnCourseBack(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
			if (data.CourseID > 0)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				EventRallyCockpit eventRallyCockpit = mCockpit as EventRallyCockpit;
				eventRallyCockpit.DisplayCourseBackButton();
				eventRallyCockpit.DisplayCourseNextButton();
				data.CourseID--;
				StartCoroutine(MoveBackCourse());
				mState.Reset(STATE.SERIESOPEN_WAIT, true);
			}
		}
	}

	protected void OnHowToDialogClosed()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
		}
		mHelpDialog = null;
		mGame.Save();
		if (mStartWebview)
		{
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			int num = 0;
			for (int i = 0; i < sMEventPageData.EventCourseList.Count; i++)
			{
				SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[i];
				num = sMEventCourseSetting.ClearAccessoryID;
				if (num != 0)
				{
					AccessoryData accessoryData = mGame.GetAccessoryData(num);
					if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
					{
						mIntroAccessory = accessoryData.Index;
						mIntroCourse = i;
						StartCoroutine(GrayIn());
						StartCoroutine(MoveNextIntroCourse(i));
						break;
					}
				}
			}
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	private void OnHowToErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (mHelpDialog != null)
		{
			switch (item)
			{
			case ConfirmDialog.SELECT_ITEM.POSITIVE:
				mHelpDialog.Reload();
				break;
			case ConfirmDialog.SELECT_ITEM.NEGATIVE:
				mHelpDialog.Close();
				break;
			}
		}
		mConfirmDialog = null;
	}

	private void OnHowToErrorDialogClosed2(ConfirmDialog.SELECT_ITEM item)
	{
		if (mHelpDialog != null)
		{
			mHelpDialog.Close();
		}
		mConfirmDialog = null;
	}

	private void OnHowToDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mHelpDialog.ShowWebview(true);
		}
		else
		{
			mHelpDialog.ShowWebview(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
			mConfirmDialog.SetClosedCallback(OnHowToErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	protected void OnHowToDialog()
	{
		if (mHelpDialog.IsOpend)
		{
			mState.Reset(STATE.WEBVIEW_WAIT, true);
			mHelpDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_DOMAIN);
			StringBuilder stringBuilder = new StringBuilder();
			SeasonEventSettings seasonEvent = mGame.EventProfile.GetSeasonEvent(CurrentEventID);
			if (seasonEvent != null)
			{
				stringBuilder.Append(seasonEvent.HelpURL);
				mHelpDialog.LoadURL(stringBuilder.ToString());
				return;
			}
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnHowToErrorDialogClosed2);
		}
	}

	protected bool RankingStartCheck()
	{
		return false;
	}

	protected bool RankingRankUpStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsStage(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.STAGE);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected bool RankingRankUpStarStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsStar(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.STAR);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected bool RankingRankUpTrophyStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsTrophy(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.TROPHY);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected void OnRankUpDialogClosed()
	{
		StartCoroutine(GrayIn());
		mRankUpDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected bool StoryDemoStartCheck()
	{
		if (mShowStoryDemoList.Count > 0)
		{
			return true;
		}
		return false;
	}

	protected void StartStoryDemo(int demoName)
	{
		mCurrentDemoIndex = demoName;
		mGameState.FadeOutMaskForStory();
		mGame.StopMusic(0, 0.5f);
		mIsPlayingBGM = false;
		mState.Change(STATE.STORY_DEMO_FADE);
	}

	public void OnStoryDemoFinished(int storyIndex)
	{
		StartCoroutine(GrayIn(1f));
		mGame.mPlayer.StoryComplete(CurrentEventID, storyIndex);
		if (mCurrentStoryDemo != null)
		{
			if (mCurrentStoryDemo.ClearStage == -1 && mCurrentStoryDemo.SelectStage != -1)
			{
				int selectStage = mCurrentStoryDemo.SelectStage;
				StartCoroutine(GrayOut());
				StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, selectStage, string.Empty, 0));
			}
			else
			{
				int a_main;
				int a_sub;
				Def.SplitStageNo(mCurrentStoryDemo.ClearStage, out a_main, out a_sub);
				SMChapterSetting chapterSearch = mMapPageData.GetChapterSearch(a_main + 1);
				if (chapterSearch != null)
				{
					int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
					int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
					if (playableMaxLevel < nextRoadBlockLevel)
					{
						int stageNo = Def.GetStageNo(chapterSearch.mSubNo, 0);
						mCurrentPage.OpenNewStageUntilNextChapter(playableMaxLevel, stageNo);
						if (mGame.mPlayer.StageOpenList.Count > 0)
						{
							mIsOpenNewChapter = true;
						}
					}
				}
				mState.Change(STATE.UNLOCK_ACTING);
				mCockpit.SetEnableButtonAction(true);
				mCockpitOperation = true;
			}
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
			mCockpit.SetEnableButtonAction(true);
			mCockpitOperation = true;
		}
		mGame.PlayMusic(DEFAULT_EVENT_BGM, 0, true, false, true);
	}

	private bool TutorialStartCheckOnInit()
	{
		return false;
	}

	public void OnStageButtonPushed(int stage)
	{
		bool flag = false;
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL2_2 && stage == 200)
		{
			mGame.mTutorialManager.DeleteTutorialArrow();
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL2_2);
			flag = true;
		}
		if (!flag && ((mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.UNLOCK_ACTING) || base.IsShowingMarketingScreen))
		{
			return;
		}
		if (IsEventExpired(false, delegate
		{
			mConfirmDialog = null;
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}, delegate
		{
			mState.Change(STATE.MAIN);
		}))
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			return;
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(stage, out a_main, out a_sub);
		SMMapStageSetting mapStage = mMapPageData.GetMapStage(a_main, a_sub, CurrentCourseID);
		int num = -1;
		if (mapStage != null)
		{
			int storyDemoAtStageSelect = mapStage.GetStoryDemoAtStageSelect();
			if (storyDemoAtStageSelect != -1 && !mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, storyDemoAtStageSelect))
			{
				num = storyDemoAtStageSelect;
			}
		}
		if (num == -1 || mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, num))
		{
			StartCoroutine(GrayOut());
			StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, stage, string.Empty, 0));
			return;
		}
		StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
		storyDemoTemp.DemoIndex = num;
		storyDemoTemp.SelectStage = stage;
		storyDemoTemp.ClearStage = -1;
		mShowStoryDemoList.Add(storyDemoTemp);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override bool IsStageButtonSECall(int stage)
	{
		bool result = true;
		if ((mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.UNLOCK_ACTING) || base.IsShowingMarketingScreen)
		{
			result = false;
		}
		return result;
	}

	protected virtual IEnumerator StartStage(Def.SERIES a_series, int stage, string _DisplayStageNumber, int _LimitTimeOrMoves, int _NoticeStageNo = 0)
	{
		mState.Reset(STATE.NETWORK_STAGE, true);
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(a_series, CurrentEventID, out eventData);
		PlayerMapData data = eventData.CourseData[CurrentCourseID];
		data.CurrentLevel = stage;
		mGame.mPlayer.Data.SetMapData(a_series, data);
		int main;
		int sub;
		Def.SplitStageNo(stage, out main, out sub);
		stage = Def.GetStageNo(CurrentCourseID, main, sub);
		bool _stage_load_finished = false;
		bool _stage_load_success = false;
		float _start_time = Time.realtimeSinceStartup;
		GameStatePuzzle.STAGE_DISPLAY_NUMBER = _DisplayStageNumber;
		GameStatePuzzle.STAGE_LIMIT_TIMES_OR_MOVES = _LimitTimeOrMoves;
		string _DisplayStageNumber2 = default(string);
		int _LimitTimeOrMoves2 = default(int);
		StartCoroutine(SMStageData.CreateAsync(userSegment: mGame.GetUserSegment(), a_series: a_series, eventID: CurrentEventID, stage: stage, fn: delegate(int _stage, SMStageData.EventArgs _e)
		{
			if (_e != null)
			{
				_stage_load_success = true;
				GameStatePuzzle.STAGE_DATA_REQUIRE = false;
				mGame.mCurrentStage = _e.StageData;
				if (!string.IsNullOrEmpty(_DisplayStageNumber2))
				{
					mGame.mCurrentStage.DisplayStageNumber = _DisplayStageNumber2;
				}
				if (_LimitTimeOrMoves2 > 0)
				{
					Def.STAGE_LOSE_CONDITION loseType = mGame.mCurrentStage.LoseType;
					if (loseType != 0 && loseType == Def.STAGE_LOSE_CONDITION.TIME)
					{
						mGame.mCurrentStage.OverwriteLimitTimeSec = _LimitTimeOrMoves2;
					}
					else
					{
						mGame.mCurrentStage.OverwriteLimitMoves = _LimitTimeOrMoves2;
					}
				}
				short a_course;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(_stage, out a_course, out a_main, out a_sub);
				SMEventPageData eventPageData = GameMain.GetEventPageData(CurrentEventID);
				mGame.mCurrentMapPageData = eventPageData;
				mGame.mCurrentStageInfo = eventPageData.GetMapStage(a_main, a_sub, a_course);
				if (mGame.mCurrentStageInfo == null)
				{
					mGame.mCurrentStageInfo = new SMMapStageSetting();
				}
				else
				{
					GameStatePuzzle.STAGE_DISPLAY_NUMBER = mGame.mCurrentStageInfo.DisplayStageNo;
					mGame.mCurrentStage.DisplayStageNumber = mGame.mCurrentStageInfo.DisplayStageNo;
				}
			}
			_stage_load_finished = true;
		}));
		while (!_stage_load_finished)
		{
			yield return 0;
		}
		float _end_span = Time.realtimeSinceStartup - _start_time;
		if (!_stage_load_success || mGame.mCurrentStage == null)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string message = string.Format(Localization.Get("StageData_Error_Desc_02"));
			mConfirmDialog.Init(Localization.Get("StageData_Error_Title_02"), message, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnStageDataErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
			yield break;
		}
		if (mStageInfo == null)
		{
			mStageInfo = Util.CreateGameObject("StageInfoDialog", mRoot).AddComponent<StageInfoDialog>();
			mStageInfo.Init();
			mStageInfo.SetClosedCallback(OnStageInfoDialogClosed);
			mStageInfo.SetPartnerSelectCallback(OnStageCharaDialogOpen);
			mStageInfo.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			mStageInfo.SetBaseDepth(65);
		}
		SetScrollPower(0f);
	}

	private void OnStageDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		mConfirmDialog = null;
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnStageCharaDialogOpen()
	{
		mStageChara = Util.CreateGameObject("StageCharaDialog", mRoot).AddComponent<StageCharaDialog>();
		mStageChara.SetClosedCallback(OnStageCharaDialogClosed);
	}

	public void OnStageCharaDialogClosed(StageCharaDialog.SELECT_ITEM selectItem)
	{
		int selectedPartnerNo = mStageChara.SelectedPartnerNo;
		if (mStageInfo != null && selectedPartnerNo != -1)
		{
			mStageInfo.SetCompanion(selectedPartnerNo);
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
	}

	public void OnStageInfoDialogClosed(StageInfoDialog.SELECT_ITEM selectItem)
	{
		switch (selectItem)
		{
		case StageInfoDialog.SELECT_ITEM.PLAY:
		{
			List<StageRankingData> rankingData = null;
			if (base.RankingData != null)
			{
				rankingData = base.RankingData.results;
			}
			mGame.InitFriendsRanking(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, ref rankingData, mGame.mPlayer);
			mState.Change(STATE.PLAY);
			break;
		}
		case StageInfoDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		}
		if (mStageInfo != null)
		{
			UnityEngine.Object.Destroy(mStageInfo.gameObject);
			mStageInfo = null;
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
		if (GameMain.MaintenanceMode)
		{
			mGame.IsTitleReturnMaintenance = true;
			OnExitToTitle(true);
		}
	}

	public override void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void OnBackKeyPushed(bool ignore = false)
	{
		if (ignore || (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen))
		{
			OnEventExit(null);
		}
	}

	public void OnDebugPushed()
	{
		mState.Reset(STATE.WAIT, true);
		SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
		sMDDebugDialog.SetGameState(this);
		sMDDebugDialog.SetClosedCallback(delegate(SMDDebugDialog.SELECT_ITEM i)
		{
			if (i == SMDDebugDialog.SELECT_ITEM.PUSH_BACKKEY)
			{
				OnBackKeyPushed(true);
			}
			else
			{
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
		});
	}

	public void OnSDPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.EVENTMAP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(mRoot);
			mState.Reset(STATE.WAIT, true);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected new void OnPurchaseDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnHeartPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_EVENTMAP;
			BuyHeartDialog buyHeartDialog = Util.CreateGameObject("HeartDialog", mRoot).AddComponent<BuyHeartDialog>();
			mState.Reset(STATE.WAIT, true);
			buyHeartDialog.SetClosedCallback(OnHeartDialogDialogClosed);
			buyHeartDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected void OnHeartDialogDialogClosed(BuyHeartDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnMugenHeartButtonPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && mCockpit.mMugenflg)
		{
			mState.Reset(STATE.WAIT, true);
			base.OnMugenHeartButtonPushed(go);
		}
	}

	public override void OnMugenHeartDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnMugenHeartDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void CreateMugenHeartDialog(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START_WAIT;
		}
		else
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.END_WAIT;
		}
		mState.Reset(STATE.WAIT, true);
		base.CreateMugenHeartDialog(i);
	}

	public override void OnMugenDialogDialogClosed(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			base.OnMugenDialogDialogClosed(i);
			return;
		}
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.NONE;
		mGame.mPlayer.Data.mUseMugenHeartSetTime = 0;
		mGame.Save();
		base.OnMugenDialogDialogClosed(i);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void StartMugenHeart()
	{
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START;
		mGame.mPlayer.Data.MugenHeartUseTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		Def.ITEMGET_TYPE a_type;
		mGame.mPlayer.SubBoosterNum(Def.MugenHeartTimeToKindData[mGame.mPlayer.Data.mUseMugenHeartSetTime], 1, out a_type);
		if (mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) != mGame.mPlayer.Data.MaxNormalLifeCount)
		{
			mGame.mPlayer.SetItemCount(Def.ITEM_CATEGORY.CONSUME, 1, mGame.mPlayer.Data.MaxNormalLifeCount);
		}
		mGame.Save();
		base.StartMugenHeart();
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnStoreBagPushed(GameObject go, bool linkBanner = false)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && mCockpit.mShopflg)
		{
			mState.Reset(STATE.WAIT, true);
			base.OnStoreBagPushed(go);
		}
	}

	public override void OnStoreBagDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnStoreBagDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		mState.Change((STATE)state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthAbandon()
	{
		mGame.IsTitleReturnMaintenance = true;
		mGame.PlaySe("SE_NEGATIVE", -1);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void OnConnectErrorSegmentRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorSegmentAbandon()
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		mState.Change(STATE.NETWORK_02);
	}

	protected void OnConnectErrorAccessoryRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAccessoryAbandon()
	{
		GameStateSMMapBase.mIsShowErrorDialog = false;
		AccessoryData accessoryData = mGame.GetAccessoryData(AccessoryConnectIndex);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			mGame.mPlayer.AddAccessoryConnect(AccessoryConnectIndex);
		}
		mGame.AlreadyConnectAccessoryList.Add(AccessoryConnectIndex);
		AccessoryConnectIndex = 0;
		mState.Reset(STATE.UNLOCK_ACTING, true);
		StartCoroutine(GrayIn());
	}

	protected virtual void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugLoginBonusTimeReset)
			{
				mGame.mPlayer.Data.LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
				mGame.mDebugLoginBonusTimeReset = false;
			}
			if (mState.GetStatus() == STATE.MAIN)
			{
				mState.Reset(STATE.NETWORK_RESUME_MAINTENACE, true);
			}
			else
			{
				mGame.DoLoginBonusServerCheck = true;
			}
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	protected override bool SetDeviceScreen(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		Map_DeviceScreenSizeY = vector.y;
		Event_DeviceOffset = 213f;
		MAP_BOUNDS_RANGE = 67f / mGame.mDeviceRatioScale;
		float num = 12.7f;
		float num2 = 635f;
		Map_DeviceOffset = num2 - vector.y / 2f / mGame.mDeviceRatioScale - MAP_BOUNDS_RANGE - num;
		Map_DeviceOffset += Event_DeviceOffset;
		return o == ScreenOrientation.LandscapeLeft || o == ScreenOrientation.LandscapeRight;
	}

	protected override void SetLayout(ScreenOrientation o)
	{
		if (mSocialRanking != null)
		{
			UnityEngine.Object.Destroy(mSocialRanking.gameObject);
			mSocialRanking = null;
			Network_OnStageWait();
		}
		mMapPageRoot.transform.parent = mAnchorCenter.gameObject.transform;
		mMapPageRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
		mMapPageRoot.transform.localScale = new Vector3(mGame.mDeviceRatioScale, mGame.mDeviceRatioScale, 1f);
		bool islandscape = SetDeviceScreen(o);
		if (mCockpit == null)
		{
			mCockpit = Util.CreateGameObject("EventRallyCockpit", base.gameObject).AddComponent<EventRallyCockpit>();
			if (mState.GetStatus() == STATE.MAIN)
			{
				mCockpit.Init(this);
			}
			else
			{
				mCockpit.Init(this, false);
			}
			mCockpit.SetCallback(OnCockpitOpend, OnCockpitClosed);
		}
		UpdateCockpit(false, mState.GetStatus() == STATE.MAIN);
		GiftAttentionCheck();
		if (mState.GetStatus() == STATE.MAIN)
		{
			mCockpit.SetEnableButtonAction(true);
		}
		else
		{
			mCockpit.SetEnableButtonAction(false);
		}
		EventRallyCockpit eventRallyCockpit = mCockpit as EventRallyCockpit;
		if (eventRallyCockpit != null)
		{
			eventRallyCockpit.LineCreate();
			eventRallyCockpit.SetLayout(o);
		}
		UpdateCurrentMapPos(islandscape);
	}

	private void UpdateCockpit(bool a_se, bool a_draw = true)
	{
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		EventRallyCockpit eventRallyCockpit = mCockpit as EventRallyCockpit;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		if (a_draw)
		{
			int count = sMEventPageData.EventCourseList.Count;
			if (CurrentCourseID == 0)
			{
				eventRallyCockpit.DisplayCourseBackButton();
			}
			else
			{
				eventRallyCockpit.DisplayCourseBackButton(true, a_se);
			}
			if (CurrentCourseID == count - 1)
			{
				eventRallyCockpit.DisplayCourseNextButton();
			}
			else if (data.PlayableMaxCourseID > CurrentCourseID)
			{
				eventRallyCockpit.DisplayCourseNextButton(true, a_se);
			}
			else
			{
				eventRallyCockpit.DisplayCourseNextButton();
			}
		}
		else
		{
			eventRallyCockpit.DisplayCourseBackButton();
			eventRallyCockpit.DisplayCourseNextButton();
		}
		int courseTotalStars = sMEventPageData.GetCourseTotalStars(CurrentCourseID, 3);
		int eventGotStars = mGame.mPlayer.GetEventGotStars(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID);
		EventRallyCockpit eventRallyCockpit2 = mCockpit as EventRallyCockpit;
		if (eventRallyCockpit2 != null)
		{
			eventRallyCockpit2.StarCurrent(courseTotalStars, eventGotStars);
			eventRallyCockpit2.SetCourse(CurrentCourseID, data.PlayableMaxCourseID);
		}
	}

	public override bool OnDrawCockpit()
	{
		return true;
	}

	protected void OnCockpitOpend(bool opening)
	{
	}

	protected void OnCockpitClosed(bool closing)
	{
	}

	public override bool IsEnableButton()
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return false;
		}
		return true;
	}

	protected bool AccessoryConnectCheck()
	{
		int count = mGame.mPlayer.Data.AccessoryConnect.Count;
		if (count > 0 && AccessoryConnectIndex == 0)
		{
			for (int i = 0; i < mGame.mPlayer.Data.AccessoryConnect.Count; i++)
			{
				int num = mGame.mPlayer.Data.AccessoryConnect[i];
				if (!mGame.AlreadyConnectAccessoryList.Contains(num))
				{
					AccessoryConnectIndex = num;
					return true;
				}
			}
		}
		return false;
	}

	protected void OnGotErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnGetRewardDialogErrorClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void OnMapDestroyUpDateClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override void OnMapDestroyClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void GetCourseReward(bool a_isClear = true)
	{
		bool flag = false;
		AccessoryData accessoryData = null;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[CurrentCourseID];
		int num = 0;
		num = ((!a_isClear) ? sMEventCourseSetting.CompleteAccessoryID : sMEventCourseSetting.ClearAccessoryID);
		if (num == 0)
		{
			return;
		}
		accessoryData = mGame.GetAccessoryData(num);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
		{
			mIsNextPartnerIntro = true;
			if (!mGame.mPlayer.IsAccessoryUnlock(num))
			{
				if (mGame.mCompanionData[accessoryData.GetGotIDByNum()].IsExpandBase())
				{
					mPartnerGetInrtoflg = true;
				}
				if (mGame.mPlayer.IsCompanionUnlock(accessoryData.GetCompanionID()))
				{
					flag = true;
					mGame.mPlayer.UnlockAccessory(accessoryData.Index);
				}
			}
			else
			{
				flag = true;
			}
		}
		if (flag)
		{
			SetAlternateReward(num);
			return;
		}
		mAutoUnlockAccessory = accessoryData;
		if (mAutoUnlockAccessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			mGame.AddAccessoryConnect(mAutoUnlockAccessory.Index);
		}
		else
		{
			mGame.mPlayer.AddAccessory(mAutoUnlockAccessory);
		}
	}

	public override void CheckRewardOnEnterMap()
	{
		if (!mFirstCheckReward)
		{
			return;
		}
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, CurrentEventID, out data);
		PlayerMapData value = data.CourseData[CurrentCourseID];
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		List<int> mapStageNo = sMEventPageData.GetMapStageNo(CurrentCourseID);
		mGame.mPlayer.CheckCourseClear(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, mapStageNo);
		int num = mGame.mPlayer.CheckCourseComplete(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, CurrentCourseID, mapStageNo);
		data.CourseData[CurrentCourseID] = value;
		if (mGame.mPlayer.ShowClearAnimation)
		{
			GetCourseReward();
			if (mAutoUnlockAccessory != null)
			{
				mAutoUnlockAccessoryList.Add(mAutoUnlockAccessory);
				mAutoUnlockAccessory = null;
			}
			else if (mAlterUnlockAccessory.Count > 0)
			{
				mAlterUnlockAccessoryList.Add(new List<AccessoryData>(mAlterUnlockAccessory));
				mAlterUnlockAccessory.Clear();
			}
			data.UpdateCourseClear(CurrentCourseID, true);
			int count = sMEventPageData.EventCourseList.Count;
			if (data.PlayableMaxCourseID < count - 1)
			{
				data.PlayableMaxCourseID++;
				data.ShowCourseDescFlg = true;
			}
		}
		mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
		if (mAlterUnlockAccessoryList.Count > 0 || mAutoUnlockAccessoryList.Count > 0)
		{
			mGame.Save();
		}
		mFirstCheckReward = false;
	}

	protected void OnClearAnimationFinished(SsSprite sprite)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnCompleteAnimationFinished(SsSprite sprite)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnNextCourseOpenDialogClosed()
	{
		EventRallyCockpit eventRallyCockpit = mCockpit as EventRallyCockpit;
		StartCoroutine(GrayIn());
		SMEventPageData eventPageData = GameMain.GetEventPageData(CurrentEventID);
		if (CurrentCourseID < eventPageData.EventCourseList.Count - 1)
		{
			mNewCourseNotifyList[CurrentCourseID + 1] = true;
			mState.Reset(STATE.MAIN);
			if (!mIsNextPartnerIntro)
			{
				OnCourseNext(null);
				return;
			}
			mNewCourseNotifyList[CurrentCourseID + 1] = true;
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			int num = 0;
			bool flag = false;
			int num2 = CurrentCourseID + 1;
			if (num2 < sMEventPageData.EventCourseList.Count)
			{
				for (int i = num2; i < sMEventPageData.EventCourseList.Count; i++)
				{
					SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[i];
					num = sMEventCourseSetting.ClearAccessoryID;
					if (num != 0)
					{
						AccessoryData accessoryData = mGame.GetAccessoryData(num);
						if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
						{
							mIntroAccessory = accessoryData.Index;
							mIntroCourse = i;
							StartCoroutine(GrayIn());
							StartCoroutine(MoveNextIntroCourse(i, true));
							mState.Reset(STATE.WAIT, true);
							flag = true;
							break;
						}
					}
				}
			}
			if (!flag)
			{
				OnCourseNext(null);
			}
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	protected void OnCourseNotifyDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void OnExitToTitle(bool force)
	{
		if ((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || force)
		{
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mState.Change(STATE.UNLOAD_WAIT);
		}
	}

	public void OnGiftPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			JellyImageButton component = go.GetComponent<JellyImageButton>();
			if (component.IsEnable())
			{
				StartCoroutine("GrayOut");
				mState.Reset(STATE.WAIT, true);
				mGiftFullDialog = Util.CreateGameObject("GiftDialog", mRoot).AddComponent<GiftFullDialog>();
				mGiftFullDialog.SetClosedCallback(OnGiftDialogClosed);
			}
		}
	}

	public void OnGiftDialogClosed(bool stageOpen, bool friendHelp, bool friendhelpSuccess, bool advChara, bool advTicket)
	{
		if (stageOpen)
		{
			mState.Reset(STATE.WAIT);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Gift_AferReturnTitle"), Localization.Get("Gift_AferReturnDesc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			return;
		}
		if (friendHelp && mGame.PlayingFriendHelp != null)
		{
			StartCoroutine(GrayOut());
			StartCoroutine(StartStage((Def.SERIES)mGame.PlayingFriendHelp.Gift.Data.ItemKind, mGame.PlayingFriendHelp.Gift.Data.ItemID, string.Empty, 0));
			return;
		}
		if (mCockpit != null)
		{
			mCockpit.SetEnableMailAttention(mGame.mPlayer.HasModifiedGifts());
		}
		StartCoroutine("GrayIn");
		if (friendhelpSuccess)
		{
			int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
			int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
			mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, -1);
			mState.Change(STATE.UNLOCK_ACTING);
		}
		else if (mGame.AddedNewFriend)
		{
			mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
			mState.Change(STATE.NETWORK_PROLOGUE);
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	protected override bool GiftStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return false;
		}
		return base.GiftStartCheck();
	}

	public void OnGameCenterPushed(GameObject go)
	{
		JellyImageButton component = go.GetComponent<JellyImageButton>();
		if (component == null || mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return;
		}
		if (!component.IsEnable() || !mGame.EnableAdvEnter())
		{
			OnGameCenterNoEnterPushed(go);
			return;
		}
		EventCockpit cockpit = mCockpit as EventCockpit;
		Action action = delegate
		{
			Action<Type> fnPositive = delegate(Type fn_dialog_type)
			{
				if (!mGame.EnableAdvEnter())
				{
					if (fn_dialog_type == typeof(EventAdvConfirmDialog))
					{
						UnityEngine.Object.Destroy(mEventAdvConfirmDialog.gameObject);
						mEventAdvConfirmDialog = null;
					}
					OnGameCenterNoEnterPushed(go);
				}
				else
				{
					bool[] wonHistory = mGame.GetWonHistory(0, 10);
					int a_main;
					int a_sub;
					Def.SplitStageNo(mGame.mPlayer.PlayableMaxLevel, out a_main, out a_sub);
					PlayerEventData data;
					mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
					ServerCram.TapIconArcade(CurrentEventID, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, CurrentEventID * 1000000 + data.CourseID * 100000 + mGame.mPlayer.PlayableMaxLevel, mGame.LastWon, (wonHistory != null) ? wonHistory.Length : 0, mGame.GetWonCount(0, 10, false), mGame.GetWonMaxCountContinue(0, 10, false), GetPossessCompanionCramValue(), mGame.mPlayer.LifeCount, mGame.LastStar);
					mGame.EnterADV();
				}
			};
			Action<Type> fnNegative = delegate(Type fn_dialog_type)
			{
				if (fn_dialog_type == typeof(ConfirmDialog))
				{
					mConfirmDialog = null;
				}
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			};
			if (!mGame.IsSeasonEventExpired(CurrentEventID) && cockpit != null && cockpit.IsOldEventFinished)
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("EvMap_GameCenter_Conf_Expired_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
				{
					switch (i)
					{
					case ConfirmDialog.SELECT_ITEM.POSITIVE:
						fnPositive(typeof(ConfirmDialog));
						break;
					case ConfirmDialog.SELECT_ITEM.NEGATIVE:
						fnNegative(typeof(ConfirmDialog));
						break;
					}
				});
			}
			else
			{
				mEventAdvConfirmDialog = Util.CreateGameObject("EventAdvConfirmDialog", mRoot).AddComponent<EventAdvConfirmDialog>();
				mEventAdvConfirmDialog.Init(mGame.IsSeasonEventExpired(CurrentEventID));
				mEventAdvConfirmDialog.SetClosedCallback(delegate(EventAdvConfirmDialog.SELECT_ITEM i)
				{
					switch (i)
					{
					case EventAdvConfirmDialog.SELECT_ITEM.POSITIVE:
						fnPositive(typeof(EventAdvConfirmDialog));
						break;
					case EventAdvConfirmDialog.SELECT_ITEM.NEGATIVE:
						fnNegative(typeof(EventAdvConfirmDialog));
						break;
					}
					mEventAdvConfirmDialog = null;
				});
			}
		};
		if (cockpit == null || !cockpit.TryOpenOldEventExtensionDialog(delegate
		{
			mState.Change(STATE.MAIN);
		}, action))
		{
			action();
		}
		StartCoroutine(GrayOut());
		mState.Reset(STATE.WAIT, true);
	}

	public void OnGameCenterNoEnterPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mAdvNotEnterDialog = Util.CreateGameObject("mAdvNotEnterDialog", mRoot).AddComponent<AdvNotEnterDialog>();
			if (!mGame.EnableAdvSession())
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.MAINTENANCE_ADV);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
			else
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.STAGE_NO_CLEAR);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
		}
	}

	public void AdvNotEnterDialogClosed()
	{
		if (mAdvNotEnterDialog != null)
		{
			UnityEngine.Object.Destroy(mAdvNotEnterDialog.gameObject);
			mAdvNotEnterDialog = null;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnTitleReturnMessageClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGame.StopMusic(0, 0.5f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public override void DEBUG_ChangeStateToInit()
	{
		mState.Change(STATE.INIT);
	}

	public override void ReEnterMap()
	{
		mState.Reset(STATE.GOTO_REENTER);
	}

	private bool OldEventEffectCheck()
	{
		EventCockpit eventCockpit = mCockpit as EventCockpit;
		if (eventCockpit != null)
		{
			if (mGame.mOldEventExtend && !mEffectedOldEvent)
			{
				mState.Change(STATE.OLD_EVENT_EFFECT_WAIT);
				eventCockpit.CanOldEventPlutoEffect = true;
				return true;
			}
			eventCockpit.CanOldEventCountUpRemain = true;
		}
		return false;
	}

	private void StartConnectFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_GETFRIENDINFO;
		}
	}

	private void StartConnectInLoadGameState()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1044);
		list.Add(1001);
		list.Add(1031);
		list.Add(1005);
		list.Add(1045);
		if (mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			list.Add(2012);
			list.Add(1017);
			list.Add(1015);
			list.Add(1016);
			list.Add(3);
		}
		else
		{
			list.Add(1016);
		}
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, false, false, StartConnectFinishCallback);
	}

	private void StartConnectToGetRewardMailFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_RESUME_CAMPAIGN;
		}
	}

	private void StartConnectToGetRewardMail()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1001);
		list.Add(2012);
		list.Add(1017);
		list.Add(3);
		mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
		mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, true, false, StartConnectToGetRewardMailFinishCallback);
	}

	private void StartConnectAfterResumeFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_RESUME_CAMPAIGN;
		}
	}

	private void StartConnectAfterResume()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1044);
		list.Add(1001);
		list.Add(1031);
		list.Add(1005);
		list.Add(1045);
		list.Add(2012);
		list.Add(1017);
		list.Add(3);
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, true, false, StartConnectAfterResumeFinishCallback);
	}
}
