using System.Collections.Generic;

public class SMEventMapSetting : SMMapSetting
{
	public List<short> CharaGetRoadblock = new List<short>();

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		base.Serialize(a_writer);
		int count = CharaGetRoadblock.Count;
		a_writer.WriteInt(count);
		for (int i = 0; i < CharaGetRoadblock.Count; i++)
		{
			a_writer.WriteShort(CharaGetRoadblock[i]);
		}
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		base.Deserialize(a_reader);
		int num = a_reader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			CharaGetRoadblock.Add(a_reader.ReadShort());
		}
	}
}
