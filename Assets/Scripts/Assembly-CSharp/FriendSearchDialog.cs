using System;
using UnityEngine;

public class FriendSearchDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private string mFriendNum;

	private IdSearchDialog mIdSearchDialog;

	private FriendFacebookSearchDialog mFriendFacebookSearchDialog;

	private ConfirmDialog mConfirmDialog;

	private BIJSNS mSNS;

	public bool Modified { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							Modified = false;
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
						mFriendFacebookSearchDialog = Util.CreateGameObject("FriendFacebookSearchDialog", base.ParentGameObject).AddComponent<FriendFacebookSearchDialog>();
						mFriendFacebookSearchDialog.SetClosedCallback(Dialogclosed);
						mFriendFacebookSearchDialog.SetBaseDepth(mBaseDepth + 70);
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns))
				{
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}, null);
			break;
		}
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("FriendSearch_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UIButton uIButton = Util.CreateJellyImageButton("IDsearchButton", base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, 51f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnIdsearchPushed", UIButtonMessage.Trigger.OnClick);
		titleDescKey = string.Format(Localization.Get("ID_Search"));
		UILabel label = Util.CreateLabel("IDSearchDesk", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(label, titleDescKey, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		if ((GameMain.IsSNSFunctionEnabled(SNS_FLAG.FriendSearch) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.FriendSearch, SNS.Facebook)) || (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite, SNS.Facebook)))
		{
			uIButton = Util.CreateJellyImageButton("FacebookButton", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, -58f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnFacbooksearchPushed", UIButtonMessage.Trigger.OnClick);
			UISprite uISprite = Util.CreateSprite("FacebookIcon", uIButton.gameObject, image);
			Util.SetSpriteInfo(uISprite, "icon_sns00", mBaseDepth + 4, new Vector3(-161f, 0f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(50, 50);
			titleDescKey = string.Format(Localization.Get("Facebook_Search"));
			label = Util.CreateLabel("FacebookSearchDesk", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(label, titleDescKey, mBaseDepth + 4, new Vector3(30f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		}
		titleDescKey = string.Format(Localization.Get("FriendTotal"));
		label = Util.CreateLabel("FriendTotalDesk", base.gameObject, atlasFont);
		Util.SetLabelInfo(label, titleDescKey, mBaseDepth + 3, new Vector3(-40f, -155f, 0f), 22, 0, 0, UIWidget.Pivot.Left);
		label.color = Def.DEFAULT_MESSAGE_COLOR;
		mFriendNum = string.Format(Localization.Get("FriendTotalNum"), mGame.mPlayer.Friends.Count, Constants.GAME_FRIEND_MAX);
		label = Util.CreateLabel("FriendNumDesk", base.gameObject, atlasFont);
		Util.SetLabelInfo(label, mFriendNum, mBaseDepth + 3, new Vector3(171f, -155f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		label.color = Def.DEFAULT_MESSAGE_COLOR;
		CreateCancelButton("OnCancelPushed");
	}

	public void OnIdsearchPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mIdSearchDialog = Util.CreateGameObject("IdSearchDialog", base.ParentGameObject).AddComponent<IdSearchDialog>();
			mIdSearchDialog.SetClosedCallback(Dialogclosed);
			mIdSearchDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	public void Dialogclosed()
	{
		mState.Change(STATE.MAIN);
	}

	public void OnFacbooksearchPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSNS = SocialManager.Instance[SNS.Facebook];
			if (IsFacebookLogined())
			{
				mState.Reset(STATE.WAIT, true);
				mFriendFacebookSearchDialog = Util.CreateGameObject("FriendFacebookSearchDialog", base.ParentGameObject).AddComponent<FriendFacebookSearchDialog>();
				mFriendFacebookSearchDialog.SetClosedCallback(Dialogclosed);
				mFriendFacebookSearchDialog.SetBaseDepth(mBaseDepth + 70);
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = base.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
