using UnityEngine;

public class SpeedTransformCompo : MonoBehaviour
{
	protected delegate void SpeedUpdateFunc();

	public delegate void OnTweenFinishCallback();

	public Transform _transform;

	protected SpeedUpdateFunc speedUpdateFunc;

	private OnTweenFinishCallback mOnTweenFinishCallback;

	public bool modified;

	protected Vector3 m_v_pos = new Vector3(0f, 0f, 0f);

	protected Vector3 m_v_rot = new Vector3(0f, 0f, 0f);

	protected Vector3 m_v_scl = new Vector3(0f, 0f, 0f);

	public bool tweenMoving;

	public Vector3 _pos
	{
		get
		{
			return _transform.localPosition;
		}
		set
		{
			_transform.localPosition = value;
		}
	}

	public float _posx
	{
		get
		{
			return _pos.x;
		}
		set
		{
			Vector3 pos = _pos;
			pos.x = value;
			_pos = pos;
		}
	}

	public float _posy
	{
		get
		{
			return _pos.y;
		}
		set
		{
			Vector3 pos = _pos;
			pos.y = value;
			_pos = pos;
		}
	}

	public float _posz
	{
		get
		{
			return _pos.z;
		}
		set
		{
			Vector3 pos = _pos;
			pos.z = value;
			_pos = pos;
		}
	}

	public Vector3 _wpos
	{
		get
		{
			return _transform.position;
		}
		set
		{
			_transform.position = value;
		}
	}

	public Vector3 _rot
	{
		get
		{
			return _transform.localRotation.eulerAngles;
		}
		set
		{
			Quaternion identity = Quaternion.identity;
			identity.eulerAngles = value;
			_transform.localRotation = identity;
		}
	}

	public float _rotx
	{
		get
		{
			return _rot.x;
		}
		set
		{
			Vector3 rot = _rot;
			rot.x = value;
			_rot = rot;
		}
	}

	public float _roty
	{
		get
		{
			return _rot.y;
		}
		set
		{
			Vector3 rot = _rot;
			rot.y = value;
			_rot = rot;
		}
	}

	public float _rotz
	{
		get
		{
			return _rot.z;
		}
		set
		{
			Vector3 rot = _rot;
			rot.z = value;
			_rot = rot;
		}
	}

	public Vector3 _scl
	{
		get
		{
			return _transform.localScale;
		}
		set
		{
			_transform.localScale = value;
		}
	}

	public float _sclx
	{
		get
		{
			return _scl.x;
		}
		set
		{
			Vector3 scl = _scl;
			scl.x = value;
			_scl = scl;
		}
	}

	public float _scly
	{
		get
		{
			return _scl.y;
		}
		set
		{
			Vector3 scl = _scl;
			scl.y = value;
			_scl = scl;
		}
	}

	public float _sclz
	{
		get
		{
			return _scl.z;
		}
		set
		{
			Vector3 scl = _scl;
			scl.z = value;
			_scl = scl;
		}
	}

	public Vector3 v_pos
	{
		get
		{
			return m_v_pos;
		}
		set
		{
			modified = true;
			m_v_pos = value;
		}
	}

	public float v_posx
	{
		get
		{
			return v_pos.x;
		}
		set
		{
			Vector3 vector = v_pos;
			vector.x = value;
			v_pos = vector;
		}
	}

	public float v_posy
	{
		get
		{
			return v_pos.y;
		}
		set
		{
			Vector3 vector = v_pos;
			vector.y = value;
			v_pos = vector;
		}
	}

	public float v_posz
	{
		get
		{
			return v_pos.z;
		}
		set
		{
			Vector3 vector = v_pos;
			vector.z = value;
			v_pos = vector;
		}
	}

	public Vector3 v_rot
	{
		get
		{
			return m_v_rot;
		}
		set
		{
			modified = true;
			m_v_rot = value;
		}
	}

	public float v_rotx
	{
		get
		{
			return v_rot.x;
		}
		set
		{
			Vector3 vector = v_rot;
			vector.x = value;
			v_rot = vector;
		}
	}

	public float v_roty
	{
		get
		{
			return v_rot.y;
		}
		set
		{
			Vector3 vector = v_rot;
			vector.y = value;
			v_rot = vector;
		}
	}

	public float v_rotz
	{
		get
		{
			return v_rot.z;
		}
		set
		{
			Vector3 vector = v_rot;
			vector.z = value;
			v_rot = vector;
		}
	}

	public Vector3 v_scl
	{
		get
		{
			return m_v_scl;
		}
		set
		{
			modified = true;
			m_v_scl = value;
		}
	}

	public float v_sclx
	{
		get
		{
			return v_scl.x;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.x = value;
			v_scl = vector;
		}
	}

	public float v_scly
	{
		get
		{
			return v_scl.y;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.y = value;
			v_scl = vector;
		}
	}

	public float v_sclz
	{
		get
		{
			return v_scl.z;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.z = value;
			v_scl = vector;
		}
	}

	public void SetOnTweenFinishCallback(OnTweenFinishCallback _callback)
	{
		mOnTweenFinishCallback = _callback;
	}

	public virtual void Awake()
	{
		_transform = base.transform;
		_transform.localScale = new Vector3(1f, 1f, 1f);
	}

	public virtual void Start()
	{
		speedUpdateFunc = SUD_v;
	}

	protected virtual void Update()
	{
		if (Time.timeScale != 0f && speedUpdateFunc != null)
		{
			speedUpdateFunc();
		}
	}

	protected virtual void SUD_v()
	{
		if (modified)
		{
			_pos += m_v_pos * GameMain.frameDeltaMul;
			_rot += m_v_rot * GameMain.frameDeltaMul;
			_scl += m_v_scl * GameMain.frameDeltaMul;
			if (m_v_pos.x == 0f && m_v_pos.y == 0f && m_v_pos.z == 0f && m_v_rot.x == 0f && m_v_rot.y == 0f && m_v_rot.z == 0f && m_v_scl.x == 0f && m_v_scl.y == 0f && m_v_scl.z == 0f)
			{
				modified = false;
			}
		}
	}

	private void OnTweenMoveFinished()
	{
		tweenMoving = false;
		if (mOnTweenFinishCallback != null)
		{
			mOnTweenFinishCallback();
		}
	}
}
