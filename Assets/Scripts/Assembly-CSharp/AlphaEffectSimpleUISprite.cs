using UnityEngine;

public class AlphaEffectSimpleUISprite : MonoBehaviour
{
	private UISprite sprite;

	private float mTime;

	private int state;

	private Color vColor = new Color(0f, 0f, 0f, 0f);

	protected Vector3 m_v_scl = new Vector3(0f, 0f, 0f);

	public Vector3 v_scl
	{
		get
		{
			return m_v_scl;
		}
		set
		{
			m_v_scl = value;
		}
	}

	public float v_sclx
	{
		get
		{
			return v_scl.x;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.x = value;
			v_scl = vector;
		}
	}

	public float v_scly
	{
		get
		{
			return v_scl.y;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.y = value;
			v_scl = vector;
		}
	}

	public float v_sclz
	{
		get
		{
			return v_scl.z;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.z = value;
			v_scl = vector;
		}
	}

	public static AlphaEffectSimpleUISprite Make(string _name, GameObject _parent, BIJImage _atlas, string _spritename, int _depth, Vector3 _position, bool _flip, UIWidget.Pivot _pv, float _time)
	{
		GameObject gameObject = Util.CreateGameObject(_name, _parent);
		AlphaEffectSimpleUISprite alphaEffectSimpleUISprite = gameObject.AddComponent<AlphaEffectSimpleUISprite>();
		alphaEffectSimpleUISprite.sprite = Util.CreateSprite(_name, gameObject, _atlas);
		Util.SetSpriteInfo(alphaEffectSimpleUISprite.sprite, _spritename, _depth, Vector3.zero, Vector3.one, _flip, _pv);
		alphaEffectSimpleUISprite.transform.localPosition = _position;
		alphaEffectSimpleUISprite.transform.localScale = Vector3.one;
		alphaEffectSimpleUISprite.transform.localEulerAngles = Vector3.zero;
		alphaEffectSimpleUISprite.mTime = _time;
		return alphaEffectSimpleUISprite;
	}

	protected void Awake()
	{
		state = 0;
	}

	protected void Start()
	{
	}

	protected void Update()
	{
		switch (state)
		{
		default:
			return;
		case 0:
		{
			float num = 0f - sprite.color.r;
			float num2 = 0f - sprite.color.g;
			float num3 = 0f - sprite.color.b;
			float num4 = 0f - sprite.color.a;
			vColor = new Color(num / (mTime / Def.TG_FRAME_TIME), num2 / (mTime / Def.TG_FRAME_TIME), num3 / (mTime / Def.TG_FRAME_TIME), num4 / (mTime / Def.TG_FRAME_TIME));
			v_scl = new Vector3(3f, 3f, 0f) / (mTime / Def.TG_FRAME_TIME);
			state = 4096;
			break;
		}
		case 4096:
			break;
		}
		sprite.color += vColor * GameMain.frameDeltaMul;
		base.transform.localScale += v_scl * GameMain.frameDeltaMul;
		mTime -= Time.deltaTime;
		if (mTime <= 0f)
		{
			GameMain.SafeDestroy(base.gameObject);
			state = 8192;
		}
	}
}
