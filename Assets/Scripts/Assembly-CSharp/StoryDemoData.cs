public class StoryDemoData
{
	public short index;

	public string path;

	public string chapter;

	public string title;

	public string iconName;

	public string chapterImage;

	public Def.SERIES series;

	public StoryDemoData()
	{
		index = 0;
		path = string.Empty;
		chapter = string.Empty;
		title = string.Empty;
		iconName = "null";
		chapterImage = string.Empty;
		series = Def.SERIES.SM_FIRST;
	}

	public void deserialize(BIJBinaryReader stream)
	{
		index = stream.ReadShort();
		path = stream.ReadUTF();
		chapter = stream.ReadUTF();
		title = stream.ReadUTF();
		iconName = stream.ReadUTF();
		chapterImage = stream.ReadUTF();
		series = (Def.SERIES)stream.ReadShort();
	}

	public StoryDemoData Clone()
	{
		return (StoryDemoData)MemberwiseClone();
	}

	public void Dump()
	{
	}
}
