using System;
using System.Collections.Generic;
using UnityEngine;

public class AlternateRewardDialog : DialogBase
{
	public class VertiaclListItem : SMVerticalListItem, IComparable
	{
		public string ImageName { get; set; }

		public int ItemNum { get; set; }

		int IComparable.CompareTo(object _obj)
		{
			return CompareTo(_obj as VertiaclListItem);
		}

		public int CompareTo(VertiaclListItem _obj)
		{
			return ImageName.CompareTo(_obj.ImageName);
		}

		public static VertiaclListItem Make(string a_imageName, int a_num)
		{
			VertiaclListItem vertiaclListItem = new VertiaclListItem();
			vertiaclListItem.ImageName = a_imageName;
			vertiaclListItem.ItemNum = a_num;
			return vertiaclListItem;
		}
	}

	public enum SELECT_ITEM
	{
		OK = 0
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private List<AccessoryData> mData;

	private BIJImage atlas;

	private float mCounter;

	private bool mOKEnable;

	protected SMVerticalList mList;

	protected SMVerticalListInfo mListInfo;

	protected List<SMVerticalListItem> mListItem = new List<SMVerticalListItem>();

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(List<AccessoryData> a_data)
	{
		mData = new List<AccessoryData>(a_data);
		mGame.PlaySe("VOICE_005", -1);
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mCounter += Time.deltaTime;
		if (mOKEnable && mCounter > 0.8f)
		{
			string text = "LC_button_ok2";
			UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
			Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 3, new Vector3(0f, -120f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
			mOKEnable = false;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(Localization.Get("GetAccessory_Item_Title"));
		UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, atlas);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 55f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 115);
		uISprite.type = UIBasicSprite.Type.Sliced;
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		string text = Localization.Get("chara_same_Desc00") + "\n" + Localization.Get("chara_same_Desc01") + Localization.Get("GetItemMail_Title");
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 5, new Vector3(0f, -45f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		for (int i = 0; i < mData.Count; i++)
		{
			string a_imageName = string.Empty;
			int num = 0;
			AccessoryData accessoryData = mData[i];
			num = accessoryData.GetNum();
			switch (accessoryData.AccessoryType)
			{
			case AccessoryData.ACCESSORY_TYPE.GEM:
				a_imageName = "icon_84jem";
				num = int.Parse(accessoryData.GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.BOOSTER:
			{
				Def.BOOSTER_KIND a_kind;
				int a_num;
				accessoryData.GetBooster(out a_kind, out a_num);
				BoosterData boosterData = mGame.mBoosterData[(int)a_kind];
				a_imageName = boosterData.iconNameOff;
				num = a_num;
				break;
			}
			case AccessoryData.ACCESSORY_TYPE.HEART:
				a_imageName = "icon_heart";
				break;
			case AccessoryData.ACCESSORY_TYPE.TROPHY:
				a_imageName = "icon_ranking_trophy";
				break;
			case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
				a_imageName = "ac_icon_wallpaper_complete";
				break;
			case AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE:
				a_imageName = "icon_wallpaper";
				break;
			case AccessoryData.ACCESSORY_TYPE.GROWUP:
				a_imageName = "icon_level";
				break;
			}
			VertiaclListItem item = VertiaclListItem.Make(a_imageName, num);
			mListItem.Add(item);
		}
		int num2 = mData.Count * 90;
		if (num2 > 450)
		{
			num2 = 450;
		}
		mListInfo = new SMVerticalListInfo(1, num2, 100f, 1, num2, 100f, 1, 90);
		mList = SMVerticalList.Make(uISprite.gameObject, this, mListInfo, mListItem, 0f, 0f, mBaseDepth + 6, true, UIScrollView.Movement.Horizontal);
		mList.OnCreateItem = OnCreateItem;
		mList.HideScrollBar();
	}

	protected virtual void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 7;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		VertiaclListItem vertiaclListItem = item as VertiaclListItem;
		float cellWidth = mList.mList.cellWidth;
		float cellHeight = mList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back" + index, itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", num, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite2 = Util.CreateSprite("Board_" + index, itemGO, image);
		Util.SetSpriteInfo(uISprite2, vertiaclListItem.ImageName, num + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("RewardNum", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Format(Localization.Get("Gift_Number"), vertiaclListItem.ItemNum), num + 2, new Vector3(40f, -30f, 0f), 18, 0, 0, UIWidget.Pivot.Right);
		uILabel.color = new Color(0.4f, 0.2627451f, 0.14901961f);
		uILabel.effectStyle = UILabel.Effect.Outline8;
		uILabel.effectColor = Color.white;
		uILabel.effectDistance = new Vector2(2f, 2f);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
		mList.ScrollCustomStop();
		mOKEnable = true;
		mCounter = 0f;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		if (!mOKEnable)
		{
			OnClosePushed();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
