using System;
using System.Reflection;
using System.Text;

public class BIJSNSData<T> : ICloneable, ICopyable where T : class
{
	object ICloneable.Clone()
	{
		return Clone();
	}

	void ICopyable.CopyTo(object _obj)
	{
		CopyTo(_obj as T);
	}

	public T Clone()
	{
		return MemberwiseClone() as T;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[{0}: {1}]", typeof(T).Name, stringBuilder.ToString());
	}

	public void CopyTo(T _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(T);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}
}
