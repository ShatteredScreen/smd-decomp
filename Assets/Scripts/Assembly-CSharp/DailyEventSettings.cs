using System;
using System.Collections.Generic;

public class DailyEventSettings : ICloneable, IComparable
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("ids")]
	public List<int> EventIDList { get; set; }

	public DailyEventSettings()
	{
		EventIDList = new List<int>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object obj)
	{
		return CompareTo(obj as DailyEventSettings);
	}

	public DailyEventSettings Clone()
	{
		return MemberwiseClone() as DailyEventSettings;
	}

	public int CompareTo(DailyEventSettings obj)
	{
		if (obj == null)
		{
			return int.MaxValue;
		}
		return (int)(StartTime - obj.StartTime);
	}
}
