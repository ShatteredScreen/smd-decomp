using UnityEngine;

public class FriendHelpOKDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		YES = 0
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private bool mFriendHelp;

	private BoxCollider mColliderFriend;

	private UIButton mFriendHelpButton;

	private string mImageName = "LC_button_ok2";

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("FriendHelp_Conf_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		titleDescKey = ((!mGame.mEventMode) ? (Localization.Get("LevelInfo_Level") + mGame.mCurrentStage.DisplayStageNumber) : (mGame.mEventName + mGame.mCurrentStage.DisplayStageNumber));
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, 80f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.black;
		uILabel = Util.CreateLabel("FriendHelp", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("FriendHelp_Conf_Desc01"), mBaseDepth + 3, new Vector3(0f, -10f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.spacingY = 8;
		mFriendHelpButton = Util.CreateJellyImageButton("ButtonOK", base.gameObject, image);
		Util.SetImageButtonInfo(mFriendHelpButton, mImageName, mImageName, mImageName, mBaseDepth + 4, new Vector3(0f, -110f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mFriendHelpButton, this, "OnFriendHelpOKPushed", UIButtonMessage.Trigger.OnClick);
		mColliderFriend = mFriendHelpButton.GetComponent<BoxCollider>();
	}

	public void OnFriendHelpOKPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.YES;
			Close();
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnFriendHelpOKPushed(null);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
