using System.Text;

public static class SsEncoding
{
	public static Encoding SJIS
	{
		get
		{
			return Encoding.GetEncoding(932);
		}
	}

	public static Encoding UTF8
	{
		get
		{
			return Encoding.GetEncoding("utf-8");
		}
	}

	public static string ConvertTo(Encoding srcEnc, Encoding dstEnc, string srcStr)
	{
		byte[] bytes = srcEnc.GetBytes(srcStr);
		byte[] array = Encoding.Convert(srcEnc, dstEnc, bytes);
		char[] array2 = new char[dstEnc.GetCharCount(array, 0, array.Length)];
		dstEnc.GetChars(array, 0, array.Length, array2, 0);
		return new string(array2);
	}
}
