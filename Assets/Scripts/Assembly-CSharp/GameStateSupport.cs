using System.Collections;
using UnityEngine;

public class GameStateSupport : GameStateTransfer
{
	public int mBaseDepth = 50;

	private UISprite mBack;

	private UILabel mDesc00;

	private UILabel mDesc01;

	private UILabel mDesc02;

	private UISprite mUserInfo;

	private UIButton mInquiryButton;

	private UIButton mHelpButton;

	private UILabel mVersion;

	public new void OnDestroy()
	{
	}

	public override void Unload()
	{
		base.Unload();
	}

	public override IEnumerator LoadGameState()
	{
		ResSound resSound = ResourceManager.LoadSoundAsync("BGM_PUZZLE");
		ResImage resTitleImage = ResourceManager.LoadImageAsync("TITLE");
		while (resTitleImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		BIJImage titleAtlas = resTitleImage.Image;
		UIFont font = GameMain.LoadFont();
		UISprite sprite5 = null;
		UILabel label7 = null;
		Vector2 logScreen = Util.LogScreenSize();
		mAnchorTop = Util.CreateAnchorWithChild("SupportAnchorTop", mRoot, UIAnchor.Side.Top, mGame.mCamera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("SupportAnchorTopRight", mRoot, UIAnchor.Side.TopRight, mGame.mCamera).gameObject;
		mAnchorBottomRight = Util.CreateAnchorWithChild("SupportAnchorBottomRight", mRoot, UIAnchor.Side.BottomRight, mGame.mCamera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("SupportAnchorBottomLeft", mRoot, UIAnchor.Side.BottomLeft, mGame.mCamera).gameObject;
		mBack = Util.CreateSprite("BackGround", mRoot, atlas);
		Util.SetSpriteInfo(mBack, "bg00", mBaseDepth, Vector3.zero, Vector3.one, false);
		mBack.type = UIBasicSprite.Type.Tiled;
		mBack.SetDimensions(1138, 1138);
		sprite5 = Util.CreateSprite("TitleBarR", mAnchorTop.gameObject, atlas);
		Util.SetSpriteInfo(sprite5, "menubar_frame", mBaseDepth + 1, new Vector3(-2f, 173f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite5 = Util.CreateSprite("TitleBarL", mAnchorTop.gameObject, atlas);
		Util.SetSpriteInfo(sprite5, "menubar_frame", mBaseDepth + 1, new Vector3(2f, 173f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		label7 = Util.CreateLabel("TitleBarLabel", mAnchorTop.gameObject, font);
		Util.SetLabelInfo(label7, Localization.Get("HelpSupport_Title00"), mBaseDepth + 2, new Vector3(0f, -26f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
		mVersion = Util.CreateLabel("VersionLabel", mAnchorBottomRight.gameObject, font);
		Util.SetLabelInfo(mVersion, "Version : " + Application.version, mBaseDepth + 2, new Vector3(-20f, 32f, 0f), 24, 0, 0, UIWidget.Pivot.Right);
		mDesc00 = Util.CreateLabel("Desc00", mRoot, font);
		Util.SetLabelInfo(mDesc00, Localization.Get("HelpSupport_Desc00"), mBaseDepth + 2, new Vector3(-240f, 420f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		mDesc01 = Util.CreateLabel("Desc01", mRoot, font);
		Util.SetLabelInfo(mDesc01, Localization.Get("HelpSupport_Desc01"), mBaseDepth + 2, new Vector3(-240f, 330f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		mUserInfo = Util.CreateSprite("UserInfo", mRoot, atlas);
		Util.SetSpriteInfo(mUserInfo, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 115f, 0f), Vector3.one, false);
		mUserInfo.type = UIBasicSprite.Type.Sliced;
		mUserInfo.SetDimensions(490, 320);
		sprite5 = Util.CreateSprite("line", mUserInfo.gameObject, atlas);
		Util.SetSpriteInfo(sprite5, "line00", mBaseDepth + 2, new Vector3(-230f, 35f, 0f), Vector3.one, false, UIWidget.Pivot.Left);
		sprite5.type = UIBasicSprite.Type.Sliced;
		sprite5.SetDimensions(460, 6);
		label7 = Util.CreateLabel("Nickname_Title", mUserInfo.gameObject, font);
		Util.SetLabelInfo(label7, Localization.Get("HelpSupport_Nickname_Title"), mBaseDepth + 2, new Vector3(-220f, 120f, 0f), 32, 0, 0, UIWidget.Pivot.Left);
		label7.color = Def.DEFAULT_MESSAGE_COLOR;
		label7 = Util.CreateLabel("Nickname", mUserInfo.gameObject, font);
		Util.SetLabelInfo(label7, mGame.mPlayer.Data.NickName, mBaseDepth + 2, new Vector3(-200f, 70f, 0f), 32, 0, 0, UIWidget.Pivot.Left);
		label7.color = Def.DEFAULT_MESSAGE_COLOR;
		label7.overflowMethod = UILabel.Overflow.ShrinkContent;
		label7.SetDimensions(350, 32);
		string str2 = string.Format(Localization.Get("UserID_Desc")) + string.Format(Localization.Get("UserID_Data"), mGame.mPlayer.Data.SearchID);
		label7 = Util.CreateLabel("UserID", mUserInfo.gameObject, font);
		Util.SetLabelInfo(label7, str2, mBaseDepth + 2, new Vector3(-220f, -5f, 0f), 32, 0, 0, UIWidget.Pivot.Left);
		label7.color = Def.DEFAULT_MESSAGE_COLOR;
		UIButton button = Util.CreateJellyImageButton("IDCopy", mUserInfo.gameObject, atlas);
		Util.SetImageButtonInfo(button, "LC_button_copy", "LC_button_copy", "LC_button_copy", mBaseDepth + 3, new Vector3(190f, -5f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnIDCopyPushed", UIButtonMessage.Trigger.OnClick);
		sprite5 = Util.CreateSprite("line2", mUserInfo.gameObject, atlas);
		Util.SetSpriteInfo(sprite5, "line00", mBaseDepth + 2, new Vector3(-230f, -40f, 0f), Vector3.one, false, UIWidget.Pivot.Left);
		sprite5.type = UIBasicSprite.Type.Sliced;
		sprite5.SetDimensions(460, 6);
		label7 = Util.CreateLabel("Password_Title", mUserInfo.gameObject, font);
		Util.SetLabelInfo(label7, Localization.Get("HelpSupport_Password_Title"), mBaseDepth + 2, new Vector3(-220f, -80f, 0f), 32, 0, 0, UIWidget.Pivot.Left);
		label7.color = Def.DEFAULT_MESSAGE_COLOR;
		if (string.IsNullOrEmpty(mGame.mUserUniqueID.TransferPass))
		{
			str2 = Localization.Get("HelpSupport_Password_NoIssue");
		}
		else
		{
			string str3 = mGame.mUserUniqueID.TransferPass.Substring(0, 4);
			string str4 = mGame.mUserUniqueID.TransferPass.Substring(4, 4);
			string str5 = mGame.mUserUniqueID.TransferPass.Substring(8, 4);
			string str6 = mGame.mUserUniqueID.TransferPass.Substring(12, 4);
			str2 = string.Format(Localization.Get("Transfer_ShowPassword_PassFormat"), str3, str4, str5, str6);
		}
		label7 = Util.CreateLabel("Password", mUserInfo.gameObject, font);
		Util.SetLabelInfo(label7, str2, mBaseDepth + 2, new Vector3(0f, -130f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		label7.color = Def.DEFAULT_MESSAGE_COLOR;
		label7.overflowMethod = UILabel.Overflow.ShrinkContent;
		label7.SetDimensions(420, 32);
		mDesc02 = Util.CreateLabel("Desc02", mRoot, font);
		Util.SetLabelInfo(mDesc02, Localization.Get("Transfer_ShowPassword_Desc01"), mBaseDepth + 2, new Vector3(0f, -140f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		mDesc02.color = Color.yellow;
		mDesc02.overflowMethod = UILabel.Overflow.ShrinkContent;
		mDesc02.spacingY = 4;
		mDesc02.SetDimensions(490, 130);
		mInquiryButton = Util.CreateJellyImageButton("InquiryButton", mRoot, titleAtlas);
		Util.SetImageButtonInfo(mInquiryButton, "LC_button_inquiry", "LC_button_inquiry", "LC_button_inquiry", mBaseDepth + 1, new Vector3(0f, -270f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mInquiryButton, this, "OnInquiryButtonPushed", UIButtonMessage.Trigger.OnClick);
		mHelpButton = Util.CreateJellyImageButton("HelpButton", mRoot, titleAtlas);
		Util.SetImageButtonInfo(mHelpButton, "LC_button_help", "LC_button_help", "LC_button_help", mBaseDepth + 1, new Vector3(0f, -380f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mHelpButton, this, "OnHelpButtonPushed", UIButtonMessage.Trigger.OnClick);
		mBackbutton = Util.CreateJellyImageButton("ExitButton", mAnchorBottomLeft.gameObject, atlas);
		Util.SetImageButtonInfo(mBackbutton, "button_back", "button_back", "button_back", mBaseDepth + 2, new Vector3(60f, 60f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mBackbutton, this, "OnExitPushed", UIButtonMessage.Trigger.OnClick);
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mPlayBGM = true;
		LoadGameStateFinished();
		mViewLoaded = true;
		SetLayout(Util.ScreenOrientation);
		yield return 0;
	}

	protected override void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mDesc00.transform.localPosition = new Vector3(-240f, 420f, 0f);
			mDesc01.transform.localPosition = new Vector3(-240f, 330f, 0f);
			mUserInfo.transform.localPosition = new Vector3(0f, 115f, 0f);
			mDesc02.transform.localPosition = new Vector3(0f, -140f, 0f);
			mInquiryButton.transform.localPosition = new Vector3(0f, -270f, 0f);
			mHelpButton.transform.localPosition = new Vector3(0f, -380f, 0f);
			if (mVersion != null)
			{
				mVersion.transform.parent = mAnchorBottomRight.gameObject.transform;
				mVersion.transform.localPosition = new Vector3(-20f, 32f);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mDesc00.transform.localPosition = new Vector3(20f, 150f, 0f);
			mDesc01.transform.localPosition = new Vector3(20f, 70f, 0f);
			mUserInfo.transform.localPosition = new Vector3(-260f, 40f, 0f);
			mDesc02.transform.localPosition = new Vector3(260f, -50f, 0f);
			mInquiryButton.transform.localPosition = new Vector3(-200f, -200f, 0f);
			mHelpButton.transform.localPosition = new Vector3(200f, -200f, 0f);
			if (mVersion != null)
			{
				mVersion.transform.parent = mAnchorTopRight.gameObject.transform;
				mVersion.transform.localPosition = new Vector3(-20f, -120f);
			}
			break;
		}
		if (mBack != null)
		{
			Vector2 vector = Util.LogScreenSize();
			float num = vector.y / 909f;
			mBack.transform.localScale = new Vector3(num, num, 1f);
			mBack.SetDimensions((int)(vector.x / num), 909);
		}
	}

	public override IEnumerator UnloadGameState()
	{
		yield return StartCoroutine(base.UnloadGameState());
	}

	public void OnHelpButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mState.Reset(STATE.WEBVIEW_SHOW_HELP_START, true);
		}
	}

	public void OnIDCopyPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			BIJUnity.setClipboardText(mGame.mPlayer.Data.SearchID);
			mState.Reset(STATE.WAIT, true);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("CopyID_Title"), Localization.Get("CopyID_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnIDCopyDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
			SetGrayOut(true);
		}
	}

	private void OnIDCopyDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		if (mState.GetStatus() == STATE.WAIT)
		{
			mConfirmDialog = null;
			mState.Change(STATE.MAIN);
			SetGrayOut(false);
		}
	}
}
