using System;

public class BIJSNSEventCallback : IDisposable, BIJSNS.IEventCallback
{
	protected bool mDisposed;

	protected BIJSNS mParent;

	private int mSeqNo;

	private int mRegCount;

	public virtual int SeqNo
	{
		get
		{
			return mSeqNo;
		}
	}

	protected virtual BIJSNS Parent
	{
		get
		{
			return mParent;
		}
	}

	protected virtual int RegCount
	{
		get
		{
			return mRegCount;
		}
	}

	public BIJSNSEventCallback(BIJSNS _sns, int _seqno)
	{
		mDisposed = false;
		mParent = _sns;
		mSeqNo = _seqno;
		mRegCount = 0;
	}

	void IDisposable.Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
	}

	~BIJSNSEventCallback()
	{
		Dispose(false);
	}

	protected virtual void Dispose(bool disposing)
	{
		if (!mDisposed)
		{
			while (mRegCount > 0)
			{
				((BIJSNS.IEventCallback)this).Unregister();
			}
			mDisposed = true;
		}
	}

	public override string ToString()
	{
		return "eventcb #" + SeqNo + "  ref=" + mRegCount;
	}

	protected static string ToSS(object o)
	{
		return (o != null) ? o.ToString() : string.Empty;
	}

	public virtual bool EqualsSeqNo(int _seqno)
	{
		return SeqNo == _seqno;
	}

	public virtual void Register()
	{
		mRegCount++;
	}

	public virtual void Unregister()
	{
		mRegCount--;
	}
}
