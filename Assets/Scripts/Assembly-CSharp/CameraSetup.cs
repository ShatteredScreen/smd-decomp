using UnityEngine;

public class CameraSetup : MonoBehaviour
{
	private const float TIME_TO_WAIT_FOR_MOUSE_MOVEMENT = 0.1f;

	private UICamera uiCamera;

	private Vector3 lastMousePosition;

	private float timeSinceLastMouseMovement;

	private void Awake()
	{
		uiCamera = GetComponent<UICamera>();
		uiCamera.useTouch = false;
		uiCamera.useMouse = true;
		lastMousePosition = Input.mousePosition;
	}

	private void Update()
	{
		float num = Vector3.Distance(lastMousePosition, Input.mousePosition);
		lastMousePosition = Input.mousePosition;
		if (Input.touchCount > 0)
		{
			if (!uiCamera.useTouch)
			{
				uiCamera.useTouch = true;
				uiCamera.useMouse = false;
			}
		}
		else if (num > 0f)
		{
			timeSinceLastMouseMovement = 0.1f;
			if (!uiCamera.useMouse)
			{
				uiCamera.useTouch = false;
				uiCamera.useMouse = true;
				uiCamera.SetHoverEnable(false);
			}
		}
		else
		{
			if (timeSinceLastMouseMovement < 0f && !uiCamera.useTouch && !Input.GetMouseButton(0) && !Input.GetMouseButton(1))
			{
				uiCamera.useTouch = true;
				uiCamera.useMouse = false;
				uiCamera.SetHoverEnable(false);
			}
			timeSinceLastMouseMovement -= Time.deltaTime;
		}
	}
}
