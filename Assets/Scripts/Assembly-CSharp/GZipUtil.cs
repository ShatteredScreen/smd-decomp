using Ionic.Zlib;

public class GZipUtil
{
	public static byte[] CompressBuffer(byte[] b)
	{
		return GZipStream.CompressBuffer(b);
	}

	public static byte[] UncompressBuffer(byte[] compressed)
	{
		return GZipStream.UncompressBuffer(compressed);
	}
}
