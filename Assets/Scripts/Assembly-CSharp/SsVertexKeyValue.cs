using System;
using UnityEngine;

[Serializable]
public class SsVertexKeyValue : SsInterpolatable, SsAttrValueInterface
{
	public SsPoint[] Vertices;

	public SsVertexKeyValue(int verticesNum)
	{
		if (verticesNum > 0)
		{
			Vertices = SsPoint.CreateArray(verticesNum);
		}
	}

	public SsVertexKeyValue(SsVertexKeyValue r)
	{
		Vertices = (SsPoint[])r.Vertices.Clone();
	}

	public Vector3 Vertex3(int i)
	{
		return new Vector3(Vertices[i].X, -Vertices[i].Y, 0f);
	}

	public SsAttrValueInterface Clone()
	{
		return new SsVertexKeyValue(this);
	}

	public SsInterpolatable GetInterpolated(SsCurveParams curve, float time, SsInterpolatable start, SsInterpolatable end, int startTime, int endTime)
	{
		SsVertexKeyValue ssVertexKeyValue = new SsVertexKeyValue(((SsVertexKeyValue)end).Vertices.Length);
		return ssVertexKeyValue.Interpolate(curve, time, start, end, startTime, endTime);
	}

	public SsInterpolatable Interpolate(SsCurveParams curve, float time, SsInterpolatable start_, SsInterpolatable end_, int startTime, int endTime)
	{
		SsVertexKeyValue ssVertexKeyValue = (SsVertexKeyValue)start_;
		SsVertexKeyValue ssVertexKeyValue2 = (SsVertexKeyValue)end_;
		float time2 = SsInterpolation.Interpolate(curve, time, 0f, 1f, startTime, endTime);
		SsCurveParams ssCurveParams = new SsCurveParams();
		ssCurveParams.Type = SsInterpolationType.Linear;
		for (int i = 0; i < Vertices.Length; i++)
		{
			Vertices[i].Interpolate(ssCurveParams, time2, ssVertexKeyValue.Vertices[i], ssVertexKeyValue2.Vertices[i], startTime, endTime);
		}
		return this;
	}

	public override string ToString()
	{
		string text = "Vertices: ";
		if (Vertices != null)
		{
			int num = 0;
			SsPoint[] vertices = Vertices;
			foreach (SsPoint ssPoint in vertices)
			{
				string text2 = text;
				text = text2 + "[" + num + "]: " + ssPoint;
				num++;
			}
		}
		return text;
	}
}
