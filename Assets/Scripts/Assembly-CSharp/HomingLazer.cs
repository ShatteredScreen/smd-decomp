using System;
using UnityEngine;

public class HomingLazer : LazerInstanceBase
{
	public enum STATE
	{
		WAIT = 0,
		BRAKE = 1,
		HOMING = 2,
		HIT = 3,
		FINISH = 4
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	private Vector2 mPos;

	private Vector2 mTargetPos;

	private Util.TriFunc mTriFunc;

	private float mRadian;

	private float mScalar;

	private float mInitialScalar;

	private float mLifeTimer;

	private float mDelayTimer;

	public int IntParam0;

	public int IntParam1;

	public HomingLazer(Vector2 beginPoint, Vector2 endPoint, int length, float width, string spriteName, float delayTime)
		: base(length, width, spriteName, STYLE.LAZER)
	{
		mPos = beginPoint;
		mTargetPos = endPoint;
		mRadian = Mathf.Atan2(mTargetPos.y - mPos.y, mTargetPos.x - mPos.x);
		float num = UnityEngine.Random.Range(45f, 135f);
		if (UnityEngine.Random.Range(0, 100) < 50)
		{
			num *= -1f;
		}
		mRadian += num * ((float)Math.PI / 180f);
		mScalar = UnityEngine.Random.Range(1500f, 2000f);
		mInitialScalar = mScalar;
		mTriFunc = new Util.TriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
		mLifeTimer = 0f;
		mDelayTimer = delayTime;
	}

	public override void Update(float deltaTime)
	{
		mLifeTimer += deltaTime;
		Vector2 right = Vector2.right;
		switch (mState.GetStatus())
		{
		case STATE.WAIT:
			mDelayTimer -= deltaTime;
			if (mDelayTimer <= 0f)
			{
				SetNewPos(mPos);
				UpdateBrake(deltaTime);
				mState.Change(STATE.BRAKE);
			}
			break;
		case STATE.BRAKE:
			UpdateBrake(deltaTime);
			break;
		case STATE.HOMING:
			UpdateRadian(deltaTime);
			mScalar += 3000f * deltaTime;
			right = new Vector2(Mathf.Cos(mRadian) * mScalar, Mathf.Sin(mRadian) * mScalar);
			mPos += right * deltaTime;
			SetNewPos(mPos);
			if (Vector2.Distance(mPos, mTargetPos) < 40f)
			{
				mState.Change(STATE.HIT);
			}
			if (mLifeTimer > 3f)
			{
				mState.Change(STATE.HIT);
			}
			break;
		case STATE.HIT:
			mLength--;
			if (mLength == 0)
			{
				Finish();
				mState.Change(STATE.FINISH);
			}
			break;
		}
		mState.Update();
	}

	private void UpdateBrake(float deltaTime)
	{
		Vector2 right = Vector2.right;
		mTriFunc.AddDegree(deltaTime * 360f);
		mScalar = mInitialScalar * (1f - mTriFunc.Sin());
		if (mTriFunc.degree > 70f)
		{
			mState.Change(STATE.HOMING);
			UpdateRadian(deltaTime);
		}
		right = new Vector2(Mathf.Cos(mRadian) * mScalar, Mathf.Sin(mRadian) * mScalar);
		mPos += right * deltaTime;
		SetNewPos(mPos);
	}

	private void UpdateRadian(float deltaTime)
	{
		float num = Mathf.Atan2(mTargetPos.y - mPos.y, mTargetPos.x - mPos.x) * 57.29578f;
		float num2 = num - mRadian * 57.29578f;
		if (num2 >= 180f)
		{
			num2 -= 360f;
		}
		if (num2 < -180f)
		{
			num2 += 360f;
		}
		float num3 = 30f;
		if (num2 > num3)
		{
			num2 = num3;
		}
		if (num2 < 0f - num3)
		{
			num2 = 0f - num3;
		}
		mRadian += num2 * ((float)Math.PI / 180f);
	}

	private void SetNewPos(Vector2 pos)
	{
		if (mLength < mMaxLength)
		{
			mLength++;
		}
		mTrail[mTrailPtr] = pos;
		mForePoint = mTrail[mTrailPtr];
		mTrailPtr = (mTrailPtr + 1) % mMaxLength;
	}
}
