using UnityEngine;

public class SMDTools
{
	public void SMDSeries(GameObject parent, int depth, int series, int stageNumber, float parentX, float parentY)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = string.Format("{0:00}", series);
		UISprite uISprite = Util.CreateSprite("redRibon", parent.gameObject, image);
		string imageName = "instruction_accessory10";
		if (Def.IsGaidenSeries((Def.SERIES)series))
		{
			imageName = "instruction_accessory14";
		}
		Util.SetSpriteInfo(uISprite, imageName, depth + 4, new Vector3(parentX, parentY, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(172, 20);
		string text2 = string.Format(Localization.Get("SeasonTitle_" + text));
		UILabel uILabel = Util.CreateLabel("RedDesc", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text2, depth + 5, new Vector3(0f, 0.7f, 0f), 14, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		uILabel.effectStyle = UILabel.Effect.Shadow;
		uILabel.effectColor = Color.black;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(148, 14);
		UISprite uISprite2 = Util.CreateSprite("SeriesIcon", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "panel_season" + text + "_fver", depth + 3, new Vector3(-44f, -14f, 0f), Vector3.one, false);
		uISprite2.SetDimensions(85, 47);
		string text3 = string.Format(Localization.Get("MyStage_Desk"));
		UILabel uILabel2 = Util.CreateLabel("StageDesk", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel2, text3, depth + 4, new Vector3(-9f, -30f, 0f), 14, 0, 0, UIWidget.Pivot.Center);
		uILabel2.color = new Color(0.30980393f, 4f / 15f, 0.03137255f, 1f);
		uILabel2.effectStyle = UILabel.Effect.Outline;
		uILabel2.effectColor = Color.white;
		uILabel2 = Util.CreateLabel("StageNumDesk", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel2, string.Empty + stageNumber, depth + 4, new Vector3(52f, -26f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel2.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel2.color = new Color(0.30980393f, 4f / 15f, 0.03137255f, 1f);
	}
}
