public class GetMail : ParameterObject<GetMail>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("cnt")]
	public int MailCount { get; set; }

	public GetMail()
	{
		if (SingletonMonoBehaviour<GameMain>.Instance.mGameProfile != null)
		{
			MailCount = SingletonMonoBehaviour<GameMain>.Instance.mGameProfile.MailGetCount;
		}
		else
		{
			MailCount = Constants.GIFT_AMOUNT;
		}
	}
}
