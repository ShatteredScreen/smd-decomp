using System;
using System.Collections.Generic;

public class SeriesSelectItem : SMVerticalListItem, IComparable
{
	public string ImageName { get; set; }

	public string TitleKey { get; set; }

	public Def.SERIES Series { get; set; }

	public int MaxStage { get; set; }

	public int MaxStar { get; set; }

	public List<CharaSilhouettte> SilhouetteList { get; set; }

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as SeriesSelectItem);
	}

	public int CompareTo(SeriesSelectItem _obj)
	{
		return Series - _obj.Series;
	}

	public static SeriesSelectItem Make(Def.SERIES a_series, int a_maxStage, int a_maxStar, string a_imageName, string a_titleKey, List<CharaSilhouettte> a_charaList)
	{
		SeriesSelectItem seriesSelectItem = new SeriesSelectItem();
		seriesSelectItem.Series = a_series;
		seriesSelectItem.MaxStage = a_maxStage;
		seriesSelectItem.MaxStar = a_maxStar;
		seriesSelectItem.ImageName = a_imageName;
		seriesSelectItem.TitleKey = a_titleKey;
		seriesSelectItem.SilhouetteList = new List<CharaSilhouettte>(a_charaList);
		return seriesSelectItem;
	}
}
