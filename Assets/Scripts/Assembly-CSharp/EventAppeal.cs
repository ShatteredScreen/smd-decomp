using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAppeal : MonoBehaviour
{
	public enum STATE
	{
		INIT = 0,
		INIT_WAIT = 1,
		ACTION = 2,
		MAIN = 3,
		FADEOUT = 4,
		FADEOUT_WAIT = 5,
		SWITCH_FADEIN = 6,
		SWITCH_FADEIN_WAIT = 7,
		END = 8,
		WAIT = 9
	}

	public enum NormalEventState
	{
		INIT = 0,
		FADE_IN = 1,
		FADE_IN_WAIT = 2,
		SKILL_CUTIN = 3,
		SKILL_CUTIN_WAIT = 4,
		ANIME_START = 5,
		ANIME_WAIT = 6,
		MAIN = 7
	}

	public enum AdvEventState
	{
		INIT = 0,
		FADE_IN = 1,
		FADE_IN_WAIT = 2,
		ANIME_START = 3,
		ANIME_WAIT = 4,
		MAIN = 5
	}

	protected delegate bool UpdateFunc(float a_deltaTime);

	protected const float FORCE_ANIMATION_FINISH_TIMER = 5f;

	protected const float FADE_IN_Z = 100f;

	protected const float MAIN_Z = 0f;

	protected const float BACK1_Z = 21f;

	protected const float BACK2_Z = 20f;

	protected const float BACK3_Z = 19f;

	protected const float LACE_Z = 10f;

	protected const float PARTICLE_Z = 10f;

	protected const float FADE_OUT_Z = -100f;

	protected const float SSAX_Y = 0f;

	protected const float BACK_Y = -64f;

	protected const float BACKP_Y = 93f;

	protected const float LACE_Y = -72f;

	protected const float PARTICLESOUND_INTERVAL = 2f;

	protected const int PARTICLESOUND_COUNT = 5;

	protected const float LANDSCAPE_ANIME_MAX_SCALE = 0.836f;

	protected const float LANDSCAPE_ANIME_MIN_SCALE = 0.6f;

	protected const int ANIME_COMP_FRAME = 74;

	protected const int ANIME_COMP_ADV_FRAME = 74;

	private const long MAX_CRAM_DURATION_TIME = 99999999999L;

	public const float EFFECT_SIZE = 0.6f;

	public const float EFFECT_BG_SIZE = 1f;

	private const float BACKDISP_SPEED = 272.72726f;

	private const float LACEDISP_SPEED = 136.36363f;

	public StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	protected GameMain mGame;

	protected int mPartnerID;

	protected int mSkillRank = 2;

	protected string mLoadImageDictKey = string.Empty;

	protected string mLoadLive2DDictKey = string.Empty;

	protected string mLoadSoundDictKey = string.Empty;

	protected List<string> mLoadAnimeDictKeyList = new List<string>();

	protected List<ResourceInstance> mLoadInstanceList = new List<ResourceInstance>();

	protected string mAnimePrefix = string.Empty;

	protected OneShotAnime mEventAnime;

	protected LoopAnime mBackAnime1;

	protected LoopAnime mBackAnime2;

	protected LoopAnime mBackAnime3;

	protected PartsChangedSprite mLaceSprite;

	protected PartsChangedSprite mFadeInAnime;

	protected PartsChangedSprite mFadeOutAnime;

	protected UpdateFunc mUpdateFunc;

	protected GameObject mAnchorBottom;

	protected GameObject mAnchorTopRight;

	protected GameObject mAnchorBottomRight;

	protected UILabel mEventScheduleText;

	protected UIButton mSkipButton;

	protected UIButton mCloseButton;

	protected UIButton mNextButton;

	protected bool mIsAnimationFinished;

	protected float mAnimationForceFinishTimer;

	protected GameObject mCameraRoot;

	protected GameObject mRoot;

	protected GameObject mSkillCutinRoot;

	protected int mBaseDepth;

	protected ResImage mAtlasHud;

	protected UIFont mFont;

	protected bool mFadeInToNext;

	protected bool mFadeOutToNext;

	protected bool mSwichFadeInToNext;

	protected DateTime mStartTime;

	protected DateTime mEndTime;

	protected string mEventScheduleLayer = "event_schedule";

	protected string mEventScheduleScaleLayer = "event_set_main";

	protected float mParticleSoundTimeInterval;

	protected bool mIsParticleSoundFinished;

	protected int mParticleSoundCount;

	protected Vector3 mSkipButtonPos = Vector3.zero;

	protected Vector3 mNextButtonPos = Vector3.zero;

	protected Vector3 mCloseButtonPos = Vector3.zero;

	protected float mSkipButtonScale = 1f;

	protected float mNextButtonScale = 1f;

	protected float mCloseButtonScale = 1f;

	protected bool mIsSkipped;

	protected ScreenOrientation mOldOrientation;

	private DateTime mSkipBeginDate;

	private DateTime mPlayBeginDate;

	private long mSkipTime;

	private bool mIsBGMVolumeRevert;

	protected static readonly string EventAnimeName = "_MAIN";

	protected static readonly string EventBackAnime1Name = "_BACK1";

	protected static readonly string EventBackAnime2Name = "_BACK2";

	protected static readonly string EventBackAnime3Name = "_BACK3";

	protected static readonly string EventFixedAnimeName = "_FIXED";

	protected StatusManager<NormalEventState> mNormalEventState = new StatusManager<NormalEventState>(NormalEventState.INIT);

	private Partner mPartner;

	protected bool mIsCutinFinished;

	protected StatusManager<AdvEventState> mAdvEventState = new StatusManager<AdvEventState>(AdvEventState.INIT);

	protected bool mIsCalledEffectSound;

	public bool IsAdvMode { get; protected set; }

	public int EventID { get; protected set; }

	protected bool IsBGShow { get; set; }

	public bool HasNextPushed { get; protected set; }

	private void Start()
	{
		mSkipBeginDate = (mPlayBeginDate = DateTime.Now);
		mGame.mServerCramVariableData.EventAppealCount++;
		mGame.SaveServerCramVariableData();
	}

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void OnDestroy()
	{
		if (!string.IsNullOrEmpty(mLoadImageDictKey))
		{
			ResourceManager.UnloadImage(mLoadImageDictKey);
		}
		if (!string.IsNullOrEmpty(mLoadSoundDictKey))
		{
			ResourceManager.UnloadSound(mLoadSoundDictKey);
		}
		if (!string.IsNullOrEmpty(mLoadLive2DDictKey))
		{
			ResourceManager.UnloadLive2DAnimation(mLoadLive2DDictKey);
		}
		for (int i = 0; i < mLoadAnimeDictKeyList.Count; i++)
		{
			ResourceManager.UnloadSsAnimation(mLoadAnimeDictKeyList[i]);
		}
		if (mPartner != null)
		{
			UnityEngine.Object.Destroy(mPartner.gameObject);
			mPartner = null;
		}
		if (mCameraRoot != null)
		{
			UnityEngine.Object.Destroy(mCameraRoot);
			mCameraRoot = null;
		}
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			LoadComponent();
			mState.Change(STATE.INIT_WAIT);
			break;
		case STATE.INIT_WAIT:
		{
			bool flag = true;
			if (mPartner != null && !mPartner.IsPartnerLoadFinished())
			{
				flag = false;
			}
			for (int i = 0; i < mLoadInstanceList.Count; i++)
			{
				if (mLoadInstanceList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				mState.Change(STATE.ACTION);
			}
			break;
		}
		case STATE.ACTION:
			if (mUpdateFunc != null && mUpdateFunc(Time.deltaTime))
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.FADEOUT:
			if (mNextButton != null)
			{
				UnityEngine.Object.Destroy(mNextButton.gameObject);
				mNextButton = null;
			}
			if (mSkipButton != null)
			{
				UnityEngine.Object.Destroy(mSkipButton.gameObject);
				mSkipButton = null;
			}
			if (mCloseButton != null)
			{
				UnityEngine.Object.Destroy(mCloseButton.gameObject);
				mCloseButton = null;
			}
			if (mEventScheduleText != null)
			{
				UnityEngine.Object.Destroy(mEventScheduleText.gameObject);
				mEventScheduleText = null;
			}
			mIsParticleSoundFinished = true;
			ShowFadeOutAnimation();
			mState.Reset(STATE.FADEOUT_WAIT, true);
			break;
		case STATE.FADEOUT_WAIT:
			if (mFadeOutToNext)
			{
				if (HasNextPushed)
				{
					mState.Change(STATE.END);
				}
				else
				{
					DoSwitchFadeIn();
				}
			}
			break;
		case STATE.SWITCH_FADEIN:
			ShowSwitchFadeInAnimation();
			mState.Reset(STATE.SWITCH_FADEIN_WAIT, true);
			break;
		case STATE.SWITCH_FADEIN_WAIT:
			if (mSwichFadeInToNext)
			{
				mState.Change(STATE.END);
			}
			break;
		}
		mState.Update();
		if (mGame.mBackKeyTrg)
		{
			OnBackPushed();
		}
		if (mState.GetStatus() != 0 && mOldOrientation != Util.ScreenOrientation)
		{
			mOldOrientation = Util.ScreenOrientation;
			OnScreenChanged(mOldOrientation);
		}
	}

	public bool IsAppealEnd()
	{
		return mState.GetStatus() == STATE.END;
	}

	public void DoSwitchFadeIn()
	{
		if (mRoot != null)
		{
			UnityEngine.Object.Destroy(mRoot);
			mRoot = null;
		}
		mState.Change(STATE.SWITCH_FADEIN);
	}

	public void Init(EventAppealSettings a_setting)
	{
		IsAdvMode = a_setting.IsAdvEvent > 0;
		EventID = a_setting.EventID;
		IsBGShow = a_setting.IsShowBG == 1;
		bool flag = false;
		bool a_ishold = false;
		if (IsAdvMode)
		{
			mUpdateFunc = AdvEventUpdate;
			flag = mGame.GetAdvEventSchedule(EventID, out mStartTime, out mEndTime, out a_ishold);
		}
		else
		{
			mPartnerID = a_setting.PartnerID;
			mUpdateFunc = NormalEventUpdate;
			flag = mGame.GetEventSchedule(EventID, out mStartTime, out mEndTime, out a_ishold);
		}
		mStartTime = mStartTime.ToUniversalTime();
		mEndTime = mEndTime.ToUniversalTime();
		string text = "EVENTAPPEAL_";
		if (IsAdvMode)
		{
			text = "ADV_" + text;
		}
		text = string.Format(text + "{0:D3}", EventID);
		mAnimePrefix = text;
		if (flag)
		{
			flag = ResourceManager.EnableLoadImage(mAnimePrefix);
		}
		if (flag)
		{
			mState.Change(STATE.INIT);
		}
		else
		{
			mState.Change(STATE.END);
		}
	}

	public void LoadComponent()
	{
		mLoadImageDictKey = mAnimePrefix;
		mLoadAnimeDictKeyList.Add(mAnimePrefix + EventAnimeName);
		mLoadAnimeDictKeyList.Add(mAnimePrefix + EventBackAnime1Name);
		mLoadAnimeDictKeyList.Add(mAnimePrefix + EventBackAnime2Name);
		mLoadAnimeDictKeyList.Add(mAnimePrefix + EventBackAnime3Name);
		mLoadAnimeDictKeyList.Add(mAnimePrefix + EventFixedAnimeName);
		if (!IsAdvMode)
		{
			mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
			mPartner.Init((short)mPartnerID, new Vector3(0f, 10f, 0f), 1f, 3, base.gameObject, false, 0);
			if (mGame.mCompanionData[mPartnerID].skill2Id == -1)
			{
				mSkillRank = 2;
			}
			else
			{
				mSkillRank = 3;
			}
		}
		ResImage item = ResourceManager.LoadImageAsync(mLoadImageDictKey);
		mLoadInstanceList.Add(item);
		for (int i = 0; i < mLoadAnimeDictKeyList.Count; i++)
		{
			mLoadInstanceList.Add(ResourceManager.LoadSsAnimationAsync(mLoadAnimeDictKeyList[i]));
		}
		if (!string.IsNullOrEmpty(mLoadSoundDictKey))
		{
			mLoadInstanceList.Add(ResourceManager.LoadSoundAsync(mLoadSoundDictKey));
		}
		mCameraRoot = SkillEffectPlayer.CreateDemoCamera(true);
		Camera component = mCameraRoot.GetComponent<Camera>();
		mRoot = Util.CreateGameObject("EventAppealRoot", mCameraRoot);
		mAnchorTopRight = Util.CreateAnchorWithChild("EventAppealAnchorTopRight", mCameraRoot, UIAnchor.Side.TopRight, component).gameObject;
		mAnchorBottom = Util.CreateAnchorWithChild("EventAppealAnchorBottom", mCameraRoot, UIAnchor.Side.Bottom, component).gameObject;
		mAnchorBottomRight = Util.CreateAnchorWithChild("EventAppealAnchorBottomRight", mCameraRoot, UIAnchor.Side.BottomRight, component).gameObject;
		mAtlasHud = ResourceManager.LoadImage("HUD");
		mFont = GameMain.LoadFont();
		OnScreenChanged(Util.ScreenOrientation);
	}

	protected void OnScreenChanged(ScreenOrientation o)
	{
		mSkipButtonPos = Vector3.zero;
		mNextButtonPos = Vector3.zero;
		mCloseButtonPos = Vector3.zero;
		mSkipButtonScale = 1f;
		mNextButtonScale = 1f;
		mCloseButtonScale = 1f;
		if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
		{
			if (mRoot != null)
			{
				mRoot.transform.localScale = Vector3.one;
				mRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
			}
			mSkipButtonPos = new Vector3(-80f, 67f, 0f);
			mCloseButtonPos = new Vector3(-41f, -67f, 0f);
			mNextButtonPos = new Vector3(0f, 100f, 0f);
		}
		else
		{
			float num = 0.6f;
			num = 0.6f + 0.236f * (Util.LogScreenSize().y - 639f) / 311f;
			if (mRoot != null)
			{
				mRoot.transform.localScale = new Vector3(num, num, 1f);
				mRoot.transform.localPosition = new Vector3(0f, -50f, 0f);
			}
			mSkipButtonPos = new Vector3(-120f, 67f, 0f);
			mCloseButtonPos = new Vector3(-85f, -67f, 0f);
			mNextButtonPos = new Vector3(365f, 50f, 0f);
			mSkipButtonScale = 0.8f;
		}
		if (mNextButton != null)
		{
			mNextButton.transform.localPosition = mNextButtonPos;
		}
		if (mSkipButton != null)
		{
			mSkipButton.transform.localPosition = mSkipButtonPos;
			mSkipButton.transform.localScale = new Vector3(mSkipButtonScale, mSkipButtonScale, 1f);
		}
		if (mCloseButton != null)
		{
			mCloseButton.transform.localPosition = mCloseButtonPos;
		}
		mOldOrientation = o;
	}

	protected bool NormalEventUpdate(float a_deltaTime)
	{
		bool result = false;
		switch (mNormalEventState.GetStatus())
		{
		case NormalEventState.INIT:
			InitComponent();
			mNormalEventState.Change(NormalEventState.FADE_IN);
			break;
		case NormalEventState.FADE_IN:
			ShowFadeInAnimation();
			mNormalEventState.Change(NormalEventState.FADE_IN_WAIT);
			break;
		case NormalEventState.FADE_IN_WAIT:
			if (mFadeInToNext)
			{
				mNormalEventState.Change(NormalEventState.SKILL_CUTIN);
			}
			break;
		case NormalEventState.SKILL_CUTIN:
			mIsCutinFinished = false;
			StartCoroutine(CoSkill(mSkillRank));
			mNormalEventState.Change(NormalEventState.SKILL_CUTIN_WAIT);
			break;
		case NormalEventState.SKILL_CUTIN_WAIT:
			if (mIsCutinFinished)
			{
				mNormalEventState.Change(NormalEventState.ANIME_START);
			}
			break;
		case NormalEventState.ANIME_START:
			ShowEventSchedule();
			mEventAnime.Play();
			mIsAnimationFinished = false;
			mAnimationForceFinishTimer = 0f;
			mNormalEventState.Change(NormalEventState.ANIME_WAIT);
			break;
		case NormalEventState.ANIME_WAIT:
			if (!mIsAnimationFinished)
			{
				mAnimationForceFinishTimer += a_deltaTime;
				if (mAnimationForceFinishTimer >= 5f)
				{
					OnEventAnimeFinished(null);
				}
			}
			if (mIsAnimationFinished)
			{
				mNormalEventState.Change(NormalEventState.MAIN);
			}
			break;
		case NormalEventState.MAIN:
			result = true;
			break;
		}
		mNormalEventState.Update();
		if (mEventAnime != null)
		{
			if (mEventScheduleText != null)
			{
				Vector3 vector = new Vector3(0f, 0f, 0f);
				Vector3 vector2 = new Vector3(10000f, 10000f, 10000f);
				Vector3 vector3 = Vector3.zero;
				SsSprite sprite = mEventAnime._sprite;
				SsPartRes[] partList = sprite.Animation.PartList;
				bool flag = false;
				bool flag2 = false;
				for (int i = 0; i < partList.Length; i++)
				{
					if (!partList[i].IsRoot)
					{
						if (partList[i].Name.CompareTo(mEventScheduleLayer) == 0)
						{
							vector = new Vector3(partList[i].PosX((int)sprite.AnimFrame), 0f - partList[i].PosY((int)sprite.AnimFrame), 0f);
							flag = true;
						}
						else if (partList[i].Name.CompareTo(mEventScheduleScaleLayer) == 0)
						{
							vector2 = new Vector3(partList[i].PosX((int)sprite.AnimFrame), 0f - partList[i].PosY((int)sprite.AnimFrame), 0f);
							vector3 = new Vector3(partList[i].ScaleX((int)sprite.AnimFrame), partList[i].ScaleY((int)sprite.AnimFrame), 0f);
							flag2 = true;
						}
						if (flag && flag2)
						{
							break;
						}
					}
				}
				mEventScheduleText.transform.localPosition = mEventAnime.transform.localPosition + vector2 + vector;
				mEventScheduleText.transform.localScale = new Vector3(mEventAnime.transform.localScale.x * vector3.x, mEventAnime.transform.localScale.y * vector3.y, 1f);
			}
			if (mEventAnime._sprite.AnimFrame >= 74f)
			{
				ShowLaceAnimation();
				ShowEventSchedule();
				OnEffectStart(string.Empty);
			}
		}
		return result;
	}

	public void InitComponent()
	{
		mEventAnime = Util.CreateOneShotAnime("EventAppeal" + EventID, mAnimePrefix + EventAnimeName, mRoot, new Vector3(0f, 0f, 0f), Vector3.one, 0f, 0f, false, OnEventAnimeFinished);
		mEventAnime.AddUserKeyTrigger("user_key", new Dictionary<string, OneShotAnime.UserKeyHandler>
		{
			{ "ef_start", OnEffectStart },
			{ "lace_start", OnLaceLoopEffectStart }
		});
		mEventAnime.AddSoundTrigger("sound_key", new Dictionary<string, string>
		{
			{ "title_se", "SE_DIALY_COMPLETE_STAMP" },
			{ "chara_se", "SE_DEMO_SMOKE" }
		});
		mEventAnime.Pause();
		mSkipButton = Util.CreateJellyImageButton("SkipButton", mAnchorBottomRight.gameObject, mAtlasHud.Image);
		Util.SetImageButtonInfo(mSkipButton, "LC_button_skip2", "LC_button_skip2", "LC_button_skip2", mBaseDepth + 10, mSkipButtonPos, new Vector3(mSkipButtonScale, mSkipButtonScale, 1f));
		Util.AddImageButtonMessage(mSkipButton, this, "OnSkipButtonPushed", UIButtonMessage.Trigger.OnClick);
	}

	protected IEnumerator CoSkill(int rank)
	{
		mSkillCutinRoot = Util.CreateGameObject("CutinRoot", mCameraRoot);
		bool skillWait = true;
		SkillEffectPlayer skillEffect = Util.CreateGameObject("SkillManager", mSkillCutinRoot).AddComponent<SkillEffectPlayer>();
		skillEffect.Init(mPartner, rank, mSkillCutinRoot, SkillEffectPlayer.MODE.APPEAL, delegate
		{
			skillWait = false;
		});
		while (skillWait)
		{
			yield return 0;
		}
		if (mSkillCutinRoot != null)
		{
			UnityEngine.Object.Destroy(skillEffect.gameObject);
			UnityEngine.Object.Destroy(mSkillCutinRoot);
			mSkillCutinRoot = null;
		}
		mIsCutinFinished = true;
	}

	protected bool AdvEventUpdate(float a_deltaTime)
	{
		bool result = false;
		switch (mAdvEventState.GetStatus())
		{
		case AdvEventState.INIT:
			InitComponent();
			mAdvEventState.Change(AdvEventState.FADE_IN);
			break;
		case AdvEventState.FADE_IN:
			ShowFadeInAnimation();
			mAdvEventState.Change(AdvEventState.FADE_IN_WAIT);
			break;
		case AdvEventState.FADE_IN_WAIT:
			if (mFadeInToNext)
			{
				mAdvEventState.Change(AdvEventState.ANIME_START);
			}
			break;
		case AdvEventState.ANIME_START:
			ShowEventSchedule();
			mEventAnime.Play();
			mIsAnimationFinished = false;
			mAnimationForceFinishTimer = 0f;
			mAdvEventState.Change(AdvEventState.ANIME_WAIT);
			break;
		case AdvEventState.ANIME_WAIT:
			if (!mIsAnimationFinished)
			{
				mAnimationForceFinishTimer += a_deltaTime;
				if (mAnimationForceFinishTimer >= 5f)
				{
					OnEventAnimeFinished(null);
				}
			}
			if (mIsAnimationFinished)
			{
				mAdvEventState.Change(AdvEventState.MAIN);
			}
			break;
		case AdvEventState.MAIN:
			result = true;
			break;
		}
		mAdvEventState.Update();
		if (mEventAnime != null)
		{
			if (mEventScheduleText != null)
			{
				Vector3 vector = new Vector3(0f, 0f, 0f);
				Vector3 vector2 = new Vector3(10000f, 10000f, 10000f);
				Vector3 vector3 = Vector3.zero;
				SsSprite sprite = mEventAnime._sprite;
				SsPartRes[] partList = sprite.Animation.PartList;
				bool flag = false;
				bool flag2 = false;
				for (int i = 0; i < partList.Length; i++)
				{
					if (!partList[i].IsRoot)
					{
						if (partList[i].Name.CompareTo(mEventScheduleLayer) == 0)
						{
							vector = new Vector3(partList[i].PosX((int)sprite.AnimFrame), 0f - partList[i].PosY((int)sprite.AnimFrame), 0f);
							flag = true;
						}
						else if (partList[i].Name.CompareTo(mEventScheduleScaleLayer) == 0)
						{
							vector2 = new Vector3(partList[i].PosX((int)sprite.AnimFrame), 0f - partList[i].PosY((int)sprite.AnimFrame), 0f);
							vector3 = new Vector3(partList[i].ScaleX((int)sprite.AnimFrame), partList[i].ScaleY((int)sprite.AnimFrame), 0f);
							flag2 = true;
						}
						if (flag && flag2)
						{
							break;
						}
					}
				}
				mEventScheduleText.transform.localPosition = mEventAnime.transform.localPosition + vector2 + vector;
				mEventScheduleText.transform.localScale = new Vector3(mEventAnime.transform.localScale.x * vector3.x, mEventAnime.transform.localScale.y * vector3.y, 1f);
			}
			if (mEventAnime._sprite.AnimFrame >= 74f)
			{
				ShowLaceAnimation();
				ShowEventSchedule();
				OnEffectStart(string.Empty);
			}
		}
		return result;
	}

	public void InitComponentAdv()
	{
		InitComponent();
	}

	protected void OnEffectStart(string a_key)
	{
		if (mBackAnime1 == null)
		{
			mBackAnime1 = Util.CreateLoopAnime("EventAppealBack1", mAnimePrefix + EventBackAnime1Name, mRoot, new Vector3(0f, -64f, 21f), Vector3.one, 0f, 0f, false, null);
			mBackAnime1.transform.localScale = Vector3.zero;
			StartCoroutine(BackEffectAnimeDisplay(mBackAnime1));
		}
		if (mBackAnime2 == null)
		{
			mBackAnime2 = Util.CreateLoopAnime("EventAppealBack2", mAnimePrefix + EventBackAnime2Name, mRoot, new Vector3(0f, 93f, 20f), Vector3.one, 0f, 0f, false, null);
		}
		if (mBackAnime3 == null)
		{
			mBackAnime3 = Util.CreateLoopAnime("EventAppealBack3", mAnimePrefix + EventBackAnime3Name, mRoot, new Vector3(0f, 93f, 19f), Vector3.one, 0f, 0f, false, null);
		}
		if (!mIsCalledEffectSound)
		{
			StartCoroutine(SoundParticle());
			mIsCalledEffectSound = true;
		}
	}

	protected void OnLaceLoopEffectStart(string a_key)
	{
		ShowLaceAnimation();
	}

	protected void OnEventAnimeFinished(SsSprite a_anime)
	{
		if (mSkipButton != null)
		{
			UnityEngine.Object.Destroy(mSkipButton.gameObject);
			mSkipButton = null;
		}
		mNextButton = Util.CreateJellyImageButton("NextButton", mAnchorBottom.gameObject, mAtlasHud.Image);
		Util.SetImageButtonInfo(mNextButton, "LC_button_event_go", "LC_button_event_go", "LC_button_event_go", mBaseDepth + 10, mNextButtonPos, Vector3.one);
		Util.AddImageButtonMessage(mNextButton, this, "OnNextButtonPushed", UIButtonMessage.Trigger.OnClick);
		mCloseButton = Util.CreateJellyImageButton("CloseButton", mAnchorTopRight.gameObject, mAtlasHud.Image);
		Util.SetImageButtonInfo(mCloseButton, "button_return", "button_return", "button_return", mBaseDepth + 10, mCloseButtonPos, Vector3.one);
		Util.AddImageButtonMessage(mCloseButton, this, "OnCloseButtonPushed", UIButtonMessage.Trigger.OnClick);
		mIsAnimationFinished = true;
	}

	private void OnBackPushed()
	{
		if (mCloseButton != null)
		{
			OnCloseButtonPushed(mCloseButton.gameObject);
		}
	}

	protected void OnNextButtonPushed(GameObject go)
	{
		if (IsAdvMode)
		{
			if (mAdvEventState.GetStatus() != AdvEventState.MAIN)
			{
				return;
			}
		}
		else if (mNormalEventState.GetStatus() != NormalEventState.MAIN)
		{
			return;
		}
		long num = (long)(DateTime.Now - mPlayBeginDate).TotalMilliseconds;
		num = ((num <= int.MaxValue) ? num : int.MaxValue);
		long num2 = ((mSkipTime <= int.MaxValue) ? mSkipTime : int.MaxValue);
		ServerCram.EventAppealStart(EventID, IsAdvMode ? 1 : 0, 1, mIsSkipped ? 1 : 0, (int)num2, (int)num, mGame.mServerCramVariableData.EventAppealCount);
		HasNextPushed = true;
		mGame.PlaySe("SE_POSITIVE", -1);
		mState.Reset(STATE.FADEOUT, true);
		if (mIsBGMVolumeRevert)
		{
			mGame.SetMusicVolume(1f, 0);
		}
	}

	protected void OnCloseButtonPushed(GameObject go)
	{
		if (IsAdvMode)
		{
			if (mAdvEventState.GetStatus() != AdvEventState.MAIN)
			{
				return;
			}
		}
		else if (mNormalEventState.GetStatus() != NormalEventState.MAIN)
		{
			return;
		}
		long num = (long)(DateTime.Now - mPlayBeginDate).TotalMilliseconds;
		num = ((num <= int.MaxValue) ? num : int.MaxValue);
		long num2 = ((mSkipTime <= int.MaxValue) ? mSkipTime : int.MaxValue);
		ServerCram.EventAppealStart(EventID, IsAdvMode ? 1 : 0, 0, mIsSkipped ? 1 : 0, (int)num2, (int)num, mGame.mServerCramVariableData.EventAppealCount);
		HasNextPushed = false;
		mGame.PlaySe("SE_NEGATIVE", -1);
		mState.Reset(STATE.FADEOUT, true);
		if (mIsBGMVolumeRevert)
		{
			mGame.SetMusicVolume(1f, 0);
		}
	}

	protected void OnSkipButtonPushed(GameObject go)
	{
		bool flag = true;
		if (IsAdvMode)
		{
			switch (mAdvEventState.GetStatus())
			{
			case AdvEventState.INIT:
			case AdvEventState.FADE_IN:
			case AdvEventState.ANIME_START:
			case AdvEventState.MAIN:
				flag = false;
				break;
			}
		}
		else
		{
			switch (mNormalEventState.GetStatus())
			{
			case NormalEventState.INIT:
			case NormalEventState.FADE_IN:
			case NormalEventState.SKILL_CUTIN:
			case NormalEventState.ANIME_START:
			case NormalEventState.MAIN:
				flag = false;
				break;
			}
		}
		if (flag)
		{
			mIsSkipped = true;
			mSkipTime = (long)(DateTime.Now - mSkipBeginDate).TotalMilliseconds;
			mGame.PlaySe("SE_NEGATIVE", -1);
			if (IsAdvMode)
			{
				mAdvEventState.Reset(AdvEventState.MAIN, true);
			}
			else
			{
				mNormalEventState.Reset(NormalEventState.MAIN, true);
			}
			if (mPartner != null)
			{
				UnityEngine.Object.Destroy(mPartner.gameObject);
				mPartner = null;
			}
			if (mSkillCutinRoot != null)
			{
				UnityEngine.Object.Destroy(mSkillCutinRoot);
				mSkillCutinRoot = null;
			}
			if (mEventAnime != null)
			{
				ShowEventSchedule();
				OnEffectStart(string.Empty);
				OnLaceLoopEffectStart(string.Empty);
				mEventAnime.SkipToEnd();
				mEventAnime.Play();
			}
		}
	}

	protected void ShowEventSchedule()
	{
		if (mEventScheduleText == null)
		{
			string text = string.Format(Localization.Get("EventAppeal_Schedule"), mStartTime.Month, mStartTime.Day, Localization.Get("DayOfWeek_" + (int)mStartTime.DayOfWeek), mEndTime.Month, mEndTime.Day, Localization.Get("DayOfWeek_" + (int)mEndTime.DayOfWeek), mEndTime.Hour, mEndTime.Minute);
			mEventScheduleText = Util.CreateLabel("EventSchedule", mRoot, mFont);
			Util.SetLabelInfo(mEventScheduleText, text, mBaseDepth + 10, new Vector3(10000f, 10000f, 10000f), 32, 0, 0, UIWidget.Pivot.Center);
			mEventScheduleText.effectStyle = UILabel.Effect.Outline;
			mEventScheduleText.effectColor = new Color(0.03137255f, 5f / 51f, 19f / 85f);
			mEventScheduleText.height = 35;
			if (!mIsBGMVolumeRevert)
			{
				mGame.SetMusicVolume(0.25f, 0);
				mIsBGMVolumeRevert = true;
			}
		}
	}

	protected void ShowLaceAnimation()
	{
		if (!IsAdvMode && mLaceSprite == null)
		{
			CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[1] { default(CHANGE_PART_INFO) };
			array[0].partName = "change000";
			array[0].spriteName = "back_lace";
			mLaceSprite = Util.CreateGameObject("LaceObject", mEventAnime.gameObject).AddComponent<PartsChangedSprite>();
			mLaceSprite.Atlas = ResourceManager.LoadImageAsync(mAnimePrefix).Image;
			mLaceSprite.SetBaseColor(new Color(1f, 1f, 1f, 0f));
			mLaceSprite.ChangeAnime(mAnimePrefix + EventFixedAnimeName, array, true, 0, null);
			mLaceSprite.transform.localPosition = new Vector3(0f, -72f, 10f);
			mLaceSprite.transform.localScale = new Vector3(1.2f, 1.2f, 1f);
			mLaceSprite.enabled = false;
			StartCoroutine(LaceDisplay());
			StartCoroutine(LaceRotate());
		}
	}

	protected void ShowFadeInAnimation()
	{
		if (mFadeInAnime == null)
		{
			CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[1] { default(CHANGE_PART_INFO) };
			array[0].partName = "change000";
			array[0].spriteName = "color";
			mFadeInAnime = Util.CreateGameObject("FadeInAnime", mEventAnime.gameObject).AddComponent<PartsChangedSprite>();
			mFadeInAnime.Atlas = ResourceManager.LoadImageAsync(mAnimePrefix).Image;
			mFadeInAnime.transform.localScale = Vector3.zero;
			mFadeInAnime.transform.localPosition = new Vector3(10000f, 10000f, 100f);
			mFadeInAnime.SetBaseColor(new Color(1f, 1f, 1f, 0f));
			mFadeInAnime.ChangeAnime(mAnimePrefix + EventFixedAnimeName, array, true, 0, null);
			mFadeInAnime.enabled = false;
			StartCoroutine(FadeInDisplay());
		}
	}

	protected void ShowFadeOutAnimation()
	{
		if (mFadeOutAnime == null)
		{
			CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[1] { default(CHANGE_PART_INFO) };
			array[0].partName = "change000";
			array[0].spriteName = "black";
			mFadeOutAnime = Util.CreateGameObject("FadeInAnime", mCameraRoot).AddComponent<PartsChangedSprite>();
			mFadeOutAnime.Atlas = ResourceManager.LoadImageAsync(mAnimePrefix).Image;
			mFadeOutAnime.transform.localScale = Vector3.zero;
			mFadeOutAnime.transform.localPosition = new Vector3(10000f, 10000f, 100f);
			mFadeOutAnime.SetBaseColor(new Color(1f, 1f, 1f, 0f));
			mFadeOutAnime.ChangeAnime(mAnimePrefix + EventFixedAnimeName, array, true, 0, null);
			mFadeOutAnime.enabled = false;
			StartCoroutine(FadeOutDisplay());
		}
	}

	protected void ShowSwitchFadeInAnimation()
	{
		if (mFadeOutAnime != null)
		{
			StartCoroutine(SwitchFadeInDisplay());
		}
		else
		{
			mSwichFadeInToNext = true;
		}
	}

	protected IEnumerator SoundParticle()
	{
		mIsParticleSoundFinished = false;
		mParticleSoundTimeInterval = 1f;
		mParticleSoundCount = 0;
		while (!mIsParticleSoundFinished)
		{
			mParticleSoundTimeInterval += Time.deltaTime;
			if (mParticleSoundTimeInterval > 2f)
			{
				mParticleSoundTimeInterval = 0f;
				mParticleSoundCount++;
				mGame.PlaySe("SE_MAP_OPEN_AREA", -1, 0.8f);
			}
			if (mParticleSoundCount >= 5)
			{
				break;
			}
			yield return null;
		}
	}

	protected IEnumerator BackEffectAnimeDisplay(LoopAnime a_anim)
	{
		float angle = 0f;
		if (a_anim == null)
		{
			yield break;
		}
		yield return null;
		a_anim.enabled = true;
		a_anim.transform.localScale = new Vector3(0f, 0f, 1f);
		while (true)
		{
			bool f = false;
			angle += Time.deltaTime * 272.72726f;
			if (angle >= 90f)
			{
				angle = 90f;
				f = true;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			if (alpha >= 1f)
			{
				alpha = 1f;
			}
			if (a_anim == null)
			{
				break;
			}
			a_anim.transform.localScale = new Vector3(alpha, alpha, 1f);
			if (f)
			{
				break;
			}
			yield return null;
		}
	}

	protected IEnumerator LaceDisplay()
	{
		float angle = 0f;
		if (mLaceSprite == null)
		{
			yield break;
		}
		yield return null;
		mLaceSprite.enabled = true;
		while (true)
		{
			bool f = false;
			angle += Time.deltaTime * 136.36363f;
			if (angle >= 90f)
			{
				angle = 90f;
				f = true;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			if (alpha >= 1f)
			{
				alpha = 1f;
			}
			mLaceSprite.SetColor(new Color(1f, 1f, 1f, alpha));
			if (f)
			{
				break;
			}
			yield return null;
		}
	}

	protected IEnumerator FadeInDisplay()
	{
		mFadeInToNext = false;
		float angle = 0f;
		if (mFadeInAnime == null)
		{
			mFadeInToNext = true;
			yield break;
		}
		yield return null;
		mFadeInAnime.enabled = true;
		mFadeInAnime.transform.localPosition = new Vector3(0f, 0f, 100f);
		mFadeInAnime.transform.localScale = new Vector3(40f, 40f, 1f);
		while (true)
		{
			bool f = false;
			angle += Time.deltaTime * 240f;
			if (angle >= 90f)
			{
				angle = 90f;
				f = true;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f)) * 0.8f;
			mFadeInAnime.SetColor(new Color(1f, 1f, 1f, alpha));
			if (f)
			{
				break;
			}
			yield return null;
		}
		mFadeInToNext = true;
	}

	protected IEnumerator FadeOutDisplay()
	{
		mFadeOutToNext = false;
		float angle = 0f;
		if (mFadeOutAnime == null)
		{
			mFadeOutToNext = true;
			yield break;
		}
		yield return null;
		mFadeOutAnime.enabled = true;
		mFadeOutAnime.transform.localPosition = new Vector3(0f, 0f, -100f);
		mFadeOutAnime.transform.localScale = new Vector3(284f, 284f, 1f);
		while (true)
		{
			bool f = false;
			angle += Time.deltaTime * 240f;
			if (angle >= 90f)
			{
				angle = 90f;
				f = true;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mFadeOutAnime.SetColor(new Color(1f, 1f, 1f, alpha));
			if (f)
			{
				break;
			}
			yield return null;
		}
		mFadeOutToNext = true;
	}

	protected IEnumerator SwitchFadeInDisplay()
	{
		mSwichFadeInToNext = false;
		float angle = 0f;
		if (mFadeOutAnime == null)
		{
			mSwichFadeInToNext = true;
			yield break;
		}
		yield return null;
		mFadeOutAnime.enabled = true;
		mFadeOutAnime.transform.localPosition = new Vector3(0f, 0f, -100f);
		mFadeOutAnime.transform.localScale = new Vector3(284f, 284f, 1f);
		while (true)
		{
			bool f = false;
			angle += Time.deltaTime * 240f;
			if (angle >= 90f)
			{
				angle = 90f;
				f = true;
			}
			float alpha = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mFadeOutAnime.SetColor(new Color(1f, 1f, 1f, 1f - alpha));
			if (f)
			{
				break;
			}
			yield return null;
		}
		mSwichFadeInToNext = true;
	}

	protected IEnumerator LaceRotate()
	{
		float angle = 0f;
		while (!(mLaceSprite == null))
		{
			angle += Time.deltaTime * 30f;
			if (angle > 360f)
			{
				angle = 0f;
			}
			mLaceSprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}
}
