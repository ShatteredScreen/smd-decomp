using System.Collections.Generic;

public class ConnectAdvGetMasterTableDataParam : ConnectParameterBase
{
	public List<AdvMasterDataInfo> MasterTableList { get; private set; }

	public ConnectAdvGetMasterTableDataParam(List<AdvMasterDataInfo> a_master_table_list)
	{
		MasterTableList = new List<AdvMasterDataInfo>(a_master_table_list);
	}
}
