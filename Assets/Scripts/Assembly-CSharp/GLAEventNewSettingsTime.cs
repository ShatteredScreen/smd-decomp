public class GLAEventNewSettingsTime
{
	[MiniJSONAlias("start")]
	public short mStart { get; set; }

	[MiniJSONAlias("end")]
	public short mEnd { get; set; }
}
