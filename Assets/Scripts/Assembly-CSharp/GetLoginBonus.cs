public class GetLoginBonus : ParameterObject<GetLoginBonus>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }
}
