using System.Collections.Generic;

public class SMAlternateRewardData
{
	public int BaseAccessoryID;

	public List<int> RewardList = new List<int>();

	public virtual void Serialize(BIJFileBinaryWriter sw)
	{
		sw.WriteInt(1290);
		sw.WriteInt(BaseAccessoryID);
		sw.WriteInt(RewardList.Count);
		for (int i = 0; i < RewardList.Count; i++)
		{
			sw.WriteInt(RewardList[i]);
		}
	}

	public virtual void Deserialize(BIJBinaryReader a_reader)
	{
		int num = a_reader.ReadInt();
		BaseAccessoryID = a_reader.ReadInt();
		int num2 = a_reader.ReadInt();
		for (int i = 0; i < num2; i++)
		{
			RewardList.Add(a_reader.ReadInt());
		}
	}

	public string GetString()
	{
		string empty = string.Empty;
		string text = empty;
		empty = text + "ID:" + BaseAccessoryID + "\n";
		empty += "Reward\n";
		foreach (int reward in RewardList)
		{
			empty = empty + reward + "\n";
		}
		return empty;
	}
}
