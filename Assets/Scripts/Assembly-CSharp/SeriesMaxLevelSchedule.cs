using System;

public class SeriesMaxLevelSchedule
{
	[MiniJSONAlias("maxlvl")]
	public int MaxLevel { get; set; }

	[MiniJSONAlias("start")]
	public long StartUnixTime { get; set; }

	[MiniJSONAlias("banner")]
	public string BannerFileName { get; set; }

	public DateTime StartTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(StartUnixTime).ToLocalTime();
		}
	}

	public SeriesMaxLevelSchedule()
	{
		MaxLevel = 100;
		StartUnixTime = 0L;
		BannerFileName = string.Empty;
	}
}
