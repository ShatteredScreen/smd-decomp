public enum GiftMailType
{
	GIFT = 0,
	FRIEND = 1,
	SUPPORT = 2,
	REWARD = 3,
	ADV = 4,
	FROM_ADV = 5,
	LOCAL = 90,
	LOCAL_EVENT = 91,
	LOCAL_MAP = 92,
	LOCAL_ADV = 93,
	LOCAL_TROPHY = 94
}
