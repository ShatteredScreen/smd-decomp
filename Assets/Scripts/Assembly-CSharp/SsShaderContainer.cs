using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SsShaderContainer : MonoBehaviour
{
	public List<Shader> _shaders = new List<Shader>();
}
