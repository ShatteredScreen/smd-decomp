using System;
using System.Collections;
using UnityEngine;

public class BIJSNSTwitter : BIJSNSAppSwitcher
{
	private const float ATTACHEMENT_RESIZE_SCALE = 0.5f;

	public override SNS Kind
	{
		get
		{
			return SNS.Twitter;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		OP_TYPE oP_TYPE = OP_TYPE.NONE;
		try
		{
			oP_TYPE = (OP_TYPE)(int)Enum.ToObject(typeof(OP_TYPE), type);
		}
		catch (Exception)
		{
			return false;
		}
		switch (oP_TYPE)
		{
		case OP_TYPE.POSTMESSAGE:
		case OP_TYPE.INVITE:
		case OP_TYPE.UPLOADIMAGE:
			return true;
		default:
			return false;
		}
	}

	private IEnumerator BIJSNSTwitter_CommonProcess_Coroutine(bool withUI, int seqno, IBIJSNSPostData _data, Handler handler, object userdata, float scale)
	{
		yield return new WaitForSeconds(0.5f);
		string title = BIJSNS.ToSS(_data.Title);
		string message = BIJSNS.ToSS(_data.Message) + ((!string.IsNullOrEmpty(_data.LinkURL)) ? ("\n" + BIJSNS.ToSS(_data.LinkURL)) : string.Empty);
		BIJSNS.dbg("@@@ SNS @@@ " + message);
		bool ret = BIJUnity.invokeTwitterWithMessage(title, message, string.Empty);
		BIJSNS.dbg("@@@ SNS @@@ SNSInvokeTwitterWithMessageAndImage: " + ret);
		if (!ret)
		{
			if (base.CurrentSeqNo == seqno)
			{
				base.CurrentRes = new Response(1, "Twitter did not invoked", null);
				ChangeStep(STEP.COMM_RESULT);
			}
			SetSpawn(0);
		}
	}

	private void DoPostMessage(bool withUI, OP_TYPE type, IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitter.DoPostMessage: " + type);
		if (spawn)
		{
			CallCallback(handler, new Response(1, "Twitter must logged in", null), userdata);
			return;
		}
		int num = PushReq((int)type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			base.surrogateMonoBehaviour.StartCoroutine(BIJSNSTwitter_CommonProcess_Coroutine(withUI, num, postdata, handler, userdata, 0.5f));
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "Twitter not requested", null), userdata);
		}
	}

	public override void PostMessage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitter.PostMessage");
		DoPostMessage(true, OP_TYPE.POSTMESSAGE, postdata, handler, userdata);
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitter.UploadImage");
		DoPostMessage(false, OP_TYPE.UPLOADIMAGE, postdata, handler, userdata);
	}

	public override void Invite(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSTwitter.Invite");
		DoPostMessage(true, OP_TYPE.INVITE, postdata, handler, userdata);
	}
}
