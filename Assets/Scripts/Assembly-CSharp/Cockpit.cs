using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cockpit : MonoBehaviour
{
	public enum STATE
	{
		INIT = 0,
		MOVE = 1,
		OPEN = 2,
		CLOSE = 3,
		MAIN = 4,
		WAIT = 5
	}

	protected enum MOVE_STATUS
	{
		OPENING = 0,
		OPEN = 1,
		CLOSING = 2,
		CLOSE = 3
	}

	public delegate void OpenFunction(bool opening);

	public delegate void CloseFunction(bool closing);

	protected delegate bool CheckDraw();

	protected const float SaleIconBasePosX_P = -70f;

	protected const float SaleIconBasePosY_P = 231f;

	protected const float SaleIconBasePosX_L = -70f;

	protected const float SaleIconBasePosY_L = 165f;

	protected const float SaleIconPlaceInterval = 122f;

	protected GameMain mGame;

	protected BIJImage HudAtlas;

	protected UIFont Font;

	protected GameStateSMMapBase mGSM;

	protected GameObject mAnchorT;

	protected GameObject mAnchorTL;

	protected GameObject mAnchorTR;

	protected GameObject mAnchorB;

	protected GameObject mAnchorBL;

	protected GameObject mAnchorBR;

	protected GameObject mAnchorC;

	protected Attention mMailAttention;

	protected Attention mMenuButtonAttention;

	protected UIButton HeartPlus;

	protected UIButton GemPlus;

	protected UIButton dailybutton;

	protected UIButton advbutton;

	protected UIButton shopBagbutton;

	protected UISprite shopBagSalesprite;

	protected UIButton RemainTimeButton;

	protected UIButton mugenHeartbutton;

	protected UISprite mugenHeartSalesprite;

	public int mMugenHeartItemID;

	protected UIButton mAllCrushButton;

	protected UISprite mAllCrushSalesprite;

	public int mAllCrushItemID;

	protected SlideWindowPanel mSlideWindowPanel;

	protected UIButton Menu0;

	protected UIButton Menu3;

	protected UIButton Menu5;

	protected UIButton Menu6;

	protected UIButton mButtonMenu;

	protected UIButton mButtonMailBox;

	protected UIButton mButtonStore;

	protected UIButton mButtonGameCenter;

	protected UIButton mButtonEvent;

	protected UIButton mButtonDebug;

	protected UIButton DL;

	protected UILabel mGrowDesc;

	protected bool mEventing;

	protected bool mTutorialend;

	protected bool mTutorialend2;

	protected bool mTutorialend3;

	protected bool mTutorialend4;

	protected bool mTutorialend5;

	protected bool mTutorialend6;

	protected bool mTutorialend7;

	protected bool mTutorialend8;

	protected bool mEventTutorialend;

	protected bool mShopTutorialend;

	protected bool mInfinityTutorialend;

	protected bool mSeasonTutorialend;

	protected bool mAdvTutorialend;

	protected bool mButtonEnableFlg = true;

	protected UISprite mEventLastDay;

	protected UISsSprite mEventButtonEffect;

	protected string mbutton_LvUP = "button_LvUP";

	protected string mGrowUpstr = "CockpitIconGrow";

	protected bool mALLbuttonflg = true;

	protected int mBasedepth;

	[SerializeField]
	protected bool mbDraw;

	[SerializeField]
	protected bool mbDrawOption;

	protected Vector2 mLogScreen = Vector2.zero;

	protected UIPanel mPanel;

	protected List<UIButton> mCockpitButtons = new List<UIButton>();

	protected UISprite mDownBoardL;

	protected UISprite mDownBoardR;

	protected SeriesCampaignButton mSCButton;

	protected UISprite mSeriesTitleSprite;

	protected UILabel mSeriesTitleLabel;

	protected UISprite mSeriesCampaignBand;

	protected int mForceDepth = -1;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	protected List<MOVE_STATUS> mMoveStatus = new List<MOVE_STATUS>();

	protected int PrevGrayScreenDepth = -1;

	protected OpenFunction mOpenCallback;

	protected CloseFunction mCloseCallback;

	protected GameObject mCockpitRoot;

	private bool mCreateLinkBannerEnable;

	private bool mShowLinkBannerEnable;

	public int mSaleItemID { get; protected set; }

	public int mSaleItemCount { get; protected set; }

	public bool mShopflg { get; protected set; }

	public int? mMugenHeartShopCount { get; private set; }

	public bool mMugenflg { get; protected set; }

	public bool mAllCrushFlg { get; protected set; }

	protected UIButton DailyChallengeButton { get; set; }

	private UISsSprite DailyChallengeEffect { get; set; }

	public Attention DailyChallengeAttention { get; private set; }

	public bool ShopMugenHeartEnable { get; set; }

	public bool ShopAllCrushEnable { get; set; }

	public bool ShopBagEnable { get; set; }

	public bool EventButtonEnble { get; private set; }

	public bool DailyEventEnble { get; private set; }

	public void SetForceDepth(int a_depth)
	{
		mForceDepth = a_depth;
	}

	protected int AddMoveStatus()
	{
		int count = mMoveStatus.Count;
		mMoveStatus.Add(MOVE_STATUS.CLOSE);
		return count;
	}

	public void SetCallback(OpenFunction open, CloseFunction close)
	{
		mOpenCallback = open;
		mCloseCallback = close;
	}

	public void SetEnableMailAttention(bool flag)
	{
		if (!(mMailAttention == null))
		{
			mMailAttention.SetAttentionEnable(flag);
		}
	}

	public void SetEnableMenuButtonAttention(bool flag)
	{
		if (!(mMenuButtonAttention == null))
		{
			mMenuButtonAttention.SetAttentionEnable(flag);
		}
	}

	public void SetLayoutCockpitPosition(ScreenOrientation o)
	{
		if (mDownBoardR != null && mDownBoardL != null)
		{
			float num = 123f;
			if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
			{
				mDownBoardR.transform.localPosition = new Vector3(-2f, 0f - num, 0f);
				mDownBoardL.transform.localPosition = new Vector3(2f, 0f - num, 0f);
			}
			else
			{
				mDownBoardR.transform.localPosition = new Vector3(-2f, -52f - num, 0f);
				mDownBoardL.transform.localPosition = new Vector3(2f, -52f - num, 0f);
			}
		}
	}

	public void SetEnableButtonAction(bool buttonflg)
	{
		mALLbuttonflg = buttonflg;
		int count = mCockpitButtons.Count;
		for (int i = 0; i < count; i++)
		{
			BoxCollider component = mCockpitButtons[i].GetComponent<BoxCollider>();
			if (component != null)
			{
				component.enabled = buttonflg;
			}
		}
		if (mSlideWindowPanel != null)
		{
			mSlideWindowPanel.SetEnableButtonAction(buttonflg);
		}
	}

	protected virtual void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		HudAtlas = ResourceManager.LoadImage("HUD").Image;
		Font = GameMain.LoadFont();
		mLogScreen = Util.LogScreenSize();
	}

	public void UpdateUIPanel()
	{
		int num = mGame.mDialogManager.GrayScreenDepth - 1;
		if (num > Def.DIALOG_BASE_DEPTH)
		{
			num = Def.DIALOG_BASE_DEPTH;
		}
		if (mForceDepth > 0)
		{
			num = mForceDepth;
		}
		if (PrevGrayScreenDepth != num)
		{
			mPanel.depth = num - 2;
			mPanel.sortingOrder = num - 2;
			PrevGrayScreenDepth = num;
		}
	}

	protected virtual Vector3 ShopBugLocation()
	{
		return (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown) ? ((!mMugenflg) ? new Vector3(-70f, 231f) : new Vector3(-70f, 353f)) : ((!mMugenflg) ? new Vector3(-70f, 165f) : new Vector3(-70f, 287f));
	}

	protected virtual Vector3 MugenHeartLocation()
	{
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			return new Vector3(-70f, 231f);
		}
		return new Vector3(-70f, 165f);
	}

	protected virtual Vector3 AllCrushIconLocation()
	{
		int num = 0;
		if (mShopflg)
		{
			num++;
		}
		if (mMugenflg)
		{
			num++;
		}
		Vector3 result;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			result = new Vector3(-70f, 231f + 122f * (float)num);
		}
		else
		{
			float num2 = 0f;
			if (num > 1)
			{
				num = 0;
				num2 = -112f;
			}
			result = new Vector3(-70f + num2, 165f + 122f * (float)num);
		}
		return result;
	}

	public void Init(GameStateSMMapBase a_map, bool buttonEnableFlg = true)
	{
		mButtonEnableFlg = buttonEnableFlg;
		mGSM = a_map;
		GameObject mRoot = mGSM.mRoot;
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		mCockpitRoot = Util.CreateGameObject("CockpitRoot", mRoot);
		int num = Def.DIALOG_BASE_DEPTH - 2;
		mPanel = mCockpitRoot.AddComponent<UIPanel>();
		mPanel.depth = num;
		mPanel.sortingOrder = num;
		PrevGrayScreenDepth = num;
		mAnchorT = Util.CreateAnchorWithChild("CockpitAnchorT", mCockpitRoot, UIAnchor.Side.Top, component).gameObject;
		mAnchorTL = Util.CreateAnchorWithChild("CockpitAnchorTL", mCockpitRoot, UIAnchor.Side.TopLeft, component).gameObject;
		mAnchorTR = Util.CreateAnchorWithChild("CockpitAnchorTR", mCockpitRoot, UIAnchor.Side.TopRight, component).gameObject;
		mAnchorB = Util.CreateAnchorWithChild("CockpitAnchorB", mCockpitRoot, UIAnchor.Side.Bottom, component).gameObject;
		mAnchorBR = Util.CreateAnchorWithChild("CockpitAnchorBR", mCockpitRoot, UIAnchor.Side.BottomRight, component).gameObject;
		mAnchorBL = Util.CreateAnchorWithChild("CockpitAnchorBL", mCockpitRoot, UIAnchor.Side.BottomLeft, component).gameObject;
		mAnchorC = Util.CreateAnchorWithChild("CockpitAnchorC", mCockpitRoot, UIAnchor.Side.Center, component).gameObject;
	}

	protected virtual void Start()
	{
	}

	protected virtual void Update()
	{
		if (mGSM == null)
		{
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (!mState.IsChanged())
			{
				CreateCockpitStatus(40);
				CreateCockpitItems(40);
				CreateCockpitDebug(40);
				CreateEvent(50);
				CreateSaleItem(40);
				CreateMugenHeart(40);
				CreateLinkBanner(40);
				if (!mButtonEnableFlg)
				{
					SetEnableButtonAction(false);
					mButtonEnableFlg = true;
				}
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.OPEN:
			if (mState.IsChanged() && mOpenCallback != null)
			{
				mOpenCallback(false);
			}
			break;
		case STATE.MAIN:
		{
			bool flag = (mGame.IsSeasonEventHolding() & GameMain.EventState.Season) == GameMain.EventState.Season;
			bool flag2 = mGame.mTutorialManager.IsInitialTutorialCompleted();
			bool isTipsScreenVisibled = mGame.IsTipsScreenVisibled;
			if (flag && flag2 && !isTipsScreenVisibled)
			{
				EventButtonEnble = true;
			}
			else
			{
				EventButtonEnble = false;
			}
			if (mGame.mTutorialManager.IsInitialTutorialCompleted() && mGame.IsSPDailyEventCheck())
			{
				DailyEventEnble = true;
			}
			else
			{
				DailyEventEnble = false;
			}
			if (flag2)
			{
				UpdateSaleFlg();
				UpdateMugenHeartIconFlg();
				mShowLinkBannerEnable = true;
			}
			if (mShopflg && !isTipsScreenVisibled)
			{
				ShopBagEnable = true;
			}
			else
			{
				ShopBagEnable = false;
			}
			if (mMugenflg && !isTipsScreenVisibled)
			{
				ShopMugenHeartEnable = true;
			}
			else
			{
				ShopMugenHeartEnable = false;
			}
			if (mAllCrushFlg && !isTipsScreenVisibled)
			{
				ShopAllCrushEnable = true;
			}
			else
			{
				ShopAllCrushEnable = false;
			}
			if (mGame.mTutorialManager.IsTutorialPlaying())
			{
				if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL8_2 && !mTutorialend6)
				{
					mGame.mTutorialManager.SetTutorialArrow(mAnchorB.gameObject, mButtonMailBox.gameObject, false, 100, Def.DIR.DOWN);
					mTutorialend6 = true;
				}
				if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL8_3 && !mTutorialend7)
				{
					mGame.mTutorialManager.SetTutorialArrow(mAnchorB.gameObject, mButtonStore.gameObject, false, 100, Def.DIR.DOWN);
					mTutorialend7 = true;
				}
				if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL10_3 && !mTutorialend5)
				{
					mGame.mTutorialManager.SetTutorialArrow(mAnchorB.gameObject, mButtonMenu.gameObject, false, 100, Def.DIR.DOWN);
					mGame.mTutorialManager.SetTutorialArrowOffset(0f, -40f);
					mTutorialend5 = true;
				}
				if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL11_1 && !mTutorialend)
				{
					mGame.mTutorialManager.SetTutorialArrow(mAnchorB.gameObject, mButtonMailBox.gameObject, false, 100, Def.DIR.DOWN);
					mTutorialend = true;
				}
				if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL11_4 && !mTutorialend2)
				{
					mGame.mTutorialManager.SetTutorialArrow(mAnchorT.gameObject, GemPlus.gameObject);
					mTutorialend2 = true;
				}
				if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL12_1 && !mTutorialend3)
				{
					mGame.mTutorialManager.SetTutorialArrow(mAnchorB.gameObject, mButtonMenu.gameObject, false, 100, Def.DIR.DOWN);
					mGame.mTutorialManager.SetTutorialArrowOffset(0f, -40f);
					mTutorialend3 = true;
				}
				if (mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL17_2 && !mTutorialend4)
				{
					mGame.mTutorialManager.SetTutorialArrow(mAnchorB.gameObject, mButtonMenu.gameObject, false, 100, Def.DIR.DOWN);
					mGame.mTutorialManager.SetTutorialArrowOffset(0f, -40f);
					mTutorialend4 = true;
				}
				if ((mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_0 || mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_1 || mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_2) && !mEventTutorialend)
				{
					mGame.mTutorialManager.SetTutorialPoint1Arrow(mAnchorB.gameObject, mButtonEvent.gameObject, false, 100, Def.DIR.DOWN, 0f);
					mGame.mTutorialManager.SetTutorialmArrowPoint1Offset(0f, -40f);
					mEventTutorialend = true;
				}
				if ((mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_0 || mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_1 || mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_2) && !mEventTutorialend)
				{
					mGame.mTutorialManager.SetTutorialPoint1Arrow(mAnchorB.gameObject, mButtonGameCenter.gameObject, false, 100, Def.DIR.DOWN, 0f);
					mGame.mTutorialManager.SetTutorialmArrowPoint1Offset(0f, -40f);
					mEventTutorialend = true;
				}
				if ((mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_SHOP_0 || mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_SHOP_1) && !mShopTutorialend)
				{
					mGame.mTutorialManager.SetTutorialPoint1Arrow(mAnchorB.gameObject, shopBagbutton.gameObject, false, 100, Def.DIR.RIGHT, 0f);
					mShopTutorialend = true;
				}
				if ((mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_INFINITY_0 || mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_INFINITY_1) && !mInfinityTutorialend)
				{
					mGame.mTutorialManager.SetTutorialPoint1Arrow(mAnchorB.gameObject, mugenHeartbutton.gameObject, false, 100, Def.DIR.RIGHT, 0f);
					mInfinityTutorialend = true;
				}
				if (mButtonMenu != null && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0 && !mSeasonTutorialend)
				{
					mGame.mTutorialManager.SetTutorialPoint1Arrow(mAnchorB.gameObject, mButtonMenu.gameObject, false, 100, Def.DIR.DOWN, 0f);
					mGame.mTutorialManager.SetTutorialmArrowPoint1Offset(0f, -40f);
					mSeasonTutorialend = true;
				}
				if (mButtonGameCenter != null && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL22_0 && !mAdvTutorialend)
				{
					Util.SetImageButtonGraphic(mButtonGameCenter, "button_gesen", "button_gesen", "button_gesen");
					Util.SetImageButtonMessage(mButtonGameCenter, mGSM, "OnGameCenterPushed", UIButtonMessage.Trigger.OnClick);
					mGame.mTutorialManager.SetTutorialPoint1Arrow(mAnchorB.gameObject, mButtonGameCenter.gameObject, false, 100, Def.DIR.DOWN, 0f);
					mGame.mTutorialManager.SetTutorialmArrowPoint1Offset(0f, -40f);
					mAdvTutorialend = true;
				}
			}
			break;
		}
		case STATE.CLOSE:
			if (mState.IsChanged() && mCloseCallback != null)
			{
				mCloseCallback(false);
			}
			break;
		case STATE.MOVE:
			if (!mState.IsChanged())
			{
				break;
			}
			switch (mState.GetPreStatus())
			{
			case STATE.OPEN:
				if (mCloseCallback != null)
				{
					mCloseCallback(true);
				}
				break;
			case STATE.CLOSE:
				if (mOpenCallback != null)
				{
					mOpenCallback(true);
				}
				break;
			}
			break;
		}
		mState.Update();
		UpdateUIPanel();
		UpdateDrawFlag();
		UpdateDrawOptionFlag();
		UpdateStatus();
	}

	protected void UpdateDrawFlag()
	{
		mbDraw = mGSM.OnDrawCockpit();
	}

	protected void UpdateDrawOptionFlag()
	{
		if (mbDrawOption && !mbDraw)
		{
			mbDrawOption = false;
		}
	}

	protected virtual void UpdateStatus()
	{
	}

	protected virtual void OnDestroy()
	{
		if (!(GameObject.Find("Anchor") == null))
		{
			if (mAnchorT != null)
			{
				GameMain.SafeDestroy(mAnchorT.gameObject);
			}
			if (mAnchorTL != null)
			{
				GameMain.SafeDestroy(mAnchorTL.gameObject);
			}
			if (mAnchorTR != null)
			{
				GameMain.SafeDestroy(mAnchorTR.gameObject);
			}
			if (mAnchorB != null)
			{
				GameMain.SafeDestroy(mAnchorB.gameObject);
			}
			if (mAnchorBL != null)
			{
				GameMain.SafeDestroy(mAnchorBL.gameObject);
			}
			if (mAnchorBR != null)
			{
				GameMain.SafeDestroy(mAnchorBR.gameObject);
			}
			if (mAnchorC != null)
			{
				GameMain.SafeDestroy(mAnchorC.gameObject);
			}
			if (mCockpitRoot != null)
			{
				GameMain.SafeDestroy(mCockpitRoot);
			}
		}
	}

	protected bool IsDraw()
	{
		return mbDraw;
	}

	protected bool IsDrawMapInfo()
	{
		return false;
	}

	protected bool IsDrawOption()
	{
		return mbDrawOption;
	}

	protected virtual void CreateCockpitTop(int baseDepth)
	{
		mBasedepth = baseDepth;
		float num = 123f;
		UISprite sprite = Util.CreateSprite("StatusR", mAnchorT.gameObject, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(-2f, 45f + num, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite = Util.CreateSprite("StatusL", mAnchorT.gameObject, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(2f, 45f + num, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		BoxCollider boxCollider = sprite.gameObject.AddComponent<BoxCollider>();
		boxCollider.size = new Vector3(sprite.localSize.x * 2f, sprite.localSize.y, 0f);
		boxCollider.center = new Vector3(0f, sprite.localSize.y / 2f, 0f);
		float num2 = -78f;
		UISprite uISprite = Util.CreateSprite("Heart", sprite.gameObject, HudAtlas);
		Util.SetSpriteInfo(uISprite, "life_panel", baseDepth + 2, new Vector3(-219f, num2 - num, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("HeartNum", uISprite.gameObject, Font);
		Util.SetLabelInfo(uILabel, string.Empty + mGame.mPlayer.LifeCount, baseDepth + 3, new Vector3(-59f, 0f, 0f), 21, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MAP_LIFE_COLOR;
		uILabel.effectStyle = UILabel.Effect.Shadow;
		uILabel.effectDistance = new Vector2(2f, 2f);
		uILabel.effectColor = new Color(0f, 0f, 0f, 0.3f);
		UILabel uILabel2 = Util.CreateLabel("HeartTime", uISprite.gameObject, Font);
		Util.SetLabelInfo(uILabel2, Localization.Get("HUD_LifeMax"), baseDepth + 3, new Vector3(13f, -1f, 0f), 21, 0, 0, UIWidget.Pivot.Center);
		uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel2.color = Def.DEFAULT_MAP_GEM_COLOR;
		uILabel2.SetDimensions(110, 30);
		UILabel uILabel3 = Util.CreateLabel("HeartEX", uISprite.gameObject, Font);
		Util.SetLabelInfo(uILabel3, string.Empty, baseDepth + 3, new Vector3(-33f, -11f, 0f), 21, 0, 0, UIWidget.Pivot.Center);
		uILabel3.color = Color.white;
		uILabel3.effectStyle = UILabel.Effect.Outline;
		uILabel3.effectColor = Color.black;
		StartCoroutine(UpdateHeartInfo(uILabel, uILabel2, uILabel3));
		HeartPlus = Util.CreateJellyImageButton("HeartPlus", uISprite.gameObject, HudAtlas);
		Util.SetImageButtonInfo(HeartPlus, "button_plus", "button_plus", "button_plus", baseDepth + 3, new Vector3(86f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(HeartPlus, mGSM, "OnHeartPushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(HeartPlus);
		BoxCollider component = HeartPlus.GetComponent<BoxCollider>();
		if (component != null)
		{
			component.center = new Vector3(-75f, 0f, 0f);
			component.size = new Vector3(200f, 50f, 1f);
			component.enabled = mALLbuttonflg;
		}
		StartCoroutine(UpdateHeartPlusButton(HeartPlus));
		UISprite uISprite2 = Util.CreateSprite("Gem", sprite.gameObject, HudAtlas);
		Util.SetSpriteInfo(uISprite2, "jem_panel", baseDepth + 2, new Vector3(-7f, num2 - num, 0f), Vector3.one, false);
		UILabel uILabel4 = Util.CreateLabel("SDNum", uISprite2.gameObject, Font);
		Util.SetLabelInfo(uILabel4, string.Empty + mGame.mPlayer.Dollar, baseDepth + 3, new Vector3(42f, 0f, 0f), 21, 0, 0, UIWidget.Pivot.Right);
		uILabel4.color = Def.DEFAULT_MAP_GEM_COLOR;
		StartCoroutine(UpdateGemNum(uILabel4));
		StartCoroutine(UpdateGemAppeal(uISprite2.gameObject, baseDepth));
		GemPlus = Util.CreateJellyImageButton("GemPlus", uISprite2.gameObject, HudAtlas);
		Util.SetImageButtonInfo(GemPlus, "button_plus", "button_plus", "button_plus", baseDepth + 3, new Vector3(74f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(GemPlus, mGSM, "OnSDPushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(GemPlus);
		BoxCollider component2 = GemPlus.GetComponent<BoxCollider>();
		if (component2 != null)
		{
			component2.center = new Vector3(-75f, 0f, 0f);
			component2.size = new Vector3(200f, 50f, 1f);
			component2.enabled = mALLbuttonflg;
		}
		StartCoroutine(UpdateGemPlusButton(GemPlus));
		long num3 = mGame.mPlayer.GetTrophy();
		UISprite uISprite3 = Util.CreateSprite("nice", sprite.gameObject, HudAtlas);
		Util.SetSpriteInfo(uISprite3, "trophy_panel", baseDepth + 2, new Vector3(194f, num2 - num, 0f), Vector3.one, false);
		UILabel uILabel5 = Util.CreateLabel("nicecount", uISprite3.gameObject, Font);
		Util.SetLabelInfo(uILabel5, string.Empty + num3, baseDepth + 3, new Vector3(65f, 0f, 0f), 21, 0, 0, UIWidget.Pivot.Right);
		uILabel5.color = Def.DEFAULT_MAP_GEM_COLOR;
		StartCoroutine(UpdateNicelabel(uILabel5, baseDepth));
		if (Def.SeriesMapRouteData.ContainsKey(mGame.mPlayer.Data.CurrentSeries))
		{
			mSeriesTitleSprite = Util.CreateSprite("SeriesTitleSprite", mAnchorT.gameObject, HudAtlas);
			Util.SetSpriteInfo(mSeriesTitleSprite, "adv_menu_title_side", baseDepth + 5, new Vector3(0f, num2 - 30f, 0f), Vector3.one, false);
			mSeriesTitleSprite.SetDimensions(400, 70);
			mSeriesTitleSprite.type = UIBasicSprite.Type.Sliced;
			mSeriesTitleLabel = Util.CreateLabel("SeriesTitleLabel", mSeriesTitleSprite.gameObject, Font);
			Util.SetLabelInfo(mSeriesTitleLabel, string.Empty + Localization.Get(Def.SeriesLanguage[mGame.mPlayer.Data.CurrentSeries]), baseDepth + 6, new Vector3(0f, -10f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			mSeriesTitleLabel.color = Def.DEFAULT_MAP_GEM_COLOR;
			mSeriesTitleLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			mSeriesTitleLabel.SetDimensions(300, 26);
			StartCoroutine(UpdateSeriesCampaign(mSeriesTitleSprite, baseDepth));
		}
	}

	protected virtual void CreateCockpitStatus(int baseDepth)
	{
		CreateCockpitTop(baseDepth);
	}

	protected virtual void CreateMugenHeart(int baseDepth)
	{
		GameObject parent = mAnchorBR.gameObject;
		float x = MugenHeartLocation().x;
		float y = MugenHeartLocation().y;
		UpdateMugenHeartIconFlg();
		mugenHeartbutton = Util.CreateJellyImageButton("mugenheartbutton", parent, HudAtlas);
		Util.SetImageButtonInfo(mugenHeartbutton, "button_heartinfinite", "button_heartinfinite", "button_heartinfinite", baseDepth + 2, new Vector3(x, y, 0f), Vector3.one);
		mCockpitButtons.Add(mugenHeartbutton);
		Util.AddImageButtonMessage(mugenHeartbutton, mGSM, "OnMugenHeartButtonPushed", UIButtonMessage.Trigger.OnClick);
		NGUITools.SetActive(mugenHeartbutton.gameObject, false);
		StartCoroutine(UpdateShopMugenHeartbutton(mugenHeartbutton));
		string imageName = "LC_sale_mini00";
		if (mMugenHeartItemID != 0 && mGame.mShopItemData[mMugenHeartItemID].SaleNum == 0)
		{
			imageName = "LC_sale_mini01";
		}
		mugenHeartSalesprite = Util.CreateSprite("mugenHeartSprite", mugenHeartbutton.gameObject, HudAtlas);
		Util.SetSpriteInfo(mugenHeartSalesprite, imageName, baseDepth + 3, new Vector3(0f, -44f, 0f), Vector2.one, false);
		StartCoroutine(UpdateSaleBound(mugenHeartSalesprite, Vector3.zero, true));
	}

	public virtual void UpdateMugenHeartIconFlg()
	{
		mMugenflg = false;
		if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			return;
		}
		DateTime dateTime = DateTimeUtil.Now();
		for (int i = 0; i < mGame.SegmentProfile.ShopMugenHeartItemList.Count; i++)
		{
			int shopItemID = mGame.SegmentProfile.ShopMugenHeartItemList[i].ShopItemID;
			if (mGame.mShopItemData.ContainsKey(shopItemID))
			{
				ShopItemData shopItemData = mGame.mShopItemData[shopItemID];
				int num = 0;
				int num2 = 0;
				int num3 = 0;
				double num4 = 0.0;
				double num5 = 0.0;
				num4 = mGame.SegmentProfile.ShopMugenHeartItemList[i].StartTime;
				num5 = mGame.SegmentProfile.ShopMugenHeartItemList[i].EndTime;
				num3 = mGame.SegmentProfile.ShopMugenHeartItemList[i].ShopItemID;
				num = mGame.SegmentProfile.ShopMugenHeartItemList[i].ShopItemLimit;
				num2 = mGame.SegmentProfile.ShopMugenHeartItemList[i].ShopCount;
				bool flag = mGame.mPlayer.IsEnablePurchaseItem(num3, num, num2);
				DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(num4, true);
				DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(num5, true);
				if ((num4 == num5 || (dateTime2 < dateTime && dateTime < dateTime3)) && flag)
				{
					mMugenHeartItemID = shopItemData.Index;
					mMugenHeartShopCount = num2;
					mMugenflg = true;
					break;
				}
			}
		}
	}

	protected virtual void CreateSaleItem(int baseDepth)
	{
		GameObject parent = mAnchorBR.gameObject;
		bool flag = false;
		UpdateSaleFlg();
		shopBagbutton = Util.CreateJellyImageButton("shopbagbutton", parent, HudAtlas);
		Util.SetImageButtonInfo(shopBagbutton, "button_itemset3", "button_itemset3", "button_itemset3", baseDepth + 2, ShopBugLocation(), Vector3.one);
		mCockpitButtons.Add(shopBagbutton);
		Util.AddImageButtonMessage(shopBagbutton, mGSM, "OnStoreBagPushed", UIButtonMessage.Trigger.OnClick);
		NGUITools.SetActive(shopBagbutton.gameObject, false);
		UISprite component = shopBagbutton.GetComponent<UISprite>();
		if ((bool)component)
		{
			int num = 100;
			int num2 = 100;
			if (component.width > component.height)
			{
				num = 100;
				num2 = (int)((float)num / (float)component.width * (float)component.height);
			}
			else if (component.height > component.width)
			{
				num2 = 100;
				num = (int)((float)num2 / (float)component.height * (float)component.width);
			}
			component.SetDimensions(num, num2);
		}
		StartCoroutine(UpdateShopBagbutton(shopBagbutton, baseDepth + 4));
		shopBagSalesprite = Util.CreateSprite("shopBugSale", shopBagbutton.gameObject, HudAtlas);
		Util.SetSpriteInfo(shopBagSalesprite, "LC_sale_mini00", baseDepth + 3, new Vector3(0f, -49f, 0f), Vector2.one, false);
		StartCoroutine(UpdateSaleBound(position_offset: new Vector3(-10f, -5f), SaleNow: shopBagSalesprite));
	}

	public virtual void UpdateSaleFlg()
	{
		mShopflg = false;
		mSaleItemCount = 0;
		if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			return;
		}
		int num = -1;
		DateTime dateTime = DateTimeUtil.Now();
		if (mGame.SegmentProfile.WebSale != null && mGame.SegmentProfile.WebSale.DetailList != null)
		{
			foreach (int detail in mGame.SegmentProfile.WebSale.DetailList)
			{
				for (int i = 0; i < mGame.SegmentProfile.ShopSetItemList.Count; i++)
				{
					int shopItemID = mGame.SegmentProfile.ShopSetItemList[i].ShopItemID;
					if (!mGame.mShopItemData.ContainsKey(shopItemID))
					{
						continue;
					}
					ShopItemData shopItemData = mGame.mShopItemData[shopItemID];
					if (shopItemID != detail)
					{
						continue;
					}
					double num2 = mGame.SegmentProfile.ShopSetItemList[i].StartTime;
					double num3 = mGame.SegmentProfile.ShopSetItemList[i].EndTime;
					DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(num2, true);
					DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(num3, true);
					if (num2 == num3 || (!(dateTime < dateTime2) && !(dateTime3 < dateTime)))
					{
						mSaleItemCount++;
						if (num == -1 || mGame.SegmentProfile.ShopSetItemList[i].SaleIconPriority > num)
						{
							num = mGame.SegmentProfile.ShopSetItemList[i].SaleIconPriority;
							mSaleItemID = shopItemData.Index;
							mShopflg = true;
							break;
						}
					}
				}
			}
		}
		for (int j = 0; j < mGame.SegmentProfile.ShopSetItemList.Count; j++)
		{
			int shopItemID2 = mGame.SegmentProfile.ShopSetItemList[j].ShopItemID;
			if (!mGame.mShopItemData.ContainsKey(shopItemID2))
			{
				continue;
			}
			ShopItemData shopItemData2 = mGame.mShopItemData[shopItemID2];
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			double num7 = 0.0;
			double num8 = 0.0;
			bool flag = false;
			int num9 = 0;
			num7 = mGame.SegmentProfile.ShopSetItemList[j].StartTime;
			num8 = mGame.SegmentProfile.ShopSetItemList[j].EndTime;
			num6 = mGame.SegmentProfile.ShopSetItemList[j].ShopItemID;
			num4 = mGame.SegmentProfile.ShopSetItemList[j].ShopItemLimit;
			num5 = mGame.SegmentProfile.ShopSetItemList[j].ShopCount;
			flag = shopItemData2.IsSale;
			num9 = mGame.SegmentProfile.ShopSetItemList[j].SaleIconPriority;
			bool flag2 = mGame.mPlayer.IsEnablePurchaseItem(num6, num4, num5);
			DateTime dateTime4 = DateTimeUtil.UnixTimeStampToDateTime(num7, true);
			DateTime dateTime5 = DateTimeUtil.UnixTimeStampToDateTime(num8, true);
			if ((num7 == num8 || (dateTime4 < dateTime && dateTime < dateTime5)) && flag2 && flag)
			{
				mSaleItemCount++;
				if (num == -1 || num9 > num)
				{
					mSaleItemID = shopItemData2.Index;
					mShopflg = true;
					num = num9;
				}
			}
		}
		for (int k = 0; k < mGame.SegmentProfile.ShopAllCrushItemList.Count; k++)
		{
			int shopItemID3 = mGame.SegmentProfile.ShopAllCrushItemList[k].ShopItemID;
			if (!mGame.mShopItemData.ContainsKey(shopItemID3))
			{
				continue;
			}
			ShopItemData shopItemData3 = mGame.mShopItemData[shopItemID3];
			int num10 = 0;
			int num11 = 0;
			int num12 = 0;
			double num13 = 0.0;
			double num14 = 0.0;
			int num15 = 0;
			num13 = mGame.SegmentProfile.ShopAllCrushItemList[k].StartTime;
			num14 = mGame.SegmentProfile.ShopAllCrushItemList[k].EndTime;
			num12 = mGame.SegmentProfile.ShopAllCrushItemList[k].ShopItemID;
			num10 = mGame.SegmentProfile.ShopAllCrushItemList[k].ShopItemLimit;
			num11 = mGame.SegmentProfile.ShopAllCrushItemList[k].ShopCount;
			num15 = mGame.SegmentProfile.ShopAllCrushItemList[k].SaleIconPriority;
			bool flag3 = mGame.mPlayer.IsEnablePurchaseItem(num12, num10, num11);
			DateTime dateTime6 = DateTimeUtil.UnixTimeStampToDateTime(num13, true);
			DateTime dateTime7 = DateTimeUtil.UnixTimeStampToDateTime(num14, true);
			if ((num13 == num14 || (dateTime6 < dateTime && dateTime < dateTime7)) && flag3)
			{
				mSaleItemCount++;
				if (num == -1 || num15 > num)
				{
					mSaleItemID = shopItemData3.Index;
					mShopflg = true;
					num = num15;
					mAllCrushItemID = shopItemData3.Index;
					mAllCrushFlg = true;
				}
			}
		}
	}

	protected void CreateLinkBanner(int baseDepth)
	{
		StartCoroutine(UpdateLinkBanner(baseDepth));
	}

	private void CreateEvent(int baseDepth)
	{
		GameObject gameObject = mAnchorTR.gameObject;
		GameObject anchor = mAnchorTL.gameObject;
		GameObject parent = mAnchorTR.gameObject;
		float x = -100f;
		float y = -200f;
		mEventButtonEffect = Util.CreateUISsSprite("Effect", mButtonEvent.gameObject, "EFFECT_BUTTON_SHINE", new Vector3(0f, 0f, 0f), new Vector3(0.8f, 0.8f, 1f), baseDepth + 3);
		mEventButtonEffect.gameObject.SetActive(false);
		StartCoroutine(UpdateEventButton(mButtonEvent));
		mEventLastDay = Util.CreateSprite("LastDay", mButtonEvent.gameObject, HudAtlas);
		Util.SetSpriteInfo(mEventLastDay, "LC_sale02", baseDepth + 4, new Vector3(0f, -60f, 0f), Vector2.one, false);
		NGUITools.SetActive(mEventLastDay.gameObject, false);
		StartCoroutine(UpdateEventLastDay(mEventLastDay));
		int num = 0;
		string text = "LC_button_dailyevent00";
		if (mGame.mEventProfile.SDailyEvent != null)
		{
			for (int i = 0; i < mGame.mSpecialDailyEventData.Count; i++)
			{
				if (mGame.EventProfile.SDailyEvent.SpecialEventID.CompareTo(mGame.mSpecialDailyEventData[i].ID) == 0)
				{
					num = mGame.mSpecialDailyEventData[i].ImageID;
					break;
				}
			}
			text = string.Format("LC_button_dailyevent{0:00}", num);
		}
		dailybutton = Util.CreateJellyImageButton("dailyevent", parent, HudAtlas);
		Util.SetImageButtonInfo(dailybutton, text, text, text, baseDepth + 2, new Vector3(x, y, 0f), Vector3.one);
		mCockpitButtons.Add(dailybutton);
		Util.AddImageButtonMessage(dailybutton, mGSM, "OnDailyPushed", UIButtonMessage.Trigger.OnClick);
		NGUITools.SetActive(dailybutton.gameObject, false);
		StartCoroutine(UpdateSPDaily(dailybutton));
		StartCoroutine(UpdateDailyChallenge(anchor, baseDepth));
	}

	private void CreateRemainTime(int baseDepth)
	{
		GameObject parent = mAnchorTL.gameObject;
		float x = 80f;
		float y = -160f;
		RemainTimeButton = Util.CreateJellyImageButton("RemainTimeButton", parent, HudAtlas);
		Util.SetImageButtonInfo(RemainTimeButton, "clock", "clock", "clock", baseDepth + 2, new Vector3(x, y, 0f), Vector3.one);
		mCockpitButtons.Add(RemainTimeButton);
		Util.AddImageButtonMessage(RemainTimeButton, mGSM, "OnRemainTimePushed", UIButtonMessage.Trigger.OnClick);
		StartCoroutine(UpdateRemainTimeButton(RemainTimeButton));
	}

	protected virtual void CreateCockpitItems(int baseDepth)
	{
		GameObject parent = mAnchorB.gameObject;
		float num = 0f;
		float num2 = 123f;
		mDownBoardR = Util.CreateSprite("ItemsR", parent, HudAtlas);
		Util.SetSpriteInfo(mDownBoardR, "menubar_frame", baseDepth, new Vector3(-2f, num - num2, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mDownBoardL = Util.CreateSprite("ItemsL", parent, HudAtlas);
		Util.SetSpriteInfo(mDownBoardL, "menubar_frame", baseDepth, new Vector3(2f, num - num2, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		BoxCollider boxCollider = mDownBoardL.gameObject.AddComponent<BoxCollider>();
		boxCollider.size = new Vector3(mDownBoardL.localSize.x * 2f, mDownBoardL.localSize.y, 0f);
		boxCollider.center = new Vector3(0f, mDownBoardL.localSize.y / 2f, 0f);
		float y = 58f;
		mButtonMenu = Util.CreateJellyImageButton("Menu", parent, HudAtlas);
		Util.SetImageButtonInfo(mButtonMenu, "button_menuopen", "button_menuopen", "button_menuopen", baseDepth + 2, new Vector3(-240f, 82f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonMenu, mGSM, "OnMenuOption", UIButtonMessage.Trigger.OnClick);
		mMenuButtonAttention = Util.CreateGameObject("menuAttention", mButtonMenu.gameObject).AddComponent<Attention>();
		mMenuButtonAttention.Init(1f, baseDepth + 3, new Vector3(40f, -50f, 0f), HudAtlas, "icon_exclamation");
		mMenuButtonAttention.SetAttentionEnable(false);
		mCockpitButtons.Add(mButtonMenu);
		mButtonMailBox = Util.CreateJellyImageButton("Mail", parent, HudAtlas);
		Util.SetImageButtonInfo(mButtonMailBox, "button_mailbox", "button_mailbox", "button_mailbox", baseDepth + 2, new Vector3(-108f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonMailBox, mGSM, "OnGiftPushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mButtonMailBox);
		mMailAttention = Util.CreateGameObject("CollectionAttention", mButtonMailBox.gameObject).AddComponent<Attention>();
		mMailAttention.Init(1f, baseDepth + 3, new Vector3(30f, -35f, 0f), HudAtlas, "icon_exclamation");
		mMailAttention.SetAttentionEnable(false);
		mButtonStore = Util.CreateJellyImageButton("Menu2", parent, HudAtlas);
		Util.SetImageButtonInfo(mButtonStore, "button_store", "button_store", "button_store", baseDepth + 2, new Vector3(0f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonStore, mGSM, "OnStorePushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mButtonStore);
		if (mGame.EnableAdvEnter() && mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL22_0))
		{
			mButtonGameCenter = Util.CreateJellyImageButton("GameCenter", parent, HudAtlas);
			Util.SetImageButtonInfo(mButtonGameCenter, "button_gesen", "button_gesen", "button_gesen", baseDepth + 2, new Vector3(108f, y, 0f), Vector3.one);
		}
		else
		{
			mButtonGameCenter = Util.CreateJellyImageButton("GameCenterOFF", parent, HudAtlas);
			Util.SetImageButtonInfo(mButtonGameCenter, "button_comingsoon", "button_comingsoon", "button_comingsoon", baseDepth + 2, new Vector3(108f, y, 0f), Vector3.one);
		}
		BoxCollider component = mButtonGameCenter.gameObject.GetComponent<BoxCollider>();
		if (component != null)
		{
			component.enabled = false;
		}
		mButtonEvent = Util.CreateJellyImageButton("Event", parent, HudAtlas);
		Util.SetImageButtonInfo(mButtonEvent, "LC_button_event", "LC_button_event", "LC_button_event", baseDepth + 2, new Vector3(240f, 82f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButtonEvent, mGSM, "OnEventPushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mButtonEvent);
		mButtonEvent.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
	}

	protected void CreateCockpitDebug(int baseDepth, int offset = 0)
	{
		if (GameMain.USE_DEBUG_DIALOG && !GameMain.HIDDEN_DEBUG_BUTTON)
		{
			GameObject parent = Util.CreateGameObject("Debug", mAnchorTL.gameObject);
			mButtonDebug = Util.CreateJellyImageButton("DebugButton", parent, HudAtlas);
			Util.SetImageButtonInfo(mButtonDebug, "button_debug", "button_debug", "button_debug", baseDepth, new Vector3(60f, -320f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButtonDebug, mGSM, "OnDebugPushed", UIButtonMessage.Trigger.OnClick);
			mCockpitButtons.Add(mButtonDebug);
			Color color = Color.white;
			if (ResourceManager.mABLoadError)
			{
				color = ResourceManager.mDebugButtonColor;
			}
			mButtonDebug.disabledColor = color;
			mButtonDebug.defaultColor = color;
			mButtonDebug.hover = color;
			mButtonDebug.pressed = color;
			mButtonDebug.UpdateColor(true);
		}
	}

	public void OnOptionPushed()
	{
		if (mGSM.IsEnableButton() && mGSM.OnDrawCockpit())
		{
			mbDrawOption = !mbDrawOption;
		}
	}

	public void ForceOpenOptionSlide()
	{
		mbDrawOption = true;
	}

	protected virtual IEnumerator UpdateCockpitPos(GameObject Obj, Vector3 Target_Offset, float Interval, CheckDraw Method, bool check_state, bool is_shake = true)
	{
		MOVE_STATUS status = MOVE_STATUS.OPEN;
		float timer = Interval;
		Vector3 Source = Obj.transform.localPosition;
		Vector3 Target = Source + Target_Offset;
		JellyImageButton[] jib_list = Obj.GetComponentsInChildren<JellyImageButton>();
		int ID = -1;
		if (check_state)
		{
			ID = AddMoveStatus();
		}
		while (!(Obj == null))
		{
			if (Method())
			{
				timer += Time.deltaTime;
				if (timer > Interval)
				{
					timer = Interval;
					if (status != MOVE_STATUS.OPEN)
					{
						if (jib_list != null && is_shake)
						{
							JellyImageButton[] array = jib_list;
							foreach (JellyImageButton jib in array)
							{
								StartCoroutine(ShakeDelay(jib, UnityEngine.Random.Range(0f, 0.07f)));
							}
						}
						float rate3 = Mathf.Cos((float)Math.PI / 2f * timer / Interval);
						Obj.transform.localPosition = rate3 * Source + (1f - rate3) * Target;
						status = MOVE_STATUS.OPEN;
					}
				}
				else
				{
					status = MOVE_STATUS.OPENING;
				}
			}
			else
			{
				timer -= Time.deltaTime;
				if (timer < 0f)
				{
					timer = 0f;
					if (status != MOVE_STATUS.CLOSE)
					{
						float rate2 = Mathf.Cos((float)Math.PI / 2f * timer / Interval);
						Obj.transform.localPosition = rate2 * Source + (1f - rate2) * Target;
						status = MOVE_STATUS.CLOSE;
					}
				}
				else
				{
					status = MOVE_STATUS.CLOSING;
				}
			}
			if (status == MOVE_STATUS.OPENING || status == MOVE_STATUS.CLOSING)
			{
				float rate = Mathf.Cos((float)Math.PI / 2f * timer / Interval);
				Obj.transform.localPosition = rate * Source + (1f - rate) * Target;
			}
			if (ID >= 0)
			{
				mMoveStatus[ID] = status;
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateHeartInfo(UILabel HeartNum, UILabel HeartTime, UILabel HeartEXNum)
	{
		int heart_num = 0;
		int old_heart_num = -1;
		long old_min = -1L;
		long old_sec = -1L;
		bool old_cheat_flag = false;
		bool settingReturn = false;
		if (HeartEXNum != null)
		{
			NGUITools.SetActive(HeartEXNum.gameObject, false);
		}
		while (!(HeartNum == null) || !(HeartTime == null))
		{
			if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.TIME_OVER_PUZZLE)
			{
				settingReturn = true;
				NGUITools.SetActive(HeartEXNum.gameObject, false);
				HeartNum.fontSize = 30;
				Util.SetLabelText(HeartNum, "∞");
				HeartTime.color = Color.red;
				HeartTime.fontSize = 25;
				long totalSec2 = mGame.GetMugenHeartEndSec();
				if (0 < totalSec2)
				{
					long min3 = totalSec2 / 60;
					Util.SetLabelText(HeartTime, string.Format(arg1: totalSec2 % 60, format: "{0:0}:{1:00}", arg0: min3));
				}
				else
				{
					Util.SetLabelText(HeartTime, "0:00");
				}
				yield return null;
			}
			else if (settingReturn)
			{
				HeartNum.fontSize = 21;
				HeartTime.color = Def.DEFAULT_MAP_GEM_COLOR;
				HeartTime.fontSize = 21;
				Util.SetLabelText(HeartNum, string.Format("{0}", mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1)));
				settingReturn = false;
			}
			else
			{
				if (HeartNum != null)
				{
					if (heart_num != mGame.mPlayer.LifeCount)
					{
						heart_num = mGame.mPlayer.LifeCount;
						if (heart_num != old_heart_num)
						{
							old_heart_num = heart_num;
							if (heart_num >= mGame.mPlayer.Data.MaxNormalLifeCount)
							{
								Util.SetLabelText(HeartNum, string.Format("{0}", mGame.mPlayer.Data.MaxNormalLifeCount));
							}
							else
							{
								Util.SetLabelText(HeartNum, string.Format("{0}", heart_num));
							}
						}
					}
					bool now_cheat_flag = mGame.mPlayer.HasTimeCheatPenalty();
					if (old_cheat_flag != now_cheat_flag)
					{
						old_cheat_flag = now_cheat_flag;
						if (old_cheat_flag)
						{
							HeartNum.color = new Color(1f, 1f, 0f);
						}
						else
						{
							HeartNum.color = new Color(1f, 1f, 1f);
						}
					}
				}
				if (HeartTime != null && HeartTime != null)
				{
					bool TimeCheat = mGame.mPlayer.HasTimeCheatPenalty();
					long totalSec = mGame.GetLifeRecoverSec();
					if (totalSec < 6000)
					{
						if (mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) == mGame.mPlayer.Data.MaxNormalLifeCount)
						{
							Util.SetLabelText(HeartTime, Localization.Get("HUD_LifeMax"));
							int heartEX2 = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 2);
							if (heartEX2 != 0)
							{
								NGUITools.SetActive(HeartEXNum.gameObject, true);
								string strNum2 = Localization.Get("HeartPlus") + heartEX2;
								Util.SetLabelText(HeartEXNum, strNum2);
							}
							else
							{
								NGUITools.SetActive(HeartEXNum.gameObject, false);
							}
						}
						else
						{
							NGUITools.SetActive(HeartEXNum.gameObject, false);
							long min2 = totalSec / 60;
							long sec2 = totalSec % 60;
							if (min2 != old_min || sec2 != old_sec)
							{
								old_min = min2;
								old_sec = sec2;
								Util.SetLabelText(HeartTime, string.Format("{0:0}:{1:00}", min2, sec2));
							}
						}
					}
					else
					{
						long min = 99L;
						long sec = 59L;
						Util.SetLabelText(HeartTime, string.Format("{0:0}:{1:00}", min, sec));
						int heartEX = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 2);
						if (heartEX != 0)
						{
							NGUITools.SetActive(HeartEXNum.gameObject, true);
							string strNum = Localization.Get("HeartPlus") + heartEX;
							Util.SetLabelText(HeartEXNum, strNum);
						}
						else
						{
							NGUITools.SetActive(HeartEXNum.gameObject, false);
						}
					}
				}
				yield return null;
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateGemNum(UILabel GemNum)
	{
		int gem_num = 0;
		while (!(GemNum == null))
		{
			if (gem_num != mGame.mPlayer.Dollar)
			{
				gem_num = mGame.mPlayer.Dollar;
				Util.SetLabelText(GemNum, string.Format("{0}", gem_num));
			}
			yield return null;
		}
	}

	private IEnumerator UpdateGemAppeal(GameObject baseGo, int depth)
	{
		UISsSprite effect = null;
		UISsSprite appeal = null;
		while (!(baseGo == null))
		{
			if (mGame.IsCampaignAvailable())
			{
				if (effect == null && appeal == null)
				{
					effect = Util.CreateUISsSprite("GemEffect", baseGo, "EFFECT_CAMPAIGN_APPEAL", new Vector3(-62f, 0f, 0f), new Vector3(1f, 1f, 1f), depth + 3);
					effect.gameObject.SetActive(true);
					appeal = Util.CreateUISsSprite("GemAppeal", baseGo, "EFFECT_GEM_CP", new Vector3(-10f, -15f, 0f), new Vector3(1f, 1f, 1f), depth + 3);
					appeal.gameObject.SetActive(true);
				}
			}
			else if (effect != null && appeal != null)
			{
				UnityEngine.Object.Destroy(effect.gameObject);
				effect = null;
				UnityEngine.Object.Destroy(appeal.gameObject);
				appeal = null;
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateSeriesCampaign(UISprite a_sprite, int baseDepth)
	{
		float checkedTime = 0f;
		while (!(a_sprite == null))
		{
			if (checkedTime + 5f <= Time.realtimeSinceStartup)
			{
				checkedTime = Time.realtimeSinceStartup;
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0))
				{
					continue;
				}
				bool hasHeldSame = false;
				bool hasHeldOther = false;
				int moves2 = mGame.GetSeriesCampaignMoves(mGame.mPlayer.Data.CurrentSeries);
				if (moves2 > 0)
				{
					hasHeldSame = true;
					if (mSeriesCampaignBand == null)
					{
						mSeriesCampaignBand = Util.CreateSprite("SeriesCampaignSprite", a_sprite.gameObject, HudAtlas);
						Util.SetSpriteInfo(mSeriesCampaignBand, "LC_campaign_hukidashi00", baseDepth + 6, new Vector3(0f, -33f, 0f), Vector3.one, false);
					}
				}
				else
				{
					for (int i = 0; i < Def.SeriesCampaignSeries.Length; i++)
					{
						Def.SERIES series = Def.SeriesCampaignSeries[i];
						if (mGame.mPlayer.Data.CurrentSeries == series)
						{
							continue;
						}
						moves2 = mGame.GetSeriesCampaignMoves(series);
						if (moves2 > 0)
						{
							hasHeldOther = true;
							if (mSCButton == null)
							{
								mSCButton = SeriesCampaignButton.Make(mAnchorTR.gameObject, series, new Vector3(-70f, -160f, 0f), baseDepth);
								mSCButton.SetCallback(mGSM.OnSeriesCampaignPushed);
							}
							break;
						}
					}
				}
				if (!hasHeldSame && mSeriesCampaignBand != null)
				{
					UnityEngine.Object.Destroy(mSeriesCampaignBand.gameObject);
					mSeriesCampaignBand = null;
				}
				if (!hasHeldOther && mSCButton != null)
				{
					StartCoroutine(DeleteSeriesCampaignButton());
				}
			}
			yield return null;
		}
	}

	protected virtual IEnumerator DeleteSeriesCampaignButton()
	{
		if (mSCButton == null)
		{
			yield break;
		}
		mSCButton.SetEnable(false);
		float scale = 1f;
		float time = 0f;
		Transform t = mSCButton.transform;
		Vector3 tempPos = t.localPosition;
		while (scale > 0f)
		{
			time += Time.deltaTime * 540f;
			scale = 1f - Mathf.Sin(time * ((float)Math.PI / 180f));
			float y = Mathf.Sin(time * ((float)Math.PI / 180f)) * 30f;
			if (time > 90f)
			{
				scale = 0f;
				NGUITools.SetActive(mSCButton.gameObject, false);
			}
			t.localPosition = new Vector3(tempPos.x, tempPos.y + y, tempPos.z);
			t.localScale = new Vector3(scale, t.localScale.y, t.localScale.z);
			yield return null;
		}
		UnityEngine.Object.Destroy(mSCButton.gameObject);
		mSCButton = null;
	}

	protected virtual IEnumerator UpdateNicelabel(UILabel Nicelabel, int baseDepth)
	{
		long TotalTro = -1L;
		float stop_time2 = 0f;
		int addFontSize = 1;
		bool fontSizeFlg = false;
		float angle = 0f;
		float posX = -30f;
		float location = 30f;
		UISprite rightEffect = null;
		int num = 0;
		while (!(Nicelabel == null))
		{
			if (mGame.mTrophyProductionFlg == GameMain.TROPHY_PRODUCTION.STOCK)
			{
				if (mGame.mTrophyNumProduction != TotalTro)
				{
					TotalTro = mGame.mTrophyNumProduction;
					Util.SetLabelText(Nicelabel, string.Format("{0}", TotalTro));
					mGame.mTrophyNumSetLayout = 0;
				}
			}
			else if (mGame.mTrophyProductionFlg == GameMain.TROPHY_PRODUCTION.OPEN0)
			{
				mGame.mTrophyNumSetLayout++;
				TotalTro = mGame.mTrophyNumProduction + mGame.mTrophyNumSetLayout;
				Util.SetLabelText(Nicelabel, string.Format("{0}", TotalTro));
				stop_time2 = 0f;
				UISsSprite uiSsAnime2 = null;
				uiSsAnime2 = Util.CreateGameObject("Shine", Nicelabel.gameObject).AddComponent<UISsSprite>();
				uiSsAnime2.Animation = ResourceManager.LoadSsAnimation("EFFECT_SHINE").SsAnime;
				uiSsAnime2.depth = baseDepth + 10;
				uiSsAnime2.transform.localScale = new Vector3(1f, 1f, 1f);
				uiSsAnime2.transform.localPosition = new Vector3(10f, -10f);
				uiSsAnime2.DestroyAtEnd = true;
				uiSsAnime2.PlayCount = 1;
				uiSsAnime2.Play();
				mGame.mTrophyProductionFlg = GameMain.TROPHY_PRODUCTION.OPEN1;
			}
			else if (mGame.mTrophyProductionFlg != GameMain.TROPHY_PRODUCTION.OPEN1 && TotalTro != mGame.mPlayer.GetTrophy())
			{
				mGame.mTrophyNumSetLayout = 0;
				TotalTro = mGame.mPlayer.GetTrophy();
				Util.SetLabelText(Nicelabel, string.Format("{0}", TotalTro));
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateMailAttention(UISprite MailAttention)
	{
		float counter = 0f;
		float basePosY = MailAttention.transform.localPosition.y;
		while (!(MailAttention == null))
		{
			counter += Time.deltaTime * 360f;
			if (counter > 720f)
			{
				counter -= 720f;
			}
			Vector3 pos = MailAttention.transform.localPosition;
			if (counter < 180f)
			{
				pos.y = basePosY + Mathf.Sin(counter * ((float)Math.PI / 180f)) * 30f;
			}
			else
			{
				pos.y = basePosY;
			}
			MailAttention.transform.localPosition = pos;
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateHeartPlusButton(UIButton HeartPlus)
	{
		bool plus_flag = true;
		while (!(HeartPlus == null) && !(mGSM == null))
		{
			bool by_enable = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) < mGame.mPlayer.Data.MaxNormalLifeCount;
			if (plus_flag != by_enable)
			{
				plus_flag = by_enable;
				HeartPlus.GetComponent<JellyImageButton>().SetButtonEnable(by_enable);
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateGemPlusButton(UIButton GemPlus)
	{
		bool plus_flag = true;
		while (!(GemPlus == null) && !(mGSM == null))
		{
			bool by_enable = true;
			if (plus_flag != by_enable)
			{
				plus_flag = by_enable;
				NGUITools.SetActive(GemPlus.gameObject, plus_flag);
			}
			yield return null;
		}
	}

	protected virtual IEnumerator ShakeDelay(JellyImageButton JIB, float Delay)
	{
		float Timer = 0f;
		while (true)
		{
			if (JIB == null)
			{
				yield break;
			}
			Timer += Time.deltaTime;
			if (Delay < Timer)
			{
				break;
			}
			yield return null;
		}
		JIB.Shake();
	}

	protected virtual IEnumerator UpdateShopMugenHeartbutton(UIButton Button)
	{
		Vector3 BasePos = Button.transform.localPosition;
		int bound_counter = 0;
		float velocity = 0f;
		float y_offset = 0f;
		float alpha_rate = 0f;
		float alpha = 0f;
		bool is_draw = true;
		bool is_draw_old2 = true;
		float prevResetTime2 = Time.realtimeSinceStartup;
		while (!(Button == null))
		{
			is_draw_old2 = is_draw;
			switch (mState.GetStatus())
			{
			case STATE.OPEN:
			case STATE.MAIN:
				is_draw = ShopMugenHeartEnable;
				BasePos = MugenHeartLocation();
				break;
			default:
				is_draw = false;
				break;
			}
			if (is_draw_old2 != is_draw)
			{
				if (is_draw)
				{
					bound_counter = 5;
					velocity = 200f;
					alpha_rate = 0.2f;
					alpha = 0f;
					NGUITools.SetActive(Button.gameObject, true);
					y_offset = 0f;
				}
				else
				{
					bound_counter = 0;
					velocity = 200f;
					alpha_rate = -0.15f;
					alpha = 1f;
					y_offset = 0f;
				}
			}
			if (bound_counter < 0)
			{
				y_offset = 0f;
			}
			else
			{
				velocity += -25f * Time.deltaTime * 13f;
				y_offset += velocity * Time.deltaTime * 13f;
				if (y_offset < 0f)
				{
					y_offset = 0f;
					bound_counter--;
					if (bound_counter < 0)
					{
						prevResetTime2 = Time.realtimeSinceStartup;
					}
					velocity = -0.8f * velocity;
				}
				else if (y_offset > 30f)
				{
					y_offset = 30f;
				}
			}
			Button.transform.localPosition = BasePos + new Vector3(0f, y_offset, 0f);
			alpha += alpha_rate;
			if (alpha > 1f)
			{
				alpha = 1f;
			}
			else if (alpha < 0f)
			{
				alpha = 0f;
				if (NGUITools.GetActive(Button.gameObject))
				{
					NGUITools.SetActive(Button.gameObject, false);
				}
			}
			Util.SetImageButtonColor(Button, new Color(1f, 1f, 1f, alpha));
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateShopAllCrushbutton(UIButton Button)
	{
		Vector3 BasePos = Button.transform.localPosition;
		int bound_counter = 0;
		float velocity = 0f;
		float y_offset = 0f;
		float alpha_rate = 0f;
		float alpha = 0f;
		bool is_draw = true;
		bool is_draw_old2 = true;
		float prevResetTime2 = Time.realtimeSinceStartup;
		while (!(Button == null))
		{
			is_draw_old2 = is_draw;
			switch (mState.GetStatus())
			{
			case STATE.OPEN:
			case STATE.MAIN:
				is_draw = ShopAllCrushEnable;
				BasePos = AllCrushIconLocation();
				break;
			default:
				is_draw = false;
				break;
			}
			if (is_draw_old2 != is_draw)
			{
				if (is_draw)
				{
					bound_counter = 5;
					velocity = 200f;
					alpha_rate = 0.2f;
					alpha = 0f;
					NGUITools.SetActive(Button.gameObject, true);
					y_offset = 0f;
				}
				else
				{
					bound_counter = 0;
					velocity = 200f;
					alpha_rate = -0.15f;
					alpha = 1f;
					y_offset = 0f;
				}
			}
			if (bound_counter < 0)
			{
				y_offset = 0f;
			}
			else
			{
				velocity += -25f * Time.deltaTime * 13f;
				y_offset += velocity * Time.deltaTime * 13f;
				if (y_offset < 0f)
				{
					y_offset = 0f;
					bound_counter--;
					if (bound_counter < 0)
					{
						prevResetTime2 = Time.realtimeSinceStartup;
					}
					velocity = -0.8f * velocity;
				}
				else if (y_offset > 30f)
				{
					y_offset = 30f;
				}
			}
			Button.transform.localPosition = BasePos + new Vector3(0f, y_offset, 0f);
			alpha += alpha_rate;
			if (alpha > 1f)
			{
				alpha = 1f;
			}
			else if (alpha < 0f)
			{
				alpha = 0f;
				if (NGUITools.GetActive(Button.gameObject))
				{
					NGUITools.SetActive(Button.gameObject, false);
				}
			}
			Util.SetImageButtonColor(Button, new Color(1f, 1f, 1f, alpha));
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateShopBagbutton(UIButton Button, int effect_depth)
	{
		Vector3 BasePos = Button.transform.localPosition;
		Vector3 offset = new Vector3(10f, 5f);
		int bound_counter = 0;
		float velocity = 0f;
		float y_offset = 0f;
		float alpha_rate = 0f;
		float alpha = 0f;
		bool is_draw = true;
		bool is_draw_old2 = true;
		float resetTime = 5f;
		float prevResetTime2 = Time.realtimeSinceStartup;
		UISsSprite buttonEffect = null;
		while (!(Button == null))
		{
			if (buttonEffect == null && Button != null && Button.gameObject.activeSelf)
			{
				buttonEffect = Util.CreateUISsSprite("Effect", Button.gameObject, "EFFECT_BUTTON_SHINE", new Vector3(0f, 0f, 0f), new Vector3(0.4f, 0.4f, 1f), effect_depth);
				buttonEffect.gameObject.SetActive(true);
			}
			is_draw_old2 = is_draw;
			switch (mState.GetStatus())
			{
			case STATE.OPEN:
			case STATE.MAIN:
				is_draw = ShopBagEnable;
				BasePos = ShopBugLocation();
				break;
			default:
				is_draw = false;
				break;
			}
			string saleIconName = "button_itemset3";
			if (Button.normalSprite != saleIconName)
			{
				Util.SetImageButtonGraphic(Button, saleIconName, saleIconName, saleIconName);
				Button.disabledSprite = saleIconName;
				if (saleIconName == "button_itemset3")
				{
					UISprite spr = Button.GetComponent<UISprite>();
					if ((bool)spr)
					{
						int w = 100;
						int h = 100;
						if (spr.width < spr.height)
						{
							w = 100;
							h = (int)((float)w / (float)spr.width * (float)spr.height);
						}
						else if (spr.height < spr.width)
						{
							h = 100;
							w = (int)((float)h / (float)spr.height * (float)spr.width);
						}
						spr.SetDimensions(w, h);
					}
				}
				else
				{
					offset = Vector3.zero;
				}
			}
			if (is_draw_old2 != is_draw)
			{
				if (is_draw)
				{
					bound_counter = 5;
					velocity = 125f;
					alpha_rate = 0.2f;
					alpha = 0f;
					NGUITools.SetActive(Button.gameObject, true);
					y_offset = 0f;
				}
				else
				{
					bound_counter = 0;
					velocity = 125f;
					alpha_rate = -0.15f;
					alpha = 1f;
					y_offset = 0f;
				}
			}
			if (bound_counter < 0)
			{
				y_offset = 0f;
			}
			else
			{
				velocity += -25f * Time.deltaTime * 13f;
				y_offset += velocity * Time.deltaTime * 13f;
				if (y_offset < 0f)
				{
					y_offset = 0f;
					bound_counter--;
					if (bound_counter < 0)
					{
						prevResetTime2 = Time.realtimeSinceStartup;
					}
					velocity = -0.8f * velocity;
				}
				else if (y_offset > 30f)
				{
					y_offset = 30f;
				}
			}
			Button.transform.localPosition = BasePos + new Vector3(0f, y_offset, 0f) + offset;
			alpha += alpha_rate;
			if (alpha > 1f)
			{
				alpha = 1f;
			}
			else if (alpha < 0f)
			{
				alpha = 0f;
				if (NGUITools.GetActive(Button.gameObject))
				{
					NGUITools.SetActive(Button.gameObject, false);
				}
			}
			Util.SetImageButtonColor(Button, new Color(1f, 1f, 1f, alpha));
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateSaleBound(UISprite SaleNow, Vector3 position_offset, bool mugenHeart = false)
	{
		float counter = 0f;
		Vector3 BasePos = ((!(SaleNow != null)) ? Vector3.zero : SaleNow.transform.localPosition);
		if ((bool)SaleNow)
		{
			SaleNow.transform.SetLocalPosition(BasePos + position_offset);
		}
		float delay_timer = 5f;
		while (!(SaleNow == null))
		{
			if (mugenHeart)
			{
				if (mMugenHeartItemID != 0 && mGame.mShopItemData[mMugenHeartItemID].SaleNum == 0)
				{
					if (SaleNow.spriteName == "LC_sale_mini00")
					{
						Util.SetSpriteImageName(SaleNow, "LC_sale_mini01", Vector3.one, false);
					}
				}
				else if (SaleNow.spriteName == "LC_sale_mini01")
				{
					Util.SetSpriteImageName(SaleNow, "LC_sale_mini00", Vector3.one, false);
				}
			}
			if (delay_timer <= 0f)
			{
				counter += Time.deltaTime * 540f;
				Transform trans = SaleNow.transform;
				if (counter > 360f)
				{
					counter = 0f;
					trans.SetLocalPosition(BasePos + position_offset);
					yield return new WaitForSeconds(3f);
				}
				trans.SetLocalPositionY(BasePos.y + Mathf.Abs(Mathf.Sin(counter * ((float)Math.PI / 180f))) * 25f + position_offset.y);
			}
			else
			{
				delay_timer -= Time.deltaTime;
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateSaleActiveFlg(UISprite SaleNow)
	{
		while (!(SaleNow == null))
		{
			if (mGame.SegmentProfile.WebSale.DetailList.Count > 0)
			{
				if (NGUITools.GetActive(SaleNow.gameObject))
				{
					NGUITools.SetActive(SaleNow.gameObject, false);
				}
			}
			else if (!NGUITools.GetActive(SaleNow.gameObject))
			{
				NGUITools.SetActive(SaleNow.gameObject, true);
			}
			yield return null;
		}
	}

	protected IEnumerator UpdateLinkBanner(int baseDepth)
	{
		while (true)
		{
			if (mShowLinkBannerEnable)
			{
				if (mSlideWindowPanel == null)
				{
					if (mGame.linkbanner_InfoList != null)
					{
						mSlideWindowPanel = Util.CreateGameObject("SlideWindowRoot", mAnchorTL.gameObject).AddComponent<SlideWindowPanel>();
						mSlideWindowPanel.gameObject.transform.localPosition = new Vector3(160f, -130f, 0f);
						mSlideWindowPanel.Init(baseDepth, mGame.linkbanner_InfoList[0].banner.width, mGame.linkbanner_InfoList[0].banner.height);
						mSlideWindowPanel.SetCursorSettings("btn_banner02", HudAtlas);
						mSlideWindowPanel.SetOrbSettings("btn_banner01", HudAtlas);
						mSlideWindowPanel.mLoopEnable = true;
						mSlideWindowPanel.mAutoMoveEnable = true;
						if (mGame.linkbanner_InfoList.Count >= 1)
						{
							for (int i = 0; i < mGame.linkbanner_InfoList.Count; i++)
							{
								if (mGame.linkbanner_InfoList[i].action == LinkBannerInfo.ACTION.NOP)
								{
									mSlideWindowPanel.AddSlideWindowPanel(mGame.linkbanner_InfoList[i].banner);
								}
								else
								{
									mSlideWindowPanel.AddSlideWindowPanel(mGame.linkbanner_InfoList[i].banner, i);
								}
							}
						}
						mSlideWindowPanel.setCallBack(mGSM.OnIndexButtonPushed);
					}
				}
				else if (mGame.linkbanner_InfoList == null && mSlideWindowPanel != null)
				{
					GameMain.SafeDestroy(mSlideWindowPanel.gameObject);
					mSlideWindowPanel = null;
				}
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateEventButton(UIButton Button)
	{
		if (Button == null)
		{
			yield break;
		}
		JellyImageButton jellyButton = Button.GetComponent<JellyImageButton>();
		bool isEnable = true;
		bool isEnable_old2 = true;
		while (true)
		{
			isEnable_old2 = isEnable;
			switch (mState.GetStatus())
			{
			case STATE.OPEN:
			case STATE.MAIN:
				isEnable = EventButtonEnble;
				break;
			default:
				isEnable = false;
				break;
			}
			if (isEnable_old2 != isEnable)
			{
				jellyButton.SetButtonEnable(isEnable);
				mEventButtonEffect.gameObject.SetActive(isEnable);
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateEventLastDay(UISprite LastDay)
	{
		float counter = 0f;
		bool is_draw = false;
		Vector3 BasePos = LastDay.transform.localPosition;
		float delay_timer = 5f;
		bool isEnable_old = false;
		while (!(LastDay == null))
		{
			bool is_last_day = mGame.IsSeasonEventLastDay();
			if (EventButtonEnble && is_last_day)
			{
				if (mState.GetStatus() == STATE.MAIN && !is_draw)
				{
					is_draw = is_last_day;
					NGUITools.SetActive(LastDay.gameObject, true);
				}
			}
			else if (is_draw)
			{
				is_draw = is_last_day;
				NGUITools.SetActive(LastDay.gameObject, false);
			}
			if (is_draw)
			{
				if (delay_timer <= 0f)
				{
					counter += Time.deltaTime * 540f;
					if (counter > 360f)
					{
						counter = 0f;
						LastDay.transform.localPosition = BasePos;
						yield return new WaitForSeconds(3f);
					}
					Vector3 pos = LastDay.transform.localPosition;
					pos.y = BasePos.y + Mathf.Abs(Mathf.Sin(counter * ((float)Math.PI / 180f))) * 25f;
					LastDay.transform.localPosition = pos;
				}
				else
				{
					delay_timer -= Time.deltaTime;
				}
			}
			else
			{
				delay_timer = 5f;
				counter = 0f;
				LastDay.transform.localPosition = BasePos;
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateSPDaily(UIButton SPDailyDay)
	{
		float counter = 0f;
		bool is_draw = false;
		Vector3 BasePos = SPDailyDay.transform.localPosition;
		float delay_timer = 5f;
		while (!(SPDailyDay == null))
		{
			bool is_last_day = DailyEventEnble;
			if (is_draw != is_last_day && mState.GetStatus() == STATE.MAIN)
			{
				is_draw = is_last_day;
				NGUITools.SetActive(SPDailyDay.gameObject, is_draw);
			}
			if (NGUITools.GetActive(SPDailyDay) && !is_last_day)
			{
				NGUITools.SetActive(SPDailyDay.gameObject, false);
			}
			if (is_draw)
			{
				if (delay_timer <= 0f)
				{
					counter += Time.deltaTime * 540f;
					if (counter > 360f)
					{
						counter = 0f;
						SPDailyDay.transform.localPosition = BasePos;
						yield return new WaitForSeconds(3f);
					}
					Vector3 pos = SPDailyDay.transform.localPosition;
					pos.y = BasePos.y + Mathf.Abs(Mathf.Sin(counter * ((float)Math.PI / 180f))) * 25f;
					SPDailyDay.transform.localPosition = pos;
				}
				else
				{
					delay_timer -= Time.deltaTime;
				}
			}
			else
			{
				delay_timer = 5f;
				counter = 0f;
				SPDailyDay.transform.localPosition = BasePos;
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateDailyChallenge(GameObject anchor, int depth)
	{
		while (true)
		{
			if (mGame.EventProfile.DailyChallengeEventList != null && mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				DailyChallengeEventSettings opened = null;
				DateTime now = DateTimeUtil.Now();
				DateTime start = DateTime.Now;
				DateTime end = DateTime.Now;
				foreach (DailyChallengeEventSettings setting in mGame.EventProfile.DailyChallengeEventList)
				{
					if (setting == null)
					{
						continue;
					}
					if (GlobalVariables.Instance.SelectedDailyChallengeSheetNo != -1 && GlobalVariables.Instance.SelectedDailyChallengeStageNo != -1 && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
					{
						long ls = ((mGame.mDebugDailyChallengeStartTime == -1) ? setting.StartTime : mGame.mDebugDailyChallengeStartTime);
						long le = ((mGame.mDebugDailyChallengeEndTime == -1) ? setting.EndTime : mGame.mDebugDailyChallengeEndTime);
						string ss = ((!(mGame.mDebugDailyChallengeSheetSetName != string.Empty)) ? setting.SheetName : mGame.mDebugDailyChallengeSheetSetName);
						int ii = ((mGame.mDebugDailyChallengeID == 0) ? setting.DailyChallengeID : mGame.mDebugDailyChallengeID);
						if (GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID != ii || GlobalVariables.Instance.SelectedDailyChallengeSetting.SheetName != ss || GlobalVariables.Instance.SelectedDailyChallengeSetting.StartTime != ls || GlobalVariables.Instance.SelectedDailyChallengeSetting.EndTime != le)
						{
							continue;
						}
					}
					long startTS = setting.StartTime;
					if (mGame.mDebugDailyChallengeStartTime != -1)
					{
						startTS = mGame.mDebugDailyChallengeStartTime;
					}
					long endTS = setting.EndTime;
					if (mGame.mDebugDailyChallengeEndTime != -1)
					{
						endTS = mGame.mDebugDailyChallengeEndTime;
					}
					start = DateTimeUtil.UnixTimeStampToDateTime(startTS, true);
					end = DateTimeUtil.UnixTimeStampToDateTime(endTS, true);
					if (now < start || end < now)
					{
						continue;
					}
					opened = setting.Clone();
					if (mGame.mDebugDailyChallengeStartTime != -1)
					{
						opened.StartTime = mGame.mDebugDailyChallengeStartTime;
					}
					if (mGame.mDebugDailyChallengeEndTime != -1)
					{
						opened.EndTime = mGame.mDebugDailyChallengeEndTime;
					}
					if (mGame.mDebugDailyChallengeSheetSetName != string.Empty)
					{
						opened.SheetName = mGame.mDebugDailyChallengeSheetSetName;
					}
					if (mGame.mDebugDailyChallengeID != 0)
					{
						opened.DailyChallengeID = mGame.mDebugDailyChallengeID;
					}
					break;
				}
				bool isOpenDialog = mGSM != null && mGSM is GameStateSMMap && (mGSM as GameStateSMMap).IsOpenDailyChallenge;
				if (opened != null)
				{
					if (DailyChallengeButton == null)
					{
						string str = "LC_button_dailychalleng00";
						DailyChallengeButton = Util.CreateJellyImageButton("DailyChallenge", anchor, HudAtlas);
						Util.SetImageButtonInfo(DailyChallengeButton, str, str, str, depth + 2, new Vector3(75f, -155f), Vector3.one);
						mCockpitButtons.Add(DailyChallengeButton);
						Util.AddImageButtonMessage(DailyChallengeButton, mGSM, "OnDailyChallenge_Clicked", UIButtonMessage.Trigger.OnClick);
						if (DailyChallengeEffect != null)
						{
							GameMain.SafeDestroy(DailyChallengeEffect.gameObject);
						}
						DailyChallengeEffect = Util.CreateUISsSprite("Effect", DailyChallengeButton.gameObject, "EFFECT_BUTTON_SHINE", new Vector3(0f, 0f, 0f), new Vector3(0.8f, 0.8f, 1f), depth + 3);
					}
					if (!isOpenDialog)
					{
						GlobalVariables.Instance.SelectedDailyChallengeSetting = opened;
						int openSheetNum = 0;
						if (DailyChallengeAttention == null)
						{
							string sheetSetName = GlobalVariables.Instance.SelectedDailyChallengeSetting.SheetName;
							if (mGame.mDebugDailyChallengeSheetSetName != string.Empty)
							{
								sheetSetName = mGame.mDebugDailyChallengeSheetSetName;
							}
							foreach (DailyChallengeSheet sheet in mGame.mDailyChallengeData)
							{
								if (sheet == null || sheet.SheetName != sheetSetName)
								{
									continue;
								}
								TimeSpan span = now - start;
								openSheetNum = ((span.Days >= 0) ? span.Days : 0) + 1;
								if (openSheetNum > sheet.StageCount)
								{
									openSheetNum = sheet.StageCount;
								}
								break;
							}
						}
						if (openSheetNum > mGame.mPlayer.Data.DailyChallengeOpenSheetNum)
						{
							if (DailyChallengeAttention == null)
							{
								DailyChallengeAttention = Util.CreateGameObject("Attention", DailyChallengeButton.gameObject).AddComponent<Attention>();
								DailyChallengeAttention.Init(1f, depth + 3, new Vector3(50f, 15f, 0f), HudAtlas, "icon_exclamation");
							}
							if (!DailyChallengeAttention.GetAttentionActiveSelf())
							{
								DailyChallengeAttention.SetAttentionEnable(true);
							}
						}
					}
				}
				else
				{
					if (!isOpenDialog)
					{
						if (DailyChallengeAttention != null)
						{
							GameMain.SafeDestroy(DailyChallengeAttention.gameObject);
							DailyChallengeAttention = null;
						}
						if (DailyChallengeEffect != null)
						{
							GameMain.SafeDestroy(DailyChallengeEffect.gameObject);
							DailyChallengeEffect = null;
						}
						if (DailyChallengeButton != null)
						{
							if (mCockpitButtons.Contains(DailyChallengeButton))
							{
								mCockpitButtons.Remove(DailyChallengeButton);
							}
							GameMain.SafeDestroy(DailyChallengeButton.gameObject);
							DailyChallengeButton = null;
						}
					}
					if (GlobalVariables.Instance.SelectedDailyChallengeSheetNo == -1 && GlobalVariables.Instance.SelectedDailyChallengeStageNo == -1)
					{
						GlobalVariables.Instance.SelectedDailyChallengeSetting = null;
					}
				}
			}
			else
			{
				if (DailyChallengeAttention != null)
				{
					GameMain.SafeDestroy(DailyChallengeAttention.gameObject);
					DailyChallengeAttention = null;
				}
				if (DailyChallengeEffect != null)
				{
					GameMain.SafeDestroy(DailyChallengeEffect.gameObject);
					DailyChallengeEffect = null;
				}
				if (DailyChallengeButton != null)
				{
					GameMain.SafeDestroy(DailyChallengeButton.gameObject);
					DailyChallengeButton = null;
				}
				GlobalVariables.Instance.SelectedDailyChallengeSetting = null;
			}
			yield return null;
		}
	}

	protected virtual IEnumerator UpdateRemainTimeButton(UIButton Button)
	{
		bool _flag = true;
		while (!(Button == null) && !(mGSM == null))
		{
			bool by_enable = mGame.IsRemainTimeButtonVisible();
			if (_flag != by_enable)
			{
				_flag = by_enable;
				NGUITools.SetActive(Button.gameObject, _flag);
			}
			yield return null;
		}
	}

	protected void PlaySE(string key, Res.SE_CHANNEL cannel = Res.SE_CHANNEL.AUTO, float vol = 1f)
	{
		mGame.PlaySe(key, (int)cannel, vol);
	}

	public IEnumerator UpdateGameCenterButton(UIButton button, int depth)
	{
		if (button == null)
		{
			yield break;
		}
		bool evenNowAnimeFinished = false;
		bool evenNow2AnimeFinished = false;
		bool gasyaIconAnimeFinished = false;
		UISsSprite buttonEffect = null;
		while (true)
		{
			if (mGame.EnableAdvEnter() && mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL22_0))
			{
				if (mGame.IsPlayEventDungeon() && mGame.GetNowAdvEventIdList() != null)
				{
					bool canGlaEvent = false;
					if (mGame.GLAEventOpenNow())
					{
						canGlaEvent = true;
						if (buttonEffect == null)
						{
							buttonEffect = Util.CreateUISsSprite("Effect", button.gameObject, "EFFECT_BUTTON_SHINE", new Vector3(0f, 0f, 0f), new Vector3(0.6f, 0.6f, 1f), depth);
							buttonEffect.gameObject.SetActive(true);
						}
						UISsSprite ss6 = Util.CreateUISsSprite("EventNow2Effect", button.gameObject, "EFFECT_EVENT2_NOW", new Vector3(0f, 0f, 0f), new Vector3(1f, 1f, 1f), depth);
						ss6.AnimationFinished = delegate
						{
							evenNow2AnimeFinished = true;
						};
						ss6.PlayCount = 1;
						ss6.Play();
						yield return null;
						while (!evenNow2AnimeFinished)
						{
							yield return null;
						}
						evenNow2AnimeFinished = false;
						if (ss6 != null)
						{
							UnityEngine.Object.Destroy(ss6.gameObject);
							ss6 = null;
						}
					}
					if (mGame.EventOpenNow_NotGLA())
					{
						if (buttonEffect == null)
						{
							buttonEffect = Util.CreateUISsSprite("Effect", button.gameObject, "EFFECT_BUTTON_SHINE", new Vector3(0f, 0f, 0f), new Vector3(0.6f, 0.6f, 1f), depth);
							buttonEffect.gameObject.SetActive(true);
						}
						UISsSprite ss4 = Util.CreateUISsSprite("EventNowEffect", button.gameObject, "EFFECT_EVENT_NOW", new Vector3(0f, 0f, 0f), new Vector3(1f, 1f, 1f), depth);
						ss4.AnimationFinished = delegate
						{
							evenNowAnimeFinished = true;
						};
						ss4.PlayCount = 1;
						ss4.Play();
						yield return null;
						while (!evenNowAnimeFinished)
						{
							yield return null;
						}
						evenNowAnimeFinished = false;
						if (ss4 != null)
						{
							UnityEngine.Object.Destroy(ss4.gameObject);
							ss4 = null;
						}
					}
					else if (!canGlaEvent && (bool)buttonEffect)
					{
						UnityEngine.Object.Destroy(buttonEffect.gameObject);
						buttonEffect = null;
					}
				}
				else if (buttonEffect != null)
				{
					UnityEngine.Object.Destroy(buttonEffect.gameObject);
					buttonEffect = null;
				}
				if (mGame.IsGasyaIconPopUp())
				{
					UISsSprite ss2 = Util.CreateUISsSprite("GasyaIconEffect", button.gameObject, "EFFECT_GASYA_ICON", new Vector3(20f, 15f, 0f), new Vector3(1f, 1f, 1f), depth);
					ss2.AnimationFinished = delegate
					{
						gasyaIconAnimeFinished = true;
					};
					ss2.PlayCount = 1;
					ss2.Play();
					yield return null;
					while (!gasyaIconAnimeFinished)
					{
						yield return null;
					}
					gasyaIconAnimeFinished = false;
					if (ss2 != null)
					{
						UnityEngine.Object.Destroy(ss2.gameObject);
						ss2 = null;
					}
				}
			}
			yield return null;
		}
	}

	public GameObject GetMailButtonGo()
	{
		if (mButtonMailBox != null && mButtonMailBox.gameObject != null)
		{
			return mButtonMailBox.gameObject;
		}
		return null;
	}
}
