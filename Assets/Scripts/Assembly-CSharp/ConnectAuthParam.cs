public class ConnectAuthParam : ConnectParameterBase
{
	public delegate void OnUserTransferedHandler();

	public bool IsUserTransferedCheck { get; private set; }

	public ConnectAuthParam()
	{
		IsUserTransferedCheck = true;
	}

	public void SetTransferedCheck(bool a_flg)
	{
		IsUserTransferedCheck = a_flg;
	}
}
