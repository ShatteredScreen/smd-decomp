using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampaignDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private bool mButtonSeEnable = true;

	private OnDialogClosed mCallback;

	private int mCampaignID;

	private PItem.KIND mIapID;

	private LinkBannerInfo mLinkBanner;

	private LinkBannerRequest mLinkBannerReq;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.LARGE, null, null, 590, 560);
		string empty = string.Empty;
		DateTime result = DateTime.Now;
		if (mGame.GetCampaignEndTime(mCampaignID, ref result, true))
		{
			empty = string.Format(Localization.Get("Gemsale01_Title"), result.Month, result.Day, result.Hour.ToString("00"), result.Minute.ToString("00"));
			InitDialogTitle(empty);
		}
		UISprite uISprite = null;
		GameObject gameObject = Util.CreateGameObject("Banner", base.gameObject);
		gameObject.transform.SetLocalPosition(0f, 90f);
		StartCoroutine(LoadBannerAsync(gameObject, 500, 194, false));
		string campaignImageURL = mGame.GetCampaignImageURL(mCampaignID);
		mLinkBanner = null;
		mLinkBannerReq = new LinkBannerRequest();
		LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerReq);
		mLinkBannerReq.SetHighPriority(true);
		LinkBannerSetting linkBannerSetting = new LinkBannerSetting();
		linkBannerSetting.BannerList = new List<LinkBannerDetail>();
		linkBannerSetting.BannerList.Add(new LinkBannerDetail
		{
			Action = LinkBannerInfo.ACTION.NOP.ToString(),
			Param = 0,
			FileName = campaignImageURL.Substring(campaignImageURL.LastIndexOf("/") + 1)
		});
		if (mLinkBannerReq.mReceiveEveryTimeCallback == null)
		{
			mLinkBannerReq.mReceiveEveryTimeCallback = new LinkBannerRequest.OnReceiveEveryTimeCallback();
			mLinkBannerReq.mReceiveEveryTimeCallback.AddListener(delegate(LinkBannerInfo fn_info)
			{
				mLinkBanner = fn_info;
			});
		}
		linkBannerSetting.BaseURL = campaignImageURL.Substring(0, campaignImageURL.LastIndexOf("/") + 1);
		LinkBannerManager.Instance.GetLinkBannerRequest(mLinkBannerReq, linkBannerSetting, delegate
		{
		});
		uISprite = Util.CreateSprite("PriceFrame", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 7, new Vector3(0f, -60f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(500, 40);
		UILabel uILabel = Util.CreateLabel("Price", base.gameObject, atlasFont);
		PItem itemFromKIND = PurchaseManager.instance.GetItemFromKIND(mIapID);
		Util.SetLabelInfo(uILabel, itemFromKIND.FormattedPrice, mBaseDepth + 8, new Vector3(0f, -60f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(500, 24);
		UIButton button = Util.CreateJellyImageButton("ButtonCampaign", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_jemshop_go", "LC_button_jemshop_go", "LC_button_jemshop_go", mBaseDepth + 9, new Vector3(0f, -140f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		uISprite = Util.CreateSprite("Runa", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "chara_play_runa", mBaseDepth + 5, new Vector3(-140f, -222f, 0f), Vector3.one, false);
		uISprite = Util.CreateSprite("Alt", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "chara_play_alt", mBaseDepth + 6, new Vector3(140f, -222f, 0f), Vector3.one, false);
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mGame.mServerCramVariableData.PurchaseCampaign != null)
		{
			int result = ((mSelectItem == SELECT_ITEM.POSITIVE) ? 1 : 0);
			int campaignGroupID = mGame.GetCampaignGroupID(mCampaignID);
			ServerCramVariableData.PurchaseCampaignInfo purchaseCampaignInfo = mGame.mServerCramVariableData.PurchaseCampaign[mCampaignID];
			ServerCram.GemGuidanceDialog(campaignGroupID, (int)mIapID, (int)purchaseCampaignInfo.StartOfferTime, purchaseCampaignInfo.ShowDialogCount, result);
		}
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (mButtonSeEnable)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
			}
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (mButtonSeEnable)
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void Init(int cp_id)
	{
		mCampaignID = cp_id;
		mIapID = PurchaseManager.instance.GetItemKind(mGame.GetCampaignIapName(cp_id));
		if (mGame.mServerCramVariableData.PurchaseCampaign == null)
		{
			mGame.mServerCramVariableData.PurchaseCampaign = new Dictionary<int, ServerCramVariableData.PurchaseCampaignInfo>();
		}
		if (!mGame.mServerCramVariableData.PurchaseCampaign.ContainsKey(cp_id))
		{
			mGame.mServerCramVariableData.PurchaseCampaign.Add(cp_id, new ServerCramVariableData.PurchaseCampaignInfo
			{
				BuyCount = 0,
				ShowDialogCount = 0,
				StartOfferTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(DateTime.Now),
				Place = 27
			});
		}
		ServerCramVariableData.PurchaseCampaignInfo value = mGame.mServerCramVariableData.PurchaseCampaign[cp_id];
		value.ShowDialogCount++;
		mGame.mServerCramVariableData.PurchaseCampaign[cp_id] = value;
		mGame.SaveServerCramVariableData();
	}

	private IEnumerator LoadBannerAsync(GameObject go, int width, int height, bool is_resize)
	{
		if (!go)
		{
			yield break;
		}
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		UISprite bg2 = Util.CreateSprite("BackGround", go, atlas);
		Util.SetSpriteInfo(bg2, "bg00w", mBaseDepth + 4, Vector3.zero, Vector3.one, false);
		Util.SetSpriteSize(bg2, width, height);
		bg2.color = Color.gray;
		UISsSprite loading2 = Util.CreateUISsSprite("LoadingAnime", go, "ADV_FIGURE_LOADING", Vector3.zero, Vector3.one, mBaseDepth + 5);
		while (true)
		{
			if (mLinkBanner == null)
			{
				yield return null;
				continue;
			}
			if (mLinkBanner.banner != null)
			{
				UITexture tex = go.AddComponent<UITexture>();
				tex.depth = mBaseDepth + 3;
				tex.mainTexture = mLinkBanner.banner;
				if (is_resize)
				{
					tex.SetDimensions(mLinkBanner.banner.width, mLinkBanner.banner.height);
				}
				else
				{
					tex.SetDimensions(width, height);
				}
				if ((bool)loading2)
				{
					GameMain.SafeDestroy(loading2.gameObject);
				}
				if ((bool)bg2)
				{
					GameMain.SafeDestroy(bg2.gameObject);
				}
				loading2 = null;
				bg2 = null;
				break;
			}
			if (mLinkBanner != null && mLinkBanner.banner == null)
			{
				break;
			}
			yield return null;
		}
	}
}
