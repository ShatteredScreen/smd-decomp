using System;
using System.Collections.Generic;

public class AdvDebug_SetItemData_Response : ICloneable
{
	[MiniJSONAlias("items")]
	public List<AdvItemData> ItemDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvDebug_SetItemData_Response Clone()
	{
		return MemberwiseClone() as AdvDebug_SetItemData_Response;
	}
}
