using System;
using System.Collections;
using System.Collections.Generic;

public class SMJsonDataConverter<T> : IMiniJSONConverter where T : SMJsonData
{
	private static string ToSS(object obj)
	{
		return (obj != null) ? obj.ToString() : string.Empty;
	}

	private static TE GetEnumValue<TE>(object obj)
	{
		return (TE)Enum.Parse(typeof(TE), ToSS(obj));
	}

	public object ConvertTo(object a_obj)
	{
		T val = (T)a_obj;
		if (val == null)
		{
			return null;
		}
		IDictionary<string, object> dictionary = new SortedDictionary<string, object>();
		string value = val.SerializeToJson();
		dictionary.Add("Key", val.GetName());
		dictionary.Add("Value", value);
		return dictionary;
	}

	public object ConvertFrom(object a_json)
	{
		IDictionary dictionary = a_json as IDictionary;
		if (dictionary == null)
		{
			return null;
		}
		string text = string.Empty;
		if (dictionary.Contains("Key"))
		{
			text = dictionary["Key"] as string;
		}
		if (string.IsNullOrEmpty(text))
		{
			return null;
		}
		string text2 = string.Empty;
		if (dictionary.Contains("Value"))
		{
			text2 = dictionary["Value"] as string;
		}
		if (string.IsNullOrEmpty(text2))
		{
			return null;
		}
		SMJsonData sMJsonData = null;
		switch (text)
		{
		case "PlayerData":
			sMJsonData = new PlayerData();
			sMJsonData.DeserializeFromJson(text2);
			break;
		case "MyFriend":
			sMJsonData = new MyFriendData();
			sMJsonData.DeserializeFromJson(text2);
			break;
		case "GiftItem":
			sMJsonData = new GiftItemData();
			sMJsonData.DeserializeFromJson(text2);
			break;
		case "PlayerStageData":
			sMJsonData = new PlayerClearData();
			sMJsonData.DeserializeFromJson(text2);
			break;
		case "PlayerSeriesData":
			sMJsonData = new PlayerMapData();
			sMJsonData.DeserializeFromJson(text2);
			break;
		}
		return sMJsonData;
	}
}
