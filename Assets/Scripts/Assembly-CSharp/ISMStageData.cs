using System.Collections.Generic;

public interface ISMStageData
{
	int FormatVersion { get; }

	int OriginalFormatVersion { get; }

	int Revision { get; }

	Def.SERIES Series { get; set; }

	int StageNumber { get; set; }

	int EventID { get; set; }

	string DisplayStageNumber { get; set; }

	int MainStageNumber { get; set; }

	int SubStageNumber { get; set; }

	int CandyKindNum { get; }

	bool UseColor0 { get; }

	bool UseColor1 { get; }

	bool UseColor2 { get; }

	bool UseColor3 { get; }

	bool UseColor4 { get; }

	bool UseColor5 { get; }

	bool UseColor6 { get; }

	Def.STAGE_TYPE StageType { get; }

	Def.STAGE_WIN_CONDITION WinType { get; }

	Def.STAGE_LOSE_CONDITION LoseType { get; }

	int LimitMoves { get; }

	int LimitTimeSec { get; }

	int OverwriteLimitMoves { get; set; }

	int OverwriteLimitTimeSec { get; set; }

	int BronzeScore { get; }

	int SilverScore { get; }

	int GoldScore { get; }

	int Color0 { get; }

	int Color1 { get; }

	int Color2 { get; }

	int Color3 { get; }

	int Color4 { get; }

	int Color5 { get; }

	int Color6 { get; }

	int Ingredient0 { get; }

	int Ingredient1 { get; }

	int Ingredient2 { get; }

	int Ingredient3 { get; }

	int Paper0 { get; }

	int Paper1 { get; }

	int Paper2 { get; }

	Def.BOOSTER_KIND BoosterSlot1 { get; }

	Def.BOOSTER_KIND BoosterSlot2 { get; }

	Def.BOOSTER_KIND BoosterSlot3 { get; }

	Def.BOOSTER_KIND BoosterSlot4 { get; }

	Def.BOOSTER_KIND BoosterSlot5 { get; }

	Def.BOOSTER_KIND BoosterSlot6 { get; }

	Def.BOOSTER_KIND InPuzzleBoosterSlot1 { get; }

	Def.BOOSTER_KIND InPuzzleBoosterSlot2 { get; }

	Def.BOOSTER_KIND InPuzzleBoosterSlot3 { get; }

	Def.BOOSTER_KIND InPuzzleBoosterSlot4 { get; }

	float BonusFreq1 { get; }

	float BonusMove1 { get; }

	int BonusLimit1 { get; }

	int BonusMoveLimit1 { get; }

	int Wave { get; }

	int Bomb { get; }

	int Rainbow { get; }

	int WaveWave { get; }

	int BombBomb { get; }

	int RainbowRainbow { get; }

	int WaveBomb { get; }

	int BombRainbow { get; }

	int WaveRainbow { get; }

	string BGM { get; }

	string EventCode { get; }

	string SegmentCode { get; }

	string TestCode { get; }

	int Timer0 { get; }

	int Timer1 { get; }

	int Timer2 { get; }

	int Timer3 { get; }

	int Timer4 { get; }

	int Timer5 { get; }

	float TimerFreq { get; }

	int TimerLimit { get; }

	int TimerRemainMin { get; }

	int TimerRemainMax { get; }

	int Pair0 { get; }

	int PairLimit0 { get; }

	float JewelFreq { get; }

	int JewelLimit { get; }

	float FCookie1Freq { get; }

	int FCookie1Limit { get; }

	float FCookie2Freq { get; }

	int FCookie2Limit { get; }

	float FCookie3Freq { get; }

	int FCookie3Limit { get; }

	int BossHP { get; }

	Def.TILE_KIND BossType { get; }

	int BossPositionX { get; }

	int BossPositionY { get; }

	int AttackCandyNum { get; }

	bool AttackUseCandy0 { get; }

	bool AttackUseCandy1 { get; }

	bool AttackUseCandy2 { get; }

	bool AttackUseCandy3 { get; }

	bool AttackUseCandy4 { get; }

	bool AttackUseCandy5 { get; }

	bool AttackUseCandy6 { get; }

	int BossFirstAttack { get; }

	int BossAttackInterval { get; }

	int SilverCrystal { get; }

	int EnemyGhost { get; }

	int BossID { get; }

	Def.BOSS_ATTACK BossAttackPattern { get; }

	int BossAttackParam { get; }

	string BossWeakChara { get; }

	int FindStencil0 { get; }

	int FindStencil1 { get; }

	int FindStencil2 { get; }

	int FindStencil3 { get; }

	List<StencilData> StencilDataList { get; }

	int BossAttackParam2 { get; }

	int BossAttackParam3 { get; }

	float FOrb1Freq { get; }

	int FOrb1Limit { get; }

	float FOrb2Freq { get; }

	int FOrb2Limit { get; }

	float FOrb3Freq { get; }

	int FOrb3Limit { get; }

	float FOrb4Freq { get; }

	int FOrb4Limit { get; }

	float FootPieceFreq1 { get; }

	int FootPieceLimit1 { get; }

	float FJewelFreq1 { get; set; }

	int FJewel1Limit1 { get; set; }

	int FJewel3Limit1 { get; set; }

	int FJewel5Limit1 { get; set; }

	int FJewel9Limit1 { get; set; }

	float FPerl00Freq1 { get; }

	int FPerl00Limit1 { get; }

	float FPerl01Freq1 { get; }

	int FPerl01Limit1 { get; }

	float FPerl02Freq1 { get; }

	int FPerl02Limit1 { get; }

	List<IntVector2> DianaWayPosList { get; }

	int Width { get; }

	int Height { get; }

	int PlayAreaX { get; }

	int PlayAreaY { get; }

	int PlayAreaWidth { get; }

	int PlayAreaHeight { get; }

	ISMTile GetTile(int x, int y);

	ISMAttributeTile GetAttributeTile(int x, int y);

	ISMTileInfo GetTileInfo(int x, int y);

	int GetTargetNum(Def.TILE_KIND a_kind);

	Def.TILE_KIND[] GetNormaKindArray();

	void SetJewelSetting(float a_freq, int a_limit1, int a_limit3, int a_limit5, int a_limit9);
}
