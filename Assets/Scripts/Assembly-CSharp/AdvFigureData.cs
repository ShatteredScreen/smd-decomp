public class AdvFigureData
{
	[MiniJSONAlias("id")]
	public int id { get; set; }

	[MiniJSONAlias("xp")]
	public int xp { get; set; }

	[MiniJSONAlias("lvl")]
	public int lvl { get; set; }

	[MiniJSONAlias("skill_xp")]
	public int skill_xp { get; set; }

	[MiniJSONAlias("skill_lvl")]
	public int skill_lvl { get; set; }

	[MiniJSONAlias("createtime")]
	public long createtime { get; set; }
}
