using System.Collections.Generic;
using UnityEngine;

public class LabyrinthCourseInfoObject : ScSpriteObject
{
	public BIJImage mAtlas;

	private OneShotAnime mInfoBack;

	private Vector3 mInfoBasePosition = Vector3.zero;

	protected Dictionary<string, string> mChangeImageDict = new Dictionary<string, string>();

	public void Init(Vector3 pos, int a_stageNo, SMEventPageData a_eventPageData, bool a_opened, int a_get_num, int a_star_num)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stageNo, out a_main, out a_sub);
		string key = "LABYRINTH_COURSEBUTTON_INFO";
		mChangeImageDict["base"] = "null";
		mChangeImageDict["course"] = string.Format("LC_course{0}", a_main);
		mChangeImageDict["get_num"] = string.Format("event_box_num{0}", a_get_num);
		mChangeImageDict["total_num"] = string.Format("event_box_num{0}", a_eventPageData.EventCourseList[a_main - 1].StageNum);
		mChangeImageDict["mag_1"] = string.Format("event_l_num{0}", a_eventPageData.EventCourseList[a_main - 1].CourseBonusRate / 10);
		mChangeImageDict["mag_2"] = string.Format("event_l_num{0}", a_eventPageData.EventCourseList[a_main - 1].CourseBonusRate % 10);
		mChangeImageDict["star_num_ten"] = string.Format("event_l_num{0}", a_star_num / 10);
		mChangeImageDict["star_num_one"] = string.Format("event_l_num{0}", a_star_num % 10);
		mAtlas = ResourceManager.LoadImage("EVENTLABYRINTH").Image;
		ChangeAnime(key, mChangeImageDict, true);
		_sprite.transform.localScale = new Vector3(1f, 1f, 1f);
		_sprite.transform.localPosition = pos;
		if (mInfoBack == null)
		{
			BIJImage image = ResourceManager.LoadImage(a_eventPageData.EventSetting.MapAtlasKey).Image;
			mInfoBack = Util.CreateOneShotAnime("InfoBase" + a_stageNo, string.Format("MAP_EV{0}_FIXEDSPRITE", a_eventPageData.EventID), base.gameObject, mInfoBasePosition, Vector3.one, new CHANGE_PART_INFO[1]
			{
				new CHANGE_PART_INFO
				{
					partName = "change000",
					spriteName = "LC_event_ribbon"
				}
			}, image, false);
		}
		_sprite.Pause();
		mInfoBack.Pause();
	}

	public void SetColor(Color col)
	{
	}

	public bool ChangeAnime(string key, Dictionary<string, string> changeParts, bool force)
	{
		if (mAtlas == null)
		{
			return false;
		}
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(key);
		if (resSsAnimation == null)
		{
			_sprite.Animation = null;
			return true;
		}
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		if (changeParts != null)
		{
			SsPartRes[] partList = ssAnime.PartList;
			Vector2 vector = default(Vector2);
			for (int i = 0; i < partList.Length; i++)
			{
				if (!partList[i].IsRoot)
				{
					if (partList[i].Name.CompareTo("base") == 0)
					{
						mInfoBasePosition = new Vector3(partList[i].PosX(0), 0f - partList[i].PosY(0), 1f);
					}
					string value;
					if (changeParts.TryGetValue(partList[i].Name, out value))
					{
						Vector2 offset = mAtlas.GetOffset(value);
						Vector2 size = mAtlas.GetSize(value);
						float x = offset.x;
						float y = offset.y;
						float x2 = offset.x + size.x;
						float y2 = offset.y + size.y;
						partList[i].UVs = new Vector2[4]
						{
							new Vector2(x, y2),
							new Vector2(x2, y2),
							new Vector2(x2, y),
							new Vector2(x, y)
						};
						Vector4 pixelPadding = mAtlas.GetPixelPadding(value);
						vector.x = partList[i].ScaleX(0);
						vector.y = partList[i].ScaleY(0);
						size = mAtlas.GetPixelSize(value);
						size.x *= vector.x;
						size.y *= vector.y;
						offset = new Vector2(partList[i].OriginX, partList[i].OriginY);
						offset.x += pixelPadding.x;
						offset.y += pixelPadding.y;
						size.x -= pixelPadding.x + pixelPadding.z;
						size.y -= pixelPadding.y + pixelPadding.w;
						x = (0f - size.x) / 2f;
						y = (0f - size.y) / 2f;
						x2 = size.x / 2f;
						y2 = size.y / 2f;
						float z = 0f;
						partList[i].OrgVertices = new Vector3[4]
						{
							new Vector3(x, y2, z),
							new Vector3(x2, y2, z),
							new Vector3(x2, y, z),
							new Vector3(x, y, z)
						};
					}
				}
			}
		}
		CleanupAnimation();
		_sprite.Animation = ssAnime;
		_sprite.Play();
		_sprite.PlayCount = 0;
		return true;
	}
}
