using System;

public class ConnectGameProfile : ConnectBase
{
	protected override void InitServerFlg()
	{
		GameMain.mGameProfileSucceed = false;
		GameMain.mGameProfileCheckDone = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mGameProfileSucceed = true;
		GameMain.mGameProfileCheckDone = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mGameProfileCheckDone = true;
		GameMain.mGameProfileSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			mGame.mGameProfile = new GameProfile();
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetGameInfoTime, DateTime.Now);
		long gET_GAME_INFO_FREQ = GameMain.GET_GAME_INFO_FREQ;
		if (gET_GAME_INFO_FREQ > 0 && num < gET_GAME_INFO_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		GameMain.UpdateVersionFlag = GameProfile.UpdateKind.NONE;
		string text = string.Empty;
		string a_datekey = string.Empty;
		if (mParam != null)
		{
			ConnectGetProfileParam connectGetProfileParam = mParam as ConnectGetProfileParam;
			if (connectGetProfileParam != null)
			{
				a_datekey = connectGetProfileParam.DebugDate;
				text = connectGetProfileParam.DebugKey;
			}
		}
		if (GameMain.DEBUG_DEBUGSERVERINFO && string.IsNullOrEmpty(text))
		{
			text = "debug";
			GameMain.GET_GAME_INFO_FREQ = 240L;
		}
		InitServerFlg();
		if (!Server.GetGameInfo(a_datekey, text))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetGameProfileFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastGetGameInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
