using System;
using System.Collections.Generic;

public class SMMapStageSetting : SMMapPageSettingBase, ICloneable
{
	public string DisplayStageNo = string.Empty;

	public string StageType = string.Empty;

	public string StageEnterCondition = string.Empty;

	public string StageEnterConditionID = string.Empty;

	public string StageBGKey = string.Empty;

	public List<Def.ITEM_CATEGORY> BoxKind = new List<Def.ITEM_CATEGORY>();

	public List<int> BoxID = new List<int>();

	public List<Def.UNLOCK_TYPE> BoxSpawnCondition = new List<Def.UNLOCK_TYPE>();

	public List<int> BoxSpawnConditionID = new List<int>();

	public List<Def.TILE_KIND> BoxSpawnTileKind = new List<Def.TILE_KIND>();

	public string StageSelectDemo = string.Empty;

	public List<int> EnterPartners = new List<int>();

	public List<int> DailyEventBoxID = new List<int>();

	public bool IsForcePartner { get; private set; }

	public int PartnerZ_Level { get; private set; }

	public bool IsDailyEvent { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public int CopyToBoxInfoAt(int index, ref SMMapStageSetting rhs)
	{
		if (index < 0 || index >= BoxID.Count)
		{
			return -1;
		}
		int num = BoxID[index];
		if (rhs.BoxID.Contains(num))
		{
			return num;
		}
		try
		{
			Def.ITEM_CATEGORY item = BoxKind[index];
			Def.UNLOCK_TYPE item2 = BoxSpawnCondition[index];
			int item3 = BoxSpawnConditionID[index];
			Def.TILE_KIND item4 = BoxSpawnTileKind[index];
			rhs.BoxKind.Add(item);
			rhs.BoxID.Add(num);
			rhs.BoxSpawnCondition.Add(item2);
			rhs.BoxSpawnConditionID.Add(item3);
			rhs.BoxSpawnTileKind.Add(item4);
			return num;
		}
		catch (Exception)
		{
			return -1;
		}
	}

	public SMMapStageSetting Clone()
	{
		SMMapStageSetting rhs = MemberwiseClone() as SMMapStageSetting;
		rhs.BoxKind = new List<Def.ITEM_CATEGORY>();
		rhs.BoxID = new List<int>();
		rhs.BoxSpawnCondition = new List<Def.UNLOCK_TYPE>();
		rhs.BoxSpawnConditionID = new List<int>();
		rhs.BoxSpawnTileKind = new List<Def.TILE_KIND>();
		for (int i = 0; i < BoxID.Count; i++)
		{
			CopyToBoxInfoAt(i, ref rhs);
		}
		return rhs;
	}

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteShort((short)mCourseNo);
		a_writer.WriteInt(mStageNo);
		a_writer.WriteInt(mSubNo);
		a_writer.WriteUTF(DisplayStageNo);
		a_writer.WriteUTF(OpenDemo);
		a_writer.WriteUTF(ClearDemo);
		a_writer.WriteUTF(StageType);
		a_writer.WriteUTF(StageEnterCondition);
		a_writer.WriteUTF(StageEnterConditionID);
		short num = (short)BoxKind.Count;
		a_writer.WriteShort(num);
		for (int i = 0; i < num; i++)
		{
			a_writer.WriteInt((int)BoxKind[i]);
		}
		num = (short)BoxID.Count;
		a_writer.WriteShort(num);
		for (int j = 0; j < num; j++)
		{
			a_writer.WriteInt(BoxID[j]);
		}
		num = (short)BoxSpawnCondition.Count;
		a_writer.WriteShort(num);
		for (int k = 0; k < num; k++)
		{
			a_writer.WriteShort((short)BoxSpawnCondition[k]);
		}
		num = (short)BoxSpawnConditionID.Count;
		a_writer.WriteShort(num);
		for (int l = 0; l < num; l++)
		{
			a_writer.WriteInt(BoxSpawnConditionID[l]);
		}
		num = (short)BoxSpawnTileKind.Count;
		a_writer.WriteShort(num);
		for (int m = 0; m < num; m++)
		{
			a_writer.WriteShort((short)BoxSpawnTileKind[m]);
		}
		a_writer.WriteUTF(StageBGKey);
		a_writer.WriteUTF(OpenDemo);
		a_writer.WriteUTF(StageSelectDemo);
		a_writer.WriteUTF(ClearDemo);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		mCourseNo = a_reader.ReadShort();
		mStageNo = a_reader.ReadInt();
		mSubNo = a_reader.ReadInt();
		DisplayStageNo = a_reader.ReadUTF();
		OpenDemo = a_reader.ReadUTF();
		ClearDemo = a_reader.ReadUTF();
		StageType = a_reader.ReadUTF();
		StageEnterCondition = a_reader.ReadUTF();
		StageEnterConditionID = a_reader.ReadUTF();
		short num = a_reader.ReadShort();
		for (int i = 0; i < num; i++)
		{
			BoxKind.Add((Def.ITEM_CATEGORY)a_reader.ReadInt());
		}
		num = a_reader.ReadShort();
		for (int j = 0; j < num; j++)
		{
			BoxID.Add(a_reader.ReadInt());
		}
		num = a_reader.ReadShort();
		for (int k = 0; k < num; k++)
		{
			BoxSpawnCondition.Add((Def.UNLOCK_TYPE)a_reader.ReadShort());
		}
		num = a_reader.ReadShort();
		for (int l = 0; l < num; l++)
		{
			BoxSpawnConditionID.Add(a_reader.ReadInt());
		}
		num = a_reader.ReadShort();
		for (int m = 0; m < num; m++)
		{
			BoxSpawnTileKind.Add((Def.TILE_KIND)a_reader.ReadShort());
		}
		StageBGKey = a_reader.ReadUTF();
		OpenDemo = a_reader.ReadUTF();
		StageSelectDemo = a_reader.ReadUTF();
		ClearDemo = a_reader.ReadUTF();
		GetEnablePartners();
	}

	public void GetEnablePartners()
	{
		string[] array = StageEnterCondition.Split(',');
		if (StageEnterCondition.CompareTo("PARTNER") == 0 && !string.IsNullOrEmpty(StageEnterConditionID))
		{
			string[] array2 = StageEnterConditionID.Split(',');
			if (array2.Length == 1)
			{
				EnterPartners.Add(int.Parse(StageEnterConditionID));
			}
			else
			{
				for (int i = 0; i < array2.Length; i++)
				{
					EnterPartners.Add(int.Parse(array2[i]));
				}
			}
			IsForcePartner = false;
		}
		else if (StageEnterCondition.CompareTo("PARTNER_F") == 0 && !string.IsNullOrEmpty(StageEnterConditionID))
		{
			EnterPartners.Add(int.Parse(StageEnterConditionID));
			IsForcePartner = true;
		}
		else if (StageEnterCondition.CompareTo("PARTNER_Z") == 0 && !string.IsNullOrEmpty(StageEnterConditionID))
		{
			string[] array3 = StageEnterConditionID.Split(',');
			if (array3.Length == 2)
			{
				EnterPartners.Add(int.Parse(array3[0]));
				PartnerZ_Level = int.Parse(array3[1]);
			}
			IsForcePartner = true;
		}
	}

	public int GetStoryDemoAtStageSelect()
	{
		int result = -1;
		if (!int.TryParse(StageSelectDemo, out result))
		{
			result = -1;
		}
		return result;
	}

	public int GetStoryDemoAtStageClear()
	{
		int result = -1;
		if (!int.TryParse(ClearDemo, out result))
		{
			result = -1;
		}
		return result;
	}

	public string GetStageBGKey()
	{
		if (string.IsNullOrEmpty(StageBGKey))
		{
			return "PARTNER_BG0";
		}
		return StageBGKey;
	}
}
