using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventPageDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2
	}

	public class SeasonEventListItem : SMVerticalListItem
	{
		public SeasonEventSettings ServerEventSetting;

		public SMSeasonEventSetting EventData;

		public BIJImage Atlas;

		public string BannerName;
	}

	public delegate void OnDialogClosed();

	public delegate void OnEventSelected(int a_eventID);

	public delegate void OnOpenCompleted();

	private const int PortraitWidth = 520;

	private const int PortraitHeight = 860;

	private const int LandscapeWidth = 1050;

	private const int LandscapeHeight = 440;

	private GameStateSMMap mGSM;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private GameStateManager mGameState;

	private Vector2 mLogScreen = Vector2.zero;

	private UIButton mExitButton;

	private OnDialogClosed mCallback;

	private OnEventSelected mEventCallback;

	private OnOpenCompleted mOpenCompletedCallback;

	private bool mLaceMove;

	private UISprite mTitleBarL;

	private UISprite mTitleBarR;

	private UISprite mLace1;

	private UISprite mLace2;

	private UISprite mLace3;

	private UISprite mLace4;

	private UISprite mGardenL;

	private UISprite mGardenR;

	private SMVerticalListInfo mListInfo;

	private SMVerticalList mList;

	private List<SMVerticalListItem> mListItem;

	private bool mMakeListFinihed;

	private int mLandscapeHeight = 440;

	private bool mIsNoEvent;

	private bool mIsOldEventGateOpen;

	private GameObject mOldEventArrowTarget;

	private Coroutine mCenterProc;

	private bool mCenterLoop;

	private List<string> mPreLoadImageList = new List<string>();

	private UISprite TitleSprite;

	public void Init()
	{
		mJellyflg = Def.DIALOG_PRODUCT.EVENT_DIALOG;
	}

	public override void Awake()
	{
		mJellyflg = Def.DIALOG_PRODUCT.EVENT_DIALOG;
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mParticleFeather = false;
		mLogScreen = Util.LogScreenSize();
		ScreenOrientation screenOrientation = Util.ScreenOrientation;
		if (screenOrientation == ScreenOrientation.Portrait || screenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			mLandscapeHeight = (int)mLogScreen.x;
		}
		else
		{
			mLandscapeHeight = (int)mLogScreen.y;
		}
		mLandscapeHeight -= 200;
		if (mLandscapeHeight < 440)
		{
			mLandscapeHeight = 440;
		}
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
		}
		else
		{
			if (!mMakeListFinihed)
			{
				return;
			}
			if (mState.GetStatus() == STATE.INIT)
			{
				if (mIsOldEventGateOpen && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.OLDEVENT_0))
				{
					TutorialStart(Def.TUTORIAL_INDEX.OLDEVENT_0);
				}
				else
				{
					mState.Reset(STATE.MAIN, true);
				}
			}
			mState.Update();
		}
	}

	public override IEnumerator BuildResource()
	{
		if (mGame.mEventProfile.SeasonEventList.Count > 0)
		{
			bool noEvent = true;
			SMSeasonEventSetting setting2 = null;
			SeasonEventSettings settingServer2 = null;
			List<ResImage> asyncList = new List<ResImage>();
			for (int j = 0; j < mGame.mEventProfile.SeasonEventList.Count; j++)
			{
				int eventID = mGame.mEventProfile.SeasonEventList[j].EventID;
				if (!mGame.IsSeasonEventExpired(eventID) && (eventID == 1000 || eventID < 200) && mGame.mEventData.InSessionEventList.ContainsKey(eventID))
				{
					settingServer2 = mGame.mEventProfile.SeasonEventList[j];
					setting2 = mGame.mEventData.InSessionEventList[eventID];
					ResImage bannerAtlas = ResourceManager.LoadImageAsync(setting2.BannerAtlas);
					mPreLoadImageList.Add(setting2.BannerAtlas);
					asyncList.Add(bannerAtlas);
					noEvent = false;
				}
			}
			if (!noEvent)
			{
				for (int i = 0; i < asyncList.Count; i++)
				{
					while (asyncList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
					{
						yield return 0;
					}
				}
			}
		}
		ResImage image = ResourceManager.LoadImageAsync("EVENT_ENTRANCE");
		mPreLoadImageList.Add("EVENT_ENTRANCE");
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		Vector2 vector = Util.LogScreenSize();
		if (mJelly != null)
		{
			JellySecondDialog jellySecondDialog = mJelly as JellySecondDialog;
			jellySecondDialog.SetInitialPosition(vector.y * 1.5f, 0f, 0.3f, 0.2f);
		}
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, Util.ScreenOrientation);
		Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopRight, Util.ScreenOrientation);
		Vector2 vector4 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomLeft, Util.ScreenOrientation);
		mLaceMove = true;
		StartCoroutine(LaceRotate(base.gameObject, mBaseDepth + 1, new Vector3(vector3.x + 30f, vector3.y - 30f, 0f), new Vector3(1f, 1f, 1f), 15f));
		StartCoroutine(LaceRotate(base.gameObject, mBaseDepth + 1, new Vector3(vector3.x - 140f, vector3.y + 10f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(base.gameObject, mBaseDepth + 1, new Vector3(vector4.x / 2f + 190f, vector4.y - 50f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(base.gameObject, mBaseDepth + 1, new Vector3(vector4.x - 20f, vector4.y + 60f, 0f), new Vector3(1f, 1f, 1f), 15f));
		UISpriteData uISpriteData = null;
		BIJImage image = ResourceManager.LoadImage("EVENT_ENTRANCE").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = Util.CreateSprite("BackGround", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "event_back", mBaseDepth - 1, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Tiled;
		UISpriteData sprite = uISprite.atlas.GetSprite("event_back");
		int num = Mathf.RoundToInt(sprite.width + sprite.paddingLeft + sprite.paddingRight);
		int num2 = Mathf.RoundToInt(sprite.height + sprite.paddingTop + sprite.paddingBottom);
		uISprite.SetDimensions(num2, num2);
		float num3 = 1f;
		float num4 = vector.y;
		if (vector.x > vector.y)
		{
			num4 = vector.x;
		}
		if (num4 > (float)num2)
		{
			num3 = num4 / (float)num2;
		}
		uISprite.transform.localScale = new Vector3(num3, num3, 1f);
		mTitleBarR = Util.CreateSprite("StatusR", base.gameObject, image2);
		Util.SetSpriteInfo(mTitleBarR, "menubar_frame", mBaseDepth + 2, new Vector3(vector2.x - 2f, vector2.y + 168f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBarL = Util.CreateSprite("StatusL", base.gameObject, image2);
		Util.SetSpriteInfo(mTitleBarL, "menubar_frame", mBaseDepth + 2, new Vector3(vector2.x + 2f, vector2.y + 168f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mGardenL = Util.CreateSprite("GarlandL", base.gameObject, image);
		Util.SetSpriteInfo(mGardenL, "garland", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		UISpriteData sprite2 = mGardenL.atlas.GetSprite("garland");
		mGardenL.transform.localPosition = new Vector3((0f - vector.x) / 2f + (float)(sprite2.width / 2) - 10f, vector2.y - (float)(sprite2.height / 2) - 50f);
		mGardenL.flip = UIBasicSprite.Flip.Horizontally;
		mGardenR = Util.CreateSprite("GarlandR", base.gameObject, image);
		Util.SetSpriteInfo(mGardenR, "garland", mBaseDepth - 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		mGardenR.transform.localPosition = new Vector3(vector.x / 2f - (float)(sprite2.width / 2) + 10f, vector2.y - (float)(sprite2.height / 2) - 50f);
		bool flag = true;
		SMSeasonEventSetting sMSeasonEventSetting = null;
		SeasonEventSettings seasonEventSettings = null;
		SeasonEventListItem seasonEventListItem = null;
		mListItem = new List<SMVerticalListItem>();
		if (mGame.mEventProfile.SeasonEventList.Count > 0)
		{
			for (int i = 0; i < mGame.mEventProfile.SeasonEventList.Count; i++)
			{
				int eventID = mGame.mEventProfile.SeasonEventList[i].EventID;
				if (!mGame.IsSeasonEventExpired(eventID) && (eventID == 1000 || eventID < 200) && mGame.mEventData.InSessionEventList.ContainsKey(eventID))
				{
					seasonEventSettings = mGame.mEventProfile.SeasonEventList[i];
					sMSeasonEventSetting = mGame.mEventData.InSessionEventList[eventID];
					BIJImage image3 = ResourceManager.LoadImage(sMSeasonEventSetting.BannerAtlas).Image;
					string bannerName = sMSeasonEventSetting.BannerName;
					SeasonEventListItem seasonEventListItem2 = new SeasonEventListItem();
					seasonEventListItem2.ServerEventSetting = seasonEventSettings;
					seasonEventListItem2.EventData = sMSeasonEventSetting;
					seasonEventListItem2.Atlas = image3;
					seasonEventListItem2.BannerName = bannerName;
					if (eventID == 1000)
					{
						seasonEventListItem = seasonEventListItem2;
					}
					else
					{
						mListItem.Add(seasonEventListItem2);
					}
					flag = false;
				}
			}
			if (seasonEventListItem != null)
			{
				mListItem.Add(seasonEventListItem);
				mIsOldEventGateOpen = true;
			}
		}
		if (flag)
		{
			mIsNoEvent = true;
			uISprite = Util.CreateSprite("EventPanel", base.gameObject, image);
			Util.SetSpriteInfo(uISprite, "LC_event_banner_no", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		}
		string text = Localization.Get("EventPage");
		UILabel uILabel = Util.CreateLabel("EventDialogTitle", mTitleBarR.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 5, new Vector3(2f, -201f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		float x = vector4.x + 55f;
		float y = vector4.y + 55f;
		mExitButton = Util.CreateJellyImageButton("ExitButton", base.gameObject, image2);
		Util.SetImageButtonInfo(mExitButton, "button_exit", "button_exit", "button_exit", mBaseDepth + 5, new Vector3(x, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(mExitButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
		UpdateBaseDepth();
		SetLayout(Util.ScreenOrientation);
	}

	private void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("EVENT_ENTRANCE").Image;
		UIFont atlasFont = GameMain.LoadFont();
		SeasonEventListItem seasonEventListItem = item as SeasonEventListItem;
		float num2 = mList.mList.cellWidth;
		float cellHeight = mList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back" + index, itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", num, Vector3.zero, Vector3.one, false);
		if (mListItem.Count % 2 == 1 && index == mListItem.Count - 1)
		{
			num2 *= 4f;
		}
		uISprite.width = (int)num2;
		uISprite.height = (int)cellHeight;
		bool flag = true;
		flag = false;
		DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(seasonEventListItem.ServerEventSetting.StartTime, flag);
		DateTime a_endTime = DateTimeUtil.UnixTimeStampToDateTime(seasonEventListItem.ServerEventSetting.EndTime, flag);
		string text = string.Format(Localization.Get("EventPage_Schedule"), dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, a_endTime.Month, a_endTime.Day, a_endTime.Hour, a_endTime.Minute);
		if (seasonEventListItem.EventData.EventID != 1000)
		{
			UISprite uISprite2 = Util.CreateSprite("EventPanel", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, "event_panel00", mBaseDepth + 1, new Vector3(0f, 180f, 0f), Vector3.one, false);
			UILabel uILabel = Util.CreateLabel("EventTitle", uISprite2.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, Vector3.zero, 24, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = new Color(1f, 0.9411765f, 0.44313726f, 1f);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(490, 28);
			uISprite2 = Util.CreateSprite("RemainPanel", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, "instruction_panel_event", mBaseDepth + 2, new Vector3(135f, -150f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(230, 50);
			uISprite2.type = UIBasicSprite.Type.Sliced;
			StartCoroutine(UpdateEventLastDay(uISprite2, a_endTime, seasonEventListItem.ServerEventSetting.EventID));
		}
		UIButton uIButton = Util.CreateJellyImageButton("EventBanner_" + seasonEventListItem.EventData.EventID, itemGO, seasonEventListItem.Atlas);
		Util.SetImageButtonInfo(uIButton, seasonEventListItem.BannerName, seasonEventListItem.BannerName, seasonEventListItem.BannerName, num + 1, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnCurrentEventPushed", UIButtonMessage.Trigger.OnClick);
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mList.mListScrollView;
		if (seasonEventListItem.EventData.EventID == 1000)
		{
			uIButton.gameObject.transform.localPosition = new Vector3(0f, 80f, 0f);
			mOldEventArrowTarget = uIButton.gameObject;
		}
	}

	protected virtual IEnumerator UpdateEventLastDay(UISprite LastDay, DateTime a_endTime, int a_eventID)
	{
		float counter2 = 0f;
		bool is_draw = false;
		Vector3 BasePos = LastDay.transform.localPosition;
		float delay_timer2 = 5f;
		UIFont Font = GameMain.LoadFont();
		bool is_last_day2 = mGame.IsSeasonEventLastDayFromEventID(a_eventID);
		string text4 = string.Empty;
		DateTime now = DateTimeUtil.Now().ToUniversalTime();
		long span2 = DateTimeUtil.BetweenMinutes(now, a_endTime);
		text4 = ((span2 < 0) ? string.Format(Localization.Get("EventExpired_Title")) : ((span2 >= 60) ? string.Format(Localization.Get("EventPageCount_00"), span2 / 60) : string.Format(Localization.Get("EventPageCount_01"), span2)));
		UILabel label = Util.CreateLabel("RemainTime", LastDay.gameObject, Font);
		Util.SetLabelInfo(label, text4, mBaseDepth + 3, Vector3.zero, 28, 0, 0, UIWidget.Pivot.Center);
		label.color = new Color(1f, 0.9843137f, 0.57254905f, 1f);
		label.overflowMethod = UILabel.Overflow.ShrinkContent;
		label.SetDimensions(200, 50);
		NGUITools.SetActive(LastDay.gameObject, false);
		while (!(LastDay == null))
		{
			is_last_day2 = mGame.IsSeasonEventLastDayFromEventID(a_eventID);
			span2 = DateTimeUtil.BetweenMinutes(now, a_endTime);
			if (is_last_day2)
			{
				now = DateTimeUtil.Now().ToUniversalTime();
				text4 = ((span2 < 0) ? string.Format(Localization.Get("EventExpired_Title")) : ((span2 >= 60) ? string.Format(Localization.Get("EventPageCount_00"), span2 / 60) : string.Format(Localization.Get("EventPageCount_01"), span2)));
				Util.SetLabelText(label, text4);
				if (!is_draw)
				{
					is_draw = is_last_day2;
					NGUITools.SetActive(LastDay.gameObject, true);
				}
			}
			else if (is_draw)
			{
				is_draw = is_last_day2;
				if (span2 <= 0)
				{
					text4 = string.Format(Localization.Get("EventExpired_Title"));
					Util.SetLabelText(label, text4);
				}
				else
				{
					NGUITools.SetActive(LastDay.gameObject, false);
				}
			}
			if (!is_draw)
			{
				delay_timer2 = 5f;
				counter2 = 0f;
				LastDay.transform.localPosition = BasePos;
			}
			yield return null;
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, Util.ScreenOrientation);
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopRight, Util.ScreenOrientation);
		Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomLeft, Util.ScreenOrientation);
		Vector2 vector4 = Util.LogScreenSize();
		bool flag = false;
		float num = 0f;
		if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
		{
			flag = true;
			num = -15f;
		}
		else
		{
			num = 10f;
		}
		mExitButton.transform.localPosition = new Vector3(vector3.x + 55f, vector3.y + 55f, 0f);
		string text = "menubar_frame";
		mTitleBarR.gameObject.transform.localPosition = new Vector3(vector.x - 2f, vector.y + 168f, 0f);
		mTitleBarL.gameObject.transform.localPosition = new Vector3(vector.x + 2f, vector.y + 168f, 0f);
		UISpriteData sprite = mGardenL.atlas.GetSprite("garland");
		mGardenL.transform.localPosition = new Vector3((0f - vector4.x) / 2f + (float)(sprite.width / 2) - 10f, vector.y - (float)(sprite.height / 2) - 50f);
		mGardenR.transform.localPosition = new Vector3(vector4.x / 2f - (float)(sprite.width / 2) + 10f, vector.y - (float)(sprite.height / 2) - 50f);
		mLace1.transform.localPosition = new Vector3(vector2.x + 30f, vector2.y - 30f, 0f);
		mLace2.transform.localPosition = new Vector3(vector2.x - 140f, vector2.y + 10f, 0f);
		mLace3.transform.localPosition = new Vector3(vector3.x + 190f, vector3.y - 50f, 0f);
		mLace4.transform.localPosition = new Vector3(vector3.x - 20f, vector3.y + 60f, 0f);
		if (mList != null)
		{
			float num2 = 1f;
			if (!flag && Util.IsWideScreen())
			{
				num2 = 0.94f;
			}
			float value = mList.Scroll();
			mList.transform.localScale = new Vector3(num2, num2, 1f);
			mList.OnScreenChanged(flag, 0f, num);
			mList.ScrollSet(value);
			if (mGame.mTutorialManager.IsTutorialPlaying() && mCenterProc != null)
			{
				mCenterProc = StartCoroutine(CenterTarget());
			}
		}
		if (TitleSprite != null)
		{
			Vector3 localPosition = ((!flag) ? new Vector3(0f, mLandscapeHeight / 2 + 30, 0f) : new Vector3(0f, 460f, 0f));
			TitleSprite.gameObject.transform.localPosition = localPosition;
		}
	}

	public void OnCurrentEventPushed(GameObject go)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		if (mEventCallback != null)
		{
			string text = go.name;
			string[] array = text.Split('_');
			if (array.Length > 1)
			{
				int a_eventID = int.Parse(array[1]);
				mEventCallback(a_eventID);
				mState.Reset(STATE.WAIT, true);
			}
		}
	}

	public void ResetState()
	{
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
		BIJImage image = ResourceManager.LoadImage("EVENT_ENTRANCE").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		mGame.PlaySe("VOICE_010", -1);
		bool flag = false;
		ScreenOrientation screenOrientation = Util.ScreenOrientation;
		float num = 0f;
		if (screenOrientation == ScreenOrientation.Portrait || screenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			flag = true;
			num = -15f;
		}
		else
		{
			num = 10f;
		}
		if (!mIsNoEvent)
		{
			float num2 = 860f;
			if (mListItem.Count == 1)
			{
				num2 = num2 / 2f + 10f;
			}
			bool flag2 = true;
			if (mListItem.Count > 2)
			{
				flag2 = false;
			}
			mListInfo = new SMVerticalListInfo(2, 1050f, mLandscapeHeight, 1, 520f, num2, 1, 410);
			mList = SMVerticalList.Make(base.gameObject, this, mListInfo, mListItem, 0f, num, mBaseDepth, flag, UIScrollView.Movement.Vertical, false, flag2);
			mList.OnCreateItem = OnCreateItem;
			mList.OnCreateFinish = OnCreateFinish;
			float num3 = 1f;
			if (!flag && Util.IsWideScreen())
			{
				num3 = 0.94f;
			}
			mList.transform.localScale = new Vector3(num3, num3, 1f);
			if (flag2)
			{
				mList.HideScrollBar();
			}
		}
		if (mOpenCompletedCallback != null)
		{
			mOpenCompletedCallback();
		}
	}

	private void OnCreateFinish()
	{
		mMakeListFinihed = true;
		if (mListItem.Count < 2)
		{
			mList.ScrollCustomStop();
		}
		SetLayout(Util.ScreenOrientation);
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		for (int i = 0; i < mPreLoadImageList.Count; i++)
		{
			ResourceManager.UnloadImage(mPreLoadImageList[i]);
		}
		yield return null;
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public override void Close()
	{
		base.Close();
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mState.Reset(STATE.WAIT, true);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetEventCallback(OnEventSelected callback)
	{
		mEventCallback = callback;
	}

	public void SetOpenFinishedCallback(OnOpenCompleted callback)
	{
		mOpenCompletedCallback = callback;
	}

	private IEnumerator LaceRotate(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		ResImage resImageCollection = ResourceManager.LoadImage("EVENT_ENTRANCE");
		UISprite sprite;
		if (mLace1 == null)
		{
			mLace1 = Util.CreateSprite("Lace", parent, resImageCollection.Image);
			Util.SetSpriteInfo(mLace1, "characolle_back_lace02", depth, pos, scale, false);
			sprite = mLace1;
		}
		else if (mLace2 == null)
		{
			mLace2 = Util.CreateSprite("Lace", parent, resImageCollection.Image);
			Util.SetSpriteInfo(mLace2, "characolle_back_lace02", depth, pos, scale, false);
			sprite = mLace2;
		}
		else if (mLace3 == null)
		{
			mLace3 = Util.CreateSprite("Lace", parent, resImageCollection.Image);
			Util.SetSpriteInfo(mLace3, "characolle_back_lace02", depth, pos, scale, false);
			sprite = mLace3;
		}
		else
		{
			mLace4 = Util.CreateSprite("Lace", parent, resImageCollection.Image);
			Util.SetSpriteInfo(mLace4, "characolle_back_lace02", depth, pos, scale, false);
			sprite = mLace4;
		}
		float angle = 0f;
		while (mLaceMove)
		{
			angle += Time.deltaTime * speed;
			sprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
		mState.Reset(STATE.WAIT, true);
		if (index == Def.TUTORIAL_INDEX.OLDEVENT_0)
		{
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, mOldEventArrowTarget, true);
			mGame.mTutorialManager.SetTutorialArrowOffset(0f, 80f);
			mCenterProc = StartCoroutine(CenterTarget());
		}
	}

	private IEnumerator CenterTarget()
	{
		mCenterLoop = true;
		mList.SetCenterOnCallBack(OnCenterFinished);
		while (mCenterLoop)
		{
			mList.CenterOn(mOldEventArrowTarget);
			yield return 0;
		}
		mList.CenterOffEnabled();
		mList.ScrollCustomStop();
		mCenterProc = null;
	}

	private void OnCenterFinished()
	{
		mCenterLoop = false;
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(tUTORIAL_INDEX);
			return;
		}
		mGame.mTutorialManager.DeleteTutorialArrow();
		if (mCenterProc != null)
		{
			StopCoroutine(mCenterProc);
			mCenterProc = null;
		}
		mList.ScrollCustomMove();
		mState.Reset(STATE.MAIN, true);
		mGame.Save();
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.TutorialSkip(index);
			if (mCenterProc != null)
			{
				StopCoroutine(mCenterProc);
				mCenterProc = null;
			}
			mList.ScrollCustomMove();
			mState.Reset(STATE.MAIN, true);
			mGame.Save();
		}
	}
}
