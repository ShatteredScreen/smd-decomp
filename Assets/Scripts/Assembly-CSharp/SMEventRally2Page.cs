using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEventRally2Page : SMMapPage
{
	protected int mEventID;

	protected short mCourseID;

	public int EventID
	{
		get
		{
			return mEventID;
		}
	}

	public short CourseID
	{
		get
		{
			return mCourseID;
		}
	}

	protected override void Update()
	{
		base.Update();
	}

	public IEnumerator Init(Def.SERIES a_series, int a_eventID, short a_courseID, int a_maxLevel, OnLevelPushed a_lvlPushedCB, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadBlockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB, SMMapPageData a_data)
	{
		mPageData = a_data;
		mSeries = a_series;
		mEventID = a_eventID;
		mCourseID = a_courseID;
		if (mSeries != mPageData.Series)
		{
		}
		SMEventPageData eventPageData = mPageData as SMEventPageData;
		mMaxModulePage = mPageData.GetMap(a_courseID).ModuleMaxPageNum;
		mLevelPushedCallback = a_lvlPushedCB;
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		yield return StartCoroutine(LoadRouteData(a_series, mGame.GetMapSize(eventPageData.EventSetting.EventType), a_eventID, a_courseID));
		mMaxPage = NewMapSsDataEntity.GetMapIndex(a_maxLevel) + 1;
		int available = Def.GetStageNo(mPageData.GetMap(a_courseID).AvailableMaxLevel, 0);
		mMaxAvailablePage = NewMapSsDataEntity.GetMapIndex(available) + 1;
		int moduleLevel = Def.GetStageNo(mPageData.GetMap(a_courseID).ModuleMaxLevel, 0);
		mMaxModulePageFromMaxLevel = NewMapSsDataEntity.GetMapIndex(moduleLevel) + 1;
		bool useBg = true;
		if (eventPageData.EventSetting.MapBackName.CompareTo("null") != 0)
		{
			useBg = false;
		}
		mMapGameObject = new GameObject[mMaxModulePage];
		mMapParts = new SMMapPart[mMaxModulePage];
		for (int i = 0; i < mMaxModulePage; i++)
		{
			mMapGameObject[i] = Util.CreateGameObject("MapGO" + (i + 1), base.gameObject);
			mMapParts[i] = mMapGameObject[i].AddComponent<SMEventRally2Part>();
			mMapParts[i].SetGameState(mGameState, this, eventPageData);
			SMEventRally2Part eventPart = mMapParts[i] as SMEventRally2Part;
			eventPart.SetCourseID(a_courseID);
			Vector3 scale;
			if (!NewMapSsDataEntity.MapScaleList.TryGetValue(i, out scale))
			{
				scale = Vector3.one;
			}
			mMapParts[i].Init(mSeries, i, scale, a_accessoryCB, a_roadBlockCB, a_snsCB, a_mapChangeCB, true, useBg);
			Vector3 pos = new Vector3(0f, 0f + 1203f * (float)i, Def.MAP_BASE_Z - 0.1f * (float)i);
			mMapGameObject[i].gameObject.transform.localPosition = pos;
		}
		asyncList.Clear();
		SMMapRoute<float>.RouteNode n = NewMapSsDataEntity.MapRoute.Root;
		SMMapRoute<float>.RouteNode p4 = null;
		bool isFirst2 = true;
		while (n != null)
		{
			if (n.mIsStage)
			{
				int stageNo3 = (int)n.Value;
				stageNo3 = Def.GetStageNo(stageNo3, 0);
				int mapIndex2;
				GameObject go2 = ((!NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(n, out mapIndex2)) ? base.gameObject : mMapGameObject[mapIndex2]);
				SMMapPart part = mMapParts[mapIndex2];
				if (isFirst2)
				{
					isFirst2 = false;
					string animeKey = part.GetButtonAnimeKey(mSeries, mapIndex2);
					asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
					asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_CURRENT", true));
					asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_CLEAR", true));
					for (int j = 0; j < asyncList.Count; j++)
					{
						while (asyncList[j].LoadState != ResourceInstance.LOADSTATE.DONE)
						{
							yield return 0;
						}
					}
				}
				Vector3 pos3 = n.Position;
				pos3.z = Def.MAP_BUTTON_Z;
				EventStageButton button2 = CreateStageButton(stageNo3, pos3, go2);
				mStageButtonList.Add(button2);
				mMapButtons.Add(stageNo3.ToString(), button2.gameObject);
			}
			if (n.NextSub != null)
			{
				for (p4 = n.NextSub; p4 != null; p4 = p4.NextSub)
				{
					if (p4.mIsStage)
					{
						int stageNo = Def.GetStageNoByRouteOrder(p4.Value);
						int mapIndex;
						GameObject go = ((!NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(n, out mapIndex)) ? base.gameObject : mMapGameObject[mapIndex]);
						Vector3 pos2 = p4.Position;
						pos2.z = Def.MAP_BUTTON_Z;
						EventStageButton button = CreateStageButton(stageNo, pos2, go, true);
						mStageButtonList.Add(button);
						mMapButtons.Add(stageNo.ToString(), button.gameObject);
					}
				}
			}
			n = n.NextMain;
		}
		if (NewMapSsDataEntity.MapLineList.Count > 0)
		{
			asyncList.Clear();
			isFirst2 = true;
			n = NewMapSsDataEntity.MapLineRoute.Root;
			p4 = null;
			while (n != null)
			{
				if (n.Name.ToString().Contains("Line"))
				{
					int stageNo6 = (int)n.Value;
					stageNo6 = Def.GetStageNo(stageNo6, 0);
					int mapIndex4;
					GameObject go4 = ((!NewMapSsDataEntity.MapLineRouteMapIndex.TryGetValue(n, out mapIndex4)) ? base.gameObject : mMapGameObject[mapIndex4]);
					SMMapPart part3 = mMapParts[mapIndex4];
					if (part3 != null)
					{
						string animeKey2 = part3.GetLineAnimeKey(mSeries, mapIndex4);
						if (isFirst2)
						{
							isFirst2 = false;
							asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey2 + "_LOCK", true));
							asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey2 + "_LOCK_IDLE", true));
							asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey2 + "_UNLOCK", true));
							asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey2 + "_UNLOCK_IDLE", true));
							asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey2 + "_NOOPEN", true));
							for (int l = 0; l < asyncList.Count; l++)
							{
								while (asyncList[l].LoadState != ResourceInstance.LOADSTATE.DONE)
								{
									yield return 0;
								}
							}
							asyncList.Clear();
						}
						List<SsMapEntity> list2 = NewMapSsDataEntity.GetMapLineListInMapIndex(mapIndex4);
						SsMapEntity e2 = list2.Find((SsMapEntity x) => x.AnimeKey.CompareTo(n.Value.ToString()) == 0);
						if (stageNo6 != 100)
						{
							EventLine button4 = CreateLine(e2.MapOffsetCounter, e2.AnimeKey, e2.Position, e2.Scale, e2.Angle, mapIndex4, go4, animeKey2);
							mLineList.Add(button4);
							mLineDict.Add(button4.StageNo, button4);
						}
					}
				}
				if (n.NextSub != null)
				{
					for (p4 = n.NextSub; p4 != null; p4 = p4.NextSub)
					{
						if (p4.Name.ToString().Contains("Line"))
						{
							int stageNo4 = Def.GetStageNoByRouteOrder(p4.Value);
							int mapIndex3;
							GameObject go3 = ((!NewMapSsDataEntity.MapLineRouteMapIndex.TryGetValue(p4, out mapIndex3)) ? base.gameObject : mMapGameObject[mapIndex3]);
							SMEventRally2Part part2 = go3.GetComponent<SMEventRally2Part>();
							if (part2 != null)
							{
								List<SsMapEntity> list = NewMapSsDataEntity.GetMapLineListInMapIndex(mapIndex3);
								SsMapEntity e = list.Find((SsMapEntity x) => x.AnimeKey.CompareTo(p4.Value.ToString()) == 0);
								EventLine button3 = CreateLine(e.MapOffsetCounter, e.AnimeKey, e.Position, e.Scale, e.Angle, mapIndex3, go3, part2.GetLineAnimeKey(mSeries, mapIndex3));
								mLineList.Add(button3);
								mLineDict.Add(button3.StageNo, button3);
							}
						}
					}
				}
				n = n.NextMain;
			}
		}
		for (int k = 0; k < mMaxModulePage; k++)
		{
			NGUITools.SetActive(mMapGameObject[k].gameObject, false);
		}
		mAvater = Util.CreateGameObject("Avatar", base.gameObject).AddComponent<MapAvater>();
		mAvater.SetCallback(OnAvaterMoveFinished, OnAvatarSave, OnGetMapData);
		mAvater.Init(this);
		yield return 0;
	}

	protected override void UnlockWalkProcess()
	{
		if (mState.IsChanged())
		{
			if (!mIsUnlockAvatarMove)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			if (mAvater != null)
			{
				int a_main;
				int a_sub;
				Def.SplitStageNo(mUnlockStageNo, out a_main, out a_sub);
				mAvater.MoveToStageButton(mUnlockStageNo, a_sub == 0);
				IsAvatorMoving = true;
			}
		}
		if (mAvater == null || !mAvater.IsMoving())
		{
			mAvater.FindPosition();
			if (mPrevButton != null)
			{
				mPrevButton.ChangeMode();
			}
			if (mUnlockButton != null)
			{
				mUnlockButton.ChangeMode();
			}
			mState.Change(STATE.MAIN);
		}
		else if ((Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight) && IsAvatorMoving && mAvater != null)
		{
			Vector3 position = mAvater.Position;
			float x = base.gameObject.transform.localPosition.x;
			float y = base.gameObject.transform.localPosition.y;
			float z = base.gameObject.transform.localPosition.z;
			float num = position.y + y;
			if (num > 10f)
			{
				num = 10f;
			}
			else if (num < -10f)
			{
				num = -10f;
			}
			float num2 = y + num * -1f;
			if (num2 > mGameState.Map_DeviceOffset)
			{
				num2 = mGameState.Map_DeviceOffset;
			}
			base.gameObject.transform.localPosition = new Vector3(x, num2, z);
		}
	}

	public new EventStageButton CreateStageButton(int a_stageno, Vector3 a_pos, GameObject a_parent, bool a_isSub = false)
	{
		SMEventPageData sMEventPageData = mPageData as SMEventPageData;
		BIJImage image = ResourceManager.LoadImage(sMEventPageData.EventSetting.MapAtlasKey).Image;
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stageno, out a_main, out a_sub);
		string text = (a_isSub ? ("stage_" + a_main + "-" + a_sub) : ("stage_" + a_main));
		Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mSeries, EventID, mCourseID, a_stageno);
		byte starNum = 0;
		PlayerClearData _psd;
		if (mGame.mPlayer.GetStageClearData(mSeries, EventID, mCourseID, a_stageno, out _psd))
		{
			starNum = (byte)_psd.GotStars;
		}
		bool a_hasItem = HasStageItem(a_main, a_sub, mCourseID);
		EventStageButton eventStageButton = Util.CreateGameObject(text, a_parent).AddComponent<EventStageButton>();
		eventStageButton.SetCourseID(mCourseID);
		string a_animeKey = string.Format("MAP_{0}_STAGEBUTTON", Def.GetSeriesString(mSeries, EventID));
		int displayNo = -1;
		SMMapStageSetting mapStage = sMEventPageData.GetMapStage(a_main, a_sub, mCourseID);
		if (mapStage != null)
		{
			displayNo = int.Parse(mapStage.DisplayStageNo);
		}
		float scale = eventStageButton.DEFAULT_SCALE;
		if (stageStatus != Player.STAGE_STATUS.UNLOCK && stageStatus != Player.STAGE_STATUS.CLEAR)
		{
			scale = 0.75f;
		}
		eventStageButton.SetUseCurrentAnime(true);
		eventStageButton.Init(a_pos, scale, a_stageno, starNum, stageStatus, image, a_animeKey, a_hasItem, displayNo);
		eventStageButton.SetGameState(mGameState);
		eventStageButton.SetPushedCallback(base.OnStageButtonPushed);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			SMMapStageSetting mapStage2 = mPageData.GetMapStage(a_main, a_sub, mCourseID);
			eventStageButton.SetDebugInfo(mapStage2);
		}
		return eventStageButton;
	}

	public new EventLine CreateLine(int counter, string animeName, Vector3 pos, Vector3 scale, float a_angle, int a_mapIndex, GameObject a_parent, string animeKey)
	{
		SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		BIJImage image = ResourceManager.LoadImage(currentEventPageData.EventSetting.MapAtlasKey).Image;
		EventLine eventLine = Util.CreateGameObject("MapLine" + counter, a_parent).AddComponent<EventLine>();
		eventLine.SetCourseID(mCourseID);
		eventLine.Init(counter, animeName, image, a_mapIndex, pos.x, pos.y, scale, a_angle, animeKey);
		return eventLine;
	}

	public override void DisplayAllAccessory()
	{
		SMEventPageData sMEventPageData = mPageData as SMEventPageData;
		int availableMaxLevel = sMEventPageData.GetMap(mCourseID).AvailableMaxLevel;
		List<int> mapStageNo = sMEventPageData.GetMapStageNo(mCourseID);
		for (int i = 0; i < mapStageNo.Count; i++)
		{
			int num = mapStageNo[i];
			foreach (AccessoryData mMapAccessoryDatum in mGame.mMapAccessoryData)
			{
				if (mMapAccessoryDatum.Series == mGame.mPlayer.Data.CurrentSeries && mMapAccessoryDatum.EventID == EventID && mMapAccessoryDatum.CourseID == CourseID && mMapAccessoryDatum.AccessoryUnlockScene.CompareTo("MAP") == 0)
				{
					int stageNo = Def.GetStageNo(mMapAccessoryDatum.MainStage, mMapAccessoryDatum.SubStage);
					if (num == stageNo)
					{
						NewMapSsDataEntity.UpdateDisplayMapAccessoryDataByLayer(mMapAccessoryDatum.AnimeLayerName, true);
					}
				}
			}
		}
	}

	public override void DisplayEpisodeSilhouette()
	{
		SMEventPageData sMEventPageData = mPageData as SMEventPageData;
		if (sMEventPageData == null)
		{
			return;
		}
		List<int> demoList = sMEventPageData.GetDemoList();
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mSeries, mEventID, out data);
		for (int i = 0; i < demoList.Count; i++)
		{
			int num = demoList[i];
			bool flag = mGame.mPlayer.IsStoryCompleted(num);
			if (flag && data != null && !data.HasShownDemo((short)num))
			{
				flag = false;
			}
			if (!flag)
			{
				NewMapSsDataEntity.UpdateDisplayEpisodeSilhouetteByIndex(num, true);
			}
			else
			{
				NewMapSsDataEntity.UpdateDisplayEpisodeSilhouetteByIndex(num, false);
			}
		}
	}

	public override float UnlockNewStageSub(int a_playable, int a_notice)
	{
		float result = 0f;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mSeries, mEventID, out data);
		PlayerMapData playerMapData = data.CourseData[mCourseID];
		int avatarStagePosition = playerMapData.AvatarStagePosition;
		int a_main;
		int a_sub;
		if (a_playable >= a_notice)
		{
			Def.SplitStageNo(a_notice, out a_main, out a_sub);
			int num = a_notice;
			if (a_playable == a_notice)
			{
				Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mSeries, mEventID, mCourseID, num);
				if (stageStatus == Player.STAGE_STATUS.LOCK && a_main > 1)
				{
					num = Def.GetStageNo(a_main - 1, 0);
				}
			}
			else if (a_playable - avatarStagePosition <= 100)
			{
			}
			List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(num, a_playable, false);
			foreach (SMMapRoute<float>.RouteNode item in stages)
			{
				if (item == null)
				{
					continue;
				}
				int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(item.Value);
				Def.SplitStageNo(stageNoByRouteOrder, out a_main, out a_sub);
				SMRoadBlockSetting roadBlock = mPageData.GetRoadBlock(a_main, a_sub, mCourseID);
				if (roadBlock != null)
				{
					int nextRoadBlockLevel = playerMapData.NextRoadBlockLevel;
					int a_main2;
					int a_sub2;
					Def.SplitStageNo(playerMapData.RoadBlockReachLevel, out a_main2, out a_sub2);
					if (a_playable >= nextRoadBlockLevel && a_main2 == roadBlock.mStageNo)
					{
						if (mGame.mPlayer.StageUnlockList.Count == 0)
						{
							mGame.mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder);
							data.CourseData[CourseID] = playerMapData;
							mGame.mPlayer.Data.SetMapData(EventID, data);
							mAvater.FindPosition();
						}
						break;
					}
				}
				AccessoryData accessoryDataOnMap = mGame.GetAccessoryDataOnMap(EventID, CourseID, a_main, a_sub);
				if (accessoryDataOnMap != null && accessoryDataOnMap.IsStop == 1 && !mGame.mPlayer.IsAccessoryUnlock(accessoryDataOnMap.Index))
				{
					if (mGame.mPlayer.StageUnlockList.Count == 0)
					{
						mGame.mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder);
						data.CourseData[CourseID] = playerMapData;
						mGame.mPlayer.Data.SetMapData(EventID, data);
						mAvater.FindPosition();
					}
					break;
				}
				PlayerClearData _psd;
				if (!mGame.mPlayer.GetStageClearData(mSeries, EventID, CourseID, stageNoByRouteOrder, out _psd))
				{
					continue;
				}
				if (item.NextMain != null)
				{
					SMMapRoute<float>.RouteNode routeNode = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextMain, true, false);
					if (routeNode != null)
					{
						int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode.Value);
						switch (mGame.mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder2))
						{
						case Player.STAGE_STATUS.LOCK:
							if (!mGame.mPlayer.StageUnlockList.Contains(stageNoByRouteOrder2))
							{
								mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder2);
							}
							mGame.mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder2);
							break;
						case Player.STAGE_STATUS.UNLOCK:
						case Player.STAGE_STATUS.CLEAR:
							if (!mGame.mPlayer.AvatarMoveList.Contains(stageNoByRouteOrder2))
							{
								mGame.mPlayer.AvatarMoveList.Add(stageNoByRouteOrder2);
							}
							mGame.mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder2);
							break;
						case Player.STAGE_STATUS.NOOPEN:
							if (mGame.mPlayer.StageOpenList.Contains(stageNoByRouteOrder2))
							{
								if (!mGame.mPlayer.StageUnlockList.Contains(stageNoByRouteOrder2))
								{
									mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder2);
								}
								mGame.mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder2);
							}
							break;
						}
					}
				}
				if (item.NextSub == null)
				{
					continue;
				}
				SMMapRoute<float>.RouteNode routeNode2 = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextSub, false, true);
				if (routeNode2 != null)
				{
					int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode2.Value);
					Player.STAGE_STATUS stageStatus2 = mGame.mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder2);
					if (stageStatus2 == Player.STAGE_STATUS.LOCK && !mGame.mPlayer.StageUnlockList.Contains(stageNoByRouteOrder2))
					{
						mGame.mPlayer.StageUnlockList.Add(stageNoByRouteOrder2);
					}
				}
			}
		}
		int stage;
		foreach (int stageUnlock in mGame.mPlayer.StageUnlockList)
		{
			stage = stageUnlock;
			mGame.mPlayer.SetStageStatus(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, stage, Player.STAGE_STATUS.UNLOCK);
			mGame.mPlayer.AvatarMoveList.RemoveAll((int p) => p < stage);
		}
		Def.SplitStageNo(a_playable, out a_main, out a_sub);
		SMMapRoute<float>.RouteNode routeNode3 = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main, a_sub, a_sub != 0));
		if (routeNode3 != null)
		{
			result = routeNode3.RoutePosition.y;
		}
		return result;
	}

	public override float UnlockNewStage(int a_playable, int a_notice, int a_cleared)
	{
		Player mPlayer = mGame.mPlayer;
		float result = 0f;
		int a_main = -1;
		int a_sub = -1;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[CourseID];
		bool flag = false;
		if (a_cleared > 0)
		{
			Def.SplitStageNo(a_cleared, out a_main, out a_sub);
			AccessoryData accessoryDataOnMap = mGame.GetAccessoryDataOnMap(EventID, CourseID, a_main, a_sub);
			if (accessoryDataOnMap != null && accessoryDataOnMap.RelationID != 0 && mPlayer.IsAccessoryUnlock(accessoryDataOnMap.RelationID))
			{
				mOpenAccessoryLayer = accessoryDataOnMap.AnimeLayerName;
			}
			if (a_sub > 0)
			{
				SMMapRoute<float>.RouteNode routeNode = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main, a_sub, a_sub != 0));
				if (routeNode != null && routeNode.IsSub && routeNode.NextSub != null)
				{
					SMMapRoute<float>.RouteNode routeNode2 = NewMapSsDataEntity.MapRoute.FindContain("Stage", routeNode.NextSub, false, true);
					if (routeNode2 != null)
					{
						int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode2.Value);
						Player.STAGE_STATUS stageStatus = mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder);
						if (stageStatus == Player.STAGE_STATUS.LOCK)
						{
							mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
							flag = true;
						}
					}
				}
			}
			int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
			int roadBlockReachLevel = mGame.mPlayer.RoadBlockReachLevel;
			if (a_cleared == nextRoadBlockLevel && roadBlockReachLevel < nextRoadBlockLevel)
			{
				playerMapData.RoadBlockReachLevel = nextRoadBlockLevel;
				DateTime now = DateTime.Now;
				playerMapData.RoadBlockReachTime = now;
				mOpenRBNo = nextRoadBlockLevel;
			}
		}
		List<int> list = new List<int>();
		int a_main2;
		int a_sub2;
		if (a_playable >= a_notice && !flag)
		{
			Def.SplitStageNo(a_notice, out a_main2, out a_sub2);
			int num = a_notice;
			if (a_playable == a_notice)
			{
				switch (mPlayer.GetStageStatus(mSeries, EventID, CourseID, num))
				{
				case Player.STAGE_STATUS.LOCK:
					if (a_main2 > 1)
					{
						num = Def.GetStageNo(a_main2 - 1, 0);
					}
					break;
				case Player.STAGE_STATUS.UNLOCK:
					num = 100;
					break;
				}
			}
			List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(num, a_playable, false);
			foreach (SMMapRoute<float>.RouteNode item in stages)
			{
				if (item == null)
				{
					continue;
				}
				int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(item.Value);
				Def.SplitStageNo(stageNoByRouteOrder2, out a_main2, out a_sub2);
				PlayerClearData _psd;
				if (!mPlayer.GetStageClearData(mSeries, EventID, CourseID, stageNoByRouteOrder2, out _psd))
				{
					continue;
				}
				list.Add(stageNoByRouteOrder2);
				if (item.NextMain != null)
				{
					SMMapRoute<float>.RouteNode routeNode3 = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextMain, true, false);
					if (routeNode3 != null)
					{
						int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode3.Value);
						switch (mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder))
						{
						case Player.STAGE_STATUS.LOCK:
							if (!mPlayer.StageUnlockList.Contains(stageNoByRouteOrder))
							{
								mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
							}
							mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder);
							break;
						case Player.STAGE_STATUS.UNLOCK:
						case Player.STAGE_STATUS.CLEAR:
							if (!mPlayer.AvatarMoveList.Contains(stageNoByRouteOrder))
							{
								mPlayer.AvatarMoveList.Add(stageNoByRouteOrder);
							}
							mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder);
							break;
						}
					}
				}
				if (item.NextSub == null)
				{
					continue;
				}
				SMMapRoute<float>.RouteNode routeNode4 = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextSub, false, true);
				if (routeNode4 != null)
				{
					int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode4.Value);
					Player.STAGE_STATUS stageStatus2 = mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder);
					if (stageStatus2 == Player.STAGE_STATUS.LOCK && !mPlayer.StageUnlockList.Contains(stageNoByRouteOrder))
					{
						mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
					}
				}
			}
		}
		int stage;
		foreach (int stageUnlock in mPlayer.StageUnlockList)
		{
			stage = stageUnlock;
			mPlayer.AvatarMoveList.RemoveAll((int x) => x < stage);
		}
		mPlayer.LineOpenList.Clear();
		mPlayer.LineUnlockList.Clear();
		if (mLineList.Count > 0)
		{
			if (a_cleared > 0)
			{
				OpenNextLine(a_cleared);
				UnlockNextLine(a_cleared);
			}
			else
			{
				if (list.Count == 0)
				{
					SMEventPageData sMEventPageData = mPageData as SMEventPageData;
					if (sMEventPageData != null)
					{
						List<SMMapStageSetting> mapStages = sMEventPageData.GetMapStages(CourseID);
						for (int i = 0; i < mapStages.Count; i++)
						{
							int stageNo = Def.GetStageNo(mapStages[i].mStageNo, mapStages[i].mSubNo);
							PlayerClearData _psd2;
							if (mPlayer.GetStageClearData(mSeries, EventID, CourseID, stageNo, out _psd2))
							{
								list.Add(stageNo);
							}
						}
					}
				}
				if (list.Count > 0)
				{
					for (int j = 0; j < list.Count; j++)
					{
						OpenNextLine(list[j]);
						UnlockNextLine(list[j]);
					}
				}
			}
		}
		List<int> list2 = new List<int>();
		List<int> list3 = new List<int>();
		mPlayer.LineUnlockList.Sort();
		for (int k = 0; k < mPlayer.StageUnlockList.Count; k++)
		{
			int num2 = mPlayer.StageUnlockList[k];
			Def.SplitStageNo(num2, out a_main2, out a_sub2);
			SMRoadBlockSetting roadBlock = mPageData.GetRoadBlock(a_main2, a_sub2, CourseID);
			if (roadBlock != null)
			{
				SMEventRoadBlockSetting sMEventRoadBlockSetting = roadBlock as SMEventRoadBlockSetting;
				if (sMEventRoadBlockSetting.UnlockAccessoryID() != 0 && !mPlayer.IsAccessoryUnlock(sMEventRoadBlockSetting.UnlockAccessoryID()))
				{
					list2.Add(num2);
				}
			}
		}
		List<SMRoadBlockSetting> roadBlockInCourse = mPageData.GetRoadBlockInCourse(mCourseID);
		for (int l = 0; l < roadBlockInCourse.Count; l++)
		{
			SMEventRoadBlockSetting sMEventRoadBlockSetting2 = roadBlockInCourse[l] as SMEventRoadBlockSetting;
			if (sMEventRoadBlockSetting2.UnlockAccessoryID() != 0 && !mPlayer.IsAccessoryUnlock(sMEventRoadBlockSetting2.UnlockAccessoryID()))
			{
				int stageNo2 = Def.GetStageNo(sMEventRoadBlockSetting2.mStageNo, sMEventRoadBlockSetting2.mSubNo);
				list3.Add(stageNo2);
			}
		}
		foreach (int item2 in list2)
		{
			mPlayer.StageUnlockList.Remove(item2);
		}
		foreach (int item3 in list3)
		{
			mPlayer.LineOpenList.Remove(item3);
			mPlayer.LineUnlockList.Remove(item3);
		}
		if (mPlayer.StageUnlockList.Count == 0)
		{
			mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, a_notice);
		}
		bool flag2 = false;
		foreach (int stageUnlock2 in mPlayer.StageUnlockList)
		{
			if (playerMapData.NextRoadBlockLevel + 100 != stageUnlock2)
			{
				mPlayer.SetStageStatus(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, CourseID, stageUnlock2, Player.STAGE_STATUS.UNLOCK);
			}
			else
			{
				flag2 = true;
			}
		}
		if (flag2)
		{
			mPlayer.StageUnlockList.Clear();
		}
		mPlayer.LineOpenList.Clear();
		if (playerMapData != null)
		{
			data.CourseData[CourseID] = playerMapData;
			mPlayer.Data.SetMapData(EventID, data);
		}
		Def.SplitStageNo(a_playable, out a_main2, out a_sub2);
		SMMapRoute<float>.RouteNode routeNode5 = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main2, a_sub2, a_sub2 != 0));
		if (routeNode5 != null)
		{
			result = routeNode5.RoutePosition.y;
		}
		return result;
	}

	public override void OpenNewStageUntilNextChapter(int a_current, int a_nextChapter)
	{
		List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(a_current, a_nextChapter, true);
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(base.Series, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[CourseID];
		for (int i = 0; i < stages.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stages[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS value;
			if (playerMapData.StageStatus.TryGetValue(stageNoByRouteOrder, out value))
			{
				if (value == Player.STAGE_STATUS.NOOPEN)
				{
					mPlayer.StageOpenList.Add(stageNoByRouteOrder);
				}
			}
			else
			{
				mPlayer.StageOpenList.Add(stageNoByRouteOrder);
			}
		}
	}

	public List<int> GetStagesUntilNextChapter(int a_current, int a_nextChapter)
	{
		List<int> list = new List<int>();
		List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(a_current, a_nextChapter, true);
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(base.Series, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[CourseID];
		for (int i = 0; i < stages.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stages[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS value;
			if (playerMapData.StageStatus.TryGetValue(stageNoByRouteOrder, out value))
			{
				if (value == Player.STAGE_STATUS.NOOPEN)
				{
					list.Add(stageNoByRouteOrder);
				}
			}
			else
			{
				list.Add(stageNoByRouteOrder);
			}
		}
		return list;
	}

	public override void OpenNextLine(int a_cleared)
	{
		if (a_cleared < 0)
		{
			return;
		}
		Player mPlayer = mGame.mPlayer;
		int a_main;
		int a_sub;
		if (a_cleared > Def.MaxStageNo)
		{
			short a_course;
			Def.SplitEventStageNo(a_cleared, out a_course, out a_main, out a_sub);
		}
		else
		{
			Def.SplitStageNo(a_cleared, out a_main, out a_sub);
		}
		SMMapRoute<float>.RouteNode line = NewMapSsDataEntity.GetLine(a_cleared, a_sub != 0);
		SMMapRoute<float>.RouteNode routeNode = null;
		if (line == null)
		{
			return;
		}
		if (line.IsSub)
		{
			if (line.NextSub == null)
			{
				return;
			}
			routeNode = line.NextSub;
			if (routeNode.NextSub != null)
			{
				SMMapRoute<float>.RouteNode nextSub = routeNode.NextSub;
				int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(nextSub.Value);
				if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder) == Player.STAGE_STATUS.NOOPEN)
				{
					mGame.mPlayer.LineOpenList.Add(stageNoByRouteOrder);
				}
			}
			return;
		}
		if (line.NextMain != null)
		{
			routeNode = line.NextMain;
			if (routeNode.NextMain != null)
			{
				SMMapRoute<float>.RouteNode nextMain = routeNode.NextMain;
				int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(nextMain.Value);
				if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder2) == Player.STAGE_STATUS.NOOPEN)
				{
					mGame.mPlayer.LineOpenList.Add(Def.GetStageNoByRouteOrder(nextMain.Value));
				}
			}
			if (routeNode.NextSub != null)
			{
				SMMapRoute<float>.RouteNode nextSub2 = routeNode.NextSub;
				int stageNoByRouteOrder3 = Def.GetStageNoByRouteOrder(nextSub2.Value);
				if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder3) == Player.STAGE_STATUS.NOOPEN)
				{
					mGame.mPlayer.LineOpenList.Add(Def.GetStageNoByRouteOrder(nextSub2.Value));
				}
			}
		}
		if (line.NextSub == null)
		{
			return;
		}
		routeNode = line.NextSub;
		if (routeNode.NextSub != null)
		{
			SMMapRoute<float>.RouteNode nextSub3 = routeNode.NextSub;
			int stageNoByRouteOrder4 = Def.GetStageNoByRouteOrder(nextSub3.Value);
			if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder4) == Player.STAGE_STATUS.NOOPEN)
			{
				mGame.mPlayer.LineOpenList.Add(Def.GetStageNoByRouteOrder(nextSub3.Value));
			}
		}
	}

	public override void UnlockNextLine(int a_cleared)
	{
		if (a_cleared < 0)
		{
			return;
		}
		Player mPlayer = mGame.mPlayer;
		int a_main;
		int a_sub;
		if (a_cleared > Def.MaxStageNo)
		{
			short a_course;
			Def.SplitEventStageNo(a_cleared, out a_course, out a_main, out a_sub);
		}
		else
		{
			Def.SplitStageNo(a_cleared, out a_main, out a_sub);
		}
		SMMapRoute<float>.RouteNode line = NewMapSsDataEntity.GetLine(a_cleared, a_sub != 0);
		SMMapRoute<float>.RouteNode routeNode = null;
		if (line == null)
		{
			return;
		}
		if (line.IsSub)
		{
			if (line.NextSub != null)
			{
				routeNode = line.NextSub;
				int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
				Player.STAGE_STATUS lineStatus = mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder);
				if (lineStatus == Player.STAGE_STATUS.LOCK)
				{
					mGame.mPlayer.LineUnlockList.Add(Def.GetStageNoByRouteOrder(routeNode.Value));
				}
			}
			return;
		}
		if (line.NextMain != null)
		{
			routeNode = line.NextMain;
			int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS lineStatus2 = mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder2);
			if (lineStatus2 == Player.STAGE_STATUS.LOCK)
			{
				mGame.mPlayer.LineUnlockList.Add(Def.GetStageNoByRouteOrder(routeNode.Value));
			}
		}
		if (line.NextSub != null)
		{
			routeNode = line.NextSub;
			int stageNoByRouteOrder3 = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS lineStatus3 = mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder3);
			if (lineStatus3 == Player.STAGE_STATUS.LOCK)
			{
				mGame.mPlayer.LineUnlockList.Add(Def.GetStageNoByRouteOrder(routeNode.Value));
			}
		}
	}

	public EventRoadBlock GetEventRoadBlock(int a_stage)
	{
		EventRoadBlock eventRoadBlock = null;
		for (int i = 0; i < mMapParts.Length; i++)
		{
			SMEventRally2Part sMEventRally2Part = mMapParts[i] as SMEventRally2Part;
			eventRoadBlock = sMEventRally2Part.GetEventRoadBlock(a_stage);
			if (eventRoadBlock != null)
			{
				break;
			}
		}
		return eventRoadBlock;
	}

	public void StartEventRoadBlockUnlockEffect(int a_roadStage)
	{
		mUnlockRBNo = a_roadStage;
		mState.Change(STATE.ROADBLOCK_UNLOCK);
	}

	protected override void RoadBlockUnlockProcess()
	{
		if (mState.IsChanged())
		{
			UnlockRoadBlock(mUnlockRBNo);
			mStateTime = 0f;
			mUnlockRBNo = 0;
			mGame.PlaySe("SE_RANKUP2", -1);
		}
		mStateTime += Time.deltaTime * 120f;
		if (mStateTime > 180f)
		{
			mStateTime = 180f;
			mState.Change(STATE.MAIN);
		}
	}

	public override void OnAvatarSave(string a_nodeName, int a_stageno, float a_pos)
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[mCourseID];
		playerMapData.AvatarNodeName = a_nodeName;
		playerMapData.AvatarStagePosition = a_stageno;
		playerMapData.AvatarPosition = a_pos;
		data.CourseData[CourseID] = playerMapData;
		mGame.mPlayer.Data.SetMapData(EventID, data);
		mGame.Save();
	}

	public override PlayerMapData OnGetMapData()
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		if (data == null)
		{
			return null;
		}
		return data.CourseData[mCourseID];
	}

	public override void StartNewAreaOpenEffect(int a_newarea)
	{
		mNewAreaNo = a_newarea;
		mState.Change(STATE.NEWAREA_OPEN);
	}

	public override void OpenNewArea(int a_page)
	{
		if (mMapParts.Length > a_page)
		{
			for (int i = 0; i < a_page; i++)
			{
				mMapParts[i].OpenNewAreaHide();
			}
			mMapParts[a_page].OpenNewArea();
		}
	}
}
