public interface IReceiptData
{
	int IAPNo { get; }

	string Payload { get; }

	string ReceiptSignature { get; }

	string ReceiptDataString { get; }

	byte[] GetPurchase();

	byte[] GetReceipt();
}
