using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class BIJSNSInstagram : BIJSNSAppSwitcher
{
	public class BIJSNSInstagramCallback : BIJSNSEventCallback
	{
		public BIJSNSInstagramCallback(BIJSNS _sns, int seqno)
			: base(_sns, seqno)
		{
		}

		public override void Register()
		{
			InstagramIF.previewSucceededEvent = (Action<string>)Delegate.Combine(InstagramIF.previewSucceededEvent, new Action<string>(OnPreviewSucceed));
			InstagramIF.previewFailedEvent = (Action<string>)Delegate.Combine(InstagramIF.previewFailedEvent, new Action<string>(OnPreviewFailed));
			base.Register();
		}

		public override void Unregister()
		{
			InstagramIF.previewSucceededEvent = (Action<string>)Delegate.Remove(InstagramIF.previewSucceededEvent, new Action<string>(OnPreviewSucceed));
			InstagramIF.previewFailedEvent = (Action<string>)Delegate.Remove(InstagramIF.previewFailedEvent, new Action<string>(OnPreviewFailed));
			base.Unregister();
		}

		protected virtual void OnPreviewSucceed(string result)
		{
			Parent.ReplyEvent(SeqNo, new Response(0, string.Empty, result));
		}

		protected virtual void OnPreviewFailed(string error)
		{
			Parent.ReplyEvent(SeqNo, new Response(1, string.Empty, error));
		}
	}

	private const float ATTACHEMENT_RESIZE_SCALE = 0.5f;

	public override SNS Kind
	{
		get
		{
			return SNS.Instagram;
		}
	}

	public override bool IsEnabled()
	{
		return true;
	}

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		if (type == 9)
		{
			return true;
		}
		return false;
	}

	protected override bool IsEnableSwitching()
	{
		return true;
	}

	public override void OnSuspned()
	{
		if (IsEnableSwitching())
		{
			BIJSNS.dbg("@@@ SNS @@@ " + ToString() + " SUSPEND --- Instagram :: " + InstagramPlugin.PluginIF.isCanceled());
			InstagramPlugin.PluginIF.onPause();
			base.OnSuspned();
		}
	}

	public override void OnResume()
	{
		if (IsEnableSwitching())
		{
			BIJSNS.dbg("@@@ SNS @@@ " + ToString() + " RESUME --- Instagram :: " + InstagramPlugin.PluginIF.isCanceled());
			InstagramPlugin.PluginIF.onResume();
			if (InstagramPlugin.PluginIF.isCanceled())
			{
				BIJSNS.dbg("@@@ SNS @@@ " + ToString() + " RESUME MANUALLY");
				base.OnResume();
			}
			else
			{
				spawn = false;
			}
		}
	}

	protected override Response OnBackToGameResult(Request req)
	{
		return base.OnBackToGameResult(req);
	}

	public override bool ReplyEvent(int seqno, Response res)
	{
		bool flag = base.ReplyEvent(seqno, res);
		if (flag)
		{
			SetSpawn(0);
		}
		return flag;
	}

	public override IEventCallback CreateEventCallback(int seqno)
	{
		return new BIJSNSInstagramCallback(this, seqno);
	}

	private IEnumerator BIJSNSInstagram_CommonProcess_Coroutine(int seqno, IBIJSNSPostData _data, Handler handler, object userdata, float scale)
	{
		yield return new WaitForSeconds(0.5f);
		string title = BIJSNS.ToSS(_data.Title);
		string message = BIJSNS.ToSS(_data.Message);
		string imageurl = BIJSNS.ToSS(_data.ImageURL);
		string link = BIJSNS.ToSS(_data.LinkURL);
		string filename = ((!string.IsNullOrEmpty(_data.FileName)) ? _data.FileName : "attachment.png");
		BIJSNS.dbg("@@@ SNS @@@ " + message);
		bool needDownload2 = false;
		string url = imageurl;
		if (string.IsNullOrEmpty(url))
		{
			needDownload2 = false;
		}
		else
		{
			if (!url.Contains("://"))
			{
				url = "file://" + url;
			}
			needDownload2 = ((!url.StartsWith("file:")) ? true : false);
		}
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSInstagram.CommonProcess ::: attachment=" + url + " scale=" + scale.ToString("0.000") + " download=" + needDownload2);
		if (needDownload2)
		{
			string localPath = Path.Combine(Application.temporaryCachePath, filename);
			BIJSNS.dbg("@@@ SNS @@@ BIJSNSInstagram FILE: " + localPath);
			if (File.Exists(localPath))
			{
				File.Delete(localPath);
			}
			WWW www = new WWW(url);
			yield return www;
			Texture2D tex = www.texture;
			if (!IsValidTexture(ref tex))
			{
				url = string.Empty;
			}
			else
			{
				if (scale > 0f)
				{
					BIJTextureScale.Bilinear(newWidth: Mathf.FloorToInt((float)tex.width * scale), newHeight: Mathf.FloorToInt((float)tex.height * scale), tex: tex);
				}
				File.WriteAllBytes(localPath, tex.EncodeToPNG());
				url = ("file://" + localPath).Replace('\\', '/');
			}
		}
		bool ret = InstagramPlugin.PluginIF.preview(title, message, link, url, string.Empty);
		BIJSNS.log("BIJSNSInstagram.preview: " + ret);
		if (!ret)
		{
			if (base.CurrentSeqNo == seqno)
			{
				base.CurrentRes = new Response(1, "Instagram did not invoked", null);
				ChangeStep(STEP.COMM_RESULT);
			}
			SetSpawn(0);
		}
	}

	private void DoPostMessage(OP_TYPE type, IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSInstagram.DoPostMessage: " + type);
		if (spawn)
		{
			CallCallback(handler, new Response(1, "Instagram already invoked.", null), userdata);
			return;
		}
		int num = PushReq((int)type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			base.surrogateMonoBehaviour.StartCoroutine(BIJSNSInstagram_CommonProcess_Coroutine(num, postdata, handler, userdata, 0.5f));
		}
		else
		{
			SetSpawn(0);
			CallCallback(handler, new Response(1, "Instagram not requested", null), userdata);
		}
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		BIJSNS.dbg("@@@ SNS @@@ BIJSNSInstagram.UploadImage");
		DoPostMessage(OP_TYPE.UPLOADIMAGE, postdata, handler, userdata);
	}
}
