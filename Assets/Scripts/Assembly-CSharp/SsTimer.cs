using UnityEngine;

public class SsTimer
{
	private static float _startTime;

	public static void StartTimer()
	{
		_startTime = Time.realtimeSinceStartup;
	}

	public static void EndTimer(string label)
	{
		UnityEngine.Debug.Log(label + ": " + ((Time.realtimeSinceStartup - _startTime) * 1000f).ToString("F4") + "msec");
	}

	public static float ElapsedTime()
	{
		return (Time.realtimeSinceStartup - _startTime) * 1000f;
	}
}
