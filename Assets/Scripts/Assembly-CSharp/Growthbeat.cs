using UnityEngine;

public class Growthbeat
{
	private static Growthbeat instance = new Growthbeat();

	private static AndroidJavaObject growthbeat;

	public Growthbeat()
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.growthbeat.Growthbeat"))
		{
			growthbeat = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]);
		}
	}

	public static Growthbeat GetInstance()
	{
		return instance;
	}

	public void SetLoggerSilent(bool silent)
	{
		if (growthbeat != null)
		{
			growthbeat.Call("setLoggerSilent", silent);
		}
	}

	public void setBaseUrl(string baseUrl)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.growthbeat.GrowthbeatCore"))
		{
			AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]);
			AndroidJavaObject androidJavaObject2 = androidJavaObject.Call<AndroidJavaObject>("getHttpClient", new object[0]);
			androidJavaObject2.Call("setBaseUrl", baseUrl);
		}
	}
}
