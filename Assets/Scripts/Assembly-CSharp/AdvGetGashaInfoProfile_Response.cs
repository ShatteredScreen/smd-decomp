using System;
using System.Collections.Generic;

public class AdvGetGashaInfoProfile_Response : ICloneable
{
	[MiniJSONAlias("gasha_groups")]
	public List<AdvGashaInfo> GashaInfoList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGetGashaInfoProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGetGashaInfoProfile_Response;
	}
}
