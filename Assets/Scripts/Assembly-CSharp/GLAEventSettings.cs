using System;

public class GLAEventSettings : ICloneable
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("event_id")]
	public short EventID { get; set; }

	[MiniJSONAlias("help_url")]
	public string HelpURL { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public GLAEventSettings Clone()
	{
		return MemberwiseClone() as GLAEventSettings;
	}
}
