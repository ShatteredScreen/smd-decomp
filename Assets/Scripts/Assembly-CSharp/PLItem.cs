public class PLItem : ListItem
{
	public enum KIND
	{
		DUMMY = 0,
		SD = 1
	}

	public KIND kind = KIND.SD;

	public PItem pitem;

	public int Repeat;
}
