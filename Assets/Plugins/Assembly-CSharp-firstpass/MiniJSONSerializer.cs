using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class MiniJSONSerializer
{
	private const int MAXT_CALL_DEPTH = 32;

	private int mCallDepth;

	private Dictionary<Type, IMiniJSONConverter> mMapper;

	public MiniJSONSerializer(Dictionary<Type, IMiniJSONConverter> mapper = null)
	{
		mMapper = mapper;
	}

	private static void log(string format, params object[] args)
	{
		try
		{
			UnityEngine.Debug.Log(string.Format(format, args));
		}
		catch (Exception)
		{
		}
	}

	private static void dbg(string format, params object[] args)
	{
		try
		{
			UnityEngine.Debug.Log(string.Format(format, args));
		}
		catch (Exception)
		{
		}
	}

	private IMiniJSONConverter GetConverter(Type type)
	{
		IMiniJSONConverter value = null;
		if (mMapper != null && mMapper.TryGetValue(type, out value))
		{
			return value;
		}
		DefaultMiniJSONConverter defaultMiniJSONConverter = new DefaultMiniJSONConverter(type);
		if (defaultMiniJSONConverter.IsHandle)
		{
			return defaultMiniJSONConverter;
		}
		return null;
	}

	private static T GetAttribute<T>(Type objType) where T : Attribute
	{
		T[] array = (T[])objType.GetCustomAttributes(typeof(T), false);
		return (array == null || array.Length <= 0) ? ((T)null) : array[0];
	}

	private static T GetFieldAttribute<T>(Type objType, string fieldName) where T : Attribute
	{
		T[] array = (T[])objType.GetProperty(fieldName).GetCustomAttributes(typeof(T), false);
		return (array == null || array.Length <= 0) ? ((T)null) : array[0];
	}

	private static void GetArrayEntryType(Type type, MiniJSONArrayAttribute array_attr, out Type entryType)
	{
		entryType = typeof(object);
		try
		{
			if (array_attr != null)
			{
				entryType = array_attr.EntryType;
			}
			else if (type.IsArray || typeof(IList).IsAssignableFrom(type))
			{
				if (type.IsGenericType)
				{
					entryType = type.GetProperty("Item").PropertyType;
				}
				else
				{
					entryType = typeof(object);
				}
			}
			else
			{
				entryType = typeof(object);
			}
		}
		catch (Exception)
		{
			entryType = typeof(object);
		}
	}

	private static void GetDictionaryEntryType(Type type, MiniJSONDictionaryAttribute dict_attr, out Type keyType, out Type valueType)
	{
		keyType = typeof(object);
		valueType = typeof(object);
		try
		{
			if (dict_attr != null)
			{
				keyType = dict_attr.KeyType;
				valueType = dict_attr.ValueType;
			}
		}
		catch (Exception)
		{
			keyType = typeof(object);
			valueType = typeof(object);
		}
	}

	private static MethodInfo GetGenericeMethod(string methodName, Type[] paramTypes)
	{
		Type typeFromHandle = typeof(MiniJSONSerializer);
		BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.NonPublic;
		MethodInfo[] methods = typeFromHandle.GetMethods(bindingAttr);
		foreach (MethodInfo methodInfo in methods)
		{
			if (methodInfo.IsGenericMethod && methodInfo.IsGenericMethodDefinition && methodInfo.ContainsGenericParameters && methodInfo.Name == methodName)
			{
				return methodInfo.MakeGenericMethod(paramTypes);
			}
		}
		return null;
	}

	private void FromJsonObject<T>(ref T obj, object json) where T : class, new()
	{
		mCallDepth++;
		Type typeFromHandle = typeof(T);
		MiniJSONArrayAttribute attribute = GetAttribute<MiniJSONArrayAttribute>(typeFromHandle);
		if (attribute != null || (obj is IList && json is IList))
		{
			IList list = json as IList;
			Type entryType;
			GetArrayEntryType(typeFromHandle, attribute, out entryType);
			IMiniJSONConverter converter = GetConverter(entryType);
			MethodInfo genericeMethod = GetGenericeMethod("FromJsonObject", new Type[1] { entryType });
			for (int i = 0; i < list.Count; i++)
			{
				object obj2 = list[i];
				object obj3;
				if (entryType == typeof(string))
				{
					string text = obj2 as string;
					obj3 = ((text == null) ? string.Empty : new string(text.ToCharArray()));
				}
				else
				{
					obj3 = Activator.CreateInstance(entryType);
				}
				object obj4 = obj2;
				if (converter != null)
				{
					obj4 = converter.ConvertFrom(obj2);
				}
				if (entryType.IsPrimitive)
				{
					obj3 = obj4;
				}
				else
				{
					genericeMethod.Invoke(this, new object[2] { obj3, obj4 });
				}
				((IList)obj).Add(obj3);
			}
		}
		else if (json is IDictionary)
		{
			IDictionary dictionary = json as IDictionary;
			PropertyInfo[] properties = typeFromHandle.GetProperties();
			PropertyInfo[] array = properties;
			foreach (PropertyInfo propertyInfo in array)
			{
				Type propertyType = propertyInfo.PropertyType;
				string key = propertyInfo.Name;
				if (!propertyInfo.CanWrite)
				{
					continue;
				}
				MiniJSONAliasAttribute fieldAttribute = GetFieldAttribute<MiniJSONAliasAttribute>(typeFromHandle, propertyInfo.Name);
				if (fieldAttribute != null && !string.IsNullOrEmpty(fieldAttribute.Alias))
				{
					key = fieldAttribute.Alias;
				}
				if (!dictionary.Contains(key))
				{
					continue;
				}
				object obj5 = dictionary[key];
				IMiniJSONConverter converter2 = GetConverter(propertyType);
				if (converter2 != null)
				{
					propertyInfo.SetValue(obj, converter2.ConvertFrom(obj5), null);
					continue;
				}
				attribute = GetFieldAttribute<MiniJSONArrayAttribute>(typeFromHandle, propertyInfo.Name);
				MiniJSONDictionaryAttribute fieldAttribute2 = GetFieldAttribute<MiniJSONDictionaryAttribute>(typeFromHandle, propertyInfo.Name);
				if (attribute != null || obj5 is IList)
				{
					object obj6;
					if (propertyType == typeof(string))
					{
						string text2 = obj5 as string;
						obj6 = ((text2 == null) ? string.Empty : new string(text2.ToCharArray()));
					}
					else
					{
						obj6 = Activator.CreateInstance(propertyType);
					}
					Type entryType2;
					GetArrayEntryType(propertyType, attribute, out entryType2);
					IList list2 = (IList)obj5;
					if (list2.Count > 0)
					{
						IMiniJSONConverter converter3 = GetConverter(entryType2);
						MethodInfo genericeMethod2 = GetGenericeMethod("FromJsonObject", new Type[1] { entryType2 });
						for (int k = 0; k < list2.Count; k++)
						{
							object obj7 = list2[k];
							object obj8;
							if (entryType2 == typeof(string))
							{
								string text3 = obj7 as string;
								obj8 = ((text3 == null) ? string.Empty : new string(text3.ToCharArray()));
							}
							else
							{
								obj8 = Activator.CreateInstance(entryType2);
							}
							object obj9 = obj7;
							if (converter3 != null)
							{
								obj9 = converter3.ConvertFrom(obj7);
							}
							if (entryType2.IsPrimitive)
							{
								obj8 = obj9;
							}
							else
							{
								genericeMethod2.Invoke(this, new object[2] { obj8, obj9 });
							}
							((IList)obj6).Add(obj8);
						}
					}
					propertyInfo.SetValue(obj, obj6, null);
				}
				else if (obj5 is IDictionary)
				{
					object obj10 = Activator.CreateInstance(propertyType);
					if (fieldAttribute2 != null)
					{
						Type keyType;
						Type valueType;
						GetDictionaryEntryType(propertyType, fieldAttribute2, out keyType, out valueType);
						IDictionary dictionary2 = (IDictionary)obj5;
						if (dictionary2.Count > 0)
						{
							IMiniJSONConverter converter4 = GetConverter(keyType);
							IMiniJSONConverter converter5 = GetConverter(valueType);
							MethodInfo genericeMethod3 = GetGenericeMethod("FromJsonObject", new Type[1] { keyType });
							MethodInfo genericeMethod4 = GetGenericeMethod("FromJsonObject", new Type[1] { valueType });
							foreach (DictionaryEntry item in dictionary2)
							{
								object key2 = item.Key;
								object value = item.Value;
								object obj11;
								if (keyType == typeof(string))
								{
									string text4 = key2 as string;
									obj11 = ((text4 == null) ? string.Empty : new string(text4.ToCharArray()));
								}
								else
								{
									obj11 = Activator.CreateInstance(keyType);
								}
								object obj12;
								if (valueType == typeof(string))
								{
									string text5 = value as string;
									obj12 = ((text5 == null) ? string.Empty : new string(text5.ToCharArray()));
								}
								else
								{
									obj12 = Activator.CreateInstance(valueType);
								}
								object obj13 = key2;
								object obj14 = value;
								if (converter4 != null)
								{
									obj13 = converter4.ConvertFrom(key2);
								}
								if (converter5 != null)
								{
									obj14 = converter5.ConvertFrom(value);
								}
								if (keyType.IsPrimitive)
								{
									obj11 = obj13;
								}
								else if (obj13 is Enum)
								{
									obj11 = obj13;
								}
								else
								{
									genericeMethod3.Invoke(this, new object[2] { obj11, obj13 });
								}
								if (valueType.IsPrimitive)
								{
									obj12 = obj14;
								}
								else if (obj14 is Enum)
								{
									obj12 = obj14;
								}
								else
								{
									genericeMethod4.Invoke(this, new object[2] { obj12, obj14 });
								}
								((IDictionary)obj10).Add(obj11, obj12);
							}
						}
					}
					else
					{
						GetGenericeMethod("FromJsonObject", new Type[1] { propertyType }).Invoke(this, new object[2] { obj10, obj5 });
					}
					propertyInfo.SetValue(obj, obj10, null);
				}
				else
				{
					propertyInfo.SetValue(obj, obj5, null);
				}
			}
		}
		else
		{
			obj = json as T;
		}
		mCallDepth--;
	}

	public void Populate<T>(ref T obj, string json) where T : class, new()
	{
		FromJsonObject(ref obj, MiniJSON.jsonDecode(json));
	}

	public void Populate<T>(ref T obj, object jsonObject) where T : class, new()
	{
		mCallDepth = 0;
		FromJsonObject(ref obj, jsonObject);
	}

	public T Deserialize<T>(string json) where T : class, new()
	{
		T obj = new T();
		Populate(ref obj, json);
		return obj;
	}

	private IDictionary<string, object> ToJsonObject(object obj)
	{
		mCallDepth++;
		if (obj == null)
		{
			mCallDepth--;
			return null;
		}
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		Type type = obj.GetType();
		PropertyInfo[] properties = type.GetProperties();
		PropertyInfo[] array = properties;
		foreach (PropertyInfo propertyInfo in array)
		{
			Type propertyType = propertyInfo.PropertyType;
			object value = propertyInfo.GetValue(obj, null);
			string key = propertyInfo.Name;
			if (!propertyInfo.CanWrite)
			{
				continue;
			}
			MiniJSONAliasAttribute fieldAttribute = GetFieldAttribute<MiniJSONAliasAttribute>(type, propertyInfo.Name);
			if (fieldAttribute != null && !string.IsNullOrEmpty(fieldAttribute.Alias))
			{
				key = fieldAttribute.Alias;
			}
			IMiniJSONConverter converter = GetConverter(propertyType);
			if (converter != null)
			{
				dictionary[key] = converter.ConvertTo(value);
			}
			else if (MiniJSON.isHandledValue(value))
			{
				MiniJSONArrayAttribute fieldAttribute2 = GetFieldAttribute<MiniJSONArrayAttribute>(type, propertyInfo.Name);
				if (fieldAttribute2 != null || value is IList)
				{
					IList<object> list = new List<object>();
					foreach (object item in (IList)value)
					{
						string text = item as string;
						if (text != null)
						{
							list.Add(new string(text.ToCharArray()));
						}
						else if (item.GetType().IsPrimitive)
						{
							list.Add(item);
						}
						else
						{
							list.Add(ToJsonObject(item));
						}
					}
					dictionary[key] = list;
				}
				else
				{
					dictionary[key] = value;
				}
			}
			else
			{
				dictionary[key] = ToJsonObject(value);
			}
		}
		mCallDepth--;
		return dictionary;
	}

	public string Serialize<T>(T obj)
	{
		mCallDepth = 0;
		return MiniJSON.jsonEncode(ToJsonObject(obj));
	}
}
