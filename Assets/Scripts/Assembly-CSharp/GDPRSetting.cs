using System.Collections.Generic;

public class GDPRSetting
{
	[MiniJSONAlias("optout")]
	public string mOptOutURL { get; set; }

	[MiniJSONAlias("summary")]
	public string mSummary { get; set; }

	[MiniJSONAlias("summaryurl")]
	public string mSummaryURL { get; set; }

	[MiniJSONAlias("tokena")]
	public string mTokenAD { get; set; }

	[MiniJSONAlias("tokeni")]
	public string mTokenIOS { get; set; }

	[MiniJSONAlias("list")]
	public List<GDPRInfo> mList { get; set; }

	public GDPRSetting()
	{
		mOptOutURL = string.Empty;
		mSummary = string.Empty;
		mSummaryURL = string.Empty;
		mTokenAD = string.Empty;
		mTokenIOS = string.Empty;
		mList = new List<GDPRInfo>();
	}
}
