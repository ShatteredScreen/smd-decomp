using System;

public class AdvGashaInfoManifest : ICloneable
{
	[MiniJSONAlias("linkbanner")]
	public LinkBannerSetting linkBnSetting { get; set; }

	[MiniJSONAlias("sort")]
	public int sort { get; set; }

	[MiniJSONAlias("figurelist_url")]
	public string figurelist_url { get; set; }

	[MiniJSONAlias("caution_url")]
	public string caution_url { get; set; }

	[MiniJSONAlias("cpid")]
	public int cpid { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGashaInfoManifest Clone()
	{
		return MemberwiseClone() as AdvGashaInfoManifest;
	}
}
