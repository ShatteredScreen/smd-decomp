using UnityEngine;

public class BuyHeartDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		BUY_COMPLETE = 0,
		CANCEL = 1,
		BUY_DOLLAR = 2
	}

	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		MAINTENANCE_CHECK_START = 3,
		MAINTENANCE_CHECK_WAIT = 4,
		NETWORK_00 = 5,
		NETWORK_00_1 = 6,
		NETWORK_00_2 = 7,
		NETWORK_01 = 8,
		NETWORK_01_1 = 9,
		NETWORK_01_2 = 10,
		MAINTENANCE_CHECK_START02 = 11,
		MAINTENANCE_CHECK_WAIT02 = 12,
		NETWORK_02 = 13,
		NETWORK_02_1 = 14,
		NETWORK_02_2 = 15,
		AUTHERROR_WAIT = 16
	}

	public enum BUYHEART_PLACE
	{
		MAP = 0,
		PUZZLE_REPLAY = 1,
		PUZZLE_START = 2,
		BINGO_RELOTTERY = 3
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAINTENANCE_CHECK_START);

	public STATE StateStatus;

	private STATE mStateAfterNetworkError = STATE.MAIN;

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private ShopItemData mBoughtItem;

	private int mPrice;

	private int mSetNum;

	private bool mBuyEnable;

	private UILabel mDesc;

	private JellyImageButton mBuyButton;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mSNSCallCount;

	private ConfirmImageDialog mConfirmImageDialog;

	private ConfirmDialog mErrorDialog;

	private UILabel mUIlabel;

	public int Place { get; set; }

	public override void Start()
	{
		mPrice = mGame.mShopItemData[mGame.mSegmentProfile.HeartItem].GemDefaultAmount;
		mSetNum = mGame.mShopItemData[mGame.mSegmentProfile.HeartItem].DefaultNum;
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		if (mBuyEnable && mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) >= mGame.mPlayer.Data.MaxNormalLifeCount)
		{
			mBuyEnable = false;
			Util.SetLabelText(mDesc, Localization.Get("BuyHeart_FullDesc02"));
			mBuyButton.SetButtonEnable(false);
		}
		StateStatus = mState.GetStatus();
		switch (StateStatus)
		{
		case STATE.INIT:
			CreateGemWindow();
			mState.Change(STATE.MAIN);
			break;
		case STATE.MAINTENANCE_CHECK_START:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT:
			if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
			{
				mState.Change(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00:
			if (!mGame.Network_UserAuth())
			{
				if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_00);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		case STATE.NETWORK_00_1:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00_2);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
				break;
			}
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			if (mUserAuthErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 5, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 5, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_00_2:
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			if (!mGame.Network_GetGemCount())
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.INIT);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_01_1);
			}
			break;
		case STATE.NETWORK_01_1:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_01_2);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			break;
		case STATE.NETWORK_01_2:
			mState.Change(STATE.INIT);
			break;
		case STATE.MAINTENANCE_CHECK_START02:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.CLOSE);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT02);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT02:
			switch (MaintenanceCheckResult())
			{
			case MAINTENANCE_RESULT.OK:
				mState.Change(STATE.NETWORK_02);
				break;
			case MAINTENANCE_RESULT.ERROR:
			case MAINTENANCE_RESULT.IN_MAINTENANCE:
				if (!GetBusy())
				{
					mState.Change(STATE.MAIN);
				}
				break;
			}
			break;
		case STATE.NETWORK_02:
		{
			bool flag = mGame.Network_BuyItem(mBoughtItem.Index);
			mGame.ConnectRetryFlg = false;
			if (!flag)
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_1);
			}
			break;
		}
		case STATE.NETWORK_02_1:
		{
			if (!GameMain.mBuyItemCheckDone)
			{
				break;
			}
			if (GameMain.mBuyItemSucceed)
			{
				mState.Change(STATE.NETWORK_02_2);
				break;
			}
			Server.ErrorCode mUserAuthErrorCode = GameMain.mBuyItemErrorCode;
			if (mUserAuthErrorCode == Server.ErrorCode.DUPLICATE_DATA)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mState.Change(STATE.WAIT);
			}
			else
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			break;
		}
		case STATE.NETWORK_02_2:
		{
			mGame.mPlayer.BoughtItem(mBoughtItem.ItemDetail);
			mBoughtItem = null;
			string empty = string.Empty;
			empty = Localization.Get("BuyHeart_Title0");
			mConfirmImageDialog = Util.CreateGameObject("Complete", base.ParentGameObject).AddComponent<ConfirmImageDialog>();
			mConfirmImageDialog.Init(empty, Localization.Get("BuyHeart_RecoveryDesc"), string.Empty, "icon_heart", ConfirmImageDialog.CONFIRM_IMAGE_DIALOG_STYLE.OK_ONLY);
			mConfirmImageDialog.SetClosedCallback(OnCompleteDialogClosed);
			int cur_series = (int)mGame.mPlayer.Data.LastPlaySeries;
			int num = mGame.mPlayer.Data.LastPlayLevel;
			if (mGame.IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
			{
				int num2 = ((mGame.mDebugDailyChallengeID <= 0) ? GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID : mGame.mDebugDailyChallengeID);
				cur_series = 200;
				num = int.Parse(num2 + (mGame.SelectedDailyChallengeSheetNo - 1).ToString("00") + mGame.SelectedDailyChallengeStageNo.ToString("0"));
			}
			else if (Place != 0 && Place != 3)
			{
				cur_series = (int)mGame.mCurrentStage.Series;
				num = mGame.mCurrentStage.StageNumber;
				if (Def.IsEventSeries(mGame.mPlayer.Data.CurrentSeries))
				{
					short a_course;
					int a_main;
					int a_sub;
					Def.SplitEventStageNo(num, out a_course, out a_main, out a_sub);
					int stageNo = Def.GetStageNo(a_course, a_main, a_sub);
					num = Def.GetStageNoForServer(mGame.mPlayer.Data.CurrentEventID, stageNo);
				}
			}
			ServerCram.BuyHeart(mPrice, mSetNum, Place, cur_series, num);
			mGame.Save();
			mGame.mOptions.BuyHeartCount++;
			mGame.SaveOptions();
			UserBehavior.Instance.BuyHeart((short)mPrice);
			UserBehavior.Save();
			mState.Change(STATE.WAIT);
			break;
		}
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int itemCount = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1);
		if (itemCount < mGame.mPlayer.Data.MaxNormalLifeCount)
		{
			mBuyEnable = true;
		}
		else
		{
			mBuyEnable = false;
		}
		string titleDescKey = ((Place == 1) ? Localization.Get("BuyHeart_Title1") : ((Place != 2) ? Localization.Get("BuyHeart_Title0") : Localization.Get("BuyHeart_Title2")));
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		Vector3[] array = new Vector3[5]
		{
			new Vector3(0f, -80f, 0f),
			new Vector3(0f, -430f, 0f),
			new Vector3(0f, -220f, 0f),
			new Vector3(0f, -340f, 0f),
			new Vector3(0f, -500f, 0f)
		};
		if (mBuyEnable)
		{
			UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, image);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 67f, 0f), Vector3.one, false);
			uISprite.SetDimensions(510, 104);
			uISprite.type = UIBasicSprite.Type.Sliced;
			UISprite sprite = Util.CreateSprite("heart", uISprite.gameObject, image);
			Util.SetSpriteInfo(sprite, "icon_heart+5", mBaseDepth + 3, new Vector3(0f, -3f, 0f), Vector3.one, false);
			titleDescKey = ((itemCount != 4) ? Localization.Get("BuyHeart_Desc") : Localization.Get("BuyHeart_DescNotive"));
			mDesc = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(mDesc, titleDescKey, mBaseDepth + 3, new Vector3(0f, -32f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(mDesc);
			if (itemCount == 4)
			{
				mDesc.color = new Color(1f, 0.1f, 0.1f);
			}
			UIButton uIButton = Util.CreateJellyImageButton("BuyButton", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "LC_button_buy", "LC_button_buy", "LC_button_buy", mBaseDepth + 3, new Vector3(0f, -117f, 0f), Vector3.one);
			mBuyButton = uIButton.gameObject.GetComponent<JellyImageButton>();
			Util.AddImageButtonMessage(uIButton, this, "OnBuyPushed", UIButtonMessage.Trigger.OnClick);
			mUIlabel = Util.CreateLabel("Price", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(mUIlabel, string.Empty + mPrice, mBaseDepth + 4, new Vector3(65f, -4f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(mUIlabel);
			if (mPrice > mGame.mPlayer.Dollar)
			{
				mUIlabel.color = new Color(1f, 0.1f, 0.1f);
			}
			CreateCancelButton("OnCancelPushed");
		}
		else
		{
			titleDescKey = ((mGame.mPlayer.Data.mMugenHeart < Def.MUGEN_HEART_STATUS.START) ? Localization.Get("BuyHeart_FullDesc") : Localization.Get("BuyHeart_DescNG_Mugen"));
			mDesc = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(mDesc, titleDescKey, mBaseDepth + 3, new Vector3(0f, 41f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(mDesc);
			mDesc.fontSize = 32;
			UIButton uIButton = Util.CreateJellyImageButton("SDButton", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 3, new Vector3(0f, -90f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnBuyPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && go.GetComponent<JellyImageButton>().IsEnable())
		{
			if (mPrice <= mGame.mPlayer.Dollar)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				mSelectItem = SELECT_ITEM.BUY_COMPLETE;
				mBoughtItem = mGame.mShopItemData[mGame.mSegmentProfile.HeartItem];
				GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
				gemUseConfirmDialog.Init(mPrice, 11);
				gemUseConfirmDialog.SetClosedCallback(OnGemUseConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				confirmDialog.SetClosedCallback(OnConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
		}
	}

	public void OnGemUseConfirmClosed(GemUseConfirmDialog.SELECT_ITEM item, int aValue)
	{
		if (item == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Change((STATE)aValue);
			return;
		}
		mBoughtItem = null;
		mState.Change(STATE.MAIN);
	}

	public void OnConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
			purchaseDialog.SetBaseDepth(mBaseDepth + 10);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(PurchaseAuthError);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	private void PurchaseAuthError()
	{
		mState.Change(STATE.AUTHERROR_WAIT);
	}

	private void OnDuplicateDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mGame.GetGemCountFreqFlag = false;
		mState.Change(STATE.NETWORK_00);
	}

	private void OnCompleteDialogClosed(ConfirmImageDialog.SELECT_ITEM i)
	{
		Close();
	}

	public void OnPurchaseDialogClosed()
	{
		if (mPrice > mGame.mPlayer.Dollar)
		{
			mUIlabel.color = new Color(1f, 0.1f, 0.1f);
		}
		else
		{
			mUIlabel.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		mState.Change(STATE.MAIN);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAINTENANCE_CHECK_WAIT))
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void ShowNetworkErrorDialog(STATE aNextState)
	{
		mStateAfterNetworkError = aNextState;
		mState.Reset(STATE.WAIT, true);
		mErrorDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mErrorDialog.SetClosedCallback(OnNetworkErrorDialogClosed);
	}

	private void OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(mStateAfterNetworkError);
	}

	private void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorAuthAbandon()
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}
}
