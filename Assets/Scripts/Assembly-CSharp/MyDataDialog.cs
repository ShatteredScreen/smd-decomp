using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyDataDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3,
		NETWORK_LOGOUT = 4,
		AUTHERROR_RETURN_TITLE = 5,
		AUTHERROR_RETURN_WAIT = 6
	}

	public class MyDataList : SMVerticalListItem
	{
		public string leftStr;

		public string rightStr;

		public bool coroutineTime;
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private UISprite mIconSprite;

	private UISprite mIconLevelSprite;

	private UISprite mFacebookIcon;

	private UILabel mNiceNameLabel;

	public bool mFaceBookLogin;

	private Partner mPartner;

	private CompanionData mCompanionData;

	private UILabel mUserIDLabel;

	private UISprite mSpriteFlame;

	private UIButton mCharaBGSprite;

	private UITexture mTex;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mStagenum;

	private UIButton mButtonGPGSAchievement;

	private SMVerticalListInfo mListInfo;

	private SMVerticalList mVerticalList;

	private List<SMVerticalListItem> mList = new List<SMVerticalListItem>();

	private int mSNSCallCount;

	private bool mAnimationFinised;

	private int mIndex;

	public bool Modified { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns3 = mSNS;
			_sns3.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns3, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns3);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns2, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							Modified = false;
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns2))
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
				else
				{
					mState.Reset(STATE.MAIN, true);
					if (mSNS == SocialManager.Instance[SNS.GooglePlayGameServices] && GameMain.IsSNSLogined(_sns2))
					{
						OnGPGSAchievementButtonPushed();
					}
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGOUT:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.Logout(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					string id = null;
					string text = null;
					GameMain.TryGetSNSUserInfo(_sns, out id, out text);
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
						mGame.mPlayer.ResetFriends();
						mGame.Save();
					}
					mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				mState.Change(STATE.MAIN);
			}, null);
			break;
		}
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		if (mGame.mPlayer.Data.PlayerIcon >= 10000)
		{
			mCompanionData = mGame.mCompanionData[mGame.mPlayer.Data.PlayerIcon - 10000];
		}
		else
		{
			mCompanionData = mGame.mCompanionData[mGame.mPlayer.Data.PlayerIcon];
		}
		mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		mPartner.transform.localPosition = Vector3.zero;
		int level = mGame.mPlayer.GetCompanionLevel(mCompanionData.index);
		mPartner.Init(mCompanionData.index, Vector3.zero, 1f, level, mPartner.gameObject, true, mBaseDepth + 2);
		while (!mPartner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
		UIPanel parentPanel = base.gameObject.GetComponent<UIPanel>();
		UIPanel panel = mPartner.gameObject.AddComponent<UIPanel>();
		panel.sortingOrder = parentPanel.sortingOrder + 1;
		panel.clipping = UIDrawCall.Clipping.SoftClip;
		panel.baseClipRegion = new Vector4(-100f, 204f, 275f, 300f);
		yield return 0;
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("MyData_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		mSpriteFlame = Util.CreateSprite("Instruction", base.gameObject, image);
		Util.SetSpriteInfo(mSpriteFlame, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 133f, 0f), Vector3.one, false);
		mSpriteFlame.type = UIBasicSprite.Type.Sliced;
		mSpriteFlame.SetDimensions(490, 177);
		float x = 40f;
		float x2 = 167f;
		float x3 = 235f;
		float num = -70f;
		float num2 = 0f;
		num = -60f;
		num2 = 27f;
		int num3 = 0;
		for (int i = 0; i < mGame.mCompanionData.Count; i++)
		{
			CompanionData companionData = mGame.mCompanionData[i];
			if (mGame.mPlayer.IsCompanionUnlock(companionData.index) && !mGame.mCompanionData[i].IsExpanded())
			{
				num3++;
			}
		}
		UILabel uILabel = Util.CreateLabel("totalChara", mSpriteFlame.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("MyData_Desc06"), mBaseDepth + 3, new Vector3(x, num, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("colon", mSpriteFlame.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("MyData_Colon"), mBaseDepth + 3, new Vector3(x2, num, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("ten", mSpriteFlame.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Format(Localization.Get("MyData_Number_05"), num3), mBaseDepth + 3, new Vector3(x3, num, 0f), 20, 0, 0, UIWidget.Pivot.Right);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(135, 20);
		mCharaBGSprite = Util.CreateImageButton("BackGround", mSpriteFlame.gameObject, image);
		Util.SetImageButtonInfo(mCharaBGSprite, "my2d_back", "my2d_back", "my2d_back", mBaseDepth + 3, new Vector3(-100f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mCharaBGSprite, this, "OnCharaBGPushed", UIButtonMessage.Trigger.OnPress);
		List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
		for (int j = 0; j < playStage.Count; j++)
		{
			if (playStage[j].Series == (int)mGame.mPlayer.Data.CurrentSeries)
			{
				mStagenum = playStage[j].Level / 100;
				break;
			}
		}
		if (mStagenum == 0)
		{
			mStagenum = 1;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(mSpriteFlame.gameObject, mBaseDepth + 3, (int)mGame.mPlayer.Data.CurrentSeries, mStagenum, 150f, num2);
		UISprite uISprite = Util.CreateSprite("Instruction", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, -96f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(490, 255);
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement, SNS.GooglePlayGameServices))
		{
			mButtonGPGSAchievement = Util.CreateJellyImageButton("GPGSButton", base.gameObject, image);
			Util.SetImageButtonInfo(mButtonGPGSAchievement, "button00", "button00", "button00", mBaseDepth + 2, new Vector3(0f, -250f, 0f), new Vector3(0.65f, 0.65f));
			Util.AddImageButtonMessage(mButtonGPGSAchievement, this, "OnGPGSAchievementButtonPushed", UIButtonMessage.Trigger.OnClick);
			mButtonGPGSAchievement.gameObject.GetComponent<JellyImageButton>().SetBaseScale(0.65f);
			uISprite = Util.CreateSprite("GPGSIcon", mButtonGPGSAchievement.gameObject, image);
			Util.SetSpriteInfo(uISprite, "icon_sns06", mBaseDepth + 3, new Vector3(-167f, 0f), new Vector3(0.5f, 0.5f, 1f), false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(60, 60);
			uILabel = Util.CreateLabel("message", mButtonGPGSAchievement.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("GooglePlus_Achievement"), mBaseDepth + 3, new Vector3(-121f, -4f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(uILabel);
			uILabel.fontSize = 28;
			uILabel.color = Color.white;
			uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
			uILabel.SetDimensions(250, 40);
		}
		mPartner.SetPartnerUIScreenDepth(mBaseDepth + 10);
		mPartner.SetPartnerScreenScale(0.54f);
		mPartner.SetPartnerScreenPos(new Vector3(-100f, -24f, 0f));
		mPartner.SetPartnerScreenEnable(true);
		CreateCancelButton("OnCancelPushed");
	}

	public void OnCharaBGPushed()
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		int num = UnityEngine.Random.Range(0, 3);
		if (num == mIndex)
		{
			num++;
			if (num > 2)
			{
				num = 0;
			}
		}
		mIndex = num;
		switch (mIndex)
		{
		case 0:
			mPartner.StartMotion(CompanionData.MOTION.HIT0, true);
			break;
		case 1:
			mPartner.StartMotion(CompanionData.MOTION.HIT1, true);
			break;
		case 2:
			mPartner.StartMotion(CompanionData.MOTION.WIN, true);
			break;
		}
	}

	private void OnGPGSAchievementButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN && 0 == 0 && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Achievement, SNS.GooglePlayGameServices))
		{
			BIJSNS sns = SocialManager.Instance[SNS.GooglePlayGameServices];
			mSNSCallCount = 0;
			DoSNS(sns);
		}
	}

	private void DoSNS(BIJSNS _sns)
	{
		mSNSCallCount++;
		try
		{
			mState.Change(STATE.WAIT);
			mSNS = _sns;
			if (_sns.IsOpEnabled(2) && !_sns.IsLogined())
			{
				if (mSNSCallCount > 1)
				{
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.NETWORK_LOGIN00);
				}
			}
			else if (!GameMain.ShowAchievements(_sns, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSLogout iBIJSNSLogout = res.detail as IBIJSNSLogout;
						if (iBIJSNSLogout != null && iBIJSNSLogout.Logout)
						{
							mState.Change(STATE.NETWORK_LOGOUT);
							return;
						}
					}
				}
				catch (Exception)
				{
				}
				mState.Change(STATE.MAIN);
			}, null))
			{
				mState.Change(STATE.MAIN);
			}
		}
		catch (Exception)
		{
			mState.Change(STATE.MAIN);
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = base.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public List<MyDataList> MYdata()
	{
		List<MyDataList> list = new List<MyDataList>();
		MyDataList myDataList = new MyDataList();
		string text = "MyData_Desc";
		int num = 2;
		myDataList.leftStr = string.Format(Localization.Get(text + string.Format("{0:D2}", num)));
		myDataList.rightStr = string.Format(Localization.Get("MyData_Number_02"), mGame.mPlayer.Data.MaxCombo);
		list.Add(myDataList);
		myDataList = new MyDataList();
		num++;
		myDataList.leftStr = string.Format(Localization.Get(text + string.Format("{0:D2}", num)));
		myDataList.rightStr = string.Format(Localization.Get("MyData_Number_00"), mGame.mPlayer.Data.MaxScore);
		list.Add(myDataList);
		myDataList = new MyDataList();
		num++;
		myDataList.leftStr = string.Format(Localization.Get(text + string.Format("{0:D2}", num)));
		int num2 = (int)mGame.mPlayer.Data.TotalPlayTime;
		int num3 = num2 / 3600;
		num2 %= 3600;
		int num4 = num2 / 60;
		int num5 = num2 % 60;
		string arg = string.Format("{0:D2}", num4);
		string arg2 = string.Format("{0:D2}", num5);
		myDataList.rightStr = string.Format(Localization.Get("DataCheck_TimeFormat"), num3, arg, arg2);
		myDataList.coroutineTime = true;
		list.Add(myDataList);
		myDataList = new MyDataList();
		num++;
		myDataList.leftStr = string.Format(Localization.Get(text + string.Format("{0:D2}", num)));
		long num6 = mGame.mPlayer.Data.TotalWin + mGame.mPlayer.Data.TotalLose;
		myDataList.rightStr = string.Format(Localization.Get("MyData_Number_04"), num6);
		list.Add(myDataList);
		myDataList = new MyDataList();
		num++;
		num++;
		myDataList.leftStr = Localization.Get("MyData_Desc00");
		myDataList.rightStr = string.Format(Localization.Get("MyData_Number_00"), mGame.mPlayer.TotalHighScore);
		list.Add(myDataList);
		myDataList = new MyDataList();
		myDataList.leftStr = Localization.Get("MyData_Desc01");
		myDataList.rightStr = string.Format(Localization.Get("MyData_Number_01"), mGame.mPlayer.TotalStars);
		list.Add(myDataList);
		myDataList = new MyDataList();
		myDataList.leftStr = string.Format(Localization.Get(text + string.Format("{0:D2}", num)));
		myDataList.rightStr = string.Format(Localization.Get("MyData_Number_01"), mGame.mPlayer.GetTotalTrophy());
		list.Add(myDataList);
		myDataList = new MyDataList();
		num++;
		myDataList.leftStr = string.Format(Localization.Get(text + string.Format("{0:D2}", num)));
		int num7 = 0;
		List<int> clearedArray = MissionManager.GetClearedArray();
		for (int i = 0; i < clearedArray.Count; i++)
		{
			MissionProgress missionProgress = MissionManager.GetMissionProgress(clearedArray[i]);
			num7 += missionProgress.mStep - 1;
		}
		myDataList.rightStr = string.Empty + num7;
		list.Add(myDataList);
		myDataList = new MyDataList();
		num++;
		myDataList.leftStr = string.Format(Localization.Get(text + string.Format("{0:D2}", num)));
		int num8 = 0;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_FIRST, out data);
		int num9 = data.GetClearedMainStage() * 100;
		if (num9 >= data.MaxMainStageNum && num9 != 0)
		{
			num8++;
		}
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_R, out data);
		num9 = data.GetClearedMainStage() * 100;
		if (num9 >= data.MaxMainStageNum && num9 != 0)
		{
			num8++;
		}
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_S, out data);
		num9 = data.GetClearedMainStage() * 100;
		if (num9 >= data.MaxMainStageNum && num9 != 0)
		{
			num8++;
		}
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_SS, out data);
		num9 = data.GetClearedMainStage() * 100;
		if (num9 >= data.MaxMainStageNum && num9 != 0)
		{
			num8++;
		}
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_STARS, out data);
		num9 = data.GetClearedMainStage() * 100;
		if (num9 >= data.MaxMainStageNum && num9 != 0)
		{
			num8++;
		}
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_GF00, out data);
		num9 = data.GetClearedMainStage() * 100;
		if (num9 >= data.MaxMainStageNum && num9 != 0)
		{
			num8++;
		}
		myDataList.rightStr = string.Empty + num8;
		list.Add(myDataList);
		myDataList = new MyDataList();
		num++;
		return list;
	}

	private void OnCreateItem_Friend(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		UIFont atlasFont = GameMain.LoadFont();
		MyDataList myDataList = item as MyDataList;
		UILabel uILabel = Util.CreateLabel("leftDesc" + index, itemGO.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, myDataList.leftStr, mBaseDepth + 5, new Vector3(-214f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("rightDesc" + index, itemGO.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, myDataList.rightStr, mBaseDepth + 5, new Vector3(201f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Right);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		if (myDataList.coroutineTime)
		{
			StartCoroutine(UpdateTimeNum(uILabel));
		}
	}

	private IEnumerator UpdateTimeNum(UILabel time)
	{
		while (!(time == null))
		{
			int timeBuf2 = (int)mGame.mPlayer.Data.TotalPlayTime;
			int hour = timeBuf2 / 3600;
			timeBuf2 %= 3600;
			int min = timeBuf2 / 60;
			int sec = timeBuf2 % 60;
			Util.SetLabelText(time, string.Format(arg1: string.Format("{0:D2}", min), arg2: string.Format("{0:D2}", sec), format: Localization.Get("DataCheck_TimeFormat"), arg0: hour));
			yield return null;
		}
	}

	public override void OnOpenFinished()
	{
		List<MyDataList> list = MYdata();
		mList.Clear();
		for (int i = 0; i < list.Count; i++)
		{
			mList.Add(list[i]);
		}
		mListInfo = new SMVerticalListInfo(1, 495f, 246f, 1, 495f, 246f, 1, 26);
		mVerticalList = SMVerticalList.Make(base.gameObject, this, mListInfo, mList, 0f, -35f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mVerticalList.OnCreateItem = OnCreateItem_Friend;
		mVerticalList.OnCreateFinish = OnCreateFin;
		mVerticalList.transform.localPosition = new Vector3(0f, -61f);
		mVerticalList.SetScrollBarDisplay(false);
	}

	public void OnCreateFin()
	{
		mVerticalList.SetScrollBarPosition(new Vector3(227f, 0f));
		mVerticalList.SetScrollBarDisplay();
	}

	public override void OnCloseFinished()
	{
		if (mPartner != null)
		{
			UnityEngine.Object.Destroy(mPartner.gameObject);
		}
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
