using UnityEngine;

public class GooglePlusAndroid : GooglePlusIF
{
	private static AndroidJavaObject mJavaInstance;

	private static AndroidJavaObject javaInstance
	{
		get
		{
			if (mJavaInstance == null)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.bij.bijunityplugin.MainActivity"))
				{
					mJavaInstance = androidJavaClass.CallStatic<AndroidJavaObject>("getGooglePlusPlugin", new object[0]);
				}
			}
			return mJavaInstance;
		}
	}

	public override void enableLogging(bool enable, bool verbose)
	{
		javaInstance.Call("enableLogging", enable, verbose);
	}

	public override void setup(string clientid, bool autoConnect)
	{
		javaInstance.Call("setup", base.gameObject.name, clientid, autoConnect);
	}

	public override void onPause()
	{
	}

	public override void onResume()
	{
	}

	public override bool isCanceled()
	{
		return false;
	}

	public override bool isSignedIn()
	{
		return javaInstance.Call<bool>("isSignedIn", new object[0]);
	}

	public override void authenticate()
	{
		javaInstance.Call("authenticate");
	}

	public override void signOut()
	{
		javaInstance.Call("signOut");
	}

	public override bool loadPerson(string userid)
	{
		return javaInstance.Call<bool>("loadPerson", new object[1] { GooglePlusIF.ToSS(userid) });
	}

	public override bool loadVisiblePeople(string userid)
	{
		return javaInstance.Call<bool>("loadVisiblePeople", new object[1] { GooglePlusIF.ToSS(userid) });
	}

	public override bool getUserImageURL(string userid)
	{
		return javaInstance.Call<bool>("getUserImageURL", new object[1] { GooglePlusIF.ToSS(userid) });
	}

	public override bool interactivePost(string title, string message, string link, string attachUrl, string attachMime, string actionLabel, string actionUri, string deepLinkID, string description, string thumbnailUri)
	{
		string[] args = new string[10]
		{
			GooglePlusIF.ToSS(title),
			GooglePlusIF.ToSS(message),
			GooglePlusIF.ToSS(link),
			GooglePlusIF.ToSS(attachUrl),
			GooglePlusIF.ToSS(attachMime),
			GooglePlusIF.ToSS(actionLabel),
			GooglePlusIF.ToSS(actionUri),
			GooglePlusIF.ToSS(deepLinkID),
			GooglePlusIF.ToSS(description),
			GooglePlusIF.ToSS(thumbnailUri)
		};
		return javaInstance.Call<bool>("interactivePost", args);
	}
}
