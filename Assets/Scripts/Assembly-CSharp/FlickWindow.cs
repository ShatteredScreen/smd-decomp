using UnityEngine;

public class FlickWindow : MonoBehaviour
{
	public delegate void OnFlickStart(float directionX, float directionY);

	public delegate void OnFlickEnd();

	private Vector3 mTouchStartPos;

	private Vector3 mTouchEndPos;

	private bool mFlickEnable;

	private float mInitPosX;

	private float mInitPosY;

	private float mWidth;

	private float mHeight;

	private OnFlickStart mFlickStartCallback;

	private OnFlickEnd mFlickEndCallback;

	public void Start()
	{
		SingletonMonoBehaviour<GameMain>.Instance.AddCallbackEvent(GameMain.EVENT_TYPE.OnTouchDown, OnTouchDown);
		SingletonMonoBehaviour<GameMain>.Instance.AddCallbackEvent(GameMain.EVENT_TYPE.OnTouchUp, OnTouchUp);
	}

	public void Update()
	{
		if (mFlickEnable && mFlickStartCallback != null)
		{
			mFlickStartCallback(SingletonMonoBehaviour<GameMain>.Instance.mTouchPos.x - mTouchStartPos.x, SingletonMonoBehaviour<GameMain>.Instance.mTouchPos.y - mTouchStartPos.y);
		}
	}

	public void UpdateInitPos(float _posX, float _posY, float _width, float _height)
	{
		mInitPosX = _posX;
		mInitPosY = _posY;
		mWidth = _width;
		mHeight = _height;
	}

	public void Init(float _posX, float _posY, float _width, float _height)
	{
		mInitPosX = _posX;
		mInitPosY = _posY;
		mWidth = _width;
		mHeight = _height;
	}

	public void SetFlickStartCallBack(OnFlickStart _Callback)
	{
		mFlickStartCallback = _Callback;
	}

	public void SetFlickEndCallBack(OnFlickEnd _Callback)
	{
		mFlickEndCallback = _Callback;
	}

	public void FlickClear()
	{
		mFlickEnable = false;
	}

	private void OnTouchDown()
	{
		Vector3 vector = SingletonMonoBehaviour<GameMain>.Instance.mTouchPos;
		if (!mFlickEnable && vector.x >= mInitPosX && vector.x <= mInitPosX + mWidth && vector.y >= mInitPosY && vector.y <= mInitPosY + mHeight)
		{
			mFlickEnable = true;
			mTouchStartPos = vector;
		}
	}

	private void OnTouchUp()
	{
		if (mFlickEnable)
		{
			mFlickEnable = false;
			if (mFlickEndCallback != null)
			{
				mFlickEndCallback();
			}
		}
	}

	public void OnDestroy()
	{
		if (SingletonMonoBehaviour<GameMain>.Instance != null)
		{
			SingletonMonoBehaviour<GameMain>.Instance.RemoveCallbackEvent(GameMain.EVENT_TYPE.OnTouchDown, OnTouchDown);
			SingletonMonoBehaviour<GameMain>.Instance.RemoveCallbackEvent(GameMain.EVENT_TYPE.OnTouchUp, OnTouchUp);
		}
	}
}
