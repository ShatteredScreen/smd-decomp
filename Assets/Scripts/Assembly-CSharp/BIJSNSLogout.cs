using System;

public class BIJSNSLogout : BIJSNSData<BIJSNSLogout>, ICloneable, ICopyable, IBIJSNSLogout
{
	public bool Logout { get; set; }

	public BIJSNSLogout()
		: this(false)
	{
	}

	public BIJSNSLogout(bool flag)
	{
		Logout = flag;
	}
}
