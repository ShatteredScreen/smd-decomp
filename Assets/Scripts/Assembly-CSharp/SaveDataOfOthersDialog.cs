using UnityEngine;

public class SaveDataOfOthersDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private Player mPlayerData;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("Debug_SaveDataOfOthers_Success_Title");
		string text = "LC_button_close";
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UILabel uILabel = Util.CreateLabel("uuid", base.gameObject, atlasFont);
		string text2 = string.Format("UUID : " + mPlayerData.UUID);
		Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, 80f, 0f), 35, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("searchid", base.gameObject, atlasFont);
		text2 = ((mPlayerData.SearchID != null) ? string.Format("SearchID : " + mPlayerData.SearchID.ToString()) : "SearchID : null");
		Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(-24f, 30f, 0f), 35, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Debug_SaveDataOfOthers_Success_Desc"), mBaseDepth + 3, new Vector3(0f, -30f, 0f), 25, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		if (!GameMain.mGetSaveDataOfOthersCXSucceed)
		{
			uILabel = Util.CreateLabel("desc2", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Debug_OfOther_CX_NotFound"), mBaseDepth + 3, new Vector3(0f, -45f, 0f), 25, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.red;
		}
		UIButton button = Util.CreateJellyImageButton("Button", base.gameObject, image);
		Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 3, new Vector3(0f, -117f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(Player p)
	{
		mPlayerData = p;
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			string key = "SE_POSITIVE";
			mGame.PlaySe(key, -1);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
