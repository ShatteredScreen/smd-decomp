using System;
using System.Collections.Generic;

public class AdvGameInfoSettings : ICloneable
{
	[MiniJSONAlias("start")]
	public long start_time { get; set; }

	[MiniJSONAlias("end")]
	public long end_time { get; set; }

	[MiniJSONAlias("info_hide")]
	public int InfoHide { get; set; }

	[MiniJSONAlias("info_cnt")]
	public int InfoCount { get; set; }

	[MiniJSONAlias("info_url")]
	public List<string> InfoURL { get; set; }

	[MiniJSONAlias("linkbanner")]
	public LinkBannerSetting LinkBanner { get; set; }

	[MiniJSONAlias("help_url")]
	public string HelpUrl { get; set; }

	[MiniJSONAlias("max_dungeon_lvl")]
	public List<AdvMaxDugeonLevelSettings> MaxLvl { get; set; }

	[MiniJSONAlias("send_medal")]
	public int SendMedal { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGameInfoSettings Clone()
	{
		return MemberwiseClone() as AdvGameInfoSettings;
	}
}
