using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EventLabyrinthCockpit : EventCockpit
{
	private enum LayoutTarget
	{
		Possess = 0,
		Course = 1,
		HowToPlay = 2,
		Title = 3,
		Restart = 4,
		Frame_1 = 5,
		Frame_2 = 6,
		Line = 7,
		Reward = 8,
		RewardList = 9,
		Gauge = 10,
		PopupP = 11,
		PopupL = 12
	}

	private SMEventPageData mEventPageData;

	private PlayerEventData mPlayerEventData;

	private Dictionary<LayoutTarget, Transform> mLayoutTarget = new Dictionary<LayoutTarget, Transform>();

	private GameObject mGoAnchorT;

	private GameObject mGoAnchorTL;

	private GameObject mGoAnchorTR;

	private GameObject mGoAnchorL;

	private UIPanel mPanelL2D;

	private UIPanel mPanelPopup;

	private UILabel mLabelPossessJewel;

	private UILabel mLabelBonusText;

	private UILabel mLabelBonusMul;

	private UILabel mLabelBonusRatio;

	private UISprite mSpriteCourseNo;

	private UIButton mButtonReward;

	private UIButton mButtonBackToCourse;

	private Dictionary<string, UISsSprite> mAnimFeatureJewel = new Dictionary<string, UISsSprite>();

	private UISsSprite mAnimNextStage;

	private GaugeSprite mGauge;

	private int mLive2dDepth = -1;

	private int mCachePanelDepth = -1;

	private int mCacheJewelAmount = -1;

	private float mPortraitOffset;

	private bool mCanUpdateJewel;

	private bool mForceUpdateJewel = true;

	private bool mIsNextCharacter = true;

	private BIJImage mAtlasEventLabyrinth;

	private int mAnimNextStageDepth;

	protected GameObject mFooterRoot;

	protected override Vector3 ShopBugLocation()
	{
		ScreenOrientation screenOrientation = Util.ScreenOrientation;
		if (screenOrientation == ScreenOrientation.LandscapeLeft || screenOrientation == ScreenOrientation.LandscapeRight)
		{
			return new Vector3(-65f, 65f);
		}
		return new Vector3(-70f, (!base.mMugenflg) ? 172 : 282);
	}

	protected override Vector3 MugenHeartLocation()
	{
		ScreenOrientation screenOrientation = Util.ScreenOrientation;
		if (screenOrientation == ScreenOrientation.LandscapeLeft || screenOrientation == ScreenOrientation.LandscapeRight)
		{
			float num = 55 + 108 * (base.mShopflg ? 1 : 0) + (base.mShopflg ? 5 : 0);
			return new Vector3(0f - num, 60f);
		}
		return new Vector3(-70f, 172f);
	}

	protected override void Update()
	{
		if (mGSM == null)
		{
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			CreateCockpitStatus(40);
			CreateCockpitItems(40);
			CreateCockpitDebug(40, -90);
			CreateSaleItem(40);
			CreateMugenHeart(40);
			if (!mButtonEnableFlg)
			{
				SetEnableButtonAction(false);
				mButtonEnableFlg = true;
			}
			mState.Change(STATE.MAIN);
			break;
		case STATE.OPEN:
			if (mState.IsChanged() && mOpenCallback != null)
			{
				mOpenCallback(false);
			}
			break;
		case STATE.MAIN:
			UpdateSaleFlg();
			if (base.mShopflg)
			{
				base.ShopBagEnable = true;
			}
			else
			{
				base.ShopBagEnable = false;
			}
			UpdateMugenHeartIconFlg();
			if (base.mMugenflg)
			{
				base.ShopMugenHeartEnable = true;
			}
			else
			{
				base.ShopMugenHeartEnable = false;
			}
			break;
		case STATE.MOVE:
			if (!mState.IsChanged())
			{
				break;
			}
			switch (mState.GetPreStatus())
			{
			case STATE.OPEN:
				if (mCloseCallback != null)
				{
					mCloseCallback(true);
				}
				break;
			case STATE.CLOSE:
				if (mOpenCallback != null)
				{
					mOpenCallback(true);
				}
				break;
			}
			break;
		}
		mState.Update();
		UpdateUIPanel();
		UpdateHUD();
		UpdateDrawFlag();
		UpdateDrawOptionFlag();
		UpdateStatus();
	}

	private void UpdateHUD()
	{
		if ((mPlayerEventData != null && mPlayerEventData.LabyrinthData != null && mPlayerEventData.LabyrinthData.JewelAmount != mCacheJewelAmount && mCanUpdateJewel) || mForceUpdateJewel)
		{
			int num = mPlayerEventData.LabyrinthData.JewelAmount - mCacheJewelAmount;
			mCacheJewelAmount = mPlayerEventData.LabyrinthData.JewelAmount;
			if (mEventPageData != null)
			{
				KeyValuePair<int, int> keyValuePair = new KeyValuePair<int, int>(-1, -1);
				List<KeyValuePair<int, int>> list = new List<KeyValuePair<int, int>>();
				list.Add(new KeyValuePair<int, int>(mEventPageData.CharacterMapSetting.CharacterGetNum_1, mEventPageData.CharacterMapSetting.EventCockpitCharaID_1));
				list.Add(new KeyValuePair<int, int>(mEventPageData.CharacterMapSetting.CharacterGetNum_2, mEventPageData.CharacterMapSetting.EventCockpitCharaID_2));
				list.Add(new KeyValuePair<int, int>(mEventPageData.CharacterMapSetting.CharacterGetNum_3, mEventPageData.CharacterMapSetting.EventCockpitCharaID_3));
				list.Add(new KeyValuePair<int, int>(mEventPageData.CharacterMapSetting.CharacterGetNum_4, mEventPageData.CharacterMapSetting.EventCockpitCharaID_4));
				List<KeyValuePair<int, int>> list2 = list;
				int max = -1;
				foreach (KeyValuePair<int, int> item in list2)
				{
					if (max < item.Key)
					{
						max = item.Key;
					}
					if (mCacheJewelAmount < item.Key)
					{
						keyValuePair = item;
						break;
					}
				}
				if (keyValuePair.Key == -1 && max >= 0)
				{
					keyValuePair = list2.FirstOrDefault((KeyValuePair<int, int> n) => n.Key == max);
				}
				if (keyValuePair.Key < 0 || keyValuePair.Key <= mCacheJewelAmount)
				{
					if ((bool)mPanelPopup && mPanelPopup.gameObject.activeSelf)
					{
						mPanelPopup.gameObject.SetActive(false);
					}
					mIsNextCharacter = false;
					keyValuePair = new KeyValuePair<int, int>(mEventPageData.CharacterMapSetting.CharacterGetNum_4 + 1, mEventPageData.CharacterMapSetting.EventCockpitCharaID_Comp);
				}
				else
				{
					if ((bool)mPanelPopup && !mPanelPopup.gameObject.activeSelf)
					{
						NGUITools.SetActive(mPanelPopup.gameObject, true);
					}
					if (mAnimFeatureJewel != null)
					{
						int num2 = keyValuePair.Key - mPlayerEventData.LabyrinthData.JewelAmount;
						int num3 = ((num2 == 0) ? 1 : ((int)Mathf.Log10(num2) + 1));
						List<CHANGE_PART_INFO> list3 = new List<CHANGE_PART_INFO>();
						string[] array = new string[5] { "one_", "ten_", "hundred_", "thousand_", "ten_thousand_" };
						for (int i = 0; i < array.Length; i++)
						{
							int num4 = ((num2 <= 0) ? (-1) : (num2 % 10));
							num2 /= 10;
							for (int j = i; j < array.Length; j++)
							{
								list3.Add(new CHANGE_PART_INFO
								{
									partName = new StringBuilder(array[j]).Append(i + 1).ToString(),
									spriteName = ((num3 != j + 1 || num4 < 0) ? "null" : new StringBuilder("event_panel_num").Append(num4).ToString())
								});
							}
						}
						if (list3.Count > 0)
						{
							foreach (KeyValuePair<string, UISsSprite> item2 in mAnimFeatureJewel)
							{
								if (item2.Value != null)
								{
									ChangeAnime(item2.Value, mAtlasEventLabyrinth, item2.Key, list3.ToArray());
								}
							}
						}
						list3.Clear();
						list3 = null;
					}
					mIsNextCharacter = true;
				}
				if (keyValuePair.Value != -1 && !CanCreateLive2dCharacter((short)keyValuePair.Value))
				{
					CreateLive2d(mGame.mCompanionData[keyValuePair.Value], CompanionData.MOTION.STAY0, new IntVector2(200, 200), new IntVector2(200, 200), mGSM.MapPageRoot, mPanelL2D.gameObject, Vector3.zero, mLive2dDepth, 0.35f);
				}
			}
			if ((bool)mLabelPossessJewel)
			{
				Util.SetLabelText(mLabelPossessJewel, mCacheJewelAmount.ToString());
			}
			if (mGauge != null)
			{
				if (num != 0)
				{
					mGauge.Animation(num, 1f);
				}
				else
				{
					mGauge.SetValue(mCacheJewelAmount);
				}
			}
		}
		mCanUpdateJewel = false;
		mForceUpdateJewel = false;
		if (mPanel != null && mPanel.depth != mCachePanelDepth)
		{
			mPanel.depth--;
			mPanel.sortingOrder--;
			int num5 = mPanel.depth + 1;
			int num6 = mPanel.sortingOrder + 1;
			if ((bool)mPanelL2D)
			{
				mPanelL2D.depth = num5;
				mPanelL2D.sortingOrder = num6;
			}
			if ((bool)mPanelPopup)
			{
				mPanelPopup.depth = num5 + 1;
				mPanelPopup.sortingOrder = num6 + 1;
			}
			mCachePanelDepth = mPanel.depth;
		}
		if (!(mGSM is GameStateEventLabyrinth))
		{
			return;
		}
		GameStateEventLabyrinth gameStateEventLabyrinth = mGSM as GameStateEventLabyrinth;
		if (gameStateEventLabyrinth.IsInCourseSelect && mAnimNextStage != null)
		{
			UnityEngine.Object.Destroy(mAnimNextStage.gameObject);
			mAnimNextStage = null;
		}
		else if (gameStateEventLabyrinth.IsNewCourseUnlocked && mAnimNextStage == null)
		{
			float y = 100f;
			float num7 = 1f;
			ScreenOrientation screenOrientation = Util.ScreenOrientation;
			if (screenOrientation == ScreenOrientation.LandscapeLeft || screenOrientation == ScreenOrientation.LandscapeRight)
			{
				num7 = 0.7f;
				if (Util.IsWideScreen())
				{
					y = 90f;
				}
			}
			mAnimNextStage = Util.CreateUISsSprite("Animation", mAnchorBL.gameObject, "LABYRINTH_NEXT_STAGE_OPEN", new Vector3(83f, y), Vector3.one, mAnimNextStageDepth);
			mAnimNextStage.DestroyAtEnd = false;
			mAnimNextStage.PlayCount = 0;
			mAnimNextStage.SetLocalScale(num7, num7, 1f);
		}
		else if (!gameStateEventLabyrinth.IsNewCourseUnlocked && mAnimNextStage != null)
		{
			UnityEngine.Object.Destroy(mAnimNextStage.gameObject);
			mAnimNextStage = null;
		}
	}

	protected override void CreateCockpitStatus(int base_depth)
	{
		base.CreateCockpitStatus(base_depth);
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		Vector2 vector = Util.LogScreenSize();
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		mAtlasEventLabyrinth = ResourceManager.LoadImage("EVENTLABYRINTH").Image;
		Vector2 vector2 = Util.LogScreenSize();
		mGoAnchorT = mAnchorT.gameObject;
		mGoAnchorTL = mAnchorTL.gameObject;
		mGoAnchorTR = mAnchorTR.gameObject;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		UIGrid uIGrid = null;
		GameObject gameObject = null;
		UIButton uIButton = null;
		UILabel uILabel = null;
		string text = "null";
		Vector2 vector3 = ((!(vector.x > vector.y)) ? new Vector2(vector.y, vector.x) : vector);
		float num = 0.82080925f;
		float num2 = 0.75f;
		float num3 = vector3.y / vector3.x - num;
		mPortraitOffset = ((num3 != 9f) ? (20f * ((vector3.y / vector3.x - num) / (num2 - num))) : 0f);
		if (mPortraitOffset > 20f)
		{
			mPortraitOffset = 20f;
		}
		mGoAnchorL = Util.CreateAnchor("CockpitAnchorL", mCockpitRoot, UIAnchor.Side.Left, component).gameObject;
		mEventPageData = GameMain.GetEventPageData(mGame.mPlayer.Data.CurrentEventID);
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out mPlayerEventData);
		uISprite = Util.CreateImageButton("CaptionFrameL", mGoAnchorT, mAtlasEventLabyrinth).GetComponent<UISprite>();
		Util.SetSpriteInfo(uISprite, "event_frame", base_depth - 1, new Vector3(2f, 0f), Vector3.one, false, UIWidget.Pivot.TopLeft);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(570, 267);
		uISprite.SetLocalScale(-1f, 1f, 1f);
		uISprite.ResizeCollider();
		uIButton = uISprite.GetComponent<UIButton>();
		if (uIButton != null)
		{
			uISprite.color = Color.white;
			uIButton.hover = Color.white;
			uIButton.pressed = Color.white;
			uIButton.disabledColor = Color.white;
		}
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Frame_1))
		{
			mLayoutTarget.Add(LayoutTarget.Frame_1, uISprite.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Frame_1] = uISprite.transform;
		}
		uISprite = Util.CreateImageButton("CaptionFrameR", mGoAnchorT, mAtlasEventLabyrinth).GetComponent<UISprite>();
		Util.SetSpriteInfo(uISprite, "event_frame", base_depth - 1, new Vector3(-2f, 0f), Vector3.one, false, UIWidget.Pivot.TopLeft);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(570, 267);
		uISprite.SetLocalScale(1f, 1f, 1f);
		uISprite.ResizeCollider();
		uIButton = uISprite.GetComponent<UIButton>();
		if (uIButton != null)
		{
			uISprite.color = Color.white;
			uIButton.hover = Color.white;
			uIButton.pressed = Color.white;
			uIButton.disabledColor = Color.white;
		}
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Frame_2))
		{
			mLayoutTarget.Add(LayoutTarget.Frame_2, uISprite.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Frame_2] = uISprite.transform;
		}
		int currentEventID = mGame.mPlayer.Data.CurrentEventID;
		SMSeasonEventSetting sMSeasonEventSetting = mGame.mEventData.InSessionEventList[currentEventID];
		BIJImage image2 = ResourceManager.LoadImage(sMSeasonEventSetting.BannerAtlas).Image;
		uISprite = Util.CreateSprite("EventTitle", mGoAnchorT, image2);
		Util.SetSpriteInfo(uISprite, mEventPageData.EventSetting.MapEventTitle, base_depth + 2, new Vector3(-136f, -113f), Vector3.one, false);
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Title))
		{
			mLayoutTarget.Add(LayoutTarget.Title, uISprite.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Title] = uISprite.transform;
		}
		uISprite = Util.CreateSprite("Line", mGoAnchorT, mAtlasEventLabyrinth);
		Util.SetSpriteInfo(uISprite, "line02", base_depth + 2, new Vector3(-136f, -138f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(360, 20);
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Line))
		{
			mLayoutTarget.Add(LayoutTarget.Line, uISprite.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Line] = uISprite.transform;
		}
		uIGrid = Util.CreateGameObject("Course", mGoAnchorT).AddComponent<UIGrid>();
		uIGrid.arrangement = UIGrid.Arrangement.Horizontal;
		uIGrid.cellWidth = 158f;
		uIGrid.pivot = UIWidget.Pivot.Center;
		uIGrid.SetLocalPosition(-130f, -111f);
		gameObject = uIGrid.gameObject;
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Course))
		{
			mLayoutTarget.Add(LayoutTarget.Course, uIGrid.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Course] = uIGrid.transform;
		}
		mSpriteCourseNo = Util.CreateSprite("Image", gameObject, mAtlasEventLabyrinth);
		Util.SetSpriteInfo(mSpriteCourseNo, "LC_course" + 1, base_depth + 2, Vector3.zero, Vector3.one, false);
		gameObject = Util.CreateGameObject("Bonus", gameObject);
		mLabelBonusText = Util.CreateLabel("Header", gameObject, atlasFont);
		Util.SetLabelInfo(mLabelBonusText, Localization.Get("Bonus"), base_depth + 2, new Vector3(-49f, -2f), 21, 76, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(mLabelBonusText, new Color32(85, 55, 41, byte.MaxValue));
		Util.SetLabeShrinkContent(mLabelBonusText, 76, 21);
		mLabelBonusText.spacingX = -2;
		mLabelBonusText.effectColor = Color.white;
		mLabelBonusText.effectDistance = new Vector2(2f, 2f);
		mLabelBonusMul = Util.CreateLabel("Cross", gameObject, atlasFont);
		Util.SetLabelInfo(mLabelBonusMul, Localization.Get("Xtext"), base_depth + 2, new Vector3(0f, -3f), 26, 26, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(mLabelBonusMul, new Color32(85, 55, 41, byte.MaxValue));
		Util.SetLabeShrinkContent(mLabelBonusMul, 26, 26);
		mLabelBonusMul.effectColor = Color.white;
		mLabelBonusMul.effectDistance = new Vector2(2f, 2f);
		mLabelBonusRatio = Util.CreateLabel("Value", gameObject, atlasFont);
		Util.SetLabelInfo(mLabelBonusRatio, "1.0", base_depth + 2, new Vector3(10f, -3f), 26, 70, 0, UIWidget.Pivot.Left);
		Util.SetLabelColor(mLabelBonusRatio, new Color32(85, 55, 41, byte.MaxValue));
		Util.SetLabeShrinkContent(mLabelBonusRatio, 70, 26);
		mLabelBonusRatio.spacingX = -3;
		mLabelBonusRatio.effectColor = Color.white;
		mLabelBonusRatio.effectDistance = new Vector2(2f, 2f);
		text = "LC_button_restart";
		uIButton = Util.CreateJellyImageButton("Restart", mGoAnchorT, mAtlasEventLabyrinth);
		Util.SetImageButtonInfo(uIButton, text, text, text, base_depth + 1, new Vector3(-229f, -237f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, mGSM, "OnRestartCourse", UIButtonMessage.Trigger.OnClick);
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Restart))
		{
			mLayoutTarget.Add(LayoutTarget.Restart, uIButton.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Restart] = uIButton.transform;
		}
		GaugeSprite.Config config = new GaugeSprite.Config();
		uISprite = Util.CreateSprite("Progress", mGoAnchorT, mAtlasEventLabyrinth);
		Util.SetSpriteInfo(uISprite, "event_instruction_panel00", base_depth + 1, Vector3.zero, Vector3.one, false, UIWidget.Pivot.Left);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(245, 48);
		gameObject = uISprite.gameObject;
		config.Frame = uISprite;
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Gauge))
		{
			mLayoutTarget.Add(LayoutTarget.Gauge, gameObject.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Gauge] = gameObject.transform;
		}
		uISprite = Util.CreateSprite("Image", gameObject, mAtlasEventLabyrinth);
		Util.SetSpriteInfo(uISprite, "event_gauge01", base_depth + 2, new Vector3(6f, 0f), Vector3.one, false, UIWidget.Pivot.Left);
		uISprite.type = UIBasicSprite.Type.Filled;
		uISprite.fillDirection = UIBasicSprite.FillDirection.Horizontal;
		uISprite.SetDimensions(191, 23);
		config.Body = uISprite;
		config.Icon = Util.CreateGameObject("Item", gameObject);
		config.Icon.transform.SetLocalPosition(220f, 0f);
		config.IconSize = new IntVector2(42, 42);
		config.IconDepth = base_depth + 3;
		uILabel = Util.CreateLabel("Label", gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, base_depth + 4, new Vector3(15f, -13f), 17, 180, 0, UIWidget.Pivot.Left);
		Util.SetLabelColor(uILabel, Color.white);
		Util.SetLabeShrinkContent(uILabel, 180, 17);
		uILabel.effectStyle = UILabel.Effect.Outline8;
		uILabel.effectColor = new Color32(85, 55, 41, byte.MaxValue);
		uILabel.effectDistance = new Vector2(2f, 2f);
		config.Label = uILabel;
		uISprite = Util.CreateSprite("Complete", gameObject, mAtlasEventLabyrinth);
		Util.SetSpriteInfo(uISprite, "oldevent_Complete", base_depth + 5, new Vector3(105f, 3f), new Vector3(0.75f, 0.75f, 1f), false);
		config.Complete = uISprite;
		config.MaxHorizontalDimension = 190;
		config.MinHorizontalDimension = 16;
		config.LocalizeKey = "Labyrinth_CharacterIncentive_02";
		config.IsShowNonNextReward = true;
		config.AchieveSeKey = "SE_MAP_GET_RBKEY";
		config.CompleteIconAtlas = mAtlasEventLabyrinth;
		config.CompleteIconKey = "event_instruction_ALLGET";
		mGauge = new GaugeSprite(config, mEventPageData.EventRewardList.ToArray());
		if (mPlayerEventData != null && mPlayerEventData.LabyrinthData != null)
		{
			mGauge.SetValue(mPlayerEventData.LabyrinthData.JewelAmount);
			mCacheJewelAmount = mPlayerEventData.LabyrinthData.JewelAmount;
		}
		else
		{
			mGauge.SetValue(0);
		}
		gameObject = Util.CreateGameObject("Reward", mGoAnchorT);
		gameObject.transform.SetLocalPosition(229f, -177f);
		if (!mLayoutTarget.ContainsKey(LayoutTarget.Reward))
		{
			mLayoutTarget.Add(LayoutTarget.Reward, gameObject.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.Reward] = gameObject.transform;
		}
		uISprite = Util.CreateSprite("BackGround", gameObject, mAtlasEventLabyrinth);
		Util.SetSpriteInfo(uISprite, "event_panel04", base_depth + 2, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(165, 200);
		uISprite = Util.CreateSprite("Character", uISprite.gameObject, mAtlasEventLabyrinth);
		Util.SetSpriteInfo(uISprite, "event_panel03", base_depth + 3, new Vector3(0f, -2f), Vector3.one, false);
		mPanelL2D = Util.CreatePanel("Panel", uISprite.gameObject, base_depth + 2, base_depth + 2);
		mPanelL2D.clipping = UIDrawCall.Clipping.SoftClip;
		mPanelL2D.clipSoftness = Vector2.zero;
		mPanelL2D.clipOffset = Vector2.zero;
		mPanelL2D.baseClipRegion = new Vector4(0f, 2f, 138f, 175f);
		mLive2dDepth = base_depth + 3;
		CreateLive2d(mGame.mCompanionData[mEventPageData.CharacterMapSetting.EventCockpitCharaID_1], CompanionData.MOTION.STAY0, new IntVector2(275, 275), new IntVector2(200, 200), mGSM.MapPageRoot, mPanelL2D.gameObject, Vector3.zero, mLive2dDepth, 0.35f);
		mPanelPopup = Util.CreatePanel("NextPopup", gameObject, base_depth + 2, base_depth + 2);
		mPanelPopup.SetLocalPosition(0f, 0f);
		string text2 = "LABYRINTH_POPUP_PORTRAIT";
		UISsSprite uISsSprite = Util.CreateUISsSprite("AnimationP", mPanelPopup.gameObject, text2, new Vector3(-170f, 10f), Vector3.one, base_depth + 3);
		uISsSprite.DestroyAtEnd = false;
		uISsSprite.PlayCount = 0;
		if (mAnimFeatureJewel.ContainsKey(text2))
		{
			mAnimFeatureJewel[text2] = uISsSprite;
		}
		else
		{
			mAnimFeatureJewel.Add(text2, uISsSprite);
		}
		if (!mLayoutTarget.ContainsKey(LayoutTarget.PopupP))
		{
			mLayoutTarget.Add(LayoutTarget.PopupP, uISsSprite.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.PopupP] = uISsSprite.transform;
		}
		text2 = "LABYRINTH_POPUP_LANDSCAPE";
		uISsSprite = Util.CreateUISsSprite("AnimationL", mPanelPopup.gameObject, text2, new Vector3(0f, 115f), Vector3.one, base_depth + 3);
		uISsSprite.DestroyAtEnd = false;
		uISsSprite.PlayCount = 0;
		if (mAnimFeatureJewel.ContainsKey(text2))
		{
			mAnimFeatureJewel[text2] = uISsSprite;
		}
		else
		{
			mAnimFeatureJewel.Add(text2, uISsSprite);
		}
		if (!mLayoutTarget.ContainsKey(LayoutTarget.PopupL))
		{
			mLayoutTarget.Add(LayoutTarget.PopupL, uISsSprite.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.PopupL] = uISsSprite.transform;
		}
		text = "LC_button_ｌist";
		uIButton = Util.CreateJellyImageButton("RewardImage", mGoAnchorT, image);
		Util.SetImageButtonInfo(uIButton, text, text, text, base_depth + 2, new Vector3(91f, -116f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, mGSM, "OnRewardList", UIButtonMessage.Trigger.OnClick);
		if (!mLayoutTarget.ContainsKey(LayoutTarget.RewardList))
		{
			mLayoutTarget.Add(LayoutTarget.RewardList, uIButton.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.RewardList] = uIButton.transform;
		}
	}

	protected override void CreateCockpitItems(int baseDepth)
	{
		base.CreateCockpitItems(baseDepth);
		GameObject parent = mAnchorB.gameObject;
		float num = 0f;
		float num2 = 170f;
		mFooterRoot = Util.CreateGameObject("FooterParent", parent);
		UISprite sprite = Util.CreateSprite("StatusR", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(-2f, num - num2, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite = Util.CreateSprite("StatusL", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(sprite, "menubar_frame", baseDepth, new Vector3(2f, num - num2, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		if (!mLayoutTarget.ContainsKey(LayoutTarget.HowToPlay))
		{
			mLayoutTarget.Add(LayoutTarget.HowToPlay, mHowToPlay.transform);
		}
		else
		{
			mLayoutTarget[LayoutTarget.HowToPlay] = mHowToPlay.transform;
		}
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		GameObject gameObject = mAnchorBR.gameObject;
		GameObject parent2 = mAnchorBL.gameObject;
		Vector2 vector = Util.LogScreenSize();
		mButtonBackToCourse = Util.CreateJellyImageButton("Back2", parent2, HudAtlas);
		Util.SetImageButtonInfo(mButtonBackToCourse, "button_back", "button_back", "button_back", baseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mButtonBackToCourse, mGSM, "OnExitStageSelect", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mButtonBackToCourse);
		mButtonBackToCourse.SetLocalPosition(55f, 55f);
		mAnimNextStageDepth = baseDepth + 3;
		SetLayout(Util.ScreenOrientation);
		SetCourseSelectMode();
	}

	public override void SetLayout(ScreenOrientation o)
	{
		base.SetLayout(o);
		foreach (int value in Enum.GetValues(typeof(LayoutTarget)))
		{
			if (!mLayoutTarget.ContainsKey((LayoutTarget)value) || !mLayoutTarget[(LayoutTarget)value])
			{
				continue;
			}
			Transform transform = mLayoutTarget[(LayoutTarget)value];
			Vector3? vector = null;
			Vector3? vector2 = null;
			float? num = null;
			Vector2? vector3 = null;
			Transform transform2 = null;
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				switch ((LayoutTarget)value)
				{
				case LayoutTarget.HowToPlay:
					vector = new Vector3(-65f, 55f + mPortraitOffset);
					break;
				case LayoutTarget.Course:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(-130f, -111f);
					break;
				case LayoutTarget.Restart:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(-229f, -237f);
					break;
				case LayoutTarget.RewardList:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(91f, -116f);
					break;
				case LayoutTarget.Possess:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(36f, -158f);
					vector2 = Vector3.one;
					break;
				case LayoutTarget.Frame_1:
					transform2 = mGoAnchorT.transform;
					num = 0f;
					vector = new Vector3(2f, 67f);
					vector3 = new Vector2(570f, 267f);
					break;
				case LayoutTarget.Frame_2:
					transform2 = mGoAnchorT.transform;
					num = 0f;
					vector = new Vector3(-2f, 67f);
					vector3 = new Vector2(570f, 267f);
					break;
				case LayoutTarget.Title:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(-136f, -113f);
					vector2 = Vector3.one;
					break;
				case LayoutTarget.Line:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(-136f, -138f);
					vector3 = new Vector2(360f, 20f);
					break;
				case LayoutTarget.Reward:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(229f, -177f);
					vector2 = Vector3.one;
					break;
				case LayoutTarget.Gauge:
					transform2 = mGoAnchorT.transform;
					vector = new Vector3(-299f, -168f);
					break;
				case LayoutTarget.PopupP:
				{
					UISsSprite component4 = transform.GetComponent<UISsSprite>();
					if ((bool)component4)
					{
						Color color4 = component4.color;
						color4.a = 1f;
						component4.color = color4;
					}
					break;
				}
				case LayoutTarget.PopupL:
				{
					UISsSprite component3 = transform.GetComponent<UISsSprite>();
					if ((bool)component3)
					{
						Color color3 = component3.color;
						color3.a = 0f;
						component3.color = color3;
					}
					break;
				}
				default:
					continue;
				}
				if (vector.HasValue)
				{
					vector = new Vector3(vector.Value.x, vector.Value.y - mPortraitOffset);
				}
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
			{
				float offsetRatio = GetOffsetRatio();
				switch ((LayoutTarget)value)
				{
				case LayoutTarget.Course:
					transform2 = mGoAnchorTL.transform;
					vector = new Vector3(165f, -113f);
					break;
				case LayoutTarget.Restart:
					transform2 = mGoAnchorTL.transform;
					vector = new Vector3(366f, -110f);
					break;
				case LayoutTarget.HowToPlay:
				{
					float num10 = 55 + 108 * ((base.mShopflg ? 1 : 0) + (base.mMugenflg ? 1 : 0) + (base.mAllCrushFlg ? 1 : 0)) + (base.mShopflg ? 5 : 0);
					vector = new Vector3(0f - num10, 55f);
					break;
				}
				case LayoutTarget.Possess:
					vector = new Vector3(37f, -372f);
					vector2 = new Vector3(1.2f, 1.2f, 1f);
					break;
				case LayoutTarget.Frame_1:
				{
					transform2 = mGoAnchorL.transform;
					num = 90f;
					vector = new Vector3(0f, 2f);
					float num2 = 0f;
					if (Util.IsWideScreen())
					{
						float num3 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
						float num4 = 0.5625f;
						float num5 = 0.4617052f;
						num2 = (num4 - num3) / (num4 - num5) * 110f;
					}
					vector3 = new Vector2(570f, 267f + num2);
					break;
				}
				case LayoutTarget.Frame_2:
				{
					transform2 = mGoAnchorL.transform;
					num = 90f;
					vector = new Vector3(0f, -2f);
					float num6 = 0f;
					if (Util.IsWideScreen())
					{
						float num7 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
						float num8 = 0.5625f;
						float num9 = 0.4617052f;
						num6 = (num8 - num7) / (num8 - num9) * 110f;
					}
					vector3 = new Vector2(570f, 267f + num6);
					break;
				}
				case LayoutTarget.Title:
					transform2 = mGoAnchorTL.transform;
					vector = new Vector3(127f, -115f + -20f * offsetRatio);
					vector2 = new Vector3(0.7f, 0.7f, 1f);
					break;
				case LayoutTarget.Line:
					transform2 = mGoAnchorTL.transform;
					vector = new Vector3(130f, -135f + -20f * offsetRatio);
					vector3 = new Vector2(243f, 20f);
					break;
				case LayoutTarget.Reward:
					transform2 = mGoAnchorTL.transform;
					vector = new Vector3(130f, -280f + -27f * offsetRatio);
					break;
				case LayoutTarget.RewardList:
					transform2 = mGoAnchorTL.transform;
					vector = new Vector3(175f, -465f + -41f * offsetRatio);
					break;
				case LayoutTarget.Gauge:
					transform2 = mGoAnchorTL.transform;
					vector = new Vector3(5f, -410f + -44f * offsetRatio);
					break;
				case LayoutTarget.PopupP:
				{
					UISsSprite component2 = transform.GetComponent<UISsSprite>();
					if ((bool)component2)
					{
						Color color2 = component2.color;
						color2.a = 0f;
						component2.color = color2;
					}
					break;
				}
				case LayoutTarget.PopupL:
				{
					UISsSprite component = transform.GetComponent<UISsSprite>();
					if ((bool)component)
					{
						Color color = component.color;
						color.a = 1f;
						component.color = color;
					}
					break;
				}
				default:
					continue;
				}
				break;
			}
			}
			if (transform2 != null)
			{
				transform.SetParent(transform2);
			}
			if (vector.HasValue)
			{
				transform.SetLocalPosition(vector.Value);
			}
			if (vector2.HasValue)
			{
				transform.SetLocalScale(vector2.Value);
			}
			if (num.HasValue)
			{
				transform.SetLocalEulerAnglesZ(num.Value);
			}
			if (vector3.HasValue)
			{
				UIWidget component5 = transform.GetComponent<UIWidget>();
				if (component5 != null)
				{
					component5.SetDimensions((int)vector3.Value.x, (int)vector3.Value.y);
				}
			}
		}
		if ((bool)mAnimNextStage)
		{
			float y = 100f;
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mAnimNextStage.SetLocalScale(1f);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				if (Util.IsWideScreen())
				{
					y = 90f;
				}
				mAnimNextStage.SetLocalScale(0.7f, 0.7f, 1f);
				break;
			}
			mAnimNextStage.SetLocalPositionY(y);
		}
		if ((bool)mLabelBonusText)
		{
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mLabelBonusText.SetLocalPosition(-49f, -2f);
				mLabelBonusText.effectStyle = UILabel.Effect.None;
				mLabelBonusText.fontSize = 21;
				mLabelBonusText.spacingX = -2;
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mLabelBonusText.SetLocalPosition(-64f, 5f);
				mLabelBonusText.effectStyle = UILabel.Effect.Outline8;
				mLabelBonusText.fontSize = 16;
				mLabelBonusText.spacingX = 0;
				break;
			}
		}
		if ((bool)mLabelBonusMul)
		{
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mLabelBonusMul.SetLocalPosition(0f, -3f);
				mLabelBonusMul.effectStyle = UILabel.Effect.None;
				mLabelBonusMul.fontSize = 26;
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mLabelBonusMul.SetLocalPosition(-84f, -13f);
				mLabelBonusMul.effectStyle = UILabel.Effect.Outline8;
				mLabelBonusMul.fontSize = 18;
				break;
			}
		}
		if ((bool)mLabelBonusRatio)
		{
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mLabelBonusRatio.SetLocalPosition(10f, -3f);
				mLabelBonusRatio.effectStyle = UILabel.Effect.None;
				mLabelBonusRatio.fontSize = 26;
				mLabelBonusRatio.spacingX = -3;
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mLabelBonusRatio.SetLocalPosition(-73f, -12f);
				mLabelBonusRatio.effectStyle = UILabel.Effect.Outline8;
				mLabelBonusRatio.fontSize = 20;
				mLabelBonusRatio.spacingX = 0;
				break;
			}
		}
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = Vector3.zero;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = new Vector3(0f, -200f, 0f);
			}
			break;
		}
	}

	public void SetCourseSelectMode()
	{
		if (mLayoutTarget.ContainsKey(LayoutTarget.Restart) && (bool)mLayoutTarget[LayoutTarget.Restart] && mLayoutTarget[LayoutTarget.Restart].gameObject.activeSelf)
		{
			mLayoutTarget[LayoutTarget.Restart].gameObject.SetActive(false);
		}
		if (mLayoutTarget.ContainsKey(LayoutTarget.Course) && (bool)mLayoutTarget[LayoutTarget.Course] && mLayoutTarget[LayoutTarget.Course].gameObject.activeSelf)
		{
			mLayoutTarget[LayoutTarget.Course].gameObject.SetActive(false);
		}
		if ((bool)mBackToMap && !mBackToMap.gameObject.activeSelf)
		{
			mBackToMap.gameObject.SetActive(true);
		}
		if ((bool)mButtonBackToCourse && mButtonBackToCourse.gameObject.activeSelf)
		{
			mButtonBackToCourse.gameObject.SetActive(false);
		}
		if (mLayoutTarget.ContainsKey(LayoutTarget.Title) && (bool)mLayoutTarget[LayoutTarget.Title] && !mLayoutTarget[LayoutTarget.Title].gameObject.activeSelf)
		{
			mLayoutTarget[LayoutTarget.Title].gameObject.SetActive(true);
		}
	}

	public void SetStageSelectMode(bool is_playing_course)
	{
		if (mLayoutTarget.ContainsKey(LayoutTarget.Restart) && (bool)mLayoutTarget[LayoutTarget.Restart])
		{
			if (!mLayoutTarget[LayoutTarget.Restart].gameObject.activeSelf)
			{
				mLayoutTarget[LayoutTarget.Restart].gameObject.SetActive(true);
			}
			JellyImageButton component = mLayoutTarget[LayoutTarget.Restart].GetComponent<JellyImageButton>();
			if (component != null)
			{
				component.SetButtonEnable(is_playing_course);
			}
		}
		if (mLayoutTarget.ContainsKey(LayoutTarget.Course) && (bool)mLayoutTarget[LayoutTarget.Course] && !mLayoutTarget[LayoutTarget.Course].gameObject.activeSelf)
		{
			mLayoutTarget[LayoutTarget.Course].gameObject.SetActive(true);
		}
		if ((bool)mSpriteCourseNo)
		{
			GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(mGSM.CurrentEventID);
			if (labyrinthData != null)
			{
				Util.SetSpriteImageName(mSpriteCourseNo, "LC_course" + (labyrinthData.LabyrinthSelectedCourseID + 1), Vector3.one, false);
			}
		}
		if (mLabelBonusRatio != null)
		{
			int courseBonusRate = mEventPageData.EventCourseList[mPlayerEventData.CourseID].CourseBonusRate;
			Util.SetLabelText(mLabelBonusRatio, ((float)courseBonusRate / 10f).ToString("0.0"));
		}
		if ((bool)mBackToMap && mBackToMap.gameObject.activeSelf)
		{
			mBackToMap.gameObject.SetActive(false);
		}
		if ((bool)mButtonBackToCourse && !mButtonBackToCourse.gameObject.activeSelf)
		{
			mButtonBackToCourse.gameObject.SetActive(true);
		}
		if (mLayoutTarget.ContainsKey(LayoutTarget.Title) && (bool)mLayoutTarget[LayoutTarget.Title] && mLayoutTarget[LayoutTarget.Title].gameObject.activeSelf)
		{
			mLayoutTarget[LayoutTarget.Title].gameObject.SetActive(false);
		}
	}

	public void TriggerUpdateJewel()
	{
		mCanUpdateJewel = true;
	}

	private void ChangeAnime(UISsSprite sprite, BIJImage atlas, string anim_key, params CHANGE_PART_INFO[] change_parts)
	{
		if (change_parts == null || sprite == null)
		{
			return;
		}
		sprite.Animation = null;
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(anim_key);
		if (resSsAnimation == null)
		{
			return;
		}
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		if (ssAnime == null)
		{
			return;
		}
		Dictionary<int, CHANGE_PART_INFO> dictionary = new Dictionary<int, CHANGE_PART_INFO>();
		for (int i = 0; i < change_parts.Length; i++)
		{
			dictionary.Add(change_parts[i].partName.GetHashCode(), change_parts[i]);
		}
		SsPartRes[] partList = ssAnime.PartList;
		for (int j = 0; j < partList.Length; j++)
		{
			CHANGE_PART_INFO value;
			if (partList[j].IsRoot || !dictionary.TryGetValue(partList[j].Name.GetHashCode(), out value))
			{
				continue;
			}
			Vector2 offset = atlas.GetOffset(value.spriteName);
			Vector2 size = atlas.GetSize(value.spriteName);
			float x = offset.x;
			float y = offset.y;
			float x2 = offset.x + size.x;
			float y2 = offset.y + size.y;
			partList[j].UVs = new Vector2[4]
			{
				new Vector2(x, y2),
				new Vector2(x2, y2),
				new Vector2(x2, y),
				new Vector2(x, y)
			};
			Vector4 pixelPadding = atlas.GetPixelPadding(value.spriteName);
			size = atlas.GetPixelSize(value.spriteName);
			if (value.enableOffset)
			{
				offset = value.centerOffset;
			}
			else
			{
				offset = new Vector2(partList[j].OriginX, partList[j].OriginY);
				int num = partList[j].PicArea.Right - partList[j].PicArea.Left;
				int num2 = partList[j].PicArea.Bottom - partList[j].PicArea.Top;
				if (num == 1 && num2 == 1)
				{
					offset.x = size.x / 2f;
					offset.y = size.y / 2f;
				}
			}
			offset.x += pixelPadding.x;
			offset.y += pixelPadding.y;
			size.x -= pixelPadding.x + pixelPadding.z;
			size.y -= pixelPadding.y + pixelPadding.w;
			x = 0f - offset.x;
			y = 0f - (size.y - offset.y);
			x2 = size.x - offset.x;
			y2 = offset.y;
			float z = 0f;
			partList[j].OrgVertices = new Vector3[4]
			{
				new Vector3(x, y2, z),
				new Vector3(x2, y2, z),
				new Vector3(x2, y, z),
				new Vector3(x, y, z)
			};
		}
		UISsPart[] parts = sprite.GetParts();
		if (parts != null)
		{
			for (int k = 0; k < parts.Length; k++)
			{
				parts[k].RevertChangedMaterial();
			}
		}
		if (sprite._materials != null)
		{
			for (int l = 0; l < sprite._materials.Length; l++)
			{
				sprite._materials[l] = null;
			}
		}
		sprite.Animation = ssAnime;
		sprite.Play();
	}

	private float GetOffsetRatio()
	{
		float num = 2.1658843f;
		float num2 = 1.3333334f;
		float num3 = num;
		Vector2 vector = Util.LogScreenSize();
		num3 = ((!(vector.x > vector.y)) ? (vector.y / vector.x) : (vector.x / vector.y));
		return (num - num3) / (num - num2);
	}

	public override IEnumerator JumpUpLive2dCharacter(CompanionData.MOTION motion)
	{
		if (mPanelPopup != null)
		{
			NGUITools.SetActive(mPanelPopup.gameObject, false);
		}
		yield return StartCoroutine(base.JumpUpLive2dCharacter(motion));
	}

	public override IEnumerator JumpDownLive2dCharacter(CompanionData.MOTION motion, CompanionData.MOTION after_loop_motion = CompanionData.MOTION.STAY0)
	{
		yield return StartCoroutine(base.JumpDownLive2dCharacter(motion, after_loop_motion));
		if (mPanelPopup != null)
		{
			NGUITools.SetActive(mPanelPopup.gameObject, mIsNextCharacter);
		}
	}

	protected override void OnDestroy()
	{
		if (GameObject.Find("Anchor") != null && mGoAnchorL != null)
		{
			GameMain.SafeDestroy(mGoAnchorL);
		}
		if (mGauge != null)
		{
			mGauge.Dispose();
		}
		mGauge = null;
		base.OnDestroy();
	}
}
