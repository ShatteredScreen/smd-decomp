using System;
using UnityEngine;

[Serializable]
public class SsAnimation : ScriptableObject
{
	public string OriginalPath;

	[HideInInspector]
	public bool UseScaleFactor;

	[HideInInspector]
	public float ScaleFactor = 1f;

	public int FPS;

	public int EndFrame;

	public bool hvFlipForImageOnly;

	public SsImageFile[] ImageList;

	public SsPartRes[] PartList;

	[HideInInspector]
	public int ImportedTime;

	[HideInInspector]
	public int ImportedTimeHigh;

	public void UpdateImportedTime()
	{
		long num = DateTime.Now.ToBinary();
		ImportedTime = (int)num;
		ImportedTimeHigh = (int)(num >> 32);
	}

	public bool EqualsImportedTime(int importedTime, int importedTimeHigh)
	{
		return ImportedTime == importedTime && ImportedTimeHigh == importedTimeHigh;
	}

	public static bool operator true(SsAnimation p)
	{
		return p != null;
	}

	public static bool operator false(SsAnimation p)
	{
		return p == null;
	}
}
