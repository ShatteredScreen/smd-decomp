using System;
using System.Collections.Generic;

public class BIJSNSPostData : BIJSNSData<BIJSNSPostData>, ICloneable, ICopyable, IBIJSNSPostData
{
	public string ToID
	{
		get
		{
			if (ToIDs != null && ToIDs.Length > 0)
			{
				return ToIDs[0];
			}
			return null;
		}
		set
		{
			if (ToIDs == null || ToIDs.Length < 1)
			{
				ToIDs = new string[1] { value };
			}
		}
	}

	public string PostID
	{
		get
		{
			if (PostIDs != null && PostIDs.Length > 0)
			{
				return PostIDs[0];
			}
			return null;
		}
		set
		{
			if (PostIDs == null || PostIDs.Length < 1)
			{
				PostIDs = new string[1] { value };
			}
		}
	}

	public string[] ToIDs { get; set; }

	public string[] PostIDs { get; set; }

	public string Title { get; set; }

	public string Message { get; set; }

	public string ImageURL { get; set; }

	public string LinkURL { get; set; }

	public string MimeType { get; set; }

	public string FileName { get; set; }

	public List<string> ExcludeIds { get; set; }
}
