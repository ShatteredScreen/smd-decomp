using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowupResultDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0
	}

	public enum PLACE
	{
		MENU = 50,
		GETTING = 51
	}

	public enum STATE
	{
		INIT = 0,
		ANIMATION_LEVELUP = 1,
		ANIMATION_LEVELUP_WAIT = 2,
		ANIMATION_BEFORE_HIT = 3,
		ANIMATION_HIT = 4,
		ANIMATION_HIT_WAIT = 5,
		ANIMATION_WIN = 6,
		ANIMATION_WIN_WAIT = 7,
		MAIN = 8,
		MAIN2 = 9,
		WAIT = 10
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private PLACE mPlace = PLACE.GETTING;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private OnDialogClosed mCallback;

	private Partner mPartner;

	private CompanionData mCompanionData;

	private BIJImage atlas;

	private UISsSprite mPartnerHalo;

	private UISsSprite mLevelupAnime;

	private UIButton mButton;

	private float mStateTime;

	private bool mTutorialCurrent;

	private AccessoryData mData;

	private MapAvater mMapAvater;

	public AccessoryData Data
	{
		get
		{
			return mData;
		}
	}

	public bool IsReplayStamp { get; private set; }

	public bool CanRepeat { get; set; }

	public void SetPlace(PLACE aPlace)
	{
		mPlace = aPlace;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(AccessoryData a_data, int a_companionID, bool a_addAccessory = true, AccessoryData.ACCESSORY_TYPE a_type = AccessoryData.ACCESSORY_TYPE.NONE, bool a_close = false)
	{
		IsReplayStamp = a_close;
		mData = a_data;
		mCompanionData = mGame.mCompanionData[a_companionID];
		if (mCompanionData == null)
		{
			BIJLog.E("Companion Data is NULL !!!");
		}
		if (!a_addAccessory)
		{
			return;
		}
		int companionLevel = mGame.mPlayer.GetCompanionLevel(a_companionID);
		mGame.mPlayer.AddCompanionLevel(mCompanionData.index);
		if (mData != null)
		{
			if (mData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GROWUP)
			{
				mGame.mPlayer.AddAccessory(mData, true, true);
				mGame.mPlayer.SubGrowup(1);
			}
			else if (mData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GROWUP_F)
			{
				mGame.mPlayer.AddAccessory(mData, true, true);
			}
			else
			{
				BIJLog.E("Growup AccessoryData Type ERROR!! : " + mData.AccessoryType);
			}
		}
		else
		{
			switch (a_type)
			{
			case AccessoryData.ACCESSORY_TYPE.GROWUP:
				mGame.mPlayer.SubGrowup(1);
				break;
			case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
				if (mGame.mPlayer.GrowUpPiece >= 24)
				{
					mGame.mPlayer.SubGrowupPiece(24);
				}
				else
				{
					mGame.mPlayer.SubGrowup(1);
				}
				break;
			}
		}
		if (mGame.mPlayer.GetCompanionLevel(a_companionID) == 5)
		{
			MissionManager.UpdateMission(25, 1);
		}
		mGame.Save();
		mGame.AchievementPartnerLvlMax();
		int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
		int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
		int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
		int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
		int companionLevel2 = mGame.mPlayer.GetCompanionLevel(a_companionID);
		int place = (int)mPlace;
		string text = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
		string text2 = string.Empty;
		for (int i = 0; i < mGame.mCompanionData.Count; i++)
		{
			byte b = 0;
			if (mGame.mPlayer.IsCompanionUnlock(i))
			{
				b = (byte)mGame.mPlayer.GetCompanionLevel(i);
			}
			if (b > 9 && b > 35)
			{
				b = 0;
			}
			text2 += text.Substring(b, 1);
		}
		ServerCram.CharacterLevelUP(a_companionID, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, companionLevel, companionLevel2, text2, place);
	}

	public void Init(bool a_close)
	{
		IsReplayStamp = a_close;
	}

	public void MapUpdateInit(MapAvater a_avater)
	{
		mMapAvater = a_avater;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mState.Change(STATE.ANIMATION_LEVELUP);
			break;
		case STATE.ANIMATION_LEVELUP:
		{
			mLevelupAnime = Util.CreateGameObject("PartnerHalo", base.gameObject).AddComponent<UISsSprite>();
			mLevelupAnime.Animation = ResourceManager.LoadSsAnimation("LEVELUP_ANIM").SsAnime;
			mLevelupAnime.depth = mBaseDepth + 2;
			mLevelupAnime.gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
			mLevelupAnime.Play();
			mLevelupAnime.PlayCount = 1;
			UISsSprite uISsSprite = mLevelupAnime;
			uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnLevelUpAnimationFinished));
			mGame.PlaySe("VOICE_004", -1);
			mState.Change(STATE.ANIMATION_LEVELUP_WAIT);
			break;
		}
		case STATE.ANIMATION_BEFORE_HIT:
			if (mLevelupAnime != null)
			{
				UnityEngine.Object.Destroy(mLevelupAnime.gameObject);
				mLevelupAnime = null;
			}
			mState.Change(STATE.ANIMATION_HIT);
			break;
		case STATE.ANIMATION_HIT:
			mPartner.StartMotion(CompanionData.MOTION.HIT0, false);
			BuildDialogInternal();
			mState.Change(STATE.ANIMATION_HIT_WAIT);
			break;
		case STATE.ANIMATION_HIT_WAIT:
			if (mPartner.IsMotionFinished())
			{
				mState.Change(STATE.ANIMATION_WIN);
			}
			break;
		case STATE.ANIMATION_WIN:
			mPartner.StartMotion(CompanionData.MOTION.WIN, true);
			mState.Change(STATE.ANIMATION_WIN_WAIT);
			break;
		case STATE.ANIMATION_WIN_WAIT:
			if (mState.IsChanged())
			{
				mStateTime = 0f;
			}
			mStateTime += Time.deltaTime;
			if (mStateTime > 0.2f)
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.MAIN:
			if (mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL9_7))
			{
				int num = 0;
				List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
				for (int i = 0; i < playStage.Count; i++)
				{
					if (playStage[i].Series == 0)
					{
						num = playStage[i].Level;
						break;
					}
				}
				if (num == 900)
				{
					mButton = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
					Util.SetImageButtonInfo(mButton, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
					Util.AddImageButtonMessage(mButton, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
					mState.Change(STATE.MAIN2);
				}
			}
			if (!mTutorialCurrent)
			{
				mButton = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
				Util.SetImageButtonInfo(mButton, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
				Util.AddImageButtonMessage(mButton, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
				mState.Change(STATE.MAIN2);
			}
			break;
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		mPartner.transform.localPosition = Vector3.zero;
		int level = mGame.mPlayer.GetCompanionLevel(mCompanionData.index);
		mPartner.Init(mCompanionData.index, Vector3.zero, 1f, level, base.gameObject, true, mBaseDepth + 2);
		while (!mPartner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
	}

	private void BuildDialogInternal()
	{
		UIFont atlasFont = GameMain.LoadFont();
		int companionLevel = mGame.mPlayer.GetCompanionLevel(mCompanionData.index);
		int num = companionLevel;
		string imageName = ((companionLevel >= mCompanionData.maxLevel) ? Def.CharacterViewLevelMaxImageTbl[companionLevel] : Def.CharacterViewLevelImageTbl[companionLevel]);
		UISprite sprite = Util.CreateSprite("lvl", base.gameObject, atlas);
		Util.SetSpriteInfo(sprite, imageName, mBaseDepth + 2, new Vector3(-82f, 215f, 0f), Vector3.one, false);
		string empty = string.Empty;
		empty += Util.MakeLText("GrowupResult_Desk");
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, empty, mBaseDepth + 2, new Vector3(70f, 209f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		empty = ((mCompanionData.motionType == 0) ? Util.MakeLText("PartnerDesc_CommonGrow" + num) : ((num != 4) ? Util.MakeLText("PartnerDesc_CommonGrow" + num) : Util.MakeLText("PartnerDesc_CommonGrow" + num + "_02")));
		uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, empty, mBaseDepth + 2, new Vector3(0f, -155f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mPartnerHalo = Util.CreateGameObject("PartnerHalo", base.gameObject).AddComponent<UISsSprite>();
		mPartnerHalo.Animation = ResourceManager.LoadSsAnimation("MAP_HALO").SsAnime;
		mPartnerHalo.depth = mBaseDepth + 1;
		mPartnerHalo.gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1f);
		mPartnerHalo.gameObject.transform.localPosition = new Vector3(0f, 35f, 0f);
		mPartnerHalo.Play();
		mPartner.SetPartnerUIScreenDepth(mBaseDepth + 5);
		if (mCompanionData.IsTall())
		{
			mPartner.SetPartnerScreenScale(0.5f);
		}
		else
		{
			mPartner.SetPartnerScreenScale(0.58f);
		}
		mPartner.SetPartnerScreenPos(new Vector3(0f, -98f, 0f));
		mPartner.SetPartnerScreenEnable(true);
		mPartner.MakeUIShadow(atlas, "character_foot2", mBaseDepth + 3);
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		string titleDescKey = Localization.Get("GrowupResult_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		if (mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL9_7))
		{
			return;
		}
		int num = 0;
		List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
		for (int i = 0; i < playStage.Count; i++)
		{
			if (playStage[i].Series == 0)
			{
				num = playStage[i].Level;
				break;
			}
		}
		if (num == 900)
		{
			mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL9_6, base.ParentGameObject, delegate
			{
			}, delegate
			{
			}, true);
			mTutorialCurrent = true;
		}
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN2)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void OnOpenFinished()
	{
		if (mMapAvater != null)
		{
			mMapAvater.SetIcon();
		}
		mGame.Network_NicknameEdit(string.Empty);
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mPartner != null)
		{
			UnityEngine.Object.Destroy(mPartner.gameObject);
		}
		if (mPartnerHalo != null)
		{
			UnityEngine.Object.Destroy(mPartnerHalo.gameObject);
		}
		if (mLevelupAnime != null)
		{
			UnityEngine.Object.Destroy(mLevelupAnime.gameObject);
		}
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.MAIN2, true);
		}
	}

	private void OnLevelUpAnimationFinished(UISsSprite sprite)
	{
		mState.Change(STATE.ANIMATION_BEFORE_HIT);
	}
}
