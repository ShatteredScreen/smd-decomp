public class SMEventCourseSetting : SMMapPageSettingBase
{
	public short StageNum;

	public int ClearAccessoryID;

	public int CompleteAccessoryID;

	public string RouteAnimeKey = string.Empty;

	public string SignAnimeKey = string.Empty;

	public string CourseClearAnimeKey = string.Empty;

	public string CourseCompleteAnimeKey = string.Empty;

	public string CourseTitleKey = string.Empty;

	public string CourseClearDescKey = string.Empty;

	public int OpenAccessoryID = -1;

	public int Difficulty;

	public int CourseBonusRate = 10;

	public int CourseClearRewardNum;

	public int JewelUpChanceCondition;

	public int CourseStarCompleteAccessoryID;

	public int CourseStagesAllClearRewardNum;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteShort((short)mCourseNo);
		a_writer.WriteShort(StageNum);
		a_writer.WriteInt(ClearAccessoryID);
		a_writer.WriteInt(CompleteAccessoryID);
		a_writer.WriteUTF(RouteAnimeKey);
		a_writer.WriteUTF(SignAnimeKey);
		a_writer.WriteUTF(CourseClearAnimeKey);
		a_writer.WriteUTF(CourseCompleteAnimeKey);
		a_writer.WriteUTF(CourseTitleKey);
		a_writer.WriteUTF(CourseClearDescKey);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		mCourseNo = a_reader.ReadShort();
		StageNum = a_reader.ReadShort();
		ClearAccessoryID = a_reader.ReadInt();
		CompleteAccessoryID = a_reader.ReadInt();
		RouteAnimeKey = a_reader.ReadUTF();
		SignAnimeKey = a_reader.ReadUTF();
		CourseClearAnimeKey = a_reader.ReadUTF();
		CourseCompleteAnimeKey = a_reader.ReadUTF();
		CourseTitleKey = a_reader.ReadUTF();
		CourseClearDescKey = a_reader.ReadUTF();
	}

	public void ConvertToEachEvent(Def.EVENT_TYPE a_eventType)
	{
		if (a_eventType == Def.EVENT_TYPE.SM_LABYRINTH)
		{
			OpenAccessoryID = ClearAccessoryID;
			Difficulty = CompleteAccessoryID;
			CourseBonusRate = int.Parse(RouteAnimeKey);
			CourseClearRewardNum = int.Parse(SignAnimeKey);
			JewelUpChanceCondition = int.Parse(CourseClearAnimeKey);
			string text = CourseCompleteAnimeKey;
			if (text.CompareTo("NULL") == 0)
			{
				text = "0";
			}
			CourseStarCompleteAccessoryID = int.Parse(text);
			CourseStagesAllClearRewardNum = int.Parse(CourseTitleKey);
		}
	}
}
