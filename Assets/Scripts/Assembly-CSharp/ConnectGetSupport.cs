using System;
using System.Collections.Generic;

public class ConnectGetSupport : ConnectBase
{
	protected class JsonSerializerTemp
	{
		public List<GetMailResponseSupport> results { get; set; }
	}

	protected Server.MailType mMailType;

	protected string mJson;

	protected bool mNetworkDataModified;

	public void SetResponseData(Server.MailType a_kind, string a_json)
	{
		mMailType = a_kind;
		mJson = a_json;
	}

	protected override void InitServerFlg()
	{
		GameMain.mMapSupportCheckDone = false;
		GameMain.mMapSupportSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mMapSupportCheckDone = true;
		GameMain.mMapSupportSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mMapSupportCheckDone = true;
		GameMain.mMapSupportSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetSupportTime, DateTime.Now);
		long gET_SUPPORT_FREQ = GameMain.GET_SUPPORT_FREQ;
		if (gET_SUPPORT_FREQ > 0 && num < gET_SUPPORT_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			ServerFlgSucceeded();
			base.StateStart();
			return;
		}
		if (mParam == null)
		{
			mParam = new ConnectGetSupportParam(-1);
			mParam.SetIgnoreError(true);
		}
		ConnectGetSupportParam connectGetSupportParam = mParam as ConnectGetSupportParam;
		int num = connectGetSupportParam.mCustomReceiveCount;
		if (num == -1)
		{
			num = mGame.GameProfile.MailGetCount;
			if (mGame.GameProfile == null || mGame.GameProfile.MailGetCount == 0)
			{
				GameMain.MapSupportRequestCount = Constants.GIFT_AMOUNT;
			}
			else
			{
				GameMain.MapSupportRequestCount = mGame.GameProfile.MailGetCount;
			}
		}
		InitServerFlg();
		if (!Server.GetSupportGift(num))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		try
		{
			JsonSerializerTemp obj = new JsonSerializerTemp();
			new MiniJSONSerializer().Populate(ref obj, mJson);
			List<GetMailResponseSupport> list = new List<GetMailResponseSupport>();
			if (obj != null)
			{
				list.AddRange(obj.results);
			}
			if (list != null && list.Count > 0)
			{
				List<int> list2 = new List<int>();
				foreach (GetMailResponseSupport item in list)
				{
					GiftItemData giftItemData = item.GetGiftItemData();
					GiftItem giftItem = new GiftItem();
					giftItem.SetData(giftItemData);
					giftItem.Data.SetGiftSource((Server.MailType)giftItemData.MailType);
					if (giftItem.Data.ItemCategory == 23)
					{
						mPlayer.ResetTimeCheaterPenalty(giftItem.Data.Quantity);
						int num = 0;
						if (GameMain.ServerTime.HasValue)
						{
							num = (int)mGame.MaintenanceProfile.ServerTime;
						}
						int num2 = (int)DateTimeUtil.DateTimeToUnixTimeStamp(DateTime.Now.ToUniversalTime());
						int num3 = (int)DateTimeUtil.DateTimeToUnixTimeStamp(mPlayer.Data.LastPlayTime.ToUniversalTime());
						ServerCram.CheatUserSut(num, num2, num3, -1);
					}
					else if (mGame.CheckCorrectReceiveGiftItem(giftItem))
					{
						GameMain.MapSupportReceiveCount++;
						mPlayer.AddGift(giftItem);
					}
					list2.Add(giftItem.Data.RemoteID);
					mNetworkDataModified = true;
				}
				if (list2.Count > 0)
				{
					Server.GotSupport(Server.MailType.SUPPORT, list2);
				}
			}
			mPlayer.Data.LastGetSupportTime = DateTime.Now;
			if (mNetworkDataModified)
			{
				mGame.Save();
			}
		}
		catch (Exception ex)
		{
			BIJLog.E("GetSupport Error : " + ex);
		}
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
			mConnectResultKind = 3;
			base.StateConnectFinish();
			return false;
		}
		return false;
	}
}
