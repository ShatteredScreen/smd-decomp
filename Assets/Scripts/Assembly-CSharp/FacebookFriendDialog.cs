using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class FacebookFriendDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		NETWORK_00 = 3,
		NETWORK_00_1 = 4,
		NETWORK_00_2 = 5,
		NETWORK_01_1 = 6,
		NETWORK_01_2 = 7,
		NETWORK_FRIEND_02_1 = 8,
		NETWORK_FRIEND_02_2 = 9
	}

	public delegate void OnDialogClosed(bool empty);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private bool mIsStage;

	private int mSeries;

	private int mStageNo;

	private UIInput mInputs;

	private string mIDdata;

	private bool mApplyUserSucceed;

	private bool mApplyUserCheckDone;

	private bool mSaveflg;

	private SMVerticalListInfo mFacebookUserListInfo;

	private SMVerticalList mFacebookUserList;

	private List<SMVerticalListItem> mList = new List<SMVerticalListItem>();

	private List<FacebookIconCheckData> mFacebookIconCheckData = new List<FacebookIconCheckData>();

	private MyFaceBookFriend mSelectedData;

	private bool mFriendAllbutton;

	private STATE mFirstState = STATE.NETWORK_00;

	private ConfirmDialog mConfirmDialog;

	private OnDialogClosed mCallback;

	private FBUserManager mFBUserManager;

	private UILabel mRemainLabel;

	public override void Start()
	{
		if (mGame.mFbUserMngFbFriend == null)
		{
			mGame.mFbUserMngFbFriend = FBUserManager.CreateInstance();
		}
		mFBUserManager = SingletonMonoBehaviour<GameMain>.Instance.mFbUserMngFbFriend;
		mFBUserManager.SetPriority(true);
		mGame.mFbUserMngFbFriend.SetFBUserList(mGame.FaceBookUserList);
		base.Start();
		Server.GiveApplyEvent += Server_OnGiveApply;
	}

	public override void OnDestroy()
	{
		mFBUserManager.SetPriority(false);
		base.OnDestroy();
	}

	public void Init()
	{
		mIsStage = false;
		if (!mIsStage)
		{
			mFirstState = STATE.MAIN;
		}
	}

	public void Init(int a_series, int a_stageno)
	{
		mIsStage = true;
		mSeries = a_series;
		mStageNo = a_stageno;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
		{
			if (mFacebookIconCheckData.Count <= 0)
			{
				break;
			}
			BIJImage image = ResourceManager.LoadImage("ICON").Image;
			int num = mBaseDepth + 5;
			for (int num2 = mFacebookIconCheckData.Count - 1; num2 >= 0; num2--)
			{
				FacebookIconCheckData facebookIconCheckData = mFacebookIconCheckData[num2];
				FBUserData.STATE iconStatus = mFBUserManager.GetIconStatus(facebookIconCheckData.uuid);
				if (iconStatus == FBUserData.STATE.SUCCESS || iconStatus == FBUserData.STATE.FAILURE)
				{
					if (facebookIconCheckData.loadingIcon != null)
					{
						Object.Destroy(facebookIconCheckData.loadingIcon);
					}
					Texture2D icon = mFBUserManager.GetIcon(facebookIconCheckData.uuid);
					if (icon != null)
					{
						UITexture uITexture = Util.CreateGameObject("Icon", facebookIconCheckData.boardImage.gameObject).AddComponent<UITexture>();
						uITexture.depth = num + 2;
						uITexture.transform.localPosition = new Vector3(-202f, -8f, 0f);
						uITexture.mainTexture = icon;
						uITexture.SetDimensions(65, 65);
						int num3 = facebookIconCheckData.iconid - 10000;
						if (num3 >= mGame.mCompanionData.Count)
						{
							num3 = 0;
						}
						string iconName = mGame.mCompanionData[num3].iconName;
						UISprite uISprite = Util.CreateSprite("CharaIcon", uITexture.gameObject, image);
						Util.SetSpriteInfo(uISprite, iconName, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
						uISprite.SetDimensions(45, 45);
					}
					else
					{
						string imageName = "icon_chara_facebook";
						UISprite uISprite2 = Util.CreateSprite("Icon", facebookIconCheckData.boardImage.gameObject, image);
						Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-202f, -8f, 0f), Vector3.one, false);
						uISprite2.SetDimensions(65, 65);
						int num4 = facebookIconCheckData.iconid - 10000;
						if (num4 >= mGame.mCompanionData.Count)
						{
							num4 = 0;
						}
						string iconName2 = mGame.mCompanionData[num4].iconName;
						UISprite uISprite3 = Util.CreateSprite("CharaIcon", uISprite2.gameObject, image);
						Util.SetSpriteInfo(uISprite3, iconName2, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
						uISprite3.SetDimensions(45, 45);
					}
					mFacebookIconCheckData.RemoveAt(num2);
				}
			}
			break;
		}
		case STATE.NETWORK_00_1:
			mState.Change(STATE.NETWORK_00_2);
			break;
		case STATE.NETWORK_00_2:
			BuildDialogList(mGame.FaceBookUserList);
			mState.Change(STATE.MAIN);
			break;
		case STATE.NETWORK_01_1:
			mState.Change(STATE.NETWORK_01_2);
			break;
		case STATE.NETWORK_01_2:
			if (!mFriendAllbutton)
			{
				mState.Change(STATE.MAIN);
			}
			else
			{
				OnAllFriendPushed();
			}
			break;
		case STATE.NETWORK_FRIEND_02_1:
			mState.Change(STATE.NETWORK_FRIEND_02_2);
			break;
		case STATE.NETWORK_FRIEND_02_2:
			mState.Change(STATE.MAIN);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("RandomFriend_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		titleDescKey = string.Format(Localization.Get("Com_Notice"));
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 1, new Vector3(0f, 206f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.54509807f, 0.38431373f, 0.18039216f, 1f);
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UISprite uISprite = Util.CreateSprite("Display", base.gameObject, image2);
		Util.SetSpriteInfo(uISprite, "instruction_panel7", mBaseDepth + 1, new Vector3(0f, -155f), Vector3.one, false);
		uISprite.SetDimensions(494, 68);
		uISprite.type = UIBasicSprite.Type.Sliced;
		mInputs = Util.CreateInput("Input", uISprite.gameObject);
		Util.SetInputInfo(mInputs, uIFont, 24, Def.InputName, image2, "instruction_panel2", mBaseDepth + 1, new Vector3(-62f, 0f, 0f), Vector3.one, 332, 46, UIWidget.Pivot.Center, 0f, string.Empty);
		Util.SetInputType(mInputs);
		string imageName = "icon_sns00";
		UISprite uISprite2 = Util.CreateSprite("FacebookIcon", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite2, imageName, mBaseDepth + 1, new Vector3(-211f, 0f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(50, 50);
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", uISprite.gameObject, image2);
		Util.SetImageButtonInfo(button, "LC_button_search2", "LC_button_search2", "LC_button_search2", mBaseDepth + 3, new Vector3(173f, 1.7f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnIdsearchPushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image2);
		Util.SetImageButtonInfo(button, "LC_button_application_all", "LC_button_application_all", "LC_button_application_all", mBaseDepth + 1, new Vector3(0f, -228f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnAllFriendPushed", UIButtonMessage.Trigger.OnClick);
		titleDescKey = string.Format(Localization.Get("FriendTotalNum"), mGame.mPlayer.Friends.Count, Constants.GAME_FRIEND_MAX);
		uILabel = Util.CreateLabel("FriendNumDesk", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(208f, -250f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpenFinished()
	{
		if (!mIsStage)
		{
			BuildDialogList(mGame.FaceBookUserList);
		}
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSaveflg);
		}
		Server.GiveApplyEvent -= Server_OnGiveApply;
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void BuildDialogList(List<MyFaceBookFriend> a_list, bool serach = false)
	{
		if (a_list.Count == 0)
		{
			BuildDialogNoApplyError();
			return;
		}
		mList.Clear();
		for (int i = 0; i < a_list.Count; i++)
		{
			MyFaceBookFriend myFaceBookFriend = a_list[i];
			if (myFaceBookFriend.UUID != mGame.mPlayer.UUID && !mGame.mPlayer.Friends.ContainsKey(myFaceBookFriend.UUID) && !mGame.mPlayer.ApplyFriends.ContainsKey(myFaceBookFriend.UUID))
			{
				myFaceBookFriend.FbName = mGame.FBNameData[myFaceBookFriend.Fbid];
				try
				{
					myFaceBookFriend.FbName = myFaceBookFriend.FbName.Remove(16);
				}
				catch
				{
				}
				if (!serach)
				{
					mList.Add(myFaceBookFriend);
				}
				else if (myFaceBookFriend.FbName.StartsWith(mIDdata))
				{
					mList.Add(myFaceBookFriend);
				}
			}
		}
		if (mList.Count == 0)
		{
			BuildDialogNoApplyError();
			return;
		}
		float posX = 0f;
		float num = 512f;
		mFacebookUserListInfo = new SMVerticalListInfo(1, num, 310f, 1, num, 310f, 1, 120);
		if (mFacebookUserList != null)
		{
			Object.Destroy(mFacebookUserList.gameObject);
		}
		mFacebookUserList = SMVerticalList.Make(base.gameObject, this, mFacebookUserListInfo, mList, posX, 36f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mFacebookUserList.OnCreateItem = OnCreateItem;
	}

	private void OnIdsearchPushed()
	{
		mIDdata = GetInputPassword();
		BuildDialogList(mGame.FaceBookUserList, true);
	}

	private string GetInputPassword()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(mInputs.value);
		return stringBuilder.ToString();
	}

	private void BuildDialogError()
	{
		if (mConfirmDialog != null)
		{
			Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01");
		mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(delegate
		{
			mState.Reset(STATE.MAIN);
		});
		mState.Reset(STATE.WAIT, true);
	}

	private void BuildDialogNoApplyError()
	{
		if (mConfirmDialog != null)
		{
			Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("ID_Search_NotFound_Title"), Localization.Get("ID_Search_NotFound"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(delegate
		{
			mState.Reset(STATE.MAIN);
		});
		mState.Reset(STATE.WAIT, true);
	}

	private void BuildDialogApplyError()
	{
		if (mConfirmDialog != null)
		{
			Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Friend_Request_Result_NG"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(delegate
		{
			mState.Reset(STATE.MAIN);
		});
		mState.Reset(STATE.WAIT, true);
	}

	private void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		MyFaceBookFriend myFaceBookFriend = item as MyFaceBookFriend;
		float cellWidth = mFacebookUserList.mList.cellWidth;
		float cellHeight = mFacebookUserList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		string imageName = "icon_chara_facebook";
		string text = "lv_1";
		int num2 = 0;
		int num3 = 0;
		if (mGame.mCompanionData.Count > myFaceBookFriend.IconID)
		{
			num3 = myFaceBookFriend.IconID;
		}
		if (num3 >= 0)
		{
			CompanionData companionData = mGame.mCompanionData[num3];
			imageName = companionData.iconName;
		}
		int iconLevel = myFaceBookFriend.IconLevel;
		text = "lv_" + iconLevel;
		int a_main;
		int a_sub;
		Def.SplitStageNo(myFaceBookFriend.Level, out a_main, out a_sub);
		string text2 = string.Format(Localization.Get("Stage_Data"), a_main);
		string text3 = myFaceBookFriend.Name;
		string fbName = myFaceBookFriend.FbName;
		UISprite uISprite2 = Util.CreateSprite("Instruction", itemGO.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel2", num, new Vector3(0f, 19f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(490, 108);
		if (myFaceBookFriend.IconID > 9999)
		{
			Texture2D icon = mFBUserManager.GetIcon(myFaceBookFriend.UUID);
			switch (mFBUserManager.GetIconStatus(myFaceBookFriend.UUID))
			{
			case FBUserData.STATE.DEFAULT:
			case FBUserData.STATE.FAILURE:
			{
				string imageName2 = "icon_chara_facebook";
				UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite4, imageName2, num + 2, new Vector3(-202f, -8f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(65, 65);
				int num6 = myFaceBookFriend.IconID - 10000;
				if (num6 >= mGame.mCompanionData.Count)
				{
					num6 = 0;
				}
				string iconName3 = mGame.mCompanionData[num6].iconName;
				UISprite uISprite6 = Util.CreateSprite("CharaIcon", uISprite4.gameObject, image2);
				Util.SetSpriteInfo(uISprite6, iconName3, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
				uISprite6.SetDimensions(45, 45);
				break;
			}
			case FBUserData.STATE.LOADING:
			{
				string imageName2 = "icon_chara_facebook02";
				UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite4, imageName2, num + 2, new Vector3(-202f, -8f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(65, 65);
				FacebookIconCheckData facebookIconCheckData = new FacebookIconCheckData();
				facebookIconCheckData.boardImage = uISprite2;
				facebookIconCheckData.loadingIcon = uISprite4;
				facebookIconCheckData.uuid = myFaceBookFriend.UUID;
				facebookIconCheckData.iconid = myFaceBookFriend.IconID;
				mFacebookIconCheckData.Add(facebookIconCheckData);
				break;
			}
			case FBUserData.STATE.SUCCESS:
				icon = mFBUserManager.GetIcon(myFaceBookFriend.UUID);
				if (icon != null)
				{
					UITexture uITexture = Util.CreateGameObject("Icon", uISprite2.gameObject).AddComponent<UITexture>();
					uITexture.depth = num + 2;
					uITexture.transform.localPosition = new Vector3(-202f, -8f, 0f);
					uITexture.mainTexture = icon;
					uITexture.SetDimensions(65, 65);
					int num4 = myFaceBookFriend.IconID - 10000;
					if (num4 >= mGame.mCompanionData.Count)
					{
						num4 = 0;
					}
					string iconName = mGame.mCompanionData[num4].iconName;
					UISprite uISprite3 = Util.CreateSprite("CharaIcon", uITexture.gameObject, image2);
					Util.SetSpriteInfo(uISprite3, iconName, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
					uISprite3.SetDimensions(45, 45);
				}
				else
				{
					string imageName2 = "icon_chara_facebook";
					UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
					Util.SetSpriteInfo(uISprite4, imageName2, num + 2, new Vector3(-202f, -8f, 0f), Vector3.one, false);
					uISprite4.SetDimensions(65, 65);
					int num5 = myFaceBookFriend.IconID - 10000;
					if (num5 >= mGame.mCompanionData.Count)
					{
						num5 = 0;
					}
					string iconName2 = mGame.mCompanionData[num5].iconName;
					UISprite uISprite5 = Util.CreateSprite("CharaIcon", uISprite4.gameObject, image2);
					Util.SetSpriteInfo(uISprite5, iconName2, num + 4, new Vector3(28f, 28f, 0f), Vector3.one, false);
					uISprite5.SetDimensions(45, 45);
				}
				break;
			}
		}
		else
		{
			uISprite = Util.CreateSprite("FriendIcon", uISprite2.gameObject, image2);
			Util.SetSpriteInfo(uISprite, imageName, num + 1, new Vector3(-194f, 3f, 0f), Vector3.one, false);
			uISprite.SetDimensions(80, 80);
			uISprite = Util.CreateSprite("CharaLv", uISprite2.gameObject, image);
			Util.SetSpriteInfo(uISprite, text, num + 2, new Vector3(-194f, -37f, 0f), Vector3.one, false);
		}
		string imageName3 = "icon_sns00";
		uISprite = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
		Util.SetSpriteInfo(uISprite, imageName3, num + 1, new Vector3(-130f, 28f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(41, 41);
		UILabel uILabel = Util.CreateLabel("userFBNameDesc", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, fbName, num + 1, new Vector3(-7f, 24f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(192, 26);
		uISprite = Util.CreateSprite("Linetop", uISprite2.gameObject, image);
		Util.SetSpriteInfo(uISprite, "line00", num + 1, new Vector3(-24f, 7f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(243, 6);
		uILabel = Util.CreateLabel("userNameDesc", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text3, num + 1, new Vector3(-7f, -17f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(192, 26);
		uISprite = Util.CreateSprite("LineDown", uISprite2.gameObject, image);
		Util.SetSpriteInfo(uISprite, "line00", num + 1, new Vector3(-24f, -34f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(243, 6);
		string text4 = string.Format(Localization.Get("MyStage_Desk"));
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", uISprite2.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_application2", "LC_button_application2", "LC_button_application2", num + 1, new Vector3(173f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnFriendPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void OnFriendPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mGame.mPlayer.Friends.Count >= Constants.GAME_FRIEND_MAX)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mConfirmDialog = Util.CreateGameObject("LimitFriendDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Approval_Friend_Title"), Localization.Get("Friend_Request_Result_Mine_Full"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Change(STATE.WAIT);
			return;
		}
		MyFaceBookFriend myFaceBookFriend = mFacebookUserList.FindListItem(go) as MyFaceBookFriend;
		if (myFaceBookFriend == null)
		{
			BuildDialogApplyError();
			return;
		}
		mSelectedData = myFaceBookFriend;
		Network_GiveApply(myFaceBookFriend.UUID);
		mState.Change(STATE.NETWORK_00_1);
	}

	private void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
	}

	public void OnAllFriendPushed()
	{
		List<MyFaceBookFriend> faceBookUserList = mGame.FaceBookUserList;
		if (mGame.mPlayer.Friends.Count >= Constants.GAME_FRIEND_MAX)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mConfirmDialog = Util.CreateGameObject("LimitFriendDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Approval_Friend_Title"), Localization.Get("Friend_Request_Result_Mine_Full"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Change(STATE.WAIT);
			return;
		}
		if (mGame.mPlayer.Friends.Count + (faceBookUserList.Count - 1) >= Constants.GAME_FRIEND_MAX)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mConfirmDialog = Util.CreateGameObject("LimitFriendDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Friend_Request_Result_Mine_Error_T"), Localization.Get("Friend_Request_Result_Mine_Error_D"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Change(STATE.WAIT);
			return;
		}
		int num = 0;
		for (int i = 0; i < faceBookUserList.Count; i++)
		{
			MyFaceBookFriend myFaceBookFriend = faceBookUserList[i];
			if (myFaceBookFriend.UUID != mGame.mPlayer.UUID && !mGame.mPlayer.Friends.ContainsKey(myFaceBookFriend.UUID) && !mGame.mPlayer.ApplyFriends.ContainsKey(myFaceBookFriend.UUID))
			{
				Network_GiveApply(myFaceBookFriend.UUID);
				num++;
				mSelectedData = myFaceBookFriend;
				break;
			}
		}
		if (num == 0)
		{
			mState.Change(STATE.MAIN);
			mFriendAllbutton = false;
		}
		else
		{
			mFriendAllbutton = true;
			mState.Change(STATE.NETWORK_FRIEND_02_1);
		}
	}

	private void Network_GiveApply(int a_target)
	{
		mApplyUserCheckDone = false;
		mApplyUserSucceed = false;
		GiftItemData giftItemData = new GiftItemData();
		giftItemData.FromID = mGame.mPlayer.UUID;
		giftItemData.Message = mGame.mPlayer.NickName;
		giftItemData.ItemCategory = 16;
		giftItemData.ItemKind = 0;
		giftItemData.Quantity = 1;
		giftItemData.PlayStage = mGame.mPlayer.PlayStage;
		if (!Server.GiveApply(giftItemData, a_target))
		{
			mApplyUserCheckDone = true;
		}
	}

	private void Server_OnGiveApply(int a_result, int a_code)
	{
		if (a_result == 0)
		{
			if (mSelectedData != null)
			{
				MyFriend myFriend = new MyFriend();
				myFriend.Data.UUID = mSelectedData.UUID;
				myFriend.Data.Name = mSelectedData.Name;
				myFriend.Data.Level = mSelectedData.Level;
				myFriend.Data.IconID = mSelectedData.IconID;
				myFriend.Data.IconLevel = mSelectedData.IconLevel;
				myFriend.Data.Series = mSelectedData.Series;
				if (!mGame.mPlayer.ApplyFriends.ContainsKey(myFriend.Data.UUID))
				{
					mGame.mPlayer.ApplyFriends.Add(myFriend.Data.UUID, myFriend);
				}
				mSaveflg = true;
				mFacebookUserList.RemoveListItem(mSelectedData);
			}
			else
			{
				BuildDialogApplyError();
			}
			mSelectedData = null;
			mApplyUserSucceed = true;
			mState.Reset(STATE.NETWORK_01_2, true);
		}
		else
		{
			mApplyUserSucceed = false;
			BuildDialogApplyError();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
