using System.Collections;
using UnityEngine;

public class EventIntroCharacterDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		ANIMATION_WAIT = 1,
		ANIMATION_WAIT_WAIT = 2,
		MAIN = 3,
		MAIN2 = 4,
		WAIT = 5
	}

	public delegate void OnDialogClosed();

	protected Def.EVENT_TYPE mEventType = Def.EVENT_TYPE.SM_STAR;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private OnDialogClosed mCallback;

	private Partner mPartner;

	private CompanionData mCompanionData;

	private BIJImage atlas;

	private BIJImage atlasIcon;

	private BIJImage atlasEvent;

	private UISsSprite mPartnerHalo;

	private UIButton mButton;

	private MapAvater mMapAvater;

	private AccessoryData mAccessoryData;

	private AccessoryData mData;

	private int mGetnum;

	private bool mSecondReward;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(int accessoryID, Def.EVENT_TYPE eventtype, int getnum, bool SecondReward = false)
	{
		mData = mGame.GetAccessoryData(accessoryID);
		mCompanionData = mGame.mCompanionData[mData.GetGotIDByNum()];
		mEventType = eventtype;
		mGetnum = getnum;
		mSecondReward = SecondReward;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mState.Change(STATE.ANIMATION_WAIT);
			break;
		case STATE.ANIMATION_WAIT:
			mState.Change(STATE.ANIMATION_WAIT_WAIT);
			break;
		case STATE.ANIMATION_WAIT_WAIT:
			BuildDialogInternal();
			mState.Change(STATE.MAIN);
			break;
		case STATE.MAIN:
			mButton = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
			Util.SetImageButtonInfo(mButton, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mButton, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
			mState.Change(STATE.MAIN2);
			break;
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		mPartner.transform.localPosition = Vector3.zero;
		int level = mGame.mPlayer.GetCompanionLevel(mCompanionData.index);
		mPartner.Init(mCompanionData.index, Vector3.zero, 1f, level, base.gameObject, true, mBaseDepth + 2);
		while (!mPartner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
	}

	private void BuildDialogInternal()
	{
		UIFont atlasFont = GameMain.LoadFont();
		int companionLevel = mGame.mPlayer.GetCompanionLevel(mCompanionData.index);
		int num = companionLevel;
		string imageName = ((mCompanionData.maxLevel == 5) ? Def.CharacterViewLevelMaxImageTbl[mCompanionData.maxLevel] : Def.CharacterViewLevelImageTbl[mCompanionData.maxLevel]);
		string imageName2;
		if (mEventType != Def.EVENT_TYPE.SM_NONE)
		{
			imageName2 = (mSecondReward ? "LC_event_instruction_text03" : "LC_event_instruction_text02");
			UISprite sprite = Util.CreateSprite("instruction", base.gameObject, atlasEvent);
			Util.SetSpriteInfo(sprite, imageName2, mBaseDepth + 135, new Vector3(0f, 291f, 0f), Vector3.one, false);
		}
		imageName2 = string.Empty;
		switch (mEventType)
		{
		case Def.EVENT_TYPE.SM_RALLY:
			imageName2 = string.Format(Localization.Get("EvRemuneration_Desc01"), mGetnum);
			break;
		case Def.EVENT_TYPE.SM_RALLY2:
			imageName2 = string.Format(Localization.Get("EvRemuneration_Desc02"), mGetnum);
			break;
		case Def.EVENT_TYPE.SM_STAR:
			imageName2 = string.Format(Localization.Get("EvRemuneration_Desc00"), mGetnum);
			break;
		case Def.EVENT_TYPE.SM_BINGO:
			imageName2 = string.Format(Localization.Get("EvRemuneration_Desc03"), mGetnum);
			break;
		case Def.EVENT_TYPE.SM_LABYRINTH:
			imageName2 = string.Format(Localization.Get("EvRemuneration_Desc04"), mGetnum);
			break;
		case Def.EVENT_TYPE.SM_NONE:
			imageName2 = string.Format(Localization.Get("Remuneration_Desc00"), mGetnum);
			break;
		}
		UILabel uILabel = Util.CreateLabel("lvlDesc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, imageName2, mBaseDepth + 2, new Vector3(0f, 210f, 0f), 25, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0f, 0.59607846f, 38f / 85f, 1f);
		uILabel.fontStyle = FontStyle.Bold;
		uILabel.spacingX = 2;
		uILabel.transform.localScale = new Vector3(1f, 1.2f);
		UISprite uISprite = Util.CreateSprite("MessageFrameTitle", base.gameObject, atlas);
		Util.SetSpriteInfo(uISprite, "instruction_accessory10", mBaseDepth + 2, new Vector3(0f, -125f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(498, 42);
		imageName2 = string.Format(Localization.Get(mCompanionData.name));
		uILabel = Util.CreateLabel("SkillName", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, imageName2, mBaseDepth + 6, new Vector3(0f, 2f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.effectStyle = UILabel.Effect.Shadow;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(470, 24);
		float num2 = 0f;
		num2 = -30f;
		UISprite uISprite2 = Util.CreateSprite("lvlIcon", base.gameObject, atlas);
		Util.SetSpriteInfo(uISprite2, imageName, mBaseDepth + 2, new Vector3(-125f + num2, -166f, 0f), Vector3.one, false);
		if (!mSecondReward)
		{
			num2 = 0f;
			num2 = -20f;
			imageName2 = string.Format(Localization.Get("EvRemuneration_GlowLimit00"));
			uILabel = Util.CreateLabel("parnerDesc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, imageName2, mBaseDepth + 2, new Vector3(71f + num2, -167f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		else
		{
			uISprite2.transform.localPosition = new Vector3(-165f, -166f);
			uISprite2.transform.localScale = new Vector3(0.9f, 0.9f);
			num2 = 0f;
			num2 = -10f;
			imageName2 = string.Format(Localization.Get("EvRemuneration_GlowLimit01"));
			uILabel = Util.CreateLabel("parnerDesc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, imageName2, mBaseDepth + 2, new Vector3(59f + num2, -169f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		mPartnerHalo = Util.CreateGameObject("PartnerHalo", base.gameObject).AddComponent<UISsSprite>();
		mPartnerHalo.Animation = ResourceManager.LoadSsAnimation("MAP_HALO").SsAnime;
		mPartnerHalo.depth = mBaseDepth + 1;
		mPartnerHalo.gameObject.transform.localScale = new Vector3(1.4f, 1.4f, 1f);
		mPartnerHalo.gameObject.transform.localPosition = new Vector3(0f, 38f, 0f);
		mPartnerHalo.Play();
		mPartner.SetPartnerUIScreenDepth(mBaseDepth + 5);
		if (mCompanionData.IsTall())
		{
			mPartner.SetPartnerScreenScale(0.5f);
		}
		else
		{
			mPartner.SetPartnerScreenScale(0.58f);
		}
		mPartner.SetPartnerScreenPos(new Vector3(0f, -98f, 0f));
		mPartner.SetPartnerScreenEnable(true);
		mPartner.StartMotion(CompanionData.MOTION.STAY0, true);
		mPartner.MakeUIShadow(atlas, "character_foot2", mBaseDepth + 3);
	}

	public override void BuildDialog()
	{
		atlas = ResourceManager.LoadImage("HUD").Image;
		atlasIcon = ResourceManager.LoadImage("ICON").Image;
		atlasEvent = ResourceManager.LoadImage("EVENTHUD").Image;
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		string titleDescKey = string.Empty;
		if (mEventType == Def.EVENT_TYPE.SM_NONE)
		{
			titleDescKey = Localization.Get("Remuneration_Title");
		}
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey, null, null, 480);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN2)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mPartner != null)
		{
			Object.Destroy(mPartner.gameObject);
		}
		if (mPartnerHalo != null)
		{
			Object.Destroy(mPartnerHalo.gameObject);
		}
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.MAIN2, true);
		}
	}
}
