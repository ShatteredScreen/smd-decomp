using System;
using System.IO;
using System.Text;

public class BIJTempFileBinaryReader : BIJBinaryReader
{
	private FileStream mStream;

	public BIJTempFileBinaryReader(string name)
		: this(name, Encoding.UTF8)
	{
	}

	public BIJTempFileBinaryReader(string name, Encoding encoding)
	{
		mStream = null;
		Reader = null;
		try
		{
			mPath = System.IO.Path.Combine(GetBasePath(), name);
			mStream = new FileStream(mPath, FileMode.Open);
			if (mStream == null)
			{
				throw new Exception("Cant open file : " + mPath);
			}
			Reader = new BinaryReader(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	internal static string GetBasePath()
	{
		return BIJUnity.getTempPath();
	}

	public override void Close()
	{
		base.Close();
		try
		{
			if (mStream != null)
			{
				mStream.Close();
			}
		}
		catch (Exception)
		{
		}
		mStream = null;
	}
}
