using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDPRAcceptDialog : DialogBase
{
	public enum STATE
	{
		OPEN = 0,
		MAIN = 1,
		WAIT = 2
	}

	public enum WEBVIEW_STATE
	{
		NONE = 0,
		PROGRESS = 1,
		ERROR = 2
	}

	public enum EXTERNAL_SPAWN_MODE
	{
		NONE = 0,
		SPAWN_AT_OUT_OF_PAGE = 1,
		SPAWN_AT_OUT_OF_DOMAIN = 2
	}

	public delegate void OnDialogClosed(List<GDPRAcceptInfo> aAcceptList);

	public delegate void OnPageMoved(bool first, bool error);

	public delegate void OnWebViewMessage(string message);

	private const float BOTTOM_UI_OFFSET = 13f;

	private const string CB_Webview_onError = "Webview_onError";

	private const string CB_Webview_onClose = "Webview_onClose";

	private const string CB_Webview_onPageStarted = "Webview_onPageStarted";

	private const string CB_Webview_onPageFinished = "Webview_onPageFinished";

	private const string CB_Webview_onProgress = "Webview_onProgress";

	private const string CB_Webview_onJson = "Webview_onJson";

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.OPEN);

	private WEBVIEW_STATE mWebViewState;

	private EXTERNAL_SPAWN_MODE mSpawnMode;

	private OnDialogClosed mCallback;

	private OnPageMoved mPageMovedCallback;

	private OnWebViewMessage mWebViewMessageCallback;

	private ConfirmDialog mConfirmDialog;

	private ConfirmDialog mErrorDialog;

	private BIJImage mHudAtlas;

	private BIJImage mTitleAtlas;

	private UISprite mBackground;

	private UISprite mWebBackground;

	protected Color mBackGroundColor = new Color32(byte.MaxValue, 230, 235, byte.MaxValue);

	private GameObject mFooter;

	private UISprite mBottomBarL;

	private UISprite mBottomBarR;

	private UIButton mSelectAcceptButton;

	private UIButton mAllAcceptButton;

	private UIButton mAllRejectButton;

	private List<SMVerticalListItem> mGDPRItemList = new List<SMVerticalListItem>();

	private SMVerticalList mGDPRList;

	private static int mListItemHeight = 80;

	private static SMVerticalListInfo mGDPRListInfo = new SMVerticalListInfo(1, 130f, 340f, 1, 130f, 780f, 1, mListItemHeight);

	private bool mListCreated;

	private bool mShowScrollBarL = true;

	private bool mShowScrollBarP = true;

	private Color mDefaultColor = new Color32(150, 150, 150, byte.MaxValue);

	private Color mSelectedColor = Color.white;

	private int mSelectedIndex;

	private WebViewObject mWebViewObject;

	private bool mLoadURLCall;

	private string mLoadURL;

	private string mFallbackURL;

	protected bool mIsClickDeepLink;

	private List<GDPRInfo> mInfoList;

	private List<GDPRAcceptInfo> mAcceptList = new List<GDPRAcceptInfo>();

	public bool IsOpend { get; private set; }

	protected bool IsBusy
	{
		get
		{
			return GetBusy() || mState.GetStatus() != STATE.MAIN;
		}
	}

	public override void Start()
	{
		base.Start();
		IsOpend = false;
		mIsClickDeepLink = false;
		mBaseDepth = GetSortingOder();
		SetPageMovedCallback(OnGDPRAcceptDialogPageMoved);
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		mWebViewObject = null;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.OPEN:
			mState.Change(STATE.MAIN);
			break;
		}
		mState.Update();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		SetLayout(o);
	}

	public override IEnumerator BuildResource()
	{
		ResImage resTitleImage = ResourceManager.LoadImageAsync("TITLE");
		while (resTitleImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mTitleAtlas = resTitleImage.Image;
	}

	public override void BuildDialog()
	{
		SetJellyTargetScale(1f);
		mHudAtlas = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		Vector2 vector = Util.LogScreenSize();
		mBackground = Util.CreateSprite("BackGround", base.gameObject, mHudAtlas);
		Util.SetSpriteInfo(mBackground, "bg00w", mBaseDepth, Vector3.zero, Vector3.one, false);
		mBackground.type = UIBasicSprite.Type.Sliced;
		mBackground.SetDimensions(1386, 1386);
		mBackground.color = mBackGroundColor;
		mWebBackground = Util.CreateSprite("WebBackGround", base.gameObject, mHudAtlas);
		Util.SetSpriteInfo(mWebBackground, "bg00w", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		mWebBackground.type = UIBasicSprite.Type.Sliced;
		mWebBackground.SetDimensions(0, 0);
		mFooter = Util.CreateGameObject("Footer", base.gameObject);
		float y = -136f;
		mBottomBarR = Util.CreateSprite("BottomBarR", mFooter, mHudAtlas);
		Util.SetSpriteInfo(mBottomBarR, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, y, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mBottomBarL = Util.CreateSprite("BottomBarL", mFooter, mHudAtlas);
		Util.SetSpriteInfo(mBottomBarL, "menubar_frame", mBaseDepth + 2, new Vector3(2f, y, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		Vector3 position = new Vector3(0f, 178f);
		string text = "LC_btn_accept_selected";
		mSelectAcceptButton = Util.CreateJellyImageButton("SelectAcceptButton", mBottomBarR.gameObject, mTitleAtlas);
		Util.SetImageButtonInfo(mSelectAcceptButton, text, text, text, mBaseDepth + 3, position, Vector3.one);
		Util.AddImageButtonMessage(mSelectAcceptButton, this, "OnSelectAcceptPushed", UIButtonMessage.Trigger.OnClick);
		text = "LC_btn_accept_all";
		mAllAcceptButton = Util.CreateJellyImageButton("AllAcceptButton", mBottomBarR.gameObject, mTitleAtlas);
		Util.SetImageButtonInfo(mAllAcceptButton, text, text, text, mBaseDepth + 3, new Vector3(200f, position.y), Vector3.one);
		Util.AddImageButtonMessage(mAllAcceptButton, this, "OnAllAcceptPushed", UIButtonMessage.Trigger.OnClick);
		text = "LC_btn_reject_all";
		mAllRejectButton = Util.CreateJellyImageButton("AllRejectButton", mBottomBarR.gameObject, mTitleAtlas);
		Util.SetImageButtonInfo(mAllRejectButton, text, text, text, mBaseDepth + 3, new Vector3(-200f, position.y), Vector3.one);
		Util.AddImageButtonMessage(mAllRejectButton, this, "OnAllRejectPushed", UIButtonMessage.Trigger.OnClick);
		GDPRSetting mGDPRSetting = SingletonMonoBehaviour<GameMain>.Instance.GameProfile.mGDPRSetting;
		GDPRListItem gDPRListItem = new GDPRListItem();
		gDPRListItem.mInfo = new GDPRInfo();
		gDPRListItem.mInfo.mName = mGDPRSetting.mSummary;
		gDPRListItem.mInfo.mURL = mGDPRSetting.mSummaryURL;
		mGDPRItemList.Add(gDPRListItem);
		for (int i = 0; i < mInfoList.Count; i++)
		{
			gDPRListItem = new GDPRListItem();
			gDPRListItem.mInfo = mInfoList[i];
			mGDPRItemList.Add(gDPRListItem);
		}
		float num = vector.y / 2f;
		float num2 = (0f - vector.y) / 2f;
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, ScreenOrientation.Portrait);
		Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, ScreenOrientation.LandscapeLeft);
		Vector2 vector4 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, ScreenOrientation.Portrait);
		Vector2 vector5 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, ScreenOrientation.LandscapeLeft);
		float num3 = mGDPRItemList.Count * mListItemHeight + 10;
		float num4 = vector.y - 150f - (num - vector2.y) - (vector4.y - num2);
		float num5 = vector.x - 150f - (num - vector3.y) - (vector5.y - num2);
		if (num3 < num4)
		{
			num4 = num3;
			mShowScrollBarP = false;
		}
		if (num3 < num5)
		{
			num5 = num3;
			mShowScrollBarL = false;
		}
		mGDPRListInfo.portrait_height = num4;
		mGDPRListInfo.landscape_height = num5;
		mGDPRList = SMVerticalList.Make(base.gameObject, this, mGDPRListInfo, mGDPRItemList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mGDPRList.OnCreateItem = OnCreateGDPRItem;
		mGDPRList.OnBeforeCreate = OnBeforeCreateGDPRItem;
		mGDPRList.OnCreateFinish = OnCreateFinishGDPRItem;
		Vector3 vector6 = new Vector3(-160f, 0f);
		mGDPRList.ScrollBarPositionOffsetPortrait = vector6;
		mGDPRList.ScrollBarPositionOffsetLandscape = vector6;
		SetLayout(Util.ScreenOrientation);
		GameObject gameObject = Util.CreateGameObject("WebView", base.gameObject);
		mWebViewObject = gameObject.AddComponent<WebViewObject>();
		mWebViewObject.Init(OnWebViewObjectCallback);
		mWebViewObject.SetVisibility(false);
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		boxCollider.center = new Vector3(0f, 0f, 0f);
		boxCollider.size = new Vector3(vector.x, vector.y, 1f);
	}

	protected virtual void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		float num = vector.y / 2f;
		float num2 = (0f - vector.y) / 2f;
		float num3 = vector.x / 2f;
		bool flag = true;
		bool scrollBarDisplay = mShowScrollBarP;
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopRight, Util.ScreenOrientation);
		Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomLeft, Util.ScreenOrientation);
		if (o != ScreenOrientation.Portrait && o != ScreenOrientation.PortraitUpsideDown)
		{
			flag = false;
			scrollBarDisplay = mShowScrollBarL;
		}
		if (mFooter != null)
		{
			mFooter.transform.localPosition = new Vector3(0f, vector3.y, 0f);
		}
		if (mGDPRList != null)
		{
			float num4 = 10f + (num - vector2.y);
			float num5 = 130f + (vector3.y - num2);
			float num6 = mGDPRListInfo.portrait_width + 42f + (vector3.x + num3);
			float num7 = 10f + (num3 - vector2.x);
			float num8 = mGDPRListInfo.portrait_height;
			if (!flag)
			{
				num8 = mGDPRListInfo.landscape_height;
			}
			mGDPRList.gameObject.transform.localPosition = new Vector3(vector3.x + mGDPRListInfo.portrait_width / 2f + 40f, num - num8 / 2f - num4);
			mGDPRList.gameObject.SetActive(false);
			mGDPRList.gameObject.SetActive(true);
			if (mListCreated)
			{
				mGDPRList.OnScreenChanged(flag);
				mGDPRList.SetScrollBarDisplay(scrollBarDisplay);
			}
			Vector2 aSize = new Vector2(vector.x - num6 - num7, vector.y - num4 - num5);
			Vector2 vector4 = new Vector2(num6 + aSize.x / 2f - vector.x / 2f, num5 + aSize.y / 2f - vector.y / 2f);
			mWebBackground.gameObject.transform.localPosition = vector4;
			mWebBackground.SetDimensions((int)aSize.x, (int)aSize.y);
			SetWebViewMargin(vector4, aSize);
		}
	}

	private void OnCreateGDPRItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		UIFont atlasFont = GameMain.LoadFont();
		GDPRListItem gDPRListItem = (GDPRListItem)item;
		UISprite uISprite = Util.CreateSprite("Tab", itemGO.gameObject, mTitleAtlas);
		Util.SetSpriteInfo(uISprite, "button_tab", mBaseDepth + 15, Vector3.zero, Vector3.one, false);
		uISprite.color = mDefaultColor;
		gDPRListItem.mTab = uISprite;
		UILabel uILabel = Util.CreateLabel("Name", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, gDPRListItem.mInfo.mName, mBaseDepth + 16, new Vector3(60f, 0f, 0f), 24, 0, 0, UIWidget.Pivot.Right);
		uILabel.color = mDefaultColor;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(92, 55);
		gDPRListItem.mName = uILabel;
		string text = "pointer";
		UIButton uIButton = Util.CreateJellyImageButton("Tab@" + index, itemGO, mHudAtlas);
		Util.SetImageButtonInfoScale(uIButton, text, text, text, mBaseDepth + 17, Vector3.zero, new Vector3(130f, 75f));
		Util.AddImageButtonMessage(uIButton, this, "OnTabPushed", UIButtonMessage.Trigger.OnClick);
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mGDPRList.mListScrollView;
		if (gDPRListItem.mInfo.mKind != -1)
		{
			text = "button_checkbox";
			uIButton = Util.CreateJellyImageButton("CheckBox@" + index, itemGO, mTitleAtlas);
			Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 18, new Vector3(-46f, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnCheckBoxPushed", UIButtonMessage.Trigger.OnClick);
			uISprite = Util.CreateSprite("Check", uIButton.gameObject, mTitleAtlas);
			Util.SetSpriteInfo(uISprite, "button_check", mBaseDepth + 19, Vector3.zero, Vector3.one, false);
			uISprite.enabled = false;
			gDPRListItem.mCheck = uISprite;
		}
	}

	public void OnBeforeCreateGDPRItem()
	{
		switch (Util.ScreenOrientation)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mGDPRList.OnScreenChanged(true);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mGDPRList.OnScreenChanged(false);
			break;
		}
	}

	public void OnCreateFinishGDPRItem()
	{
		mListCreated = true;
		SetLayout(Util.ScreenOrientation);
		mSelectedIndex = 0;
		GDPRListItem gDPRListItem = (GDPRListItem)mGDPRItemList[0];
		gDPRListItem.mTab.color = mSelectedColor;
		gDPRListItem.mName.color = mSelectedColor;
		LoadURL(gDPRListItem.mInfo.mURL);
	}

	public void OnTabPushed(GameObject go)
	{
		string[] array = go.name.Split('@');
		int num = int.Parse(array[1]);
		if (mSelectedIndex == num)
		{
			ToggleCheck(num);
		}
		else
		{
			ChangeTab(num);
		}
	}

	private void ChangeTab(int index)
	{
		mGame.PlaySe("SE_POSITIVE", -1);
		GDPRListItem gDPRListItem = (GDPRListItem)mGDPRItemList[mSelectedIndex];
		gDPRListItem.mTab.color = mDefaultColor;
		gDPRListItem.mName.color = mDefaultColor;
		gDPRListItem = (GDPRListItem)mGDPRItemList[index];
		mSelectedIndex = index;
		gDPRListItem.mTab.color = mSelectedColor;
		gDPRListItem.mName.color = mSelectedColor;
		LoadURL(gDPRListItem.mInfo.mURL);
	}

	public void OnCheckBoxPushed(GameObject go)
	{
		string[] array = go.name.Split('@');
		int num = int.Parse(array[1]);
		if (mSelectedIndex == num)
		{
			ToggleCheck(num);
		}
		else
		{
			ChangeTab(num);
		}
	}

	private void ToggleCheck(int index)
	{
		GDPRListItem gDPRListItem = (GDPRListItem)mGDPRItemList[index];
		mGame.PlaySe("SE_POSITIVE", -1);
		gDPRListItem.mSelected = !gDPRListItem.mSelected;
		gDPRListItem.mCheck.enabled = gDPRListItem.mSelected;
	}

	private void OnWebViewObjectCallback(string message)
	{
		if (mWebViewMessageCallback != null)
		{
			mWebViewMessageCallback(message);
		}
		if (string.Compare("Webview_onPageStarted", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.PROGRESS;
			}
		}
		else if (string.Compare("Webview_onPageFinished", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS || mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.NONE;
				if (mPageMovedCallback != null)
				{
					mPageMovedCallback(mLoadURLCall, false);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onProgress", message) == 0)
		{
			if (mWebViewState != WEBVIEW_STATE.PROGRESS)
			{
			}
		}
		else if (string.Compare("Webview_onError", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS)
			{
				mWebViewState = WEBVIEW_STATE.ERROR;
				if (mPageMovedCallback != null)
				{
					mPageMovedCallback(mLoadURLCall, true);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onClose", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.NONE)
			{
				mState.Change(STATE.WAIT);
				Close();
			}
		}
		else if (message.StartsWith("Webview_onJson") && mWebViewState == WEBVIEW_STATE.NONE)
		{
			string a_json = message.Remove(0, "Webview_onJson".Length);
			SingletonMonoBehaviour<GameMain>.Instance.OnDeeplinkAction(a_json);
			mIsClickDeepLink = true;
			mState.Change(STATE.WAIT);
			Close();
		}
	}

	private void LoadURL(string url)
	{
		LoadURL(url, null);
	}

	private void LoadURL(string url, string fallback)
	{
		if (mWebViewObject != null)
		{
			mLoadURLCall = true;
			mWebViewState = WEBVIEW_STATE.NONE;
			mLoadURL = url;
			mFallbackURL = fallback;
			mWebViewObject.LoadURL(url);
		}
	}

	private void Reload()
	{
		if (mWebViewObject != null)
		{
			mLoadURLCall = true;
			mWebViewState = WEBVIEW_STATE.NONE;
			mWebViewObject.Reload();
		}
	}

	public void ShowWebview(bool show)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(show);
		}
	}

	protected void SetWebViewMargin(Vector2 aPos, Vector2 aSize)
	{
		Vector2 vector = Util.LogScreenSize();
		Vector2 vector2 = new Vector2(vector.x - aSize.x, vector.y - aSize.y);
		int left = (int)(Util.ToDevX(vector2.x / 2f) + Util.ToDevX(aPos.x));
		int top = (int)(Util.ToDevY(vector2.y / 2f) - Util.ToDevY(aPos.y));
		int right = (int)(Util.ToDevX(vector2.x / 2f) - Util.ToDevX(aPos.x));
		int bottom = (int)(Util.ToDevY(vector2.y / 2f) + Util.ToDevY(aPos.y));
		if (mWebViewObject != null)
		{
			mWebViewObject.SetMargins(left, top, right, bottom);
		}
	}

	public override void OnOpenFinished()
	{
		IsOpend = true;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mAcceptList);
		}
	}

	public override void Close()
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetVisibility(false);
		}
		mGame.FinishConnecting();
		base.Close();
	}

	private void CreateAcceptList(bool isSelected, bool ret = false)
	{
		for (int i = 1; i < mGDPRItemList.Count; i++)
		{
			GDPRListItem gDPRListItem = (GDPRListItem)mGDPRItemList[i];
			GDPRAcceptInfo gDPRAcceptInfo = new GDPRAcceptInfo();
			gDPRAcceptInfo.mKind = gDPRListItem.mInfo.mKind;
			if (isSelected)
			{
				gDPRAcceptInfo.mAccept = gDPRListItem.mSelected;
			}
			else
			{
				gDPRAcceptInfo.mAccept = ret;
			}
			gDPRAcceptInfo.mDate = gDPRListItem.mInfo.mDate;
			gDPRAcceptInfo.mAdjustKey = gDPRListItem.mInfo.mAdjustKey;
			mAcceptList.Add(gDPRAcceptInfo);
		}
	}

	public void OnSelectAcceptPushed(GameObject go)
	{
		if (!IsBusy)
		{
			CreateAcceptList(true);
			mGame.PlaySe("SE_POSITIVE", -1);
			ShowWebview(false);
			Close();
		}
	}

	public void OnAllAcceptPushed(GameObject go)
	{
		if (!IsBusy)
		{
			CreateAcceptList(false, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			ShowWebview(false);
			Close();
		}
	}

	public void OnAllRejectPushed(GameObject go)
	{
		if (!IsBusy)
		{
			CreateAcceptList(false);
			mGame.PlaySe("SE_NEGATIVE", -1);
			ShowWebview(false);
			Close();
		}
	}

	public override void OnBackKeyPress()
	{
		if (IsBusy)
		{
		}
	}

	private void OnBackKeyConfirmDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Reset(STATE.MAIN, true);
			OnAllRejectPushed(null);
		}
		else
		{
			ShowWebview(true);
			mState.Change(STATE.MAIN);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void SetPageMovedCallback(OnPageMoved callback)
	{
		mPageMovedCallback = callback;
	}

	private void SetWebViewMessageCallback(OnWebViewMessage callback)
	{
		mWebViewMessageCallback = callback;
	}

	public void SetGDPRInfo(List<GDPRInfo> aList)
	{
		mInfoList = aList;
	}

	public static List<GDPRInfo> CheckGDPRAccept(GDPRSetting aSetting, PlayerData aPlayer)
	{
		List<GDPRInfo> list = new List<GDPRInfo>();
		Dictionary<int, GDPRAcceptInfo> gDPRAcceptDict = aPlayer.GDPRAcceptDict;
		List<GDPRInfo> mList = aSetting.mList;
		long num = (long)DateTimeUtil.DateTimeToUnixTimeStamp(DateTimeUtil.Now(), true);
		for (int i = 0; i < mList.Count; i++)
		{
			GDPRInfo gDPRInfo = mList[i];
			if (gDPRInfo.mDate == 0L || num < gDPRInfo.mDate)
			{
				continue;
			}
			int mKind = gDPRInfo.mKind;
			if (mKind < 0)
			{
				continue;
			}
			bool flag = false;
			bool flag2 = false;
			if (gDPRAcceptDict.ContainsKey(mKind))
			{
				if (gDPRInfo.mUpdateDate > 0 && num >= gDPRInfo.mUpdateDate && gDPRInfo.mUpdateDate > gDPRAcceptDict[mKind].mDate)
				{
					flag = true;
					flag2 = true;
				}
				else if (gDPRInfo.mDate > gDPRAcceptDict[mKind].mDate)
				{
					flag = true;
				}
			}
			else
			{
				flag = true;
				if (gDPRInfo.mUpdateDate > 0 && num >= gDPRInfo.mUpdateDate)
				{
					flag2 = true;
				}
			}
			if (flag)
			{
				GDPRInfo gDPRInfo2 = new GDPRInfo();
				gDPRInfo2.mKind = gDPRInfo.mKind;
				gDPRInfo2.mName = gDPRInfo.mName;
				gDPRInfo2.mURL = gDPRInfo.mURL;
				gDPRInfo2.mAdjustKey = gDPRInfo.mAdjustKey;
				if (!flag2)
				{
					gDPRInfo2.mDate = gDPRInfo.mDate;
				}
				else
				{
					gDPRInfo2.mDate = gDPRInfo.mUpdateDate;
				}
				list.Add(gDPRInfo2);
			}
		}
		return list;
	}

	protected bool IsEqualStatus(STATE state)
	{
		return state == mState.GetStatus();
	}

	protected bool IsEqualWebViewStatus(WEBVIEW_STATE state)
	{
		return state == mWebViewState;
	}

	private void OnGDPRAcceptDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mErrorDialog != null)
			{
				mErrorDialog.Close();
			}
			ShowWebview(true);
		}
		else
		{
			ShowWebview(false);
			mErrorDialog = Util.CreateGameObject("ErrorDialog", base.gameObject).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_ONLY);
			mErrorDialog.SetClosedCallback(OnErrorDialogClosed);
			mErrorDialog.SetBaseDepth(150);
			mState.Reset(STATE.WAIT, true);
		}
	}

	private void OnErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		Reload();
		mErrorDialog = null;
		mState.Change(STATE.MAIN);
	}
}
