using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Prime31;
using UnityEngine;

public class GameStateSMMap : GameStateSMMapBase
{
	private enum SNS_PROCESSING
	{
		NONE = 0,
		SOCIAL_RANKING = 1,
		FACEBOOK_INDUCTION = 2,
		FACEBOOK_INVITE_FRIEND = 3,
		FRIENDS_RANKING = 4
	}

	private class _GetRandomUserData
	{
		public List<SearchUserData> results { get; set; }
	}

	public enum STATE
	{
		NONE = 0,
		LOAD_WAIT = 1,
		UNLOAD_WAIT = 2,
		INIT = 3,
		WAIT_SOUND = 4,
		UNLOCK_ACTING = 5,
		MAIN = 6,
		DLLISTHASH_WAIT = 7,
		STAGEOPEN_WAIT = 8,
		STAGEUNLOCK_WAIT = 9,
		ACCESSORIEOPEN_WAIT = 10,
		RBOPEN_WAIT = 11,
		RBUNLOCK_WAIT = 12,
		RBKEYBROKEN_WAIT = 13,
		SERIESOPEN_WAIT = 14,
		AVATARMOVE_WAIT = 15,
		NEWAREA_OPEN = 16,
		NEWAREA_OPEN_ANIMEWAIT = 17,
		NEWAREA_OPEN_WAIT = 18,
		NEWAREA_RETURN = 19,
		DAILY_EVENT_APPEAR = 20,
		DAILY_EVENT_DISAPPEAR = 21,
		WAIT = 22,
		PLAY = 23,
		PAUSE = 24,
		STORY_DEMO_FADE = 25,
		STORY_DEMO = 26,
		WEBVIEW = 27,
		WEBVIEW_WAIT = 28,
		SERIES_SELECT_INIT_WAIT = 29,
		SERIES_SELECT = 30,
		SERIES_SELECT_CANCEL = 31,
		SERIES_SELECT_FADE = 32,
		EVENTAPPEAL = 33,
		EVENTAPPEAL_WAIT = 34,
		TUTORIAL = 35,
		NETWORK_CONNECT_WAIT = 36,
		NETWORK_PROLOGUE = 37,
		NETWORK_MAINTENANCE = 38,
		NETWORK_MAINTENANCE_00 = 39,
		NETWORK_AUTH = 40,
		NETWORK_AUTH_00 = 41,
		NETWORK_00 = 42,
		NETWORK_00_00 = 43,
		NETWORK_01 = 44,
		NETWORK_01_00 = 45,
		NETWORK_02 = 46,
		NETWORK_02_00 = 47,
		NETWORK_03 = 48,
		NETWORK_03_00 = 49,
		NETWORK_04 = 50,
		NETWORK_ADV_GET_MAIL = 51,
		NETWORK_ADV_GET_MAIL_WAIT = 52,
		NETWORK_04_00 = 53,
		NETWORK_04_00_1 = 54,
		NETWORK_04_00_2 = 55,
		NETWORK_04_01 = 56,
		NETWORK_04_01_1 = 57,
		NETWORK_04_01_2 = 58,
		NETWORK_04_02 = 59,
		NETWORK_04_02_1 = 60,
		NETWORK_04_02_2 = 61,
		NETWORK_04_02_3 = 62,
		NETWORK_04_03 = 63,
		NETWORK_04_03_1 = 64,
		NETWORK_04_04 = 65,
		NETWORK_04_99 = 66,
		NETWORK_GETRANDOMUSER = 67,
		NETWORK_GETRANDOMUSER_00 = 68,
		NETWORK_GETFRIENDINFO = 69,
		NETWORK_GETFRIENDINFO_00 = 70,
		NETWORK_GETCAMPAIGNINFO = 71,
		NETWORK_GETCAMPAIGNINFO_00 = 72,
		NETWORK_EPILOGUE = 73,
		RESTORE_TRANSACTIONS = 74,
		RESTORE_TRANSACTIONS_00 = 75,
		NETWORK_TUTCOMP_CHECK = 76,
		NETWORK_TUTCOMP_CHECK_00 = 77,
		NETWORK_REWARD_CHECK = 78,
		NETWORK_REWARD_CHECK_00 = 79,
		NETWORK_REWARD_MAINTENACE = 80,
		NETWORK_REWARD_MAINTENACE_00 = 81,
		NETWORK_REWARD_SET = 82,
		NETWORK_REWARD_SET_00 = 83,
		NETWORK_ADV_REWARD_SET = 84,
		NETWORK_ADV_REWARD_SET_00 = 85,
		NETWORK_RESUME_MAINTENACE = 86,
		NETWORK_RESUME_MAINTENACE_00 = 87,
		NETWORK_RESUME_GAMEINFO = 88,
		NETWORK_RESUME_GAMEINFO_00 = 89,
		NETWORK_RESUME_CAMPAIGN = 90,
		NETWORK_RESUME_CAMPAIGN_00 = 91,
		NETWORK_RESUME_LOGINBONUS = 92,
		NETWORK_RESUME_LOGINBONUS_00 = 93,
		NETWORK_STAGE = 94,
		NETWORK_STAGE_WAIT = 95,
		NETWORK_NOTICE_WAIT = 96,
		AUTHERROR_RETURN_TITLE = 97,
		AUTHERROR_RETURN_WAIT = 98,
		MAINTENANCE_RETURN_TITLE = 99,
		MAINTENANCE_RETURN_WAIT = 100,
		GOTO_PRETITLE = 101,
		GOTO_TITLE = 102,
		GOTO_REENTER = 103,
		DEEPLINK_PUSHED = 104,
		WAIT_JUMPSTORE = 105,
		NETWORK_INFORMATION = 106,
		NETWORK_INFORMATION_WAIT = 107,
		NETWORK_INFORMATION_GASHA = 108,
		NETWORK_INFORMATION_GASHA_WAIT = 109,
		PRIVACY_OPTION = 110
	}

	private const string BGM_NAME = "BGM_MAP_";

	private const int mRewardIdStage10Clear = 204;

	public static bool mProductListReceiveDone;

	public static bool mProductListReceiveSuccess;

	public int oldSD;

	public int resultSD;

	private SNS_PROCESSING mSNSProcessing;

	private FacebookInductionDialog mFacebookInduction;

	private bool FacebookInductionFlag;

	private FriendRankingDialog mRankingDialog;

	private RankingInductionDialog mRankingInductionDialog;

	private bool mRankingInductionSilentClose;

	private bool mMapRandomUserSucceed;

	private bool mMapRandomUserCheckDone;

	private GameObject mMapRoot;

	private Def.TUTORIAL_INDEX mNextIndex;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private STATE mRewardErrorPrevState = STATE.MAIN;

	private string mGuID;

	public float mFadeTargetPos;

	public float mFadeBeforePos;

	public int mFadeTargetPage;

	protected float mWaitTime;

	protected int mOldMaxLevel;

	protected int mBundleAccessoryCount;

	protected AccessoryData mOpenPuzzleAccessory;

	protected AccessoryData mOtherOpenPuzzleAccessory;

	protected AccessoryData mAccessory_GrowStamp;

	protected bool mFriendIconVisible;

	protected EULADialog mEULADialog;

	protected NoticeDialog mNoticeDialog;

	protected MenuDialog mMenuDialog;

	protected SelectOptionDialog mSelectOptionDialog;

	protected MyFriendDialog mMyFriendDialog;

	protected RoadBlockDialog mRoadBlockDialog;

	protected RandomFriendDialog mStrayUserDialog;

	protected BuyBoosterDialog mBuyBoosterDialog;

	protected AgeConfirmationDialog mAgeConfirmationDialog;

	protected FriendRankingDialog mFriendRankingDialog;

	protected TutorialMessageDialog mTutDialog;

	protected EventPageDialog mEventEntranceDialog;

	protected RemainTimeDialog mRemainTimeDialog;

	protected NextSeasonNoticeDialog mNextSeasonDialog;

	protected MapChangeDialog mMapChangeDialog;

	protected TrophyGuideDialog mTrophyGuideDialog;

	protected GetSPStampDialog mGetSPStampDialog;

	protected DailyChallengeDialog mDailyChallengeDialog;

	protected int mStampDailyItemCount;

	protected WebViewMultiPageDialog mWebViewMultiPageDialog;

	protected NoticeConstructDialog mNoticeConstructDialog;

	protected CampaignDialog mCampaignDialog;

	protected bool mGotCampaignData;

	protected bool mGettingCampaignData;

	protected SeriesNoticeDialog mSeriesNoticeDialog;

	protected EventAppeal mEventAppeal;

	protected EventAppealSettings mEventAppealSetting;

	protected bool mNeedClearCache;

	protected OneShotAnime mLensFlareAnime;

	protected UIImageButton mRankingButton;

	protected bool mCockpitOpen;

	protected bool mIsFirstOpenStage;

	private float PurchaseStartTime;

	private string DEFAULT_BGM = string.Empty;

	private bool mStrayTutorialFlg;

	private bool mShowedEventAppeal;

	protected BIJSNS mSNS;

	private IDictionary<int, SMDailyEventSetting> mDailyEventsData = new Dictionary<int, SMDailyEventSetting>();

	private bool mDailyEventsDidAppered;

	private bool mDailyEventsDidDisappered;

	private bool mDirectStoreTransition;

	protected GameFriendManager mFriendManager;

	private STATE mStateAfterConnect = STATE.NETWORK_GETCAMPAIGNINFO;

	private STATE mBootConnectErrorState;

	private StatusManager<STATE> mStateConnecting = new StatusManager<STATE>(STATE.MAIN);

	private ResourceInstance mResSound;

	private bool mBGMLoadError;

	private SeriesSelectManager mSeriesSelectManager;

	private bool mWallpaperTutorialFlg;

	private bool mActiveTutorialFlg;

	private bool mFirstTutorialFlg;

	private static bool mRewardCheckDoneStage10Clear;

	private static bool mRewardShowStage10Clear;

	private bool mIsSailorGuardianTutorialDialog;

	private bool mIsInitialTutorialCompleted;

	public STATE StateStatus = STATE.WAIT;

	public STATE OverwriteState = STATE.WAIT;

	public STATE ConnectingStatus = STATE.WAIT;

	protected float mPageMoving;

	private bool mIsLastSPDailyAccessory;

	protected bool mIsDebugSeriesNoticeCheck = true;

	protected bool mIsFinishedNewAreaOpen;

	protected bool mIsFinishedNewAreaReturn;

	private int mSelectedseries;

	public bool IsOpenDailyChallenge
	{
		get
		{
			return mDailyChallengeDialog != null;
		}
	}

	private void OBSupportedEvent()
	{
	}

	private void OBNotSupportedEvent(string _s0)
	{
	}

	private void OQInventorySucceededEvent(List<GooglePurchase> _l0, List<GoogleSkuInfo> _l1)
	{
		mProductListReceiveDone = true;
		mProductListReceiveSuccess = true;
		string[] array = new string[_l0.Count];
		for (int i = 0; i < _l0.Count; i++)
		{
			array[i] = _l0[i].productId;
		}
		for (int j = 0; j < _l1.Count; j++)
		{
		}
	}

	private void OQInventoryFailedEvent(string _s0)
	{
		mProductListReceiveDone = true;
		mProductListReceiveSuccess = false;
	}

	private void OPCompleteAwaitingVerificationEvent(string _s0, string _s1)
	{
	}

	private void OPSucceededEvent(List<PItem> _plist)
	{
	}

	private void OPFailedEvent(string _s0)
	{
	}

	private void OCPSucceededEvent(GooglePurchase _gp, int _state, PItem item)
	{
		if (_state == 0)
		{
			long num = 0L;
			if (PurchaseStartTime >= 0f)
			{
				num = Mathf.FloorToInt((Time.realtimeSinceStartup - PurchaseStartTime) * 1000f);
			}
			ItemAdd(item);
		}
	}

	private void OCPFailedEvent(string _s0)
	{
	}

	public void InitPurchaseManager()
	{
		PurchaseManager.instance.restoreMode = false;
		PurchaseManager.instance.onBillingSupportedEventFunc = OBSupportedEvent;
		PurchaseManager.instance.onBillingNotSupportedEventFunc = OBNotSupportedEvent;
		PurchaseManager.instance.onQueryInventorySucceededEventFunc = OQInventorySucceededEvent;
		PurchaseManager.instance.onQueryInventoryFailedEventFunc = OQInventoryFailedEvent;
		PurchaseManager.instance.onPurchaseCompleteAwaitingVerificationEventFunc = OPCompleteAwaitingVerificationEvent;
		PurchaseManager.instance.onPurchaseSucceededEventFunc = OPSucceededEvent;
		PurchaseManager.instance.onPurchaseFailedEventFunc = OPFailedEvent;
		PurchaseManager.instance.onConsumePurchaseSucceededEventFunc = OCPSucceededEvent;
		PurchaseManager.instance.onConsumePurchaseFailedEventFunc = OCPFailedEvent;
	}

	public void CleanupPurchaseManager()
	{
		PurchaseManager.instance.onBillingSupportedEventFunc = null;
		PurchaseManager.instance.onBillingNotSupportedEventFunc = null;
		PurchaseManager.instance.onQueryInventorySucceededEventFunc = null;
		PurchaseManager.instance.onQueryInventoryFailedEventFunc = null;
		PurchaseManager.instance.onPurchaseCompleteAwaitingVerificationEventFunc = null;
		PurchaseManager.instance.onPurchaseSucceededEventFunc = null;
		PurchaseManager.instance.onPurchaseFailedEventFunc = null;
		PurchaseManager.instance.onConsumePurchaseSucceededEventFunc = null;
		PurchaseManager.instance.onConsumePurchaseFailedEventFunc = null;
	}

	private void ItemAdd(PItem _item)
	{
		if (_item != null)
		{
			PItem.KIND kind = _item.kind;
			AddSD(_item.amount);
		}
	}

	private void AddSD(int _amount)
	{
		oldSD = mGame.mPlayer.Dollar;
		mGame.mPlayer.AddDollar(_amount, true);
		resultSD = mGame.mPlayer.Dollar;
		mGame.mOptions.TotalMasterCurrency += _amount;
		mGame.Save();
	}

	protected override void RegisterServerCallback()
	{
		Server.GetStageRankingDataEvent += Server_OnGetStageRankingData;
		Server.GetRandomUserEvent += Server_OnGetRandomUser;
	}

	protected override void UnregisterServerCallback()
	{
		Server.GetStageRankingDataEvent -= Server_OnGetStageRankingData;
		Server.GetRandomUserEvent -= Server_OnGetRandomUser;
	}

	protected virtual STATE MapNetwork_GetRandomUser()
	{
		STATE result = STATE.NETWORK_GETRANDOMUSER_00;
		long num = DateTimeUtil.BetweenMinutes(mGame.LastRandomUserGetTime, DateTime.Now);
		long gET_RANDOMUSER_FREQ = GameMain.GET_RANDOMUSER_FREQ;
		if (gET_RANDOMUSER_FREQ > 0 && num < gET_RANDOMUSER_FREQ)
		{
			mMapRandomUserCheckDone = true;
			mMapRandomUserSucceed = true;
			return STATE.NETWORK_GETFRIENDINFO;
		}
		if (GameMain.NO_NETWORK)
		{
			mMapRandomUserCheckDone = true;
			mMapRandomUserSucceed = true;
			return STATE.NETWORK_GETFRIENDINFO;
		}
		mMapRandomUserCheckDone = false;
		mMapRandomUserSucceed = false;
		int series = SingletonMonoBehaviour<Network>.Instance.Series;
		int level = SingletonMonoBehaviour<Network>.Instance.Level;
		int a_range = 500;
		int rANDOM_USER_MAX = GameMain.RANDOM_USER_MAX;
		mGame.LastRandomUserGetTime = DateTime.Now;
		if (!Server.GetRandomUser(series, level, a_range, rANDOM_USER_MAX))
		{
			mGame.LastRandomUserGetTime = DateTimeUtil.UnitLocalTimeEpoch;
			mMapRandomUserCheckDone = true;
			return STATE.NETWORK_GETFRIENDINFO;
		}
		return result;
	}

	protected virtual void Server_OnGetRandomUser(int result, int code, string json)
	{
		if (result == 0)
		{
			try
			{
				_GetRandomUserData obj = new _GetRandomUserData();
				new MiniJSONSerializer().Populate(ref obj, json);
				mGame.RandomUserList = new List<SearchUserData>();
				if (obj != null && obj.results != null && obj.results.Count > 0)
				{
					List<SearchUserData> strayUserList = mGame.mPlayer.GetStrayUserList(obj.results);
					foreach (SearchUserData item in strayUserList)
					{
						mGame.RandomUserList.Add(item.Clone());
					}
				}
				mMapRandomUserSucceed = true;
			}
			catch
			{
				mGame.RandomUserList.Clear();
			}
		}
		mMapRandomUserCheckDone = true;
	}

	protected override void Network_OnStage()
	{
		STATE aStatus = STATE.WAIT;
		int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
		int currentLevel = mGame.mPlayer.CurrentLevel;
		base.RankingData = null;
		mState.Change(STATE.NETWORK_STAGE_WAIT);
		bool flag = false;
		if (!Server.GetStageRankingData(currentSeries, currentLevel))
		{
			mState.Change(aStatus);
		}
	}

	protected override void Network_OnStageWait()
	{
		_GetRankingData rankingData = base.RankingData;
		if (rankingData != null && rankingData.results.Count > 0)
		{
			mState.Change(STATE.WAIT);
			if (EnableSocialRankingDialog())
			{
				ShowSocialRankingDialog(rankingData.series, rankingData.stage, rankingData.results);
			}
		}
	}

	protected virtual void Server_OnGetStageRankingData(int result, int code, string json)
	{
		if (mState.GetStatus() != STATE.NETWORK_STAGE_WAIT || result != 0)
		{
			return;
		}
		try
		{
			_GetRankingData obj = new _GetRankingData();
			new MiniJSONSerializer().Populate(ref obj, json);
			if (obj != null && obj.results != null)
			{
				for (int num = obj.results.Count - 1; num >= 0; num--)
				{
					if (obj.results[num].uuid == mGame.mPlayer.UUID)
					{
						int num2 = 0;
						PlayerClearData _psd;
						if (mGame.mPlayer.GetStageClearData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.CurrentLevel, out _psd))
						{
							num2 = _psd.HightScore;
						}
						if (num2 <= 0)
						{
							obj.results.RemoveAt(num);
						}
					}
					else if (!mGame.mPlayer.Friends.ContainsKey(obj.results[num].uuid))
					{
						obj.results.RemoveAt(num);
					}
				}
				obj.results.Sort();
			}
			base.RankingData = obj;
		}
		catch (Exception)
		{
		}
	}

	protected void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		mState.Change((STATE)state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthAbandon()
	{
		mGame.IsTitleReturnMaintenance = true;
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void OnConnectErrorAccessoryRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAccessoryAbandon()
	{
		GameStateSMMapBase.mIsShowErrorDialog = false;
		AccessoryData accessoryData = mGame.GetAccessoryData(AccessoryConnectIndex);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			mGame.mPlayer.AddAccessoryConnect(AccessoryConnectIndex);
		}
		mGame.AlreadyConnectAccessoryList.Add(AccessoryConnectIndex);
		AccessoryConnectIndex = 0;
		mState.Reset(STATE.UNLOCK_ACTING, true);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAdvAccessoryAbandon()
	{
		GameStateSMMapBase.mAdvIsShowErrorDialog = false;
		mAdvAutoUnlockAccessory = null;
		AccessoryData accessoryData = mGame.GetAccessoryData(AdvAccessoryConnectIndex);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
		{
			mGame.mPlayer.AddAdvAccessoryConnect(AdvAccessoryConnectIndex);
		}
		mGame.AlreadyConnectAdvAccessoryList.Add(AdvAccessoryConnectIndex);
		AdvAccessoryConnectIndex = 0;
		mState.Reset(STATE.UNLOCK_ACTING, true);
		StartCoroutine(GrayIn());
	}

	protected void OnMaintenancheCheckErrorBoot(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		mBootConnectErrorState = (STATE)state;
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthRetryBoot(int a_state)
	{
		mBootConnectErrorState = (STATE)a_state;
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthAbandonBoot()
	{
		mGame.IsTitleReturnMaintenance = true;
		mBootConnectErrorState = STATE.GOTO_TITLE;
	}

	protected void OnConnectErrorAccessoryRetryBoot(int a_state)
	{
		mBootConnectErrorState = (STATE)a_state;
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAccessoryAbandonBoot()
	{
		GameStateSMMapBase.mIsShowErrorDialog = false;
		AccessoryData accessoryData = mGame.GetAccessoryData(AccessoryConnectIndex);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			mGame.mPlayer.AddAccessoryConnect(AccessoryConnectIndex);
		}
		mGame.AlreadyConnectAccessoryList.Add(AccessoryConnectIndex);
		AccessoryConnectIndex = 0;
		mBootConnectErrorState = STATE.UNLOCK_ACTING;
		StartCoroutine(GrayIn());
	}

	protected void OnMapDestroyUpDateClosedBoot(ConfirmDialog.SELECT_ITEM i)
	{
		mBootConnectErrorState = STATE.GOTO_PRETITLE;
	}

	protected void OnMapDestroyClosedBoot(ConfirmDialog.SELECT_ITEM i)
	{
		mBootConnectErrorState = STATE.GOTO_PRETITLE;
	}

	protected void Network_DebugTerminal()
	{
		if (Server.DebugTerminal())
		{
		}
	}

	protected void Server_OnDebugTerminal(int result, int code, Stream resStream)
	{
		if (result == 0)
		{
			try
			{
			}
			catch (Exception)
			{
			}
		}
		Server.DebugTerminalRegistration -= Server_OnDebugTerminal;
	}

	private bool TutorialMapStage()
	{
		bool result = false;
		int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
		if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST)
		{
			switch (playableMaxLevel)
			{
			case 900:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL9_7))
				{
					result = true;
				}
				break;
			case 1000:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL10_0))
				{
					result = true;
				}
				break;
			case 1400:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL14_0))
				{
					result = true;
				}
				break;
			}
		}
		return result;
	}

	private bool TutorialStartCheckOnInit(bool wallpaperTutorial = false)
	{
		bool flag = false;
		if (wallpaperTutorial && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL17_0))
		{
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL17_0, mGame.mPlayer.IsTutorialSkipped()))
			{
				TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL17_0);
				flag = true;
			}
		}
		else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST)
		{
			if (mRewardShowStage10Clear)
			{
			}
			switch (mGame.mPlayer.PlayableMaxLevel)
			{
			case 200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL2_1, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL2_1);
					flag = true;
				}
				break;
			case 300:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL3_1, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL3_1);
					flag = true;
				}
				break;
			case 400:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL4_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL4_0);
					flag = true;
				}
				break;
			case 800:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL8_0))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL8_0);
					flag = true;
				}
				break;
			case 900:
			{
				if (mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL9_7))
				{
					break;
				}
				int a_stage = 800;
				int a_main;
				int a_sub;
				Def.SplitStageNo(a_stage, out a_main, out a_sub);
				AccessoryData accessoryDataOnMap = mGame.GetAccessoryDataOnMap(a_main, a_sub);
				if (!mGame.mPlayer.IsAccessoryUnlock(accessoryDataOnMap.Index))
				{
					if (mGame.mPlayer.IsTutorialSkipped())
					{
						mGame.mTutorialManager.TutorialSkip(Def.TUTORIAL_INDEX.MAP_LEVEL9_0);
						mGame.SavePlayerData();
					}
					else
					{
						TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL9_0);
						flag = true;
					}
				}
				break;
			}
			case 1000:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL10_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL10_0);
					flag = true;
				}
				break;
			case 1100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL11_0))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL11_0);
					flag = true;
				}
				break;
			case 1200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL12_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL12_0);
					flag = true;
				}
				break;
			case 1400:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL14_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL14_0);
					flag = true;
				}
				break;
			case 1600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL16_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL16_0);
					flag = true;
				}
				break;
			case 1700:
			{
				int stageNo = mGame.mPlayer.PlayableMaxLevel - 100;
				PlayerClearData _psd;
				mGame.mPlayer.GetStageClearData(Def.SERIES.SM_FIRST, stageNo, out _psd);
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL17_0, mGame.mPlayer.IsTutorialSkipped()) && _psd != null && _psd.GotStars > 0)
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL17_0);
					flag = true;
				}
				break;
			}
			}
		}
		else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_R)
		{
			int num = 0;
			List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == (int)mGame.mPlayer.Data.CurrentSeries)
				{
					num = playStage[i].Level;
					break;
				}
			}
			switch (num)
			{
			case 200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL12_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL12_0);
					flag = true;
				}
				break;
			case 700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL14_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL14_0);
					flag = true;
				}
				break;
			}
		}
		else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_S)
		{
			int num2 = 0;
			List<PlayStageParam> playStage2 = mGame.mPlayer.PlayStage;
			for (int j = 0; j < playStage2.Count; j++)
			{
				if (playStage2[j].Series == (int)mGame.mPlayer.Data.CurrentSeries)
				{
					num2 = playStage2[j].Level;
					break;
				}
			}
			switch (num2)
			{
			case 200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL12_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL12_0);
					flag = true;
				}
				break;
			case 700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL14_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL14_0);
					flag = true;
				}
				break;
			case 15100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_TUT_BONUS_0_0))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_TUT_BONUS_0_0);
					flag = true;
				}
				break;
			case 16600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_TUT_BONUS_1_0))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_TUT_BONUS_1_0);
					flag = true;
				}
				break;
			}
		}
		else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_SS)
		{
			int num3 = 0;
			List<PlayStageParam> playStage3 = mGame.mPlayer.PlayStage;
			for (int k = 0; k < playStage3.Count; k++)
			{
				if (playStage3[k].Series == (int)mGame.mPlayer.Data.CurrentSeries)
				{
					num3 = playStage3[k].Level;
					break;
				}
			}
			int num4 = num3;
			if (num4 == 700 && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL14_0, mGame.mPlayer.IsTutorialSkipped()))
			{
				TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL14_0);
				flag = true;
			}
		}
		else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_STARS)
		{
			int num5 = 0;
			List<PlayStageParam> playStage4 = mGame.mPlayer.PlayStage;
			for (int l = 0; l < playStage4.Count; l++)
			{
				if (playStage4[l].Series == (int)mGame.mPlayer.Data.CurrentSeries)
				{
					num5 = playStage4[l].Level;
					break;
				}
			}
			int num4 = num5;
			if (num4 == 700 && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL14_0, mGame.mPlayer.IsTutorialSkipped()))
			{
				TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL14_0);
				flag = true;
			}
		}
		else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_GF00)
		{
			int num6 = 0;
			List<PlayStageParam> playStage5 = mGame.mPlayer.PlayStage;
			for (int m = 0; m < playStage5.Count; m++)
			{
				if (playStage5[m].Series == (int)mGame.mPlayer.Data.CurrentSeries)
				{
					num6 = playStage5[m].Level;
					break;
				}
			}
			switch (num6)
			{
			case 700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL14_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL14_0);
					flag = true;
				}
				break;
			case 9100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_TUT_BONUS_2_2))
				{
					TutorialStart(Def.TUTORIAL_INDEX.MAP_TUT_BONUS_2_0);
					flag = true;
				}
				break;
			}
		}
		if (!flag && mGame.mTutorialManager.IsInitialTutorialCompleted() && mCockpit.mShopflg && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_SHOP_0))
		{
			TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_SHOP_0);
			flag = true;
		}
		if (!flag && mGame.mTutorialManager.IsInitialTutorialCompleted() && mCockpit.mMugenflg && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_INFINITY_0))
		{
			TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL_TUT_INFINITY_0);
			flag = true;
		}
		if (!flag && mGame.mTutorialManager.IsInitialTutorialCompleted() && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0))
		{
			TutorialStart(Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0);
			flag = true;
		}
		if (!flag && mGame.mTutorialManager.IsInitialTutorialCompleted() && mGame.EnableAdvEnter() && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL22_0))
		{
			TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL22_0);
			flag = true;
		}
		CheckEventAppealDisplay();
		return flag;
	}

	private bool TutorialStartCheckOnMain()
	{
		return false;
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		MapStageButton mapStageButton = null;
		MapStageButton mapStageButton2 = null;
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
		if (mGame.mTutorialManager.IsMessageFakeClosed())
		{
			mGame.mTutorialManager.MessageFakeOpen();
		}
		switch (index)
		{
		case Def.TUTORIAL_INDEX.MAP_LEVEL2_2:
			mapStageButton = mCurrentPage.GetStageButton(200);
			mGame.mTutorialManager.SetTutorialFinger(mapStageButton.gameObject, mapStageButton.gameObject, Def.DIR.NONE);
			mGame.mTutorialManager.AddAllowedButton(mapStageButton.gameObject);
			if (mCurrentPage != null)
			{
				mCurrentPage.SetEnableButton(true);
			}
			break;
		case Def.TUTORIAL_INDEX.MAP_LEVEL3_1:
			mapStageButton = mCurrentPage.GetStageButton(100);
			mGame.mTutorialManager.SetTutorialPoint1Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, true, 80, Def.DIR.NONE, 45f);
			mapStageButton = mCurrentPage.GetStageButton(200);
			mGame.mTutorialManager.SetTutorialPoint2Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, true, 80, Def.DIR.NONE, 45f);
			break;
		case Def.TUTORIAL_INDEX.MAP_LEVEL4_1:
			mMapRoot = Util.CreateGameObject("MapRoot", mRoot);
			mMapRoot.transform.localPosition = new Vector3(160f, 0f, 0f);
			mGame.mTutorialManager.SetTutorialFinger(mGame.mAnchor.gameObject, mMapRoot.gameObject, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.MAP_LEVEL4_2:
		{
			mGame.mTutorialManager.DeleteTutorialPointArrow();
			mapStageButton = mCurrentPage.GetStageButton(800);
			MapAccessory mapAccessory3 = mCurrentPage.StageAccessory(800);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, mapAccessory3.gameObject);
			mGame.mTutorialManager.AddAllowedButton(mapStageButton.gameObject);
			break;
		}
		case Def.TUTORIAL_INDEX.MAP_LEVEL9_0:
		{
			float stageWorldPos2 = mCurrentPage.GetStageWorldPos(800);
			StartCoroutine(SlideMapMove(0f - stageWorldPos2));
			MapAccessory mapAccessory2 = mCurrentPage.StageAccessory(800);
			mGame.mTutorialManager.SetTutorialFinger(mapAccessory2.gameObject, mapAccessory2.gameObject, Def.DIR.NONE);
			mGame.mTutorialManager.AddAllowedButton(mapAccessory2.gameObject);
			break;
		}
		case Def.TUTORIAL_INDEX.MAP_LEVEL10_2:
		{
			GameObject gameObject = mCurrentPage.StageRandomFriend();
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, gameObject.gameObject, false, 80, Def.DIR.LEFT);
			if (mCurrentPage != null)
			{
				mCurrentPage.SetEnableButton(true);
				float stageWorldPos3 = mCurrentPage.GetStageWorldPos(100);
				StartCoroutine(DirectMapMove(0f - stageWorldPos3));
			}
			break;
		}
		case Def.TUTORIAL_INDEX.MAP_LEVEL14_0:
			if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST)
			{
				mapStageButton = mCurrentPage.GetStageButton(1301);
				mapStageButton2 = mCurrentPage.GetStageButton(1302);
				mGame.mTutorialManager.SetTutorialPoint1Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, false, 80, Def.DIR.NONE, 0f);
				mGame.mTutorialManager.SetTutorialPoint2Arrow(mGame.mAnchor.gameObject, mapStageButton2.gameObject, false, 80, Def.DIR.NONE, 0f);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_R)
			{
				mapStageButton = mCurrentPage.GetStageButton(601);
				mapStageButton2 = mCurrentPage.GetStageButton(602);
				mGame.mTutorialManager.SetTutorialPoint1Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, false, 80, Def.DIR.NONE, 0f);
				mGame.mTutorialManager.SetTutorialPoint2Arrow(mGame.mAnchor.gameObject, mapStageButton2.gameObject, false, 80, Def.DIR.NONE, 0f);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_S)
			{
				mapStageButton = mCurrentPage.GetStageButton(601);
				mapStageButton2 = mCurrentPage.GetStageButton(602);
				mGame.mTutorialManager.SetTutorialPoint1Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, false, 80, Def.DIR.NONE, 0f);
				mGame.mTutorialManager.SetTutorialPoint2Arrow(mGame.mAnchor.gameObject, mapStageButton2.gameObject, false, 80, Def.DIR.NONE, 0f);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_SS)
			{
				mapStageButton = mCurrentPage.GetStageButton(601);
				mapStageButton2 = mCurrentPage.GetStageButton(602);
				mGame.mTutorialManager.SetTutorialPoint1Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, false, 80, Def.DIR.DOWN, 0f);
				mGame.mTutorialManager.SetTutorialPoint2Arrow(mGame.mAnchor.gameObject, mapStageButton2.gameObject, false, 80, Def.DIR.DOWN, 0f);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_STARS)
			{
				mapStageButton = mCurrentPage.GetStageButton(601);
				mapStageButton2 = mCurrentPage.GetStageButton(602);
				mGame.mTutorialManager.SetTutorialPoint1Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, false, 80, Def.DIR.DOWN, 0f);
				mGame.mTutorialManager.SetTutorialPoint2Arrow(mGame.mAnchor.gameObject, mapStageButton2.gameObject, false, 80, Def.DIR.DOWN, 0f);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_GF00)
			{
				mapStageButton = mCurrentPage.GetStageButton(601);
				mapStageButton2 = mCurrentPage.GetStageButton(602);
				mGame.mTutorialManager.SetTutorialPoint1Arrow(mGame.mAnchor.gameObject, mapStageButton.gameObject, false, 80, Def.DIR.NONE, 0f);
				mGame.mTutorialManager.SetTutorialPoint2Arrow(mGame.mAnchor.gameObject, mapStageButton2.gameObject, false, 80, Def.DIR.NONE, 0f);
			}
			break;
		case Def.TUTORIAL_INDEX.MAP_LEVEL14_1:
			if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				MapAccessory mapAccessory = mCurrentPage.StageAccessory(1302);
				mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, mapAccessory.gameObject);
				if (mCurrentPage != null)
				{
					float stageWorldPos = mCurrentPage.GetStageWorldPos(800);
					if (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
					{
						StartCoroutine(DirectMapMove(0f - stageWorldPos));
					}
				}
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_R)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				GameObject target = GameObject.Find("Accessory1100");
				mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_S)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				GameObject target2 = GameObject.Find("Accessory2064");
				mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target2);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_SS)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				GameObject target3 = GameObject.Find("Accessory4132");
				mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target3);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_STARS)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				GameObject target4 = GameObject.Find("Accessory5219");
				mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target4, false, 80, Def.DIR.DOWN);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_GF00)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				GameObject target5 = GameObject.Find("Accessory7276");
				mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target5);
			}
			break;
		case Def.TUTORIAL_INDEX.MAP_LEVEL16_0:
			mapStageButton = mCurrentPage.GetStageButton(1600);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, mapStageButton.gameObject);
			break;
		}
	}

	public void OnTutorial()
	{
		mGame.mTutorialManager.MessageFakeOpen();
		TutorialStart(mNextIndex);
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		mNextIndex = mGame.mTutorialManager.TutorialComplete(index);
		if (mNextIndex != Def.TUTORIAL_INDEX.NONE)
		{
			if (mNextIndex == Def.TUTORIAL_INDEX.MAP_LEVEL8_2 || mNextIndex == Def.TUTORIAL_INDEX.MAP_LEVEL11_1 || mNextIndex == Def.TUTORIAL_INDEX.MAP_LEVEL12_1 || mNextIndex == Def.TUTORIAL_INDEX.MAP_LEVEL17_2)
			{
				mGame.mTutorialManager.MessageFakeClose(OnTutorial);
			}
			else
			{
				TutorialStart(mNextIndex);
			}
		}
		else if (mState.GetStatus() == STATE.TUTORIAL)
		{
			mGame.mTutorialManager.DeleteTutorialPointArrow();
			mActiveTutorialFlg = false;
			if (index != Def.TUTORIAL_INDEX.MAP_LEVEL11_4)
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
		}
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mActiveTutorialFlg = false;
			mGame.mTutorialManager.TutorialSkip(index);
			if (mState.GetStatus() == STATE.TUTORIAL)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				mState.Change(STATE.UNLOCK_ACTING);
			}
		}
		if (index == Def.TUTORIAL_INDEX.MAP_LEVEL8_0 || index == Def.TUTORIAL_INDEX.MAP_LEVEL8_1 || index == Def.TUTORIAL_INDEX.MAP_LEVEL8_2 || index == Def.TUTORIAL_INDEX.MAP_LEVEL8_3)
		{
			mGame.AddAccessoryFromTutorial(284);
			mGame.AddAccessoryFromTutorial(285);
			mGame.Save();
			mState.Change(STATE.UNLOCK_ACTING);
		}
		if ((index == Def.TUTORIAL_INDEX.MAP_LEVEL11_0 || index == Def.TUTORIAL_INDEX.MAP_LEVEL11_1 || index == Def.TUTORIAL_INDEX.MAP_LEVEL11_4) && !mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			mGame.AddAccessoryConnect(204);
			mGame.mTutorialManager.InitialTutorialComplete();
			base.MapPage.mTutorialCompButton.SetFukidashi(false, "image", "icon_84jem");
			mGame.Save();
			mRewardShowStage10Clear = false;
			int skip = 0;
			if (mGame.mPlayer.IsTutorialSkipped())
			{
				skip = 1;
			}
			ServerCram.TutorialComplete(skip);
			SingletonMonoBehaviour<Marketing>.Instance.AdjustSendEvent("qlu9a4");
			mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
		}
		if (index == Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0 || index == Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_1 || index == Def.TUTORIAL_INDEX.MAP_TUT_BONUS_0_2 || index == Def.TUTORIAL_INDEX.MAP_TUT_BONUS_1_3 || index == Def.TUTORIAL_INDEX.MAP_TUT_BONUS_2_2 || index == Def.TUTORIAL_INDEX.MAP_LEVEL14_0 || index == Def.TUTORIAL_INDEX.MAP_LEVEL14_1 || index == Def.TUTORIAL_INDEX.MAP_LEVEL14_2)
		{
			mGame.Save();
		}
	}

	public void CheckEventAppealDisplay()
	{
	}

	public static void InitStaticData()
	{
		mRewardCheckDoneStage10Clear = false;
		mRewardShowStage10Clear = false;
	}

	public override void Start()
	{
		base.Start();
		RegisterServerCallback();
		PurchaseStartTime = -1f;
		CleanupPurchaseManager();
		InitPurchaseManager();
		mGame.mPlayer.UpdateRestoreCount();
		PurchaseStartTime = Time.realtimeSinceStartup;
		PurchaseManager.instance.RestoreTransactions();
		mGame.mPlayer.AddRestoreCount();
		mIsInitialTutorialCompleted = mGame.mTutorialManager.IsInitialTutorialCompleted();
	}

	public void OnDestroy()
	{
		UnregisterServerCallback();
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public override void Update()
	{
		DragPower = Vector2.zero;
		base.Update();
		if (OverwriteState != STATE.WAIT)
		{
			mState.Change(OverwriteState);
			OverwriteState = STATE.WAIT;
		}
		StateStatus = mState.GetStatus();
		ConnectingStatus = mStateConnecting.GetStatus();
		if (mState.IsChanged())
		{
			if (StateStatus == STATE.MAIN)
			{
				if (mCockpit != null)
				{
					mCockpit.SetEnableButtonAction(true);
				}
				if (mCurrentPage != null)
				{
					mCurrentPage.SetEnableButton(true);
				}
			}
			else
			{
				if (mCockpit != null)
				{
					mCockpit.SetEnableButtonAction(false);
				}
				if (mCurrentPage != null)
				{
					mCurrentPage.SetEnableButton(false);
				}
			}
		}
		BGMSoundCheck();
		if (mStateConnecting.GetStatus() != STATE.MAIN && UpdateConnecting())
		{
			mHasBootConnectingError = true;
			if (GameMain.mUserAuthTransfered)
			{
				mBootConnectErrorState = STATE.AUTHERROR_RETURN_TITLE;
			}
			else if (GameMain.MaintenanceMode)
			{
				mBootConnectErrorState = STATE.MAINTENANCE_RETURN_TITLE;
			}
			else
			{
				mBootConnectErrorState = STATE.NETWORK_CONNECT_WAIT;
			}
		}
		switch (StateStatus)
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mState.Change(STATE.NETWORK_CONNECT_WAIT);
				if (!GameMain.NO_NETWORK && !mProductListReceiveSuccess)
				{
					mProductListReceiveDone = false;
					mProductListReceiveSuccess = false;
					PurchaseManager.instance.RequestProductData();
				}
				else
				{
					mProductListReceiveDone = true;
				}
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_STAGE:
			Network_OnStage();
			break;
		case STATE.NETWORK_STAGE_WAIT:
			Network_OnStageWait();
			break;
		case STATE.NETWORK_CONNECT_WAIT:
			if (!mIsConnecting)
			{
				if (!mHasBootConnectingError)
				{
					mConnectionTime = Time.realtimeSinceStartup;
					mState.Change(STATE.NETWORK_EPILOGUE);
				}
				else if (mBootConnectErrorState != 0)
				{
					mState.Reset(mBootConnectErrorState, true);
				}
			}
			break;
		case STATE.GOTO_TITLE:
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		case STATE.GOTO_PRETITLE:
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		case STATE.GOTO_REENTER:
			if (GameMain.USE_DEBUG_DIALOG)
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
				mState.Change(STATE.UNLOAD_WAIT);
			}
			else
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
				mState.Change(STATE.UNLOAD_WAIT);
			}
			break;
		case STATE.INIT:
			if (mState.IsChanged() && MaintenanceModeStartCheck())
			{
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				Update_INIT();
			}
			break;
		case STATE.WAIT_SOUND:
			Update_WAIT_SOUND();
			break;
		case STATE.UNLOCK_ACTING:
			if (mState.IsChanged() && MaintenanceModeStartCheck())
			{
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				Update_UNLOCK_ACTING();
			}
			break;
		case STATE.MAIN:
			if (mState.IsChanged())
			{
				if (MaintenanceModeStartCheck())
				{
					mState.Reset(STATE.WAIT, true);
					break;
				}
				switch (DLFilelistHashCheck())
				{
				case DLFILE_CHECK_RESULT.FILE_DESTROY:
					mState.Reset(STATE.WAIT, true);
					break;
				case DLFILE_CHECK_RESULT.HASH_NG:
					mState.Reset(STATE.WAIT, true);
					break;
				default:
					if (CheaterStartCheck() || SeriesNoticeCheck() || InformationCheck() || WebviewStartCheck() || GiftStartCheck())
					{
						break;
					}
					if (RatingStartCheck())
					{
						FacebookInductionFlag = true;
					}
					else if (!RankingStartCheck() && !RankingRankUpStarStartCheck() && !RankingRankUpStartCheck() && !CollectionUpStartCheck() && !MightOpenDailyChallenge())
					{
						if (mEventAppealSetting == null)
						{
							mEventAppealSetting = mGame.EventProfile.GetEventAppeal();
						}
						float y;
						if (mEventAppeal == null && mEventAppealSetting != null)
						{
							mState.Reset(STATE.EVENTAPPEAL, true);
						}
						else if (!OldEventStartCheck() && !CharacterIntroCheck() && mLensFlareAnime == null && mCurrentPage.IsPlayableLensFlare(out y))
						{
							mLensFlareAnime = Util.CreateOneShotAnime("LensFlare", "MAP_LENS_FLARE", mRoot, new Vector3(0f, 0f, Def.MAP_LENSFLARE_Z), Vector3.one, 0f, 0f, false, OnLensFlareFinished);
						}
					}
					break;
				}
			}
			else
			{
				if (mSocialRanking != null && mStageInfo == null)
				{
					mSocialRanking.Close();
					mSocialRanking = null;
				}
				Update_MAIN();
				UpdateMugenHeartStatus();
			}
			break;
		case STATE.EVENTAPPEAL:
			mEventAppeal = Util.CreateGameObject("EventAppeal", mRoot).AddComponent<EventAppeal>();
			mEventAppeal.Init(mEventAppealSetting);
			mEventAppealSetting = null;
			mState.Reset(STATE.EVENTAPPEAL_WAIT, true);
			break;
		case STATE.EVENTAPPEAL_WAIT:
		{
			bool flag = false;
			if (mEventAppeal != null)
			{
				if (mEventAppeal.IsAppealEnd())
				{
					flag = true;
					mShowedEventAppeal = true;
					if (!mEventAppeal.HasNextPushed)
					{
						mState.Reset(STATE.UNLOCK_ACTING);
					}
					else
					{
						mState.Reset(STATE.WAIT);
						if (mEventAppeal.IsAdvMode)
						{
							if (mGame.CheckAdvEventExpired(mEventAppeal.EventID))
							{
								short[] nowAdvEventIdList = mGame.GetNowAdvEventIdList();
								if (nowAdvEventIdList != null && nowAdvEventIdList.Length > 1)
								{
									mGame.mADVLink = GameMain.ADV_LINK.PLAY;
									mGame.EnterADV();
									mState.Reset(STATE.WAIT, true);
								}
								else
								{
									mGame.mADVLink = GameMain.ADV_LINK.EVENT;
									mGame.mADVLinkParam = mEventAppeal.EventID;
									mGame.EnterADV();
									mState.Reset(STATE.WAIT, true);
								}
							}
							else
							{
								StartCoroutine(GrayOut());
								mConfirmDialog = Util.CreateGameObject("EventExpiredDialog", mRoot).AddComponent<ConfirmDialog>();
								mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerExpired_Event_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
								mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
							}
						}
						else
						{
							bool flag2 = false;
							if (mGame.IsSeasonEventMultiHolding(true))
							{
								flag = false;
								mEventEntranceDialog = Util.CreateGameObject("EventPageDialog", mRoot).AddComponent<EventPageDialog>();
								mEventEntranceDialog.SetClosedCallback(OnEventEntranceClosed);
								mEventEntranceDialog.SetEventCallback(OnCurrentEventPushed);
								mEventEntranceDialog.SetOpenFinishedCallback(OnEventEntranceOpened);
							}
							else if (!mGame.IsSeasonEventExpired(mEventAppeal.EventID))
							{
								flag = false;
								OnCurrentEventPushed(mEventAppeal.EventID);
							}
							else
							{
								StartCoroutine(GrayOut());
								mConfirmDialog = Util.CreateGameObject("EventExpiredDialog", mRoot).AddComponent<ConfirmDialog>();
								mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerExpired_Event_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
								mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
							}
						}
					}
				}
			}
			else
			{
				flag = true;
				mState.Reset(STATE.UNLOCK_ACTING);
			}
			if (flag && mEventAppeal != null)
			{
				UnityEngine.Object.Destroy(mEventAppeal.gameObject);
				mEventAppeal = null;
			}
			break;
		}
		case STATE.STAGEOPEN_WAIT:
		case STATE.STAGEUNLOCK_WAIT:
		case STATE.ACCESSORIEOPEN_WAIT:
		case STATE.RBOPEN_WAIT:
		case STATE.AVATARMOVE_WAIT:
			if (mCurrentPage.IsEffectFinished())
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case STATE.RBUNLOCK_WAIT:
		{
			if (!mCurrentPage.IsEffectFinished())
			{
				break;
			}
			int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
			int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
			int a_main;
			int a_sub;
			Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
			SMChapterSetting chapterSearch = mMapPageData.GetChapterSearch(a_main + 1);
			if (chapterSearch != null)
			{
				int stageNo = Def.GetStageNo(chapterSearch.mSubNo, 0);
				mCurrentPage.OpenNewStageUntilNextChapter(playableMaxLevel, stageNo);
				if (mGame.mPlayer.StageOpenList.Count > 0)
				{
					mIsOpenNewChapter = true;
				}
			}
			mCurrentPage.UnlockNewStageSub(playableMaxLevel, openNoticeLevel);
			MapStageButton stageButton = mCurrentPage.GetStageButton(openNoticeLevel);
			stageButton.ChangeMode();
			if (mDemoNoAfterRBUnlock != -1 && !mGame.mPlayer.IsStoryCompleted(mDemoNoAfterRBUnlock))
			{
				StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
				storyDemoTemp.DemoIndex = mDemoNoAfterRBUnlock;
				storyDemoTemp.SelectStage = -1;
				storyDemoTemp.ClearStage = -1;
				mShowStoryDemoList.Add(storyDemoTemp);
				mDemoNoAfterRBUnlock = -1;
			}
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		}
		case STATE.NEWAREA_OPEN:
			if (mState.IsChanged())
			{
				StartCoroutine(ProcessNewAreaOpen());
			}
			else if (mIsFinishedNewAreaOpen)
			{
				mState.Change(STATE.NEWAREA_OPEN_ANIMEWAIT);
			}
			break;
		case STATE.NEWAREA_OPEN_ANIMEWAIT:
			if (mCurrentPage.IsEffectFinished())
			{
				mConfirmDialog = Util.CreateGameObject("NewAreaOpen", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("Area_Release_Title"), Localization.Get("Area_Release_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnNewAreaOpenDialogClosed);
				mState.Change(STATE.NEWAREA_OPEN_WAIT);
			}
			break;
		case STATE.NEWAREA_RETURN:
			if (mState.IsChanged())
			{
				StartCoroutine(ProcessNewAreaReturn());
			}
			else if (mIsFinishedNewAreaReturn)
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case STATE.SERIESOPEN_WAIT:
			if (mCurrentPage.IsEffectFinished())
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case STATE.WAIT:
			if (mSocialRanking != null && mStageInfo == null)
			{
				mSocialRanking.Close();
				mSocialRanking = null;
			}
			break;
		case STATE.PLAY:
		{
			mGame.mIsStageClearStar3 = false;
			mGame.mReplay = false;
			mGame.ClearPresentBox();
			if (mGame.mEventMode)
			{
				mGame.mSkinIndex = mGame.mEventSkinIndex;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mEventCompanionIndex;
				}
			}
			else
			{
				mGame.mSkinIndex = mGame.mPlayer.Data.SelectedSkin;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mPlayer.Data.SelectedCompanion;
				}
			}
			if (mGame.mPlayer.IsCompanionUnlock(mGame.mPlayer.Data.SelectedCompanion) && mGame.mLastCompanionSetting)
			{
				int playerIcon = mGame.mPlayer.Data.PlayerIcon;
				mGame.mPlayer.Data.LastUsedIcon = mGame.mPlayer.Data.SelectedCompanion;
				if (!mGame.mPlayer.Data.LastUsedIconCancelFlg)
				{
					if (mGame.mPlayer.Data.PlayerIcon >= 10000)
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion + 10000;
					}
					else
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion;
					}
				}
				if (playerIcon != mGame.mPlayer.Data.PlayerIcon)
				{
					mGame.Network_NicknameEdit(string.Empty);
				}
			}
			Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
			if (!Def.IsEventSeries(currentSeries) && !GlobalVariables.Instance.IsDailyChallengePuzzle)
			{
				int mainStageNumber = mGame.mCurrentStage.MainStageNumber;
				int subStageNumber = mGame.mCurrentStage.SubStageNumber;
				mCurrentPage.SetAvatarNode(Def.GetStageNo(mainStageNumber, subStageNumber));
			}
			mGame.AlreadyConnectAccessoryList.Clear();
			mGame.AlreadyConnectAdvAccessoryList.Clear();
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.PUZZLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		}
		case STATE.STORY_DEMO_FADE:
			if (mGameState.IsFadeFinished)
			{
				mGameState.FadeIn();
				if (mCurrentDemoIndex != -1)
				{
					StoryDemoManager storyDemoManager = Util.CreateGameObject("DemoManager", base.gameObject).AddComponent<StoryDemoManager>();
					storyDemoManager.ChangeStoryDemo(mCurrentDemoIndex, OnStoryDemoFinished);
					mCurrentDemoIndex = -1;
					StartCoroutine(GrayIn(1f, 0.8f));
					mState.Change(STATE.STORY_DEMO);
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			break;
		case STATE.SERIES_SELECT_INIT_WAIT:
			if (mSeriesSelectManager != null && mSeriesSelectManager.IsLoadFinished)
			{
				mState.Change(STATE.SERIES_SELECT);
			}
			break;
		case STATE.SERIES_SELECT_FADE:
			if (mGameState.IsFadeFinished)
			{
				mGame.mPlayer.Data.CurrentSeries = (Def.SERIES)mSelectedseries;
				mGame.StopMusic(0, 0.5f);
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
				mState.Reset(STATE.UNLOAD_WAIT);
			}
			break;
		case STATE.SERIES_SELECT_CANCEL:
			if (mSeriesSelectManager != null)
			{
				if (mSeriesSelectManager.IsDisappearFinished)
				{
					StartCoroutine(GrayIn());
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			else
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case STATE.WEBVIEW:
			OnHelpDialog();
			break;
		case STATE.TUTORIAL:
			if (mCurrentPage != null)
			{
				mCurrentPage.SetEnableButton(true);
			}
			break;
		case STATE.DAILY_EVENT_APPEAR:
			DisplayMapDailyEvents();
			mState.Change(STATE.MAIN);
			break;
		case STATE.DAILY_EVENT_DISAPPEAR:
			UndisplayMapDailyEventsAll();
			mState.Change(STATE.MAIN);
			break;
		case STATE.PRIVACY_OPTION:
			OnPrivacyOption();
			break;
		case STATE.NETWORK_PROLOGUE:
			mConnectionTime = Time.realtimeSinceStartup;
			mFriendManager = null;
			if (!GameMain.CanCommunication())
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			else if (mTipsScreenDisplayed)
			{
				mGameState.SetLoadingUISsAnimation(2);
				mState.Change(STATE.NETWORK_MAINTENANCE);
			}
			else
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_MAINTENANCE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_MAINTENANCE_00);
			break;
		case STATE.NETWORK_MAINTENANCE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else if (mGame.AddedNewFriend)
				{
					mGame.AddedNewFriend = false;
					mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitTimeEpoch;
					mState.Change(STATE.NETWORK_GETFRIENDINFO);
				}
				else
				{
					mState.Change(STATE.NETWORK_AUTH);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 38);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_AUTH:
			mGame.Network_UserAuth();
			mState.Change(STATE.NETWORK_AUTH_00);
			break;
		case STATE.NETWORK_AUTH_00:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00);
					break;
				}
				mGame.FinishConnecting();
				mState.Change(STATE.AUTHERROR_RETURN_TITLE);
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = mUserAuthErrorCode;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 40, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 40, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_00:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_00_00);
			break;
		case STATE.NETWORK_00_00:
			if (!GameMain.mGameProfileCheckDone)
			{
				break;
			}
			if (GameMain.mGameProfileSucceed)
			{
				if (mGame.mGameProfile.LinkBanner != null && mGame.mGameProfile.LinkBanner.BannerList.Count > 0)
				{
					mGame.LinkBannerList_Clear();
					LinkBannerManager.Instance.GetLinkBannerRequest(mGame.mLBRequest, mGame.mGameProfile.LinkBanner, OnGetLinkBannerComplete);
				}
				mState.Change(STATE.NETWORK_01);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 42, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_01:
			mGame.Network_GetSegmentProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_01_00);
			break;
		case STATE.NETWORK_01_00:
		{
			if (!GameMain.mSegmentProfileCheckDone)
			{
				break;
			}
			if (GameMain.mSegmentProfileSucceed)
			{
				mState.Change(STATE.NETWORK_02);
				break;
			}
			Server.ErrorCode mSegmentProfileErrorCode = GameMain.mSegmentProfileErrorCode;
			Server.ErrorCode errorCode = mSegmentProfileErrorCode;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 44, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 44, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_02:
			mState.Change(STATE.NETWORK_03);
			break;
		case STATE.NETWORK_03:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_03_00);
			break;
		case STATE.NETWORK_03_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				mGame.DoLoginBonusServerCheck = false;
				mState.Change(STATE.NETWORK_04);
			}
			else
			{
				Server.ErrorCode mLoginBonusErrorCode = GameMain.mLoginBonusErrorCode;
				Server.ErrorCode errorCode = mLoginBonusErrorCode;
				if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 48, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 48, OnConnectErrorAuthRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.NETWORK_04:
			mGame.mGotGiftIds = new List<int>();
			mGame.mGotApplyIds = new List<int>();
			mGame.mGotSupportIds = new List<int>();
			mGame.mGotSupportPurchaseIds = new List<int>();
			if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				mState.Change(STATE.NETWORK_04_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_02);
			}
			break;
		case STATE.NETWORK_ADV_GET_MAIL:
		{
			int is_retry2 = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry2 = 1;
			}
			mGame.Network_AdvGetMail(mGuID, is_retry2);
			mState.Change(STATE.NETWORK_ADV_GET_MAIL_WAIT);
			break;
		}
		case STATE.NETWORK_ADV_GET_MAIL_WAIT:
		{
			if (!GameMain.mAdvGetMailCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetMailSucceed)
			{
				mGuID = null;
				mState.Change(STATE.NETWORK_04_00);
				break;
			}
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = GameMain.mAdvGetMailErrorCode;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 51, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 51, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_04_00:
		{
			STATE aStatus4 = STATE.NETWORK_04_00_1;
			if (mGame.MapNetwork_SupportCheck())
			{
				aStatus4 = STATE.NETWORK_04_01;
			}
			mState.Change(aStatus4);
			break;
		}
		case STATE.NETWORK_04_00_1:
			if (GameMain.mMapSupportCheckDone)
			{
				mState.Change(STATE.NETWORK_04_00_2);
			}
			break;
		case STATE.NETWORK_04_00_2:
			mGame.MapNetwork_SupportGot();
			mState.Change(STATE.NETWORK_04_01);
			break;
		case STATE.NETWORK_04_01:
		{
			STATE aStatus3 = STATE.NETWORK_04_01_1;
			if (mGame.MapNetwork_GiftCheck())
			{
				aStatus3 = STATE.NETWORK_04_02;
			}
			mState.Change(aStatus3);
			break;
		}
		case STATE.NETWORK_04_01_1:
			if (GameMain.mMapGiftCheckDone)
			{
				mState.Change(STATE.NETWORK_04_01_2);
			}
			break;
		case STATE.NETWORK_04_01_2:
			mGame.MapNetwork_GiftGot();
			mState.Change(STATE.NETWORK_04_02);
			break;
		case STATE.NETWORK_04_02:
		{
			STATE aStatus2 = STATE.NETWORK_04_02_1;
			if (mGame.MapNetwork_ApplyCheck())
			{
				aStatus2 = STATE.NETWORK_04_03;
			}
			mState.Change(aStatus2);
			break;
		}
		case STATE.NETWORK_04_02_1:
			if (GameMain.mMapApplyCheckDone)
			{
				mState.Change(STATE.NETWORK_04_02_2);
			}
			break;
		case STATE.NETWORK_04_02_2:
			mGame.MapNetwork_ApplyGot();
			if (GameMain.mMapApplyRepeat)
			{
				mState.Change(STATE.NETWORK_04_02_3);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_03);
			}
			break;
		case STATE.NETWORK_04_02_3:
		{
			long num = DateTimeUtil.BetweenMilliseconds(mGame.mPlayer.Data.LastGetApplyTime, DateTime.Now);
			if (num > GameMain.mMapApplyRepeatWait)
			{
				mGame.mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
				mState.Change(STATE.NETWORK_04_02);
			}
			break;
		}
		case STATE.NETWORK_04_03:
			if (GameMain.NO_NETWORK)
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			else if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				STATE aStatus = STATE.NETWORK_04_03_1;
				if (mGame.MapNetwork_SupportPurchaseCheck())
				{
					aStatus = STATE.NETWORK_04_04;
				}
				mState.Change(aStatus);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_03_1:
			if (GameMain.mMapSupportPurchaseCheckDone)
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_04:
			mState.Reset(STATE.NETWORK_04_99, true);
			break;
		case STATE.NETWORK_04_99:
		{
			mState.Change(STATE.NETWORK_GETRANDOMUSER);
			if (mGame.mFbUserMngGift == null)
			{
				mGame.mFbUserMngGift = FBUserManager.CreateInstance();
			}
			List<GiftItemData> list = new List<GiftItemData>();
			if (mGame.mPlayer.Gifts.Count <= 0)
			{
				break;
			}
			foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
			{
				GiftItem value2 = gift.Value;
				list.Add(value2.Data);
			}
			mGame.mFbUserMngGift.SetFBUserList(list);
			break;
		}
		case STATE.NETWORK_GETRANDOMUSER:
		{
			if (!mGame.mPlayer.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL9_7.ToString()))
			{
				mMapRandomUserCheckDone = false;
				mState.Change(STATE.NETWORK_GETFRIENDINFO);
				break;
			}
			STATE sTATE = MapNetwork_GetRandomUser();
			if (sTATE == STATE.NETWORK_GETFRIENDINFO)
			{
				mStrayTutorialFlg = true;
			}
			mState.Change(sTATE);
			break;
		}
		case STATE.NETWORK_GETRANDOMUSER_00:
			if (mMapRandomUserCheckDone)
			{
				mState.Change(STATE.NETWORK_GETFRIENDINFO);
			}
			break;
		case STATE.NETWORK_GETFRIENDINFO:
			mFriendManager = new GameFriendManager(mGame);
			if (!mFriendManager.Start())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mState.Change(STATE.NETWORK_GETFRIENDINFO_00);
			}
			break;
		case STATE.NETWORK_GETFRIENDINFO_00:
			if (mFriendManager == null || mFriendManager.IsFinished())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mFriendManager.Update();
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO:
			if (mProductListReceiveDone)
			{
				if (mGame.Network_CampaignCheck(mIsInitialTutorialCompleted))
				{
					mState.Change(STATE.NETWORK_GETCAMPAIGNINFO_00);
				}
				else
				{
					mState.Change(STATE.NETWORK_EPILOGUE);
				}
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_EPILOGUE:
			if (!mProductListReceiveDone)
			{
				break;
			}
			if (mCurrentPage.mFriendIcons != null)
			{
				int count = mCurrentPage.mFriendIcons.Count;
				for (int num2 = count - 1; num2 >= 0; num2--)
				{
					MapSNSIcon mapSNSIcon = mCurrentPage.mFriendIcons[num2];
					if (!mGame.mPlayer.Friends.ContainsKey(mapSNSIcon.UuID))
					{
						mCurrentPage.mFriendIcons.Remove(mapSNSIcon);
						UnityEngine.Object.Destroy(mapSNSIcon.gameObject);
					}
				}
				for (int k = 0; k < mCurrentPage.mFriendIcons.Count; k++)
				{
					MapSNSIcon mapSNSIcon2 = mCurrentPage.mFriendIcons[k];
					MapSNSSetting item = default(MapSNSSetting);
					foreach (KeyValuePair<int, MyFriend> friend in mGame.mPlayer.Friends)
					{
						if (friend.Key == mapSNSIcon2.UuID)
						{
							MyFriend value3 = friend.Value;
							item.IconID = value3.Data.IconID;
						}
					}
					item.Icon = null;
					mapSNSIcon2.UpdateIcon(new List<MapSNSSetting> { item });
				}
			}
			if (mGame.RandomUserList.Count > 0)
			{
				mCurrentPage.MakeStrayUser();
			}
			if (mStrayTutorialFlg)
			{
				mStrayTutorialFlg = false;
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL10_3))
				{
					mCurrentPage.MakeTutorialStrayUser();
				}
			}
			if (mGame.mNetworkDataModified || (mFriendManager != null && mFriendManager.IsDataModified))
			{
				mGame.Save();
			}
			if (mGame.mGotGiftIds != null)
			{
				mGame.mGotGiftIds.Clear();
			}
			if (mGame.mGotSupportIds != null)
			{
				mGame.mGotSupportIds.Clear();
			}
			if (mGame.mGotApplyIds != null)
			{
				mGame.mGotApplyIds.Clear();
			}
			if (mGame.mGotSupportPurchaseIds != null)
			{
				mGame.mGotSupportPurchaseIds.Clear();
			}
			mGame.AchievementFriend();
			mGame.FirstBootFlg = false;
			if (mTipsScreenDisplayed)
			{
				mGameState.TipsScreenOff();
			}
			mState.Change(STATE.RESTORE_TRANSACTIONS);
			break;
		case STATE.RESTORE_TRANSACTIONS:
			if (GameMain.NO_NETWORK || !mProductListReceiveSuccess)
			{
				mState.Change(STATE.NETWORK_TUTCOMP_CHECK);
			}
			else
			{
				if (!mState.IsChanged())
				{
					break;
				}
				if (PurchaseManager.LogData.HasPLog())
				{
					PurchaseLog.PLog lastOnePLog3 = PurchaseManager.LogData.GetLastOnePLog();
					if (lastOnePLog3.purchaseState == 200)
					{
						ConfirmDialog confirmDialog3 = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
						confirmDialog3.Init(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverDesc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, DialogBase.DIALOG_SIZE.MEDIUM, string.Empty, string.Empty);
						confirmDialog3.SetClosedCallback(delegate
						{
							mState.Change(STATE.RESTORE_TRANSACTIONS_00);
						});
					}
					else
					{
						mState.Change(STATE.NETWORK_TUTCOMP_CHECK);
					}
				}
				else
				{
					mState.Change(STATE.NETWORK_TUTCOMP_CHECK);
				}
			}
			break;
		case STATE.RESTORE_TRANSACTIONS_00:
			if (mState.IsChanged())
			{
				PurchaseLog.PLog lastOnePLog = PurchaseManager.LogData.GetLastOnePLog();
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.TRANSACTION;
				mGame.Network_BuyGem(PurchaseManager.instance.GetItemIdx(lastOnePLog.itemID), lastOnePLog.receipt, lastOnePLog.signature);
				mGame.StartConnecting();
			}
			else
			{
				if (!GameMain.mBuyGemCheckDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mBuyGemSucceed)
				{
					PurchaseLog.PLog lastOnePLog2 = PurchaseManager.LogData.GetLastOnePLog();
					if (lastOnePLog2 != null)
					{
						CampaignLog.CLog lastCLog = PurchaseManager.CampaignLogData.GetLastCLog(lastOnePLog2.itemID);
						if (lastCLog != null)
						{
							CampaignData campaignData = null;
							List<CampaignData> mCampaignList = mGame.mCampaignList;
							for (int j = 0; j < mCampaignList.Count; j++)
							{
								CampaignData campaignData2 = mCampaignList[j];
								if (lastCLog.iap_campaign_id == campaignData2.iap_campaign_id)
								{
									campaignData = campaignData2;
									break;
								}
							}
							if (campaignData == null)
							{
								campaignData = new CampaignData();
								campaignData.iap_campaign_id = lastCLog.iap_campaign_id;
								campaignData.iapname = lastCLog.iapname;
								campaignData.debug_iapname = lastCLog.iapname;
								campaignData.endtime = lastCLog.endtime;
							}
							mGame.Network_CampaignCharge(campaignData.iap_campaign_id, lastCLog.datatime);
							if (campaignData != null)
							{
								if (mGame.mServerCramVariableData.PurchaseCampaign == null)
								{
									mGame.mServerCramVariableData.PurchaseCampaign = new Dictionary<int, ServerCramVariableData.PurchaseCampaignInfo>();
								}
								if (!mGame.mServerCramVariableData.PurchaseCampaign.ContainsKey(campaignData.iap_campaign_id))
								{
									mGame.mServerCramVariableData.PurchaseCampaign.Add(campaignData.iap_campaign_id, new ServerCramVariableData.PurchaseCampaignInfo
									{
										BuyCount = 0,
										ShowDialogCount = 0,
										StartOfferTime = 0L,
										Place = 0
									});
								}
								ServerCramVariableData.PurchaseCampaignInfo value = mGame.mServerCramVariableData.PurchaseCampaign[campaignData.iap_campaign_id];
								value.BuyCount++;
								mGame.mServerCramVariableData.PurchaseCampaign[campaignData.iap_campaign_id] = value;
								mGame.SaveServerCramVariableData();
								int itemKind = (int)PurchaseManager.instance.GetItemKind(mGame.GetCampaignIapName(campaignData.iap_campaign_id));
								ServerCram.GemPurchase(campaignData.iap_campaign_group_id, itemKind, (int)campaignData.RemainMinutes(), (int)campaignData.endtime, (int)GameMain.mBuyGemPlace, (int)value.StartOfferTime, value.ShowDialogCount, value.BuyCount);
							}
							PurchaseManager.CampaignLogData.DeleteCLog(campaignData.iap_campaign_id, true);
						}
					}
					PurchaseManager.LogData.PurchaseComplete();
					PurchaseManager.SavePurchaseData();
					ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
					confirmDialog.Init(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverSuccess"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, DialogBase.DIALOG_SIZE.SMALL, string.Empty, string.Empty);
					confirmDialog.SetClosedCallback(delegate
					{
						mState.Change(STATE.NETWORK_TUTCOMP_CHECK);
					});
					mState.Change(STATE.WAIT);
				}
				else
				{
					ConfirmDialog confirmDialog2 = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
					confirmDialog2.Init(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverFail"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, DialogBase.DIALOG_SIZE.MEDIUM, string.Empty, string.Empty);
					confirmDialog2.SetClosedCallback(delegate
					{
						mState.Change(STATE.NETWORK_TUTCOMP_CHECK);
					});
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.NETWORK_TUTCOMP_CHECK:
		{
			PlayerClearData _psd;
			if (mRewardCheckDoneStage10Clear || mGame.mTutorialManager.IsInitialTutorialCompleted() || !mGame.mPlayer.GetStageClearData(Def.SERIES.SM_FIRST, 100, out _psd))
			{
				if (mRewardShowStage10Clear && base.MapPage != null && base.MapPage.mTutorialCompButton != null)
				{
					base.MapPage.mTutorialCompButton.SetReservedFukidashiFlg("image", "icon_84jem");
				}
				mState.Change(STATE.INIT);
			}
			else if (!mGame.Network_RewardCheck(204))
			{
				mState.Change(STATE.INIT);
			}
			else
			{
				mState.Change(STATE.NETWORK_TUTCOMP_CHECK_00);
			}
			break;
		}
		case STATE.NETWORK_TUTCOMP_CHECK_00:
			if (!GameMain.mRewardCheckCheckDone)
			{
				break;
			}
			if (GameMain.mRewardCheckSucceed)
			{
				if (GameMain.mRewardCheckStatus == 0)
				{
					mRewardShowStage10Clear = true;
					if (base.MapPage != null && base.MapPage.mTutorialCompButton != null)
					{
						base.MapPage.mTutorialCompButton.SetReservedFukidashiFlg("image", "icon_84jem");
					}
				}
				mRewardCheckDoneStage10Clear = true;
			}
			mState.Change(STATE.INIT);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_REWARD_MAINTENACE_00);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_REWARD_SET);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 80);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_REWARD_SET:
			Server.ManualConnecting = true;
			mGame.Network_RewardSet(AccessoryConnectIndex);
			mState.Reset(STATE.NETWORK_REWARD_SET_00, true);
			break;
		case STATE.NETWORK_REWARD_SET_00:
		{
			if (!GameMain.mRewardSetCheckDone)
			{
				break;
			}
			Server.ManualConnecting = false;
			string empty3 = string.Empty;
			string empty4 = string.Empty;
			if (GameMain.mRewardSetSucceed)
			{
				AccessoryData accessoryData3 = mGame.GetAccessoryData(AccessoryConnectIndex);
				bool flag4 = mGame.mPlayer.SubAccessoryConnect(accessoryData3);
				AccessoryConnectIndex = 0;
				mGame.Save();
				if (GameMain.mRewardSetStatus == 0)
				{
					int index2 = accessoryData3.Index;
					if (index2 != 0)
					{
						int cur_series2 = (int)mGame.mPlayer.Data.CurrentSeries;
						int get_type2 = 4;
						if (mGame.IsDailyChallengePuzzle)
						{
							cur_series2 = 200;
						}
						ServerCram.GetAccessory(index2, cur_series2, get_type2);
					}
					if (flag4)
					{
						mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
						mState.Reset(STATE.NETWORK_04_03, true);
					}
					else
					{
						mState.Reset(STATE.UNLOCK_ACTING, true);
					}
				}
				else
				{
					empty3 = Localization.Get("GotError_Title");
					empty4 = Localization.Get("GotError_Desc");
					if (GameStateSMMapBase.mIsShowErrorDialog)
					{
						StartCoroutine(GrayOut());
						GetRewardDialog getRewardDialog = Util.CreateGameObject("NoGetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
						List<AccessoryData> list2 = new List<AccessoryData>();
						list2.Add(accessoryData3);
						getRewardDialog.Init(list2, true);
						getRewardDialog.SetClosedCallback(OnGetRewardDialogErrorClosed);
						mState.Reset(STATE.WAIT);
					}
					else
					{
						mState.Reset(STATE.UNLOCK_ACTING, true);
					}
				}
			}
			else
			{
				empty3 = Localization.Get("Network_Error_Title");
				empty4 = Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b");
				if (GameStateSMMapBase.mIsShowErrorDialog)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 82, OnConnectErrorAccessoryRetry, OnConnectErrorAccessoryAbandon);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					OnConnectErrorAccessoryAbandon();
				}
			}
			break;
		}
		case STATE.NETWORK_ADV_REWARD_SET:
		{
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			AccessoryData accessoryData2 = mGame.GetAccessoryData(AdvAccessoryConnectIndex);
			int[] mail_rewards_ids = new int[1] { accessoryData2.GetGotIDByNum() };
			mGame.Network_AdvSetReward(null, mail_rewards_ids, mGuID, is_retry);
			mState.Reset(STATE.NETWORK_ADV_REWARD_SET_00, true);
			break;
		}
		case STATE.NETWORK_ADV_REWARD_SET_00:
			if (!GameMain.mAdvSetRewardCheckDone)
			{
				break;
			}
			if (GameMain.mAdvSetRewardSucceed)
			{
				AccessoryData accessoryData = mGame.GetAccessoryData(AdvAccessoryConnectIndex);
				bool flag3 = mGame.mPlayer.SubAdvAccessoryConnect(accessoryData);
				AdvAccessoryConnectIndex = 0;
				mGame.Save();
				int index = accessoryData.Index;
				if (index != 0)
				{
					int cur_series = (int)mGame.mPlayer.Data.CurrentSeries;
					int get_type = 12;
					if (mGame.IsDailyChallengePuzzle)
					{
						cur_series = 200;
					}
					ServerCram.GetAccessory(index, cur_series, get_type);
				}
				if (flag3)
				{
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mState.Reset(STATE.NETWORK_ADV_GET_MAIL, true);
				}
				else
				{
					mState.Reset(STATE.UNLOCK_ACTING, true);
				}
				mGuID = null;
			}
			else
			{
				string empty = string.Empty;
				string empty2 = string.Empty;
				empty = Localization.Get("Network_Error_Title");
				empty2 = Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b");
				if (GameStateSMMapBase.mAdvIsShowErrorDialog)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 84, OnConnectErrorAccessoryRetry, OnConnectErrorAdvAccessoryAbandon);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					OnConnectErrorAdvAccessoryAbandon();
				}
			}
			break;
		case STATE.NETWORK_RESUME_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_MAINTENACE_00);
			break;
		case STATE.NETWORK_RESUME_MAINTENACE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_RESUME_GAMEINFO);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 86);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_GAMEINFO:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_GAMEINFO_00);
			break;
		case STATE.NETWORK_RESUME_GAMEINFO_00:
			if (!GameMain.mGameProfileCheckDone)
			{
				break;
			}
			if (GameMain.mGameProfileSucceed)
			{
				if (mGame.mGameProfile.LinkBanner != null && mGame.mGameProfile.LinkBanner.BannerList.Count > 0)
				{
					mGame.LinkBannerList_Clear();
					LinkBannerManager.Instance.GetLinkBannerRequest(mGame.mLBRequest, mGame.mGameProfile.LinkBanner, OnGetLinkBannerComplete);
				}
				mState.Change(STATE.NETWORK_RESUME_CAMPAIGN);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 88, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN:
			if (PurchaseFullDialog.IsShow())
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			else if (mGame.Network_CampaignCheck(mIsInitialTutorialCompleted))
			{
				mState.Change(STATE.NETWORK_RESUME_CAMPAIGN_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_RESUME_LOGINBONUS_00);
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				mGame.DoLoginBonusServerCheck = false;
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
					mState.Change(STATE.NETWORK_04);
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 92, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.AUTHERROR_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Transfer_CertError_Title"), Localization.Get("Transfered_TitleBack_Desc2"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.AUTHERROR_RETURN_WAIT);
			break;
		case STATE.MAINTENANCE_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("MaintenanceMode_ReturnTitle_Title"), Localization.Get("MaintenanceMode_ReturnTitle_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.MAINTENANCE_RETURN_WAIT);
			mGame.IsTitleReturnMaintenance = true;
			break;
		case STATE.NETWORK_INFORMATION:
			InformationManager.Instance.MightUpdateInfo();
			if (InformationManager.Instance.IsConnectDone && InformationManager.Instance.IsConnectSuccessed)
			{
				mState.Change(STATE.MAIN);
			}
			else
			{
				mState.Reset(STATE.NETWORK_INFORMATION_WAIT);
			}
			break;
		case STATE.NETWORK_INFORMATION_WAIT:
			if (!InformationManager.Instance.IsConnectDone)
			{
				break;
			}
			if (InformationManager.Instance.IsConnectSuccessed)
			{
				mState.Change(STATE.MAIN);
				break;
			}
			mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 106, delegate(int _state)
			{
				mState.Change((STATE)_state);
			}, null);
			mState.Change(STATE.WAIT);
			break;
		case STATE.NETWORK_INFORMATION_GASHA:
			mGame.Network_AdvGetGashaInfo();
			mState.Change(STATE.NETWORK_INFORMATION_GASHA_WAIT);
			break;
		case STATE.NETWORK_INFORMATION_GASHA_WAIT:
			if (!GameMain.mAdvGetGashaInfoCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetGashaInfoSucceed)
			{
				mState.Change(STATE.MAIN);
				break;
			}
			mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 108, delegate(int _state)
			{
				mState.Change((STATE)_state);
			}, null);
			mState.Change(STATE.WAIT);
			break;
		}
		UpdateMapPages();
		mState.Update();
	}

	public override IEnumerator LoadGameState()
	{
		mGame.SwitchTileData(false);
		mGame.mAdvMode = false;
		float ss = Time.realtimeSinceStartup;
		float totalstart = Time.realtimeSinceStartup;
		if (Def.IsEventSeries(mGame.mPlayer.Data.CurrentSeries) || Def.IsAdvSeries(mGame.mPlayer.Data.CurrentSeries))
		{
			if (Def.IsEventSeries(mGame.mPlayer.Data.PreviousSeries) || Def.IsAdvSeries(mGame.mPlayer.Data.PreviousSeries))
			{
				mGame.mPlayer.SetNextSeries(Def.SERIES.SM_FIRST);
			}
			else
			{
				mGame.mPlayer.SetNextSeries(mGame.mPlayer.Data.PreviousSeries);
			}
		}
		mGame.Save();
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		yield return StartCoroutine(LoadCommonResources());
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		GameObject parent = mRoot;
		mAnchorBottom = Util.CreateAnchorWithChild("MapAnchorBottom", parent, UIAnchor.Side.Bottom, camera).gameObject;
		mAnchorRight = Util.CreateAnchorWithChild("MapAnchorRight", parent, UIAnchor.Side.Right, camera).gameObject;
		mAnchorCenter = Util.CreateAnchorWithChild("MapAnchorCenter", parent, UIAnchor.Side.Center, camera).gameObject;
		mMapPageRoot = Util.CreateGameObject("MapPageRoot", mAnchorCenter.gameObject);
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		mMapPageData = GameMain.GetCurrentMapPageData();
		mCurrentPage = Util.CreateGameObject("NewMapPage", mMapPageRoot).AddComponent<SMMapPage>();
		mCurrentPage.SetGameState(this);
		mCurrentPage.LoadRouteAnimeAsync(currentSeries, mMapPageData);
		mTipsScreenDisplayed = false;
		if (mGame.mPlayer.IsStoryCompleted(0))
		{
			mTipsScreenDisplayed = true;
			yield return StartCoroutine(mGameState.CoTipsScreenOn(2, false));
		}
		else
		{
			mGame.StartLoading();
		}
		mStateConnecting.Reset(STATE.NETWORK_PROLOGUE, true);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			if (!GameMain.DEBUG_TERMINAL_ENTRY)
			{
				Server.DebugTerminalRegistration += Server_OnDebugTerminal;
				Network_DebugTerminal();
			}
		}
		else if (GameMain.USE_DEBUG_TIER)
		{
			Server.DebugTerminalRegistration += Server_OnDebugTerminal;
			Network_DebugTerminal();
		}
		else
		{
			switch (new BIJMD5(BIJUnity.getBundleName()).ToString())
			{
			case "17bec987fc8488f35b98311737c93a93":
			case "0da4b5fb83b82cf5e399ccc6a31eb6a7":
			case "1b6293f9f5a9343a3188a4584a6d6d00":
			case "66c61d1ce3f478b723c9b985ef95844d":
				Server.DebugTerminalRegistration += Server_OnDebugTerminal;
				Network_DebugTerminal();
				break;
			}
		}
		ResourceManager.LoadImage("FADE");
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(currentSeries, out data);
		List<SMRoadBlockSetting> list = mMapPageData.GetRoadBlockInCourse();
		if (data.NextRoadBlockLevel == 0)
		{
			foreach (SMRoadBlockSetting s in list)
			{
				if (s.mSubNo != 0)
				{
					continue;
				}
				int nextRB = s.RoadBlockLevel;
				data.NextRoadBlockLevel = nextRB;
				break;
			}
			data.MaxLevel = Def.GetStageNo(mMapPageData.GetMap().MaxLevel, 0);
			mGame.mPlayer.Data.SetMapData(currentSeries, data);
		}
		else if (data.NextRoadBlockLevel == Def.GetStageNo(999, 0))
		{
			int latestUnlock = data.LatestUnlockedRoadBlock();
			SMRoadBlockSetting rbSetting = mMapPageData.GetRoadBlockByRBNo(latestUnlock + 1);
			if (rbSetting != null)
			{
				SMMapSetting mapSetting = mMapPageData.GetMap();
				if (mapSetting.ModuleMaxRoadBlockID != rbSetting.RoadBlockID)
				{
					data.NextRoadBlockLevel = rbSetting.RoadBlockLevel;
					mGame.mPlayer.Data.SetMapData(currentSeries, data);
				}
			}
		}
		long day = DateTimeUtil.BetweenDays0(mGame.mPlayer.Data.LastApplyRandomUserTime, DateTime.Now);
		if (day > 0)
		{
			mGame.mPlayer.Data.ApplyRandomUserNum = 0;
		}
		mPageMoveLock = Def.PRESS.NONE;
		mPageMoved = false;
		SetScrollPower(0f);
		mFriendIconVisible = true;
		yield return StartCoroutine(mCurrentPage.Init(currentSeries, data.MaxLevel, OnStageButtonPushed, OnMapAccessoryTapped, OnMapRoadBlockTapped, OnMapSNSTapped, OnMapChangeTapped));
		mMaxPage = mCurrentPage.MaxPage;
		mMaxAvailablePage = mCurrentPage.MaxAvailablePage;
		mMaxModulePage = mCurrentPage.MaxModulePageFromMaxLevel;
		mCurrentPage.SetEnableButton(false);
		if (mGame.mPlayer.PlayableMaxLevel == 100)
		{
			yield return StartCoroutine(mCurrentPage.ActiveMapButton(0));
		}
		CheckOpenNewStage();
		UpdateMapPages();
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		List<ResourceInstance> asyncList = new List<ResourceInstance>
		{
			ResourceManager.LoadSsAnimationAsync("EFFECT_SHINE", true),
			ResourceManager.LoadSsAnimationAsync("EFFECT_FUKIDASHI_R", true),
			ResourceManager.LoadSsAnimationAsync("EFFECT_FUKIDASHI_L", true)
		};
		UIFont font = GameMain.LoadFont();
		mGame.mUseBooster0 = false;
		mGame.mUseBooster1 = false;
		mGame.mUseBooster2 = false;
		mGame.mUseBooster3 = false;
		mGame.mUseBooster4 = false;
		mGame.mUseBooster5 = false;
		CheckAccessoryDisplay();
		SetLayout(Util.ScreenOrientation);
		float cameraY = ClearedStagePosition();
		yield return StartCoroutine(DirectMapMove(0f - cameraY));
		SetLayout(Util.ScreenOrientation);
		for (int j = 0; j < mMaxModulePage; j++)
		{
			StartCoroutine(mCurrentPage.ActiveMapButton(j));
		}
		ResScriptableObject so = ResourceManager.LoadScriptableObjectAsync("DAILY_CHALLENGE");
		while (so.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		if (so != null && so.ScriptableObject != null && so.ScriptableObject is DailyChallengeSheetList)
		{
			DailyChallengeSheetList sheetList = so.ScriptableObject as DailyChallengeSheetList;
			string[] sheetNames = sheetList.SheetNames;
			mGame.mDailyChallengeData.Clear();
			if (sheetNames != null)
			{
				string[] array = sheetNames;
				foreach (string sheetName in array)
				{
					DailyChallengeSheet sheet = sheetList.GetSheet(sheetName);
					if (sheet != null)
					{
						mGame.mDailyChallengeData.Add(sheet);
					}
				}
			}
		}
		ResourceManager.UnloadScriptableObject("DAILY_CHALLENGE");
		DEFAULT_BGM = "BGM_MAP_" + Def.SeriesPrefix[currentSeries];
		mResSound = ResourceManager.LoadSoundAsync(DEFAULT_BGM);
		for (int i = 0; i < asyncList.Count; i++)
		{
			while (asyncList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		if (!mTipsScreenDisplayed)
		{
			mGame.FinishLoading();
		}
		ResImage image = ResourceManager.LoadImageAsync("HUD2", true);
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		ResourceManager.EnableLoadAllResourceFromAssetBundle();
		LoadGameStateFinished();
		SetLayout(Util.ScreenOrientation);
	}

	public override IEnumerator UnloadGameState()
	{
		if (mTipsScreenDisplayed)
		{
			mGameState.TipsScreenOff();
		}
		else
		{
			Server.ManualConnecting = false;
		}
		ResourceManager.UnloadSound(DEFAULT_BGM);
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		if (mEventAppeal != null)
		{
			UnityEngine.Object.Destroy(mEventAppeal.gameObject);
			mEventAppeal = null;
		}
		if (mCockpit != null)
		{
			GameMain.SafeDestroy(mCockpit.gameObject);
			mCockpit = null;
		}
		yield return StartCoroutine(mCurrentPage.DeactiveAllObject());
		UnityEngine.Object.Destroy(mAnchorBottom.gameObject);
		mAnchorBottom = null;
		UnityEngine.Object.Destroy(mAnchorRight.gameObject);
		mAnchorRight = null;
		UnityEngine.Object.Destroy(mAnchorCenter.gameObject);
		mAnchorCenter = null;
		UnityEngine.Object.Destroy(mCurrentPage.gameObject);
		UnityEngine.Object.Destroy(mRoot);
		ResourceManager.UnloadSsAnimationAll();
		ResourceManager.UnloadImageAll();
		mGame.UnloadNotPreloadSoundResources(true);
		UnloadGameStateFinished();
		yield return 0;
	}

	protected override void Update_INIT()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(currentSeries, out data);
		mOldMaxLevel = data.MaxLevel;
		int stageNo = Def.GetStageNo(mMapPageData.GetMap().MaxLevel, 0);
		if (stageNo > mOldMaxLevel)
		{
			data.MaxLevel = stageNo;
		}
		CheckStageClear(ref data);
		mGame.mPlayer.Data.SetMapData(currentSeries, data);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void Update_UNLOCK_ACTING()
	{
		if (!isLoadFinished() || !mGameState.IsFadeFinished)
		{
			return;
		}
		if (mGame.DoLoginBonusServerCheck)
		{
			mGame.DoLoginBonusServerCheck = false;
			mState.Reset(STATE.NETWORK_RESUME_MAINTENACE, true);
		}
		else if (AccessoryConnectCheck())
		{
			mState.Reset(STATE.NETWORK_REWARD_MAINTENACE, true);
		}
		else if (AdvAccessoryConnectCheck())
		{
			mState.Reset(STATE.NETWORK_ADV_REWARD_SET, true);
		}
		else if (StoryDemoStartCheck())
		{
			mCurrentStoryDemo = mShowStoryDemoList[0];
			mShowStoryDemoList.RemoveAt(0);
			if (mCurrentStoryDemo.DemoIndex != -1)
			{
				int stageno;
				if (mCurrentStoryDemo.IsStageDemo(out stageno))
				{
					mCurrentPage.ShownDemo(stageno);
				}
				StartStoryDemo(mCurrentStoryDemo.DemoIndex);
			}
		}
		else if (mAutoUnlockPartner != null)
		{
			mAutoUnlockAccessory = mAutoUnlockPartner;
			if (mCurrentPage.RemoveSilhouette(mAutoUnlockPartner.AnimeLayerName))
			{
				mState.Reset(STATE.ACCESSORIEOPEN_WAIT);
			}
			else
			{
				mState.Reset(STATE.UNLOCK_ACTING);
			}
			mAutoUnlockPartner = null;
		}
		else if (mAutoUnlockAccessory != null)
		{
			bool a_add = true;
			if (mGame.mPlayer.IsCompanionUnlock(mAutoUnlockAccessory.GetCompanionID()))
			{
				a_add = false;
			}
			StartCoroutine(GrayOut());
			mGetPartnerDialog = Util.CreateGameObject("GetPartnerDialog", mRoot).AddComponent<GetPartnerDialog>();
			mGetPartnerDialog.Init(mAutoUnlockAccessory, GetPartnerDialog.PLACE.MAIN, a_add);
			mGetPartnerDialog.SetClosedCallback(OnGetPartnerDialogClosed);
			mState.Reset(STATE.WAIT, true);
			if (mGame.SetPartnerCollectionAttention(mAutoUnlockAccessory))
			{
				mCockpit.SetEnableMenuButtonAttention(true);
			}
			mAutoUnlockAccessory = null;
		}
		else if (mAlterUnlockAccessory.Count > 0)
		{
			StartCoroutine(GrayOut());
			for (int j = 0; j < mAlterUnlockAccessory.Count; j++)
			{
				AccessoryData accessoryData = mAlterUnlockAccessory[j];
				if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
				{
					mGame.AddAccessoryConnect(accessoryData.Index);
					continue;
				}
				mGame.mPlayer.AddAccessory(accessoryData);
				mGame.mPlayer.SetCollectionNotify(accessoryData.Index);
				mCockpit.SetEnableMenuButtonAttention(true);
			}
			mGame.Save();
			mAlternateRewardDialog = Util.CreateGameObject("AlterRewardDialog", mRoot).AddComponent<AlternateRewardDialog>();
			mAlternateRewardDialog.Init(mAlterUnlockAccessory);
			mAlternateRewardDialog.SetClosedCallback(delegate
			{
				mAlternateRewardDialog = null;
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			});
			mState.Reset(STATE.WAIT, true);
			mAlterUnlockAccessory.Clear();
		}
		else if (mGame.GetPresentBoxNum() > 0)
		{
			mOpenPuzzleAccessory = mGame.GetPresentBox(0);
			if (mOpenPuzzleAccessory.AccessoryType != AccessoryData.ACCESSORY_TYPE.TROPHY)
			{
				StartCoroutine(GrayOut());
			}
			int num = mGame.GetPresentBoxNum();
			if (mOpenPuzzleAccessory != null)
			{
				if (num > 1)
				{
					int num2 = 0;
					while (num > 1)
					{
						mOtherOpenPuzzleAccessory = mGame.GetPresentBox(num - 1);
						if (mOpenPuzzleAccessory.AccessoryType == mOtherOpenPuzzleAccessory.AccessoryType)
						{
							mBundleAccessoryCount++;
							mGame.RemovePresentBox(mOtherOpenPuzzleAccessory);
							num2 = mOtherOpenPuzzleAccessory.GetGotIDByNum() + mOpenPuzzleAccessory.GetGotIDByNum();
						}
						num--;
					}
					if (mOpenPuzzleAccessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.SP_DAILY)
					{
						mGame.RemovePresentBox(mOpenPuzzleAccessory);
						mGetSPStampDialog = Util.CreateGameObject("GetSPStampDialog", mRoot).AddComponent<GetSPStampDialog>();
						mGetSPStampDialog.Init(mOpenPuzzleAccessory);
						mGetSPStampDialog.SetClosedCallback(OnAccessoryGetDailyClosed);
						mBundleAccessoryCount = 0;
						mState.Change(STATE.WAIT);
					}
					else
					{
						mGame.RemovePresentBox(mOpenPuzzleAccessory);
						mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
						mGetAccessoryDialog.Init(mOpenPuzzleAccessory, mBundleAccessoryCount, false);
						mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
						mBundleAccessoryCount = 0;
						mState.Change(STATE.WAIT);
					}
				}
				else
				{
					mGame.RemovePresentBox(mOpenPuzzleAccessory);
					if (mOpenPuzzleAccessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.SP_DAILY)
					{
						mGetSPStampDialog = Util.CreateGameObject("GetSPStampDialog", mRoot).AddComponent<GetSPStampDialog>();
						mGetSPStampDialog.Init(mOpenPuzzleAccessory);
						mGetSPStampDialog.SetClosedCallback(OnAccessoryGetDailyClosed);
						mState.Reset(STATE.WAIT, true);
					}
					else
					{
						mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
						mGetAccessoryDialog.Init(mOpenPuzzleAccessory, mBundleAccessoryCount, false);
						mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
						mState.Change(STATE.WAIT);
					}
				}
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
		}
		else if (mCurrentPage.IsOpenRoadBlock)
		{
			mCurrentPage.StartRoadBlockOpenEffect();
			mState.Change(STATE.RBOPEN_WAIT);
		}
		else if (mGame.mPlayer.StageOpenList.Count > 0)
		{
			if (mIsOpenNewChapter)
			{
				mIsOpenNewChapter = false;
				mGame.PlaySe("SE_OPEN_NEWCHAPTER", -1);
			}
			mGame.mPlayer.StageOpenList.Sort();
			int num3 = mGame.mPlayer.StageOpenList[0];
			mGame.mPlayer.StageOpenList.RemoveAt(0);
			int a_main;
			int a_sub;
			Def.SplitStageNo(num3, out a_main, out a_sub);
			mCurrentPage.StartStageOpenEffect(num3, mGame.mPlayer.StageOpenList.Count);
			mGame.mPlayer.SetStageStatus(mGame.mPlayer.Data.CurrentSeries, num3, Player.STAGE_STATUS.LOCK);
			if (mGame.mPlayer.StageOpenList.Count == 0)
			{
				int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
				int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
				mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, -1);
			}
			mState.Change(STATE.STAGEOPEN_WAIT);
			if (mGame.mPlayer.StageOpenList.Count == 0)
			{
				mGame.Save();
			}
		}
		else if (mGame.mPlayer.StageUnlockList.Count > 0)
		{
			mGame.mPlayer.StageUnlockList.Sort();
			int stageNo = 0;
			int count = mGame.mPlayer.StageUnlockList.Count;
			if (count > 2)
			{
				while (count > 1)
				{
					ProcessMultiUnlock();
					count = mGame.mPlayer.StageUnlockList.Count;
				}
				stageNo = mGame.mPlayer.StageUnlockList[count - 1];
				mGame.mPlayer.StageUnlockList.Clear();
			}
			else
			{
				stageNo = mGame.mPlayer.StageUnlockList[0];
				mGame.mPlayer.StageUnlockList.RemoveAt(0);
				mGame.mPlayer.SetStageStatus(mGame.mPlayer.Data.CurrentSeries, stageNo, Player.STAGE_STATUS.UNLOCK);
			}
			mGame.mPlayer.AvatarMoveList.Clear();
			int a_main2;
			int a_sub2;
			Def.SplitStageNo(stageNo, out a_main2, out a_sub2);
			mCurrentPage.StartStageUnlockEffect(stageNo, true);
			PlayerMapData data;
			mGame.mPlayer.Data.GetMapData(mCurrentPage.Series, out data);
			if (data != null)
			{
				if (data.TempStageClearData.Count > 0)
				{
					int num4 = data.TempStageClearData.FindIndex((PlayerClearData a) => a.Series == mCurrentPage.Series && a.StageNo == stageNo);
					if (num4 != -1)
					{
						PlayerClearData playerClearData = data.TempStageClearData[num4];
						if (playerClearData != null)
						{
							mGame.mPlayer.AddStageClearData(playerClearData);
							mGame.mPlayer.SetClearStageFromTemporary(playerClearData.StageNo);
							mGame.mPlayer.SetPlayableMaxLevel(mCurrentPage.Series, Def.GetStageNo(a_main2 + 1, 0));
							data.TempStageClearData.Remove(playerClearData);
							CheckOpenNewStage();
							CheckUnlockNewStage();
							CheckStageClear(ref data);
							SMMapStageSetting mapStage = mMapPageData.GetMapStage(a_main2, a_sub2);
							if (mapStage != null)
							{
								int storyDemoAtStageSelect = mapStage.GetStoryDemoAtStageSelect();
								if (storyDemoAtStageSelect != -1 && !mGame.mPlayer.IsStoryCompleted(storyDemoAtStageSelect))
								{
									mGame.mPlayer.StoryComplete(storyDemoAtStageSelect);
								}
								for (int k = 0; k < mapStage.BoxID.Count; k++)
								{
									Def.ITEM_CATEGORY iTEM_CATEGORY = mapStage.BoxKind[k];
									int num5 = mapStage.BoxID[k];
									Def.UNLOCK_TYPE uNLOCK_TYPE = mapStage.BoxSpawnCondition[k];
									if (iTEM_CATEGORY == Def.ITEM_CATEGORY.ACCESSORY && uNLOCK_TYPE == Def.UNLOCK_TYPE.STAR)
									{
										int num6 = mapStage.BoxSpawnConditionID[k];
										if (num6 <= playerClearData.GotStars && !mGame.mPlayer.IsAccessoryUnlock(num5))
										{
											mGame.AddAccessoryFromPresentBox(num5);
										}
									}
								}
							}
							if (playerClearData.Series == Def.SERIES.SM_R && playerClearData.StageNo == 4500 && mGame.mPlayer.IsAccessoryUnlock(1121))
							{
								mGame.mPlayer.ResetAccessory(1121);
							}
							Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mCurrentPage.Series, playerClearData.StageNo);
							mCurrentPage.ChangeStageButton(playerClearData.StageNo, stageStatus, (byte)playerClearData.GotStars, true);
						}
					}
				}
				if (data.TempStageChallengeData.Count > 0)
				{
					int num7 = data.TempStageChallengeData.FindIndex((PlayerStageData a) => a.Series == mCurrentPage.Series && a.StageNo == stageNo);
					if (num7 != -1)
					{
						PlayerStageData playerStageData = data.TempStageChallengeData[num7];
						if (playerStageData != null)
						{
							mGame.mPlayer.SetMapStageChallengeData(mCurrentPage.Series, playerStageData);
							data.TempStageChallengeData.Remove(playerStageData);
						}
					}
				}
			}
			if (mGame.mPlayer.StageUnlockList.Count == 0)
			{
				mGame.Save();
			}
			mState.Change(STATE.STAGEUNLOCK_WAIT);
		}
		else if (!mActiveTutorialFlg && TutorialStartCheckOnInit(mWallpaperTutorialFlg))
		{
			mActiveTutorialFlg = true;
			mWallpaperTutorialFlg = false;
			mState.Change(STATE.TUTORIAL);
			mGame.TrophyGuideWinCompFlg = false;
		}
		else if (mGame.mPlayer.HeartUpPiece >= 15)
		{
			mGame.mPlayer.SubHeartPiece(15);
			GiftItem giftItem = new GiftItem();
			giftItem.Data.Message = Localization.Get("Gift_RatingReward");
			giftItem.Data.ItemCategory = 1;
			giftItem.Data.ItemKind = 1;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = 1;
			mGame.mPlayer.AddGift(giftItem);
			ServerCram.ArcadeGetItem(1, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 2, 1, 30, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
			mGame.Save();
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init("icon_heart", Localization.Get("GetAccessory_Item_Title"), string.Format(Localization.Get("GetAccessory_HEART_Desc"), 1));
			mGetAccessoryDialog.SetClosedCallback(OnGetHeartPieaceDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else if (mGame.mPlayer.AvatarMoveList.Count > 0)
		{
			mGame.mPlayer.AvatarMoveList.Sort();
			int count2 = mGame.mPlayer.AvatarMoveList.Count;
			int a_nextStage = mGame.mPlayer.AvatarMoveList[count2 - 1];
			mGame.mPlayer.AvatarMoveList.Clear();
			mCurrentPage.StartAvatarMove(a_nextStage);
			mGame.Save();
			mState.Change(STATE.AVATARMOVE_WAIT);
		}
		else if (mGame.AutoPopupStageNo > 0)
		{
			mFirstTutorialFlg = true;
			OnStageButtonPushed(mGame.AutoPopupStageNo);
			mGame.ResetAutoPopupStage();
		}
		else if (mGame.TrophyGuideWinCompFlg && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.TROPHY_GUIDE_WIN))
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.TROPHY_GUIDE_WIN);
			mGame.TrophyGuideWinCompFlg = false;
			mTrophyGuideDialog = Util.CreateGameObject("TrophyGuideDialog", mRoot).AddComponent<TrophyGuideDialog>();
			mTrophyGuideDialog.Init(false);
			mTrophyGuideDialog.SetClosedCallback(TrophyGuideDialogClosed);
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mGame.Save();
		}
		else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST && mIsSailorGuardianTutorialDialog)
		{
			mIsSailorGuardianTutorialDialog = false;
			ImageTextDescDialog imageTextDescDialog = Util.CreateGameObject("ImageTextDescDialog", mRoot).AddComponent<ImageTextDescDialog>();
			imageTextDescDialog.Init(new ImageTextDescDialog.ConfigLinkBanner
			{
				BaseURL = mGame.mGameProfile.LinkBannerBaseURL,
				FileName = "chara_info.png",
				Desc = string.Empty,
				DescRowMin = 0,
				ImageWidth = 500,
				ImageHeight = 400,
				Title = Localization.Get("5fighters_Title"),
				Buttons = ImageTextDescDialog.BUTTONS.OK,
				UsePresetSize = true,
				PresetSize = DialogBase.DIALOG_SIZE.LARGE
			});
			imageTextDescDialog.SetCallback(ImageTextDescDialog.SELECT_ITEM.POSITIVE, delegate
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			});
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
		}
		else if (mLoginBonusScreen == null && mGame.mTutorialManager.IsInitialTutorialCompleted() && mGame.mLoginBonusData != null && mGame.mLoginBonusData.HasLoginData)
		{
			GetLoginBonusResponse loginBonusByIndex = mGame.mLoginBonusData.GetLoginBonusByIndex(0);
			if (loginBonusByIndex.IsDislpay)
			{
				mLoginBonusScreen = Util.CreateGameObject("LoginBonusScreen", mRoot).AddComponent<LoginBonusScreen>();
				mLoginBonusScreen.init(loginBonusByIndex, delegate
				{
					mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
					mState.Reset(STATE.UNLOCK_ACTING, true);
					mLoginBonusScreen = null;
				});
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
		}
		else if (!mIsInitialTutorialCompleted && mGame.mTutorialManager.IsInitialTutorialCompleted() && PurchaseManager.instance.GetItemFromKIND(PItem.KIND.CA0).storeInfo && !mGame.GameProfile.mOldPurchaseDialog && !mGotCampaignData)
		{
			if (!mGettingCampaignData)
			{
				mGettingCampaignData = true;
				mGame.Network_CampaignCheckForce(true);
			}
			else if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
					mIsInitialTutorialCompleted = true;
					mGotCampaignData = true;
				}
				else
				{
					StartCoroutine(GrayOut());
					ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
					confirmDialog.Init(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkError_CampaignDesc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
					confirmDialog.SetClosedCallback(OnCampaignErrorDialogClosed);
					mState.Reset(STATE.WAIT, true);
				}
				mGettingCampaignData = false;
			}
		}
		else
		{
			if (MightOpenCampaignDialog())
			{
				return;
			}
			BIJSNS sns = SocialManager.Instance[SNS.GooglePlayGameServices];
			if (GameMain.IsSNSLogined(sns))
			{
				long num8 = DateTimeUtil.BetweenMinutes(mGame.AchievementCheckTime, DateTime.Now);
				long num9 = 15L;
				if (num8 > num9)
				{
					mGame.AchievementAllCheck();
					mGame.AchievementCheckTime = DateTime.Now;
				}
			}
			if (mGame.mPlayer.GrowUpPiece >= 24)
			{
				mGame.mPlayer.SubGrowupPiece(24);
				mGame.mPlayer.AddGrowup(1);
				int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
				int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
				int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
				int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
				int advLastStage = mGame.mPlayer.AdvLastStage;
				int advMaxStamina = mGame.mPlayer.AdvMaxStamina;
				ServerCram.ArcadeGetItem(1, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 10, 1, 30, advLastStage, advMaxStamina);
			}
			mState.Change(STATE.MAIN);
		}
	}

	public void TrophyGuideDialogClosed(TrophyGuideDialog.SELECT_ITEM item)
	{
		if (mTrophyGuideDialog != null)
		{
			UnityEngine.Object.Destroy(mTrophyGuideDialog.gameObject);
			mTrophyGuideDialog = null;
		}
		if (item == TrophyGuideDialog.SELECT_ITEM.MAP)
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
		else
		{
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TROPHY);
		}
	}

	protected void BGMSoundCheck()
	{
		switch (mState.GetStatus())
		{
		case STATE.PLAY:
		case STATE.STORY_DEMO_FADE:
		case STATE.STORY_DEMO:
			return;
		}
		if (mResSound != null && mResSound.LoadState == ResourceInstance.LOADSTATE.DONE)
		{
			if (((ResSound)mResSound).mAudioClip == null)
			{
				mBGMLoadError = true;
			}
			else if (mGame.mPlayer.IsStoryCompleted(0) && !mIsPlayingBGM && mShowStoryDemoList.Count == 0)
			{
				mGame.PlayMusic(DEFAULT_BGM, 0, true, false, true);
				mGame.SetMusicVolume(1f, 0);
				mIsPlayingBGM = true;
			}
			mResSound = null;
		}
	}

	protected override void Update_MAIN()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(currentSeries, out data);
		int maxLevel = mGame.mGameProfile.GetMaxLevel(currentSeries);
		if (maxLevel > data.MaxLevel)
		{
			data.MaxLevel = maxLevel;
		}
		if (GameMain.USE_DEBUG_DIALOG)
		{
		}
		if (data.MaxLevel > mOldMaxLevel)
		{
			mGame.Save();
			mFadeBeforePos = mCurrentPage.mAvater.Position.y;
			int maxLevel2 = data.MaxLevel;
			int num = mCurrentPage.GetMapIndexByStage(maxLevel2);
			int mapIndexByStage = mCurrentPage.GetMapIndexByStage(mOldMaxLevel);
			if (num == mapIndexByStage)
			{
				mConfirmDialog = Util.CreateGameObject("NewAreaOpen", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("Area_Release_Title"), Localization.Get("Area_Release_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnNewAreaOpenDialogClosedNoEffect);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				if (num > mMaxModulePage - 1)
				{
					num = mMaxModulePage - 1;
				}
				float map_DeviceOffset = Map_DeviceOffset;
				float num2 = 1136f * (float)num + map_DeviceOffset;
				mFadeTargetPos = num2;
				mFadeTargetPage = num;
				mState.Change(STATE.NEWAREA_OPEN);
			}
			mOldMaxLevel = data.MaxLevel;
		}
		if (mState.GetStatus() == STATE.MAIN && mState.GetNextStatus() == STATE.MAIN)
		{
			bool flag = false;
			if (mGame.GetRemainDailyEventTime() > 0)
			{
				if (!mDailyEventsDidAppered)
				{
					UpdateMapDailyEvents();
					mState.Change(STATE.DAILY_EVENT_APPEAR);
					mDailyEventsDidAppered = true;
					mDailyEventsDidDisappered = false;
					flag = true;
				}
			}
			else if (!mDailyEventsDidDisappered)
			{
				mState.Change(STATE.DAILY_EVENT_DISAPPEAR);
				mDailyEventsDidAppered = false;
				mDailyEventsDidDisappered = true;
				flag = true;
			}
			if (!flag)
			{
			}
		}
		MapMove();
		if (mGame.mTouchDownTrg)
		{
			if (mCurrentPage != null)
			{
				Vector3 position = new Vector3(mGame.mTouchPos.x, mGame.mTouchPos.y, 0f);
				mPageMoveLock = mCurrentPage.OnScreenPress(mCamera.ScreenToWorldPoint(position));
			}
		}
		else if (mGame.mBackKeyTrg)
		{
			if (GameMain.USE_DEBUG_DIALOG && GameMain.HIDDEN_DEBUG_BUTTON)
			{
				OnDebugPushed();
			}
			else
			{
				OnBackKeyPushed();
			}
		}
	}

	public void CheckOpenNewStage(bool a_lookNotice = true)
	{
		int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
		int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
		int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
		int a_nextChapter = -1;
		int a_main;
		int a_sub;
		if (a_lookNotice)
		{
			Def.SplitStageNo(openNoticeLevel, out a_main, out a_sub);
		}
		else
		{
			Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
		}
		if (nextRoadBlockLevel > playableMaxLevel && (mGame.mPlayer.Data.CurrentSeries != 0 || (playableMaxLevel != 1000 && playableMaxLevel != 2000)))
		{
			a_main++;
		}
		SMChapterSetting chapterSearch = mMapPageData.GetChapterSearch(a_main);
		if (chapterSearch != null)
		{
			a_nextChapter = Def.GetStageNo(chapterSearch.mSubNo, 0);
		}
		if (playableMaxLevel == 100)
		{
			mGame.mPlayer.StageOpenList.Add(100);
			foreach (int stageOpen in mGame.mPlayer.StageOpenList)
			{
				mGame.mPlayer.SetStageStatus(mGame.mPlayer.Data.CurrentSeries, stageOpen, Player.STAGE_STATUS.UNLOCK);
				mCurrentPage.ChangeStageButton(stageOpen, Player.STAGE_STATUS.UNLOCK);
			}
			mGame.mPlayer.StageOpenList.Clear();
			mGame.mPlayer.SetOpenNoticeLevel(mGame.mPlayer.Data.CurrentSeries, Def.GetStageNo(1, 0));
			if (mCurrentPage.Series == Def.SERIES.SM_FIRST)
			{
				if (!mGame.mPlayer.IsTutorialSkipped())
				{
					mIsFirstOpenStage = true;
					GrayOutForce(1f);
					mGame.SetAutoPopupStage(100);
				}
				return;
			}
			mIsFirstOpenStage = false;
			mCurrentPage.OpenNewStageUntilNextChapter(100, a_nextChapter);
			if (mGame.mPlayer.StageOpenList.Count <= 0)
			{
				return;
			}
			foreach (int stageOpen2 in mGame.mPlayer.StageOpenList)
			{
				if (mGame.mPlayer.GetStageStatus(mGame.mPlayer.Data.CurrentSeries, stageOpen2) == Player.STAGE_STATUS.NOOPEN)
				{
					mGame.mPlayer.SetStageStatus(mGame.mPlayer.Data.CurrentSeries, stageOpen2, Player.STAGE_STATUS.LOCK);
					mCurrentPage.ChangeStageButton(stageOpen2, Player.STAGE_STATUS.LOCK);
				}
			}
			mGame.mPlayer.StageOpenList.Clear();
		}
		else
		{
			mCurrentPage.OpenNewStageUntilNextChapter(100, a_nextChapter);
			if (mGame.mPlayer.StageOpenList.Count > 0)
			{
				mIsOpenNewChapter = true;
			}
		}
	}

	public float CheckUnlockNewStage()
	{
		float result = 0f;
		int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
		int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
		int lastClearedLevel = mGame.mPlayer.LastClearedLevel;
		if (playableMaxLevel != 100)
		{
			result = mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, lastClearedLevel);
		}
		return result;
	}

	public override float ClearedStagePosition(bool a_newcheck = true)
	{
		float num = 0f;
		int lastClearedLevel = mGame.mPlayer.LastClearedLevel;
		int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
		if (lastClearedLevel > 0)
		{
			num = mCurrentPage.GetStageWorldPos(lastClearedLevel);
			mCurrentPage.SetAvatarPositionOnStage(lastClearedLevel);
			CheckUnlockNewStage();
		}
		else
		{
			CheckUnlockNewStage();
			float mValue = mCurrentPage.mAvater.GetCurrentNode().mValue;
			lastClearedLevel = Def.GetStageNoByRouteOrder(mValue);
			num = mCurrentPage.GetStageWorldPos(lastClearedLevel);
		}
		return num;
	}

	protected void CheckAccessoryDisplay()
	{
		mCurrentPage.DisplayAllAccessory();
		mCurrentPage.DisplaySilhouette();
	}

	protected bool CheaterStartCheck()
	{
		if (!mGame.CheckTimeCheater())
		{
			return false;
		}
		mState.Change(STATE.WAIT);
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("Cheater_TimeCheat_Desc"));
		mConfirmDialog.Init(Localization.Get("Cheater_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnTimeCheatErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(70);
		return true;
	}

	protected void OnTimeCheatErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		OnExitToTitle(true);
	}

	protected bool RatingStartCheck()
	{
		bool flag = false;
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.RATING_F_MARS) && mGame.mPlayer.IsCompanionUnlock(2))
		{
			flag = true;
		}
		if (!flag)
		{
			return false;
		}
		mGame.SetRatingTutorial();
		if (NativeReviewManager.EnableReview())
		{
			NativeReviewManager.RequestReview();
			return false;
		}
		mState.Reset(STATE.WAIT, true);
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("Rating_Title"), Localization.Get("Rating_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
		mConfirmDialog.SetClosedCallback(OnRatingDialogClosed);
		mConfirmDialog.SetBaseDepth(70);
		StartCoroutine(GrayOut());
		return true;
	}

	protected void OnRatingDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			string url = string.Format(SingletonMonoBehaviour<Network>.Instance.Store_URL, BIJUnity.getCountryCode());
			Application.OpenURL(url);
		}
		mConfirmDialog = null;
		mGame.mPlayer.Data.LastRatingDisplayTime = DateTime.Now;
		mGame.mPlayer.Data.LastRatingDisplayLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		mGame.mPlayer.Data.RatingDisplayCount++;
		mGame.Save();
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected bool RankingStartCheck()
	{
		return false;
	}

	protected bool RankingRankUpStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsStage(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.STAGE);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected bool RankingRankUpStarStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsStar(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.STAR);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected bool RankingRankUpTrophyStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsTrophy(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.TROPHY);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected void OnRankUpDialogClosed()
	{
		StartCoroutine(GrayIn());
		mRankUpDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected bool CollectionUpStartCheck()
	{
		if (mCockpit != null)
		{
			bool flag = false;
			for (int i = 0; i < mGame.mVisualCollectionData.Count; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					if (mGame.mPlayer.GetCollectionNotifyStatus(mGame.mVisualCollectionData[i].piceIds[j]) == Player.NOTIFICATION_STATUS.NOTIFY)
					{
						mCockpit.SetEnableMenuButtonAttention(true);
						flag = true;
						break;
					}
				}
				if (flag)
				{
					break;
				}
			}
			if (!flag)
			{
				List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
				for (int k = 0; k < accessoryByType.Count; k++)
				{
					if (mGame.mPlayer.GetCollectionNotifyStatus(accessoryByType[k].Index) == Player.NOTIFICATION_STATUS.NOTIFY)
					{
						mCockpit.SetEnableMenuButtonAttention(true);
						break;
					}
				}
			}
		}
		return false;
	}

	protected bool OldEventStartCheck()
	{
		DateTime dateTime = DateTimeUtil.Now();
		foreach (short key in mGame.mPlayer.Data.OldEventDataDict.Keys)
		{
			PlayerOldEventData data;
			mGame.mPlayer.Data.GetOldEventData(key, out data);
			if (data == null || !data.HasOpened || data.ExtendedOfferDone || (data.CloseTime - dateTime).Ticks > 0)
			{
				continue;
			}
			StartCoroutine(GrayOut());
			OldEventContinueDialog dialog = Util.CreateGameObject("OldEventContinueDialog", mAnchorCenter.gameObject).AddComponent<OldEventContinueDialog>();
			dialog.Init(data.EventID, OldEventContinueDialog.PLACE.MAIN_MAP);
			dialog.SetClosedCallback(delegate(OldEventContinueDialog.SELECT_ITEM i)
			{
				StartCoroutine(GrayIn());
				if (i == OldEventContinueDialog.SELECT_ITEM.POSITIVE)
				{
					mGame.mOldEventMode = true;
					mGame.mOldEventExtend = true;
					OnCurrentEventPushed(data.EventID);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
				UnityEngine.Object.Destroy(dialog.gameObject);
				dialog = null;
			});
			mState.Reset(STATE.WAIT, true);
			return true;
		}
		return false;
	}

	protected void OnMaxLevelOpenDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override bool OnDrawCockpit()
	{
		return true;
	}

	public override bool IsEnableButton()
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return false;
		}
		return true;
	}

	protected void OnCockpitOpend(bool opening)
	{
		if (opening)
		{
			mCockpitOpen = false;
		}
	}

	protected void OnCockpitClosed(bool closing)
	{
		if (closing)
		{
			mCockpitOpen = true;
		}
	}

	protected bool StoryDemoStartCheck()
	{
		if (mShowStoryDemoList.Count > 0)
		{
			return true;
		}
		return false;
	}

	protected void StartStoryDemo(int demoName)
	{
		mCurrentDemoIndex = demoName;
		if (!mIsFirstOpenStage)
		{
			mGameState.FadeOutMaskForStory();
		}
		if (demoName == 0 && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9020);
			mGame.Save();
		}
		mGame.StopMusic(0, 0.5f);
		mIsPlayingBGM = false;
		mState.Reset(STATE.STORY_DEMO_FADE, true);
	}

	public void OnStoryDemoFinished(int storyIndex)
	{
		if (mAutoUnlockPartner == null && mGame.GetPresentBoxNum() == 0)
		{
			StartCoroutine(GrayIn(1f));
		}
		mGame.mPlayer.StoryComplete(storyIndex);
		if (mCurrentStoryDemo != null)
		{
			if (mCurrentStoryDemo.ClearStage == -1 && mCurrentStoryDemo.SelectStage != -1)
			{
				int selectStage = mCurrentStoryDemo.SelectStage;
				if (mCurrentPage.Series == Def.SERIES.SM_FIRST && selectStage == 100)
				{
					mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9021);
					StartCoroutine("GrayOut");
					TutorialSkipConfirmDialog tutorialSkipConfirmDialog = Util.CreateGameObject("TutorialSkipDialog", mRoot).AddComponent<TutorialSkipConfirmDialog>();
					tutorialSkipConfirmDialog.SetClosedCallback(OnTutorialSkipConfirmDialogClosed);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					StartCoroutine(GrayOut());
					StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, selectStage, string.Empty, 0));
				}
			}
			else
			{
				int a_main;
				int a_sub;
				Def.SplitStageNo(mCurrentStoryDemo.ClearStage, out a_main, out a_sub);
				SMChapterSetting chapterSearch = mMapPageData.GetChapterSearch(a_main + 1);
				if (chapterSearch != null)
				{
					int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
					int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
					int stageNo = Def.GetStageNo(chapterSearch.mSubNo, 0);
					if (playableMaxLevel < nextRoadBlockLevel && playableMaxLevel < stageNo)
					{
						mCurrentPage.OpenNewStageUntilNextChapter(playableMaxLevel, stageNo);
						if (mGame.mPlayer.StageOpenList.Count > 0)
						{
							mIsOpenNewChapter = true;
						}
					}
				}
				SMMapSetting map = mMapPageData.GetMap();
				if (map != null && map.SeasonLastDemoID != -1 && storyIndex == map.SeasonLastDemoID)
				{
					ShowNextSeasonNotice();
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
					mCockpit.SetEnableButtonAction(true);
					mCockpitOperation = true;
				}
				mGame.Save();
			}
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
			mCockpit.SetEnableButtonAction(true);
			mCockpitOperation = true;
		}
		if (!mBGMLoadError)
		{
			mGame.PlayMusic(DEFAULT_BGM, 0, true, false, true);
		}
	}

	public void OnSDPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.MAP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(mRoot);
			mState.Reset(STATE.WAIT, true);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected new void OnPurchaseDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnHeartPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_MAP;
			BuyHeartDialog buyHeartDialog = Util.CreateGameObject("HeartDialog", mRoot).AddComponent<BuyHeartDialog>();
			mState.Reset(STATE.WAIT, true);
			buyHeartDialog.SetClosedCallback(OnHeartDialogDialogClosed);
			buyHeartDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected void OnHeartDialogDialogClosed(BuyHeartDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnStorePushed(GameObject go)
	{
		if ((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || mDirectStoreTransition)
		{
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL8_3))
			{
				StartCoroutine("GrayOut");
				mConfirmDialog = Util.CreateGameObject("AttentionDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("SeasonStore_Error01_Title"), Localization.Get("SeasonStore_Error01_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
				mState.Reset(STATE.WAIT);
				mDirectStoreTransition = false;
			}
			else
			{
				StartCoroutine("GrayOut");
				mState.Reset(STATE.WAIT, true);
				mShopFullDialog = Util.CreateGameObject("ShopFullDialog", mRoot).AddComponent<ShopFullDialog>();
				mShopFullDialog.Init(mDirectStoreTransition ? ShopFullDialog.LIST_TYPE.SALE : ShopFullDialog.LIST_TYPE.BOOSTER);
				mShopFullDialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, OnStoreDialogClosed);
				mShopFullDialog.SetCallback(ShopFullDialog.EVENT_STATE.AUTH_ERROR, OnDialogAuthErrorClosed);
				mDirectStoreTransition = false;
			}
		}
	}

	public void OnStoreDialogClosed()
	{
		mCockpit.UpdateSaleFlg();
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnMugenHeartButtonPushed(GameObject go)
	{
		if (((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || mState.GetStatus() == STATE.DEEPLINK_PUSHED) && mCockpit.mMugenflg)
		{
			mState.Reset(STATE.WAIT, true);
			base.OnMugenHeartButtonPushed(go);
		}
	}

	public override void OnMugenHeartDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnMugenHeartDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void CreateMugenHeartDialog(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START_WAIT;
		}
		else
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.END_WAIT;
		}
		mState.Reset(STATE.WAIT, true);
		base.CreateMugenHeartDialog(i);
	}

	public override void OnMugenDialogDialogClosed(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			base.OnMugenDialogDialogClosed(i);
			return;
		}
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.NONE;
		mGame.mPlayer.Data.mUseMugenHeartSetTime = 0;
		mGame.Save();
		base.OnMugenDialogDialogClosed(i);
		if (mGame.IsDailyChallengePuzzle)
		{
			mState.Reset(STATE.WAIT);
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public override void StartMugenHeart()
	{
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START;
		mGame.mPlayer.Data.MugenHeartUseTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		Def.ITEMGET_TYPE a_type;
		mGame.mPlayer.SubBoosterNum(Def.MugenHeartTimeToKindData[mGame.mPlayer.Data.mUseMugenHeartSetTime], 1, out a_type);
		if (mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) != mGame.mPlayer.Data.MaxNormalLifeCount)
		{
			mGame.mPlayer.SetItemCount(Def.ITEM_CATEGORY.CONSUME, 1, mGame.mPlayer.Data.MaxNormalLifeCount);
		}
		mGame.Save();
		base.StartMugenHeart();
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnStoreBagPushed(GameObject go, bool linkBanner = false)
	{
		if (((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || mState.GetStatus() == STATE.DEEPLINK_PUSHED) && mCockpit.mShopflg)
		{
			mState.Reset(STATE.WAIT, true);
			mDirectStoreTransition = true;
			base.OnStoreBagPushed(go, linkBanner);
		}
	}

	public override void OnStoreBagDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnStoreBagDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnGameCenterPushed(GameObject go)
	{
		JellyImageButton component = go.GetComponent<JellyImageButton>();
		if (!(component == null) && mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			if (!component.IsEnable() || !mGame.EnableAdvEnter())
			{
				OnGameCenterNoEnterPushed(go);
				return;
			}
			mGame.PlaySe("SE_POSITIVE", -1);
			mGame.EnterADV();
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnGameCenterNoEnterPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mAdvNotEnterDialog = Util.CreateGameObject("mAdvNotEnterDialog", mRoot).AddComponent<AdvNotEnterDialog>();
			if (!mGame.EnableAdvSession())
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.MAINTENANCE_ADV);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
			else
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.STAGE_NO_CLEAR);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
		}
	}

	public void AdvNotEnterDialogClosed()
	{
		if (mAdvNotEnterDialog != null)
		{
			UnityEngine.Object.Destroy(mAdvNotEnterDialog.gameObject);
			mAdvNotEnterDialog = null;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnEventPushed(GameObject go)
	{
		JellyImageButton component = go.GetComponent<JellyImageButton>();
		if (component == null || mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		if (!component.IsEnable())
		{
			if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				mConfirmDialog = Util.CreateGameObject("AttentionDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("SeasonEvent_Error01_Title"), Localization.Get("SeasonEvent_Error01_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnEventExpiredClosed);
				StartCoroutine(GrayOut());
			}
			else
			{
				mConfirmDialog = Util.CreateGameObject("ErrorDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("EventExpired_Title03"), Localization.Get("EventExpired_Desc03"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnEventExpiredClosed);
				StartCoroutine(GrayOut());
			}
		}
		else
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mEventEntranceDialog = Util.CreateGameObject("EventPageDialog", mRoot).AddComponent<EventPageDialog>();
			mEventEntranceDialog.SetClosedCallback(OnEventEntranceClosed);
			mEventEntranceDialog.SetEventCallback(OnCurrentEventPushed);
		}
	}

	public void OnEventExpiredClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnEventEntranceClosed()
	{
		mEventEntranceDialog = null;
		mState.Reset(STATE.UNLOCK_ACTING, true);
	}

	public void OnEventEntranceOpened()
	{
		if (mEventAppeal != null)
		{
			StartCoroutine(EventAppealFadeIn());
		}
	}

	public IEnumerator EventAppealFadeIn()
	{
		if (!(mEventAppeal == null))
		{
			mEventAppeal.DoSwitchFadeIn();
			yield return null;
			while (!(mEventAppeal == null) && !mEventAppeal.IsAppealEnd())
			{
				yield return null;
			}
			if (mEventAppeal != null)
			{
				UnityEngine.Object.Destroy(mEventAppeal.gameObject);
				mEventAppeal = null;
			}
		}
	}

	public void OnCurrentEventPushed(int a_eventID)
	{
		if (!mGame.IsSeasonEventExpired(a_eventID))
		{
			if (a_eventID == 1000)
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.OLD_EVENT_GATE);
				return;
			}
			mGame.mEventMode = true;
			SMEventPageData eventPageData = GameMain.GetEventPageData(a_eventID);
			mGame.mPlayer.SetEvent(a_eventID, (short)eventPageData.EventCourseList.Count, eventPageData.EventSetting.EventType);
			mGame.ClearWonHistory();
			if (GameMain.USE_DEBUG_DIALOG && GlobalVariables.Instance.IsSelectedEventTimeOut)
			{
				for (int i = 0; i < mGame.EventProfile.SeasonEventList.Count; i++)
				{
					mGame.EventProfile.SeasonEventList[i].EndTime = 0L;
				}
			}
			switch (eventPageData.EventSetting.EventType)
			{
			case Def.EVENT_TYPE.SM_RALLY:
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.EVENT_RALLY);
				break;
			case Def.EVENT_TYPE.SM_STAR:
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.EVENT_STAR);
				break;
			case Def.EVENT_TYPE.SM_RALLY2:
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.EVENT_RALLY2);
				break;
			case Def.EVENT_TYPE.SM_BINGO:
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.EVENT_BINGO);
				break;
			case Def.EVENT_TYPE.SM_LABYRINTH:
			{
				GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(a_eventID);
				if (labyrinthData != null)
				{
					GlobalVariables.Instance.SetLabyrinthData(a_eventID, new GlobalLabyrinthEventData());
				}
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.EVENT_LABYRINTH);
				break;
			}
			case Def.EVENT_TYPE.SM_ADV:
				break;
			}
		}
		else
		{
			mConfirmDialog = Util.CreateGameObject("ErrorDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerExpired_Event_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnEventExpiredDialogClosed);
			mState.Change(STATE.WAIT);
		}
	}

	public void OnEventExpiredDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		if (mEventEntranceDialog != null)
		{
			mEventEntranceDialog.ResetState();
		}
	}

	public void OnDailyPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			mGetSPStampDialog = Util.CreateGameObject("GetSPStampDialog", mRoot).AddComponent<GetSPStampDialog>();
			mGetSPStampDialog.SetClosedCallback(OnDailyEventClosed);
			StartCoroutine(GrayOut());
		}
	}

	private void OnDailyChallenge_Clicked(GameObject go)
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen || GlobalVariables.Instance.SelectedDailyChallengeSetting == null)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		int num = GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID;
		if (mGame.mDebugDailyChallengeID > 0)
		{
			num = mGame.mDebugDailyChallengeID;
		}
		int dailyChallengeOpenSheetNum = mGame.mPlayer.Data.DailyChallengeOpenSheetNum;
		if (mGame.mDailyChallengeData != null)
		{
			string text = GlobalVariables.Instance.SelectedDailyChallengeSetting.SheetName;
			if (mGame.mDebugDailyChallengeSheetSetName != string.Empty)
			{
				text = mGame.mDebugDailyChallengeSheetSetName;
			}
			foreach (DailyChallengeSheet mDailyChallengeDatum in mGame.mDailyChallengeData)
			{
				if (mDailyChallengeDatum == null || mDailyChallengeDatum.SheetName != text)
				{
					continue;
				}
				long num2 = GlobalVariables.Instance.SelectedDailyChallengeSetting.StartTime;
				if (mGame.mDebugDailyChallengeStartTime != -1)
				{
					num2 = mGame.mDebugDailyChallengeStartTime;
				}
				TimeSpan timeSpan = DateTimeUtil.Now() - DateTimeUtil.UnixTimeStampToDateTime(num2, true);
				mGame.mPlayer.Data.DailyChallengeOpenSheetNum = ((timeSpan.Days >= 0) ? timeSpan.Days : 0) + 1;
				if (mGame.mPlayer.Data.DailyChallengeOpenSheetNum > mDailyChallengeDatum.StageCount)
				{
					mGame.mPlayer.Data.DailyChallengeOpenSheetNum = mDailyChallengeDatum.StageCount;
				}
				if (mGame.mPlayer.Data.DailyChallengeStayStage == null || mGame.mPlayer.Data.DailyChallengeID != num)
				{
					mGame.mPlayer.Data.DailyChallengeStayStage = new Dictionary<int, int>();
				}
				if (mGame.mPlayer.Data.DailyChallengeID != num)
				{
					mGame.mPlayer.Data.DailyChallengeStayStage.Clear();
				}
				while (mGame.mPlayer.Data.DailyChallengeStayStage.Count < mGame.mPlayer.Data.DailyChallengeOpenSheetNum)
				{
					mGame.mPlayer.Data.DailyChallengeStayStage.Add(mGame.mPlayer.Data.DailyChallengeStayStage.Count + 1, 1);
				}
				if (mGame.mPlayer.Data.DailyChallengeStayStage.Count <= mGame.mPlayer.Data.DailyChallengeOpenSheetNum)
				{
					break;
				}
				int[] array = new int[mGame.mPlayer.Data.DailyChallengeStayStage.Keys.Count];
				mGame.mPlayer.Data.DailyChallengeStayStage.Keys.CopyTo(array, 0);
				int[] array2 = array;
				foreach (int num3 in array2)
				{
					if (num3 > mGame.mPlayer.Data.DailyChallengeOpenSheetNum)
					{
						mGame.mPlayer.Data.DailyChallengeStayStage.Remove(num3);
					}
				}
				break;
			}
		}
		bool isOpenNewSheet = mGame.mPlayer.Data.DailyChallengeOpenSheetNum > dailyChallengeOpenSheetNum;
		if (mGame.mPlayer.Data.DailyChallengeID != num)
		{
			isOpenNewSheet = false;
			mGame.mPlayer.Data.DailyChallengeLastSelectSheetNo = 1;
		}
		mGame.mPlayer.Data.DailyChallengeID = num;
		mGame.Save();
		mGame.PlaySe("SE_POSITIVE", -1);
		TryOpenDailyChallengeDialog(() => mDailyChallengeDialog.Init(mRoot, this, isOpenNewSheet));
		if (mCockpit != null && mCockpit.DailyChallengeAttention != null)
		{
			mCockpit.DailyChallengeAttention.SetAttentionEnable(false);
		}
		StartCoroutine(GrayOut());
	}

	private bool MightOpenDailyChallenge()
	{
		if (GlobalVariables.Instance.SelectedDailyChallengeSheetNo != -1 && GlobalVariables.Instance.SelectedDailyChallengeStageNo != -1)
		{
			if (!TryOpenDailyChallengeDialog(() => mDailyChallengeDialog.InitComeBackFromPuzzle(mRoot, this)))
			{
				return false;
			}
			if (mCockpit != null && mCockpit.DailyChallengeAttention != null)
			{
				mCockpit.DailyChallengeAttention.SetAttentionEnable(false);
			}
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			return true;
		}
		return false;
	}

	private bool TryOpenDailyChallengeDialog(Func<bool> on_init)
	{
		if (mDailyChallengeDialog != null)
		{
			UnityEngine.Object.Destroy(mDailyChallengeDialog.gameObject);
			mDailyChallengeDialog = null;
		}
		mDailyChallengeDialog = Util.CreateGameObject("DailyChallengeDialog", mRoot).AddComponent<DailyChallengeDialog>();
		if (on_init != null && !on_init())
		{
			UnityEngine.Object.Destroy(mDailyChallengeDialog.gameObject);
			mDailyChallengeDialog = null;
			return false;
		}
		mDailyChallengeDialog.SetCallback(DailyChallengeDialog.EVENT_TYPE.PLAY, delegate
		{
			mState.Change(STATE.PLAY);
		});
		mDailyChallengeDialog.SetCallback(DailyChallengeDialog.EVENT_TYPE.MIGHT_MAINTENANCE, delegate
		{
			if (GameMain.MaintenanceMode)
			{
				mGame.IsTitleReturnMaintenance = true;
				OnExitToTitle(true);
			}
		});
		mDailyChallengeDialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, delegate
		{
			StartCoroutine(GrayIn());
			if (!mDailyChallengeDialog.IsPlayStage)
			{
				mState.Change(STATE.MAIN);
			}
			mDailyChallengeDialog = null;
		});
		return true;
	}

	private void OnDailyEventClosed(int i, AccessoryData accessory)
	{
		if (mGetSPStampDialog != null)
		{
			UnityEngine.Object.Destroy(mGetSPStampDialog.gameObject);
			mGetSPStampDialog = null;
		}
		StartCoroutine(GrayIn());
		mState.Reset(STATE.UNLOCK_ACTING, true);
	}

	private void OnAccessoryGetDailyClosed(int i, AccessoryData accessory)
	{
		if (mGetSPStampDialog != null)
		{
			if (mGetSPStampDialog.IsLastSheet && mGetSPStampDialog.IsAfterComplete)
			{
				mIsLastSPDailyAccessory = true;
			}
			UnityEngine.Object.Destroy(mGetSPStampDialog.gameObject);
			mGetSPStampDialog = null;
		}
		if (accessory != null)
		{
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(accessory);
			mGetAccessoryDialog.SetClosedCallback(OnGetDailyAccessoryDialogClosed);
			mStampDailyItemCount = i;
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Reset(STATE.UNLOCK_ACTING, true);
		}
	}

	private void OnGetDailyAccessoryDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetAccessoryDialog != null)
		{
			UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
			mGetAccessoryDialog = null;
		}
		if (mStampDailyItemCount == 0)
		{
			if (mIsLastSPDailyAccessory)
			{
				mIsLastSPDailyAccessory = false;
				mState.Reset(STATE.WAIT, true);
				ConfirmDialog confirmDialog = Util.CreateGameObject("CompDialog", mRoot).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("Chocolate_Clear_Title"), Localization.Get("Chocolate_Clear_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				confirmDialog.SetClosedCallback(delegate
				{
					StartCoroutine(GrayIn());
					mState.Reset(STATE.UNLOCK_ACTING, true);
				});
			}
			else
			{
				StartCoroutine(GrayIn());
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
		}
		else
		{
			mGetSPStampDialog = Util.CreateGameObject("GetSPStampDialog", mRoot).AddComponent<GetSPStampDialog>();
			mGetSPStampDialog.Init(mStampDailyItemCount);
			mGetSPStampDialog.SetClosedCallback(OnAccessoryGetDailyClosed);
			mStampDailyItemCount = 0;
		}
	}

	public void OnRemainTimePushed(GameObject go)
	{
	}

	public void OnRemainTimeDialogClosed()
	{
		mRemainTimeDialog = null;
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void UpdateMapDailyEvents()
	{
		mDailyEventsData.Clear();
		int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
		int a_sub;
		int a_main;
		Def.SplitStageNo(playableMaxLevel, out a_main, out a_sub);
		a_main = Math.Max(0, a_main - 1);
		int stageNo = Def.GetStageNo(a_main, 0);
		List<DailyEventSettings> dailyEvents = mGame.GetDailyEvents();
		for (int i = 0; i < dailyEvents.Count; i++)
		{
			DailyEventSettings dailyEventSettings = dailyEvents[i];
			if (dailyEventSettings.EventIDList == null || dailyEventSettings.EventIDList.Count < 1)
			{
				continue;
			}
			for (int j = 0; j < dailyEventSettings.EventIDList.Count; j++)
			{
				SMDailyEventSetting dailyEvent = mMapPageData.GetDailyEvent(dailyEventSettings.EventIDList[j]);
				if (dailyEvent != null)
				{
					int stageNo2 = dailyEvent.StageNo;
					if (stageNo2 <= stageNo)
					{
						mDailyEventsData[stageNo2] = dailyEvent;
					}
				}
			}
		}
	}

	private void DisplayMapDailyEvents()
	{
		foreach (KeyValuePair<int, SMDailyEventSetting> mDailyEventsDatum in mDailyEventsData)
		{
			int key = mDailyEventsDatum.Key;
			string AnimKey = null;
			SMDailyEventSetting mapDailyEvent = GetMapDailyEvent(key, out AnimKey);
			if (mapDailyEvent != null && !string.IsNullOrEmpty(AnimKey))
			{
				mCurrentPage.CreateDailyEvent(key, AnimKey, false);
			}
		}
	}

	private void UndisplayMapDailyEventsAll()
	{
		foreach (KeyValuePair<int, SMDailyEventSetting> mDailyEventsDatum in mDailyEventsData)
		{
			int key = mDailyEventsDatum.Key;
			mCurrentPage.RemoveDailyEvent(key);
		}
	}

	private SMDailyEventSetting GetMapDailyEvent(int StageNo)
	{
		string AnimKey = null;
		return GetMapDailyEvent(StageNo, out AnimKey);
	}

	private SMDailyEventSetting GetMapDailyEvent(int StageNo, out string AnimKey)
	{
		AnimKey = null;
		if (mGame.GetRemainDailyEventTime() <= 0)
		{
			return null;
		}
		SMDailyEventSetting value = null;
		if (mDailyEventsData.TryGetValue(StageNo, out value))
		{
			if (value.BoxID == null || value.BoxID.Count < 1)
			{
				return null;
			}
			AccessoryData.ACCESSORY_TYPE aCCESSORY_TYPE = mGame.GuessDailyEventPresentBoxType(value.BoxID);
			if (aCCESSORY_TYPE == AccessoryData.ACCESSORY_TYPE.NONE)
			{
				return null;
			}
			AnimKey = mGame.GetDailyEventPresentBoxAnimeKey(aCCESSORY_TYPE);
			return value;
		}
		return null;
	}

	public void OnGiftPushed(GameObject go)
	{
		if ((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || mState.GetStatus() == STATE.DEEPLINK_PUSHED)
		{
			JellyImageButton component = go.GetComponent<JellyImageButton>();
			if (component.IsEnable())
			{
				StartCoroutine("GrayOut");
				mState.Reset(STATE.WAIT, true);
				mGiftFullDialog = Util.CreateGameObject("GiftDialog", mRoot).AddComponent<GiftFullDialog>();
				mGiftFullDialog.SetClosedCallback(OnGiftDialogClosed);
			}
		}
	}

	public void OnGiftDialogClosed(bool stageOpen, bool friendHelp, bool friendhelpSuccess, bool advChara, bool advTicket)
	{
		if (stageOpen)
		{
			mState.Reset(STATE.WAIT);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Gift_AferReturnTitle"), Localization.Get("Gift_AferReturnDesc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
		}
		else if (friendHelp && mGame.PlayingFriendHelp != null)
		{
			StartCoroutine(GrayOut());
			StartCoroutine(StartStage((Def.SERIES)mGame.PlayingFriendHelp.Gift.Data.ItemKind, mGame.PlayingFriendHelp.Gift.Data.ItemID, string.Empty, 0));
		}
		else
		{
			if (mCockpit != null)
			{
				mCockpit.SetEnableMailAttention(mGame.mPlayer.HasModifiedGifts());
			}
			StartCoroutine("GrayIn");
			if (friendhelpSuccess)
			{
				int playableMaxLevel = mGame.mPlayer.PlayableMaxLevel;
				int openNoticeLevel = mGame.mPlayer.OpenNoticeLevel;
				mCurrentPage.UnlockNewStage(playableMaxLevel, openNoticeLevel, -1);
				mState.Change(STATE.UNLOCK_ACTING);
			}
			else if (mGame.AddedNewFriend)
			{
				mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
				mState.Change(STATE.NETWORK_PROLOGUE);
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
		}
		mGiftFullDialog = null;
	}

	public void OnTitleReturnMessageClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGame.StopMusic(0, 0.5f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override bool GiftStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return false;
		}
		return base.GiftStartCheck();
	}

	public void OnGrowPushed(GameObject go)
	{
		int num = 0;
		List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
		for (int i = 0; i < playStage.Count; i++)
		{
			if (playStage[i].Series == 0)
			{
				num = playStage[i].Level;
				break;
			}
		}
		if (num > 800)
		{
			StartCoroutine(GrayOut());
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(null, false, AccessoryData.ACCESSORY_TYPE.GROWUP);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mGrowupDialog.CanRepeat = true;
			mGrowupFromMenu = true;
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			StartCoroutine(GrayOut());
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Growup_Error01_Title"), Localization.Get("Growup_Error01_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Reset(STATE.WAIT);
		}
	}

	public void OnTitlePushed(GameObject go)
	{
		StartCoroutine(GrayOut());
		ConfirmDialog confirmDialog = Util.CreateGameObject("QuitDialog", mRoot).AddComponent<ConfirmDialog>();
		confirmDialog.SetClosedCallback(OnQuitDialogClosed);
		confirmDialog.Init(Localization.Get("Exit_Title"), Localization.Get("Exit_Desk"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
		mState.Change(STATE.WAIT);
	}

	protected void OnQuitDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mState.Change(STATE.UNLOAD_WAIT);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public virtual void OnBackKeyPushed(bool ignore = false)
	{
		if (ignore || (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen))
		{
			OnTitlePushed(null);
		}
	}

	public void OnStageButtonPushed(int stage)
	{
		bool flag = false;
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL2_2 && stage == 200)
		{
			mGame.mTutorialManager.DeleteTutorialArrow();
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL2_2);
			flag = true;
		}
		if (!flag && !mFirstTutorialFlg && (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen))
		{
			return;
		}
		mFirstTutorialFlg = false;
		int a_main;
		int a_sub;
		Def.SplitStageNo(stage, out a_main, out a_sub);
		SMMapStageSetting mapStage = mMapPageData.GetMapStage(a_main, a_sub);
		int num = -1;
		if (mapStage != null)
		{
			int storyDemoAtStageSelect = mapStage.GetStoryDemoAtStageSelect();
			if (storyDemoAtStageSelect != -1 && !mGame.mPlayer.IsStoryCompleted(storyDemoAtStageSelect))
			{
				num = storyDemoAtStageSelect;
			}
		}
		if (num == -1 || mGame.mPlayer.IsStoryCompleted(num))
		{
			StartCoroutine(GrayOut());
			StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, stage, string.Empty, 0));
			return;
		}
		StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
		storyDemoTemp.DemoIndex = num;
		storyDemoTemp.SelectStage = stage;
		storyDemoTemp.ClearStage = -1;
		mShowStoryDemoList.Add(storyDemoTemp);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override bool IsStageButtonSECall(int stage)
	{
		bool result = true;
		bool flag = false;
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.MAP_LEVEL2_2 && stage == 200)
		{
			flag = true;
		}
		if (!flag && !mFirstTutorialFlg && (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen))
		{
			result = false;
		}
		return result;
	}

	protected virtual IEnumerator StartStage(Def.SERIES a_series, int stage, string _DisplayStageNumber, int _LimitTimeOrMoves, int _NoticeStageNo = 0)
	{
		mState.Reset(STATE.NETWORK_STAGE, true);
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(a_series, out data);
		data.CurrentLevel = stage;
		mGame.mPlayer.Data.SetMapData(a_series, data);
		bool _stage_load_finished = false;
		bool _stage_load_success = false;
		float _start_time = Time.realtimeSinceStartup;
		GameStatePuzzle.STAGE_DISPLAY_NUMBER = _DisplayStageNumber;
		GameStatePuzzle.STAGE_LIMIT_TIMES_OR_MOVES = _LimitTimeOrMoves;
		string _user_segment = mGame.GetUserSegment();
		string _DisplayStageNumber2 = default(string);
		int _LimitTimeOrMoves2 = default(int);
		int _NoticeStageNo2 = default(int);
		Def.SERIES a_series2 = default(Def.SERIES);
		StartCoroutine(SMStageData.CreateAsync(a_series, stage, _user_segment, delegate(int _stage, SMStageData.EventArgs _e)
		{
			if (_e != null)
			{
				_stage_load_success = true;
				GameStatePuzzle.STAGE_DATA_REQUIRE = false;
				mGame.mCurrentStage = _e.StageData;
				if (!string.IsNullOrEmpty(_DisplayStageNumber2))
				{
					mGame.mCurrentStage.DisplayStageNumber = _DisplayStageNumber2;
				}
				if (_LimitTimeOrMoves2 > 0)
				{
					Def.STAGE_LOSE_CONDITION loseType = mGame.mCurrentStage.LoseType;
					if (loseType != 0 && loseType == Def.STAGE_LOSE_CONDITION.TIME)
					{
						mGame.mCurrentStage.OverwriteLimitTimeSec = _LimitTimeOrMoves2;
					}
					else
					{
						mGame.mCurrentStage.OverwriteLimitMoves = _LimitTimeOrMoves2;
					}
				}
				if (_NoticeStageNo2 > 0)
				{
					mGame.mCurrentStage.StageNumber = _NoticeStageNo2;
					mGame.mPlayer.Data.GetMapData(a_series2, out data);
					data.CurrentLevel = _NoticeStageNo2;
					mGame.mPlayer.Data.SetMapData(a_series2, data);
				}
				int a_main;
				int a_sub;
				Def.SplitStageNo(_stage, out a_main, out a_sub);
				SMMapPageData seriesMapPageData = GameMain.GetSeriesMapPageData(mGame.mPlayer.Data.CurrentSeries);
				mGame.mCurrentMapPageData = seriesMapPageData;
				SMMapStageSetting mapStage = seriesMapPageData.GetMapStage(a_main, a_sub);
				if (mapStage == null)
				{
					mGame.mCurrentStageInfo = new SMMapStageSetting();
				}
				else
				{
					mGame.mCurrentStageInfo = mapStage.Clone();
				}
			}
			_stage_load_finished = true;
		}));
		while (!_stage_load_finished)
		{
			yield return 0;
		}
		float _end_span = Time.realtimeSinceStartup - _start_time;
		if (!_stage_load_success || mGame.mCurrentStage == null)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string message = string.Format(Localization.Get("StageData_Error_Desc_02"));
			mConfirmDialog.Init(Localization.Get("StageData_Error_Title_02"), message, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnStageDataErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
			yield break;
		}
		SMDailyEventSetting _dailyEvent = GetMapDailyEvent(stage);
		mGame.mCurrentStageInfo.DailyEventBoxID.Clear();
		mGame.mCurrentStageInfo.IsDailyEvent = false;
		if (_dailyEvent != null && _dailyEvent.BoxID != null && _dailyEvent.BoxID.Count > 0)
		{
			for (int i = 0; i < _dailyEvent.BoxID.Count; i++)
			{
				int _id = _dailyEvent.BoxID[i];
				if (!mGame.mPlayer.IsAccessoryUnlock(_id))
				{
					int ret = _dailyEvent.CopyToBoxInfoAt(i, ref mGame.mCurrentStageInfo);
					if (ret >= 0)
					{
						mGame.mCurrentStageInfo.DailyEventBoxID.Add(ret);
						mGame.mCurrentStageInfo.IsDailyEvent = true;
					}
				}
			}
		}
		if (mStageInfo == null)
		{
			mStageInfo = Util.CreateGameObject("StageInfoDialog", mRoot).AddComponent<StageInfoDialog>();
			mStageInfo.Init();
			if (!mGame.mEventMode)
			{
				mStageInfo.SetClosedCallback(OnStageInfoDialogClosed);
				mStageInfo.SetPartnerSelectCallback(OnStageCharaDialogOpen);
				mStageInfo.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mStageInfo.SetDailyEvent(_dailyEvent, mGame.mCurrentStageInfo.DailyEventBoxID);
			}
			mStageInfo.SetBaseDepth(65);
		}
		SetScrollPower(0f);
	}

	private void OnStageDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		mConfirmDialog = null;
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnStageCharaDialogOpen()
	{
		mStageChara = Util.CreateGameObject("StageCharaDialog", mRoot).AddComponent<StageCharaDialog>();
		mStageChara.SetClosedCallback(OnStageCharaDialogClosed);
	}

	public void OnStageCharaDialogClosed(StageCharaDialog.SELECT_ITEM selectItem)
	{
		int selectedPartnerNo = mStageChara.SelectedPartnerNo;
		if (mStageInfo != null && selectedPartnerNo != -1)
		{
			mStageInfo.SetCompanion(selectedPartnerNo);
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
	}

	public void OnStageInfoDialogClosed(StageInfoDialog.SELECT_ITEM selectItem)
	{
		switch (selectItem)
		{
		case StageInfoDialog.SELECT_ITEM.PLAY:
		{
			List<StageRankingData> rankingData = null;
			if (base.RankingData != null)
			{
				rankingData = base.RankingData.results;
			}
			mGame.InitFriendsRanking(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, ref rankingData, mGame.mPlayer);
			mGame.mTrophyNumProduction = mGame.mPlayer.GetTrophy();
			mState.Change(STATE.PLAY);
			break;
		}
		case StageInfoDialog.SELECT_ITEM.CANCEL:
			if (!mGame.mEventMode)
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case StageInfoDialog.SELECT_ITEM.GOTOADV:
			if (mGame.EnableAdvEnter())
			{
				mGame.EnterADV();
				mState.Reset(STATE.WAIT, true);
			}
			else if (!mGame.mEventMode)
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		}
		if (mStageInfo != null)
		{
			UnityEngine.Object.Destroy(mStageInfo.gameObject);
			mStageInfo = null;
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
		if (GameMain.MaintenanceMode)
		{
			mGame.IsTitleReturnMaintenance = true;
			OnExitToTitle(true);
		}
	}

	public override void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void OnTrophyGetClosed(ConfirmImageDialog.SELECT_ITEM item)
	{
	}

	public void OnGetLinkBannerComplete(List<LinkBannerInfo> aBannerList)
	{
		mGame.linkbanner_InfoList = aBannerList;
	}

	public void OnMapAccessoryTapped(AccessoryData data, MapAccessory a_accessory)
	{
		if (data == null || (mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.TUTORIAL))
		{
			a_accessory.RequestOpenAnime();
		}
		else if (mState.GetStatus() == STATE.TUTORIAL && mGame.mTutorialManager.GetCurrentTutorial() != Def.TUTORIAL_INDEX.MAP_LEVEL9_0)
		{
			a_accessory.RequestOpenAnime();
		}
		else if (!string.IsNullOrEmpty(data.DemoName) && data.DemoName != "NONE")
		{
			if (mState.GetStatus() != STATE.MAIN)
			{
				a_accessory.RequestOpenAnime();
				return;
			}
			SetScrollPower(0f);
			UpdateMapPages();
			StartCoroutine(GrayOut());
		}
		else
		{
			GetAccessoryProcess(data);
			mPushedAccessory = a_accessory;
		}
	}

	public void GetAccessoryProcess(AccessoryData data)
	{
		switch (data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.PARTNER:
			StartCoroutine(GrayOut());
			StartCoroutine(GrayOut());
			mGetPartnerDialog = Util.CreateGameObject("GetPartnerDialog", mRoot).AddComponent<GetPartnerDialog>();
			mGetPartnerDialog.Init(data, GetPartnerDialog.PLACE.MAIN);
			mGetPartnerDialog.SetClosedCallback(OnGetPartnerDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
			if (mState.GetStatus() == STATE.TUTORIAL)
			{
				StartCoroutine(GrayOut());
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL9_0);
				mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL9_1, mRoot, delegate
				{
				}, delegate
				{
				}, true);
				mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
				mGrowupDialog.Init(data, false, AccessoryData.ACCESSORY_TYPE.GROWUP);
				mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			}
			else
			{
				StartCoroutine(GrayOut());
				mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
				mGrowupDialog.Init(data, false, AccessoryData.ACCESSORY_TYPE.GROWUP);
				mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
				mState.Reset(STATE.WAIT, true);
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART:
		case AccessoryData.ACCESSORY_TYPE.GEM:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		}
	}

	public void OnGetPartnerDialogClosed(GetPartnerDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGetPartnerDialog.Data;
		UnityEngine.Object.Destroy(mGetPartnerDialog.gameObject);
		mGetPartnerDialog = null;
		if (data == null || data.HasDemo)
		{
		}
		mPushedAccessory = null;
		mCurrentPage.DisplaySilhouette();
		CheckUnlockNewStage();
		DateTime dateTime = DateTimeUtil.Now();
		int num = 0;
		string s = dateTime.ToString("yyyyMMdd");
		try
		{
			num = int.Parse(s);
		}
		catch
		{
			num = 0;
		}
		if (DisplayCharacterIntroDialog(num))
		{
			mState.Reset(STATE.WAIT, true);
			return;
		}
		mState.Change(STATE.UNLOCK_ACTING);
		StartCoroutine(GrayIn());
	}

	public void OnGrowupSelectDialogClosed(GrowupPartnerSelectDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupDialog.SelectedPartnerNo;
		bool isCloseButton = mGrowupDialog.IsCloseButton;
		bool canRepeat = mGrowupDialog.CanRepeat;
		mGrowupDialog.Init(false);
		AccessoryData data = mGrowupDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupDialog.gameObject);
		mGrowupDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerSelectDialog.SELECT_ITEM.PLAY:
			mGrowupPartnerConfirmtDialog = Util.CreateGameObject("GrowupPartnerConfirmDialog", mRoot).AddComponent<GrowupPartnerConfirmDialog>();
			mGrowupPartnerConfirmtDialog.Init(data, selectedPartnerNo, isCloseButton, accessoryType);
			mGrowupPartnerConfirmtDialog.CanRepeat = canRepeat;
			mGrowupPartnerConfirmtDialog.SetClosedCallback(OnGrowupPartnerConfirmDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerSelectDialog.SELECT_ITEM.CANCEL:
			mGrowupFromMenu = false;
			StartCoroutine(GrayIn());
			CheckUnlockNewStage();
			if (mPushedAccessory != null)
			{
				mPushedAccessory.OpenAnime();
			}
			mState.Change(STATE.UNLOCK_ACTING);
			if (isCloseButton && mGame.mPlayer.GrowUpPiece > 0)
			{
				StartCoroutine(GrayOut());
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mGetStampDialog.CanRepeat = canRepeat;
				mState.Reset(STATE.WAIT, true);
			}
			break;
		}
	}

	public void OnGrowupPartnerConfirmDialogClosed(GrowupPartnerConfirmDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupPartnerConfirmtDialog.SelectedPartnerNo;
		bool isReplayStamp = mGrowupPartnerConfirmtDialog.IsReplayStamp;
		bool canRepeat = mGrowupPartnerConfirmtDialog.CanRepeat;
		mGrowupPartnerConfirmtDialog.Init(false);
		AccessoryData data = mGrowupPartnerConfirmtDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupPartnerConfirmtDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupPartnerConfirmtDialog.gameObject);
		mGrowupPartnerConfirmtDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerConfirmDialog.SELECT_ITEM.PLAY:
			mGrowupResultDialog = Util.CreateGameObject("GrowupResultDialog", mRoot).AddComponent<GrowupResultDialog>();
			if (mGrowupFromMenu)
			{
				mGrowupResultDialog.SetPlace(GrowupResultDialog.PLACE.MENU);
			}
			mGrowupResultDialog.Init(data, selectedPartnerNo, true, accessoryType, isReplayStamp);
			mGrowupResultDialog.MapUpdateInit(mCurrentPage.mAvater);
			mGrowupResultDialog.SetClosedCallback(OnGrowupResultDialogClosed);
			mGrowupResultDialog.CanRepeat = canRepeat;
			mGrowupFromMenu = false;
			mPushedAccessory = null;
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.CANCEL:
			mGrowupFromMenu = false;
			StartCoroutine(GrayIn());
			CheckUnlockNewStage();
			if (mPushedAccessory != null)
			{
				mPushedAccessory.OpenAnime();
			}
			if (isReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mGetStampDialog.CanRepeat = canRepeat;
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.BACK:
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(data, isReplayStamp, accessoryType);
			mGrowupDialog.CanRepeat = canRepeat;
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		}
	}

	public void OnGrowupResultDialogClosed(GrowupResultDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGrowupResultDialog.Data;
		bool canRepeat = mGrowupResultDialog.CanRepeat;
		StartCoroutine(GrayIn());
		if (data == null || data.HasDemo)
		{
		}
		if (canRepeat && mGame.mPlayer.GrowUp >= 1)
		{
			UnityEngine.Object.Destroy(mGrowupResultDialog.gameObject);
			mGrowupResultDialog = null;
			OnGrowPushed(null);
			return;
		}
		if (mGrowupResultDialog != null)
		{
			if (mGrowupResultDialog.IsReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				StartCoroutine(GrayOut());
				mGrowupResultDialog.Init(false);
				mGrowupResultDialog.MapUpdateInit(mCurrentPage.mAvater);
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mGetStampDialog.CanRepeat = canRepeat;
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
		UnityEngine.Object.Destroy(mGrowupResultDialog.gameObject);
		mGrowupResultDialog = null;
		CheckUnlockNewStage();
	}

	public void OnGetStampDialogClosed(GetStampDialog.SELECT_ITEM a_selectItem)
	{
		bool canRepeat = false;
		if (mGetStampDialog != null)
		{
			canRepeat = mGetStampDialog.CanRepeat;
			UnityEngine.Object.Destroy(mGetStampDialog.gameObject);
		}
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
		if (a_selectItem == GetStampDialog.SELECT_ITEM.GROWUP)
		{
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(null, true, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mGrowupDialog.CanRepeat = canRepeat;
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			StartCoroutine(GrayIn());
		}
	}

	public void OnGetHeartPieaceDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetAccessoryDialog != null)
		{
			UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
			mGetAccessoryDialog = null;
		}
		if (mGame.mPlayer.HeartUpPiece > 0)
		{
			StartCoroutine(GrayOut());
			mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
			mGetStampDialog.Init(null, 0, false, true, false);
			mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			mBundleAccessoryCount = 0;
			mState.Change(STATE.WAIT);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
			mPushedAccessory = null;
		}
	}

	public void OnGetAccessoryDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGetAccessoryDialog.Data;
		if (data == null || data.HasDemo)
		{
		}
		if (data != null)
		{
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER)
			{
				List<AccessoryData> wallPaperPieces = mGame.GetWallPaperPieces(data.GetWallPaperIndex());
				mGame.mPlayer.SetCollectionNotify(wallPaperPieces[0].Index);
				mCockpit.SetEnableMenuButtonAttention(true);
			}
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE)
			{
				mWallpaperTutorialFlg = true;
				VisualCollectionData visualCollectionData = mGame.mVisualCollectionData[data.IsFlip];
				int num = 0;
				for (int i = 0; i < visualCollectionData.piceIds.Length; i++)
				{
					if (mGame.mPlayer.IsAccessoryUnlock(visualCollectionData.piceIds[i]))
					{
						num++;
					}
				}
				mGame.mPlayer.SetCollectionNotify(data.Index);
				mCockpit.SetEnableMenuButtonAttention(true);
				if (visualCollectionData.piceIds.Length == num)
				{
					if (mGetAccessoryDialog != null)
					{
						UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
						mGetAccessoryDialog = null;
					}
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.Init(true);
					mGetAccessoryDialog.SetClosedCallback(OnWallPeperCompClosed);
					mState.Reset(STATE.WAIT, true);
					return;
				}
			}
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.TROPHY)
			{
				mGame.mTrophyProductionFlg = GameMain.TROPHY_PRODUCTION.NONE;
			}
		}
		StartCoroutine(GrayIn());
		if (mGetAccessoryDialog != null)
		{
			UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
			mGetAccessoryDialog = null;
		}
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
	}

	protected void OnWallPeperCompClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
	}

	public void OnMapRoadBlockTapped(SMRoadBlockSetting a_setting, MapRoadBlock a_roadblock)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mRoadBlockDialog = Util.CreateGameObject("RoadBlockDialog", mRoot).AddComponent<RoadBlockDialog>();
			mRoadBlockDialog.Init(a_setting);
			mRoadBlockDialog.SetClosedCallback(OnRoadBlockDialogClosed);
			mRoadBlockDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnRoadBlockDialogClosed(RoadBlockDialog.SELECT_ITEM i)
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(currentSeries, out data);
		if (i != RoadBlockDialog.SELECT_ITEM.CANCEL)
		{
			SMRoadBlockSetting currentRoadBlockSetting = mRoadBlockDialog.CurrentRoadBlockSetting;
			data.UpdateUnlockedRoadBlock(currentRoadBlockSetting.RoadBlockID);
			mDemoNoAfterRBUnlock = currentRoadBlockSetting.GetStoryDemoClear();
			if (currentSeries == Def.SERIES.SM_FIRST && currentRoadBlockSetting.RoadBlockLevel == 3000)
			{
				SingletonMonoBehaviour<Marketing>.Instance.AdjustSendEvent("m9fowf");
				int num = 0;
				num = 39437;
				if (num != 0)
				{
					SingletonMonoBehaviour<Marketing>.Instance.PartytrackSendEvent(num);
				}
			}
			List<SMRoadBlockSetting> list = new List<SMRoadBlockSetting>(mMapPageData.RoadBlockDataList.Values);
			list.Sort((SMRoadBlockSetting x, SMRoadBlockSetting y) => x.RoadBlockID - y.RoadBlockID);
			int num2 = currentRoadBlockSetting.RoadBlockID + 1;
			if (num2 >= list.Count)
			{
				data.NextRoadBlockLevel = Def.GetStageNo(999, 0);
			}
			else
			{
				data.NextRoadBlockLevel = list[num2].RoadBlockLevel;
			}
			mCurrentPage.SetAvatarNode(currentRoadBlockSetting.StageNo, true);
			float stageWorldPos = mCurrentPage.GetStageWorldPos(currentRoadBlockSetting.StageNo);
			StartCoroutine(DirectMapMove(0f - stageWorldPos));
			UpdateMapPages();
			if (i != RoadBlockDialog.SELECT_ITEM.UNLOCK_KEY)
			{
				int roadBlockLevel = currentRoadBlockSetting.RoadBlockLevel;
				mGetAccessoryDialog = Util.CreateGameObject("BrokenRBKey", mRoot).AddComponent<GetAccessoryDialog>();
				mGetAccessoryDialog.InitRBKeyUnlockByOther(roadBlockLevel, i, currentSeries);
				mGetAccessoryDialog.SetClosedCallback(OnBrokenRBKeyClosed);
				List<AccessoryData> roadBlockAccessory = mGame.GetRoadBlockAccessory(roadBlockLevel, currentSeries);
				for (int j = 0; j < roadBlockAccessory.Count; j++)
				{
					mGame.mPlayer.AddAccessory(roadBlockAccessory[j]);
				}
				mGame.mPlayer.SetRoadBlockKey(currentSeries, 0);
				mCurrentPage.UpdateMapButtonState();
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.mPlayer.SetRoadBlockKey(currentSeries, 0);
				mCurrentPage.StartRoadBlockUnlockEffect(data.RoadBlockReachLevel);
				mState.Change(STATE.RBUNLOCK_WAIT);
			}
			List<AccessoryData> nextRBKeyID = GetNextRBKeyID();
			for (int k = 0; k < nextRBKeyID.Count; k++)
			{
				if (mGame.mPlayer.IsAccessoryUnlock(nextRBKeyID[k].Index))
				{
					mGame.mPlayer.AddRoadBlockKey(1, currentSeries);
				}
			}
			CompactionRoadBlockResponses();
			mGame.Save();
		}
		else
		{
			mCurrentPage.OpenRoadBlock(data.RoadBlockReachLevel);
			mState.Change(STATE.UNLOCK_ACTING);
		}
		StartCoroutine(GrayIn());
	}

	public void CompactionRoadBlockResponses()
	{
		List<GiftItem> list = new List<GiftItem>();
		if (mGame.mPlayer.Gifts.Count > 0)
		{
			foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
			{
				GiftItem value = gift.Value;
				if (value == null || value.GiftItemCategory != Def.ITEM_CATEGORY.ROADBLOCK_RES)
				{
					continue;
				}
				for (int i = 0; i < 5; i++)
				{
					if (value.Data.ItemKind == i && value.Data.ItemID < mGame.mPlayer.Level)
					{
						list.Add(value);
					}
				}
			}
		}
		if (list.Count <= 0)
		{
			return;
		}
		foreach (GiftItem item in list)
		{
			mGame.mPlayer.RemoveGift(item);
		}
	}

	public void OnBrokenRBKeyClosed(GetAccessoryDialog.SELECT_ITEM a_electItem)
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(currentSeries, out data);
		mCurrentPage.StartRoadBlockUnlockEffect(data.RoadBlockReachLevel);
		mState.Change(STATE.RBUNLOCK_WAIT);
		UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
		mGetAccessoryDialog = null;
	}

	public void OnMapChangeTapped(SMChapterSetting a_setting, ChangeMapButton a_mapChange)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		bool flag = false;
		Def.SERIES value;
		if (Def.NextSeries.TryGetValue(mGame.mPlayer.Data.CurrentSeries, out value) && !Def.IsEventSeries(value) && mGame.mGameProfile.IsSeriesUnlocked(value))
		{
			flag = true;
		}
		if (!flag)
		{
			if (mGame.mPlayer.Data.CurrentSeries == Def.SeriesGaidenMax)
			{
				ShowSelectSeries();
				return;
			}
			mMapChangeDialog = Util.CreateGameObject("MapChangeDialog", mRoot).AddComponent<MapChangeDialog>();
			mMapChangeDialog.Init();
			mMapChangeDialog.SetClosedCallback(OnMapChangeDialogClosed);
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			mSelectedseries = (int)value;
			if (!mIsFirstOpenStage)
			{
				mGameState.FadeOutMaskForStory();
			}
			mState.Reset(STATE.SERIES_SELECT_FADE, true);
		}
	}

	public void OnMapChangeDialogClosed(MapChangeDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnMapSNSTapped(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !(mStrayUserDialog != null))
		{
			StartCoroutine(GrayOut());
			if (mGame.mPlayer.Data.ApplyRandomUserNum >= GameMain.APPLY_RANDOM_USER_MAX)
			{
				mConfirmDialog = Util.CreateGameObject("LimitApplyDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("Friend_Request_Close_Title"), Localization.Get("Friend_Request_Close_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mStrayUserDialog = Util.CreateGameObject("StrayUserDialog", mRoot).AddComponent<RandomFriendDialog>();
				mStrayUserDialog.Init();
				mStrayUserDialog.SetClosedCallback(OnStrayUserDialogClosed);
				mState.Reset(STATE.WAIT, true);
			}
		}
	}

	protected void OnStrayUserDialogClosed(bool a_isEmpty, bool FacebookLogin)
	{
		StartCoroutine(GrayIn());
		List<SearchUserData> strayUserList = mGame.mPlayer.GetStrayUserList(mGame.RandomUserList);
		mGame.RandomUserList.Clear();
		foreach (SearchUserData item in strayUserList)
		{
			mGame.RandomUserList.Add(item.Clone());
		}
		if (FacebookLogin)
		{
			mState.Change(STATE.NETWORK_PROLOGUE);
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	protected void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnTutorialSkipConfirmDialogClosed(TutorialSkipConfirmDialog.SELECT_ITEM i)
	{
		if (i == TutorialSkipConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9022);
			mGame.mPlayer.SetTutorialSkip(true);
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
			mCockpit.SetEnableButtonAction(true);
			mCockpitOperation = true;
		}
		else
		{
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_9023);
			int selectStage = mCurrentStoryDemo.SelectStage;
			StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, selectStage, string.Empty, 0));
		}
		mGame.Save();
	}

	protected override void OnExitToTitle(bool force)
	{
		if ((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || force)
		{
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mState.Change(STATE.UNLOAD_WAIT);
		}
	}

	protected bool SeriesNoticeCheck()
	{
		if (string.IsNullOrEmpty(mGame.mGameProfile.LinkBannerBaseURL))
		{
			return false;
		}
		bool flag = false;
		switch (mCurrentPage.Series)
		{
		case Def.SERIES.SM_R:
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_R))
			{
				flag = true;
			}
			break;
		case Def.SERIES.SM_S:
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_S))
			{
				flag = true;
			}
			break;
		case Def.SERIES.SM_SS:
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_SS))
			{
				flag = true;
			}
			break;
		case Def.SERIES.SM_STARS:
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_STARS))
			{
				flag = true;
			}
			break;
		case Def.SERIES.SM_GF00:
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_GF00))
			{
				flag = true;
			}
			break;
		}
		if (!flag)
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mSeriesNoticeDialog = Util.CreateGameObject("SeriesNoticeDialog", mRoot).AddComponent<SeriesNoticeDialog>();
		mSeriesNoticeDialog.Init(mCurrentPage.Series);
		mSeriesNoticeDialog.SetCallback(OnSeriesNoticeDialogClosed);
		mState.Reset(STATE.WAIT, true);
		return true;
	}

	public void OnSeriesNoticeDialogClosed(SeriesNoticeDialog.SELECT_ITEM item, Def.SERIES a_series)
	{
		StartCoroutine(GrayIn());
		switch (a_series)
		{
		case Def.SERIES.SM_R:
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_R);
			break;
		case Def.SERIES.SM_S:
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_S);
			break;
		case Def.SERIES.SM_SS:
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_SS);
			break;
		case Def.SERIES.SM_STARS:
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_STARS);
			break;
		case Def.SERIES.SM_GF00:
			mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_SERIES_NOTICE_GF00);
			break;
		}
		mGame.Save();
		mSeriesNoticeDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private bool CharacterIntroCheck()
	{
		if (!Def.CharacterIntroList.ContainsKey(mCurrentPage.Series))
		{
			return false;
		}
		if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			return false;
		}
		DateTime dateTime = DateTimeUtil.Now();
		int num = 0;
		string s = dateTime.ToString("yyyyMMdd");
		try
		{
			num = int.Parse(s);
		}
		catch
		{
			return false;
		}
		if (!UserDataInfo.Instance.ShowCharacterInfoDialogDate.ContainsKey(mCurrentPage.Series))
		{
			UserDataInfo.Instance.ShowCharacterInfoDialogDate.Add(mCurrentPage.Series, 0);
		}
		int num2 = UserDataInfo.Instance.ShowCharacterInfoDialogDate[mCurrentPage.Series];
		if (num2 != 0)
		{
			int seriesCampaignMoves = mGame.GetSeriesCampaignMoves(mCurrentPage.Series);
			if (seriesCampaignMoves <= 0)
			{
				return false;
			}
			int seriesCampaignStartDate = mGame.GetSeriesCampaignStartDate(mCurrentPage.Series);
			if (num2 >= seriesCampaignStartDate)
			{
				return false;
			}
		}
		if (!DisplayCharacterIntroDialog(num))
		{
			return false;
		}
		mState.Reset(STATE.WAIT, true);
		return true;
	}

	private bool DisplayCharacterIntroDialog(int a_currentDate)
	{
		bool flag = false;
		int[] array = Def.CharacterIntroList[mCurrentPage.Series];
		int num = -1;
		foreach (int num2 in array)
		{
			if (!mGame.mPlayer.IsCompanionUnlock(num2))
			{
				num = num2;
				flag = true;
				break;
			}
		}
		if (Def.CharacterIntroExcept.Contains(num))
		{
			flag = false;
		}
		if (!flag)
		{
			return false;
		}
		AccessoryData accessoryData = null;
		List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
		for (int j = 0; j < accessoryByType.Count; j++)
		{
			if (accessoryByType[j].GetCompanionID() == num)
			{
				accessoryData = accessoryByType[j];
				break;
			}
		}
		if (accessoryData == null)
		{
			return false;
		}
		UserDataInfo.Instance.ShowCharacterInfoDialogDate[mCurrentPage.Series] = a_currentDate;
		UserDataInfo.Instance.Save();
		mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
		mEventIntroCharacterDialog.Init(accessoryData.Index, Def.EVENT_TYPE.SM_NONE, accessoryData.MainStage);
		mEventIntroCharacterDialog.SetClosedCallback(delegate
		{
			mState.Change(STATE.UNLOCK_ACTING);
			StartCoroutine(GrayIn());
			mEventIntroCharacterDialog = null;
		});
		return true;
	}

	protected bool InformationCheck()
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return false;
		}
		InformationManager.Instance.MightUpdateInfo();
		if (InformationManager.Instance.IsConnectDone && InformationManager.Instance.IsConnectSuccessed)
		{
			return false;
		}
		mState.Reset(STATE.NETWORK_INFORMATION_WAIT);
		return true;
	}

	protected override bool WebviewStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return false;
		}
		int count = InformationManager.Instance.Count;
		string[] uRL = InformationManager.Instance.URL;
		if (count == 0 || (uRL.Length < 1 && !NoticeConstructDialog.CanMake()))
		{
			return false;
		}
		bool noticeFirstBootFlg = mGame.NoticeFirstBootFlg;
		mGame.NoticeFirstBootFlg = false;
		if (mGame.mPlayer.Data.LastEventInfoID == count)
		{
			DateTime lastEventInfoDisplayTime = mGame.mPlayer.Data.LastEventInfoDisplayTime;
			DateTime dateTime = new DateTime(lastEventInfoDisplayTime.Year, lastEventInfoDisplayTime.Month, lastEventInfoDisplayTime.Day, 24 - Def.RENOTIFICATION, 0, 0, 0);
			if (!(mGame.mPlayer.Data.LastEventInfoDisplayTime != dateTime) || !noticeFirstBootFlg)
			{
				long num = DateTimeUtil.BetweenHours(mGame.mPlayer.Data.LastEventInfoDisplayTime, DateTime.Now);
				if (num < Def.RENOTIFICATION)
				{
					return false;
				}
			}
		}
		else if (mGame.mPlayer.Data.LastEventInfoID > count)
		{
			return false;
		}
		StartCoroutine(GrayOut());
		OnMenuProces(MenuDialog.SELECT_MENU.NOTICE);
		mState.Reset(STATE.WAIT, true);
		return true;
	}

	public void OnMenuOption()
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut(0.5f, Color.black, 1f));
			mMenuDialog = Util.CreateGameObject("MenuDialog", mRoot).AddComponent<MenuDialog>();
			mMenuDialog.Init(Localization.Get("Menu_Title"), null);
			mMenuDialog.SetClosedCallback(OnMenuProces);
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_OPENOPTION", -1);
		}
	}

	public void OnCollectionPushed(GameObject go)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.COLLECTION);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void OnMenuProces(MenuDialog.SELECT_MENU item)
	{
		switch (item)
		{
		case MenuDialog.SELECT_MENU.SERIES:
			mConfirmDialog = null;
			ShowSelectSeries();
			break;
		case MenuDialog.SELECT_MENU.TROPHY:
			mConfirmDialog = null;
			StartCoroutine(GrayOut());
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TROPHY);
			mState.Reset(STATE.WAIT, true);
			break;
		case MenuDialog.SELECT_MENU.COLLECTION:
			mConfirmDialog = null;
			OnCollectionPushed(null);
			break;
		case MenuDialog.SELECT_MENU.OPTION:
			mConfirmDialog = null;
			mSelectOptionDialog = Util.CreateGameObject("OptionDialog", mRoot).AddComponent<SelectOptionDialog>();
			mSelectOptionDialog.Init(Localization.Get("Setting_Select"), null, SelectOptionDialog.OPTION_DIALOG_STYLE.OPTION_ICON);
			mSelectOptionDialog.SetClosedCallback(OptionMenu);
			break;
		case MenuDialog.SELECT_MENU.MYPAGE:
			mConfirmDialog = null;
			mMyFriendDialog = Util.CreateGameObject("MyFriendDialog", mRoot).AddComponent<MyFriendDialog>();
			mMyFriendDialog.Init(mCurrentPage.mAvater);
			mMyFriendDialog.SetClosedCallback(Friendclosed);
			break;
		case MenuDialog.SELECT_MENU.NOTICE:
			mConfirmDialog = null;
			OpenNoticeDialog();
			break;
		case MenuDialog.SELECT_MENU.RANKING:
			mConfirmDialog = null;
			mFriendRankingDialog = Util.CreateGameObject("RankingDialog", mRoot).AddComponent<FriendRankingDialog>();
			mFriendRankingDialog.SetClosedCallback(OnFriendRankingClosed);
			break;
		case MenuDialog.SELECT_MENU.GROWUP:
			mConfirmDialog = null;
			OnGrowPushed(null);
			break;
		case MenuDialog.SELECT_MENU.TITLE:
			mConfirmDialog = null;
			OnTitlePushed(null);
			break;
		default:
			mConfirmDialog = null;
			StartCoroutine(GrayIn(1f));
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		}
	}

	public void OpenNoticeDialog()
	{
		if (InformationManager.Instance.URL != null && InformationManager.Instance.URL.Length > 0)
		{
			bool have_other_page = NoticeConstructDialog.CanMake();
			mWebViewMultiPageDialog = Util.CreateGameObject("NOTICEDialog", mRoot).AddComponent<WebViewMultiPageDialog>();
			mWebViewMultiPageDialog.Init(Localization.Get("Webview_Title"), InformationManager.Instance.URL.Length, InformationManager.Instance.URL[0], false, new Color32(byte.MaxValue, 230, 235, byte.MaxValue), have_other_page);
			mWebViewMultiPageDialog.SetMultiPageCallback(OnMultiPageWebViewClosed);
			mWebViewMultiPageDialog.SetPageMovedCallback(OnNoticeDialogPageMoved);
			mWebViewMultiPageDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_DOMAIN);
		}
		else if (NoticeConstructDialog.CanMake())
		{
			mNoticeConstructDialog = Util.CreateGameObject("NoticeDialog", mRoot).AddComponent<NoticeConstructDialog>();
			mNoticeConstructDialog.Init(new Color32(byte.MaxValue, 230, 235, byte.MaxValue), mRoot);
			mNoticeConstructDialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, OnNoticeConstructDialogClosed);
		}
	}

	public void OnFriendRankingClosed()
	{
		StartCoroutine(GrayIn());
		if (mFriendRankingDialog != null)
		{
			UnityEngine.Object.Destroy(mFriendRankingDialog.gameObject);
			mFriendRankingDialog = null;
		}
		mState.Change(STATE.MAIN);
	}

	public void OptionMenu(SelectOptionDialog.SELECT_OPTION data)
	{
		switch (data)
		{
		case SelectOptionDialog.SELECT_OPTION.BGM:
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		case SelectOptionDialog.SELECT_OPTION.EULA:
			mEULADialog = Util.CreateGameObject("EULADialog", mRoot).AddComponent<EULADialog>();
			mEULADialog.Init(EULADialog.MODE.EULA);
			mEULADialog.SetCallback(OnEULADialogClosed);
			break;
		case SelectOptionDialog.SELECT_OPTION.PRIVACY:
			mEULADialog = Util.CreateGameObject("EULADialog", mRoot).AddComponent<EULADialog>();
			mEULADialog.Init(EULADialog.MODE.PRIVACY);
			mEULADialog.SetCallback(OnEULADialogClosed);
			break;
		case SelectOptionDialog.SELECT_OPTION.PRIVACY_OPTION:
		{
			GameObject gameObject2 = Util.CreateGameObject("PrivacyOptionDialog", mRoot);
			mHelpDialog = gameObject2.AddComponent<WebViewDialog>();
			mHelpDialog.Title = Localization.Get("PrivacyOption_Title_En");
			mHelpDialog.SetClosedCallback(OnPrivacyOptionClosed);
			mHelpDialog.SetPageMovedCallback(OnPrivacyOptionPageMoved);
			mState.Change(STATE.PRIVACY_OPTION);
			break;
		}
		case SelectOptionDialog.SELECT_OPTION.HELP:
		{
			GameObject gameObject = Util.CreateGameObject("HelpDialog", mRoot);
			mHelpDialog = gameObject.AddComponent<WebViewDialog>();
			mHelpDialog.Title = Localization.Get("Help_Title");
			mHelpDialog.SetClosedCallback(OnHelpDialogClosed);
			mHelpDialog.SetPageMovedCallback(OnHelpDialogPageMoved);
			mState.Change(STATE.WEBVIEW);
			break;
		}
		default:
			StartCoroutine(GrayIn(1f));
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		}
	}

	protected void OnPrivacyOption()
	{
		if (mHelpDialog.IsOpend)
		{
			mState.Reset(STATE.WAIT, true);
			mHelpDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.NONE);
			mHelpDialog.LoadURL(mGame.GameProfile.mGDPRSetting.mOptOutURL);
		}
	}

	protected void OnPrivacyOptionClosed()
	{
		StartCoroutine(GrayIn());
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
		}
		mHelpDialog = null;
		mState.Change(STATE.MAIN);
	}

	protected void OnPrivacyOptionPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mHelpDialog.ShowWebview(true);
		}
		else
		{
			mHelpDialog.ShowWebview(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
			mConfirmDialog.SetClosedCallback(OnHelpErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	protected void OnHelpDialogClosed()
	{
		StartCoroutine(GrayIn());
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
		}
		mHelpDialog = null;
		mState.Change(STATE.MAIN);
	}

	protected void OnHelpDialog()
	{
		if (mHelpDialog.IsOpend)
		{
			mState.Reset(STATE.WAIT, true);
			mHelpDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.NONE);
			StringBuilder stringBuilder = new StringBuilder();
			Dictionary<string, object> p = new Dictionary<string, object>();
			stringBuilder.Append(mGame.GameProfile.HelpUrl);
			string supportParams = Network.GetSupportParams(ref p);
			if (p.Count > 0)
			{
				stringBuilder.Append("?");
				stringBuilder.Append(supportParams);
			}
			mHelpDialog.LoadURL(stringBuilder.ToString());
		}
	}

	protected void OnHelpDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mHelpDialog.ShowWebview(true);
		}
		else
		{
			mHelpDialog.ShowWebview(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
			mConfirmDialog.SetClosedCallback(OnHelpErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	protected void OnHelpErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (mHelpDialog != null)
		{
			switch (item)
			{
			case ConfirmDialog.SELECT_ITEM.POSITIVE:
				mHelpDialog.Reload();
				break;
			case ConfirmDialog.SELECT_ITEM.NEGATIVE:
				mHelpDialog.Close();
				break;
			}
		}
		mConfirmDialog = null;
	}

	protected void Friendclosed(bool FacebookLogin)
	{
		StartCoroutine(GrayIn());
		if (mMyFriendDialog != null)
		{
			UnityEngine.Object.Destroy(mMyFriendDialog.gameObject);
			mMyFriendDialog = null;
		}
		if (FacebookLogin)
		{
			mState.Change(STATE.NETWORK_PROLOGUE);
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public void OnMultiPageWebViewClosed(WebViewMultiPageDialog.SELECT_ITEM item, int page)
	{
		bool flag = NoticeConstructDialog.CanMake();
		int num = InformationManager.Instance.URL.Length - page;
		if (page != 0 && InformationManager.Instance.URL != null && num < InformationManager.Instance.URL.Length)
		{
			mWebViewMultiPageDialog = null;
			mWebViewMultiPageDialog = Util.CreateGameObject("NOTICEDialog", mRoot).AddComponent<WebViewMultiPageDialog>();
			mWebViewMultiPageDialog.Init(Localization.Get("Webview_Title"), page, InformationManager.Instance.URL[InformationManager.Instance.URL.Length - page], false, new Color32(byte.MaxValue, 230, 235, byte.MaxValue), flag);
			mWebViewMultiPageDialog.SetMultiPageCallback(OnMultiPageWebViewClosed);
			mWebViewMultiPageDialog.SetPageMovedCallback(OnNoticeDialogPageMoved);
			mWebViewMultiPageDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_DOMAIN);
			return;
		}
		if (mGame.TappedDeepLink == null && flag && mWebViewMultiPageDialog.CanShowAutoNotice)
		{
			mNoticeConstructDialog = Util.CreateGameObject("NoticeDialog", mRoot).AddComponent<NoticeConstructDialog>();
			mNoticeConstructDialog.Init(new Color32(byte.MaxValue, 230, 235, byte.MaxValue), mRoot);
			mNoticeConstructDialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, OnNoticeConstructDialogClosed);
			mWebViewMultiPageDialog = null;
			return;
		}
		StartCoroutine(GrayIn());
		mGame.mPlayer.Data.LastEventInfoID = InformationManager.Instance.Count;
		if (mWebViewMultiPageDialog.IsCheckState)
		{
			DateTime now = DateTime.Now;
			now = new DateTime(now.Year, now.Month, now.Day, 24 - Def.RENOTIFICATION, 0, 0, 0);
			mGame.mPlayer.Data.LastEventInfoDisplayTime = now;
		}
		else
		{
			DateTime lastEventInfoDisplayTime = DateTime.Now;
			if (lastEventInfoDisplayTime.Hour >= 24 - Def.RENOTIFICATION)
			{
				lastEventInfoDisplayTime = new DateTime(lastEventInfoDisplayTime.Year, lastEventInfoDisplayTime.Month, lastEventInfoDisplayTime.Day, 24 - Def.RENOTIFICATION, 0, 0, 1);
			}
			mGame.mPlayer.Data.LastEventInfoDisplayTime = lastEventInfoDisplayTime;
		}
		mGame.Save();
		mWebViewMultiPageDialog = null;
		if (mGame.TappedDeepLink != null)
		{
			mState.Reset(STATE.DEEPLINK_PUSHED, true);
			OnDeepLinkPushed();
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public void OnNoticeConstructDialogClosed()
	{
		if (InformationManager.Instance.AutoNoticeQueueCount > 0)
		{
			int page = mNoticeConstructDialog.Page;
			mNoticeConstructDialog = null;
			mNoticeConstructDialog = Util.CreateGameObject("NoticeDialog", mRoot).AddComponent<NoticeConstructDialog>();
			mNoticeConstructDialog.Init(new Color32(byte.MaxValue, 230, 235, byte.MaxValue), mRoot, page + 1);
			mNoticeConstructDialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, OnNoticeConstructDialogClosed);
			return;
		}
		StartCoroutine(GrayIn());
		mGame.mPlayer.Data.LastEventInfoID = InformationManager.Instance.Count;
		if (mNoticeConstructDialog.IsCheckState)
		{
			DateTime now = DateTime.Now;
			now = new DateTime(now.Year, now.Month, now.Day, 24 - Def.RENOTIFICATION, 0, 0, 0);
			mGame.mPlayer.Data.LastEventInfoDisplayTime = now;
		}
		else
		{
			DateTime lastEventInfoDisplayTime = DateTime.Now;
			if (lastEventInfoDisplayTime.Hour >= 24 - Def.RENOTIFICATION)
			{
				lastEventInfoDisplayTime = new DateTime(lastEventInfoDisplayTime.Year, lastEventInfoDisplayTime.Month, lastEventInfoDisplayTime.Day, 24 - Def.RENOTIFICATION, 0, 0, 1);
			}
			mGame.mPlayer.Data.LastEventInfoDisplayTime = lastEventInfoDisplayTime;
		}
		mGame.Save();
		mNoticeConstructDialog = null;
		if (mGame.TappedDeepLink != null)
		{
			mState.Reset(STATE.DEEPLINK_PUSHED, true);
			OnDeepLinkPushed();
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public void OnNoticeDialogClosed(NoticeDialog.SELECT_ITEM item, int page)
	{
		if (page != 0)
		{
			mNoticeDialog = null;
			mNoticeDialog = Util.CreateGameObject("NoticeDialog", mRoot).AddComponent<NoticeDialog>();
			mNoticeDialog.Init(NoticeDialog.MODE.PAGE, page);
			mNoticeDialog.SetCallback(OnNoticeDialogClosed);
			return;
		}
		StartCoroutine(GrayIn());
		mGame.mPlayer.Data.LastEventInfoID = InformationManager.Instance.Count;
		if (mNoticeDialog.mCheck_state)
		{
			DateTime now = DateTime.Now;
			now = new DateTime(now.Year, now.Month, now.Day, 24 - Def.RENOTIFICATION, 0, 0, 0);
			mGame.mPlayer.Data.LastEventInfoDisplayTime = now;
		}
		else
		{
			DateTime lastEventInfoDisplayTime = DateTime.Now;
			if (lastEventInfoDisplayTime.Hour >= 24 - Def.RENOTIFICATION)
			{
				lastEventInfoDisplayTime = new DateTime(lastEventInfoDisplayTime.Year, lastEventInfoDisplayTime.Month, lastEventInfoDisplayTime.Day, 24 - Def.RENOTIFICATION, 0, 0, 1);
			}
			mGame.mPlayer.Data.LastEventInfoDisplayTime = lastEventInfoDisplayTime;
		}
		mGame.Save();
		mNoticeDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnNoticeDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mWebViewMultiPageDialog.ShowWebview(true);
			return;
		}
		mWebViewMultiPageDialog.ShowWebview(false);
		mWebViewMultiPageDialog.PauseState();
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("Network_Error_Desc"));
		mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
		mConfirmDialog.SetBaseDepth(70);
		mConfirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
		{
			switch (i)
			{
			case ConfirmDialog.SELECT_ITEM.POSITIVE:
				mWebViewMultiPageDialog.ResumeStateWithReload();
				break;
			case ConfirmDialog.SELECT_ITEM.NEGATIVE:
				mWebViewMultiPageDialog.ResumeState();
				mWebViewMultiPageDialog.ClosePage(false);
				break;
			}
		});
	}

	public new void OnEULADialogClosed(EULADialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		mEULADialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnCampaignDialogClosed(CampaignDialog.SELECT_ITEM item)
	{
		if (item == CampaignDialog.SELECT_ITEM.POSITIVE)
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.BIGINNER_DIALOG;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(mRoot);
			mState.Reset(STATE.WAIT, true);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
		if (mCampaignDialog != null)
		{
			UnityEngine.Object.Destroy(mCampaignDialog.gameObject);
			mCampaignDialog = null;
		}
		mIsInitialTutorialCompleted = true;
		mGame.Save();
	}

	public void OnCampaignErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		if (item != 0)
		{
			mGotCampaignData = true;
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnCharacterChoice()
	{
		mGame.PlaySe("SE_PUSH", -1);
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.COLLECTION);
			mState.Change(STATE.UNLOAD_WAIT);
		}
	}

	public void OnBgmPushed()
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			mGame.ToggleBGM();
			mGame.PlaySe("SE_PUSH", -1);
		}
	}

	public void OnSePushed()
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			mGame.ToggleSE();
			mGame.PlaySe("SE_PUSH", -1);
		}
	}

	public override void OnSeriesCampaignPushed(Def.SERIES a_series)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			mSelectedseries = (int)a_series;
			if (!mIsFirstOpenStage)
			{
				mGameState.FadeOutMaskForStory();
			}
			mState.Reset(STATE.SERIES_SELECT_FADE, true);
			mGame.PlaySe("SE_POSITIVE", -1);
		}
	}

	public void OnDebugPushed()
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
		sMDDebugDialog.SetGameState(this);
		sMDDebugDialog.SetClosedCallback(delegate
		{
			mState.Reset(STATE.UNLOCK_ACTING, true);
			if (mGame.mDebugShowEventAppealSetting != null)
			{
				mEventAppealSetting = mGame.mDebugShowEventAppealSetting.Clone();
				mGame.mDebugShowEventAppealSetting = null;
			}
			else if (mGame.TappedDeepLink != null)
			{
				mState.Reset(STATE.DEEPLINK_PUSHED, true);
				OnDeepLinkPushed();
			}
			else if (mGame.mDebugNoticeSeries)
			{
				mGame.mDebugNoticeSeries = false;
				ShowNextSeasonNotice();
			}
		});
	}

	public override void OnIndexButtonPushed(int index)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && mGame.linkbanner_InfoList.Count >= index)
		{
			LinkBannerInfo linkBannerInfo = mGame.linkbanner_InfoList[index];
			ServerCram.LinkBannerClick((int)linkBannerInfo.action, index + 1, linkBannerInfo.fileName);
			OnLinkBannerAction(linkBannerInfo);
		}
	}

	protected void OnLinkBaseConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private void OnLinkBannerAction(LinkBannerInfo linkBanner)
	{
		switch (linkBanner.action)
		{
		case LinkBannerInfo.ACTION.NOP:
			if (mState.GetStatus() == STATE.DEEPLINK_PUSHED)
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case LinkBannerInfo.ACTION.NOTICE:
			OpenNoticeDialog();
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			break;
		case LinkBannerInfo.ACTION.SALE:
		{
			ShopItemData shopItemData = mGame.mShopItemData[linkBanner.param];
			double num = 0.0;
			double num2 = 0.0;
			int num3 = 0;
			int num4 = 0;
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			bool flag4 = false;
			for (int i = 0; i < mGame.SegmentProfile.ShopItemList.Count; i++)
			{
				int shopItemID = mGame.SegmentProfile.ShopItemList[i].ShopItemID;
				if (shopItemID.CompareTo(shopItemData.Index) == 0)
				{
					num = mGame.SegmentProfile.ShopItemList[i].StartTime;
					num2 = mGame.SegmentProfile.ShopItemList[i].EndTime;
					num3 = mGame.SegmentProfile.ShopItemList[i].ShopItemLimit;
					num4 = mGame.SegmentProfile.ShopItemList[i].ShopCount;
					flag = mGame.mPlayer.IsEnablePurchaseItem(shopItemID, num3, num4);
					flag2 = true;
					break;
				}
			}
			for (int j = 0; j < mGame.SegmentProfile.ShopMugenHeartItemList.Count; j++)
			{
				if (mGame.SegmentProfile.ShopMugenHeartItemList[j].ShopItemID.CompareTo(shopItemData.Index) == 0)
				{
					flag3 = true;
					break;
				}
			}
			for (int k = 0; k < mGame.SegmentProfile.ShopAllCrushItemList.Count; k++)
			{
				int shopItemID2 = mGame.SegmentProfile.ShopAllCrushItemList[k].ShopItemID;
				if (shopItemID2.CompareTo(shopItemData.Index) == 0)
				{
					num = mGame.SegmentProfile.ShopAllCrushItemList[k].StartTime;
					num2 = mGame.SegmentProfile.ShopAllCrushItemList[k].EndTime;
					num3 = mGame.SegmentProfile.ShopAllCrushItemList[k].ShopItemLimit;
					num4 = mGame.SegmentProfile.ShopAllCrushItemList[k].ShopCount;
					flag = mGame.mPlayer.IsEnablePurchaseItem(shopItemID2, num3, num4);
					flag4 = true;
					break;
				}
			}
			if (!flag2 && !flag3 && !flag4)
			{
				mConfirmDialog = Util.CreateGameObject("TimeOverSaleDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerExpired_Shop_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				mConfirmDialog.SetClosedCallback(OnLinkBaseConfirmDialogClosed);
				StartCoroutine(GrayOut());
				mState.Reset(STATE.WAIT, true);
				break;
			}
			DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(num, true);
			DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(num2, true);
			DateTime dateTime3 = DateTimeUtil.Now();
			if (num == num2 || (dateTime < dateTime3 && dateTime3 < dateTime2))
			{
				if (flag)
				{
					if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL8_3))
					{
						StartCoroutine("GrayOut");
						mConfirmDialog = Util.CreateGameObject("AttentionDialog", mRoot).AddComponent<ConfirmDialog>();
						mConfirmDialog.Init(Localization.Get("SeasonStore_Error01_Title"), Localization.Get("SeasonStore_Error01_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
						mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
						mState.Reset(STATE.WAIT);
					}
					else if (mCockpit.mMugenflg && flag3)
					{
						OnMugenHeartButtonPushed(null);
						StartCoroutine(GrayOut());
						mState.Reset(STATE.WAIT, true);
					}
					else if (mCockpit.mShopflg)
					{
						OnStoreBagPushed(null, true);
						StartCoroutine(GrayOut());
						mState.Reset(STATE.WAIT, true);
					}
					else
					{
						mConfirmDialog = Util.CreateGameObject("NoLimitSaleDialog", mRoot).AddComponent<ConfirmDialog>();
						mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerSoldOut_Shop_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
						mConfirmDialog.SetClosedCallback(OnLinkBaseConfirmDialogClosed);
						StartCoroutine(GrayOut());
						mState.Reset(STATE.WAIT, true);
					}
				}
				else
				{
					mConfirmDialog = Util.CreateGameObject("NoLimitSaleDialog", mRoot).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerSoldOut_Shop_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
					mConfirmDialog.SetClosedCallback(OnLinkBaseConfirmDialogClosed);
					StartCoroutine(GrayOut());
					mState.Reset(STATE.WAIT, true);
				}
			}
			else
			{
				mConfirmDialog = Util.CreateGameObject("TimeOverSaleDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerExpired_Shop_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				mConfirmDialog.SetClosedCallback(OnLinkBaseConfirmDialogClosed);
				StartCoroutine(GrayOut());
				mState.Reset(STATE.WAIT, true);
			}
			break;
		}
		case LinkBannerInfo.ACTION.EVENT:
			if (!mGame.IsSeasonEventExpired(linkBanner.param))
			{
				if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
				{
					StartCoroutine("GrayOut");
					mConfirmDialog = Util.CreateGameObject("AttentionDialog", mRoot).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("SeasonEvent_Error01_Title"), Localization.Get("SeasonEvent_Error01_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
					mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					OnCurrentEventPushed(linkBanner.param);
					StartCoroutine(GrayOut());
					mState.Reset(STATE.WAIT, true);
				}
			}
			else
			{
				mConfirmDialog = Util.CreateGameObject("TimeOverSaleDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("LinkBannerExpired_Title"), Localization.Get("LinkBannerExpired_Event_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				mConfirmDialog.SetClosedCallback(OnLinkBaseConfirmDialogClosed);
				StartCoroutine(GrayOut());
				mState.Reset(STATE.WAIT, true);
			}
			break;
		case LinkBannerInfo.ACTION.ADV:
		case LinkBannerInfo.ACTION.ADV_LB_PLAY:
		case LinkBannerInfo.ACTION.ADV_LB_GASHAMENU:
		case LinkBannerInfo.ACTION.ADV_LB_EVENT:
		case LinkBannerInfo.ACTION.ADV_LB_GASHA:
			if ((mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen) && mState.GetStatus() != STATE.DEEPLINK_PUSHED)
			{
				mState.Change(STATE.UNLOCK_ACTING);
				break;
			}
			if (!mGame.EnableAdvEnter())
			{
				OnGameCenterNoEnterPushed(null);
				break;
			}
			switch (linkBanner.action)
			{
			case LinkBannerInfo.ACTION.ADV_LB_EVENT:
				if (mGame.IsPlayEventDungeon())
				{
					mGame.mADVLink = GameMain.ADV_LINK.EVENT;
					mGame.mADVLinkParam = linkBanner.param;
				}
				else
				{
					mGame.mADVLink = GameMain.ADV_LINK.HOME;
				}
				break;
			case LinkBannerInfo.ACTION.ADV_LB_GASHA:
				if (mGame.mTutorialManager.IsGashaAdvTutorialCompleted())
				{
					mGame.mADVLink = GameMain.ADV_LINK.GASHA;
					mGame.mADVLinkParam = linkBanner.param;
				}
				else
				{
					mGame.mADVLink = GameMain.ADV_LINK.HOME;
				}
				break;
			case LinkBannerInfo.ACTION.ADV_LB_PLAY:
				if (mGame.IsPlayEventDungeon())
				{
					mGame.mADVLink = GameMain.ADV_LINK.PLAY;
				}
				else
				{
					mGame.mADVLink = GameMain.ADV_LINK.HOME;
				}
				break;
			case LinkBannerInfo.ACTION.ADV_LB_GASHAMENU:
				if (mGame.mTutorialManager.IsGashaAdvTutorialCompleted())
				{
					mGame.mADVLink = GameMain.ADV_LINK.GASHAMENU;
				}
				else
				{
					mGame.mADVLink = GameMain.ADV_LINK.HOME;
				}
				break;
			default:
				mGame.mADVLink = GameMain.ADV_LINK.HOME;
				break;
			}
			mGame.PlaySe("SE_POSITIVE", -1);
			mGame.EnterADV();
			mState.Reset(STATE.WAIT, true);
			break;
		case LinkBannerInfo.ACTION.MAIL:
		{
			GameObject mailButtonGo = mCockpit.GetMailButtonGo();
			OnGiftPushed(mailButtonGo);
			break;
		}
		case LinkBannerInfo.ACTION.PURCHASE:
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.MAP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(mRoot);
			mState.Reset(STATE.WAIT, true);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			break;
		}
		case LinkBannerInfo.ACTION.JUMP_STORE:
			Util.JumpToStore(false);
			mState.Reset(STATE.WAIT_JUMPSTORE);
			break;
		default:
			if (mState.GetStatus() == STATE.DEEPLINK_PUSHED)
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		}
	}

	public void OnDeepLinkPushed()
	{
		if (mState.GetStatus() != STATE.DEEPLINK_PUSHED)
		{
			mGame.ClearTappedDeepLink();
			return;
		}
		LinkBannerInfo linkBannerInfo = mGame.TappedDeepLink.GetLinkBannerInfo();
		OnLinkBannerAction(linkBannerInfo);
		ServerCram.TapDeepLink((int)linkBannerInfo.action, mGame.TappedDeepLink.PageNo, mGame.TappedDeepLink.InfoPlace, mGame.TappedDeepLink.BannerID, linkBannerInfo.param, 0);
		mGame.ClearTappedDeepLink();
	}

	protected virtual void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugLoginBonusTimeReset)
			{
				mGame.mPlayer.Data.LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
				mGame.mDebugLoginBonusTimeReset = false;
			}
			if (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.WAIT_JUMPSTORE)
			{
				mState.Reset(STATE.NETWORK_RESUME_MAINTENACE, true);
			}
			else
			{
				mGame.DoLoginBonusServerCheck = true;
			}
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	protected override bool SetDeviceScreen(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		Map_DeviceScreenSizeY = vector.y;
		bool result = false;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			MAP_BOUNDS_RANGE = 67f;
			Map_DeviceOffset = 0f;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			MAP_BOUNDS_RANGE = 67f * mGame.mDeviceRatioScale;
			Map_DeviceOffset = (1136f - vector.y) / 2f * mGame.mDeviceRatioScale;
			result = true;
			break;
		}
		return result;
	}

	protected override void SetLayout(ScreenOrientation o)
	{
		if (mSocialRanking != null)
		{
			UnityEngine.Object.Destroy(mSocialRanking.gameObject);
			mSocialRanking = null;
			Network_OnStageWait();
		}
		mMapPageRoot.transform.parent = mAnchorCenter.gameObject.transform;
		mMapPageRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
		mMapPageRoot.transform.localScale = new Vector3(mGame.mDeviceRatioScale, mGame.mDeviceRatioScale, 1f);
		bool flag = SetDeviceScreen(o);
		if (mCockpit == null)
		{
			mCockpit = Util.CreateGameObject("Cockpit", base.gameObject).AddComponent<Cockpit>();
			if (mState.GetStatus() == STATE.MAIN)
			{
				mCockpit.Init(this);
			}
			else
			{
				mCockpit.Init(this, false);
			}
			mCockpit.SetCallback(OnCockpitOpend, OnCockpitClosed);
		}
		CollectionUpStartCheck();
		GiftAttentionCheck();
		if (MissionManager.EnableGetReward())
		{
			mCockpit.SetEnableMenuButtonAttention(true);
		}
		if (mState.GetStatus() == STATE.MAIN)
		{
			mCockpit.SetEnableButtonAction(true);
		}
		else
		{
			mCockpit.SetEnableButtonAction(false);
		}
		mCockpit.SetLayoutCockpitPosition(o);
		if (!mGame.mTutorialManager.IsInitialTutorialCompleted() && mGame.mTutorialManager.IsTutorialPlaying())
		{
			UpdateCurrentMapPos(flag);
		}
		mCurrentPage.SetLayout(flag);
		SetScrollPower(0f);
		mScrollPowerYTemp = 0f;
		mGame.ResetTouch();
		UpdateMapPages();
		if ((o == ScreenOrientation.LandscapeLeft || o == ScreenOrientation.LandscapeRight) && mState.GetStatus() == STATE.TUTORIAL && mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST && TutorialMapStage())
		{
			float num = 0f;
			switch (mGame.mPlayer.PlayableMaxLevel)
			{
			case 900:
				num = mCurrentPage.GetStageWorldPos(800);
				break;
			case 1000:
				num = mCurrentPage.GetStageWorldPos(100);
				break;
			case 1400:
				num = mCurrentPage.GetStageWorldPos(1000);
				break;
			}
			StartCoroutine(DirectMapMove(0f - num));
		}
	}

	public override void DEBUG_ChangeStateToInit()
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void ReEnterMap()
	{
		mState.Reset(STATE.GOTO_REENTER);
	}

	protected virtual IEnumerator ProcessNewAreaOpen()
	{
		mIsFinishedNewAreaOpen = false;
		yield return StartCoroutine(GrayOut(1f, Color.black, 1f));
		yield return StartCoroutine(DirectMapMove(0f - mFadeTargetPos));
		yield return StartCoroutine(GrayIn());
		mGame.PlaySe("SE_MAP_OPEN_AREA", -1);
		mCurrentPage.StartNewAreaOpenEffect(mFadeTargetPage);
		mFadeTargetPage = 0;
		mIsFinishedNewAreaOpen = true;
	}

	protected virtual IEnumerator ProcessNewAreaReturn()
	{
		mIsFinishedNewAreaReturn = false;
		yield return StartCoroutine(GrayOut(1f, Color.black, 0.5f));
		yield return StartCoroutine(DirectMapMove(0f - mFadeBeforePos));
		mFadeBeforePos = 0f;
		yield return StartCoroutine(GrayIn());
		mIsFinishedNewAreaReturn = true;
	}

	protected override void OnMapDestroyUpDateClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override void OnMapDestroyClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void OnNewAreaOpenDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.NEWAREA_RETURN);
	}

	protected void OnNewAreaOpenDialogClosedNoEffect(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnLensFlareFinished(OneShotAnime anime)
	{
		if (mLensFlareAnime != null)
		{
			UnityEngine.Object.Destroy(mLensFlareAnime.gameObject);
			mLensFlareAnime = null;
		}
	}

	protected bool AccessoryConnectCheck()
	{
		int count = mGame.mPlayer.Data.AccessoryConnect.Count;
		if (count > 0 && AccessoryConnectIndex == 0)
		{
			for (int i = 0; i < mGame.mPlayer.Data.AccessoryConnect.Count; i++)
			{
				int num = mGame.mPlayer.Data.AccessoryConnect[i];
				if (!mGame.AlreadyConnectAccessoryList.Contains(num))
				{
					AccessoryConnectIndex = num;
					return true;
				}
			}
		}
		return false;
	}

	protected bool AdvAccessoryConnectCheck()
	{
		if (mGame.mMaintenanceProfile.IsAdvMaintenanceMode)
		{
			return false;
		}
		int count = mGame.mPlayer.Data.AdvAccessoryConnect.Count;
		if (count > 0 && AdvAccessoryConnectIndex == 0)
		{
			for (int i = 0; i < mGame.mPlayer.Data.AdvAccessoryConnect.Count; i++)
			{
				int num = mGame.mPlayer.Data.AdvAccessoryConnect[i];
				if (!mGame.AlreadyConnectAdvAccessoryList.Contains(num))
				{
					AdvAccessoryConnectIndex = num;
					return true;
				}
			}
		}
		return false;
	}

	private bool MightOpenCampaignDialog()
	{
		int[] result = null;
		int firstAvailableCampaignID = mGame.GetFirstAvailableCampaignID(out result);
		bool flag = false;
		if (result != null)
		{
			int[] array = result;
			foreach (int num in array)
			{
				if (!GlobalVariables.Instance.AcquiredCampaignId.Contains(num))
				{
					GlobalVariables.Instance.AcquiredCampaignId.Add(num);
				}
				else if (num == firstAvailableCampaignID)
				{
					flag = true;
				}
			}
		}
		if (firstAvailableCampaignID == -1 || flag)
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mCampaignDialog = Util.CreateGameObject("CampaignDialog", mRoot).AddComponent<CampaignDialog>();
		mCampaignDialog.Init(firstAvailableCampaignID);
		mCampaignDialog.SetClosedCallback(OnCampaignDialogClosed);
		mState.Reset(STATE.WAIT, true);
		return true;
	}

	protected void OnGotErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnGetRewardDialogErrorClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected bool UpdateConnecting()
	{
		bool flag = false;
		switch (mStateConnecting.GetStatus())
		{
		case STATE.NETWORK_PROLOGUE:
			mIsConnecting = true;
			mFriendManager = null;
			if (!GameMain.CanCommunication())
			{
				mStateConnecting.Change(STATE.NETWORK_EPILOGUE);
			}
			else if (!mTipsScreenDisplayed)
			{
				mStateConnecting.Change(STATE.NETWORK_EPILOGUE);
			}
			else
			{
				mStateConnecting.Change(STATE.NETWORK_MAINTENANCE);
			}
			break;
		case STATE.NETWORK_MAINTENANCE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mStateConnecting.Change(STATE.NETWORK_MAINTENANCE_00);
			break;
		case STATE.NETWORK_MAINTENANCE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					flag = true;
				}
				else if (mGame.AddedNewFriend)
				{
					mGame.AddedNewFriend = false;
					mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitTimeEpoch;
					mStateConnecting.Change(STATE.NETWORK_GETFRIENDINFO);
				}
				else
				{
					mStateConnecting.Change(STATE.NETWORK_AUTH);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorBoot, 38);
				flag = true;
			}
			break;
		case STATE.NETWORK_AUTH:
			mGame.Network_UserAuth();
			mStateConnecting.Change(STATE.NETWORK_AUTH_00);
			break;
		case STATE.NETWORK_AUTH_00:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mStateConnecting.Change(STATE.NETWORK_00);
					break;
				}
				mGame.FinishConnecting();
				flag = true;
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode mAdvGetMailErrorCode = mUserAuthErrorCode;
			if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 40, OnConnectErrorAuthRetryBoot, OnConnectErrorAuthAbandonBoot);
				flag = true;
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 40, OnConnectErrorAuthRetryBoot, null);
				flag = true;
			}
			break;
		}
		case STATE.NETWORK_00:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mStateConnecting.Change(STATE.NETWORK_00_00);
			break;
		case STATE.NETWORK_00_00:
			if (!GameMain.mGameProfileCheckDone)
			{
				break;
			}
			if (GameMain.mGameProfileSucceed)
			{
				if (mGame.mGameProfile.LinkBanner != null && mGame.mGameProfile.LinkBanner.BannerList.Count > 0)
				{
					mGame.LinkBannerList_Clear();
					LinkBannerManager.Instance.GetLinkBannerRequest(mGame.mLBRequest, mGame.mGameProfile.LinkBanner, OnGetLinkBannerComplete);
				}
				mStateConnecting.Change(STATE.NETWORK_01);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 42, OnConnectErrorAuthRetryBoot, null);
				flag = true;
			}
			break;
		case STATE.NETWORK_01:
			mGame.Network_GetSegmentProfile(string.Empty, string.Empty);
			mStateConnecting.Change(STATE.NETWORK_01_00);
			break;
		case STATE.NETWORK_01_00:
		{
			if (!GameMain.mSegmentProfileCheckDone)
			{
				break;
			}
			if (GameMain.mSegmentProfileSucceed)
			{
				mStateConnecting.Change(STATE.NETWORK_02);
				break;
			}
			Server.ErrorCode mSegmentProfileErrorCode = GameMain.mSegmentProfileErrorCode;
			Server.ErrorCode mAdvGetMailErrorCode = mSegmentProfileErrorCode;
			if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 44, OnConnectErrorAuthRetryBoot, OnConnectErrorAuthAbandonBoot);
				flag = true;
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 44, OnConnectErrorAuthRetryBoot, null);
				flag = true;
			}
			break;
		}
		case STATE.NETWORK_02:
			mStateConnecting.Change(STATE.NETWORK_03);
			break;
		case STATE.NETWORK_03:
			mGame.Network_LoginBonus();
			mStateConnecting.Change(STATE.NETWORK_03_00);
			break;
		case STATE.NETWORK_03_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				mGame.DoLoginBonusServerCheck = false;
				mStateConnecting.Change(STATE.NETWORK_04);
			}
			else
			{
				Server.ErrorCode mLoginBonusErrorCode = GameMain.mLoginBonusErrorCode;
				Server.ErrorCode mAdvGetMailErrorCode = mLoginBonusErrorCode;
				if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 48, OnConnectErrorAuthRetryBoot, OnConnectErrorAuthAbandon);
					flag = true;
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 48, OnConnectErrorAuthRetryBoot, null);
					flag = true;
				}
			}
			break;
		case STATE.NETWORK_04:
			mGame.CheckMailDeadLine();
			mGame.mGotGiftIds = new List<int>();
			mGame.mGotApplyIds = new List<int>();
			mGame.mGotSupportIds = new List<int>();
			mGame.mGotSupportPurchaseIds = new List<int>();
			if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				mStateConnecting.Change(STATE.NETWORK_04_00);
			}
			else
			{
				mStateConnecting.Change(STATE.NETWORK_04_02);
			}
			break;
		case STATE.NETWORK_ADV_GET_MAIL:
		{
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			mGame.Network_AdvGetMail(mGuID, is_retry);
			mStateConnecting.Change(STATE.NETWORK_ADV_GET_MAIL_WAIT);
			break;
		}
		case STATE.NETWORK_ADV_GET_MAIL_WAIT:
		{
			if (!GameMain.mAdvGetMailCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetMailSucceed)
			{
				mGuID = null;
				mStateConnecting.Change(STATE.NETWORK_04_00);
				break;
			}
			StartCoroutine(GrayOut());
			Server.ErrorCode mAdvGetMailErrorCode = GameMain.mAdvGetMailErrorCode;
			if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 51, OnConnectErrorAuthRetryBoot, OnConnectErrorAuthAbandonBoot);
				flag = true;
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 51, OnConnectErrorAuthRetryBoot, null);
				flag = true;
			}
			break;
		}
		case STATE.NETWORK_04_00:
		{
			STATE aStatus4 = STATE.NETWORK_04_00_1;
			if (mGame.MapNetwork_SupportCheck())
			{
				aStatus4 = STATE.NETWORK_04_01;
			}
			mStateConnecting.Change(aStatus4);
			break;
		}
		case STATE.NETWORK_04_00_1:
			if (GameMain.mMapSupportCheckDone)
			{
				mStateConnecting.Change(STATE.NETWORK_04_00_2);
			}
			break;
		case STATE.NETWORK_04_00_2:
			mGame.MapNetwork_SupportGot();
			mStateConnecting.Change(STATE.NETWORK_04_01);
			break;
		case STATE.NETWORK_04_01:
		{
			STATE aStatus2 = STATE.NETWORK_04_01_1;
			if (mGame.MapNetwork_GiftCheck())
			{
				aStatus2 = STATE.NETWORK_04_02;
			}
			mStateConnecting.Change(aStatus2);
			break;
		}
		case STATE.NETWORK_04_01_1:
			if (GameMain.mMapGiftCheckDone)
			{
				mStateConnecting.Change(STATE.NETWORK_04_01_2);
			}
			break;
		case STATE.NETWORK_04_01_2:
			mGame.MapNetwork_GiftGot();
			mStateConnecting.Change(STATE.NETWORK_04_02);
			break;
		case STATE.NETWORK_04_02:
		{
			STATE aStatus = STATE.NETWORK_04_02_1;
			if (mGame.MapNetwork_ApplyCheck())
			{
				aStatus = STATE.NETWORK_04_03;
			}
			mStateConnecting.Change(aStatus);
			break;
		}
		case STATE.NETWORK_04_02_1:
			if (GameMain.mMapApplyCheckDone)
			{
				mStateConnecting.Change(STATE.NETWORK_04_02_2);
			}
			break;
		case STATE.NETWORK_04_02_2:
			mGame.MapNetwork_ApplyGot();
			if (GameMain.mMapApplyRepeat)
			{
				mStateConnecting.Change(STATE.NETWORK_04_02_3);
			}
			else
			{
				mStateConnecting.Change(STATE.NETWORK_04_03);
			}
			break;
		case STATE.NETWORK_04_02_3:
		{
			long num = DateTimeUtil.BetweenMilliseconds(mGame.mPlayer.Data.LastGetApplyTime, DateTime.Now);
			if (num > GameMain.mMapApplyRepeatWait)
			{
				mGame.mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
				mStateConnecting.Change(STATE.NETWORK_04_02);
			}
			break;
		}
		case STATE.NETWORK_04_03:
			if (GameMain.NO_NETWORK)
			{
				mStateConnecting.Change(STATE.NETWORK_04_04);
			}
			else if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				STATE aStatus3 = STATE.NETWORK_04_03_1;
				if (mGame.MapNetwork_SupportPurchaseCheck())
				{
					aStatus3 = STATE.NETWORK_04_04;
				}
				mStateConnecting.Change(aStatus3);
			}
			else
			{
				mStateConnecting.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_03_1:
			if (GameMain.mMapSupportPurchaseCheckDone)
			{
				mStateConnecting.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_04:
			mStateConnecting.Reset(STATE.NETWORK_04_99, true);
			break;
		case STATE.NETWORK_04_99:
		{
			mStateConnecting.Change(STATE.NETWORK_GETRANDOMUSER);
			if (mGame.mFbUserMngGift == null)
			{
				mGame.mFbUserMngGift = FBUserManager.CreateInstance();
			}
			List<GiftItemData> list = new List<GiftItemData>();
			if (mGame.mPlayer.Gifts.Count <= 0)
			{
				break;
			}
			foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
			{
				GiftItem value = gift.Value;
				list.Add(value.Data);
			}
			mGame.mFbUserMngGift.SetFBUserList(list);
			break;
		}
		case STATE.NETWORK_GETRANDOMUSER:
		{
			if (!mGame.mPlayer.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL9_7.ToString()))
			{
				mMapRandomUserCheckDone = false;
				mStateConnecting.Change(STATE.NETWORK_GETFRIENDINFO);
				break;
			}
			STATE sTATE = MapNetwork_GetRandomUser();
			if (sTATE == STATE.NETWORK_GETFRIENDINFO)
			{
				mStrayTutorialFlg = true;
			}
			mStateConnecting.Change(sTATE);
			break;
		}
		case STATE.NETWORK_GETRANDOMUSER_00:
			if (mMapRandomUserCheckDone)
			{
				mStateConnecting.Change(STATE.NETWORK_GETFRIENDINFO);
			}
			break;
		case STATE.NETWORK_GETFRIENDINFO:
			mFriendManager = new GameFriendManager(mGame);
			if (!mFriendManager.Start())
			{
				mStateConnecting.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mStateConnecting.Change(STATE.NETWORK_GETFRIENDINFO_00);
			}
			break;
		case STATE.NETWORK_GETFRIENDINFO_00:
			if (mFriendManager == null || mFriendManager.IsFinished())
			{
				mStateConnecting.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mFriendManager.Update();
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO:
			if (mProductListReceiveDone)
			{
				if (mGame.Network_CampaignCheck(mIsInitialTutorialCompleted))
				{
					mStateConnecting.Change(STATE.NETWORK_GETCAMPAIGNINFO_00);
				}
				else
				{
					mStateConnecting.Change(STATE.NETWORK_EPILOGUE);
				}
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mStateConnecting.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_EPILOGUE:
			mIsConnecting = false;
			mStateConnecting.Reset(STATE.MAIN, true);
			break;
		}
		if (flag)
		{
			mIsConnecting = false;
			mStateConnecting.Reset(STATE.MAIN, true);
		}
		mStateConnecting.Update();
		return flag;
	}

	public void ShowNextSeasonNotice()
	{
		if (mNextSeasonDialog == null)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mNextSeasonDialog = Util.CreateGameObject("NextSeasonDialog", mRoot).AddComponent<NextSeasonNoticeDialog>();
			if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_FIRST)
			{
				mNextSeasonDialog.Init();
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_R)
			{
				mNextSeasonDialog.Init(22, 23);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_S)
			{
				mNextSeasonDialog.Init(21);
			}
			else if (mGame.mPlayer.Data.CurrentSeries == Def.SERIES.SM_SS)
			{
				mNextSeasonDialog.Init(62, 60, 64);
			}
			mNextSeasonDialog.SetClosedCallback(OnNextSeasonDialogClosed);
		}
	}

	protected void OnNextSeasonDialogClosed(NextSeasonNoticeDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mNextSeasonDialog.gameObject);
		mNextSeasonDialog = null;
		if (mState.GetStatus() != STATE.SERIES_SELECT_FADE)
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public void ShowSelectSeries()
	{
		List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
		int level = playStage[0].Level;
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_SEASON_SELECT_0))
		{
			StartCoroutine("GrayOut");
			mConfirmDialog = Util.CreateGameObject("AttentionDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("SeasonEvent_Error01_Title"), Localization.Get("SeasonEvent_Error01_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Reset(STATE.WAIT);
		}
		else if (mSeriesSelectManager == null)
		{
			StartCoroutine(GrayOut(0.8f, Color.black, 0.5f));
			if (GameMain.USE_NEW_SERIES_SELECT)
			{
				mSeriesSelectManager = Util.CreateGameObject("SeriesSelectManager", base.gameObject).AddComponent<SeriesSelectManager2>();
			}
			else
			{
				mSeriesSelectManager = Util.CreateGameObject("SeriesSelectManager", base.gameObject).AddComponent<SeriesSelectManager>();
			}
			mSeriesSelectManager.SetCallback(OnSelectSeriesClosed);
			mState.Reset(STATE.SERIES_SELECT_INIT_WAIT, true);
		}
	}

	public void OnSelectSeriesClosed(int a_selectedseries)
	{
		switch (mState.GetStatus())
		{
		case STATE.UNLOAD_WAIT:
		case STATE.UNLOCK_ACTING:
		case STATE.SERIES_SELECT_CANCEL:
		case STATE.SERIES_SELECT_FADE:
			return;
		}
		if (a_selectedseries == -1 || !Enum.IsDefined(typeof(Def.SERIES), a_selectedseries) || a_selectedseries == (int)mGame.mPlayer.Data.CurrentSeries)
		{
			if (mSeriesSelectManager != null)
			{
				mSeriesSelectManager.CancelPushed();
			}
			mState.Reset(STATE.SERIES_SELECT_CANCEL, true);
			return;
		}
		mSelectedseries = a_selectedseries;
		if (!mIsFirstOpenStage)
		{
			mGameState.FadeOutMaskForAnchor("SeriesSelectPanel");
		}
		mState.Reset(STATE.SERIES_SELECT_FADE, true);
	}

	private List<AccessoryData> GetNextRBKeyID()
	{
		List<AccessoryData> result = new List<AccessoryData>();
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, out data);
		int nextRoadBlockLevel = data.NextRoadBlockLevel;
		int a_main;
		int a_sub;
		Def.SplitStageNo(nextRoadBlockLevel, out a_main, out a_sub);
		SMMapPageData currentMapPageData = GameMain.GetCurrentMapPageData();
		SMRoadBlockSetting roadBlock = currentMapPageData.GetRoadBlock(a_main, 0);
		if (roadBlock != null)
		{
			int roadBlockLevel = roadBlock.RoadBlockLevel;
			result = mGame.GetRoadBlockAccessory(roadBlockLevel, mGame.mPlayer.Data.CurrentSeries);
		}
		return result;
	}

	private void ProcessMultiUnlock()
	{
		mGame.mPlayer.StageUnlockList.Sort();
		int stageNo = mGame.mPlayer.StageUnlockList[0];
		mGame.mPlayer.SetStageStatus(mGame.mPlayer.Data.CurrentSeries, stageNo, Player.STAGE_STATUS.UNLOCK);
		mCurrentPage.UnlockStageButton(stageNo);
		mGame.mPlayer.StageUnlockList.RemoveAt(0);
		PlayerMapData data;
		mGame.mPlayer.Data.GetMapData(mCurrentPage.Series, out data);
		if (data == null)
		{
			return;
		}
		if (data.TempStageClearData.Count > 0)
		{
			int num = data.TempStageClearData.FindIndex((PlayerClearData a) => a.Series == mCurrentPage.Series && a.StageNo == stageNo);
			if (num != -1)
			{
				PlayerClearData playerClearData = data.TempStageClearData[num];
				if (playerClearData != null)
				{
					mGame.mPlayer.AddStageClearData(playerClearData);
					mGame.mPlayer.SetClearStageFromTemporary(playerClearData.StageNo);
					data.TempStageClearData.Remove(playerClearData);
					CheckUnlockNewStage();
					Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mCurrentPage.Series, playerClearData.StageNo);
					mCurrentPage.ChangeStageButton(playerClearData.StageNo, stageStatus, (byte)playerClearData.GotStars);
				}
			}
		}
		if (data.TempStageChallengeData.Count <= 0)
		{
			return;
		}
		int num2 = data.TempStageChallengeData.FindIndex((PlayerStageData a) => a.Series == mCurrentPage.Series && a.StageNo == stageNo);
		if (num2 != -1)
		{
			PlayerStageData playerStageData = data.TempStageChallengeData[num2];
			if (playerStageData != null)
			{
				mGame.mPlayer.SetMapStageChallengeData(mCurrentPage.Series, playerStageData);
				data.TempStageChallengeData.Remove(playerStageData);
			}
		}
	}
}
