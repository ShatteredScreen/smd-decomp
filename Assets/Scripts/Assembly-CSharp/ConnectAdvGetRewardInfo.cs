public class ConnectAdvGetRewardInfo : ConnectBase
{
	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			base.StateStart();
			return;
		}
		ConnectAdvGetRewardInfoParam connectAdvGetRewardInfoParam = mParam as ConnectAdvGetRewardInfoParam;
		if (connectAdvGetRewardInfoParam == null)
		{
			BIJLog.E("Type Error Parameter:" + GetType().Name + "<=" + mParam.GetType().Name);
			SetServerResponse(1, -1);
		}
		else if (!Server.AdvGetRewardInfo(connectAdvGetRewardInfoParam.RewardIDList, connectAdvGetRewardInfoParam.Category))
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetAdvGetRewardInfoFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
