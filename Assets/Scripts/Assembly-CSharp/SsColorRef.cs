using System;
using UnityEngine;

[Serializable]
public class SsColorRef : SsInterpolatable
{
	public byte R;

	public byte G;

	public byte B;

	public byte A;

	public SsColorRef(SsColorRef r)
	{
		R = r.R;
		G = r.G;
		B = r.B;
		A = r.A;
	}

	public SsColorRef()
	{
	}

	public static SsColorRef[] CreateArray(int num)
	{
		SsColorRef[] array = new SsColorRef[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = new SsColorRef();
		}
		return array;
	}

	public SsColorRef Clone()
	{
		return new SsColorRef(this);
	}

	public override string ToString()
	{
		return "R: " + R + ", G: " + G + ", B: " + B + ", A: " + A;
	}

	public SsInterpolatable GetInterpolated(SsCurveParams curve, float time, SsInterpolatable start, SsInterpolatable end, int startTime, int endTime)
	{
		SsColorRef ssColorRef = new SsColorRef();
		return ssColorRef.Interpolate(curve, time, start, end, startTime, endTime);
	}

	public SsInterpolatable Interpolate(SsCurveParams curve, float time, SsInterpolatable start_, SsInterpolatable end_, int startTime, int endTime)
	{
		SsColorRef ssColorRef = (SsColorRef)start_;
		SsColorRef ssColorRef2 = (SsColorRef)end_;
		R = (byte)SsInterpolation.Interpolate(curve, time, ssColorRef.R, ssColorRef2.R, startTime, endTime);
		G = (byte)SsInterpolation.Interpolate(curve, time, ssColorRef.G, ssColorRef2.G, startTime, endTime);
		B = (byte)SsInterpolation.Interpolate(curve, time, ssColorRef.B, ssColorRef2.B, startTime, endTime);
		A = (byte)SsInterpolation.Interpolate(curve, time, ssColorRef.A, ssColorRef2.A, startTime, endTime);
		return this;
	}

	public static explicit operator Color(SsColorRef s)
	{
		Color result = default(Color);
		result.r = (float)(int)s.R / 255f;
		result.g = (float)(int)s.G / 255f;
		result.b = (float)(int)s.B / 255f;
		result.a = (float)(int)s.A / 255f;
		return result;
	}
}
