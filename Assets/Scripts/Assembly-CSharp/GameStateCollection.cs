using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class GameStateCollection : GameState
{
	public enum STATE
	{
		LOAD_WAIT = 0,
		UNLOAD_WAIT = 1,
		INIT = 2,
		SELECT = 3,
		WAIT = 4
	}

	public enum MODE
	{
		CHARACTER = 0,
		WALLPAPER = 1,
		EPISODE = 2,
		EPISODE2 = 3,
		CONSUME = 4
	}

	public enum PAGE
	{
		TOP = 0,
		CHARACTER_SELECT = 1,
		CHARACTER_VIEW = 2,
		WALLPAPER_SELECT = 3,
		WALLPAPER_VIEW = 4,
		EPISODE_SERIES = 5,
		EPISODE_SELECT = 6,
		EPISODE_VIEW = 7,
		CONSUME_SELECT = 8
	}

	private const float DELAY_FROM_TOP = 0.1f;

	private const float DELAY_FROM_TOP_EPISODE = 0.3f;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private MODE mMode;

	private PAGE[][] PageTbl = new PAGE[5][]
	{
		new PAGE[3]
		{
			PAGE.TOP,
			PAGE.CHARACTER_SELECT,
			PAGE.CHARACTER_VIEW
		},
		new PAGE[3]
		{
			PAGE.TOP,
			PAGE.WALLPAPER_SELECT,
			PAGE.WALLPAPER_VIEW
		},
		new PAGE[4]
		{
			PAGE.TOP,
			PAGE.EPISODE_SERIES,
			PAGE.EPISODE_SELECT,
			PAGE.EPISODE_VIEW
		},
		new PAGE[3]
		{
			PAGE.TOP,
			PAGE.EPISODE_SERIES,
			PAGE.EPISODE_VIEW
		},
		new PAGE[2]
		{
			PAGE.TOP,
			PAGE.CONSUME_SELECT
		}
	};

	private GameStateManager mGameState;

	private GameObject mAnchorTop;

	private GameObject mAnchorBottomLeft;

	private GameObject mAnchorTopRight;

	private int mBaseDepth;

	private UISprite mBgSprite;

	private UILabel mTitleBarLabel;

	private GameObject[] mPageRoots = new GameObject[4];

	private UIButton mBackButton;

	private float mScrollX;

	private int mCurrentPage;

	private bool mLaceMove;

	private List<SMVerticalListItem> mCharacterList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mSkillList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mMotionList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mWallPaperList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mEpisodeSeriesList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mEventSeriesList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mEpisodeList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mConsumeList = new List<SMVerticalListItem>();

	private List<SMVerticalListItem> mConsumeADVList = new List<SMVerticalListItem>();

	private SMVerticalList mCharacterSelectList;

	private SMVerticalList mSkillSelectList;

	private SMVerticalList mMotionSelectList;

	private SMVerticalList mWallPaperSelectList;

	private SMVerticalList mEpisodeSeriesSelectList;

	private SMVerticalList mEpisodeSelectList;

	private SMVerticalList mConsumeSelectList;

	private static SMVerticalListInfo CharacterListInfo = new SMVerticalListInfo(4, 580f, 515f, 4, 580f, 900f, 1, 140);

	private static SMVerticalListInfo SkillListInfo = new SMVerticalListInfo(1, 540f, 148f, 1, 540f, 148f, 1, 68);

	private static SMVerticalListInfo MotionListInfo = new SMVerticalListInfo(2, 570f, 240f, 2, 570f, 240f, 1, 70);

	private static SMVerticalListInfo WallPaperListInfo = new SMVerticalListInfo(4, 930f, 515f, 3, 580f, 900f, 1, 300);

	private static SMVerticalListInfo EpisodeSeriesListInfo = new SMVerticalListInfo(1, 580f, 506f, 1, 580f, 890f, 1, 180);

	private static SMVerticalListInfo EpisodeSeriesListInfo2 = new SMVerticalListInfo(1, 580f, 466f, 1, 580f, 890f, 1, 180);

	private static SMVerticalListInfo EpisodeListInfo = new SMVerticalListInfo(4, 930f, 400f, 3, 580f, 750f, 1, 220);

	private static SMVerticalListInfo EpisodeListInfo2 = new SMVerticalListInfo(4, 930f, 360f, 3, 580f, 750f, 1, 220);

	private static SMVerticalListInfo MessageListInfo = new SMVerticalListInfo(3, 580f, 506f, 3, 580f, 890f, 1, 220);

	private static SMVerticalListInfo MessageListInfo2 = new SMVerticalListInfo(3, 580f, 466f, 3, 580f, 890f, 1, 220);

	private static SMVerticalListInfo ConsumeListInfo = new SMVerticalListInfo(1, 580f, 515f, 1, 580f, 900f, 1, 122);

	private UIButton mCharacterButton;

	private UIButton mWallPaperButton;

	private UIButton mEpisodeButton;

	private UIButton mConsumeButton;

	private byte mSelectedConsumeCategory;

	private UISprite mWindow;

	private UILabel mWindowTitleLabel;

	private UISprite mLineSprite;

	private UILabel mNameLabel;

	private UISprite mLevelSprite;

	private UISprite[] mLevelStars = new UISprite[5];

	private UIButton mSkillButton;

	private UIButton mMotionButton;

	private UISprite mShadowLace;

	private Partner mPartner;

	private bool mSkillEffectPlaying;

	private BoxCollider mSkillEffectMask;

	private int mSelectedCharacterIndex;

	private int mCharacterInfoPage;

	private int mRequestMotion;

	private bool mSeNegativeCalled;

	private GameObject mCharacterInfoPageRoot;

	private UILabel mSkillNameLabel;

	private UILabel mSkillDescLabel;

	private UISprite mWallPaperFrame;

	private UIButton mDownloadButton;

	private byte mSelectedSeriesCategory;

	private StoryDemoManager mStoryDemo;

	private EpisodeSeriesSelectListItem mSelectedEpisodeSeriesItem;

	private int mSelectedStoryDemoIndex;

	private UISprite mSeriesCurtain;

	private UIButton mSeriesMapButton;

	private UIButton mSeriesEventButton;

	private UIButton mSeriesMessageButton;

	private UISprite mSeriesCheck;

	private UILabel mSeriesEventDescLabel;

	private ConfirmDialog mConfirmDialog;

	private ConfirmImageDialog mConfirmImageDialog;

	private string mWallPaperURL;

	private bool mGetWallPaperDone;

	private string mSaveWallPaperError;

	private bool mSaveWallPaperDone;

	private string[] CharacterViewLevelImageTbl = new string[6] { "lv_1_2", "lv_1_2", "lv_2_2", "lv_3_2", "lv_4_2", "lv_5_2" };

	private string[] CharacterViewLevelMaxImageTbl = new string[6] { "lv_1_2", "lv_1_2", "lv_2_2", "lv_3_2_max", "lv_4_2", "lv_5_2" };

	private bool mSkillPlaying;

	private static string[] CharacterIconLevelImageTbl = new string[6] { "lv_1", "lv_1", "lv_2", "lv_3", "lv_4", "lv_5_0" };

	private static string[] CharacterIconLevelMaxImageTbl = new string[6] { "lv_1", "lv_1", "lv_2", "lv_3_max", "lv_4", "lv_5_0" };

	private static string[] LevelDescDisableTbl = new string[6]
	{
		string.Empty,
		string.Empty,
		"PartnerDesc_Common2",
		"PartnerDesc_Common3",
		"PartnerDesc_Common4",
		"PartnerDesc_Common5"
	};

	private static string[] LevelDescEnableTbl = new string[6]
	{
		string.Empty,
		string.Empty,
		"PartnerDesc_CommonGrow2",
		"PartnerDesc_CommonGrow3",
		"PartnerDesc_CommonGrow4",
		"PartnerDesc_CommonGrow5"
	};

	private string[] LevelImageDisableTbl = new string[6]
	{
		string.Empty,
		"lv_1",
		"lv_2",
		"lv_3",
		"lv_4",
		"lv_5"
	};

	private string[] LevelImageEnableTbl = new string[6]
	{
		string.Empty,
		"lv_1",
		"lv_2",
		"lv_3",
		"lv_4",
		"lv_5"
	};

	private static string[] NumImageNameTbl = new string[10] { "cllection_num0", "cllection_num1", "cllection_num2", "cllection_num3", "cllection_num4", "cllection_num5", "cllection_num6", "cllection_num7", "cllection_num8", "cllection_num9" };

	private static string[] EpisodeSeriesIcon = new string[6] { "panel_season00", "panel_season01", "panel_season02", "panel_season03", "panel_season04", "panel_season10" };

	private static string[] EpisodeSelectIcon = new string[6] { "panel_season00", "panel_season01", "panel_season02", "panel_season03", "panel_season04", "panel_season10_2" };

	private static string[] EpisodeSeriesTitle = new string[6] { "SeasonTitle_00", "SeasonTitle_01", "SeasonTitle_02", "SeasonTitle_03", "SeasonTitle_04", "SeasonTitle_10" };

	private IEnumerator LoadPageResources(PAGE page, bool pageUp)
	{
		List<ResImage> resImageList = new List<ResImage>();
		switch (page)
		{
		case PAGE.WALLPAPER_SELECT:
			resImageList.Add(ResourceManager.LoadImageAsync("WALL_PAPER_ICON"));
			break;
		case PAGE.EPISODE_SERIES:
			resImageList.Add(ResourceManager.LoadImageAsync("EPISODE_ICON"));
			break;
		case PAGE.EPISODE_SELECT:
			if (mSelectedEpisodeSeriesItem.eventId != -1)
			{
				resImageList.Add(ResourceManager.LoadImageAsync("EVENT_EPISODE_" + mSelectedEpisodeSeriesItem.eventId));
			}
			else
			{
				resImageList.Add(ResourceManager.LoadImageAsync("EPISODE_ICON"));
			}
			break;
		}
		while (true)
		{
			bool busy = false;
			foreach (ResImage res in resImageList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					busy = true;
				}
			}
			if (!busy)
			{
				break;
			}
			yield return 0;
		}
	}

	private IEnumerator UnloadPageResources(PAGE page, bool pageUp)
	{
		switch (page)
		{
		case PAGE.WALLPAPER_SELECT:
			ResourceManager.UnloadImage("WALL_PAPER_ICON");
			break;
		case PAGE.EPISODE_SERIES:
			if (!pageUp)
			{
				ResourceManager.UnloadImage("EPISODE_ICON");
			}
			break;
		case PAGE.EPISODE_SELECT:
			if (mSelectedEpisodeSeriesItem.eventId != -1)
			{
				ResourceManager.UnloadImage("EVENT_EPISODE_" + mSelectedEpisodeSeriesItem.eventId);
			}
			if (pageUp)
			{
				ResourceManager.UnloadImage("EPISODE_ICON");
			}
			break;
		}
		yield return 0;
	}

	private void SetPageTitle(PAGE page)
	{
		switch (page)
		{
		case PAGE.TOP:
			mTitleBarLabel.text = Localization.Get("GameStateCollection_Title");
			break;
		case PAGE.CHARACTER_SELECT:
			mTitleBarLabel.text = Localization.Get("GameStateCharacter_Title");
			break;
		case PAGE.CHARACTER_VIEW:
			mTitleBarLabel.text = Localization.Get("GameStateCharacter_Title");
			break;
		case PAGE.WALLPAPER_SELECT:
			mTitleBarLabel.text = Localization.Get("GameStateCollection_WallPaper");
			break;
		case PAGE.WALLPAPER_VIEW:
			mTitleBarLabel.text = Localization.Get("GameStateCollection_WallPaper");
			break;
		case PAGE.EPISODE_SERIES:
			mTitleBarLabel.text = Localization.Get("GameStateCollection_Episode");
			break;
		case PAGE.EPISODE_SELECT:
			mTitleBarLabel.text = Localization.Get("GameStateCollection_Episode");
			break;
		case PAGE.EPISODE_VIEW:
			mTitleBarLabel.text = Localization.Get("GameStateCollection_Episode");
			break;
		case PAGE.CONSUME_SELECT:
			mTitleBarLabel.text = Localization.Get("GameStateCollection_Consume");
			break;
		}
	}

	private IEnumerator CreatePage(PAGE page, int pageIndex, bool pageUp, int param = 0)
	{
		if (mPageRoots[pageIndex] == null)
		{
			mPageRoots[pageIndex] = Util.CreateGameObject("PageRoot" + pageIndex, mRoot);
			mPageRoots[pageIndex].transform.localPosition = new Vector3(1384f * (float)(pageIndex - mCurrentPage), 0f, 0f);
			switch (page)
			{
			case PAGE.TOP:
				yield return StartCoroutine(CreatePage_Top(pageIndex));
				break;
			case PAGE.CHARACTER_SELECT:
				yield return StartCoroutine(CreatePage_CharacterSelect(pageIndex));
				break;
			case PAGE.CHARACTER_VIEW:
				yield return StartCoroutine(CreatePage_CharacterView(pageIndex, param));
				break;
			case PAGE.WALLPAPER_SELECT:
				yield return StartCoroutine(CreatePage_WallpaperSelect(pageIndex));
				break;
			case PAGE.WALLPAPER_VIEW:
				yield return StartCoroutine(CreatePage_WallpaperView(pageIndex, param));
				break;
			case PAGE.EPISODE_SERIES:
				yield return StartCoroutine(CreatePage_EpisodeSeries(pageIndex));
				break;
			case PAGE.EPISODE_SELECT:
				yield return StartCoroutine(CreatePage_EpisodeSelect(pageIndex, mSelectedEpisodeSeriesItem));
				break;
			case PAGE.EPISODE_VIEW:
				yield return StartCoroutine(CreatePage_EpisodeView(pageIndex));
				break;
			case PAGE.CONSUME_SELECT:
				yield return StartCoroutine(CreatePage_ConsumeSelect(pageIndex));
				break;
			}
			SetLayout(Util.ScreenOrientation);
		}
	}

	private IEnumerator DeletePage(PAGE page, int pageIndex, bool pageUp)
	{
		if (mPageRoots[pageIndex] != null)
		{
			switch (page)
			{
			case PAGE.CHARACTER_VIEW:
				DeletePage_CharacterView();
				break;
			case PAGE.EPISODE_SELECT:
				mEpisodeList.Clear();
				break;
			}
			UnityEngine.Object.Destroy(mPageRoots[pageIndex]);
			mPageRoots[pageIndex] = null;
			yield return StartCoroutine(UnloadPageResources(page, pageUp));
			yield return Resources.UnloadUnusedAssets();
		}
	}

	private void CreatePage_CommonBackButton(int pageIndex)
	{
	}

	private IEnumerator CreatePage_Top(int pageIndex)
	{
		UIFont font = GameMain.LoadFont();
		ResImage resImageHud = ResourceManager.LoadImage("HUD");
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		GameObject parent = mPageRoots[pageIndex];
		mCharacterButton = Util.CreateJellyImageButton("Button3", parent, resImageCollection.Image);
		Util.SetImageButtonInfo(mCharacterButton, "LC_button_collection03", "LC_button_collection03", "LC_button_collection03", mBaseDepth + 1, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mCharacterButton, this, "OnCharacterCollectionButtonPushed", UIButtonMessage.Trigger.OnClick);
		List<AccessoryData> companionAccessories = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
		for (int j = 0; j < companionAccessories.Count; j++)
		{
			if (mGame.mPlayer.GetCollectionNotifyStatus(companionAccessories[j].Index) == Player.NOTIFICATION_STATUS.NOTIFY)
			{
				UISsSprite attention2 = Util.CreateGameObject("AttentionHalo", mCharacterButton.gameObject).AddComponent<UISsSprite>();
				attention2.Animation = ResourceManager.LoadSsAnimation("NEW_ATTENTION").SsAnime;
				attention2.depth = mBaseDepth + 5;
				attention2.transform.localPosition = new Vector3(-223f, 66f, 0f);
				attention2.transform.localScale = new Vector3(1.4f, 1.4f, 1f);
				attention2.Play();
				break;
			}
		}
		mWallPaperButton = Util.CreateJellyImageButton("Button0", parent, resImageCollection.Image);
		Util.SetImageButtonInfo(mWallPaperButton, "LC_button_collection00", "LC_button_collection00", "LC_button_collection00", mBaseDepth + 1, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mWallPaperButton, this, "OnVsualCollectionButtonPushed", UIButtonMessage.Trigger.OnClick);
		bool AttentionFlg = false;
		for (int i = 0; i < mGame.mVisualCollectionData.Count; i++)
		{
			for (int a = 0; a < 9; a++)
			{
				if (mGame.mPlayer.GetCollectionNotifyStatus(mGame.mVisualCollectionData[i].piceIds[a]) == Player.NOTIFICATION_STATUS.NOTIFY)
				{
					AttentionFlg = true;
					UISsSprite attention = Util.CreateGameObject("AttentionHalo", mWallPaperButton.gameObject).AddComponent<UISsSprite>();
					attention.Animation = ResourceManager.LoadSsAnimation("NEW_ATTENTION").SsAnime;
					attention.depth = mBaseDepth + 5;
					attention.transform.localPosition = new Vector3(-223f, 66f, 0f);
					attention.transform.localScale = new Vector3(1.4f, 1.4f, 1f);
					attention.Play();
					break;
				}
			}
			if (AttentionFlg)
			{
				break;
			}
		}
		mEpisodeButton = Util.CreateJellyImageButton("Button1", parent, resImageCollection.Image);
		Util.SetImageButtonInfo(mEpisodeButton, "LC_button_collection01", "LC_button_collection01", "LC_button_collection01", mBaseDepth + 1, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mEpisodeButton, this, "OnEpisodeCollectionButtonPushed", UIButtonMessage.Trigger.OnClick);
		mConsumeButton = Util.CreateJellyImageButton("Button2", parent, resImageCollection.Image);
		Util.SetImageButtonInfo(mConsumeButton, "LC_button_collection02", "LC_button_collection02", "LC_button_collection02", mBaseDepth + 1, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mConsumeButton, this, "OnConsumeCollectionButtonPushed", UIButtonMessage.Trigger.OnClick);
		yield return 0;
	}

	private IEnumerator CreatePage_CharacterSelect(int pageIndex)
	{
		CreatePage_CommonBackButton(pageIndex);
		UIFont font = GameMain.LoadFont();
		ResImage resImageHud = ResourceManager.LoadImage("HUD");
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		GameObject parent = mPageRoots[pageIndex];
		mCharacterSelectList = SMVerticalList.Make(parent, this, CharacterListInfo, mCharacterList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mCharacterSelectList.OnCreateItem = OnCreateItem_Character;
		yield return 0;
	}

	private IEnumerator CreatePage_CharacterView(int pageIndex, int selectCharIndex)
	{
		CreatePage_CommonBackButton(pageIndex);
		UIFont font = GameMain.LoadFont();
		ResImage resImageHud = ResourceManager.LoadImage("HUD");
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		GameObject parent = mPageRoots[pageIndex];
		mGame.StartLoading();
		mSelectedCharacterIndex = selectCharIndex;
		mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		mPartner.transform.localPosition = Vector3.zero;
		int level = mGame.mPlayer.GetCompanionLevel(selectCharIndex);
		mPartner.Init((short)selectCharIndex, Vector3.zero, 1f, level, parent, false, 0, true);
		while (!mPartner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
		mPartner.SetPartnerScreenEnable(true);
		int skill0id = mGame.mCompanionData[mSelectedCharacterIndex].skill0Id;
		int skill1id = mGame.mCompanionData[mSelectedCharacterIndex].skill1Id;
		int skill2id = mGame.mCompanionData[mSelectedCharacterIndex].skill2Id;
		List<ResSsAnimation> animList = new List<ResSsAnimation>();
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcText))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcText));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcA))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcA));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcB))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcB));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcC))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcC));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcD))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcD));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0id].efcBack))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill0id].efcBack));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcText))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcText));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcA))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcA));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcB))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcB));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcC))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcC));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcD))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcD));
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1id].efcBack))
		{
			animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill1id].efcBack));
		}
		if (skill2id != -1)
		{
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcText))
			{
				animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcText));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcA))
			{
				animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcA));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcB))
			{
				animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcB));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcC))
			{
				animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcC));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcD))
			{
				animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcD));
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2id].efcBack))
			{
				animList.Add(ResourceManager.LoadSsAnimationAsync(mGame.mSkillEffectData[skill2id].efcBack));
			}
		}
		while (true)
		{
			bool wait = false;
			foreach (ResSsAnimation res in animList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
		mRequestMotion = -1;
		mPartner.StartMotion(CompanionData.MOTION.STAY0, true);
		mSkillEffectPlaying = false;
		mWindow = Util.CreateSprite("Window", parent, resImageCollection.Image);
		Util.SetSpriteInfo(mWindow, "instruction_panel3", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		mWindow.type = UIBasicSprite.Type.Sliced;
		mWindow.SetDimensions(640, 348);
		float xpos2 = 210f;
		mMotionButton = Util.CreateJellyImageButton("ButtonMotion", mWindow.gameObject, resImageCollection.Image);
		Util.SetImageButtonInfo(mMotionButton, "LC_button_gesture", "LC_button_gesture", "LC_button_gesture", mBaseDepth + 2, new Vector3(xpos2, -204f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mMotionButton, this, "OnMotionPushed", UIButtonMessage.Trigger.OnClick);
		xpos2 -= 150f;
		mSkillButton = Util.CreateJellyImageButton("ButtonSkill", mWindow.gameObject, resImageCollection.Image);
		Util.SetImageButtonInfo(mSkillButton, "LC_button_deathblow", "LC_button_deathblow", "LC_button_deathblow", mBaseDepth + 2, new Vector3(xpos2, -204f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSkillButton, this, "OnSkillPushed", UIButtonMessage.Trigger.OnClick);
		mLineSprite = Util.CreateSprite("Line", parent, resImageHud.Image);
		Util.SetSpriteInfo(mLineSprite, "line00", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		mLineSprite.type = UIBasicSprite.Type.Sliced;
		mNameLabel = Util.CreateLabel("CharacterName", parent, font);
		Util.SetLabelInfo(mNameLabel, string.Empty, mBaseDepth + 4, Vector3.zero, 30, 0, 0, UIWidget.Pivot.Center);
		mNameLabel.color = Color.white;
		mNameLabel.effectStyle = UILabel.Effect.Shadow;
		mNameLabel.effectColor = Color.black;
		mNameLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		mNameLabel.SetDimensions(520, 30);
		mLevelSprite = Util.CreateSprite("Level", parent, resImageHud.Image);
		Util.SetSpriteInfo(mLevelSprite, string.Empty, mBaseDepth + 5, Vector3.zero, Vector3.one, false);
		for (int i = 0; i < 5; i++)
		{
			mLevelStars[i] = Util.CreateSprite("Star" + i, parent, resImageCollection.Image);
			Util.SetSpriteInfo(mLevelStars[i], string.Empty, mBaseDepth + 5, Vector3.zero, Vector3.one, false);
		}
		mPartner.MakeUIShadow(resImageCollection.Image, "character_foot", mBaseDepth + 1);
		mShadowLace = Util.CreateSprite("ShadowLace", parent, resImageCollection.Image);
		Util.SetSpriteInfo(mShadowLace, "characolle_back_lace02", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		mSeriesCheck = Util.CreateSprite("Check", parent, resImageCollection.Image);
		Util.SetSpriteInfo(mSeriesCheck, "button_character_check", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		ChangeCharacterInfoPage(0);
		mGame.FinishLoading();
		UIPanel panel = Util.CreatePanel("MaskPanel", parent, 99, 99);
		UISprite spr = Util.CreateSprite("CollisionMask", panel.gameObject, resImageHud.Image);
		Util.SetSpriteInfo(spr, "null", 0, Vector2.zero, Vector2.one, false);
		spr.SetDimensions(1384, 1384);
		mSkillEffectMask = spr.gameObject.AddComponent<BoxCollider>();
		spr.autoResizeBoxCollider = true;
		spr.ResizeCollider();
		mSkillEffectMask.enabled = false;
		yield return 0;
	}

	private void DeletePage_CharacterView()
	{
		UnityEngine.Object.Destroy(mPartner.gameObject);
		int skill0Id = mGame.mCompanionData[mSelectedCharacterIndex].skill0Id;
		int skill1Id = mGame.mCompanionData[mSelectedCharacterIndex].skill1Id;
		int skill2Id = mGame.mCompanionData[mSelectedCharacterIndex].skill2Id;
		List<ResSsAnimation> list = new List<ResSsAnimation>();
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0Id].efcText))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill0Id].efcText);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0Id].efcA))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill0Id].efcA);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0Id].efcB))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill0Id].efcB);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0Id].efcC))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill0Id].efcC);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0Id].efcD))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill0Id].efcD);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill0Id].efcBack))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill0Id].efcBack);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1Id].efcText))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill1Id].efcText);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1Id].efcA))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill1Id].efcA);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1Id].efcB))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill1Id].efcB);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1Id].efcC))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill1Id].efcC);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1Id].efcD))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill1Id].efcD);
		}
		if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill1Id].efcBack))
		{
			ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill1Id].efcBack);
		}
		if (skill2Id != -1)
		{
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2Id].efcText))
			{
				ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill2Id].efcText);
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2Id].efcA))
			{
				ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill2Id].efcA);
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2Id].efcB))
			{
				ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill2Id].efcB);
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2Id].efcC))
			{
				ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill2Id].efcC);
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2Id].efcD))
			{
				ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill2Id].efcD);
			}
			if (!string.IsNullOrEmpty(mGame.mSkillEffectData[skill2Id].efcBack))
			{
				ResourceManager.UnloadSsAnimation(mGame.mSkillEffectData[skill2Id].efcBack);
			}
		}
	}

	private IEnumerator CreatePage_WallpaperSelect(int pageIndex)
	{
		CreatePage_CommonBackButton(pageIndex);
		GameObject parent = mPageRoots[pageIndex];
		mWallPaperSelectList = SMVerticalList.Make(parent, this, WallPaperListInfo, mWallPaperList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mWallPaperSelectList.OnCreateItem = OnCreateItem_WallPaper;
		yield return 0;
	}

	private IEnumerator CreatePage_WallpaperView(int pageIndex, int selectCollectionIndex)
	{
		CreatePage_CommonBackButton(pageIndex);
		UIFont font = GameMain.LoadFont();
		ResImage resImageHud = ResourceManager.LoadImage("HUD");
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		GameObject parent = mPageRoots[pageIndex];
		Vector2 screenSize = Util.LogScreenSize();
		UISprite sprite3 = null;
		mWallPaperFrame = Util.CreateSprite("Frame", parent, resImageCollection.Image);
		Util.SetSpriteInfo(mWallPaperFrame, "collection_wallpaperframe", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		mWallPaperFrame.type = UIBasicSprite.Type.Sliced;
		mWallPaperFrame.SetDimensions(518, 850);
		mWallPaperFrame.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
		VisualCollectionData vcData = mGame.mVisualCollectionData[selectCollectionIndex];
		ResImage resPicesAtlas = ResourceManager.LoadImageAsync(vcData.atlasKey);
		while (resPicesAtlas.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		sprite3 = Util.CreateSprite("Visual", mWallPaperFrame.gameObject, resPicesAtlas.Image);
		Util.SetSpriteInfo(sprite3, vcData.imageKey, mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		bool isComplete = true;
		for (int i = 0; i < vcData.piceIds.Length; i++)
		{
			if (!mGame.mPlayer.IsAccessoryUnlock(vcData.piceIds[i]))
			{
				sprite3 = Util.CreateSprite("Pice" + i, mWallPaperFrame.gameObject, resImageCollection.Image);
				float posx = (float)(i % 3 * 144) - 144f;
				Util.SetSpriteInfo(position: new Vector3(posx, (float)(i / 3 * -256) + 256f, 0f), sprite: sprite3, imageName: "wallpaper_hide", depth: mBaseDepth + 4, scale: Vector3.one, flip: false);
				isComplete = false;
			}
		}
		if (isComplete ? true : false)
		{
			mDownloadButton = Util.CreateJellyImageButton("DownloadButton_" + selectCollectionIndex, parent, resImageCollection.Image);
			Util.SetImageButtonInfo(mDownloadButton, "LC_button_wallpaperSave", "LC_button_wallpaperSave", "LC_button_wallpaperSave", mBaseDepth + 4, Vector3.zero, new Vector3(0.8f, 0.8f, 1f));
			Util.AddImageButtonMessage(mDownloadButton, this, "OnDownloadPushed", UIButtonMessage.Trigger.OnClick);
			mDownloadButton.gameObject.GetComponent<JellyImageButton>().SetBaseScale(0.8f);
		}
		yield return 0;
	}

	private IEnumerator CreatePage_EpisodeSeries(int pageIndex)
	{
		CreatePage_CommonBackButton(pageIndex);
		UIFont font = GameMain.LoadFont();
		GameObject parent = mPageRoots[pageIndex];
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		float xpos3 = 210f;
		mSeriesMessageButton = Util.CreateJellyImageButton("Button_2", parent, resImageCollection.Image);
		Util.SetImageButtonInfo(mSeriesMessageButton, "LC_button_message", "LC_button_message", "LC_button_message", mBaseDepth + 2, new Vector3(xpos3, -514f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSeriesMessageButton, this, "OnSeriesCategoryPushed", UIButtonMessage.Trigger.OnClick);
		bool enable = false;
		foreach (StoryDemoData data in mGame.mStoryDemoData)
		{
			if (data.series != Def.SERIES.SM_MESSAGECARD || !mGame.mPlayer.IsStoryCompleted(data.index) || 1 == 0)
			{
				continue;
			}
			enable = true;
			break;
		}
		if (!enable)
		{
			mSeriesMessageButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		xpos3 -= 150f;
		mSeriesEventButton = Util.CreateJellyImageButton("Button_1", parent, resImageCollection.Image);
		Util.SetImageButtonInfo(mSeriesEventButton, "LC_button_event", "LC_button_event", "LC_button_event", mBaseDepth + 2, new Vector3(xpos3, -514f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSeriesEventButton, this, "OnSeriesCategoryPushed", UIButtonMessage.Trigger.OnClick);
		xpos3 -= 150f;
		mSeriesMapButton = Util.CreateJellyImageButton("Button_0", parent, resImageCollection.Image);
		Util.SetImageButtonInfo(mSeriesMapButton, "LC_button_map", "LC_button_map", "LC_button_map", mBaseDepth + 2, new Vector3(xpos3, -514f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSeriesMapButton, this, "OnSeriesCategoryPushed", UIButtonMessage.Trigger.OnClick);
		mSeriesCheck = Util.CreateSprite("Check", parent, resImageCollection.Image);
		Util.SetSpriteInfo(mSeriesCheck, "button_character_check", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		OnSelectSeriesSub(mSelectedSeriesCategory, parent);
		yield return 0;
	}

	public void OnSeriesCategoryPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.SELECT && go.GetComponent<JellyImageButton>().IsEnable())
		{
			string[] array = go.name.Split('_');
			byte result;
			if (array.Length >= 2 && byte.TryParse(array[1], out result) && mSelectedSeriesCategory != result)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				OnSelectSeriesSub(result, mPageRoots[mCurrentPage]);
			}
		}
	}

	private void OnSelectSeriesSub(byte newCategory, GameObject parent)
	{
		if (mEpisodeSeriesSelectList != null)
		{
			UnityEngine.Object.Destroy(mEpisodeSeriesSelectList.gameObject);
			mEpisodeSeriesSelectList = null;
		}
		if (mEpisodeSelectList != null)
		{
			UnityEngine.Object.Destroy(mEpisodeSelectList.gameObject);
			mEpisodeSelectList = null;
		}
		if (mSeriesEventDescLabel != null)
		{
			UnityEngine.Object.Destroy(mSeriesEventDescLabel.gameObject);
			mSeriesEventDescLabel = null;
		}
		UIFont atlasFont = GameMain.LoadFont();
		switch (newCategory)
		{
		case 0:
			mMode = MODE.EPISODE;
			mEpisodeSeriesSelectList = SMVerticalList.Make(parent, this, EpisodeSeriesListInfo, mEpisodeSeriesList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
			mEpisodeSeriesSelectList.OnCreateItem = OnCreateItem_EpisodeSeries;
			mSeriesCheck.transform.parent = mSeriesMapButton.transform;
			break;
		case 1:
			mMode = MODE.EPISODE;
			if (Util.IsWideScreen())
			{
				mEpisodeSeriesSelectList = SMVerticalList.Make(parent, this, EpisodeSeriesListInfo2, mEventSeriesList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
			}
			else
			{
				mEpisodeSeriesSelectList = SMVerticalList.Make(parent, this, EpisodeSeriesListInfo, mEventSeriesList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
			}
			mEpisodeSeriesSelectList.OnCreateItem = OnCreateItem_EpisodeSeries;
			mSeriesEventDescLabel = Util.CreateLabel("LabelEventDesc", parent, atlasFont);
			Util.SetLabelInfo(mSeriesEventDescLabel, Localization.Get("GameStateCollection_Episode_Info"), mBaseDepth + 2, Vector3.zero, 24, 0, 0, UIWidget.Pivot.Center);
			mSeriesEventDescLabel.color = new Color(2f / 15f, 8f / 85f, 52f / 85f);
			mSeriesCheck.transform.parent = mSeriesEventButton.transform;
			break;
		case 2:
		{
			mMode = MODE.EPISODE2;
			mEpisodeList.Clear();
			int num = 0;
			foreach (StoryDemoData mStoryDemoDatum in mGame.mStoryDemoData)
			{
				if (mStoryDemoDatum.series == Def.SERIES.SM_MESSAGECARD && (mGame.mPlayer.IsStoryCompleted(mStoryDemoDatum.index) ? true : false))
				{
					EpisodeSelectListItem episodeSelectListItem = new EpisodeSelectListItem();
					episodeSelectListItem.enable = true;
					episodeSelectListItem.index = num;
					episodeSelectListItem.data = mStoryDemoDatum;
					mEpisodeList.Add(episodeSelectListItem);
					num++;
				}
			}
			int num2 = (3 - mEpisodeList.Count % 3) % 3;
			for (int i = 0; i < num2; i++)
			{
				EpisodeSelectListItem item = new EpisodeSelectListItem();
				mEpisodeList.Add(item);
			}
			if (Util.IsWideScreen())
			{
				mEpisodeSelectList = SMVerticalList.Make(parent, this, MessageListInfo2, mEpisodeList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
			}
			else
			{
				mEpisodeSelectList = SMVerticalList.Make(parent, this, MessageListInfo, mEpisodeList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
			}
			mEpisodeSelectList.OnCreateItem = OnCreateItem_Episode;
			mSeriesEventDescLabel = Util.CreateLabel("LabelEventDesc", parent, atlasFont);
			Util.SetLabelInfo(mSeriesEventDescLabel, Localization.Get("GameStateCollection_Episode_Info_02"), mBaseDepth + 2, Vector3.zero, 23, 0, 0, UIWidget.Pivot.Center);
			mSeriesEventDescLabel.color = new Color(2f / 15f, 8f / 85f, 52f / 85f);
			mSeriesCheck.transform.parent = mSeriesMessageButton.transform;
			mSelectedEpisodeSeriesItem = null;
			break;
		}
		}
		mSeriesCheck.transform.localPosition = new Vector3(-60f, 0f, 0f);
		mSeriesCheck.transform.localScale = Vector3.one;
		mSelectedSeriesCategory = newCategory;
		StartCoroutine(CreateFinished());
	}

	private IEnumerator CreatePage_EpisodeSelect(int pageIndex, EpisodeSeriesSelectListItem seriesItem)
	{
		CreatePage_CommonBackButton(pageIndex);
		mEpisodeList.Clear();
		int index = 0;
		if (seriesItem.eventId != -1)
		{
			Dictionary<int, List<int>> eventDict = mGame.GetDemoRallyDemoList();
			foreach (KeyValuePair<int, List<int>> pair in eventDict)
			{
				if (pair.Key >= 200 || seriesItem.eventId != pair.Key)
				{
					continue;
				}
				foreach (int demoIndex in pair.Value)
				{
					EpisodeSelectListItem item2 = new EpisodeSelectListItem();
					if (mGame.mPlayer.IsStoryCompleted(mGame.mStoryDemoData[demoIndex].index))
					{
						item2.enable = true;
					}
					else
					{
						item2.enable = false;
					}
					item2.index = index;
					item2.data = mGame.mStoryDemoData[demoIndex];
					mEpisodeList.Add(item2);
					index++;
				}
			}
		}
		else
		{
			for (int i = 0; i < mGame.mStoryDemoData.Count; i++)
			{
				if ((Def.SERIES)seriesItem.series == mGame.mStoryDemoData[i].series)
				{
					EpisodeSelectListItem item = new EpisodeSelectListItem();
					if (mGame.mPlayer.IsStoryCompleted(mGame.mStoryDemoData[i].index))
					{
						item.enable = true;
					}
					else
					{
						item.enable = false;
					}
					item.index = index;
					item.data = mGame.mStoryDemoData[i];
					mEpisodeList.Add(item);
					index++;
				}
			}
		}
		UIFont font = GameMain.LoadFont();
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		GameObject parent = mPageRoots[pageIndex];
		mSeriesCurtain = Util.CreateSprite("Curtain", parent, resImageCollection.Image);
		Util.SetSpriteInfo(mSeriesCurtain, "curtain00", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		mSeriesCurtain.pivot = UIWidget.Pivot.Top;
		mSeriesCurtain.type = UIBasicSprite.Type.Tiled;
		mSeriesCurtain.SetDimensions(1138, 242);
		GameObject seriesParent = mSeriesCurtain.gameObject;
		Color shadowColor2 = Color.black;
		string iconImageKey;
		string seriesTitle;
		string ribbonKey;
		if (seriesItem.eventId != -1)
		{
			iconImageKey = string.Format("panel_event{0:D2}", seriesItem.eventId);
			seriesTitle = Localization.Get(string.Format("EVENT_{0:D2}_TITLE", seriesItem.eventId));
			ribbonKey = "ribbon_blue";
			shadowColor2 = new Color(0.11764706f, 11f / 85f, 0.40392157f, 1f);
		}
		else
		{
			iconImageKey = EpisodeSelectIcon[seriesItem.index];
			seriesTitle = Localization.Get(EpisodeSeriesTitle[seriesItem.index]);
			if (Def.IsGaidenSeries((Def.SERIES)seriesItem.series))
			{
				ribbonKey = "ribbon_purple";
				shadowColor2 = new Color(0.3254902f, 0.20392157f, 0.4745098f, 1f);
			}
			else
			{
				ribbonKey = "ribbon_red";
				shadowColor2 = new Color(0.5019608f, 11f / 85f, 14f / 51f, 1f);
			}
		}
		UISprite sprite3 = Util.CreateSprite("IconImage", seriesParent, resImageCollection.Image);
		Util.SetSpriteInfo(sprite3, iconImageKey, mBaseDepth + 3, new Vector3(-180f, -145f, 0f), Vector3.one, false);
		sprite3 = Util.CreateSprite("Ribbon", seriesParent, resImageCollection.Image);
		Util.SetSpriteInfo(sprite3, ribbonKey, mBaseDepth + 3, new Vector3(128f, -123f, 0f), Vector3.one, false);
		sprite3.type = UIBasicSprite.Type.Sliced;
		sprite3.SetDimensions(360, 40);
		UILabel label3 = Util.CreateLabel("SeriesTitle", sprite3.gameObject, font);
		Util.SetLabelInfo(label3, seriesTitle, mBaseDepth + 4, Vector3.zero, 30, 340, 0, UIWidget.Pivot.Center);
		label3.overflowMethod = UILabel.Overflow.ShrinkContent;
		label3.color = Color.white;
		label3.effectStyle = UILabel.Effect.Shadow;
		label3.effectColor = shadowColor2;
		label3.effectDistance = new Vector2(2f, 2f);
		label3.SetDimensions(314, 30);
		bool episodeComplete2 = true;
		if (mSelectedEpisodeSeriesItem.openEpisodeNum < mSelectedEpisodeSeriesItem.maxEpisodeNum || mSelectedEpisodeSeriesItem.maxEpisodeNum == 0)
		{
			episodeComplete2 = false;
		}
		string episodeNumFrameKey2 = string.Empty;
		episodeNumFrameKey2 = "panel_incomplete";
		Color numColor = Color.white;
		sprite3 = Util.CreateSprite("EpisodeNumFrame", seriesParent, resImageCollection.Image);
		Util.SetSpriteInfo(sprite3, episodeNumFrameKey2, mBaseDepth + 3, new Vector3(210f, -182f, 0f), Vector3.one, false);
		sprite3.type = UIBasicSprite.Type.Sliced;
		sprite3.SetDimensions(208, 43);
		label3 = Util.CreateLabel("EpisodeNum", sprite3.gameObject, font);
		Util.SetLabelInfo(label3, string.Empty + mSelectedEpisodeSeriesItem.openEpisodeNum + " / " + mSelectedEpisodeSeriesItem.maxEpisodeNum, mBaseDepth + 4, new Vector3(0f, -2f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		label3.color = numColor;
		label3 = Util.CreateLabel("CollectEpisode", seriesParent, font);
		Util.SetLabelInfo(label3, Localization.Get("GameStateCollection_CollectEpisode"), mBaseDepth + 4, new Vector3(28f, -184f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		label3.color = Color.white;
		label3.effectStyle = UILabel.Effect.Shadow;
		label3.effectColor = Color.black;
		label3.effectDistance = new Vector2(2f, 2f);
		label3.overflowMethod = UILabel.Overflow.ShrinkContent;
		label3.SetDimensions(150, 40);
		if (Util.IsWideScreen())
		{
			mEpisodeSelectList = SMVerticalList.Make(parent, this, EpisodeListInfo2, mEpisodeList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		}
		else
		{
			mEpisodeSelectList = SMVerticalList.Make(parent, this, EpisodeListInfo, mEpisodeList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		}
		mEpisodeSelectList.OnCreateItem = OnCreateItem_Episode;
		yield return 0;
	}

	private IEnumerator CreatePage_EpisodeView(int pageIndex)
	{
		yield return 0;
	}

	private IEnumerator CreatePage_ConsumeSelect(int pageIndex)
	{
		CreatePage_CommonBackButton(pageIndex);
		UIFont font = GameMain.LoadFont();
		ResImage resImageHud = ResourceManager.LoadImage("HUD");
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		GameObject parent = mPageRoots[pageIndex];
		mConsumeSelectList = SMVerticalList.Make(parent, this, ConsumeListInfo, mConsumeList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mConsumeSelectList.OnCreateItem = OnCreateItem_Consume;
		OnSelectConsumeSub(mSelectedConsumeCategory, parent);
		yield return 0;
	}

	private void OnSelectConsumeSub(byte newCategory, GameObject parent)
	{
		if (mConsumeSelectList != null)
		{
			UnityEngine.Object.Destroy(mConsumeSelectList.gameObject);
			mConsumeSelectList = null;
		}
		switch (newCategory)
		{
		case 0:
			mConsumeSelectList = SMVerticalList.Make(parent, this, ConsumeListInfo, mConsumeList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
			mConsumeSelectList.OnCreateItem = OnCreateItem_Consume;
			break;
		case 1:
			if (mGame.mMaintenanceProfile.IsAdvMaintenanceMode)
			{
				StartCoroutine(GrayOut());
				mConfirmDialog = Util.CreateGameObject("ErrorDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("KeyItem_GameCenter_Error_Title"), Localization.Get("KeyItem_GameCenter_Error_Desc00"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				mConfirmDialog.SetClosedCallback(OnConsumeDialogClosed);
				mState.Reset(STATE.WAIT, true);
				return;
			}
			mConsumeSelectList = SMVerticalList.Make(parent, this, ConsumeListInfo, mConsumeADVList, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
			mConsumeSelectList.OnCreateItem = OnCreateItem_Consume;
			break;
		}
		mSelectedConsumeCategory = newCategory;
		StartCoroutine(CreateFinished());
	}

	private IEnumerator CreateFinished()
	{
		yield return null;
		SetLayout(Util.ScreenOrientation);
	}

	private void OnConsumeDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		OnSelectConsumeSub(mSelectedConsumeCategory, mPageRoots[mCurrentPage]);
		mState.Change(STATE.SELECT);
	}

	private void SetLayout_Top(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mCharacterButton.transform.localPosition = new Vector3(0f, 300f, 0f);
			mWallPaperButton.transform.localPosition = new Vector3(0f, 100f, 0f);
			mEpisodeButton.transform.localPosition = new Vector3(0f, -100f, 0f);
			mConsumeButton.transform.localPosition = new Vector3(0f, -300f, 0f);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mCharacterButton.transform.localPosition = new Vector3(-260f, 90f, 0f);
			mWallPaperButton.transform.localPosition = new Vector3(260f, 90f, 0f);
			mEpisodeButton.transform.localPosition = new Vector3(-260f, -120f, 0f);
			mConsumeButton.transform.localPosition = new Vector3(260f, -120f, 0f);
			break;
		}
	}

	private void SetLayout_CharacterSelect(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mCharacterSelectList.OnScreenChanged(true);
			mCharacterSelectList.transform.localPosition = Vector3.zero;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mCharacterSelectList.OnScreenChanged(false);
			mCharacterSelectList.transform.localPosition = new Vector3(0f, -30f, 0f);
			break;
		}
	}

	private void SetLayout_CharacterView(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			if (!(mWindow != null))
			{
				break;
			}
			mWindow.transform.localPosition = new Vector3(0f, -280f, 0f);
			mLineSprite.transform.localPosition = new Vector3(0f, -50f, 0f);
			mLineSprite.SetDimensions(450, 6);
			mNameLabel.pivot = UIWidget.Pivot.Center;
			mNameLabel.transform.localPosition = new Vector3(0f, -34f, 0f);
			mLevelSprite.transform.localPosition = new Vector3(-130f, -78f, 0f);
			for (int j = 0; j < 5; j++)
			{
				if (mLevelStars[j] != null)
				{
					mLevelStars[j].transform.localPosition = new Vector3((float)j * 50f - 30f, -78f, 0f);
				}
			}
			mShadowLace.transform.localPosition = new Vector3(0f, 190f, 0f);
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			if (!(mWindow != null))
			{
				break;
			}
			mWindow.transform.localPosition = new Vector3(240f, -46f, 0f);
			mLineSprite.transform.localPosition = new Vector3(240f, 180f, 0f);
			mLineSprite.SetDimensions(600, 6);
			mNameLabel.pivot = UIWidget.Pivot.Left;
			mNameLabel.transform.localPosition = new Vector3(-20f, 196f, 0f);
			mLevelSprite.transform.localPosition = new Vector3(30f, 152f, 0f);
			for (int i = 0; i < 5; i++)
			{
				if (mLevelStars[i] != null)
				{
					mLevelStars[i].transform.localPosition = new Vector3((float)i * 50f + 280f, 152f, 0f);
				}
			}
			mShadowLace.transform.localPosition = new Vector3(-300f, -70f, 0f);
			break;
		}
		}
	}

	private void SetLayout_WallpaperSelect(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mWallPaperSelectList.OnScreenChanged(true);
			mWallPaperSelectList.transform.localPosition = Vector3.zero;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mWallPaperSelectList.OnScreenChanged(false);
			mWallPaperSelectList.transform.localPosition = new Vector3(0f, -32f, 0f);
			break;
		}
	}

	private void SetLayout_WallpaperView(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mWallPaperFrame != null)
			{
				mWallPaperFrame.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
				mWallPaperFrame.transform.localScale = Vector3.one;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mWallPaperFrame != null)
			{
				mWallPaperFrame.transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
				mWallPaperFrame.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
			}
			break;
		}
		if (mDownloadButton != null)
		{
			Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, o);
			float num = 33f;
			if (!Util.IsWideScreen())
			{
				num = 50f;
			}
			mDownloadButton.transform.localPosition = new Vector3(vector2.x, vector2.y + num, 0f);
		}
	}

	private void SetLayout_EpisodeSeries(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			if (mEpisodeSeriesSelectList != null)
			{
				mEpisodeSeriesSelectList.OnScreenChanged(true);
				mEpisodeSeriesSelectList.transform.localPosition = Vector3.zero;
			}
			if (mEpisodeSelectList != null)
			{
				mEpisodeSelectList.OnScreenChanged(true);
				mEpisodeSelectList.transform.localPosition = Vector3.zero;
			}
			float num2 = 210f;
			mSeriesMessageButton.transform.localPosition = new Vector3(num2, -514f, 0f);
			num2 -= 150f;
			mSeriesEventButton.transform.localPosition = new Vector3(num2, -514f, 0f);
			num2 -= 150f;
			mSeriesMapButton.transform.localPosition = new Vector3(num2, -514f, 0f);
			if (mSeriesEventDescLabel != null)
			{
				mSeriesEventDescLabel.transform.localPosition = new Vector3(0f, 460f, 0f);
			}
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			if (mEpisodeSeriesSelectList != null)
			{
				mEpisodeSeriesSelectList.OnScreenChanged(false);
				if (mSelectedSeriesCategory == 0)
				{
					mEpisodeSeriesSelectList.transform.localPosition = new Vector3(0f, -30f, 0f);
				}
				else
				{
					mEpisodeSeriesSelectList.transform.localPosition = new Vector3(0f, -52f, 0f);
				}
			}
			if (mEpisodeSelectList != null)
			{
				mEpisodeSelectList.OnScreenChanged(false);
				if (mSelectedSeriesCategory == 0)
				{
					mEpisodeSelectList.transform.localPosition = new Vector3(0f, -30f, 0f);
				}
				else
				{
					mEpisodeSelectList.transform.localPosition = new Vector3(0f, -52f, 0f);
				}
			}
			float num = 183f;
			mSeriesMessageButton.transform.localPosition = new Vector3(-456f, (0f - vector.y) / 2f + num, 0f);
			num += 80f;
			mSeriesEventButton.transform.localPosition = new Vector3(-456f, (0f - vector.y) / 2f + num, 0f);
			num += 80f;
			mSeriesMapButton.transform.localPosition = new Vector3(-456f, (0f - vector.y) / 2f + num, 0f);
			if (mSeriesEventDescLabel != null)
			{
				mSeriesEventDescLabel.transform.localPosition = new Vector3(0f, 216f, 0f);
			}
			break;
		}
		}
	}

	private void SetLayout_EpisodeSelect(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mEpisodeSelectList != null)
			{
				mEpisodeSelectList.OnScreenChanged(true);
				mEpisodeSelectList.transform.localPosition = new Vector3(0f, -60f, 0f);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mEpisodeSelectList != null)
			{
				mEpisodeSelectList.OnScreenChanged(false);
				mEpisodeSelectList.transform.localPosition = new Vector3(0f, -100f, 0f);
			}
			break;
		}
		mSeriesCurtain.SetDimensions((int)vector.x + 2, 242);
		Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, o);
		mSeriesCurtain.transform.localPosition = new Vector3(vector2.x, vector2.y + 7f, 0f);
	}

	private void SetLayout_EpisodeView(ScreenOrientation o)
	{
		switch (o)
		{
		}
	}

	private void SetLayout_ConsumeSelect(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mConsumeSelectList.OnScreenChanged(true);
			mConsumeSelectList.transform.localPosition = Vector3.zero;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mConsumeSelectList.OnScreenChanged(false);
			mConsumeSelectList.transform.localPosition = new Vector3(0f, -30f, 0f);
			break;
		}
	}

	public override void Start()
	{
		Server.GetWallPaperEvent += OnGetWallPaperURL;
		base.Start();
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}

	public override void Unload()
	{
		base.Unload();
		Server.GetWallPaperEvent -= OnGetWallPaperURL;
	}

	public void OnDestroy()
	{
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mState.Change(STATE.SELECT);
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.SELECT:
			if (mGame.mBackKeyTrg)
			{
				OnBackPushed(null);
			}
			break;
		}
		if (mPartner != null && !mSkillEffectPlaying)
		{
			mPartner.SetPartnerScreenPos(GetPartnerScreenBasePos(Util.ScreenOrientation));
		}
		mState.Update();
	}

	public override IEnumerator LoadGameState()
	{
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		mAnchorTop = Util.CreateAnchorWithChild("CollectionAnchorTop", camera.gameObject, UIAnchor.Side.Top, camera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("CollectionAnchorBottomLeft", camera.gameObject, UIAnchor.Side.BottomLeft, camera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("CollectionAnchorTopRight", camera.gameObject, UIAnchor.Side.TopRight, camera).gameObject;
		List<AccessoryData> companionAccessories = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
		List<CharacterSelectListItem> work = new List<CharacterSelectListItem>();
		for (int n = 0; n < mGame.mCompanionData.Count; n++)
		{
			CompanionData data = mGame.mCompanionData[n];
			if ((!GameMain.USE_DEBUG_DIALOG && mGame.mCompanionData[n].sortCategory == byte.MaxValue) || data.index == 33 || data.index == 38)
			{
				continue;
			}
			int index = data.index;
			if (data.IsExpanded())
			{
				continue;
			}
			if (data.IsExpandBase())
			{
				index = mGame.getExpandedCompanionID(data.index);
				if (index < 0)
				{
					index = data.index;
				}
			}
			CharacterSelectListItem item = new CharacterSelectListItem
			{
				data = mGame.mCompanionData[index],
				enable = mGame.mPlayer.IsCompanionUnlock(index)
			};
			for (int j2 = 0; j2 < companionAccessories.Count; j2++)
			{
				AccessoryData accessory = companionAccessories[j2];
				int GotID = int.Parse(accessory.GotID);
				if (GotID == index)
				{
					item.accessoryId = accessory.Index;
					if (mGame.mPlayer.GetCollectionNotifyStatus(accessory.Index) == Player.NOTIFICATION_STATUS.NOTIFY)
					{
						item.showAttention = true;
					}
					break;
				}
			}
			work.Add(item);
		}
		work.Sort(CharacterSelectListItem.CompareByCategoryAndOrder);
		byte category = byte.MaxValue;
		int padding2 = 0;
		while (work.Count > 0)
		{
			CharacterSelectListItem item2 = work[0];
			if (item2.data.sortCategory != category)
			{
				while (padding2 > 0)
				{
					CharacterSelectListItem temp = new CharacterSelectListItem();
					mCharacterList.Add(temp);
					padding2--;
				}
				category = item2.data.sortCategory;
				for (int i = 0; i < 4; i++)
				{
					CharacterSelectListItem temp2 = new CharacterSelectListItem();
					if (i == 0)
					{
						temp2.category = category;
					}
					mCharacterList.Add(temp2);
				}
			}
			mCharacterList.Add(item2);
			work.RemoveAt(0);
			padding2--;
			padding2 &= 3;
		}
		for (int m = 0; m < mGame.mVisualCollectionData.Count; m++)
		{
			int accessoryId = mGame.mVisualCollectionData[m].accessoryId;
			AccessoryData accessoryData = mGame.mAccessoryData[accessoryId];
			WallPaperSelectListItem item3 = new WallPaperSelectListItem
			{
				index = m
			};
			VisualCollectionData vcData = mGame.mVisualCollectionData[accessoryData.IsFlip];
			int Count = 0;
			for (int a = 0; a < vcData.piceIds.Length; a++)
			{
				if (mGame.mPlayer.IsAccessoryUnlock(vcData.piceIds[a]))
				{
					Count++;
				}
			}
			item3.collectNum = Count;
			item3.data = accessoryData;
			mWallPaperList.Add(item3);
		}
		int cnt2 = 0;
		for (Def.SERIES s = Def.SERIES.SM_FIRST; s <= Def.SeriesGaidenMax; s++)
		{
			if (s < Def.SERIES.SM_EV || s >= Def.SERIES.SM_GF00)
			{
				EpisodeSeriesSelectListItem item4 = new EpisodeSeriesSelectListItem
				{
					enable = mGame.mGameProfile.IsSeriesAvailable(s),
					index = cnt2,
					series = (short)s
				};
				mEpisodeSeriesList.Add(item4);
				cnt2++;
			}
		}
		for (int l = 0; l < mGame.mStoryDemoData.Count; l++)
		{
			int seriesIndex = (int)mGame.mStoryDemoData[l].series;
			if (seriesIndex >= 5 && seriesIndex < 10)
			{
				continue;
			}
			if (Def.IsGaidenSeries((Def.SERIES)seriesIndex))
			{
				seriesIndex -= 5;
			}
			if (seriesIndex < mEpisodeSeriesList.Count)
			{
				EpisodeSeriesSelectListItem seriesItem = (EpisodeSeriesSelectListItem)mEpisodeSeriesList[seriesIndex];
				seriesItem.maxEpisodeNum++;
				if (mGame.mPlayer.IsStoryCompleted(mGame.mStoryDemoData[l].index))
				{
					seriesItem.openEpisodeNum++;
				}
			}
		}
		Dictionary<int, List<int>> eventDict = mGame.GetDemoRallyDemoList();
		cnt2 = 0;
		foreach (KeyValuePair<int, List<int>> pair in eventDict)
		{
			if (pair.Key >= 200)
			{
				continue;
			}
			EpisodeSeriesSelectListItem item7 = new EpisodeSeriesSelectListItem
			{
				index = cnt2,
				series = 5,
				eventId = (short)pair.Key
			};
			mEventSeriesList.Add(item7);
			item7.maxEpisodeNum = (short)pair.Value.Count;
			item7.enable = false;
			foreach (int demoIndex in pair.Value)
			{
				if (mGame.mPlayer.IsStoryCompleted(mGame.mStoryDemoData[demoIndex].index))
				{
					item7.enable = true;
					item7.openEpisodeNum++;
				}
			}
			cnt2++;
		}
		Def.CONSUME_ID[] ID_TBL = new Def.CONSUME_ID[11]
		{
			Def.CONSUME_ID.GROWUP,
			Def.CONSUME_ID.GROWUP_PIECE,
			Def.CONSUME_ID.HEART_PIECE,
			Def.CONSUME_ID.ROADBLOCK_KEY,
			Def.CONSUME_ID.ROADBLOCK_KEY_R,
			Def.CONSUME_ID.ROADBLOCK_KEY_S,
			Def.CONSUME_ID.ROADBLOCK_KEY_SS,
			Def.CONSUME_ID.ROADBLOCK_KEY_STARS,
			Def.CONSUME_ID.ROADBLOCK_KEY_GF00,
			Def.CONSUME_ID.ROADBLOCK_TICKET,
			Def.CONSUME_ID.OLDEVENT_KEY
		};
		for (int k = 0; k < ID_TBL.Length; k++)
		{
			int num = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, (int)ID_TBL[k]);
			if (num >= 0)
			{
				ConsumeSelectListItem item5 = new ConsumeSelectListItem
				{
					adv_data = false,
					num = num,
					consume_id = ID_TBL[k]
				};
				mConsumeList.Add(item5);
			}
		}
		Def.ADV_CONSUME_ID[] ID_TBL_ADV = new Def.ADV_CONSUME_ID[3]
		{
			Def.ADV_CONSUME_ID.GASYA_TICKET_NORMAL,
			Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM,
			Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM_SUB
		};
		for (int j = 0; j < ID_TBL_ADV.Length; j++)
		{
			int num2 = 0;
			switch (ID_TBL_ADV[j])
			{
			case Def.ADV_CONSUME_ID.GASYA_TICKET_NORMAL:
				num2 = mGame.mPlayer.AdvSaveData.mGachaTicket_Normal;
				break;
			case Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM:
				num2 = mGame.mPlayer.AdvSaveData.mGachaTicket_Premium;
				break;
			case Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM_SUB:
				num2 = mGame.mPlayer.AdvSaveData.mGachaTicket_PremiumAuxiliary;
				break;
			}
			ConsumeSelectListItem item6 = new ConsumeSelectListItem
			{
				adv_data = true,
				num = num2,
				consume_adv_id = ID_TBL_ADV[j]
			};
			mConsumeADVList.Add(item6);
		}
		UIFont font = GameMain.LoadFont();
		ResImage resImageHud = ResourceManager.LoadImage("HUD");
		ResImage resImageCollection = ResourceManager.LoadImageAsync("COLLECTION");
		ResSsAnimation resSsAnime = ResourceManager.LoadSsAnimationAsync("NEW_ATTENTION");
		ResSound resSound = ResourceManager.LoadSoundAsync("BGM_COLLECTION");
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (resImageCollection.LoadState != ResourceInstance.LOADSTATE.DONE || resSsAnime.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mBgSprite = Util.CreateSprite("BackGround", mRoot, resImageCollection.Image);
		Util.SetSpriteInfo(mBgSprite, "characolle_back00", mBaseDepth, new Vector3(0f, 0f, 200f), Vector3.one, false);
		mBgSprite.type = UIBasicSprite.Type.Tiled;
		UISprite sprite2 = Util.CreateSprite("TitleBarR", mAnchorTop.gameObject, resImageHud.Image);
		Util.SetSpriteInfo(sprite2, "menubar_frame", mBaseDepth + 5, new Vector3(-2f, 173f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite2 = Util.CreateSprite("TitleBarL", mAnchorTop.gameObject, resImageHud.Image);
		Util.SetSpriteInfo(sprite2, "menubar_frame", mBaseDepth + 5, new Vector3(2f, 173f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBarLabel = Util.CreateLabel("TitleBarLabel", mAnchorTop.gameObject, font);
		Util.SetLabelInfo(mTitleBarLabel, Localization.Get("GameStateCollection_Title"), mBaseDepth + 7, new Vector3(0f, -26f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
		mTitleBarLabel.color = Color.white;
		mBackButton = Util.CreateJellyImageButton("ExitButton", mAnchorBottomLeft, resImageHud.Image);
		Util.SetImageButtonInfo(mBackButton, "button_exit", "button_exit", "button_exit", mBaseDepth + 3, new Vector3(64f, 64f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mBackButton, this, "OnBackPushed", UIButtonMessage.Trigger.OnClick);
		mLaceMove = true;
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, mBaseDepth + 1, new Vector3(30f, -110f, 0f), new Vector3(1f, 1f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, mBaseDepth + 1, new Vector3(-140f, -10f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(190f, -50f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(-20f, 60f, 0f), new Vector3(1f, 1f, 1f), 15f));
		mCurrentPage = 0;
		mScrollX = 0f;
		mSelectedSeriesCategory = 0;
		mSelectedConsumeCategory = 0;
		yield return StartCoroutine(CreatePage(PAGE.TOP, mCurrentPage, true));
		SetLayout(Util.ScreenOrientation);
		mGame.PlayMusic("BGM_COLLECTION", 0, true, false, true);
		mGame.SetMusicVolume(1f, 0);
		StartCoroutine(MotionController());
		LoadGameStateFinished();
	}

	private IEnumerator LaceRotate(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		ResImage resImageCollection = ResourceManager.LoadImage("COLLECTION");
		UISprite sprite = Util.CreateSprite("Lace", parent, resImageCollection.Image);
		Util.SetSpriteInfo(sprite, "characolle_back_lace02", depth, pos, scale, false);
		float angle = 0f;
		while (mLaceMove)
		{
			angle += Time.deltaTime * speed;
			sprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	private IEnumerator UpdateMenuAttention(UISprite MenuAttention)
	{
		float counter = 0f;
		float basePosY = MenuAttention.transform.localPosition.y;
		while (!(MenuAttention == null))
		{
			counter += Time.deltaTime * 360f;
			if (counter > 720f)
			{
				counter -= 720f;
			}
			Vector3 pos = MenuAttention.transform.localPosition;
			if (counter < 180f)
			{
				pos.y = basePosY + Mathf.Sin(counter * ((float)Math.PI / 180f)) * 30f;
			}
			else
			{
				pos.y = basePosY;
			}
			MenuAttention.transform.localPosition = pos;
			yield return null;
		}
	}

	public override IEnumerator UnloadGameState()
	{
		mLaceMove = false;
		ResourceManager.UnloadSound("BGM_COLLECTION");
		ResourceManager.UnloadImage("COLLECTION");
		ResourceManager.UnloadSsAnimation("NEW_ATTENTION");
		if ((bool)mAnchorTop)
		{
			UnityEngine.Object.Destroy(mAnchorTop.gameObject);
		}
		if ((bool)mAnchorBottomLeft)
		{
			UnityEngine.Object.Destroy(mAnchorBottomLeft.gameObject);
		}
		if ((bool)mAnchorTopRight)
		{
			UnityEngine.Object.Destroy(mAnchorTopRight.gameObject);
		}
		mGame.UnloadNotPreloadSoundResources(true);
		yield return 0;
		UnloadGameStateFinished();
		yield return 0;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		for (int i = 0; i < mPageRoots.Length; i++)
		{
			if (mPageRoots[i] != null)
			{
				switch (PageTbl[(int)mMode][i])
				{
				case PAGE.TOP:
					SetLayout_Top(o);
					break;
				case PAGE.CHARACTER_SELECT:
					SetLayout_CharacterSelect(o);
					break;
				case PAGE.CHARACTER_VIEW:
					SetLayout_CharacterView(o);
					break;
				case PAGE.WALLPAPER_SELECT:
					SetLayout_WallpaperSelect(o);
					break;
				case PAGE.WALLPAPER_VIEW:
					SetLayout_WallpaperView(o);
					break;
				case PAGE.EPISODE_SERIES:
					SetLayout_EpisodeSeries(o);
					break;
				case PAGE.EPISODE_SELECT:
					SetLayout_EpisodeSelect(o);
					break;
				case PAGE.EPISODE_VIEW:
					SetLayout_EpisodeView(o);
					break;
				case PAGE.CONSUME_SELECT:
					SetLayout_ConsumeSelect(o);
					break;
				}
			}
		}
		SetLayout_Common();
	}

	private void SetLayout_Common()
	{
		Vector2 vector = Util.LogScreenSize();
		float num = 55f;
		num = 70f;
		if (mBgSprite != null)
		{
			float num2 = vector.y / 1136f;
			mBgSprite.transform.localScale = new Vector3(num2, num2, 1f);
			mBgSprite.SetDimensions((int)(vector.x / num2), (int)(vector.y / num2));
		}
	}

	private Vector3 GetPartnerScreenBasePos(ScreenOrientation o)
	{
		Vector3 result = Vector3.zero;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			result = new Vector3(0f, 0f, -100f);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			result = new Vector3(-300f, -260f, -100f);
			break;
		}
		return result;
	}

	private void ChangeCharacterInfoPage(int page)
	{
		mCharacterInfoPage = page;
		if (mCharacterInfoPageRoot != null)
		{
			UnityEngine.Object.Destroy(mCharacterInfoPageRoot);
		}
		mCharacterInfoPageRoot = Util.CreateGameObject("LevelRoot", mWindow.gameObject);
		CompanionData companionData = mGame.mCompanionData[mSelectedCharacterIndex];
		int companionLevel = mGame.mPlayer.GetCompanionLevel(mSelectedCharacterIndex);
		UIFont atlasFont = GameMain.LoadFont();
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage("COLLECTION");
		switch (page)
		{
		case 0:
		{
			mSkillList.Clear();
			if (companionData.maxLevel >= CompanionData.SKILL00_LEVEL)
			{
				SkillSelectListItem skillSelectListItem = new SkillSelectListItem();
				if (companionLevel < CompanionData.SKILL00_LEVEL)
				{
					skillSelectListItem.enable = false;
				}
				skillSelectListItem.level = CompanionData.SKILL00_LEVEL;
				mSkillList.Add(skillSelectListItem);
			}
			if (companionData.maxLevel >= CompanionData.SKILL01_LEVEL)
			{
				SkillSelectListItem skillSelectListItem = new SkillSelectListItem();
				if (companionLevel < CompanionData.SKILL01_LEVEL)
				{
					skillSelectListItem.enable = false;
				}
				skillSelectListItem.level = CompanionData.SKILL01_LEVEL;
				mSkillList.Add(skillSelectListItem);
			}
			mSkillSelectList = SMVerticalList.Make(mCharacterInfoPageRoot, this, SkillListInfo, mSkillList, -10f, 80f, mBaseDepth + 3, true, UIScrollView.Movement.Vertical, false, true);
			mSkillSelectList.OnCreateItem = OnCreateItem_Skill;
			UISprite uISprite = Util.CreateSprite("Line", mCharacterInfoPageRoot, resImage2.Image);
			Util.SetSpriteInfo(uISprite, "line02", mBaseDepth + 2, new Vector3(0f, 2f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(576, 6);
			uISprite = Util.CreateSprite("MessageFrame", mCharacterInfoPageRoot, resImage.Image);
			Util.SetSpriteInfo(uISprite, "instruction_panel15", mBaseDepth + 2, new Vector3(0f, -84f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(534, 112);
			GameObject parent = uISprite.gameObject;
			uISprite = Util.CreateSprite("MessageFrameTitle", parent, resImage.Image);
			Util.SetSpriteInfo(uISprite, "instruction_accessory10", mBaseDepth + 3, new Vector3(0f, 52f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(518, 42);
			string empty = string.Empty;
			string empty2 = string.Empty;
			if (companionLevel >= CompanionData.SKILL01_LEVEL)
			{
				empty = companionData.GetSkillName(2);
				empty2 = "[403512]" + companionData.GetSkillDesc(2) + companionData.GetSkillSubDesc(2) + "[-]";
			}
			else if (companionLevel >= CompanionData.SKILL00_LEVEL)
			{
				empty = companionData.GetSkillName(1);
				empty2 = ((companionData.maxLevel < CompanionData.SKILL01_LEVEL) ? ("[403512]" + companionData.GetSkillDesc(1) + "[-]") : ("[403512]" + companionData.GetSkillDesc(1) + companionData.GetSkillSubDesc(1) + "[-]"));
			}
			else
			{
				empty = companionData.GetSkillName(1);
				empty2 = "[403512]" + companionData.GetSkillDesc(1) + "[-]\n[FF0000]" + Localization.Get("SkillInfoEmpty") + "[-]";
			}
			empty = empty.Replace("\n", string.Empty);
			mSkillNameLabel = Util.CreateLabel("SkillName", parent, atlasFont);
			Util.SetLabelInfo(mSkillNameLabel, empty, mBaseDepth + 4, new Vector3(0f, 53f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
			mSkillNameLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			mSkillNameLabel.SetDimensions(500, 24);
			mSkillNameLabel.color = Color.white;
			mSkillNameLabel.effectStyle = UILabel.Effect.Shadow;
			mSkillNameLabel.effectColor = Color.black;
			mSkillDescLabel = Util.CreateLabel("SkillDesc", parent, atlasFont);
			Util.SetLabelInfo(mSkillDescLabel, empty2, mBaseDepth + 4, new Vector3(0f, -6f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
			mSkillDescLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			mSkillDescLabel.useFloatSpacing = true;
			mSkillDescLabel.floatSpacingY = 3f;
			mSkillDescLabel.SetDimensions(510, 75);
			if (mPartner != null)
			{
				mPartner.StartMotion(CompanionData.MOTION.STAY0, true);
			}
			mSeriesCheck.transform.parent = mSkillButton.transform;
			break;
		}
		case 1:
		{
			mMotionList.Clear();
			if (companionData.motionType == 0)
			{
				if (companionData.maxLevel >= CompanionData.SKILL00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(0, Localization.Get("GameStateCharacter_Skill00"), CompanionData.MOTION.SKILL0, CompanionData.SKILL00_LEVEL, companionLevel >= CompanionData.SKILL00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.SKILL01_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(1, Localization.Get("GameStateCharacter_Skill01"), CompanionData.MOTION.SKILL1, CompanionData.SKILL01_LEVEL, companionLevel >= CompanionData.SKILL01_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WAIT00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(2, Localization.Get("GameStateCharacter_Wait00"), CompanionData.MOTION.STAY0, CompanionData.WAIT00_LEVEL, companionLevel >= CompanionData.WAIT00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WAIT01_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(3, Localization.Get("GameStateCharacter_Wait01"), CompanionData.MOTION.STAY1, CompanionData.WAIT01_LEVEL, companionLevel >= CompanionData.WAIT01_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HIT00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(4, Localization.Get("GameStateCharacter_Hit00"), CompanionData.MOTION.HIT0, CompanionData.HIT00_LEVEL, companionLevel >= CompanionData.HIT00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HIT01_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(5, Localization.Get("GameStateCharacter_Hit01"), CompanionData.MOTION.HIT1, CompanionData.HIT01_LEVEL, companionLevel >= CompanionData.HIT01_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HIT02_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(6, Localization.Get("GameStateCharacter_Hit02"), CompanionData.MOTION.HIT2, CompanionData.HIT02_LEVEL, companionLevel >= CompanionData.HIT02_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WIN00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(7, Localization.Get("GameStateCharacter_Win00"), CompanionData.MOTION.WIN, CompanionData.WIN00_LEVEL, companionLevel >= CompanionData.WIN00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.LOSE00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(8, Localization.Get("GameStateCharacter_Lose00"), CompanionData.MOTION.LOSE, CompanionData.LOSE00_LEVEL, companionLevel >= CompanionData.LOSE00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HURRY00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(9, Localization.Get("GameStateCharacter_Hurry00"), CompanionData.MOTION.HURRY, CompanionData.HURRY00_LEVEL, companionLevel >= CompanionData.HURRY00_LEVEL);
					mMotionList.Add(item);
				}
			}
			else if (companionData.motionType == 1)
			{
				if (companionData.maxLevel >= CompanionData.SKILL00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(0, Localization.Get("GameStateCharacter_Skill00"), CompanionData.MOTION.SKILL0, CompanionData.SKILL00_LEVEL, companionLevel >= CompanionData.SKILL00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WAIT00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(2, Localization.Get("GameStateCharacter_Wait00"), CompanionData.MOTION.STAY0, CompanionData.WAIT00_LEVEL, companionLevel >= CompanionData.WAIT00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WAIT01_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(3, Localization.Get("GameStateCharacter_Wait01"), CompanionData.MOTION.STAY1, CompanionData.WAIT01_LEVEL, companionLevel >= CompanionData.WAIT01_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HIT00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(4, Localization.Get("GameStateCharacter_Hit00"), CompanionData.MOTION.HIT0, CompanionData.HIT00_LEVEL, companionLevel >= CompanionData.HIT00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HIT02_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(6, Localization.Get("GameStateCharacter_Hit01"), CompanionData.MOTION.HIT2, CompanionData.HIT02_LEVEL, companionLevel >= CompanionData.HIT02_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WIN00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(7, Localization.Get("GameStateCharacter_Win00"), CompanionData.MOTION.WIN, CompanionData.WIN00_LEVEL, companionLevel >= CompanionData.WIN00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.LOSE00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(8, Localization.Get("GameStateCharacter_Lose00"), CompanionData.MOTION.LOSE, CompanionData.LOSE00_LEVEL, companionLevel >= CompanionData.LOSE00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HURRY00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(9, Localization.Get("GameStateCharacter_Hurry00"), CompanionData.MOTION.HURRY, CompanionData.HURRY00_LEVEL, companionLevel >= CompanionData.HURRY00_LEVEL);
					mMotionList.Add(item);
				}
			}
			else
			{
				if (companionData.maxLevel >= CompanionData.SKILL00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(0, Localization.Get("GameStateCharacter_Skill00"), CompanionData.MOTION.SKILL0, CompanionData.SKILL00_LEVEL, companionLevel >= CompanionData.SKILL00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.SKILL01_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(1, Localization.Get("GameStateCharacter_Skill01"), CompanionData.MOTION.SKILL1, CompanionData.SKILL01_LEVEL, companionLevel >= CompanionData.SKILL01_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WAIT00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(2, Localization.Get("GameStateCharacter_Wait00"), CompanionData.MOTION.STAY0, CompanionData.WAIT00_LEVEL, companionLevel >= CompanionData.WAIT00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WAIT01_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(3, Localization.Get("GameStateCharacter_Wait01"), CompanionData.MOTION.STAY1, CompanionData.WAIT01_LEVEL, companionLevel >= CompanionData.WAIT01_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HIT00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(4, Localization.Get("GameStateCharacter_Hit00"), CompanionData.MOTION.HIT0, CompanionData.HIT00_LEVEL, companionLevel >= CompanionData.HIT00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HIT02_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(6, Localization.Get("GameStateCharacter_Hit01"), CompanionData.MOTION.HIT1, CompanionData.HIT02_LEVEL, companionLevel >= CompanionData.HIT02_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.WIN00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(7, Localization.Get("GameStateCharacter_Win00"), CompanionData.MOTION.WIN, CompanionData.WIN00_LEVEL, companionLevel >= CompanionData.WIN00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.LOSE00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(8, Localization.Get("GameStateCharacter_Lose00"), CompanionData.MOTION.LOSE, CompanionData.LOSE00_LEVEL, companionLevel >= CompanionData.LOSE00_LEVEL);
					mMotionList.Add(item);
				}
				if (companionData.maxLevel >= CompanionData.HURRY00_LEVEL)
				{
					MotionSelectListItem item = new MotionSelectListItem(9, Localization.Get("GameStateCharacter_Hurry00"), CompanionData.MOTION.HURRY, CompanionData.HURRY00_LEVEL, companionLevel >= CompanionData.HURRY00_LEVEL);
					mMotionList.Add(item);
				}
			}
			mMotionSelectList = SMVerticalList.Make(mCharacterInfoPageRoot, this, MotionListInfo, mMotionList, -10f, -20f, mBaseDepth + 3, true, UIScrollView.Movement.Vertical);
			mMotionSelectList.OnCreateItem = OnCreateItem_Motion;
			UILabel uILabel = Util.CreateLabel("SkillDesc", mCharacterInfoPageRoot, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("GrowUp"), mBaseDepth + 4, new Vector3(0f, 132f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = new Color(0.4f, 16f / 51f, 0.47058824f);
			UISprite uISprite = Util.CreateSprite("Line", mCharacterInfoPageRoot, resImage2.Image);
			Util.SetSpriteInfo(uISprite, "line02", mBaseDepth + 2, new Vector3(0f, 112f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(576, 6);
			mSeriesCheck.transform.parent = mMotionButton.transform;
			break;
		}
		}
		Util.SetLabelText(mNameLabel, Util.MakeLText(companionData.name));
		string empty3 = string.Empty;
		Util.SetSpriteImageName(imageName: (companionLevel >= companionData.maxLevel) ? CharacterViewLevelMaxImageTbl[companionLevel] : CharacterViewLevelImageTbl[companionLevel], sprite: mLevelSprite, scale: Vector3.one, flip: false);
		for (int i = 0; i < 5; i++)
		{
			if (i < companionLevel)
			{
				Util.SetSpriteImageName(mLevelStars[i], "character_lv", Vector3.one, false);
			}
			else
			{
				Util.SetSpriteImageName(mLevelStars[i], "character_lv_black", Vector3.one, false);
			}
			if (i < companionData.maxLevel)
			{
				mLevelStars[i].gameObject.SetActive(true);
			}
			else
			{
				mLevelStars[i].gameObject.SetActive(false);
			}
		}
		mSeriesCheck.transform.localPosition = new Vector3(-60f, 0f, 0f);
		mSeriesCheck.transform.localScale = Vector3.one;
	}

	private IEnumerator MotionController()
	{
		while (true)
		{
			if (mRequestMotion == -1)
			{
				yield return 0;
				continue;
			}
			if (mPartner != null)
			{
				MotionSelectListItem motionItem = (MotionSelectListItem)mMotionList[mRequestMotion];
				if (motionItem.motionIndex == 0 || motionItem.motionIndex == 1)
				{
					if ((bool)mSkillEffectMask)
					{
						mSkillEffectMask.enabled = true;
					}
					mSkillPlaying = true;
					int rank = 1;
					if (motionItem.motionIndex == 0)
					{
						rank = 1;
					}
					else if (motionItem.motionIndex == 1)
					{
						rank = 2;
					}
					CompanionData data = mGame.mCompanionData[mSelectedCharacterIndex];
					if (data.IsHalloweenHaruka() || data.IsHalloweenMichiru() || data.IsHalloweenUsagi() || data.IsHalloweenHotaru() || data.IsHalloweenUsagi2())
					{
						rank = ((UnityEngine.Random.Range(0, 100) < 50) ? 1 : 2);
					}
					if (data.IsSailorChibiMoon())
					{
						int rnd = UnityEngine.Random.Range(0, 100);
						rank = ((rnd < 33) ? 1 : ((rnd >= 66) ? 3 : 2));
					}
					yield return StartCoroutine(StartSkillEffect(rank));
					mSkillPlaying = false;
					if ((bool)mSkillEffectMask)
					{
						mSkillEffectMask.enabled = false;
					}
				}
				else
				{
					mPartner.StartMotion(motionItem.motion, true);
				}
			}
			mRequestMotion = -1;
		}
	}

	private IEnumerator StartSkillEffect(int rank)
	{
		bool skillWait = true;
		GameObject skillParent = SkillEffectPlayer.CreateDemoCamera();
		mSkillEffectPlaying = true;
		SkillEffectPlayer skillEffect = Util.CreateGameObject("SkillManager", mRoot).AddComponent<SkillEffectPlayer>();
		skillEffect.Init(mPartner, rank, skillParent, SkillEffectPlayer.MODE.COLLECTION, delegate
		{
			skillWait = false;
		});
		while (skillWait)
		{
			yield return 0;
		}
		mSkillEffectPlaying = false;
		UnityEngine.Object.Destroy(skillEffect.gameObject);
		UnityEngine.Object.Destroy(skillParent);
	}

	private IEnumerator MoveUp(MODE nextMode, int param, float _delay)
	{
		mState.Reset(STATE.WAIT, true);
		int nextIndex = mCurrentPage + 1;
		PAGE page = PageTbl[(int)nextMode][nextIndex];
		PAGE oldPage = PageTbl[(int)nextMode][mCurrentPage];
		mMode = nextMode;
		yield return StartCoroutine(LoadPageResources(page, true));
		yield return StartCoroutine(CreatePage(page, nextIndex, true, param));
		if (_delay > 0f)
		{
			yield return new WaitForSeconds(_delay);
		}
		mBackButton.gameObject.SetActive(false);
		float angle = 0f;
		mScrollX = (float)mCurrentPage * -1384f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 180f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float xpos = mScrollX + Mathf.Sin(angle * ((float)Math.PI / 180f)) * -1384f;
			for (int pageNum = 0; pageNum < mPageRoots.Length; pageNum++)
			{
				if (mPageRoots[pageNum] != null)
				{
					mPageRoots[pageNum].transform.localPosition = new Vector3(xpos + 1384f * (float)pageNum, 0f, 0f);
				}
			}
			yield return 0;
		}
		mScrollX += 1384f;
		SetPageTitle(page);
		yield return StartCoroutine(DeletePage(oldPage, mCurrentPage, true));
		mCurrentPage = nextIndex;
		mBackButton.gameObject.SetActive(true);
		if (page == PAGE.TOP)
		{
			Util.SetImageButtonGraphic(mBackButton, "button_exit", "button_exit", "button_exit");
		}
		else
		{
			Util.SetImageButtonGraphic(mBackButton, "button_back", "button_back", "button_back");
		}
		if (page == PAGE.EPISODE_VIEW)
		{
			mGame.StopMusic(0, 0.2f);
			StartCoroutine(GrayOut(0.8f, Color.black, 1f));
			mStoryDemo = Util.CreateGameObject("StoryDemo", mPageRoots[mCurrentPage]).AddComponent<StoryDemoManager>();
			mStoryDemo.ChangeStoryDemo(mSelectedStoryDemoIndex, OnStoryDemoFinished);
		}
		mState.Reset(STATE.SELECT, true);
	}

	private IEnumerator MoveDown(float _delay)
	{
		mState.Reset(STATE.WAIT, true);
		int nextIndex = mCurrentPage - 1;
		PAGE page = PageTbl[(int)mMode][nextIndex];
		PAGE oldPage = PageTbl[(int)mMode][mCurrentPage];
		yield return StartCoroutine(LoadPageResources(page, false));
		yield return StartCoroutine(CreatePage(page, nextIndex, false));
		if (_delay > 0f)
		{
			yield return new WaitForSeconds(_delay);
		}
		mBackButton.gameObject.SetActive(false);
		float angle = 0f;
		mScrollX = (float)mCurrentPage * -1384f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 180f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float xpos = mScrollX + Mathf.Sin(angle * ((float)Math.PI / 180f)) * 1384f;
			for (int pageNum = 0; pageNum < mPageRoots.Length; pageNum++)
			{
				if (mPageRoots[pageNum] != null)
				{
					mPageRoots[pageNum].transform.localPosition = new Vector3(xpos + 1384f * (float)pageNum, 0f, 0f);
				}
			}
			yield return 0;
		}
		mScrollX -= 1384f;
		SetPageTitle(page);
		yield return StartCoroutine(DeletePage(oldPage, mCurrentPage, false));
		mCurrentPage = nextIndex;
		mBackButton.gameObject.SetActive(true);
		if (page == PAGE.TOP)
		{
			Util.SetImageButtonGraphic(mBackButton, "button_exit", "button_exit", "button_exit");
		}
		else
		{
			Util.SetImageButtonGraphic(mBackButton, "button_back", "button_back", "button_back");
		}
		if (oldPage == PAGE.EPISODE_VIEW)
		{
			mGame.PlayMusic("BGM_COLLECTION", 0, true, false, true);
		}
		mState.Change(STATE.SELECT);
	}

	private void OnCreateItem_Character(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("ICON");
		ResImage resImage2 = ResourceManager.LoadImage("HUD");
		UISprite uISprite = null;
		float cellWidth = mCharacterSelectList.mList.cellWidth;
		float cellHeight = mCharacterSelectList.mList.cellHeight;
		CharacterSelectListItem characterSelectListItem = (CharacterSelectListItem)item;
		CompanionData data = characterSelectListItem.data;
		uISprite = Util.CreateSprite("Back", itemGO, resImage2.Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		if (characterSelectListItem.enable)
		{
			string iconName = characterSelectListItem.data.iconName;
			UIButton uIButton = Util.CreateJellyImageButton("FaceIcon_" + index, itemGO, resImage.Image);
			Util.SetImageButtonInfo(uIButton, iconName, iconName, iconName, mBaseDepth + 2, Vector3.zero, Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnCharacterPushed", UIButtonMessage.Trigger.OnClick);
			uISprite = Util.CreateSprite("Level", uIButton.gameObject, resImage2.Image);
			string empty = string.Empty;
			int companionLevel = mGame.mPlayer.GetCompanionLevel(data.index);
			empty = ((companionLevel >= data.maxLevel) ? CharacterIconLevelMaxImageTbl[companionLevel] : CharacterIconLevelImageTbl[companionLevel]);
			Util.SetSpriteInfo(uISprite, empty, mBaseDepth + 3, new Vector3(0f, -56f, 0f), Vector3.one, false);
			UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView.scrollView = mCharacterSelectList.mListScrollView;
			if (characterSelectListItem.showAttention)
			{
				UISsSprite uISsSprite = Util.CreateGameObject("AttentionHalo", uISprite.gameObject).AddComponent<UISsSprite>();
				uISsSprite.Animation = ResourceManager.LoadSsAnimation("NEW_ATTENTION").SsAnime;
				uISsSprite.depth = mBaseDepth + 5;
				uISsSprite.transform.localPosition = new Vector3(-53f, 109f, 0f);
				uISsSprite.transform.localScale = new Vector3(1f, 1f, 1f);
				uISsSprite.Play();
			}
		}
		else if (characterSelectListItem.data != null)
		{
			UIButton uIButton = Util.CreateJellyImageButton("FaceIcon_" + index, itemGO, resImage.Image);
			Util.SetImageButtonInfo(uIButton, "icon_chara_question", "icon_chara_question", "icon_chara_question", mBaseDepth + 2, Vector3.zero, Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnNegativeSePushed", UIButtonMessage.Trigger.OnClick);
			UIDragScrollView uIDragScrollView2 = uIButton.gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView2.scrollView = mCharacterSelectList.mListScrollView;
		}
		else if (characterSelectListItem.category != byte.MaxValue)
		{
			UISprite uISprite2 = Util.CreateSprite("CategoryRibbon", itemGO, resImage.Image);
			Util.SetSpriteInfo(uISprite2, "iconchoice_ribbon", mBaseDepth + 2, new Vector3(218f, -20f, 0f), Vector3.one, false);
			uISprite2.type = UIBasicSprite.Type.Sliced;
			uISprite2.SetDimensions(400, 74);
			Vector3[] array = new Vector3[4]
			{
				new Vector3(-92f, 27f, 0f),
				new Vector3(91f, 27f, 0f),
				new Vector3(-92f, -23f, 0f),
				new Vector3(91f, -23f, 0f)
			};
			Vector3[] array2 = new Vector3[4]
			{
				Vector3.one,
				new Vector3(-1f, 1f, 1f),
				new Vector3(1f, -1f, 1f),
				new Vector3(-1f, -1f, 1f)
			};
			for (int i = 0; i < array.Length; i++)
			{
				uISprite = Util.CreateSprite("RibbonLine", uISprite2.gameObject, resImage.Image);
				Util.SetSpriteInfo(uISprite, "iconchoice_line", mBaseDepth + 3, array[i], Vector3.one, false);
				uISprite.type = UIBasicSprite.Type.Sliced;
				uISprite.SetDimensions(184, 12);
				uISprite.transform.localScale = array2[i];
			}
			UIFont atlasFont = GameMain.LoadFont();
			UILabel label = Util.CreateLabel("Category", uISprite2.gameObject, atlasFont);
			Util.SetLabelInfo(label, PartnerListItem.GetCategoryStr(characterSelectListItem.category), mBaseDepth + 4, new Vector3(0f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		}
	}

	private void OnCreateItem_Skill(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage("COLLECTION");
		UIFont atlasFont = GameMain.LoadFont();
		SkillSelectListItem skillSelectListItem = (SkillSelectListItem)item;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)mSkillSelectList.mList.cellWidth;
		uISprite.height = (int)mSkillSelectList.mList.cellHeight;
		uISprite = Util.CreateSprite("Frame", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 3, new Vector3(10f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(500, 50);
		UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 4, new Vector3(0f, -4f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		if (skillSelectListItem.enable)
		{
			uILabel.text = Localization.Get(LevelDescEnableTbl[skillSelectListItem.level]);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uISprite.color = Color.white;
		}
		else
		{
			uILabel.text = Localization.Get(LevelDescDisableTbl[skillSelectListItem.level]);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR / 2f;
			uISprite.color = Color.gray;
		}
		if (skillSelectListItem.enable)
		{
			uISprite = Util.CreateSprite("Label", itemGO, resImage2.Image);
			Util.SetSpriteInfo(uISprite, "chara_ribbon_mini", mBaseDepth + 4, new Vector3(-190f, 24f, 0f), Vector3.one, false);
			uISprite = Util.CreateSprite("Level", itemGO, resImage.Image);
			Util.SetSpriteInfo(uISprite, LevelImageEnableTbl[skillSelectListItem.level], mBaseDepth + 5, new Vector3(-190f, 24f, 0f), Vector3.one, false);
		}
		else
		{
			uISprite = Util.CreateSprite("Lock", itemGO, resImage2.Image);
			Util.SetSpriteInfo(uISprite, "key_gesture", mBaseDepth + 4, new Vector3(-230f, 0f, 0f), Vector3.one, false);
			uISprite = Util.CreateSprite("Level", itemGO, resImage.Image);
			Util.SetSpriteInfo(uISprite, LevelImageDisableTbl[skillSelectListItem.level], mBaseDepth + 5, new Vector3(-230f, 0f, 0f), Vector3.one, false);
		}
	}

	private void OnCreateItem_Motion(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage("COLLECTION");
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		MotionSelectListItem motionSelectListItem = (MotionSelectListItem)item;
		uISprite = Util.CreateSprite("Back", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)mMotionSelectList.mList.cellWidth;
		uISprite.height = (int)mMotionSelectList.mList.cellHeight;
		UIButton uIButton = Util.CreateJellyImageButton("Button_" + index, itemGO, resImage2.Image);
		Util.SetImageButtonInfo(uIButton, "button_animation", "button_animation", "button_animation", mBaseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnMotionButtonPushed", UIButtonMessage.Trigger.OnClick);
		UILabel uILabel = Util.CreateLabel("Desc", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, motionSelectListItem.name, mBaseDepth + 3, Vector3.zero, 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(170, 26);
		if (!motionSelectListItem.enable)
		{
			uIButton.disabledColor = Color.gray;
			uIButton.defaultColor = Color.gray;
			uIButton.hover = Color.gray;
			uIButton.pressed = Color.gray;
			uIButton.UpdateColor(true);
			uILabel.color = Color.gray;
			GameObject parent = uIButton.gameObject;
			uISprite = Util.CreateSprite("Lock", parent, resImage2.Image);
			Util.SetSpriteInfo(uISprite, "key_gesture", mBaseDepth + 4, new Vector3(-116f, 0f, 0f), Vector3.one, false);
			uISprite = Util.CreateSprite("Level", parent, resImage.Image);
			Util.SetSpriteInfo(uISprite, LevelImageDisableTbl[motionSelectListItem.level], mBaseDepth + 5, new Vector3(-116f, 0f, 0f), Vector3.one, false);
		}
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mMotionSelectList.mListScrollView;
	}

	private void OnCreateItem_WallPaper(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("WALL_PAPER_ICON");
		ResImage resImage2 = ResourceManager.LoadImage("HUD");
		ResImage resImage3 = ResourceManager.LoadImage("COLLECTION");
		UISprite uISprite = null;
		float cellWidth = mWallPaperSelectList.mList.cellWidth;
		float cellHeight = mWallPaperSelectList.mList.cellHeight;
		WallPaperSelectListItem wallPaperSelectListItem = (WallPaperSelectListItem)item;
		uISprite = Util.CreateSprite("Back", itemGO, resImage2.Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		string imageName = wallPaperSelectListItem.data.ImageName;
		UIButton uIButton;
		GameObject parent;
		if (wallPaperSelectListItem.collectNum == 0)
		{
			uIButton = Util.CreateJellyImageButton("WallPaper_" + wallPaperSelectListItem.index, itemGO, resImage.Image);
			Util.SetImageButtonInfo(uIButton, "thumbnail_question", "thumbnail_question", "thumbnail_question", mBaseDepth + 3, Vector3.zero, Vector3.one);
			parent = uIButton.gameObject;
			Util.AddImageButtonMessage(uIButton, this, "OnNegativeSePushed", UIButtonMessage.Trigger.OnClick);
		}
		else
		{
			uIButton = Util.CreateJellyImageButton("WallPaper_" + wallPaperSelectListItem.index, itemGO, resImage.Image);
			Util.SetImageButtonInfo(uIButton, imageName, imageName, imageName, mBaseDepth + 3, Vector3.zero, Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnWallPaperPushed", UIButtonMessage.Trigger.OnClick);
			parent = uIButton.gameObject;
			if (wallPaperSelectListItem.collectNum != 9)
			{
				uIButton.disabledColor = Color.gray;
				uIButton.defaultColor = Color.gray;
				uIButton.hover = Color.gray;
				uIButton.pressed = Color.gray;
				uIButton.UpdateColor(true);
				uISprite = Util.CreateSprite("Num", itemGO, resImage3.Image);
				Util.SetSpriteInfo(uISprite, NumImageNameTbl[wallPaperSelectListItem.collectNum], mBaseDepth + 4, new Vector3(-30f, 0f, 0f), Vector3.one, false);
				uISprite = Util.CreateSprite("Per", itemGO, resImage3.Image);
				Util.SetSpriteInfo(uISprite, "cllection_num_slash", mBaseDepth + 4, new Vector3(0f, 0f, 0f), Vector3.one, false);
				uISprite = Util.CreateSprite("Nine", itemGO, resImage3.Image);
				Util.SetSpriteInfo(uISprite, NumImageNameTbl[9], mBaseDepth + 4, new Vector3(30f, 0f, 0f), Vector3.one, false);
			}
			VisualCollectionData visualCollectionData = mGame.mVisualCollectionData[wallPaperSelectListItem.data.IsFlip];
			for (int i = 0; i < 9; i++)
			{
				if (mGame.mPlayer.GetCollectionNotifyStatus(visualCollectionData.piceIds[i]) == Player.NOTIFICATION_STATUS.NOTIFY)
				{
					UISsSprite uISsSprite = Util.CreateGameObject("AttentionHalo", uISprite.gameObject).AddComponent<UISsSprite>();
					uISsSprite.Animation = ResourceManager.LoadSsAnimation("NEW_ATTENTION").SsAnime;
					uISsSprite.depth = mBaseDepth + 5;
					uISsSprite.transform.localPosition = new Vector3(-68f, 119f, 0f);
					uISsSprite.transform.localScale = new Vector3(1.3f, 1.3f, 1f);
					uISsSprite.Play();
					break;
				}
			}
		}
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mWallPaperSelectList.mListScrollView;
		float num = 0.3164f;
		uISprite = Util.CreateSprite("Frame", parent, resImage3.Image);
		uISprite.type = UIBasicSprite.Type.Sliced;
		Util.SetSpriteInfo(uISprite, "collection_wallpaperframe", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		uISprite.SetDimensions((int)(165f / num), (int)(269f / num));
		uISprite.transform.localScale = new Vector3(num, num, 0f);
	}

	private void OnCreateItem_EpisodeSeries(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage("COLLECTION");
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		EpisodeSeriesSelectListItem episodeSeriesSelectListItem = (EpisodeSeriesSelectListItem)item;
		string text = "panel_season";
		if (Def.IsGaidenSeries((Def.SERIES)episodeSeriesSelectListItem.series))
		{
			text = "panel_seasonGF";
		}
		string imageName;
		string text2;
		GameObject parent;
		if (episodeSeriesSelectListItem.enable)
		{
			UIButton uIButton = Util.CreateJellyImageButton("EpisodeSeries_" + episodeSeriesSelectListItem.index, itemGO, resImage2.Image);
			Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 3, new Vector3(0f, 0f, -100f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnEpisodeSeriesPushed", UIButtonMessage.Trigger.OnClick);
			UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView.scrollView = mEpisodeSeriesSelectList.mListScrollView;
			if (episodeSeriesSelectListItem.eventId != -1)
			{
				imageName = string.Format("panel_event{0:D2}", episodeSeriesSelectListItem.eventId);
				text2 = Localization.Get(string.Format("EVENT_{0:D2}_TITLE", episodeSeriesSelectListItem.eventId));
			}
			else
			{
				imageName = EpisodeSeriesIcon[episodeSeriesSelectListItem.index];
				text2 = Localization.Get(EpisodeSeriesTitle[episodeSeriesSelectListItem.index]);
			}
			parent = uIButton.gameObject;
		}
		else
		{
			uISprite = Util.CreateSprite("Frame", itemGO, resImage2.Image);
			Util.SetSpriteInfo(uISprite, text, mBaseDepth + 1, Vector3.zero, Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			imageName = "panel_season_no";
			text2 = ((episodeSeriesSelectListItem.eventId == -1) ? Localization.Get("SeasonTitle_99") : Localization.Get("SeasonTitle_99b"));
			parent = uISprite.gameObject;
		}
		uISprite = Util.CreateSprite("IconImage", parent, resImage2.Image);
		Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 4, new Vector3(-138f, 0f, 0f), Vector3.one, false);
		string imageName2 = "ribbon_red";
		Color effectColor = new Color(0.5019608f, 11f / 85f, 14f / 51f, 1f);
		if (episodeSeriesSelectListItem.eventId != -1)
		{
			imageName2 = "ribbon_blue";
			effectColor = new Color(0.11764706f, 11f / 85f, 0.40392157f, 1f);
		}
		else if (Def.IsGaidenSeries((Def.SERIES)episodeSeriesSelectListItem.series))
		{
			imageName2 = "ribbon_purple";
			effectColor = new Color(0.3254902f, 0.20392157f, 0.4745098f, 1f);
		}
		uISprite = Util.CreateSprite("Ribbon", parent, resImage2.Image);
		Util.SetSpriteInfo(uISprite, imageName2, mBaseDepth + 5, new Vector3(72f, 43f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(344, 40);
		UILabel uILabel = Util.CreateLabel("SeriesTitle", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text2, mBaseDepth + 6, Vector3.zero, 30, 340, 0, UIWidget.Pivot.Center);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.color = Color.white;
		uILabel.effectStyle = UILabel.Effect.Shadow;
		uILabel.effectColor = effectColor;
		uILabel.effectDistance = new Vector2(2f, 2f);
		uILabel.SetDimensions(314, 30);
		string empty = string.Empty;
		empty = "panel_complete01";
		if (Def.IsGaidenSeries((Def.SERIES)episodeSeriesSelectListItem.series))
		{
			empty = "panel_complete02";
		}
		Color white = Color.white;
		uISprite = Util.CreateSprite("EpisodeNumFrame", parent, resImage2.Image);
		Util.SetSpriteInfo(uISprite, empty, mBaseDepth + 5, new Vector3(102f, -37f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(208, 43);
		uILabel = Util.CreateLabel("EpisodeNum", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty + episodeSeriesSelectListItem.openEpisodeNum + " / " + episodeSeriesSelectListItem.maxEpisodeNum, mBaseDepth + 6, new Vector3(0f, -2f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = white;
		uILabel = Util.CreateLabel("CollectEpisode", parent, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("GameStateCollection_CollectEpisode"), mBaseDepth + 6, new Vector3(102f, 2f, 0f), 21, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		uILabel.effectStyle = UILabel.Effect.Shadow;
		uILabel.effectColor = new Color(0.38431373f, 0.3019608f, 46f / 85f, 1f);
		uILabel.effectDistance = new Vector2(2f, 2f);
	}

	private void OnCreateItem_Episode(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = null;
		resImage = ((mSelectedEpisodeSeriesItem == null || mSelectedEpisodeSeriesItem.eventId == -1) ? ResourceManager.LoadImage("EPISODE_ICON") : ResourceManager.LoadImage("EVENT_EPISODE_" + mSelectedEpisodeSeriesItem.eventId));
		ResImage resImage2 = ResourceManager.LoadImage("HUD");
		ResImage resImage3 = ResourceManager.LoadImage("COLLECTION");
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		EpisodeSelectListItem episodeSelectListItem = (EpisodeSelectListItem)item;
		uISprite = Util.CreateSprite("Back", itemGO, resImage2.Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)mEpisodeSelectList.mList.cellWidth;
		uISprite.height = (int)mEpisodeSelectList.mList.cellHeight;
		if (episodeSelectListItem.data != null)
		{
			string text = episodeSelectListItem.data.iconName;
			string imageName = episodeSelectListItem.data.chapterImage;
			string text2 = Localization.Get(episodeSelectListItem.data.title);
			if (!episodeSelectListItem.enable)
			{
				text = "icon_question";
				imageName = "chapter_question";
				text2 = Localization.Get("StoryDemo_TitleUnknown");
			}
			UIButton uIButton = Util.CreateJellyImageButton("Episode_" + episodeSelectListItem.index, itemGO, resImage.Image);
			Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 3, new Vector3(0f, 12f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnEpisodePushed", UIButtonMessage.Trigger.OnClick);
			UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView.scrollView = mEpisodeSelectList.mListScrollView;
			uISprite = Util.CreateSprite("Frame", uIButton.gameObject, resImage3.Image);
			Util.SetSpriteInfo(uISprite, "collection_iconframe", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
			uISprite = Util.CreateSprite("Frame2", itemGO, resImage3.Image);
			Util.SetSpriteInfo(uISprite, "collection_iconframe2", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
			UISprite uISprite2 = uISprite;
			if (mSelectedSeriesCategory == 0)
			{
				uISprite = Util.CreateSprite("Chapter", itemGO, resImage3.Image);
				Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 3, new Vector3(0f, 94f, 0f), Vector3.one, false);
			}
			UILabel label = Util.CreateLabel("Title", itemGO, atlasFont);
			Util.SetLabelInfo(label, text2, mBaseDepth + 3, new Vector3(0f, -78f, 0f), 18, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabeShrinkContent(label, uISprite2.width - 10, 18);
		}
	}

	private void OnCreateItem_Consume(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage("COLLECTION");
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		UIButton uIButton = null;
		float cellWidth = mConsumeSelectList.mList.cellWidth;
		float cellHeight = mConsumeSelectList.mList.cellHeight;
		ConsumeSelectListItem consumeSelectListItem = (ConsumeSelectListItem)item;
		uISprite = Util.CreateSprite("Back", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		string imageName = string.Empty;
		string key = string.Empty;
		uIButton = Util.CreateJellyImageButton(string.Empty, itemGO, resImage2.Image);
		Util.SetImageButtonInfo(uIButton, "instruction_panel2", "instruction_panel2", "instruction_panel2", mBaseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnConsumeItemPushed", UIButtonMessage.Trigger.OnClick);
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mConsumeSelectList.mListScrollView;
		UISprite component = uIButton.GetComponent<UISprite>();
		if (component != null)
		{
			component.SetDimensions(525, 110);
			component.type = UIBasicSprite.Type.Sliced;
		}
		BoxCollider component2 = uIButton.GetComponent<BoxCollider>();
		if (uIButton != null)
		{
			component2.center = new Vector3(0f, 0f, 0f);
			component2.size = new Vector3(525f, 110f, 1f);
		}
		if (!consumeSelectListItem.adv_data)
		{
			uIButton.gameObject.name = string.Empty + consumeSelectListItem.consume_id;
			switch (consumeSelectListItem.consume_id)
			{
			case Def.CONSUME_ID.GROWUP:
				imageName = "icon_level";
				key = "ConsumeItemDesc_Growup";
				break;
			case Def.CONSUME_ID.GROWUP_PIECE:
				imageName = "icon_dailyEvent_stamp02";
				key = "ConsumeItemDesc_GrowupPiece";
				break;
			case Def.CONSUME_ID.HEART_PIECE:
				imageName = "icon_dailyEvent_stamp01";
				key = "ConsumeItemDesc_HeartPiece";
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY:
				imageName = "icon_loadblock_key";
				key = "ConsumeItemDesc_RoadBlockKey";
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_R:
				imageName = "icon_loadblock_key_R";
				key = "ConsumeItemDesc_RoadBlockKeyR";
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_S:
				imageName = "icon_loadblock_key_S";
				key = "ConsumeItemDesc_RoadBlockKeyS";
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_SS:
				imageName = "icon_loadblock_key_SS";
				key = "ConsumeItemDesc_RoadBlockKeySS";
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_STARS:
				imageName = "icon_loadblock_key_STARS";
				key = "ConsumeItemDesc_RoadBlockKeySTARS";
				break;
			case Def.CONSUME_ID.ROADBLOCK_KEY_GF00:
				imageName = "icon_loadblock_key_GF00";
				key = "ConsumeItemDesc_RoadBlockKeyGF00";
				break;
			case Def.CONSUME_ID.ROADBLOCK_TICKET:
				imageName = "icon_ticket";
				key = "ConsumeItemDesc_RoadBlockTicket";
				break;
			case Def.CONSUME_ID.OLDEVENT_KEY:
				imageName = "icon_old_event_key";
				key = "ConsumeItemDesc_OldEventKey";
				break;
			}
		}
		else
		{
			uIButton.gameObject.name = string.Empty + consumeSelectListItem.consume_adv_id;
			switch (consumeSelectListItem.consume_adv_id)
			{
			case Def.ADV_CONSUME_ID.GASYA_TICKET_NORMAL:
				imageName = "adv_item_tiket00";
				key = "AdvName_NORMAL_TICKET";
				break;
			case Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM:
				imageName = "adv_item_tiket01";
				key = "AdvName_PREMIUM_TICKET";
				break;
			case Def.ADV_CONSUME_ID.GASYA_TICKET_PREMIUM_SUB:
				imageName = "adv_item_tiket02";
				key = "AdvName_PREMIUM_SUB_TICKET";
				break;
			}
		}
		uISprite = Util.CreateSprite("Icon", uIButton.gameObject, resImage.Image);
		Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 3, new Vector3(-190f, 0f, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("Name", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get(key), mBaseDepth + 3, new Vector3(-140f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		uILabel.maxLineCount = 1;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(280, 52);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("X", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Xtext"), mBaseDepth + 3, new Vector3(150f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("Num", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty + consumeSelectListItem.num, mBaseDepth + 3, new Vector3(200f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
	}

	private void OnCharacterCollectionButtonPushed()
	{
		_OnTopPageButtonPushed(MODE.CHARACTER);
	}

	private void OnVsualCollectionButtonPushed()
	{
		_OnTopPageButtonPushed(MODE.WALLPAPER);
	}

	private void OnEpisodeCollectionButtonPushed()
	{
		_OnTopPageButtonPushed(MODE.EPISODE);
	}

	private void OnConsumeCollectionButtonPushed()
	{
		_OnTopPageButtonPushed(MODE.CONSUME);
	}

	private void _OnTopPageButtonPushed(MODE mode)
	{
		if (mState.GetStatus() == STATE.SELECT)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			StartCoroutine(MoveUp(mode, 0, 0.1f));
			mState.Reset(STATE.WAIT, true);
		}
	}

	private void OnConsumeItemPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.SELECT)
		{
			StartCoroutine(GrayOut());
			string text = go.name;
			string image = string.Empty;
			string key = string.Empty;
			string key2 = string.Empty;
			switch (text)
			{
			case "GROWUP":
				image = "icon_level";
				key = "KeyItemInfo_LvUP_Title";
				key2 = "KeyItemInfo_LvUP_Desc";
				break;
			case "GROWUP_PIECE":
				image = "icon_dailyEvent_stamp02";
				key = "KeyItemInfo_LvStamp_Title";
				key2 = "KeyItemInfo_LvStamp_Desc";
				break;
			case "HEART_PIECE":
				image = "icon_dailyEvent_stamp01";
				key = "KeyItemInfo_HeartStamp_Title";
				key2 = "KeyItemInfo_HeartStamp_Desc";
				break;
			case "ROADBLOCK_KEY":
				image = "icon_loadblock_key";
				key = "KeyItemInfo_RibbonKey_Title";
				key2 = "KeyItemInfo_RibbonKey_Desc";
				break;
			case "ROADBLOCK_TICKET":
				image = "icon_ticket";
				key = "KeyItemInfo_RibbonTicket_Title";
				key2 = "KeyItemInfo_RibbonTicket_Desc";
				break;
			case "ROADBLOCK_KEY_R":
				image = "icon_loadblock_key_R";
				key = "KeyItemInfo_RibbonKey_Title";
				key2 = "KeyItemInfo_RibbonKey_Desc_01";
				break;
			case "ROADBLOCK_KEY_S":
				image = "icon_loadblock_key_S";
				key = "KeyItemInfo_RibbonKey_Title";
				key2 = "KeyItemInfo_RibbonKey_Desc_02";
				break;
			case "ROADBLOCK_KEY_SS":
				image = "icon_loadblock_key_SS";
				key = "KeyItemInfo_RibbonKey_Title";
				key2 = "KeyItemInfo_RibbonKey_Desc_03";
				break;
			case "ROADBLOCK_KEY_STARS":
				image = "icon_loadblock_key_STARS";
				key = "KeyItemInfo_RibbonKey_Title";
				key2 = "KeyItemInfo_RibbonKey_Desc_04";
				break;
			case "ROADBLOCK_KEY_GF00":
				image = "icon_loadblock_key_GF00";
				key = "KeyItemInfo_RibbonKey_Title";
				key2 = "KeyItemInfo_RibbonKey_Desc_10";
				break;
			case "OLDEVENT_KEY":
				image = "icon_old_event_key";
				key = "ConsumeItemDesc_OldEventKey";
				key2 = "KeyItemInfo_OldEventKey_Desc";
				break;
			case "GASYA_TICKET_NORMAL":
				image = "adv_item_tiket00";
				key = "ConsumeItemDesc_N_Ticket";
				key2 = "KeyItemInfo_N_Ticket_Desc";
				break;
			case "GASYA_TICKET_PREMIUM":
				image = "adv_item_tiket01";
				key = "ConsumeItemDesc_P_Ticket";
				key2 = "KeyItemInfo_P_Ticket_Desc";
				break;
			case "GASYA_TICKET_PREMIUM_SUB":
				image = "adv_item_tiket02";
				key = "ConsumeItemDesc_P_Support";
				key2 = "KeyItemInfo_P_Support_Desc";
				break;
			}
			mConfirmImageDialog = Util.CreateGameObject("ConsumeDialog", mRoot).AddComponent<ConfirmImageDialog>();
			mConfirmImageDialog.ConsumeInit(Localization.Get(key), Localization.Get(key2), image, ConfirmImageDialog.CONFIRM_IMAGE_DIALOG_STYLE.CONSUME, DialogBase.DIALOG_SIZE.LARGE, 10);
			mConfirmImageDialog.SetClosedCallback(OnConsumeItemClosed);
			mState.Reset(STATE.WAIT, true);
		}
	}

	private void OnConsumeItemClosed(ConfirmImageDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.SELECT);
	}

	private void OnBackPushed(GameObject go)
	{
		if (mState.GetStatus() != STATE.SELECT)
		{
			return;
		}
		if (mCurrentPage == 0)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mGame.StopMusic(0, 0.2f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
			mState.Change(STATE.UNLOAD_WAIT);
		}
		else if (mStoryDemo != null)
		{
			mStoryDemo.OnSkipPushed();
		}
		else if (!mSkillPlaying && (mMode != 0 || mRequestMotion == -1))
		{
			if (!mSeNegativeCalled)
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
			mSeNegativeCalled = false;
			StartCoroutine(MoveDown(0.1f));
		}
	}

	private void OnNegativeSePushed(GameObject go)
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
	}

	private void OnCharacterPushed(GameObject go)
	{
		if (mState.GetStatus() != STATE.SELECT)
		{
			return;
		}
		string[] array = go.name.Split('_');
		int index = int.Parse(array[1]);
		CharacterSelectListItem characterSelectListItem = (CharacterSelectListItem)mCharacterList[index];
		if (mGame.mPlayer.GetCollectionNotifyStatus(characterSelectListItem.accessoryId) == Player.NOTIFICATION_STATUS.NOTIFY)
		{
			mGame.mPlayer.CollectionNotified(characterSelectListItem.accessoryId);
			if (characterSelectListItem.data.IsExpanded())
			{
				List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
				CompanionData companionData = characterSelectListItem.data;
				while (companionData.backCharacterIndex != -1)
				{
					for (int i = 0; i < accessoryByType.Count; i++)
					{
						AccessoryData accessoryData = accessoryByType[i];
						int num = int.Parse(accessoryData.GotID);
						if (num == companionData.backCharacterIndex && mGame.mPlayer.IsCompanionUnlock(num))
						{
							if (mGame.mPlayer.GetCollectionNotifyStatus(accessoryData.Index) == Player.NOTIFICATION_STATUS.NOTIFY)
							{
								mGame.mPlayer.CollectionNotified(accessoryData.Index);
							}
							break;
						}
					}
					companionData = mGame.mCompanionData[companionData.backCharacterIndex];
				}
			}
			mGame.Save();
			characterSelectListItem.showAttention = false;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		StartCoroutine(MoveUp(mMode, characterSelectListItem.data.index, 0f));
		mState.Reset(STATE.WAIT, true);
	}

	private void OnMotionButtonPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.SELECT)
		{
			string[] array = go.name.Split('_');
			int index = int.Parse(array[1]);
			MotionSelectListItem motionSelectListItem = (MotionSelectListItem)mMotionList[index];
			if (motionSelectListItem.enable)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				mRequestMotion = index;
			}
		}
	}

	private void OnSkillPushed()
	{
		if (mCharacterInfoPage != 0 && !mSkillPlaying)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			ChangeCharacterInfoPage(0);
		}
	}

	private void OnMotionPushed()
	{
		if (mCharacterInfoPage != 1 && !mSkillPlaying)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			ChangeCharacterInfoPage(1);
		}
	}

	public void OnWallPaperPushed(GameObject go)
	{
		if (mState.GetStatus() != STATE.SELECT)
		{
			return;
		}
		string[] array = go.name.Split('_');
		int num = int.Parse(array[1]);
		VisualCollectionData visualCollectionData = mGame.mVisualCollectionData[num];
		bool flag = false;
		for (int i = 0; i < 9; i++)
		{
			if (mGame.mPlayer.GetCollectionNotifyStatus(visualCollectionData.piceIds[i]) == Player.NOTIFICATION_STATUS.NOTIFY)
			{
				mGame.mPlayer.CollectionNotified(visualCollectionData.piceIds[i]);
				flag = true;
			}
		}
		if (flag)
		{
			mGame.Save();
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		StartCoroutine(MoveUp(mMode, num, 0f));
	}

	public void OnDownloadPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.SELECT)
		{
			string[] array = go.name.Split('_');
			int paperIndex = int.Parse(array[1]);
			mGame.PlaySe("SE_POSITIVE", -1);
			StartCoroutine(WallPaperDownload(paperIndex));
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnEpisodeSeriesPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.SELECT)
		{
			string[] array = go.name.Split('_');
			int num = int.Parse(array[1]);
			if (mSelectedSeriesCategory == 0)
			{
				mSelectedEpisodeSeriesItem = (EpisodeSeriesSelectListItem)mEpisodeSeriesList[num];
			}
			else
			{
				mSelectedEpisodeSeriesItem = (EpisodeSeriesSelectListItem)mEventSeriesList[num];
			}
			mGame.PlaySe("SE_POSITIVE", -1);
			StartCoroutine(MoveUp(mMode, num, 0f));
		}
	}

	public void OnEpisodePushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.SELECT)
		{
			string[] array = go.name.Split('_');
			int num = int.Parse(array[1]);
			mSelectedStoryDemoIndex = ((EpisodeSelectListItem)mEpisodeList[num]).data.index;
			if (((EpisodeSelectListItem)mEpisodeList[num]).enable)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				StartCoroutine(MoveUp(mMode, num, 0f));
			}
			else
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
		}
	}

	private IEnumerator WallPaperDownload(int paperIndex)
	{
		float TIMEOUT = 60f;
		mWallPaperURL = null;
		mGetWallPaperDone = false;
		mSaveWallPaperError = null;
		mSaveWallPaperDone = false;
		Server.ManualConnecting = true;
		float _time2 = Time.realtimeSinceStartup;
		string imageNum = string.Format("{0:000}", paperIndex);
		if (!Server.GetWallPaper(imageNum))
		{
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(1));
			mState.Reset(STATE.SELECT, true);
			yield break;
		}
		while (Time.realtimeSinceStartup - _time2 < TIMEOUT && !mGetWallPaperDone)
		{
			yield return new WaitForSeconds(0.1f);
		}
		if (!mGetWallPaperDone || string.IsNullOrEmpty(mWallPaperURL))
		{
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(1));
			mState.Reset(STATE.SELECT, true);
			yield break;
		}
		yield return 0;
		int seqNo = 0;
		string tmpPath2 = null;
		do
		{
			tmpPath2 = Path.Combine(path2: string.Format("sailormoondrops_{0}_{1}.png", DateTime.Now.ToString("yyyyMMddHHmmss"), seqNo.ToString("00")), path1: Path.Combine(BIJUnity.getTempPath(), "wp"));
			seqNo++;
			yield return 0;
		}
		while (File.Exists(tmpPath2) && seqNo < 100);
		if (string.IsNullOrEmpty(tmpPath2) || File.Exists(tmpPath2))
		{
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(1));
			Resources.UnloadUnusedAssets();
			mState.Reset(STATE.SELECT, true);
			yield break;
		}
		WWW www = new WWW(mWallPaperURL);
		yield return StartCoroutine(GetWallPaperWWW(www, TIMEOUT));
		if (www.error != null || !www.isDone || www.texture == null)
		{
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(1));
			Resources.UnloadUnusedAssets();
			mState.Reset(STATE.SELECT, true);
			yield break;
		}
		byte[] wallpaper = www.texture.EncodeToPNG();
		www.Dispose();
		if (!SaveImageToPath(wallpaper, tmpPath2) || !File.Exists(tmpPath2))
		{
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(2));
			Resources.UnloadUnusedAssets();
			mState.Reset(STATE.SELECT, true);
			yield break;
		}
		yield return 0;
		_time2 = Time.realtimeSinceStartup;
		if (!BIJUnity.writeToPhotoAlbumAsync(tmpPath2, base.gameObject, "OnDownloadWallPaperComplete"))
		{
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(2));
			Resources.UnloadUnusedAssets();
			mState.Reset(STATE.SELECT, true);
			yield break;
		}
		while (Time.realtimeSinceStartup - _time2 < TIMEOUT && !mSaveWallPaperDone)
		{
			yield return new WaitForSeconds(0.1f);
		}
		if (!mSaveWallPaperDone || !string.IsNullOrEmpty(mSaveWallPaperError))
		{
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(2));
			Resources.UnloadUnusedAssets();
			mState.Reset(STATE.SELECT, true);
		}
		else
		{
			yield return 0;
			Server.ManualConnecting = false;
			yield return StartCoroutine(ShowWallPaperError(0));
			Resources.UnloadUnusedAssets();
			mState.Reset(STATE.SELECT, true);
		}
	}

	private IEnumerator ShowWallPaperError(int _error)
	{
		yield return 0;
		string titleKey = "SaveVC_Title";
		string descKey2 = "SaveVC_Success_Desc";
		switch (_error)
		{
		case 0:
			descKey2 = "SaveVC_Success_Desc";
			break;
		case 1:
			descKey2 = "SaveVC_NetworkError_Desc";
			break;
		default:
			descKey2 = "SaveVC_CapacityError_Desc";
			break;
		}
		bool dialogClosed = false;
		ConfirmDialog dlg = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		dlg.Init(Localization.Get(titleKey), Localization.Get(descKey2), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, DialogBase.DIALOG_SIZE.MEDIUM, string.Empty, string.Empty);
		dlg.SetClosedCallback(delegate
		{
			dialogClosed = true;
		});
		while (!dialogClosed)
		{
			yield return 0;
		}
	}

	private void OnGetWallPaperURL(int result, int code, string url)
	{
		if (result == 0 && code == 0)
		{
			mWallPaperURL = url;
		}
		else
		{
			mWallPaperURL = null;
		}
		mGetWallPaperDone = true;
	}

	private IEnumerator GetWallPaperWWW(WWW www, float timeout)
	{
		float requestTime = Time.realtimeSinceStartup;
		while (!www.isDone && Time.realtimeSinceStartup - requestTime < timeout)
		{
			yield return null;
		}
		yield return null;
	}

	private bool SaveImageToPath(byte[] imageData, string path)
	{
		try
		{
			if (imageData == null || string.IsNullOrEmpty(path))
			{
				return false;
			}
			using (BIJBinaryWriter bIJBinaryWriter = new BIJFileBinaryWriter(path, Encoding.UTF8, false, true))
			{
				bIJBinaryWriter.WriteRaw(imageData, 0, imageData.Length);
				bIJBinaryWriter.Flush();
			}
		}
		catch (Exception)
		{
			return false;
		}
		return true;
	}

	private void OnDownloadWallPaperComplete(string error)
	{
		mSaveWallPaperError = error;
		mSaveWallPaperDone = true;
	}

	private void OnStoryDemoFinished(int storyDemoIndex)
	{
		mStoryDemo = null;
		mSeNegativeCalled = true;
		StartCoroutine(GrayIn());
		OnBackPushed(null);
	}
}
