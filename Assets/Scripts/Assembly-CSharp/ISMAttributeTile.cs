public interface ISMAttributeTile : ISMTile
{
	ISMAttributeTile Next { get; }
}
