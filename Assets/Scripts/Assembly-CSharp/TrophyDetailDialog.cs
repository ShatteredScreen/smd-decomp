using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrophyDetailDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		BUY = 0,
		CANCEL = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(TrophyItemListItem ItemData, SELECT_ITEM item);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private Def.BOOSTER_KIND mBoosterKind;

	private TrophyItemListItem mItemData;

	private UILabel mLabel;

	private ResImage mResShopImage;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		mResShopImage = ResourceManager.LoadImageAsync("SHOP");
		while (mResShopImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = mResShopImage.Image;
		BIJImage image3 = ResourceManager.LoadImage("TROPHY").Image;
		BIJImage image4 = ResourceManager.LoadImage("DIALOG_BASE").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int num = (int)mBoosterKind;
		string text = string.Format(Localization.Get(mItemData.data.Name));
		InitDialogFrame(DIALOG_SIZE.LARGE, "DIALOG_BASE", "instruction_panel11");
		mTitleBoard = Util.CreateSprite("TitleBoard", base.gameObject, image4);
		Util.SetSpriteInfo(mTitleBoard, "popup_panel_title3", mBaseDepth + 7, new Vector3(0f, 280f, 0f), Vector3.one, false);
		mTitleBoard.type = UIBasicSprite.Type.Sliced;
		mTitleBoard.SetDimensions(442, 85);
		UILabel uILabel = Util.CreateLabel("Title", mTitleBoard.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 8, new Vector3(0f, -2f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(uILabel, text);
		uILabel.color = new Color(1f, 1f, 1f, 1f);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(370, 38);
		uILabel.effectStyle = UILabel.Effect.Shadow;
		UISprite uISprite = Util.CreateSprite("MainBoard", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel13", mBaseDepth + 2, new Vector3(0f, 105f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 210);
		uISprite.type = UIBasicSprite.Type.Sliced;
		UISprite sprite = Util.CreateSprite("Accessory9Top", uISprite.gameObject, image);
		Util.SetSpriteInfo(sprite, "instruction_accessory09", mBaseDepth + 3, new Vector3(0f, 113f, 0f), Vector3.one, false);
		sprite = Util.CreateSprite("Accessory9Down", uISprite.gameObject, image);
		Util.SetSpriteInfo(sprite, "instruction_accessory09", mBaseDepth + 3, new Vector3(0f, -113f, 0f), Vector3.one, false);
		sprite.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);
		UISprite uISprite2 = Util.CreateSprite("BoosterBoardDown", base.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, -106f, 0f), Vector3.one, false);
		uISprite2.SetDimensions(510, 155);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		string text2 = Localization.Get(mItemData.data.Desc);
		uILabel = Util.CreateLabel("Desc", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text2, mBaseDepth + 3, new Vector3(0f, -7f, 0f), 31, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		uILabel.fontSize = 26;
		uILabel.spacingY = 10;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(490, 140);
		UISprite uISprite3 = Util.CreateSprite("BoosterBoardIn", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite3, "instruction_panel14", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite3.SetDimensions(495, 195);
		uISprite3.type = UIBasicSprite.Type.Sliced;
		text = mItemData.data.IconName;
		int num2 = 1;
		foreach (KeyValuePair<TrophyItemDetail.ItemKind, int> bundleItem in mItemData.data.ItemDetail.BundleItems)
		{
			if (bundleItem.Value != 1)
			{
				NumberImageString numberImageString = Util.CreateGameObject("NumDefault", uISprite.gameObject).AddComponent<NumberImageString>();
				numberImageString.Init(1, bundleItem.Value, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, false, image, false, Res.BuyNumImageString);
				numberImageString.transform.localPosition = new Vector3(98f, 0f, 0f);
			}
			num2 = bundleItem.Value;
		}
		if (num2 != 1)
		{
			UISprite uISprite4 = Util.CreateSprite("BoosterIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite4, text, mBaseDepth + 3, new Vector3(-80f, -3f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(145, 145);
			text = Localization.Get("Xtext");
			uILabel = Util.CreateLabel("NumDesc", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(25f, -6f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(uILabel);
			uILabel.fontSize = 56;
			uILabel.color = new Color(0.7372549f, 61f / 85f, 7f / 85f, 1f);
		}
		else
		{
			UISprite uISprite4 = Util.CreateSprite("BoosterIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite4, text, mBaseDepth + 3, new Vector3(0f, -3f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(145, 145);
		}
		if (mItemData.limit <= 0)
		{
			sprite = Util.CreateSprite("BuyButton", base.gameObject, image3);
			Util.SetSpriteInfo(sprite, "LC_soldout", mBaseDepth + 3, new Vector3(0f, -233f, 0f), Vector3.one, false);
		}
		else if (mItemData.data.UseTrophy <= mGame.mPlayer.GetTrophy())
		{
			UIButton uIButton = Util.CreateJellyImageButton("BuyButton", base.gameObject, image3);
			Util.SetImageButtonInfo(uIButton, "LC_button_exchange", "LC_button_exchange", "LC_button_exchange", mBaseDepth + 3, new Vector3(0f, -233f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnBuyPushed", UIButtonMessage.Trigger.OnClick);
			mLabel = Util.CreateLabel("Price", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(mLabel, string.Empty + mItemData.data.UseTrophy, mBaseDepth + 4, new Vector3(46f, -2f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(mLabel);
			mLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			mLabel.fontSize = 24;
		}
		else
		{
			sprite = Util.CreateSprite("BuyButton", base.gameObject, image3);
			Util.SetSpriteInfo(sprite, "LC_button_exchange", mBaseDepth + 3, new Vector3(0f, -233f, 0f), Vector3.one, false);
			sprite.color = Color.gray;
			mLabel = Util.CreateLabel("Price", sprite.gameObject, atlasFont);
			Util.SetLabelInfo(mLabel, string.Empty + mItemData.data.UseTrophy, mBaseDepth + 4, new Vector3(46f, -2f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(mLabel);
			mLabel.color = new Color(16f / 51f, 0f, 0f, 1f);
			mLabel.fontSize = 24;
		}
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(TrophyItemListItem a_item)
	{
		mItemData = a_item;
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mItemData, mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnBuyPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.BUY;
			Close();
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}
}
