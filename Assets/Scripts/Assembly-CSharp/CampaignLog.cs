using System;
using System.Collections.Generic;

public class CampaignLog : ICloneable
{
	public class CLog
	{
		public int iap_campaign_id { get; set; }

		public string iapname { get; set; }

		public long endtime { get; set; }

		public long datatime { get; set; }

		public long recordtime { get; set; }

		public override string ToString()
		{
			return string.Format("[CLog<{0}> iapname={1} endtime={2} datatime={3} recordtime={4}", iap_campaign_id, iapname, endtime, datatime, recordtime);
		}
	}

	public IDictionary<int, CLog> Logs { get; private set; }

	protected CampaignLog()
	{
		Logs = new Dictionary<int, CLog>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public CampaignLog Clone()
	{
		return MemberwiseClone() as CampaignLog;
	}

	public static CampaignLog CreateNewCampaignLog()
	{
		return new CampaignLog();
	}

	public static void CampaignBIJLog(string str)
	{
	}

	public void OutPutOneLog(string aOperation, CLog aLog)
	{
	}

	public void OutPutAllLog()
	{
	}

	public void Serialize(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		CLog[] array = new CLog[Logs.Count];
		Logs.Values.CopyTo(array, 0);
		data.WriteInt(array.Length);
		foreach (CLog cLog in array)
		{
			data.WriteInt(cLog.iap_campaign_id);
			data.WriteUTF(cLog.iapname);
			data.WriteLong(cLog.endtime);
			data.WriteLong(cLog.datatime);
			data.WriteLong(cLog.recordtime);
		}
	}

	public static CampaignLog Deserialize(BIJBinaryReader data)
	{
		short num = data.ReadShort();
		CampaignLog campaignLog = new CampaignLog();
		int num2 = data.ReadInt();
		for (int i = 0; i < num2; i++)
		{
			CLog cLog = new CLog();
			cLog.iap_campaign_id = data.ReadInt();
			cLog.iapname = data.ReadUTF();
			cLog.endtime = data.ReadLong();
			cLog.datatime = data.ReadLong();
			cLog.recordtime = data.ReadLong();
			campaignLog.Logs[cLog.iap_campaign_id] = cLog;
		}
		return campaignLog;
	}

	public static CampaignLog Load()
	{
		CampaignLog campaignLog = null;
		bool flag = false;
		string cAMPAIGN_FILE = Constants.CAMPAIGN_FILE;
		try
		{
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(cAMPAIGN_FILE))
			{
				campaignLog = Deserialize(bIJEncryptDataReader);
				bIJEncryptDataReader.Close();
			}
			flag = campaignLog != null;
		}
		catch (Exception)
		{
			flag = false;
		}
		if (flag)
		{
			return campaignLog;
		}
		campaignLog = CreateNewCampaignLog();
		campaignLog.Save();
		return campaignLog;
	}

	public static void Save(CampaignLog aCampaignLog)
	{
		aCampaignLog.Save();
	}

	public void Save()
	{
		try
		{
			string cAMPAIGN_FILE = Constants.CAMPAIGN_FILE;
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(cAMPAIGN_FILE))
			{
				Serialize(bIJEncryptDataWriter);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	public bool HasCLog()
	{
		return Logs.Count > 0;
	}

	public void SetCLog(CLog aLog, bool isSave)
	{
		if (Logs.ContainsKey(aLog.iap_campaign_id))
		{
			Logs[aLog.iap_campaign_id] = aLog;
		}
		else
		{
			Logs.Add(aLog.iap_campaign_id, aLog);
		}
		OutPutOneLog("Set", aLog);
		OutPutAllLog();
		if (isSave)
		{
			Save();
		}
	}

	public CLog GetLastCLog(int aCampaignId)
	{
		if (Logs.ContainsKey(aCampaignId))
		{
			OutPutOneLog("Get", Logs[aCampaignId]);
			return Logs[aCampaignId];
		}
		return null;
	}

	public CLog GetLastCLog(string aIapname)
	{
		CampaignBIJLog("CampaignLog GetLastCLog : aIapname=" + aIapname);
		OutPutAllLog();
		List<CLog> list = new List<CLog>(Logs.Values);
		if (list.Count == 0)
		{
			return null;
		}
		for (int num = list.Count - 1; num >= 0; num--)
		{
			CLog cLog = list[num];
			if (cLog.iapname == aIapname)
			{
				OutPutOneLog("Get", cLog);
				return cLog;
			}
		}
		return null;
	}

	public void DeleteCLog(int aCampaignId, bool isSave)
	{
		if (Logs.ContainsKey(aCampaignId))
		{
			OutPutOneLog("Delete", Logs[aCampaignId]);
			Logs.Remove(aCampaignId);
			OutPutAllLog();
			if (isSave)
			{
				Save();
			}
		}
	}

	public void DeleteCLog(string aIapname, bool isSave)
	{
		List<CLog> list = new List<CLog>(Logs.Values);
		if (list.Count == 0)
		{
			return;
		}
		for (int num = list.Count - 1; num >= 0; num--)
		{
			CLog cLog = list[num];
			if (cLog.iapname == aIapname)
			{
				OutPutOneLog("Delete", cLog);
				Logs.Remove(cLog.iap_campaign_id);
				OutPutAllLog();
				if (isSave)
				{
					Save();
				}
			}
		}
	}

	public void ClearCLog(bool isSave)
	{
		if (Logs.Count > 0)
		{
			CampaignBIJLog("CampaignLog Clear");
			Logs.Clear();
			if (isSave)
			{
				Save();
			}
		}
	}
}
