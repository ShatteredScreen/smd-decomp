using System.Collections.Generic;
using UnityEngine;

public class DemoCollectionDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		APPEAR = 1,
		AFTER_DEMO = 2,
		MAIN = 3,
		WAIT = 4,
		DISAPPEAR = 5
	}

	public class EpisodeSelectListItem : SMVerticalListItem
	{
		public int index;

		public bool enable;

		public StoryDemoData data;
	}

	public delegate void OnDialogClosed();

	private const int itemWidth = 332;

	private const int itemHeight = 220;

	private const float itemLNum = 2.6f;

	private const float itemPNum = 4.85f;

	private const float appearPosY = -35f;

	private const float disappearPosY = -1222f;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private GameObject mAnchorTop;

	private GameObject mAnchorLeft;

	private UIButton mExitButton;

	private OnDialogClosed mCallback;

	private bool mClosing;

	private int mSortOrder;

	private GameState mGameState;

	private SMEventPageData mEventPageData;

	private List<int> mIdList;

	private List<SMVerticalListItem> mEpisodeList = new List<SMVerticalListItem>();

	private SMVerticalList mEpisodeSelectList;

	private static SMVerticalListInfo mEpisodeListInfo = new SMVerticalListInfo(1, 332f, 572f, 1, 332f, 1067f, 1, 220);

	private bool mListCreated;

	private int mSelectedStoryDemoIndex;

	private StoryDemoManager mStoryDemo;

	private UISprite mTitleBarL;

	private UISprite mTitleBarR;

	public void Init(GameState aGameState, SMEventPageData aEventPageData)
	{
		mGameState = aGameState;
		mEventPageData = aEventPageData;
		mIdList = aEventPageData.GetDemoList();
	}

	public override void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mParticleFeather = false;
		SetOpenImmediate(true);
		SetCloseImmediate(true);
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.APPEAR:
		case STATE.AFTER_DEMO:
		{
			Vector3 localPosition2 = mEpisodeSelectList.gameObject.transform.localPosition;
			localPosition2.y += 120f * (Time.deltaTime * 30f);
			if (localPosition2.y >= -35f)
			{
				if (mState.GetStatus() == STATE.AFTER_DEMO)
				{
					mPanel.sortingOrder = mSortOrder;
				}
				localPosition2.y = -35f;
				mState.Change(STATE.MAIN);
				NGUITools.SetActive(mExitButton.gameObject, true);
			}
			mEpisodeSelectList.gameObject.transform.localPosition = localPosition2;
			mEpisodeSelectList.gameObject.SetActive(false);
			mEpisodeSelectList.gameObject.SetActive(true);
			break;
		}
		case STATE.DISAPPEAR:
		{
			Vector3 localPosition = mEpisodeSelectList.gameObject.transform.localPosition;
			localPosition.y -= 120f * (Time.deltaTime * 30f);
			if (localPosition.y <= -1222f)
			{
				localPosition.y = -1222f;
				mState.Reset(STATE.WAIT, true);
				if (mClosing)
				{
					Close();
				}
				else
				{
					mGame.StopMusic(0, 0.2f);
					mStoryDemo = Util.CreateGameObject("StoryDemo", base.gameObject).AddComponent<StoryDemoManager>();
					mStoryDemo.ChangeStoryDemo(mSelectedStoryDemoIndex, OnStoryDemoFinished);
				}
			}
			mEpisodeSelectList.gameObject.transform.localPosition = localPosition;
			break;
		}
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		mEpisodeList.Clear();
		int num = 0;
		for (int i = 0; i < mGame.mStoryDemoData.Count; i++)
		{
			if (mIdList.Contains(i))
			{
				EpisodeSelectListItem episodeSelectListItem = new EpisodeSelectListItem();
				if (mGame.mPlayer.IsStoryCompleted(mGame.mStoryDemoData[i].index))
				{
					episodeSelectListItem.enable = true;
				}
				else
				{
					episodeSelectListItem.enable = false;
				}
				episodeSelectListItem.index = num;
				episodeSelectListItem.data = mGame.mStoryDemoData[i];
				mEpisodeList.Add(episodeSelectListItem);
				num++;
			}
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		mAnchorTop = Util.CreateAnchorWithChild("AnchorTop", base.gameObject, UIAnchor.Side.Top, component).gameObject;
		mAnchorLeft = Util.CreateAnchorWithChild("AnchorLeft", base.gameObject, UIAnchor.Side.Left, component).gameObject;
		SetJellyTargetScale(1f);
		SetHeaderLayout();
		UpdateBaseDepth();
	}

	private void OnCreateItem_Episode(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		string key = "EVENT_EPISODE_" + mEventPageData.EventID;
		ResImage resImage = ResourceManager.LoadImage(key);
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		EpisodeSelectListItem episodeSelectListItem = (EpisodeSelectListItem)item;
		uISprite = Util.CreateSprite("Back", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)mEpisodeSelectList.mList.cellWidth;
		uISprite.height = (int)mEpisodeSelectList.mList.cellHeight;
		string text = episodeSelectListItem.data.iconName;
		string chapterImage = episodeSelectListItem.data.chapterImage;
		string text2 = Localization.Get(episodeSelectListItem.data.title);
		if (!episodeSelectListItem.enable)
		{
			text = "icon_question";
			chapterImage = "chapter_question";
			text2 = Localization.Get("StoryDemo_TitleUnknown");
		}
		UIButton uIButton = Util.CreateJellyImageButton("Episode_" + episodeSelectListItem.index, itemGO, resImage.Image);
		Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 3, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnEpisodePushed", UIButtonMessage.Trigger.OnClick);
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mEpisodeSelectList.mListScrollView;
		uISprite = Util.CreateSprite("Frame", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "collection_iconframe", mBaseDepth + 2, new Vector3(-0.5f, 9f, 0f), Vector3.one, false);
		uISprite = Util.CreateSprite("Frame2", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "collection_iconframe2", mBaseDepth + 2, new Vector3(0f, -78f, 0f), Vector3.one, false);
		UISprite uISprite2 = uISprite;
		UILabel label = Util.CreateLabel("Title", itemGO, atlasFont);
		Util.SetLabelInfo(label, text2, mBaseDepth + 3, new Vector3(0f, -78f, 0f), 18, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabeShrinkContent(label, uISprite2.width - 52, 18);
	}

	public void OnEpisodePushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			string[] array = go.name.Split('_');
			int index = int.Parse(array[1]);
			mSelectedStoryDemoIndex = ((EpisodeSelectListItem)mEpisodeList[index]).data.index;
			if (((EpisodeSelectListItem)mEpisodeList[index]).enable)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				mSortOrder = mPanel.sortingOrder;
				mPanel.sortingOrder = mGameState.GrayScreenSortOrder - 1;
				StartCoroutine(mGameState.GrayOut(0.8f, Color.black, 1f));
				mState.Reset(STATE.DISAPPEAR, true);
				mClosing = false;
				NGUITools.SetActive(mExitButton.gameObject, false);
			}
			else
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
		}
	}

	private void OnStoryDemoFinished(int storyDemoIndex)
	{
		mStoryDemo = null;
		GameStateEventRally2 gameStateEventRally = (GameStateEventRally2)mGameState;
		mGame.PlayMusic(gameStateEventRally.EventBGM, 0, true);
		StartCoroutine(mGameState.GrayOut());
		mState.Change(STATE.AFTER_DEMO);
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		SetHeaderLayout();
		if (mListCreated)
		{
			bool a_portrait = false;
			float num = 0f;
			if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
			{
				a_portrait = true;
				num = 444f;
			}
			else
			{
				num = 200f;
			}
			mEpisodeSelectList.OnScreenChanged(a_portrait);
			Vector3 vector = new Vector3(340f, num, 0f);
			if (mExitButton == null)
			{
				string key = "EVENT_EPISODE_" + mEventPageData.EventID;
				ResImage resImage = ResourceManager.LoadImage(key);
				mExitButton = Util.CreateJellyImageButton("ButtonCancel", mAnchorLeft, resImage.Image);
				Util.SetImageButtonInfo(mExitButton, "button_return", "button_return", "button_return", mBaseDepth + 5, vector, Vector3.one);
				Util.AddImageButtonMessage(mExitButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
				NGUITools.SetActive(mExitButton.gameObject, false);
			}
			else
			{
				mExitButton.gameObject.transform.localPosition = vector;
			}
		}
	}

	private void SetHeaderLayout()
	{
	}

	public override void OnOpenFinished()
	{
		mEpisodeSelectList = SMVerticalList.Make(mAnchorLeft, this, mEpisodeListInfo, mEpisodeList, 140f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mEpisodeSelectList.OnCreateItem = OnCreateItem_Episode;
		mEpisodeSelectList.OnCreateFinish = OnCreateFinish;
		mEpisodeSelectList.HideScrollBar();
		Vector3 localPosition = mEpisodeSelectList.gameObject.transform.localPosition;
		localPosition.y = -1222f;
		mEpisodeSelectList.gameObject.transform.localPosition = localPosition;
	}

	private void OnCreateFinish()
	{
		mListCreated = true;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mTitleBarR = Util.CreateSprite("StatusR", mAnchorTop, image);
		Util.SetSpriteInfo(mTitleBarR, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, 173f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBarL = Util.CreateSprite("StatusL", mAnchorTop, image);
		Util.SetSpriteInfo(mTitleBarL, "menubar_frame", mBaseDepth + 2, new Vector3(2f, 173f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		string key = "EVENT_EPISODE_" + mEventPageData.EventID;
		ResImage resImage = ResourceManager.LoadImage(key);
		UISprite sprite = Util.CreateSprite("episode", mAnchorTop, resImage.Image);
		Util.SetSpriteInfo(sprite, "episode", mBaseDepth + 3, new Vector3(-2f, -26f, 0f), Vector3.one, false);
		SetLayout(Util.ScreenOrientation);
		StartCoroutine(mGameState.GrayOut());
		mState.Change(STATE.APPEAR);
		UIPanel listPanel = mEpisodeSelectList.ListPanel;
		int num = mIdList.Count * 220;
		float y = listPanel.finalClipRegion.y - listPanel.clipSoftness.y - ((float)num - listPanel.finalClipRegion.w) / 2f;
		sprite = Util.CreateSprite("Film", listPanel.gameObject, resImage.Image);
		Util.SetSpriteInfo(sprite, "collection_film", listPanel.sortingOrder - 1, new Vector3(0f, y, 0f), Vector3.one, false);
		sprite.type = UIBasicSprite.Type.Tiled;
		sprite.width = 332;
		sprite.height = num;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public override void Close()
	{
		base.Close();
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			StartCoroutine(mGameState.GrayIn());
			mState.Reset(STATE.DISAPPEAR, true);
			mClosing = true;
			NGUITools.SetActive(mExitButton.gameObject, false);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
