using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Partner : MonoBehaviour
{
	public enum COMPANION_ACTION
	{
		STAY = 0,
		WIN = 1,
		LOSE = 2,
		HIT0 = 3,
		HIT1 = 4,
		HIT2 = 5,
		HURRY = 6,
		JUMP = 7,
		LANDING = 8,
		SKILL = 9,
		CUT_IN = 10
	}

	public enum CUT_IN_TRIGGER
	{
		COMBO = 0,
		CHANCE = 1,
		WIN = 2,
		LOSE = 3
	}

	public delegate void OnFinish(Partner sender);

	private const int RENDER_SCREEN_SIZE = 568;

	private COMPANION_ACTION mCurrentAction;

	public static Vector3 PUZZLE_PAIRPARTNER_OFS = new Vector3(0f, 30f, 0f);

	private GameMain mGame;

	private List<Live2DInstance> mPartnerList = new List<Live2DInstance>();

	private List<Live2DRender> mL2DRenderList = new List<Live2DRender>();

	private List<SimpleTexture> mPartnerScreenList = new List<SimpleTexture>();

	private List<UITexture> mPartnerUIScreenList = new List<UITexture>();

	private List<Vector3> mPartnerPosList = new List<Vector3>();

	private List<Vector3> mPartnerOffsetPosList = new List<Vector3>();

	private List<bool> mL2DAnimeProtectionList = new List<bool>();

	private List<float> mPartnerScaleList = new List<float>();

	private Vector3 mBasePos;

	private List<UISprite> mUIShadowList = new List<UISprite>();

	private bool mCollectionMode;

	private bool mPartnerLoadFinished;

	private CompanionData.MOTION[] mCurrentMotion;

	private bool[] mMotionLoop;

	private bool mHurry;

	private bool mWin;

	private bool mLose;

	public CompanionData mData;

	private int mBaseDepth;

	private GameObject mScreenParent;

	private int mLevel;

	private OnFinish mOnAnimeFinish;

	public float SkillPower { get; private set; }

	public void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	public void OnDestroy()
	{
		for (int i = 0; i < mL2DRenderList.Count; i++)
		{
			Object.Destroy(mL2DRenderList[i].gameObject);
		}
		for (int j = 0; j < mPartnerScreenList.Count; j++)
		{
			Object.Destroy(mPartnerScreenList[j].gameObject);
		}
		for (int k = 0; k < mPartnerUIScreenList.Count; k++)
		{
			if (mPartnerUIScreenList[k] != null)
			{
				Object.Destroy(mPartnerUIScreenList[k].gameObject);
			}
		}
		for (int l = 0; l < mData.ModelCount; l++)
		{
			ResourceManager.ProtectLive2DAnimation(mData.GetModelKey(l), mL2DAnimeProtectionList[l]);
			ResourceManager.UnloadLive2DAnimation(mData.GetModelKey(l));
		}
	}

	public void Update()
	{
		if (!IsPartnerLoadFinished())
		{
			return;
		}
		if (IsMotionFinished())
		{
			for (int i = 0; i < mData.ModelCount; i++)
			{
				if (mMotionLoop != null && mMotionLoop[i])
				{
					StartMotion(mCurrentMotion[i], true, i);
				}
			}
		}
		switch (mCurrentAction)
		{
		case COMPANION_ACTION.STAY:
			if (IsMotionFinished())
			{
				Stay(true);
			}
			break;
		case COMPANION_ACTION.HIT0:
			if (IsMotionFinished())
			{
				Stay(true);
			}
			break;
		case COMPANION_ACTION.HIT1:
			if (IsMotionFinished())
			{
				Stay(true);
			}
			break;
		case COMPANION_ACTION.HIT2:
			if (IsMotionFinished())
			{
				Stay(true);
			}
			break;
		case COMPANION_ACTION.WIN:
			if (IsMotionFinished())
			{
				Win();
			}
			break;
		case COMPANION_ACTION.LOSE:
			if (IsMotionFinished())
			{
				Lose();
			}
			break;
		case COMPANION_ACTION.LANDING:
			if (IsMotionFinished())
			{
				Stay(CompanionData.MOTION.STAY0, mCollectionMode);
			}
			break;
		}
		for (int j = 0; j < mPartnerScreenList.Count; j++)
		{
			float z = -0.01f * (float)(int)mData.GetZPosition(mCurrentMotion[j], j);
			float x = mData.GetPosition(j) * mData.GetScale(j) * mPartnerScaleList[j];
			Vector3 vector = new Vector3(x, 0f, z);
			mPartnerScreenList[j].transform.localPosition = mPartnerPosList[j] + mPartnerOffsetPosList[j] + vector;
			mPartnerScreenList[j].transform.localScale = new Vector3(mData.GetScale(j) * mPartnerScaleList[j], mData.GetScale(j) * mPartnerScaleList[j], 1f);
		}
		for (int k = 0; k < mPartnerUIScreenList.Count; k++)
		{
			int depth = mBaseDepth + mData.GetZPosition(mCurrentMotion[k], k);
			mPartnerUIScreenList[k].depth = depth;
			float x2 = mData.GetPosition(k) * mData.GetScale(k) * mPartnerScaleList[k];
			Vector3 vector2 = new Vector3(x2, 0f, 0f);
			mPartnerUIScreenList[k].transform.localPosition = mPartnerPosList[k] + mPartnerOffsetPosList[k] + vector2;
			mPartnerUIScreenList[k].transform.localScale = new Vector3(mData.GetScale(k) * mPartnerScaleList[k], mData.GetScale(k) * mPartnerScaleList[k], 1f);
		}
		for (int l = 0; l < mUIShadowList.Count; l++)
		{
			float num = Mathf.Max(0f, 400f - Mathf.Max(0f, mPartnerOffsetPosList[l].y) * mPartnerScaleList[l]) / 400f;
			float x3 = mData.GetPosition(l) * mData.GetScale(l) * mPartnerScaleList[l] + mPartnerPosList[l].x;
			float y = (mData.GetShadowOfs(l) + 10f) * mData.GetScale(l) * mPartnerScaleList[l] + mPartnerPosList[l].y;
			float num2 = mData.GetScale(l) * mPartnerScaleList[l] * num;
			mUIShadowList[l].transform.localPosition = new Vector3(x3, y, 0f);
			mUIShadowList[l].transform.localScale = new Vector3(num2, num2, 1f);
			mUIShadowList[l].color = new Color(1f, 1f, 1f, num);
		}
	}

	public void Init(short partnerIndex, Vector3 pos, float scale, int level, GameObject screenParent, bool useUITexture, int baseDepth, bool collectionMode = false)
	{
		mPartnerLoadFinished = false;
		mData = mGame.mCompanionData[partnerIndex];
		mLevel = level;
		mBasePos = pos;
		mBaseDepth = baseDepth;
		mScreenParent = screenParent;
		SkillPower = 0f;
		mCollectionMode = collectionMode;
		StartCoroutine(PartnerLoad(screenParent, scale, useUITexture));
	}

	private IEnumerator PartnerLoad(GameObject screenParent, float scale, bool useUITexture)
	{
		List<ResourceInstance> resList = new List<ResourceInstance>();
		for (int n = 0; n < mData.ModelCount; n++)
		{
			mL2DRenderList.Add(mGame.CreateLive2DRender(568, 568));
		}
		for (int m = 0; m < mData.ModelCount; m++)
		{
			resList.Add(ResourceManager.LoadLive2DAnimationAsync(mData.GetModelKey(m)));
		}
		while (true)
		{
			bool wait2 = false;
			foreach (ResourceInstance res in resList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait2 = true;
					break;
				}
			}
			if (!wait2)
			{
				break;
			}
			yield return 0;
		}
		for (int l = 0; l < mData.ModelCount; l++)
		{
			mL2DAnimeProtectionList.Add(ResourceManager.IsProtectedLive2DAnimation(mData.GetModelKey(l)));
			ResourceManager.ProtectLive2DAnimation(mData.GetModelKey(l));
			mPartnerList.Add(Util.CreateGameObject("PartnerInstance", mL2DRenderList[l].gameObject).AddComponent<Live2DInstance>());
			mPartnerList[l].transform.localPosition = Vector3.zero;
			mPartnerList[l].Init(mData.GetModelKey(l), Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
		}
		while (true)
		{
			bool wait = false;
			foreach (Live2DInstance inst in mPartnerList)
			{
				if (!inst.IsLoadFinished())
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
		for (int k = 0; k < mData.ModelCount; k++)
		{
			mL2DRenderList[k].ResisterInstance(mPartnerList[k]);
		}
		for (int j = 0; j < mData.ModelCount; j++)
		{
			if (useUITexture)
			{
				mPartnerUIScreenList.Add(Util.CreateGameObject("PartnerUIScreen", screenParent).AddComponent<UITexture>());
				mPartnerUIScreenList[j].mainTexture = mL2DRenderList[j].RenderTexture;
				mPartnerUIScreenList[j].SetDimensions(568, 568);
				mPartnerUIScreenList[j].pivot = UIWidget.Pivot.Bottom;
				mPartnerUIScreenList[j].enabled = false;
			}
			else
			{
				mPartnerScreenList.Add(Util.CreateGameObject("PartnerScreen", screenParent).AddComponent<SimpleTexture>());
				mPartnerScreenList[j].Init(mL2DRenderList[j].RenderTexture, new Vector2(568f, 568f), new Rect(0f, 0f, 1f, 1f), SimpleTexture.PIVOT.BOTTOM, true);
			}
			mPartnerOffsetPosList.Add(Vector3.zero);
			mPartnerPosList.Add(Vector3.zero);
			mPartnerScaleList.Add(scale);
		}
		mCurrentMotion = new CompanionData.MOTION[mData.ModelCount];
		mMotionLoop = new bool[mData.ModelCount];
		for (int i = 0; i < mData.ModelCount; i++)
		{
			mCurrentMotion[i] = CompanionData.MOTION.STAY0;
			mMotionLoop[i] = false;
		}
		SetPartnerScreenScale(1f);
		Stay(true);
		yield return 0;
		mPartnerLoadFinished = true;
	}

	public bool IsPartnerLoadFinished()
	{
		return mPartnerLoadFinished;
	}

	public void Stay(bool force = false)
	{
		if (mCurrentAction == COMPANION_ACTION.WIN || mCurrentAction == COMPANION_ACTION.SKILL)
		{
			return;
		}
		if (mWin)
		{
			Win();
		}
		else if (mLose)
		{
			Lose();
		}
		else
		{
			if (mCurrentAction == COMPANION_ACTION.STAY && !force)
			{
				return;
			}
			if (mHurry && mLevel >= CompanionData.HURRY00_LEVEL)
			{
				StartMotion(CompanionData.MOTION.HURRY, false);
			}
			else
			{
				int num = mData.idle0ratio;
				if (mLevel >= CompanionData.WAIT01_LEVEL)
				{
					num += mData.idle1ratio;
				}
				int num2 = Random.Range(0, num);
				if (num2 < mData.idle0ratio)
				{
					StartMotion(CompanionData.MOTION.STAY0, false);
				}
				else
				{
					StartMotion(CompanionData.MOTION.STAY1, false);
				}
			}
			mCurrentAction = COMPANION_ACTION.STAY;
		}
	}

	public void Stay(CompanionData.MOTION motion, bool loop = false)
	{
		StartMotion(motion, loop);
		mCurrentAction = COMPANION_ACTION.STAY;
	}

	public void Win(OnFinish fin = null)
	{
		mWin = true;
		if (mCurrentAction != COMPANION_ACTION.CUT_IN)
		{
			StartMotion(CompanionData.MOTION.WIN, false);
			mCurrentAction = COMPANION_ACTION.WIN;
		}
	}

	public void Lose(OnFinish fin = null)
	{
		mLose = true;
		if (mCurrentAction != COMPANION_ACTION.CUT_IN)
		{
			StartMotion(CompanionData.MOTION.LOSE, false);
			mCurrentAction = COMPANION_ACTION.LOSE;
		}
	}

	public void Hit(OnFinish fin = null)
	{
		if (mCurrentAction == COMPANION_ACTION.WIN || mHurry || (mCurrentAction == COMPANION_ACTION.HIT0 && mPartnerList[0].MotionPlayTimer < mData.hit0ViewSec) || (mCurrentAction == COMPANION_ACTION.HIT1 && mPartnerList[0].MotionPlayTimer < mData.hit1ViewSec) || (mCurrentAction == COMPANION_ACTION.HIT2 && mPartnerList[0].MotionPlayTimer < mData.hit2ViewSec))
		{
			return;
		}
		int num = mData.hit0ratio;
		if (mData.motionType == 0)
		{
			if (mLevel >= CompanionData.HIT01_LEVEL)
			{
				num += mData.hit1ratio;
			}
			if (mLevel >= CompanionData.HIT02_LEVEL)
			{
				num += mData.hit2ratio;
			}
			int num2 = Random.Range(0, num);
			if (num2 < mData.hit0ratio)
			{
				StartMotion(CompanionData.MOTION.HIT0, false);
				mCurrentAction = COMPANION_ACTION.HIT0;
			}
			else if (num2 < mData.hit0ratio + mData.hit1ratio)
			{
				StartMotion(CompanionData.MOTION.HIT1, false);
				mCurrentAction = COMPANION_ACTION.HIT1;
			}
			else
			{
				StartMotion(CompanionData.MOTION.HIT2, false);
				mCurrentAction = COMPANION_ACTION.HIT2;
			}
		}
		else
		{
			if (mLevel >= CompanionData.HIT02_LEVEL)
			{
				num += mData.hit2ratio;
			}
			int num3 = Random.Range(0, num);
			if (num3 < mData.hit0ratio)
			{
				StartMotion(CompanionData.MOTION.HIT0, false);
				mCurrentAction = COMPANION_ACTION.HIT0;
			}
			else
			{
				StartMotion(CompanionData.MOTION.HIT2, false);
				mCurrentAction = COMPANION_ACTION.HIT2;
			}
		}
	}

	public void Hurry()
	{
		mHurry = true;
		if (mCurrentAction != COMPANION_ACTION.CUT_IN)
		{
			Stay();
		}
	}

	public void Continue()
	{
		mWin = false;
		mLose = false;
		mHurry = false;
		Stay();
	}

	public void Jump(int characterIndex = -1)
	{
		StartMotion(CompanionData.MOTION.JUMP, false, characterIndex);
		mCurrentAction = COMPANION_ACTION.JUMP;
	}

	public void Landing(int characterIndex = -1)
	{
		StartMotion(CompanionData.MOTION.LANDING, false, characterIndex);
		mCurrentAction = COMPANION_ACTION.LANDING;
	}

	public void Skill(int rank = -1, int characterIndex = -1)
	{
		if (rank == -1)
		{
			rank = ((mLevel < CompanionData.SKILL01_LEVEL) ? 1 : 2);
		}
		switch (rank)
		{
		case 1:
			StartMotion(CompanionData.MOTION.SKILL0, false, characterIndex);
			break;
		case 2:
			StartMotion(CompanionData.MOTION.SKILL1, false, characterIndex);
			break;
		default:
			StartMotion(CompanionData.MOTION.SKILL2, false, characterIndex);
			break;
		}
		mCurrentAction = COMPANION_ACTION.SKILL;
	}

	public void StartMotion(CompanionData.MOTION motion, bool loop, int characterIndex = -1, int fadeInMsec = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mData.ModelCount; i++)
			{
				mPartnerList[i].StartMotion(mData.GetMotionPath(motion, i), false, fadeInMsec);
				mCurrentMotion[i] = motion;
				mMotionLoop[i] = loop;
			}
		}
		else
		{
			mPartnerList[characterIndex].StartMotion(mData.GetMotionPath(motion, characterIndex), false, fadeInMsec);
			mCurrentMotion[characterIndex] = motion;
			mMotionLoop[characterIndex] = loop;
		}
	}

	public Texture GetRenderTexture(int characterIndex)
	{
		if (characterIndex < mL2DRenderList.Count)
		{
			return mL2DRenderList[characterIndex].RenderTexture;
		}
		return null;
	}

	public SimpleTexture GetPartnerScreen(int characterIndex)
	{
		if (mPartnerScreenList.Count < 1)
		{
			return null;
		}
		if (characterIndex < mPartnerScreenList.Count)
		{
			return mPartnerScreenList[characterIndex];
		}
		return null;
	}

	public UITexture GetPartnerUIScreen(int characterIndex)
	{
		if (mPartnerUIScreenList.Count < 1)
		{
			return null;
		}
		if (characterIndex < mPartnerUIScreenList.Count)
		{
			return mPartnerUIScreenList[characterIndex];
		}
		return null;
	}

	public Live2DInstance GetPartnerInstance(int characterIndex)
	{
		if (characterIndex < mPartnerList.Count)
		{
			return mPartnerList[characterIndex];
		}
		return null;
	}

	public void SetPartnerScreenPos(Vector3 pos, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerPosList.Count; i++)
			{
				mPartnerPosList[i] = pos;
			}
		}
		else if (characterIndex < mPartnerPosList.Count)
		{
			mPartnerPosList[characterIndex] = pos;
		}
	}

	public void SetPartnerScreenColor(Color col, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerScreenList.Count; i++)
			{
				mPartnerScreenList[i].SetColor(col);
			}
			for (int j = 0; j < mPartnerUIScreenList.Count; j++)
			{
				mPartnerUIScreenList[j].color = col;
			}
		}
		else
		{
			if (characterIndex < mPartnerScreenList.Count)
			{
				mPartnerScreenList[characterIndex].SetColor(col);
			}
			if (characterIndex < mPartnerUIScreenList.Count)
			{
				mPartnerUIScreenList[characterIndex].color = col;
			}
		}
	}

	public void SetPartnerScreenScale(float scale, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerScaleList.Count; i++)
			{
				mPartnerScaleList[i] = scale;
			}
		}
		else if (characterIndex < mPartnerScaleList.Count)
		{
			mPartnerScaleList[characterIndex] = scale;
		}
	}

	public float GetPartnerScreenScale(int characterIndex = 0)
	{
		if (characterIndex < mPartnerScaleList.Count)
		{
			return mPartnerScaleList[characterIndex];
		}
		return 0f;
	}

	public void SetPartnerScreenOffset(Vector3 pos, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerOffsetPosList.Count; i++)
			{
				mPartnerOffsetPosList[i] = pos;
			}
		}
		else if (characterIndex < mPartnerOffsetPosList.Count)
		{
			mPartnerOffsetPosList[characterIndex] = pos;
		}
	}

	public void SetPartnerScreenEnable(bool enable, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerScreenList.Count; i++)
			{
				mPartnerScreenList[i].gameObject.SetActive(enable);
			}
			for (int j = 0; j < mPartnerUIScreenList.Count; j++)
			{
				mPartnerUIScreenList[j].enabled = enable;
			}
		}
		else
		{
			if (characterIndex < mPartnerScreenList.Count)
			{
				mPartnerScreenList[characterIndex].gameObject.SetActive(enable);
			}
			if (characterIndex < mPartnerUIScreenList.Count)
			{
				mPartnerUIScreenList[characterIndex].enabled = enable;
			}
		}
	}

	public void SetPartnerScreenParent(GameObject parent, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerScreenList.Count; i++)
			{
				mPartnerScreenList[i].transform.parent = parent.transform;
			}
			for (int j = 0; j < mPartnerUIScreenList.Count; j++)
			{
				mPartnerUIScreenList[j].transform.parent = parent.transform;
			}
		}
		else
		{
			if (characterIndex < mPartnerScreenList.Count)
			{
				mPartnerScreenList[characterIndex].transform.parent = parent.transform;
			}
			if (characterIndex < mPartnerUIScreenList.Count)
			{
				mPartnerUIScreenList[characterIndex].transform.parent = parent.transform;
			}
		}
	}

	public void SetPartnerScreenLayer(int layer, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerScreenList.Count; i++)
			{
				mPartnerScreenList[i].gameObject.layer = layer;
			}
			for (int j = 0; j < mPartnerUIScreenList.Count; j++)
			{
				mPartnerUIScreenList[j].gameObject.layer = layer;
			}
		}
		else
		{
			if (characterIndex < mPartnerScreenList.Count)
			{
				mPartnerScreenList[characterIndex].gameObject.layer = layer;
			}
			if (characterIndex < mPartnerUIScreenList.Count)
			{
				mPartnerUIScreenList[characterIndex].gameObject.layer = layer;
			}
		}
	}

	public void SetPartnerUIScreenDepth(int depth)
	{
		mBaseDepth = depth;
	}

	public void SetPartnerUIScreenPivot(UIWidget.Pivot pivot, int characterIndex = -1)
	{
		if (characterIndex == -1)
		{
			for (int i = 0; i < mPartnerUIScreenList.Count; i++)
			{
				mPartnerUIScreenList[i].pivot = pivot;
			}
		}
		else if (characterIndex < mPartnerUIScreenList.Count)
		{
			mPartnerUIScreenList[characterIndex].pivot = pivot;
		}
	}

	public bool IsMotionFinished(int characterIndex = -1)
	{
		bool result = true;
		if (characterIndex == -1)
		{
			for (int i = 0; i < mData.ModelCount; i++)
			{
				if (!mPartnerList[i].IsAnimationFinished())
				{
					result = false;
					break;
				}
			}
		}
		else if (characterIndex < mPartnerList.Count)
		{
			result = mPartnerList[characterIndex].IsAnimationFinished();
		}
		return result;
	}

	public int GetSkillRank()
	{
		if (mLevel >= CompanionData.SKILL01_LEVEL)
		{
			return 2;
		}
		if (mLevel >= CompanionData.SKILL00_LEVEL)
		{
			return 1;
		}
		return 0;
	}

	public string GetSkillName()
	{
		int skillRank = GetSkillRank();
		return mData.GetSkillName(skillRank);
	}

	public string GetSkillDesc()
	{
		int skillRank = GetSkillRank();
		return mData.GetSkillDesc(skillRank);
	}

	public string GetSkillDescLineFeed()
	{
		int skillRank = GetSkillRank();
		return mData.GetSkillDescLineFeed(skillRank);
	}

	public List<ByteVector2> GetSkillArea()
	{
		int skillRank = GetSkillRank();
		return mData.GetSkillArea(skillRank);
	}

	public Def.SkillFuncData GetSkillFuncData()
	{
		int skillRank = GetSkillRank();
		return mData.GetSkillFuncData(skillRank);
	}

	public void MakeUIShadow(BIJImage atlas, string spriteName, int depth)
	{
		foreach (UISprite mUIShadow in mUIShadowList)
		{
			Object.Destroy(mUIShadow.gameObject);
		}
		mUIShadowList.Clear();
		for (int i = 0; i < mData.ModelCount; i++)
		{
			float x = mData.GetPosition(i) * mData.GetScale(i) * mPartnerScaleList[i];
			float y = (mData.GetShadowOfs(i) + 10f) * mData.GetScale(i) * mPartnerScaleList[i] + mPartnerPosList[i].y;
			float num = mData.GetScale(i) * mPartnerScaleList[i];
			UISprite uISprite = Util.CreateSprite("Shadow", mScreenParent, atlas);
			Util.SetSpriteInfo(uISprite, spriteName, depth, new Vector3(x, y, 0f), new Vector3(num, num, 1f), false);
			mUIShadowList.Add(uISprite);
		}
	}

	public void EnableUIShadow(bool enable)
	{
		foreach (UISprite mUIShadow in mUIShadowList)
		{
			mUIShadow.enabled = enable;
		}
	}
}
