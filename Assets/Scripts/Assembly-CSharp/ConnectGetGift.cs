using System;
using System.Collections.Generic;

public class ConnectGetGift : ConnectBase
{
	protected class JsonSerializerTemp
	{
		public List<GetMailResponseGift> results { get; set; }
	}

	protected Server.MailType mMailType;

	protected string mJson;

	protected bool mNetworkDataModified;

	public void SetResponseData(Server.MailType a_kind, string a_json)
	{
		mMailType = a_kind;
		mJson = a_json;
	}

	protected override void InitServerFlg()
	{
		GameMain.mMapGiftCheckDone = false;
		GameMain.mMapGiftSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mMapGiftCheckDone = true;
		GameMain.mMapGiftSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mMapGiftCheckDone = true;
		GameMain.mMapGiftSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetGiftTime, DateTime.Now);
		long gET_GIFT_FREQ = GameMain.GET_GIFT_FREQ;
		if (gET_GIFT_FREQ > 0 && num < gET_GIFT_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			ServerFlgSucceeded();
			base.StateStart();
			return;
		}
		if (mParam == null)
		{
			mParam = new ConnectParameterBase();
			mParam.SetIgnoreError(true);
		}
		InitServerFlg();
		if (!Server.GetGift(mPlayer.Gifts.Count))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		try
		{
			JsonSerializerTemp obj = new JsonSerializerTemp();
			new MiniJSONSerializer().Populate(ref obj, mJson);
			List<GetMailResponseGift> list = new List<GetMailResponseGift>();
			if (obj != null)
			{
				list.AddRange(obj.results);
			}
			if (list != null && list.Count > 0)
			{
				List<int> list2 = new List<int>();
				foreach (GetMailResponseGift item in list)
				{
					GiftItemData giftItemData = item.GetGiftItemData();
					if (giftItemData.IsValid())
					{
						GiftItem giftItem = new GiftItem();
						giftItem.SetData(giftItemData);
						giftItem.Data.SetGiftSource(mMailType);
						mPlayer.AddGift(giftItem);
						list2.Add(giftItem.Data.RemoteID);
						mNetworkDataModified = true;
						if (giftItem.Data.ItemCategory == 20 && mGame.IsPlayAdvMode())
						{
							giftItem = new GiftItem();
							giftItem.SetData(giftItemData);
							giftItem.Data.SetGiftSource(mMailType);
							giftItem.Data.ItemCategory = 31;
							giftItem.Data.ItemKind = 1;
							mPlayer.AddGift(giftItem);
						}
					}
				}
				if (list2.Count > 0)
				{
					Server.GotGift(Server.MailType.GIFT, list2);
				}
			}
			mPlayer.Data.LastGetGiftTime = DateTime.Now;
			if (mNetworkDataModified)
			{
				mGame.Save();
			}
		}
		catch (Exception ex)
		{
			BIJLog.E("GetGift Error : " + ex);
		}
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastGetGiftTime = DateTimeUtil.UnitLocalTimeEpoch;
			mConnectResultKind = 3;
			base.StateConnectFinish();
			return false;
		}
		return false;
	}
}
