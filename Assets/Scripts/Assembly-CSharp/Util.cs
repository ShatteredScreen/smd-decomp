using System;
using System.IO;
using System.Text;
using UnityEngine;

public static class Util
{
	public enum BUTTON_TYPE
	{
		NORMAL = 0,
		NORMAL2 = 1
	}

	public class TriFunc
	{
		public enum MAXFUNC
		{
			STOP = 0,
			LOOP = 1
		}

		public float degree { get; set; }

		public float maxDegree { get; set; }

		public MAXFUNC maxFunc { get; set; }

		public TriFunc(float _initialDeg, float _maxDeg, MAXFUNC _maxFunc)
		{
			degree = _initialDeg;
			maxDegree = _maxDeg;
			maxFunc = _maxFunc;
		}

		public bool AddRadian(float deltaRad)
		{
			return AddDegree(deltaRad * 57.29578f);
		}

		public bool AddDegree(float deltaDeg)
		{
			bool result = false;
			degree += deltaDeg;
			if (degree >= maxDegree)
			{
				switch (maxFunc)
				{
				case MAXFUNC.STOP:
					degree = maxDegree;
					result = true;
					break;
				case MAXFUNC.LOOP:
					degree -= maxDegree;
					result = true;
					break;
				}
			}
			return result;
		}

		public float Sin()
		{
			return Mathf.Sin(degree * ((float)Math.PI / 180f));
		}

		public float Cos()
		{
			return Mathf.Cos(degree * ((float)Math.PI / 180f));
		}

		public void SetDegree(float deg)
		{
			degree = deg;
		}
	}

	public static ScreenOrientation ScreenOrientation
	{
		get
		{
			return Screen.orientation;
		}
		set
		{
			Screen.orientation = value;
		}
	}

	public static bool IsLandscape()
	{
		return ScreenOrientation == ScreenOrientation.LandscapeLeft || ScreenOrientation == ScreenOrientation.LandscapeRight;
	}

	public static Vector2 LogScreenRatio()
	{
		float num;
		if (ScreenOrientation == ScreenOrientation.LandscapeLeft || ScreenOrientation == ScreenOrientation.LandscapeRight)
		{
			num = ((!((float)Screen.width > (float)Screen.height)) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
			if (num < 0.5625f)
			{
				return new Vector2(0.5625f / num, 0.5625f);
			}
			return new Vector2(1f, num);
		}
		num = ((!((float)Screen.width > (float)Screen.height)) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
		if (num < 0.5625f)
		{
			return new Vector2(0.5625f, 0.5625f / num);
		}
		return new Vector2(num, 1f);
	}

	public static Vector2 LogScreenSize()
	{
		Vector2 vector = LogScreenRatio();
		return new Vector2(1136f * vector.x, 1136f * vector.y);
	}

	public static float LogLongSideScreenSize()
	{
		Vector2 vector = LogScreenSize();
		if (vector.x < vector.y)
		{
			return vector.y;
		}
		return vector.x;
	}

	public static bool IsWideScreen()
	{
		float num = ((!((float)Screen.width > (float)Screen.height)) ? ((float)Screen.width / (float)(Screen.height - 1)) : ((float)Screen.height / (float)(Screen.width - 1)));
		if (num < 0.5625f)
		{
			return true;
		}
		return false;
	}

	public static float ToLogX(float x)
	{
		return x * LogScreenRatio().x;
	}

	public static float ToLogY(float y)
	{
		return y * LogScreenRatio().y;
	}

	public static float ToDevX(float x)
	{
		int num = ((ScreenOrientation == ScreenOrientation.LandscapeLeft || ScreenOrientation == ScreenOrientation.LandscapeRight) ? ((Screen.width <= Screen.height) ? Screen.height : Screen.width) : ((Screen.width <= Screen.height) ? Screen.width : Screen.height));
		return x / LogScreenSize().x * (float)num;
	}

	public static float ToDevY(float y)
	{
		int num = ((ScreenOrientation == ScreenOrientation.LandscapeLeft || ScreenOrientation == ScreenOrientation.LandscapeRight) ? ((!((float)Screen.width > (float)Screen.height)) ? Screen.width : Screen.height) : ((!((float)Screen.width > (float)Screen.height)) ? Screen.height : Screen.width));
		return y / LogScreenSize().y * (float)num;
	}

	public static float CalculateCameraHeight()
	{
		float result = 0f;
		if (ScreenOrientation == ScreenOrientation.Portrait || ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			result = ToLogY(1136f);
		}
		else if (ScreenOrientation == ScreenOrientation.LandscapeLeft || ScreenOrientation == ScreenOrientation.LandscapeRight)
		{
			result = ToLogX(1136f);
		}
		return result;
	}

	public static GameObject CreateGameObject(string name, GameObject parent)
	{
		GameObject gameObject = new GameObject(name);
		if (parent != null)
		{
			gameObject.transform.parent = parent.transform;
			gameObject.layer = parent.layer;
		}
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localPosition = Vector3.zero;
		return gameObject;
	}

	public static GameObject SetGameObject(GameObject go, string name, GameObject parent)
	{
		go.name = name;
		if (parent != null)
		{
			go.transform.parent = parent.transform;
			go.layer = parent.layer;
		}
		go.transform.localScale = Vector3.one;
		go.transform.localPosition = Vector3.zero;
		return go;
	}

	public static GameObject SetGameObject(GameObject go, GameObject parent)
	{
		if (parent != null)
		{
			go.transform.parent = parent.transform;
			go.layer = parent.layer;
		}
		go.transform.localScale = Vector3.one;
		go.transform.localPosition = Vector3.zero;
		return go;
	}

	public static GameObject SetGameObjectInclueChild(GameObject go, string name, GameObject parent)
	{
		go.name = name;
		if (parent != null)
		{
			go.transform.parent = parent.transform;
			go.layer = parent.layer;
		}
		Transform[] componentsInChildren = go.GetComponentsInChildren<Transform>(true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			componentsInChildren[i].gameObject.layer = go.layer;
		}
		go.transform.localScale = Vector3.one;
		go.transform.localPosition = Vector3.zero;
		return go;
	}

	public static void ChangeParent(GameObject a_go, GameObject a_newparent)
	{
		if (a_newparent != null)
		{
			a_go.transform.parent = a_newparent.gameObject.transform;
		}
		else
		{
			a_go.transform.parent = null;
		}
	}

	public static void RedrawCommand(GameObject a_go)
	{
		NGUITools.SetActive(a_go, false);
		NGUITools.SetActive(a_go, true);
	}

	public static UIPanel CreatePanel(string name, GameObject parent, int depth, int sortOrder)
	{
		GameObject gameObject = CreateGameObject(name, parent);
		UIPanel uIPanel = gameObject.AddComponent<UIPanel>();
		uIPanel.depth = depth;
		uIPanel.sortingOrder = sortOrder;
		return uIPanel;
	}

	public static UIAnchor CreateAnchor(string name, GameObject parent, UIAnchor.Side side, Camera camera)
	{
		GameObject gameObject = CreateGameObject(name, parent);
		UIAnchor uIAnchor = gameObject.AddComponent<UIAnchor>();
		uIAnchor.side = side;
		uIAnchor.runOnlyOnce = false;
		uIAnchor.uiCamera = camera;
		uIAnchor.transform.localPosition = Vector3.zero;
		uIAnchor.transform.localScale = Vector3.one;
		return uIAnchor;
	}

	public static UIChildAnchor CreateAnchorWithChild(string name, GameObject parent, UIAnchor.Side side, Camera camera)
	{
		UIAnchor uIAnchor = CreateAnchor(name, parent, side, camera);
		return CreateGameObject("ChildAnchor", uIAnchor.gameObject).gameObject.AddComponent<UIChildAnchor>();
	}

	public static UISprite CreateSprite(string name, GameObject parent, BIJImage atlas)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.UISPRITE));
		SetGameObject(gameObject, name, parent);
		UISprite component = gameObject.GetComponent<UISprite>();
		BIJNGUIImage bIJNGUIImage = null;
		if (atlas is BIJNGUIImage)
		{
			bIJNGUIImage = (BIJNGUIImage)atlas;
		}
		if (bIJNGUIImage != null)
		{
			component.atlas = bIJNGUIImage.Atlas;
		}
		component.keepAspectRatio = UIWidget.AspectRatioSource.Free;
		component.autoResizeBoxCollider = true;
		return component;
	}

	public static void SetSpriteInfo(UISprite sprite, string imageName, int depth, Vector3 position, Vector3 scale, bool flip, UIWidget.Pivot _pivot = UIWidget.Pivot.Center)
	{
		sprite.pivot = _pivot;
		sprite.spriteName = imageName;
		sprite.depth = depth;
		UISpriteData uISpriteData = null;
		if (sprite.atlas != null)
		{
			uISpriteData = sprite.atlas.GetSprite(imageName);
		}
		sprite.gameObject.transform.localPosition = position;
		int num = 0;
		int h = 0;
		if (uISpriteData != null)
		{
			num = Mathf.RoundToInt((float)(uISpriteData.width + uISpriteData.paddingLeft + uISpriteData.paddingRight) * scale.x);
			h = Mathf.RoundToInt((float)(uISpriteData.height + uISpriteData.paddingTop + uISpriteData.paddingBottom) * scale.y);
		}
		if (flip)
		{
			sprite.SetDimensions(-num, h);
		}
		else
		{
			sprite.SetDimensions(num, h);
		}
	}

	public static void SetSpriteImageName(UISprite sprite, string imageName, Vector3 scale, bool flip)
	{
		sprite.spriteName = imageName;
		UISpriteData sprite2 = sprite.atlas.GetSprite(imageName);
		int num = 0;
		int h = 0;
		if (sprite2 != null)
		{
			num = Mathf.RoundToInt((float)(sprite2.width + sprite2.paddingLeft + sprite2.paddingRight) * scale.x);
			h = Mathf.RoundToInt((float)(sprite2.height + sprite2.paddingTop + sprite2.paddingBottom) * scale.y);
		}
		if (flip)
		{
			sprite.SetDimensions(-num, h);
		}
		else
		{
			sprite.SetDimensions(num, h);
		}
	}

	public static void SetSpriteScale(UISprite sprite, Vector3 scale, bool flip = false)
	{
		UISpriteData sprite2 = sprite.atlas.GetSprite(sprite.spriteName);
		if (sprite2 != null)
		{
			int num = Mathf.RoundToInt((float)(sprite2.width + sprite2.paddingLeft + sprite2.paddingRight) * scale.x);
			int h = Mathf.RoundToInt((float)(sprite2.height + sprite2.paddingTop + sprite2.paddingBottom) * scale.y);
			if (flip)
			{
				sprite.SetDimensions(-num, h);
			}
			else
			{
				sprite.SetDimensions(num, h);
			}
		}
	}

	public static void SetSpriteSize(UISprite sprite, int width, int height)
	{
		UISpriteData sprite2 = sprite.atlas.GetSprite(sprite.spriteName);
		if (sprite2 != null)
		{
			sprite.SetDimensions(width, height);
		}
	}

	public static UIButton CreateImageButton(string name, GameObject parent, BIJImage atlas)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.UIIMAGEBUTTON));
		SetGameObject(gameObject, name, parent);
		UISprite component = gameObject.GetComponent<UISprite>();
		UIButton component2 = gameObject.GetComponent<UIButton>();
		BIJNGUIImage bIJNGUIImage = atlas as BIJNGUIImage;
		if (bIJNGUIImage != null)
		{
			component.atlas = bIJNGUIImage.Atlas;
		}
		return component2;
	}

	public static UIButton CreateJellyImageButton(string name, GameObject parent, BIJImage atlas, BUTTON_TYPE a_type = BUTTON_TYPE.NORMAL)
	{
		GameObject gameObject = ((a_type == BUTTON_TYPE.NORMAL || a_type != BUTTON_TYPE.NORMAL2) ? ((GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.JELLYIMAGEBUTTON))) : ((GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.JELLYIMAGEBUTTON2))));
		SetGameObject(gameObject, name, parent);
		UISprite component = gameObject.GetComponent<UISprite>();
		UIButton component2 = gameObject.GetComponent<UIButton>();
		BIJNGUIImage bIJNGUIImage = atlas as BIJNGUIImage;
		if (bIJNGUIImage != null)
		{
			component.atlas = bIJNGUIImage.Atlas;
		}
		return component2;
	}

	public static void SetImageButtonInfo(UIButton button, string normal, string hover, string push, int depth, Vector3 position, Vector3 scale)
	{
		BoxCollider component = button.gameObject.GetComponent<BoxCollider>();
		UISprite component2 = button.gameObject.GetComponent<UISprite>();
		button.transform.localPosition = position;
		button.transform.localScale = scale;
		button.normalSprite = normal;
		button.hoverSprite = hover;
		button.pressedSprite = push;
		button.disabledSprite = normal;
		button.disabledColor = Color.white;
		button.defaultColor = Color.white;
		button.hover = Color.white;
		button.pressed = Color.white;
		button.duration = 0.1f;
		component2.color = Color.white;
		component2.depth = depth;
		SetSpriteImageName(component2, normal, Vector3.one, false);
		UISpriteData sprite = component2.atlas.GetSprite(normal);
		int num = 0;
		int num2 = 0;
		if (sprite != null)
		{
			num = Mathf.RoundToInt(sprite.width + sprite.paddingLeft + sprite.paddingRight);
			num2 = Mathf.RoundToInt(sprite.height + sprite.paddingTop + sprite.paddingBottom);
		}
		component.size = new Vector3(num, num2, 1f);
	}

	public static void SetImageButtonInfoScale(UIButton button, string normal, string hover, string push, int depth, Vector3 position, Vector3 scale)
	{
		BoxCollider component = button.gameObject.GetComponent<BoxCollider>();
		UISprite component2 = button.gameObject.GetComponent<UISprite>();
		button.transform.localPosition = position;
		button.transform.localScale = Vector3.one;
		button.normalSprite = normal;
		button.hoverSprite = hover;
		button.pressedSprite = push;
		button.disabledSprite = normal;
		button.disabledColor = Color.white;
		button.defaultColor = Color.white;
		button.hover = Color.white;
		button.pressed = Color.white;
		button.duration = 0.1f;
		component2.color = Color.white;
		component2.depth = depth;
		SetSpriteImageName(component2, normal, scale, false);
		UISpriteData sprite = component2.atlas.GetSprite(normal);
		int num = 0;
		int num2 = 0;
		if (sprite != null)
		{
			num = Mathf.RoundToInt((float)(sprite.width + sprite.paddingLeft + sprite.paddingRight) * scale.x);
			num2 = Mathf.RoundToInt((float)(sprite.height + sprite.paddingTop + sprite.paddingBottom) * scale.y);
		}
		component.size = new Vector3(num, num2, 1f);
	}

	public static void AddImageButtonMessage(UIButton button, MonoBehaviour target, string funcName, UIButtonMessage.Trigger trigger)
	{
		UIEventTrigger component = button.gameObject.GetComponent<UIEventTrigger>();
		EventDelegate eventDelegate = new EventDelegate(target, funcName);
		if (eventDelegate.parameters != null)
		{
			EventDelegate.Parameter parameter = eventDelegate.parameters[0];
			parameter.obj = button.gameObject;
		}
		switch (trigger)
		{
		case UIButtonMessage.Trigger.OnClick:
			component.onClick.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnPress:
			component.onPress.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnRelease:
			component.onRelease.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnMouseOver:
		case UIButtonMessage.Trigger.OnMouseOut:
			break;
		}
	}

	public static void SetImageButtonGraphic(UIButton button, string normal, string hover, string push)
	{
		SetImageButtonGraphic(button, normal, hover, push, Vector3.one);
	}

	public static void SetImageButtonGraphic(UIButton button, string normal, string hover, string push, Vector3 scale)
	{
		BoxCollider component = button.gameObject.GetComponent<BoxCollider>();
		UISprite component2 = button.gameObject.GetComponent<UISprite>();
		button.normalSprite = normal;
		button.hoverSprite = hover;
		button.pressedSprite = push;
		SetSpriteImageName(component2, normal, scale, false);
		UISpriteData sprite = component2.atlas.GetSprite(normal);
		int num = 0;
		int num2 = 0;
		if (sprite != null)
		{
			num = Mathf.RoundToInt(sprite.width + sprite.paddingLeft + sprite.paddingRight);
			num2 = Mathf.RoundToInt(sprite.height + sprite.paddingTop + sprite.paddingBottom);
		}
		component.size = new Vector3(num, num2, 1f);
	}

	public static void SetImageButtonMessage(UIButton button, MonoBehaviour target, string funcName, UIButtonMessage.Trigger trigger)
	{
		UIEventTrigger component = button.gameObject.GetComponent<UIEventTrigger>();
		EventDelegate eventDelegate = new EventDelegate(target, funcName);
		if (eventDelegate.parameters != null)
		{
			EventDelegate.Parameter parameter = eventDelegate.parameters[0];
			parameter.obj = button.gameObject;
		}
		switch (trigger)
		{
		case UIButtonMessage.Trigger.OnClick:
			component.onClick.Clear();
			component.onClick.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnPress:
			component.onPress.Clear();
			component.onPress.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnRelease:
			component.onRelease.Clear();
			component.onRelease.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnMouseOver:
		case UIButtonMessage.Trigger.OnMouseOut:
			break;
		}
	}

	public static void SetImageButtonColor(UIButton button, Color color)
	{
		button.disabledColor = color;
		button.defaultColor = color;
		button.hover = color;
		button.pressed = color;
		button.UpdateColor(true);
	}

	public static UITexture CreateJellyTextureButton(string name, GameObject parent, Texture2D textureData)
	{
		GameObject gameObject = CreateGameObject(name, parent);
		UITexture uITexture = gameObject.AddComponent<UITexture>();
		uITexture.mainTexture = textureData;
		uITexture.width = textureData.width;
		uITexture.height = textureData.height;
		BoxCollider boxCollider = gameObject.AddComponent<BoxCollider>();
		boxCollider.size = new Vector3(textureData.width, textureData.height, 1f);
		gameObject.AddComponent<UIEventTrigger>();
		gameObject.AddComponent<JellyTextureButton>();
		return uITexture;
	}

	public static void SetTextureButtonCallback(UITexture texture, MonoBehaviour target, string funcName, UIButtonMessage.Trigger trigger)
	{
		UIEventTrigger component = texture.gameObject.GetComponent<UIEventTrigger>();
		EventDelegate eventDelegate = new EventDelegate(target, funcName);
		if (eventDelegate.parameters != null)
		{
			EventDelegate.Parameter parameter = eventDelegate.parameters[0];
			parameter.obj = texture.gameObject;
		}
		switch (trigger)
		{
		case UIButtonMessage.Trigger.OnClick:
			component.onClick.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnPress:
			component.onPress.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnRelease:
			component.onRelease.Add(eventDelegate);
			break;
		case UIButtonMessage.Trigger.OnMouseOver:
		case UIButtonMessage.Trigger.OnMouseOut:
			break;
		}
	}

	public static SsSprite CreateSsSprite(string name, GameObject parent, string _animkey, Vector3 _pos, Vector3 _scl)
	{
		GameObject gameObject = CreateGameObject(name, parent);
		SsSprite ssSprite = gameObject.AddComponent<SsSprite>();
		ssSprite.Position = _pos;
		ssSprite.Scale = _scl;
		ssSprite.Rotation = new Vector3(0f, 0f, 0f);
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_animkey);
		ssSprite.Animation = resSsAnimation.SsAnime;
		ssSprite.Play();
		return ssSprite;
	}

	public static UISsSprite CreateUISsSprite(string name, GameObject parent, string _animkey, Vector3 _pos, Vector3 _scl, int _depth)
	{
		GameObject gameObject = CreateGameObject(name, parent);
		UISsSprite uISsSprite = gameObject.AddComponent<UISsSprite>();
		uISsSprite.Position = _pos;
		uISsSprite.Scale = _scl;
		uISsSprite.Rotation = new Vector3(0f, 0f, 0f);
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_animkey);
		uISsSprite.Animation = resSsAnimation.SsAnime;
		uISsSprite.depth = _depth;
		uISsSprite.Play();
		return uISsSprite;
	}

	public static UISsSpriteP CreateUISsSpriteP(string name, GameObject parent, string _animkey, Vector3 _pos, Vector3 _scl, int _depth)
	{
		GameObject gameObject = CreateGameObject(name, parent);
		UISsSpriteP uISsSpriteP = gameObject.AddComponent<UISsSpriteP>();
		uISsSpriteP.Position = _pos;
		uISsSpriteP.Scale = _scl;
		uISsSpriteP.Rotation = new Vector3(0f, 0f, 0f);
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_animkey);
		uISsSpriteP.Animation = resSsAnimation.SsAnime;
		uISsSpriteP.depth = _depth;
		uISsSpriteP.Play();
		return uISsSpriteP;
	}

	public static OneShotAnime CreateOneShotAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas, bool noPlay)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.ONESHOTANIME));
		SetGameObject(gameObject, name, parent);
		OneShotAnime component = gameObject.GetComponent<OneShotAnime>();
		component.Init(key, changePartsArray, atlas, pos, scale, noPlay);
		return component;
	}

	public static void SetImageButtonSize(UIButton button, int width, int height, UIBasicSprite.Type spriteType)
	{
		BoxCollider component = button.gameObject.GetComponent<BoxCollider>();
		UISprite component2 = button.gameObject.GetComponent<UISprite>();
		component2.SetDimensions(width, height);
		component2.type = spriteType;
		component.size = new Vector3(width, height, 1f);
	}

	public static UILabel CreateLabel(string name, GameObject parent, UIFont atlasFont)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.UILABEL));
		SetGameObject(gameObject, name, parent);
		UILabel component = gameObject.GetComponent<UILabel>();
		component.bitmapFont = atlasFont;
		return component;
	}

	public static void SetLabelInfo(UILabel label, string text, int depth, Vector3 position, int fontSize, int linewidth, int linecount, UIWidget.Pivot pivot)
	{
		label.text = text;
		label.color = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
		label.depth = depth;
		label.fontSize = fontSize;
		label.lineWidth = linewidth;
		label.maxLineCount = linecount;
		label.overflowMethod = UILabel.Overflow.ResizeFreely;
		label.pivot = pivot;
		label.gameObject.transform.localPosition = position;
		label.gameObject.transform.localScale = Vector3.one;
	}

	public static void SetLabelText(UILabel label, string text)
	{
		label.text = text;
	}

	public static void SetLabelColor(UILabel label, Color32 color)
	{
		label.color = color;
	}

	public static string MakeLText(string key, params object[] param)
	{
		return string.Format(Localization.Get(key), param);
	}

	public static void SetLabeShrinkContent(UILabel _label, int _w, int _h)
	{
		_label.overflowMethod = UILabel.Overflow.ShrinkContent;
		_label.SetDimensions(_w, _h);
	}

	public static UIInput CreateInput(string name, GameObject parent)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.UIINPUT));
		SetGameObject(gameObject, name, parent);
		return gameObject.GetComponent<UIInput>();
	}

	public static void SetInputInfo(UIInput input, UIFont font, int fontSize, int characterLimit, BIJImage atlas, string imageName, int depth, Vector3 position, Vector3 scale, int DimensionX = 0, int DimensionY = 0, UIWidget.Pivot pivot = UIWidget.Pivot.Center, float innerOffsetX = 0f, string StartDesc = "")
	{
		UISprite uISprite = CreateSprite("bg", input.gameObject, atlas);
		uISprite.pivot = pivot;
		SetSpriteInfo(uISprite, imageName, depth, Vector3.zero, Vector3.one, false);
		if (DimensionX != 0 && DimensionY != 0)
		{
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(DimensionX, DimensionY);
		}
		UILabel uILabel = CreateLabel("inner", input.gameObject, font);
		SetLabelInfo(uILabel, StartDesc, depth + 1, new Vector3(innerOffsetX, 0f, 0f), fontSize, 0, 1, pivot);
		uILabel.color = Color.black;
		SetInputCollider(input, uISprite);
		input.label = uILabel;
		input.characterLimit = characterLimit;
		input.activeTextColor = Color.black;
		input.caretColor = Color.black;
		input.transform.localPosition = position;
		input.transform.localScale = scale;
		SetInputType(input);
	}

	public static void SetInputCollider(UIInput input, UISprite back, float rate = 1f)
	{
		BoxCollider component = input.GetComponent<BoxCollider>();
		Bounds bounds = NGUIMath.CalculateRelativeWidgetBounds(back.transform);
		component.center = new Vector3(bounds.center.x, bounds.center.y, 0f);
		component.size = new Vector3(back.drawingDimensions.z - back.drawingDimensions.x, back.drawingDimensions.w - back.drawingDimensions.y, 0f) * rate;
	}

	public static void SetInputType(UIInput input, UIInput.InputType inputType = UIInput.InputType.Standard, UIInput.KeyboardType keyboradType = UIInput.KeyboardType.Default, UIInput.Validation validationType = UIInput.Validation.None)
	{
		input.inputType = inputType;
		input.keyboardType = keyboradType;
		input.validation = validationType;
	}

	public static void SetInputOnSubmit(UIInput input, EventDelegate.Callback callback)
	{
		input.onSubmit.Add(new EventDelegate(callback));
	}

	public static void SetInputOnSubmit(UIInput input, MonoBehaviour target, string funcName, GameObject a_gameobject)
	{
		EventDelegate eventDelegate = new EventDelegate(target, funcName);
		if (eventDelegate.parameters != null)
		{
			EventDelegate.Parameter parameter = eventDelegate.parameters[0];
			parameter.obj = a_gameobject;
		}
		input.onSubmit.Add(eventDelegate);
	}

	public static void SetInputOnChange(UIInput input, EventDelegate.Callback callback)
	{
		input.onChange.Add(new EventDelegate(callback));
	}

	public static void SetInputOnChange(UIInput input, MonoBehaviour target, string funcName, GameObject a_gameobject)
	{
		EventDelegate eventDelegate = new EventDelegate(target, funcName);
		if (eventDelegate.parameters != null)
		{
			EventDelegate.Parameter parameter = eventDelegate.parameters[0];
			parameter.obj = a_gameobject;
		}
		input.onChange.Add(eventDelegate);
	}

	public static GameObject CreateSpriteObject(string name, GameObject parent, BIJImage atlas)
	{
		return CreateGameObject(name, parent);
	}

	public static OneShotAnime CreateOneShotAnimeNoInit(string name, GameObject parent)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.ONESHOTANIME));
		SetGameObject(gameObject, name, parent);
		return gameObject.GetComponent<OneShotAnime>();
	}

	public static OneShotAnime CreateOneShotAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, float rotateDeg, float delay, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas, bool autoDestroy = true, SsSprite.AnimationCallback onFinished = null)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.ONESHOTANIME));
		SetGameObject(gameObject, name, parent);
		OneShotAnime component = gameObject.GetComponent<OneShotAnime>();
		component.Init(key, changePartsArray, atlas, pos, scale, rotateDeg, delay, autoDestroy, onFinished);
		return component;
	}

	public static OneShotAnime CreateOneShotAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy = true, SsSprite.AnimationCallback onFinished = null)
	{
		return CreateOneShotAnime(name, key, parent, pos, scale, rotateDeg, delay, null, null, autoDestroy, onFinished);
	}

	public static OneShotAnime CreateOneShotAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, float rotateDeg, float delay, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas, bool autoDestroy, OneShotAnime.AnimationCallback onFinished)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.ONESHOTANIME));
		SetGameObject(gameObject, name, parent);
		OneShotAnime component = gameObject.GetComponent<OneShotAnime>();
		component.Init(key, changePartsArray, atlas, pos, scale, rotateDeg, delay, autoDestroy, onFinished);
		return component;
	}

	public static OneShotAnime CreateOneShotAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, OneShotAnime.AnimationCallback onFinished)
	{
		return CreateOneShotAnime(name, key, parent, pos, scale, rotateDeg, delay, null, null, autoDestroy, onFinished);
	}

	public static LoopAnime CreateLoopAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, OneShotAnime.AnimationCallback onFinished)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.LOOPANIME));
		SetGameObject(gameObject, name, parent);
		LoopAnime component = gameObject.GetComponent<LoopAnime>();
		component.ParentGO = gameObject;
		component.Init(key, pos, scale, rotateDeg, delay, autoDestroy, onFinished);
		return component;
	}

	public static LoopAnime CreateLoopAnime(string name, string key, GameObject parent, Vector3 pos, Vector3 scale, float rotateDeg, float delay, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas)
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.LOOPANIME));
		SetGameObject(gameObject, name, parent);
		LoopAnime component = gameObject.GetComponent<LoopAnime>();
		component.ParentGO = gameObject;
		component.Init(key, changePartsArray, atlas, pos, scale, rotateDeg, delay, false);
		return component;
	}

	public static SimpleSprite CreateSimpleSprite(string _name, GameObject _parent, BIJImage _atlas, string _sprName, UIWidget.Pivot _pv, Vector3 _position, Vector3 _scale)
	{
		GameObject gameObject = new GameObject(_name);
		gameObject.transform.parent = _parent.transform;
		gameObject.layer = _parent.layer;
		SimpleSprite simpleSprite = gameObject.AddComponent<SimpleSprite>();
		simpleSprite.mAtlas = _atlas;
		simpleSprite.Pivot = _pv;
		simpleSprite.mSpriteName = _sprName;
		simpleSprite.Init();
		simpleSprite._pos = _position;
		simpleSprite._scl = _scale;
		simpleSprite._rot = Vector3.zero;
		return simpleSprite;
	}

	public static SimpleSprite AddSimpleSprite(GameObject _go, BIJImage _atlas, string _sprName, UIWidget.Pivot _pv, Vector3 _position, Vector3 _scale)
	{
		SimpleSprite simpleSprite = _go.GetComponent<SimpleSprite>();
		if (simpleSprite == null)
		{
			simpleSprite = _go.AddComponent<SimpleSprite>();
		}
		simpleSprite.mAtlas = _atlas;
		simpleSprite.Pivot = _pv;
		simpleSprite.mSpriteName = _sprName;
		simpleSprite.Init();
		simpleSprite._pos = _position;
		simpleSprite._scl = _scale;
		simpleSprite._rot = Vector3.zero;
		return simpleSprite;
	}

	public static TriFunc CreateTriFunc(float initialDeg, float maxDeg, TriFunc.MAXFUNC maxFunc)
	{
		return new TriFunc(initialDeg, maxDeg, maxFunc);
	}

	public static OneShotParticle CreateOneShotParticle(string name, string prefabPath, GameObject parent, Vector3 pos, Vector3 scale, float lifeTime, float delayTime)
	{
		SMParticle sMParticle = CreateGameObject(name, parent).AddComponent<SMParticle>();
		sMParticle.LoadFromPrefab(prefabPath);
		sMParticle.gameObject.transform.localScale = scale;
		sMParticle.gameObject.transform.localPosition = pos;
		OneShotParticle oneShotParticle = sMParticle.gameObject.AddComponent<OneShotParticle>();
		oneShotParticle.Init(lifeTime, delayTime);
		return oneShotParticle;
	}

	public static SMParticle CreateSMParticle(string name, string prefabPath, GameObject parent, Vector3 pos, Vector3 scale, int a_layer)
	{
		SMParticle sMParticle = CreateGameObject(name, parent).AddComponent<SMParticle>();
		sMParticle.gameObject.layer = a_layer;
		sMParticle.LoadFromPrefab(prefabPath);
		sMParticle.gameObject.transform.localScale = scale;
		sMParticle.gameObject.transform.localPosition = pos;
		return sMParticle;
	}

	public static GameObject CreateNewPSFromPrefab(string _name, GameObject parent, string _path)
	{
		GameObject particle = GameMain.GetParticle(_path);
		particle.name = _name;
		if (parent != null)
		{
			particle.layer = parent.layer;
			particle.transform.parent = parent.transform;
			Transform[] componentsInChildren = particle.GetComponentsInChildren<Transform>(true);
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].gameObject.layer = particle.layer;
			}
		}
		particle.transform.localPosition = Vector3.zero;
		particle.transform.localScale = Vector3.one;
		particle.transform.localRotation = Quaternion.identity;
		return particle;
	}

	public static void JumpToStore(bool a_quit = true)
	{
		string url = string.Format(SingletonMonoBehaviour<Network>.Instance.Store_URL, BIJUnity.getCountryCode());
		Application.OpenURL(url);
		if (a_quit)
		{
			GameMain.ApplicationQuit();
		}
	}

	public static int CommonLottery(int[] ratioTbl)
	{
		int num = 0;
		for (int i = 0; i < ratioTbl.Length; i++)
		{
			num += ratioTbl[i];
		}
		int num2 = UnityEngine.Random.Range(0, num);
		int result = -1;
		num = 0;
		for (int j = 0; j < ratioTbl.Length; j++)
		{
			num += ratioTbl[j];
			if (num2 < num)
			{
				result = j;
				break;
			}
		}
		return result;
	}

	public static bool CommonLottery(int hitRatio, int maxRatio)
	{
		bool result = false;
		if (UnityEngine.Random.Range(0, maxRatio) < hitRatio)
		{
			result = true;
		}
		return result;
	}

	public static int CalcBonusToInt(int value, float ratio, float additional_ratio, params int[] additional_bonus)
	{
		int num = Mathf.CeilToInt((float)value * ratio);
		num = Mathf.CeilToInt((float)num * additional_ratio);
		if (additional_bonus != null && additional_bonus.Length > 0)
		{
			foreach (int num2 in additional_bonus)
			{
				num += num2;
			}
		}
		return num;
	}

	public static string ConvertInfoBannerFileName(string src)
	{
		string text = ".png";
		if (Path.GetExtension(src) != text)
		{
			return src;
		}
		string value = src.Replace(Path.GetFileName(src), string.Empty);
		string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(src);
		BIJMD5 bIJMD = new BIJMD5(fileNameWithoutExtension + Def.HASH_SALT);
		StringBuilder stringBuilder = new StringBuilder("_");
		for (int i = 0; i < 4; i++)
		{
			stringBuilder.Append(bIJMD.Hash[i].ToString("x2"));
		}
		bIJMD = null;
		return new StringBuilder().Append(value).Append(fileNameWithoutExtension).Append(stringBuilder.ToString())
			.Append(text)
			.ToString();
	}
}
