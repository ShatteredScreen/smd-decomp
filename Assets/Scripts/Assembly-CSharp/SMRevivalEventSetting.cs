public class SMRevivalEventSetting : SMSeasonEventSetting
{
	public int PaidGemNum { get; set; }

	public int AccessoryID { get; set; }

	public override void Serialize(BIJFileBinaryWriter sw)
	{
		base.Serialize(sw);
		sw.WriteInt(PaidGemNum);
		sw.WriteInt(AccessoryID);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		base.Deserialize(a_reader);
		PaidGemNum = a_reader.ReadInt();
		AccessoryID = a_reader.ReadInt();
	}
}
