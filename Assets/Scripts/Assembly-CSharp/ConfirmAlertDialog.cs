using System.Collections.Generic;
using UnityEngine;

public class ConfirmAlertDialog : ConfirmDialog
{
	public class LabelSetting
	{
		public int FontSize { get; set; }

		public UIWidget.Pivot Pivot { get; set; }

		public Vector3 Position { get; set; }

		public string Description { get; set; }

		public bool UseColor { get; set; }

		public Color DescColor { get; set; }

		public UILabel.Overflow Overflow { get; set; }

		public Vector2 Dimension { get; set; }

		public LabelSetting()
		{
			FontSize = 26;
			UseColor = false;
			DescColor = Def.DEFAULT_MESSAGE_COLOR;
			Pivot = UIWidget.Pivot.Center;
			Overflow = UILabel.Overflow.ShrinkContent;
			Dimension = new Vector2(400f, FontSize * 2 + 10);
		}
	}

	private List<LabelSetting> mSetting;

	public override void BuildDialog()
	{
		if (mSetting == null)
		{
			base.BuildDialog();
			return;
		}
		if (ReservedSortingOrder > 0)
		{
			mPanel.sortingOrder = ReservedSortingOrder;
			mPanel.depth = ReservedSortingOrder;
		}
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(mDialogSize);
		if (mIsUseTitle)
		{
			InitDialogTitle(mTitle);
		}
		for (int i = 0; i < mSetting.Count; i++)
		{
			LabelSetting labelSetting = mSetting[i];
			UILabel uILabel = Util.CreateLabel("Desc" + i, base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, labelSetting.Description, mBaseDepth + 4 + i, labelSetting.Position, labelSetting.FontSize, 0, 0, labelSetting.Pivot);
			uILabel.SetDimensions((int)labelSetting.Dimension.x, (int)labelSetting.Dimension.y);
			uILabel.overflowMethod = labelSetting.Overflow;
			uILabel.spacingY = 6;
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			if (labelSetting.UseColor)
			{
				uILabel.color = labelSetting.DescColor;
			}
		}
		if (mRequestCreateButton)
		{
			TryCreateButton();
		}
	}

	public void SetLabelSetting(List<LabelSetting> a_list)
	{
		if (a_list != null)
		{
			mSetting = new List<LabelSetting>(a_list);
		}
	}
}
