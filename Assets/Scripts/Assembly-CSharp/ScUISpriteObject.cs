using UnityEngine;

public class ScUISpriteObject : MonoBehaviour
{
	public UISsSprite _sprite;

	public string _animationName;

	private Transform _transform;

	public Vector3 _pos
	{
		get
		{
			return _transform.localPosition;
		}
		set
		{
			_transform.localPosition = value;
		}
	}

	public float _xpos
	{
		get
		{
			return _pos.x;
		}
		set
		{
			Vector3 pos = _pos;
			pos.x = value;
			_pos = pos;
		}
	}

	public float _ypos
	{
		get
		{
			return _pos.y;
		}
		set
		{
			Vector3 pos = _pos;
			pos.y = value;
			_pos = pos;
		}
	}

	public float _zpos
	{
		get
		{
			return _pos.z;
		}
		set
		{
			Vector3 pos = _pos;
			pos.z = value;
			_pos = pos;
		}
	}

	public Vector3 _rot
	{
		get
		{
			return _transform.localRotation.eulerAngles;
		}
		set
		{
			Quaternion identity = Quaternion.identity;
			identity.eulerAngles = value;
			_transform.localRotation = identity;
		}
	}

	public float _xrot
	{
		get
		{
			return _rot.x;
		}
		set
		{
			Vector3 rot = _rot;
			rot.x = value;
			_rot = rot;
		}
	}

	public float _yrot
	{
		get
		{
			return _rot.y;
		}
		set
		{
			Vector3 rot = _rot;
			rot.y = value;
			_rot = rot;
		}
	}

	public float _zrot
	{
		get
		{
			return _rot.z;
		}
		set
		{
			Vector3 rot = _rot;
			rot.z = value;
			_rot = rot;
		}
	}

	public Vector3 _scl
	{
		get
		{
			return _transform.localScale;
		}
		set
		{
			_transform.localScale = value;
		}
	}

	public float _xscl
	{
		get
		{
			return _scl.x;
		}
		set
		{
			Vector3 scl = _scl;
			scl.x = value;
			_scl = scl;
		}
	}

	public float _yscl
	{
		get
		{
			return _scl.y;
		}
		set
		{
			Vector3 scl = _scl;
			scl.y = value;
			_scl = scl;
		}
	}

	public float _zscl
	{
		get
		{
			return _scl.z;
		}
		set
		{
			Vector3 scl = _scl;
			scl.z = value;
			_scl = scl;
		}
	}

	public virtual void Awake()
	{
		_transform = base.transform;
		_sprite = base.gameObject.GetComponent<UISsSprite>();
		if (_sprite == null)
		{
			_sprite = base.gameObject.AddComponent<UISsSprite>();
		}
		_transform.localScale = new Vector3(1f, 1f, 1f);
	}

	public virtual void Start()
	{
	}

	public virtual void Update()
	{
	}

	public virtual void OnDestroy()
	{
		CleanupAnimation();
	}

	public virtual bool EqulasAnime(string name)
	{
		return (bool)_sprite.Animation && _sprite.Animation.name.Equals(name);
	}

	protected virtual void CleanupAnimation()
	{
		UISsPart[] parts = _sprite.GetParts();
		if (parts != null)
		{
			for (int i = 0; i < parts.Length; i++)
			{
				parts[i].RevertChangedMaterial();
			}
		}
		if (_sprite._materials != null)
		{
			for (int j = 0; j < _sprite._materials.Length; j++)
			{
				_sprite._materials[j] = null;
			}
		}
		_sprite.Animation = null;
	}

	public virtual bool ChangeAnime(string fileName)
	{
		if (fileName == _animationName)
		{
			return false;
		}
		CleanupAnimation();
		_animationName = fileName;
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_animationName);
		_sprite.Animation = resSsAnimation.SsAnime;
		_sprite.Play();
		return true;
	}
}
