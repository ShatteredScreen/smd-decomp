using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ImageTextDescDialog : DialogBase
{
	private enum LOAD_ATLAS
	{
		HUD = 0
	}

	private enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		LOAD_LINKBANNER = 3
	}

	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum BUTTONS
	{
		OK = 0,
		CLOSE = 1,
		RETRY = 2,
		CONFIRM = 3,
		CHOICE = 4,
		TIMER = 5,
		GOTO_OLD_EVENT = 6,
		YES_NO = 7,
		CONFIRM_CLOSE = 8,
		RETRY_CLOSE = 9
	}

	public sealed class AtlasInfo : IDisposable
	{
		private bool mInit;

		private bool mForceNotUnload;

		private bool mDisposed;

		public ResImage Resource { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Resource == null) ? null : Resource.Image;
			}
		}

		public string Key { get; private set; }

		public AtlasInfo(string key)
		{
			Key = key;
		}

		~AtlasInfo()
		{
			Dispose(false);
		}

		public IEnumerator Load(bool not_unload = false)
		{
			if (Resource != null)
			{
				UnLoad();
			}
			if (Res.ResImageDict.ContainsKey(Key) && ResourceManager.IsLoaded(Key))
			{
				Resource = ResourceManager.LoadImage(Key);
				if (Resource != null)
				{
					yield break;
				}
			}
			Resource = ResourceManager.LoadImageAsync(Key);
			if (Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.INIT)
			{
				mInit = true;
				mForceNotUnload = not_unload;
			}
		}

		public void UnLoad()
		{
			if (mInit && !mForceNotUnload)
			{
				ResourceManager.UnloadImage(Key);
			}
			mInit = false;
		}

		public bool IsDone()
		{
			return !mInit || (mInit && Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.DONE);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	public interface IAtlasLoadable
	{
		string Atlas { get; set; }

		string Sprite { get; set; }

		bool IsNotUnloadAtlas { get; set; }

		UIBasicSprite.Type SpriteType { get; set; }
	}

	public sealed class LoadInnerAtlasOption : IAtlasLoadable
	{
		public string Name { get; set; }

		public string Atlas { get; set; }

		public string Sprite { get; set; }

		public int OffsetDepth { get; set; }

		public bool IsNotUnloadAtlas { get; set; }

		public Vector3 LocalPosition { get; set; }

		public Vector3 LocalScale { get; set; }

		public bool IsFlip { get; set; }

		public UIWidget.Pivot Pivot { get; set; }

		public bool IsDimensions { get; set; }

		public IntVector2 Dimensions { get; set; }

		public UIBasicSprite.Type SpriteType { get; set; }

		public LoadInnerAtlasOption()
		{
			Name = "Child";
			Atlas = "HUD";
			Sprite = "null";
			OffsetDepth = 1;
			IsNotUnloadAtlas = false;
			LocalPosition = Vector3.zero;
			LocalScale = Vector3.one;
			IsFlip = false;
			Pivot = UIWidget.Pivot.Center;
			IsDimensions = false;
			Dimensions = new IntVector2(0, 0);
			SpriteType = UIBasicSprite.Type.Simple;
		}
	}

	public abstract class ConfigBase
	{
		public const float CLOSE_TIMER_INFINITY = -1f;

		public string Desc { get; set; }

		public Color DescColor { get; set; }

		public int DescRowMin { get; set; }

		public bool IsShowCancelButton { get; set; }

		public string Title { get; set; }

		public BUTTONS Buttons { get; set; }

		public bool InitInvisibleButton { get; set; }

		public bool InitInvisibleCancelButton { get; set; }

		public bool CanBackKeyByPositiveOnly { get; set; }

		public bool DisableSE { get; set; }

		public int ImageWidth { get; set; }

		public int ImageHeight { get; set; }

		public bool IsAutoResizeLoadedImage { get; set; }

		public float SpacingHeight { get; set; }

		public float HeaderHeight { get; set; }

		public float FooterHeight { get; set; }

		public DIALOG_SIZE PresetSize { get; set; }

		public bool UsePresetSize { get; set; }

		public string ChoicePositiveHudSprite { get; set; }

		public string ChoiceNegativeHudSprite { get; set; }

		public float CloseTimer { get; set; }

		public UnityAction<SELECT_ITEM> ClosedEvent { get; set; }

		public virtual float ImageAndSpacingHeight
		{
			get
			{
				return (float)ImageHeight + SpacingHeight;
			}
		}

		public ConfigBase()
		{
			Buttons = BUTTONS.CLOSE;
			Desc = string.Empty;
			DescColor = Def.DEFAULT_MESSAGE_COLOR;
			CloseTimer = -1f;
			IsAutoResizeLoadedImage = true;
			DescRowMin = 2;
			SpacingHeight = 40f;
			HeaderHeight = 40f;
			FooterHeight = 40f;
			ClosedEvent = null;
		}
	}

	public class ConfigAtlas : ConfigBase, IAtlasLoadable
	{
		public string Atlas { get; set; }

		public string Sprite { get; set; }

		public bool IsNotUnloadAtlas { get; set; }

		public UIBasicSprite.Type SpriteType { get; set; }

		public ConfigAtlas()
		{
			Atlas = "HUD";
			Sprite = "null";
			IsNotUnloadAtlas = false;
		}
	}

	public sealed class ConfigLinkBanner : ConfigBase
	{
		public string BaseURL { get; set; }

		public string FileName { get; set; }
	}

	public sealed class ConfigNoImage : ConfigBase
	{
		public override float ImageAndSpacingHeight
		{
			get
			{
				return 0f;
			}
		}

		public ConfigNoImage()
		{
			base.ImageHeight = 0;
			base.IsAutoResizeLoadedImage = false;
		}
	}

	public class ConfigAtlasInAtlases : ConfigAtlas
	{
		public List<LoadInnerAtlasOption> InnerAtlases { private get; set; }

		public LoadInnerAtlasOption[] InnerAtlasesArray
		{
			get
			{
				return (InnerAtlases == null) ? null : InnerAtlases.ToArray();
			}
		}

		public ConfigAtlasInAtlases()
		{
			InnerAtlases = null;
		}
	}

	public sealed class ConfigAtlasInAtlasesSorting : ConfigAtlasInAtlases
	{
		public enum Arrangement
		{
			Horizontal = 0,
			Vertical = 1
		}

		public Arrangement Sorting { get; set; }

		public Vector2 SortingLocalPosition { get; set; }

		public UIWidget.Pivot SortingPivot { get; set; }

		public int SortingMaxPerLine { get; set; }

		public Vector2 SortingCell { get; set; }

		public ConfigAtlasInAtlasesSorting()
		{
			Sorting = Arrangement.Horizontal;
			SortingLocalPosition = Vector2.zero;
			SortingPivot = UIWidget.Pivot.TopLeft;
			SortingMaxPerLine = 0;
			SortingCell = new Vector2(200f, 200f);
		}
	}

	private const float BUTTON_HEIGHT = 84f;

	private GameObject mGameObject;

	private Dictionary<LOAD_ATLAS, AtlasInfo> mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();

	private Dictionary<string, AtlasInfo> mCustomAtlas = new Dictionary<string, AtlasInfo>();

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private Dictionary<SELECT_ITEM, UnityEvent> mCallback;

	private SELECT_ITEM mSelectedItem;

	private bool mIsInit;

	private ConfigBase mConfig;

	private bool mIsCreatedButton;

	private bool mHasNegativeButton;

	private float mCurrentCloseTimer = -1f;

	private LinkBannerInfo mLinkBanner;

	private LinkBannerRequest mLinkBannerReq;

	private bool mIsOpened;

	private GameObject GameObject
	{
		get
		{
			if (mGameObject == null)
			{
				mGameObject = base.gameObject;
			}
			return mGameObject;
		}
	}

	public override void Update()
	{
		base.Update();
		if (!IsBusy() && mConfig != null && mConfig.Buttons == BUTTONS.TIMER && mCurrentCloseTimer >= 0f)
		{
			mCurrentCloseTimer -= Time.deltaTime;
			if (mCurrentCloseTimer <= 0f || mGame.mBackKeyTrg)
			{
				Close();
			}
		}
		mState.Update();
	}

	public void Init(ConfigBase config)
	{
		if (!mIsInit)
		{
			mConfig = config;
			if (mConfig != null && mConfig.Buttons == BUTTONS.TIMER)
			{
				mCurrentCloseTimer = mConfig.CloseTimer;
			}
			mIsInit = true;
		}
	}

	public override void BuildDialog()
	{
		if (mConfig == null)
		{
			InitDialogFrame(DIALOG_SIZE.MEDIUM);
		}
		else
		{
			float num = ((!string.IsNullOrEmpty(mConfig.Title)) ? 20f : 0f);
			int num2 = ((!(mConfig.Desc == string.Empty)) ? (mConfig.Desc.Length - mConfig.Desc.Replace("\n", string.Empty).Length + 1) : 0);
			if (num2 < mConfig.DescRowMin)
			{
				num2 = mConfig.DescRowMin;
			}
			float num3 = mConfig.HeaderHeight + num + mConfig.ImageAndSpacingHeight + -13f + ((num2 <= 0) ? 0f : (32f * (float)num2 + mConfig.SpacingHeight)) + 84f + mConfig.FooterHeight;
			if (num3 > 600f)
			{
				UnityEngine.Debug.LogWarning("<color=red>ImageTextDescDialog => 高さがDIALOG_SIZE.LARGE(h: 600)を超過しています。超過分を補正します。</color> (h: " + num3 + ")");
				num3 = 600f;
			}
			if (mConfig.UsePresetSize)
			{
				InitDialogFrame(mConfig.PresetSize);
			}
			else
			{
				InitDialogFrame(DIALOG_SIZE.MEDIUM, null, null, 0, (int)num3);
			}
			if (!string.IsNullOrEmpty(mConfig.Title))
			{
				InitDialogTitle(mConfig.Title);
			}
			if (mConfig.IsShowCancelButton && !mConfig.InitInvisibleCancelButton)
			{
				CreateCancelButton("OnButtoNegative_Clicked");
			}
			GameObject gameObject = Util.CreateGameObject("VerticalLayoutGroup", GameObject);
			UIGrid uIGrid = gameObject.AddComponent<UIGrid>();
			uIGrid.arrangement = UIGrid.Arrangement.Vertical;
			if (mConfig is ConfigAtlas)
			{
				ConfigAtlas configAtlas = mConfig as ConfigAtlas;
				AtlasInfo atlasByName = GetAtlasByName(configAtlas.Atlas);
				if (atlasByName != null)
				{
					UISprite uISprite = Util.CreateSprite("Image", gameObject, atlasByName.Image);
					Util.SetSpriteInfo(uISprite, configAtlas.Sprite, mBaseDepth + 4, Vector3.zero, Vector3.one, false);
					UIBasicSprite.Type spriteType = configAtlas.SpriteType;
					if (spriteType == UIBasicSprite.Type.Tiled)
					{
						uISprite.type = UIBasicSprite.Type.Advanced;
						uISprite.topType = (uISprite.bottomType = (uISprite.centerType = (uISprite.leftType = (uISprite.rightType = UIBasicSprite.AdvancedType.Tiled))));
					}
					else
					{
						uISprite.type = configAtlas.SpriteType;
					}
					if (!configAtlas.IsAutoResizeLoadedImage)
					{
						uISprite.height = configAtlas.ImageHeight;
						uISprite.width = configAtlas.ImageWidth;
					}
					float num4 = (float)uISprite.height * 0.5f;
					uIGrid.cellWidth = uISprite.width;
					uIGrid.cellHeight = num4 + mConfig.SpacingHeight;
					uIGrid.transform.SetLocalPositionY((float)mDialogFrame.height * 0.5f - num4 - mConfig.SpacingHeight);
					if (mConfig is ConfigAtlasInAtlases)
					{
						ConfigAtlasInAtlases configAtlasInAtlases = mConfig as ConfigAtlasInAtlases;
						if (configAtlasInAtlases != null && configAtlasInAtlases.InnerAtlasesArray != null && configAtlasInAtlases.InnerAtlasesArray.Length > 0)
						{
							GameObject gameObject2 = Util.CreateGameObject("Inner", uISprite.gameObject);
							LoadInnerAtlasOption[] innerAtlasesArray = configAtlasInAtlases.InnerAtlasesArray;
							foreach (LoadInnerAtlasOption loadInnerAtlasOption in innerAtlasesArray)
							{
								AtlasInfo atlasByName2 = GetAtlasByName(loadInnerAtlasOption.Atlas);
								uISprite = Util.CreateSprite(loadInnerAtlasOption.Name, gameObject2, atlasByName2.Image);
								Util.SetSpriteInfo(uISprite, loadInnerAtlasOption.Sprite, mBaseDepth + 4 + loadInnerAtlasOption.OffsetDepth, loadInnerAtlasOption.LocalPosition, loadInnerAtlasOption.LocalScale, loadInnerAtlasOption.IsFlip, loadInnerAtlasOption.Pivot);
								spriteType = loadInnerAtlasOption.SpriteType;
								if (spriteType == UIBasicSprite.Type.Tiled)
								{
									uISprite.type = UIBasicSprite.Type.Advanced;
									uISprite.topType = (uISprite.bottomType = (uISprite.centerType = (uISprite.leftType = (uISprite.rightType = UIBasicSprite.AdvancedType.Tiled))));
								}
								else
								{
									uISprite.type = loadInnerAtlasOption.SpriteType;
								}
							}
							if (mConfig is ConfigAtlasInAtlasesSorting)
							{
								ConfigAtlasInAtlasesSorting configAtlasInAtlasesSorting = mConfig as ConfigAtlasInAtlasesSorting;
								UIGrid uIGrid2 = gameObject2.AddComponent<UIGrid>();
								uIGrid2.SetLocalPosition(configAtlasInAtlasesSorting.SortingLocalPosition);
								uIGrid2.arrangement = ((configAtlasInAtlasesSorting.Sorting != 0) ? UIGrid.Arrangement.Vertical : UIGrid.Arrangement.Horizontal);
								uIGrid2.pivot = configAtlasInAtlasesSorting.SortingPivot;
								uIGrid2.maxPerLine = configAtlasInAtlasesSorting.SortingMaxPerLine;
								uIGrid2.cellWidth = configAtlasInAtlasesSorting.SortingCell.x;
								uIGrid2.cellHeight = configAtlasInAtlasesSorting.SortingCell.y;
							}
						}
					}
				}
			}
			else if (mConfig is ConfigLinkBanner)
			{
				ConfigLinkBanner configLinkBanner = mConfig as ConfigLinkBanner;
				GameObject go = Util.CreateGameObject("Banner", gameObject);
				StartCoroutine(LoadBannerAsync(go, configLinkBanner.ImageWidth, configLinkBanner.ImageHeight, configLinkBanner.IsAutoResizeLoadedImage));
				float num5 = (float)configLinkBanner.ImageHeight * 0.5f;
				uIGrid.cellWidth = configLinkBanner.ImageWidth;
				uIGrid.cellHeight = num5 + mConfig.SpacingHeight;
				uIGrid.transform.SetLocalPositionY((float)mDialogFrame.height * 0.5f - num5 - mConfig.SpacingHeight);
				mLinkBanner = null;
				mLinkBannerReq = new LinkBannerRequest();
				LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerReq);
				mLinkBannerReq.SetHighPriority(true);
				LinkBannerSetting linkBannerSetting = new LinkBannerSetting();
				linkBannerSetting.BannerList = new List<LinkBannerDetail>();
				linkBannerSetting.BannerList.Add(new LinkBannerDetail
				{
					Action = LinkBannerInfo.ACTION.NOP.ToString(),
					Param = 0,
					FileName = configLinkBanner.FileName
				});
				if (mLinkBannerReq.mReceiveEveryTimeCallback == null)
				{
					mLinkBannerReq.mReceiveEveryTimeCallback = new LinkBannerRequest.OnReceiveEveryTimeCallback();
					mLinkBannerReq.mReceiveEveryTimeCallback.AddListener(delegate(LinkBannerInfo fn_info)
					{
						mLinkBanner = fn_info;
					});
				}
				linkBannerSetting.BaseURL = configLinkBanner.BaseURL;
				LinkBannerManager.Instance.GetLinkBannerRequest(mLinkBannerReq, linkBannerSetting, delegate
				{
				});
			}
			else if (mConfig is ConfigNoImage)
			{
				uIGrid.transform.SetLocalPositionY((float)mDialogFrame.height * 0.5f - mConfig.SpacingHeight * 1.5f);
			}
			UIFont atlasFont = GameMain.LoadFont();
			UILabel uILabel = Util.CreateLabel("Desc", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, mConfig.Desc, mBaseDepth + 4, Vector3.zero, 26, 0, 0, UIWidget.Pivot.Top);
			DialogBase.SetDialogLabelEffect(uILabel);
			Util.SetLabelColor(uILabel, mConfig.DescColor);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(mDialogFrame.width - 30, (uILabel.fontSize + uILabel.spacingY) * num2);
			gameObject.transform.SubLocalPositionY(num);
		}
		if (!mIsCreatedButton)
		{
			TryCraeteButton();
		}
		mState.Change(STATE.WAIT);
	}

	public bool TryCraeteButton()
	{
		if (mIsCreatedButton || mConfig == null)
		{
			return false;
		}
		Vector3 zero = Vector3.zero;
		UIButton uIButton = null;
		string text = null;
		string text2 = null;
		zero.y = 0f - ((float)mDialogFrame.height * 0.5f - 42f - mConfig.SpacingHeight);
		switch (mConfig.Buttons)
		{
		case BUTTONS.OK:
			text = "LC_button_ok2";
			break;
		case BUTTONS.CLOSE:
			text = "LC_button_close2";
			break;
		case BUTTONS.RETRY:
			text = "LC_button_retry";
			break;
		case BUTTONS.CONFIRM:
			text = "LC_button_check";
			break;
		case BUTTONS.GOTO_OLD_EVENT:
			text = "LC_button_oldevent_go";
			break;
		case BUTTONS.RETRY_CLOSE:
			text = "LC_button_retry";
			text2 = "LC_button_close";
			break;
		case BUTTONS.CONFIRM_CLOSE:
			text = "LC_button_check";
			text2 = "LC_button_close";
			break;
		case BUTTONS.CHOICE:
			text = mConfig.ChoicePositiveHudSprite;
			text2 = mConfig.ChoiceNegativeHudSprite;
			break;
		default:
			text = "LC_button_yes";
			text2 = "LC_button_no";
			break;
		}
		if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(text2))
		{
			zero.x = 130f;
		}
		if (!string.IsNullOrEmpty(text))
		{
			uIButton = Util.CreateJellyImageButton("ButtonPositive", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 4, new Vector3(0f - zero.x, zero.y), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnButtonPositive_Clicked", UIButtonMessage.Trigger.OnClick);
		}
		if (string.IsNullOrEmpty(text2))
		{
			mHasNegativeButton = false;
		}
		else
		{
			uIButton = Util.CreateJellyImageButton("ButtonNegative", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetImageButtonInfo(uIButton, text2, text2, text2, mBaseDepth + 4, new Vector3(zero.x, zero.y), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnButtonNegative_Clicked", UIButtonMessage.Trigger.OnClick);
			mHasNegativeButton = true;
		}
		if (mConfig.IsShowCancelButton && mConfig.InitInvisibleCancelButton)
		{
			CreateCancelButton("OnButtoNegative_Clicked");
		}
		mIsCreatedButton = true;
		return true;
	}

	private void CloseProcess(string se_name, SELECT_ITEM select)
	{
		if (!IsBusy())
		{
			if (mConfig == null || !mConfig.DisableSE)
			{
				mGame.PlaySe(se_name, -1);
			}
			mSelectedItem = select;
			Close();
		}
	}

	protected virtual bool IsBusy()
	{
		return (mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.LOAD_LINKBANNER) || GetBusy();
	}

	private AtlasInfo GetAtlasByName(string atlas_name)
	{
		if (mAtlas != null)
		{
			foreach (AtlasInfo value in mAtlas.Values)
			{
				if (value != null && value.Key == atlas_name)
				{
					return value;
				}
			}
		}
		if (mCustomAtlas != null && mCustomAtlas.ContainsKey(atlas_name))
		{
			return mCustomAtlas[atlas_name];
		}
		return null;
	}

	public override IEnumerator BuildResource()
	{
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();
		}
		foreach (KeyValuePair<LOAD_ATLAS, AtlasInfo> mAtla in mAtlas)
		{
			mAtla.Value.UnLoad();
		}
		mAtlas.Clear();
		foreach (int type in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			AtlasInfo info4 = new AtlasInfo(((LOAD_ATLAS)type).ToString());
			mAtlas.Add((LOAD_ATLAS)type, info4);
			StartCoroutine(info4.Load());
		}
		if (mConfig != null && mConfig is ConfigAtlas)
		{
			ConfigAtlas config = mConfig as ConfigAtlas;
			AtlasInfo info3 = new AtlasInfo(config.Atlas);
			mCustomAtlas.Add(config.Atlas, info3);
			StartCoroutine(info3.Load());
			if (mConfig != null && mConfig is ConfigAtlasInAtlases)
			{
				ConfigAtlasInAtlases cfg = mConfig as ConfigAtlasInAtlases;
				if (cfg != null && cfg.InnerAtlasesArray != null)
				{
					LoadInnerAtlasOption[] innerAtlasesArray = cfg.InnerAtlasesArray;
					foreach (LoadInnerAtlasOption opt in innerAtlasesArray)
					{
						if (opt != null)
						{
							AtlasInfo inf = new AtlasInfo(opt.Atlas);
							if (!mCustomAtlas.ContainsKey(opt.Atlas))
							{
								mCustomAtlas.Add(opt.Atlas, inf);
								StartCoroutine(inf.Load());
							}
						}
					}
				}
			}
		}
		foreach (AtlasInfo info2 in mAtlas.Values)
		{
			while (!info2.IsDone())
			{
				yield return null;
			}
		}
		foreach (AtlasInfo info in mCustomAtlas.Values)
		{
			while (!info.IsDone())
			{
				yield return null;
			}
		}
	}

	private IEnumerator LoadBannerAsync(GameObject go, int width, int height, bool is_resize)
	{
		if (!go)
		{
			yield break;
		}
		UISprite bg2 = Util.CreateSprite("BackGround", go, mAtlas[LOAD_ATLAS.HUD].Image);
		Util.SetSpriteInfo(bg2, "bg00w", mBaseDepth + 4, Vector3.zero, Vector3.one, false);
		Util.SetSpriteSize(bg2, width, height);
		bg2.color = Color.gray;
		UISsSprite loading2 = Util.CreateUISsSprite("LoadingAnime", go, "ADV_FIGURE_LOADING", Vector3.zero, Vector3.one, mBaseDepth + 5);
		while (true)
		{
			if (mLinkBanner == null)
			{
				yield return null;
				continue;
			}
			if (mLinkBanner.banner != null)
			{
				UITexture tex = go.AddComponent<UITexture>();
				tex.depth = mBaseDepth + 3;
				tex.mainTexture = mLinkBanner.banner;
				if (is_resize)
				{
					tex.SetDimensions(mLinkBanner.banner.width, mLinkBanner.banner.height);
				}
				else
				{
					tex.SetDimensions(width, height);
				}
				if ((bool)loading2)
				{
					GameMain.SafeDestroy(loading2.gameObject);
				}
				if ((bool)bg2)
				{
					GameMain.SafeDestroy(bg2.gameObject);
				}
				loading2 = null;
				bg2 = null;
				break;
			}
			if (mLinkBanner != null && mLinkBanner.banner == null)
			{
				break;
			}
			yield return null;
		}
	}

	public override void OnOpenFinished()
	{
		mIsOpened = true;
		mState.Change(STATE.MAIN);
	}

	public override void OnCloseFinished()
	{
		InvokeCallback(mSelectedItem);
		if (mAtlas != null)
		{
			foreach (AtlasInfo value in mAtlas.Values)
			{
				value.UnLoad();
			}
			mAtlas.Clear();
		}
		if (mCustomAtlas != null)
		{
			foreach (AtlasInfo value2 in mAtlas.Values)
			{
				value2.UnLoad();
			}
			mCustomAtlas.Clear();
		}
		if (mLinkBannerReq != null)
		{
			LinkBannerManager.Instance.ClearLinkBanner(mLinkBannerReq);
		}
		if (mCallback != null)
		{
			mCallback.Clear();
		}
		mAtlas = null;
		mCustomAtlas = null;
		mLinkBannerReq = null;
		mLinkBanner = null;
		mConfig = null;
		mCallback = null;
		UnityEngine.Object.Destroy(GameObject);
		mGameObject = null;
	}

	public override void OnBackKeyPress()
	{
		if (mConfig != null && mConfig.CanBackKeyByPositiveOnly)
		{
			OnButtonPositive_Clicked(null);
		}
		else
		{
			OnButtoNegative_Clicked(null);
		}
	}

	private void OnButtonPositive_Clicked(GameObject go)
	{
		CloseProcess("SE_POSITIVE", SELECT_ITEM.POSITIVE);
	}

	private void OnButtoNegative_Clicked(GameObject go)
	{
		CloseProcess("SE_NEGATIVE", SELECT_ITEM.NEGATIVE);
	}

	private void ExecCallbackMethod(UnityAction<UnityAction> method, params UnityAction[] calls)
	{
		if (method == null || calls == null)
		{
			return;
		}
		foreach (UnityAction unityAction in calls)
		{
			if (unityAction != null)
			{
				method(unityAction);
			}
		}
	}

	private UnityEvent GetCallback(SELECT_ITEM type, bool force_create)
	{
		if (mCallback == null && force_create)
		{
			mCallback = new Dictionary<SELECT_ITEM, UnityEvent>();
		}
		if (mCallback != null && !mCallback.ContainsKey(type) && force_create)
		{
			mCallback.Add(type, new UnityEvent());
		}
		return (mCallback == null || !mCallback.ContainsKey(type)) ? null : mCallback[type];
	}

	private void InvokeCallback(SELECT_ITEM type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.Invoke();
		}
		if (mConfig != null && mConfig.ClosedEvent != null)
		{
			mConfig.ClosedEvent(type);
		}
	}

	public void AddCallback(SELECT_ITEM type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.AddListener, calls);
			}
		}
	}

	public void RemoveCallback(SELECT_ITEM type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.RemoveListener, calls);
			}
		}
	}

	public void ClearCallback(SELECT_ITEM type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.RemoveAllListeners();
		}
	}

	public new void ClearCallback()
	{
		foreach (int value in Enum.GetValues(typeof(SELECT_ITEM)))
		{
			ClearCallback((SELECT_ITEM)value);
		}
	}

	public void SetCallback(SELECT_ITEM type, params UnityAction[] calls)
	{
		ClearCallback(type);
		AddCallback(type, calls);
	}
}
