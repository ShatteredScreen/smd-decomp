using System;
using System.Collections;
using System.Collections.Generic;

public class SMGiftItemDataListConverter : IMiniJSONConverter
{
	private static string ToSS(object obj)
	{
		return (obj != null) ? obj.ToString() : string.Empty;
	}

	private static T GetEnumValue<T>(object obj)
	{
		return (T)Enum.Parse(typeof(T), ToSS(obj));
	}

	public object ConvertTo(object a_obj)
	{
		List<GiftItemData> list = (List<GiftItemData>)a_obj;
		if (list == null)
		{
			return null;
		}
		List<object> list2 = new List<object>();
		foreach (GiftItemData item2 in list)
		{
			string item = new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(item2);
			list2.Add(item);
		}
		return list2;
	}

	public object ConvertFrom(object a_json)
	{
		ArrayList arrayList = a_json as ArrayList;
		if (arrayList == null)
		{
			return null;
		}
		List<GiftItemData> list = new List<GiftItemData>();
		foreach (object item in arrayList)
		{
			string json = item as string;
			try
			{
				GiftItemData obj = new GiftItemData();
				new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, json);
				list.Add(obj);
			}
			catch (Exception)
			{
			}
		}
		return list;
	}
}
