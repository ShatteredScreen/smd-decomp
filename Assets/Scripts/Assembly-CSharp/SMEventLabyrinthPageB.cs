using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEventLabyrinthPageB : SMMapPage
{
	public const float Labyrinth_ButtonOffset_Y = 0f;

	public static readonly Dictionary<short, List<short>> STAGE_CONNECTION = new Dictionary<short, List<short>>
	{
		{
			10000,
			new List<short> { 100, 200 }
		},
		{
			100,
			new List<short> { 300, 400 }
		},
		{
			200,
			new List<short> { 400, 500 }
		},
		{
			300,
			new List<short> { 600, 700 }
		},
		{
			400,
			new List<short> { 700, 800 }
		},
		{
			500,
			new List<short> { 800, 900 }
		},
		{
			600,
			new List<short> { 20000 }
		},
		{
			700,
			new List<short> { 20000 }
		},
		{
			800,
			new List<short> { 20000 }
		},
		{
			900,
			new List<short> { 20000 }
		}
	};

	public static readonly Dictionary<string, string> STAGE_LINE_UNLOCK = new Dictionary<string, string>
	{
		{ "S-1", "6" },
		{ "S-2", "6" },
		{ "1-3", "6" },
		{ "1-4", "6" },
		{ "2-4", "6" },
		{ "2-5", "6" },
		{ "3-6", "13" },
		{ "3-7", "8" },
		{ "4-7", "8" },
		{ "4-8", "8" },
		{ "5-8", "8" },
		{ "5-9", "13" },
		{ "6-G", "6" },
		{ "7-G", "8" },
		{ "8-G", "8" },
		{ "9-G", "6" }
	};

	protected int mEventID;

	protected short mCourseID;

	private bool mDoneActiveCamera;

	public int EventID
	{
		get
		{
			return mEventID;
		}
	}

	public short CourseID
	{
		get
		{
			return mCourseID;
		}
	}

	public List<short> ClearedStageList { get; private set; }

	public List<short> NextStageList { get; private set; }

	public bool IsLineUnlockEffect { get; private set; }

	public List<string> UnlockLineList { get; private set; }

	public int AvaterPosition { get; private set; }

	public bool IsAvatarHide { get; private set; }

	public EventLabyrinthButton GoalButton { get; private set; }

	protected override void Update()
	{
		base.Update();
	}

	public override IEnumerator UnloadResource()
	{
		SMEventPageData eventPageData = mPageData as SMEventPageData;
		ResourceManager.UnloadResourceByAssetBundleName(ResourceManager.GetAssetBundleNameByResourceKey(eventPageData.EventSetting.MapAtlasKey + "_IN"));
		yield return Resources.UnloadUnusedAssets();
	}

	public IEnumerator Init(Def.SERIES a_series, int a_eventID, short a_courseID, int a_maxLevel, OnLevelPushed a_lvlPushedCB, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadBlockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB, SMMapPageData a_data)
	{
		mPageData = a_data;
		mSeries = a_series;
		mEventID = a_eventID;
		mCourseID = a_courseID;
		if (mSeries != mPageData.Series)
		{
		}
		SMEventPageData eventPageData = a_data as SMEventPageData;
		mMaxModulePage = 1;
		GlobalLabyrinthEventData globalLabyrinthData = GlobalVariables.Instance.GetLabyrinthData(a_eventID);
		bool doSetGlobalLabyrinthData = false;
		mLevelPushedCallback = a_lvlPushedCB;
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		yield return StartCoroutine(LoadRouteData(a_series, mGame.GetMapSize(eventPageData.EventSetting.EventType), a_eventID, a_courseID));
		mMaxPage = NewMapSsDataEntity.GetMapIndex(a_maxLevel) + 1;
		int available = Def.GetStageNo(a_maxLevel, 0);
		mMaxAvailablePage = NewMapSsDataEntity.GetMapIndex(available) + 1;
		int moduleLevel = Def.GetStageNo(a_maxLevel, 0);
		mMaxModulePageFromMaxLevel = NewMapSsDataEntity.GetMapIndex(moduleLevel) + 1;
		bool useBg = false;
		mMapGameObject = new GameObject[mMaxModulePage];
		mMapParts = new SMMapPart[mMaxModulePage];
		for (int i = 0; i < mMaxModulePage; i++)
		{
			mMapGameObject[i] = Util.CreateGameObject("MapGO" + (i + 1), base.gameObject);
			mMapParts[i] = mMapGameObject[i].AddComponent<SMEventLabyrinthPartB>();
			mMapParts[i].SetGameState(mGameState, this, eventPageData);
			SMEventLabyrinthPartB eventPart = mMapParts[i] as SMEventLabyrinthPartB;
			eventPart.SetCourseID(a_courseID);
			Vector3 scale;
			if (!NewMapSsDataEntity.MapScaleList.TryGetValue(i, out scale))
			{
				scale = Vector3.one;
			}
			mMapParts[i].Init(mSeries, i, scale, a_accessoryCB, a_roadBlockCB, a_snsCB, a_mapChangeCB, true, useBg);
			Vector3 pos = new Vector3(0f, 0f + 1136f * (float)i, Def.MAP_BASE_Z - 0.1f * (float)i);
			mMapGameObject[i].gameObject.transform.localPosition = pos;
		}
		asyncList.Clear();
		int mapIndex = 0;
		GameObject go = mMapGameObject[mapIndex];
		SMMapPart part = mMapParts[mapIndex];
		string animeKey3 = part.GetButtonAnimeKey(mSeries, mapIndex);
		List<short> difficulty_kind = new List<short>();
		List<int> stageListKeys = new List<int>(eventPageData.LabyrinthStageList.Keys);
		stageListKeys.Sort();
		for (int i7 = 0; i7 < stageListKeys.Count; i7++)
		{
			int key = stageListKeys[i7];
			if (!difficulty_kind.Contains(eventPageData.LabyrinthStageList[key].Difficulty))
			{
				difficulty_kind.Add(eventPageData.LabyrinthStageList[key].Difficulty);
			}
		}
		for (int i6 = 0; i6 < difficulty_kind.Count; i6++)
		{
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey3 + difficulty_kind[i6], true));
		}
		asyncList.Add(ResourceManager.LoadSsAnimationAsync(string.Format(GameStateEventLabyrinth.FixedSpriteAnimationNameFormat_IN, mEventID)));
		asyncList.Add(ResourceManager.LoadSsAnimationAsync(GameStateEventLabyrinth.FixedSpriteAnimationNameFormatLabyrinth));
		asyncList.Add(ResourceManager.LoadSsAnimationAsync(GameStateEventLabyrinth.GoalAnimationNameFormat));
		string lineAnimeKey = part.GetLineAnimeKey(mSeries, mapIndex);
		List<string> line_kind = new List<string>();
		foreach (KeyValuePair<string, string> kv in STAGE_LINE_UNLOCK)
		{
			if (!line_kind.Contains(kv.Value))
			{
				line_kind.Add(kv.Value);
			}
		}
		for (int i5 = 0; i5 < line_kind.Count; i5++)
		{
			asyncList.Add(ResourceManager.LoadSsAnimationAsync(lineAnimeKey + line_kind[i5]));
		}
		for (int i4 = 0; i4 < asyncList.Count; i4++)
		{
			while (asyncList[i4].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		if (NextStageList != null)
		{
			NextStageList.Clear();
		}
		else
		{
			NextStageList = new List<short>();
		}
		if (ClearedStageList != null)
		{
			ClearedStageList.Clear();
		}
		else
		{
			ClearedStageList = new List<short>();
		}
		if (UnlockLineList != null)
		{
			UnlockLineList.Clear();
		}
		else
		{
			UnlockLineList = new List<string>();
		}
		IsLineUnlockEffect = false;
		int avater_stageno2 = 0;
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, eventPageData.EventID, out eventData);
		PlayerEventLabyrinthData data = eventData.LabyrinthData;
		Dictionary<int, Vector3> stages = new Dictionary<int, Vector3>();
		int laststageno3 = -1;
		short lastcourseID;
		int lastmain;
		if (globalLabyrinthData.LastClearedStage > -1)
		{
			laststageno3 = globalLabyrinthData.LastClearedStage;
			int lastsub2;
			Def.SplitEventStageNo(globalLabyrinthData.LastClearedStage, out lastcourseID, out lastmain, out lastsub2);
			bool doResetLastClearedStage = true;
			if (globalLabyrinthData.RewardAccessoryDataList != null && globalLabyrinthData.RewardAccessoryDataList.Count > 0)
			{
				for (int i3 = 0; i3 < globalLabyrinthData.RewardAccessoryDataList.Count; i3++)
				{
					AccessoryData accessoryData = globalLabyrinthData.RewardAccessoryDataList[i3];
					if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY)
					{
						doResetLastClearedStage = false;
						break;
					}
				}
			}
			if (doResetLastClearedStage)
			{
				globalLabyrinthData.LastClearedStage = -1;
				doSetGlobalLabyrinthData = true;
			}
		}
		else
		{
			laststageno3 = -1;
			lastcourseID = -1;
			lastmain = -1;
			int lastsub2 = 0;
		}
		List<string> keys2 = new List<string>(NewMapSsDataEntity.MapStageList.Keys);
		for (int i2 = 0; i2 < keys2.Count; i2++)
		{
			string key2 = keys2[i2];
			SsMapEntity entity = NewMapSsDataEntity.MapStageList[key2];
			Vector3 pos2 = entity.Position;
			pos2.z = Def.MAP_BUTTON_Z;
			int stageNo = 0;
			string[] name = entity.Name.Split('_');
			if (name[0].CompareTo("stage") == 0)
			{
				stageNo = Def.GetStageNo(int.Parse(entity.Start), 0);
			}
			else if (name[0].CompareTo("L") == 0)
			{
				if (entity.Start.CompareTo("start") == 0)
				{
					stageNo = 10000;
				}
				else if (entity.Start.CompareTo("goal") == 0)
				{
					stageNo = 20000;
				}
			}
			if (stageNo == 0)
			{
				continue;
			}
			stages.Add(stageNo, pos2);
			if (data.CurrentClearedStages != null && data.CurrentClearedStages.ContainsKey(mCourseID))
			{
				List<short> display_stages = data.CurrentClearedStages[mCourseID];
				if (display_stages.Contains((short)stageNo))
				{
					ClearedStageList.Add((short)stageNo);
				}
			}
		}
		short currentNo = 10000;
		ClearedStageList.Sort();
		if (ClearedStageList.Count > 0)
		{
			currentNo = ClearedStageList[ClearedStageList.Count - 1];
			NextStageList.AddRange(STAGE_CONNECTION[currentNo]);
			if (lastcourseID == -1 || mCourseID != lastcourseID)
			{
				lastcourseID = -1;
				lastmain = -1;
			}
			avater_stageno2 = currentNo;
		}
		else
		{
			NextStageList.AddRange(STAGE_CONNECTION[currentNo]);
			avater_stageno2 = 10000;
		}
		if (NewMapSsDataEntity.MapLineList.Count > 0)
		{
			List<string> clearLines = new List<string>();
			if (ClearedStageList.Count > 0)
			{
				short temp_no = ClearedStageList[0];
				int temp_no_main;
				int temp_no_sub;
				Def.SplitStageNo(temp_no, out temp_no_main, out temp_no_sub);
				if (ClearedStageList.Count > 1)
				{
					for (int n = 1; n < ClearedStageList.Count; n++)
					{
						short next = ClearedStageList[n];
						int next_main;
						int next_sub;
						Def.SplitStageNo(next, out next_main, out next_sub);
						string key6 = temp_no_main + "-" + next_main;
						clearLines.Add(key6);
						temp_no = next;
						Def.SplitStageNo(temp_no, out temp_no_main, out temp_no_sub);
					}
				}
				if (STAGE_CONNECTION.ContainsKey(temp_no))
				{
					if (lastcourseID > -1 && temp_no == Def.GetStageNo(lastmain, 0))
					{
						IsLineUnlockEffect = true;
					}
					for (int m = 0; m < STAGE_CONNECTION[temp_no].Count; m++)
					{
						string key5 = string.Empty;
						int s = STAGE_CONNECTION[temp_no][m];
						if (s == 20000)
						{
							key5 = temp_no_main + "-G";
						}
						else
						{
							int s_main;
							int s_sub;
							Def.SplitStageNo(s, out s_main, out s_sub);
							key5 = temp_no_main + "-" + s_main;
						}
						UnlockLineList.Add(key5);
					}
				}
			}
			keys2 = new List<string>(NewMapSsDataEntity.MapLineList.Keys);
			for (int l = 0; l < keys2.Count; l++)
			{
				string key3 = keys2[l];
				SsMapEntity entity2 = NewMapSsDataEntity.MapLineList[key3];
				Vector3 pos4 = entity2.Position;
				pos4.z = Def.MAP_BUTTON_Z;
				int stageNo4 = 0;
				animeKey3 = part.GetLineAnimeKey(mSeries, mapIndex);
				string[] name2 = entity2.Name.Split('_');
				if (name2[0].CompareTo("Lline") != 0)
				{
					continue;
				}
				short startStageNo2 = 0;
				short endStageNo2 = 0;
				startStageNo2 = (short)((entity2.Start.CompareTo("S") != 0) ? ((short)Def.GetStageNo(int.Parse(entity2.Start), 0)) : 10000);
				endStageNo2 = (short)((entity2.End.CompareTo("G") != 0) ? ((short)Def.GetStageNo(int.Parse(entity2.End), 0)) : 20000);
				stageNo4 = name2[1].GetHashCode();
				Player.STAGE_STATUS status = Player.STAGE_STATUS.LOCK;
				if (currentNo == 10000)
				{
					if (entity2.Start.CompareTo("S") == 0)
					{
						status = Player.STAGE_STATUS.UNLOCK;
					}
				}
				else if (entity2.Start.CompareTo("S") == 0)
				{
					if (ClearedStageList.Contains(endStageNo2))
					{
						status = Player.STAGE_STATUS.CLEAR;
					}
				}
				else if (entity2.End.CompareTo("G") == 0)
				{
					if (ClearedStageList.Contains(startStageNo2))
					{
						status = Player.STAGE_STATUS.UNLOCK;
					}
				}
				else if (clearLines.Contains(name2[1]))
				{
					status = Player.STAGE_STATUS.CLEAR;
				}
				else if (UnlockLineList.Contains(name2[1]))
				{
					status = Player.STAGE_STATUS.UNLOCK;
				}
				bool unlockEffect = false;
				if (status == Player.STAGE_STATUS.UNLOCK && lastmain > -1)
				{
					unlockEffect = true;
				}
				EventLabyrinthLine line = CreateLine(name2[1], pos4, Vector3.one, entity2.Angle, 0, go, animeKey3, status, unlockEffect);
				mLineList.Add(line);
				mLineDict.Add(stageNo4, line);
			}
		}
		animeKey3 = part.GetButtonAnimeKey(mSeries, mapIndex);
		List<int> stage_keys = new List<int>(stages.Keys);
		stage_keys.Sort();
		for (int k = 0; k < stage_keys.Count; k++)
		{
			string animeName2 = animeKey3;
			int stageNo2 = stage_keys[k];
			Vector3 pos3 = stages[stageNo2];
			switch (stageNo2)
			{
			case 10000:
				animeName2 = string.Format(GameStateEventLabyrinth.FixedSpriteAnimationNameFormatLabyrinth, mEventID);
				break;
			case 20000:
				animeName2 = string.Format(GameStateEventLabyrinth.FixedSpriteAnimationNameFormatLabyrinth, mEventID);
				break;
			default:
			{
				int main;
				int sub;
				Def.SplitStageNo(stageNo2, out main, out sub);
				animeName2 = animeKey3 + eventPageData.GetLabyrinthStageSetting(main, 0, CourseID).Difficulty;
				break;
			}
			}
			bool isClearEffect = false;
			if (lastmain > -1)
			{
				int laststageNo = Def.GetStageNo(lastmain, 0);
				if (laststageNo == stageNo2)
				{
					isClearEffect = true;
				}
			}
			EventLabyrinthButton button = CreateStageButton(stageNo2, pos3, go, animeName2, isClearEffect);
			if (!mStageButtonList.Contains(button))
			{
				mStageButtonList.Add(button);
			}
			if (!mMapButtons.ContainsKey(stageNo2.ToString()))
			{
				mMapButtons.Add(stageNo2.ToString(), button.gameObject);
			}
			if (stageNo2 == 20000)
			{
				GoalButton = button;
			}
		}
		for (int j = 0; j < mMaxModulePage; j++)
		{
			NGUITools.SetActive(mMapGameObject[j].gameObject, false);
		}
		IsAvatarHide = true;
		EventLabyrinthAvater avater = Util.CreateGameObject("Avatar", base.gameObject).AddComponent<EventLabyrinthAvater>();
		avater.SetCallback(OnAvaterMoveFinished, OnAvatarSave, OnGetMapData);
		avater.Init(avater_stageno2, this, !IsAvatarHide, 70f);
		mAvater = avater;
		AvaterPosition = avater_stageno2;
		if (doSetGlobalLabyrinthData)
		{
			GlobalVariables.Instance.SetLabyrinthData(a_eventID, globalLabyrinthData);
		}
		yield return 0;
	}

	protected override IEnumerator LoadRouteData(Def.SERIES a_series, float a_mapSize, int a_eventID = -1, short a_courseID = -1)
	{
		bool useBin = true;
		if (a_series < Def.SERIES.SM_EV)
		{
			useBin = true;
		}
		else if (a_series == Def.SERIES.SM_EV)
		{
			useBin = false;
		}
		if (useBin)
		{
			NewMapSsDataEntity = mGame.GetMapRouteData(a_series, a_eventID, a_courseID, a_mapSize);
			if (NewMapSsDataEntity == null)
			{
				BIJLog.E(string.Concat("ROUTE LOAD ERROR!!!", a_series, ": ", a_eventID, " ", a_courseID));
			}
		}
		if (NewMapSsDataEntity != null)
		{
			yield break;
		}
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		string prefix = Def.SeriesPrefix[mSeries] + a_eventID;
		string routeKey = string.Format("MAP_PAGE_{0}_IN_ANIMATION_{1}", prefix, a_courseID);
		for (int j = 0; j < mMaxModulePage; j++)
		{
			string anime_key = routeKey + "_" + (j + 1);
			ResSsAnimation entityAnime = ResourceManager.LoadSsAnimationAsync(anime_key, true);
			mLoadAsyncList.Add(entityAnime);
		}
		for (int i = 0; i < mLoadAsyncList.Count; i++)
		{
			while (mLoadAsyncList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		NewMapSsDataEntity = new SMMapSsData();
		NewMapSsDataEntity.MapSize(a_mapSize);
		if (!string.IsNullOrEmpty(routeKey))
		{
			NewMapSsDataEntity.Load(mMaxModulePage, mSeries, routeKey);
		}
		else
		{
			NewMapSsDataEntity.Load(mMaxModulePage, mSeries);
		}
	}

	public EventLabyrinthButton CreateStageButton(int a_stageno, Vector3 a_pos, GameObject a_parent, string a_animeKey, bool a_isClearEffect)
	{
		SMEventPageData sMEventPageData = mPageData as SMEventPageData;
		BIJImage bIJImage = null;
		BIJImage image = ResourceManager.LoadImage(sMEventPageData.EventSetting.MapAtlasKey + "_IN").Image;
		BIJImage image2 = ResourceManager.LoadImage("EVENTLABYRINTH").Image;
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stageno, out a_main, out a_sub);
		bool flag = true;
		Player.STAGE_STATUS sTAGE_STATUS = Player.STAGE_STATUS.LOCK;
		string text;
		switch (a_stageno)
		{
		case 10000:
			bIJImage = image2;
			text = "stage_start";
			flag = false;
			sTAGE_STATUS = Player.STAGE_STATUS.UNLOCK;
			break;
		case 20000:
			bIJImage = image2;
			text = "goal_" + a_main;
			if (NextStageList.Contains((short)a_stageno))
			{
				sTAGE_STATUS = Player.STAGE_STATUS.UNLOCK;
			}
			break;
		default:
			bIJImage = image;
			text = "stage_" + a_main + "-" + a_sub;
			if (ClearedStageList.Contains((short)a_stageno))
			{
				sTAGE_STATUS = Player.STAGE_STATUS.CLEAR;
			}
			else if (NextStageList.Contains((short)a_stageno))
			{
				sTAGE_STATUS = Player.STAGE_STATUS.UNLOCK;
			}
			break;
		}
		float scale = 1f;
		SMMapStageSetting mapStage = sMEventPageData.GetMapStage(a_main, a_sub, mCourseID);
		byte starNum = 0;
		int displayNo = -1;
		bool flag2 = false;
		PlayerClearData _psd;
		if (mGame.mPlayer.GetStageClearData(mSeries, EventID, mCourseID, a_stageno, out _psd))
		{
			starNum = (byte)_psd.GotStars;
		}
		else
		{
			flag2 = true;
		}
		Vector3 vector = Vector3.zero;
		EventLabyrinthButton eventLabyrinthButton = Util.CreateGameObject(text, a_parent).AddComponent<EventLabyrinthButton>();
		eventLabyrinthButton.SetCourseID(mCourseID);
		if (flag)
		{
			if (mapStage != null)
			{
				displayNo = int.Parse(mapStage.DisplayStageNo);
				if (flag2 && !mapStage.IsForcePartner)
				{
					flag2 = false;
				}
			}
			vector = new Vector3(0f, 0f, 0f);
		}
		eventLabyrinthButton.SetUseCurrentAnime(true);
		if (sTAGE_STATUS == Player.STAGE_STATUS.CLEAR && a_isClearEffect)
		{
			eventLabyrinthButton.SetStatusTypeReserved(EventLabyrinthButton.StatusChangeType.TO_CLEAR);
		}
		else if (sTAGE_STATUS == Player.STAGE_STATUS.UNLOCK)
		{
			eventLabyrinthButton.SetStatusTypeReserved(IsLineUnlockEffect ? EventLabyrinthButton.StatusChangeType.TO_UNLOCK : EventLabyrinthButton.StatusChangeType.NONE);
		}
		eventLabyrinthButton.Init(a_pos + vector, scale, a_stageno, starNum, sTAGE_STATUS, bIJImage, a_animeKey, flag2, displayNo);
		if (flag)
		{
			eventLabyrinthButton.SetGameState(mGameState);
			eventLabyrinthButton.SetPushedCallback(base.OnStageButtonPushed);
		}
		return eventLabyrinthButton;
	}

	public EventLabyrinthLine CreateLine(string lineName, Vector3 pos, Vector3 scale, float a_angle, int a_mapIndex, GameObject a_parent, string animeKey, Player.STAGE_STATUS a_status, bool a_isUnlockEffect)
	{
		SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		BIJImage image = ResourceManager.LoadImage(currentEventPageData.EventSetting.MapAtlasKey + "_IN").Image;
		string empty = string.Empty;
		if (STAGE_LINE_UNLOCK.ContainsKey(lineName))
		{
			animeKey += STAGE_LINE_UNLOCK[lineName];
		}
		EventLabyrinthLine eventLabyrinthLine = Util.CreateGameObject("MapLine" + lineName, a_parent).AddComponent<EventLabyrinthLine>();
		eventLabyrinthLine.SetCourseID(mCourseID);
		if (a_isUnlockEffect)
		{
			eventLabyrinthLine.SetStatusTypeReserved(EventLabyrinthLine.StatusChangeType.TO_UNLOCK);
		}
		eventLabyrinthLine.Init(lineName, image, a_mapIndex, pos.x, pos.y, scale, a_angle, animeKey, a_status, empty, IsLineUnlockEffect);
		return eventLabyrinthLine;
	}

	public EventLabyrinthLine GetLine(string a_name)
	{
		EventLabyrinthLine eventLabyrinthLine = null;
		for (int i = 0; i < mLineList.Count; i++)
		{
			eventLabyrinthLine = mLineList[i] as EventLabyrinthLine;
			if (!(eventLabyrinthLine == null))
			{
				string text = eventLabyrinthLine.gameObject.name;
				string[] array = text.Split('_');
				if (array.Length == 2 && array[1].CompareTo(a_name) == 0)
				{
					break;
				}
			}
		}
		return eventLabyrinthLine;
	}

	public override float GetStageWorldPos(int a_stageNo)
	{
		if (mMapButtons.ContainsKey(a_stageNo.ToString()))
		{
			return mMapButtons[a_stageNo.ToString()].transform.localPosition.y;
		}
		return 0f;
	}

	public IEnumerator AvaterWarpToStart()
	{
		yield return StartCoroutine(mAvater.AvaterWarpOutProcess());
		yield return new WaitForSeconds(0.5f);
	}

	public IEnumerator AvatarWarpInInit()
	{
		yield return StartCoroutine(mAvater.AvaterWarpInProcess());
		yield return new WaitForSeconds(0.5f);
		IsAvatarHide = false;
	}

	public override void OnAvatarSave(string a_nodeName, int a_stageno, float a_pos)
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[mCourseID];
		playerMapData.AvatarNodeName = a_nodeName;
		playerMapData.AvatarStagePosition = a_stageno;
		playerMapData.AvatarPosition = a_pos;
		data.CourseData[CourseID] = playerMapData;
		mGame.mPlayer.Data.SetMapData(EventID, data);
		mGame.IsPlayerSaveRequest = true;
	}

	public override PlayerMapData OnGetMapData()
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		if (data == null)
		{
			return null;
		}
		return data.CourseData[mCourseID];
	}

	public override IEnumerator ActiveObjectInCamera(float y)
	{
		if (IsActiveObject)
		{
			yield break;
		}
		List<SMMapPart> atvList = new List<SMMapPart>();
		IsActiveObject = true;
		for (int j = 0; j < mMapGameObject.Length; j++)
		{
			bool isNowActive = NGUITools.GetActive(mMapGameObject[j]);
			StartCoroutine(mMapParts[j].Active(NewMapSsDataEntity, j, false));
			atvList.Add(mMapParts[j]);
		}
		while (true)
		{
			bool endflag = true;
			int i = 0;
			for (int imax = atvList.Count; i < imax; i++)
			{
				if (atvList[i].IsActivating)
				{
					endflag = false;
					break;
				}
			}
			if (endflag)
			{
				break;
			}
			yield return null;
		}
		IsActiveObject = false;
	}

	public IEnumerator FirstClearEffect()
	{
		if (ClearedStageList.Count == 0)
		{
			yield break;
		}
		short currentNo = ClearedStageList[ClearedStageList.Count - 1];
		if (mMapButtons.ContainsKey(currentNo.ToString()))
		{
			EventLabyrinthButton button = GetStageButton(currentNo) as EventLabyrinthButton;
			if (!(button == null))
			{
				yield return StartCoroutine(button.DisappearBox());
			}
		}
	}

	public IEnumerator UnlockStage()
	{
		IsLineUnlockEffect = false;
		if (ClearedStageList.Count == 0)
		{
			yield break;
		}
		short currentNo = ClearedStageList[ClearedStageList.Count - 1];
		if (!mMapButtons.ContainsKey(currentNo.ToString()))
		{
			yield break;
		}
		EventLabyrinthButton clearbutton = GetStageButton(currentNo) as EventLabyrinthButton;
		if (clearbutton == null)
		{
			yield break;
		}
		yield return StartCoroutine(clearbutton.ClearProcess());
		yield return new WaitForSeconds(0.4f);
		List<EventLabyrinthLine> unlockLines = new List<EventLabyrinthLine>();
		for (int k = 0; k < UnlockLineList.Count; k++)
		{
			string line_name = UnlockLineList[k];
			EventLabyrinthLine line = GetLine(line_name);
			if (!(line == null))
			{
				unlockLines.Add(line);
				StartCoroutine(line.UnlockProcess());
			}
		}
		mGame.PlaySe("SE_DEMO_ENTER_CHARACTER02", 2);
		while (true)
		{
			bool isFinished = true;
			for (int j = 0; j < unlockLines.Count; j++)
			{
				if (!unlockLines[j].IsUnlockFinished)
				{
					isFinished = false;
					break;
				}
			}
			if (isFinished)
			{
				break;
			}
			yield return null;
		}
		mGame.PlaySe("SE_RAINBOW", 2);
		for (int i = 0; i < NextStageList.Count; i++)
		{
			EventLabyrinthButton button = GetStageButton(NextStageList[i]) as EventLabyrinthButton;
			if (!(button == null))
			{
				StartCoroutine(button.UnlockProcess());
			}
		}
		yield return new WaitForSeconds(0.5f);
		GlobalLabyrinthEventData globalLabyrinthData = GlobalVariables.Instance.GetLabyrinthData(EventID);
		if (globalLabyrinthData.LastClearedStage > -1)
		{
			globalLabyrinthData.LastClearedStage = -1;
			GlobalVariables.Instance.SetLabyrinthData(EventID, globalLabyrinthData);
		}
	}

	public IEnumerator GoalDisappear()
	{
		if (mMapButtons.ContainsKey(20000.ToString()))
		{
			EventLabyrinthButton goal = mMapButtons[20000.ToString()].GetComponent<EventLabyrinthButton>();
			if (!(goal == null))
			{
				yield return StartCoroutine(goal.DisappearGoal());
			}
		}
	}
}
