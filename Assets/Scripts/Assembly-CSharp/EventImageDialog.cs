using UnityEngine;

public class EventImageDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private OnDialogClosed mCallback;

	private string mSpriteName;

	private string mTitle;

	private string mDesc;

	private new string mLoadImageAtlas;

	private string mButtonImageName = string.Empty;

	private float mImageY;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
		}
	}

	public void SetButtonImageName(string a_name)
	{
		mButtonImageName = a_name;
	}

	public override void BuildDialog()
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage(mLoadImageAtlas);
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(mTitle);
		if (mSpriteName != null)
		{
			UISprite sprite = Util.CreateSprite("TitleSprite", base.gameObject, resImage2.Image);
			Util.SetSpriteInfo(sprite, mSpriteName, mBaseDepth + 2, new Vector3(0f, mImageY, 0f), Vector3.one, false);
		}
		if (mDesc != null)
		{
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, mDesc, mBaseDepth + 3, new Vector3(0f, -38f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.spacingY = 2;
		}
		UIButton button = Util.CreateJellyImageButton("ButtonClose", base.gameObject, resImage.Image);
		string text = "LC_button_close";
		if (!string.IsNullOrEmpty(mButtonImageName))
		{
			text = mButtonImageName;
		}
		Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(0f, -118f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(string spriteName, string title, string desc, string LoadImageAtlas = "EVENTHUD", float a_imageY = 65f)
	{
		mSpriteName = spriteName;
		mTitle = title;
		mDesc = desc;
		mLoadImageAtlas = LoadImageAtlas;
		mImageY = a_imageY;
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		if (!GetBusy())
		{
			OnCancelPushed(null);
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
