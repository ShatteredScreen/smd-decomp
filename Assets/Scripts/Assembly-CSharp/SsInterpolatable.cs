public interface SsInterpolatable
{
	SsInterpolatable GetInterpolated(SsCurveParams curve, float time, SsInterpolatable startValue, SsInterpolatable endValue, int startTime, int endTime);

	SsInterpolatable Interpolate(SsCurveParams curve, float time, SsInterpolatable startValue, SsInterpolatable endValue, int startTime, int endTime);
}
