using System;
using System.Collections.Generic;
using UnityEngine;

public class MapAccessory : MapEntity
{
	public delegate void OnPushed(AccessoryData data, MapAccessory a_accessory);

	public STATE mStateStatus;

	public AccessoryData mData;

	protected OnPushed mCallback;

	protected LoopAnime mLaceAnime;

	protected ParticleOnSSAnime mParticle;

	protected new float mStateTime;

	protected int mMapIndex;

	protected bool mFirstInitialized;

	protected bool mBookedCallback;

	protected BoxCollider mBoxCollider;

	protected UIEventTrigger mEventTrigger;

	protected int mColliderSize = 100;

	protected Def.SERIES mSeries;

	protected bool mRequestOpenAnime;

	public int Debug_Accessories;

	public void SetPushedCallback(OnPushed callback)
	{
		mCallback = callback;
	}

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		if (mLaceAnime != null)
		{
			UnityEngine.Object.Destroy(mLaceAnime.gameObject);
			mLaceAnime = null;
		}
		base.OnDestroy();
	}

	public override void Update()
	{
		mStateStatus = mState.GetStatus();
		switch (mStateStatus)
		{
		case STATE.STAY:
			if (mState.IsChanged() && mRequestOpenAnime)
			{
				mRequestOpenAnime = false;
				OpenAnime();
			}
			break;
		case STATE.ON:
		{
			if (mState.IsChanged())
			{
				mStateTime = 0f;
				break;
			}
			mStateTime += Time.deltaTime * 480f;
			if (mStateTime > 90f)
			{
				mStateTime = 90f;
				if (!mGame.mTouchCnd)
				{
					mState.Change(STATE.OFF);
				}
			}
			float x = (1f + Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.x;
			float y = (1f - Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.y;
			_sprite.transform.localScale = new Vector3(x, y, 1f);
			break;
		}
		case STATE.OFF:
		{
			if (mState.IsChanged())
			{
				if (IsLockTapAnime)
				{
					mGame.PlaySe("SE_COLLECTION_LOCK", -1);
				}
				else
				{
					mGame.PlaySe("SE_ITEM_OPEN", -1);
				}
				break;
			}
			mStateTime -= Time.deltaTime * 640f;
			if (mStateTime < 0f)
			{
				mStateTime = 0f;
				if (mCallCallback)
				{
					if (IsLockTapAnime)
					{
						mForceAnimeFinished = true;
					}
					else
					{
						PlayAnime(AnimeKey + "_TAP", false);
					}
					mState.Change(STATE.WAIT);
				}
				else
				{
					mState.Change(STATE.STAY);
				}
			}
			float x = (1f + Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.x;
			float y = (1f - Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.y;
			_sprite.transform.localScale = new Vector3(x, y, 1f);
			break;
		}
		case STATE.WAIT:
			if (!_sprite.IsAnimationFinished() && !mForceAnimeFinished)
			{
				break;
			}
			mForceAnimeFinished = false;
			if (mCallCallback && mCallback != null)
			{
				mCallCallback = false;
				if (!IsLockTapAnime)
				{
					mCallback(mData, this);
					mChangeParts = new Dictionary<string, string>();
					mChangeParts["icon_00"] = "null";
					mChangeParts["icon_01"] = "null";
					mChangeParts["mess"] = "null";
					mChangeParts["NewCell"] = "null";
					PlayAnime(AnimeKey, false);
					mLaceAnime._sprite.AnimFrame = 0f;
					mLaceAnime._sprite.Pause();
					HasTapAnime = false;
				}
				else
				{
					PlayAnime(AnimeKey + "_LOCK", true);
				}
			}
			mBoxCollider = base.gameObject.GetComponent<BoxCollider>();
			if (mBoxCollider == null)
			{
				mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
			}
			mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
			mState.Change(STATE.STAY);
			break;
		}
		mState.Update();
	}

	public virtual void Init(int _counter, string _name, AccessoryData data, BIJImage atlas, float x, float y, Vector3 scale, bool a_alreadyHas, int a_mapIndex, Def.SERIES a_series, string a_animeName)
	{
		mSeries = a_series;
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeName;
		DefaultAnimeKey = a_animeName;
		mAtlas = atlas;
		mScale = scale;
		float num = Def.MAP_BUTTON_Z - 0.15f;
		base._pos = new Vector3(x, y, num);
		mData = data;
		if (!a_alreadyHas)
		{
			mChangeParts = new Dictionary<string, string>();
			mChangeParts["icon_00"] = data.ImageName;
			mChangeParts["icon_01"] = data.ImageName;
			mChangeParts["mess"] = "ac_mess_tap";
			bool flag = true;
			Player mPlayer = mGame.mPlayer;
			int stageNo = Def.GetStageNo(data.MainStage, data.SubStage);
			if (data.UnlockCondition.CompareTo("TAP") == 0)
			{
				PlayerClearData _psd;
				if (mGame.mEventMode)
				{
					if (mPlayer.GetStageClearData(a_series, mPlayer.Data.CurrentEventID, stageNo, out _psd))
					{
						flag = false;
					}
				}
				else if (mPlayer.GetStageClearData(a_series, stageNo, out _psd))
				{
					flag = false;
				}
			}
			if (flag)
			{
				IsLockTapAnime = true;
				mChangeParts["icon_00"] = data.ImageName;
				mChangeParts["icon_01"] = data.ImageName;
				mChangeParts["mess"] = "null";
				mScale = new Vector3(0.7f, 0.7f, 1f);
				PlayAnime(AnimeKey + "_LOCK", true);
			}
			else
			{
				PlayAnime(AnimeKey, true);
			}
			HasTapAnime = true;
			if (!mFirstInitialized)
			{
				mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
				mEventTrigger = base.gameObject.AddComponent<UIEventTrigger>();
				mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
				EventDelegate item = new EventDelegate(this, "OnButtonPushed");
				mEventTrigger.onPress.Add(item);
				EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
				mEventTrigger.onRelease.Add(item2);
				EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
				mEventTrigger.onClick.Add(item3);
			}
		}
		else
		{
			HasTapAnime = false;
		}
		if (a_alreadyHas)
		{
			mLaceAnime = Util.CreateLoopAnime("Lace_" + data.ImageName, "MAP_LACE_ROTATE", base.gameObject.transform.parent.gameObject, new Vector3(x, y, num + 0.01f), mScale * 1.8f, 0f, 0f, false, null);
			mLaceAnime._sprite.AnimFrame = 0f;
			mLaceAnime._sprite.Pause();
		}
		else
		{
			mLaceAnime = Util.CreateLoopAnime("Lace_" + data.ImageName, "MAP_LACE_ROTATE", base.gameObject.transform.parent.gameObject, new Vector3(x, y, num + 0.01f), mScale * 1.8f, 0f, 0f, false, null);
		}
		mFirstInitialized = true;
	}

	public virtual void Init(int _counter, string _name, AccessoryData data, BIJImage atlas, float x, float y, Vector3 scale, bool a_alreadyHas, int a_mapIndex, Def.SERIES a_series, bool tutorial, string animeName)
	{
		mSeries = a_series;
		mMapIndex = a_mapIndex;
		AnimeKey = animeName;
		DefaultAnimeKey = animeName;
		mAtlas = atlas;
		mScale = scale;
		float z = Def.MAP_BUTTON_Z - 0.1f;
		base._pos = new Vector3(x, y, z);
		mData = data;
	}

	public void OnButtonPushed()
	{
		if (mStateStatus == STATE.STAY && HasTapAnime)
		{
			STATE aStatus = STATE.ON;
			mState.Change(aStatus);
		}
	}

	public void OnButtonClicked()
	{
		if (!mGame.IsTipsScreenVisibled)
		{
			if (HasTapAnime)
			{
				mCallCallback = true;
			}
			else
			{
				mCallCallback = false;
			}
		}
	}

	public void OnButtonReleased()
	{
	}

	public void OpenAnime()
	{
		AnimeKey = DefaultAnimeKey;
		mChangeParts = new Dictionary<string, string>();
		mChangeParts["mess"] = "ac_mess_tap";
		mChangeParts["icon_00"] = mData.ImageName;
		mChangeParts["icon_01"] = mData.ImageName;
		HasTapAnime = true;
		mScale = Vector3.one;
		if (mLaceAnime != null)
		{
			mLaceAnime._sprite.Play();
		}
		PlayAnime(AnimeKey, true);
		mParticle = Util.CreateGameObject("OpenPartilce", base.gameObject).AddComponent<ParticleOnSSAnime>();
		mParticle.Init(_sprite, "Particles/StageOpenParticle", -10f, "mess");
		mParticle.SetEmitterDestroyCallback(OnParticleFinished);
	}

	public void RequestOpenAnime()
	{
		mRequestOpenAnime = true;
	}

	public void OnParticleFinished()
	{
		if (mParticle != null)
		{
			GameMain.SafeDestroy(mParticle.gameObject);
			mParticle = null;
		}
	}

	public void SetEnable(bool a_flg)
	{
		if (mBoxCollider != null)
		{
			mBoxCollider.enabled = a_flg;
		}
	}

	public virtual void SetDebugInfo(AccessoryData a_setting)
	{
		Debug_Accessories = a_setting.Index;
	}
}
