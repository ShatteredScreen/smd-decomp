public class ChargeCheckResponse : ParameterObject<ChargeCheckResponse>
{
	public bool result { get; set; }

	public CCheckLimitCheck limit_check { get; set; }

	public CCheckAgeCheck age_check { get; set; }

	public int code { get; set; }

	public int code_ex { get; set; }

	public long server_time { get; set; }
}
