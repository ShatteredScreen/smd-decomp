using System;
using System.Collections.Generic;
using UnityEngine;

public class FriendRankingList : ListBase<FriendRankingItem>
{
	public enum STATE
	{
		CHECK = 0,
		SETUP = 1,
		CONTENTS_SET = 2,
		MAIN = 3
	}

	public enum MODE
	{
		LEVEL = 0,
		STAR = 1
	}

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.CHECK);

	private MODE mMode = MODE.STAR;

	private GameMain mGame = SingletonMonoBehaviour<GameMain>.Instance;

	private UIFont font = GameMain.LoadFont();

	private List<FriendRankingItem> mFriendRankingItems = new List<FriendRankingItem>();

	private float mScrollBarWidth = 14f;

	private int mCheckNum;

	private int mMaxNum;

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.CHECK:
			CreateContents(base.gameObject, "Contents");
			mState.Reset(STATE.SETUP, true);
			break;
		case STATE.SETUP:
			UpdateList();
			mListScrollView.UpdateScrollbars(false);
			mState.Reset(STATE.CONTENTS_SET, true);
			break;
		case STATE.CONTENTS_SET:
			mState.Change(STATE.MAIN);
			break;
		}
		mState.Update();
	}

	protected override int GetCellHeight()
	{
		return 105;
	}

	protected override void CreateContents(GameObject parent, string name)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		CreateList(parent, "List");
		UISprite uISprite = Util.CreateSprite("BackShadow", parent, image);
		Util.SetSpriteInfo(uISprite, "325", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = Mathf.FloorToInt(mWindowRect.width);
		uISprite.height = Mathf.FloorToInt(mWindowRect.height) + 10;
	}

	public void Init(List<MyFriend> friends, Rect rect, int baseDepth, float barWidth = 14f)
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		if (GameMain.IsSNSLogined(sns) && friends != null && friends.Count > 0)
		{
			foreach (MyFriend friend in friends)
			{
				FriendRankingItem item = new FriendRankingItem(friend);
				mFriendRankingItems.Add(item);
			}
		}
		FriendRankingItem item2 = new FriendRankingItem();
		mFriendRankingItems.Add(item2);
		ItemSort(mMode);
		mWindowRect = rect;
		mBaseDepth = baseDepth;
		mScrollBarWidth = barWidth;
		mCheckNum = 0;
		mMaxNum = mFriendRankingItems.Count;
		base.gameObject.transform.localPosition = new Vector3(0f, rect.yMin - rect.height / 2f, 0f);
	}

	public void ChangeMode(MODE mode)
	{
		if (mode != mMode)
		{
			mMode = mode;
			ItemSort(mMode);
			mState.Change(STATE.SETUP);
		}
	}

	private void ItemSort(MODE mode)
	{
		if (mFriendRankingItems != null)
		{
			switch (mMode)
			{
			case MODE.LEVEL:
				mFriendRankingItems.Sort(LevelSort);
				break;
			case MODE.STAR:
				mFriendRankingItems.Sort(StarSort);
				break;
			}
		}
	}

	public static int LevelSort(FriendRankingItem a, FriendRankingItem b)
	{
		if (b.Level > a.Level)
		{
			return 1;
		}
		if (a.Level > b.Level)
		{
			return -1;
		}
		if (a.UUID != b.UUID)
		{
			return -1;
		}
		return 0;
	}

	public static int StarSort(FriendRankingItem a, FriendRankingItem b)
	{
		if (b.TotalStar > a.TotalStar)
		{
			return 1;
		}
		if (a.TotalStar > b.TotalStar)
		{
			return -1;
		}
		if (a.UUID != b.UUID)
		{
			return -1;
		}
		return 0;
	}

	public static int TrophySort(FriendRankingItem a, FriendRankingItem b)
	{
		if (b.TotalTrophy > a.TotalTrophy)
		{
			return 1;
		}
		if (a.TotalTrophy > b.TotalTrophy)
		{
			return -1;
		}
		if (a.UUID != b.UUID)
		{
			return -1;
		}
		return 0;
	}

	public MyFriend GetMyFriend(GameObject go)
	{
		UIDragScrollView uIDragScrollView = NGUITools.FindInParents<UIDragScrollView>(go);
		if (uIDragScrollView == null)
		{
			return null;
		}
		int num = ListIndexOf(uIDragScrollView.gameObject);
		if (num < 0)
		{
			return null;
		}
		return mFriendRankingItems[num].Info;
	}

	protected override void CreateScrollBar(GameObject parent, string name)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		base.CreateScrollBar(parent, name);
		mScroll.fillDirection = UIProgressBar.FillDirection.TopToBottom;
		mScroll.transform.localPosition = new Vector3((mWindowRect.width - mScrollBarWidth) / 2f - 5f, 0f, 0f);
		UISprite uISprite = Util.CreateSprite("Scroll_F", mScroll.gameObject, image);
		Util.SetSpriteInfo(uISprite, "173", mBaseDepth + 5, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = (int)mScrollBarWidth;
		uISprite.height = (int)mWindowRect.height;
		UISprite uISprite2 = Util.CreateSprite("Scroll_B", mScroll.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "174", mBaseDepth + 4, Vector3.zero, Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.width = (int)mScrollBarWidth;
		uISprite2.height = (int)mWindowRect.height;
		mScroll.foregroundWidget = uISprite;
		mScroll.backgroundWidget = uISprite2;
	}

	protected override void CreateItem(GameObject go, int index, FriendRankingItem item, FriendRankingItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UISprite uISprite = Util.CreateSprite("Board", go, image);
		string imageName = ((index % 2 != 0) ? "320" : "319");
		Util.SetSpriteInfo(uISprite, imageName, mBaseDepth, new Vector3(-5f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(490, 102);
		UISprite uISprite2 = Util.CreateSprite("RankNo", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "324", mBaseDepth + 1, new Vector3(-190f, 0f, 0f), Vector3.one * 0.8f, false);
		switch (index)
		{
		case 0:
			Util.SetSpriteImageName(uISprite2, "321", Vector3.one, false);
			break;
		case 1:
			Util.SetSpriteImageName(uISprite2, "322", Vector3.one, false);
			break;
		case 2:
			Util.SetSpriteImageName(uISprite2, "323", Vector3.one, false);
			break;
		default:
		{
			NumberImageString numberImageString = Util.CreateGameObject("Number", uISprite2.gameObject).AddComponent<NumberImageString>();
			numberImageString.Init(4, index + 1, mBaseDepth + 2, 0.9f, UIWidget.Pivot.Center, false, image);
			numberImageString.transform.localPosition = Vector3.zero;
			numberImageString.SetColor(Def.DEFAULT_MESSAGE_COLOR);
			break;
		}
		}
		UISprite uISprite3 = Util.CreateSprite("Mat", uISprite.gameObject, image);
		NumberImageString numberImageString2 = Util.CreateGameObject("Number", uISprite3.gameObject).AddComponent<NumberImageString>();
		switch (mMode)
		{
		case MODE.STAR:
			Util.SetSpriteInfo(uISprite3, "77", mBaseDepth + 1, new Vector3(-110f, -3f, 0f), new Vector3(0.95f, 0.95f, 1f), false);
			numberImageString2.Init(3, (int)item.TotalStar, mBaseDepth + 2, 0.8f, UIWidget.Pivot.Center, false, image);
			numberImageString2.transform.localPosition = new Vector3(0f, -10f, 0f);
			break;
		case MODE.LEVEL:
			Util.SetSpriteInfo(uISprite3, "76", mBaseDepth + 1, new Vector3(-110f, -10f, 0f), Vector3.one, false);
			numberImageString2.Init(3, item.Level, mBaseDepth + 2, 0.8f, UIWidget.Pivot.Center, false, image);
			numberImageString2.transform.localPosition = new Vector3(0f, -5f, 0f);
			break;
		}
		numberImageString2.SetColor(Def.DEFAULT_MESSAGE_COLOR);
		UIButton uIButton = Util.CreateImageButton("Frame", uISprite.gameObject, image);
		Util.SetImageButtonInfo(uIButton, "225", "225", "225", mBaseDepth + 2, new Vector3(-25f, 0f, 0f), Vector3.one * 0.65f);
		if (item.Icon != null)
		{
			UITexture uITexture = Util.CreateGameObject("Icon", uIButton.gameObject).AddComponent<UITexture>();
			uITexture.depth = mBaseDepth + 1;
			uITexture.transform.localPosition = Vector3.zero;
			uITexture.transform.localScale = Vector3.one;
			uITexture.mainTexture = item.Icon;
		}
		else
		{
			string imageName2 = "200";
			if (string.Compare("male", item.Gender, StringComparison.OrdinalIgnoreCase) == 0)
			{
				imageName2 = "175";
			}
			else if (string.Compare("female", item.Gender, StringComparison.OrdinalIgnoreCase) == 0)
			{
				imageName2 = "199";
			}
			UISprite sprite = Util.CreateSprite("Icon", uIButton.gameObject, image);
			Util.SetSpriteInfo(sprite, imageName2, mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		}
		UILabel uILabel = Util.CreateLabel("Name", uISprite.gameObject, font);
		Util.SetLabelInfo(uILabel, item.Name, mBaseDepth + 1, new Vector3(20f, 15f, 0f), 30, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(200, 35);
		UILabel uILabel2 = Util.CreateLabel("Level", uISprite.gameObject, font);
		Util.SetLabelInfo(uILabel2, string.Format(Localization.Get("Ranking_AccumScoreFormat"), item.TotalScore), mBaseDepth + 1, new Vector3(220f, -20f, 0f), 20, 0, 0, UIWidget.Pivot.Right);
		uILabel2.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel2.SetDimensions(200, 25);
	}

	protected override List<FriendRankingItem> GetListItem()
	{
		return mFriendRankingItems;
	}

	public FriendRankingItem FindListItem(GameObject go)
	{
		int num = FindListIndexOf(go);
		if (num < 0 || mFriendRankingItems.Count <= num)
		{
			return null;
		}
		return mFriendRankingItems[num];
	}

	public override void ClearList()
	{
		mMapItems.Clear();
		mItems.Clear();
		UIDragScrollView[] componentsInChildren = mList.GetComponentsInChildren<UIDragScrollView>();
		UIDragScrollView[] array = componentsInChildren;
		foreach (UIDragScrollView uIDragScrollView in array)
		{
			UnityEngine.Object.DestroyImmediate(uIDragScrollView.gameObject);
		}
	}
}
