using UnityEngine;

public class FixedSprite : PartsChangedSprite
{
	public string mSpriteName;

	public Vector2 mSpriteSize;

	public Vector2 mSpriteOffset;

	public void Init(string animeFileName, string spriteName, Vector3 pos, BIJImage atlas)
	{
		mSpriteName = spriteName;
		Atlas = atlas;
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[1];
		array[0].partName = "change000";
		array[0].spriteName = spriteName;
		Vector2 vector = (mSpriteSize = atlas.GetPixelSize(spriteName));
		mSpriteOffset = atlas.GetPixelOffset(spriteName);
		array[0].centerOffset = new Vector2(vector.x / 2f, vector.y / 2f);
		ChangeAnime(animeFileName, array, true, 0, null);
		base.transform.localPosition = pos;
	}

	public void ChangeSprite(string spriteName)
	{
		Init(mCurrentAnimation, spriteName, base.transform.localPosition, Atlas);
	}

	public void ChangeSpriteSize(Vector2 spriteSize, Vector2 cutOffTexSize)
	{
		SsAnimation animation = _sprite.Animation;
		SsPartRes[] partList = animation.PartList;
		for (int i = 0; i < partList.Length; i++)
		{
			if (!partList[i].IsRoot)
			{
				Vector2 vector = new Vector2(Atlas.Texture.width, Atlas.Texture.height);
				if (string.Compare(partList[i].Name, "change000") == 0)
				{
					Vector2 offset = Atlas.GetOffset(mSpriteName);
					Vector2 size = Atlas.GetSize(mSpriteName);
					float num = offset.x + size.x / 2f - cutOffTexSize.x / vector.x / 2f;
					float num2 = offset.y + size.y / 2f - cutOffTexSize.y / vector.y / 2f;
					float x = num + cutOffTexSize.x / vector.x;
					float y = num2 + cutOffTexSize.y / vector.y;
					partList[i].UVs = new Vector2[4]
					{
						new Vector2(num, y),
						new Vector2(x, y),
						new Vector2(x, num2),
						new Vector2(num, num2)
					};
					num = (0f - spriteSize.x) / 2f;
					num2 = spriteSize.y / 2f;
					x = spriteSize.x / 2f;
					y = (0f - spriteSize.y) / 2f;
					float z = 0f;
					partList[i].OrgVertices = new Vector3[4]
					{
						new Vector3(num, y, z),
						new Vector3(x, y, z),
						new Vector3(x, num2, z),
						new Vector3(num, num2, z)
					};
				}
			}
		}
		CleanupAnimation();
		_sprite.Animation = animation;
		_sprite.Play();
		_sprite.PlayCount = mPlayCount;
		_sprite.AnimationFinished = mAnimationFinished;
	}

	public new void SetColor(Color col)
	{
		for (int i = 0; i < _sprite._colors.Length; i++)
		{
			_sprite._colors[i] = col;
		}
		for (int j = 0; j < _sprite.GetParts().Length; j++)
		{
			_sprite.GetParts()[j].ForceAlpha(col.a);
		}
		_sprite._colorChanged = true;
	}
}
