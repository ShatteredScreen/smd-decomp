using UnityEngine;

public class GetScoreEffect : MonoBehaviour
{
	private const float TIME = 0.8f;

	private const float ERASE_TIME = 0.25f;

	private static int MAX_SCORE = 99999;

	private static int MAX_SCORE_RANK = MAX_SCORE.ToString().Length;

	private FixedSprite[] mNum = new FixedSprite[MAX_SCORE_RANK];

	private int mState;

	private int mKeta;

	private Color mColor;

	private float mTime;

	private float mScale;

	private float mDelayTime;

	private Vector3 mPos;

	private static Color[] ColorTbl = new Color[7]
	{
		new Color(1f, 0.1764706f, 5f / 51f, 1f),
		new Color(0.050980393f, 0.5254902f, 1f, 1f),
		new Color(6f / 85f, 0.69803923f, 0f, 1f),
		new Color(1f, 0.8509804f, 0f, 1f),
		new Color(1f, 0.49803922f, 0f, 1f),
		new Color(0.7372549f, 0.30980393f, 1f, 1f),
		new Color(1f, 1f, 1f, 1f)
	};

	private static string[] SpriteNameTbl = new string[10] { "num0", "num1", "num2", "num3", "num4", "num5", "num6", "num7", "num8", "num9" };

	private int[] num;

	private static float numPitch = 20f;

	private void Start()
	{
	}

	private void Update()
	{
		switch (mState)
		{
		case 0:
			if (mDelayTime > 0f)
			{
				mDelayTime -= Time.deltaTime;
				if (!(mDelayTime <= 0f))
				{
					break;
				}
				CreateNumberSprites();
			}
			mTime -= Time.deltaTime;
			mScale += Time.deltaTime;
			base.gameObject.transform.localScale = new Vector3(mScale, mScale, 1f);
			mPos.y += Time.deltaTime * 50f;
			base.gameObject.transform.localPosition = mPos;
			if (mTime <= 0f)
			{
				mTime = 0.25f;
				mState = 4096;
			}
			break;
		case 4096:
		{
			mTime -= Time.deltaTime;
			float num = mTime / 0.25f;
			if (num < 0f)
			{
				num = 0f;
			}
			for (int i = 0; i < mKeta; i++)
			{
				if (mNum[i] != null)
				{
					mNum[i].SetColor(new Color(mColor.r * num, mColor.g * num, mColor.b * num, num));
				}
			}
			mScale += Time.deltaTime;
			base.gameObject.transform.localScale = new Vector3(mScale, mScale, 1f);
			mPos.y += Time.deltaTime * 50f;
			base.gameObject.transform.localPosition = mPos;
			if (mTime <= 0f)
			{
				Object.Destroy(base.gameObject);
				mState = 8192;
			}
			break;
		}
		}
	}

	public void Init(int score, Def.TILE_KIND kind, float delay)
	{
		if (score > MAX_SCORE)
		{
			score = MAX_SCORE;
		}
		if (kind < Def.TILE_KIND.COLOR0 || (int)kind >= ColorTbl.Length)
		{
			mColor = new Color(1f, 1f, 1f, 1f);
		}
		else
		{
			mColor = ColorTbl[(int)kind];
		}
		mKeta = 0;
		num = new int[MAX_SCORE_RANK];
		while (score > 0)
		{
			num[mKeta] = score % 10;
			mKeta++;
			score /= 10;
		}
		if (delay <= 0f)
		{
			CreateNumberSprites();
		}
		mTime = 0.8f;
		mScale = 0.9f;
		mDelayTime = delay;
		mPos = base.transform.localPosition;
	}

	private void CreateNumberSprites()
	{
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_TILE");
		for (int i = 0; i < mKeta; i++)
		{
			mNum[i] = Util.CreateGameObject("Num", base.gameObject).AddComponent<FixedSprite>();
			mNum[i].Init("PUZZLETILE_FIXED_SPRITE", SpriteNameTbl[num[i]], new Vector3(((float)mKeta / 2f - 0.5f) * numPitch - (float)i * numPitch, 0f, 0f), resImage.Image);
			mNum[i].SetColor(mColor);
		}
	}
}
