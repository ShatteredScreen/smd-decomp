using System;

public class SpCharaSettings : ICloneable
{
	[MiniJSONAlias("chara_id")]
	public int CharaId { get; set; }

	[MiniJSONAlias("sp_table")]
	public short SpTable { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public SpCharaSettings Clone()
	{
		return MemberwiseClone() as SpCharaSettings;
	}
}
