using System;
using System.Collections.Generic;

public class AdvDebug_GetAllItem_Response : ICloneable
{
	[MiniJSONAlias("items")]
	public List<AdvItemData> ItemDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvDebug_GetAllItem_Response Clone()
	{
		return MemberwiseClone() as AdvDebug_GetAllItem_Response;
	}
}
