using System;

public class GetMailResponseGift : GetMailResponseSupport, ICloneable
{
	[MiniJSONAlias("iconid")]
	public int IconID { get; set; }

	[MiniJSONAlias("iconlvl")]
	public int IconLvl { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public new GetMailResponseGift Clone()
	{
		return MemberwiseClone() as GetMailResponseGift;
	}

	public override GiftItemData GetGiftItemData()
	{
		GiftItemData giftItemData = base.GetGiftItemData();
		giftItemData.UseIcon = true;
		giftItemData.IconID = IconID;
		giftItemData.IconLevel = IconLvl;
		if (giftItemData.ItemCategory == 14 || giftItemData.ItemCategory == 18 || giftItemData.ItemCategory == 19)
		{
			giftItemData.UsePlayerInfo = true;
			giftItemData.Series = giftItemData.ItemKind;
			giftItemData.SeriesStage = giftItemData.ItemID;
		}
		return giftItemData;
	}
}
