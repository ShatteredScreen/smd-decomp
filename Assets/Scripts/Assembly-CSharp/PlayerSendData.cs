using System;

public class PlayerSendData : SMJsonData, ICloneable
{
	public int Version { get; set; }

	[MiniJSONAlias("uuid")]
	public int UUID { get; set; }

	public DateTime LastHeartSendTime { get; set; }

	public DateTime LastRoadBlockRequestTime { get; set; }

	public DateTime LastFriendHelpRequestTime { get; set; }

	public PlayerSendData()
	{
		Version = 1290;
		LastHeartSendTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastRoadBlockRequestTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastFriendHelpRequestTime = DateTimeUtil.UnitLocalTimeEpoch;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void CopyFrom(MyFriendData a_data)
	{
		UUID = a_data.UUID;
		LastHeartSendTime = a_data.LastHeartSendTime;
		LastRoadBlockRequestTime = a_data.LastRoadBlockRequestTime;
		LastFriendHelpRequestTime = a_data.LastFriendHelpRequestTime;
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(Version);
		data.WriteInt(UUID);
		data.WriteDateTime(LastHeartSendTime);
		data.WriteDateTime(LastRoadBlockRequestTime);
		data.WriteDateTime(LastFriendHelpRequestTime);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		Version = data.ReadInt();
		UUID = data.ReadInt();
		LastHeartSendTime = data.ReadDateTime();
		LastRoadBlockRequestTime = data.ReadDateTime();
		LastFriendHelpRequestTime = data.ReadDateTime();
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerSendData obj = new PlayerSendData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public PlayerSendData Clone()
	{
		return MemberwiseClone() as PlayerSendData;
	}
}
