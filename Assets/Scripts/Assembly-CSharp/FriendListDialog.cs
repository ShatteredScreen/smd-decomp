using System;
using System.Collections.Generic;
using UnityEngine;

public class FriendListDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_00 = 2,
		NETWORK_00_01 = 3,
		NETWORK_01_00 = 4,
		NETWORK_01_01 = 5
	}

	public delegate void OnDialogClosed(bool stageOpen);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private SMVerticalListInfo mFriendListInfo;

	private SMVerticalList mFriendList;

	private FriendItem mSelectedFriend;

	private GameObject mSelectedFriendGameObject;

	private string mFriendNum;

	private UILabel mFriendNumLabel;

	private float mBasePosY;

	private ConfirmDialog mConfirmDialog;

	private RemoveFriendConfirmDialog mRemoveConfirmDialog;

	private int mRemoveUUID = -1;

	private bool mStageOpenGiftReceived;

	private List<FriendIconCheckData> mFriendIconCheckData = new List<FriendIconCheckData>();

	private List<SMVerticalListItem> mList = new List<SMVerticalListItem>();

	private FBUserManager mFBUserManager;

	private DateTime mNow;

	private void Server_OnRemoveFriend(int result, int code)
	{
		if (result == 0)
		{
			mState.Reset(STATE.NETWORK_01_00, true);
		}
		else
		{
			mState.Reset(STATE.NETWORK_01_01, true);
		}
	}

	private void SendRemoveSuccess()
	{
		FriendItem friendItem = mSelectedFriend;
		mSelectedFriend = null;
		int index = mFriendList.FindListIndexOf(mSelectedFriendGameObject);
		mFriendList.RemoveListItem(index);
		mFriendList.OnScreenChanged(true);
		DateTime now = DateTime.Now;
		int num = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
		int num2 = 1;
		int num3 = 0;
		int currentLevel = mGame.mPlayer.CurrentLevel;
		int lifeCount = mGame.mPlayer.LifeCount;
		int totalStars = mGame.mPlayer.TotalStars;
		mGame.mPlayer.RemoveFriend(friendItem.Info);
		mFriendNum = string.Format(Localization.Get("FriendTotalNum"), mGame.mPlayer.Friends.Count, Constants.GAME_FRIEND_MAX);
		Util.SetLabelText(mFriendNumLabel, mFriendNum);
		mGame.Save();
		string desc = Localization.Get("Reary_DestroyAF_Desc");
		GameObject parentGameObject = base.ParentGameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parentGameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("Reary_DestroyAF_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnRemoveSuccessDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		mState.Reset(STATE.WAIT, true);
	}

	private void SendRemoveFail()
	{
		FriendItem friendItem = mSelectedFriend;
		mSelectedFriend = null;
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		GameObject parentGameObject = base.ParentGameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parentGameObject).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("Gift_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnRemoveErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		mState.Reset(STATE.WAIT, true);
	}

	public override void Start()
	{
		if (mGame.mFbUserMngFriend != null)
		{
			mFBUserManager = SingletonMonoBehaviour<GameMain>.Instance.mFbUserMngFriend;
			mFBUserManager.SetPriority(true);
		}
		Server.RemoveFriendEvent += Server_OnRemoveFriend;
		mNow = DateTime.Now;
		base.Start();
	}

	public override void OnDestroy()
	{
		if (mGame.mFbUserMngFriend != null)
		{
			Server.RemoveFriendEvent -= Server_OnRemoveFriend;
			mFBUserManager.SetPriority(false);
		}
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
		{
			if (mGame.mFbUserMngFriend == null || mFriendIconCheckData.Count <= 0)
			{
				break;
			}
			BIJImage image = ResourceManager.LoadImage("ICON").Image;
			int num = mBaseDepth + 5;
			for (int num2 = mFriendIconCheckData.Count - 1; num2 >= 0; num2--)
			{
				FriendIconCheckData friendIconCheckData = mFriendIconCheckData[num2];
				FBUserData.STATE iconStatus = mFBUserManager.GetIconStatus(friendIconCheckData.uuid);
				if (iconStatus == FBUserData.STATE.SUCCESS || iconStatus == FBUserData.STATE.FAILURE)
				{
					if (friendIconCheckData.loadingIcon != null)
					{
						UnityEngine.Object.Destroy(friendIconCheckData.loadingIcon);
					}
					Texture2D icon = mFBUserManager.GetIcon(friendIconCheckData.uuid);
					if (icon != null)
					{
						UITexture uITexture = Util.CreateGameObject("Icon", friendIconCheckData.boardImage.gameObject).AddComponent<UITexture>();
						uITexture.depth = num + 2;
						uITexture.transform.localPosition = new Vector3(-194f, 3f, 0f);
						uITexture.mainTexture = icon;
						uITexture.SetDimensions(80, 80);
						int num3 = friendIconCheckData.iconid - 10000;
						if (num3 >= mGame.mCompanionData.Count)
						{
							num3 = 0;
						}
						string iconName = mGame.mCompanionData[num3].iconName;
						UISprite uISprite = Util.CreateSprite("Icon", uITexture.gameObject, image);
						Util.SetSpriteInfo(uISprite, iconName, num + 3, new Vector3(28f, 28f, 0f), Vector3.one, false);
						uISprite.SetDimensions(50, 50);
					}
					else
					{
						string imageName = "icon_chara_facebook";
						UISprite uISprite2 = Util.CreateSprite("Icon", friendIconCheckData.boardImage.gameObject, image);
						Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-194f, 3f, 0f), Vector3.one, false);
						uISprite2.SetDimensions(80, 80);
						int num4 = friendIconCheckData.iconid - 10000;
						if (num4 >= mGame.mCompanionData.Count)
						{
							num4 = 0;
						}
						string iconName2 = mGame.mCompanionData[num4].iconName;
						UISprite uISprite3 = Util.CreateSprite("Icon", uISprite2.gameObject, image);
						Util.SetSpriteInfo(uISprite3, iconName2, num + 3, new Vector3(28f, 28f, 0f), Vector3.one, false);
						uISprite3.SetDimensions(50, 50);
					}
					mFriendIconCheckData.RemoveAt(num2);
				}
			}
			break;
		}
		case STATE.NETWORK_00:
			Network_RemoveFriend();
			break;
		case STATE.NETWORK_01_00:
			SendRemoveSuccess();
			break;
		case STATE.NETWORK_01_01:
			SendRemoveFail();
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("FriendList");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		mFriendListInfo = new SMVerticalListInfo(1, 512f, 430f, 1, 512f, 430f, 1, 140);
		SetLayout(Util.ScreenOrientation);
		titleDescKey = string.Format(Localization.Get("Com_Notice02"));
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 1, new Vector3(-120f, 217f, 0f), 16, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = new Color(0.54509807f, 0.38431373f, 0.18039216f, 1f);
		List<FriendItem> list = new List<FriendItem>();
		foreach (KeyValuePair<int, MyFriend> friend in mGame.mPlayer.Friends)
		{
			list.Add(FriendItem.Make(friend.Value));
		}
		list.Sort(FriendItemSort);
		mList.Clear();
		for (int i = 0; i < list.Count; i++)
		{
			mList.Add(list[i]);
		}
		titleDescKey = string.Format(Localization.Get("FriendTotal"));
		uILabel = Util.CreateLabel("FriendTotalDesk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(-40f, -257f, 0f), 22, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mFriendNum = string.Format(Localization.Get("FriendTotalNum"), mGame.mPlayer.Friends.Count, Constants.GAME_FRIEND_MAX);
		mFriendNumLabel = Util.CreateLabel("FriendNumDesk", base.gameObject, atlasFont);
		Util.SetLabelInfo(mFriendNumLabel, mFriendNum, mBaseDepth + 3, new Vector3(171f, -257f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		mFriendNumLabel.color = Def.DEFAULT_MESSAGE_COLOR;
		CreateCancelButton("OnCancelPushed");
		mStageOpenGiftReceived = false;
	}

	public int FriendItemSort(FriendItem a, FriendItem b)
	{
		if (b.Info.Data.UpdateTimeEpoch > a.Info.Data.UpdateTimeEpoch)
		{
			return 1;
		}
		if (a.Info.Data.UpdateTimeEpoch > b.Info.Data.UpdateTimeEpoch)
		{
			return -1;
		}
		return 0;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		}
	}

	private void OnCreateItem_Friend(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		FriendItem friendItem = item as FriendItem;
		string text = "icon_chara_facebook";
		string text2 = "lv_1";
		int num2 = 0;
		int num3 = 0;
		num3 = ((friendItem.Info.Data.IconID < 10000) ? friendItem.Info.Data.IconID : (friendItem.Info.Data.IconID - 10000));
		if (num3 >= mGame.mCompanionData.Count)
		{
			num3 = 0;
		}
		CompanionData companionData = mGame.mCompanionData[num3];
		text = companionData.iconName;
		int iconLevel = friendItem.Info.Data.IconLevel;
		text2 = "lv_" + iconLevel;
		int a_main;
		int a_sub;
		Def.SplitStageNo(friendItem.Info.Data.Level, out a_main, out a_sub);
		string text3 = string.Format(Localization.Get("Stage_Data"), a_main);
		string text4 = friendItem.Info.Data.Name;
		float cellWidth = mFriendList.mList.cellWidth;
		float cellHeight = mFriendList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		UISprite uISprite2 = Util.CreateSprite("Instruction", itemGO.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel2", num, new Vector3(0f, 10f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(430, 128);
		UISprite uISprite3;
		if (GameMain.IsFacebookIconEnable() && friendItem.Info.Data.IconID >= 10000)
		{
			if (mGame.mFbUserMngFriend != null)
			{
				Texture2D icon = mFBUserManager.GetIcon(friendItem.Info.Data.UUID);
				switch (mFBUserManager.GetIconStatus(friendItem.Info.Data.UUID))
				{
				case FBUserData.STATE.DEFAULT:
				case FBUserData.STATE.FAILURE:
				{
					if (friendItem.Info.Icon != null)
					{
						UITexture uITexture2 = Util.CreateGameObject("FriendIcon", uISprite2.gameObject).AddComponent<UITexture>();
						uITexture2.depth = num + 1;
						uITexture2.transform.localPosition = new Vector3(-165f, -5f, 0f);
						uITexture2.mainTexture = friendItem.Info.Icon;
						uITexture2.SetDimensions(65, 65);
					}
					else
					{
						uISprite3 = Util.CreateSprite("FriendIcon", uISprite2.gameObject, image2);
						Util.SetSpriteInfo(uISprite3, "icon_chara_facebook", num + 1, new Vector3(-165f, 3f, 0f), Vector3.one, false);
						uISprite3.SetDimensions(80, 80);
					}
					int num5 = friendItem.Info.Data.IconID - 10000;
					if (num5 >= mGame.mCompanionData.Count)
					{
						num5 = 0;
					}
					string iconName2 = mGame.mCompanionData[num5].iconName;
					uISprite3 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
					Util.SetSpriteInfo(uISprite3, iconName2, num + 3, new Vector3(-138f, 25f, 0f), Vector3.one, false);
					uISprite3.SetDimensions(45, 45);
					break;
				}
				case FBUserData.STATE.LOADING:
				{
					string imageName = "icon_chara_facebook02";
					UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
					Util.SetSpriteInfo(uISprite4, imageName, num + 2, new Vector3(-165f, 3f, 0f), Vector3.one, false);
					uISprite4.SetDimensions(80, 80);
					FriendIconCheckData friendIconCheckData = new FriendIconCheckData();
					friendIconCheckData.boardImage = uISprite2;
					friendIconCheckData.loadingIcon = uISprite4;
					friendIconCheckData.uuid = friendItem.Info.Data.UUID;
					friendIconCheckData.iconid = friendItem.Info.Data.IconID;
					mFriendIconCheckData.Add(friendIconCheckData);
					break;
				}
				case FBUserData.STATE.SUCCESS:
				{
					icon = mFBUserManager.GetIcon(friendItem.Info.Data.UUID);
					if (icon != null)
					{
						UITexture uITexture = Util.CreateGameObject("FriendIcon", uISprite2.gameObject).AddComponent<UITexture>();
						uITexture.depth = num + 1;
						uITexture.transform.localPosition = new Vector3(-165f, -5f, 0f);
						uITexture.mainTexture = icon;
						uITexture.SetDimensions(65, 65);
					}
					else
					{
						uISprite3 = Util.CreateSprite("FriendIcon", uISprite2.gameObject, image2);
						Util.SetSpriteInfo(uISprite3, "icon_chara_facebook", num + 1, new Vector3(-165f, 3f, 0f), Vector3.one, false);
						uISprite3.SetDimensions(80, 80);
					}
					int num4 = friendItem.Info.Data.IconID - 10000;
					if (num4 >= mGame.mCompanionData.Count)
					{
						num4 = 0;
					}
					string iconName = mGame.mCompanionData[num4].iconName;
					uISprite3 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
					Util.SetSpriteInfo(uISprite3, iconName, num + 3, new Vector3(-138f, 25f, 0f), Vector3.one, false);
					uISprite3.SetDimensions(45, 45);
					break;
				}
				}
			}
			else
			{
				if (friendItem.Info.Icon != null)
				{
					UITexture uITexture3 = Util.CreateGameObject("FriendIcon", uISprite2.gameObject).AddComponent<UITexture>();
					uITexture3.depth = num + 1;
					uITexture3.transform.localPosition = new Vector3(-165f, -5f, 0f);
					uITexture3.mainTexture = friendItem.Info.Icon;
					uITexture3.SetDimensions(65, 65);
				}
				else
				{
					uISprite3 = Util.CreateSprite("FriendIcon", uISprite2.gameObject, image2);
					Util.SetSpriteInfo(uISprite3, "icon_chara_facebook", num + 1, new Vector3(-165f, -5f, 0f), Vector3.one, false);
					uISprite3.SetDimensions(65, 65);
				}
				int num6 = friendItem.Info.Data.IconID - 10000;
				if (num6 >= mGame.mCompanionData.Count)
				{
					num6 = 0;
				}
				string iconName3 = mGame.mCompanionData[num6].iconName;
				uISprite3 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite3, iconName3, num + 3, new Vector3(-138f, 25f, 0f), Vector3.one, false);
				uISprite3.SetDimensions(45, 45);
			}
		}
		else
		{
			uISprite3 = Util.CreateSprite("FriendIcon", uISprite2.gameObject, image2);
			Util.SetSpriteInfo(uISprite3, text, num + 1, new Vector3(-160f, 3f, 0f), Vector3.one, false);
			uISprite3.SetDimensions(80, 80);
			uISprite3 = Util.CreateSprite("CharaLv", uISprite2.gameObject, image);
			Util.SetSpriteInfo(uISprite3, text2, num + 2, new Vector3(-160f, -37f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(friendItem.Info.Data.Fbid))
			{
				string imageName2 = "icon_sns00_mini";
				uISprite3 = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite3, imageName2, num + 4, new Vector3(-136f, 28f, 0f), Vector3.one, false);
			}
		}
		uISprite3 = Util.CreateSprite("Line00", uISprite2.gameObject, image);
		Util.SetSpriteInfo(uISprite3, "line00", num + 1, new Vector3(53f, 17f, 0f), Vector3.one, false);
		uISprite3.type = UIBasicSprite.Type.Sliced;
		uISprite3.SetDimensions(292, 6);
		UILabel uILabel = Util.CreateLabel("LastLogin", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("LastVisit"), num + 2, new Vector3(-104f, 3f, 0f), 16, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		DateTime fdt = DateTimeUtil.UnixTimeStampToDateTime(friendItem.Info.Data.UpdateTimeEpoch, true);
		long num7 = DateTimeUtil.BetweenDays0(fdt, mNow);
		string text5 = ((num7 <= 0) ? Localization.Get("LastVisitData02") : ((num7 < 30) ? string.Format(Localization.Get("LastVisitData00"), num7) : string.Format(Localization.Get("LastVisitData01"), 30)));
		UILabel uILabel2 = Util.CreateLabel("LastLoginDays", uISprite2.gameObject, atlasFont);
		float x = uILabel.transform.localPosition.x + (float)uILabel.width;
		float y = uILabel.transform.localPosition.y;
		Util.SetLabelInfo(uILabel2, text5, num + 2, new Vector3(x, y, 0f), 16, 0, 0, UIWidget.Pivot.Left);
		uILabel2.color = new Color(1f, 0.1f, 0.1f);
		uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel2.SetDimensions(200, 16);
		int num8 = friendItem.Info.Data.Series;
		int stageNumber = 1;
		List<PlayStageParam> playStage = friendItem.Info.Data.PlayStage;
		if (playStage != null && playStage.Count != 0)
		{
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == num8)
				{
					stageNumber = playStage[i].Level / 100;
					break;
				}
			}
		}
		else
		{
			num8 = 0;
			Def.SplitStageNo(friendItem.Info.Data.Level, out a_main, out a_sub);
			stageNumber = a_main;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(uISprite2.gameObject, num, num8, stageNumber, -17f, -18f);
		uILabel = Util.CreateLabel("UserName", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text4, num + 1, new Vector3(53f, 34f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(297, 26);
		UIButton button = Util.CreateJellyImageButton("RemoveButton", uISprite2.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_delete2", "LC_button_delete2", "LC_button_delete2", num + 1, new Vector3(142f, -32f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnRemovePushed", UIButtonMessage.Trigger.OnClick);
	}

	public override void OnOpenFinished()
	{
		mFriendList = SMVerticalList.Make(base.gameObject, this, mFriendListInfo, mList, 0f, -20f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mFriendList.OnCreateItem = OnCreateItem_Friend;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mStageOpenGiftReceived);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
		if (mFriendList != null)
		{
			GameMain.SafeDestroy(mFriendList.gameObject);
			mFriendList = null;
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void OnRemovePushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			FriendItem friendItem = mFriendList.FindListItem(go) as FriendItem;
			if (friendItem != null && mRemoveConfirmDialog == null)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				mSelectedFriend = friendItem;
				mSelectedFriendGameObject = go;
				SearchUserData searchUserData = new SearchUserData();
				searchUserData.uuid = friendItem.Info.Data.UUID;
				searchUserData.iconid = friendItem.Info.Data.IconID;
				searchUserData.iconlvl = friendItem.Info.Data.IconLevel;
				searchUserData.series = friendItem.Info.Data.Series;
				searchUserData.lvl = friendItem.Info.Data.Level;
				searchUserData.nickname = friendItem.Info.Data.Name;
				searchUserData.fbid = friendItem.Info.Data.Fbid;
				searchUserData.PlayStage = friendItem.Info.Data.PlayStage;
				mRemoveConfirmDialog = Util.CreateGameObject("RemoveConfirm", base.ParentGameObject).AddComponent<RemoveFriendConfirmDialog>();
				mRemoveConfirmDialog.Init(searchUserData);
				mRemoveConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
				mState.Change(STATE.WAIT);
			}
		}
	}

	private void OnConfirmDialogClosed(RemoveFriendConfirmDialog.SELECT_ITEM i, int uuid)
	{
		if (i == RemoveFriendConfirmDialog.SELECT_ITEM.REMOVE)
		{
			if (uuid < 0)
			{
				SendRemoveFail();
				return;
			}
			mRemoveUUID = uuid;
			mState.Change(STATE.NETWORK_00);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
		mRemoveConfirmDialog = null;
	}

	private void OnRemoveSuccessDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void OnRemoveErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void EnableList(bool enabled)
	{
		if (!(mFriendList == null))
		{
			NGUITools.SetActive(mFriendList.gameObject, enabled);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void Network_RemoveFriend()
	{
		if (!Server.RemoveFriend(mRemoveUUID))
		{
			mState.Change(STATE.NETWORK_01_01);
		}
		mState.Change(STATE.NETWORK_00_01);
	}
}
