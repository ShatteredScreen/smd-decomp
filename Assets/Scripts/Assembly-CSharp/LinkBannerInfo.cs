using UnityEngine;

public class LinkBannerInfo
{
	public enum ACTION
	{
		NOP = 0,
		NOTICE = 1,
		SALE = 2,
		DAILY = 3,
		EVENT = 4,
		ADV = 5,
		ADV_PLAY = 6,
		ADV_GASHAMENU = 7,
		ADV_GASHA_BN = 8,
		ADV_GASHA_INFO = 9,
		ADV_LB_PLAY = 10,
		ADV_LB_GASHAMENU = 11,
		ADV_LB_EVENT = 12,
		ADV_LB_GASHA = 13,
		ADV_GASHA = 14,
		ADV_EVENT = 15,
		MAIL = 16,
		PURCHASE = 17,
		JUMP_STORE = 18
	}

	public string fileName { get; set; }

	public ACTION action { get; set; }

	public int param { get; set; }

	public Texture2D banner { get; set; }

	public bool isDone { get; set; }

	public bool isError { get; set; }

	public LinkBannerInfo()
	{
		fileName = string.Empty;
		action = ACTION.NOP;
		param = 0;
		banner = null;
		isDone = false;
		isError = false;
	}

	public void SetAction(string actionStr)
	{
		if (actionStr == ACTION.NOTICE.ToString())
		{
			action = ACTION.NOTICE;
		}
		else if (actionStr == ACTION.SALE.ToString())
		{
			action = ACTION.SALE;
		}
		else if (actionStr == ACTION.DAILY.ToString())
		{
			action = ACTION.DAILY;
		}
		else if (actionStr == ACTION.EVENT.ToString())
		{
			action = ACTION.EVENT;
		}
		else if (actionStr == ACTION.ADV.ToString())
		{
			action = ACTION.ADV;
		}
		else if (actionStr == ACTION.ADV_PLAY.ToString())
		{
			action = ACTION.ADV_PLAY;
		}
		else if (actionStr == ACTION.ADV_GASHAMENU.ToString())
		{
			action = ACTION.ADV_GASHAMENU;
		}
		else if (actionStr == ACTION.ADV_GASHA_BN.ToString())
		{
			action = ACTION.ADV_GASHA_BN;
		}
		else if (actionStr == ACTION.ADV_GASHA_INFO.ToString())
		{
			action = ACTION.ADV_GASHA_INFO;
		}
		else if (actionStr == ACTION.ADV_LB_PLAY.ToString())
		{
			action = ACTION.ADV_LB_PLAY;
		}
		else if (actionStr == ACTION.ADV_LB_GASHAMENU.ToString())
		{
			action = ACTION.ADV_LB_GASHAMENU;
		}
		else if (actionStr == ACTION.ADV_LB_EVENT.ToString())
		{
			action = ACTION.ADV_LB_EVENT;
		}
		else if (actionStr == ACTION.ADV_LB_GASHA.ToString())
		{
			action = ACTION.ADV_LB_GASHA;
		}
		else if (actionStr == ACTION.ADV_GASHA.ToString())
		{
			action = ACTION.ADV_GASHA;
		}
		else if (actionStr == ACTION.ADV_EVENT.ToString())
		{
			action = ACTION.ADV_EVENT;
		}
		else if (actionStr == ACTION.MAIL.ToString())
		{
			action = ACTION.MAIL;
		}
		else if (actionStr == ACTION.PURCHASE.ToString())
		{
			action = ACTION.PURCHASE;
		}
		else if (actionStr == ACTION.JUMP_STORE.ToString())
		{
			action = ACTION.JUMP_STORE;
		}
		else
		{
			action = ACTION.NOP;
		}
	}
}
