using System;
using System.Collections.Generic;

public interface IBIJSNSPostData : ICloneable, ICopyable
{
	string ToID { get; }

	string PostID { get; set; }

	string[] ToIDs { get; }

	string[] PostIDs { get; set; }

	string Title { get; }

	string Message { get; }

	string ImageURL { get; }

	string LinkURL { get; }

	string MimeType { get; }

	string FileName { get; }

	List<string> ExcludeIds { get; }
}
