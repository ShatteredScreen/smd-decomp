public interface INetworkConstants
{
	string COUNTRY_CODE { get; }

	string NETWORK_SALT { get; }

	string APPNAME { get; }

	string AppBase_URL { get; }

	string AppEventBase_URL { get; }

	string ClientData_URL { get; }

	string Social_URL { get; }

	string NICKNAME_URL { get; }

	string VIP_URL { get; }

	string IAP_URL { get; }

	string CRAM_URL { get; }

	string CRAM_KPI_URL { get; }

	string MoreGames_URL { get; }

	string InfoBarApp_URL { get; }

	string InfoBarPage_URL { get; }

	string Campaign_URL { get; }

	string HELP_URL { get; }

	string STORE_URL { get; }

	string Reward_URL { get; }

	string TransferBase_URL { get; }

	string Facebook_APPID { get; }

	string Facebook_SECRET { get; }

	string GooglePlus_APPID { get; }

	string GooglePlus_CLIENTID { get; }

	string Twitter_KEY { get; }

	string Twitter_SECRET { get; }

	string Instagram_KEY { get; }

	string Instagram_SECRET { get; }

	int Partytrack_APPID { get; }

	string Partytrack_KEY { get; }

	string GrowthPush_APPID { get; }

	string GrowthPush_SECRET { get; }

	string GrowthPush_GCM_SENDER_ID { get; }

	string EULA_URL { get; }

	string DLFILELIST_URL { get; }

	string HEADER_PREFIX { get; }

	string GameInfo_URL { get; }

	string[] SSL_ACCEPT_CERTS { get; }

	string Connection_URL { get; }

	string NETWORK_ADV_SALT { get; }

	string AdvAppBase_URL { get; }
}
