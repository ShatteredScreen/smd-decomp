public class EpisodeSeriesSelectListItem : SMVerticalListItem
{
	public int index;

	public bool enable;

	public short openEpisodeNum;

	public short maxEpisodeNum;

	public short series = -1;

	public short eventId = -1;
}
