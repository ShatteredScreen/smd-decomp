using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowupPartnerConfirmDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		PLAY = 0,
		CANCEL = 1,
		BACK = 2
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private Partner mPartner;

	private CompanionData mCompanionData;

	private OnDialogClosed mCallback;

	private AccessoryData mData;

	private GameObject mScrollGO;

	private float mStateTime;

	public AccessoryData.ACCESSORY_TYPE AccessoryType;

	private GameObject mConfirmSelectGO;

	private UIButton mGrowUpbutton;

	private UISprite mSelectedPartner;

	private UISprite mSelectedPartnerLvl;

	private UISprite mSelectedPartnerAfterLvl;

	private BIJImage atlas;

	public bool IsReplayStamp { get; private set; }

	public AccessoryData Data
	{
		get
		{
			return mData;
		}
	}

	public bool CanRepeat { get; set; }

	public int SelectedPartnerNo { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public void Init(AccessoryData a_data, int mSelectedPartnerNo, bool a_close, AccessoryData.ACCESSORY_TYPE a_type)
	{
		mData = a_data;
		SelectedPartnerNo = mSelectedPartnerNo;
		mCompanionData = mGame.mCompanionData[mSelectedPartnerNo];
		IsReplayStamp = a_close;
		AccessoryType = a_type;
		if (mCompanionData == null)
		{
			BIJLog.E("Companion Data is NULL !!!");
		}
	}

	public void Init(bool a_close)
	{
		IsReplayStamp = a_close;
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
		{
			if (!mGame.mTutorialManager.IsTutorialPlaying() || mGame.mTutorialManager.GetCurrentTutorial() != Def.TUTORIAL_INDEX.MAP_LEVEL9_5)
			{
				break;
			}
			int num = 0;
			List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == 0)
				{
					num = playStage[i].Level;
					break;
				}
			}
			if (num == 900)
			{
				BIJImage image = ResourceManager.LoadImage("HUD").Image;
				mGrowUpbutton = Util.CreateJellyImageButton("ButtonPlay", mConfirmSelectGO.gameObject, image);
				Util.SetImageButtonInfo(mGrowUpbutton, "LC_button_growth", "LC_button_growth", "LC_button_growth", mBaseDepth + 3, new Vector3(0f, -129f, 0f), Vector3.one);
				Util.AddImageButtonMessage(mGrowUpbutton, this, "OnOKPushed", UIButtonMessage.Trigger.OnClick);
				mGame.mTutorialManager.DeleteTutorialPointArrow();
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL9_5);
				mGame.mTutorialManager.SetTutorialFinger(mGrowUpbutton.gameObject, mGrowUpbutton.gameObject, Def.DIR.NONE, 100);
				mGame.mTutorialManager.AddAllowedButton(mGrowUpbutton.gameObject);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		mPartner = Util.CreateGameObject("Partner", base.gameObject).AddComponent<Partner>();
		mPartner.transform.localPosition = Vector3.zero;
		int level = mGame.mPlayer.GetCompanionLevel(mCompanionData.index);
		mPartner.Init(mCompanionData.index, Vector3.zero, 1f, level, base.gameObject, true, mBaseDepth + 2);
		while (!mPartner.IsPartnerLoadFinished())
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get(string.Empty);
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UILabel uILabel = Util.CreateLabel("Title", mTitleBoard.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 4, new Vector3(32f, -4f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		titleDescKey = Util.MakeLText("GetLevelup_Title_00");
		Util.SetLabelText(uILabel, titleDescKey);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.fontSize = 38;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UISprite sprite = Util.CreateSprite("IconLVUP", mTitleBoard.gameObject, image2);
		Util.SetSpriteInfo(sprite, "icon_level", mBaseDepth + 1, new Vector3(-148f, 1f, 0f), Vector3.one, false);
		mScrollGO = Util.CreateGameObject("ScrollGO", base.gameObject);
		mConfirmSelectGO = Util.CreateGameObject("ConfirmSelectGO", mScrollGO);
		mConfirmSelectGO.transform.localPosition = new Vector3(0f, -99f, 0f);
		uILabel = Util.CreateLabel("ConfirmPartner", mConfirmSelectGO.gameObject, atlasFont);
		titleDescKey = Util.MakeLText("Growup_ConfirmPartner");
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, 300f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, 30);
		uILabel.fontSize = 30;
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mSelectedPartner = Util.CreateSprite("SelectedPartner", base.gameObject, image);
		int companionLevel = mGame.mPlayer.GetCompanionLevel(SelectedPartnerNo);
		mSelectedPartnerAfterLvl = Util.CreateSprite("SelectedPartnerAfterLvl", mSelectedPartner.gameObject, atlas);
		UISprite sprite2 = Util.CreateSprite("Lvlleft", mConfirmSelectGO.gameObject, atlas);
		string imageName = "lv_" + companionLevel + "_2";
		Util.SetSpriteInfo(sprite2, imageName, mBaseDepth + 1, new Vector3(-79f, -28f, 0f), Vector3.one, false);
		UISprite sprite3 = Util.CreateSprite("Lvlright", mConfirmSelectGO.gameObject, atlas);
		imageName = ((companionLevel + 1 >= mCompanionData.maxLevel) ? Def.CharacterViewLevelMaxImageTbl[companionLevel + 1] : Def.CharacterViewLevelImageTbl[companionLevel + 1]);
		Util.SetSpriteInfo(sprite3, imageName, mBaseDepth + 1, new Vector3(79f, -28f, 0f), Vector3.one, false);
		UISprite sprite4 = Util.CreateSprite("Lvlmid_arrow", mConfirmSelectGO.gameObject, atlas);
		imageName = "arrow2";
		Util.SetSpriteInfo(sprite4, imageName, mBaseDepth + 1, new Vector3(0f, -28f, 0f), Vector3.one, false);
		titleDescKey = ((mCompanionData.motionType == 0) ? Util.MakeLText("PartnerDesc_Common" + (companionLevel + 1)) : ((companionLevel != 3) ? Util.MakeLText("PartnerDesc_Common" + (companionLevel + 1)) : Util.MakeLText("PartnerDesc_Common" + (companionLevel + 1) + "_02")));
		uILabel = Util.CreateLabel("lvldesk_Down", mConfirmSelectGO.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, -74f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, 30);
		uILabel.fontSize = 26;
		bool flag = true;
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL9_7))
		{
			int num = 0;
			List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == 0)
				{
					num = playStage[i].Level;
					break;
				}
			}
			if (num == 900)
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.MAP_LEVEL9_2);
				mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL9_3, base.ParentGameObject, OnTutorialMessageFinished, OnTutorialMessageClosed, true);
				mGame.mTutorialManager.SetTutorialPoint1Arrow(base.gameObject, uILabel.gameObject, false, 100, Def.DIR.NONE, 0f);
				flag = false;
			}
		}
		if (flag)
		{
			mGrowUpbutton = Util.CreateJellyImageButton("ButtonPlay", mConfirmSelectGO.gameObject, atlas);
			Util.SetImageButtonInfo(mGrowUpbutton, "LC_button_growth", "LC_button_growth", "LC_button_growth", mBaseDepth + 3, new Vector3(0f, -129f, 0f), Vector3.one);
			Util.AddImageButtonMessage(mGrowUpbutton, this, "OnOKPushed", UIButtonMessage.Trigger.OnClick);
		}
		mPartner.SetPartnerUIScreenDepth(mBaseDepth + 2);
		if (mCompanionData.IsTall())
		{
			mPartner.SetPartnerScreenScale(0.5f);
		}
		else
		{
			mPartner.SetPartnerScreenScale(0.58f);
		}
		mPartner.SetPartnerScreenPos(new Vector3(0f, -98f, 0f));
		mPartner.SetPartnerScreenEnable(true);
		mPartner.MakeUIShadow(atlas, "character_foot2", mBaseDepth + 1);
		CreateCancelButton("OnBackToCompanionPushed");
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(tUTORIAL_INDEX);
			if (tUTORIAL_INDEX == Def.TUTORIAL_INDEX.MAP_LEVEL9_5)
			{
				mGame.mTutorialManager.DeleteTutorialPointArrow();
			}
		}
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(tUTORIAL_INDEX);
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mPartner != null)
		{
			Object.Destroy(mPartner.gameObject);
		}
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnBackToCompanionPushed(null);
	}

	public void OnOKPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.PLAY;
			Close();
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnPartnerPushedFromList(GameObject a_go)
	{
		if (a_go != null)
		{
			string text = a_go.name;
			if (text.Contains("_"))
			{
				string[] array = text.Split('_');
				if (array.Length > 1)
				{
					int selectedPartnerNo = int.Parse(array[1]);
					SelectedPartnerNo = selectedPartnerNo;
				}
			}
		}
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = SELECT_ITEM.PLAY;
			Close();
		}
	}

	public void OnBackToCompanionPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.BACK;
			Close();
		}
	}
}
