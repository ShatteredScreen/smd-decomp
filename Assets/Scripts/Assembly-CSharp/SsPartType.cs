public enum SsPartType
{
	Normal = 0,
	Root = 1,
	Null = 2,
	Bound = 3,
	Sound = 4,
	Num = 5
}
