using System;
using UnityEngine;

public class FriendItem : SMVerticalListItem, IComparable
{
	public enum MODE
	{
		STAR = 0,
		STAGE = 1,
		TROPHY = 2
	}

	public MODE mItem;

	public GameMain mGame = SingletonMonoBehaviour<GameMain>.Instance;

	public string Enable { get; set; }

	public DateTime LatestScroungeTime { get; set; }

	public Texture2D Icon { get; set; }

	public string Gender { get; set; }

	public bool IsSend { get; set; }

	public MyFriend Info { get; set; }

	public GameMain.RANKINGMode Mode { get; set; }

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as FriendItem);
	}

	public int CompareTo(FriendItem _obj)
	{
		try
		{
			if (_obj == null)
			{
				return int.MinValue;
			}
			if (Mode == GameMain.RANKINGMode.STAR)
			{
				return (int)(-(Info.Data.TotalStars - _obj.Info.Data.TotalStars));
			}
			if (Mode == GameMain.RANKINGMode.STAGE)
			{
				return -(Info.Data.Level - _obj.Info.Data.Level);
			}
			if (Mode == GameMain.RANKINGMode.TROPHY)
			{
				return (int)(-(Info.Data.TotalTrophy - _obj.Info.Data.TotalTrophy));
			}
		}
		catch (Exception)
		{
		}
		return int.MinValue;
	}

	public static FriendItem Make(MyFriend a_friend, GameMain.RANKINGMode mode)
	{
		FriendItem friendItem = new FriendItem();
		friendItem.Info = a_friend;
		friendItem.Icon = null;
		friendItem.IsSend = false;
		friendItem.Gender = string.Empty;
		friendItem.Mode = mode;
		return friendItem;
	}

	public static FriendItem Make(MyFriend a_friend)
	{
		FriendItem friendItem = new FriendItem();
		friendItem.Info = a_friend;
		friendItem.Icon = null;
		friendItem.IsSend = false;
		friendItem.Gender = string.Empty;
		return friendItem;
	}
}
