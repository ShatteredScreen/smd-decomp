using System;
using System.Collections.Generic;

public class PlayerBingoEventData : SMJsonData, ICloneable
{
	public int mEventId { get; set; }

	public List<short> mLotteryData { get; set; }

	public int mReLotteryTicket { get; set; }

	public List<List<bool>> mReachEffectList { get; set; }

	public List<List<bool>> mBingoEffectList { get; set; }

	public List<bool> mAllBingoEffectList { get; set; }

	public List<bool> mAllStarCompEffectList { get; set; }

	public List<bool> mGuideDialogList { get; set; }

	public List<int> GotAccessoryList { get; private set; }

	public PlayerBingoEventData()
	{
		mEventId = 0;
		mLotteryData = new List<short>();
		mReLotteryTicket = 0;
		mReachEffectList = new List<List<bool>>();
		mBingoEffectList = new List<List<bool>>();
		mAllBingoEffectList = new List<bool>();
		mAllStarCompEffectList = new List<bool>();
		mGuideDialogList = new List<bool>();
		GotAccessoryList = new List<int>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void GotBingoReward(int a_accessoryID)
	{
		if (!GotAccessoryList.Contains(a_accessoryID))
		{
			GotAccessoryList.Add(a_accessoryID);
		}
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(mEventId);
		data.WriteInt(mLotteryData.Count);
		for (int i = 0; i < mLotteryData.Count; i++)
		{
			data.WriteShort(mLotteryData[i]);
		}
		data.WriteInt(mReLotteryTicket);
		data.WriteInt(mReachEffectList.Count);
		for (int j = 0; j < mReachEffectList.Count; j++)
		{
			List<bool> list = mReachEffectList[j];
			data.WriteInt(list.Count);
			for (int k = 0; k < list.Count; k++)
			{
				data.WriteBool(list[k]);
			}
		}
		data.WriteInt(mBingoEffectList.Count);
		for (int l = 0; l < mBingoEffectList.Count; l++)
		{
			List<bool> list2 = mBingoEffectList[l];
			data.WriteInt(list2.Count);
			for (int m = 0; m < list2.Count; m++)
			{
				data.WriteBool(list2[m]);
			}
		}
		data.WriteInt(mAllBingoEffectList.Count);
		for (int n = 0; n < mAllBingoEffectList.Count; n++)
		{
			data.WriteBool(mAllBingoEffectList[n]);
		}
		data.WriteInt(mAllStarCompEffectList.Count);
		for (int num = 0; num < mAllStarCompEffectList.Count; num++)
		{
			data.WriteBool(mAllStarCompEffectList[num]);
		}
		data.WriteInt(mGuideDialogList.Count);
		for (int num2 = 0; num2 < mGuideDialogList.Count; num2++)
		{
			data.WriteBool(mGuideDialogList[num2]);
		}
		data.WriteInt(GotAccessoryList.Count);
		for (int num3 = 0; num3 < GotAccessoryList.Count; num3++)
		{
			data.WriteInt(GotAccessoryList[num3]);
		}
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		mEventId = data.ReadInt();
		int num = data.ReadInt();
		for (int i = 0; i < num; i++)
		{
			mLotteryData.Add(data.ReadShort());
		}
		mReLotteryTicket = data.ReadInt();
		num = data.ReadInt();
		for (int j = 0; j < num; j++)
		{
			List<bool> list = new List<bool>();
			int num2 = data.ReadInt();
			for (int k = 0; k < num2; k++)
			{
				list.Add(data.ReadBool());
			}
			mReachEffectList.Add(list);
		}
		num = data.ReadInt();
		for (int l = 0; l < num; l++)
		{
			List<bool> list2 = new List<bool>();
			int num3 = data.ReadInt();
			for (int m = 0; m < num3; m++)
			{
				list2.Add(data.ReadBool());
			}
			mBingoEffectList.Add(list2);
		}
		num = data.ReadInt();
		for (int n = 0; n < num; n++)
		{
			mAllBingoEffectList.Add(data.ReadBool());
		}
		num = data.ReadInt();
		for (int num4 = 0; num4 < num; num4++)
		{
			mAllStarCompEffectList.Add(data.ReadBool());
		}
		num = data.ReadInt();
		for (int num5 = 0; num5 < num; num5++)
		{
			mGuideDialogList.Add(data.ReadBool());
		}
		if (reqVersion >= 1230)
		{
			num = data.ReadInt();
			for (int num6 = 0; num6 < num; num6++)
			{
				GotAccessoryList.Add(data.ReadInt());
			}
		}
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerBingoEventData obj = new PlayerBingoEventData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public PlayerBingoEventData Clone()
	{
		return MemberwiseClone() as PlayerBingoEventData;
	}
}
