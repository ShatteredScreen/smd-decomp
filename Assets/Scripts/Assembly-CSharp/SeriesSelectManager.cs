using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeriesSelectManager : MonoBehaviour
{
	public enum STATE
	{
		INIT = 0,
		INIT_APPEAR = 1,
		IN = 2,
		IN_AFTER = 3,
		MAIN = 4,
		FIRST_SELECTED = 5,
		MOVE = 6,
		CANCEL = 7,
		RELEASE = 8,
		WAIT = 9
	}

	protected struct ListItemObject
	{
		public int Index;

		public SMVerticalListItem Item;

		public GameObject GO;

		public float MoveTime;

		public bool IsMoveFinished;
	}

	protected struct LaceObject
	{
		public UISprite Sprite;

		public bool IsDisplayed;

		public int RotateSpeed;

		public Vector3 PositionP;

		public Vector3 PositionL;
	}

	public delegate void OnSeriesSelectFinished(int a_series);

	protected const int INITIAL_X = -520;

	protected const int MOVE_SPEED = 200;

	protected const float MOVE_INTERVAL = 0.05f;

	protected const float MOVE_ALPHA = 5f;

	protected const float MOVE_ALPHA_INTERVAL = 0.02f;

	protected const float INITIAL_WAIT = 0.25f;

	public StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	protected GameMain mGame;

	protected GameStateManager mGameState;

	protected Camera mSubCamera;

	protected UIPanel mSeriesSelectPanel;

	protected UIPanel mSeriesSelectUIPanel;

	protected GameObject mAnchorLeft;

	protected GameObject mSeriesSelectRoot;

	protected ResImage mAtlasHudMap;

	protected ResImage mAtlasHud;

	protected UIFont mFont;

	protected int mBaseDepth = 60;

	protected UILabel mText;

	private OnSeriesSelectFinished OnSelectCallback;

	protected int mCurrentSeries = -1;

	protected int mSelectedSeries = -1;

	protected SMVerticalList mList;

	protected SMVerticalListInfo mListInfo;

	protected Dictionary<int, ListItemObject> mDictItem = new Dictionary<int, ListItemObject>();

	protected List<SMVerticalListItem> mListItem = new List<SMVerticalListItem>();

	protected Dictionary<int, GameObject> mButtonItems = new Dictionary<int, GameObject>();

	protected float mInitialWait;

	protected bool mIsMoveFinished;

	protected bool mIsLaceDisplayed;

	protected bool mIsDescBoardDisplayed;

	protected GameObject mAnchorTop;

	protected GameObject mAnchorTopLeft;

	protected GameObject mAnchorBottomLeft;

	protected LaceObject[] mLaceList = new LaceObject[5];

	protected UISprite mTitleBoardL;

	protected UISprite mTitleBoardR;

	protected UILabel mTitleText;

	protected UISprite mDescBoard;

	protected UILabel mDescText1;

	protected UILabel mDescText2;

	protected UIButton mBackButton;

	protected ScreenOrientation mOrientation;

	protected string mOpenSEName = "SE_GROWN_UP";

	protected string mMoveStartSEName = string.Empty;

	protected string mCancelSEName = "SE_CLOSEOPTION";

	protected float mWaitTime;

	protected Dictionary<int, bool> mIsStartCurrentSeries = new Dictionary<int, bool>();

	public bool IsInitialized { get; protected set; }

	public bool IsLoadFinished { get; protected set; }

	public bool IsDisappearFinished { get; private set; }

	public void SetCallback(OnSeriesSelectFinished a_callback)
	{
		OnSelectCallback = a_callback;
	}

	public void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	public virtual IEnumerator Start()
	{
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
		mBaseDepth = mGame.mDialogManager.GrayScreenDepth + 1;
		GameObject go = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.DEMO_CAMERA));
		go.transform.parent = mGame.mCamera.transform.parent;
		go.transform.localPosition = Vector3.zero;
		go.transform.localScale = Vector3.one;
		mSubCamera = go.GetComponent<Camera>();
		mSubCamera.eventMask = mSubCamera.gameObject.layer;
		mSeriesSelectPanel = Util.CreateGameObject("SeriesSelectPanel", mSubCamera.gameObject).AddComponent<UIPanel>();
		mSeriesSelectPanel.depth = mBaseDepth;
		mSeriesSelectPanel.sortingOrder = mBaseDepth;
		mSeriesSelectUIPanel = Util.CreateGameObject("SeriesSelectUIPanel", mSubCamera.gameObject).AddComponent<UIPanel>();
		mSeriesSelectUIPanel.depth = mBaseDepth + 1;
		mSeriesSelectUIPanel.sortingOrder = mBaseDepth + 1;
		mAnchorLeft = Util.CreateAnchorWithChild("SeiresAnchorLeft", mSeriesSelectPanel.gameObject, UIAnchor.Side.Left, mSubCamera).gameObject;
		mAnchorTop = Util.CreateAnchorWithChild("SeiresAnchorTop", mSeriesSelectPanel.gameObject, UIAnchor.Side.Top, mSubCamera).gameObject;
		mAnchorTopLeft = Util.CreateAnchorWithChild("SeiresAnchorTopLeft", mSeriesSelectPanel.gameObject, UIAnchor.Side.TopLeft, mSubCamera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("SeiresAnchorBottomLeft", mSeriesSelectPanel.gameObject, UIAnchor.Side.BottomLeft, mSubCamera).gameObject;
		mSeriesSelectRoot = Util.CreateGameObject("SeriesSelectRoot", mAnchorLeft.gameObject);
		ResImage hudMap = ResourceManager.LoadImageAsync("HUDMAP");
		while (hudMap.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		mAtlasHudMap = hudMap;
		mAtlasHud = ResourceManager.LoadImage("HUD");
		mFont = GameMain.LoadFont();
		Vector2 logScreen = Util.LogScreenSize();
		CreateLace(0, mAnchorTopLeft, new Vector3(100f, -300f, 0f), new Vector3(184f, -132f, 0f), new Vector3(1f, 1f, 1f), -10);
		CreateLace(1, mAnchorTopLeft, new Vector3(360f, -540f, 0f), new Vector3(512f, -265f, 0f), new Vector3(1f, 1f, 1f), -10);
		CreateLace(2, mAnchorBottomLeft, new Vector3(80f, 490f, 0f), new Vector3(821f, 0f, 0f), new Vector3(1.2f, 1.2f, 1f), -10);
		CreateLace(3, mAnchorBottomLeft, new Vector3(430f, 150f, 0f), new Vector3(155f, 155f, 0f), new Vector3(1.2f, 1.2f, 1f), -10);
		CreateLace(4, mAnchorBottomLeft, new Vector3(170f, 150f, 0f), new Vector3(534f, 21f, 0f), new Vector3(1.2f, 1.2f, 1f), -10);
		mTitleBoardR = Util.CreateSprite("TitleBarR", mAnchorTop.gameObject, mAtlasHud.Image);
		Util.SetSpriteInfo(mTitleBoardR, "menubar_frame", mBaseDepth + 10, new Vector3(-2f, 168f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBoardL = Util.CreateSprite("TitleBarL", mAnchorTop.gameObject, mAtlasHud.Image);
		Util.SetSpriteInfo(mTitleBoardL, "menubar_frame", mBaseDepth + 10, new Vector3(2f, 168f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleText = Util.CreateLabel("TitleText", mAnchorTop.gameObject, mFont);
		Util.SetLabelInfo(mTitleText, Localization.Get("SeriesSelect"), mBaseDepth + 12, new Vector3(0f, -26f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
		mTitleText.color = Color.white;
		mDescBoard = Util.CreateSprite("Desc", mAnchorLeft.gameObject, mAtlasHudMap.Image);
		Util.SetSpriteInfo(mDescBoard, "instruction_panel2", mBaseDepth + 10, new Vector3(265f, 400f, 0f), Vector3.one, false);
		mDescBoard.type = UIBasicSprite.Type.Advanced;
		mDescBoard.centerType = UIBasicSprite.AdvancedType.Sliced;
		mDescBoard.SetDimensions(420, 90);
		mDescBoard.alpha = 0f;
		NGUITools.SetActive(mDescBoard.gameObject, false);
		mDescText1 = Util.CreateLabel("DescText1", mDescBoard.gameObject, mFont);
		Util.SetLabelInfo(mDescText1, Localization.Get("SeasonNotice_00"), mBaseDepth + 11, new Vector3(0f, 10f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		mDescText1.color = Def.DEFAULT_MESSAGE_COLOR;
		mDescText1.spacingY = 6;
		mDescText1.alpha = 0f;
		mDescText2 = Util.CreateLabel("DescText2", mDescBoard.gameObject, mFont);
		Util.SetLabelInfo(mDescText2, Localization.Get("SeasonNotice_01"), mBaseDepth + 12, new Vector3(0f, -15f, 0f), 18, 0, 0, UIWidget.Pivot.Center);
		mDescText2.color = Color.red;
		mDescText2.spacingY = 6;
		mDescText2.alpha = 0f;
		mBackButton = Util.CreateJellyImageButton("Back", mAnchorLeft.gameObject, mAtlasHud.Image);
		Util.SetImageButtonInfo(mBackButton, "button_return", "button_return", "button_return", mBaseDepth + 10, new Vector3(-102f, -168f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mBackButton, this, "OnBackButtonPushed", UIButtonMessage.Trigger.OnClick);
		NGUITools.SetActive(mBackButton.gameObject, false);
		if (!string.IsNullOrEmpty(mOpenSEName))
		{
			mGame.PlaySe(mOpenSEName, -1);
		}
		mOrientation = Util.ScreenOrientation;
		SetLayout(mOrientation);
		IsInitialized = true;
		mState.Change(STATE.INIT);
	}

	protected IEnumerator ReleaseResurces()
	{
		UnityEngine.Object.Destroy(base.gameObject);
		yield break;
	}

	protected void OnDestroy()
	{
		if (mAnchorLeft != null)
		{
			UnityEngine.Object.Destroy(mAnchorLeft.gameObject);
			mAnchorLeft = null;
		}
		if (mAnchorTop != null)
		{
			UnityEngine.Object.Destroy(mAnchorTop.gameObject);
			mAnchorTop = null;
		}
		if (mAnchorTopLeft != null)
		{
			UnityEngine.Object.Destroy(mAnchorTopLeft.gameObject);
			mAnchorTopLeft = null;
		}
		if (mAnchorBottomLeft != null)
		{
			UnityEngine.Object.Destroy(mAnchorBottomLeft.gameObject);
			mAnchorBottomLeft = null;
		}
		if (mSubCamera != null)
		{
			UnityEngine.Object.Destroy(mSubCamera.gameObject);
		}
		if (mSeriesSelectUIPanel != null)
		{
			UnityEngine.Object.Destroy(mSeriesSelectUIPanel.gameObject);
		}
	}

	protected void OnBackButtonPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mSelectedSeries = -1;
			OnClose();
		}
	}

	protected void OnClose()
	{
		if (OnSelectCallback != null)
		{
			OnSelectCallback(mSelectedSeries);
		}
	}

	protected virtual void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (IsInitialized)
			{
				mState.Reset(STATE.INIT_APPEAR, true);
			}
			break;
		case STATE.INIT_APPEAR:
			if (mState.IsChanged())
			{
				mInitialWait = 0f;
				int num = 0;
				int num2 = 0;
				SMMapPageData value;
				mGame.mMapPageData.TryGetValue(Def.SERIES.SM_FIRST, out value);
				if (value != null)
				{
					num = value.MaxMainStageNum;
					num2 = value.MaxSubStageNum;
				}
				List<CharaSilhouettte> firstCharaInfo = GetFirstCharaInfo();
				ListItemObject value2 = default(ListItemObject);
				value2.Index = 0;
				SMVerticalListItem item = (value2.Item = SeriesSelectItem.Make(Def.SERIES.SM_FIRST, num, (num + num2) * 3, "panel_season00", "SeasonTitle_00", firstCharaInfo));
				mListItem.Add(item);
				mDictItem.Add(value2.Index, value2);
				mGame.mMapPageData.TryGetValue(Def.SERIES.SM_R, out value);
				if (value != null)
				{
					num = value.MaxMainStageNum;
					num2 = value.MaxSubStageNum;
				}
				firstCharaInfo = GetRCharaInfo();
				value2 = default(ListItemObject);
				value2.Index = 1;
				item = (value2.Item = SeriesSelectItem.Make(Def.SERIES.SM_R, num, (num + num2) * 3, "panel_season01", "SeasonTitle_01", firstCharaInfo));
				mListItem.Add(item);
				mDictItem.Add(value2.Index, value2);
				mGame.mMapPageData.TryGetValue(Def.SERIES.SM_S, out value);
				if (value != null)
				{
					num = value.MaxMainStageNum;
					num2 = value.MaxSubStageNum;
				}
				firstCharaInfo = GetSCharaInfo();
				value2 = default(ListItemObject);
				value2.Index = 2;
				item = (value2.Item = SeriesSelectItem.Make(Def.SERIES.SM_S, num, (num + num2) * 3, "panel_season02", "SeasonTitle_02", firstCharaInfo));
				mListItem.Add(item);
				mDictItem.Add(value2.Index, value2);
				mGame.mMapPageData.TryGetValue(Def.SERIES.SM_SS, out value);
				if (value != null)
				{
					num = value.MaxMainStageNum;
					num2 = value.MaxSubStageNum;
				}
				firstCharaInfo = GetSSCharaInfo();
				value2 = default(ListItemObject);
				value2.Index = 3;
				item = (value2.Item = SeriesSelectItem.Make(Def.SERIES.SM_SS, num, (num + num2) * 3, "panel_season03", "SeasonTitle_03", firstCharaInfo));
				mListItem.Add(item);
				mDictItem.Add(value2.Index, value2);
				mGame.mMapPageData.TryGetValue(Def.SERIES.SM_STARS, out value);
				if (value != null)
				{
					num = value.MaxMainStageNum;
					num2 = value.MaxSubStageNum;
				}
				firstCharaInfo = GetSTARSCharaInfo();
				value2 = default(ListItemObject);
				value2.Index = 4;
				item = (value2.Item = SeriesSelectItem.Make(Def.SERIES.SM_STARS, num, (num + num2) * 3, "panel_season04", "SeasonTitle_04", firstCharaInfo));
				mListItem.Add(item);
				mDictItem.Add(value2.Index, value2);
				for (Def.SERIES sERIES = Def.SERIES.SM_GF00; sERIES <= Def.SeriesGaidenMax; sERIES++)
				{
					mGame.mMapPageData.TryGetValue(sERIES, out value);
					if (value != null)
					{
						num = value.MaxMainStageNum;
						num2 = value.MaxSubStageNum;
					}
					firstCharaInfo = GetGSeriesCharaInfo(sERIES);
					value2 = default(ListItemObject);
					value2.Index = (int)sERIES;
					string a_imageName = "panel_season" + value2.Index;
					string a_titleKey = "SeasonTitle_" + value2.Index;
					item = (value2.Item = SeriesSelectItem.Make(sERIES, num, (num + num2) * 3, a_imageName, a_titleKey, firstCharaInfo));
					mListItem.Add(item);
					mDictItem.Add(value2.Index, value2);
				}
				int num3 = 600;
				int num4 = 880;
				mListInfo = new SMVerticalListInfo(1, num3, 450f, 1, num3, num4, 1, 210);
				SetLayout(Util.ScreenOrientation);
			}
			mInitialWait += Time.deltaTime;
			if (mList.initilized && mInitialWait > 0.25f)
			{
				IsLoadFinished = true;
				mInitialWait = 0f;
				ResetListItemPosition();
				mState.Change(STATE.IN);
			}
			break;
		case STATE.IN:
			if (mState.IsChanged())
			{
				MoveIn();
				DisplayLace();
			}
			if (mIsMoveFinished && mIsLaceDisplayed)
			{
				mState.Change(STATE.IN_AFTER);
			}
			break;
		case STATE.IN_AFTER:
			if (mState.IsChanged())
			{
				DisplayDescBoard();
			}
			if (mIsDescBoardDisplayed)
			{
				NGUITools.SetActive(mBackButton.gameObject, true);
				NGUITools.SetActive(mDescBoard.gameObject, true);
				mGame.PlaySe("VOICE_025", -1);
				mList.CreateScrollBarForce();
				mIsStartCurrentSeries[mCurrentSeries] = true;
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.MAIN:
			if (mState.IsChanged())
			{
			}
			if (mGame.mBackKeyTrg)
			{
				OnBackButtonPushed();
			}
			break;
		case STATE.FIRST_SELECTED:
			if (mState.IsChanged())
			{
				mWaitTime = 0f;
			}
			mWaitTime += Time.deltaTime;
			if (mWaitTime > 0.1f)
			{
				FirstSelectProcess();
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.MOVE:
			return;
		case STATE.CANCEL:
			if (mState.IsChanged())
			{
				NGUITools.SetActive(mBackButton.gameObject, false);
				NGUITools.SetActive(mDescBoard.gameObject, false);
				NGUITools.SetActive(mTitleBoardL.gameObject, false);
				NGUITools.SetActive(mTitleBoardR.gameObject, false);
				NGUITools.SetActive(mTitleText.gameObject, false);
				mList.HideScrollBar();
				StartCoroutine(LaceDisplay(-5f, 0f));
				MoveOut();
			}
			if (mIsMoveFinished)
			{
				mState.Change(STATE.RELEASE);
			}
			break;
		case STATE.RELEASE:
			if (mState.IsChanged())
			{
				StartCoroutine(ReleaseResurces());
			}
			break;
		}
		mState.Update();
		if (mOrientation != Util.ScreenOrientation && IsInitialized && IsLoadFinished)
		{
			SetLayout(Util.ScreenOrientation);
			mOrientation = Util.ScreenOrientation;
		}
	}

	protected virtual void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		bool flag = false;
		float num = 0f;
		float num2 = 0f;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			flag = true;
			num = 265f;
			num2 = -110f;
			if (mDescBoard != null)
			{
				mDescBoard.gameObject.transform.localPosition = new Vector3(265f, 400f, 0f);
			}
			if (mBackButton != null)
			{
				mBackButton.gameObject.transform.localPosition = new Vector3(555f, 400f, 0f);
			}
			for (int j = 0; j < mLaceList.Length; j++)
			{
				mLaceList[j].Sprite.gameObject.transform.localPosition = mLaceList[j].PositionP;
			}
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			num = 265f;
			num2 = -85f;
			if (mDescBoard != null)
			{
				mDescBoard.gameObject.transform.localPosition = new Vector3(265f, 185f, 0f);
			}
			if (mBackButton != null)
			{
				mBackButton.gameObject.transform.localPosition = new Vector3(555f, 185f, 0f);
			}
			for (int i = 0; i < mLaceList.Length; i++)
			{
				mLaceList[i].Sprite.gameObject.transform.localPosition = mLaceList[i].PositionL;
			}
			break;
		}
		}
		if (mList == null)
		{
			if (mListItem.Count > 0)
			{
				mList = SMVerticalList.Make(mSeriesSelectRoot.gameObject, this, mListInfo, mListItem, num, num2, mBaseDepth + 6, flag, UIScrollView.Movement.Vertical, false, !mIsMoveFinished);
				mList.OnCreateItem = OnCreateItem;
			}
		}
		else
		{
			mList.OnScreenChanged(flag, num, num2);
		}
	}

	protected void ResetListItemPosition()
	{
		foreach (KeyValuePair<int, ListItemObject> item in mDictItem)
		{
			GameObject gO = item.Value.GO;
			NGUITools.SetActive(gO, true);
			Vector3 localPosition = gO.transform.localPosition;
			gO.transform.localPosition = new Vector3(-520f, localPosition.y, localPosition.z);
		}
	}

	protected SeriesSelectItem GetSelectedButton(GameObject go, out int selected)
	{
		selected = -1;
		int num = 0;
		string text = go.name;
		string[] array = text.Split('_');
		if (array.Length == 2)
		{
			try
			{
				num = int.Parse(array[1]);
			}
			catch (Exception ex)
			{
				BIJLog.E("button name parse error!:" + ex.Message);
				num = -1;
			}
		}
		else
		{
			BIJLog.E("button name format error!:" + text);
			num = -1;
		}
		ListItemObject value;
		if (!mDictItem.TryGetValue(num, out value))
		{
			BIJLog.E("Error No Get Value Index=" + num);
			return null;
		}
		SeriesSelectItem seriesSelectItem = value.Item as SeriesSelectItem;
		if (seriesSelectItem == null)
		{
			return null;
		}
		selected = num;
		return seriesSelectItem;
	}

	public virtual void OnSeriesSelected(GameObject go)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		int selected;
		SeriesSelectItem selectedButton = GetSelectedButton(go, out selected);
		if (selectedButton != null)
		{
			bool flag = true;
			if (!mGame.mGameProfile.IsSeriesAvailable(selectedButton.Series))
			{
				flag = false;
			}
			if (flag)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				mSelectedSeries = selected;
				OnClose();
			}
			else
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
		}
	}

	protected virtual void OnCreateItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 6;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUDMAP").Image;
		UIFont atlasFont = GameMain.LoadFont();
		SeriesSelectItem seriesSelectItem = item as SeriesSelectItem;
		int series = (int)seriesSelectItem.Series;
		ListItemObject value;
		if (!mDictItem.TryGetValue(series, out value))
		{
			BIJLog.E("Error No Get Value Index=" + index + ", Series=" + series);
			return;
		}
		bool flag = Def.IsGaidenSeries(seriesSelectItem.Series);
		GameObject gameObject = (value.GO = Util.CreateGameObject("Root" + series, itemGO));
		value.MoveTime = 0f;
		value.IsMoveFinished = false;
		mDictItem[series] = value;
		bool flag2 = false;
		if (!mGame.mGameProfile.IsSeriesAvailable(seriesSelectItem.Series))
		{
			flag2 = true;
		}
		float cellWidth = mList.mList.cellWidth;
		float cellHeight = mList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back" + series, itemGO, image2);
		Util.SetSpriteInfo(uISprite, "null", num, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		GameObject gameObject2 = null;
		string empty = string.Empty;
		string text = "panel_season";
		string imageName = "ribbon_red";
		if (flag)
		{
			text = "panel_season1";
			imageName = "ribbon_purple";
		}
		if (!flag2)
		{
			UIButton uIButton = Util.CreateJellyImageButton("Board_" + series, gameObject, image2, Util.BUTTON_TYPE.NORMAL2);
			Util.SetImageButtonInfo(uIButton, text, text, text, num + 1, new Vector3(0f, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnSeriesSelected", UIButtonMessage.Trigger.OnClick);
			UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView.scrollView = mList.mListScrollView;
			gameObject2 = uIButton.gameObject;
			empty = Localization.Get(seriesSelectItem.TitleKey);
			if (mGame.mPlayer.Data.CurrentSeries == seriesSelectItem.Series)
			{
				mCurrentSeries = (int)seriesSelectItem.Series;
				StartCoroutine(DisplayCurrentSeries(gameObject2, num, mCurrentSeries));
			}
		}
		else
		{
			UISprite uISprite2 = Util.CreateSprite("Board_" + series, gameObject, image2);
			Util.SetSpriteInfo(uISprite2, text, num + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
			gameObject2 = uISprite2.gameObject;
			empty = Localization.Get("SeasonTitle_99");
		}
		mButtonItems.Add(series, gameObject2);
		UISprite uISprite3 = Util.CreateSprite("TitleBoard" + series, gameObject2, image2);
		Util.SetSpriteInfo(uISprite3, imageName, num + 4, new Vector3(63f, 63f, 0f), Vector3.one, false);
		uISprite3.type = UIBasicSprite.Type.Sliced;
		uISprite3.SetDimensions(350, 40);
		UILabel uILabel = Util.CreateLabel("TitleStr" + series, uISprite3.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, empty, num + 5, new Vector3(0f, 0f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		uILabel.effectStyle = UILabel.Effect.Shadow;
		uILabel.effectColor = new Color(0f, 0f, 0f);
		uILabel.effectDistance = new Vector2(2f, 2f);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(320, 30);
		string imageName2 = seriesSelectItem.ImageName;
		if (flag2)
		{
			imageName2 = "panel_season_no";
		}
		UISprite sprite = Util.CreateSprite("EnemyImage" + series, gameObject2, image2);
		Util.SetSpriteInfo(sprite, imageName2, num + 3, new Vector3(-117f, 0f, 0f), Vector3.one, false);
		int seriesCampaignMoves = mGame.GetSeriesCampaignMoves(seriesSelectItem.Series);
		if (seriesCampaignMoves > 0)
		{
			UISprite sprite2 = Util.CreateSprite("Campaign" + series, gameObject2, image2);
			Util.SetSpriteInfo(sprite2, "LC_series_campaign00", num + 4, new Vector3(-186f, 87f, 0f), Vector3.one, false);
		}
		if (!flag2)
		{
			for (int i = 0; i < seriesSelectItem.SilhouetteList.Count; i++)
			{
				CharaSilhouettte charaSilhouettte = seriesSelectItem.SilhouetteList[i];
				UISprite sprite3 = Util.CreateSprite("chara" + charaSilhouettte.CharaID, gameObject2, image2);
				Util.SetSpriteInfo(sprite3, charaSilhouettte.ImageName, num + 3, charaSilhouettte.Position, Vector3.one, false);
			}
			string imageName3 = "panel_value";
			if (flag)
			{
				imageName3 = "panel_value1";
			}
			UISprite uISprite4 = Util.CreateSprite("StageBoard" + series, gameObject2, image2);
			Util.SetSpriteInfo(uISprite4, imageName3, num + 4, new Vector3(40f, -65f, 0f), Vector3.one, false);
			uISprite4.type = UIBasicSprite.Type.Sliced;
			uISprite4.SetDimensions(120, 40);
			UISprite sprite4 = Util.CreateSprite("StageButton" + series, uISprite4.gameObject, image2);
			Util.SetSpriteInfo(sprite4, "btn_stage_traverse", num + 5, new Vector3(-39f, 0f, 0f), Vector3.one, false);
			UILabel label = Util.CreateLabel("StageStr" + series, uISprite4.gameObject, atlasFont);
			PlayerMapData data;
			mGame.mPlayer.Data.GetMapData(seriesSelectItem.Series, out data);
			int clearedMainStage = data.GetClearedMainStage();
			string text2 = string.Empty + clearedMainStage + "/" + seriesSelectItem.MaxStage;
			Util.SetLabelInfo(label, text2, num + 6, new Vector3(18f, 0f, 0f), 16, 0, 0, UIWidget.Pivot.Center);
			UISprite uISprite5 = Util.CreateSprite("StarBoard" + series, gameObject2, image2);
			Util.SetSpriteInfo(uISprite5, imageName3, num + 4, new Vector3(178f, -65f, 0f), Vector3.one, false);
			uISprite5.type = UIBasicSprite.Type.Sliced;
			uISprite5.SetDimensions(120, 40);
			UISprite sprite5 = Util.CreateSprite("StarButton" + series, uISprite5.gameObject, image2);
			Util.SetSpriteInfo(sprite5, "btn_stage_star01", num + 5, new Vector3(-39f, 0f, 0f), Vector3.one, false);
			UILabel label2 = Util.CreateLabel("StarStr" + series, uISprite5.gameObject, atlasFont);
			int clearedStarCount = data.GetClearedStarCount();
			string text3 = string.Empty + clearedStarCount + "/" + seriesSelectItem.MaxStar;
			Util.SetLabelInfo(label2, text3, num + 6, new Vector3(18f, 0f, 0f), 16, 0, 0, UIWidget.Pivot.Center);
			bool flag3 = false;
			bool flag4 = false;
			if (clearedStarCount >= seriesSelectItem.MaxStar)
			{
				flag4 = true;
			}
			else if (clearedMainStage >= seriesSelectItem.MaxStage)
			{
				flag3 = true;
			}
			UISprite uISprite6 = null;
			if (flag4)
			{
				uISprite6 = Util.CreateSprite("CompleteSP" + series, gameObject2, image2);
				Util.SetSpriteInfo(uISprite6, "season_complete", num + 6, new Vector3(-142f, -65f, 0f), Vector3.one, false);
			}
			else if (flag3)
			{
				uISprite6 = Util.CreateSprite("ClearedSP" + series, gameObject2, image2);
				Util.SetSpriteInfo(uISprite6, "season_clear", num + 6, new Vector3(-142f, -65f, 0f), Vector3.one, false);
			}
		}
		NGUITools.SetActive(gameObject, mIsMoveFinished);
	}

	public void CancelPushed()
	{
		if (!string.IsNullOrEmpty(mCancelSEName))
		{
			mGame.PlaySe(mCancelSEName, -1);
		}
		mState.Change(STATE.CANCEL);
	}

	protected List<CharaSilhouettte> GetFirstCharaInfo()
	{
		List<CharaSilhouettte> list = new List<CharaSilhouettte>();
		CharaSilhouettte item = default(CharaSilhouettte);
		item.CharaID = 1;
		item.ImageName = "char_mercury";
		item.Position = new Vector3(33f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 2;
		item.ImageName = "char_mars";
		item.Position = new Vector3(90f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 3;
		item.ImageName = "char_jupiter";
		item.Position = new Vector3(150f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 4;
		item.ImageName = "char_venus";
		item.Position = new Vector3(210f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		return list;
	}

	protected List<CharaSilhouettte> GetRCharaInfo()
	{
		List<CharaSilhouettte> list = new List<CharaSilhouettte>();
		CharaSilhouettte item = default(CharaSilhouettte);
		item.CharaID = 16;
		item.ImageName = "char_chibi";
		item.Position = new Vector3(33f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 15;
		item.ImageName = "char_moon";
		item.Position = new Vector3(113f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 19;
		item.ImageName = "char_pluto";
		item.Position = new Vector3(184f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		return list;
	}

	protected List<CharaSilhouettte> GetSCharaInfo()
	{
		List<CharaSilhouettte> list = new List<CharaSilhouettte>();
		CharaSilhouettte item = default(CharaSilhouettte);
		item.CharaID = 29;
		item.ImageName = "char_moon_2";
		item.Position = new Vector3(33f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 26;
		item.ImageName = "char_michiru";
		item.Position = new Vector3(109f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 25;
		item.ImageName = "char_haruka";
		item.Position = new Vector3(180f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		return list;
	}

	protected List<CharaSilhouettte> GetSSCharaInfo()
	{
		List<CharaSilhouettte> list = new List<CharaSilhouettte>();
		CharaSilhouettte item = default(CharaSilhouettte);
		item.CharaID = 66;
		item.ImageName = "char_chibi_2";
		item.Position = new Vector3(80f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 67;
		item.ImageName = "char_elios";
		item.Position = new Vector3(145f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		return list;
	}

	protected List<CharaSilhouettte> GetSTARSCharaInfo()
	{
		List<CharaSilhouettte> list = new List<CharaSilhouettte>();
		CharaSilhouettte item = default(CharaSilhouettte);
		item.CharaID = 106;
		item.ImageName = "char_StarsYaten";
		item.Position = new Vector3(33f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 105;
		item.ImageName = "char_StarsTaiki";
		item.Position = new Vector3(109f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		item = default(CharaSilhouettte);
		item.CharaID = 104;
		item.ImageName = "char_StarsSeiya";
		item.Position = new Vector3(180f, 0f, 0f);
		if (mGame.mPlayer.IsCompanionUnlock(item.CharaID))
		{
			item.ImageName += "_c";
		}
		else
		{
			item.ImageName += "_s";
		}
		list.Add(item);
		return list;
	}

	protected List<CharaSilhouettte> GetGSeriesCharaInfo(Def.SERIES aSeries)
	{
		List<CharaSilhouettte> result = new List<CharaSilhouettte>();
		if (aSeries == Def.SERIES.SM_GF00)
		{
		}
		return result;
	}

	protected virtual IEnumerator MoveProcess(int a_direction, int a_targetX)
	{
		int moveNum = 1;
		if (a_direction < 0)
		{
			moveNum = mLaceList.Length;
		}
		if (!string.IsNullOrEmpty(mMoveStartSEName))
		{
			mGame.PlaySe(mMoveStartSEName, -1);
		}
		bool[] isMoveFinishes = new bool[mListItem.Count];
		float[] degs = new float[mListItem.Count];
		float[] init_pos = new float[mListItem.Count];
		for (int k = 0; k < mListItem.Count; k++)
		{
			ListItemObject lio;
			if (!mDictItem.TryGetValue((int)((SeriesSelectItem)mListItem[k]).Series, out lio))
			{
				BIJLog.E("Error No Get Value Index=" + k);
			}
			else
			{
				init_pos[k] = lio.GO.transform.localPosition.x;
			}
		}
		float interval = 0f;
		while (true)
		{
			for (int j = 0; j < moveNum; j++)
			{
				ListItemObject lio2;
				if (!mDictItem.TryGetValue((int)((SeriesSelectItem)mListItem[j]).Series, out lio2))
				{
					BIJLog.E("Error No Get Value Index=" + j);
					continue;
				}
				Vector3 pos = lio2.GO.transform.localPosition;
				degs[j] += Time.deltaTime * (float)Mathf.Abs(a_direction);
				if (degs[j] > 90f)
				{
					degs[j] = 90f;
				}
				float x = init_pos[j] + ((float)a_targetX - init_pos[j]) * Mathf.Sin(degs[j] * ((float)Math.PI / 180f));
				if (a_direction > 0 && x >= (float)a_targetX)
				{
					isMoveFinishes[j] = true;
					x = a_targetX;
				}
				else if (a_direction < 0 && x <= (float)a_targetX)
				{
					isMoveFinishes[j] = true;
					x = a_targetX;
				}
				lio2.GO.transform.localPosition = new Vector3(x, pos.y, pos.z);
			}
			if (moveNum < mDictItem.Count)
			{
				interval += Time.deltaTime;
				if (interval > 0.05f)
				{
					moveNum++;
					if (!string.IsNullOrEmpty(mMoveStartSEName))
					{
						mGame.PlaySe(mMoveStartSEName, -1);
					}
					interval = 0f;
					if (moveNum >= mDictItem.Count)
					{
						moveNum = mDictItem.Count;
					}
				}
			}
			bool isFinished = true;
			for (int i = 0; i < isMoveFinishes.Length; i++)
			{
				if (!isMoveFinishes[i])
				{
					isFinished = false;
					break;
				}
			}
			if (isFinished)
			{
				break;
			}
			yield return null;
		}
		mIsMoveFinished = true;
	}

	protected void MoveIn()
	{
		mIsMoveFinished = false;
		StartCoroutine(MoveProcess(200, 0));
	}

	protected void MoveOut()
	{
		mIsMoveFinished = false;
		StartCoroutine(MoveProcess(-600, -520));
	}

	protected void CreateLace(int a_index, GameObject a_anchor, Vector3 a_posP, Vector3 a_posL, Vector3 a_scale, int a_speed)
	{
		UISprite uISprite = Util.CreateSprite("Lace" + a_index, a_anchor, mAtlasHudMap.Image);
		Util.SetSpriteInfo(uISprite, "back_lace", mBaseDepth + a_index, a_posP, a_scale, false);
		uISprite.alpha = 0f;
		LaceObject laceObject = default(LaceObject);
		laceObject.Sprite = uISprite;
		laceObject.IsDisplayed = false;
		laceObject.RotateSpeed = a_speed;
		laceObject.PositionP = a_posP;
		laceObject.PositionL = a_posL;
		mLaceList[a_index] = laceObject;
	}

	protected void DisplayLace()
	{
		mIsLaceDisplayed = false;
		for (int i = 0; i < mLaceList.Length; i++)
		{
			StartCoroutine(LaceRotate(mLaceList[i].Sprite, mLaceList[i].RotateSpeed));
		}
		StartCoroutine(LaceDisplay(5f, 1f));
	}

	protected IEnumerator LaceDisplay(float a_direction, float a_targetA, bool a_same = false)
	{
		int moveNum = 1;
		bool[] isMoveFinishes = new bool[mLaceList.Length];
		if (a_same)
		{
			moveNum = mLaceList.Length;
		}
		float interval = 0f;
		while (true)
		{
			for (int j = 0; j < moveNum; j++)
			{
				LaceObject lo = mLaceList[j];
				float alpha2 = lo.Sprite.alpha;
				alpha2 += a_direction * Time.deltaTime;
				if (a_direction > 0f && alpha2 >= a_targetA)
				{
					isMoveFinishes[j] = true;
					alpha2 = 1f;
				}
				else if (a_direction < 0f && alpha2 <= a_targetA)
				{
					isMoveFinishes[j] = true;
					alpha2 = 0f;
				}
				lo.Sprite.alpha = alpha2;
			}
			if (moveNum < mLaceList.Length)
			{
				interval += Time.deltaTime;
				if (interval > 0.02f)
				{
					moveNum++;
					interval = 0f;
					if (moveNum >= mLaceList.Length)
					{
						moveNum = mLaceList.Length;
					}
				}
			}
			bool isFinished = true;
			for (int i = 0; i < isMoveFinishes.Length; i++)
			{
				if (!isMoveFinishes[i])
				{
					isFinished = false;
					break;
				}
			}
			if (isFinished)
			{
				break;
			}
			yield return null;
		}
		mIsLaceDisplayed = true;
	}

	protected IEnumerator LaceRotate(UISprite lace, float speed)
	{
		float angle = 0f;
		while (true)
		{
			angle += Time.deltaTime * speed;
			lace.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	protected void DisplayDescBoard()
	{
		mIsDescBoardDisplayed = false;
		StartCoroutine(DescBoardDisplay(5f, 1f));
	}

	protected IEnumerator DescBoardDisplay(float a_direction, float a_targetA)
	{
		while (true)
		{
			float alpha2 = mDescBoard.alpha;
			alpha2 += a_direction * Time.deltaTime;
			bool isFinished = false;
			if (a_direction > 0f && alpha2 >= a_targetA)
			{
				alpha2 = 1f;
				isFinished = true;
			}
			else if (a_direction < 0f && alpha2 <= a_targetA)
			{
				alpha2 = 0f;
				isFinished = true;
			}
			mDescBoard.alpha = alpha2;
			mDescText1.alpha = alpha2;
			mDescText2.alpha = alpha2;
			if (isFinished)
			{
				break;
			}
			yield return null;
		}
		mIsDescBoardDisplayed = true;
	}

	protected virtual IEnumerator DisplayCurrentSeries(GameObject a_go, int a_depth, int series_id)
	{
		bool coroutine_enable = false;
		while (!coroutine_enable)
		{
			if (!mIsStartCurrentSeries.TryGetValue(series_id, out coroutine_enable))
			{
				coroutine_enable = false;
			}
			if (!coroutine_enable)
			{
				yield return null;
			}
		}
		UISprite currentSprite = Util.CreateSprite("CurrentObject", a_go, mAtlasHudMap.Image);
		Util.SetSpriteInfo(currentSprite, "panel_imakokoring1", a_depth + 2, Vector3.zero, Vector3.one, false);
		currentSprite.type = UIBasicSprite.Type.Sliced;
		currentSprite.SetDimensions(530, 230);
		currentSprite.alpha = 0.8f;
		List<UISprite> shadowSpriteList = new List<UISprite>();
		for (int m = 0; m < 2; m++)
		{
			UISprite sprite = Util.CreateSprite("Shadow_" + m, currentSprite.gameObject, mAtlasHudMap.Image);
			Util.SetSpriteInfo(sprite, "panel_imakokoring2", a_depth + 1 - m, Vector3.zero, Vector3.one, false);
			sprite.type = UIBasicSprite.Type.Sliced;
			sprite.SetDimensions(530, 230);
			sprite.alpha = 0f;
			shadowSpriteList.Add(sprite);
		}
		float turnoffTime = 0f;
		float highlightTime = 1.5f;
		bool isHighlight = false;
		bool[] isHighlightMove = new bool[shadowSpriteList.Count];
		float[] highlightMove = new float[shadowSpriteList.Count];
		for (int l = 0; l < isHighlightMove.Length; l++)
		{
			isHighlightMove[l] = false;
			highlightMove[l] = 0f;
		}
		while (coroutine_enable)
		{
			highlightTime += Time.deltaTime;
			if (highlightTime > 1.5f)
			{
				isHighlight = true;
				highlightTime = 0f;
			}
			if (isHighlight)
			{
				int look = -1;
				for (int j = 0; j < isHighlightMove.Length; j++)
				{
					if (!isHighlightMove[j])
					{
						look = j;
						break;
					}
				}
				if (look != -1)
				{
					currentSprite.alpha = 1f;
					turnoffTime = 0.1f;
					isHighlightMove[look] = true;
					highlightMove[look] = 0f;
					shadowSpriteList[look].alpha = 1f;
				}
				isHighlight = false;
			}
			for (int i = 0; i < 2; i++)
			{
				if (isHighlightMove[i])
				{
					highlightMove[i] += Time.deltaTime * 60f;
					float deg = highlightMove[i];
					if (deg > 90f)
					{
						deg = 0f;
						isHighlightMove[i] = false;
						highlightMove[i] = 0f;
					}
					float rad = deg * ((float)Math.PI / 180f);
					float size = 1f + 0.1f * Mathf.Sin(rad);
					shadowSpriteList[i].alpha = 1f - 1f * Mathf.Sin(rad);
					shadowSpriteList[i].transform.localScale = new Vector3(size, size, 1f);
				}
			}
			if (turnoffTime > 0f)
			{
				turnoffTime -= Time.deltaTime;
				if (turnoffTime < 0f)
				{
					currentSprite.alpha = 0.8f;
					turnoffTime = 0f;
				}
			}
			if (!mIsStartCurrentSeries.TryGetValue(series_id, out coroutine_enable))
			{
				coroutine_enable = false;
			}
			yield return null;
		}
		UnityEngine.Object.Destroy(currentSprite.gameObject);
		for (int k = 0; k < 2; k++)
		{
			UnityEngine.Object.Destroy(shadowSpriteList[k].gameObject);
		}
	}

	protected virtual void FirstSelectProcess()
	{
	}
}
