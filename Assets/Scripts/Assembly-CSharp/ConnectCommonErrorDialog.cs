using System.Collections.Generic;
using UnityEngine;

public class ConnectCommonErrorDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		RETRY = 0,
		GIVEUP = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public enum STYLE
	{
		NONE = 0,
		RETRY = 1,
		RETRY_CLOSE = 2,
		CLOSE = 3,
		GOTO_TITLE = 4,
		RETRY_COUNT = 5,
		MAINTENANCE_RETRY = 6,
		MAINTENANCE_RETRY_CLOSE = 7,
		MAINTENANCE_CLOSE = 8,
		MAINTENANCE_TITLE = 9
	}

	public delegate void OnDialogClosed(SELECT_ITEM i, int a_state);

	public const int NOUSE_RETRYCOUNT = -100;

	private const int MAX_RETRY_NUM = 5;

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	[SerializeField]
	private STYLE mStyle = STYLE.RETRY;

	private string mTitle = string.Empty;

	private string mDesc = string.Empty;

	private int mRetryCount;

	private bool mRetryOver;

	private float mCloseTimer = 1f;

	private float mCounter;

	private bool mButtonDisplay;

	private OnDialogClosed mCallback;

	private List<UIButton> mButtonList = new List<UIButton>();

	private int mCallbackState;

	private bool mIsForemost;

	private SELECT_ITEM mBackKeySelected;

	public override void Start()
	{
		base.Start();
	}

	public void Init(string title, string desc, STYLE a_style, int retry = -100, int a_state = 0)
	{
		mTitle = title;
		mDesc = desc;
		mStyle = a_style;
		mCallbackState = a_state;
		STYLE sTYLE = mStyle;
		if (sTYLE == STYLE.GOTO_TITLE || sTYLE == STYLE.RETRY_COUNT)
		{
			mRetryCount = retry;
		}
		else
		{
			mRetryCount = -100;
		}
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		if (!mButtonDisplay)
		{
			mCounter += Time.deltaTime;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (!mButtonDisplay && mCounter >= 2f)
			{
				for (int i = 0; i < mButtonList.Count; i++)
				{
					NGUITools.SetActive(mButtonList[i].gameObject, true);
				}
				mButtonDisplay = true;
			}
			break;
		}
		mState.Update();
	}

	public void SetForceDialogForemost()
	{
		mIsForemost = true;
	}

	private string GetTitle()
	{
		string empty = string.Empty;
		switch (mStyle)
		{
		case STYLE.RETRY:
		case STYLE.RETRY_CLOSE:
		case STYLE.CLOSE:
		case STYLE.GOTO_TITLE:
		case STYLE.RETRY_COUNT:
			empty = Localization.Get("Network_Error_Title");
			break;
		case STYLE.MAINTENANCE_RETRY:
		case STYLE.MAINTENANCE_RETRY_CLOSE:
		case STYLE.MAINTENANCE_CLOSE:
		case STYLE.MAINTENANCE_TITLE:
			empty = Localization.Get("Network_Error_MainteTitle");
			break;
		default:
			empty = Localization.Get("Network_Error_Title");
			break;
		}
		if (mStyle == STYLE.GOTO_TITLE || mStyle == STYLE.RETRY_COUNT)
		{
			if (mRetryCount <= 0)
			{
				mRetryCount = 1;
			}
			empty = ((mRetryCount > 5) ? Localization.Get("RetryError_Title") : (empty + string.Format("({0}/{1})", mRetryCount, 5)));
		}
		return empty;
	}

	private string GetDesc()
	{
		string empty = string.Empty;
		switch (mStyle)
		{
		case STYLE.RETRY:
		case STYLE.RETRY_CLOSE:
		case STYLE.CLOSE:
			return Localization.Get("Network_Error_Desc");
		case STYLE.GOTO_TITLE:
			if (mRetryCount <= 5)
			{
				return Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b") + Localization.Get("Network_Error_Desc02c") + Localization.Get("Network_Error_Desc02c_02");
			}
			return Localization.Get("RetryError_Desc");
		case STYLE.RETRY_COUNT:
			if (mRetryCount <= 5)
			{
				return Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b");
			}
			return Localization.Get("RetryError_Desc_02") + Localization.Get("RetryError_Desc_02_02");
		case STYLE.MAINTENANCE_TITLE:
			if (mGame.mMaintenanceProfile != null)
			{
				return mGame.mMaintenanceProfile.MaintenanceText;
			}
			return Localization.Get("Network_Error_MainteDesc");
		case STYLE.MAINTENANCE_RETRY:
		case STYLE.MAINTENANCE_RETRY_CLOSE:
		case STYLE.MAINTENANCE_CLOSE:
			if (mGame.mMaintenanceProfile != null)
			{
				return mGame.mMaintenanceProfile.MaintenanceText;
			}
			return Localization.Get("Network_Error_MainteDesc");
		default:
			return Localization.Get("Network_Error_Desc");
		}
	}

	private void MakeButton()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		switch (mStyle)
		{
		case STYLE.RETRY:
		case STYLE.RETRY_CLOSE:
		case STYLE.MAINTENANCE_RETRY_CLOSE:
		{
			UIButton uIButton3 = Util.CreateJellyImageButton("RetryConnect", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton3, "LC_button_retry2", "LC_button_retry2", "LC_button_retry2", mBaseDepth + 1, new Vector3(0f, -100f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton3, this, "OnRetryPushed", UIButtonMessage.Trigger.OnClick);
			mButtonList.Add(uIButton3);
			break;
		}
		case STYLE.GOTO_TITLE:
		case STYLE.RETRY_COUNT:
		case STYLE.MAINTENANCE_TITLE:
			if (mRetryCount <= 5)
			{
				UIButton button = Util.CreateJellyImageButton("RetryConnect", base.gameObject, image);
				Util.SetImageButtonInfo(button, "LC_button_retry2", "LC_button_retry2", "LC_button_retry2", mBaseDepth + 1, new Vector3(0f, -130f, 0f), Vector3.one);
				Util.AddImageButtonMessage(button, this, "OnRetryPushed", UIButtonMessage.Trigger.OnClick);
				UIButton button2 = Util.CreateJellyImageButton("AbandonConnect", base.gameObject, image);
				Util.SetImageButtonInfo(button2, "LC_button_giveup", "LC_button_giveup", "LC_button_giveup", mBaseDepth + 1, new Vector3(0f, -230f, 0f), Vector3.one);
				Util.AddImageButtonMessage(button2, this, "OnAbandonPushed", UIButtonMessage.Trigger.OnClick);
			}
			else
			{
				UIButton button3 = Util.CreateJellyImageButton("AbandonConnect", base.gameObject, image);
				Util.SetImageButtonInfo(button3, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 1, new Vector3(0f, -100f, 0f), Vector3.one);
				Util.AddImageButtonMessage(button3, this, "OnAbandonPushed", UIButtonMessage.Trigger.OnClick);
			}
			break;
		case STYLE.MAINTENANCE_RETRY:
		{
			UIButton uIButton2 = Util.CreateJellyImageButton("RetryConnect", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton2, "LC_button_retry2", "LC_button_retry2", "LC_button_retry2", mBaseDepth + 1, new Vector3(0f, -100f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton2, this, "OnRetryPushed", UIButtonMessage.Trigger.OnClick);
			mButtonList.Add(uIButton2);
			break;
		}
		case STYLE.CLOSE:
		case STYLE.MAINTENANCE_CLOSE:
		{
			UIButton uIButton = Util.CreateJellyImageButton("Button", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 1, new Vector3(0f, -100f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnAbandonPushed", UIButtonMessage.Trigger.OnClick);
			mButtonList.Add(uIButton);
			break;
		}
		default:
			BIJLog.E("Not Implemented BuildDialog Button:" + mStyle);
			break;
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mPanel.sortingOrder = 510;
		UIFont atlasFont = GameMain.LoadFont();
		string title = mTitle;
		string desc = mDesc;
		bool flag = string.IsNullOrEmpty(title);
		bool flag2 = string.IsNullOrEmpty(desc);
		DIALOG_SIZE dIALOG_SIZE = DIALOG_SIZE.MEDIUM;
		bool flag3 = false;
		if (flag)
		{
			title = GetTitle();
		}
		if (flag2)
		{
			desc = GetDesc();
		}
		switch (mStyle)
		{
		case STYLE.RETRY:
		case STYLE.MAINTENANCE_RETRY:
			mBackKeySelected = SELECT_ITEM.RETRY;
			break;
		case STYLE.GOTO_TITLE:
		case STYLE.RETRY_COUNT:
		case STYLE.MAINTENANCE_TITLE:
			if (mRetryCount <= 5)
			{
				dIALOG_SIZE = DIALOG_SIZE.LARGE;
				mBackKeySelected = SELECT_ITEM.RETRY;
			}
			else
			{
				dIALOG_SIZE = DIALOG_SIZE.MEDIUM;
				mBackKeySelected = SELECT_ITEM.GIVEUP;
			}
			break;
		case STYLE.RETRY_CLOSE:
		case STYLE.MAINTENANCE_RETRY_CLOSE:
			mBackKeySelected = SELECT_ITEM.GIVEUP;
			flag3 = true;
			break;
		case STYLE.CLOSE:
		case STYLE.MAINTENANCE_CLOSE:
			mBackKeySelected = SELECT_ITEM.GIVEUP;
			break;
		}
		InitDialogFrame(dIALOG_SIZE);
		InitDialogTitle(title);
		UILabel uILabel = Util.CreateLabel("DescConnectError", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, desc, mBaseDepth + 1, new Vector3(0f, (dIALOG_SIZE != DIALOG_SIZE.LARGE) ? 100f : 170f, 0f), 26, 0, 0, UIWidget.Pivot.Top);
		DialogBase.SetDialogLabelEffect2(uILabel);
		MakeButton();
		if (flag3)
		{
			UIButton item = CreateCancelButton("OnAbandonPushed");
			mButtonList.Add(item);
		}
		for (int i = 0; i < mButtonList.Count; i++)
		{
			NGUITools.SetActive(mButtonList[i].gameObject, false);
		}
		if (mIsForemost || SingletonMonoBehaviour<GameMain>.Instance.IsLoading() || SingletonMonoBehaviour<GameMain>.Instance.IsConnecting())
		{
			mPanel.sortingOrder = 1005;
		}
	}

	public override void OnBackKeyPress()
	{
		if (mButtonDisplay)
		{
			mSelectItem = mBackKeySelected;
			OnCancelPushed(null);
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		if (mState.GetStatus() == STATE.MAIN && !GetBusy())
		{
			mState.Reset(STATE.WAIT, true);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem, mCallbackState);
		}
		Object.Destroy(base.gameObject);
	}

	private void OnRetryPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mSelectItem = SELECT_ITEM.RETRY;
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	private void OnAbandonPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mSelectItem = SELECT_ITEM.GIVEUP;
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
