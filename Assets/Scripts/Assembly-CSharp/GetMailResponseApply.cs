using System;
using System.Collections.Generic;

public class GetMailResponseApply : GetMailResponseGift, ICloneable
{
	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int SeriesStage { get; set; }

	[MiniJSONAlias("fbid")]
	public string Fbid { get; set; }

	[MiniJSONAlias("play_stage")]
	public List<PlayStageParam> PlayStage { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public new GetMailResponseApply Clone()
	{
		return MemberwiseClone() as GetMailResponseApply;
	}

	public override GiftItemData GetGiftItemData()
	{
		GiftItemData giftItemData = base.GetGiftItemData();
		giftItemData.UsePlayerInfo = true;
		giftItemData.Series = Series;
		giftItemData.SeriesStage = SeriesStage;
		giftItemData.Fbid = Fbid;
		giftItemData.PlayStage = PlayStage;
		return giftItemData;
	}
}
