using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Sprite Studio/SsSprite")]
public class SsSprite : MonoBehaviour
{
	public delegate void AnimationCallback(SsSprite sprite);

	public BetterList<SsSubAnimeController> subAnimations = new BetterList<SsSubAnimeController>();

	[HideInInspector]
	[SerializeField]
	internal SsAnimation _animation;

	private SsAnimation _prevAnimation;

	internal Transform _transform;

	internal bool _flipChanged = true;

	private bool _recalcNormals = true;

	private bool _hFlip;

	private bool _vFlip;

	private bool _isPlaying = true;

	public AnimationCallback AnimationFinished;

	private bool _isFinished;

	public float _animeFrame;

	internal float _prevFrame;

	internal int _prevStepSign;

	public int _startAnimeFrame;

	public int _endAnimeFrame;

	public SsAnimePlayDirection _playDirection;

	[HideInInspector]
	public SsAnimePlayDirection _prevPlayDirection;

	internal int _currentStepSign = 1;

	private bool _returnTrip;

	public int PlayCount;

	private int _currentPlayCount;

	public float Speed = 1f;

	public bool PlayAtStart = true;

	public bool DestroyAtEnd;

	public float _lifeTime;

	private float _lifeTimeCount;

	public bool UpdateCollider;

	public bool DrawBoundingBox;

	public bool DrawBoundingParts;

	private SsPartRes[] _partResList;

	private SsImageFile[] _imageList;

	private int _partsNum;

	private SsPart[] _parts;

	internal Bounds _bounds = new Bounds(Vector3.zero, Vector3.zero);

	private List<SsPart> _boundPartList;

	internal int _playFrameLength;

	private MeshFilter _meshFilter;

	private MeshRenderer _meshRenderer;

	private BoxCollider _boxCollider;

	private SphereCollider _sphereCollider;

	private CapsuleCollider _capsuleCollider;

	internal SsPart _rootPart;

	internal Material[] _materials;

	internal Mesh _mesh;

	internal Vector3[] _vertices;

	internal Vector2[] _uvs;

	internal Color[] _colors;

	internal Vector2[] _extras;

	internal bool _vertChanged;

	internal bool _uvChanged;

	internal bool _colorChanged;

	internal bool _prioChanged;

	internal bool _matChanged;

	internal bool _extraChanged;

	[HideInInspector]
	[SerializeField]
	private int ImportedTime;

	[HideInInspector]
	[SerializeField]
	private int ImportedTimeHigh;

	private bool update_visiblity = true;

	public int SeqNo;

	private bool _isFirstUpdate = true;

	private float update_last_delta_time;

	public bool DoUpdateBoundingBox2;

	public bool DrawBoundingBox2;

	private Vector3 BoundsLT = Vector3.zero;

	private Vector3 BoundsRB = Vector3.zero;

	public SsAnimation Animation
	{
		get
		{
			return _animation;
		}
		set
		{
			_animation = value;
			if (!(value == _prevAnimation))
			{
				Init();
				_startAnimeFrame = 0;
				if ((bool)_animation)
				{
					_endAnimeFrame = _animation.EndFrame;
				}
				else
				{
					_endAnimeFrame = 0;
				}
				_prevAnimation = _animation;
				if (PlayAtStart && Application.isPlaying)
				{
					Play();
				}
			}
		}
	}

	public Vector3 Position
	{
		get
		{
			return _transform.localPosition;
		}
		set
		{
			_transform.localPosition = value;
		}
	}

	public Vector3 Rotation
	{
		get
		{
			return _transform.localRotation.eulerAngles;
		}
		set
		{
			Quaternion identity = Quaternion.identity;
			identity.eulerAngles = value;
			_transform.localRotation = identity;
		}
	}

	public Vector3 Scale
	{
		get
		{
			return _transform.localScale;
		}
		set
		{
			_transform.localScale = value;
		}
	}

	public bool hFlip
	{
		get
		{
			return _hFlip;
		}
		set
		{
			if (_hFlip != value)
			{
				_hFlip = value;
				_flipChanged = true;
			}
		}
	}

	public bool vFlip
	{
		get
		{
			return _vFlip;
		}
		set
		{
			if (_vFlip != value)
			{
				_vFlip = value;
				_flipChanged = true;
			}
		}
	}

	public float AnimFrame
	{
		get
		{
			return _animeFrame;
		}
		set
		{
			if (!(value < 0f) && (bool)_animation && !(value > (float)_animation.EndFrame))
			{
				_animeFrame = value;
			}
		}
	}

	public float StartFrame
	{
		get
		{
			return _startAnimeFrame;
		}
		set
		{
			if (!(value < 0f) && (bool)_animation && !(value > (float)_animation.EndFrame) && !(value > (float)_endAnimeFrame))
			{
				_startAnimeFrame = (int)value;
			}
		}
	}

	public float EndFrame
	{
		get
		{
			return _endAnimeFrame;
		}
		set
		{
			if (!(value < 0f) && (bool)_animation && !(value > (float)_animation.EndFrame) && !(value < (float)_startAnimeFrame))
			{
				_endAnimeFrame = (int)value;
			}
		}
	}

	public SsAnimePlayDirection PlayDirection
	{
		get
		{
			return _playDirection;
		}
		set
		{
			_playDirection = value;
			ResetAnimationStatus();
		}
	}

	private bool IsRoundTrip
	{
		get
		{
			return PlayDirection == SsAnimePlayDirection.RoundTrip || PlayDirection == SsAnimePlayDirection.ReverseRoundTrip;
		}
	}

	public float LifeTime
	{
		get
		{
			return _lifeTime;
		}
		set
		{
			_lifeTime = value;
			_lifeTimeCount = 0f;
		}
	}

	public SsPartRes[] PartResList
	{
		get
		{
			return _partResList;
		}
	}

	public Bounds bounds
	{
		get
		{
			return _bounds;
		}
	}

	public Mesh mesh
	{
		get
		{
			return _mesh;
		}
	}

	public MeshRenderer renderer
	{
		get
		{
			return _meshRenderer;
		}
	}

	public HashSet<string> HitIgnoreParts { get; set; }

	public static GameObject CreateGameObject(string name)
	{
		GameObject gameObject = new GameObject(name);
		gameObject.AddComponent<SsSprite>();
		return gameObject;
	}

	public static GameObject CreateGameObjectWithAnime(string name, string animName)
	{
		GameObject gameObject = new GameObject(name);
		SsSprite ssSprite = gameObject.AddComponent<SsSprite>();
		SsAnimation animation = (SsAnimation)Resources.Load(animName, typeof(SsAnimation));
		ssSprite.Animation = animation;
		return gameObject;
	}

	public void ReplaceAnimation(SsAnimation anm)
	{
		if (!_animation)
		{
			Animation = anm;
			return;
		}
		if (anm.PartList.Length != _animation.PartList.Length)
		{
			UnityEngine.Debug.LogError("Can't replace animation because of the difference of the parts count.\n" + anm.name + ": " + anm.PartList.Length + " " + _animation.name + ": " + _animation.PartList.Length);
			return;
		}
		_animation = anm;
		if (anm == _prevAnimation)
		{
			return;
		}
		if (!_animation)
		{
			Init();
			return;
		}
		ResetAnimationStatus();
		Pause();
		_partResList = _animation.PartList;
		_imageList = _animation.ImageList;
		_partsNum = _partResList.Length;
		for (int i = 0; i < _parts.Length; i++)
		{
			_parts[i]._res = _partResList[i];
		}
		_vertChanged = true;
		_uvChanged = true;
		_extraChanged = true;
		UpdateAlways();
		_startAnimeFrame = 0;
		if ((bool)_animation)
		{
			_endAnimeFrame = _animation.EndFrame;
		}
		else
		{
			_endAnimeFrame = 0;
		}
		_prevAnimation = _animation;
		if (PlayAtStart && Application.isPlaying)
		{
			Play();
		}
	}

	public SsPart GetPart(int index)
	{
		if (_parts == null)
		{
			return null;
		}
		if (index >= _parts.Length)
		{
			return null;
		}
		return _parts[index];
	}

	public SsPart GetPart(string name)
	{
		if (name == null)
		{
			return _parts[0];
		}
		SsPart[] parts = _parts;
		foreach (SsPart ssPart in parts)
		{
			if (ssPart._res.Name == name)
			{
				return ssPart;
			}
		}
		return null;
	}

	public SsPart[] GetParts()
	{
		return _parts;
	}

	public Transform TransformAt(string name)
	{
		if (name == null)
		{
			return _transform;
		}
		SsPart part = GetPart(name);
		if (part == null)
		{
			return null;
		}
		return part._transform;
	}

	public Vector3 PositionAt(string name)
	{
		Transform transform = TransformAt(name);
		if (!transform)
		{
			UnityEngine.Debug.LogError("Can't find child part: " + name);
			return Vector3.zero;
		}
		return transform.position;
	}

	public void Play()
	{
		_isPlaying = true;
		_isFinished = false;
	}

	public void Pause()
	{
		_isPlaying = false;
	}

	public bool IsPlaying()
	{
		return _isPlaying;
	}

	public bool IsAnimationFinished()
	{
		return _isFinished;
	}

	public void SetStartEndFrame(int start, int end)
	{
		_startAnimeFrame = ((start >= 0) ? ((start <= _animation.EndFrame) ? start : _animation.EndFrame) : 0);
		_endAnimeFrame = ((end >= 0) ? ((end <= _animation.EndFrame) ? end : _animation.EndFrame) : 0);
	}

	public bool IsLastFrame()
	{
		return _animeFrame >= (float)_endAnimeFrame;
	}

	public void SetPlayDirection(SsAnimePlayDirection dir, bool keepFrame)
	{
		_playDirection = dir;
		float animeFrame = _animeFrame;
		ResetAnimationStatus();
		if (keepFrame)
		{
			_animeFrame = animeFrame;
		}
	}

	public bool IntersectsByBounds(SsSprite other, bool ignoreZ)
	{
		if (!_mesh || !other._mesh)
		{
			return false;
		}
		if (ignoreZ)
		{
			Bounds bounds = other._bounds;
			Vector3 center = bounds.center;
			center.z = _bounds.center.z;
			bounds.center = center;
			return _bounds.Intersects(bounds);
		}
		return _bounds.Intersects(other._bounds);
	}

	public bool IntersectsByBoundingParts(SsSprite other, bool ignoreZ, bool useAABB)
	{
		foreach (SsPart boundPart in _boundPartList)
		{
			foreach (SsPart boundPart2 in other._boundPartList)
			{
				if (useAABB)
				{
					if (boundPart.IntersectsByAABB(boundPart2, ignoreZ))
					{
						return true;
					}
				}
				else if (boundPart.Intersects(boundPart2, ignoreZ))
				{
					return true;
				}
			}
		}
		return false;
	}

	public bool ContainsPoint(Vector3 point, bool ignoreZ)
	{
		if (!_mesh)
		{
			return false;
		}
		if (ignoreZ)
		{
			Vector3 point2 = point;
			point2.z = _bounds.center.z;
			return _bounds.Contains(point2);
		}
		return _bounds.Contains(point);
	}

	private void Awake()
	{
		_transform = base.transform;
		EnsureMeshComponent();
		_boxCollider = GetComponent<BoxCollider>();
		_sphereCollider = GetComponent<SphereCollider>();
		_capsuleCollider = GetComponent<CapsuleCollider>();
		Init();
	}

	private void EnsureMeshComponent()
	{
		if (!_meshFilter)
		{
			_meshFilter = base.gameObject.GetComponent<MeshFilter>();
			if (_meshFilter == null)
			{
				_meshFilter = base.gameObject.AddComponent<MeshFilter>();
			}
		}
		if (!_meshRenderer)
		{
			_meshRenderer = base.gameObject.GetComponent<MeshRenderer>();
			if (_meshRenderer == null)
			{
				_meshRenderer = base.gameObject.AddComponent<MeshRenderer>();
			}
		}
		_meshRenderer.castShadows = false;
		_meshRenderer.receiveShadows = false;
	}

	private void Start()
	{
		_isFirstUpdate = true;
		if (PlayAtStart && Application.isPlaying)
		{
			Play();
		}
	}

	private void Init()
	{
		UnityEngine.Object.DestroyImmediate(_mesh);
		if (!_animation)
		{
			_partResList = null;
			_imageList = null;
			_partsNum = 0;
			_parts = null;
			_boundPartList = null;
			_materials = null;
			_vertices = null;
			_uvs = null;
			_colors = null;
			_extras = null;
			_mesh = null;
			_meshFilter.mesh = null;
			_meshRenderer.sharedMaterials = new Material[1];
			_isPlaying = (_isFinished = false);
			PlayCount = 0;
			Speed = 1f;
			_prevPlayDirection = (_playDirection = SsAnimePlayDirection.Forward);
			ResetAnimationStatus();
			_startAnimeFrame = (_endAnimeFrame = (_playFrameLength = 0));
			return;
		}
		EnsureMeshComponent();
		ResetAnimationStatus();
		Pause();
		_isFinished = false;
		_partResList = _animation.PartList;
		_imageList = _animation.ImageList;
		_partsNum = _partResList.Length;
		_materials = new Material[_partsNum - 1];
		int num = (_partsNum - 1) * 4;
		_vertices = new Vector3[num];
		_uvs = new Vector2[num];
		_colors = new Color[num];
		_extras = new Vector2[num];
		_mesh = new Mesh();
		_mesh.MarkDynamic();
		_mesh.vertices = _vertices;
		_mesh.subMeshCount = _partsNum - 1;
		_mesh.uv2 = _extras;
		_meshFilter.mesh = _mesh;
		_parts = new SsPart[_partsNum];
		for (int i = 0; i < _parts.Length; i++)
		{
			SsPartRes ssPartRes = _partResList[i];
			SsImageFile imageFile = ((!ssPartRes.IsRoot) ? _imageList[ssPartRes.SrcObjId] : null);
			_parts[i] = new SsPart(this, i, ssPartRes, imageFile);
			if (i == 0)
			{
				_rootPart = _parts[i];
			}
			else
			{
				_materials[i - 1] = _parts[i]._material;
			}
			if (ssPartRes.Type == SsPartType.Bound)
			{
				if (_boundPartList == null)
				{
					_boundPartList = new List<SsPart>();
				}
				_boundPartList.Add(_parts[i]);
			}
		}
		_meshRenderer.sharedMaterials = _materials;
		_mesh.vertices = _vertices;
		_mesh.uv = _uvs;
		_vertChanged = true;
		_extraChanged = true;
		UpdateAlways();
	}

	private void OnEnable()
	{
	}

	private void EnsureLatestAnime()
	{
		if ((bool)_animation && !_animation.EqualsImportedTime(ImportedTime, ImportedTimeHigh))
		{
			EnsureMeshComponent();
			Init();
			ImportedTime = _animation.ImportedTime;
			ImportedTimeHigh = _animation.ImportedTimeHigh;
		}
	}

	private void OnDestroy()
	{
		DeleteTransformChildren();
	}

	private void DeleteTransformChildren()
	{
		if (_mesh != null)
		{
			if (_meshFilter != null)
			{
				_meshFilter.mesh = null;
			}
			UnityEngine.Object.DestroyImmediate(_mesh);
			_mesh = null;
		}
		if ((bool)_transform)
		{
			foreach (Transform item in _transform)
			{
				UnityEngine.Object.DestroyImmediate(item.gameObject);
			}
		}
		if (_parts != null)
		{
			for (int i = 0; i < _parts.Length; i++)
			{
				_parts[i].DeleteResources();
			}
		}
	}

	public void ResetAnimationStatus()
	{
		if (PlayDirection == SsAnimePlayDirection.Reverse || PlayDirection == SsAnimePlayDirection.ReverseRoundTrip)
		{
			_currentStepSign = -1;
			_animeFrame = _endAnimeFrame;
		}
		else
		{
			_currentStepSign = 1;
			_animeFrame = _startAnimeFrame;
		}
		_playFrameLength = _endAnimeFrame + 1 - _startAnimeFrame;
		_currentPlayCount = 0;
		_returnTrip = false;
		_prevFrame = -1f;
		_prevStepSign = _currentStepSign;
	}

	public void UpdateVertex()
	{
		_vertChanged = true;
	}

	private void Update()
	{
		UpdateAlways();
	}

	public void DoBecameVisible()
	{
		update_visiblity = true;
		_meshRenderer.enabled = true;
	}

	public void DoBecameInvisible()
	{
		update_visiblity = false;
		_meshRenderer.enabled = false;
	}

	public void UpdateAlways()
	{
		if (_animation == null || _parts == null || _parts[_partsNum - 1] == null)
		{
			return;
		}
		float deltaTime = Time.deltaTime;
		_isFirstUpdate = false;
		if (_lifeTime > 0f)
		{
			_lifeTimeCount += deltaTime;
			if (_lifeTimeCount >= _lifeTime)
			{
				UnityEngine.Object.Destroy(base.gameObject);
				return;
			}
		}
		if (subAnimations != null)
		{
			int i = 0;
			for (int size = subAnimations.size; i < size; i++)
			{
				SsSubAnimeController ssSubAnimeController = subAnimations[i];
				if (ssSubAnimeController.Animation == null)
				{
					continue;
				}
				if (ssSubAnimeController.BindsToAllParts)
				{
					for (int j = 1; j < _parts.Length; j++)
					{
						for (int k = 0; k < ssSubAnimeController.Animation.PartList.Length; k++)
						{
							_parts[j].AddSubAnime(ssSubAnimeController, ssSubAnimeController.Animation.PartList[k]);
						}
					}
					continue;
				}
				for (int l = 0; l < ssSubAnimeController.Animation.PartList.Length; l++)
				{
					SsPart ssPart = null;
					ssPart = ((!ssSubAnimeController.BindsByPartName) ? GetPart(l) : GetPart(ssSubAnimeController.Animation.PartList[l].Name));
					if (ssPart != null)
					{
						ssPart.AddSubAnime(ssSubAnimeController, ssSubAnimeController.Animation.PartList[l]);
					}
				}
			}
		}
		if (update_visiblity)
		{
			int m = 0;
			for (int num = _parts.Length; m < num; m++)
			{
				SsPart ssPart2 = _parts[m];
				ssPart2.Update();
			}
			if (_prioChanged)
			{
				SortByPriority();
				_prioChanged = false;
			}
			if (_vertChanged)
			{
				_mesh.vertices = _vertices;
				updateBoundingBox();
				_vertChanged = false;
			}
			if (_uvChanged)
			{
				_mesh.uv = _uvs;
				_uvChanged = false;
			}
			if (_colorChanged)
			{
				_mesh.colors = _colors;
				_colorChanged = false;
			}
			if (_extraChanged)
			{
				_mesh.uv2 = _extras;
				_extraChanged = false;
			}
			if (_matChanged)
			{
				_meshRenderer.sharedMaterials = _materials;
				_matChanged = false;
			}
		}
		else
		{
			updateBoundingBox();
		}
		if (!_isPlaying)
		{
			return;
		}
		float num2 = deltaTime * (float)_animation.FPS * Speed * (float)_currentStepSign;
		_prevFrame = _animeFrame;
		_animeFrame += num2;
		if (_animeFrame < (float)_startAnimeFrame || (int)_animeFrame > _endAnimeFrame)
		{
			bool flag = true;
			if (IsRoundTrip)
			{
				_prevStepSign = _currentStepSign;
				_currentStepSign *= -1;
				if (!_returnTrip)
				{
					_returnTrip = true;
				}
				else
				{
					_returnTrip = false;
				}
			}
			else
			{
				flag = false;
			}
			if (!_returnTrip && PlayCount != 0 && ++_currentPlayCount >= PlayCount)
			{
				flag = true;
				_isPlaying = false;
				_isFinished = true;
				if (AnimationFinished != null)
				{
					AnimationFinished(this);
				}
				if (DestroyAtEnd)
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
			}
			float num3 = 0f;
			if (_animeFrame < (float)_startAnimeFrame)
			{
				num3 = (_animeFrame - (float)_startAnimeFrame) % (float)_playFrameLength;
				num3 *= -1f;
			}
			else if ((int)_animeFrame > _endAnimeFrame)
			{
				num3 = (_animeFrame - 1f - (float)_endAnimeFrame) % (float)_playFrameLength;
			}
			if (num3 < 0f)
			{
				num3 = 0f;
			}
			if (flag)
			{
				if (IsRoundTrip)
				{
					if (_animeFrame < (float)_startAnimeFrame)
					{
						_animeFrame = (float)_startAnimeFrame + num3;
					}
					else if (_animeFrame > (float)_endAnimeFrame)
					{
						_animeFrame = (float)_endAnimeFrame - num3;
					}
				}
				else if (_animeFrame < (float)_startAnimeFrame)
				{
					_animeFrame = _startAnimeFrame;
				}
				else if (_animeFrame > (float)_endAnimeFrame)
				{
					_animeFrame = _endAnimeFrame;
				}
			}
			else if (_animeFrame < (float)_startAnimeFrame)
			{
				_animeFrame = (float)(_endAnimeFrame + 1) - num3;
			}
			else if (_animeFrame > (float)_endAnimeFrame)
			{
				_animeFrame = (float)_startAnimeFrame + num3;
			}
		}
		int n = 0;
		for (int size2 = subAnimations.size; n < size2; n++)
		{
			SsSubAnimeController ssSubAnimeController2 = subAnimations[n];
			ssSubAnimeController2.StepFrame(Time.deltaTime);
		}
	}

	internal int _StepFrameFromPrev(int frame)
	{
		frame += ((!(Speed > 0f)) ? (-_prevStepSign) : _prevStepSign);
		if (frame > _endAnimeFrame)
		{
			if (IsRoundTrip)
			{
				_prevStepSign *= -1;
				frame = _endAnimeFrame - 1;
				if (frame < 0)
				{
					frame = _endAnimeFrame;
				}
			}
			else
			{
				frame = _startAnimeFrame;
			}
		}
		else if (frame < _startAnimeFrame)
		{
			if (IsRoundTrip)
			{
				_prevStepSign *= -1;
				frame = _startAnimeFrame + 1;
			}
			else
			{
				frame = _endAnimeFrame;
			}
		}
		return frame;
	}

	public void ForcePartsUpdate()
	{
		SsPart[] parts = _parts;
		foreach (SsPart ssPart in parts)
		{
			ssPart.Update();
		}
		if (_prioChanged)
		{
			SortByPriority();
			_prioChanged = false;
		}
		if (_vertChanged)
		{
			_mesh.vertices = _vertices;
			updateBoundingBox();
			_vertChanged = false;
		}
		if (_uvChanged)
		{
			_mesh.uv = _uvs;
			_uvChanged = false;
		}
		if (_colorChanged)
		{
			_mesh.colors = _colors;
			_colorChanged = false;
		}
		if (_extraChanged)
		{
			_mesh.uv2 = _extras;
			_extraChanged = false;
		}
		if (_matChanged)
		{
			_meshRenderer.sharedMaterials = _materials;
			_matChanged = false;
		}
	}

	public void DebugPartsName()
	{
		int num = 0;
		SsPart[] parts = _parts;
		foreach (SsPart ssPart in parts)
		{
			num++;
		}
	}

	internal void updateBoundingBox()
	{
		_mesh.RecalculateBounds();
		if (_recalcNormals)
		{
			_mesh.RecalculateNormals();
			_recalcNormals = false;
		}
		_bounds = renderer.bounds;
		if (UpdateCollider)
		{
			if ((bool)_boxCollider)
			{
				UnityEngine.Object.DestroyImmediate(_boxCollider);
				_boxCollider = base.gameObject.AddComponent<BoxCollider>();
				Vector3 size = _boxCollider.size;
				size.z = 1f;
				_boxCollider.size = size;
			}
			else if ((bool)_sphereCollider)
			{
				UnityEngine.Object.DestroyImmediate(_sphereCollider);
				_sphereCollider = base.gameObject.AddComponent<SphereCollider>();
			}
			else if ((bool)_capsuleCollider)
			{
				UnityEngine.Object.DestroyImmediate(_capsuleCollider);
				_capsuleCollider = base.gameObject.AddComponent<CapsuleCollider>();
			}
		}
		if (DoUpdateBoundingBox2 && _mesh != null)
		{
			updateBoundingBox2();
		}
	}

	private void SortByPriority()
	{
		SsPart[] array = new SsPart[_parts.Length];
		for (int i = 0; i < _parts.Length; i++)
		{
			array[i] = _parts[i];
		}
		Array.Sort(array);
		int num = 0;
		for (int j = 0; j < _parts.Length; j++)
		{
			if (!array[j]._res.IsRoot)
			{
				array[j].SetToSubmeshArray(num);
				_materials[num] = array[j]._material;
				num++;
			}
		}
		_matChanged = true;
	}

	internal SsPart Sprite(int index)
	{
		if (index < 0 || index >= _partsNum)
		{
			return null;
		}
		return _parts[index];
	}

	internal void updateBoundingBox2()
	{
		BoundsLT = Vector3.zero;
		BoundsRB = Vector3.zero;
		bool flag = true;
		Vector3 position = new Vector3(float.MaxValue, float.MaxValue);
		Vector3 position2 = new Vector3(float.MinValue, float.MinValue);
		SsPart[] parts = _parts;
		foreach (SsPart ssPart in parts)
		{
			if (!(ssPart._mgr != this) && !ssPart._res.IsRoot && (HitIgnoreParts == null || !HitIgnoreParts.Contains(ssPart._res.Name)))
			{
				flag = false;
				Vector3 lt;
				Vector3 rb;
				ssPart.MakeMinMaxPosition(out lt, out rb);
				if (lt.x < position.x)
				{
					position.x = lt.x;
				}
				if (rb.x > position2.x)
				{
					position2.x = rb.x;
				}
				if (lt.y < position.y)
				{
					position.y = lt.y;
				}
				if (rb.y > position2.y)
				{
					position2.y = rb.y;
				}
			}
		}
		if (!flag)
		{
			BoundsLT = base.gameObject.transform.TransformPoint(position);
			BoundsRB = base.gameObject.transform.TransformPoint(position2);
		}
	}

	public bool ContainsPoint2(Vector3 worldPoint)
	{
		return ContainsPoint(worldPoint, true);
	}

	public bool ContainsPoint2(Vector3 worldPoint, bool ignoreZ)
	{
		if (!_mesh)
		{
			return false;
		}
		Vector3 boundsLT = BoundsLT;
		Vector3 boundsRB = BoundsRB;
		bool flag = false;
		if (ignoreZ)
		{
			return worldPoint.x >= boundsLT.x && worldPoint.x <= boundsRB.x && worldPoint.y >= boundsLT.y && worldPoint.y <= boundsRB.y;
		}
		return worldPoint.x >= boundsLT.x && worldPoint.x <= boundsRB.x && worldPoint.y >= boundsLT.y && worldPoint.y <= boundsRB.y && worldPoint.z >= boundsLT.z && worldPoint.z <= boundsRB.z;
	}
}
