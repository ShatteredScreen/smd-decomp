using System;
using System.Collections.Generic;
using UnityEngine;

public class MyIconSetDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3,
		NETWORK_LOGOUT = 4
	}

	public delegate void OnDialogClosed(bool mHereLogin, bool mcheckIconflg);

	private const int FACEBOOK_BUTTON_HEIGHT = 84;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private BIJSNS mSNS;

	private OnDialogClosed mCallback;

	public bool mHereLogin;

	private ConfirmDialog mConfirmDialog;

	private UILabel mFacebook;

	private UISsSprite mCursorAnime;

	private SMVerticalListInfo mIconListInfo;

	private SMVerticalList mIconList;

	private GameObject mListCenter;

	private int mcheckIconId;

	private bool mcheckIconflg;

	public bool Modified { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							Modified = false;
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendIconTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetMyIconTime = DateTimeUtil.UnitLocalTimeEpoch;
						mHereLogin = true;
						if (mGame.mPlayer.Data.PlayerIcon < 10000)
						{
							mGame.mPlayer.Data.PlayerIcon += 10000;
						}
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns))
				{
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}, null);
			break;
		}
		case STATE.MAIN:
			UpdateButtonText();
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("SetPickIconTitle");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		int num = 0;
		if (!GameMain.IsFacebookIconEnable())
		{
			num = 84;
		}
		mIconListInfo = new SMVerticalListInfo(4, 490f, 370 + num, 4, 490f, 370 + num, 1, 130);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		string text = "button00";
		UILabel uILabel;
		if (GameMain.IsFacebookIconEnable())
		{
			UIButton uIButton = Util.CreateJellyImageButton("FacebookButton", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 2, new Vector3(0f, 202f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnFaceBookIconPushed", UIButtonMessage.Trigger.OnClick);
			UISprite component = uIButton.GetComponent<UISprite>();
			if (component != null)
			{
				component.SetDimensions(525, 84);
				component.type = UIBasicSprite.Type.Sliced;
			}
			BoxCollider component2 = uIButton.GetComponent<BoxCollider>();
			if (component2 != null)
			{
				component2.center = new Vector3(0f, 0f, 0f);
				component2.size = new Vector3(525f, 84f, 1f);
			}
			text = "icon_sns00";
			UISprite sprite = Util.CreateSprite("FacebookIcon", uIButton.gameObject, image);
			Util.SetSpriteInfo(sprite, text, mBaseDepth + 3, new Vector3(-208f, 0f, 0f), new Vector3(0.6f, 0.6f, 1f), false);
			uILabel = Util.CreateLabel("message", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("SelectedIconFB"), mBaseDepth + 3, new Vector3(-180f, -5f, 0f), 24, 0, 0, UIWidget.Pivot.Left);
			DialogBase.SetDialogLabelEffect(uILabel);
			uILabel.fontSize = 24;
			uILabel.color = Color.white;
			text = "button01";
			sprite = Util.CreateSprite("FacebookBar", uIButton.gameObject, image);
			Util.SetSpriteInfo(sprite, text, mBaseDepth + 3, new Vector3(200f, 0f, 0f), Vector3.one, false);
			sprite.SetDimensions(86, 45);
			uILabel = Util.CreateLabel("Status", sprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 4, new Vector3(0f, -2f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.alignment = NGUIText.Alignment.Center;
			mFacebook = uILabel;
		}
		uILabel = Util.CreateLabel("DescBot", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("SetPickIcon02"), mBaseDepth + 2, new Vector3(0f, -254f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mcheckIconId = mGame.mPlayer.Data.PlayerIcon;
		CreateCancelButton("OnCancelPushed");
	}

	private void UpdateButtonText()
	{
		string text = Localization.Get("SelectedIconFB_ON");
		string text2 = Localization.Get("SelectedIconFB_OFF");
		if (mFacebook != null)
		{
			if (mGame.mPlayer.Data.PlayerIcon >= 10000)
			{
				Util.SetLabelText(mFacebook, text);
				mFacebook.color = Color.red;
			}
			else
			{
				Util.SetLabelText(mFacebook, text2);
				mFacebook.color = Def.DEFAULT_MESSAGE_COLOR;
			}
		}
	}

	private void ListOpen()
	{
		List<SMVerticalListItem> items = PartnerListItem.CreatePartnerList(true);
		float num = 0f;
		int num2 = 0;
		if (!GameMain.IsFacebookIconEnable())
		{
			num = 42f;
			num2 = 84;
		}
		mListCenter = Util.CreateGameObject("GridRoot", base.gameObject);
		mListCenter.transform.localPosition = new Vector3(-8f, -36f + num, 0f);
		mIconList = SMVerticalList.Make(mListCenter, this, mIconListInfo, items, 0f, 0f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mIconList.OnCreateItem = OnCreateIconListItem;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UISprite uISprite = Util.CreateSprite("ListBg", mListCenter.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(8f, 0f, 0f), Vector3.one, false);
		uISprite.SetDimensions(530, 390 + num2);
		uISprite.type = UIBasicSprite.Type.Sliced;
	}

	private void OnCreateIconListItem(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIButton uIButton = null;
		UISprite uISprite = null;
		uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)mIconList.mList.cellWidth;
		uISprite.height = (int)mIconList.mList.cellHeight;
		PartnerListItem partnerListItem = (PartnerListItem)item;
		if (partnerListItem.data == null)
		{
			if (partnerListItem.category != byte.MaxValue)
			{
				PartnerListItem.CreateCategoryRibbon(itemGO, mBaseDepth + 2, partnerListItem.category);
			}
			return;
		}
		string text = "icon_chara_question";
		string imageName = "null";
		string funcName = "OnCharacterPushed";
		int companionLevel = mGame.mPlayer.GetCompanionLevel(partnerListItem.data.index);
		if (partnerListItem.enable)
		{
			if (index == 0)
			{
				text = "icon_chara_unknown";
				funcName = "OnLastUsedIconPushed";
			}
			else
			{
				text = partnerListItem.data.iconName;
			}
			imageName = ((partnerListItem.data.maxLevel <= companionLevel) ? Def.CharacterIconLevelMaxImageTbl[companionLevel] : Def.CharacterIconLevelImageTbl[companionLevel]);
		}
		uIButton = Util.CreateJellyImageButton("Icon_" + partnerListItem.data.index, itemGO, image2);
		Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 2, Vector3.one, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, funcName, UIButtonMessage.Trigger.OnClick);
		uIButton.GetComponent<JellyImageButton>().SetBaseScale(0.9f);
		uIButton.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mIconList.mListScrollView;
		bool flag = false;
		if (index == 0)
		{
			uISprite = Util.CreateSprite("LastIcon", uIButton.gameObject, image2);
			Util.SetSpriteInfo(uISprite, "LC_icon_text", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
			if (!mGame.mPlayer.Data.LastUsedIconCancelFlg)
			{
				flag = true;
			}
		}
		else
		{
			uISprite = Util.CreateSprite("Lvl_" + partnerListItem.data.index, uIButton.gameObject, image);
			Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 3, new Vector3(0f, -55f, 0f), Vector3.one, false);
			if (mGame.mPlayer.Data.LastUsedIconCancelFlg)
			{
				int num = mGame.mPlayer.Data.PlayerIcon;
				if (num >= 10000)
				{
					num -= 10000;
				}
				if (partnerListItem.data.index == num)
				{
					flag = true;
				}
			}
		}
		if (flag)
		{
			mCursorAnime = Util.CreateGameObject("IconCursor", uIButton.gameObject).AddComponent<UISsSprite>();
			mCursorAnime.Animation = ResourceManager.LoadSsAnimation("ICON_CURSOR").SsAnime;
			mCursorAnime.depth = mBaseDepth + 2;
			mCursorAnime.Play();
		}
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnFaceBookIconPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSNS = SocialManager.Instance[SNS.Facebook];
			if (IsFacebookLogined() && mGame.mPlayer.Data.PlayerIcon >= 10000)
			{
				mGame.mPlayer.Data.PlayerIcon -= 10000;
			}
			else if (IsFacebookLogined() && mGame.mPlayer.Data.PlayerIcon < 10000)
			{
				mGame.mPlayer.Data.PlayerIcon += 10000;
			}
			else if (!IsFacebookLogined() && mGame.mPlayer.Data.PlayerIcon >= 10000)
			{
				mGame.mPlayer.Data.PlayerIcon -= 10000;
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
			mGame.PlaySe("SE_POSITIVE", -1);
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = base.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public void OnLastUsedIconPushed(GameObject a_go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mGame.mPlayer.Data.LastUsedIconCancelFlg = false;
			if (mGame.mPlayer.Data.PlayerIcon >= 10000)
			{
				mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.LastUsedIcon + 10000;
			}
			else
			{
				mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.LastUsedIcon;
			}
			if (mCursorAnime != null)
			{
				mCursorAnime.transform.parent = a_go.transform;
				mCursorAnime.transform.localPosition = Vector3.zero;
				mCursorAnime.transform.localScale = Vector3.one;
			}
		}
	}

	public void OnCharacterPushed(GameObject a_go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		if (a_go != null)
		{
			string text = a_go.name;
			if (text.Contains("_"))
			{
				string[] array = text.Split('_');
				if (array.Length > 1)
				{
					int num = int.Parse(array[1]);
					if (mGame.mPlayer.Data.PlayerIcon >= 10000)
					{
						mGame.mPlayer.Data.PlayerIcon = num + 10000;
					}
					else
					{
						mGame.mPlayer.Data.PlayerIcon = num;
					}
					mGame.mPlayer.Data.LastUsedIconCancelFlg = true;
				}
			}
		}
		if (mCursorAnime != null)
		{
			mCursorAnime.transform.parent = a_go.transform;
			mCursorAnime.transform.localPosition = Vector3.zero;
			mCursorAnime.transform.localScale = Vector3.one;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
	}

	public override void OnOpenFinished()
	{
		ListOpen();
	}

	public override void OnCloseFinished()
	{
		if (mcheckIconId < 10000 && mGame.mPlayer.Data.PlayerIcon >= 10000)
		{
			mcheckIconflg = true;
		}
		if (mCallback != null)
		{
			mCallback(mHereLogin, mcheckIconflg);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
