using System.Collections.Generic;

public class TrophyMissionGuageUpListItem : SMVerticalListItem
{
	public List<ShowGuageInfo> data;

	public int id;

	public string name;

	public int maxMission;
}
