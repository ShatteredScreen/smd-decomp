using System;

public class EventAppealSettings : ICloneable
{
	[MiniJSONAlias("is_adv")]
	public short IsAdvEvent { get; set; }

	[MiniJSONAlias("event_id")]
	public short EventID { get; set; }

	[MiniJSONAlias("partner_id")]
	public short PartnerID { get; set; }

	[MiniJSONAlias("is_not_show")]
	public short IsNotShow { get; set; }

	[MiniJSONAlias("is_bg_show")]
	public short IsShowBG { get; set; }

	public EventAppealSettings()
	{
		IsShowBG = 1;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public EventAppealSettings Clone()
	{
		return MemberwiseClone() as EventAppealSettings;
	}
}
