using System;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEngine;

public class MyFriend : ICloneable, IComparable
{
	private const string ICON_PATH_FORMAT = "{0}.dat";

	public MyFriendData Data;

	public DateTime UpdateTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(Data.UpdateTimeEpoch, true);
		}
	}

	public static string IconBasePath
	{
		get
		{
			return Path.Combine(BIJUnity.getTempPath(), "icons");
		}
	}

	public string IconFilePath
	{
		get
		{
			BIJMD5 bIJMD = new BIJMD5(Data.UUID);
			string arg = bIJMD.ToString();
			return Path.Combine(IconBasePath, string.Format("{0}.dat", arg));
		}
	}

	public string Gender { get; set; }

	public Texture2D Icon { get; set; }

	public MyFriend()
	{
		Data = new MyFriendData();
		Data.Version = 1290;
		Data.LastHeartSendTime = DateTimeUtil.UnitLocalTimeEpoch;
		Data.LastRoadBlockRequestTime = DateTimeUtil.UnitLocalTimeEpoch;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as MyFriend);
	}

	public MyFriend Clone()
	{
		return MemberwiseClone() as MyFriend;
	}

	public int CompareTo(MyFriend _obj)
	{
		if (_obj == null)
		{
			return int.MaxValue;
		}
		return Data.UpdateTimeEpoch - _obj.Data.UpdateTimeEpoch;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[MyFriend: {0}]", stringBuilder.ToString());
	}

	public void CopyTo(MyFriend _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(MyFriend);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}

	public void SetData(MyFriendData a_ref)
	{
		Data = null;
		Data = a_ref.Clone();
	}

	public void SaveIcon()
	{
		if (!(Icon != null))
		{
			return;
		}
		try
		{
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(IconFilePath, Encoding.UTF8, true))
			{
				bIJEncryptDataWriter.WriteAll(Icon.EncodeToPNG());
			}
		}
		catch (Exception)
		{
		}
	}

	public void LoadIcon()
	{
		Texture2D texture2D = null;
		try
		{
			texture2D = new Texture2D(1, 1, TextureFormat.ARGB4444, false);
			bool flag = false;
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(IconFilePath))
			{
				flag = texture2D.LoadImage(bIJEncryptDataReader.ReadAll());
			}
			if (flag)
			{
				Icon = texture2D;
				return;
			}
			Icon = null;
			Data.IconModified = DateTimeUtil.UnitLocalTimeEpoch;
			if (texture2D != null)
			{
				texture2D = null;
			}
		}
		catch (Exception)
		{
			Icon = null;
			Data.IconModified = DateTimeUtil.UnitLocalTimeEpoch;
			if (texture2D != null)
			{
				texture2D = null;
			}
		}
	}

	public void RemoveIcon()
	{
		try
		{
			if (File.Exists(IconFilePath))
			{
				File.Delete(IconFilePath);
			}
		}
		catch (Exception)
		{
		}
		if (Icon != null)
		{
			GameMain.SafeDestroy(Icon);
		}
		Icon = null;
	}

	public FileInfo GetIconFileInfo()
	{
		FileInfo result = null;
		if (File.Exists(IconFilePath))
		{
			result = new FileInfo(IconFilePath);
		}
		return result;
	}
}
