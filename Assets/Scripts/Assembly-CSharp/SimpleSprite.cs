using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SimpleSprite : MonoBehaviour
{
	public const float PIXELS_PER_UNIT = 1f;

	public string mSpriteName = string.Empty;

	private Color mColor = new Color(1f, 1f, 1f, 1f);

	private UIWidget.Pivot mPivot = UIWidget.Pivot.Center;

	private Vector2 mPivotVector;

	private short[] mVertices;

	public BIJImage mAtlas;

	public SpriteRenderer mSpriteRenderer;

	private int mSortingOrder;

	private bool mModified;

	public byte work;

	private Transform _transform;

	public UIWidget.Pivot Pivot
	{
		get
		{
			return mPivot;
		}
		set
		{
			mPivot = value;
			mPivotVector = NGUIMath.GetPivotOffset(mPivot);
			Modified = true;
		}
	}

	public Vector3 _pos
	{
		get
		{
			return _transform.localPosition;
		}
		set
		{
			_transform.localPosition = value;
		}
	}

	public float _xpos
	{
		get
		{
			return _pos.x;
		}
		set
		{
			Vector3 pos = _pos;
			pos.x = value;
			_pos = pos;
		}
	}

	public float _ypos
	{
		get
		{
			return _pos.y;
		}
		set
		{
			Vector3 pos = _pos;
			pos.y = value;
			_pos = pos;
		}
	}

	public float _zpos
	{
		get
		{
			return _pos.z;
		}
		set
		{
			Vector3 pos = _pos;
			pos.z = value;
			_pos = pos;
		}
	}

	public Vector3 _rot
	{
		get
		{
			return _transform.localRotation.eulerAngles;
		}
		set
		{
			Quaternion identity = Quaternion.identity;
			identity.eulerAngles = value;
			_transform.localRotation = identity;
		}
	}

	public float _xrot
	{
		get
		{
			return _rot.x;
		}
		set
		{
			Vector3 rot = _rot;
			rot.x = value;
			_rot = rot;
		}
	}

	public float _yrot
	{
		get
		{
			return _rot.y;
		}
		set
		{
			Vector3 rot = _rot;
			rot.y = value;
			_rot = rot;
		}
	}

	public float _zrot
	{
		get
		{
			return _rot.z;
		}
		set
		{
			Vector3 rot = _rot;
			rot.z = value;
			_rot = rot;
		}
	}

	public Vector3 _scl
	{
		get
		{
			return _transform.localScale;
		}
		set
		{
			_transform.localScale = value;
		}
	}

	public float _xscl
	{
		get
		{
			return _scl.x;
		}
		set
		{
			Vector3 scl = _scl;
			scl.x = value;
			_scl = scl;
		}
	}

	public float _yscl
	{
		get
		{
			return _scl.y;
		}
		set
		{
			Vector3 scl = _scl;
			scl.y = value;
			_scl = scl;
		}
	}

	public float _zscl
	{
		get
		{
			return _scl.z;
		}
		set
		{
			Vector3 scl = _scl;
			scl.z = value;
			_scl = scl;
		}
	}

	public bool Modified
	{
		get
		{
			return mModified;
		}
		set
		{
			mModified = value;
		}
	}

	public string SpriteName
	{
		get
		{
			return mSpriteName;
		}
		set
		{
			mSpriteName = value;
			Modified = true;
		}
	}

	public Color Color
	{
		get
		{
			return mColor;
		}
		set
		{
			mColor = value;
			mSpriteRenderer.color = mColor;
		}
	}

	protected virtual void Awake()
	{
		_transform = base.transform;
		mSpriteRenderer = GetComponent<SpriteRenderer>();
		mSpriteRenderer.castShadows = false;
		mSpriteRenderer.receiveShadows = false;
		mSpriteRenderer.sortingOrder = mSortingOrder;
	}

	public virtual void Init()
	{
		mPivotVector = NGUIMath.GetPivotOffset(mPivot);
		UpdateSprite();
	}

	protected virtual void Start()
	{
		Color = mColor;
	}

	protected virtual void Update()
	{
		if (Modified)
		{
			UpdateSprite();
			Modified = false;
		}
	}

	protected virtual void OnDestroy()
	{
	}

	private void DebugOut()
	{
		if (!UnityEngine.Debug.isDebugBuild)
		{
		}
	}

	public void SetSprite(string aSpriteName, BIJImage aAtlas, int aSortingOrder, Vector3 aPosision, Vector3 aScale)
	{
		mAtlas = aAtlas;
		SpriteName = aSpriteName;
		mSortingOrder = aSortingOrder;
		_pos = aPosision;
		_scl = aScale;
	}

	private void UpdateSprite()
	{
		mPivotVector = NGUIMath.GetPivotOffset(mPivot);
		Vector2 pixelSize = mAtlas.GetPixelSize(mSpriteName);
		Vector2 pixelOffset = mAtlas.GetPixelOffset(mSpriteName);
		Sprite sprite = Sprite.Create((Texture2D)mAtlas.Texture, new Rect(pixelOffset.x, (float)mAtlas.Texture.height - (pixelOffset.y + pixelSize.y), pixelSize.x, pixelSize.y), mPivotVector, 1f);
		mSpriteRenderer.sprite = sprite;
		mSpriteRenderer.sortingOrder = mSortingOrder;
	}

	public void UpdateSortOrder(int sortOrder)
	{
		mSortingOrder = sortOrder;
		Modified = true;
	}
}
