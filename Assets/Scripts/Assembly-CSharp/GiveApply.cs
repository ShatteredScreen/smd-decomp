using System;

public class GiveApply : GiveGift, ICloneable
{
	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int SeriesStage { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public new GiveApply Clone()
	{
		return MemberwiseClone() as GiveApply;
	}
}
