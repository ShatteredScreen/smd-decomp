using UnityEngine;

public class LazerInstanceBase
{
	public enum STYLE
	{
		LAZER = 0,
		SQUARE = 1
	}

	public delegate void OnFinishedDelegate(LazerInstanceBase self);

	public STYLE mStyle;

	public int mMaxLength;

	public float mWidth;

	public int mLength;

	public float mRatio;

	public Vector2[] mTrail;

	public Vector2 mForePoint;

	public int mTrailPtr;

	public bool mFinished;

	public string mSpriteName;

	public OnFinishedDelegate OnFinished = delegate
	{
	};

	public LazerInstanceBase(int length, float width, string spriteName, STYLE style)
	{
		mMaxLength = length;
		mLength = 0;
		mWidth = width;
		mTrail = new Vector2[mMaxLength];
		mRatio = 0f;
		mTrailPtr = 0;
		mFinished = false;
		mSpriteName = spriteName;
		mStyle = style;
	}

	public virtual void Update(float deltaTime)
	{
	}

	public int GetPartsNum()
	{
		if (mStyle == STYLE.SQUARE)
		{
			return 1;
		}
		if (mLength < 3)
		{
			return 0;
		}
		return mLength - 1;
	}

	public Vector2 GetForePoint()
	{
		return mForePoint;
	}

	protected void Finish()
	{
		OnFinished(this);
		mFinished = true;
	}
}
