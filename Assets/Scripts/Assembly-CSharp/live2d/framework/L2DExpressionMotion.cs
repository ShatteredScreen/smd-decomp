using System.Collections.Generic;
using System.Text;

namespace live2d.framework
{
	public class L2DExpressionMotion : AMotion
	{
		public class L2DExpressionParam
		{
			public string id;

			public int type;

			public float value;
		}

		private const string EXPRESSION_DEFAULT = "DEFAULT";

		public const int TYPE_SET = 0;

		public const int TYPE_ADD = 1;

		public const int TYPE_MULT = 2;

		private List<L2DExpressionParam> paramList;

		public L2DExpressionMotion()
		{
			paramList = new List<L2DExpressionParam>();
		}

		public override void updateParamExe(ALive2DModel model, long timeMSec, float weight, MotionQueueEnt motionQueueEnt)
		{
			for (int num = paramList.Count - 1; num >= 0; num--)
			{
				L2DExpressionParam l2DExpressionParam = paramList[num];
				if (l2DExpressionParam.type == 1)
				{
					model.addToParamFloat(l2DExpressionParam.id, l2DExpressionParam.value, weight);
				}
				else if (l2DExpressionParam.type == 2)
				{
					model.multParamFloat(l2DExpressionParam.id, l2DExpressionParam.value, weight);
				}
				else if (l2DExpressionParam.type == 0)
				{
					model.setParamFloat(l2DExpressionParam.id, l2DExpressionParam.value, weight);
				}
			}
		}

		public static L2DExpressionMotion loadJson(byte[] buf)
		{
			return loadJson(Encoding.GetEncoding("UTF-8").GetString(buf));
		}

		public static L2DExpressionMotion loadJson(string buf)
		{
			return loadJson(buf.ToCharArray());
		}

		public static L2DExpressionMotion loadJson(char[] buf)
		{
			L2DExpressionMotion l2DExpressionMotion = new L2DExpressionMotion();
			Value value = Json.parseFromBytes(buf);
			l2DExpressionMotion.setFadeIn(value.get("fade_in").toInt(1000));
			l2DExpressionMotion.setFadeOut(value.get("fade_out").toInt(1000));
			if (!value.getMap(null).ContainsKey("params"))
			{
				return l2DExpressionMotion;
			}
			Value value2 = value.get("params");
			int count = value2.getVector(null).Count;
			l2DExpressionMotion.paramList = new List<L2DExpressionParam>(count);
			for (int i = 0; i < count; i++)
			{
				Value value3 = value2.get(i);
				string id = value3.get("id").toString();
				float num = value3.get("val").toFloat();
				int num2 = 1;
				string text = ((!value3.getMap(null).ContainsKey("calc")) ? "add" : value3.get("calc").toString());
				num2 = (text.Equals("add") ? 1 : (text.Equals("mult") ? 2 : ((!text.Equals("set")) ? 1 : 0)));
				switch (num2)
				{
				case 1:
				{
					float num4 = (value3.getMap(null).ContainsKey("def") ? value3.get("def").toFloat() : 0f);
					num -= num4;
					break;
				}
				case 2:
				{
					float num3 = (value3.getMap(null).ContainsKey("def") ? value3.get("def").toFloat(0f) : 1f);
					if (num3 == 0f)
					{
						num3 = 1f;
					}
					num /= num3;
					break;
				}
				}
				L2DExpressionParam l2DExpressionParam = new L2DExpressionParam();
				l2DExpressionParam.id = id;
				l2DExpressionParam.type = num2;
				l2DExpressionParam.value = num;
				l2DExpressionMotion.paramList.Add(l2DExpressionParam);
			}
			return l2DExpressionMotion;
		}

		public static Dictionary<string, AMotion> loadExpressionJsonV09(byte[] bytes)
		{
			Dictionary<string, AMotion> dictionary = new Dictionary<string, AMotion>();
			char[] jsonBytes = Encoding.GetEncoding("UTF-8").GetString(bytes).ToCharArray();
			Value value = Json.parseFromBytes(jsonBytes);
			Value defaultExpr = value.get("DEFAULT");
			List<string> list = value.keySet();
			foreach (string item in list)
			{
				if (!"DEFAULT".Equals(item))
				{
					Value expr = value.get(item);
					L2DExpressionMotion value2 = loadJsonV09(defaultExpr, expr);
					dictionary.Add(item, value2);
				}
			}
			return dictionary;
		}

		private static L2DExpressionMotion loadJsonV09(Value defaultExpr, Value expr)
		{
			L2DExpressionMotion l2DExpressionMotion = new L2DExpressionMotion();
			l2DExpressionMotion.setFadeIn(expr.get("FADE_IN").toInt(1000));
			l2DExpressionMotion.setFadeOut(expr.get("FADE_OUT").toInt(1000));
			Value value = defaultExpr.get("PARAMS");
			Value value2 = expr.get("PARAMS");
			List<string> list = value2.keySet();
			List<string> list2 = new List<string>();
			foreach (string item in list)
			{
				list2.Add(item);
			}
			for (int num = list2.Count - 1; num >= 0; num--)
			{
				string text = list2[num];
				float num2 = value.get(text).toFloat(0f);
				float num3 = value2.get(text).toFloat(0f);
				float value3 = num3 - num2;
				L2DExpressionParam l2DExpressionParam = new L2DExpressionParam();
				l2DExpressionParam.id = text;
				l2DExpressionParam.type = 1;
				l2DExpressionParam.value = value3;
				l2DExpressionMotion.paramList.Add(l2DExpressionParam);
			}
			return l2DExpressionMotion;
		}
	}
}
