using System.Collections.Generic;

public class SMSpecialDailyEventData
{
	public int ID = -1;

	public int ImageID;

	public List<int> BackImageRGBA = new List<int>();

	public List<SMSpecialDailyRewardData> RewardList = new List<SMSpecialDailyRewardData>();

	public virtual void Serialize(BIJFileBinaryWriter sw)
	{
		sw.WriteInt(1290);
		sw.WriteInt(ID);
		sw.WriteInt(ImageID);
		sw.WriteInt(BackImageRGBA.Count);
		for (int i = 0; i < BackImageRGBA.Count; i++)
		{
			sw.WriteInt(BackImageRGBA[i]);
		}
		sw.WriteInt(RewardList.Count);
		for (int j = 0; j < RewardList.Count; j++)
		{
			RewardList[j].Serialize(sw);
		}
	}

	public virtual void Deserialize(BIJBinaryReader a_reader)
	{
		int num = a_reader.ReadInt();
		ID = a_reader.ReadInt();
		ImageID = a_reader.ReadInt();
		int num2 = a_reader.ReadInt();
		for (int i = 0; i < num2; i++)
		{
			BackImageRGBA.Add(a_reader.ReadInt());
		}
		num2 = a_reader.ReadInt();
		for (int j = 0; j < num2; j++)
		{
			SMSpecialDailyRewardData sMSpecialDailyRewardData = new SMSpecialDailyRewardData();
			sMSpecialDailyRewardData.Deserialize(a_reader);
			RewardList.Add(sMSpecialDailyRewardData);
		}
	}

	public string GetString()
	{
		string empty = string.Empty;
		string text = empty;
		empty = text + "ID:" + ID + "\n";
		text = empty;
		empty = text + "ImageID:" + ImageID + "\n";
		empty += "BackRGBA\n";
		for (int i = 0; i < BackImageRGBA.Count; i++)
		{
			empty = empty + "," + BackImageRGBA[i];
		}
		empty += "Reward\n";
		foreach (SMSpecialDailyRewardData reward in RewardList)
		{
			empty += reward.GetString();
		}
		return empty;
	}
}
