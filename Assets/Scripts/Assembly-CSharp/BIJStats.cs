using System;
using System.Text;
using UnityEngine;

public class BIJStats : MonoBehaviour
{
	private float lastCollect;

	private float lastCollectNum;

	private float collectDelta;

	private float lastDeltaTime;

	private int allocRate;

	private int lastAllocMemory;

	private float lastAllocSet = -9999f;

	private int allocMem;

	private int collectAlloc;

	private int peakAlloc;

	private float lastTime;

	private int updateCount;

	private float fps;

	public static string debug0 = string.Empty;

	public void Update()
	{
		updateCount++;
		int num = GC.CollectionCount(0);
		if (lastCollectNum != (float)num)
		{
			lastCollectNum = num;
			collectDelta = Time.realtimeSinceStartup - lastCollect;
			lastCollect = Time.realtimeSinceStartup;
			lastDeltaTime = Time.deltaTime;
			collectAlloc = allocMem;
		}
		allocMem = (int)GC.GetTotalMemory(false);
		peakAlloc = ((allocMem <= peakAlloc) ? peakAlloc : allocMem);
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		if (realtimeSinceStartup - lastAllocSet > 0.3f)
		{
			int num2 = allocMem - lastAllocMemory;
			lastAllocMemory = allocMem;
			lastAllocSet = Time.realtimeSinceStartup;
			if (num2 >= 0)
			{
				allocRate = num2;
			}
		}
		if (realtimeSinceStartup - lastTime > 1f)
		{
			fps = (float)updateCount / (realtimeSinceStartup - lastTime);
			updateCount = 0;
			lastTime = realtimeSinceStartup;
		}
	}

	public void OnGUI()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("Currently allocated\t\t");
		stringBuilder.Append(((float)allocMem / 1000000f).ToString("0"));
		stringBuilder.Append("mb");
		stringBuilder.Append("\n");
		stringBuilder.Append("Peak allocated\t\t\t");
		stringBuilder.Append(((float)peakAlloc / 1000000f).ToString("0"));
		stringBuilder.Append("mb (last collect ");
		stringBuilder.Append(((float)collectAlloc / 1000000f).ToString("0"));
		stringBuilder.Append(" mb)");
		stringBuilder.Append("\n");
		stringBuilder.Append("Allocation rate\t\t\t");
		stringBuilder.Append(((float)allocRate / 1000000f).ToString("0.0"));
		stringBuilder.Append("mb");
		stringBuilder.Append("\n");
		stringBuilder.Append("Collection freq.\t\t\t");
		stringBuilder.Append(collectDelta.ToString("0.00"));
		stringBuilder.Append("s");
		stringBuilder.Append("\n");
		stringBuilder.Append("Last collect delta\t\t");
		stringBuilder.Append(lastDeltaTime.ToString("0.000"));
		stringBuilder.Append("s (");
		stringBuilder.Append((1f / lastDeltaTime).ToString("0.0"));
		stringBuilder.Append(" fps)");
		stringBuilder.Append("\n");
		stringBuilder.Append("FPS\t\t\t\t\t\t");
		stringBuilder.Append(fps.ToString("0.0") + " fps");
		stringBuilder.Append("\n");
		stringBuilder.Append("Screen Size\t\t\t\t");
		stringBuilder.Append(string.Empty + Screen.width + " x " + Screen.height + " (" + Screen.dpi.ToString("0.0") + ")");
		stringBuilder.Append("\n");
		stringBuilder.Append("Zoom Camera\t\t\t\t");
		BIJZoomCamera bIJZoomCamera = UnityEngine.Object.FindObjectOfType(typeof(BIJZoomCamera)) as BIJZoomCamera;
		if (bIJZoomCamera != null)
		{
			stringBuilder.Append(string.Empty + bIJZoomCamera.GetComponent<Camera>().pixelWidth + " x " + bIJZoomCamera.GetComponent<Camera>().pixelHeight + " (" + bIJZoomCamera.GetComponent<Camera>().orthographicSize.ToString("0.0") + ")");
		}
		stringBuilder.Append("\n");
		stringBuilder.Append("TotalMemory : ");
		stringBuilder.Append((float)GC.GetTotalMemory(false) / 1000000f);
		stringBuilder.Append("MB\n");
		Rect position = new Rect(5f, 5f, Screen.width / 4 - 10, Screen.height / 4 - 10);
		GUIStyle gUIStyle = new GUIStyle(GUI.skin.GetStyle("Box"));
		gUIStyle.fontSize = 10;
		gUIStyle.alignment = TextAnchor.MiddleLeft;
		GUI.Box(position, stringBuilder.ToString(), gUIStyle);
	}
}
