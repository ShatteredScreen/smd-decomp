using System.Collections.Generic;

namespace live2d.framework
{
	public class L2DPartsParam
	{
		public const int TYPE_VISIBLE = 0;

		public const bool optimize = false;

		public string id;

		public int paramIndex = -1;

		public int partsIndex = -1;

		public int type;

		public List<L2DPartsParam> link;

		public L2DPartsParam(string id)
		{
			this.id = id;
		}

		public void initIndex(ALive2DModel model)
		{
			if (type == 0)
			{
				paramIndex = model.getParamIndex("VISIBLE:" + id);
			}
			partsIndex = model.getPartsDataIndex(PartsDataID.getID(id));
			model.setParamFloat(paramIndex, 1f);
		}
	}
}
