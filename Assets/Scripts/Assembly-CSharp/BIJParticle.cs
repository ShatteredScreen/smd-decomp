using UnityEngine;

public class BIJParticle : MonoBehaviour
{
	public ParticleSystem mParticle;

	public float mInitialSize;

	public float mInitialSpeed;

	protected ScreenOrientation mOldOrientation;

	public bool mUseClearAtScreenChanged;

	public bool mIsInitialized;

	public virtual void Awake()
	{
	}

	public virtual void Start()
	{
	}

	public virtual void Update()
	{
		if (mOldOrientation != Util.ScreenOrientation)
		{
			mOldOrientation = Util.ScreenOrientation;
			OnScreenChanged(mOldOrientation);
		}
	}

	public virtual void OnScreenChanged(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mParticle != null)
			{
				mParticle.startSize = mInitialSize;
				mParticle.startSpeed = mInitialSpeed;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			Vector2 vector = Util.LogScreenSize();
			float num = vector.x / vector.y;
			if (mParticle != null)
			{
				mParticle.startSize = mInitialSize * num;
				mParticle.startSpeed = mInitialSpeed * num;
			}
			break;
		}
		}
		if (mParticle != null && mUseClearAtScreenChanged)
		{
			mParticle.Clear();
			mParticle.Play();
		}
	}

	protected void OnLoadFinished()
	{
		if (mParticle != null)
		{
			mInitialSize = mParticle.startSize;
			mInitialSpeed = mParticle.startSpeed;
		}
		mOldOrientation = Util.ScreenOrientation;
		OnScreenChanged(mOldOrientation);
	}

	public void SetParticleSystem(ParticleSystem a_particle)
	{
		mParticle = a_particle;
		if (mParticle != null && !mIsInitialized)
		{
			mIsInitialized = true;
			mInitialSize = mParticle.startSize;
			mInitialSpeed = mParticle.startSpeed;
		}
		mOldOrientation = Util.ScreenOrientation;
		OnScreenChanged(mOldOrientation);
	}

	public virtual bool LoadFromPrefab(string name)
	{
		return false;
	}

	public void OnDestroy()
	{
		ParticleSystem[] componentsInChildren = GetComponentsInChildren<ParticleSystem>(true);
		if (componentsInChildren == null)
		{
			return;
		}
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			if (componentsInChildren[i] == null)
			{
				continue;
			}
			Renderer[] components = componentsInChildren[i].GetComponents<Renderer>();
			if (components != null)
			{
				for (int j = 0; j < components.Length; j++)
				{
					Renderer renderer = components[j];
					if (renderer != null && renderer.materials != null)
					{
						for (int k = 0; k < renderer.sharedMaterials.Length; k++)
						{
							if (!(renderer.sharedMaterials[k] == null))
							{
								renderer.sharedMaterials[k].mainTexture = null;
								Object.DestroyImmediate(renderer.sharedMaterials[k]);
								renderer.sharedMaterials[k] = null;
							}
						}
					}
					components[j] = null;
				}
			}
			componentsInChildren[i] = null;
		}
	}
}
