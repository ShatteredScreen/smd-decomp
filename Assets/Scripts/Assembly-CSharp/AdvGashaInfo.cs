using System.Collections.Generic;

public class AdvGashaInfo
{
	[MiniJSONAlias("id")]
	public int id { get; set; }

	[MiniJSONAlias("category")]
	public string category { get; set; }

	[MiniJSONAlias("manifest")]
	public AdvGashaInfoManifest manifest { get; set; }

	[MiniJSONAlias("starttime")]
	public long starttime { get; set; }

	[MiniJSONAlias("endtime")]
	public long endtime { get; set; }

	[MiniJSONAlias("gashas")]
	public List<AdvGashaInfoDetail> GashaInfoDetailList { get; set; }
}
