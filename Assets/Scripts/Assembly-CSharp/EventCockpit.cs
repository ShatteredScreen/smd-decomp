using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public abstract class EventCockpit : Cockpit
{
	protected sealed class Live2dInfo : IDisposable
	{
		private bool mDisposed;

		private Partner mPartner;

		private GameObject mRoot;

		private UISprite mShadow;

		private string[] mModelKey;

		private float localPositionY;

		public bool IsDone { get; private set; }

		public float TextureLocalPositionY
		{
			get
			{
				return localPositionY;
			}
			set
			{
				localPositionY = value;
				if (mPartner != null)
				{
					mPartner.SetPartnerScreenOffset(new Vector3(0f, value, 0f));
				}
			}
		}

		public Vector3 ShadowScale
		{
			get
			{
				return (!mShadow) ? Vector3.zero : mShadow.transform.localScale;
			}
			set
			{
				if ((bool)mShadow)
				{
					mShadow.SetLocalScale(value);
				}
			}
		}

		public float ShadowAlpha
		{
			get
			{
				return (!mShadow) ? 0f : mShadow.color.a;
			}
			set
			{
				if ((bool)mShadow)
				{
					mShadow.color = new Color(mShadow.color.r, mShadow.color.g, mShadow.color.b, value);
				}
			}
		}

		public float DefaultTextureLocalPositionY { get; private set; }

		public Live2dInfo()
		{
			IsDone = false;
		}

		~Live2dInfo()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				if (mPartner != null)
				{
					UnityEngine.Object.Destroy(mPartner.gameObject);
				}
				if (mRoot != null)
				{
					UnityEngine.Object.Destroy(mRoot.gameObject);
				}
				mRoot = null;
				mPartner = null;
				mDisposed = true;
			}
		}

		public IEnumerator LoadAsync(CompanionData data, CompanionData.MOTION motion, IntVector2 texture_size, IntVector2 ui_dimension, GameObject texture_root, GameObject ui_root, Vector3 ui_local_position, int ui_depth, float scale = 0.4f)
		{
			if (!IsDone)
			{
				mModelKey = new string[data.ModelCount];
				for (int i = 0; i < data.ModelCount; i++)
				{
					mModelKey[i] = data.GetModelKey(i);
				}
				mRoot = Util.CreateGameObject("CharaRoot", texture_root);
				mRoot.transform.SetLocalPosition(136f, 369f);
				mPartner = Util.CreateGameObject("Partner", mRoot).AddComponent<Partner>();
				mPartner.transform.localPosition = Vector3.zero;
				int level = 5;
				mPartner.Init(data.index, Vector3.zero, 1f, level, ui_root, true, ui_depth + 1);
				while (!mPartner.IsPartnerLoadFinished())
				{
					yield return 0;
				}
				DefaultTextureLocalPositionY = ui_local_position.y;
				mPartner.SetPartnerScreenScale(scale);
				mPartner.SetPartnerScreenPos(new Vector3(0f, -86f, 0f));
				TextureLocalPositionY = DefaultTextureLocalPositionY;
				BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
				mPartner.MakeUIShadow(atlas, "character_foot2", ui_depth);
				mPartner.StartMotion(motion, true);
				mPartner.SetPartnerScreenEnable(true);
				IsDone = true;
			}
		}

		public void SetMotion(CompanionData.MOTION motion, bool can_loop)
		{
			if (IsDone && !(mPartner == null))
			{
				mPartner.StartMotion(motion, can_loop);
			}
		}

		public bool IsCompareModel(params string[] keys)
		{
			if (keys == null && mModelKey == null)
			{
				return true;
			}
			if (keys == null || mModelKey == null)
			{
				return false;
			}
			if (keys.Length != mModelKey.Length)
			{
				return false;
			}
			foreach (string value in keys)
			{
				if (!mModelKey.Contains(value))
				{
					return false;
				}
			}
			return true;
		}
	}

	private const float ANIMATION_CURVE_TANGENT = 1.5708f;

	protected Transform mOldEventRemainFrame;

	private UILabel mOldEventRemainLabel;

	private string mOldEventRemainText = string.Empty;

	protected UIButton mBackToMap;

	protected UIButton mHowToPlay;

	protected UIButton mGiftButton;

	private Live2dInfo mLive2dInfo;

	public bool CanOldEventPlutoEffect { private get; set; }

	public bool IsOldEventPlutoEffecting { get; private set; }

	public bool IsOldEventFinished { get; private set; }

	public bool CanOldEventCountUpRemain { private get; set; }

	protected override void OnDestroy()
	{
		base.OnDestroy();
		if (mLive2dInfo != null)
		{
			mLive2dInfo.Dispose();
		}
		mLive2dInfo = null;
	}

	protected override void CreateCockpitStatus(int baseDepth)
	{
		PlayerOldEventData data;
		mGame.mPlayer.Data.GetOldEventData((short)mGame.mPlayer.Data.CurrentEventID, out data);
		if (mGame.mOldEventMode)
		{
			UISprite uISprite = Util.CreateSprite("Door", mAnchorTR.gameObject, HudAtlas);
			Util.SetSpriteInfo(uISprite, "oldevent_panel_timelimit", baseDepth + 2, new Vector3(-140f, -230f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(250, 36);
			mOldEventRemainFrame = uISprite.transform;
			mOldEventRemainText = Localization.Get("OldEventCount_Open");
			mOldEventRemainLabel = Util.CreateLabel("DoorRemain", uISprite.gameObject, GameMain.LoadFont());
			Util.SetLabelInfo(mOldEventRemainLabel, string.Format(mOldEventRemainText, 24, 0, 0), baseDepth + 3, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(mOldEventRemainLabel, new Color32(byte.MaxValue, 240, 113, byte.MaxValue));
			TimeSpan result = TimeSpan.Zero;
			if (mGame.mOldEventExtend)
			{
				data.CloseTime = data.CloseTime.Subtract(new TimeSpan(0, Def.OLD_EVENT_UNLOCKMINUTES + Def.OLD_EVENT_CONTINUE_ADDITIONALMINUTES, 0));
				UpdateOldEventRemain(data.CloseTime, ref result);
				StartCoroutine(Routine_OldEventExtensionAnimation(data, null));
			}
			else if (mGame.mOldEventOpen)
			{
				data.CloseTime = DateTimeUtil.Now().Add(new TimeSpan(0, 0, Def.OLD_EVENT_UNLOCKMINUTES, 0, 999));
				UpdateOldEventRemain(data.CloseTime, ref result);
				StartCoroutine(Routine_OldEventRemain(data, delegate
				{
					data.CloseTime = DateTimeUtil.Now().Add(new TimeSpan(0, 0, Def.OLD_EVENT_UNLOCKMINUTES, 0, 999));
					mGame.mPlayer.Data.SetOldEventData(data.EventID, data);
					mGame.Save();
					mGame.mOldEventOpen = false;
					return data;
				}));
			}
			else
			{
				DateTime closeTime = data.CloseTime;
				TimeSpan remain = data.CloseTime - DateTimeUtil.Now();
				UpdateOldEventRemain(closeTime, ref result);
				StartCoroutine(Routine_OldEventRemain(data, delegate
				{
					data.CloseTime = DateTimeUtil.Now() + remain;
					mGame.mPlayer.Data.SetOldEventData(data.EventID, data);
					mGame.Save();
					return data;
				}));
			}
		}
		CreateCockpitTop(baseDepth);
	}

	protected override void CreateCockpitItems(int baseDepth)
	{
		GameObject gameObject = mAnchorB.gameObject;
		GameObject parent = mAnchorBL.gameObject;
		GameObject parent2 = mAnchorBR.gameObject;
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		Vector2 vector = Util.LogScreenSize();
		string text = null;
		text = "button_exit";
		mBackToMap = Util.CreateJellyImageButton("Back", parent, HudAtlas);
		Util.SetImageButtonInfo(mBackToMap, text, text, text, baseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mBackToMap, mGSM, "OnEventExit", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mBackToMap);
		mBackToMap.SetLocalPosition(55f, 55f);
		text = "button_mailbox";
		mGiftButton = Util.CreateJellyImageButton("Mail", parent, HudAtlas);
		Util.SetImageButtonInfo(mGiftButton, text, text, text, baseDepth + 2, Vector2.zero, Vector2.one);
		Util.AddImageButtonMessage(mGiftButton, mGSM, "OnGiftPushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mGiftButton);
		mGiftButton.SetLocalPosition(163f, 55f);
		mMailAttention = Util.CreateGameObject("MailAttention", mGiftButton.gameObject).AddComponent<Attention>();
		mMailAttention.Init(1f, baseDepth + 3, new Vector3(30f, -25f), HudAtlas, "icon_exclamation");
		mMailAttention.SetAttentionEnable(false);
		text = "LC_button_howto";
		mHowToPlay = Util.CreateJellyImageButton("Howto", parent2, image);
		Util.SetImageButtonInfo(mHowToPlay, text, text, text, baseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mHowToPlay, mGSM, "OnEventHelp", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mHowToPlay);
		mHowToPlay.SetLocalPosition(-55f, 55f);
	}

	public virtual void SetLayout(ScreenOrientation o)
	{
	}

	private bool UpdateOldEventRemain(DateTime end_time, ref TimeSpan result)
	{
		if (!mOldEventRemainLabel)
		{
			return false;
		}
		result = end_time - DateTimeUtil.Now();
		if (result.TotalSeconds <= 0.0)
		{
			Util.SetLabelText(mOldEventRemainLabel, string.Format(mOldEventRemainText, 0, 0, 0));
			return false;
		}
		Util.SetLabelText(mOldEventRemainLabel, string.Format(mOldEventRemainText, result.Days * 24 + result.Hours, result.Minutes, result.Seconds));
		return true;
	}

	public bool TryOpenOldEventExtensionDialog(Action on_stay, Action on_move)
	{
		if (!IsOldEventFinished)
		{
			return false;
		}
		PlayerOldEventData data;
		mGame.mPlayer.Data.GetOldEventData((short)mGame.mPlayer.Data.CurrentEventID, out data);
		if (data.ExtendedOfferDone)
		{
			return false;
		}
		if (!data.HasOpened)
		{
			return false;
		}
		mState.Reset(STATE.WAIT);
		StartCoroutine(mGSM.GrayOut());
		data.EventID = (short)mGame.mPlayer.Data.CurrentEventID;
		OldEventContinueDialog dialog = Util.CreateGameObject("OldEventContinueDialog", mAnchorC).AddComponent<OldEventContinueDialog>();
		dialog.Init(data.EventID, OldEventContinueDialog.PLACE.EVENT_MAP);
		dialog.SetClosedCallback(delegate(OldEventContinueDialog.SELECT_ITEM i)
		{
			StartCoroutine(mGSM.GrayIn());
			switch (i)
			{
			case OldEventContinueDialog.SELECT_ITEM.POSITIVE:
				mState.Change(STATE.MAIN);
				CanOldEventPlutoEffect = true;
				data.CloseTime = data.CloseTime.Subtract(new TimeSpan(0, Def.OLD_EVENT_UNLOCKMINUTES + Def.OLD_EVENT_CONTINUE_ADDITIONALMINUTES, 0));
				StartCoroutine(Routine_OldEventExtensionAnimation(data, on_stay));
				break;
			case OldEventContinueDialog.SELECT_ITEM.NEGATIVE:
				if (on_move != null)
				{
					on_move();
				}
				break;
			case OldEventContinueDialog.SELECT_ITEM.ERROR:
				if (on_stay != null)
				{
					on_stay();
				}
				break;
			}
			UnityEngine.Object.Destroy(dialog.gameObject);
			dialog = null;
		});
		return true;
	}

	private IEnumerator Routine_OldEventRemain(PlayerOldEventData old_ev_data, Func<PlayerOldEventData> on_begine_countup = null)
	{
		bool isBegine = false;
		TimeSpan remain = TimeSpan.Zero;
		while (true)
		{
			if (CanOldEventCountUpRemain)
			{
				if (!isBegine && on_begine_countup != null)
				{
					old_ev_data = on_begine_countup();
				}
				isBegine = true;
				if (!UpdateOldEventRemain(old_ev_data.CloseTime, ref remain))
				{
					break;
				}
			}
			yield return null;
		}
		IsOldEventFinished = true;
	}

	private IEnumerator Routine_OldEventExtensionAnimation(PlayerOldEventData old_ev_data, Action on_finished_callback)
	{
		int TOTAL_OLD_EVENT_UNLOCK_MINUTES = Def.OLD_EVENT_UNLOCKMINUTES + Def.OLD_EVENT_CONTINUE_ADDITIONALMINUTES;
		TimeSpan remain = old_ev_data.CloseTime - DateTimeUtil.Now();
		if (remain.Ticks < 0)
		{
			remain = TimeSpan.Zero;
		}
		while (mState.GetStatus() != STATE.MAIN)
		{
			yield return null;
		}
		mState.Reset(STATE.WAIT);
		IsOldEventPlutoEffecting = true;
		while (!CanOldEventPlutoEffect)
		{
			yield return null;
		}
		StartCoroutine(mGSM.GrayOut());
		yield return new WaitForSeconds(0.5f);
		bool isFinishedEffect = false;
		OldEventExtendEffect effect = Util.CreateGameObject("Pluto", mAnchorC).AddComponent<OldEventExtendEffect>();
		effect.SetFinishedCallback(delegate
		{
			isFinishedEffect = true;
		});
		while (!isFinishedEffect)
		{
			yield return null;
		}
		StartCoroutine(mGSM.GrayIn());
		yield return new WaitForSeconds(0.5f);
		AnimationCurve animTime = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1.5f, TOTAL_OLD_EVENT_UNLOCK_MINUTES * 60));
		AnimationCurve animScale = new AnimationCurve();
		for (int i = 0; i < 2; i++)
		{
			animScale.AddKey((float)i * 0.25f, 1f + (float)(i % 2) * 0.125f);
		}
		animScale.preWrapMode = WrapMode.PingPong;
		animScale.postWrapMode = WrapMode.PingPong;
		mGame.PlaySe("SE_OLD_EVENT_EXTENSION_01", -1);
		float time = 0f;
		while (time <= 1.5f)
		{
			TimeSpan span = TimeSpan.Zero.Add(new TimeSpan(0, 0, (int)animTime.Evaluate(time))) + remain;
			if ((bool)mOldEventRemainFrame)
			{
				mOldEventRemainFrame.SetLocalScale(animScale.Evaluate(time));
			}
			if ((bool)mOldEventRemainLabel)
			{
				Util.SetLabelText(mOldEventRemainLabel, string.Format(mOldEventRemainText, span.Days * 24 + span.Hours, span.Minutes, span.Seconds));
			}
			yield return null;
			float tmp = time;
			time += Time.deltaTime;
			if (tmp == 1.5f)
			{
				remain = span;
			}
			else if (tmp < 1.5f && time > 1.5f)
			{
				time = 1.5f;
			}
		}
		yield return new WaitForSeconds(0.5f);
		ConfirmDialog dialog = Util.CreateGameObject("ExtensionConfirmDialog", mAnchorC).AddComponent<ConfirmDialog>();
		dialog.Init(Localization.Get("OldEventExtensionOffer_B_Title"), string.Format(Localization.Get("OldEventExtensionOffer_B_Desc"), TOTAL_OLD_EVENT_UNLOCK_MINUTES / 60), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		PlayerOldEventData old_ev_data2 = default(PlayerOldEventData);
		Action on_finished_callback2 = default(Action);
		dialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, delegate
		{
			old_ev_data2.CloseTime = DateTimeUtil.Now() + remain;
			mGame.mPlayer.Data.SetOldEventData(old_ev_data2.EventID, old_ev_data2);
			mGame.mOldEventExtend = false;
			mGame.Save();
			mState.Change(STATE.MAIN);
			CanOldEventCountUpRemain = true;
			StartCoroutine(Routine_OldEventRemain(old_ev_data2));
			IsOldEventPlutoEffecting = false;
			IsOldEventFinished = false;
			if (on_finished_callback2 != null)
			{
				on_finished_callback2();
			}
		});
	}

	protected void CreateLive2d(CompanionData data, CompanionData.MOTION motion, IntVector2 texture_size, IntVector2 ui_dimension, GameObject texture_root, GameObject ui_root, Vector3 ui_local_position, int ui_depth, float scale = 0.4f)
	{
		if (mLive2dInfo != null)
		{
			mLive2dInfo.Dispose();
		}
		mLive2dInfo = null;
		mLive2dInfo = new Live2dInfo();
		StartCoroutine(mLive2dInfo.LoadAsync(data, motion, texture_size, ui_dimension, texture_root, ui_root, ui_local_position, ui_depth, scale));
	}

	public bool CanCreateLive2dCharacter(short partner_id)
	{
		if (partner_id < 0 || partner_id >= mGame.mCompanionData.Count)
		{
			return false;
		}
		CompanionData companionData = mGame.mCompanionData[partner_id];
		if (companionData == null)
		{
			return false;
		}
		string[] array = new string[companionData.ModelCount];
		int modelCount = companionData.ModelCount;
		for (int i = 0; i < modelCount; i++)
		{
			array[i] = companionData.GetModelKey(i);
		}
		return mLive2dInfo != null && mLive2dInfo.IsCompareModel(array);
	}

	public virtual IEnumerator JumpUpLive2dCharacter(CompanionData.MOTION motion)
	{
		yield return StartCoroutine(JumpLive2dCharacter_Sub(motion, 700f, 0f, 1.75f, true, true, CompanionData.MOTION.MAXNUM));
	}

	public virtual IEnumerator JumpDownLive2dCharacter(CompanionData.MOTION motion, CompanionData.MOTION after_loop_motion = CompanionData.MOTION.STAY0)
	{
		float target = 0f;
		if (mLive2dInfo != null)
		{
			target = mLive2dInfo.DefaultTextureLocalPositionY;
		}
		yield return StartCoroutine(JumpLive2dCharacter_Sub(motion, target, 1f, 0.75f, false, false, after_loop_motion));
	}

	private IEnumerator JumpLive2dCharacter_Sub(CompanionData.MOTION motion, float target_y, float target_shadow_a, float target_shadow_scale, bool is_animation_delay, bool is_jump_sound_enable, CompanionData.MOTION after_loop_motion)
	{
		if (mLive2dInfo == null)
		{
			yield break;
		}
		while (!mLive2dInfo.IsDone)
		{
			yield return null;
		}
		mLive2dInfo.SetMotion(motion, false);
		if (is_animation_delay)
		{
			yield return new WaitForSeconds(1f / 6f);
		}
		if (is_jump_sound_enable)
		{
			mGame.PlaySe("SE_PRESENTBOX", -1);
		}
		float fromY = mLive2dInfo.TextureLocalPositionY;
		Vector3 fromS = mLive2dInfo.ShadowScale;
		Vector3 toS = new Vector3(target_shadow_scale, target_shadow_scale, 1f);
		float fromA = mLive2dInfo.ShadowAlpha;
		float time = 4f / 15f;
		float distanceY = target_y - fromY;
		Vector3 distanceS = toS - fromS;
		float distanceA = target_shadow_a - fromA;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f / time;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mLive2dInfo.TextureLocalPositionY = fromY + distanceY * ratio;
			mLive2dInfo.ShadowAlpha = fromA + distanceA * ratio;
			mLive2dInfo.ShadowScale = fromS + distanceS * ratio;
			yield return null;
		}
		if (after_loop_motion < CompanionData.MOTION.MAXNUM)
		{
			mLive2dInfo.SetMotion(after_loop_motion, true);
		}
	}
}
