public class AdvGashaResult
{
	[MiniJSONAlias("prize_id")]
	public int prize_id { get; set; }

	[MiniJSONAlias("reward_id")]
	public int reward_id { get; set; }

	[MiniJSONAlias("receive_type")]
	public int receive_type { get; set; }

	[MiniJSONAlias("category")]
	public int category { get; set; }

	[MiniJSONAlias("sub_category")]
	public int sub_category { get; set; }

	[MiniJSONAlias("itemid")]
	public int itemid { get; set; }

	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }
}
