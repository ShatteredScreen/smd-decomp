public class ConnectAdvGetMasterTableData : ConnectBase
{
	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			base.StateStart();
			return;
		}
		if (mParam == null)
		{
			BIJLog.E("Must Parameter:" + GetType().Name);
			SetServerResponse(1, -1);
			return;
		}
		ConnectAdvGetMasterTableDataParam connectAdvGetMasterTableDataParam = mParam as ConnectAdvGetMasterTableDataParam;
		if (connectAdvGetMasterTableDataParam == null)
		{
			BIJLog.E("Type Error Parameter:" + GetType().Name + "<=" + mParam.GetType().Name);
			SetServerResponse(1, -1);
		}
		else if (!Server.AdvGetMasterTableData(connectAdvGetMasterTableDataParam.MasterTableList))
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetAdvGetMasterTableDataFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
