using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMask : MapEntity
{
	public STATE mStateStatus;

	protected new float mStateTime;

	protected int mMapIndex;

	protected bool mFirstInitialized;

	protected bool mBookedCallback;

	protected Def.SERIES mSeries;

	protected bool mIsUnlockAnimeFinished;

	protected OneShotAnime mOneShotAnime;

	public bool IsUnlockAnimeFinished
	{
		get
		{
			return mIsUnlockAnimeFinished;
		}
	}

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		mStateStatus = mState.GetStatus();
		switch (mStateStatus)
		{
		case STATE.ON:
			if (mIsUnlockAnimeFinished)
			{
				mChangeParts = new Dictionary<string, string>();
				mChangeParts["change000"] = "null";
				PlayAnime(DefaultAnimeKey, true);
				mState.Change(STATE.STAY);
			}
			break;
		case STATE.WAIT:
			if (_sprite.IsAnimationFinished() || mForceAnimeFinished)
			{
				mForceAnimeFinished = false;
				mState.Change(STATE.STAY);
			}
			break;
		}
		mState.Update();
	}

	public virtual void Init(int _counter, string _name, BIJImage atlas, float x, float y, Vector3 scale, int a_mapIndex, string a_animeKey, bool a_isLock)
	{
		mSeries = Def.SERIES.SM_FIRST;
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mAtlas = atlas;
		mScale = scale;
		mChangeParts = new Dictionary<string, string>();
		float mAP_SIGN_Z = Def.MAP_SIGN_Z;
		base._pos = new Vector3(x, y, mAP_SIGN_Z);
		bool flag = a_isLock;
		float num = 1f;
		if (flag)
		{
			IsLockTapAnime = false;
			num = 1f;
			mChangeParts["change000"] = _name;
		}
		else
		{
			mChangeParts["change000"] = "null";
			num = 0f;
		}
		PlayAnime(AnimeKey, true);
		SetAlpha(num);
		HasTapAnime = false;
		mFirstInitialized = true;
	}

	public void UnlockAnime()
	{
		AnimeKey = DefaultAnimeKey;
		HasTapAnime = true;
		mState.Change(STATE.ON);
		mIsUnlockAnimeFinished = false;
		StartCoroutine(UnlockAnimation());
	}

	public void SetEnable(bool a_flg)
	{
	}

	protected IEnumerator UnlockAnimation()
	{
		float startAlpha = 1f;
		float targetAlpha = 0f;
		float angle = 0f;
		float alpha2 = startAlpha;
		while (angle < 90f)
		{
			bool isBreak = false;
			angle += Time.deltaTime * 90f;
			if (angle > 90f)
			{
				angle = 90f;
				isBreak = true;
			}
			alpha2 = startAlpha + Mathf.Sin(angle * ((float)Math.PI / 180f)) * (targetAlpha - startAlpha);
			SetAlpha(alpha2);
			if (isBreak)
			{
				break;
			}
			yield return 0;
		}
		OnUnlockAnimationFinished();
	}

	public void OnUnlockAnimationFinished()
	{
		mIsUnlockAnimeFinished = true;
	}
}
