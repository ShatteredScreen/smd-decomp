using UnityEngine;

public class EventLine : MapLine
{
	protected short mCourseID;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public override void Init(int _counter, string _name, BIJImage atlas, int a_mapIndex, float x, float y, Vector3 scale, float a_rad, string a_animeKey)
	{
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mAtlas = atlas;
		mScale = scale;
		base._zrot = a_rad;
		float order = float.Parse(_name);
		float mAP_LINE_Z = Def.MAP_LINE_Z;
		base._pos = new Vector3(x, y, mAP_LINE_Z);
		Player mPlayer = mGame.mPlayer;
		mSeries = mPlayer.Data.CurrentSeries;
		int num = (base.StageNo = Def.GetStageNoByRouteOrder(order));
		base.gameObject.name = "line_" + num;
		Player.STAGE_STATUS sTAGE_STATUS = Player.STAGE_STATUS.NOOPEN;
		if (mGame.mEventMode)
		{
			PlayerEventData data;
			mPlayer.Data.GetMapData(mSeries, mPlayer.Data.CurrentEventID, out data);
			if (data != null)
			{
				sTAGE_STATUS = mPlayer.GetLineStatus(mPlayer.Data.CurrentEventID, mCourseID, num);
			}
		}
		else
		{
			sTAGE_STATUS = mPlayer.GetLineStatus(mSeries, num);
		}
		switch (sTAGE_STATUS)
		{
		case Player.STAGE_STATUS.NOOPEN:
			PlayAnime(AnimeKey + "_NOOPEN", true);
			break;
		case Player.STAGE_STATUS.LOCK:
			PlayAnime(AnimeKey + "_LOCK_IDLE", true);
			break;
		default:
			PlayAnime(AnimeKey + "_UNLOCK_IDLE", true);
			break;
		}
		IsLockTapAnime = false;
		HasTapAnime = false;
		mFirstInitialized = true;
	}
}
