using System;
using System.Collections.Generic;
using UnityEngine;
using com.adjust.sdk;

public class Marketing : SingletonMonoBehaviour<Marketing>
{
	private class AdjustItemToken
	{
		public int mItemNo;

		public string mAD_EvToken;

		public string miOS_EvToken;

		public AdjustItemToken(int itemNo, string AD_EvToken, string iOS_EvToken)
		{
			mItemNo = itemNo;
			mAD_EvToken = AD_EvToken;
			miOS_EvToken = iOS_EvToken;
		}
	}

	private bool tokenSent;

	private bool isTerminating;

	private AdjustItemToken[] AdjustItemTokenTable = new AdjustItemToken[6]
	{
		new AdjustItemToken(0, "g3z0g2", "3m0c38"),
		new AdjustItemToken(1, "mxqb5l", "eaxn4j"),
		new AdjustItemToken(2, "qaj6g1", "176vt3"),
		new AdjustItemToken(3, "7n3i7e", "aba8hx"),
		new AdjustItemToken(4, "oqjq3q", "q41hjd"),
		new AdjustItemToken(5, "rs5di3", "cxgcls")
	};

	public static INetworkConstants MarketingConstants { get; set; }

	public static string DeviceToken { get; set; }

	public Marketing()
	{
		SingletonMonoBehaviour<Marketing>.SetSingletonType(typeof(Marketing));
	}

	protected override void Awake()
	{
		base.Awake();
	}

	private void AwakeSingleton()
	{
		if (MarketingConstants == null)
		{
		}
		InitPartytrack();
		InitGrowthPush();
		InitAdjust();
	}

	private void Update()
	{
	}

	private new void OnDestroy()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			Term();
		}
	}

	public void OnApplicationQuit()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			SendMessage("OnApplicationQuit", SendMessageOptions.DontRequireReceiver);
			Term();
		}
	}

	public void Term()
	{
	}

	private static void log(string msg)
	{
		try
		{
			long num = (long)Math.Floor((double)Time.realtimeSinceStartup * 1000.0);
		}
		catch (Exception)
		{
		}
	}

	private static void dbg(string msg)
	{
	}

	private void InitPartytrack()
	{
	}

	public void PartytrackSendPayment(string item_name)
	{
	}

	public void PartytrackSendPayment(string item_name, int item_num, string item_price_currency, double item_price)
	{
	}

	public void PartytrackSendEvent(int _id)
	{
	}

	private void InitGrowthPush()
	{
		GrowthPush.Environment environment = GrowthPush.Environment.Production;
		string growthPush_APPID = MarketingConstants.GrowthPush_APPID;
		string growthPush_SECRET = MarketingConstants.GrowthPush_SECRET;
		string growthPush_GCM_SENDER_ID = MarketingConstants.GrowthPush_GCM_SENDER_ID;
		string empty = string.Empty;
		empty = "Information";
		GrowthPush.GetInstance().Initialize(growthPush_APPID, growthPush_SECRET, environment, true, empty);
		GrowthPush.GetInstance().RequestDeviceToken(growthPush_GCM_SENDER_ID);
		GrowthPush.GetInstance().ClearBadge();
		GrowthPush.GetInstance().TrackEvent("Launch");
		GrowthPush.GetInstance().SetTag("BHiveId", string.Empty + SingletonMonoBehaviour<Network>.Instance.HiveId);
		GrowthPush.GetInstance().SetTag("UUID", string.Empty + SingletonMonoBehaviour<Network>.Instance.UUID);
		GrowthPush.GetInstance().SetTag("NickName", SingletonMonoBehaviour<Network>.Instance.NickName);
		GrowthPush.GetInstance().SetTag("SearchID", SingletonMonoBehaviour<Network>.Instance.SearchID);
	}

	public static void GrowthPushTrackEvent(string name)
	{
		GrowthPushTrackEvent(name, null);
	}

	public static void GrowthPushTrackEvent(string name, string value)
	{
		if (value == null)
		{
			GrowthPush.GetInstance().TrackEvent(name);
		}
		else
		{
			GrowthPush.GetInstance().TrackEvent(name, value);
		}
	}

	public static void GrowthPushSetTag(string name)
	{
		GrowthPushSetTag(name, null);
	}

	public static void GrowthPushSetTag(string name, string value)
	{
		if (value == null)
		{
			GrowthPush.GetInstance().SetTag(name);
		}
		else
		{
			GrowthPush.GetInstance().SetTag(name, value);
		}
	}

	private void InitAdjust()
	{
		GameObject gameObject = new GameObject("Adjust");
		Adjust adjust = gameObject.AddComponent<Adjust>();
	}

	public void AdjustSendEvent(string evtToken)
	{
		AdjustEvent adjustEvent = new AdjustEvent(evtToken);
		Adjust.trackEvent(adjustEvent);
	}

	public void AdjustSendPayment(int _itemNo, string _currency, double _price)
	{
		string text = null;
		for (int i = 0; i < AdjustItemTokenTable.Length; i++)
		{
			if (AdjustItemTokenTable[i].mItemNo == _itemNo)
			{
				text = AdjustItemTokenTable[i].mAD_EvToken;
				break;
			}
		}
		if (text != null)
		{
			AdjustEvent adjustEvent = new AdjustEvent(text);
			adjustEvent.setRevenue(_price, _currency);
			Adjust.trackEvent(adjustEvent);
		}
	}

	public void AdjustSendGDPR(string _token, List<string> _keys, List<int> _values)
	{
		if (string.IsNullOrEmpty(_token))
		{
			return;
		}
		AdjustEvent adjustEvent = new AdjustEvent(_token);
		for (int i = 0; i < _keys.Count; i++)
		{
			if (!string.IsNullOrEmpty(_keys[i]))
			{
				adjustEvent.addCallbackParameter(_keys[i], _values[i].ToString());
			}
		}
		if (adjustEvent.callbackList != null && adjustEvent.callbackList.Count > 0)
		{
			Adjust.trackEvent(adjustEvent);
		}
	}
}
