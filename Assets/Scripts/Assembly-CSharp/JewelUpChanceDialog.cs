using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JewelUpChanceDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		CANCEL = 0,
		CONSUME = 1
	}

	private enum LOAD_ATLAS
	{
		DIALOG_BASE = 0,
		JEWEL_UP_CHANCE_DIALOG = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		MAINTENANCE_CHECK_START = 2,
		MAINTENANCE_CHECK_WAIT = 3,
		MAINTENANCE_CHECK_START2 = 4,
		MAINTENANCE_CHECK_WAIT2 = 5,
		NETWORK_00 = 6,
		NETWORK_00_1 = 7,
		NETWORK_00_2 = 8,
		NETWORK_01 = 9,
		NETWORK_01_1 = 10,
		NETWORK_01_2 = 11,
		NETWORK_02 = 12,
		NETWORK_02_1 = 13,
		NETWORK_02_2 = 14,
		AUTHERROR_WAIT = 15
	}

	private sealed class ClosedCallback : UnityEvent<SELECT_ITEM>
	{
	}

	public sealed class AtlasInfo : IDisposable
	{
		private string mKey;

		private bool mInit;

		private bool mForceNotUnload;

		private bool mDisposed;

		public ResImage Res { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Res == null) ? null : Res.Image;
			}
		}

		public AtlasInfo(string key)
		{
			mKey = key;
		}

		~AtlasInfo()
		{
			Dispose(false);
		}

		public IEnumerator Load(bool not_unload = false)
		{
			if (Res != null)
			{
				UnLoad();
			}
			if (Res != null)
			{
				yield break;
			}
			if (ResourceManager.IsLoaded(mKey))
			{
				Res = ResourceManager.LoadImage(mKey);
				if (Res != null)
				{
					yield break;
				}
			}
			Res = ResourceManager.LoadImageAsync(mKey);
			mInit = true;
			mForceNotUnload = not_unload;
		}

		public void UnLoad()
		{
			if (mInit && !mForceNotUnload)
			{
				ResourceManager.UnloadImage(mKey);
			}
			mInit = false;
		}

		public bool IsDone()
		{
			return !mInit || (mInit && Res != null && Res.LoadState == ResourceInstance.LOADSTATE.DONE);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	private static readonly Color PRICE_LABEL_LACKING_GEM_COLOR = new Color(1f, 0.1f, 0.1f);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAINTENANCE_CHECK_START);

	private Dictionary<LOAD_ATLAS, AtlasInfo> mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();

	private SELECT_ITEM mSelectItem;

	private ClosedCallback mClosedCallback = new ClosedCallback();

	private UnityEvent mAuthErrorCallback = new UnityEvent();

	private int mAcquireJewel;

	private GameObject mGameObject;

	private UILabel mPriceLabel;

	private bool mIsCreatedGemWindow;

	private short mCost;

	public bool IsBusy
	{
		get
		{
			return GetBusy() || mState.GetStatus() != STATE.MAIN;
		}
	}

	protected GameObject GameObject
	{
		get
		{
			if (mGameObject == null)
			{
				mGameObject = base.gameObject;
			}
			return mGameObject;
		}
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy())
		{
			switch (mState.GetStatus())
			{
			case STATE.MAINTENANCE_CHECK_START:
				MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
				mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
				break;
			case STATE.MAINTENANCE_CHECK_WAIT:
				if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
				{
					mState.Change(STATE.NETWORK_00);
				}
				break;
			case STATE.MAINTENANCE_CHECK_START2:
				MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY);
				mState.Change(STATE.MAINTENANCE_CHECK_WAIT2);
				break;
			case STATE.MAINTENANCE_CHECK_WAIT2:
				switch (MaintenanceCheckResult())
				{
				case MAINTENANCE_RESULT.OK:
					mState.Change(STATE.NETWORK_02);
					break;
				case MAINTENANCE_RESULT.ERROR:
				case MAINTENANCE_RESULT.IN_MAINTENANCE:
					if (!GetBusy())
					{
						mState.Change(STATE.MAIN);
					}
					break;
				}
				break;
			case STATE.NETWORK_00:
				if (!mGame.Network_UserAuth())
				{
					if (GameMain.mUserAuthSucceed)
					{
						mState.Change(STATE.NETWORK_01);
					}
					else
					{
						ShowNetworkErrorDialog(STATE.NETWORK_00);
					}
				}
				else
				{
					mState.Change(STATE.NETWORK_00_1);
				}
				break;
			case STATE.NETWORK_00_1:
			{
				if (!GameMain.mUserAuthCheckDone)
				{
					break;
				}
				if (GameMain.mUserAuthSucceed)
				{
					if (!GameMain.mUserAuthTransfered)
					{
						mState.Change(STATE.NETWORK_00_2);
					}
					else
					{
						mState.Change(STATE.AUTHERROR_WAIT);
					}
					break;
				}
				GameMain.OnConnectRetry a_callback2 = delegate(int fn_a_state)
				{
					mState.Change((STATE)fn_a_state);
				};
				GameMain.OnConnectAbandon a_abandon2 = delegate
				{
					mGame.PlaySe("SE_NEGATIVE", -1);
					Close();
				};
				Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
				if (mUserAuthErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 6, a_callback2, a_abandon2);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 6, a_callback2, null);
				}
				mState.Change(STATE.WAIT);
				break;
			}
			case STATE.NETWORK_00_2:
				mState.Change(STATE.NETWORK_01);
				break;
			case STATE.AUTHERROR_WAIT:
				ClearCallback(CALLBACK_STATE.OnCloseFinished);
				ClearClosedEvent();
				if (mAuthErrorCallback != null)
				{
					mAuthErrorCallback.Invoke();
				}
				Close();
				mState.Change(STATE.WAIT);
				break;
			case STATE.NETWORK_01:
				if (!mGame.Network_GetGemCount())
				{
					if (GameMain.mGetGemCountSucceed)
					{
						mState.Change(STATE.MAIN);
					}
					else
					{
						ShowNetworkErrorDialog(STATE.NETWORK_01);
					}
				}
				mState.Change(STATE.NETWORK_01_1);
				break;
			case STATE.NETWORK_01_1:
				if (GameMain.mGetGemCountCheckDone)
				{
					if (GameMain.mGetGemCountSucceed)
					{
						mState.Change(STATE.NETWORK_01_2);
					}
					else
					{
						ShowNetworkErrorDialog(STATE.NETWORK_01);
					}
				}
				break;
			case STATE.NETWORK_01_2:
				mState.Change(STATE.MAIN);
				break;
			case STATE.NETWORK_02:
			{
				ShopItemData shopItemData = ((!mGame.mShopItemData.ContainsKey(mGame.SegmentProfile.LabyrinthBonusItemID)) ? null : mGame.mShopItemData[mGame.SegmentProfile.LabyrinthBonusItemID]);
				mGame.ConnectRetryFlg = false;
				if (shopItemData != null && !mGame.Network_BuyItem(shopItemData.Index))
				{
					ShowNetworkErrorDialog(STATE.MAIN);
				}
				else
				{
					mState.Change(STATE.NETWORK_02_1);
				}
				break;
			}
			case STATE.NETWORK_02_1:
			{
				if (!GameMain.mBuyItemCheckDone)
				{
					break;
				}
				if (GameMain.mBuyItemSucceed)
				{
					mState.Change(STATE.NETWORK_02_2);
					break;
				}
				GameMain.OnConnectRetry a_callback = delegate(int fn_a_state)
				{
					mGame.ConnectRetryFlg = true;
					mState.Change((STATE)fn_a_state);
				};
				GameMain.OnConnectAbandon a_abandon = delegate
				{
					mGame.ConnectRetryFlg = false;
					mGame.PlaySe("SE_NEGATIVE", -1);
					Close();
				};
				switch (GameMain.mBuyItemErrorCode)
				{
				case Server.ErrorCode.MAINTENANCE_MODE:
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 12, a_callback, a_abandon);
					mState.Change(STATE.WAIT);
					break;
				case Server.ErrorCode.DUPLICATE_DATA:
				{
					ConfirmDialog confirmDialog = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
					confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
					confirmDialog.SetClosedCallback(delegate
					{
						mGame.GetGemCountFreqFlag = false;
						mState.Change(STATE.NETWORK_00);
					});
					mState.Change(STATE.WAIT);
					break;
				}
				default:
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 12, a_callback, a_abandon);
					mState.Change(STATE.WAIT);
					break;
				}
				break;
			}
			case STATE.NETWORK_02_2:
			{
				mSelectItem = SELECT_ITEM.CONSUME;
				int currentEventID = mGame.mPlayer.Data.CurrentEventID;
				if (!mGame.mServerCramVariableData.Labyrinth.ContainsKey(currentEventID))
				{
					mGame.mServerCramVariableData.Labyrinth.Add(currentEventID, new ServerCramVariableData.LabyrinthInfo
					{
						CourseClearCount = new Dictionary<int, int>(),
						DoubleUpChanceCount = 0,
						TotalStageClearCount = 0
					});
				}
				ServerCramVariableData.LabyrinthInfo value = mGame.mServerCramVariableData.Labyrinth[currentEventID];
				value.DoubleUpChanceCount++;
				mGame.mServerCramVariableData.Labyrinth[currentEventID] = value;
				mGame.SaveServerCramVariableData();
				Close();
				mState.Change(STATE.MAIN);
				break;
			}
			}
		}
		mState.Update();
	}

	public void Init(int acquire_jewel)
	{
		mAcquireJewel = acquire_jewel;
	}

	public override void BuildDialog()
	{
		UISprite uISprite = null;
		UIButton uIButton = null;
		UIFont atlasFont = GameMain.LoadFont();
		UIGrid uIGrid = null;
		GameObject gameObject = null;
		string text = null;
		int num = ((mAcquireJewel * 2 == 0) ? 1 : ((int)Mathf.Log10(mAcquireJewel * 2) + 1));
		InitDialogFrame(DIALOG_SIZE.MEDIUM, "DIALOG_BASE", "instruction_panel15", 575, 470);
		mDialogFrame.type = UIBasicSprite.Type.Advanced;
		mDialogFrame.topType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.bottomType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.leftType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.rightType = UIBasicSprite.AdvancedType.Tiled;
		mDialogFrame.centerType = UIBasicSprite.AdvancedType.Tiled;
		CreateCancelButton("OnCancelButton_Clicked");
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			CreateGemWindow();
			mIsCreatedGemWindow = true;
		}
		uISprite = Util.CreateSprite("Title", GameObject, mAtlas[LOAD_ATLAS.JEWEL_UP_CHANCE_DIALOG].Image);
		Util.SetSpriteInfo(uISprite, "jewelup_chara", mBaseDepth + 1, new Vector3(0f, 226f), Vector3.one, false);
		text = "LC_button_increase";
		uIButton = Util.CreateJellyImageButton("DecideButton", GameObject, mAtlas[LOAD_ATLAS.JEWEL_UP_CHANCE_DIALOG].Image);
		Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 1, new Vector3(0f, -170f), Vector3.one);
		Util.SetImageButtonMessage(uIButton, this, "OnDecideButton_Clicked", UIButtonMessage.Trigger.OnClick);
		int labyrinthBonusItemID = mGame.SegmentProfile.LabyrinthBonusItemID;
		int num2 = ((!mGame.mShopItemData.ContainsKey(labyrinthBonusItemID)) ? 3 : mGame.mShopItemData[labyrinthBonusItemID].GemAmount);
		mPriceLabel = Util.CreateLabel("PossessGem", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(mPriceLabel, num2.ToString(), mBaseDepth + 2, new Vector3(75f, -1f), 26, 60, 0, UIWidget.Pivot.Center);
		Util.SetLabeShrinkContent(mPriceLabel, 60, 26);
		if (num2 > mGame.mPlayer.Dollar)
		{
			Util.SetLabelColor(mPriceLabel, PRICE_LABEL_LACKING_GEM_COLOR);
		}
		else
		{
			Util.SetLabelColor(mPriceLabel, Def.DEFAULT_MESSAGE_COLOR);
		}
		mCost = (short)num2;
		uISprite = Util.CreateSprite("InnerFrame", GameObject, mAtlas[LOAD_ATLAS.DIALOG_BASE].Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, -10f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(500, 240);
		gameObject = uISprite.gameObject;
		uISprite = Util.CreateSprite("Caption", gameObject, mAtlas[LOAD_ATLAS.JEWEL_UP_CHANCE_DIALOG].Image);
		Util.SetSpriteInfo(uISprite, "jewelup_chance00", mBaseDepth + 2, new Vector3(0f, 40f), Vector3.one, false);
		uIGrid = Util.CreateGameObject("Detail", gameObject).AddComponent<UIGrid>();
		uIGrid.cellWidth = ((num > 3) ? 65 : 80);
		uIGrid.pivot = UIWidget.Pivot.Center;
		uIGrid.SetLocalPosition((num > 3) ? (-40) : (-15), -60f);
		gameObject = uIGrid.gameObject;
		int keta = ((mAcquireJewel == 0) ? 1 : ((int)Mathf.Log10(mAcquireJewel) + 1));
		NumberImageString numberImageString = Util.CreateGameObject("BeforeNum", gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(keta, mAcquireJewel, mBaseDepth + 3, 1f, UIWidget.Pivot.Right, false, mAtlas[LOAD_ATLAS.JEWEL_UP_CHANCE_DIALOG].Image, false, new string[10] { "jewelup_l_num0", "jewelup_l_num1", "jewelup_l_num2", "jewelup_l_num3", "jewelup_l_num4", "jewelup_l_num5", "jewelup_l_num6", "jewelup_l_num7", "jewelup_l_num8", "jewelup_l_num9" });
		numberImageString.SetPitch(31f);
		uISprite = Util.CreateSprite("To", gameObject, mAtlas[LOAD_ATLAS.JEWEL_UP_CHANCE_DIALOG].Image);
		Util.SetSpriteInfo(uISprite, "jewelup_chance01", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		numberImageString = Util.CreateGameObject("AfterNum", gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(num, mAcquireJewel * 2, mBaseDepth + 3, 1f, UIWidget.Pivot.Left, false, mAtlas[LOAD_ATLAS.JEWEL_UP_CHANCE_DIALOG].Image, false, new string[10] { "jewelup_b_num0", "jewelup_b_num1", "jewelup_b_num2", "jewelup_b_num3", "jewelup_b_num4", "jewelup_b_num5", "jewelup_b_num6", "jewelup_b_num7", "jewelup_b_num8", "jewelup_b_num9" });
		numberImageString.SetPitch(50f);
		mJelly.SetCustomOpenEvent(delegate(float ratio)
		{
			Transform self = mJelly.transform;
			ratio = 1f - ratio;
			if (ratio <= 0.8f)
			{
				float num3 = ratio / 0.8f;
				self.SetLocalScale(num3 * 1.1f);
				self.SetLocalEulerAnglesZ((0f - num3) * 720f);
			}
			else if (ratio <= 0.9f)
			{
				self.SetLocalScale(0.95f + (1f - (ratio - 0.8f) / 0.1f) * 0.15f);
				self.SetLocalEulerAnglesZ(-720f);
			}
			else
			{
				self.SetLocalScale(0.95f + (ratio - 0.9f) / 0.1f * 0.05f);
				self.SetLocalEulerAnglesZ(-720f);
				if (Mathf.Approximately(ratio, 1f) || ratio >= 1f)
				{
					mGame.mDialogManager.OpenParticle(base.transform.localPosition);
					if (!mIsCreatedGemWindow)
					{
						CreateGemWindow();
					}
				}
			}
		}, 120f);
	}

	private void ShowNetworkErrorDialog(STATE aNextState)
	{
		mState.Reset(STATE.WAIT, true);
		ConfirmDialog confirmDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(delegate
		{
			mState.Change(aNextState);
		});
	}

	public override IEnumerator BuildResource()
	{
		yield return base.BuildResource();
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();
		}
		foreach (AtlasInfo info3 in mAtlas.Values)
		{
			info3.UnLoad();
		}
		mAtlas.Clear();
		foreach (int type in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			AtlasInfo info2 = new AtlasInfo(((LOAD_ATLAS)type).ToString());
			mAtlas.Add((LOAD_ATLAS)type, info2);
			StartCoroutine(info2.Load());
		}
		foreach (AtlasInfo info in mAtlas.Values)
		{
			while (!info.IsDone())
			{
				yield return null;
			}
		}
	}

	private void OnDecideButton_Clicked(GameObject go)
	{
		if (IsBusy)
		{
			return;
		}
		ShopItemData shop = ((!mGame.mShopItemData.ContainsKey(mGame.SegmentProfile.LabyrinthBonusItemID)) ? null : mGame.mShopItemData[mGame.SegmentProfile.LabyrinthBonusItemID]);
		if (shop == null)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		if (shop.GemAmount <= mGame.mPlayer.Dollar)
		{
			GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
			gemUseConfirmDialog.Init(shop.GemAmount, 4);
			gemUseConfirmDialog.SetClosedCallback(delegate(GemUseConfirmDialog.SELECT_ITEM i, int a_value)
			{
				if (i == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					mState.Change((STATE)a_value);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			});
		}
		else
		{
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			confirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
			{
				if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
					purchaseDialog.SetBaseDepth(mBaseDepth + 10);
					purchaseDialog.SetClosedCallback(delegate
					{
						mGemDisplayNum = mGame.mPlayer.Dollar;
						mGemNumLabel.text = mGemDisplayNum.ToString();
						if (shop.GemAmount > mGame.mPlayer.Dollar)
						{
							mPriceLabel.color = PRICE_LABEL_LACKING_GEM_COLOR;
						}
						else
						{
							DialogBase.SetDialogLabelEffect2(mPriceLabel);
						}
						mState.Change(STATE.MAIN);
					});
					purchaseDialog.SetAuthErrorClosedCallback(delegate
					{
						mState.Change(STATE.AUTHERROR_WAIT);
					});
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			});
		}
		mGame.PlaySe("SE_POSITIVE", -1);
	}

	private void OnCancelButton_Clicked(GameObject go)
	{
		if (!IsBusy)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public override void OnCloseFinished()
	{
		bool aBuy = false;
		if (mSelectItem != 0)
		{
			aBuy = true;
		}
		UserBehavior.Instance.CountSpecialChance(5, aBuy, mCost);
		UserBehavior.Save();
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		if (mAtlas != null)
		{
			foreach (AtlasInfo info in mAtlas.Values)
			{
				info.Dispose();
			}
			mAtlas.Clear();
		}
		mAtlas = null;
		if (mClosedCallback != null)
		{
			mClosedCallback.Invoke(mSelectItem);
		}
		yield break;
	}

	public override void OnBackKeyPress()
	{
		OnCancelButton_Clicked(null);
	}

	public void AddClosedEvent(params UnityAction<SELECT_ITEM>[] calls)
	{
		if (calls != null)
		{
			if (mClosedCallback == null)
			{
				mClosedCallback = new ClosedCallback();
			}
			foreach (UnityAction<SELECT_ITEM> call in calls)
			{
				mClosedCallback.AddListener(call);
			}
		}
	}

	public void RemoveClosedEvent(params UnityAction<SELECT_ITEM>[] calls)
	{
		if (calls != null && mClosedCallback != null)
		{
			foreach (UnityAction<SELECT_ITEM> call in calls)
			{
				mClosedCallback.RemoveListener(call);
			}
		}
	}

	public void ClearClosedEvent()
	{
		if (mClosedCallback != null)
		{
			mClosedCallback.RemoveAllListeners();
		}
	}

	public void SetClosedEvent(params UnityAction<SELECT_ITEM>[] calls)
	{
		ClearClosedEvent();
		AddClosedEvent(calls);
	}

	public void AddAuthErrorEvent(params UnityAction[] calls)
	{
		if (calls != null)
		{
			if (mAuthErrorCallback == null)
			{
				mAuthErrorCallback = new UnityEvent();
			}
			foreach (UnityAction call in calls)
			{
				mAuthErrorCallback.AddListener(call);
			}
		}
	}

	public void RemoveAuthErrorEvent(params UnityAction[] calls)
	{
		if (calls != null && mAuthErrorCallback != null)
		{
			foreach (UnityAction call in calls)
			{
				mAuthErrorCallback.RemoveListener(call);
			}
		}
	}

	public void ClearAuthErrorEvent()
	{
		if (mAuthErrorCallback != null)
		{
			mAuthErrorCallback.RemoveAllListeners();
		}
	}

	public void SetAuthErrorEvent(params UnityAction[] calls)
	{
		ClearAuthErrorEvent();
		AddAuthErrorEvent(calls);
	}
}
