public class BIJNullable
{
	public static implicit operator bool(BIJNullable o)
	{
		return o != null;
	}
}
