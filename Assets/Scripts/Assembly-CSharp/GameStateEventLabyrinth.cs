using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameStateEventLabyrinth : GameStateSMMapBase
{
	public enum STATE
	{
		NONE = 0,
		LOAD_WAIT = 1,
		UNLOAD_WAIT = 2,
		INIT = 3,
		UNLOCK_ACTING = 4,
		MAIN = 5,
		SWITCH_TO_IN = 6,
		SWITCH_FROM_IN = 7,
		SWITCH_WAIT = 8,
		WAIT = 9,
		PLAY = 10,
		PAUSE = 11,
		STORY_DEMO_FADE = 12,
		STORY_DEMO = 13,
		WEBVIEW = 14,
		WEBVIEW_WAIT = 15,
		TUTORIAL = 16,
		NETWORK_CONNECT_WAIT = 17,
		NETWORK_PROLOGUE = 18,
		NETWORK_MAINTENANCE = 19,
		NETWORK_MAINTENANCE_00 = 20,
		NETWORK_AUTH = 21,
		NETWORK_AUTH_00 = 22,
		NETWORK_00 = 23,
		NETWORK_00_00 = 24,
		NETWORK_01 = 25,
		NETWORK_01_00 = 26,
		NETWORK_02 = 27,
		NETWORK_02_00 = 28,
		NETWORK_03 = 29,
		NETWORK_03_00 = 30,
		NETWORK_04 = 31,
		NETWORK_ADV_GET_MAIL = 32,
		NETWORK_ADV_GET_MAIL_WAIT = 33,
		NETWORK_04_00 = 34,
		NETWORK_04_00_1 = 35,
		NETWORK_04_00_2 = 36,
		NETWORK_04_01 = 37,
		NETWORK_04_01_1 = 38,
		NETWORK_04_01_2 = 39,
		NETWORK_04_02 = 40,
		NETWORK_04_02_1 = 41,
		NETWORK_04_02_2 = 42,
		NETWORK_04_02_3 = 43,
		NETWORK_04_03 = 44,
		NETWORK_04_03_1 = 45,
		NETWORK_04_04 = 46,
		NETWORK_04_99 = 47,
		NETWORK_GETFRIENDINFO = 48,
		NETWORK_GETFRIENDINFO_00 = 49,
		NETWORK_GETCAMPAIGNINFO = 50,
		NETWORK_GETCAMPAIGNINFO_00 = 51,
		NETWORK_EPILOGUE = 52,
		NETWORK_REWARD_CHECK = 53,
		NETWORK_REWARD_CHECK_00 = 54,
		NETWORK_REWARD_MAINTENACE = 55,
		NETWORK_REWARD_MAINTENACE_00 = 56,
		NETWORK_REWARD_SET = 57,
		NETWORK_REWARD_SET_00 = 58,
		NETWORK_ADV_GETREWARDINFO = 59,
		NETWORK_ADV_GETREWARDINFO_00 = 60,
		NETWORK_ADV_REWARD_SET = 61,
		NETWORK_ADV_REWARD_SET_00 = 62,
		NETWORK_RESUME_MAINTENACE = 63,
		NETWORK_RESUME_MAINTENACE_00 = 64,
		NETWORK_RESUME_GAMEINFO = 65,
		NETWORK_RESUME_GAMEINFO_00 = 66,
		NETWORK_RESUME_CAMPAIGN = 67,
		NETWORK_RESUME_CAMPAIGN_00 = 68,
		NETWORK_RESUME_LOGINBONUS = 69,
		NETWORK_RESUME_LOGINBONUS_00 = 70,
		NETWORK_STAGE = 71,
		NETWORK_STAGE_WAIT = 72,
		NETWORK_NOTICE_WAIT = 73,
		AUTHERROR_RETURN_TITLE = 74,
		AUTHERROR_RETURN_WAIT = 75,
		MAINTENANCE_RETURN_TITLE = 76,
		MAINTENANCE_RETURN_WAIT = 77,
		GOTO_REENTER = 78,
		OLD_EVENT_EFFECT_WAIT = 79
	}

	protected enum ANIME_TYPE
	{
		COURSE_CLEAR = 0,
		COURSESTAGE_COMP = 1,
		COURSESTAR_COMP = 2,
		EVENT_COMP = 3
	}

	private const float LABYRINTH_STAGEPOS_OFFSET_COURSE = 100f;

	private const float LABYRINTH_STAGEPOS_OFFSET_STAGE = 250f;

	private const float Map_LimitOffset_PageA = 300f;

	private const float Map_LimitOffset_PageB = 0f;

	public static readonly string FixedSpriteAnimationNameFormat = "MAP_EV{0}_FIXEDSPRITE";

	public static readonly string FixedSpriteAnimationNameFormat_IN = "MAP_EV{0}_IN_FIXEDSPRITE";

	public static readonly string FixedSpriteAnimationNameFormatLabyrinth = "LABYRINTH_FIXEDSPRITE";

	public static readonly string GoalAnimationNameFormat = "LABYRINTHREWARD_JEWELCUP";

	public static readonly string RewardItemGetAnimationFormat = "LABYRINTHREWARD_ITEMGET";

	public static readonly string RewardFixedSpriteAnimationFormat = "LABYRINTHREWARD_FIXEDSPRITE";

	public static readonly string RewardBoxAnimationFormat = "LABYRINTHREWARD_BOX";

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private STATE mRewardErrorPrevState = STATE.MAIN;

	private string mGuID;

	protected int mBundleAccessoryCount;

	protected AccessoryData mOpenPuzzleAccessory;

	protected AccessoryData mOtherOpenPuzzleAccessory;

	protected AccessoryData mAccessory_GrowStamp;

	protected OneShotAnime mClearAnimation;

	protected OneShotAnime mCompleteAnimation;

	protected bool mIsNextCourseDialogFlg;

	protected bool mIsFinishedCourseMove;

	protected List<short> mNewCourseNotifyList = new List<short>();

	protected bool mFirstRoadblockDialogFlg;

	protected bool mAdvGuideDialogFlg;

	private EventImageDialog mEventImageDialog;

	private bool mHasSecondCompanion;

	private string DEFAULT_EVENT_BGM = "BGM_MAP_F";

	private SMEventPageData mEventPageData;

	public short CurrentCourseID = -1;

	private bool mEffectedOldEvent;

	private bool mIntroStagingFlg;

	private int mIntroAccessory;

	private int mIntroStars;

	private float mIntroCurrentPos;

	private bool mSecondAccessoryFlg;

	private bool mSecondAccessoryDisplayFlg;

	private STATE mStateAfterConnect = STATE.NETWORK_EPILOGUE;

	private STATE mBootConnectErrorState;

	private STATE mSwitchToNextState;

	private SMMapBG mBGImage;

	private bool mIsCourseRestart;

	private ImageTextDescDialog mImageTextDialog;

	private new ConfirmImageDialog mConfirmImageDialog;

	private LabyrinthResultDialog mLabyrinthResultDialog;

	private int mCourseClearRewardNum;

	private List<AccessoryData> mGetGemAccessoryDataList = new List<AccessoryData>();

	private List<AccessoryData> mGetPartnerAccessoryDataList = new List<AccessoryData>();

	private List<AccessoryData> mGetKeyAccessoryDataList = new List<AccessoryData>();

	private List<AccessoryData> mGetAccessoryDataList = new List<AccessoryData>();

	public STATE StateStatus = STATE.WAIT;

	public STATE OverwriteState = STATE.WAIT;

	private bool mIsSwitchScreen;

	protected bool mStartWebview;

	public float Event_DeviceOffset;

	public bool IsNewCourseUnlocked
	{
		get
		{
			return mNewCourseNotifyList.Count > 0;
		}
	}

	public bool IsInCourseSelect { get; private set; }

	protected override void RegisterServerCallback()
	{
		Server.GetEventStageRankingDataEvent += Server_OnGetStageRankingData;
	}

	protected override void UnregisterServerCallback()
	{
		Server.GetEventStageRankingDataEvent -= Server_OnGetStageRankingData;
	}

	protected override void Network_OnStage()
	{
		STATE aStatus = STATE.WAIT;
		int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
		int currentLevel = playerMapData.CurrentLevel;
		base.RankingData = null;
		mState.Change(STATE.NETWORK_STAGE_WAIT);
		bool flag = false;
		if (!Server.GetEventStageRankingData(currentSeries, currentLevel, CurrentEventID, CurrentCourseID))
		{
			mState.Change(aStatus);
		}
	}

	protected override void Network_OnStageWait()
	{
		_GetRankingData rankingData = base.RankingData;
		if (rankingData != null && rankingData.results.Count > 0)
		{
			mState.Change(STATE.WAIT);
			if (EnableSocialRankingDialog())
			{
				ShowSocialRankingDialog(rankingData.series, rankingData.stage, rankingData.results);
			}
		}
	}

	protected virtual void Server_OnGetStageRankingData(int result, int code, string json)
	{
		if (mState.GetStatus() != STATE.NETWORK_STAGE_WAIT || result != 0)
		{
			return;
		}
		try
		{
			_GetRankingData obj = new _GetRankingData();
			new MiniJSONSerializer().Populate(ref obj, json);
			if (obj != null && obj.results != null)
			{
				for (int num = obj.results.Count - 1; num >= 0; num--)
				{
					if (obj.results[num].uuid == mGame.mPlayer.UUID)
					{
						int num2 = 0;
						PlayerEventData data;
						mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
						PlayerMapData playerMapData = data.CourseData[CurrentCourseID];
						PlayerClearData _psd;
						if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, CurrentCourseID, playerMapData.CurrentLevel, out _psd))
						{
							num2 = _psd.HightScore;
						}
						if (num2 <= 0)
						{
							obj.results.RemoveAt(num);
						}
					}
					else if (!mGame.mPlayer.Friends.ContainsKey(obj.results[num].uuid))
					{
						obj.results.RemoveAt(num);
					}
				}
				obj.results.Sort();
			}
			base.RankingData = obj;
		}
		catch (Exception)
		{
		}
	}

	public override void Start()
	{
		base.Start();
		RegisterServerCallback();
		CurrentEventID = mGame.mPlayer.Data.CurrentEventID;
		CurrentCourseID = -1;
		GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(mGame.mPlayer.Data.CurrentEventID);
		if (labyrinthData.LabyrinthSelectedCourseID > -1)
		{
			CurrentCourseID = labyrinthData.LabyrinthSelectedCourseID;
		}
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
		data.CourseID = CurrentCourseID;
	}

	public void OnDestroy()
	{
		UnregisterServerCallback();
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public override void Update()
	{
		DragPower = Vector2.zero;
		base.Update();
		if (OverwriteState != STATE.WAIT)
		{
			mState.Change(OverwriteState);
			OverwriteState = STATE.WAIT;
		}
		StateStatus = mState.GetStatus();
		if (mState.IsChanged())
		{
			if (StateStatus == STATE.MAIN)
			{
				if (mCockpit != null)
				{
					mCockpit.SetEnableButtonAction(true);
				}
				if (mCurrentPage != null)
				{
					mCurrentPage.SetEnableButton(true);
				}
			}
			else
			{
				if (mCockpit != null)
				{
					mCockpit.SetEnableButtonAction(false);
				}
				if (mCurrentPage != null)
				{
					mCurrentPage.SetEnableButton(false);
				}
			}
		}
		switch (StateStatus)
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mState.Change(STATE.NETWORK_PROLOGUE);
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_STAGE:
			Network_OnStage();
			break;
		case STATE.NETWORK_STAGE_WAIT:
			Network_OnStageWait();
			break;
		case STATE.NETWORK_CONNECT_WAIT:
			if (!mIsConnecting)
			{
				if (!mHasBootConnectingError)
				{
					mConnectionTime = Time.realtimeSinceStartup;
					mState.Change(STATE.NETWORK_PROLOGUE);
				}
				else if (mBootConnectErrorState != 0)
				{
					mState.Reset(mBootConnectErrorState, true);
				}
			}
			break;
		case STATE.GOTO_REENTER:
			if (GameMain.USE_DEBUG_DIALOG)
			{
				mGameState.SetNextGameState(mGame.GetNextGameState());
				mState.Change(STATE.UNLOAD_WAIT);
			}
			else
			{
				mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
				mState.Change(STATE.UNLOAD_WAIT);
			}
			break;
		case STATE.INIT:
			if (mState.IsChanged() && MaintenanceModeStartCheck())
			{
				mState.Reset(STATE.WAIT, true);
			}
			else if (IsInCourseSelect)
			{
				Update_INIT();
			}
			else
			{
				Update_IN_INIT();
			}
			break;
		case STATE.UNLOCK_ACTING:
			if (!isLoadFinished() || !mGameState.IsFadeFinished)
			{
				mState.Reset(mState.GetStatus(), true);
			}
			else if (mState.IsChanged() && MaintenanceModeStartCheck())
			{
				mState.Reset(STATE.WAIT, true);
			}
			else if (!Update_Common_UNLOCK_ACTING())
			{
				if (IsInCourseSelect)
				{
					Update_UNLOCK_ACTING();
				}
				else
				{
					Update_IN_UNLOCK_ACTING();
				}
			}
			break;
		case STATE.MAIN:
			if (mState.IsChanged())
			{
				if (MaintenanceModeStartCheck())
				{
					mState.Reset(STATE.WAIT, true);
					break;
				}
				switch (DLFilelistHashCheck())
				{
				case DLFILE_CHECK_RESULT.FILE_DESTROY:
					mState.Reset(STATE.WAIT, true);
					break;
				case DLFILE_CHECK_RESULT.HASH_NG:
					mState.Reset(STATE.WAIT, true);
					break;
				default:
					if (!CheaterStartCheck() && !WebviewStartCheck() && !GiftStartCheck() && !RankingRankUpStarStartCheck() && !OldEventEffectCheck())
					{
					}
					break;
				}
			}
			else
			{
				if (mSocialRanking != null)
				{
					mSocialRanking.Close();
					mSocialRanking = null;
				}
				if (IsInCourseSelect)
				{
					Update_MAIN();
				}
				else
				{
					Update_IN_MAIN();
				}
				UpdateMugenHeartStatus();
			}
			break;
		case STATE.SWITCH_TO_IN:
			StartCoroutine(SwitchScreen(LoadStageSelect()));
			mSwitchToNextState = STATE.INIT;
			mState.Reset(STATE.SWITCH_WAIT, true);
			break;
		case STATE.SWITCH_FROM_IN:
			StartCoroutine(SwitchScreen(LoadCourseSelect()));
			mSwitchToNextState = STATE.INIT;
			mState.Reset(STATE.SWITCH_WAIT, true);
			break;
		case STATE.SWITCH_WAIT:
			if (!mIsSwitchScreen)
			{
				if (mSwitchToNextState != 0)
				{
					mState.Reset(mSwitchToNextState);
					mSwitchToNextState = STATE.NONE;
				}
				else
				{
					BIJLog.E("error:NONE");
				}
			}
			break;
		case STATE.PLAY:
		{
			mGame.mReplay = false;
			mGame.ClearPresentBox();
			if (mGame.mEventMode)
			{
				mGame.mSkinIndex = mGame.mEventSkinIndex;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mPlayer.Data.SelectedCompanion;
				}
			}
			else
			{
				mGame.mSkinIndex = mGame.mPlayer.Data.SelectedSkin;
				if (mGame.mLastCompanionSetting)
				{
					mGame.mCompanionIndex = mGame.mPlayer.Data.SelectedCompanion;
				}
			}
			int playerIcon = mGame.mPlayer.Data.PlayerIcon;
			if (mGame.mPlayer.IsCompanionUnlock(mGame.mPlayer.Data.SelectedCompanion) && mGame.mLastCompanionSetting)
			{
				mGame.mPlayer.Data.LastUsedIcon = mGame.mPlayer.Data.SelectedCompanion;
				if (!mGame.mPlayer.Data.LastUsedIconCancelFlg)
				{
					if (mGame.mPlayer.Data.PlayerIcon >= 10000)
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion + 10000;
					}
					else
					{
						mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.SelectedCompanion;
					}
				}
				if (playerIcon != mGame.mPlayer.Data.PlayerIcon)
				{
					mGame.Network_NicknameEdit(string.Empty);
				}
			}
			mGame.AlreadyConnectAccessoryList.Clear();
			mGame.AlreadyConnectAdvAccessoryList.Clear();
			Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
			int stageNumber = mGame.mCurrentStage.StageNumber;
			short a_course;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(stageNumber, out a_course, out a_main, out a_sub);
			mCurrentPage.SetAvatarNode(Def.GetStageNo(a_main, a_sub));
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.PUZZLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		}
		case STATE.STORY_DEMO_FADE:
			if (mGameState.IsFadeFinished)
			{
				mGameState.FadeIn();
				if (mCurrentDemoIndex != -1)
				{
					StoryDemoManager storyDemoManager = Util.CreateGameObject("DemoManager", base.gameObject).AddComponent<StoryDemoManager>();
					storyDemoManager.ChangeStoryDemo(mCurrentDemoIndex, OnStoryDemoFinished);
					mCurrentDemoIndex = -1;
					StartCoroutine(GrayIn(1f, 0.8f));
					mState.Change(STATE.STORY_DEMO);
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			break;
		case STATE.WEBVIEW:
			OnHowToDialog();
			break;
		case STATE.TUTORIAL:
			if (mCurrentPage != null)
			{
				mCurrentPage.SetEnableButton(true);
			}
			break;
		case STATE.NETWORK_PROLOGUE:
			if (!GameMain.CanCommunication())
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
				break;
			}
			if (!mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				Server.ManualConnecting = true;
			}
			else if (mTipsScreenDisplayed)
			{
				mGameState.SetLoadingUISsAnimation(2);
			}
			mState.Change(STATE.NETWORK_MAINTENANCE);
			break;
		case STATE.NETWORK_MAINTENANCE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_MAINTENANCE_00);
			break;
		case STATE.NETWORK_MAINTENANCE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_AUTH);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 19);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_AUTH:
			mGame.Network_UserAuth();
			mState.Change(STATE.NETWORK_AUTH_00);
			break;
		case STATE.NETWORK_AUTH_00:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00);
					break;
				}
				mGame.FinishConnecting();
				mState.Change(STATE.AUTHERROR_RETURN_TITLE);
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode2 = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = mUserAuthErrorCode2;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 21, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 21, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_00:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_00_00);
			break;
		case STATE.NETWORK_00_00:
			if (GameMain.mGameProfileCheckDone)
			{
				if (GameMain.mGameProfileSucceed)
				{
					mState.Change(STATE.NETWORK_01);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 23, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_01:
			mGame.Network_GetSegmentProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_01_00);
			break;
		case STATE.NETWORK_01_00:
		{
			if (!GameMain.mSegmentProfileCheckDone)
			{
				break;
			}
			if (GameMain.mSegmentProfileSucceed)
			{
				mState.Change(STATE.NETWORK_02);
				break;
			}
			mGame.FinishConnecting();
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = mUserAuthErrorCode;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 25, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 25, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_02:
			mState.Change(STATE.NETWORK_03);
			break;
		case STATE.NETWORK_03:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_03_00);
			break;
		case STATE.NETWORK_03_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
				}
				mGame.DoLoginBonusServerCheck = false;
				mState.Change(STATE.NETWORK_04);
			}
			else
			{
				Server.ErrorCode mLoginBonusErrorCode = GameMain.mLoginBonusErrorCode;
				Server.ErrorCode errorCode = mLoginBonusErrorCode;
				if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 29, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 29, OnConnectErrorAuthRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.NETWORK_04:
			mGame.mGotGiftIds = new List<int>();
			mGame.mGotApplyIds = new List<int>();
			mGame.mGotSupportIds = new List<int>();
			mGame.mGotSupportPurchaseIds = new List<int>();
			if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				mState.Change(STATE.NETWORK_04_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_02);
			}
			break;
		case STATE.NETWORK_ADV_GET_MAIL:
		{
			int is_retry2 = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry2 = 1;
			}
			mGame.Network_AdvGetMail(mGuID, is_retry2);
			mState.Change(STATE.NETWORK_ADV_GET_MAIL_WAIT);
			break;
		}
		case STATE.NETWORK_ADV_GET_MAIL_WAIT:
		{
			if (!GameMain.mAdvGetMailCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetMailSucceed)
			{
				mGuID = null;
				mState.Change(STATE.NETWORK_04_00);
				break;
			}
			StartCoroutine(GrayOut());
			Server.ErrorCode errorCode = GameMain.mAdvGetMailErrorCode;
			if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 32, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 32, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_04_00:
		{
			STATE aStatus3 = STATE.NETWORK_04_00_1;
			if (mGame.MapNetwork_SupportCheck())
			{
				aStatus3 = STATE.NETWORK_04_01;
			}
			mState.Change(aStatus3);
			break;
		}
		case STATE.NETWORK_04_00_1:
			if (GameMain.mMapSupportCheckDone)
			{
				mState.Change(STATE.NETWORK_04_00_2);
			}
			break;
		case STATE.NETWORK_04_00_2:
			mGame.MapNetwork_SupportGot();
			mState.Change(STATE.NETWORK_04_01);
			break;
		case STATE.NETWORK_04_01:
		{
			STATE aStatus2 = STATE.NETWORK_04_01_1;
			if (mGame.MapNetwork_GiftCheck())
			{
				aStatus2 = STATE.NETWORK_04_02;
			}
			mState.Change(aStatus2);
			break;
		}
		case STATE.NETWORK_04_01_1:
			if (GameMain.mMapGiftCheckDone)
			{
				mState.Change(STATE.NETWORK_04_01_2);
			}
			break;
		case STATE.NETWORK_04_01_2:
			mGame.MapNetwork_GiftGot();
			mState.Change(STATE.NETWORK_04_02);
			break;
		case STATE.NETWORK_04_02:
		{
			STATE aStatus = STATE.NETWORK_04_02_1;
			if (mGame.MapNetwork_ApplyCheck())
			{
				aStatus = STATE.NETWORK_04_03;
			}
			mState.Change(aStatus);
			break;
		}
		case STATE.NETWORK_04_02_1:
			if (GameMain.mMapApplyCheckDone)
			{
				mState.Change(STATE.NETWORK_04_02_2);
			}
			break;
		case STATE.NETWORK_04_02_2:
			mGame.MapNetwork_ApplyGot();
			if (GameMain.mMapApplyRepeat)
			{
				mState.Change(STATE.NETWORK_04_02_3);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_03);
			}
			break;
		case STATE.NETWORK_04_02_3:
		{
			long num = DateTimeUtil.BetweenMilliseconds(mGame.mPlayer.Data.LastGetApplyTime, DateTime.Now);
			if (num > GameMain.mMapApplyRepeatWait)
			{
				mGame.mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
				mState.Change(STATE.NETWORK_04_02);
			}
			break;
		}
		case STATE.NETWORK_04_03:
			if (GameMain.NO_NETWORK)
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			else if (mGame.mTutorialManager.IsInitialTutorialCompleted())
			{
				STATE aStatus4 = STATE.NETWORK_04_03_1;
				if (mGame.MapNetwork_SupportPurchaseCheck())
				{
					aStatus4 = STATE.NETWORK_04_04;
				}
				mState.Change(aStatus4);
			}
			else
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_03_1:
			if (GameMain.mMapSupportPurchaseCheckDone)
			{
				mState.Change(STATE.NETWORK_04_04);
			}
			break;
		case STATE.NETWORK_04_04:
			mState.Reset(STATE.NETWORK_04_99, true);
			break;
		case STATE.NETWORK_04_99:
		{
			mState.Change(STATE.NETWORK_GETFRIENDINFO);
			if (mGame.mFbUserMngGift == null)
			{
				mGame.mFbUserMngGift = FBUserManager.CreateInstance();
			}
			List<GiftItemData> list = new List<GiftItemData>();
			if (mGame.mPlayer.Gifts.Count <= 0)
			{
				break;
			}
			foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
			{
				GiftItem value = gift.Value;
				list.Add(value.Data);
			}
			mGame.mFbUserMngGift.SetFBUserList(list);
			break;
		}
		case STATE.NETWORK_GETFRIENDINFO:
			mGame.mFriendManager = new GameFriendManager(mGame);
			if (!mGame.mFriendManager.Start())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mState.Change(STATE.NETWORK_GETFRIENDINFO_00);
			}
			break;
		case STATE.NETWORK_GETFRIENDINFO_00:
			if (mGame.mFriendManager == null || mGame.mFriendManager.IsFinished())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO);
			}
			else
			{
				mGame.mFriendManager.Update();
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO:
			if (mGame.Network_CampaignCheck())
			{
				mState.Change(STATE.NETWORK_GETCAMPAIGNINFO_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_GETCAMPAIGNINFO_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_EPILOGUE);
			}
			break;
		case STATE.NETWORK_EPILOGUE:
			if (mGame.mNetworkDataModified)
			{
				mGame.IsPlayerSaveRequest = true;
			}
			if (mGame.mGotGiftIds != null)
			{
				mGame.mGotGiftIds.Clear();
			}
			if (mGame.mGotSupportIds != null)
			{
				mGame.mGotSupportIds.Clear();
			}
			if (mGame.mGotApplyIds != null)
			{
				mGame.mGotApplyIds.Clear();
			}
			if (mGame.mGotSupportPurchaseIds != null)
			{
				mGame.mGotSupportPurchaseIds.Clear();
			}
			if (mTipsScreenDisplayed)
			{
				mGameState.TipsScreenOff();
			}
			else
			{
				Server.ManualConnecting = false;
			}
			mState.Change(STATE.INIT);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_REWARD_MAINTENACE_00);
			break;
		case STATE.NETWORK_REWARD_MAINTENACE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_REWARD_SET);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 55);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_REWARD_SET:
			Server.ManualConnecting = true;
			mGame.Network_RewardSet(AccessoryConnectIndex);
			mState.Reset(STATE.NETWORK_REWARD_SET_00, true);
			break;
		case STATE.NETWORK_REWARD_SET_00:
		{
			if (!GameMain.mRewardSetCheckDone)
			{
				break;
			}
			Server.ManualConnecting = false;
			string empty3 = string.Empty;
			string empty4 = string.Empty;
			if (GameMain.mRewardSetSucceed)
			{
				AccessoryData accessoryData3 = mGame.GetAccessoryData(AccessoryConnectIndex);
				bool flag2 = mGame.mPlayer.SubAccessoryConnect(accessoryData3);
				AccessoryConnectIndex = 0;
				mGame.IsPlayerSaveRequest = true;
				if (GameMain.mRewardSetStatus == 0)
				{
					int index2 = accessoryData3.Index;
					if (index2 != 0)
					{
						int cur_series2 = (int)mGame.mPlayer.Data.CurrentSeries;
						int get_type2 = 4;
						if (mGame.IsDailyChallengePuzzle)
						{
							cur_series2 = 200;
						}
						ServerCram.GetAccessory(index2, cur_series2, get_type2);
					}
					if (flag2)
					{
						mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
						mState.Reset(STATE.NETWORK_04_03, true);
					}
					else
					{
						mState.Reset(STATE.UNLOCK_ACTING, true);
					}
				}
				else
				{
					empty3 = Localization.Get("GotError_Title");
					empty4 = Localization.Get("GotError_Desc");
					if (GameStateSMMapBase.mIsShowErrorDialog)
					{
						StartCoroutine(GrayOut());
						GetRewardDialog getRewardDialog = Util.CreateGameObject("NoGetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
						List<AccessoryData> list2 = new List<AccessoryData>();
						list2.Add(accessoryData3);
						getRewardDialog.Init(list2, true);
						getRewardDialog.SetClosedCallback(OnGetRewardDialogErrorClosed);
						mState.Reset(STATE.WAIT);
					}
					else
					{
						mState.Reset(STATE.UNLOCK_ACTING, true);
					}
				}
			}
			else
			{
				empty3 = Localization.Get("Network_Error_Title");
				empty4 = Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b");
				if (GameStateSMMapBase.mIsShowErrorDialog)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 57, OnConnectErrorAccessoryRetry, OnConnectErrorAccessoryAbandon);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					OnConnectErrorAccessoryAbandon();
				}
			}
			break;
		}
		case STATE.NETWORK_ADV_GETREWARDINFO:
		{
			int[] reward_ids = new int[1] { mAdvAutoUnlockAccessory.GetGotIDByNum() };
			mGame.Network_AdvGetRewardInfo(reward_ids);
			mState.Reset(STATE.NETWORK_ADV_GETREWARDINFO_00, true);
			break;
		}
		case STATE.NETWORK_ADV_GETREWARDINFO_00:
			if (!GameMain.mAdvGetRewardInfoCheckDone)
			{
				break;
			}
			if (GameMain.mAdvGetRewardInfoSucceed)
			{
				AdvRewardListData rewardListData = mGame.GetRewardListData(mAdvAutoUnlockAccessory.GetGotIDByNum());
				if (rewardListData != null)
				{
					StartCoroutine(GrayOut());
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.AdvGetInit(mAdvAutoUnlockAccessory, rewardListData);
					mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
					mState.Reset(STATE.WAIT, true);
					mAdvAutoUnlockAccessory = null;
				}
				else if (mGame.mAdvGetRewardInfoProfile.Received_List.Count > 0)
				{
					StartCoroutine(GrayOut());
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.AdvGotInit(mAdvAutoUnlockAccessory, mGame.mAdvGetRewardInfoProfile.Received_List[0]);
					mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
					mState.Reset(STATE.WAIT, true);
					mAdvAutoUnlockAccessory = null;
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 59, OnConnectErrorAccessoryRetry, OnConnectErrorAdvAccessoryAbandon);
					mState.Reset(STATE.WAIT);
				}
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 59, OnConnectErrorAccessoryRetry, OnConnectErrorAdvAccessoryAbandon);
				mState.Reset(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_ADV_REWARD_SET:
		{
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			AccessoryData accessoryData2 = mGame.GetAccessoryData(AdvAccessoryConnectIndex);
			int[] mail_rewards_ids = new int[1] { accessoryData2.GetGotIDByNum() };
			mGame.Network_AdvSetReward(null, mail_rewards_ids, mGuID, is_retry);
			mState.Reset(STATE.NETWORK_ADV_REWARD_SET_00, true);
			break;
		}
		case STATE.NETWORK_ADV_REWARD_SET_00:
			if (!GameMain.mAdvSetRewardCheckDone)
			{
				break;
			}
			if (GameMain.mAdvSetRewardSucceed)
			{
				AccessoryData accessoryData = mGame.GetAccessoryData(AdvAccessoryConnectIndex);
				bool flag = mGame.mPlayer.SubAdvAccessoryConnect(accessoryData);
				AdvAccessoryConnectIndex = 0;
				mGame.IsPlayerSaveRequest = true;
				int index = accessoryData.Index;
				if (index != 0)
				{
					int cur_series = (int)mGame.mPlayer.Data.CurrentSeries;
					int get_type = 12;
					if (mGame.IsDailyChallengePuzzle)
					{
						cur_series = 200;
					}
					ServerCram.GetAccessory(index, cur_series, get_type);
				}
				if (flag)
				{
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mState.Reset(STATE.NETWORK_ADV_GET_MAIL, true);
				}
				else
				{
					mState.Reset(STATE.UNLOCK_ACTING, true);
				}
				mGuID = null;
			}
			else
			{
				string empty = string.Empty;
				string empty2 = string.Empty;
				empty = Localization.Get("Network_Error_Title");
				empty2 = Localization.Get("Network_Error_Desc02a") + Localization.Get("Network_Error_Desc02b");
				if (GameStateSMMapBase.mAdvIsShowErrorDialog)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 61, OnConnectErrorAccessoryRetry, OnConnectErrorAdvAccessoryAbandon);
					mState.Reset(STATE.WAIT);
				}
				else
				{
					OnConnectErrorAdvAccessoryAbandon();
				}
			}
			break;
		case STATE.NETWORK_RESUME_MAINTENACE:
			mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_MAINTENACE_00);
			break;
		case STATE.NETWORK_RESUME_MAINTENACE_00:
			if (!GameMain.mMaintenanceProfileCheckDone)
			{
				break;
			}
			if (GameMain.mMaintenanceProfileSucceed)
			{
				if (GameMain.MaintenanceMode)
				{
					mGame.FinishConnecting();
					mState.Change(STATE.MAINTENANCE_RETURN_TITLE);
				}
				else
				{
					mState.Change(STATE.NETWORK_RESUME_GAMEINFO);
				}
			}
			else
			{
				mGame.FinishConnecting();
				StartCoroutine(GrayOut());
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 63);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_GAMEINFO:
			mGame.Network_GetGameProfile(string.Empty, string.Empty);
			mState.Change(STATE.NETWORK_RESUME_GAMEINFO_00);
			break;
		case STATE.NETWORK_RESUME_GAMEINFO_00:
			if (GameMain.mGameProfileCheckDone)
			{
				if (GameMain.mGameProfileSucceed)
				{
					mState.Change(STATE.NETWORK_RESUME_CAMPAIGN);
					break;
				}
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 65, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN:
			if (PurchaseFullDialog.IsShow())
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			else if (mGame.Network_CampaignCheck())
			{
				mState.Change(STATE.NETWORK_RESUME_CAMPAIGN_00);
			}
			else
			{
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_CAMPAIGN_00:
			if (GameMain.mCampaignCheckDone)
			{
				if (GameMain.mCampaignCheckSucceed)
				{
					mGame.GetCampaignBanner(true);
				}
				mState.Change(STATE.NETWORK_RESUME_LOGINBONUS);
			}
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS:
			mGame.Network_LoginBonus();
			mState.Change(STATE.NETWORK_RESUME_LOGINBONUS_00);
			break;
		case STATE.NETWORK_RESUME_LOGINBONUS_00:
			if (!GameMain.mLoginBonusCheckDone)
			{
				break;
			}
			if (GameMain.mLoginBonusSucceed)
			{
				mGame.DoLoginBonusServerCheck = false;
				if (mGame.mLoginBonusData.HasLoginData)
				{
					mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
					mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
					mState.Change(STATE.NETWORK_04);
				}
				else
				{
					mState.Change(STATE.UNLOCK_ACTING);
				}
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 69, OnConnectErrorAuthRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.AUTHERROR_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Transfer_CertError_Title"), Localization.Get("Transfered_TitleBack_Desc2"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.AUTHERROR_RETURN_WAIT);
			break;
		case STATE.MAINTENANCE_RETURN_TITLE:
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("MaintenanceMode_ReturnTitle_Title"), Localization.Get("MaintenanceMode_ReturnTitle_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetSortOrder(510);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			mState.Change(STATE.MAINTENANCE_RETURN_WAIT);
			mGame.IsTitleReturnMaintenance = true;
			break;
		case STATE.OLD_EVENT_EFFECT_WAIT:
		{
			EventCockpit eventCockpit = mCockpit as EventCockpit;
			if (!(eventCockpit != null) || !eventCockpit.IsOldEventPlutoEffecting)
			{
				mEffectedOldEvent = true;
				mState.Change(STATE.MAIN);
			}
			break;
		}
		}
		UpdateMapPages();
		if (mGame.IsPlayerSaveRequest)
		{
			mGame.IsPlayerSaveRequest = false;
			mGame.Save();
		}
		mState.Update();
	}

	public override IEnumerator LoadGameState()
	{
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		mTipsScreenDisplayed = false;
		if (mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			mTipsScreenDisplayed = true;
			yield return StartCoroutine(mGameState.CoTipsScreenOn(1, false));
		}
		yield return StartCoroutine(LoadCommonResources());
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		GameObject parent = mRoot;
		mAnchorBottom = Util.CreateAnchorWithChild("MapAnchorBottom", parent, UIAnchor.Side.Bottom, camera).gameObject;
		mAnchorRight = Util.CreateAnchorWithChild("MapAnchorRight", parent, UIAnchor.Side.Right, camera).gameObject;
		mAnchorCenter = Util.CreateAnchorWithChild("MapAnchorCenter", parent, UIAnchor.Side.Center, camera).gameObject;
		mMapPageRoot = Util.CreateGameObject("MapPageRoot", mAnchorCenter.gameObject);
		ResourceManager.LoadImage("FADE");
		mMapPageData = GameMain.GetCurrentEventPageData();
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		DEFAULT_EVENT_BGM = eventPageData.EventSetting.MapEventBGM;
		if (DEFAULT_EVENT_BGM == string.Empty)
		{
			DEFAULT_EVENT_BGM = "BGM_MAP";
		}
		ResSound resSound = ResourceManager.LoadSoundAsync(DEFAULT_EVENT_BGM);
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		Player p = mGame.mPlayer;
		Def.SERIES currentSeries = p.Data.CurrentSeries;
		p.Data.MakeLabyrinthEventData(CurrentEventID, eventPageData);
		List<ResourceInstance> loadInstanceList = new List<ResourceInstance>();
		ResImage resImage = ResourceManager.LoadImageAsync(eventPageData.EventSetting.MapAtlasKey + "_BG");
		loadInstanceList.Add(resImage);
		ResImage resImageEvent = ResourceManager.LoadImageAsync("EVENTLABYRINTH");
		loadInstanceList.Add(resImageEvent);
		loadInstanceList.Add(ResourceManager.LoadImageAsync("EVENTLABYRINTH_REWARD"));
		loadInstanceList.Add(ResourceManager.LoadSsAnimationAsync(RewardItemGetAnimationFormat));
		loadInstanceList.Add(ResourceManager.LoadSsAnimationAsync(GoalAnimationNameFormat));
		loadInstanceList.Add(ResourceManager.LoadSsAnimationAsync(RewardFixedSpriteAnimationFormat));
		loadInstanceList.Add(ResourceManager.LoadSsAnimationAsync(RewardBoxAnimationFormat));
		loadInstanceList.Add(ResourceManager.LoadSsAnimationAsync("LABYRINTH_POPUP_LANDSCAPE"));
		loadInstanceList.Add(ResourceManager.LoadSsAnimationAsync("LABYRINTH_POPUP_PORTRAIT"));
		int eventID = mGame.mPlayer.Data.CurrentEventID;
		SMSeasonEventSetting eventsetting = mGame.mEventData.InSessionEventList[eventID];
		ResImage bannerAtlas = ResourceManager.LoadImageAsync(eventsetting.BannerAtlas);
		loadInstanceList.Add(bannerAtlas);
		while (true)
		{
			bool f = true;
			for (int i = 0; i < loadInstanceList.Count; i++)
			{
				if (loadInstanceList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					f = false;
					break;
				}
			}
			if (f)
			{
				break;
			}
			yield return null;
		}
		mPageMoveLock = Def.PRESS.NONE;
		mPageMoved = false;
		SetScrollPower(0f);
		if (mCockpit == null)
		{
			mCockpit = Util.CreateGameObject("EventLabyrinthCockpit", base.gameObject).AddComponent<EventLabyrinthCockpit>();
			if (mState.GetStatus() == STATE.MAIN)
			{
				mCockpit.Init(this);
			}
			else
			{
				mCockpit.Init(this, false);
			}
			mCockpit.SetCallback(OnCockpitOpend, OnCockpitClosed);
		}
		IsInCourseSelect = CurrentCourseID < 0;
		if (IsInCourseSelect)
		{
			yield return StartCoroutine(LoadCourseSelect());
		}
		else
		{
			yield return StartCoroutine(LoadStageSelect());
		}
		mCurrentPage.SetEnableButton(false);
		NGUITools.SetActive(mCurrentPage.gameObject, false);
		float x = 0f;
		float y = mCurrentPage.gameObject.transform.localPosition.y;
		float z = mCurrentPage.gameObject.transform.localPosition.z;
		mCurrentPage.gameObject.transform.localPosition = new Vector3(x, y, z);
		yield return null;
		NGUITools.SetActive(mCurrentPage.gameObject, true);
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		ResSsAnimation anim = ResourceManager.LoadSsAnimationAsync("EFFECT_SHINE", true);
		UIFont font = GameMain.LoadFont();
		mGame.mUseBooster0 = false;
		mGame.mUseBooster1 = false;
		mGame.mUseBooster2 = false;
		mGame.mUseBooster3 = false;
		mGame.mUseBooster4 = false;
		mGame.mUseBooster5 = false;
		SetDeviceScreen(Util.ScreenOrientation);
		float cameraY = ClearedStagePosition();
		yield return StartCoroutine(DirectMapMove(0f - cameraY));
		UpdateMapPages();
		SetLayout(Util.ScreenOrientation);
		UpdateMapPages();
		CreateGetReward();
		if (!mIsPlayingBGM && mShowStoryDemoList.Count == 0 && resSound.mAudioClip != null)
		{
			mGame.PlayMusic(DEFAULT_EVENT_BGM, 0, true, false, true);
			mGame.SetMusicVolume(1f, 0);
			mIsPlayingBGM = true;
		}
		yield return Resources.UnloadUnusedAssets();
		ResourceManager.EnableLoadAllResourceFromAssetBundle();
		LoadGameStateFinished();
	}

	public override IEnumerator UnloadGameState()
	{
		if (mTipsScreenDisplayed)
		{
			mGameState.TipsScreenOff();
		}
		else
		{
			Server.ManualConnecting = false;
		}
		ResourceManager.UnloadSound(DEFAULT_EVENT_BGM);
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		if (mCockpit != null)
		{
			GameMain.SafeDestroy(mCockpit.gameObject);
			mCockpit = null;
		}
		yield return StartCoroutine(mCurrentPage.DeactiveAllObject());
		UnityEngine.Object.Destroy(mAnchorBottom.gameObject);
		mAnchorBottom = null;
		UnityEngine.Object.Destroy(mAnchorRight.gameObject);
		mAnchorRight = null;
		UnityEngine.Object.Destroy(mAnchorCenter.gameObject);
		mAnchorCenter = null;
		UnityEngine.Object.Destroy(mCurrentPage.gameObject);
		UnityEngine.Object.Destroy(mRoot);
		ResourceManager.UnloadSsAnimationAll();
		ResourceManager.UnloadImageAll();
		mGame.UnloadNotPreloadSoundResources(true);
		UnloadGameStateFinished();
		yield return 0;
	}

	private bool Labyrinth_UnlockActing()
	{
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		bool flag = false;
		if (GameMain.USE_DEBUG_DIALOG)
		{
			GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(CurrentEventID);
			if (labyrinthData.DoCreateReward)
			{
				CreateGetReward();
				labyrinthData.DoCreateReward = false;
			}
			GlobalVariables.Instance.SetLabyrinthData(CurrentEventID, labyrinthData);
		}
		if (mGetAccessoryDataList.Count > 0)
		{
			StartCoroutine(GrayOut());
			List<AccessoryData> list = new List<AccessoryData>();
			for (int i = 0; i < 4; i++)
			{
				if (mGetAccessoryDataList.Count == 0)
				{
					break;
				}
				list.Add(mGetAccessoryDataList[0]);
				mGetAccessoryDataList.RemoveAt(0);
			}
			GetRewardDialog getRewardDialog = Util.CreateGameObject("GetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
			getRewardDialog.Init(list);
			getRewardDialog.SetClosedCallback(OnGetRewardAccessoryDialogClosed);
			flag = true;
		}
		else if (mGetGemAccessoryDataList.Count > 0)
		{
			StartCoroutine(GrayOut());
			List<AccessoryData> list2 = new List<AccessoryData>();
			for (int j = 0; j < 4; j++)
			{
				if (mGetGemAccessoryDataList.Count == 0)
				{
					break;
				}
				list2.Add(mGetGemAccessoryDataList[0]);
				mGetGemAccessoryDataList.RemoveAt(0);
			}
			GetRewardDialog getRewardDialog2 = Util.CreateGameObject("GetRewardDialog", mRoot).AddComponent<GetRewardDialog>();
			getRewardDialog2.Init(list2);
			getRewardDialog2.SetClosedCallback(OnGetRewardGemDialogClosed);
			flag = true;
		}
		else if (mGetPartnerAccessoryDataList.Count > 0)
		{
			StartCoroutine(GetPartnerProcess(mGetPartnerAccessoryDataList[0]));
			mGetPartnerAccessoryDataList.RemoveAt(0);
			flag = true;
		}
		else if (mGetKeyAccessoryDataList.Count > 0)
		{
			InitOpenCourse();
			if (IsNewCourseUnlocked)
			{
				StartCoroutine(GrayOut());
				mEventImageDialog = Util.CreateGameObject("EventExplainDialog", mRoot).AddComponent<EventImageDialog>();
				mEventImageDialog.Init("howto_ps06", Localization.Get("GetLabyrinthKey_Title"), Localization.Get("GetLabyrinthKey_Desc"), "EVENTLABYRINTH", 75f);
				mEventImageDialog.SetButtonImageName("LC_button_ok2");
				mEventImageDialog.SetClosedCallback(delegate
				{
					mEventImageDialog = null;
					StartCoroutine(GrayIn());
					mState.Change(STATE.UNLOCK_ACTING);
				});
				flag = true;
			}
			mGetKeyAccessoryDataList.RemoveAll((AccessoryData a) => a != null);
			mGetKeyAccessoryDataList.Clear();
		}
		if (flag)
		{
			return flag;
		}
		List<short> list3 = null;
		if (IsInCourseSelect && IsNewCourseUnlocked)
		{
			list3 = new List<short>();
			mNewCourseNotifyList.Sort();
			for (int k = 0; k < mNewCourseNotifyList.Count; k++)
			{
				if (mNewCourseNotifyList[k] > 0)
				{
					list3.Add(mNewCourseNotifyList[k]);
				}
			}
			mNewCourseNotifyList.Clear();
			if (mGetKeyAccessoryDataList.Count > 0)
			{
				mGetKeyAccessoryDataList.RemoveAll((AccessoryData a) => a != null);
				mGetKeyAccessoryDataList.Clear();
			}
		}
		if (list3 != null && list3.Count > 0)
		{
			StartCoroutine(UnlockCourseProcess(list3));
			flag = true;
		}
		if (flag)
		{
			return flag;
		}
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		if (!data.HasShownNotice(104))
		{
			bool flag2 = true;
			List<SMEventRewardSetting> list4 = new List<SMEventRewardSetting>(sMEventPageData.EventRewardList.Values);
			list4.Sort((SMEventRewardSetting a, SMEventRewardSetting b) => b.TotalNum - a.TotalNum);
			AccessoryData a_data = null;
			for (int l = 0; l < list4.Count; l++)
			{
				if (!mGame.mPlayer.IsAccessoryUnlock(list4[l].RewardID))
				{
					flag2 = false;
					break;
				}
			}
			if (flag2)
			{
				data.ShowNotice(104);
				mGame.IsPlayerSaveRequest = true;
				for (int m = 0; m < list4.Count; m++)
				{
					AccessoryData accessoryData = mGame.GetAccessoryData(list4[m].RewardID);
					if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
					{
						a_data = accessoryData;
						break;
					}
				}
				StartCoroutine(RewardCompleteProcess(a_data));
				flag = true;
			}
		}
		if (flag)
		{
			return flag;
		}
		return flag;
	}

	private void OnGetRewardAccessoryDialogClosed()
	{
		if (mGetAccessoryDataList.Count == 0)
		{
			mGetAccessoryDataList.Clear();
			StartCoroutine(GrayIn());
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private void OnGetRewardGemDialogClosed()
	{
		if (mGetGemAccessoryDataList.Count == 0)
		{
			mGetGemAccessoryDataList.Clear();
			StartCoroutine(GrayIn());
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private void OnGetRewardKeyDialogClosed()
	{
		if (mGetKeyAccessoryDataList.Count == 0)
		{
			mGetKeyAccessoryDataList.Clear();
			StartCoroutine(GrayIn());
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private IEnumerator GetPartnerProcess(AccessoryData a_data)
	{
		EventCockpit eventCockpit = mCockpit as EventCockpit;
		yield return StartCoroutine(eventCockpit.JumpUpLive2dCharacter(CompanionData.MOTION.JUMP));
		StartCoroutine(GrayOut());
		bool dialogOpen3 = true;
		mGetPartnerDialog = Util.CreateGameObject("GetPartnerDialog", mRoot).AddComponent<GetPartnerDialog>();
		mGetPartnerDialog.Init(a_data, GetPartnerDialog.PLACE.MAIN, false, true);
		mGetPartnerDialog.SetClosedCallback(delegate
		{
			dialogOpen3 = false;
		});
		while (dialogOpen3)
		{
			yield return null;
		}
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		int introAccessory = -1;
		int reward_key = 0;
		List<int> keys = new List<int>(eventPageData.EventRewardList.Keys);
		for (int i = 0; i < keys.Count; i++)
		{
			int key = keys[i];
			SMEventRewardSetting eventRewardData = eventPageData.EventRewardList[key];
			AccessoryData accessoryData = mGame.GetAccessoryData(eventRewardData.RewardID);
			if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER && !mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index))
			{
				introAccessory = accessoryData.Index;
				reward_key = key;
				break;
			}
		}
		UnityEngine.Object.Destroy(mGetPartnerDialog.gameObject);
		mGetPartnerDialog = null;
		dialogOpen3 = false;
		if (introAccessory != -1)
		{
			dialogOpen3 = true;
			mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
			mEventIntroCharacterDialog.Init(introAccessory, Def.EVENT_TYPE.SM_LABYRINTH, reward_key, true);
			mEventIntroCharacterDialog.SetClosedCallback(delegate
			{
				dialogOpen3 = false;
			});
			while (dialogOpen3)
			{
				yield return null;
			}
			mEventIntroCharacterDialog = null;
		}
		StartCoroutine(GrayIn());
		yield return StartCoroutine(eventCockpit.JumpDownLive2dCharacter(CompanionData.MOTION.LANDING));
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private IEnumerator RewardCompleteProcess(AccessoryData a_data)
	{
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		yield return StartCoroutine(PlayClearAnimation(ANIME_TYPE.EVENT_COMP));
		bool dialogOpen2 = true;
		StartCoroutine(GrayOut());
		EventCompDialog compDialog = Util.CreateGameObject("EventCompleteDialog", mRoot).AddComponent<EventCompDialog>();
		compDialog.Init(a_data.GetCompanionID(), eventPageData.EventSetting.HudAtlasKey, eventPageData.EventSetting.EventType);
		compDialog.SetClosedCallback(delegate
		{
			UpdateCockpit(false);
			UnityEngine.Object.Destroy(compDialog.gameObject);
			dialogOpen2 = false;
		});
		while (dialogOpen2)
		{
			yield return null;
		}
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out eventData);
		bool isStarComplete = true;
		foreach (KeyValuePair<int, SMEventCourseSetting> kv in eventPageData.EventCourseList)
		{
			short stageClear;
			short starGet;
			eventData.GetEventClearStarCount((short)kv.Key, out stageClear, out starGet);
			if (starGet < 27)
			{
				isStarComplete = false;
				break;
			}
		}
		if (!isStarComplete)
		{
			dialogOpen2 = true;
			mEventImageDialog = Util.CreateGameObject("EventExplainDialog", mRoot).AddComponent<EventImageDialog>();
			mEventImageDialog.Init("howto_ps00", Localization.Get("Labyrinth_StarComp_Title"), Localization.Get("Labyrinth_StarComp_Desc"), "EVENTLABYRINTH");
			mEventImageDialog.SetClosedCallback(delegate
			{
				mEventImageDialog = null;
				dialogOpen2 = false;
			});
			while (dialogOpen2)
			{
				yield return null;
			}
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected bool Update_Common_UNLOCK_ACTING()
	{
		bool result = false;
		if (mGame.DoLoginBonusServerCheck)
		{
			mState.Reset(STATE.NETWORK_RESUME_MAINTENACE, true);
			result = true;
		}
		else if (AccessoryConnectCheck())
		{
			mState.Reset(STATE.NETWORK_REWARD_MAINTENACE, true);
			result = true;
		}
		else if (mAdvAutoUnlockAccessory == null && AdvAccessoryConnectCheck())
		{
			mState.Reset(STATE.NETWORK_ADV_REWARD_SET, true);
			result = true;
		}
		else if (mAdvAutoUnlockAccessory != null)
		{
			mState.Reset(STATE.NETWORK_ADV_GETREWARDINFO, true);
			result = true;
		}
		else if (mAutoUnlockAccessory != null)
		{
			StartCoroutine(GrayOut());
			if (mAutoUnlockAccessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
			{
				List<AccessoryData> list = new List<AccessoryData>();
				list.Add(mAutoUnlockAccessory);
				GetRewardDialog getRewardDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetRewardDialog>();
				getRewardDialog.InitUseBundle(list);
				getRewardDialog.SetClosedCallback(delegate
				{
					StartCoroutine(GrayIn());
					mState.Change(STATE.UNLOCK_ACTING);
				});
				mState.Reset(STATE.WAIT, true);
			}
			else if (mAutoUnlockAccessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
			{
				GetAccessoryProcess(mAutoUnlockAccessory, false);
				if (mGame.SetPartnerCollectionAttention(mAutoUnlockAccessory))
				{
					mCockpit.SetEnableMenuButtonAttention(true);
				}
			}
			else
			{
				GetAccessoryProcess(mAutoUnlockAccessory, false);
				mGame.mPlayer.SetCollectionNotify(mAutoUnlockAccessory.Index);
				mCockpit.SetEnableMenuButtonAttention(true);
			}
			mAutoUnlockAccessory = null;
			result = true;
		}
		else if (mAlterUnlockAccessory.Count > 0)
		{
			StartCoroutine(GrayOut());
			mAlternateRewardDialog = Util.CreateGameObject("AlterRewardDialog", mRoot).AddComponent<AlternateRewardDialog>();
			mAlternateRewardDialog.Init(mAlterUnlockAccessory);
			mAlternateRewardDialog.SetClosedCallback(delegate
			{
				UnityEngine.Object.Destroy(mAlternateRewardDialog.gameObject);
				mAlternateRewardDialog = null;
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			});
			mAlterUnlockAccessory.Clear();
			mState.Reset(STATE.WAIT, true);
			result = true;
		}
		else if (Labyrinth_UnlockActing())
		{
			mState.Reset(STATE.WAIT, true);
			result = true;
		}
		else if (mGame.mPlayer.HeartUpPiece >= 15)
		{
			mGame.mPlayer.SubHeartPiece(15);
			GiftItem giftItem = new GiftItem();
			giftItem.Data.Message = Localization.Get("Gift_RatingReward");
			giftItem.Data.ItemCategory = 1;
			giftItem.Data.ItemKind = 1;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = 1;
			mGame.mPlayer.AddGift(giftItem);
			ServerCram.ArcadeGetItem(1, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 2, 1, 30, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
			mGame.IsPlayerSaveRequest = true;
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init("icon_heart", Localization.Get("GetAccessory_Item_Title"), string.Format(Localization.Get("GetAccessory_HEART_Desc"), 1));
			mGetAccessoryDialog.SetClosedCallback(OnGetHeartPieaceDialogClosed);
			mState.Reset(STATE.WAIT, true);
			result = true;
		}
		else if (mLoginBonusScreen == null && mGame.mLoginBonusData != null && mGame.mLoginBonusData.HasLoginData)
		{
			GetLoginBonusResponse loginBonusByIndex = mGame.mLoginBonusData.GetLoginBonusByIndex(0);
			if (loginBonusByIndex.IsDislpay)
			{
				mLoginBonusScreen = Util.CreateGameObject("LoginBonusScreen", mRoot).AddComponent<LoginBonusScreen>();
				mLoginBonusScreen.init(loginBonusByIndex, delegate
				{
					mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
					mState.Reset(STATE.UNLOCK_ACTING, true);
					mLoginBonusScreen = null;
				});
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mGame.mLoginBonusData.RemoveLoginBonusDataByIndex(0);
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
			result = true;
		}
		return result;
	}

	protected IEnumerator LoadCourseSelect()
	{
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		Player p = mGame.mPlayer;
		Def.SERIES currentSeries = p.Data.CurrentSeries;
		ResImage resMapImage = ResourceManager.LoadImageAsync(eventPageData.EventSetting.MapAtlasKey);
		while (resMapImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		short courseID = 0;
		SMMapSetting data = eventPageData.GetMap(courseID);
		SMEventLabyrinthPageA page = Util.CreateGameObject("NewMapPageA_" + courseID, mMapPageRoot).AddComponent<SMEventLabyrinthPageA>();
		page.SetGameState(this);
		InitOpenCourse();
		yield return StartCoroutine(page.Init(currentSeries, CurrentEventID, courseID, data.MaxLevel, OnCourseButtonPushed, null, null, null, null, eventPageData));
		Map_LimitOffset = 300f;
		EventLabyrinthCockpit labyrinthCockpit = mCockpit as EventLabyrinthCockpit;
		if (labyrinthCockpit != null)
		{
			labyrinthCockpit.SetCourseSelectMode();
		}
		mCurrentPage = page;
		mMaxPage = mCurrentPage.MaxPage;
		mMaxAvailablePage = mCurrentPage.MaxAvailablePage;
		mMaxModulePage = mCurrentPage.MaxAvailablePage;
	}

	protected override void Update_INIT()
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void Update_UNLOCK_ACTING()
	{
		Player mPlayer = mGame.mPlayer;
		if (StoryDemoStartCheck())
		{
			mCurrentStoryDemo = mShowStoryDemoList[0];
			mShowStoryDemoList.RemoveAt(0);
			if (mCurrentStoryDemo.DemoIndex != -1)
			{
				StartStoryDemo(mCurrentStoryDemo.DemoIndex);
			}
		}
		else if (TutorialStartCheckOnInit())
		{
			mState.Change(STATE.TUTORIAL);
		}
		else if (mAdvGuideDialogFlg)
		{
			mAdvGuideDialogFlg = false;
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			StartCoroutine(GrayOut());
			EvenAdvGuideDialog evenAdvGuideDialog = Util.CreateGameObject("EvenAdvGuideDialog", mRoot).AddComponent<EvenAdvGuideDialog>();
			evenAdvGuideDialog.Init("LC_howtogesen", Localization.Get("EvMap_GameCenter_Title"), Localization.Get("EvMap_GameCenter_Desc00"), Localization.Get("EvMap_GameCenter_Desc01"), sMEventPageData.EventSetting.MapAtlasKey);
			evenAdvGuideDialog.SetClosedCallback(OnDialogClosedToUnlockActing);
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	protected override void Update_MAIN()
	{
		MapMove();
		if (mGame.mTouchDownTrg)
		{
			if (mCurrentPage != null)
			{
				Vector3 position = new Vector3(mGame.mTouchPos.x, mGame.mTouchPos.y, 0f);
				mPageMoveLock = mCurrentPage.OnScreenPress(mCamera.ScreenToWorldPoint(position));
			}
		}
		else if (mGame.mBackKeyTrg)
		{
			if (GameMain.USE_DEBUG_DIALOG && GameMain.HIDDEN_DEBUG_BUTTON)
			{
				OnDebugPushed();
			}
			else
			{
				OnBackKeyPushed();
			}
		}
	}

	protected override void MapMove()
	{
		if (mGame.mTouchDownTrg)
		{
			mDownPos = mGame.mTouchPos;
		}
		if (mPageMoveLock != Def.PRESS.HIT)
		{
			if (mGame.mTouchWasDrag && !mPageMoved)
			{
				Vector3 vector = mCamera.ScreenToWorldPoint(new Vector3(0f, mGame.mTouchPos.y));
				Vector3 vector2 = mCamera.ScreenToWorldPoint(new Vector3(0f, mGame.mTouchPosOld.y));
				MoveVector = (vector - vector2) * Map_DeviceScreenSizeY / 2f;
				mScrollPowerYTemp += MoveVector.y;
				mPageMoveBacking = false;
				if (Mathf.Abs(mScrollPowerYTemp) > GameStateSMMapBase.s_ScrollThresholdY)
				{
					float num = Vector2.Dot(MoveVector, MoveVectorOld);
					if (num < 0f)
					{
						mScrollPowerYTemp = mScrollPowerY / 5f;
					}
					if (Mathf.Abs(mScrollPowerYTemp) > 75f)
					{
						mScrollPowerYTemp = ((!(mScrollPowerYTemp < 0f)) ? 75f : (-75f));
					}
					DragPower = new Vector2(0f, mScrollPowerYTemp);
					SetScrollPower(mScrollPowerYTemp);
					mScrollPowerYTemp = 0f;
					MoveVectorOld = MoveVector;
				}
			}
			else
			{
				MoveVectorOld = Vector2.zero;
			}
		}
		if (!mGame.mTouchCnd)
		{
			mPageMoved = false;
			mPageMoveLock = Def.PRESS.NONE;
		}
	}

	public override void UpdateMapPages()
	{
		base.UpdateMapPages();
		if (!IsInCourseSelect && mBGImage != null && mCurrentPage != null)
		{
			mBGImage.transform.localPosition = new Vector3(mBGImage.transform.localPosition.x, mCurrentPage.gameObject.transform.localPosition.y, mBGImage.transform.localPosition.z);
		}
	}

	private IEnumerator UnlockCourseProcess(List<short> a_unlockList)
	{
		short prev_courseID = -1;
		SMEventLabyrinthPageA coursePage = mCurrentPage as SMEventLabyrinthPageA;
		Player p = mGame.mPlayer;
		PlayerEventData eventData;
		p.Data.GetMapData(p.Data.CurrentSeries, CurrentEventID, out eventData);
		for (int i = 0; i < a_unlockList.Count; i++)
		{
			short unlockCourseID = a_unlockList[i];
			float targetPos2 = coursePage.GetStageWorldPos(unlockCourseID);
			targetPos2 += -100f;
			float limit = GetLimitMapPageY();
			if (targetPos2 > limit)
			{
				targetPos2 = limit;
			}
			yield return StartCoroutine(SlideMapMove(0f - targetPos2));
			yield return new WaitForSeconds(0.3f);
			yield return StartCoroutine(coursePage.UnlockCourse(unlockCourseID));
			int main;
			int sub;
			Def.SplitStageNo(unlockCourseID, out main, out sub);
			int prev_main = main - 1;
			int prev_stageno = Def.GetStageNo(prev_main, 0);
			eventData.LabyrinthData.CourseStageStatus[unlockCourseID] = Player.STAGE_STATUS.UNLOCK;
			eventData.LabyrinthData.CourseLineStatus[(short)prev_stageno] = Player.STAGE_STATUS.UNLOCK;
		}
		yield return new WaitForSeconds(0.5f);
		mGame.IsPlayerSaveRequest = true;
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected IEnumerator LoadStageSelect()
	{
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		Player p = mGame.mPlayer;
		Def.SERIES currentSeries = p.Data.CurrentSeries;
		BIJImage atlasBG = ResourceManager.LoadImage(eventPageData.EventSetting.MapAtlasKey + "_BG").Image;
		mBGImage = Util.CreateGameObject("EventBG", mMapPageRoot).AddComponent<SMEventBG>();
		string backname = "event_map_course";
		mBGImage.Active(atlasBG, backname, new Vector3(0f, 0f, Def.MAP_BASE_Z), new Vector2(2f, 2f));
		ResImage resMapImage = ResourceManager.LoadImageAsync(eventPageData.EventSetting.MapAtlasKey + "_IN");
		while (resMapImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		SMEventLabyrinthPageB page = Util.CreateGameObject("NewMapPageB_" + CurrentCourseID, mMapPageRoot).AddComponent<SMEventLabyrinthPageB>();
		page.SetGameState(this);
		InitOpenCourse();
		yield return StartCoroutine(page.Init(currentSeries, CurrentEventID, CurrentCourseID, 9, OnStageButtonPushed, null, null, null, null, eventPageData));
		Map_LimitOffset = 0f;
		EventLabyrinthCockpit labyrinthCockpit = mCockpit as EventLabyrinthCockpit;
		if (labyrinthCockpit != null)
		{
			bool enableReset = true;
			if (page.AvaterPosition == 10000 || (page.GoalButton != null && page.GoalButton.mMode == Player.STAGE_STATUS.UNLOCK))
			{
				enableReset = false;
			}
			labyrinthCockpit.SetStageSelectMode(enableReset);
		}
		mCurrentPage = page;
		mMaxPage = mCurrentPage.MaxPage;
		mMaxAvailablePage = mCurrentPage.MaxAvailablePage;
		mMaxModulePage = mCurrentPage.MaxAvailablePage;
	}

	private void Update_IN_INIT()
	{
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, CurrentEventID, out data);
		UpdateCockpit(true);
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(CurrentEventID);
		bool flag = false;
		bool a_isClear;
		bool a_isComplete;
		data.CheckEventStageClearReward(sMEventPageData.EventSetting.EventType, CurrentCourseID, Def.LabyrinthStageList, out a_isClear, out a_isComplete);
		if (a_isClear)
		{
			data.UpdateCourseClear(CurrentCourseID, true);
			int courseStagesAllClearRewardNum = sMEventPageData.EventCourseList[CurrentCourseID].CourseStagesAllClearRewardNum;
			mGame.AddEventPoint(CurrentEventID, courseStagesAllClearRewardNum);
			if (labyrinthData.CourseStagesAllClearList == null)
			{
				labyrinthData.CourseStagesAllClearList = new Dictionary<short, int>();
			}
			if (!labyrinthData.CourseStagesAllClearList.ContainsKey(CurrentCourseID))
			{
				labyrinthData.CourseStagesAllClearList.Add(CurrentCourseID, courseStagesAllClearRewardNum);
				flag = true;
			}
			ServerCram.EventLabyrinthGetJewel(CurrentCourseID, (int)mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.LastPlayLevel, CurrentEventID, 4, courseStagesAllClearRewardNum, 0, 0, 0, 0, 0, 0, courseStagesAllClearRewardNum, data.LabyrinthData.JewelAmount);
		}
		if (a_isComplete)
		{
			data.UpdateCourseComplete(CurrentCourseID, true);
			int courseStarCompleteAccessoryID = sMEventPageData.EventCourseList[CurrentCourseID].CourseStarCompleteAccessoryID;
			if (courseStarCompleteAccessoryID > 0)
			{
				AccessoryData accessoryData = mGame.GetAccessoryData(courseStarCompleteAccessoryID);
				if (!mGame.mPlayer.IsAccessoryUnlock(courseStarCompleteAccessoryID))
				{
					mGame.AddAccessoryConnect(courseStarCompleteAccessoryID);
					if (labyrinthData.CourseStarCompleteList == null)
					{
						labyrinthData.CourseStarCompleteList = new Dictionary<short, int>();
					}
					if (!labyrinthData.CourseStarCompleteList.ContainsKey(CurrentCourseID))
					{
						labyrinthData.CourseStarCompleteList.Add(CurrentCourseID, courseStarCompleteAccessoryID);
						flag = true;
					}
				}
			}
		}
		if (flag)
		{
			GlobalVariables.Instance.SetLabyrinthData(CurrentEventID, labyrinthData);
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void Update_IN_UNLOCK_ACTING()
	{
		SMEventLabyrinthPageB sMEventLabyrinthPageB = mCurrentPage as SMEventLabyrinthPageB;
		if (sMEventLabyrinthPageB == null)
		{
			BIJLog.E("ERROR!!!!\nCurrentPage is Invalid!!!");
			mState.Change(STATE.MAIN);
			return;
		}
		GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(CurrentEventID);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		if (sMEventLabyrinthPageB.IsAvatarHide)
		{
			StartCoroutine(AvatarAppearProcess());
			mState.Reset(STATE.WAIT);
		}
		else if (!data.HasShownNotice(101))
		{
			data.ShowNotice(101);
			mGame.IsPlayerSaveRequest = true;
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mEventImageDialog = Util.CreateGameObject("EventExplainDialog", mRoot).AddComponent<EventImageDialog>();
			mEventImageDialog.Init("howto_ps02", Localization.Get("Labyrinth_Guid03_Title"), Localization.Get("Labyrinth_Guid03_Desc"), "EVENTLABYRINTH");
			mEventImageDialog.SetClosedCallback(delegate
			{
				mEventImageDialog = null;
				OnDialogClosedToUnlockActing();
			});
		}
		else if (mCourseClearRewardNum > 0)
		{
			mCourseClearRewardNum = 0;
			if (IsNewCourseUnlocked)
			{
				StartCoroutine(GrayOut());
				mState.Reset(STATE.WAIT, true);
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("Labyrinth_Course_Loop_Desc_00"), CurrentCourseID + 1);
				mConfirmDialog.Init(Localization.Get("Labyrinth_Course_Loop_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnRestartCourseResult);
			}
			else
			{
				mIsCourseRestart = true;
				OnRestartCourseConfirm(ConfirmDialog.SELECT_ITEM.POSITIVE);
			}
			int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
			int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
			short a_course;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(lastPlayLevel, out a_course, out a_main, out a_sub);
			int evt_id = ((!mGame.mEventMode) ? 9999 : mGame.mPlayer.Data.CurrentEventID);
			ServerCram.EventLabyrinthCourseReset(CurrentCourseID, currentSeries, lastPlayLevel, evt_id, 0, data.LabyrinthData.JewelAmount);
		}
		else if (mIsCourseRestart && !data.HasShownNotice(103))
		{
			mIsCourseRestart = false;
			data.ShowNotice(103);
			mGame.IsPlayerSaveRequest = true;
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mEventImageDialog = Util.CreateGameObject("EventExplainDialog", mRoot).AddComponent<EventImageDialog>();
			mEventImageDialog.Init("howto_ps02", Localization.Get("Labyrinth_Guid01_Title"), Localization.Get("Labyrinth_Guid01_Desc"), "EVENTLABYRINTH", 80f);
			mEventImageDialog.SetClosedCallback(delegate
			{
				mEventImageDialog = null;
				OnDialogClosedToUnlockActing();
			});
		}
		else if (mGame.GetPresentBoxNum() > 0)
		{
			StartCoroutine(GrayOut());
			mOpenPuzzleAccessory = mGame.GetPresentBox(0);
			int num = mGame.GetPresentBoxNum();
			if (mOpenPuzzleAccessory != null)
			{
				if (num > 1)
				{
					while (num > 1)
					{
						mOtherOpenPuzzleAccessory = mGame.GetPresentBox(num - 1);
						if (mOpenPuzzleAccessory.AccessoryType == mOtherOpenPuzzleAccessory.AccessoryType)
						{
							mBundleAccessoryCount++;
							mGame.RemovePresentBox(mOtherOpenPuzzleAccessory);
						}
						num--;
					}
					mGame.RemovePresentBox(mOpenPuzzleAccessory);
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.Init(mOpenPuzzleAccessory, mBundleAccessoryCount, false);
					mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
					mBundleAccessoryCount = 0;
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.RemovePresentBox(mOpenPuzzleAccessory);
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.Init(mOpenPuzzleAccessory, mBundleAccessoryCount, false);
					mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
					mState.Change(STATE.WAIT);
				}
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
		}
		else if (labyrinthData.CourseStagesAllClearList != null && labyrinthData.CourseStagesAllClearList.ContainsKey(CurrentCourseID))
		{
			int a_amount = labyrinthData.CourseStagesAllClearList[CurrentCourseID];
			labyrinthData.CourseStagesAllClearList.Remove(CurrentCourseID);
			GlobalVariables.Instance.SetLabyrinthData(CurrentEventID, labyrinthData);
			StartCoroutine(CourseStageCompleteProcess(a_amount));
			mState.Reset(STATE.WAIT, true);
		}
		else if (sMEventLabyrinthPageB.IsLineUnlockEffect)
		{
			StartCoroutine(UnlockStageProcess());
			mState.Reset(STATE.WAIT, true);
		}
		else if (labyrinthData.CourseStarCompleteList != null && labyrinthData.CourseStarCompleteList.ContainsKey(CurrentCourseID))
		{
			int a_index = labyrinthData.CourseStarCompleteList[CurrentCourseID];
			labyrinthData.CourseStarCompleteList.Remove(CurrentCourseID);
			GlobalVariables.Instance.SetLabyrinthData(CurrentEventID, labyrinthData);
			AccessoryData accessoryData = mGame.GetAccessoryData(a_index);
			StartCoroutine(StarCompleteProcess(accessoryData));
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			if (mIsCourseRestart && data.HasShownNotice(103))
			{
				mIsCourseRestart = false;
			}
			mState.Change(STATE.MAIN);
		}
	}

	protected void Update_IN_MAIN()
	{
		MapMove();
		if (mGame.mTouchDownTrg)
		{
			if (mCurrentPage != null)
			{
				Vector3 position = new Vector3(mGame.mTouchPos.x, mGame.mTouchPos.y, 0f);
				mPageMoveLock = mCurrentPage.OnScreenPress(mCamera.ScreenToWorldPoint(position));
			}
		}
		else if (mGame.mBackKeyTrg)
		{
			if (GameMain.USE_DEBUG_DIALOG && GameMain.HIDDEN_DEBUG_BUTTON)
			{
				OnDebugPushed();
			}
			else
			{
				OnBackKeyPushed();
			}
		}
	}

	protected float AvatarPosition(short a_courseID)
	{
		float num = 0f;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		PlayerMapData playerMapData = data.CourseData[a_courseID];
		int lastClearedLevel = playerMapData.LastClearedLevel;
		int playableMaxLevel = playerMapData.PlayableMaxLevel;
		if (lastClearedLevel > 0)
		{
			num = mCurrentPage.GetStageWorldPos(lastClearedLevel);
			mCurrentPage.SetAvatarPositionOnStage(lastClearedLevel);
		}
		else
		{
			float mValue = mCurrentPage.mAvater.GetCurrentNode().mValue;
			lastClearedLevel = Def.GetStageNoByRouteOrder(mValue);
			num = mCurrentPage.GetStageWorldPos(lastClearedLevel);
		}
		float y = mCurrentPage.transform.localPosition.y;
		if (SetDeviceScreen(Util.ScreenOrientation) && num > y)
		{
			num += 80f;
		}
		float map_DeviceOffset = Map_DeviceOffset;
		float num2 = 0f - (1136f * (float)(mMaxModulePage - 1) + map_DeviceOffset);
		if (num > map_DeviceOffset)
		{
			num = map_DeviceOffset;
		}
		else if (num < num2)
		{
			num = num2;
		}
		return 0f - num;
	}

	private IEnumerator AvatarAppearProcess()
	{
		SMEventLabyrinthPageB pageB = mCurrentPage as SMEventLabyrinthPageB;
		yield return StartCoroutine(pageB.AvatarWarpInInit());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private IEnumerator ClearDialogProcess(int a_amount, LabyrinthResultDialog.DISPLAY_TYPE a_displayType, STATE a_next = STATE.NONE)
	{
		SMEventLabyrinthPageB pageB = mCurrentPage as SMEventLabyrinthPageB;
		if (pageB == null)
		{
			yield break;
		}
		string dialogTitle = string.Format(Localization.Get("Labyrinth_Course_Clear_Title"), CurrentCourseID + 1);
		string dialogDesc = string.Format(Localization.Get("Labyrinth_Get_Jwel_Desc"), a_amount);
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out eventData);
		bool dialogOpen = true;
		StartCoroutine(GrayOut());
		mLabyrinthResultDialog = Util.CreateGameObject("LabyrinthResultDialog", mRoot).AddComponent<LabyrinthResultDialog>();
		mLabyrinthResultDialog.SetCallback(LabyrinthResultDialog.SELECT_ITEM.OnClosed, delegate
		{
			dialogOpen = false;
		});
		mLabyrinthResultDialog.SetCallback(LabyrinthResultDialog.SELECT_ITEM.OnUpdatePossess, delegate
		{
			if (mCockpit is EventLabyrinthCockpit)
			{
				(mCockpit as EventLabyrinthCockpit).TriggerUpdateJewel();
			}
		});
		mLabyrinthResultDialog.Init(a_amount, eventData.LabyrinthData.JewelAmount - a_amount, this, a_displayType);
		while (dialogOpen)
		{
			yield return null;
		}
		UnityEngine.Object.Destroy(mLabyrinthResultDialog.gameObject);
		mLabyrinthResultDialog = null;
		StartCoroutine(GrayIn());
		if (mCockpit is EventLabyrinthCockpit)
		{
			(mCockpit as EventLabyrinthCockpit).TriggerUpdateJewel();
		}
		CreateGetReward();
		yield return new WaitForSeconds(0.5f);
		if (a_next != 0)
		{
			mState.Change(a_next);
		}
	}

	private IEnumerator UnlockStageProcess()
	{
		SMEventLabyrinthPageB pageB = mCurrentPage as SMEventLabyrinthPageB;
		if (!(pageB == null))
		{
			yield return StartCoroutine(pageB.UnlockStage());
			yield return new WaitForSeconds(0.5f);
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	private IEnumerator CourseStageCompleteProcess(int a_amount)
	{
		yield return StartCoroutine(PlayClearAnimation(ANIME_TYPE.COURSESTAGE_COMP));
		yield return new WaitForSeconds(0.3f);
		yield return StartCoroutine(ClearDialogProcess(a_amount, LabyrinthResultDialog.DISPLAY_TYPE.STAGE_ALL_CLEAR));
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private IEnumerator StarCompleteProcess(AccessoryData a_accessoryData)
	{
		yield return StartCoroutine(PlayClearAnimation(ANIME_TYPE.COURSESTAR_COMP));
		yield return new WaitForSeconds(0.3f);
		mAutoUnlockAccessory = a_accessoryData;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private IEnumerator SwitchScreen(IEnumerator a_coroutine)
	{
		mIsSwitchScreen = true;
		yield return StartCoroutine(GrayOut(1f, Color.white, 0.5f));
		yield return StartCoroutine(mCurrentPage.DeactiveAllObject());
		yield return StartCoroutine(mCurrentPage.UnloadResource());
		UnityEngine.Object.Destroy(mCurrentPage.gameObject);
		mCurrentPage = null;
		if (mBGImage != null)
		{
			UnityEngine.Object.Destroy(mBGImage.gameObject);
			mBGImage = null;
		}
		yield return StartCoroutine(a_coroutine);
		SetDeviceScreen(Util.ScreenOrientation);
		float cameraY = ClearedStagePosition();
		yield return StartCoroutine(DirectMapMove(0f - cameraY));
		UpdateMapPages();
		SetLayout(Util.ScreenOrientation);
		UpdateMapPages();
		yield return StartCoroutine(GrayIn());
		mIsSwitchScreen = false;
	}

	public void InitOpenCourse()
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		SMEventPageData sMEventPageData = base.MapPageData as SMEventPageData;
		List<int> list = new List<int>(sMEventPageData.EventCourseList.Keys);
		list.Sort();
		for (int i = 0; i < list.Count; i++)
		{
			int num = list[i];
			short num2 = (short)Def.GetStageNo(num + 1, 0);
			if (!data.LabyrinthData.CourseStageStatus.ContainsKey(num2) || data.LabyrinthData.CourseStageStatus[num2] == Player.STAGE_STATUS.NOOPEN)
			{
				data.LabyrinthData.CourseStageStatus[num2] = Player.STAGE_STATUS.LOCK;
				data.LabyrinthData.CourseLineStatus[num2] = Player.STAGE_STATUS.LOCK;
			}
			if (data.LabyrinthData.CourseStageStatus[num2] == Player.STAGE_STATUS.LOCK)
			{
				SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[num];
				int openAccessoryID = sMEventCourseSetting.OpenAccessoryID;
				if (openAccessoryID < 0)
				{
					data.LabyrinthData.CourseStageStatus[num2] = Player.STAGE_STATUS.UNLOCK;
					data.LabyrinthData.CourseLineStatus[num2] = Player.STAGE_STATUS.LOCK;
				}
				else if (mGame.mPlayer.IsAccessoryUnlock(openAccessoryID) && !mNewCourseNotifyList.Contains(num2))
				{
					mNewCourseNotifyList.Add(num2);
				}
			}
		}
	}

	public override float ClearedStagePosition(bool a_newcheck = true)
	{
		float num = 0f;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		int num2 = 100;
		float num3 = 0f;
		if (IsInCourseSelect)
		{
			num2 = data.LabyrinthData.LastPlayCourseNo;
			num3 = 100f;
			if ((Util.IsWideScreen() && Util.ScreenOrientation == ScreenOrientation.LandscapeLeft) || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				float num4 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num5 = 0.5625f;
				float num6 = 0.4617052f;
				num3 -= 70f * (num5 - num4) / (num5 - num6);
			}
		}
		else
		{
			if (data.LabyrinthData.CurrentClearedStages.ContainsKey(CurrentCourseID))
			{
				int count = data.LabyrinthData.CurrentClearedStages[CurrentCourseID].Count;
				num2 = ((count <= 0) ? 10000 : data.LabyrinthData.CurrentClearedStages[CurrentCourseID][count - 1]);
			}
			else
			{
				num2 = 10000;
			}
			num3 = 250f;
			if ((Util.IsWideScreen() && Util.ScreenOrientation == ScreenOrientation.LandscapeLeft) || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				float num7 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num8 = 0.5625f;
				float num9 = 0.4617052f;
				float num10 = (num8 - num7) / (num8 - num9);
				switch (num2)
				{
				case 10000:
					num3 -= 100f * num10;
					break;
				case 100:
				case 200:
					num3 -= 40f * num10;
					break;
				case 300:
				case 400:
				case 500:
					num3 -= 20f * num10;
					break;
				case 600:
				case 900:
					num3 -= 40f * num10;
					break;
				}
			}
		}
		num = mCurrentPage.GetStageWorldPos(num2);
		float limitMapPageY = GetLimitMapPageY();
		if (num > limitMapPageY)
		{
			return limitMapPageY;
		}
		return num + num3;
	}

	protected void CheckAccessoryDisplay(SMMapPage a_page)
	{
		a_page.DisplayAllAccessory();
	}

	public bool CreateGetReward()
	{
		bool result = false;
		GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(CurrentEventID);
		if (labyrinthData != null)
		{
			if (labyrinthData.RewardAccessoryDataList != null && labyrinthData.RewardAccessoryDataList.Count > 0)
			{
				for (int i = 0; i < labyrinthData.RewardAccessoryDataList.Count; i++)
				{
					AccessoryData data = labyrinthData.RewardAccessoryDataList[i];
					switch (data.AccessoryType)
					{
					case AccessoryData.ACCESSORY_TYPE.GEM:
						if (!mGetGemAccessoryDataList.Exists((AccessoryData a) => a.Index == data.Index))
						{
							mGetGemAccessoryDataList.Add(data);
						}
						break;
					case AccessoryData.ACCESSORY_TYPE.PARTNER:
						if (!mGetPartnerAccessoryDataList.Exists((AccessoryData a) => a.Index == data.Index))
						{
							mGetPartnerAccessoryDataList.Add(data);
						}
						break;
					case AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY:
						if (!mGetKeyAccessoryDataList.Exists((AccessoryData a) => a.Index == data.Index))
						{
							mGetKeyAccessoryDataList.Add(data);
						}
						break;
					default:
						if (!mGetAccessoryDataList.Exists((AccessoryData a) => a.Index == data.Index))
						{
							mGetAccessoryDataList.Add(data);
						}
						break;
					}
				}
				labyrinthData.RewardAccessoryDataList.RemoveAll((AccessoryData a) => a != null);
				labyrinthData.RewardAccessoryDataList = null;
				result = true;
			}
			else if (labyrinthData.RewardGCAccessoryDataList != null && labyrinthData.RewardGCAccessoryDataList.Count <= 0)
			{
			}
			GlobalVariables.Instance.SetLabyrinthData(CurrentEventID, labyrinthData);
		}
		return result;
	}

	public void OnMapAccessoryTapped(AccessoryData data, MapAccessory a_accessory)
	{
		if (data == null || (mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.TUTORIAL))
		{
			a_accessory.RequestOpenAnime();
		}
		else if (!string.IsNullOrEmpty(data.DemoName) && data.DemoName != "NONE")
		{
			if (mState.GetStatus() != STATE.MAIN)
			{
				a_accessory.RequestOpenAnime();
				return;
			}
			SetScrollPower(0f);
			UpdateMapPages();
			StartCoroutine(GrayOut());
		}
		else
		{
			GetAccessoryProcess(data);
			mPushedAccessory = a_accessory;
		}
	}

	public void GetAccessoryProcess(AccessoryData data, bool a_add = true)
	{
		switch (data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.PARTNER:
			StartCoroutine(GrayOut());
			mGetPartnerDialog = Util.CreateGameObject("GetPartnerDialog", mRoot).AddComponent<GetPartnerDialog>();
			mGetPartnerDialog.Init(data, GetPartnerDialog.PLACE.MAIN);
			mGetPartnerDialog.SetClosedCallback(OnGetPartnerDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART:
		case AccessoryData.ACCESSORY_TYPE.GEM:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
			StartCoroutine(GrayOut());
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init(data, 0, a_add);
			mGetAccessoryDialog.SetClosedCallback(OnGetAccessoryDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		}
	}

	public void OnGetPartnerDialogClosed(GetPartnerDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGetPartnerDialog.Data;
		UnityEngine.Object.Destroy(mGetPartnerDialog.gameObject);
		mGetPartnerDialog = null;
		if (data == null || data.HasDemo)
		{
		}
		mPushedAccessory = null;
		mState.Change(STATE.UNLOCK_ACTING);
		StartCoroutine(GrayIn());
	}

	public void OnGrowupSelectDialogClosed(GrowupPartnerSelectDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupDialog.SelectedPartnerNo;
		bool isCloseButton = mGrowupDialog.IsCloseButton;
		mGrowupDialog.Init(false);
		AccessoryData data = mGrowupDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupDialog.gameObject);
		mGrowupDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerSelectDialog.SELECT_ITEM.PLAY:
			mGrowupPartnerConfirmtDialog = Util.CreateGameObject("GrowupPartnerConfirmDialog", mRoot).AddComponent<GrowupPartnerConfirmDialog>();
			mGrowupPartnerConfirmtDialog.Init(data, selectedPartnerNo, isCloseButton, accessoryType);
			mGrowupPartnerConfirmtDialog.SetClosedCallback(OnGrowupPartnerConfirmDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerSelectDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			if (mPushedAccessory != null)
			{
				mPushedAccessory.OpenAnime();
			}
			mState.Change(STATE.UNLOCK_ACTING);
			if (isCloseButton && mGame.mPlayer.GrowUpPiece > 0)
			{
				StartCoroutine(GrayOut());
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mState.Reset(STATE.WAIT, true);
			}
			break;
		}
	}

	public void OnGrowupPartnerConfirmDialogClosed(GrowupPartnerConfirmDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupPartnerConfirmtDialog.SelectedPartnerNo;
		bool isReplayStamp = mGrowupPartnerConfirmtDialog.IsReplayStamp;
		mGrowupPartnerConfirmtDialog.Init(false);
		AccessoryData data = mGrowupPartnerConfirmtDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupPartnerConfirmtDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupPartnerConfirmtDialog.gameObject);
		mGrowupPartnerConfirmtDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerConfirmDialog.SELECT_ITEM.PLAY:
			mGrowupResultDialog = Util.CreateGameObject("GrowupResultDialog", mRoot).AddComponent<GrowupResultDialog>();
			mGrowupResultDialog.Init(data, selectedPartnerNo, true, accessoryType, isReplayStamp);
			mGrowupResultDialog.MapUpdateInit(mCurrentPage.mAvater);
			mGrowupResultDialog.SetClosedCallback(OnGrowupResultDialogClosed);
			mPushedAccessory = null;
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			if (mPushedAccessory != null)
			{
				mPushedAccessory.OpenAnime();
			}
			if (isReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.BACK:
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(data, isReplayStamp, accessoryType);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		}
	}

	public void OnGrowupResultDialogClosed(GrowupResultDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGrowupResultDialog.Data;
		StartCoroutine(GrayIn());
		if (data == null || data.HasDemo)
		{
		}
		if (mGrowupResultDialog != null)
		{
			if (mGrowupResultDialog.IsReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				StartCoroutine(GrayOut());
				mGrowupResultDialog.Init(false);
				mGrowupResultDialog.MapUpdateInit(mCurrentPage.mAvater);
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
			}
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
		UnityEngine.Object.Destroy(mGrowupResultDialog.gameObject);
		mGrowupResultDialog = null;
	}

	public void OnGetHeartPieaceDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetAccessoryDialog != null)
		{
			UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
			mGetAccessoryDialog = null;
		}
		if (mGame.mPlayer.HeartUpPiece > 0)
		{
			StartCoroutine(GrayOut());
			mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
			mGetStampDialog.Init(null, 0, false, true, false);
			mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			mBundleAccessoryCount = 0;
			mState.Change(STATE.WAIT);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
			mPushedAccessory = null;
		}
	}

	public void OnGetStampDialogClosed(GetStampDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetStampDialog != null)
		{
			UnityEngine.Object.Destroy(mGetStampDialog.gameObject);
		}
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
		if (a_selectItem == GetStampDialog.SELECT_ITEM.GROWUP)
		{
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(null, true, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else if (mGame.mPlayer.HeartUpPiece >= 15)
		{
			mGame.mPlayer.SubHeartPiece(15);
			GiftItem giftItem = new GiftItem();
			giftItem.Data.Message = Localization.Get("Gift_RatingReward");
			giftItem.Data.ItemCategory = 1;
			giftItem.Data.ItemKind = 1;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = 1;
			mGame.mPlayer.AddGift(giftItem);
			mGame.IsPlayerSaveRequest = true;
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init("icon_heart", Localization.Get("GetAccessory_Item_Title"), string.Format(Localization.Get("GetAccessory_HEART_Desc"), 1));
			mGetAccessoryDialog.SetClosedCallback(OnGetHeartPieaceDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			StartCoroutine(GrayIn());
		}
	}

	public void OnGetAccessoryDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGetAccessoryDialog.Data;
		if (data == null || data.HasDemo)
		{
		}
		if (data != null)
		{
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER)
			{
				List<AccessoryData> wallPaperPieces = mGame.GetWallPaperPieces(data.GetWallPaperIndex());
				mGame.mPlayer.SetCollectionNotify(wallPaperPieces[0].Index);
				mCockpit.SetEnableMenuButtonAttention(true);
			}
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE)
			{
				VisualCollectionData visualCollectionData = mGame.mVisualCollectionData[data.IsFlip];
				int num = 0;
				for (int i = 0; i < visualCollectionData.piceIds.Length; i++)
				{
					if (mGame.mPlayer.IsAccessoryUnlock(visualCollectionData.piceIds[i]))
					{
						num++;
					}
				}
				if (visualCollectionData.piceIds.Length == num)
				{
					mGame.mPlayer.SetCollectionNotify(visualCollectionData.accessoryId);
					mCockpit.SetEnableMenuButtonAttention(true);
					mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
					mGetAccessoryDialog.Init(true);
					mGetAccessoryDialog.SetClosedCallback(OnWallPeperCompClosed);
					mState.Reset(STATE.WAIT, true);
					return;
				}
			}
		}
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
	}

	protected void OnWallPeperCompClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
		mGetAccessoryDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
		mPushedAccessory = null;
	}

	protected bool CheaterStartCheck()
	{
		if (!mGame.CheckTimeCheater())
		{
			return false;
		}
		mState.Change(STATE.WAIT);
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("Cheater_TimeCheat_Desc"));
		mConfirmDialog.Init(Localization.Get("Cheater_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnTimeCheatErrorDialogClosed);
		return true;
	}

	protected void OnTimeCheatErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		OnExitToTitle(true);
	}

	protected void OnEventExit(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			if (!IsEventExpired(true, OnMoveToMap, delegate
			{
				mState.Change(STATE.MAIN);
			}))
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("ReturnMap_Desc"));
				mConfirmDialog.Init(Localization.Get("ReturnMap_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(OnMoveToMap);
			}
		}
	}

	protected void OnMoveToMapForce(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		MoveToMap();
	}

	protected void OnMoveToMap(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			MoveToMap();
			return;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private void OnExitStageSelect(GameObject go)
	{
		mGame.PlaySe("SE_LEVEL_BUTTON", -1);
		GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(mGame.mPlayer.Data.CurrentEventID);
		labyrinthData.LabyrinthSelectedCourseID = -1;
		CurrentCourseID = labyrinthData.LabyrinthSelectedCourseID;
		IsInCourseSelect = CurrentCourseID < 0;
		GlobalVariables.Instance.SetLabyrinthData(mGame.mPlayer.Data.CurrentEventID, labyrinthData);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
		data.CourseID = CurrentCourseID;
		mState.Reset(STATE.SWITCH_FROM_IN, true);
	}

	private void OnRestartCourse(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !IsInCourseSelect)
		{
			JellyImageButton component = go.GetComponent<JellyImageButton>();
			if (!component.IsEnable())
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
				return;
			}
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_POSITIVE", -1);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Labyrinth_Cource_Reset_Desc"), CurrentCourseID + 1);
			mConfirmDialog.Init(Localization.Get("Labyrinth_Cource_Reset_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			mConfirmDialog.SetClosedCallback(OnRestartCourseClose);
		}
	}

	private void OnRestartCourseClose(ConfirmDialog.SELECT_ITEM i)
	{
		OnRestartCourseConfirm(i);
		if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
			int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
			short a_course;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(lastPlayLevel, out a_course, out a_main, out a_sub);
			int evt_id = ((!mGame.mEventMode) ? 9999 : mGame.mPlayer.Data.CurrentEventID);
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
			ServerCram.EventLabyrinthCourseReset(CurrentCourseID, currentSeries, lastPlayLevel, evt_id, 1, data.LabyrinthData.JewelAmount);
		}
	}

	private void OnRestartCourseConfirm(ConfirmDialog.SELECT_ITEM i)
	{
		if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			StartCoroutine(RestartCourseProcess());
			return;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private IEnumerator RestartCourseProcess()
	{
		mState.Reset(STATE.WAIT, true);
		yield return StartCoroutine(GrayIn());
		SMEventLabyrinthPageB currentPageB = mCurrentPage as SMEventLabyrinthPageB;
		yield return StartCoroutine(currentPageB.AvaterWarpToStart());
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out eventData);
		eventData.LabyrinthData.RestartCourse(CurrentCourseID);
		GlobalLabyrinthEventData globalLabyrinthData = GlobalVariables.Instance.GetLabyrinthData(mGame.mPlayer.Data.CurrentEventID);
		globalLabyrinthData.RestartCourse();
		GlobalVariables.Instance.SetLabyrinthData(mGame.mPlayer.Data.CurrentEventID, globalLabyrinthData);
		mGame.PlaySe("SE_DEMO_TEAR", 2);
		mGame.IsPlayerSaveRequest = true;
		mState.Reset(STATE.SWITCH_TO_IN, true);
	}

	private void OnRestartCourseResult(ConfirmDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		if (i == ConfirmDialog.SELECT_ITEM.NEGATIVE)
		{
			mIsCourseRestart = true;
			ConfirmDialog.SELECT_ITEM i2 = ConfirmDialog.SELECT_ITEM.POSITIVE;
			OnRestartCourseConfirm(i2);
		}
		else
		{
			OnExitStageSelect(null);
		}
	}

	protected void MoveToMap()
	{
		mGame.StopMusic(0, 0.5f);
		mGame.mPlayer.SetNextSeries(mGame.mPlayer.Data.PreviousSeries);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override bool WebviewStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return false;
		}
		if (mGame.mEventProfile == null)
		{
			return false;
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		if (seasonEvent == null)
		{
			return false;
		}
		int helpID = seasonEvent.HelpID;
		string helpURL = seasonEvent.HelpURL;
		if (helpID == 0 || string.IsNullOrEmpty(helpURL))
		{
			return false;
		}
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		if (data.FirstHowToPlayFlg >= seasonEvent.HelpID)
		{
			return false;
		}
		mStartWebview = true;
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		OnEventHelp(null);
		return true;
	}

	protected void OnDebugEventHowToClosed(ConfirmDialog.SELECT_ITEM select)
	{
		if (mConfirmDialog != null)
		{
			UnityEngine.Object.Destroy(mConfirmDialog.gameObject);
			mConfirmDialog = null;
		}
		HowToDialogCloseProcess();
	}

	protected void OnEventNewAreaOpenDialogClosed()
	{
		StartCoroutine(GrayIn());
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		SMEventMapSetting sMEventMapSetting = sMEventPageData.GetMap(CurrentCourseID) as SMEventMapSetting;
		base.MapPage.UpdateMapInfomation(sMEventMapSetting.MaxLevel);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnCourseClearDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnCourseCompleteDialogClosed(ConfirmImageDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnEventHelp(GameObject go)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
			mHelpDialog = null;
		}
		if (IsEventHelpExpired(delegate
		{
			mConfirmDialog = null;
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}))
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			return;
		}
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		if (seasonEvent != null)
		{
			int helpID = seasonEvent.HelpID;
			string helpURL = seasonEvent.HelpURL;
			if (helpID != 0 && !string.IsNullOrEmpty(helpURL))
			{
				StartCoroutine(GrayOut());
				mGame.PlaySe("SE_POSITIVE", -1);
				mHelpDialog = Util.CreateGameObject("HowToDialog", mRoot).AddComponent<WebViewDialog>();
				mHelpDialog.Title = Localization.Get("EventPage");
				mHelpDialog.SetClosedCallback(OnHowToDialogClosed);
				mHelpDialog.SetPageMovedCallback(OnHowToDialogPageMoved);
				mState.Reset(STATE.WEBVIEW, true);
			}
		}
	}

	protected void OnHowToDialogClosed()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		if (mHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mHelpDialog.gameObject);
		}
		mHelpDialog = null;
		HowToDialogCloseProcess();
	}

	private void HowToDialogCloseProcess()
	{
		SeasonEventSettings seasonEvent = mGame.mEventProfile.GetSeasonEvent(CurrentEventID);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, CurrentEventID, out data);
		data.FirstHowToPlayFlg = (byte)seasonEvent.HelpID;
		mGame.mPlayer.Data.SetMapData(CurrentEventID, data);
		mGame.IsPlayerSaveRequest = true;
		int getnum = 0;
		if (mStartWebview)
		{
			mStartWebview = false;
			SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
			int num = 0;
			List<int> list = new List<int>(sMEventPageData.EventRewardList.Keys);
			list.Sort();
			for (int i = 0; i < list.Count; i++)
			{
				int num2 = list[i];
				SMEventRewardSetting sMEventRewardSetting = sMEventPageData.EventRewardList[num2];
				num = sMEventRewardSetting.RewardID;
				if (num != 0)
				{
					AccessoryData accessoryData = mGame.GetAccessoryData(num);
					if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
					{
						mIntroAccessory = accessoryData.Index;
						getnum = num2;
						break;
					}
				}
			}
		}
		if (mIntroAccessory > 0)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mEventIntroCharacterDialog = Util.CreateGameObject("EventIntroCharacterDialog", mRoot).AddComponent<EventIntroCharacterDialog>();
			mEventIntroCharacterDialog.Init(mIntroAccessory, Def.EVENT_TYPE.SM_LABYRINTH, getnum);
			mEventIntroCharacterDialog.SetClosedCallback(OnEventIntroCharactertDialogClosed);
			mIntroAccessory = 0;
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	private void OnEventIntroCharactertDialogClosed()
	{
		StartCoroutine(GrayOut());
		mState.Reset(STATE.WAIT, true);
		mEventImageDialog = Util.CreateGameObject("EventExplainDialog", mRoot).AddComponent<EventImageDialog>();
		mEventImageDialog.Init("howto_ps01", Localization.Get("Labyrinth_Guid00_Title"), Localization.Get("Labyrinth_Guid00_Desc"), "EVENTLABYRINTH");
		mEventImageDialog.SetClosedCallback(delegate
		{
			mEventImageDialog = null;
			OnDialogClosedToUnlockActing();
		});
	}

	protected void OnDialogClosedToUnlockActing()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	private void OnHowToErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (mHelpDialog != null)
		{
			switch (item)
			{
			case ConfirmDialog.SELECT_ITEM.POSITIVE:
				mHelpDialog.Reload();
				break;
			case ConfirmDialog.SELECT_ITEM.NEGATIVE:
				mHelpDialog.Close();
				break;
			}
		}
		mConfirmDialog = null;
	}

	private void OnHowToErrorDialogClosed2(ConfirmDialog.SELECT_ITEM item)
	{
		if (mHelpDialog != null)
		{
			mHelpDialog.Close();
		}
		mConfirmDialog = null;
	}

	private void OnHowToDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mConfirmDialog != null)
			{
				mConfirmDialog.Close();
			}
			mHelpDialog.ShowWebview(true);
		}
		else
		{
			mHelpDialog.ShowWebview(false);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_CLOSE);
			mConfirmDialog.SetClosedCallback(OnHowToErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
		}
	}

	protected void OnHowToDialog()
	{
		if (mHelpDialog.IsOpend)
		{
			mState.Reset(STATE.WEBVIEW_WAIT, true);
			mHelpDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_DOMAIN);
			StringBuilder stringBuilder = new StringBuilder();
			SeasonEventSettings seasonEvent = mGame.EventProfile.GetSeasonEvent(CurrentEventID);
			if (seasonEvent != null)
			{
				stringBuilder.Append(seasonEvent.HelpURL);
				mHelpDialog.LoadURL(stringBuilder.ToString());
				return;
			}
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
			mConfirmDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnHowToErrorDialogClosed2);
		}
	}

	protected void OnRewardList(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			if (mHelpDialog != null)
			{
				UnityEngine.Object.Destroy(mHelpDialog.gameObject);
				mHelpDialog = null;
			}
			StartCoroutine(GrayOut());
			mGame.PlaySe("SE_POSITIVE", -1);
			RewardListDialog rewardListDialog = Util.CreateGameObject("RewardListDialog", mRoot).AddComponent<RewardListDialog>();
			rewardListDialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, delegate
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			});
			mState.Reset(STATE.WAIT, true);
		}
	}

	protected bool RankingStartCheck()
	{
		return false;
	}

	protected bool RankingRankUpStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsStage(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.STAGE);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected bool RankingRankUpStarStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsStar(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.STAR);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected bool RankingRankUpTrophyStartCheck()
	{
		Def.SERIES currentSeries = mGame.mPlayer.Data.CurrentSeries;
		int totalPlayableLevel = mGame.mPlayer.Data.TotalPlayableLevel;
		MyFriend rankingData = null;
		int oldPlayerRank = -1;
		int newPlayerRank = -1;
		if (!mGame.CheckRankUpFriendsTrophy(currentSeries, totalPlayableLevel, out rankingData, out newPlayerRank, out oldPlayerRank))
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mState.Change(STATE.WAIT);
		mRankUpDialog = Util.CreateGameObject("RankUpDialog", mRoot).AddComponent<RankUpDialog>();
		mRankUpDialog.Init(newPlayerRank, oldPlayerRank, rankingData, RankUpDialog.MODE.TROPHY);
		mRankUpDialog.SetClosedCallback(OnRankUpDialogClosed);
		mRankUpDialog.SetBaseDepth(75);
		return true;
	}

	protected void OnRankUpDialogClosed()
	{
		StartCoroutine(GrayIn());
		mRankUpDialog = null;
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected bool StoryDemoStartCheck()
	{
		if (mShowStoryDemoList.Count > 0)
		{
			return true;
		}
		return false;
	}

	protected void StartStoryDemo(int demoName)
	{
		mCurrentDemoIndex = demoName;
		mGameState.FadeOutMaskForStory();
		mGame.StopMusic(0, 0.5f);
		mIsPlayingBGM = false;
		mState.Change(STATE.STORY_DEMO_FADE);
	}

	public void OnStoryDemoFinished(int storyIndex)
	{
		StartCoroutine(GrayIn(1f));
		mGame.mPlayer.StoryComplete(CurrentEventID, storyIndex);
		if (mCurrentStoryDemo != null)
		{
			if (mCurrentStoryDemo.ClearStage == -1 && mCurrentStoryDemo.SelectStage != -1)
			{
				int selectStage = mCurrentStoryDemo.SelectStage;
				StartCoroutine(GrayOut());
				StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, selectStage, string.Empty, 0));
			}
			else
			{
				mState.Change(STATE.UNLOCK_ACTING);
				mCockpit.SetEnableButtonAction(true);
				mCockpitOperation = true;
			}
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
			mCockpit.SetEnableButtonAction(true);
			mCockpitOperation = true;
		}
		mGame.PlayMusic(DEFAULT_EVENT_BGM, 0, true, false, true);
	}

	private bool TutorialStartCheckOnInit()
	{
		return false;
	}

	public void OnCourseButtonPushed(int a_course)
	{
		if ((mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.UNLOCK_ACTING) && !base.IsShowingMarketingScreen)
		{
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, CurrentEventID, out data);
			if (data == null || data.LabyrinthData == null)
			{
				BIJLog.E("no event data");
				return;
			}
			EventCockpit eventCockpit = mCockpit as EventCockpit;
			int a_sub;
			int a_main;
			Def.SplitStageNo(a_course, out a_main, out a_sub);
			a_main--;
			GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(CurrentEventID);
			labyrinthData.LabyrinthSelectedCourseID = (short)a_main;
			CurrentCourseID = labyrinthData.LabyrinthSelectedCourseID;
			IsInCourseSelect = CurrentCourseID < 0;
			data.CourseID = CurrentCourseID;
			data.LabyrinthData.LastPlayCourseNo = (short)a_course;
			mGame.IsPlayerSaveRequest = true;
			GlobalVariables.Instance.SetLabyrinthData(CurrentEventID, labyrinthData);
			mState.Reset(STATE.SWITCH_TO_IN, true);
		}
	}

	public void OnStageButtonPushed(int stage)
	{
		if ((mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.UNLOCK_ACTING) || base.IsShowingMarketingScreen)
		{
			return;
		}
		if (stage != 20000 && IsEventExpired(false, delegate
		{
			mConfirmDialog = null;
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
		}, delegate
		{
			mState.Change(STATE.MAIN);
		}))
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			return;
		}
		SMEventLabyrinthPageB sMEventLabyrinthPageB = mCurrentPage as SMEventLabyrinthPageB;
		if (sMEventLabyrinthPageB == null)
		{
			return;
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(stage, out a_main, out a_sub);
		SMMapStageSetting mapStage = mMapPageData.GetMapStage(a_main, a_sub, CurrentCourseID);
		int num = -1;
		if (mapStage != null)
		{
			int storyDemoAtStageSelect = mapStage.GetStoryDemoAtStageSelect();
			if (storyDemoAtStageSelect != -1 && !mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, storyDemoAtStageSelect))
			{
				num = storyDemoAtStageSelect;
			}
		}
		if (num == -1 || mGame.mPlayer.IsStoryCompletedInEvent(CurrentEventID, num))
		{
			if (stage == 20000)
			{
				SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
				SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[CurrentCourseID];
				mGame.AddEventPoint(CurrentEventID, sMEventCourseSetting.CourseClearRewardNum);
				mCourseClearRewardNum = sMEventCourseSetting.CourseClearRewardNum;
				PlayerEventData data;
				mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, CurrentEventID, out data);
				ServerCram.EventLabyrinthGetJewel(CurrentCourseID, (int)mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.LastPlayLevel, CurrentEventID, 2, sMEventCourseSetting.CourseClearRewardNum, 0, 0, 0, 0, sMEventCourseSetting.CourseClearRewardNum, 0, 0, data.LabyrinthData.JewelAmount);
				if (!mGame.mServerCramVariableData.Labyrinth.ContainsKey(CurrentEventID))
				{
					mGame.mServerCramVariableData.Labyrinth.Add(CurrentEventID, new ServerCramVariableData.LabyrinthInfo
					{
						CourseClearCount = new Dictionary<int, int>(),
						DoubleUpChanceCount = 0,
						TotalStageClearCount = 0
					});
				}
				if (CurrentEventID > 0)
				{
					if (!mGame.mServerCramVariableData.Labyrinth[CurrentEventID].CourseClearCount.ContainsKey(CurrentCourseID))
					{
						mGame.mServerCramVariableData.Labyrinth[CurrentEventID].CourseClearCount.Add(CurrentCourseID, 0);
					}
					Dictionary<int, int> courseClearCount;
					Dictionary<int, int> dictionary = (courseClearCount = mGame.mServerCramVariableData.Labyrinth[CurrentEventID].CourseClearCount);
					int currentCourseID;
					int key = (currentCourseID = CurrentCourseID);
					currentCourseID = courseClearCount[currentCourseID];
					dictionary[key] = currentCourseID + 1;
					mGame.SaveServerCramVariableData();
				}
				PlayerEventData data2;
				mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data2);
				data2.LabyrinthData.RestartCourse(CurrentCourseID);
				GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(mGame.mPlayer.Data.CurrentEventID);
				labyrinthData.RestartCourse();
				GlobalVariables.Instance.SetLabyrinthData(mGame.mPlayer.Data.CurrentEventID, labyrinthData);
				mGame.IsPlayerSaveRequest = true;
				StartCoroutine(StartCourseClearAnimation());
				mState.Change(STATE.WAIT);
			}
			else if (!sMEventLabyrinthPageB.ClearedStageList.Contains((short)stage))
			{
				StartCoroutine(GrayOut());
				StartCoroutine(StartStage(mGame.mPlayer.Data.CurrentSeries, stage, string.Empty, 0));
			}
			else
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmAlertDialog>();
				mConfirmDialog.Init(string.Format(Localization.Get("LevelStart_Title"), a_main), string.Empty, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				((ConfirmAlertDialog)mConfirmDialog).SetLabelSetting(new List<ConfirmAlertDialog.LabelSetting>
				{
					new ConfirmAlertDialog.LabelSetting
					{
						UseColor = true,
						DescColor = Color.red,
						Dimension = new Vector2(540f, 40f),
						Position = new Vector3(0f, 100f, 0f),
						Description = Localization.Get("Labyrinth_StageCaution_Desc00")
					},
					new ConfirmAlertDialog.LabelSetting
					{
						Dimension = new Vector2(540f, 120f),
						Position = new Vector3(0f, 18f, 0f),
						Description = Localization.Get("Labyrinth_StageCaution_Desc01")
					}
				});
				mConfirmDialog.SetClosedCallback(delegate
				{
					mConfirmDialog = null;
					OnDialogClosedToUnlockActing();
				});
				mConfirmDialog.SetBaseDepth(70);
				mState.Change(STATE.WAIT);
			}
		}
		else
		{
			StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
			storyDemoTemp.DemoIndex = num;
			storyDemoTemp.SelectStage = stage;
			storyDemoTemp.ClearStage = -1;
			mShowStoryDemoList.Add(storyDemoTemp);
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public override bool IsStageButtonSECall(int stage)
	{
		bool result = true;
		if ((mState.GetStatus() != STATE.MAIN && mState.GetStatus() != STATE.UNLOCK_ACTING) || base.IsShowingMarketingScreen)
		{
			result = false;
		}
		return result;
	}

	protected virtual IEnumerator StartStage(Def.SERIES a_series, int stage, string _DisplayStageNumber, int _LimitTimeOrMoves, int _NoticeStageNo = 0)
	{
		mState.Reset(STATE.NETWORK_STAGE, true);
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(a_series, CurrentEventID, out eventData);
		PlayerMapData data = eventData.CourseData[CurrentCourseID];
		data.CurrentLevel = stage;
		mGame.mPlayer.Data.SetMapData(a_series, data);
		int main;
		int sub;
		Def.SplitStageNo(stage, out main, out sub);
		stage = Def.GetStageNo(CurrentCourseID, main, sub);
		bool _stage_load_finished = false;
		bool _stage_load_success = false;
		float _start_time = Time.realtimeSinceStartup;
		GameStatePuzzle.STAGE_DISPLAY_NUMBER = _DisplayStageNumber;
		GameStatePuzzle.STAGE_LIMIT_TIMES_OR_MOVES = _LimitTimeOrMoves;
		string _DisplayStageNumber2 = default(string);
		int _LimitTimeOrMoves2 = default(int);
		StartCoroutine(SMStageData.CreateAsync(userSegment: mGame.GetUserSegment(), a_series: a_series, eventID: CurrentEventID, stage: stage, fn: delegate(int _stage, SMStageData.EventArgs _e)
		{
			if (_e != null)
			{
				_stage_load_success = true;
				GameStatePuzzle.STAGE_DATA_REQUIRE = false;
				mGame.mCurrentStage = _e.StageData;
				if (!string.IsNullOrEmpty(_DisplayStageNumber2))
				{
					mGame.mCurrentStage.DisplayStageNumber = _DisplayStageNumber2;
				}
				if (_LimitTimeOrMoves2 > 0)
				{
					Def.STAGE_LOSE_CONDITION loseType = mGame.mCurrentStage.LoseType;
					if (loseType != 0 && loseType == Def.STAGE_LOSE_CONDITION.TIME)
					{
						mGame.mCurrentStage.OverwriteLimitTimeSec = _LimitTimeOrMoves2;
					}
					else
					{
						mGame.mCurrentStage.OverwriteLimitMoves = _LimitTimeOrMoves2;
					}
				}
				short a_course;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(_stage, out a_course, out a_main, out a_sub);
				SMEventPageData eventPageData = GameMain.GetEventPageData(CurrentEventID);
				mGame.mCurrentMapPageData = eventPageData;
				mGame.mCurrentStageInfo = eventPageData.GetMapStage(a_main, a_sub, a_course);
				if (mGame.mCurrentStageInfo == null)
				{
					mGame.mCurrentStageInfo = new SMMapStageSetting();
				}
				else
				{
					GameStatePuzzle.STAGE_DISPLAY_NUMBER = mGame.mCurrentStageInfo.DisplayStageNo;
					mGame.mCurrentStage.DisplayStageNumber = mGame.mCurrentStageInfo.DisplayStageNo;
				}
				SMEventLabyrinthStageSetting labyrinthStageSetting = eventPageData.GetLabyrinthStageSetting(a_main, a_sub, a_course);
				if (labyrinthStageSetting != null)
				{
					mGame.mCurrentStage.SetJewelSetting(labyrinthStageSetting.ItemDropRate, labyrinthStageSetting.ItemDropLimit_1, labyrinthStageSetting.ItemDropLimit_3, labyrinthStageSetting.ItemDropLimit_5, labyrinthStageSetting.ItemDropLimit_9);
				}
			}
			_stage_load_finished = true;
		}));
		while (!_stage_load_finished)
		{
			yield return 0;
		}
		float _end_span = Time.realtimeSinceStartup - _start_time;
		if (!_stage_load_success || mGame.mCurrentStage == null)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string message = string.Format(Localization.Get("StageData_Error_Desc_02"));
			mConfirmDialog.Init(Localization.Get("StageData_Error_Title_02"), message, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnStageDataErrorDialogClosed);
			mConfirmDialog.SetBaseDepth(70);
			yield break;
		}
		if (mStageInfo == null)
		{
			mStageInfo = Util.CreateGameObject("StageInfoDialog", mRoot).AddComponent<StageInfoDialog>();
			mStageInfo.Init();
			mStageInfo.SetClosedCallback(OnStageInfoDialogClosed);
			mStageInfo.SetPartnerSelectCallback(OnStageCharaDialogOpen);
			mStageInfo.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			mStageInfo.SetBaseDepth(65);
		}
		SetScrollPower(0f);
	}

	private void OnStageDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		mConfirmDialog = null;
		if (mSocialRanking != null)
		{
			mSocialRanking.Close();
			mSocialRanking = null;
		}
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnStageCharaDialogOpen()
	{
		mStageChara = Util.CreateGameObject("StageCharaDialog", mRoot).AddComponent<StageCharaDialog>();
		mStageChara.SetClosedCallback(OnStageCharaDialogClosed);
	}

	public void OnStageCharaDialogClosed(StageCharaDialog.SELECT_ITEM selectItem)
	{
		int selectedPartnerNo = mStageChara.SelectedPartnerNo;
		if (mStageInfo != null && selectedPartnerNo != -1)
		{
			mStageInfo.SetCompanion(selectedPartnerNo);
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
	}

	public void OnStageInfoDialogClosed(StageInfoDialog.SELECT_ITEM selectItem)
	{
		switch (selectItem)
		{
		case StageInfoDialog.SELECT_ITEM.PLAY:
		{
			List<StageRankingData> rankingData = null;
			if (base.RankingData != null)
			{
				rankingData = base.RankingData.results;
			}
			mGame.InitFriendsRanking(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, ref rankingData, mGame.mPlayer);
			mState.Change(STATE.PLAY);
			break;
		}
		case StageInfoDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			mState.Change(STATE.UNLOCK_ACTING);
			break;
		}
		if (mStageInfo != null)
		{
			UnityEngine.Object.Destroy(mStageInfo.gameObject);
			mStageInfo = null;
		}
		if (mStageChara != null)
		{
			UnityEngine.Object.Destroy(mStageChara.gameObject);
			mStageChara = null;
		}
		if (GameMain.MaintenanceMode)
		{
			mGame.IsTitleReturnMaintenance = true;
			OnExitToTitle(true);
		}
	}

	public override void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void OnBackKeyPushed(bool ignore = false)
	{
		if (ignore || (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen))
		{
			if (IsInCourseSelect)
			{
				OnEventExit(null);
			}
			else
			{
				OnExitStageSelect(null);
			}
		}
	}

	public void OnDebugPushed()
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
		sMDDebugDialog.SetGameState(this);
		sMDDebugDialog.SetClosedCallback(delegate(SMDDebugDialog.SELECT_ITEM i)
		{
			if (i == SMDDebugDialog.SELECT_ITEM.PUSH_BACKKEY)
			{
				OnBackKeyPushed(true);
			}
			else
			{
				mState.Reset(STATE.UNLOCK_ACTING, true);
			}
		});
	}

	public void OnSDPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.EVENTMAP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(mRoot);
			mState.Reset(STATE.WAIT, true);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected new void OnPurchaseDialogClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnHeartPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_EVENTMAP;
			BuyHeartDialog buyHeartDialog = Util.CreateGameObject("HeartDialog", mRoot).AddComponent<BuyHeartDialog>();
			mState.Reset(STATE.WAIT, true);
			buyHeartDialog.SetClosedCallback(OnHeartDialogDialogClosed);
			buyHeartDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
		}
	}

	protected void OnHeartDialogDialogClosed(BuyHeartDialog.SELECT_ITEM i)
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnMugenHeartButtonPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && mCockpit.mMugenflg)
		{
			mState.Reset(STATE.WAIT, true);
			base.OnMugenHeartButtonPushed(go);
		}
	}

	public override void OnMugenHeartDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnMugenHeartDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void CreateMugenHeartDialog(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START_WAIT;
		}
		else
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.END_WAIT;
		}
		mState.Reset(STATE.WAIT, true);
		base.CreateMugenHeartDialog(i);
	}

	public override void OnMugenDialogDialogClosed(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			base.OnMugenDialogDialogClosed(i);
			return;
		}
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.NONE;
		mGame.mPlayer.Data.mUseMugenHeartSetTime = 0;
		mGame.IsPlayerSaveRequest = true;
		base.OnMugenDialogDialogClosed(i);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void StartMugenHeart()
	{
		mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START;
		mGame.mPlayer.Data.MugenHeartUseTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		Def.ITEMGET_TYPE a_type;
		mGame.mPlayer.SubBoosterNum(Def.MugenHeartTimeToKindData[mGame.mPlayer.Data.mUseMugenHeartSetTime], 1, out a_type);
		if (mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) != mGame.mPlayer.Data.MaxNormalLifeCount)
		{
			mGame.mPlayer.SetItemCount(Def.ITEM_CATEGORY.CONSUME, 1, mGame.mPlayer.Data.MaxNormalLifeCount);
		}
		mGame.IsPlayerSaveRequest = true;
		base.StartMugenHeart();
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public override void OnStoreBagPushed(GameObject go, bool linkBanner = false)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen && mCockpit.mShopflg)
		{
			mState.Reset(STATE.WAIT, true);
			base.OnStoreBagPushed(go);
		}
	}

	public override void OnStoreBagDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		base.OnStoreBagDialogClosed(ItemData, item);
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		mState.Change((STATE)state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAuthAbandon()
	{
		mGame.IsTitleReturnMaintenance = true;
		mGame.PlaySe("SE_NEGATIVE", -1);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected void OnConnectErrorSegmentRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorSegmentAbandon()
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		mState.Change(STATE.NETWORK_02);
	}

	protected void OnConnectErrorAccessoryRetry(int a_state)
	{
		mState.Change((STATE)a_state);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAccessoryAbandon()
	{
		GameStateSMMapBase.mIsShowErrorDialog = false;
		AccessoryData accessoryData = mGame.GetAccessoryData(AccessoryConnectIndex);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			mGame.mPlayer.AddAccessoryConnect(AccessoryConnectIndex);
		}
		mGame.AlreadyConnectAccessoryList.Add(AccessoryConnectIndex);
		AccessoryConnectIndex = 0;
		mState.Reset(STATE.UNLOCK_ACTING, true);
		StartCoroutine(GrayIn());
	}

	protected void OnConnectErrorAdvAccessoryAbandon()
	{
		GameStateSMMapBase.mAdvIsShowErrorDialog = false;
		mAdvAutoUnlockAccessory = null;
		AccessoryData accessoryData = mGame.GetAccessoryData(AdvAccessoryConnectIndex);
		if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
		{
			mGame.mPlayer.AddAdvAccessoryConnect(AdvAccessoryConnectIndex);
		}
		mGame.AlreadyConnectAdvAccessoryList.Add(AdvAccessoryConnectIndex);
		AdvAccessoryConnectIndex = 0;
		mState.Reset(STATE.UNLOCK_ACTING, true);
		StartCoroutine(GrayIn());
	}

	protected virtual void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugLoginBonusTimeReset)
			{
				mGame.mPlayer.Data.LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
				mGame.mDebugLoginBonusTimeReset = false;
			}
			if (mState.GetStatus() == STATE.MAIN)
			{
				mState.Reset(STATE.NETWORK_RESUME_MAINTENACE, true);
			}
			else
			{
				mGame.DoLoginBonusServerCheck = true;
			}
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	protected override bool SetDeviceScreen(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		Map_DeviceScreenSizeY = vector.y;
		bool result = false;
		if (IsInCourseSelect)
		{
			Event_DeviceOffset = -15f;
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				MAP_BOUNDS_RANGE = 67f;
				Map_DeviceOffset = 0f;
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				MAP_BOUNDS_RANGE = 67f * mGame.mDeviceRatioScale;
				Map_DeviceOffset = Event_DeviceOffset + (1136f - vector.y) / 2f * mGame.mDeviceRatioScale;
				if (Util.IsWideScreen())
				{
					float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
					float num2 = 0.5625f;
					float num3 = 0.4617052f;
					Map_DeviceOffset -= (num2 - num) / (num2 - num3) * 40f;
				}
				result = true;
				break;
			}
		}
		else
		{
			Event_DeviceOffset = 350f;
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				MAP_BOUNDS_RANGE = 67f;
				Map_DeviceOffset = Event_DeviceOffset;
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				MAP_BOUNDS_RANGE = 67f * mGame.mDeviceRatioScale;
				Map_DeviceOffset = Event_DeviceOffset + (1136f - vector.y) / 2f * mGame.mDeviceRatioScale;
				result = true;
				break;
			}
		}
		return result;
	}

	protected override void SetLayout(ScreenOrientation o)
	{
		if (mSocialRanking != null)
		{
			UnityEngine.Object.Destroy(mSocialRanking.gameObject);
			mSocialRanking = null;
			Network_OnStageWait();
		}
		bool flag = SetDeviceScreen(o);
		mMapPageRoot.transform.parent = mAnchorCenter.gameObject.transform;
		if (flag)
		{
			mMapPageRoot.transform.localPosition = new Vector3(120f, 0f, 0f);
			if (mClearAnimation != null)
			{
				mClearAnimation.transform.SetLocalPosition(120f, 0f, Def.MAP_LENSFLARE_Z);
			}
		}
		else
		{
			mMapPageRoot.transform.localPosition = new Vector3(-15f, 0f, 0f);
			if (mClearAnimation != null)
			{
				mClearAnimation.transform.SetLocalPosition(0f, 0f, Def.MAP_LENSFLARE_Z);
			}
		}
		mMapPageRoot.transform.localScale = new Vector3(mGame.mDeviceRatioScale, mGame.mDeviceRatioScale, 1f);
		if (mCockpit == null)
		{
			mCockpit = Util.CreateGameObject("EventLabyrinthCockpit", base.gameObject).AddComponent<EventLabyrinthCockpit>();
			if (mState.GetStatus() == STATE.MAIN)
			{
				mCockpit.Init(this);
			}
			else
			{
				mCockpit.Init(this, false);
			}
			mCockpit.SetCallback(OnCockpitOpend, OnCockpitClosed);
			if (!(mCockpit is EventLabyrinthCockpit))
			{
			}
		}
		UpdateCockpit(false);
		GiftAttentionCheck();
		if (mState.GetStatus() == STATE.MAIN)
		{
			mCockpit.SetEnableButtonAction(true);
		}
		else
		{
			mCockpit.SetEnableButtonAction(false);
		}
		EventLabyrinthCockpit eventLabyrinthCockpit = mCockpit as EventLabyrinthCockpit;
		if (eventLabyrinthCockpit != null)
		{
			eventLabyrinthCockpit.SetLayout(o);
		}
		SetScrollPower(0f);
		mScrollPowerYTemp = 0f;
		mGame.ResetTouch();
		UpdateMapPages();
	}

	private void UpdateCockpit(bool a_se)
	{
	}

	public override bool OnDrawCockpit()
	{
		return true;
	}

	protected void OnCockpitOpend(bool opening)
	{
	}

	protected void OnCockpitClosed(bool closing)
	{
	}

	public override bool IsEnableButton()
	{
		if (base.IsShowingMarketingScreen)
		{
			return false;
		}
		if (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAIN)
		{
			return true;
		}
		return false;
	}

	protected bool AccessoryConnectCheck()
	{
		int count = mGame.mPlayer.Data.AccessoryConnect.Count;
		if (count > 0 && AccessoryConnectIndex == 0)
		{
			for (int i = 0; i < mGame.mPlayer.Data.AccessoryConnect.Count; i++)
			{
				int num = mGame.mPlayer.Data.AccessoryConnect[i];
				if (!mGame.AlreadyConnectAccessoryList.Contains(num))
				{
					AccessoryConnectIndex = num;
					return true;
				}
			}
		}
		return false;
	}

	protected bool AdvAccessoryConnectCheck()
	{
		if (mGame.mMaintenanceProfile.IsAdvMaintenanceMode)
		{
			return false;
		}
		int count = mGame.mPlayer.Data.AdvAccessoryConnect.Count;
		if (count > 0 && AdvAccessoryConnectIndex == 0)
		{
			for (int i = 0; i < mGame.mPlayer.Data.AdvAccessoryConnect.Count; i++)
			{
				int num = mGame.mPlayer.Data.AdvAccessoryConnect[i];
				if (!mGame.AlreadyConnectAdvAccessoryList.Contains(num))
				{
					AdvAccessoryConnectIndex = num;
					return true;
				}
			}
		}
		return false;
	}

	protected void OnGotErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected void OnGetRewardDialogErrorClosed()
	{
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	protected override void OnMapDestroyUpDateClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override void OnMapDestroyClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.PRETITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected IEnumerator PlayClearAnimation(ANIME_TYPE a_animetype)
	{
		bool animeFinished = false;
		string animename = "LABYRINTH_COURSE_CLEAR";
		string sename = "SE_DEMO_CHAPTER";
		bool isUseAB = false;
		switch (a_animetype)
		{
		case ANIME_TYPE.COURSESTAGE_COMP:
			animename = "LABYRINTH_COURSE_STAGE_COMP";
			sename = "SE_BINGO_BINGO";
			isUseAB = true;
			break;
		case ANIME_TYPE.COURSESTAR_COMP:
			animename = "LABYRINTH_COURSE_STAR_COMP";
			sename = "SE_DIALY_COMPLETE_STAMP";
			break;
		case ANIME_TYPE.EVENT_COMP:
			animename = "LABYRINTH_EVENT_COMP";
			sename = "SE_BINGO_FULLCOMP";
			isUseAB = true;
			break;
		}
		if (isUseAB)
		{
			ResourceInstance ri = ResourceManager.LoadSoundAsync(sename);
			while (ri.LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return null;
			}
		}
		float x = 0f;
		ScreenOrientation screenOrientation = Util.ScreenOrientation;
		if (screenOrientation == ScreenOrientation.LandscapeLeft || screenOrientation == ScreenOrientation.LandscapeRight)
		{
			x = 120f;
		}
		mClearAnimation = Util.CreateOneShotAnime("ClearAnime", animename, mAnchorCenter.gameObject, new Vector3(x, 0f, Def.MAP_LENSFLARE_Z), Vector3.one, 0f, 0f, true, (SsSprite.AnimationCallback)delegate
		{
			animeFinished = true;
			mClearAnimation = null;
		});
		mGame.PlaySe(sename, 2);
		float _start = Time.realtimeSinceStartup;
		while (!animeFinished)
		{
			float timeSpan = Time.realtimeSinceStartup;
			if (timeSpan - _start > 3f)
			{
				animeFinished = true;
			}
			yield return null;
		}
		if (isUseAB)
		{
			ResourceManager.UnloadSound(sename);
		}
	}

	protected IEnumerator StartCourseClearAnimation()
	{
		SMEventPageData eventPageData = mMapPageData as SMEventPageData;
		SMEventLabyrinthPageB pageB = mCurrentPage as SMEventLabyrinthPageB;
		if (!(pageB == null))
		{
			yield return StartCoroutine(PlayClearAnimation(ANIME_TYPE.COURSE_CLEAR));
			yield return new WaitForSeconds(0.8f);
			yield return StartCoroutine(pageB.GoalDisappear());
			yield return new WaitForSeconds(0.3f);
			yield return StartCoroutine(ClearDialogProcess(mCourseClearRewardNum, LabyrinthResultDialog.DISPLAY_TYPE.COURSE_CLEAR, STATE.UNLOCK_ACTING));
		}
	}

	protected override void OnExitToTitle(bool force)
	{
		if ((mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen) || force)
		{
			mGame.StopMusic(0, 0.5f);
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mState.Change(STATE.UNLOAD_WAIT);
		}
	}

	public void OnGiftPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			JellyImageButton component = go.GetComponent<JellyImageButton>();
			if (component.IsEnable())
			{
				StartCoroutine("GrayOut");
				mState.Reset(STATE.WAIT, true);
				mGiftFullDialog = Util.CreateGameObject("GiftDialog", mRoot).AddComponent<GiftFullDialog>();
				mGiftFullDialog.SetClosedCallback(OnGiftDialogClosed);
			}
		}
	}

	public void OnGiftDialogClosed(bool stageOpen, bool friendHelp, bool friendhelpSuccess, bool advChara, bool advTicket)
	{
		if (stageOpen)
		{
			mState.Reset(STATE.WAIT);
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Gift_AferReturnTitle"), Localization.Get("Gift_AferReturnDesc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnTitleReturnMessageClosed);
			return;
		}
		if (friendHelp && mGame.PlayingFriendHelp != null)
		{
			StartCoroutine(GrayOut());
			StartCoroutine(StartStage((Def.SERIES)mGame.PlayingFriendHelp.Gift.Data.ItemKind, mGame.PlayingFriendHelp.Gift.Data.ItemID, string.Empty, 0));
			return;
		}
		if (mCockpit != null)
		{
			mCockpit.SetEnableMailAttention(mGame.mPlayer.HasModifiedGifts());
		}
		StartCoroutine(GrayIn());
		if (mGame.AddedNewFriend)
		{
			mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
			mState.Change(STATE.NETWORK_PROLOGUE);
		}
		else
		{
			mState.Change(STATE.UNLOCK_ACTING);
		}
	}

	public void OnGameCenterPushed(GameObject go)
	{
		JellyImageButton component = go.GetComponent<JellyImageButton>();
		if (component == null || mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return;
		}
		if (!component.IsEnable() || !mGame.EnableAdvEnter())
		{
			OnGameCenterNoEnterPushed(go);
			return;
		}
		EventCockpit cockpit = mCockpit as EventCockpit;
		Action action = delegate
		{
			Action<Type> fnPositive = delegate(Type fn_dialog_type)
			{
				if (!mGame.EnableAdvEnter())
				{
					if (fn_dialog_type == typeof(EventAdvConfirmDialog))
					{
						UnityEngine.Object.Destroy(mEventAdvConfirmDialog.gameObject);
						mEventAdvConfirmDialog = null;
					}
					OnGameCenterNoEnterPushed(go);
				}
				else
				{
					bool[] wonHistory = mGame.GetWonHistory(0, 10);
					int a_main;
					int a_sub;
					Def.SplitStageNo(mGame.mPlayer.PlayableMaxLevel, out a_main, out a_sub);
					PlayerEventData data;
					mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
					ServerCram.TapIconArcade(CurrentEventID, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, CurrentEventID * 1000000 + data.CourseID * 100000 + mGame.mPlayer.PlayableMaxLevel, mGame.LastWon, (wonHistory != null) ? wonHistory.Length : 0, mGame.GetWonCount(0, 10, false), mGame.GetWonMaxCountContinue(0, 10, false), GetPossessCompanionCramValue(), mGame.mPlayer.LifeCount, mGame.LastStar);
					mGame.EnterADV();
				}
			};
			Action<Type> fnNegative = delegate(Type fn_dialog_type)
			{
				if (fn_dialog_type == typeof(ConfirmDialog))
				{
					mConfirmDialog = null;
				}
				StartCoroutine(GrayIn());
				mState.Change(STATE.UNLOCK_ACTING);
			};
			if (!mGame.IsSeasonEventExpired(CurrentEventID) && cockpit != null && cockpit.IsOldEventFinished)
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("EvMap_GameCenter_Conf_Expired_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
				{
					switch (i)
					{
					case ConfirmDialog.SELECT_ITEM.POSITIVE:
						fnPositive(typeof(ConfirmDialog));
						break;
					case ConfirmDialog.SELECT_ITEM.NEGATIVE:
						fnNegative(typeof(ConfirmDialog));
						break;
					}
				});
			}
			else
			{
				mEventAdvConfirmDialog = Util.CreateGameObject("EventAdvConfirmDialog", mRoot).AddComponent<EventAdvConfirmDialog>();
				mEventAdvConfirmDialog.Init(mGame.IsSeasonEventExpired(CurrentEventID));
				mEventAdvConfirmDialog.SetClosedCallback(delegate(EventAdvConfirmDialog.SELECT_ITEM i)
				{
					switch (i)
					{
					case EventAdvConfirmDialog.SELECT_ITEM.POSITIVE:
						fnPositive(typeof(EventAdvConfirmDialog));
						break;
					case EventAdvConfirmDialog.SELECT_ITEM.NEGATIVE:
						fnNegative(typeof(EventAdvConfirmDialog));
						break;
					}
					mEventAdvConfirmDialog = null;
				});
			}
		};
		if (cockpit == null || !cockpit.TryOpenOldEventExtensionDialog(delegate
		{
			mState.Change(STATE.MAIN);
		}, action))
		{
			action();
		}
		StartCoroutine(GrayOut());
		mState.Reset(STATE.WAIT, true);
	}

	public void OnGameCenterNoEnterPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !base.IsShowingMarketingScreen)
		{
			StartCoroutine(GrayOut());
			mState.Reset(STATE.WAIT, true);
			mAdvNotEnterDialog = Util.CreateGameObject("mAdvNotEnterDialog", mRoot).AddComponent<AdvNotEnterDialog>();
			if (!mGame.EnableAdvSession())
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.MAINTENANCE_ADV);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
			else
			{
				mAdvNotEnterDialog.Init(AdvNotEnterDialog.SELECT_TYPE.STAGE_NO_CLEAR);
				mAdvNotEnterDialog.SetClosedCallback(AdvNotEnterDialogClosed);
			}
		}
	}

	public void AdvNotEnterDialogClosed()
	{
		if (mAdvNotEnterDialog != null)
		{
			UnityEngine.Object.Destroy(mAdvNotEnterDialog.gameObject);
			mAdvNotEnterDialog = null;
		}
		StartCoroutine(GrayIn());
		mState.Change(STATE.UNLOCK_ACTING);
	}

	public void OnTitleReturnMessageClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGame.StopMusic(0, 0.5f);
		mGameState.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
		mState.Change(STATE.UNLOAD_WAIT);
	}

	protected override bool GiftStartCheck()
	{
		if (mState.GetStatus() != STATE.MAIN || base.IsShowingMarketingScreen)
		{
			return false;
		}
		return base.GiftStartCheck();
	}

	public int GetMaxStars()
	{
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		return GetCoursetMaxStars(CurrentCourseID, 3);
	}

	public int GetCoursetMaxStars(short a_course, int a_star)
	{
		return 0;
	}

	public override void DEBUG_ChangeStateToInit()
	{
		mState.Change(STATE.INIT);
	}

	public override void ReEnterMap()
	{
		mState.Reset(STATE.GOTO_REENTER);
	}

	private string GetLive2DMotionName()
	{
		string result = "motions/wait00.mtn";
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		return result;
	}

	private bool OldEventEffectCheck()
	{
		EventCockpit eventCockpit = mCockpit as EventCockpit;
		if (eventCockpit != null)
		{
			if (mGame.mOldEventExtend && !mEffectedOldEvent)
			{
				mState.Change(STATE.OLD_EVENT_EFFECT_WAIT);
				eventCockpit.CanOldEventPlutoEffect = true;
				return true;
			}
			eventCockpit.CanOldEventCountUpRemain = true;
		}
		return false;
	}

	private void StartConnectFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_GETFRIENDINFO;
		}
	}

	private void StartConnectInLoadGameState()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1044);
		list.Add(1001);
		list.Add(1031);
		list.Add(1005);
		list.Add(1045);
		if (mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			list.Add(2012);
			list.Add(1017);
			list.Add(1015);
			list.Add(1016);
			list.Add(3);
		}
		else
		{
			list.Add(1016);
		}
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, false, false, StartConnectFinishCallback);
	}

	private void StartConnectToGetRewardMailFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_RESUME_CAMPAIGN;
		}
	}

	private void StartConnectToGetRewardMail()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1001);
		list.Add(2012);
		list.Add(1017);
		list.Add(3);
		mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
		mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, true, false, StartConnectToGetRewardMailFinishCallback);
	}

	private void StartConnectAfterResumeFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				mState.Reset(STATE.AUTHERROR_RETURN_TITLE, true);
				return;
			}
			mIsConnecting = false;
			mHasBootConnectingError = true;
		}
		else
		{
			mIsConnecting = false;
			mHasBootConnectingError = false;
			mStateAfterConnect = STATE.NETWORK_RESUME_CAMPAIGN;
		}
	}

	private void StartConnectAfterResume()
	{
		mIsConnecting = true;
		mHasBootConnectingError = false;
		List<int> list = new List<int>();
		Dictionary<int, ConnectParameterBase> a_params = new Dictionary<int, ConnectParameterBase>();
		list.Add(1044);
		list.Add(1001);
		list.Add(1031);
		list.Add(1005);
		list.Add(1045);
		list.Add(2012);
		list.Add(1017);
		list.Add(3);
		Network.GameConnectManager.StartConnect(list, mRoot, a_params, true, false, StartConnectAfterResumeFinishCallback);
	}
}
