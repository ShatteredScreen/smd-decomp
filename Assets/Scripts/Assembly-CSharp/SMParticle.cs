using UnityEngine;

public class SMParticle : BIJParticle
{
	private GameMain mGame;

	public override void Awake()
	{
		base.Awake();
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public override bool LoadFromPrefab(string name)
	{
		mParticle = GameMain.GetParticle(name).GetComponent<ParticleSystem>();
		if (mParticle != null)
		{
			mParticle.gameObject.transform.parent = base.gameObject.transform;
			mParticle.gameObject.layer = base.gameObject.layer;
			mParticle.gameObject.transform.localScale = Vector3.one;
			mParticle.gameObject.transform.localPosition = Vector3.zero;
			OnLoadFinished();
			return true;
		}
		return false;
	}
}
