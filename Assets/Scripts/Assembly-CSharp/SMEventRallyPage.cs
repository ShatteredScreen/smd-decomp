using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEventRallyPage : SMMapPage
{
	protected int mEventID;

	protected short mCourseID;

	public int EventID
	{
		get
		{
			return mEventID;
		}
	}

	public short CourseID
	{
		get
		{
			return mCourseID;
		}
	}

	protected override void Update()
	{
		base.Update();
	}

	public IEnumerator Init(Def.SERIES a_series, int a_eventID, short a_courseID, int a_maxLevel, OnLevelPushed a_lvlPushedCB, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadBlockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB, SMMapPageData a_data)
	{
		mPageData = a_data;
		mSeries = a_series;
		mEventID = a_eventID;
		mCourseID = a_courseID;
		if (mSeries != mPageData.Series)
		{
		}
		SMEventPageData eventPageData = a_data as SMEventPageData;
		mMaxModulePage = mPageData.GetMap(a_courseID).ModuleMaxPageNum;
		mLevelPushedCallback = a_lvlPushedCB;
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		yield return StartCoroutine(LoadRouteData(a_series, mGame.GetMapSize(eventPageData.EventSetting.EventType), a_eventID, a_courseID));
		mMaxPage = NewMapSsDataEntity.GetMapIndex(a_maxLevel) + 1;
		int available = Def.GetStageNo(mPageData.GetMap(a_courseID).AvailableMaxLevel, 0);
		mMaxAvailablePage = NewMapSsDataEntity.GetMapIndex(available) + 1;
		int moduleLevel = Def.GetStageNo(mPageData.GetMap(a_courseID).ModuleMaxLevel, 0);
		mMaxModulePageFromMaxLevel = NewMapSsDataEntity.GetMapIndex(moduleLevel) + 1;
		bool useBg = true;
		if (eventPageData.EventSetting.MapBackName.CompareTo("null") != 0)
		{
			useBg = false;
		}
		mMapGameObject = new GameObject[mMaxModulePage];
		mMapParts = new SMMapPart[mMaxModulePage];
		for (int i = 0; i < mMaxModulePage; i++)
		{
			mMapGameObject[i] = Util.CreateGameObject("MapGO" + (i + 1), base.gameObject);
			mMapParts[i] = mMapGameObject[i].AddComponent<SMEventRallyPart>();
			mMapParts[i].SetGameState(mGameState, this, eventPageData);
			SMEventRallyPart eventPart = mMapParts[i] as SMEventRallyPart;
			eventPart.SetCourseID(a_courseID);
			Vector3 scale;
			if (!NewMapSsDataEntity.MapScaleList.TryGetValue(i, out scale))
			{
				scale = Vector3.one;
			}
			mMapParts[i].Init(mSeries, i, scale, a_accessoryCB, a_roadBlockCB, a_snsCB, a_mapChangeCB, true, useBg);
			Vector3 pos = new Vector3(0f, 0f + 1136f * (float)i, Def.MAP_BASE_Z - 0.1f * (float)i);
			mMapGameObject[i].gameObject.transform.localPosition = pos;
		}
		SMMapRoute<float>.RouteNode m = NewMapSsDataEntity.MapRoute.Root;
		SMMapRoute<float>.RouteNode p4 = null;
		while (m != null)
		{
			if (m.mIsStage)
			{
				int stageNo3 = (int)m.Value;
				stageNo3 = Def.GetStageNo(stageNo3, 0);
				int mapIndex2;
				GameObject go2 = ((!NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(m, out mapIndex2)) ? base.gameObject : mMapGameObject[mapIndex2]);
				Vector3 pos3 = m.Position;
				pos3.z = Def.MAP_BUTTON_Z;
				EventStageButton button2 = CreateStageButton(stageNo3, pos3, go2);
				mStageButtonList.Add(button2);
				mMapButtons.Add(stageNo3.ToString(), button2.gameObject);
			}
			if (m.NextSub != null)
			{
				for (p4 = m.NextSub; p4 != null; p4 = p4.NextSub)
				{
					if (p4.mIsStage)
					{
						int stageNo = Def.GetStageNoByRouteOrder(p4.Value);
						int mapIndex;
						GameObject go = ((!NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(m, out mapIndex)) ? base.gameObject : mMapGameObject[mapIndex]);
						Vector3 pos2 = p4.Position;
						pos2.z = Def.MAP_BUTTON_Z;
						EventStageButton button = CreateStageButton(stageNo, pos2, go, true);
						mStageButtonList.Add(button);
						mMapButtons.Add(stageNo.ToString(), button.gameObject);
					}
				}
			}
			m = m.NextMain;
		}
		if (NewMapSsDataEntity.MapLineList.Count > 0)
		{
			asyncList.Clear();
			m = NewMapSsDataEntity.MapLineRoute.Root;
			p4 = null;
			while (m != null)
			{
				if (m.Name.ToString().Contains("Line"))
				{
					int stageNo6 = (int)m.Value;
					stageNo6 = Def.GetStageNo(stageNo6, 0);
					int mapIndex4;
					GameObject go4 = ((!NewMapSsDataEntity.MapLineRouteMapIndex.TryGetValue(m, out mapIndex4)) ? base.gameObject : mMapGameObject[mapIndex4]);
					SMMapPart part2 = mMapParts[mapIndex4];
					if (part2 != null)
					{
						string animeKey = part2.GetLineAnimeKey(mSeries, mapIndex4);
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_LOCK", true));
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_LOCK_IDLE", true));
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_UNLOCK", true));
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_UNLOCK_IDLE", true));
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey + "_NOOPEN", true));
						for (int k = 0; k < asyncList.Count; k++)
						{
							while (asyncList[k].LoadState != ResourceInstance.LOADSTATE.DONE)
							{
								yield return 0;
							}
						}
						List<SsMapEntity> list2 = NewMapSsDataEntity.GetMapLineListInMapIndex(mapIndex4);
						SsMapEntity e2 = list2.Find((SsMapEntity x) => x.AnimeKey.CompareTo(m.Value.ToString()) == 0);
						if (stageNo6 != 100)
						{
							EventLine button4 = CreateLine(e2.MapOffsetCounter, e2.AnimeKey, e2.Position, e2.Scale, e2.Angle, mapIndex4, go4, animeKey);
							mLineList.Add(button4);
							mLineDict.Add(button4.StageNo, button4);
						}
					}
				}
				if (m.NextSub != null)
				{
					for (p4 = m.NextSub; p4 != null; p4 = p4.NextSub)
					{
						if (p4.Name.ToString().Contains("Line"))
						{
							int stageNo4 = Def.GetStageNoByRouteOrder(p4.Value);
							int mapIndex3;
							GameObject go3 = ((!NewMapSsDataEntity.MapLineRouteMapIndex.TryGetValue(p4, out mapIndex3)) ? base.gameObject : mMapGameObject[mapIndex3]);
							SMEventRallyPart part = go3.GetComponent<SMEventRallyPart>();
							if (part != null)
							{
								List<SsMapEntity> list = NewMapSsDataEntity.GetMapLineListInMapIndex(mapIndex3);
								SsMapEntity e = list.Find((SsMapEntity x) => x.AnimeKey.CompareTo(p4.Value.ToString()) == 0);
								EventLine button3 = CreateLine(e.MapOffsetCounter, e.AnimeKey, e.Position, e.Scale, e.Angle, mapIndex3, go3, part.GetLineAnimeKey(mSeries, mapIndex3));
								mLineList.Add(button3);
								mLineDict.Add(button3.StageNo, button3);
							}
						}
					}
				}
				m = m.NextMain;
			}
		}
		for (int j = 0; j < mMaxModulePage; j++)
		{
			NGUITools.SetActive(mMapGameObject[j].gameObject, false);
		}
		mAvater = Util.CreateGameObject("Avatar", base.gameObject).AddComponent<MapAvater>();
		mAvater.SetCallback(OnAvaterMoveFinished, OnAvatarSave, OnGetMapData);
		mAvater.Init(this);
		yield return 0;
	}

	public new EventStageButton CreateStageButton(int a_stageno, Vector3 a_pos, GameObject a_parent, bool a_isSub = false)
	{
		SMEventPageData sMEventPageData = mPageData as SMEventPageData;
		BIJImage image = ResourceManager.LoadImage(sMEventPageData.EventSetting.MapAtlasKey).Image;
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stageno, out a_main, out a_sub);
		string text = (a_isSub ? ("stage_" + a_main + "-" + a_sub) : ("stage_" + a_main));
		Player.STAGE_STATUS stageStatus = mGame.mPlayer.GetStageStatus(mSeries, EventID, mCourseID, a_stageno);
		byte starNum = 0;
		PlayerClearData _psd;
		if (mGame.mPlayer.GetStageClearData(mSeries, EventID, mCourseID, a_stageno, out _psd))
		{
			starNum = (byte)_psd.GotStars;
		}
		bool a_hasItem = HasStageItem(a_main, a_sub, mCourseID);
		EventStageButton eventStageButton = Util.CreateGameObject(text, a_parent).AddComponent<EventStageButton>();
		eventStageButton.SetCourseID(mCourseID);
		string a_animeKey = string.Format("MAP_{0}_STAGEBUTTON", Def.GetSeriesString(mSeries, EventID));
		int displayNo = -1;
		SMMapStageSetting mapStage = sMEventPageData.GetMapStage(a_main, a_sub, mCourseID);
		if (mapStage != null)
		{
			displayNo = int.Parse(mapStage.DisplayStageNo);
		}
		float scale = eventStageButton.DEFAULT_SCALE;
		if (stageStatus != Player.STAGE_STATUS.UNLOCK && stageStatus != Player.STAGE_STATUS.CLEAR)
		{
			scale = 0.75f;
		}
		eventStageButton.SetUseCurrentAnime(false);
		eventStageButton.Init(a_pos, scale, a_stageno, starNum, stageStatus, image, a_animeKey, a_hasItem, displayNo);
		eventStageButton.SetGameState(mGameState);
		eventStageButton.SetPushedCallback(base.OnStageButtonPushed);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			SMMapStageSetting mapStage2 = mPageData.GetMapStage(a_main, a_sub, mCourseID);
			eventStageButton.SetDebugInfo(mapStage2);
		}
		return eventStageButton;
	}

	public new EventLine CreateLine(int counter, string animeName, Vector3 pos, Vector3 scale, float a_angle, int a_mapIndex, GameObject a_parent, string animeKey)
	{
		SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		BIJImage image = ResourceManager.LoadImage(currentEventPageData.EventSetting.MapAtlasKey).Image;
		EventLine eventLine = Util.CreateGameObject("MapLine" + counter, a_parent).AddComponent<EventLine>();
		eventLine.SetCourseID(mCourseID);
		eventLine.Init(counter, animeName, image, a_mapIndex, pos.x, pos.y, scale, a_angle, animeKey);
		return eventLine;
	}

	public override void DisplayAllAccessory()
	{
		SMEventPageData sMEventPageData = mPageData as SMEventPageData;
		int availableMaxLevel = sMEventPageData.GetMap(mCourseID).AvailableMaxLevel;
		int stageNo = Def.GetStageNo(availableMaxLevel, 0);
		List<int> mapStageNo = sMEventPageData.GetMapStageNo(mCourseID);
		Player mPlayer = mGame.mPlayer;
		for (int i = 0; i < mapStageNo.Count; i++)
		{
			int num = mapStageNo[i];
			foreach (AccessoryData mMapAccessoryDatum in mGame.mMapAccessoryData)
			{
				if (mMapAccessoryDatum.Series == mGame.mPlayer.Data.CurrentSeries && mMapAccessoryDatum.EventID == EventID && mMapAccessoryDatum.CourseID == CourseID && mMapAccessoryDatum.AccessoryUnlockScene.CompareTo("MAP") == 0)
				{
					int stageNo2 = Def.GetStageNo(mMapAccessoryDatum.MainStage, mMapAccessoryDatum.SubStage);
					if (num == stageNo2)
					{
						NewMapSsDataEntity.UpdateDisplayMapAccessoryDataByLayer(mMapAccessoryDatum.AnimeLayerName, true);
					}
				}
			}
		}
	}

	public override float UnlockNewStage(int a_playable, int a_notice, int a_cleared)
	{
		Player mPlayer = mGame.mPlayer;
		float result = 0f;
		int a_main = -1;
		int a_sub = -1;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[CourseID];
		bool flag = false;
		if (a_cleared > 0)
		{
			Def.SplitStageNo(a_cleared, out a_main, out a_sub);
			AccessoryData accessoryDataOnMap = mGame.GetAccessoryDataOnMap(EventID, CourseID, a_main, a_sub);
			if (accessoryDataOnMap != null && accessoryDataOnMap.RelationID != 0 && mPlayer.IsAccessoryUnlock(accessoryDataOnMap.RelationID))
			{
				mOpenAccessoryLayer = accessoryDataOnMap.AnimeLayerName;
			}
			if (a_sub > 0)
			{
				SMMapRoute<float>.RouteNode routeNode = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main, a_sub, a_sub != 0));
				if (routeNode != null && routeNode.IsSub && routeNode.NextSub != null)
				{
					SMMapRoute<float>.RouteNode routeNode2 = NewMapSsDataEntity.MapRoute.FindContain("Stage", routeNode.NextSub, false, true);
					if (routeNode2 != null)
					{
						int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode2.Value);
						Player.STAGE_STATUS stageStatus = mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder);
						if (stageStatus == Player.STAGE_STATUS.LOCK)
						{
							mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
							flag = true;
						}
					}
				}
			}
		}
		List<int> list = new List<int>();
		int a_main2;
		int a_sub2;
		if (a_playable >= a_notice && !flag)
		{
			Def.SplitStageNo(a_notice, out a_main2, out a_sub2);
			int num = a_notice;
			if (a_playable == a_notice)
			{
				switch (mPlayer.GetStageStatus(mSeries, EventID, CourseID, num))
				{
				case Player.STAGE_STATUS.LOCK:
					if (a_main2 > 1)
					{
						num = Def.GetStageNo(a_main2 - 1, 0);
					}
					break;
				case Player.STAGE_STATUS.UNLOCK:
					num = 100;
					break;
				}
			}
			List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(num, a_playable, false);
			foreach (SMMapRoute<float>.RouteNode item in stages)
			{
				if (item == null)
				{
					continue;
				}
				int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(item.Value);
				Def.SplitStageNo(stageNoByRouteOrder2, out a_main2, out a_sub2);
				PlayerClearData _psd;
				if (!mPlayer.GetStageClearData(mSeries, EventID, CourseID, stageNoByRouteOrder2, out _psd))
				{
					continue;
				}
				list.Add(stageNoByRouteOrder2);
				if (item.NextMain != null)
				{
					SMMapRoute<float>.RouteNode routeNode3 = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextMain, true, false);
					if (routeNode3 != null)
					{
						int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode3.Value);
						switch (mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder))
						{
						case Player.STAGE_STATUS.LOCK:
							if (!mPlayer.StageUnlockList.Contains(stageNoByRouteOrder))
							{
								mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
							}
							mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder);
							break;
						case Player.STAGE_STATUS.UNLOCK:
						case Player.STAGE_STATUS.CLEAR:
							if (!mPlayer.AvatarMoveList.Contains(stageNoByRouteOrder))
							{
								mPlayer.AvatarMoveList.Add(stageNoByRouteOrder);
							}
							mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, stageNoByRouteOrder);
							break;
						}
					}
				}
				if (item.NextSub == null)
				{
					continue;
				}
				SMMapRoute<float>.RouteNode routeNode4 = NewMapSsDataEntity.MapRoute.FindContain("Stage", item.NextSub, false, true);
				if (routeNode4 != null)
				{
					int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode4.Value);
					Player.STAGE_STATUS stageStatus2 = mPlayer.GetStageStatus(mSeries, EventID, CourseID, stageNoByRouteOrder);
					if (stageStatus2 == Player.STAGE_STATUS.LOCK && !mPlayer.StageUnlockList.Contains(stageNoByRouteOrder))
					{
						mPlayer.StageUnlockList.Add(stageNoByRouteOrder);
					}
				}
			}
		}
		int stage;
		foreach (int stageUnlock in mPlayer.StageUnlockList)
		{
			stage = stageUnlock;
			mPlayer.AvatarMoveList.RemoveAll((int x) => x < stage);
		}
		mPlayer.LineOpenList.Clear();
		mPlayer.LineUnlockList.Clear();
		if (mLineList.Count > 0)
		{
			if (a_cleared > 0)
			{
				OpenNextLine(a_cleared);
				UnlockNextLine(a_cleared);
			}
			else
			{
				if (list.Count == 0)
				{
					SMEventPageData sMEventPageData = mPageData as SMEventPageData;
					if (sMEventPageData != null)
					{
						List<SMMapStageSetting> mapStages = sMEventPageData.GetMapStages(CourseID);
						for (int i = 0; i < mapStages.Count; i++)
						{
							int stageNo = Def.GetStageNo(mapStages[i].mStageNo, mapStages[i].mSubNo);
							PlayerClearData _psd2;
							if (mPlayer.GetStageClearData(mSeries, EventID, CourseID, stageNo, out _psd2))
							{
								list.Add(stageNo);
							}
						}
					}
				}
				if (list.Count > 0)
				{
					for (int j = 0; j < list.Count; j++)
					{
						OpenNextLine(list[j]);
						UnlockNextLine(list[j]);
					}
				}
			}
		}
		List<int> list2 = new List<int>();
		List<int> list3 = new List<int>();
		mPlayer.LineUnlockList.Sort();
		for (int k = 0; k < mPlayer.StageUnlockList.Count; k++)
		{
			int num2 = mPlayer.StageUnlockList[k];
			Def.SplitStageNo(num2, out a_main2, out a_sub2);
			SMRoadBlockSetting roadBlock = mPageData.GetRoadBlock(a_main2, a_sub2, CourseID);
			if (roadBlock != null)
			{
				SMEventRoadBlockSetting sMEventRoadBlockSetting = roadBlock as SMEventRoadBlockSetting;
				if (sMEventRoadBlockSetting.UnlockAccessoryID() != 0 && !mPlayer.IsAccessoryUnlock(sMEventRoadBlockSetting.UnlockAccessoryID()))
				{
					list2.Add(num2);
				}
			}
		}
		List<SMRoadBlockSetting> roadBlockInCourse = mPageData.GetRoadBlockInCourse(mCourseID);
		for (int l = 0; l < roadBlockInCourse.Count; l++)
		{
			SMEventRoadBlockSetting sMEventRoadBlockSetting2 = roadBlockInCourse[l] as SMEventRoadBlockSetting;
			if (sMEventRoadBlockSetting2.UnlockAccessoryID() != 0 && !mPlayer.IsAccessoryUnlock(sMEventRoadBlockSetting2.UnlockAccessoryID()))
			{
				int stageNo2 = Def.GetStageNo(sMEventRoadBlockSetting2.mStageNo, sMEventRoadBlockSetting2.mSubNo);
				list3.Add(stageNo2);
			}
		}
		foreach (int item2 in list2)
		{
			mPlayer.StageUnlockList.Remove(item2);
		}
		foreach (int item3 in list3)
		{
			mPlayer.LineOpenList.Remove(item3);
			mPlayer.LineUnlockList.Remove(item3);
		}
		if (mPlayer.StageUnlockList.Count == 0)
		{
			mPlayer.SetOpenNoticeLevel(mSeries, EventID, CourseID, a_notice);
		}
		foreach (int stageUnlock2 in mPlayer.StageUnlockList)
		{
			mPlayer.SetStageStatus(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, CourseID, stageUnlock2, Player.STAGE_STATUS.UNLOCK);
		}
		mPlayer.LineOpenList.Clear();
		if (playerMapData != null)
		{
			data.CourseData[CourseID] = playerMapData;
			mPlayer.Data.SetMapData(EventID, data);
		}
		Def.SplitStageNo(a_playable, out a_main2, out a_sub2);
		SMMapRoute<float>.RouteNode routeNode5 = NewMapSsDataEntity.MapRoute.Find("Stage" + Def.GetRouteOrder(a_main2, a_sub2, a_sub2 != 0));
		if (routeNode5 != null)
		{
			result = routeNode5.RoutePosition.y;
		}
		return result;
	}

	public override void OpenNewStageUntilNextChapter(int a_current, int a_nextChapter)
	{
		List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(a_current, a_nextChapter, true);
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(base.Series, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[CourseID];
		for (int i = 0; i < stages.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stages[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS value;
			if (playerMapData.StageStatus.TryGetValue(stageNoByRouteOrder, out value))
			{
				if (value == Player.STAGE_STATUS.NOOPEN)
				{
					mPlayer.StageOpenList.Add(stageNoByRouteOrder);
				}
			}
			else
			{
				mPlayer.StageOpenList.Add(stageNoByRouteOrder);
			}
		}
	}

	public List<int> GetStagesUntilNextChapter(int a_current, int a_nextChapter)
	{
		List<int> list = new List<int>();
		List<SMMapRoute<float>.RouteNode> stages = NewMapSsDataEntity.GetStages(a_current, a_nextChapter, true);
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(base.Series, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[CourseID];
		for (int i = 0; i < stages.Count; i++)
		{
			SMMapRoute<float>.RouteNode routeNode = stages[i];
			int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS value;
			if (playerMapData.StageStatus.TryGetValue(stageNoByRouteOrder, out value))
			{
				if (value == Player.STAGE_STATUS.NOOPEN)
				{
					list.Add(stageNoByRouteOrder);
				}
			}
			else
			{
				list.Add(stageNoByRouteOrder);
			}
		}
		return list;
	}

	public override void OpenNextLine(int a_cleared)
	{
		if (a_cleared < 0)
		{
			return;
		}
		Player mPlayer = mGame.mPlayer;
		int a_main;
		int a_sub;
		if (a_cleared > Def.MaxStageNo)
		{
			short a_course;
			Def.SplitEventStageNo(a_cleared, out a_course, out a_main, out a_sub);
		}
		else
		{
			Def.SplitStageNo(a_cleared, out a_main, out a_sub);
		}
		SMMapRoute<float>.RouteNode line = NewMapSsDataEntity.GetLine(a_cleared, a_sub != 0);
		SMMapRoute<float>.RouteNode routeNode = null;
		if (line == null)
		{
			return;
		}
		if (line.IsSub)
		{
			if (line.NextSub == null)
			{
				return;
			}
			routeNode = line.NextSub;
			if (routeNode.NextSub != null)
			{
				SMMapRoute<float>.RouteNode nextSub = routeNode.NextSub;
				int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(nextSub.Value);
				if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder) == Player.STAGE_STATUS.NOOPEN)
				{
					mGame.mPlayer.LineOpenList.Add(stageNoByRouteOrder);
				}
			}
			return;
		}
		if (line.NextMain != null)
		{
			routeNode = line.NextMain;
			if (routeNode.NextMain != null)
			{
				SMMapRoute<float>.RouteNode nextMain = routeNode.NextMain;
				int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(nextMain.Value);
				if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder2) == Player.STAGE_STATUS.NOOPEN)
				{
					mGame.mPlayer.LineOpenList.Add(Def.GetStageNoByRouteOrder(nextMain.Value));
				}
			}
			if (routeNode.NextSub != null)
			{
				SMMapRoute<float>.RouteNode nextSub2 = routeNode.NextSub;
				int stageNoByRouteOrder3 = Def.GetStageNoByRouteOrder(nextSub2.Value);
				if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder3) == Player.STAGE_STATUS.NOOPEN)
				{
					mGame.mPlayer.LineOpenList.Add(Def.GetStageNoByRouteOrder(nextSub2.Value));
				}
			}
		}
		if (line.NextSub == null)
		{
			return;
		}
		routeNode = line.NextSub;
		if (routeNode.NextSub != null)
		{
			SMMapRoute<float>.RouteNode nextSub3 = routeNode.NextSub;
			int stageNoByRouteOrder4 = Def.GetStageNoByRouteOrder(nextSub3.Value);
			if (mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder4) == Player.STAGE_STATUS.NOOPEN)
			{
				mGame.mPlayer.LineOpenList.Add(Def.GetStageNoByRouteOrder(nextSub3.Value));
			}
		}
	}

	public override void UnlockNextLine(int a_cleared)
	{
		if (a_cleared < 0)
		{
			return;
		}
		Player mPlayer = mGame.mPlayer;
		int a_main;
		int a_sub;
		if (a_cleared > Def.MaxStageNo)
		{
			short a_course;
			Def.SplitEventStageNo(a_cleared, out a_course, out a_main, out a_sub);
		}
		else
		{
			Def.SplitStageNo(a_cleared, out a_main, out a_sub);
		}
		SMMapRoute<float>.RouteNode line = NewMapSsDataEntity.GetLine(a_cleared, a_sub != 0);
		SMMapRoute<float>.RouteNode routeNode = null;
		if (line == null)
		{
			return;
		}
		if (line.IsSub)
		{
			if (line.NextSub != null)
			{
				routeNode = line.NextSub;
				int stageNoByRouteOrder = Def.GetStageNoByRouteOrder(routeNode.Value);
				Player.STAGE_STATUS lineStatus = mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder);
				if (lineStatus == Player.STAGE_STATUS.LOCK)
				{
					mGame.mPlayer.LineUnlockList.Add(Def.GetStageNoByRouteOrder(routeNode.Value));
				}
			}
			return;
		}
		if (line.NextMain != null)
		{
			routeNode = line.NextMain;
			int stageNoByRouteOrder2 = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS lineStatus2 = mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder2);
			if (lineStatus2 == Player.STAGE_STATUS.LOCK)
			{
				mGame.mPlayer.LineUnlockList.Add(Def.GetStageNoByRouteOrder(routeNode.Value));
			}
		}
		if (line.NextSub != null)
		{
			routeNode = line.NextSub;
			int stageNoByRouteOrder3 = Def.GetStageNoByRouteOrder(routeNode.Value);
			Player.STAGE_STATUS lineStatus3 = mPlayer.GetLineStatus(mEventID, mCourseID, stageNoByRouteOrder3);
			if (lineStatus3 == Player.STAGE_STATUS.LOCK)
			{
				mGame.mPlayer.LineUnlockList.Add(Def.GetStageNoByRouteOrder(routeNode.Value));
			}
		}
	}

	public EventRoadBlock GetEventRoadBlock(int a_stage)
	{
		EventRoadBlock eventRoadBlock = null;
		for (int i = 0; i < mMapParts.Length; i++)
		{
			SMEventRallyPart sMEventRallyPart = mMapParts[i] as SMEventRallyPart;
			eventRoadBlock = sMEventRallyPart.GetEventRoadBlock(a_stage);
			if (eventRoadBlock != null)
			{
				break;
			}
		}
		return eventRoadBlock;
	}

	public void StartEventRoadBlockUnlockEffect(int a_roadStage)
	{
		mUnlockRBNo = a_roadStage;
		mState.Change(STATE.ROADBLOCK_UNLOCK);
	}

	protected override void RoadBlockUnlockProcess()
	{
		if (mState.IsChanged())
		{
			UnlockRoadBlock(mUnlockRBNo);
			mStateTime = 0f;
			mUnlockRBNo = 0;
			mGame.PlaySe("SE_RANKUP2", -1);
		}
		mStateTime += Time.deltaTime * 120f;
		if (mStateTime > 180f)
		{
			mStateTime = 180f;
			mState.Change(STATE.MAIN);
		}
	}

	public override void OnAvatarSave(string a_nodeName, int a_stageno, float a_pos)
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		PlayerMapData playerMapData = data.CourseData[mCourseID];
		playerMapData.AvatarNodeName = a_nodeName;
		playerMapData.AvatarStagePosition = a_stageno;
		playerMapData.AvatarPosition = a_pos;
		data.CourseData[CourseID] = playerMapData;
		mGame.mPlayer.Data.SetMapData(EventID, data);
		mGame.Save();
	}

	public override PlayerMapData OnGetMapData()
	{
		Player mPlayer = mGame.mPlayer;
		PlayerEventData data;
		mPlayer.Data.GetMapData(mSeries, EventID, out data);
		if (data == null)
		{
			return null;
		}
		return data.CourseData[mCourseID];
	}

	public override void StartNewAreaOpenEffect(int a_newarea)
	{
		mNewAreaNo = a_newarea;
		mState.Change(STATE.NEWAREA_OPEN);
	}

	public override void OpenNewArea(int a_page)
	{
		if (mMapParts.Length > a_page)
		{
			for (int i = 0; i < a_page; i++)
			{
				mMapParts[i].OpenNewAreaHide();
			}
			mMapParts[a_page].OpenNewArea();
		}
	}

	public override IEnumerator ActiveObjectInCamera(float y)
	{
		List<SMMapPart> atvList = new List<SMMapPart>();
		IsActiveObject = true;
		int mapIndex = NewMapSsDataEntity.GetMapIndex(Def.GetStageNoByRouteOrder(mAvater.GetCurrentNode().Value));
		for (int i = 0; i < mMapGameObject.Length; i++)
		{
			bool isNowActive = NGUITools.GetActive(mMapGameObject[i]);
			float distance = Mathf.Abs(mMapGameObject[i].gameObject.transform.localPosition.y + y);
			if ((double)distance < 1704.0)
			{
				if (!isNowActive && !mMapParts[i].IsActiveLoading)
				{
					yield return StartCoroutine(mMapParts[i].Active(NewMapSsDataEntity, i, mGame.RandomUserList.Count > 0 && mapIndex == i));
					atvList.Add(mMapParts[i]);
				}
			}
			else if (isNowActive)
			{
				mMapParts[i].Deactive();
			}
		}
		IsActiveObject = false;
	}

	protected override void UnlockWalkProcess()
	{
		if (mState.IsChanged())
		{
			if (!mIsUnlockAvatarMove)
			{
				mState.Change(STATE.MAIN);
				return;
			}
			if (mAvater != null)
			{
				int a_main;
				int a_sub;
				Def.SplitStageNo(mUnlockStageNo, out a_main, out a_sub);
				mAvater.MoveToStageButton(mUnlockStageNo, a_sub == 0);
				IsAvatorMoving = true;
			}
		}
		if (mAvater == null || !mAvater.IsMoving())
		{
			mAvater.FindPosition();
			if (mPrevButton != null)
			{
				mPrevButton.ChangeMode();
			}
			if (mUnlockButton != null)
			{
				mUnlockButton.ChangeMode();
			}
			mState.Change(STATE.MAIN);
		}
		else
		{
			if (!IsAvatorMoving || !(mAvater != null))
			{
				return;
			}
			Vector3 position = mAvater.Position;
			float x = base.gameObject.transform.localPosition.x;
			float y = base.gameObject.transform.localPosition.y;
			float z = base.gameObject.transform.localPosition.z;
			float num = position.y - y * -1f;
			if (num > 10f)
			{
				num = 10f;
			}
			else if (num < -10f)
			{
				num = -10f;
			}
			float num2 = y + num * -1f;
			if (mMaxAvailablePage > 1)
			{
				if (num2 > 0f)
				{
					num2 = mGameState.MAP_BOUNDS_RANGE * -0.1f;
				}
				else if (num2 < (float)(mMaxAvailablePage - 1) * 1136f * -1f)
				{
					num2 = (float)(mMaxAvailablePage - 1) * 1136f * -1f + mGameState.MAP_BOUNDS_RANGE * 0.1f;
				}
			}
			base.gameObject.transform.localPosition = new Vector3(x, num2, z);
		}
	}
}
