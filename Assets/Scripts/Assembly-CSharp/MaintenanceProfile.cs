using System;

public class MaintenanceProfile : ICloneable
{
	[MiniJSONAlias("time")]
	public long ServerTime { get; set; }

	[MiniJSONAlias("maintenance")]
	public int MaintenanceMode { get; set; }

	[MiniJSONAlias("maintenance_txt")]
	public string MaintenanceText { get; set; }

	[MiniJSONAlias("maintenance_url")]
	public string MaintenanceUrl { get; set; }

	[MiniJSONAlias("gc_maintenance")]
	public int Adv_MaintenanceMode { get; set; }

	public bool IsMaintenanceMode
	{
		get
		{
			return MaintenanceMode == 1 || IsServiceFinished;
		}
	}

	public bool IsAdvMaintenanceMode
	{
		get
		{
			return Adv_MaintenanceMode == 1 || IsServiceFinished;
		}
	}

	[MiniJSONAlias("gemsale")]
	public int GemSale { get; set; }

	[MiniJSONAlias("servicefinish")]
	public int ServiceFinish { get; set; }

	[MiniJSONAlias("servicefinish_url")]
	public string ServiceFinishUrl { get; set; }

	public bool IsServiceFinished
	{
		get
		{
			return ServiceFinish == 1;
		}
	}

	public MaintenanceProfile()
	{
		GemSale = 1;
		ServiceFinish = 0;
		ServiceFinishUrl = string.Empty;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public MaintenanceProfile Clone()
	{
		return MemberwiseClone() as MaintenanceProfile;
	}
}
