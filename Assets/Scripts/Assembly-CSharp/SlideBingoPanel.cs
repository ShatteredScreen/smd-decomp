using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideBingoPanel : MonoBehaviour
{
	public enum SW_STATE
	{
		INIT = 0,
		MAIN = 1,
		CURSOR_MOVE_INIT = 2,
		ORB_MOVE_INIT = 3,
		FLICK_MOVE_INIT = 4,
		MOVE = 5,
		WAIT = 6
	}

	public enum DIRECTION
	{
		LEFT = 0,
		RIGHT = 1
	}

	private class LockImgSettings
	{
		public UISprite lockSprite;

		public UISprite chainSprite1;

		public UISprite chainSprite2;

		public UISprite messageSprite;
	}

	private class BingoStageButtonInstance : MonoBehaviour
	{
		private enum BUTTON_STATE
		{
			MAIN = 0
		}

		public delegate void OnBingoStageButtonPushed(int i);

		public delegate void OnBingoStageButtonDisable();

		private const float OPENEFFECT_BASE_SCALE = 1f;

		private const float OPENEFFECT_RANGE_SCALE = 0.5f;

		private const float OPENEFFECT_SPEED = 360f;

		private OnBingoStageButtonPushed mCallback;

		private OnBingoStageButtonDisable mDisableCallback;

		public int index;

		private JellyImageButton button;

		private UISprite difficulty;

		private UISprite num;

		private UISprite icon;

		private bool buttonEnable;

		private BoxCollider collider;

		private int baseDepth;

		private bool selectThisStage;

		private BIJImage atlas;

		private UISprite currentSprite;

		private List<UISprite> shadowSpriteList;

		private string imgName = string.Empty;

		private float angle;

		private BUTTON_STATE state;

		private UISsSprite mUISsAnime;

		private bool mIsBingoStageButtonPushedInOpenEffect;

		public void Update()
		{
			if (state != 0)
			{
				return;
			}
			if (selectThisStage)
			{
				if (mUISsAnime == null)
				{
					mUISsAnime = Util.CreateGameObject("CursorSSAnime", button.gameObject).AddComponent<UISsSprite>();
					mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_BINGO_SELECT").SsAnime;
					mUISsAnime.depth = baseDepth + 4;
					mUISsAnime.Play();
				}
			}
			else if (mUISsAnime != null)
			{
				UnityEngine.Object.Destroy(mUISsAnime.gameObject);
				mUISsAnime = null;
			}
		}

		public static BingoStageButtonInstance make(GameObject _parent, int _stageNo, int _buttonNo, int _gotStar, float _posX, float _posY, BIJImage _Atlas, int _baseDepth, string _ImgName, short _difficulty, string _iconName)
		{
			UIButton uIButton = Util.CreateJellyImageButton("BingoStageButton" + _stageNo, _parent, _Atlas);
			BingoStageButtonInstance bingoStageButtonInstance = uIButton.gameObject.AddComponent<BingoStageButtonInstance>();
			Util.SetImageButtonInfo(uIButton, _ImgName, _ImgName, _ImgName, _baseDepth + 3, new Vector3(_posX, _posY, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, bingoStageButtonInstance, "OnBingoStageButtonCallBack", UIButtonMessage.Trigger.OnClick);
			bingoStageButtonInstance.button = uIButton.GetComponent<JellyImageButton>();
			bingoStageButtonInstance.index = _stageNo;
			bingoStageButtonInstance.baseDepth = _baseDepth;
			bingoStageButtonInstance.atlas = _Atlas;
			UISprite sprite = Util.CreateSprite("Difficulty", uIButton.gameObject, _Atlas);
			Util.SetSpriteInfo(sprite, "LC_event_btn_lv0" + _difficulty, _baseDepth + 4, new Vector3(0f, 46f, 0f), Vector3.one, false);
			bingoStageButtonInstance.difficulty = sprite;
			UISprite sprite2 = Util.CreateSprite("Num", uIButton.gameObject, _Atlas);
			Util.SetSpriteInfo(sprite2, "event_btn_num" + _buttonNo, _baseDepth + 4, new Vector3(0f, 0f, 0f), Vector3.one, false);
			bingoStageButtonInstance.num = sprite2;
			if (_gotStar != -1)
			{
				if (_gotStar == 0)
				{
					UISprite sprite3 = Util.CreateSprite("Star3", uIButton.gameObject, _Atlas);
					Util.SetSpriteInfo(sprite3, "event04_star", _baseDepth + 5, new Vector3(35f, -47f, 0f), Vector3.one, false);
					UISprite sprite4 = Util.CreateSprite("Star2", uIButton.gameObject, _Atlas);
					Util.SetSpriteInfo(sprite4, "event04_star", _baseDepth + 5, new Vector3(0f, -47f, 0f), Vector3.one, false);
					UISprite sprite5 = Util.CreateSprite("Star1", uIButton.gameObject, _Atlas);
					Util.SetSpriteInfo(sprite5, "event04_star", _baseDepth + 5, new Vector3(-35f, -47f, 0f), Vector3.one, false);
				}
				else
				{
					if (_gotStar == 3)
					{
						UISprite sprite6 = Util.CreateSprite("Star3", uIButton.gameObject, _Atlas);
						Util.SetSpriteInfo(sprite6, "event04_star2", _baseDepth + 5, new Vector3(35f, -47f, 0f), Vector3.one, false);
					}
					else
					{
						UISprite sprite7 = Util.CreateSprite("Star3", uIButton.gameObject, _Atlas);
						Util.SetSpriteInfo(sprite7, "event04_star", _baseDepth + 5, new Vector3(35f, -47f, 0f), Vector3.one, false);
					}
					if (_gotStar >= 2)
					{
						UISprite sprite8 = Util.CreateSprite("Star2", uIButton.gameObject, _Atlas);
						Util.SetSpriteInfo(sprite8, "event04_star2", _baseDepth + 5, new Vector3(0f, -47f, 0f), Vector3.one, false);
					}
					else
					{
						UISprite sprite9 = Util.CreateSprite("Star2", uIButton.gameObject, _Atlas);
						Util.SetSpriteInfo(sprite9, "event04_star", _baseDepth + 5, new Vector3(0f, -47f, 0f), Vector3.one, false);
					}
					UISprite sprite10 = Util.CreateSprite("Star1", uIButton.gameObject, _Atlas);
					Util.SetSpriteInfo(sprite10, "event04_star2", _baseDepth + 5, new Vector3(-35f, -47f, 0f), Vector3.one, false);
				}
			}
			if (_iconName != string.Empty)
			{
				BIJImage image = ResourceManager.LoadImage("ICON").Image;
				UISprite sprite11 = Util.CreateSprite("Icon", uIButton.gameObject, image);
				Util.SetSpriteInfo(sprite11, _iconName, _baseDepth + 6, new Vector3(-37f, -37f, 0f), new Vector3(0.36f, 0.36f, 1f), false);
				bingoStageButtonInstance.icon = sprite11;
			}
			bingoStageButtonInstance.buttonEnable = true;
			bingoStageButtonInstance.collider = uIButton.GetComponent<BoxCollider>();
			return bingoStageButtonInstance;
		}

		public void setCallBack(OnBingoStageButtonPushed _mCallback, OnBingoStageButtonDisable _mDisableCallback)
		{
			mCallback = _mCallback;
			mDisableCallback = _mDisableCallback;
		}

		private void OnBingoStageButtonCallBack()
		{
			if (buttonEnable)
			{
				mIsBingoStageButtonPushedInOpenEffect = true;
				SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_LEVEL_BUTTON", -1);
				if (mCallback != null)
				{
					mCallback(index);
				}
			}
			else
			{
				SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_LEVEL_LOCK", -1);
				if (mDisableCallback != null)
				{
					mDisableCallback();
				}
			}
		}

		public void SetStageButtonEnable(bool enable, bool a_isAnimate)
		{
			button.SetButtonEnable(enable);
			buttonEnable = enable;
			if (enable)
			{
				difficulty.color = new Color(1f, 1f, 1f);
				num.color = new Color(1f, 1f, 1f);
				if (icon != null)
				{
					icon.color = new Color(1f, 1f, 1f);
				}
				if (a_isAnimate)
				{
					StartCoroutine(StartOpenEffect());
				}
			}
			else
			{
				difficulty.color = new Color(0.49803922f, 0.49803922f, 0.49803922f);
				num.color = new Color(0.49803922f, 0.49803922f, 0.49803922f);
				if (icon != null)
				{
					icon.color = new Color(0.49803922f, 0.49803922f, 0.49803922f);
				}
			}
		}

		private IEnumerator StartOpenEffect()
		{
			List<UIWidget> widgets2 = new List<UIWidget>();
			GetComponentsInChildren(widgets2);
			UIWidget widget = GetComponent<UIWidget>();
			if (widget != null && !widgets2.Contains(widget))
			{
				widgets2.Add(widget);
			}
			for (int j = 0; j < widgets2.Count; j++)
			{
				widgets2[j].depth += 10;
			}
			float deg = 0f;
			int cnt = 0;
			float bounds_scale = 1f;
			while (true)
			{
				deg += Time.deltaTime * 360f;
				float rad = deg * ((float)Math.PI / 180f);
				float sin = Mathf.Sin(rad);
				float scale_x = 1.5f - 0.5f * sin * bounds_scale;
				float scale_y = 1.5f - 0.5f * sin * bounds_scale;
				if (deg > 90f)
				{
					deg = 0f;
					cnt++;
					bounds_scale *= 0.3f;
					if (cnt > 0)
					{
						break;
					}
				}
				if (mIsBingoStageButtonPushedInOpenEffect)
				{
					break;
				}
				base.transform.localScale = new Vector3(scale_x, scale_y, 1f);
				yield return null;
			}
			for (int i = 0; i < widgets2.Count; i++)
			{
				widgets2[i].depth -= 10;
			}
			widgets2.RemoveAll((UIWidget a) => a != null);
			widgets2 = null;
			base.transform.localScale = new Vector3(1f, 1f, 1f);
			mIsBingoStageButtonPushedInOpenEffect = false;
		}

		public void SetColliderEnable(bool enable)
		{
			collider.enabled = enable;
		}

		public void SetSelectStage(bool enable)
		{
			selectThisStage = enable;
		}
	}

	private class OrbButtonInstance : MonoBehaviour
	{
		public delegate void OnOrbButtonPushed(int i, bool force);

		private OnOrbButtonPushed mCallback;

		public int index;

		public UISprite sprite;

		public UIButton button;

		public static OrbButtonInstance make(GameObject _parent, int _index, float _posX, BIJImage _Atlas, int _baseDepth, string _ImgName)
		{
			UIButton uIButton = Util.CreateJellyImageButton("orb" + _index, _parent, _Atlas);
			OrbButtonInstance orbButtonInstance = uIButton.gameObject.AddComponent<OrbButtonInstance>();
			orbButtonInstance.button = uIButton;
			Util.SetImageButtonInfo(orbButtonInstance.button, _ImgName, _ImgName, _ImgName, _baseDepth + 3, new Vector3(_posX, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(orbButtonInstance.button, orbButtonInstance, "OnOrbButtonCallBack", UIButtonMessage.Trigger.OnClick);
			orbButtonInstance.sprite = orbButtonInstance.button.gameObject.GetComponent<UISprite>();
			orbButtonInstance.index = _index;
			return orbButtonInstance;
		}

		public void setCallBack(OnOrbButtonPushed _mCallback)
		{
			mCallback = _mCallback;
		}

		private void OnOrbButtonCallBack()
		{
			mCallback(index, false);
		}
	}

	public delegate void OnIndexButtonPushed(int i);

	public delegate void OnSlideMoved(short i);

	public delegate void OnIndexButtonDisable();

	public delegate void OnSetStateWait();

	public delegate void OnSlideStart(short i);

	private SW_STATE mState = SW_STATE.WAIT;

	public bool PanelNoMove;

	private int mBaseDepth = 10;

	private float mClipX = 452f;

	private float mClipY = 585f;

	private float mMoveX = 512f;

	private FlickWindow mFlick;

	private Dictionary<int, short> mCourseSlideDict = new Dictionary<int, short>();

	public DIRECTION mSlideDirection = DIRECTION.RIGHT;

	public float mCursorPos = 33f;

	private Util.TriFunc mCursorTriFunc;

	private UIButton mLeftCursor;

	private UIButton mRightCursor;

	private string mOrbImgName;

	private BIJImage mOrbAtlas;

	private GameObject mOrbRoot;

	private float mOrbPosX;

	public float mOrbPos = -15f;

	private List<JellyImageButton> mOrbGoList = new List<JellyImageButton>();

	private Dictionary<short, List<SMMapStageSetting>> mBingoPanelDict = new Dictionary<short, List<SMMapStageSetting>>();

	private BIJImage mAtlasMap;

	private int mEventId;

	private SortedDictionary<string, SMEventBingoStageSetting> mBingoStageDict;

	private List<short> mLotteryNumberList;

	private List<short> mNotOpenCourse;

	private Dictionary<string, BingoStageButtonInstance> mBingoStageBunttonInstDict = new Dictionary<string, BingoStageButtonInstance>();

	public bool mAutoMoveEnable;

	public int mAutoMoveSec = 6;

	private DateTime mAutoMoveStartTime;

	private OnIndexButtonPushed mCallback;

	private OnSlideMoved mOnSlideMovedCallback;

	private OnIndexButtonDisable mDisableCallback;

	private OnSetStateWait mSetStateWaitCallback;

	private OnSlideStart mSlideStartCallBack;

	private GameObject mListRoot;

	private int mSlideCount;

	private bool mSlideEnable = true;

	private int mNowViewPanelId;

	private int mMovedSlidePosX;

	private int mMoveValue;

	private int mMoveDirection = 1;

	private int mMovedPanelId;

	private int mReserveMovedPanelId = -1;

	private bool mIsForceMove;

	private List<BoxCollider> mColliderList = new List<BoxCollider>();

	private bool mFlickNow;

	private float mTheLastNowPosX;

	private bool mEnableButtonNow = true;

	private int mTotalStarNum;

	private List<int> mSheetTotalStartList = new List<int>();

	private Dictionary<short, LockImgSettings> mLockSpriteDict = new Dictionary<short, LockImgSettings>();

	private bool mForceReturnPanel;

	private bool mNoSwipeSe;

	private bool mIsFlickStart;

	private float mFlickDistance;

	private List<bool> mReachLineEffectFinishedList = new List<bool>();

	private List<bool> mBingoLineEffectFinishedList = new List<bool>();

	private short[] mBingoLine = new short[24]
	{
		1, 2, 3, 4, 5, 6, 7, 8, 9, 1,
		4, 7, 2, 5, 8, 3, 6, 9, 1, 5,
		9, 3, 5, 7
	};

	private bool mNowPlayBingoLineEffect;

	private bool sheetOpenFinishedFlg;

	public bool IsPanelNoMove
	{
		get
		{
			return !mFlickNow && mEnableButtonNow;
		}
	}

	private int mConstDirection
	{
		get
		{
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				return 1;
			}
			return -1;
		}
	}

	public float mPosX
	{
		get
		{
			if (mListRoot == null)
			{
				return 0f;
			}
			return mListRoot.transform.localPosition.x;
		}
		private set
		{
		}
	}

	public bool InitMove { private get; set; }

	public void Start()
	{
		mState = SW_STATE.INIT;
	}

	public void Update()
	{
		switch (mState)
		{
		case SW_STATE.INIT:
			CreateSlideBingoPanel();
			mState = SW_STATE.MAIN;
			break;
		case SW_STATE.MAIN:
			SetLayout();
			if (mReserveMovedPanelId != -1)
			{
				int index = mReserveMovedPanelId;
				mReserveMovedPanelId = -1;
				OnOrbButtonPushedCallBack(index, true);
			}
			break;
		case SW_STATE.CURSOR_MOVE_INIT:
			mCursorTriFunc.SetDegree(0f);
			mLeftCursor.transform.localScale = Vector3.one;
			mRightCursor.transform.localScale = Vector3.one;
			NGUITools.SetActive(mLeftCursor.gameObject, false);
			NGUITools.SetActive(mRightCursor.gameObject, false);
			mMovedSlidePosX = (int)(mListRoot.transform.localPosition.x + mMoveX * (float)mMoveDirection);
			mMoveValue = (int)mMoveX / 8;
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				if (mMoveDirection == -1)
				{
					mMovedPanelId = mNowViewPanelId + 1;
				}
				else
				{
					mMovedPanelId = mNowViewPanelId - 1;
				}
			}
			else if (mMoveDirection == -1)
			{
				mMovedPanelId = mNowViewPanelId - 1;
			}
			else
			{
				mMovedPanelId = mNowViewPanelId + 1;
			}
			mState = SW_STATE.MOVE;
			mSlideStartCallBack(mCourseSlideDict[mMovedPanelId]);
			SingletonMonoBehaviour<GameMain>.Instance.StopSe(3);
			SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_BINGO_SWIPE", 3);
			break;
		case SW_STATE.ORB_MOVE_INIT:
		{
			mCursorTriFunc.SetDegree(0f);
			mLeftCursor.transform.localScale = Vector3.one;
			mRightCursor.transform.localScale = Vector3.one;
			NGUITools.SetActive(mLeftCursor.gameObject, false);
			NGUITools.SetActive(mRightCursor.gameObject, false);
			int num3;
			if (mSlideDirection == DIRECTION.RIGHT)
			{
				if (mMovedPanelId > mNowViewPanelId)
				{
					num3 = mMovedPanelId - mNowViewPanelId;
					mMoveDirection = -1;
				}
				else
				{
					num3 = mNowViewPanelId - mMovedPanelId;
					mMoveDirection = 1;
				}
			}
			else if (mMovedPanelId > mNowViewPanelId)
			{
				num3 = mMovedPanelId - mNowViewPanelId;
				mMoveDirection = 1;
			}
			else
			{
				num3 = mNowViewPanelId - mMovedPanelId;
				mMoveDirection = -1;
			}
			mMovedSlidePosX = (int)(mMoveX * (float)(mConstDirection * -1)) * mMovedPanelId;
			mMoveValue = (int)(mMoveX * (float)num3) / (8 + (num3 - 1));
			mState = SW_STATE.MOVE;
			mSlideStartCallBack(mCourseSlideDict[mMovedPanelId]);
			if (!mNoSwipeSe)
			{
				SingletonMonoBehaviour<GameMain>.Instance.StopSe(3);
				SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_BINGO_SWIPE", 3);
			}
			else
			{
				mNoSwipeSe = false;
			}
			break;
		}
		case SW_STATE.FLICK_MOVE_INIT:
		{
			float num2 = mListRoot.transform.localPosition.x - mTheLastNowPosX;
			bool flag = true;
			if (num2 >= 0f)
			{
				if (!mForceReturnPanel && num2 > mMoveX / 4f && mNowViewPanelId != 0)
				{
					mMoveDirection = 1;
					if (mSlideDirection == DIRECTION.RIGHT)
					{
						if (mMoveDirection == -1)
						{
							mMovedPanelId = mNowViewPanelId + 1;
						}
						else
						{
							mMovedPanelId = mNowViewPanelId - 1;
						}
					}
					else if (mMoveDirection == -1)
					{
						mMovedPanelId = mNowViewPanelId - 1;
					}
					else
					{
						mMovedPanelId = mNowViewPanelId + 1;
					}
				}
				else
				{
					mMoveDirection = -1;
					mMovedPanelId = mNowViewPanelId;
					flag = false;
				}
			}
			else if (!mForceReturnPanel && num2 < mMoveX / 4f * -1f && mNowViewPanelId != mSlideCount - 1)
			{
				mMoveDirection = -1;
				if (mSlideDirection == DIRECTION.RIGHT)
				{
					if (mMoveDirection == -1)
					{
						mMovedPanelId = mNowViewPanelId + 1;
					}
					else
					{
						mMovedPanelId = mNowViewPanelId - 1;
					}
				}
				else if (mMoveDirection == -1)
				{
					mMovedPanelId = mNowViewPanelId - 1;
				}
				else
				{
					mMovedPanelId = mNowViewPanelId + 1;
				}
			}
			else
			{
				mMoveDirection = 1;
				mMovedPanelId = mNowViewPanelId;
				flag = false;
			}
			mForceReturnPanel = false;
			mMovedSlidePosX = (int)(mMoveX * (float)(mConstDirection * -1)) * mMovedPanelId;
			mMoveValue = (int)mMoveX / 8;
			mState = SW_STATE.MOVE;
			if (flag)
			{
				mSetStateWaitCallback();
				mSlideStartCallBack(mCourseSlideDict[mMovedPanelId]);
				SingletonMonoBehaviour<GameMain>.Instance.StopSe(3);
				SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_BINGO_SWIPE", 3);
			}
			break;
		}
		case SW_STATE.MOVE:
		{
			if (mListRoot.transform.localPosition.x == (float)mMovedSlidePosX)
			{
				mNowViewPanelId = mMovedPanelId;
				ChangeCursorStatus();
				ChangeOrbStatus();
				mIsForceMove = false;
				mSlideEnable = true;
				if (mOnSlideMovedCallback != null)
				{
					mOnSlideMovedCallback(mCourseSlideDict[mMovedPanelId]);
				}
				mState = SW_STATE.MAIN;
			}
			int num = (int)mListRoot.transform.localPosition.x + mMoveValue * mMoveDirection;
			if (num * mMoveDirection <= mMovedSlidePosX * mMoveDirection)
			{
				mListRoot.transform.localPosition = new Vector3(num, 0f, 0f);
			}
			else
			{
				mListRoot.transform.localPosition = new Vector3(mMovedSlidePosX, 0f, 0f);
			}
			break;
		}
		}
		if (mSlideEnable && mCursorTriFunc != null)
		{
			mCursorTriFunc.AddDegree(Time.deltaTime * 180f);
			float num4 = 0.15f * mCursorTriFunc.Sin();
			if (mRightCursor != null && mRightCursor.gameObject.activeSelf)
			{
				mRightCursor.transform.localScale = new Vector3(1f + num4, 1f + num4, 1f);
			}
			if (mLeftCursor != null && mLeftCursor.gameObject.activeSelf)
			{
				mLeftCursor.transform.localScale = new Vector3(1f + num4, 1f + num4, 1f);
			}
		}
		PanelNoMove = IsPanelNoMove;
	}

	public void Init(int _eventId, BIJImage _Atlas, SortedDictionary<string, SMEventBingoStageSetting> _bingoStageDict, List<short> _lotteryNumberList, List<short> _notOpenCourseList)
	{
		mEventId = _eventId;
		mAtlasMap = _Atlas;
		mBingoStageDict = _bingoStageDict;
		mLotteryNumberList = new List<short>();
		for (int i = 0; i < _lotteryNumberList.Count; i++)
		{
			mLotteryNumberList.Add(_lotteryNumberList[i]);
		}
		mNotOpenCourse = new List<short>();
		for (int j = 0; j < _notOpenCourseList.Count; j++)
		{
			mNotOpenCourse.Add(_notOpenCourseList[j]);
		}
	}

	public void setCallBack(OnIndexButtonPushed _mCallback, OnIndexButtonDisable _mDisableCallback, OnSetStateWait _mSetStateWaitCallback, OnSlideStart _mSlideStartCallBack)
	{
		mCallback = _mCallback;
		mDisableCallback = _mDisableCallback;
		mSetStateWaitCallback = _mSetStateWaitCallback;
		mSlideStartCallBack = _mSlideStartCallBack;
	}

	public void setSlideMovedCallBack(OnSlideMoved _mCallback)
	{
		mOnSlideMovedCallback = _mCallback;
	}

	public void AddSlideBingoPanel(short courseNum, List<SMMapStageSetting> stageSetList)
	{
		if (mBingoPanelDict != null && stageSetList != null)
		{
			mBingoPanelDict[courseNum] = stageSetList;
		}
	}

	private void CreateSlideBingoPanel()
	{
		GameObject gameObject = Util.CreateGameObject("PanelListGo", base.gameObject);
		gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
		UIPanel uIPanel = gameObject.AddComponent<UIPanel>();
		uIPanel.sortingOrder = mBaseDepth - 2;
		uIPanel.depth = mBaseDepth;
		uIPanel.clipping = UIDrawCall.Clipping.SoftClip;
		uIPanel.baseClipRegion = new Vector4(0f, 0f, mClipX, mClipY);
		uIPanel.clipSoftness = new Vector2(10f, 0f);
		mListRoot = Util.CreateGameObject("List", gameObject);
		mListRoot.transform.localPosition = new Vector3(0f, 0f, 0f);
		mSlideCount = mBingoPanelDict.Count;
		float num = 0f;
		List<short> list = new List<short>(mBingoPanelDict.Keys);
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		for (int i = 0; i < list.Count; i++)
		{
			UISprite uISprite = Util.CreateSprite("BingoBoard", mListRoot, mAtlasMap);
			Util.SetSpriteInfo(uISprite, "event_instruction_panel00", mBaseDepth, new Vector3(num, 0f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions((int)mClipX, (int)mClipY);
			short num2 = list[i];
			List<SMMapStageSetting> list2 = mBingoPanelDict[num2];
			mCourseSlideDict[i] = num2;
			float num3 = -135f;
			float posY = 85f;
			UISprite sprite = Util.CreateSprite("BoardNum", uISprite.gameObject, mAtlasMap);
			Util.SetSpriteInfo(sprite, "event_instruction_no" + (num2 + 1), mBaseDepth + 1, new Vector3(-84f, 203f, 0f), Vector3.one, false);
			UISprite uISprite2 = Util.CreateSprite("slash", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, "event02_num_slash", mBaseDepth + 1, new Vector3(143f, 186f, 0f), Vector3.one, false);
			uISprite2.gameObject.transform.localScale = new Vector3(0.9f, 0.9f);
			uISprite2 = Util.CreateSprite("Denomi10", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, "event2_num2", mBaseDepth + 1, new Vector3(159f, 186f, 0f), Vector3.one, false);
			uISprite2.gameObject.transform.localScale = new Vector3(0.9f, 0.9f);
			uISprite2 = Util.CreateSprite("Denomi1", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, "event2_num7", mBaseDepth + 1, new Vector3(177f, 186f, 0f), Vector3.one, false);
			uISprite2.gameObject.transform.localScale = new Vector3(0.9f, 0.9f);
			int num4 = 0;
			for (int j = 0; j < list2.Count; j++)
			{
				switch (j)
				{
				case 3:
					num3 = -135f;
					posY = -45f;
					break;
				case 6:
					num3 = -135f;
					posY = -175f;
					break;
				default:
					num3 += 135f;
					break;
				case 0:
					break;
				}
				SMMapStageSetting sMMapStageSetting = list2[j];
				short difficulty = 0;
				string key = string.Format("{0:D4}", sMMapStageSetting.mStageNo + 1000 * num2);
				if (mBingoStageDict != null && mBingoStageDict.ContainsKey(key))
				{
					difficulty = mBingoStageDict[key].difficulty;
				}
				PlayerClearData _psd = null;
				SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, mEventId, num2, Def.GetStageNo(j + 1, 0), out _psd);
				bool enable = false;
				bool selectStage = false;
				int num5 = -1;
				string imgName = "event_btn00";
				if (_psd != null)
				{
					num5 = _psd.GotStars;
					enable = true;
					num4 += num5;
					imgName = "event_btn01";
				}
				else if (mLotteryNumberList.Count > num2 && mLotteryNumberList[num2] == sMMapStageSetting.mStageNo)
				{
					enable = true;
					selectStage = true;
				}
				string iconName = string.Empty;
				if (_psd == null && sMMapStageSetting.StageEnterCondition.CompareTo("PARTNER_Z") == 0 && !string.IsNullOrEmpty(sMMapStageSetting.StageEnterConditionID))
				{
					string[] array = sMMapStageSetting.StageEnterConditionID.Split(',');
					CompanionData companionData = SingletonMonoBehaviour<GameMain>.Instance.mCompanionData[int.Parse(array[0])];
					iconName = companionData.iconName;
				}
				BingoStageButtonInstance bingoStageButtonInstance = BingoStageButtonInstance.make(uISprite.gameObject, sMMapStageSetting.mStageNo, j + 1, num5, num3, posY, mAtlasMap, mBaseDepth, imgName, difficulty, iconName);
				bingoStageButtonInstance.setCallBack(OnIndexButtonPushedCallBack, OnIndexButtonDisableCallBack);
				bingoStageButtonInstance.SetStageButtonEnable(enable, false);
				bingoStageButtonInstance.SetSelectStage(selectStage);
				mBingoStageBunttonInstDict[key] = bingoStageButtonInstance;
			}
			if (num4 / 10 != 0)
			{
				UISprite uISprite3 = Util.CreateSprite("Mole10", uISprite.gameObject, image);
				Util.SetSpriteInfo(uISprite3, "event2_num" + num4 / 10, mBaseDepth + 1, new Vector3(109f, 186f, 0f), Vector3.one, false);
				uISprite3.gameObject.transform.localScale = new Vector3(0.9f, 0.9f);
			}
			UISprite uISprite4 = Util.CreateSprite("Mole1", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite4, "event2_num" + num4 % 10, mBaseDepth + 1, new Vector3(127f, 186f, 0f), Vector3.one, false);
			uISprite4.gameObject.transform.localScale = new Vector3(0.9f, 0.9f);
			for (int k = 0; k < mNotOpenCourse.Count; k++)
			{
				if (mNotOpenCourse[k] == num2)
				{
					SetCourseEnable(num2, false);
					LockImgSettings lockImgSettings = new LockImgSettings();
					UISprite uISprite5 = Util.CreateSprite("Lock", uISprite.gameObject, mAtlasMap);
					Util.SetSpriteInfo(uISprite5, "sheet_lock", mBaseDepth + 15, new Vector3(0f, 0f, 0f), Vector3.one, false);
					lockImgSettings.lockSprite = uISprite5;
					UISprite uISprite6 = Util.CreateSprite("Chain1", uISprite.gameObject, mAtlasMap);
					Util.SetSpriteInfo(uISprite6, "sheet_lock02", mBaseDepth + 10, new Vector3(0f, -15f, 0f), Vector3.one, false);
					uISprite6.transform.Rotate(0f, 0f, 42f);
					lockImgSettings.chainSprite1 = uISprite6;
					uISprite6 = Util.CreateSprite("Chain2", uISprite.gameObject, mAtlasMap);
					Util.SetSpriteInfo(uISprite6, "sheet_lock02", mBaseDepth + 10, new Vector3(0f, -20f, 0f), Vector3.one, false);
					uISprite6.transform.Rotate(0f, 0f, -43f);
					lockImgSettings.chainSprite2 = uISprite6;
					UISprite uISprite7 = Util.CreateSprite("Message", uISprite.gameObject, mAtlasMap);
					Util.SetSpriteInfo(uISprite7, "LC_event_instruction_panel03", mBaseDepth + 20, new Vector3(0f, -110f, 0f), Vector3.one, false);
					lockImgSettings.messageSprite = uISprite7;
					mLockSpriteDict[num2] = lockImgSettings;
				}
			}
			num += 512f;
			mTotalStarNum += num4;
			mSheetTotalStartList.Add(num4);
		}
		mFlick = base.gameObject.AddComponent<FlickWindow>();
		Vector3 vector = Camera.main.WorldToScreenPoint(base.gameObject.transform.position);
		float _posX;
		float _posY;
		GetObjectScreenSize(new Vector3(mClipX, mClipY), out _posX, out _posY);
		mFlick.Init(vector.x - _posX / 2f, vector.y - _posY / 2f, _posX, _posY);
		mFlick.SetFlickStartCallBack(GetFlickPos);
		mFlick.SetFlickEndCallBack(EndFlick);
		mLeftCursor = Util.CreateJellyImageButton("LeftCursor", base.gameObject, mAtlasMap);
		Util.SetImageButtonInfo(mLeftCursor, "btn_banner00", "btn_banner00", "btn_banner00", mBaseDepth + 5, new Vector3(0f - mCursorPos, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mLeftCursor, this, "OnLeft_Callback", UIButtonMessage.Trigger.OnClick);
		mLeftCursor.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
		mLeftCursor.transform.localPosition = new Vector3(mLeftCursor.transform.localPosition.x - mClipX / 2f, mLeftCursor.transform.localPosition.y, mLeftCursor.transform.localPosition.z);
		NGUITools.SetActive(mLeftCursor.gameObject, false);
		mColliderList.Add(mLeftCursor.gameObject.GetComponent<BoxCollider>());
		mRightCursor = Util.CreateJellyImageButton("RightCursor", base.gameObject, mAtlasMap);
		Util.SetImageButtonInfo(mRightCursor, "btn_banner00", "btn_banner00", "btn_banner00", mBaseDepth + 5, new Vector3(mCursorPos, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mRightCursor, this, "OnRight_Callback", UIButtonMessage.Trigger.OnClick);
		mRightCursor.transform.localPosition = new Vector3(mRightCursor.transform.localPosition.x + mClipX / 2f, mRightCursor.transform.localPosition.y, mRightCursor.transform.localPosition.z);
		NGUITools.SetActive(mRightCursor.gameObject, false);
		mColliderList.Add(mRightCursor.gameObject.GetComponent<BoxCollider>());
		mCursorTriFunc = Util.CreateTriFunc(0f, 180f, Util.TriFunc.MAXFUNC.LOOP);
		ChangeCursorStatus();
		mOrbRoot = Util.CreateGameObject("OrbList", base.gameObject);
		mOrbRoot.transform.localPosition = new Vector3(0f, mOrbPos - mClipY / 2f, 0f);
		float x = 0f;
		for (int l = 0; l < mSlideCount; l++)
		{
			OrbButtonInstance orbButtonInstance = OrbButtonInstance.make(mOrbRoot, l, mOrbPosX, mAtlasMap, mBaseDepth + 5, "event_load_dot");
			orbButtonInstance.setCallBack(OnOrbButtonPushedCallBack);
			mOrbGoList.Add(orbButtonInstance.button.GetComponent<JellyImageButton>());
			mColliderList.Add(orbButtonInstance.button.gameObject.GetComponent<BoxCollider>());
			mOrbPosX += (float)mConstDirection * ((float)orbButtonInstance.sprite.width * 2f);
			mOrbRoot.transform.localPosition = new Vector3(x, mOrbRoot.transform.localPosition.y, 0f);
			x = mOrbRoot.transform.localPosition.x - (float)mConstDirection * ((float)orbButtonInstance.sprite.width * 1f);
		}
		ChangeOrbStatus();
	}

	private void ChangeCursorStatus()
	{
		if (mEnableButtonNow)
		{
			NGUITools.SetActive(mLeftCursor.gameObject, true);
			NGUITools.SetActive(mRightCursor.gameObject, true);
			if (mNowViewPanelId == 0)
			{
				if (mSlideDirection == DIRECTION.RIGHT)
				{
					NGUITools.SetActive(mLeftCursor.gameObject, false);
				}
				else
				{
					NGUITools.SetActive(mRightCursor.gameObject, false);
				}
			}
			else if (mNowViewPanelId == mSlideCount - 1)
			{
				if (mSlideDirection == DIRECTION.RIGHT)
				{
					NGUITools.SetActive(mRightCursor.gameObject, false);
				}
				else
				{
					NGUITools.SetActive(mLeftCursor.gameObject, false);
				}
			}
		}
		else
		{
			mCursorTriFunc.SetDegree(0f);
			mLeftCursor.transform.localScale = Vector3.one;
			mRightCursor.transform.localScale = Vector3.one;
			NGUITools.SetActive(mLeftCursor.gameObject, false);
			NGUITools.SetActive(mRightCursor.gameObject, false);
		}
	}

	private void ChangeOrbStatus()
	{
		for (int i = 0; i < mOrbGoList.Count; i++)
		{
			if (i == mNowViewPanelId)
			{
				mOrbGoList[i].SetButtonEnable(true);
			}
			else
			{
				mOrbGoList[i].SetButtonEnable(false);
			}
		}
	}

	private void OnIndexButtonPushedCallBack(int index)
	{
		if (mCallback != null)
		{
			mCallback(index);
		}
	}

	private void OnIndexButtonDisableCallBack()
	{
		if (mDisableCallback != null)
		{
			mDisableCallback();
		}
	}

	private void OnOrbButtonPushedCallBack(int index, bool force)
	{
		if (!mSlideEnable || mNowViewPanelId == index)
		{
			if (force && mNowViewPanelId != index)
			{
				mReserveMovedPanelId = index;
			}
		}
		else if (mEnableButtonNow || force)
		{
			mSetStateWaitCallback();
			mSlideEnable = false;
			mMovedPanelId = index;
			mIsForceMove = force;
			mState = SW_STATE.ORB_MOVE_INIT;
		}
	}

	private void OnRight_Callback()
	{
		if (mSlideEnable && mEnableButtonNow)
		{
			mSetStateWaitCallback();
			mSlideEnable = false;
			mMoveDirection = -1;
			mState = SW_STATE.CURSOR_MOVE_INIT;
		}
	}

	private void OnLeft_Callback()
	{
		if (mSlideEnable && mEnableButtonNow)
		{
			mSetStateWaitCallback();
			mSlideEnable = false;
			mMoveDirection = 1;
			mState = SW_STATE.CURSOR_MOVE_INIT;
		}
	}

	public void SetEnableButtonAction(bool buttonflg)
	{
		if (mFlickNow)
		{
			mForceReturnPanel = true;
			EndFlick();
			if (mFlick != null)
			{
				mFlick.FlickClear();
			}
		}
		for (int i = 0; i < mColliderList.Count; i++)
		{
			mColliderList[i].enabled = buttonflg;
		}
		mEnableButtonNow = buttonflg;
		List<short> list = new List<short>(mBingoPanelDict.Keys);
		if (buttonflg)
		{
			for (short num = 0; num < list.Count; num++)
			{
				bool flag = false;
				short num2 = list[num];
				for (short num3 = 0; num3 < mNotOpenCourse.Count; num3++)
				{
					if (mNotOpenCourse[num3] == num2)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					SetCourseEnable(num2, true);
				}
			}
		}
		else
		{
			for (short num4 = 0; num4 < list.Count; num4++)
			{
				short courseNum = list[num4];
				SetCourseEnable(courseNum, false);
			}
		}
		ChangeCursorStatus();
	}

	private void GetFlickPos(float _directionX, float _directionY)
	{
		if (mEnableButtonNow && (mSlideEnable || mFlickNow))
		{
			if (!mIsFlickStart)
			{
				mIsFlickStart = true;
				mFlickDistance = 0f;
				mTheLastNowPosX = mListRoot.transform.localPosition.x;
			}
			mFlickDistance = _directionX * _directionX + _directionY * _directionY;
			if (mFlickDistance > 50f)
			{
				mFlickDistance = 50f;
			}
			if (mFlickDistance > 9f && !mFlickNow)
			{
				mFlickNow = true;
				mSlideEnable = false;
			}
			if (_directionX <= mClipX - 1f && _directionX >= (mClipX - 1f) * -1f)
			{
				float x = mTheLastNowPosX + _directionX;
				mListRoot.transform.localPosition = new Vector3(x, 0f, 0f);
			}
		}
	}

	private void EndFlick()
	{
		mFlickNow = false;
		mIsFlickStart = false;
		if (mEnableButtonNow)
		{
			mState = SW_STATE.FLICK_MOVE_INIT;
		}
	}

	public void ForceEndFlick()
	{
		if (!InitMove && !mIsForceMove)
		{
			if (!mFlickNow && !mIsFlickStart)
			{
				mTheLastNowPosX = mListRoot.transform.localPosition.x;
			}
			mFlickNow = false;
			mIsFlickStart = false;
			mSlideEnable = false;
			if (mFlick != null)
			{
				mFlick.FlickClear();
			}
			mState = SW_STATE.FLICK_MOVE_INIT;
		}
	}

	private void SetLayout()
	{
		if (mFlick != null && !mFlickNow)
		{
			Vector3 vector = Camera.main.WorldToScreenPoint(base.gameObject.transform.position);
			float _posX;
			float _posY;
			GetObjectScreenSize(new Vector3(mClipX, mClipY), out _posX, out _posY);
			mFlick.UpdateInitPos(vector.x - _posX / 2f, vector.y - _posY / 2f, _posX, _posY);
		}
	}

	private void GetObjectScreenSize(Vector3 pos, out float _posX, out float _posY)
	{
		GameObject gameObject = Util.CreateGameObject("getsize", base.gameObject);
		gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f));
		gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x + pos.x, gameObject.transform.localPosition.y + pos.y);
		_posX = Camera.main.WorldToScreenPoint(gameObject.transform.position).x;
		_posY = Camera.main.WorldToScreenPoint(gameObject.transform.position).y;
		GameMain.SafeDestroy(gameObject);
	}

	public void UpdateStageButton(short courseNum, int stageNum)
	{
		string key = string.Format("{0:D4}", stageNum + 1000 * courseNum);
		if (!mBingoStageBunttonInstDict.ContainsKey(key))
		{
			return;
		}
		if (mNotOpenCourse.Contains(courseNum))
		{
			mNotOpenCourse.Remove(courseNum);
		}
		BingoStageButtonInstance bingoStageButtonInstance = mBingoStageBunttonInstDict[key];
		bingoStageButtonInstance.SetStageButtonEnable(true, true);
		bingoStageButtonInstance.SetSelectStage(true);
		if (mLotteryNumberList.Count > courseNum && mLotteryNumberList[courseNum] != stageNum)
		{
			PlayerClearData _psd = null;
			SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetStageClearData(Def.SERIES.SM_EV, mEventId, courseNum, Def.GetStageNo(mLotteryNumberList[courseNum], 0), out _psd);
			if (_psd == null)
			{
				key = string.Format("{0:D4}", mLotteryNumberList[courseNum] + 1000 * courseNum);
				if (mBingoStageBunttonInstDict.ContainsKey(key))
				{
					bingoStageButtonInstance = mBingoStageBunttonInstDict[key];
					bingoStageButtonInstance.SetStageButtonEnable(false, false);
					bingoStageButtonInstance.SetSelectStage(false);
				}
			}
		}
		if (mLotteryNumberList.Count > courseNum)
		{
			mLotteryNumberList[courseNum] = (short)stageNum;
		}
		else
		{
			mLotteryNumberList.Add((short)stageNum);
		}
	}

	public void SetCourseEnable(short courseNum, bool enable)
	{
		for (int i = 1; i < 10; i++)
		{
			string key = string.Format("{0:D4}", i + 1000 * courseNum);
			if (mBingoStageBunttonInstDict.ContainsKey(key))
			{
				BingoStageButtonInstance bingoStageButtonInstance = mBingoStageBunttonInstDict[key];
				bingoStageButtonInstance.SetColliderEnable(enable);
			}
		}
	}

	public void SetMoveSlide(int i, bool force = false)
	{
		OnOrbButtonPushedCallBack(i, force);
		mNoSwipeSe = true;
	}

	public void CreateReachLine(List<short> _reachLineList, short _courseNum)
	{
		for (short num = 0; num < _reachLineList.Count; num++)
		{
			string key = string.Empty;
			Vector3 pos = Vector3.one;
			float rotate = 0f;
			Vector3 scale = new Vector3(1f, 1f, 1f);
			if (_reachLineList[num] == 0)
			{
				key = string.Format("{0:D4}", 1 + 1000 * _courseNum);
				pos = new Vector3(-35f, 0f, 0f);
				rotate = 90f;
			}
			else if (_reachLineList[num] == 1)
			{
				key = string.Format("{0:D4}", 4 + 1000 * _courseNum);
				pos = new Vector3(-35f, 0f, 0f);
				rotate = 90f;
			}
			else if (_reachLineList[num] == 2)
			{
				key = string.Format("{0:D4}", 7 + 1000 * _courseNum);
				pos = new Vector3(-35f, 0f, 0f);
				rotate = 90f;
			}
			else if (_reachLineList[num] == 3)
			{
				key = string.Format("{0:D4}", 1 + 1000 * _courseNum);
				pos = new Vector3(0f, 35f, 0f);
			}
			else if (_reachLineList[num] == 4)
			{
				key = string.Format("{0:D4}", 2 + 1000 * _courseNum);
				pos = new Vector3(0f, 35f, 0f);
			}
			else if (_reachLineList[num] == 5)
			{
				key = string.Format("{0:D4}", 3 + 1000 * _courseNum);
				pos = new Vector3(0f, 35f, 0f);
			}
			else if (_reachLineList[num] == 6)
			{
				key = string.Format("{0:D4}", 1 + 1000 * _courseNum);
				rotate = 45f;
				scale = new Vector3(1.2f, 1.2f, 1f);
			}
			else if (_reachLineList[num] == 7)
			{
				key = string.Format("{0:D4}", 3 + 1000 * _courseNum);
				rotate = -45f;
				scale = new Vector3(1.2f, 1.2f, 1f);
			}
			if (mBingoStageBunttonInstDict.ContainsKey(key))
			{
				mReachLineEffectFinishedList.Add(false);
				StartCoroutine(ReachLineEffect(num, mBingoStageBunttonInstDict[key].gameObject, pos, rotate, scale));
			}
		}
	}

	private IEnumerator ReachLineEffect(short _index, GameObject _BaseGo, Vector3 _pos, float _rotate, Vector3 _scale)
	{
		float waitTime = 0.5f * (float)_index;
		if (waitTime > 0f)
		{
			yield return new WaitForSeconds(waitTime);
		}
		SingletonMonoBehaviour<GameMain>.Instance.StopSe(3);
		SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_MAP_OPEN_AREA", 3);
		bool ssanimeFnished = false;
		UISsSprite ss2 = Util.CreateUISsSprite("ReachEffect" + _index, _BaseGo, "EFFECT_BINGO_REACH_LINE", _pos, _scale, mBaseDepth + 10);
		ss2.AnimationFinished = delegate
		{
			ssanimeFnished = true;
		};
		ss2.transform.Rotate(0f, 0f, _rotate);
		ss2.PlayCount = 1;
		ss2.Play();
		if (mReachLineEffectFinishedList.Count == _index + 1)
		{
			yield return new WaitForSeconds(0.5f);
		}
		mReachLineEffectFinishedList[_index] = true;
		while (!ssanimeFnished)
		{
			yield return null;
		}
		if (ss2 != null)
		{
			UnityEngine.Object.Destroy(ss2.gameObject);
			ss2 = null;
		}
	}

	public bool ReachEffectFinished()
	{
		for (short num = 0; num < mReachLineEffectFinishedList.Count; num++)
		{
			if (!mReachLineEffectFinishedList[num])
			{
				return false;
			}
		}
		mReachLineEffectFinishedList.Clear();
		return true;
	}

	public void CreateBingoLine(List<short> _bingoLineList, short _courseNum)
	{
		for (short num = 0; num < _bingoLineList.Count; num++)
		{
			string empty = string.Empty;
			int num2 = _bingoLineList[num] * 3;
			List<GameObject> list = new List<GameObject>();
			list.Clear();
			for (int i = 0; i < 3; i++)
			{
				empty = string.Format("{0:D4}", mBingoLine[num2 + i] + 1000 * _courseNum);
				if (mBingoStageBunttonInstDict.ContainsKey(empty))
				{
					list.Add(mBingoStageBunttonInstDict[empty].gameObject);
				}
			}
			if (list.Count == 3)
			{
				mBingoLineEffectFinishedList.Add(false);
				StartCoroutine(BingoLineEffect(num, list));
			}
		}
	}

	private IEnumerator BingoLineEffect(short _index, List<GameObject> _stageButtonGoList)
	{
		while (mNowPlayBingoLineEffect)
		{
			yield return null;
		}
		mNowPlayBingoLineEffect = true;
		SingletonMonoBehaviour<GameMain>.Instance.StopSe(3);
		SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_BINGO_BINGO_STAGE_EFFECT", 3);
		List<bool> ssanimeFnishedList = new List<bool>();
		List<UISsSprite> ssList = new List<UISsSprite>();
		for (int j = 0; j < 3; j++)
		{
			UISsSprite ss = Util.CreateUISsSprite("BingoEffect" + j + "_" + _index, _stageButtonGoList[j], "EFFECT_BINGO_BINGO_LINE", Vector3.zero, Vector3.one, mBaseDepth + 10);
			switch (j)
			{
			case 0:
				ss.AnimationFinished = delegate
				{
					ssanimeFnishedList[0] = true;
				};
				break;
			case 1:
				ss.AnimationFinished = delegate
				{
					ssanimeFnishedList[1] = true;
				};
				break;
			default:
				ss.AnimationFinished = delegate
				{
					ssanimeFnishedList[2] = true;
				};
				break;
			}
			ss.PlayCount = 1;
			ss.Play();
			ssList.Add(ss);
			ssanimeFnishedList.Add(false);
		}
		while (!ssanimeFnishedList[0] || !ssanimeFnishedList[1] || !ssanimeFnishedList[2])
		{
			yield return null;
		}
		mBingoLineEffectFinishedList[_index] = true;
		mNowPlayBingoLineEffect = false;
		for (int i = 0; i < ssList.Count; i++)
		{
			if (ssList[i] != null)
			{
				UnityEngine.Object.Destroy(ssList[i].gameObject);
				ssList[i] = null;
			}
		}
	}

	public bool BingoEffectFinished()
	{
		for (short num = 0; num < mBingoLineEffectFinishedList.Count; num++)
		{
			if (!mBingoLineEffectFinishedList[num])
			{
				return false;
			}
		}
		mBingoLineEffectFinishedList.Clear();
		return true;
	}

	public int GetTotalStar()
	{
		return mTotalStarNum;
	}

	public int GetSheetTotalStar(short sheetNum)
	{
		int result = 0;
		if (mSheetTotalStartList.Count > sheetNum)
		{
			result = mSheetTotalStartList[sheetNum];
		}
		return result;
	}

	public bool OpenSheetEffectFinished()
	{
		if (sheetOpenFinishedFlg)
		{
			sheetOpenFinishedFlg = false;
			return true;
		}
		return false;
	}

	public void OpenSheetLock(short courseId)
	{
		sheetOpenFinishedFlg = false;
		StartCoroutine(OpenSheetEffect(courseId));
	}

	private IEnumerator OpenSheetEffect(short courseId)
	{
		if (!mLockSpriteDict.ContainsKey(courseId))
		{
			sheetOpenFinishedFlg = true;
			yield break;
		}
		LockImgSettings lockSettings = mLockSpriteDict[courseId];
		for (int i = 0; i < 3; i++)
		{
			lockSettings.messageSprite.alpha -= 0.3f;
			yield return null;
		}
		lockSettings.messageSprite.alpha = 0f;
		yield return null;
		if (lockSettings.messageSprite.gameObject != null)
		{
			UnityEngine.Object.Destroy(lockSettings.messageSprite.gameObject);
		}
		yield return null;
		int FlameCount = 0;
		SingletonMonoBehaviour<GameMain>.Instance.PlaySe("SE_RB_UNLOCK", -1);
		for (int j = 1; j <= 10; j++)
		{
			lockSettings.lockSprite.transform.localRotation = Quaternion.Euler(0f, 0f, -j);
			if (j - 1 > 0 && (j - 1) % 3 == 0)
			{
				FlameCount++;
				yield return null;
			}
		}
		for (int l = 1; l <= 15; l++)
		{
			lockSettings.lockSprite.transform.localRotation = Quaternion.Euler(0f, 0f, -10 + l);
			if (l - 1 > 0 && (l - 1) % 7 == 0)
			{
				FlameCount++;
				yield return null;
			}
		}
		lockSettings.lockSprite.transform.localRotation = Quaternion.Euler(0f, 0f, -5f);
		FlameCount++;
		yield return null;
		for (int m = 1; m <= 7; m++)
		{
			lockSettings.lockSprite.transform.localRotation = Quaternion.Euler(0f, 0f, -5 + m);
			if (m - 1 > 0 && (m - 1) % 3 == 0)
			{
				FlameCount++;
				yield return null;
			}
		}
		for (int i3 = 1; i3 <= 2; i3++)
		{
			lockSettings.lockSprite.transform.localRotation = Quaternion.Euler(0f, 0f, -2 + i3);
			FlameCount++;
			yield return null;
		}
		for (int i2 = 0; i2 < 5; i2++)
		{
			FlameCount++;
			yield return null;
		}
		for (int n = 1; n <= 11; n++)
		{
			lockSettings.lockSprite.transform.localPosition = new Vector3(0f, 0 + n, 0f);
			if (lockSettings.lockSprite.transform.localScale.x < 1.1f)
			{
				float scaleNum = (float)n * 0.01f;
				lockSettings.lockSprite.transform.localScale = new Vector3(1f + scaleNum, 1f + scaleNum);
			}
			if (n - 1 > 0 && (n + 1) % 3 == 0)
			{
				FlameCount++;
				if (FlameCount == 19)
				{
					lockSettings.chainSprite1.alpha -= 0.15f;
					lockSettings.chainSprite2.alpha -= 0.15f;
				}
				yield return null;
			}
		}
		for (int k = 1; k <= 96; k++)
		{
			lockSettings.lockSprite.transform.localPosition = new Vector3(0f, 11 - k, 0f);
			if (k % 16 == 0)
			{
				FlameCount++;
				if (FlameCount != 25)
				{
					lockSettings.chainSprite1.alpha -= 0.15f;
					lockSettings.chainSprite2.alpha -= 0.15f;
				}
				else
				{
					lockSettings.chainSprite1.alpha = 0f;
					lockSettings.chainSprite2.alpha = 0f;
					lockSettings.lockSprite.alpha = 0f;
				}
				if (FlameCount <= 23 && FlameCount > 25)
				{
					lockSettings.lockSprite.alpha -= 0.3f;
				}
				yield return null;
			}
		}
		mLockSpriteDict.Remove(courseId);
		if (lockSettings.chainSprite1.gameObject != null)
		{
			UnityEngine.Object.Destroy(lockSettings.chainSprite1.gameObject);
		}
		if (lockSettings.chainSprite2.gameObject != null)
		{
			UnityEngine.Object.Destroy(lockSettings.chainSprite2.gameObject);
		}
		if (lockSettings.lockSprite.gameObject != null)
		{
			UnityEngine.Object.Destroy(lockSettings.lockSprite.gameObject);
		}
		sheetOpenFinishedFlg = true;
	}
}
