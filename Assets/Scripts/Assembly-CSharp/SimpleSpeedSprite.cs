using UnityEngine;

public class SimpleSpeedSprite : SimpleSprite
{
	protected delegate void SpeedUpdateFunc();

	public bool modified;

	protected SpeedUpdateFunc speedUpdateFunc;

	protected Vector3 m_v_pos = new Vector3(0f, 0f, 0f);

	protected Vector3 m_v_rot = new Vector3(0f, 0f, 0f);

	protected Vector3 m_v_scl = new Vector3(0f, 0f, 0f);

	public Vector3 v_pos
	{
		get
		{
			return m_v_pos;
		}
		set
		{
			modified = true;
			m_v_pos = value;
		}
	}

	public float v_posx
	{
		get
		{
			return v_pos.x;
		}
		set
		{
			Vector3 vector = v_pos;
			vector.x = value;
			v_pos = vector;
		}
	}

	public float v_posy
	{
		get
		{
			return v_pos.y;
		}
		set
		{
			Vector3 vector = v_pos;
			vector.y = value;
			v_pos = vector;
		}
	}

	public float v_posz
	{
		get
		{
			return v_pos.z;
		}
		set
		{
			Vector3 vector = v_pos;
			vector.z = value;
			v_pos = vector;
		}
	}

	public Vector3 v_rot
	{
		get
		{
			return m_v_rot;
		}
		set
		{
			modified = true;
			m_v_rot = value;
		}
	}

	public float v_rotx
	{
		get
		{
			return v_rot.x;
		}
		set
		{
			Vector3 vector = v_rot;
			vector.x = value;
			v_rot = vector;
		}
	}

	public float v_roty
	{
		get
		{
			return v_rot.y;
		}
		set
		{
			Vector3 vector = v_rot;
			vector.y = value;
			v_rot = vector;
		}
	}

	public float v_rotz
	{
		get
		{
			return v_rot.z;
		}
		set
		{
			Vector3 vector = v_rot;
			vector.z = value;
			v_rot = vector;
		}
	}

	public Vector3 v_scl
	{
		get
		{
			return m_v_scl;
		}
		set
		{
			modified = true;
			m_v_scl = value;
		}
	}

	public float v_sclx
	{
		get
		{
			return v_scl.x;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.x = value;
			v_scl = vector;
		}
	}

	public float v_scly
	{
		get
		{
			return v_scl.y;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.y = value;
			v_scl = vector;
		}
	}

	public float v_sclz
	{
		get
		{
			return v_scl.z;
		}
		set
		{
			Vector3 vector = v_scl;
			vector.z = value;
			v_scl = vector;
		}
	}

	protected override void Start()
	{
		base.Start();
		speedUpdateFunc = SUD_v;
	}

	protected override void Update()
	{
		if (Time.timeScale != 0f)
		{
			if (speedUpdateFunc != null)
			{
				speedUpdateFunc();
			}
			base.Update();
		}
	}

	protected virtual void SUD_v()
	{
		if (modified)
		{
			base._pos += m_v_pos * GameMain.frameDeltaMul;
			base._rot += m_v_rot * GameMain.frameDeltaMul;
			base._scl += m_v_scl * GameMain.frameDeltaMul;
			if (m_v_pos.x == 0f && m_v_pos.y == 0f && m_v_pos.z == 0f && m_v_rot.x == 0f && m_v_rot.y == 0f && m_v_rot.z == 0f && m_v_scl.x == 0f && m_v_scl.y == 0f && m_v_scl.z == 0f)
			{
				modified = false;
			}
		}
	}
}
