public class StageCreator
{
	public delegate void CreateTileHandler(int x, int y, Def.TILE_KIND kind, Def.TILE_FORM form);

	public CreateTileHandler HandleCreateBackground = EmptyCreateTileHandler;

	public CreateTileHandler HandleCreateAttribute = EmptyCreateTileHandler;

	public CreateTileHandler HandleCreateEdge = EmptyCreateTileHandler;

	public CreateTileHandler HandleReservedTile = EmptyCreateTileHandler;

	public CreateTileHandler HandleCreateTile = EmptyCreateTileHandler;

	private ISMStageData mCurrentStage;

	private IntVector2 mFieldSize;

	private IntRect mPlayArea;

	public IntVector2 FieldSize
	{
		get
		{
			return mFieldSize;
		}
	}

	public IntRect PlayArea
	{
		get
		{
			return mPlayArea;
		}
	}

	public StageCreator(ref ISMStageData stageData)
	{
		mCurrentStage = stageData;
		mFieldSize = new IntVector2(mCurrentStage.Width, mCurrentStage.Height);
		mPlayArea = new IntRect(1, 1, mFieldSize.x - 1, mFieldSize.y - 1);
	}

	private static void EmptyCreateTileHandler(int x, int y, Def.TILE_KIND kind, Def.TILE_FORM form)
	{
	}

	public void Execute()
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				ISMAttributeTile attributeTile = mCurrentStage.GetAttributeTile(j, i);
				if (attributeTile != null)
				{
					HandleCreateBackground(j, i, attributeTile.Kind, attributeTile.Form);
					for (ISMAttributeTile iSMAttributeTile = attributeTile; iSMAttributeTile != null; iSMAttributeTile = iSMAttributeTile.Next)
					{
						HandleCreateAttribute(j, i, iSMAttributeTile.Kind, iSMAttributeTile.Form);
					}
				}
			}
		}
		for (int k = 0; k < mFieldSize.y; k++)
		{
			for (int l = 0; l < mFieldSize.x; l++)
			{
				HandleCreateEdge(l, k, Def.TILE_KIND.EDGE, Def.TILE_FORM.NORMAL);
			}
		}
		for (int m = 0; m < mFieldSize.y; m++)
		{
			for (int n = 0; n < mFieldSize.x; n++)
			{
				ISMTile tile = mCurrentStage.GetTile(n, m);
				if (tile != null)
				{
					HandleReservedTile(n, m, tile.Kind, tile.Form);
				}
			}
		}
		for (int num = 0; num < mFieldSize.y; num++)
		{
			for (int num2 = 0; num2 < mFieldSize.x; num2++)
			{
				ISMTile tile2 = mCurrentStage.GetTile(num2, num);
				if (tile2 != null)
				{
					HandleCreateTile(num2, num, tile2.Kind, tile2.Form);
				}
			}
		}
	}
}
