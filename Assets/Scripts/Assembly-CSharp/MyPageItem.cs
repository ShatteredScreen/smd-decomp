public class MyPageItem : ListItem
{
	public enum KIND
	{
		COMPANION = 0,
		SKIN = 1
	}

	public KIND kind;

	public CompanionData companionData;

	public bool selected;

	public UISprite selectedIcon;

	public JellyImageButton button;

	public void SetSelect(bool enable)
	{
		if (enable && !selected)
		{
			selected = true;
			button.SetButtonEnable(false);
			selectedIcon.enabled = true;
		}
		else if (!enable && selected)
		{
			selected = false;
			button.SetButtonEnable(true);
			selectedIcon.enabled = false;
		}
	}
}
