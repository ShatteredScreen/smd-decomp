using System;
using System.Collections.Generic;

public class AdvGetEventPointProfile_Response : ICloneable
{
	[MiniJSONAlias("event_points")]
	public List<AdvEventPoint> EventPointList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGetEventPointProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGetEventPointProfile_Response;
	}
}
