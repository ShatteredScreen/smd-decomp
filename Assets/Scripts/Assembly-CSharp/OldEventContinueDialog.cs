using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldEventContinueDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		PURCHASE_INIT = 1,
		MAIN = 2,
		WAIT = 3
	}

	public enum PLACE
	{
		ENTRANCE = 0,
		EVENT_MAP = 1,
		MAIN_MAP = 2
	}

	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1,
		ERROR = 2
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private SELECT_ITEM mSelectItem;

	private bool mIsKeyOffer;

	private int mPrice;

	private int mEventID;

	private SMSeasonEventSetting mEventData;

	private PLACE mPlace;

	private bool mIsEventComplete;

	private int mMcNumBuf;

	private int mScNumBuf;

	private int mKeyDisplayNum;

	private UISprite mKeyNumFrame;

	private UILabel mKeyNumLabel;

	private bool mExtendImageProtection;

	private bool mTitleImageProtection;

	private UILabel mPriceLabel;

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private STATE mStateAfterMaintenanceCheck = STATE.MAIN;

	private STATE mStateAfterNetworkError = STATE.MAIN;

	private ConfirmDialog mErrorDialog;

	private bool mIsAuthError;

	public override void Start()
	{
		base.Start();
	}

	public override IEnumerator BuildResource()
	{
		mExtendImageProtection = ResourceManager.IsLoaded("OLDEVENT_EXTEND");
		mTitleImageProtection = ResourceManager.IsLoaded("OLDEVENT_TITLE");
		ResImage imageEventExtend = ResourceManager.LoadImageAsync("OLDEVENT_EXTEND");
		ResImage imageEventTitle = ResourceManager.LoadImageAsync("OLDEVENT_TITLE");
		while (imageEventExtend.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		while (imageEventTitle.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
	}

	private void BootConnectFinishedCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				AuthErrorCallback();
			}
			else
			{
				ConnectErrorCallback();
			}
		}
		else
		{
			CreateGemWindow();
			CreateKeyWindow();
			CreateCancelButton("OnCancelPushed");
			mState.Change(STATE.MAIN);
		}
	}

	private void PurchasedConnectFinishCallback(List<ConnectResult> a_result, bool a_error, bool a_abort)
	{
		if (a_error)
		{
			bool flag = false;
			for (int i = 0; i < a_result.Count; i++)
			{
				if (a_result[i].ErrorType == ConnectResult.ERROR_TYPE.Transfered)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				AuthErrorCallback();
			}
			else
			{
				ConnectErrorCallback();
			}
		}
		else
		{
			OnPurchaseFinished();
		}
	}

	private void AuthErrorCallback()
	{
		mIsAuthError = true;
		SetClosedCallback(null);
		if (mAuthErrorCallback != null)
		{
			mAuthErrorCallback();
		}
		Close();
		mState.Change(STATE.WAIT);
	}

	private void ConnectErrorCallback()
	{
		mSelectItem = SELECT_ITEM.ERROR;
		Close();
		mState.Change(STATE.WAIT);
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
		{
			List<int> list2 = new List<int>();
			Dictionary<int, ConnectParameterBase> dictionary2 = new Dictionary<int, ConnectParameterBase>();
			list2.Add(1044);
			list2.Add(1001);
			list2.Add(2);
			ConnectParameterBase connectParameterBase3 = new ConnectParameterBase();
			connectParameterBase3.SetMaintenanceDialogStyle(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY_CLOSE);
			dictionary2.Add(1044, connectParameterBase3);
			ConnectParameterBase connectParameterBase4 = new ConnectParameterBase();
			connectParameterBase4.SetMaintenanceDialogStyle(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY_CLOSE);
			dictionary2.Add(1001, connectParameterBase4);
			ConnectParameterBase connectParameterBase5 = new ConnectParameterBase();
			connectParameterBase5.SetMaintenanceDialogStyle(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY_CLOSE);
			dictionary2.Add(2, connectParameterBase5);
			mGame.mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			Network.GameConnectManager.StartConnect(list2, base.ParentGameObject, dictionary2, false, false, BootConnectFinishedCallback);
			mState.Change(STATE.WAIT);
			break;
		}
		case STATE.PURCHASE_INIT:
		{
			List<int> list = new List<int>();
			Dictionary<int, ConnectParameterBase> dictionary = new Dictionary<int, ConnectParameterBase>();
			list.Add(1044);
			list.Add(1001);
			list.Add(1);
			ConnectParameterBase connectParameterBase = new ConnectParameterBase();
			connectParameterBase.SetMaintenanceDialogStyle(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY_CLOSE);
			dictionary.Add(1044, connectParameterBase);
			ConnectParameterBase connectParameterBase2 = new ConnectParameterBase();
			connectParameterBase2.SetMaintenanceDialogStyle(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY_CLOSE);
			dictionary.Add(1001, connectParameterBase2);
			ConnectCurrencySpendParam connectCurrencySpendParam = new ConnectCurrencySpendParam(94);
			connectCurrencySpendParam.SetMaintenanceDialogStyle(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY_CLOSE);
			dictionary.Add(1, connectCurrencySpendParam);
			mGame.mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			Network.GameConnectManager.StartConnect(list, base.ParentGameObject, dictionary, false, false, PurchasedConnectFinishCallback);
			mState.Change(STATE.WAIT);
			break;
		}
		}
		mState.Update();
		UpdateKeyDisp();
	}

	public void Init(int eventID, PLACE place, bool completeSkip = true)
	{
		mEventID = eventID;
		if (mGame.IsSeasonEventExpired(eventID) || !mGame.mEventData.InSessionEventList.ContainsKey(eventID))
		{
			mSelectItem = SELECT_ITEM.NEGATIVE;
			OfferCancel();
			WillNotOpen();
			return;
		}
		if (completeSkip)
		{
			int eventStageClearCount = mGame.mPlayer.GetEventStageClearCount(Def.SERIES.SM_EV, mEventID);
			int eventGotStars = mGame.mPlayer.GetEventGotStars(Def.SERIES.SM_EV, mEventID);
			int eventStageNum = mGame.GetEventStageNum(mEventID);
			int eventStarNum = mGame.GetEventStarNum(mEventID);
			if (eventStageClearCount >= eventStageNum && eventGotStars >= eventStarNum)
			{
				mSelectItem = SELECT_ITEM.NEGATIVE;
				OfferCancel();
				WillNotOpen();
				return;
			}
		}
		mPlace = place;
		int itemCount = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 20);
		if (itemCount >= 1)
		{
			mIsKeyOffer = true;
			mPrice = 1;
		}
		else
		{
			mIsKeyOffer = false;
			mPrice = mGame.mShopItemData[94].GemAmount;
		}
		mEventData = mGame.mEventData.InSessionEventList[eventID];
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("OLDEVENT_EXTEND").Image;
		BIJImage image3 = ResourceManager.LoadImage("OLDEVENT_TITLE").Image;
		BIJImage image4 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		SetFrameSpriteBaceAtlas("DIALOG_BASE");
		SetFrameSpriteName("oldevent_instruction_panel");
		InitDialogFrame(DIALOG_SIZE.LARGE, DIALOG_STYLE.TITLED, string.Empty);
		UISprite sprite = Util.CreateSprite("TitleImage", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "LC_oldevent_instruction_text00", mBaseDepth + 2, new Vector3(0f, 260f, 0f), Vector3.one, false);
		sprite = Util.CreateSprite("InfoFrame", base.gameObject, image);
		Util.SetSpriteInfo(sprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 102f, 0f), Vector3.one, false);
		sprite.type = UIBasicSprite.Type.Sliced;
		sprite.SetDimensions(530, 144);
		GameObject parent = sprite.gameObject;
		sprite = Util.CreateSprite("InfoLine", parent, image);
		Util.SetSpriteInfo(sprite, "line00", mBaseDepth + 3, new Vector3(76f, -10f, 0f), Vector3.one, false);
		sprite.type = UIBasicSprite.Type.Sliced;
		sprite.SetDimensions(220, 6);
		string imageName = string.Format("LC_event_title_{0:0000}", mEventID);
		sprite = Util.CreateSprite("EventTitle", parent, image3);
		Util.SetSpriteInfo(sprite, imageName, mBaseDepth + 4, new Vector3(76f, 0f, 0f), new Vector3(1.25f, 1.25f, 1f), false, UIWidget.Pivot.Bottom);
		int num = 0;
		int num2 = 0;
		int[] companionRewardIdArray = mGame.GetCompanionRewardIdArray(mEventID);
		string imageName2 = "icon_chara_question";
		if (companionRewardIdArray.Length > 0)
		{
			AccessoryData accessoryData = mGame.GetAccessoryData(companionRewardIdArray[0]);
			int companionID = accessoryData.GetCompanionID();
			imageName2 = mGame.mCompanionData[companionID].iconName;
			num = 0;
			num2 = companionRewardIdArray.Length;
			for (int i = 0; i < companionRewardIdArray.Length; i++)
			{
				accessoryData = mGame.GetAccessoryData(companionRewardIdArray[i]);
				companionID = accessoryData.GetCompanionID();
				if (mGame.mPlayer.IsCompanionUnlock(companionID))
				{
					num++;
				}
			}
		}
		sprite = Util.CreateSprite("RewardIcon", parent, image4);
		Util.SetSpriteInfo(sprite, imageName2, mBaseDepth + 4, new Vector3(-168f, 0f, 0f), Vector3.one, false);
		imageName = Util.MakeLText("OldEventCharaRem", num, num2);
		UILabel uILabel = Util.CreateLabel("RewardDesc", parent, atlasFont);
		Util.SetLabelInfo(uILabel, imageName, mBaseDepth + 4, new Vector3(76f, -32f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		int eventStageClearCount = mGame.mPlayer.GetEventStageClearCount(Def.SERIES.SM_EV, mEventID);
		int eventGotStars = mGame.mPlayer.GetEventGotStars(Def.SERIES.SM_EV, mEventID);
		int eventStageNum = mGame.GetEventStageNum(mEventID);
		int eventStarNum = mGame.GetEventStarNum(mEventID);
		mIsEventComplete = false;
		if (eventStageClearCount >= eventStageNum)
		{
			imageName = "oldevent_Clear";
			if (eventGotStars >= eventStarNum)
			{
				imageName = "oldevent_Complete";
				mIsEventComplete = true;
			}
			parent = sprite.gameObject;
			sprite = Util.CreateSprite("Complete", parent, image2);
			Util.SetSpriteInfo(sprite, imageName, mBaseDepth + 5, new Vector3(0f, -45f, 0f), Vector3.one, false);
		}
		sprite = Util.CreateSprite("Image", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "LC_oldevent_ex_text", mBaseDepth + 2, new Vector3(104f, -60f, 0f), Vector3.one, false);
		sprite = Util.CreateSprite("Character", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "chara_extension", mBaseDepth + 3, new Vector3(-150f, -120f, 0f), Vector3.one, false);
		uILabel = Util.CreateLabel("MessageLabel", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("OldEventExtensionOffer_Desc"), mBaseDepth + 2, new Vector3(0f, 186f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel = Util.CreateLabel("MessageLabel", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("OldEventExtensionOffer_Desc02"), mBaseDepth + 2, new Vector3(0f, -270f, 0f), 18, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.red;
		imageName = "LC_button_ex01";
		if (mIsKeyOffer)
		{
			imageName = "LC_button_ex00";
		}
		UIButton uIButton = Util.CreateJellyImageButton("ButtonContinue", base.gameObject, image2);
		Util.SetImageButtonInfo(uIButton, imageName, imageName, imageName, mBaseDepth + 4, new Vector3(104f, -200f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnContinuePushed", UIButtonMessage.Trigger.OnClick);
		mPriceLabel = Util.CreateLabel("ContinueLabel", uIButton.gameObject, atlasFont);
		Util.SetLabelInfo(mPriceLabel, string.Empty + mPrice, mBaseDepth + 5, new Vector3(48f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		mPriceLabel.color = Def.DEFAULT_MESSAGE_COLOR;
		if (!mIsKeyOffer && mPrice > mGame.mPlayer.Dollar)
		{
			mPriceLabel.color = new Color(66f / 85f, 0f, 0f);
		}
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		if (!mExtendImageProtection)
		{
			ResourceManager.UnloadImage("OLDEVENT_EXTEND");
		}
		if (!mTitleImageProtection)
		{
			ResourceManager.UnloadImage("OLDEVENT_TITLE");
		}
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	private void CreateKeyWindow()
	{
		if (!(mDialogFrame == null))
		{
			Vector2 localSize = mDialogFrame.localSize;
			Vector3 vector = ((Util.ScreenOrientation != ScreenOrientation.LandscapeLeft && Util.ScreenOrientation != ScreenOrientation.LandscapeRight) ? mGemFramePositionOffsetP : mGemFramePositionOffsetL);
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			UIFont atlasFont = GameMain.LoadFont();
			mKeyNumFrame = Util.CreateSprite("KeyFrame", base.gameObject, image);
			Util.SetSpriteInfo(mKeyNumFrame, "instruction_panel8", mBaseDepth, new Vector3(0f + vector.x, (0f - localSize.y) / 2f - 30f + vector.y, 0f), Vector3.one, false);
			mKeyNumFrame.type = UIBasicSprite.Type.Sliced;
			mKeyNumFrame.SetDimensions(190, 70);
			UISprite sprite = Util.CreateSprite("KeyIcon", mKeyNumFrame.gameObject, image);
			Util.SetSpriteInfo(sprite, "key_panel", mBaseDepth + 1, new Vector3(-4f, 0f, 0f), Vector3.one, false);
			mKeyDisplayNum = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 20);
			mKeyNumLabel = Util.CreateLabel("KeyNum", mKeyNumFrame.gameObject, atlasFont);
			Util.SetLabelInfo(mKeyNumLabel, string.Empty + mKeyDisplayNum, mBaseDepth + 2, new Vector3(16f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			mKeyNumLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			OnScreenChanged(Util.ScreenOrientation);
		}
	}

	private void UpdateKeyDisp()
	{
		if (mKeyNumFrame != null && mKeyDisplayNum != mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 20))
		{
			mKeyDisplayNum = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 20);
			mKeyNumLabel.text = string.Empty + mKeyDisplayNum;
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!(mKeyNumFrame == null))
		{
			Vector2 localSize = mDialogFrame.localSize;
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mKeyNumFrame.transform.localPosition = new Vector3(-2f + mGemFramePositionOffsetP.x, (0f - localSize.y) / 2f - 30f + mGemFramePositionOffsetP.y, 0f);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mKeyNumFrame.transform.localPosition = new Vector3(localSize.x / 2f + 95f + mGemFramePositionOffsetL.x, (0f - localSize.y) / 2f + 124f + mGemFramePositionOffsetL.y, 0f);
				break;
			}
		}
	}

	private void ExpandOldEventRemainTime(int hours, int minutes, int seconds, int keyCost, int mcCost, int scCost)
	{
		DateTime dateTime = DateTimeUtil.Now();
		PlayerOldEventData data;
		mGame.mPlayer.Data.GetOldEventData((short)mEventID, out data);
		data.CloseTime = dateTime.Add(new TimeSpan(hours, minutes, seconds));
		data.ExtendCount++;
		mGame.mPlayer.Data.SetOldEventData((short)mEventID, data);
		mGame.Save();
		int eventStageClearCount = mGame.mPlayer.GetEventStageClearCount(Def.SERIES.SM_EV, mEventID);
		int eventGotStars = mGame.mPlayer.GetEventGotStars(Def.SERIES.SM_EV, mEventID);
		int[] companionRewardIdArray = mGame.GetCompanionRewardIdArray(mEventID);
		int num = 0;
		int num2 = 1;
		for (int i = 0; i < companionRewardIdArray.Length; i++)
		{
			AccessoryData accessoryData = mGame.GetAccessoryData(companionRewardIdArray[i]);
			int companionID = accessoryData.GetCompanionID();
			int num3 = 0;
			if (mGame.mPlayer.IsCompanionUnlock(companionID))
			{
				num3 = 1;
			}
			List<AccessoryData> alternateReward = mGame.GetAlternateReward(companionRewardIdArray[i], false);
			if (alternateReward.Count > 0 && mGame.mPlayer.IsAccessoryUnlock(alternateReward[0].Index))
			{
				num3 = 2;
			}
			num += num3 * num2;
			num2 *= 10;
		}
		ServerCram.OldEvOpen(mEventID, (int)mPlace, data.ExtendCount, (!mIsKeyOffer) ? 1 : 0, mGame.getUnlockedCharacterCount(true, false), mGame.getUnlockedCharacterCount(true, true), mGame.getUnlockedCharacterCount(false, false), mGame.getUnlockedCharacterCount(false, true), mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 20), mcCost, scCost, keyCost, num, eventStageClearCount, eventGotStars);
	}

	private void OfferCancel()
	{
		PlayerOldEventData data;
		mGame.mPlayer.Data.GetOldEventData((short)mEventID, out data);
		data.ExtendedOfferDone = true;
		mGame.mPlayer.Data.SetOldEventData((short)mEventID, data);
		mGame.Save();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnPurchaseDialogClosed()
	{
		if (mPriceLabel != null)
		{
			mPriceLabel.text = string.Empty + mPrice;
			if (mPrice > mGame.mPlayer.Dollar)
			{
				mPriceLabel.color = new Color(66f / 85f, 0f, 0f);
			}
			else
			{
				mPriceLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			}
		}
		mState.Change(STATE.MAIN);
	}

	public void OnContinuePushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		mSelectItem = SELECT_ITEM.POSITIVE;
		GameObject parent = base.transform.parent.gameObject;
		if (mIsEventComplete)
		{
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("OldEventError_Completed_Title"), Localization.Get("OldEventError_Completed_Desc01"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			confirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
			{
				if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					OnContinuePushedSub();
				}
				else
				{
					mState.Reset(STATE.MAIN, true);
				}
			});
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			OnContinuePushedSub();
		}
	}

	private void OnContinuePushedSub()
	{
		GameObject parent = base.transform.parent.gameObject;
		if (mIsKeyOffer)
		{
			string desc = Util.MakeLText("OldEventKeyConfirmation_Desc", mPrice);
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("OldEventKeyConfirmation_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			confirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
			{
				if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					mGame.mPlayer.SubItemCount(Def.ITEM_CATEGORY.CONSUME, 20, mPrice);
					ExpandOldEventRemainTime(0, Def.OLD_EVENT_UNLOCKMINUTES + Def.OLD_EVENT_CONTINUE_ADDITIONALMINUTES, 0, mPrice, 0, 0);
					Close();
				}
				else
				{
					mState.Reset(STATE.MAIN, true);
				}
			});
			mState.Reset(STATE.WAIT, true);
			return;
		}
		if (mPrice <= mGame.mPlayer.Dollar)
		{
			GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", parent).AddComponent<GemUseConfirmDialog>();
			gemUseConfirmDialog.Init(mPrice, 0);
			gemUseConfirmDialog.SetClosedCallback(delegate(GemUseConfirmDialog.SELECT_ITEM i, int a_value)
			{
				if (i == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					mMcNumBuf = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 3);
					mScNumBuf = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 4);
					mState.Reset(STATE.PURCHASE_INIT, true);
				}
				else
				{
					mState.Reset(STATE.MAIN, true);
				}
			});
		}
		else
		{
			ConfirmDialog confirmDialog2 = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
			confirmDialog2.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			confirmDialog2.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
			{
				if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
				{
					ServerIAP.BuyGemPlace mBuyGemPlace = ServerIAP.BuyGemPlace.NONE;
					switch (mPlace)
					{
					case PLACE.ENTRANCE:
						mBuyGemPlace = ServerIAP.BuyGemPlace.CONTINUE_OLDEVENT_IN_GAMESTATE;
						break;
					case PLACE.MAIN_MAP:
						mBuyGemPlace = ServerIAP.BuyGemPlace.CONTINUE_OLDEVENT_IN_MAP;
						break;
					case PLACE.EVENT_MAP:
						mBuyGemPlace = ServerIAP.BuyGemPlace.CONTINUE_OLDEVENT_IN_EVENT;
						break;
					}
					GameMain.mBuyGemPlace = mBuyGemPlace;
					PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(parent);
					purchaseDialog.SetBaseDepth(mBaseDepth + 10);
					purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
					purchaseDialog.SetAuthErrorClosedCallback(PurchaseAuthError);
				}
				else
				{
					mState.Reset(STATE.MAIN, true);
				}
			});
		}
		mState.Reset(STATE.WAIT, true);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			OfferCancel();
			Close();
			mState.Reset(STATE.WAIT, true);
		}
	}

	private void OnPurchaseFinished()
	{
		int itemCount = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 3);
		int itemCount2 = mGame.mPlayer.GetItemCount(Def.ITEM_CATEGORY.CONSUME, 4);
		ExpandOldEventRemainTime(0, Def.OLD_EVENT_UNLOCKMINUTES + Def.OLD_EVENT_CONTINUE_ADDITIONALMINUTES, 0, 0, mMcNumBuf - itemCount, mScNumBuf - itemCount2);
		Close();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void PurchaseAuthError()
	{
		mState.Change(STATE.WAIT);
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}
}
