public class ConnectAdvGetMailParam : ConnectParameterBase
{
	public bool IsForce { get; private set; }

	public ConnectAdvGetMailParam(bool a_force)
	{
		IsForce = a_force;
	}
}
