using System;
using System.Collections.Generic;
using UnityEngine;

public class MapRoadBlock : MapEntity
{
	public delegate void OnPushed(SMRoadBlockSetting a_data, MapRoadBlock a_roadblock);

	public STATE mStateStatus;

	public SMRoadBlockSetting mData;

	public OnPushed mCallback;

	protected ParticleOnSSAnime mParticle;

	protected new float mStateTime;

	protected float mRad;

	protected string mBaseAnimeKey = string.Empty;

	protected bool mFirstInitialized;

	protected BoxCollider mBoxCollider;

	protected UIEventTrigger mEventTrigger;

	protected int mColliderSize = 100;

	protected string mName;

	protected int mCounter;

	protected bool mIsUnlock;

	public void SetPushedCallback(OnPushed callback)
	{
		mCallback = callback;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		mStateStatus = mState.GetStatus();
		switch (mStateStatus)
		{
		case STATE.ON:
		{
			if (mState.IsChanged())
			{
				mStateTime = 0f;
				break;
			}
			mStateTime += Time.deltaTime * 480f;
			if (mStateTime > 90f)
			{
				mStateTime = 90f;
				if (!mGame.mTouchCnd)
				{
					mState.Change(STATE.OFF);
				}
			}
			float x = (1f + Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.x;
			float y = (1f - Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.y;
			_sprite.transform.localScale = new Vector3(x, y, 1f);
			break;
		}
		case STATE.OFF:
		{
			if (mState.IsChanged())
			{
				break;
			}
			mStateTime -= Time.deltaTime * 640f;
			if (mStateTime < 0f)
			{
				mStateTime = 0f;
				if (mCallCallback && HasTapAnime)
				{
					if (IsLockTapAnime)
					{
						mForceAnimeFinished = true;
					}
					else
					{
						mForceAnimeFinished = true;
					}
					mState.Change(STATE.WAIT);
				}
				else
				{
					mState.Change(STATE.STAY);
				}
			}
			float x = (1f + Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.x;
			float y = (1f - Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 0.2f) * mScale.y;
			_sprite.transform.localScale = new Vector3(x, y, 1f);
			break;
		}
		case STATE.WAIT:
			if (!_sprite.IsAnimationFinished() && !mForceAnimeFinished)
			{
				break;
			}
			mForceAnimeFinished = true;
			if (mCallCallback && mCallback != null)
			{
				mCallCallback = false;
				if (!IsLockTapAnime)
				{
					mCallback(mData, this);
					if (mChangeParts == null)
					{
						mChangeParts = new Dictionary<string, string>();
					}
					mChangeParts["mess"] = "null";
				}
				ChangeMode(mIsUnlock);
			}
			PlayAnime(AnimeKey, true);
			mBoxCollider = base.gameObject.GetComponent<BoxCollider>();
			if (mBoxCollider == null)
			{
				mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
			}
			mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
			mState.Change(STATE.STAY);
			break;
		case STATE.UNLOCK_WAIT:
			if (_sprite.IsAnimationFinished())
			{
				HasTapAnime = false;
				PlayAnime(AnimeKey, true);
				mState.Change(STATE.DISPPEAR);
			}
			break;
		}
		mState.Update();
	}

	public void ChangeMode(bool a_unlock = true)
	{
		Init(mCounter, mName, mData, mAtlas, base._pos.x, base._pos.y, mScale, a_unlock, mRad, mBaseAnimeKey);
	}

	public void ChangeLockMode()
	{
		Init(mCounter, mName, mData, mAtlas, base._pos.x, base._pos.y, mScale, mIsUnlock, mRad, mBaseAnimeKey);
	}

	public virtual void Init(int _counter, string _name, SMRoadBlockSetting data, BIJImage atlas, float x, float y, Vector3 scale, bool a_unlock, float a_rad, string a_animeKey = "MAP_ROADBLOCK")
	{
		AnimeKey = a_animeKey;
		mBaseAnimeKey = a_animeKey;
		mCounter = _counter;
		mName = _name;
		mAtlas = atlas;
		mScale = scale;
		mData = data;
		mRad = 0f;
		mIsUnlock = a_unlock;
		base._pos = new Vector3(x, y, Def.MAP_BUTTON_Z - 0.1f);
		base._zrot = mRad;
		if (!mIsUnlock)
		{
			HasTapAnime = true;
			mChangeParts = new Dictionary<string, string>();
			Player mPlayer = mGame.mPlayer;
			int roadBlockLevel = data.RoadBlockLevel;
			bool flag = true;
			PlayerClearData _psd;
			if (mPlayer.GetStageClearData(mPlayer.Data.CurrentSeries, roadBlockLevel, out _psd))
			{
				flag = false;
			}
			if (flag)
			{
				IsLockTapAnime = true;
				mChangeParts["mess"] = "null";
			}
			else
			{
				IsLockTapAnime = false;
				mChangeParts["mess"] = "ac_mess_tap";
			}
			if (!mFirstInitialized)
			{
				mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
				mEventTrigger = base.gameObject.AddComponent<UIEventTrigger>();
				mBoxCollider.size = new Vector3(100f, 100f, 0f);
				EventDelegate item = new EventDelegate(this, "OnButtonPushed");
				mEventTrigger.onPress.Add(item);
				EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
				mEventTrigger.onRelease.Add(item2);
				EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
				mEventTrigger.onClick.Add(item3);
			}
		}
		else
		{
			AnimeKey = mBaseAnimeKey + "_UNLOCKED";
			mChangeParts = null;
			HasTapAnime = false;
			if (mFirstInitialized)
			{
				mBoxCollider.enabled = false;
				mEventTrigger.enabled = false;
			}
		}
		PlayAnime(AnimeKey, true);
		mFirstInitialized = true;
	}

	public void OnButtonPushed()
	{
		if (mStateStatus == STATE.STAY)
		{
			STATE aStatus = STATE.ON;
			mState.Change(aStatus);
		}
	}

	public void OnButtonClicked()
	{
		if (HasTapAnime)
		{
			mCallCallback = true;
		}
		else
		{
			mCallCallback = false;
		}
	}

	public void OnButtonReleased()
	{
		if (mStateStatus == STATE.ON)
		{
			STATE aStatus = STATE.OFF;
			mState.Change(aStatus);
		}
	}

	public void ResetAnime()
	{
		mChangeParts = new Dictionary<string, string>();
		PlayAnime(AnimeKey, true);
	}

	public virtual void OpenAnime()
	{
		ChangeLockMode();
		mParticle = Util.CreateGameObject("OpenPartilce", base.gameObject).AddComponent<ParticleOnSSAnime>();
		mParticle.Init(_sprite, "Particles/StageOpenParticle", -10f, "mess");
		mParticle.SetEmitterDestroyCallback(OnParticleFinished);
		mState.Change(STATE.STAY);
	}

	public void OnParticleFinished()
	{
		if (mParticle != null)
		{
			GameMain.SafeDestroy(mParticle.gameObject);
			mParticle = null;
		}
	}

	public virtual void UnlockAnime()
	{
		mChangeParts = null;
		if (AnimeKey.CompareTo(mBaseAnimeKey) == 0)
		{
			PlayAnime(AnimeKey + "_UNLOCK", false);
		}
		AnimeKey = mBaseAnimeKey + "_UNLOCKED";
		HasTapAnime = false;
		mState.Change(STATE.UNLOCK_WAIT);
	}

	public void SetEnable(bool a_flg)
	{
		if (mBoxCollider != null)
		{
			mBoxCollider.enabled = a_flg;
		}
	}
}
