using System.Collections;
using UnityEngine;

public class StageStartDialog : DialogBase
{
	public enum MODE
	{
		LEVEL_START = 0,
		NOMOVE_PAUSE = 1,
		MOVE_PAUSE = 2
	}

	public enum SELECT_ITEM
	{
		NONE = 0,
		GIVEUP = 1,
		RETRY = 2,
		RETURN = 3
	}

	public delegate void OnDialogClosed(SELECT_ITEM item);

	private MODE mMode;

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private bool mOptionChanged;

	private UIButton mBgmButton;

	private UIButton mSeButton;

	private UIButton mComboButton;

	private ConfirmDialog mConfirmDialog;

	private ConfirmImageDialog mConfirmImageDialog;

	private float mTimer;

	private int mAcquireJewel;

	public override void Start()
	{
		mTimer = 3f;
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy() && mMode == MODE.LEVEL_START)
		{
			mTimer -= Time.deltaTime;
			if (mTimer <= 0f)
			{
				Close();
			}
			if (mGame.mTouchDownTrg && mTimer < 2.5f)
			{
				Close();
			}
		}
	}

	public override IEnumerator BuildResource()
	{
		ResImage image = ResourceManager.LoadImageAsync("HUD2");
		ResSsAnimation anim = ResourceManager.LoadSsAnimationAsync("EFFECT_SHINE");
		yield return 0;
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (anim.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text;
		if (!mGame.IsDailyChallengePuzzle)
		{
			text = ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET) ? Localization.Get("StarGet") : ((!mGame.mEventMode) ? Util.MakeLText("LevelStart_Title", mGame.mCurrentStage.DisplayStageNumber) : (mGame.mEventName + mGame.mCurrentStage.DisplayStageNumber)));
		}
		else
		{
			text = Util.MakeLText("LevelStart_Title", mGame.SelectedDailyChallengeStageNo);
			if (mGame.mDebugDailyChallengeVisibleBaseStage)
			{
				string text2 = text;
				text = string.Concat(text2, "[sup](", mGame.mCurrentStage.Series, mGame.mCurrentStage.DisplayStageNumber, ")[/sup]");
			}
		}
		string empty = string.Empty;
		empty = ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.EATEN) ? "LC_ClearingConditions04" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.COLLECT) ? "LC_ClearingConditions03" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.CLEAN && mGame.mCurrentStage.Paper0 + mGame.mCurrentStage.Paper1 + mGame.mCurrentStage.Paper2 > 0) ? "LC_ClearingConditions02" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.TIMER || mGame.mCurrentStage.LimitTimeSec > 0) ? "LC_ClearingConditions01" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.GROWUP) ? "LC_ClearingConditions05" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY) ? "LC_ClearingConditions09" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.DEFEAT) ? ((mGame.mCurrentStage.BossID == 0) ? "LC_ClearingConditions06" : ((mGame.mCurrentStage.BossID == 1) ? "LC_ClearingConditions08" : ((mGame.mCurrentStage.BossID != 2) ? "LC_ClearingConditions12" : "LC_ClearingConditions10"))) : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.FIND) ? "LC_ClearingConditions07" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.PAIR) ? "LC_ClearingConditions11" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.GOAL) ? "LC_ClearingConditions13" : ((mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.TALISMAN) ? "LC_ClearingConditions14" : ((mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET) ? "LC_ClearingConditions00" : "LC_ClearingConditions15"))))))))))));
		string text3 = Util.MakeLText("Score_Data", mGame.mCurrentStage.BronzeScore);
		UISprite uISprite;
		if (mMode == MODE.LEVEL_START)
		{
			DIALOG_SIZE size = DIALOG_SIZE.MEDIUM;
			float y = 20f;
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
			{
				size = DIALOG_SIZE.SMALL;
				y = -15f;
			}
			InitDialogFrame(size);
			InitDialogTitle(text);
			BIJImage image2 = ResourceManager.LoadImage("HUD2").Image;
			uISprite = Util.CreateSprite("spritetitle", base.gameObject, image2);
			Util.SetSpriteInfo(uISprite, empty, mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite.transform.localPosition = new Vector3(0f, y, 0f);
			if (mGame.mCurrentStage.WinType != Def.STAGE_WIN_CONDITION.STARGET)
			{
				empty = Localization.Get("Target_score");
				UILabel uILabel = Util.CreateLabel("TargetDesc", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, empty, mBaseDepth + 3, Vector3.zero, 26, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect(uILabel);
				uILabel.SetDimensions(200, 30);
				uILabel.transform.localPosition = new Vector3(-80f, -126f, 0f);
				uILabel = Util.CreateLabel("ScoreDesc", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text3, mBaseDepth + 3, Vector3.zero, 26, 0, 0, UIWidget.Pivot.Right);
				DialogBase.SetDialogLabelEffect(uILabel);
				uILabel.SetDimensions(200, 30);
				uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
				uILabel.transform.localPosition = new Vector3(126f, -126f, 0f);
				uISprite = Util.CreateSprite("line", uISprite.gameObject, image);
				Util.SetSpriteInfo(uISprite, "line00", mBaseDepth + 5, new Vector3(0f, -139f, 0f), Vector3.one, false);
				uISprite.type = UIBasicSprite.Type.Sliced;
				uISprite.SetDimensions(290, 6);
			}
			return;
		}
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(text);
		BIJImage image3 = ResourceManager.LoadImage("HUD2").Image;
		uISprite = Util.CreateSprite("spritetitle", base.gameObject, image3);
		Util.SetSpriteInfo(uISprite, empty, mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite.transform.localPosition = new Vector3(0f, 170f, 0f);
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			uISprite.transform.localPosition = new Vector3(0f, 140f, 0f);
		}
		else
		{
			empty = Localization.Get("Target_score");
			UILabel uILabel = Util.CreateLabel("TargetDesc", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, empty, mBaseDepth + 3, Vector3.zero, 26, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect(uILabel);
			uILabel.SetDimensions(200, 30);
			uILabel.transform.localPosition = new Vector3(-80f, -96f, 0f);
			uILabel = Util.CreateLabel("ScoreDesc", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text3, mBaseDepth + 3, Vector3.zero, 30, 0, 0, UIWidget.Pivot.Right);
			DialogBase.SetDialogLabelEffect(uILabel);
			uILabel.SetDimensions(200, 30);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.transform.localPosition = new Vector3(126f, -96f, 0f);
			uISprite = Util.CreateSprite("line", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite, "line00", mBaseDepth + 5, new Vector3(0f, -110f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(290, 6);
		}
		float num = -10f;
		float num2 = -105f;
		string text4 = "LC_button_replay";
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			text4 = "LC_button_replay_starget";
		}
		else if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
		{
			text4 = "LC_button_replay_heartinfinite";
		}
		UIButton uIButton = Util.CreateJellyImageButton("ButtonRetry", base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, text4, text4, text4, mBaseDepth + 4, new Vector3(0f, num, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnRetryPushed", UIButtonMessage.Trigger.OnClick);
		if (mMode == MODE.NOMOVE_PAUSE)
		{
			uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		else
		{
			bool flag = false;
			if (Def.IsEventSeries(mGame.mCurrentStage.Series))
			{
				flag = mGame.IsSeasonEventExpired(mGame.mCurrentStage.EventID);
			}
			if (flag)
			{
				UILabel uILabel2 = Util.CreateLabel("ExpiredText", base.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel2, Localization.Get("EventExpired_Title"), mBaseDepth + 5, new Vector3(10f, 30f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
				uILabel2.color = Color.red;
				uILabel2.effectStyle = UILabel.Effect.Outline8;
				uILabel2.effectColor = Color.white;
				uILabel2.effectDistance = new Vector2(2f, 2f);
				uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
		}
		num += num2;
		string text5 = "LC_button_map2";
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			text5 = "LC_button_map2_starget";
		}
		else if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
		{
			text5 = "LC_button_map2_heartinfinite";
		}
		uIButton = Util.CreateJellyImageButton("ButtonGiveup", base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, text5, text5, text5, mBaseDepth + 4, new Vector3(0f, num, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnGiveupPushed", UIButtonMessage.Trigger.OnClick);
		num += num2;
		mBgmButton = Util.CreateJellyImageButton("ButtonBgm", base.gameObject, image);
		Util.SetImageButtonInfo(mBgmButton, "button_music_ON", "button_music_ON", "button_music_ON", mBaseDepth + 4, new Vector3(-140f, num, 0f), Vector3.one);
		Util.AddImageButtonMessage(mBgmButton, this, "OnBgmPushed", UIButtonMessage.Trigger.OnClick);
		mSeButton = Util.CreateJellyImageButton("ButtonSe", base.gameObject, image);
		Util.SetImageButtonInfo(mSeButton, "button_sound_ON", "button_sound_ON", "button_sound_ON", mBaseDepth + 4, new Vector3(0f, num, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSeButton, this, "OnSePushed", UIButtonMessage.Trigger.OnClick);
		mComboButton = Util.CreateJellyImageButton("ButtonCombo", base.gameObject, image);
		Util.SetImageButtonInfo(mComboButton, "button_gesen", "button_gesen", "button_gesen", mBaseDepth + 4, new Vector3(140f, num, 0f), Vector3.one);
		Util.AddImageButtonMessage(mComboButton, this, "OnComboPushed", UIButtonMessage.Trigger.OnClick);
		UpdateBgmButtonImage();
		UpdateSeButtonImage();
		UpdateComboButtonImage();
		CreateCancelButton("OnReturnPushed");
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
		mOptionChanged = false;
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		ResourceManager.UnloadImage("HUD2");
		yield return null;
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public void Init(int jewel_count, MODE mode)
	{
		mMode = mode;
		mAcquireJewel = jewel_count;
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && !(mConfirmDialog != null) && !(mConfirmImageDialog != null))
		{
			mSelectItem = SELECT_ITEM.RETURN;
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnGiveupPushed(GameObject go)
	{
		if (GetBusy() || mConfirmDialog != null || mConfirmImageDialog != null)
		{
			return;
		}
		string title;
		string desc;
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			title = Localization.Get("Giveup_GiveupConfirmTitle");
			desc = Localization.Get("StarGet_Giveup");
			mConfirmDialog.Init(title, desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			mConfirmDialog.SetClosedCallback(OnGiveupConfirmClosed);
			return;
		}
		if (mMode == MODE.NOMOVE_PAUSE)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			title = Localization.Get("Giveup_ReturnToMapTitle");
			desc = Localization.Get("Giveup_ReturnToMapDesc");
			mConfirmDialog.Init(title, desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			mConfirmDialog.SetClosedCallback(OnGiveupConfirmClosed);
			return;
		}
		string image = "icon_heart-1";
		string descB = Localization.Get("GiveupHeartDesc");
		if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
		{
			image = "icon_heartinfinite";
			descB = Localization.Get("GiveupHeart_Mugen_Desc");
		}
		title = Localization.Get("Giveup_GiveupConfirmTitle");
		desc = Localization.Get("Giveup_MapDesc");
		if (mGame.mCurrentMapPageData is SMEventPageData && (mGame.mCurrentMapPageData as SMEventPageData).EventSetting != null)
		{
			Def.EVENT_TYPE eventType = (mGame.mCurrentMapPageData as SMEventPageData).EventSetting.EventType;
			if (eventType == Def.EVENT_TYPE.SM_LABYRINTH)
			{
				mConfirmImageDialog = Util.CreateGameObject("ConfirmImageDialog", base.transform.parent.gameObject).AddComponent<LabyrinthRetryDialog>();
			}
			else
			{
				mConfirmImageDialog = Util.CreateGameObject("ConfirmImageDialog", base.transform.parent.gameObject).AddComponent<ConfirmImageDialog>();
			}
		}
		else
		{
			mConfirmImageDialog = Util.CreateGameObject("ConfirmImageDialog", base.transform.parent.gameObject).AddComponent<ConfirmImageDialog>();
		}
		if (mConfirmImageDialog is LabyrinthRetryDialog)
		{
			(mConfirmImageDialog as LabyrinthRetryDialog).Init(mAcquireJewel, title, desc, descB, image, ConfirmImageDialog.CONFIRM_IMAGE_DIALOG_STYLE.YES_NO);
		}
		else
		{
			mConfirmImageDialog.Init(title, desc, descB, image, ConfirmImageDialog.CONFIRM_IMAGE_DIALOG_STYLE.YES_NO);
		}
		mConfirmImageDialog.SetClosedCallback(OnGiveUpConfirmClosed);
	}

	public void OnRetryPushed(GameObject go)
	{
		if (GetBusy() || mConfirmImageDialog != null || !go.GetComponent<JellyImageButton>().IsEnable())
		{
			return;
		}
		string image = "icon_heart-1";
		string descB = Localization.Get("GiveupHeartDesc");
		if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
		{
			image = "icon_heartinfinite";
			descB = Localization.Get("GiveupHeart_Mugen_Desc");
		}
		if (mGame.mCurrentMapPageData is SMEventPageData && (mGame.mCurrentMapPageData as SMEventPageData).EventSetting != null)
		{
			Def.EVENT_TYPE eventType = (mGame.mCurrentMapPageData as SMEventPageData).EventSetting.EventType;
			if (eventType == Def.EVENT_TYPE.SM_LABYRINTH)
			{
				mConfirmImageDialog = Util.CreateGameObject("ConfirmImageDialog", base.transform.parent.gameObject).AddComponent<LabyrinthRetryDialog>();
			}
			else
			{
				mConfirmImageDialog = Util.CreateGameObject("ConfirmImageDialog", base.transform.parent.gameObject).AddComponent<ConfirmImageDialog>();
			}
		}
		else
		{
			mConfirmImageDialog = Util.CreateGameObject("ConfirmImageDialog", base.transform.parent.gameObject).AddComponent<ConfirmImageDialog>();
		}
		if (mConfirmImageDialog is LabyrinthRetryDialog)
		{
			(mConfirmImageDialog as LabyrinthRetryDialog).Init(mAcquireJewel, Localization.Get("Giveup_RetryConfirmTitle"), Localization.Get("Giveup_RetryDesc"), descB, image, ConfirmImageDialog.CONFIRM_IMAGE_DIALOG_STYLE.YES_NO);
		}
		else if (mConfirmImageDialog is ConfirmImageDialog)
		{
			mConfirmImageDialog.Init(Localization.Get("Giveup_RetryConfirmTitle"), Localization.Get("Giveup_RetryDesc"), descB, image, ConfirmImageDialog.CONFIRM_IMAGE_DIALOG_STYLE.YES_NO);
		}
		mConfirmImageDialog.SetClosedCallback(OnRetryConfirmClosed);
	}

	public void OnReturnPushed(GameObject go)
	{
		if (!GetBusy() && !(mConfirmDialog != null))
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.RETURN;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnGiveupConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mGame.PlaySe("VOICE_027", 2);
			mSelectItem = SELECT_ITEM.GIVEUP;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnGiveUpConfirmClosed(ConfirmImageDialog.SELECT_ITEM item)
	{
		mConfirmImageDialog = null;
		if (item == ConfirmImageDialog.SELECT_ITEM.POSITIVE)
		{
			mGame.PlaySe("VOICE_027", 2);
			mSelectItem = SELECT_ITEM.GIVEUP;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnRetryConfirmClosed(ConfirmImageDialog.SELECT_ITEM item)
	{
		mConfirmImageDialog = null;
		if (item == ConfirmImageDialog.SELECT_ITEM.POSITIVE)
		{
			mGame.PlaySe("VOICE_026", 2);
			mSelectItem = SELECT_ITEM.RETRY;
			Close();
			if (mOptionChanged)
			{
				mGame.SaveOptions();
			}
		}
	}

	public void OnBgmPushed()
	{
		if (!GetBusy() && !(mConfirmDialog != null))
		{
			mGame.ToggleBGM();
			UpdateBgmButtonImage();
			mOptionChanged = true;
			mGame.PlaySe("SE_POSITIVE", -1);
		}
	}

	public void OnSePushed()
	{
		if (!GetBusy() && !(mConfirmDialog != null))
		{
			mGame.ToggleSE();
			UpdateSeButtonImage();
			mOptionChanged = true;
			mGame.PlaySe("SE_POSITIVE", -1);
		}
	}

	public void OnComboPushed()
	{
		if (!GetBusy() && !(mConfirmDialog != null))
		{
			mGame.ToggleComboCutIn();
			UpdateComboButtonImage();
			mOptionChanged = true;
			mGame.PlaySe("SE_POSITIVE", -1);
		}
	}

	private void UpdateBgmButtonImage()
	{
		if (mGame.mOptions.BGMEnable)
		{
			Util.SetImageButtonGraphic(mBgmButton, "button_music_ON", "button_music_ON", "button_music_ON");
		}
		else
		{
			Util.SetImageButtonGraphic(mBgmButton, "button_music_OFF", "button_music_OFF", "button_music_OFF");
		}
	}

	private void UpdateSeButtonImage()
	{
		if (mGame.mOptions.SEEnable)
		{
			Util.SetImageButtonGraphic(mSeButton, "button_sound_ON", "button_sound_ON", "button_sound_ON");
		}
		else
		{
			Util.SetImageButtonGraphic(mSeButton, "button_sound_OFF", "button_sound_OFF", "button_sound_OFF");
		}
	}

	private void UpdateComboButtonImage()
	{
		if (mGame.mOptions.ComboCutIn)
		{
			Util.SetImageButtonGraphic(mComboButton, "button_combo_ON", "button_combo_ON", "button_combo_ON");
		}
		else
		{
			Util.SetImageButtonGraphic(mComboButton, "button_combo_OFF", "button_combo_OFF", "button_combo_OFF");
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
