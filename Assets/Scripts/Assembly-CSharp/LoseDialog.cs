using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		MAINTENANCE_CHECK_WAIT = 3,
		NETWORK_00 = 4,
		NETWORK_00_1 = 5,
		NETWORK_00_2 = 6,
		NETWORK_01 = 7,
		NETWORK_01_1 = 8,
		NETWORK_01_2 = 9,
		MAINTENANCE_CHECK_START02 = 10,
		MAINTENANCE_CHECK_WAIT02 = 11,
		NETWORK_02 = 12,
		NETWORK_02_1 = 13,
		NETWORK_02_2 = 14,
		NETWORK_STAGESKIP = 15,
		NETWORK_STAGESKIP_1 = 16,
		NETWORK_STAGESKIP_2 = 17,
		AUTHERROR_WAIT = 18
	}

	public enum SELECT_ITEM
	{
		CONTINUE = 0,
		GIVEUP = 1,
		STAGESKIP = 2
	}

	private sealed class ContinueChanceInfo
	{
		public int ShopItemID { get; set; }

		public int ChanceID { get; set; }

		public BoosterChanceDialog.PURCHASE_TYPE UserSegment { get; set; }
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	public const int STAGESKIP_SHOP_IDX_BOTTOM = 24;

	public const int STAGESKIP_SHOP_IDX_NUM = 10;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAINTENANCE_CHECK_WAIT);

	private PuzzleManager mManager;

	protected SELECT_ITEM mSelectItem;

	protected Def.ContinueStepData mData;

	private int mPrice;

	private ShopItemData mStageSkipData;

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	public STATE StateStatus;

	private STATE mStateAfterMaintenanceCheck = STATE.MAIN;

	private STATE mStateAfterNetworkError = STATE.MAIN;

	private ShopItemData mBoughtItem;

	private int mContinueCount;

	protected Def.STAGE_LOSE_REASON mLoseReason;

	private ConfirmDialog mErrorDialog;

	protected bool mTuxedoSkipMode;

	protected bool mStageSkipMode;

	private bool mChanceAnnounced;

	private UILabel mStageSkipLabel;

	private UILabel mContinueLabel;

	private UIButton mButtonContinue;

	private UIButton mButtonGiveup;

	private UIButton mButtonSkip;

	private Dictionary<UIWidget, Color> mColorContinue = new Dictionary<UIWidget, Color>();

	private Dictionary<UILabel, Color> mColorGiveup = new Dictionary<UILabel, Color>();

	private Dictionary<UILabel, Color> mColorSkip = new Dictionary<UILabel, Color>();

	private KeyValuePair<bool, ContinueChanceInfo> mContinueChancePair = new KeyValuePair<bool, ContinueChanceInfo>(false, null);

	public int ExtraAddMove { get; private set; }

	protected bool HasContinueChance
	{
		get
		{
			if (mContinueChancePair.Key && mContinueChancePair.Value != null && (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES || mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME))
			{
				return true;
			}
			return false;
		}
	}

	public override void Start()
	{
		mManager = GameObject.Find("PuzzleManager").GetComponent<PuzzleManager>();
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.LEVEL7_2)
		{
			mState.Reset(STATE.INIT, true);
		}
		else
		{
			MaintenanceCheckOnStart(ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY, ConnectCommonErrorDialog.STYLE.RETRY);
		}
		base.Start();
	}

	public override IEnumerator BuildResource()
	{
		ResImage image = ResourceManager.LoadImageAsync("PUZZLE_CONTINUE_HUD");
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		ResScriptableObject resSpChance = ResourceManager.LoadScriptableObjectAsync("SPECIAL_CHANCE");
		while (resSpChance.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		StateStatus = mState.GetStatus();
		switch (StateStatus)
		{
		case STATE.INIT:
			CreateGemWindow();
			StartCoroutine(CreateContinueButton());
			mState.Change(STATE.WAIT);
			break;
		case STATE.MAIN:
			if (mGame.mDebugRepeatPlayMode)
			{
				OnGiveupPushed(null);
			}
			break;
		case STATE.MAINTENANCE_CHECK_WAIT:
			if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
			{
				mState.Change(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00:
			if (!mGame.Network_UserAuth())
			{
				if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_00);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		case STATE.NETWORK_00_1:
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00_2);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
			}
			else
			{
				ShowNetworkErrorDialog(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00_2:
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			if (!mGame.Network_GetGemCount())
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.INIT);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_01_1);
			}
			break;
		case STATE.NETWORK_01_1:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_01_2);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			break;
		case STATE.NETWORK_01_2:
			mState.Change(STATE.INIT);
			break;
		case STATE.MAINTENANCE_CHECK_START02:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.CLOSE);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT02);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT02:
			switch (MaintenanceCheckResult())
			{
			case MAINTENANCE_RESULT.OK:
				mState.Change(mStateAfterMaintenanceCheck);
				mStateAfterMaintenanceCheck = STATE.MAIN;
				break;
			case MAINTENANCE_RESULT.ERROR:
			case MAINTENANCE_RESULT.IN_MAINTENANCE:
				if (!GetBusy())
				{
					mState.Change(STATE.MAIN);
				}
				break;
			}
			break;
		case STATE.NETWORK_02:
		{
			bool flag = mGame.Network_BuyItem(mBoughtItem.Index);
			mGame.ConnectRetryFlg = false;
			if (!flag)
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_1);
			}
			break;
		}
		case STATE.NETWORK_02_1:
		{
			if (!GameMain.mBuyItemCheckDone)
			{
				break;
			}
			if (GameMain.mBuyItemSucceed)
			{
				mState.Change(STATE.NETWORK_02_2);
				break;
			}
			Server.ErrorCode mBuyItemErrorCode = GameMain.mBuyItemErrorCode;
			if (mBuyItemErrorCode == Server.ErrorCode.DUPLICATE_DATA)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mState.Change(STATE.WAIT);
			}
			else
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			break;
		}
		case STATE.NETWORK_02_2:
			if (mBoughtItem != null)
			{
				mGame.mPlayer.BoughtItem(mBoughtItem.ItemDetail);
			}
			mBoughtItem = null;
			mSelectItem = SELECT_ITEM.CONTINUE;
			mState.Change(STATE.WAIT);
			Close();
			break;
		case STATE.NETWORK_STAGESKIP:
		{
			bool flag2 = mGame.Network_BuyItem(mBoughtItem.Index);
			mGame.ConnectRetryFlg = false;
			if (!flag2)
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.NETWORK_STAGESKIP_1);
			}
			break;
		}
		case STATE.NETWORK_STAGESKIP_1:
		{
			if (!GameMain.mBuyItemCheckDone)
			{
				break;
			}
			if (GameMain.mBuyItemSucceed)
			{
				mState.Change(STATE.NETWORK_STAGESKIP_2);
				break;
			}
			Server.ErrorCode mBuyItemErrorCode = GameMain.mBuyItemErrorCode;
			if (mBuyItemErrorCode == Server.ErrorCode.DUPLICATE_DATA)
			{
				ConfirmDialog confirmDialog2 = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog2.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog2.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mState.Change(STATE.WAIT);
			}
			else
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			break;
		}
		case STATE.NETWORK_STAGESKIP_2:
			if (mBoughtItem != null)
			{
				mGame.mPlayer.BoughtItem(mBoughtItem.ItemDetail);
			}
			mBoughtItem = null;
			mSelectItem = SELECT_ITEM.STAGESKIP;
			mState.Change(STATE.WAIT);
			Close();
			break;
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	public void Init(int continueCount, Def.STAGE_LOSE_REASON reason, bool tuxedoSkipMode, bool _stageskipmode = false, int _stageskipPriceIndex = 0, bool _chance_announced = false)
	{
		int num = continueCount;
		if (num >= Def.CONTINUE_STEP.Length)
		{
			num = Def.CONTINUE_STEP.Length - 1;
		}
		mContinueCount = num;
		mData = Def.CONTINUE_STEP[num];
		mPrice = mData.price;
		mLoseReason = reason;
		mTuxedoSkipMode = tuxedoSkipMode;
		mStageSkipMode = _stageskipmode;
		mChanceAnnounced = _chance_announced;
		int num2 = _stageskipPriceIndex;
		if (num2 >= 34)
		{
			num2 = 33;
		}
		mStageSkipData = mGame.mShopItemData[num2];
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.LEVEL7_2)
		{
			mPrice = 0;
			mState.Reset(STATE.INIT);
		}
	}

	protected void JudgeContinueChance()
	{
		ExtraAddMove = 0;
		if (mGame.mPlayer.Data.ChanceCount == null)
		{
			mGame.mPlayer.Data.ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
		}
		if (!mGame.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE))
		{
			mGame.mPlayer.Data.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE, 0);
		}
		Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> chanceCount;
		Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> dictionary = (chanceCount = mGame.mPlayer.Data.ChanceCount);
		SpecialChanceConditionSet.CHANCE_MODE key;
		SpecialChanceConditionSet.CHANCE_MODE key2 = (key = SpecialChanceConditionSet.CHANCE_MODE.CONTINUE);
		short num = chanceCount[key];
		dictionary[key2] = (short)(num + 1);
		SpecialChanceConditionItem.Option option = default(SpecialChanceConditionItem.Option);
		option.ContinueCount = mContinueCount;
		option.IsLose = true;
		option.IsLittleBitMore = mChanceAnnounced;
		SpecialChanceConditionItem.Option option2 = option;
		ChanceConditionInfo condition;
		BoosterChanceDialog.PURCHASE_TYPE segment;
		if (!BoosterChanceDialog.Lottery(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE, out condition, out segment, option2))
		{
			return;
		}
		if (condition.ItemID.Count > 0 && segment >= BoosterChanceDialog.PURCHASE_TYPE.NONE && (int)segment < condition.ItemID[0].Count && mGame.mShopItemData.ContainsKey(condition.ItemID[0][(int)segment]))
		{
			if (mGame.mShopItemData[condition.ItemID[0][(int)segment]].ItemDetail.CONTINUE_UP_M2_T6 > 0)
			{
				mContinueChancePair = new KeyValuePair<bool, ContinueChanceInfo>(true, new ContinueChanceInfo
				{
					ShopItemID = condition.ItemID[0][(int)segment],
					ChanceID = condition.ChanceID,
					UserSegment = segment
				});
				ExtraAddMove = 2;
				mData.addMove += ExtraAddMove;
				mData.addTime += ExtraAddMove * 3;
				if (mGame.mPlayer.Data.ChanceTotalOfferCount.ContainsKey(condition.ChanceID))
				{
					Dictionary<int, int> chanceTotalOfferCount;
					Dictionary<int, int> dictionary2 = (chanceTotalOfferCount = mGame.mPlayer.Data.ChanceTotalOfferCount);
					int chanceID;
					int key3 = (chanceID = condition.ChanceID);
					chanceID = chanceTotalOfferCount[chanceID];
					dictionary2[key3] = chanceID + 1;
				}
				else
				{
					mGame.mPlayer.Data.ChanceTotalOfferCount.Add(condition.ChanceID, 1);
				}
				if (!mGame.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE))
				{
					mGame.mPlayer.Data.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE, 0);
				}
				if (!mGame.mPlayer.Data.ChanceDispCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE))
				{
					mGame.mPlayer.Data.ChanceDispCount.Add(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE, 0);
				}
				mGame.mPlayer.Data.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.CONTINUE] = 0;
				Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> chanceDispCount;
				Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> dictionary3 = (chanceDispCount = mGame.mPlayer.Data.ChanceDispCount);
				SpecialChanceConditionSet.CHANCE_MODE key4 = (key = SpecialChanceConditionSet.CHANCE_MODE.CONTINUE);
				num = chanceDispCount[key];
				dictionary3[key4] = (short)(num + 1);
				mGame.Save();
			}
			else
			{
				mContinueChancePair = new KeyValuePair<bool, ContinueChanceInfo>(false, null);
			}
		}
		else
		{
			mContinueChancePair = new KeyValuePair<bool, ContinueChanceInfo>(false, null);
		}
	}

	public override void Awake()
	{
		mJellyflg = Def.DIALOG_PRODUCT.LOSE_DIALOG;
		mParticleFeather = false;
		base.Awake();
	}

	public override void BuildDialog()
	{
		Vector2 vector = Util.LogScreenSize();
		if (mJelly != null)
		{
			JellySecondDialog jellySecondDialog = mJelly as JellySecondDialog;
			float a_startPos = vector.y * 1.5f;
			Vector2 bUILD_DIALOG_POSITION_OFFSET = DialogBase.BUILD_DIALOG_POSITION_OFFSET;
			jellySecondDialog.SetInitialPosition(a_startPos, bUILD_DIALOG_POSITION_OFFSET.y, 0.3f, 0.2f);
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("PUZZLE_CONTINUE_HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		JudgeContinueChance();
		string text;
		if (mTuxedoSkipMode)
		{
			text = Localization.Get("Lose_Continue_01");
			if (mStageSkipMode)
			{
				InitDialogFrame(DIALOG_SIZE.MEDIUM, null, null, 0, 460);
			}
			else
			{
				InitDialogFrame(DIALOG_SIZE.MEDIUM);
			}
		}
		else
		{
			text = Localization.Get("Lose_Continue") + "\u3000";
			InitDialogFrame(DIALOG_SIZE.LARGE);
		}
		InitDialogTitle(string.Empty);
		UILabel uILabel = Util.CreateLabel("title", mTitleBoard.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mDialogDepth + mDialogUsedDepth, new Vector3(0f, -2f, 0f), 38, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(uILabel, text);
		uILabel.color = Def.DEFAULT_DIALOG_TITLE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(340, 38);
		if (!mTuxedoSkipMode)
		{
			UISprite sprite = Util.CreateSprite("titlecharm", base.gameObject, image2);
			Util.SetSpriteInfo(sprite, "panel_continue_rose", mBaseDepth + 2, new Vector3(24f, 296f, 0f), Vector3.one, false);
			uISprite2 = Util.CreateSprite("Character", base.gameObject, image2);
			Util.SetSpriteInfo(uISprite2, "chara_continue", mBaseDepth + 10, new Vector3(-150f, 80f, 0f), Vector3.one, false);
			uISprite = Util.CreateSprite("messageboard", base.gameObject, image2);
			Util.SetSpriteInfo(uISprite, "continue_balloon", mBaseDepth + 9, Vector3.zero, Vector3.one, false);
			if (mStageSkipMode)
			{
				text = Localization.Get("Lose_StageSkip");
			}
			else if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES)
			{
				text = Localization.Get("Lose_Continue_Move");
			}
			else if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
			{
				text = Localization.Get("Lose_Continue_Time");
			}
			uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text, mBaseDepth + 12, new Vector3(0f, 6f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		string empty = string.Empty;
		string empty2 = string.Empty;
		empty = "continue_item00";
		empty2 = ((mGame.mCurrentStage.LoseType != 0) ? "continue_itemText09" : "continue_itemText10");
		switch (mData.item)
		{
		case Def.CONTINUE_ADDITIONALITEM.SKILL_CHARGE:
			empty = "icon_continueItem00";
			empty2 = "continue_itemText00";
			break;
		case Def.CONTINUE_ADDITIONALITEM.SELECT_ONE:
			empty = "icon_continueItem01";
			empty2 = "continue_itemText01";
			break;
		case Def.CONTINUE_ADDITIONALITEM.WAVE_BOMB:
			empty = "icon_continueItem02";
			empty2 = "continue_itemText02";
			break;
		case Def.CONTINUE_ADDITIONALITEM.RAINBOW:
			if (mData.num == 2)
			{
				empty = "icon_continueItem06";
				empty2 = "continue_itemText11";
			}
			else
			{
				empty = "icon_continueItem03";
				empty2 = "continue_itemText03";
			}
			break;
		case Def.CONTINUE_ADDITIONALITEM.TAP_BOMB:
			empty = "icon_continueItem04";
			empty2 = "continue_itemText04";
			break;
		case Def.CONTINUE_ADDITIONALITEM.COLOR_CRUSH:
			empty = "icon_continueItem05";
			empty2 = "continue_itemText05";
			break;
		}
		string imageQuantity = string.Empty;
		if (mData.item != 0)
		{
			if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES)
			{
				switch (mData.addMove)
				{
				case 5:
					imageQuantity = "continue_itemText_move00";
					break;
				case 15:
					imageQuantity = "continue_itemText_move01";
					break;
				case 30:
					imageQuantity = "continue_itemText_move02";
					break;
				}
			}
			else if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
			{
				switch ((int)mData.addTime)
				{
				case 15:
					imageQuantity = "continue_itemText_time00";
					break;
				case 30:
					imageQuantity = "continue_itemText_time01";
					break;
				case 60:
					imageQuantity = "continue_itemText_time02";
					break;
				}
			}
		}
		float num = 1f;
		if (mTuxedoSkipMode)
		{
			if (!mStageSkipMode)
			{
				if (mData.item != 0)
				{
					num = 0.725f;
				}
				MakeContinueAdditionalBalloon(base.gameObject, new Vector3(0f, 33f, 0f), new Vector3(num, num, 1f), imageQuantity, empty, empty2);
			}
			else if (mLoseReason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
			{
				UISprite sprite = Util.CreateSprite("item", base.gameObject, image2);
				Util.SetSpriteInfo(sprite, "chara_miss", mBaseDepth + 2, new Vector3(0f, 60f, 0f), Vector3.one, false);
			}
			else
			{
				if (mData.item != 0)
				{
					num = 0.725f;
				}
				MakeContinueAdditionalBalloon(base.gameObject, new Vector3(0f, 60f, 0f), new Vector3(num, num, 1f), imageQuantity, empty, empty2);
			}
		}
		else if (!mStageSkipMode)
		{
			if (mData.item == Def.CONTINUE_ADDITIONALITEM.NONE)
			{
				uISprite.transform.localPosition = new Vector3(96f, 150f, 0f);
				UISprite sprite = Util.CreateSprite("flower", base.gameObject, image2);
				Util.SetSpriteInfo(sprite, "continue_rose", mBaseDepth + 2, new Vector3(80f, 10f, 0f), Vector3.one, false);
			}
			else
			{
				uISprite.transform.localPosition = new Vector3(96f, 186f, 0f);
				MakeContinueAdditionalBalloon(base.gameObject, new Vector3(99f, 33f, 0f), new Vector3(0.725f, 0.725f, 1f), imageQuantity, empty, empty2);
			}
		}
		else if (mLoseReason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
		{
			uISprite.transform.localPosition = new Vector3(96f, 150f, 0f);
			UISprite sprite = Util.CreateSprite("flower", base.gameObject, image2);
			Util.SetSpriteInfo(sprite, "continue_rose", mBaseDepth + 2, new Vector3(80f, 10f, 0f), Vector3.one, false);
		}
		else
		{
			uISprite.transform.localPosition = new Vector3(96f, 186f, 0f);
			if (mData.item != 0)
			{
				num = 0.725f;
			}
			MakeContinueAdditionalBalloon(base.gameObject, new Vector3(99f, 26f, 0f), new Vector3(num, num, 1f), imageQuantity, empty, empty2);
		}
		CreateButtons();
	}

	protected void MakeContinueAdditionalBalloon(GameObject parent, Vector3 position, Vector3 itemScale, string imageQuantity, string imageItem, string imageText)
	{
		BIJImage image = ResourceManager.LoadImage("PUZZLE_CONTINUE_HUD").Image;
		UISprite uISprite = Util.CreateSprite("lace", parent, image);
		Util.SetSpriteInfo(uISprite, "continue_lace2", mBaseDepth + 3, position, Vector3.one, false);
		if (!string.IsNullOrEmpty(imageQuantity))
		{
			UISprite sprite = Util.CreateSprite("plusX", uISprite.gameObject, image);
			Util.SetSpriteInfo(sprite, imageQuantity, mBaseDepth + 5, new Vector3(0f, 80f, 0f), Vector3.one, false);
		}
		if (!string.IsNullOrEmpty(imageItem))
		{
			UISprite sprite = Util.CreateSprite("item", uISprite.gameObject, image);
			Util.SetSpriteInfo(sprite, imageItem, mBaseDepth + 4, new Vector3(0f, 21f, 0f), itemScale, false);
		}
		if (!string.IsNullOrEmpty(imageText))
		{
			UISprite sprite = Util.CreateSprite("text", uISprite.gameObject, image);
			Util.SetSpriteInfo(sprite, imageText, mBaseDepth + 5, new Vector3(0f, -50f, 0f), Vector3.one, false);
		}
	}

	protected void CreateButtons()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("PUZZLE_CONTINUE_HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		bool flag = false;
		string text = string.Empty;
		string text2;
		string text3;
		string value;
		if (mTuxedoSkipMode)
		{
			if (mStageSkipMode)
			{
				if (mLoseReason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
				{
					text2 = null;
					text3 = "LC_button_giveup";
					value = "LC_button_clear";
				}
				else
				{
					flag = true;
					text2 = "button_continue_skip";
					text3 = "LC_button_giveup_skip";
					value = "LC_button_clear";
				}
			}
			else
			{
				flag = true;
				text2 = "button_continue_skip";
				text3 = "LC_button_giveup_skip";
				value = null;
			}
		}
		else if (mStageSkipMode)
		{
			if (mLoseReason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
			{
				text2 = null;
				text3 = "LC_button_giveup";
				value = "LC_button_clear";
			}
			else
			{
				flag = true;
				text2 = "button_continue_skip";
				text3 = "LC_button_giveup_skip";
				value = "LC_button_clear";
			}
		}
		else
		{
			text2 = "button_continue";
			text3 = "LC_button_giveup";
			value = null;
		}
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			text3 += "_starget";
		}
		else if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
		{
			text3 += "_heartinfinite";
		}
		if (!string.IsNullOrEmpty(text2))
		{
			mButtonContinue = Util.CreateJellyImageButton("ButtonContinue", base.gameObject, image2);
			Util.SetImageButtonInfo(mButtonContinue, text2, text2, text2, mBaseDepth + 4, Vector3.zero, Vector3.one);
			Util.AddImageButtonMessage(mButtonContinue, this, "OnContinuePushed", UIButtonMessage.Trigger.OnClick);
			mContinueLabel = Util.CreateLabel("ContinueLabel", mButtonContinue.gameObject, atlasFont);
			if (flag)
			{
				Util.SetLabelInfo(mContinueLabel, string.Empty + mPrice, mBaseDepth + 5, new Vector3(85f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			}
			else
			{
				Util.SetLabelInfo(mContinueLabel, string.Empty + mPrice, mBaseDepth + 5, new Vector3(120f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			}
			mContinueLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			if (mPrice > mGame.mPlayer.Dollar)
			{
				mContinueLabel.color = new Color(66f / 85f, 0f, 0f);
			}
			if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES)
			{
				text = Util.MakeLText("Lose_AddMove_button", mData.addMove);
			}
			else if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
			{
				text = Util.MakeLText("Lose_AddTime_button", (int)mData.addTime);
			}
			UILabel uILabel = Util.CreateLabel("Desc", mButtonContinue.gameObject, atlasFont);
			if (flag)
			{
				Util.SetLabelInfo(uILabel, text, mBaseDepth + 5, new Vector3(-50f, -2f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
				uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
				uILabel.SetDimensions(150, 30);
			}
			else
			{
				Util.SetLabelInfo(uILabel, text, mBaseDepth + 5, new Vector3(-60f, -2f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
			}
			uILabel.color = Color.white;
			if (HasContinueChance)
			{
				UISprite uISprite = Util.CreateSprite("ContinueChance", mButtonContinue.gameObject, image);
				Util.SetSpriteInfo(uISprite, "instruction_accessory12", mBaseDepth + 6, new Vector3(0f, 35f, 0f), Vector3.one, false);
				uISprite.type = UIBasicSprite.Type.Sliced;
				uISprite.SetDimensions(230, 42);
				string[] array = mGame.mShopItemData[mContinueChancePair.Value.ShopItemID].SupplementKey.Split(',');
				if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES)
				{
					text = array[0];
				}
				else if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
				{
					text = array[1];
				}
				uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, Localization.Get(text), mBaseDepth + 7, new Vector3(0f, 6f, 0f), 18, 220, 0, UIWidget.Pivot.Center);
				Util.SetLabelColor(uILabel, Color.white);
				Util.SetLabeShrinkContent(uILabel, 220, 18);
			}
		}
		if (!string.IsNullOrEmpty(text3))
		{
			mButtonGiveup = Util.CreateJellyImageButton("ButtonGiveup", base.gameObject, image2);
			Util.SetImageButtonInfo(mButtonGiveup, text3, text3, text3, mBaseDepth + 4, Vector3.zero, Vector3.one);
			Util.AddImageButtonMessage(mButtonGiveup, this, "OnGiveupPushed", UIButtonMessage.Trigger.OnClick);
		}
		if (!string.IsNullOrEmpty(value))
		{
			mButtonSkip = Util.CreateJellyImageButton("ButtonStageSkip", base.gameObject, image2);
			Util.SetImageButtonInfo(mButtonSkip, "LC_button_clear", "LC_button_clear", "LC_button_clear", mBaseDepth + 4, Vector3.zero, Vector3.one);
			Util.AddImageButtonMessage(mButtonSkip, this, "OnStageSkipPushed", UIButtonMessage.Trigger.OnClick);
			mStageSkipLabel = Util.CreateLabel("StageSkipPrice", mButtonSkip.gameObject, atlasFont);
			Util.SetLabelInfo(mStageSkipLabel, string.Empty + mStageSkipData.GemAmount, mBaseDepth + 5, new Vector3(158f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			mStageSkipLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			if (mStageSkipData.GemAmount > mGame.mPlayer.Dollar)
			{
				mStageSkipLabel.color = new Color(66f / 85f, 0f, 0f);
			}
		}
		if (mTuxedoSkipMode)
		{
			if (mStageSkipMode)
			{
				if (mLoseReason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
				{
					mButtonGiveup.transform.localPosition = new Vector3(0f, -90f, 0f);
					mButtonSkip.transform.localPosition = new Vector3(0f, -170f, 0f);
				}
				else
				{
					mButtonContinue.transform.localPosition = new Vector3(-137f, -80f, 0f);
					mButtonGiveup.transform.localPosition = new Vector3(137f, -80f, 0f);
					mButtonSkip.transform.localPosition = new Vector3(0f, -160f, 0f);
				}
			}
			else
			{
				mButtonContinue.transform.localPosition = new Vector3(-137f, -122f, 0f);
				mButtonGiveup.transform.localPosition = new Vector3(137f, -122f, 0f);
			}
		}
		else if (mStageSkipMode)
		{
			if (mLoseReason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
			{
				mButtonGiveup.transform.localPosition = new Vector3(0f, -122f, 0f);
				mButtonSkip.transform.localPosition = new Vector3(0f, -212f, 0f);
			}
			else
			{
				mButtonContinue.transform.localPosition = new Vector3(-137f, -122f, 0f);
				mButtonGiveup.transform.localPosition = new Vector3(137f, -122f, 0f);
				mButtonSkip.transform.localPosition = new Vector3(0f, -212f, 0f);
			}
		}
		else
		{
			mButtonContinue.transform.localPosition = new Vector3(0f, -117f, 0f);
			mButtonGiveup.transform.localPosition = new Vector3(0f, -210f, 0f);
		}
		if (mButtonContinue != null)
		{
			mColorContinue.Clear();
			UIWidget[] componentsInChildren = mButtonContinue.GetComponentsInChildren<UIWidget>(true);
			UIWidget[] array2 = componentsInChildren;
			foreach (UIWidget uIWidget in array2)
			{
				if (uIWidget != null)
				{
					mColorContinue.Add(uIWidget, uIWidget.color);
					uIWidget.color = new Color(uIWidget.color.r * 0.5f, uIWidget.color.g * 0.5f, uIWidget.color.b * 0.5f);
				}
			}
			mButtonContinue.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		if (mButtonGiveup != null)
		{
			mColorGiveup.Clear();
			UILabel[] componentsInChildren2 = mButtonGiveup.GetComponentsInChildren<UILabel>(true);
			UILabel[] array3 = componentsInChildren2;
			foreach (UILabel uILabel2 in array3)
			{
				if (uILabel2 != null)
				{
					mColorGiveup.Add(uILabel2, uILabel2.color);
					uILabel2.color = new Color(uILabel2.color.r * 0.5f, uILabel2.color.g * 0.5f, uILabel2.color.b * 0.5f);
				}
			}
			mButtonGiveup.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		if (!(mButtonSkip != null))
		{
			return;
		}
		mColorSkip.Clear();
		UILabel[] componentsInChildren3 = mButtonSkip.GetComponentsInChildren<UILabel>(true);
		UILabel[] array4 = componentsInChildren3;
		foreach (UILabel uILabel3 in array4)
		{
			if (uILabel3 != null)
			{
				mColorSkip.Add(uILabel3, uILabel3.color);
				uILabel3.color = new Color(uILabel3.color.r * 0.5f, uILabel3.color.g * 0.5f, uILabel3.color.b * 0.5f);
			}
		}
		mButtonSkip.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
	}

	private IEnumerator CreateContinueButton()
	{
		yield return new WaitForSeconds(0.2f);
		if (mButtonContinue != null)
		{
			mButtonContinue.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
			if (mColorContinue != null)
			{
				foreach (KeyValuePair<UIWidget, Color> pair3 in mColorContinue)
				{
					pair3.Key.color = pair3.Value;
				}
			}
		}
		if (mButtonGiveup != null)
		{
			mButtonGiveup.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
			if (mColorGiveup != null)
			{
				foreach (KeyValuePair<UILabel, Color> pair2 in mColorGiveup)
				{
					pair2.Key.color = pair2.Value;
				}
			}
		}
		if (mButtonSkip != null)
		{
			mButtonSkip.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
			if (mColorSkip != null)
			{
				foreach (KeyValuePair<UILabel, Color> pair in mColorSkip)
				{
					pair.Key.color = pair.Value;
				}
			}
		}
		if (mStageSkipMode && mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL7_3) && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.STAGE_SKIP_0))
		{
			mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.STAGE_SKIP_0, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
			mGame.mTutorialManager.SetTutorialArrow(base.gameObject, mButtonSkip.gameObject);
			while (mGame.mTutorialManager.IsTutorialPlaying())
			{
				yield return 0;
			}
		}
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.LEVEL7_2)
		{
			mGame.mTutorialManager.AddAllowedButton(mButtonContinue.gameObject);
			mGame.mTutorialManager.SetTutorialFinger(base.gameObject, mButtonContinue.gameObject, Def.DIR.NONE);
		}
		mState.Change(STATE.MAIN);
	}

	public override void OnOpening()
	{
		base.OnOpening();
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public virtual IEnumerator CloseProcess()
	{
		ResourceManager.UnloadImage("PUZZLE_CONTINUE_HUD");
		ResourceManager.UnloadScriptableObject("SPECIAL_CHANCE");
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		if (mContinueChancePair.Key && mContinueChancePair.Value != null)
		{
			if (mGame.mPlayer.Data.ChanceTotalSaleCount.ContainsKey(mContinueChancePair.Value.ChanceID))
			{
				Dictionary<int, int> chanceTotalSaleCount;
				Dictionary<int, int> dictionary = (chanceTotalSaleCount = mGame.mPlayer.Data.ChanceTotalSaleCount);
				int chanceID2;
				int key = (chanceID2 = mContinueChancePair.Value.ChanceID);
				chanceID2 = chanceTotalSaleCount[chanceID2];
				dictionary[key] = chanceID2 + 1;
			}
			else
			{
				mGame.mPlayer.Data.ChanceTotalSaleCount.Add(mContinueChancePair.Value.ChanceID, 1);
			}
			int chanceID = mContinueChancePair.Value.ChanceID;
			int firstPurchase = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mGame.mOptions.FirstPurchasedTime);
			short type = 4;
			int curSeries = (int)mGame.mPlayer.Data.LastPlaySeries;
			int curStage = mGame.mPlayer.Data.LastPlayLevel;
			int evtId = 9999;
			if (mGame.IsDailyChallengePuzzle && GlobalVariables.Instance.SelectedDailyChallengeSetting != null)
			{
				int sId = ((mGame.mDebugDailyChallengeID <= 0) ? GlobalVariables.Instance.SelectedDailyChallengeSetting.DailyChallengeID : mGame.mDebugDailyChallengeID);
				curSeries = 200;
				curStage = int.Parse(string.Format("{0}{1}{2}", sId, (mGame.SelectedDailyChallengeSheetNo - 1).ToString("00"), mGame.SelectedDailyChallengeStageNo));
				evtId = sId;
			}
			else if (mGame.mEventMode)
			{
				evtId = mGame.mCurrentStage.EventID;
			}
			int result = 0;
			if (mSelectItem == SELECT_ITEM.CONTINUE)
			{
				result = 1;
			}
			if (!mGame.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE))
			{
				mGame.mPlayer.Data.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE, 0);
			}
			if (!mGame.mPlayer.Data.ChanceDispCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE))
			{
				mGame.mPlayer.Data.ChanceDispCount.Add(SpecialChanceConditionSet.CHANCE_MODE.CONTINUE, 0);
			}
			ServerCram.SaleSpecialChance(chanceID, type, evtId, curSeries, curStage, mGame.mPlayer.Data.ChanceDispCount[SpecialChanceConditionSet.CHANCE_MODE.CONTINUE], mGame.mPlayer.Data.ChanceTotalSaleCount[chanceID], mGame.mPlayer.Data.ChanceTotalOfferCount[chanceID], (int)mContinueChancePair.Value.UserSegment, result, firstPurchase, (int)mGame.mChargePriceCheckData.price, mGame.mOptions.CurrencyCode);
			bool buy = false;
			if (mSelectItem == SELECT_ITEM.CONTINUE)
			{
				buy = true;
			}
			short cost = (short)mGame.mShopItemData[mContinueChancePair.Value.ShopItemID].GemAmount;
			UserBehavior.Instance.CountSpecialChance(type, buy, cost);
			UserBehavior.Save();
			if (mSelectItem == SELECT_ITEM.CONTINUE)
			{
				mGame.mPlayer.Data.UseContinueChance = true;
				mGame.mPlayer.Data.ChanceDispCount[SpecialChanceConditionSet.CHANCE_MODE.CONTINUE] = 0;
			}
			mGame.Save();
		}
	}

	public override void OnBackKeyPress()
	{
		if (!mGame.mTutorialManager.IsTutorialPlaying() || mGame.mTutorialManager.GetCurrentTutorial() != Def.TUTORIAL_INDEX.LEVEL7_2)
		{
			OnGiveupPushed(null);
		}
	}

	public void OnPurchaseDialogClosed()
	{
		UILabel uILabel = mContinueLabel;
		if (uILabel != null)
		{
			if (mPrice > mGame.mPlayer.Dollar)
			{
				uILabel.color = new Color(66f / 85f, 0f, 0f);
			}
			else
			{
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			}
		}
		if (mStageSkipLabel != null)
		{
			if (mStageSkipData.GemAmount <= mGame.mPlayer.Dollar)
			{
				mStageSkipLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			}
			else
			{
				mStageSkipLabel.color = new Color(66f / 85f, 0f, 0f);
			}
		}
		mState.Change(STATE.MAIN);
	}

	public void OnContinuePushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mPrice <= mGame.mPlayer.Dollar)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			int num = mContinueCount;
			if (num >= mGame.mSegmentProfile.ContinueItemList.Count)
			{
				num = mGame.mSegmentProfile.ContinueItemList.Count - 1;
			}
			int key = mGame.mSegmentProfile.ContinueItemList[num];
			if (mContinueChancePair.Key && mContinueChancePair.Value != null)
			{
				key = mContinueChancePair.Value.ShopItemID;
			}
			mBoughtItem = mGame.mShopItemData[key];
			if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.LEVEL7_2)
			{
				mState.Change(STATE.NETWORK_02_2);
				return;
			}
			GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
			gemUseConfirmDialog.Init(mPrice, 12);
			gemUseConfirmDialog.SetClosedCallback(OnGemUseConfirmClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
			confirmDialog.SetClosedCallback(OnConfirmClosed);
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnStageSkipPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (mStageSkipData.GemAmount <= mGame.mPlayer.Dollar)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				mBoughtItem = mStageSkipData;
				GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
				gemUseConfirmDialog.Init(mStageSkipData.GemAmount, 15);
				gemUseConfirmDialog.SetClosedCallback(OnGemUseConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				confirmDialog.SetClosedCallback(OnConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
		}
	}

	public void OnGemUseConfirmClosed(GemUseConfirmDialog.SELECT_ITEM item, int aValue)
	{
		if (item == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Change(STATE.MAINTENANCE_CHECK_START02);
			mStateAfterMaintenanceCheck = (STATE)aValue;
		}
		else
		{
			mBoughtItem = null;
			mState.Change(STATE.MAIN);
		}
	}

	public void OnConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.CONTINUE_STAGE;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(PurchaseAuthError);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	private void PurchaseAuthError()
	{
		mState.Change(STATE.AUTHERROR_WAIT);
	}

	public void OnGiveupPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("Giveup_GiveupConfirmTitle"), Localization.Get("StarGet_Giveup"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				confirmDialog.SetClosedCallback(OnGiveupConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mSelectItem = SELECT_ITEM.GIVEUP;
				Close();
			}
		}
	}

	public void OnGiveupConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mSelectItem = SELECT_ITEM.GIVEUP;
			Close();
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void ShowNetworkErrorDialog(STATE aNextState)
	{
		mStateAfterNetworkError = aNextState;
		mState.Reset(STATE.WAIT, true);
		mErrorDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mErrorDialog.SetClosedCallback(OnNetworkErrorDialogClosed);
	}

	private void OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(mStateAfterNetworkError);
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			mGame.mTutorialManager.TutorialStart(tUTORIAL_INDEX, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
		}
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.TutorialSkip(index);
		}
	}

	private void OnDuplicateDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mGame.GetGemCountFreqFlag = false;
		mState.Change(STATE.NETWORK_00);
	}
}
