public class AdvSetRewardProfile_Request : RequestBase
{
	[MiniJSONAlias("receive_reward_ids")]
	public int[] RewardIDs { get; set; }

	[MiniJSONAlias("mailbox_reward_ids")]
	public int[] MailRewardIDs { get; set; }

	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }

	[MiniJSONAlias("is_retry")]
	public int IsRetry { get; set; }
}
