public interface NetworkInterface
{
	IOptions Options { get; }

	IPlayer Player { get; }

	NetworkProfile NetworkProfile { get; }

	GameProfile GameProfile { get; }

	UserAuthResponse UserUniqueID { get; }

	UserGetResponse UserProfile { get; }

	GameConcierge Concierge { get; }
}
