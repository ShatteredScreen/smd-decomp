using UnityEngine;

public class EventSign : MapSign
{
	protected short mCourseID;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public override void Init(int _counter, string _name, BIJImage atlas, float x, float y, Vector3 scale, int a_mapIndex, string a_animeKey)
	{
		mSeries = Def.SERIES.SM_FIRST;
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mAtlas = atlas;
		mScale = scale;
		float mAP_SIGN_Z = Def.MAP_SIGN_Z;
		base._pos = new Vector3(x, y, mAP_SIGN_Z);
		bool flag = true;
		Player mPlayer = mGame.mPlayer;
		if (mGame.mEventMode)
		{
			PlayerEventData data;
			mPlayer.Data.GetMapData(mSeries, mPlayer.Data.CurrentEventID, out data);
			if (data != null && data.CourseClearFlag.ContainsKey(mCourseID))
			{
				flag = !data.CourseClearFlag[mCourseID];
			}
		}
		else
		{
			flag = true;
		}
		if (flag)
		{
			IsLockTapAnime = false;
			PlayAnime(AnimeKey + "_LOCK", true);
		}
		else
		{
			PlayAnime(AnimeKey + "_UNLOCK_IDLE", true);
		}
		HasTapAnime = false;
		mFirstInitialized = true;
	}
}
