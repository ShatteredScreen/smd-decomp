using UnityEngine;

public static class BIJExtensions
{
	public static void SetLocalPosition(this Component self, Vector2 pos)
	{
		self.SetLocalPosition(pos.x, pos.y);
	}

	public static void SetLocalPosition(this Component self, Vector3 pos)
	{
		self.SetLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void SetLocalPosition(this Component self, float x, float y)
	{
		self.SetLocalPosition(x, y, 0f);
	}

	public static void SetLocalPosition(this Component self, float x, float y, float z)
	{
		self.transform.localPosition = new Vector3(x, y, z);
	}

	public static void SetLocalPositionX(this Component self, float x)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.SetLocalPosition(x, localPosition.y, localPosition.z);
	}

	public static void SetLocalPositionY(this Component self, float y)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.SetLocalPosition(localPosition.x, y, localPosition.z);
	}

	public static void SetLocalPositionZ(this Component self, float z)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.SetLocalPosition(localPosition.x, localPosition.y, z);
	}

	public static void SetLocalPosition(this Transform self, Vector2 pos)
	{
		self.SetLocalPosition(pos.x, pos.y);
	}

	public static void SetLocalPosition(this Transform self, Vector3 pos)
	{
		self.SetLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void SetLocalPosition(this Transform self, float x, float y)
	{
		self.SetLocalPosition(x, y, 0f);
	}

	public static void SetLocalPosition(this Transform self, float x, float y, float z)
	{
		self.localPosition = new Vector3(x, y, z);
	}

	public static void SetLocalPositionX(this Transform self, float x)
	{
		Vector3 localPosition = self.localPosition;
		self.SetLocalPosition(x, localPosition.y, localPosition.z);
	}

	public static void SetLocalPositionY(this Transform self, float y)
	{
		Vector3 localPosition = self.localPosition;
		self.SetLocalPosition(localPosition.x, y, localPosition.z);
	}

	public static void SetLocalPositionZ(this Transform self, float z)
	{
		Vector3 localPosition = self.localPosition;
		self.SetLocalPosition(localPosition.x, localPosition.y, z);
	}

	public static void AddLocalPosition(this Component self, Vector2 pos)
	{
		self.AddLocalPosition(pos.x, pos.y);
	}

	public static void AddLocalPosition(this Component self, Vector3 pos)
	{
		self.AddLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void AddLocalPosition(this Component self, float x, float y)
	{
		self.AddLocalPosition(x, y, 0f);
	}

	public static void AddLocalPosition(this Component self, float x, float y, float z)
	{
		self.transform.localPosition += new Vector3(x, y, z);
	}

	public static void AddLocalPositionX(this Component self, float x)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.AddLocalPosition(x, 0f, 0f);
	}

	public static void AddLocalPositionY(this Component self, float y)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.AddLocalPosition(0f, y, 0f);
	}

	public static void AddLocalPositionZ(this Component self, float z)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.AddLocalPosition(0f, 0f, z);
	}

	public static void AddLocalPosition(this Transform self, Vector2 pos)
	{
		self.AddLocalPosition(pos.x, pos.y);
	}

	public static void AddLocalPosition(this Transform self, Vector3 pos)
	{
		self.AddLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void AddLocalPosition(this Transform self, float x, float y)
	{
		self.AddLocalPosition(x, y, 0f);
	}

	public static void AddLocalPosition(this Transform self, float x, float y, float z)
	{
		self.localPosition += new Vector3(x, y, z);
	}

	public static void AddLocalPositionX(this Transform self, float x)
	{
		Vector3 localPosition = self.localPosition;
		self.AddLocalPosition(x, 0f, 0f);
	}

	public static void AddLocalPositionY(this Transform self, float y)
	{
		Vector3 localPosition = self.localPosition;
		self.AddLocalPosition(0f, y, 0f);
	}

	public static void AddLocalPositionZ(this Transform self, float z)
	{
		Vector3 localPosition = self.localPosition;
		self.AddLocalPosition(0f, 0f, z);
	}

	public static void SubLocalPosition(this Component self, Vector2 pos)
	{
		self.SubLocalPosition(pos.x, pos.y);
	}

	public static void SubLocalPosition(this Component self, Vector3 pos)
	{
		self.SubLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void SubLocalPosition(this Component self, float x, float y)
	{
		self.SubLocalPosition(x, y, 0f);
	}

	public static void SubLocalPosition(this Component self, float x, float y, float z)
	{
		self.transform.localPosition -= new Vector3(x, y, z);
	}

	public static void SubLocalPositionX(this Component self, float x)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.SubLocalPosition(x, 0f, 0f);
	}

	public static void SubLocalPositionY(this Component self, float y)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.SubLocalPosition(0f, y, 0f);
	}

	public static void SubLocalPositionZ(this Component self, float z)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.SubLocalPosition(0f, 0f, z);
	}

	public static void SubLocalPosition(this Transform self, Vector3 pos)
	{
		self.SubLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void SubLocalPosition(this Transform self, float x, float y)
	{
		self.SubLocalPosition(x, y, 0f);
	}

	public static void SubLocalPosition(this Transform self, float x, float y, float z)
	{
		self.localPosition -= new Vector3(x, y, z);
	}

	public static void SubLocalPositionX(this Transform self, float x)
	{
		Vector3 localPosition = self.localPosition;
		self.SubLocalPosition(x, 0f, 0f);
	}

	public static void SubLocalPositionY(this Transform self, float y)
	{
		Vector3 localPosition = self.localPosition;
		self.SubLocalPosition(0f, y, 0f);
	}

	public static void SubLocalPositionZ(this Transform self, float z)
	{
		Vector3 localPosition = self.localPosition;
		self.SubLocalPosition(0f, 0f, z);
	}

	public static void MulLocalPosition(this Component self, Vector2 pos)
	{
		self.MulLocalPosition(pos.x, pos.y);
	}

	public static void MulLocalPosition(this Component self, Vector3 pos)
	{
		self.MulLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void MulLocalPosition(this Component self, float x, float y)
	{
		self.MulLocalPosition(x, y, 1f);
	}

	public static void MulLocalPosition(this Component self, float x, float y, float z)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.transform.localPosition = new Vector3(localPosition.x * x, localPosition.y * y, localPosition.z * z);
	}

	public static void MulLocalPositionX(this Component self, float x)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.MulLocalPosition(x, 1f, 1f);
	}

	public static void MulLocalPositionY(this Component self, float y)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.MulLocalPosition(1f, y, 1f);
	}

	public static void MulLocalPositionZ(this Component self, float z)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.MulLocalPosition(1f, 1f, z);
	}

	public static void MulLocalPosition(this Transform self, Vector2 pos)
	{
		self.MulLocalPosition(pos.x, pos.y);
	}

	public static void MulLocalPosition(this Transform self, Vector3 pos)
	{
		self.MulLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void MulLocalPosition(this Transform self, float x, float y)
	{
		self.MulLocalPosition(x, y, 1f);
	}

	public static void MulLocalPosition(this Transform self, float x, float y, float z)
	{
		Vector3 localPosition = self.localPosition;
		self.localPosition = new Vector3(localPosition.x * x, localPosition.y * y, localPosition.z * z);
	}

	public static void MulLocalPositionX(this Transform self, float x)
	{
		Vector3 localPosition = self.localPosition;
		self.MulLocalPosition(x, 1f, 1f);
	}

	public static void MulLocalPositionY(this Transform self, float y)
	{
		Vector3 localPosition = self.localPosition;
		self.MulLocalPosition(1f, y, 1f);
	}

	public static void MulLocalPositionZ(this Transform self, float z)
	{
		Vector3 localPosition = self.localPosition;
		self.MulLocalPosition(1f, 1f, z);
	}

	public static void DivLocalPosition(this Component self, Vector2 pos)
	{
		self.DivLocalPosition(pos.x, pos.y);
	}

	public static void DivLocalPosition(this Component self, Vector3 pos)
	{
		self.DivLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void DivLocalPosition(this Component self, float x, float y)
	{
		self.DivLocalPosition(x, y, 1f);
	}

	public static void DivLocalPosition(this Component self, float x, float y, float z)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.transform.localPosition = new Vector3(localPosition.x / ((x == 0f) ? 1f : x), localPosition.y / ((y == 0f) ? 1f : y), localPosition.z / ((z == 0f) ? 1f : z));
	}

	public static void DivLocalPositionX(this Component self, float x)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.DivLocalPosition(x, 1f, 1f);
	}

	public static void DivLocalPositionY(this Component self, float y)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.DivLocalPosition(1f, y, 1f);
	}

	public static void DivLocalPositionZ(this Component self, float z)
	{
		Vector3 localPosition = self.transform.localPosition;
		self.DivLocalPosition(1f, 1f, z);
	}

	public static void DivLocalPosition(this Transform self, Vector2 pos)
	{
		self.DivLocalPosition(pos.x, pos.y);
	}

	public static void DivLocalPosition(this Transform self, Vector3 pos)
	{
		self.DivLocalPosition(pos.x, pos.y, pos.z);
	}

	public static void DivLocalPosition(this Transform self, float x, float y)
	{
		self.DivLocalPosition(x, y, 1f);
	}

	public static void DivLocalPosition(this Transform self, float x, float y, float z)
	{
		Vector3 localPosition = self.localPosition;
		self.localPosition = new Vector3(localPosition.x / ((x == 0f) ? 1f : x), localPosition.y / ((y == 0f) ? 1f : y), localPosition.z / ((z == 0f) ? 1f : z));
	}

	public static void DivLocalPositionX(this Transform self, float x)
	{
		Vector3 localPosition = self.localPosition;
		self.DivLocalPosition(x, 1f, 1f);
	}

	public static void DivLocalPositionY(this Transform self, float y)
	{
		Vector3 localPosition = self.localPosition;
		self.DivLocalPosition(1f, y, 1f);
	}

	public static void DivLocalPositionZ(this Transform self, float z)
	{
		Vector3 localPosition = self.localPosition;
		self.DivLocalPosition(1f, 1f, z);
	}

	public static void SetPosition(this Component self, Vector2 pos)
	{
		self.SetPosition(pos.x, pos.y);
	}

	public static void SetPosition(this Component self, Vector3 pos)
	{
		self.SetPosition(pos.x, pos.y, pos.z);
	}

	public static void SetPosition(this Component self, float x, float y)
	{
		self.SetPosition(x, y, 0f);
	}

	public static void SetPosition(this Component self, float x, float y, float z)
	{
		self.transform.position = new Vector3(x, y, z);
	}

	public static void SetPositionX(this Component self, float x)
	{
		Vector3 position = self.transform.position;
		self.SetPosition(x, position.y, position.z);
	}

	public static void SetPositionY(this Component self, float y)
	{
		Vector3 position = self.transform.position;
		self.SetPosition(position.x, y, position.z);
	}

	public static void SetPositionZ(this Component self, float z)
	{
		Vector3 position = self.transform.position;
		self.SetPosition(position.x, position.y, z);
	}

	public static void SetPosition(this Transform self, Vector2 pos)
	{
		self.SetPosition(pos.x, pos.y);
	}

	public static void SetPosition(this Transform self, Vector3 pos)
	{
		self.SetPosition(pos.x, pos.y, pos.z);
	}

	public static void SetPosition(this Transform self, float x, float y)
	{
		self.SetPosition(x, y, 0f);
	}

	public static void SetPosition(this Transform self, float x, float y, float z)
	{
		self.position = new Vector3(x, y, z);
	}

	public static void SetPositionX(this Transform self, float x)
	{
		Vector3 position = self.position;
		self.SetPosition(x, position.y, position.z);
	}

	public static void SetPositionY(this Transform self, float y)
	{
		Vector3 position = self.position;
		self.SetPosition(position.x, y, position.z);
	}

	public static void SetPositionZ(this Transform self, float z)
	{
		Vector3 position = self.position;
		self.SetPosition(position.x, position.y, z);
	}

	public static void AddPosition(this Component self, Vector2 pos)
	{
		self.AddPosition(pos.x, pos.y);
	}

	public static void AddPosition(this Component self, Vector3 pos)
	{
		self.AddPosition(pos.x, pos.y, pos.z);
	}

	public static void AddPosition(this Component self, float x, float y)
	{
		self.AddPosition(x, y, 0f);
	}

	public static void AddPosition(this Component self, float x, float y, float z)
	{
		self.transform.position += new Vector3(x, y, z);
	}

	public static void AddPositionX(this Component self, float x)
	{
		Vector3 position = self.transform.position;
		self.AddPosition(x, 0f, 0f);
	}

	public static void AddPositionY(this Component self, float y)
	{
		Vector3 position = self.transform.position;
		self.AddPosition(0f, y, 0f);
	}

	public static void AddPositionZ(this Component self, float z)
	{
		Vector3 position = self.transform.position;
		self.AddPosition(0f, 0f, z);
	}

	public static void AddPosition(this Transform self, Vector2 pos)
	{
		self.AddPosition(pos.x, pos.y);
	}

	public static void AddPosition(this Transform self, Vector3 pos)
	{
		self.AddPosition(pos.x, pos.y, pos.z);
	}

	public static void AddPosition(this Transform self, float x, float y)
	{
		self.AddPosition(x, y, 0f);
	}

	public static void AddPosition(this Transform self, float x, float y, float z)
	{
		self.position += new Vector3(x, y, z);
	}

	public static void AddPositionX(this Transform self, float x)
	{
		Vector3 position = self.position;
		self.AddPosition(x, 0f, 0f);
	}

	public static void AddPositionY(this Transform self, float y)
	{
		Vector3 position = self.position;
		self.AddPosition(0f, y, 0f);
	}

	public static void AddPositionZ(this Transform self, float z)
	{
		Vector3 position = self.position;
		self.AddPosition(0f, 0f, z);
	}

	public static void SubPosition(this Component self, Vector2 pos)
	{
		self.SubPosition(pos.x, pos.y);
	}

	public static void SubPosition(this Component self, Vector3 pos)
	{
		self.SubPosition(pos.x, pos.y, pos.z);
	}

	public static void SubPosition(this Component self, float x, float y)
	{
		self.SubPosition(x, y, 0f);
	}

	public static void SubPosition(this Component self, float x, float y, float z)
	{
		self.transform.position -= new Vector3(x, y, z);
	}

	public static void SubPositionX(this Component self, float x)
	{
		Vector3 position = self.transform.position;
		self.SubPosition(x, 0f, 0f);
	}

	public static void SubPositionY(this Component self, float y)
	{
		Vector3 position = self.transform.position;
		self.SubPosition(0f, y, 0f);
	}

	public static void SubPositionZ(this Component self, float z)
	{
		Vector3 position = self.transform.position;
		self.SubPosition(0f, 0f, z);
	}

	public static void SubPosition(this Transform self, Vector2 pos)
	{
		self.SubPosition(pos.x, pos.y);
	}

	public static void SubPosition(this Transform self, Vector3 pos)
	{
		self.SubPosition(pos.x, pos.y, pos.z);
	}

	public static void SubPosition(this Transform self, float x, float y)
	{
		self.SubPosition(x, y, 0f);
	}

	public static void SubPosition(this Transform self, float x, float y, float z)
	{
		self.position -= new Vector3(x, y, z);
	}

	public static void SubPositionX(this Transform self, float x)
	{
		Vector3 position = self.position;
		self.SubPosition(x, 0f, 0f);
	}

	public static void SubPositionY(this Transform self, float y)
	{
		Vector3 position = self.position;
		self.SubPosition(0f, y, 0f);
	}

	public static void SubPositionZ(this Transform self, float z)
	{
		Vector3 position = self.position;
		self.SubPosition(0f, 0f, z);
	}

	public static void MulPosition(this Component self, Vector2 pos)
	{
		self.MulPosition(pos.x, pos.y);
	}

	public static void MulPosition(this Component self, Vector3 pos)
	{
		self.MulPosition(pos.x, pos.y, pos.z);
	}

	public static void MulPosition(this Component self, float x, float y)
	{
		self.MulPosition(x, y, 0f);
	}

	public static void MulPosition(this Component self, float x, float y, float z)
	{
		Vector3 position = self.transform.position;
		self.transform.position = new Vector3(position.x * x, position.y * y, position.z * z);
	}

	public static void MulPositionX(this Component self, float x)
	{
		Vector3 position = self.transform.position;
		self.MulPosition(x, 1f, 1f);
	}

	public static void MulPositionY(this Component self, float y)
	{
		Vector3 position = self.transform.position;
		self.MulPosition(1f, y, 1f);
	}

	public static void MulPositionZ(this Component self, float z)
	{
		Vector3 position = self.transform.position;
		self.MulPosition(1f, 1f, z);
	}

	public static void MulPosition(this Transform self, Vector2 pos)
	{
		self.MulPosition(pos.x, pos.y);
	}

	public static void MulPosition(this Transform self, Vector3 pos)
	{
		self.MulPosition(pos.x, pos.y, pos.z);
	}

	public static void MulPosition(this Transform self, float x, float y)
	{
		self.MulPosition(x, y, 0f);
	}

	public static void MulPosition(this Transform self, float x, float y, float z)
	{
		Vector3 position = self.position;
		self.position = new Vector3(position.x * x, position.y * y, position.z * z);
	}

	public static void MulPositionX(this Transform self, float x)
	{
		Vector3 position = self.position;
		self.MulPosition(x, 1f, 1f);
	}

	public static void MulPositionY(this Transform self, float y)
	{
		Vector3 position = self.position;
		self.MulPosition(1f, y, 1f);
	}

	public static void MulPositionZ(this Transform self, float z)
	{
		Vector3 position = self.position;
		self.MulPosition(1f, 1f, z);
	}

	public static void DivPosition(this Component self, Vector2 pos)
	{
		self.DivPosition(pos.x, pos.y);
	}

	public static void DivPosition(this Component self, Vector3 pos)
	{
		self.DivPosition(pos.x, pos.y, pos.z);
	}

	public static void DivPosition(this Component self, float x, float y)
	{
		self.DivPosition(x, y, 0f);
	}

	public static void DivPosition(this Component self, float x, float y, float z)
	{
		Vector3 position = self.transform.position;
		self.transform.position = new Vector3(position.x / ((x == 0f) ? 1f : x), position.y / ((y == 0f) ? 1f : y), position.z / ((z == 0f) ? 1f : z));
	}

	public static void DivPositionX(this Component self, float x)
	{
		Vector3 position = self.transform.position;
		self.DivPosition(x, 1f, 1f);
	}

	public static void DivPositionY(this Component self, float y)
	{
		Vector3 position = self.transform.position;
		self.DivPosition(1f, y, 1f);
	}

	public static void DivPositionZ(this Component self, float z)
	{
		Vector3 position = self.transform.position;
		self.DivPosition(1f, 1f, z);
	}

	public static void DivPosition(this Transform self, Vector2 pos)
	{
		self.DivPosition(pos.x, pos.y);
	}

	public static void DivPosition(this Transform self, Vector3 pos)
	{
		self.DivPosition(pos.x, pos.y, pos.z);
	}

	public static void DivPosition(this Transform self, float x, float y)
	{
		self.DivPosition(x, y, 0f);
	}

	public static void DivPosition(this Transform self, float x, float y, float z)
	{
		Vector3 position = self.position;
		self.position = new Vector3(position.x / ((x == 0f) ? 1f : x), position.y / ((y == 0f) ? 1f : y), position.z / ((z == 0f) ? 1f : z));
	}

	public static void DivPositionX(this Transform self, float x)
	{
		Vector3 position = self.position;
		self.DivPosition(x, 1f, 1f);
	}

	public static void DivPositionY(this Transform self, float y)
	{
		Vector3 position = self.position;
		self.DivPosition(1f, y, 1f);
	}

	public static void DivPositionZ(this Transform self, float z)
	{
		Vector3 position = self.position;
		self.DivPosition(1f, 1f, z);
	}

	public static void SetLocalEulerAngles(this Component self, Vector3 pos)
	{
		self.SetLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void SetLocalEulerAngles(this Component self, float x, float y, float z)
	{
		self.transform.localEulerAngles = new Vector3(x, y, z);
	}

	public static void SetLocalEulerAngles(this Component self, float value)
	{
		self.SetLocalEulerAnglesY(value);
	}

	public static void SetLocalEulerAnglesX(this Component self, float x)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.SetLocalEulerAngles(x, localEulerAngles.y, localEulerAngles.z);
	}

	public static void SetLocalEulerAnglesY(this Component self, float y)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.SetLocalEulerAngles(localEulerAngles.x, y, localEulerAngles.z);
	}

	public static void SetLocalEulerAnglesZ(this Component self, float z)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.SetLocalEulerAngles(localEulerAngles.x, localEulerAngles.y, z);
	}

	public static void SetLocalEulerAngles(this Transform self, Vector3 pos)
	{
		self.SetLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void SetLocalEulerAngles(this Transform self, float x, float y, float z)
	{
		self.localEulerAngles = new Vector3(x, y, z);
	}

	public static void SetLocalEulerAngles(this Transform self, float value)
	{
		self.SetLocalEulerAnglesY(value);
	}

	public static void SetLocalEulerAnglesX(this Transform self, float x)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.SetLocalEulerAngles(x, localEulerAngles.y, localEulerAngles.z);
	}

	public static void SetLocalEulerAnglesY(this Transform self, float y)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.SetLocalEulerAngles(localEulerAngles.x, y, localEulerAngles.z);
	}

	public static void SetLocalEulerAnglesZ(this Transform self, float z)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.SetLocalEulerAngles(localEulerAngles.x, localEulerAngles.y, z);
	}

	public static void AddLocalEulerAngles(this Component self, Vector3 pos)
	{
		self.AddLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void AddLocalEulerAngles(this Component self, float x, float y, float z)
	{
		self.transform.localEulerAngles += new Vector3(x, y, z);
	}

	public static void AddLocalEulerAngles(this Component self, float value)
	{
		self.AddLocalEulerAnglesY(value);
	}

	public static void AddLocalEulerAnglesX(this Component self, float x)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.AddLocalEulerAngles(x, 0f, 0f);
	}

	public static void AddLocalEulerAnglesY(this Component self, float y)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.AddLocalEulerAngles(0f, y, 0f);
	}

	public static void AddLocalEulerAnglesZ(this Component self, float z)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.AddLocalEulerAngles(0f, 0f, z);
	}

	public static void AddLocalEulerAngles(this Transform self, Vector3 pos)
	{
		self.AddLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void AddLocalEulerAngles(this Transform self, float x, float y, float z)
	{
		self.localEulerAngles += new Vector3(x, y, z);
	}

	public static void AddLocalEulerAngles(this Transform self, float value)
	{
		self.AddLocalEulerAnglesY(value);
	}

	public static void AddLocalEulerAnglesX(this Transform self, float x)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.AddLocalEulerAngles(x, 0f, 0f);
	}

	public static void AddLocalEulerAnglesY(this Transform self, float y)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.AddLocalEulerAngles(0f, y, 0f);
	}

	public static void AddLocalEulerAnglesZ(this Transform self, float z)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.AddLocalEulerAngles(0f, 0f, z);
	}

	public static void SubLocalEulerAngles(this Component self, Vector3 pos)
	{
		self.SubLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void SubLocalEulerAngles(this Component self, float x, float y, float z)
	{
		self.transform.localEulerAngles -= new Vector3(x, y, z);
	}

	public static void SubLocalEulerAngles(this Component self, float value)
	{
		self.SubLocalEulerAnglesY(value);
	}

	public static void SubLocalEulerAnglesX(this Component self, float x)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.SubLocalEulerAngles(x, 0f, 0f);
	}

	public static void SubLocalEulerAnglesY(this Component self, float y)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.SubLocalEulerAngles(0f, y, 0f);
	}

	public static void SubLocalEulerAnglesZ(this Component self, float z)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.SubLocalEulerAngles(0f, 0f, z);
	}

	public static void SubLocalEulerAngles(this Transform self, Vector3 pos)
	{
		self.SubLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void SubLocalEulerAngles(this Transform self, float x, float y, float z)
	{
		self.localEulerAngles -= new Vector3(x, y, z);
	}

	public static void SubLocalEulerAngles(this Transform self, float value)
	{
		self.SubLocalEulerAnglesY(value);
	}

	public static void SubLocalEulerAnglesX(this Transform self, float x)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.SubLocalEulerAngles(x, 0f, 0f);
	}

	public static void SubLocalEulerAnglesY(this Transform self, float y)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.SubLocalEulerAngles(0f, y, 0f);
	}

	public static void SubLocalEulerAnglesZ(this Transform self, float z)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.SubLocalEulerAngles(0f, 0f, z);
	}

	public static void MulLocalEulerAngles(this Component self, Vector3 pos)
	{
		self.MulLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void MulLocalEulerAngles(this Component self, float x, float y, float z)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.transform.localEulerAngles = new Vector3(localEulerAngles.x * x, localEulerAngles.y * y, localEulerAngles.z * z);
	}

	public static void MulLocalEulerAngles(this Component self, float value)
	{
		self.MulLocalEulerAnglesY(value);
	}

	public static void MulLocalEulerAnglesX(this Component self, float x)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.MulLocalEulerAngles(x, 1f, 1f);
	}

	public static void MulLocalEulerAnglesY(this Component self, float y)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.MulLocalEulerAngles(1f, y, 1f);
	}

	public static void MulLocalEulerAnglesZ(this Component self, float z)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.MulLocalEulerAngles(1f, 1f, z);
	}

	public static void MulLocalEulerAngles(this Transform self, Vector3 pos)
	{
		self.MulLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void MulLocalEulerAngles(this Transform self, float x, float y, float z)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.localEulerAngles = new Vector3(localEulerAngles.x * x, localEulerAngles.y * y, localEulerAngles.z * z);
	}

	public static void MulLocalEulerAngles(this Transform self, float value)
	{
		self.MulLocalEulerAnglesY(value);
	}

	public static void MulLocalEulerAnglesX(this Transform self, float x)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.MulLocalEulerAngles(x, 1f, 1f);
	}

	public static void MulLocalEulerAnglesY(this Transform self, float y)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.MulLocalEulerAngles(1f, y, 1f);
	}

	public static void MulLocalEulerAnglesZ(this Transform self, float z)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.MulLocalEulerAngles(1f, 1f, z);
	}

	public static void DivLocalEulerAngles(this Component self, Vector3 pos)
	{
		self.DivLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void DivLocalEulerAngles(this Component self, float x, float y, float z)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.transform.localEulerAngles = new Vector3(localEulerAngles.x / ((x == 0f) ? 1f : x), localEulerAngles.y / ((y == 0f) ? 1f : y), localEulerAngles.z / ((z == 0f) ? 1f : z));
	}

	public static void DivLocalEulerAngles(this Component self, float value)
	{
		self.DivLocalEulerAnglesY(value);
	}

	public static void DivLocalEulerAnglesX(this Component self, float x)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.DivLocalEulerAngles(x, 1f, 1f);
	}

	public static void DivLocalEulerAnglesY(this Component self, float y)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.DivLocalEulerAngles(1f, y, 1f);
	}

	public static void DivLocalEulerAnglesZ(this Component self, float z)
	{
		Vector3 localEulerAngles = self.transform.localEulerAngles;
		self.DivLocalEulerAngles(1f, 1f, z);
	}

	public static void DivLocalEulerAngles(this Transform self, Vector3 pos)
	{
		self.DivLocalEulerAngles(pos.x, pos.y, pos.z);
	}

	public static void DivLocalEulerAngles(this Transform self, float x, float y, float z)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.localEulerAngles = new Vector3(localEulerAngles.x / ((x == 0f) ? 1f : x), localEulerAngles.y / ((y == 0f) ? 1f : y), localEulerAngles.z / ((z == 0f) ? 1f : z));
	}

	public static void DivLocalEulerAngles(this Transform self, float value)
	{
		self.DivLocalEulerAnglesY(value);
	}

	public static void DivLocalEulerAnglesX(this Transform self, float x)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.DivLocalEulerAngles(x, 1f, 1f);
	}

	public static void DivLocalEulerAnglesY(this Transform self, float y)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.DivLocalEulerAngles(1f, y, 1f);
	}

	public static void DivLocalEulerAnglesZ(this Transform self, float z)
	{
		Vector3 localEulerAngles = self.localEulerAngles;
		self.DivLocalEulerAngles(1f, 1f, z);
	}

	public static void SetLocalRotation(this Component self, Quaternion pos)
	{
		self.SetLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void SetLocalRotation(this Component self, float x, float y, float z, float w)
	{
		self.transform.localRotation = new Quaternion(x, y, z, w);
	}

	public static void SetLocalRotationX(this Component self, float x)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SetLocalRotation(x, localRotation.y, localRotation.z, localRotation.w);
	}

	public static void SetLocalRotationY(this Component self, float y)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SetLocalRotation(localRotation.x, y, localRotation.z, localRotation.w);
	}

	public static void SetLocalRotationZ(this Component self, float z)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SetLocalRotation(localRotation.x, localRotation.y, z, localRotation.w);
	}

	public static void SetLocalRotationW(this Component self, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SetLocalRotation(localRotation.x, localRotation.y, localRotation.z, w);
	}

	public static void SetLocalRotation(this Transform self, Quaternion pos)
	{
		self.SetLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void SetLocalRotation(this Transform self, float x, float y, float z, float w)
	{
		self.localRotation = new Quaternion(x, y, z, w);
	}

	public static void SetLocalRotationX(this Transform self, float x)
	{
		Quaternion localRotation = self.localRotation;
		self.SetLocalRotation(x, localRotation.y, localRotation.z, localRotation.w);
	}

	public static void SetLocalRotationY(this Transform self, float y)
	{
		Quaternion localRotation = self.localRotation;
		self.SetLocalRotation(localRotation.x, y, localRotation.z, localRotation.w);
	}

	public static void SetLocalRotationZ(this Transform self, float z)
	{
		Quaternion localRotation = self.localRotation;
		self.SetLocalRotation(localRotation.x, localRotation.y, z, localRotation.w);
	}

	public static void SetLocalRotationW(this Transform self, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.SetLocalRotation(localRotation.x, localRotation.y, localRotation.z, w);
	}

	public static void AddLocalRotation(this Component self, Quaternion pos)
	{
		self.AddLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void AddLocalRotation(this Component self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.transform.localRotation = new Quaternion(localRotation.x + x, localRotation.y + y, localRotation.z + z, localRotation.w + w);
	}

	public static void AddLocalRotationX(this Component self, float x)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.AddLocalRotation(x, 0f, 0f, 0f);
	}

	public static void AddLocalRotationY(this Component self, float y)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.AddLocalRotation(0f, y, 0f, 0f);
	}

	public static void AddLocalRotationZ(this Component self, float z)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.AddLocalRotation(0f, 0f, z, 0f);
	}

	public static void AddLocalRotationW(this Component self, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.AddLocalRotation(0f, 0f, 0f, w);
	}

	public static void AddLocalRotation(this Transform self, Quaternion pos)
	{
		self.AddLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void AddLocalRotation(this Transform self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.localRotation = new Quaternion(localRotation.x + x, localRotation.y + y, localRotation.z + z, localRotation.w + w);
	}

	public static void AddLocalRotationX(this Transform self, float x)
	{
		Quaternion localRotation = self.localRotation;
		self.AddLocalRotation(x, 0f, 0f, 0f);
	}

	public static void AddLocalRotationY(this Transform self, float y)
	{
		Quaternion localRotation = self.localRotation;
		self.AddLocalRotation(0f, y, 0f, 0f);
	}

	public static void AddLocalRotationZ(this Transform self, float z)
	{
		Quaternion localRotation = self.localRotation;
		self.AddLocalRotation(0f, 0f, z, 0f);
	}

	public static void AddLocalRotationW(this Transform self, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.AddLocalRotation(0f, 0f, 0f, w);
	}

	public static void SubLocalRotation(this Component self, Quaternion pos)
	{
		self.SubLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void SubLocalRotation(this Component self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.transform.localRotation = new Quaternion(localRotation.x - x, localRotation.y - y, localRotation.z - z, localRotation.w - w);
	}

	public static void SubLocalRotationX(this Component self, float x)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SubLocalRotation(x, 0f, 0f, 0f);
	}

	public static void SubLocalRotationY(this Component self, float y)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SubLocalRotation(0f, y, 0f, 0f);
	}

	public static void SubLocalRotationZ(this Component self, float z)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SubLocalRotation(0f, 0f, z, 0f);
	}

	public static void SubLocalRotationW(this Component self, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.SubLocalRotation(0f, 0f, 0f, w);
	}

	public static void SubLocalRotation(this Transform self, Quaternion pos)
	{
		self.SubLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void SubLocalRotation(this Transform self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.localRotation = new Quaternion(localRotation.x - x, localRotation.y - y, localRotation.z - z, localRotation.w - w);
	}

	public static void SubLocalRotationX(this Transform self, float x)
	{
		Quaternion localRotation = self.localRotation;
		self.SubLocalRotation(x, 0f, 0f, 0f);
	}

	public static void SubLocalRotationY(this Transform self, float y)
	{
		Quaternion localRotation = self.localRotation;
		self.SubLocalRotation(0f, y, 0f, 0f);
	}

	public static void SubLocalRotationZ(this Transform self, float z)
	{
		Quaternion localRotation = self.localRotation;
		self.SubLocalRotation(0f, 0f, z, 0f);
	}

	public static void SubLocalRotationW(this Transform self, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.SubLocalRotation(0f, 0f, 0f, w);
	}

	public static void MulLocalRotation(this Component self, Quaternion pos)
	{
		self.MulLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void MulLocalRotation(this Component self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.transform.localRotation = new Quaternion(localRotation.x * x, localRotation.y * y, localRotation.z * z, localRotation.w * w);
	}

	public static void MulLocalRotationX(this Component self, float x)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.MulLocalRotation(x, 1f, 1f, 1f);
	}

	public static void MulLocalRotationY(this Component self, float y)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.MulLocalRotation(1f, y, 1f, 1f);
	}

	public static void MulLocalRotationZ(this Component self, float z)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.MulLocalRotation(1f, 1f, z, 1f);
	}

	public static void MulLocalRotationW(this Component self, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.MulLocalRotation(1f, 1f, 1f, w);
	}

	public static void MulLocalRotation(this Transform self, Quaternion pos)
	{
		self.MulLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void MulLocalRotation(this Transform self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.localRotation = new Quaternion(localRotation.x * x, localRotation.y * y, localRotation.z * z, localRotation.w * w);
	}

	public static void MulLocalRotationX(this Transform self, float x)
	{
		Quaternion localRotation = self.localRotation;
		self.MulLocalRotation(x, 1f, 1f, 1f);
	}

	public static void MulLocalRotationY(this Transform self, float y)
	{
		Quaternion localRotation = self.localRotation;
		self.MulLocalRotation(1f, y, 1f, 1f);
	}

	public static void MulLocalRotationZ(this Transform self, float z)
	{
		Quaternion localRotation = self.localRotation;
		self.MulLocalRotation(1f, 1f, z, 1f);
	}

	public static void MulLocalRotationW(this Transform self, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.MulLocalRotation(1f, 1f, 1f, w);
	}

	public static void DivLocalRotation(this Component self, Quaternion pos)
	{
		self.DivLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void DivLocalRotation(this Component self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.transform.localRotation = new Quaternion(localRotation.x / ((x == 0f) ? 1f : x), localRotation.y / ((y == 0f) ? 1f : y), localRotation.z / ((z == 0f) ? 1f : z), localRotation.w / ((w == 0f) ? 1f : w));
	}

	public static void DivLocalRotationX(this Component self, float x)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.DivLocalRotation(x, 1f, 1f, 1f);
	}

	public static void DivLocalRotationY(this Component self, float y)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.DivLocalRotation(1f, y, 1f, 1f);
	}

	public static void DivLocalRotationZ(this Component self, float z)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.DivLocalRotation(1f, 1f, z, 1f);
	}

	public static void DivLocalRotationW(this Component self, float w)
	{
		Quaternion localRotation = self.transform.localRotation;
		self.DivLocalRotation(1f, 1f, 1f, w);
	}

	public static void DivLocalRotation(this Transform self, Quaternion pos)
	{
		self.DivLocalRotation(pos.x, pos.y, pos.z, pos.w);
	}

	public static void DivLocalRotation(this Transform self, float x, float y, float z, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.localRotation = new Quaternion(localRotation.x / ((x == 0f) ? 1f : x), localRotation.y / ((y == 0f) ? 1f : y), localRotation.z / ((z == 0f) ? 1f : z), localRotation.w / ((w == 0f) ? 1f : w));
	}

	public static void DivLocalRotationX(this Transform self, float x)
	{
		Quaternion localRotation = self.localRotation;
		self.DivLocalRotation(x, 1f, 1f, 1f);
	}

	public static void DivLocalRotationY(this Transform self, float y)
	{
		Quaternion localRotation = self.localRotation;
		self.DivLocalRotation(1f, y, 1f, 1f);
	}

	public static void DivLocalRotationZ(this Transform self, float z)
	{
		Quaternion localRotation = self.localRotation;
		self.DivLocalRotation(1f, 1f, z, 1f);
	}

	public static void DivLocalRotationW(this Transform self, float w)
	{
		Quaternion localRotation = self.localRotation;
		self.DivLocalRotation(1f, 1f, 1f, w);
	}

	public static void SetLocalScale(this Component self, Vector2 pos)
	{
		self.SetLocalScale(pos.x, pos.y);
	}

	public static void SetLocalScale(this Component self, Vector3 pos)
	{
		self.SetLocalScale(pos.x, pos.y, pos.z);
	}

	public static void SetLocalScale(this Component self, float x, float y)
	{
		self.SetLocalScale(x, y, 0f);
	}

	public static void SetLocalScale(this Component self, float x, float y, float z)
	{
		self.transform.localScale = new Vector3(x, y, z);
	}

	public static void SetLocalScale(this Component self, float value)
	{
		self.SetLocalScale(value, value, value);
	}

	public static void SetLocalScaleX(this Component self, float x)
	{
		Vector3 localScale = self.transform.localScale;
		self.SetLocalScale(x, localScale.y, localScale.z);
	}

	public static void SetLocalScaleY(this Component self, float y)
	{
		Vector3 localScale = self.transform.localScale;
		self.SetLocalScale(localScale.x, y, localScale.z);
	}

	public static void SetLocalScaleZ(this Component self, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.SetLocalScale(localScale.x, localScale.y, z);
	}

	public static void SetLocalScale(this Transform self, Vector2 pos)
	{
		self.SetLocalScale(pos.x, pos.y);
	}

	public static void SetLocalScale(this Transform self, Vector3 pos)
	{
		self.SetLocalScale(pos.x, pos.y, pos.z);
	}

	public static void SetLocalScale(this Transform self, float x, float y)
	{
		self.SetLocalScale(x, y, 0f);
	}

	public static void SetLocalScale(this Transform self, float x, float y, float z)
	{
		self.localScale = new Vector3(x, y, z);
	}

	public static void SetLocalScale(this Transform self, float value)
	{
		self.SetLocalScale(value, value, value);
	}

	public static void SetLocalScaleX(this Transform self, float x)
	{
		Vector3 localScale = self.localScale;
		self.SetLocalScale(x, localScale.y, localScale.z);
	}

	public static void SetLocalScaleY(this Transform self, float y)
	{
		Vector3 localScale = self.localScale;
		self.SetLocalScale(localScale.x, y, localScale.z);
	}

	public static void SetLocalScaleZ(this Transform self, float z)
	{
		Vector3 localScale = self.localScale;
		self.SetLocalScale(localScale.x, localScale.y, z);
	}

	public static void AddLocalScale(this Component self, Vector2 pos)
	{
		self.AddLocalScale(pos.x, pos.y);
	}

	public static void AddLocalScale(this Component self, Vector3 pos)
	{
		self.AddLocalScale(pos.x, pos.y, pos.z);
	}

	public static void AddLocalScale(this Component self, float x, float y)
	{
		self.AddLocalScale(x, y, 0f);
	}

	public static void AddLocalScale(this Component self, float x, float y, float z)
	{
		self.transform.localScale += new Vector3(x, y, z);
	}

	public static void AddLocalScale(this Component self, float value)
	{
		self.AddLocalScale(value, value, value);
	}

	public static void AddLocalScaleX(this Component self, float x)
	{
		Vector3 localScale = self.transform.localScale;
		self.AddLocalScale(x, 0f, 0f);
	}

	public static void AddLocalScaleY(this Component self, float y)
	{
		Vector3 localScale = self.transform.localScale;
		self.AddLocalScale(0f, y, 0f);
	}

	public static void AddLocalScaleZ(this Component self, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.AddLocalScale(0f, 0f, z);
	}

	public static void AddLocalScale(this Transform self, Vector2 pos)
	{
		self.AddLocalScale(pos.x, pos.y);
	}

	public static void AddLocalScale(this Transform self, Vector3 pos)
	{
		self.AddLocalScale(pos.x, pos.y, pos.z);
	}

	public static void AddLocalScale(this Transform self, float x, float y)
	{
		self.AddLocalScale(x, y, 0f);
	}

	public static void AddLocalScale(this Transform self, float x, float y, float z)
	{
		self.localScale += new Vector3(x, y, z);
	}

	public static void AddLocalScale(this Transform self, float value)
	{
		self.AddLocalScale(value, value, value);
	}

	public static void AddLocalScaleX(this Transform self, float x)
	{
		Vector3 localScale = self.localScale;
		self.AddLocalScale(x, 0f, 0f);
	}

	public static void AddLocalScaleY(this Transform self, float y)
	{
		Vector3 localScale = self.localScale;
		self.AddLocalScale(0f, y, 0f);
	}

	public static void AddLocalScaleZ(this Transform self, float z)
	{
		Vector3 localScale = self.localScale;
		self.AddLocalScale(0f, 0f, z);
	}

	public static void SubLocalScale(this Component self, Vector2 pos)
	{
		self.SubLocalScale(pos.x, pos.y);
	}

	public static void SubLocalScale(this Component self, Vector3 pos)
	{
		self.SubLocalScale(pos.x, pos.y, pos.z);
	}

	public static void SubLocalScale(this Component self, float x, float y)
	{
		self.SubLocalScale(x, y, 0f);
	}

	public static void SubLocalScale(this Component self, float x, float y, float z)
	{
		self.transform.localScale -= new Vector3(x, y, z);
	}

	public static void SubLocalScale(this Component self, float value)
	{
		self.SubLocalScale(value, value, value);
	}

	public static void SubLocalScaleX(this Component self, float x)
	{
		Vector3 localScale = self.transform.localScale;
		self.SubLocalScale(x, 0f, 0f);
	}

	public static void SubLocalScaleY(this Component self, float y)
	{
		Vector3 localScale = self.transform.localScale;
		self.SubLocalScale(0f, y, 0f);
	}

	public static void SubLocalScaleZ(this Component self, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.SubLocalScale(0f, 0f, z);
	}

	public static void SubLocalScale(this Transform self, Vector2 pos)
	{
		self.SubLocalScale(pos.x, pos.y);
	}

	public static void SubLocalScale(this Transform self, Vector3 pos)
	{
		self.SubLocalScale(pos.x, pos.y, pos.z);
	}

	public static void SubLocalScale(this Transform self, float x, float y)
	{
		self.SubLocalScale(x, y, 0f);
	}

	public static void SubLocalScale(this Transform self, float x, float y, float z)
	{
		self.localScale -= new Vector3(x, y, z);
	}

	public static void SubLocalScale(this Transform self, float value)
	{
		self.SubLocalScale(value, value, value);
	}

	public static void SubLocalScaleX(this Transform self, float x)
	{
		Vector3 localScale = self.localScale;
		self.SubLocalScale(x, 0f, 0f);
	}

	public static void SubLocalScaleY(this Transform self, float y)
	{
		Vector3 localScale = self.localScale;
		self.SubLocalScale(0f, y, 0f);
	}

	public static void SubLocalScaleZ(this Transform self, float z)
	{
		Vector3 localScale = self.localScale;
		self.SubLocalScale(0f, 0f, z);
	}

	public static void MulLocalScale(this Component self, Vector2 pos)
	{
		self.MulLocalScale(pos.x, pos.y);
	}

	public static void MulLocalScale(this Component self, Vector3 pos)
	{
		self.MulLocalScale(pos.x, pos.y, pos.z);
	}

	public static void MulLocalScale(this Component self, float x, float y)
	{
		self.MulLocalScale(x, y, 0f);
	}

	public static void MulLocalScale(this Component self, float x, float y, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.transform.localScale = new Vector3(localScale.x * x, localScale.y * y, localScale.z * z);
	}

	public static void MulLocalScale(this Component self, float value)
	{
		self.MulLocalScale(value, value, value);
	}

	public static void MulLocalScaleX(this Component self, float x)
	{
		Vector3 localScale = self.transform.localScale;
		self.MulLocalScale(x, 1f, 1f);
	}

	public static void MulLocalScaleY(this Component self, float y)
	{
		Vector3 localScale = self.transform.localScale;
		self.MulLocalScale(1f, y, 1f);
	}

	public static void MulLocalScaleZ(this Component self, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.MulLocalScale(1f, 1f, z);
	}

	public static void MulLocalScale(this Transform self, Vector2 pos)
	{
		self.MulLocalScale(pos.x, pos.y);
	}

	public static void MulLocalScale(this Transform self, Vector3 pos)
	{
		self.MulLocalScale(pos.x, pos.y, pos.z);
	}

	public static void MulLocalScale(this Transform self, float x, float y)
	{
		self.MulLocalScale(x, y, 0f);
	}

	public static void MulLocalScale(this Transform self, float x, float y, float z)
	{
		Vector3 localScale = self.localScale;
		self.localScale = new Vector3(localScale.x * x, localScale.y * y, localScale.z * z);
	}

	public static void MulLocalScale(this Transform self, float value)
	{
		self.MulLocalScale(value, value, value);
	}

	public static void MulLocalScaleX(this Transform self, float x)
	{
		Vector3 localScale = self.localScale;
		self.MulLocalScale(x, 1f, 1f);
	}

	public static void MulLocalScaleY(this Transform self, float y)
	{
		Vector3 localScale = self.localScale;
		self.MulLocalScale(1f, y, 1f);
	}

	public static void MulLocalScaleZ(this Transform self, float z)
	{
		Vector3 localScale = self.localScale;
		self.MulLocalScale(1f, 1f, z);
	}

	public static void DivLocalScale(this Component self, Vector2 pos)
	{
		self.DivLocalScale(pos.x, pos.y);
	}

	public static void DivLocalScale(this Component self, Vector3 pos)
	{
		self.DivLocalScale(pos.x, pos.y, pos.z);
	}

	public static void DivLocalScale(this Component self, float x, float y)
	{
		self.DivLocalScale(x, y, 0f);
	}

	public static void DivLocalScale(this Component self, float x, float y, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.transform.localScale = new Vector3(localScale.x / ((x == 0f) ? 1f : x), localScale.y / ((y == 0f) ? 1f : y), localScale.z / ((z == 0f) ? 1f : z));
	}

	public static void DivLocalScale(this Component self, float value)
	{
		self.DivLocalScale(value, value, value);
	}

	public static void DivLocalScaleX(this Component self, float x)
	{
		Vector3 localScale = self.transform.localScale;
		self.DivLocalScale(x, 1f, 1f);
	}

	public static void DivLocalScaleY(this Component self, float y)
	{
		Vector3 localScale = self.transform.localScale;
		self.DivLocalScale(1f, y, 1f);
	}

	public static void DivLocalScaleZ(this Component self, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.DivLocalScale(1f, 1f, z);
	}

	public static void DivLocalScale(this Transform self, Vector2 pos)
	{
		self.DivLocalScale(pos.x, pos.y);
	}

	public static void DivLocalScale(this Transform self, Vector3 pos)
	{
		self.DivLocalScale(pos.x, pos.y, pos.z);
	}

	public static void DivLocalScale(this Transform self, float x, float y)
	{
		self.DivLocalScale(x, y, 0f);
	}

	public static void DivLocalScale(this Transform self, float x, float y, float z)
	{
		Vector3 localScale = self.localScale;
		self.localScale = new Vector3(localScale.x / ((x == 0f) ? 1f : x), localScale.y / ((y == 0f) ? 1f : y), localScale.z / ((z == 0f) ? 1f : z));
	}

	public static void DivLocalScale(this Transform self, float value)
	{
		self.DivLocalScale(value, value, value);
	}

	public static void DivLocalScaleX(this Transform self, float x)
	{
		Vector3 localScale = self.localScale;
		self.DivLocalScale(x, 1f, 1f);
	}

	public static void DivLocalScaleY(this Transform self, float y)
	{
		Vector3 localScale = self.localScale;
		self.DivLocalScale(1f, y, 1f);
	}

	public static void DivLocalScaleZ(this Transform self, float z)
	{
		Vector3 localScale = self.localScale;
		self.DivLocalScale(1f, 1f, z);
	}

	public static void RemLocalScale(this Component self, Vector2 pos)
	{
		self.RemLocalScale(pos.x, pos.y);
	}

	public static void RemLocalScale(this Component self, Vector3 pos)
	{
		self.RemLocalScale(pos.x, pos.y, pos.z);
	}

	public static void RemLocalScale(this Component self, float x, float y)
	{
		self.RemLocalScale(x, y, 0f);
	}

	public static void RemLocalScale(this Component self, float x, float y, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.transform.localScale = new Vector3(localScale.x % ((x == 0f) ? 1f : x), localScale.y % ((y == 0f) ? 1f : y), localScale.z % ((z == 0f) ? 1f : z));
	}

	public static void RemLocalScale(this Component self, float value)
	{
		self.RemLocalScale(value, value, value);
	}

	public static void RemLocalScaleX(this Component self, float x)
	{
		Vector3 localScale = self.transform.localScale;
		self.RemLocalScale(x, 1f, 1f);
	}

	public static void RemLocalScaleY(this Component self, float y)
	{
		Vector3 localScale = self.transform.localScale;
		self.RemLocalScale(1f, y, 1f);
	}

	public static void RemLocalScaleZ(this Component self, float z)
	{
		Vector3 localScale = self.transform.localScale;
		self.RemLocalScale(1f, 1f, z);
	}

	public static void RemLocalScale(this Transform self, Vector2 pos)
	{
		self.RemLocalScale(pos.x, pos.y);
	}

	public static void RemLocalScale(this Transform self, Vector3 pos)
	{
		self.RemLocalScale(pos.x, pos.y, pos.z);
	}

	public static void RemLocalScale(this Transform self, float x, float y)
	{
		self.RemLocalScale(x, y, 0f);
	}

	public static void RemLocalScale(this Transform self, float x, float y, float z)
	{
		Vector3 localScale = self.localScale;
		self.localScale = new Vector3(localScale.x % ((x == 0f) ? 1f : x), localScale.y % ((y == 0f) ? 1f : y), localScale.z % ((z == 0f) ? 1f : z));
	}

	public static void RemLocalScale(this Transform self, float value)
	{
		self.RemLocalScale(value, value, value);
	}

	public static void RemLocalScaleX(this Transform self, float x)
	{
		Vector3 localScale = self.localScale;
		self.RemLocalScale(x, 1f, 1f);
	}

	public static void RemLocalScaleY(this Transform self, float y)
	{
		Vector3 localScale = self.localScale;
		self.RemLocalScale(1f, y, 1f);
	}

	public static void RemLocalScaleZ(this Transform self, float z)
	{
		Vector3 localScale = self.localScale;
		self.RemLocalScale(1f, 1f, z);
	}

	public static void PowLocalScale(this Component self, Vector2 pos, int pow)
	{
		self.PowLocalScale(pos.x, pos.y, pow);
	}

	public static void PowLocalScale(this Component self, Vector3 pos, int pow)
	{
		self.PowLocalScale(pos.x, pos.y, pos.z, pow);
	}

	public static void PowLocalScale(this Component self, float x, float y, int pow)
	{
		self.PowLocalScale(x, y, 0f, pow);
	}

	public static void PowLocalScale(this Component self, float x, float y, float z, int pow)
	{
		Vector3 localScale = self.transform.localScale;
		self.transform.localScale = new Vector3(Mathf.Pow(x, pow), Mathf.Pow(y, pow), Mathf.Pow(z, pow));
	}

	public static void PowLocalScale(this Component self, float value, int pow)
	{
		self.PowLocalScale(value, value, value, pow);
	}

	public static void PowLocalScaleX(this Component self, float x, int pow)
	{
		Vector3 localScale = self.transform.localScale;
		self.transform.localScale = new Vector3(Mathf.Pow(x, pow), localScale.y, localScale.z);
	}

	public static void PowLocalScaleY(this Component self, float y, int pow)
	{
		Vector3 localScale = self.transform.localScale;
		self.transform.localScale = new Vector3(localScale.x, Mathf.Pow(y, pow), localScale.z);
	}

	public static void PowLocalScaleZ(this Component self, float z, int pow)
	{
		Vector3 localScale = self.transform.localScale;
		self.transform.localScale = new Vector3(localScale.x, localScale.y, Mathf.Pow(z, pow));
	}

	public static void PowLocalScale(this Transform self, Vector2 pos, int pow)
	{
		self.PowLocalScale(pos.x, pos.y, pow);
	}

	public static void PowLocalScale(this Transform self, Vector3 pos, int pow)
	{
		self.PowLocalScale(pos.x, pos.y, pos.z, pow);
	}

	public static void PowLocalScale(this Transform self, float x, float y, int pow)
	{
		self.PowLocalScale(x, y, 0f, pow);
	}

	public static void PowLocalScale(this Transform self, float x, float y, float z, int pow)
	{
		Vector3 localScale = self.localScale;
		self.localScale = new Vector3(Mathf.Pow(x, pow), Mathf.Pow(y, pow), Mathf.Pow(z, pow));
	}

	public static void PowLocalScale(this Transform self, float value, int pow)
	{
		self.PowLocalScale(value, value, value, pow);
	}

	public static void PowLocalScaleX(this Transform self, float x, int pow)
	{
		Vector3 localScale = self.localScale;
		self.localScale = new Vector3(Mathf.Pow(x, pow), localScale.y, localScale.z);
	}

	public static void PowLocalScaleY(this Transform self, float y, int pow)
	{
		Vector3 localScale = self.localScale;
		self.localScale = new Vector3(localScale.x, Mathf.Pow(y, pow), localScale.z);
	}

	public static void PowLocalScaleZ(this Transform self, float z, int pow)
	{
		Vector3 localScale = self.localScale;
		self.localScale = new Vector3(localScale.x, localScale.y, Mathf.Pow(z, pow));
	}
}
