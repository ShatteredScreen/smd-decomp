using System;

public class DefaultMiniJSONConverter : IMiniJSONConverter
{
	protected Type mReqType;

	public bool IsHandle
	{
		get
		{
			if (mReqType.IsEnum)
			{
				return true;
			}
			if (typeof(DateTime) == mReqType)
			{
				return true;
			}
			if (typeof(string) == mReqType)
			{
				return true;
			}
			switch (Type.GetTypeCode(mReqType))
			{
			case TypeCode.SByte:
			case TypeCode.Byte:
			case TypeCode.Int16:
			case TypeCode.UInt16:
			case TypeCode.Int32:
			case TypeCode.UInt32:
			case TypeCode.Int64:
			case TypeCode.UInt64:
			case TypeCode.Single:
			case TypeCode.Double:
			case TypeCode.Decimal:
				return true;
			default:
				return false;
			}
		}
	}

	public DefaultMiniJSONConverter(Type reqType)
	{
		mReqType = reqType;
	}

	protected static string ToSS(object obj)
	{
		return (obj != null) ? obj.ToString() : string.Empty;
	}

	protected static bool IsEmpty(object obj)
	{
		return string.Empty.Equals(ToSS(obj));
	}

	public virtual object ConvertTo(object obj)
	{
		if (mReqType.IsEnum)
		{
			return ToSS(obj);
		}
		if (typeof(DateTime) == mReqType)
		{
			return ToSS(obj);
		}
		if (typeof(string) == mReqType && obj != null)
		{
			return obj.ToString();
		}
		return obj;
	}

	public virtual object ConvertFrom(object json)
	{
		if (mReqType.IsEnum)
		{
			return Enum.Parse(mReqType, ToSS(json), true);
		}
		if (typeof(DateTime) == mReqType)
		{
			return DateTime.Parse(ToSS(json));
		}
		if (typeof(string) == mReqType)
		{
			return Convert.ToString(json);
		}
		switch (Type.GetTypeCode(mReqType))
		{
		case TypeCode.Byte:
			return Convert.ToByte(json);
		case TypeCode.SByte:
			return Convert.ToSByte(json);
		case TypeCode.UInt16:
			return Convert.ToUInt16(json);
		case TypeCode.UInt32:
			return Convert.ToUInt32(json);
		case TypeCode.UInt64:
			return Convert.ToUInt64(json);
		case TypeCode.Int16:
			return Convert.ToInt16(json);
		case TypeCode.Int32:
			return Convert.ToInt32(json);
		case TypeCode.Int64:
			return Convert.ToInt64(json);
		case TypeCode.Decimal:
			return Convert.ToDecimal(json);
		case TypeCode.Double:
			return Convert.ToDouble(json);
		case TypeCode.Single:
			return Convert.ToSingle(json);
		default:
			return json;
		}
	}
}
