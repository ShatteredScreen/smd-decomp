using System;

public class SMGridData
{
	private SMTile[,] mTiles;

	private SMAttributeTile[,] mAttributeTiles;

	private SMTileInfo[,] mTileInfos;

	public int Width
	{
		get
		{
			return mTiles.GetLength(1);
		}
		set
		{
			if (value != Width)
			{
				MakeGridData(value, Height);
			}
		}
	}

	public int Height
	{
		get
		{
			return mTiles.GetLength(0);
		}
		set
		{
			if (value != Height)
			{
				MakeGridData(Width, value);
			}
		}
	}

	public SMGridData()
		: this(0, 0)
	{
	}

	public SMGridData(int w, int h)
	{
		MakeGridData(w, h);
	}

	private void MakeGridData(int w, int h)
	{
		SMTile[,] oldTiles = mTiles;
		SMAttributeTile[,] oldTiles2 = mAttributeTiles;
		SMTileInfo[,] oldTiles3 = mTileInfos;
		try
		{
			mTiles = new SMTile[h, w];
			mAttributeTiles = new SMAttributeTile[h, w];
			mTileInfos = new SMTileInfo[h, w];
			GridResize(ref oldTiles, ref mTiles);
			GridResize(ref oldTiles2, ref mAttributeTiles);
			GridResize(ref oldTiles3, ref mTileInfos);
		}
		catch (Exception ex)
		{
			mTiles = oldTiles;
			mAttributeTiles = oldTiles2;
			mTileInfos = oldTiles3;
			throw ex;
		}
	}

	private static void GridResize<T>(ref T[,] oldTiles, ref T[,] newTiles)
	{
		if (oldTiles == null || newTiles == null)
		{
			return;
		}
		int length = oldTiles.GetLength(1);
		int length2 = oldTiles.GetLength(0);
		int num = length / 2;
		int num2 = length2 / 2;
		int length3 = newTiles.GetLength(1);
		int length4 = newTiles.GetLength(0);
		int num3 = length3 / 2;
		int num4 = length4 / 2;
		int num5 = num3 - num;
		int num6 = num4 - num2;
		int num7 = num5 + length;
		int num8 = num6 + length2;
		for (int i = num6; i < num8; i++)
		{
			for (int j = num5; j < num7; j++)
			{
				if (j >= 0 && j < length3 && i >= 0 && i < length4)
				{
					newTiles[i, j] = oldTiles[i - num6, j - num5];
				}
			}
		}
	}

	public bool IsTileAvailable(int x, int y)
	{
		return x >= 0 && x < Width && y >= 0 && y < Height;
	}

	public SMTile GetTile(int x, int y)
	{
		SMTile result = null;
		if (IsTileAvailable(x, y))
		{
			result = mTiles[y, x];
		}
		return result;
	}

	public void SetTile(int x, int y, SMTile tile)
	{
		if (IsTileAvailable(x, y))
		{
			mTiles[y, x] = tile;
		}
	}

	public SMAttributeTile GetAttributeTile(int x, int y)
	{
		SMAttributeTile result = null;
		if (IsTileAvailable(x, y))
		{
			result = mAttributeTiles[y, x];
		}
		return result;
	}

	public void SetAttributeTile(int x, int y, SMAttributeTile tile)
	{
		if (IsTileAvailable(x, y))
		{
			mAttributeTiles[y, x] = tile;
		}
	}

	public SMTileInfo GetTileInfo(int x, int y)
	{
		SMTileInfo result = null;
		if (IsTileAvailable(x, y))
		{
			result = mTileInfos[y, x];
		}
		return result;
	}

	public void SetTileInfo(int x, int y, SMTileInfo info)
	{
		if (IsTileAvailable(x, y))
		{
			mTileInfos[y, x] = info;
		}
	}
}
