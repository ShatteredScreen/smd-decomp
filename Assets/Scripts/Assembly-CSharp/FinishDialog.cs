using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		RETRY = 0,
		NO = 1,
		FRIENDHELP = 2
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		AUTHERROR_RETURN_TITLE = 2
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private PuzzleManager mManager;

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private bool mFriendHelpflg;

	private BoxCollider mColliderFriend;

	private UIButton mFriendHelpButton;

	private string mImageName = "LC_button_friendHelp";

	private FriendHelpDialog mHelpDialog;

	private BuyHeartDialog mBuyHeartDialog;

	private FriendHelpOKDialog mHelpOKDialog;

	private ConfirmDialog mConfirmDialog;

	public override void Start()
	{
		mManager = GameObject.Find("PuzzleManager").GetComponent<PuzzleManager>();
		base.Start();
	}

	public override IEnumerator BuildResource()
	{
		ResImage image = ResourceManager.LoadImageAsync("PUZZLE_CONTINUE_HUD");
		while (image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (mGame.mDebugRepeatPlayMode)
			{
				mGame.mPlayer.AddLifeCount(1);
				OnRetryPushed(null);
			}
			break;
		case STATE.AUTHERROR_RETURN_TITLE:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("PUZZLE_CONTINUE_HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text;
		if (!mGame.IsDailyChallengePuzzle)
		{
			text = ((!mGame.mEventMode) ? (Localization.Get("LevelInfo_Level") + mGame.mCurrentStage.DisplayStageNumber) : (mGame.mEventName + mGame.mCurrentStage.DisplayStageNumber));
		}
		else
		{
			text = Localization.Get("LevelInfo_Level") + mGame.SelectedDailyChallengeStageNo;
			if (mGame.mDebugDailyChallengeVisibleBaseStage)
			{
				string text2 = text;
				text = string.Concat(text2, "[sup](", mGame.mCurrentStage.Series, mGame.mCurrentStage.DisplayStageNumber, ")[/sup]");
			}
		}
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(text);
		UISprite sprite = Util.CreateSprite("Character", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "chara_miss", mBaseDepth + 4, new Vector3(-160f, 100f, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Finish_Desc"), mBaseDepth + 3, new Vector3(100f, 170f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		UILabel uILabel2 = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel2, Localization.Get("Score"), mBaseDepth + 3, new Vector3(100f, 100f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel2.color = Def.DEFAULT_MESSAGE_COLOR;
		string[] numTblOverride = new string[10] { "num_buy_big0", "num_buy_big1", "num_buy_big2", "num_buy_big3", "num_buy_big4", "num_buy_big5", "num_buy_big6", "num_buy_big7", "num_buy_big8", "num_buy_big9" };
		NumberImageString numberImageString = Util.CreateGameObject("Score", base.gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(7, mManager.mScore, mBaseDepth + 3, 0.5f, UIWidget.Pivot.Center, false, image, false, numTblOverride);
		numberImageString.transform.localPosition = new Vector3(100f, 60f, 0f);
		numberImageString.SetPitch(70f);
		UIButton uIButton = Util.CreateJellyImageButton("ButtonRetry", base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, "LC_button_retry", "LC_button_retry", "LC_button_retry", mBaseDepth + 4, new Vector3(-115f, -224f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnRetryPushed", UIButtonMessage.Trigger.OnClick);
		bool flag = false;
		if (Def.IsEventSeries(mGame.mCurrentStage.Series))
		{
			flag = mGame.IsSeasonEventExpired(mGame.mCurrentStage.EventID);
		}
		if (flag)
		{
			UILabel uILabel3 = Util.CreateLabel("ExpiredText", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel3, Localization.Get("EventExpired_Title"), mBaseDepth + 5, new Vector3(-110f, -191f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel3.color = Color.red;
			uILabel3.effectStyle = UILabel.Effect.Outline8;
			uILabel3.effectColor = Color.white;
			uILabel3.effectDistance = new Vector2(2f, 2f);
			uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		UIButton button = Util.CreateJellyImageButton("ButtonNo", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_map", "LC_button_map", "LC_button_map", mBaseDepth + 4, new Vector3(115f, -224f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnNoPushed", UIButtonMessage.Trigger.OnClick);
		if (mGame.GameProfile.EnableFriendHelp && mGame.mTutorialManager.IsInitialTutorialCompleted())
		{
			mFriendHelpflg = mGame.mPlayer.SendFriendHelpStatus();
			int a_main;
			int a_sub;
			Def.SplitStageNo(mGame.mCurrentStage.StageNumber, out a_main, out a_sub);
			PlayerClearData _psd;
			if (a_sub != 0 || mGame.mCurrentStage.Series == Def.SERIES.SM_EV)
			{
				mFriendHelpflg = false;
			}
			else if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, out _psd))
			{
				mFriendHelpflg = false;
			}
			mImageName = "LC_button_friendHelp";
			if (mFriendHelpflg)
			{
				mFriendHelpButton = Util.CreateJellyImageButton("ButtonHelp", base.gameObject, image);
				Util.SetImageButtonInfo(mFriendHelpButton, mImageName, mImageName, mImageName, mBaseDepth + 4, new Vector3(0f, -223f, 0f), Vector3.one);
				Util.AddImageButtonMessage(mFriendHelpButton, this, "OnFriendHelpPushed", UIButtonMessage.Trigger.OnClick);
				mColliderFriend = mFriendHelpButton.GetComponent<BoxCollider>();
			}
			else
			{
				sprite = Util.CreateSprite("ButtonHelp", base.gameObject, image);
				Util.SetSpriteInfo(sprite, mImageName, mBaseDepth + 4, new Vector3(0f, -223f, 0f), Vector3.one, false);
				sprite.color = Color.gray;
			}
		}
		List<Def.BOOSTER_KIND> list = new List<Def.BOOSTER_KIND>();
		Def.TILE_KIND[] normaKindArray = mGame.mCurrentStage.GetNormaKindArray();
		Def.BOOSTER_KIND index = Def.BOOSTER_KIND.WAVE_BOMB;
		switch (mGame.mCurrentStage.WinType)
		{
		case Def.STAGE_WIN_CONDITION.SCORE:
			list.Add(Def.BOOSTER_KIND.WAVE_BOMB);
			list.Add(Def.BOOSTER_KIND.RAINBOW);
			list.Add(Def.BOOSTER_KIND.ADD_SCORE);
			break;
		case Def.STAGE_WIN_CONDITION.EATEN:
			list.Add(Def.BOOSTER_KIND.RAINBOW);
			list.Add(Def.BOOSTER_KIND.COLOR_CRUSH);
			break;
		case Def.STAGE_WIN_CONDITION.CLEAN:
			list.Add(Def.BOOSTER_KIND.TAP_BOMB2);
			list.Add(Def.BOOSTER_KIND.SELECT_ONE);
			break;
		case Def.STAGE_WIN_CONDITION.COLLECT:
			list.Add(Def.BOOSTER_KIND.SELECT_ONE);
			list.Add(Def.BOOSTER_KIND.SELECT_COLLECT);
			break;
		case Def.STAGE_WIN_CONDITION.DEFEAT:
			list.Add(Def.BOOSTER_KIND.SKILL_CHARGE);
			list.Add(Def.BOOSTER_KIND.COLOR_CRUSH);
			break;
		case Def.STAGE_WIN_CONDITION.ENEMY:
			list.Add(Def.BOOSTER_KIND.TAP_BOMB2);
			list.Add(Def.BOOSTER_KIND.BLOCK_CRUSH);
			break;
		case Def.STAGE_WIN_CONDITION.FIND:
			list.Add(Def.BOOSTER_KIND.RAINBOW);
			list.Add(Def.BOOSTER_KIND.SELECT_ONE);
			break;
		case Def.STAGE_WIN_CONDITION.GROWUP:
			list.Add(Def.BOOSTER_KIND.TAP_BOMB2);
			list.Add(Def.BOOSTER_KIND.BLOCK_CRUSH);
			break;
		case Def.STAGE_WIN_CONDITION.PAIR:
			list.Add(Def.BOOSTER_KIND.SELECT_ONE);
			list.Add(Def.BOOSTER_KIND.PAIR_MAKER);
			break;
		}
		if (list.Count > 0)
		{
			index = list[Random.Range(0, list.Count)];
		}
		sprite = Util.CreateSprite("BoosterInfo", base.gameObject, image);
		Util.SetSpriteInfo(sprite, "instruction_panel15", mBaseDepth + 4, new Vector3(0f, -100f, 0f), Vector3.one, false);
		sprite.type = UIBasicSprite.Type.Sliced;
		sprite.SetDimensions(520, 140);
		GameObject parent = sprite.gameObject;
		sprite = Util.CreateSprite("TitleRibbon", parent, image);
		Util.SetSpriteInfo(sprite, "instruction_accessory10", mBaseDepth + 5, new Vector3(0f, 60f, 0f), Vector3.one, false);
		sprite.type = UIBasicSprite.Type.Sliced;
		sprite.SetDimensions(480, 42);
		sprite = Util.CreateSprite("TitleMessage", parent, image);
		Util.SetSpriteInfo(sprite, "LC_boosterTips_text", mBaseDepth + 6, new Vector3(-138f, 62f, 0f), Vector3.one, false);
		sprite = Util.CreateSprite("Icon", parent, image);
		Util.SetSpriteInfo(sprite, mGame.mBoosterData[(int)index].iconBuy, mBaseDepth + 5, new Vector3(-190f, -4f, 0f), Vector3.one, false);
		sprite.transform.localRotation = Quaternion.Euler(0f, 0f, 20f);
		UILabel uILabel4 = Util.CreateLabel("Name", parent, atlasFont);
		Util.SetLabelInfo(uILabel4, Localization.Get(mGame.mBoosterData[(int)index].name), mBaseDepth + 6, new Vector3(90f, 60f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel4.color = Color.white;
		uILabel4.effectStyle = UILabel.Effect.Shadow;
		uILabel4.effectColor = Color.black;
		uILabel4 = Util.CreateLabel("Desc", parent, atlasFont);
		Util.SetLabelInfo(uILabel4, Localization.Get(mGame.mBoosterData[(int)index].recommendMessage), mBaseDepth + 5, new Vector3(-120f, -10f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		uILabel4.spacingY = 4;
		uILabel4.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel4.SetDimensions(340, 120);
		uILabel4.color = Def.DEFAULT_MESSAGE_COLOR;
		if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
		{
			sprite = Util.CreateSprite("MugenHeartInfo", base.gameObject, image);
			Util.SetSpriteInfo(sprite, "instruction_panel2", mBaseDepth + 4, new Vector3(104f, 40f, 0f), Vector3.one, false);
			sprite.type = UIBasicSprite.Type.Sliced;
			sprite.SetDimensions(300, 85);
			GameObject parent2 = sprite.gameObject;
			sprite = Util.CreateSprite("Icon", parent2, image);
			Util.SetSpriteInfo(sprite, "icon_heartinfinite", mBaseDepth + 5, new Vector3(-74f, -1f, 0f), Vector3.one, false);
			uILabel4 = Util.CreateLabel("Desc", parent2, atlasFont);
			Util.SetLabelInfo(uILabel4, Localization.Get("LimitTimer_Title"), mBaseDepth + 5, new Vector3(45f, 25f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel4.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel4.fontSize = 20;
			UILabel uILabel5 = Util.CreateLabel("Time", parent2, atlasFont);
			Util.SetLabelInfo(uILabel5, "0:00", mBaseDepth + 5, new Vector3(45f, -10f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
			uILabel5.color = Color.red;
			uILabel5.fontSize = 45;
			StartCoroutine(UpdateMugenHeartFinishInfo(uILabel5));
			uILabel.transform.localPosition = new Vector3(100f, 190f, 0f);
			uILabel2.transform.localPosition = new Vector3(100f, 150f, 0f);
			numberImageString.transform.localPosition = new Vector3(100f, 112f, 0f);
		}
	}

	protected virtual IEnumerator UpdateMugenHeartFinishInfo(UILabel Time)
	{
		while (true)
		{
			if (mGame.mPlayer.Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
			{
				long totalSec = mGame.GetMugenHeartEndSec();
				if (0 < totalSec)
				{
					long min = totalSec / 60;
					Util.SetLabelText(Time, string.Format(arg1: totalSec % 60, format: "{0:0}:{1:00}", arg0: min));
				}
				else
				{
					Util.SetLabelText(Time, "0:00");
				}
			}
			else
			{
				Util.SetLabelText(Time, "0:00");
			}
			yield return null;
		}
	}

	public void OnFriendHelpPushed(GameObject go)
	{
		if (mHelpDialog == null && mBuyHeartDialog == null)
		{
			if (mGame.mPlayer.Friends.Count > 0)
			{
				mHelpDialog = Util.CreateGameObject("FriendhelpDialog", base.transform.parent.gameObject).AddComponent<FriendHelpDialog>();
				mHelpDialog.mStars = mManager.CheckGotStars();
				mHelpDialog.SetClosedCallback(OnFriendHelpDialogClosed);
			}
			else
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("FriendHelp_Error00_Desc"));
				mConfirmDialog.Init(Localization.Get("FriendHelp_Error00_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnConfirmClosed);
				mConfirmDialog.SetBaseDepth(70);
			}
			mGame.PlaySe("SE_POSITIVE", -1);
		}
	}

	private void OnConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
	}

	public void OnFriendHelpDialogClosed(FriendHelpDialog.SELECT_ITEM i)
	{
		switch (i)
		{
		case FriendHelpDialog.SELECT_ITEM.SUCCESS:
			mHelpOKDialog = Util.CreateGameObject("FriendhelpDialog", base.transform.parent.gameObject).AddComponent<FriendHelpOKDialog>();
			mHelpOKDialog.SetClosedCallback(OnFriendHelpOKDialogClosed);
			break;
		case FriendHelpDialog.SELECT_ITEM.ERROR:
		{
			mConfirmDialog = Util.CreateGameObject("FriendHelpNGDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
			mConfirmDialog.Init(Localization.Get("FriendHelp_Error00_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnFriendHelpErrorDialogClosed);
			break;
		}
		}
		Object.Destroy(mHelpDialog.gameObject);
		mHelpDialog = null;
	}

	public void OnFriendHelpOKDialogClosed(FriendHelpOKDialog.SELECT_ITEM i)
	{
		Util.SetImageButtonGraphic(mFriendHelpButton, mImageName, mImageName, mImageName);
		Util.SetImageButtonColor(mFriendHelpButton, Color.gray);
		if (mColliderFriend != null)
		{
			mColliderFriend.size = Vector3.zero;
		}
		Object.Destroy(mHelpOKDialog.gameObject);
	}

	private void OnFriendHelpErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		if (mFriendHelpflg && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.FRIEND_HELP_0))
		{
			mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.FRIEND_HELP_0, mGame.mAnchor.gameObject, delegate
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.FRIEND_HELP_0);
			}, delegate
			{
				mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.FRIEND_HELP_0);
			});
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, mFriendHelpButton.gameObject);
		}
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		ResourceManager.UnloadImage("PUZZLE_CONTINUE_HUD");
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnNoPushed(null);
	}

	public void OnRetryPushed(GameObject go)
	{
		if (!GetBusy() && mHelpDialog == null && go.GetComponent<JellyImageButton>().IsEnable())
		{
			if (mGame.mPlayer.LifeCount < 1)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.HEART_STAGE;
				mBuyHeartDialog = Util.CreateGameObject("HeartDialog", base.transform.parent.gameObject).AddComponent<BuyHeartDialog>();
				mBuyHeartDialog.SetBaseDepth(65);
				mBuyHeartDialog.Place = 1;
				mBuyHeartDialog.SetClosedCallback(OnHeartDialogClosed);
				mBuyHeartDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.PlaySe("SE_POSITIVE", -1);
				mGame.PlaySe("VOICE_024", 2);
				mSelectItem = SELECT_ITEM.RETRY;
				Close();
			}
		}
	}

	public void OnNoPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !GetBusy() && mHelpDialog == null)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mGame.PlaySe("VOICE_027", 2);
			mSelectItem = SELECT_ITEM.NO;
			Close();
		}
	}

	public void OnHeartDialogClosed(BuyHeartDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
		mBuyHeartDialog = null;
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void OnDialogAuthErrorClosed()
	{
		mState.Change(STATE.AUTHERROR_RETURN_TITLE);
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}
}
