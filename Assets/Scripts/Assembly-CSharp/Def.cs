using System;
using System.Collections.Generic;
using UnityEngine;

public static class Def
{
	public enum AdvDungeonDifficulty
	{
		LEVEL_1 = 0,
		LEVEL_2 = 1,
		LEVEL_3 = 2,
		LEVEL_4 = 3,
		LEVEL_5 = 4
	}

	public enum AdvItemId
	{
		EXP_POINT = 1,
		SKILL_POINT = 2,
		NORMAL_TICKET = 3,
		PREMIUM_TICKET = 4,
		SUB_PREMIUM_TICKET = 5,
		BUTTON_DISABLE = 9999
	}

	public enum RARE
	{
		N = 1,
		NR = 2,
		NR_P = 3,
		R = 4,
		R_P = 5,
		SR = 6,
		SR_P = 7,
		SSR = 8
	}

	public enum AdvEventType
	{
		NORMAL = 0,
		FESTIVAL = 1,
		TOWER = 2,
		POINT_GET = 3
	}

	public enum SERIES
	{
		SM_FIRST = 0,
		SM_R = 1,
		SM_S = 2,
		SM_SS = 3,
		SM_STARS = 4,
		SM_EV = 5,
		SM_GF00 = 10,
		SM_ADV = 100,
		SM_MESSAGECARD = 101,
		SM_DCEV = 200,
		SM_ALL = 999
	}

	public enum EVENT_TYPE
	{
		SM_RALLY = 0,
		SM_STAR = 1,
		SM_RALLY2 = 2,
		SM_BINGO = 3,
		SM_ADV = 4,
		SM_LABYRINTH = 5,
		SM_NONE = 1000
	}

	public enum DIR
	{
		NONE = -1,
		UP = 0,
		UP_RIGHT = 1,
		RIGHT = 2,
		DOWN_RIGHT = 3,
		DOWN = 4,
		DOWN_LEFT = 5,
		LEFT = 6,
		UP_LEFT = 7,
		DIR_NUM = 8,
		NONE_LEFT = 9
	}

	public enum STAGE_WIN_CONDITION
	{
		NONE = -1,
		SCORE = 0,
		COLLECT = 1,
		EATEN = 2,
		CLEAN = 3,
		ALL_CLEAN = 4,
		SPECIAL_EATEN = 5,
		PAIR = 6,
		TIMER = 7,
		FIND = 8,
		DEFEAT = 9,
		ENEMY = 10,
		GROWUP = 11,
		GOAL = 12,
		TALISMAN = 13,
		STARGET = 14
	}

	public enum STAGE_LOSE_CONDITION
	{
		NONE = -1,
		MOVES = 0,
		TIME = 1,
		NO_MATCH = 2,
		TIMER_TIMEOUT = 3
	}

	public enum STAGE_LOSE_REASON
	{
		NONE = -1,
		CANCEL = 0,
		MOVES = 1,
		TIME = 2,
		NO_MORE_MOVES = 3,
		TIMER_TIMEOUT = 4,
		ADV_LOSE = 5
	}

	public enum STAGE_TYPE
	{
		[StageCondition(STAGE_WIN_CONDITION.NONE, STAGE_LOSE_CONDITION.NONE)]
		NONE = -1,
		[StageCondition(STAGE_WIN_CONDITION.SCORE, STAGE_LOSE_CONDITION.MOVES)]
		SCORE_ATTACK = 0,
		[StageCondition(STAGE_WIN_CONDITION.SCORE, STAGE_LOSE_CONDITION.TIME)]
		TIME_ATTACK = 1,
		[StageCondition(STAGE_WIN_CONDITION.COLLECT, STAGE_LOSE_CONDITION.MOVES)]
		COLLECT_CHALLENGE = 2,
		[StageCondition(STAGE_WIN_CONDITION.EATEN, STAGE_LOSE_CONDITION.MOVES)]
		EAT_CANDIES = 3,
		[StageCondition(STAGE_WIN_CONDITION.CLEAN, STAGE_LOSE_CONDITION.MOVES)]
		DISCARD_PAPERS = 4,
		[StageCondition(STAGE_WIN_CONDITION.ALL_CLEAN, STAGE_LOSE_CONDITION.NO_MATCH)]
		EMPTY_SACK = 5,
		[StageCondition(STAGE_WIN_CONDITION.SPECIAL_EATEN, STAGE_LOSE_CONDITION.MOVES)]
		EAT_SPECIALCANDIES = 6,
		[StageCondition(STAGE_WIN_CONDITION.PAIR, STAGE_LOSE_CONDITION.MOVES)]
		COLLECT_PAIR = 7,
		[StageCondition(STAGE_WIN_CONDITION.TIMER, STAGE_LOSE_CONDITION.MOVES)]
		STOP_TIMEALERT = 8
	}

	public enum TILE_FORM
	{
		NONE = -1,
		NORMAL = 0,
		WAVE_V = 1,
		WAVE_H = 2,
		BOMB = 3,
		BOMB_B = 4,
		RAINBOW = 5,
		ADDTIME = 6,
		TIMER_BOMB = 7,
		JEWEL = 8,
		PAINT = 9,
		TAP_BOMB = 10,
		ADDPLUS = 11,
		ADDHP = 12,
		FOOT_STAMP = 13,
		JEWEL3 = 14,
		JEWEL5 = 15,
		JEWEL9 = 16,
		PEARL = 17,
		SKILL = 18,
		MAXNUM = 19
	}

	public enum TILE_KIND
	{
		NONE = -1,
		COLOR0 = 0,
		COLOR1 = 1,
		COLOR2 = 2,
		COLOR3 = 3,
		COLOR4 = 4,
		COLOR5 = 5,
		COLOR6 = 6,
		SPECIAL = 7,
		INGREDIENT0 = 8,
		INGREDIENT1 = 9,
		INGREDIENT2 = 10,
		INGREDIENT3 = 11,
		NORMAL = 12,
		EDGE = 13,
		THROUGH = 14,
		WALL = 15,
		SPAWN = 16,
		EXIT = 17,
		THROUGH_WALL0 = 18,
		THROUGH_WALL1 = 19,
		THROUGH_WALL2 = 20,
		BROKEN_WALL0 = 21,
		BROKEN_WALL1 = 22,
		BROKEN_WALL2 = 23,
		MOVABLE_WALL0 = 24,
		MOVABLE_WALL1 = 25,
		MOVABLE_WALL2 = 26,
		CAPTURE0 = 27,
		CAPTURE1 = 28,
		CAPTURE2 = 29,
		PRESENTBOX0 = 30,
		PRESENTBOX1 = 31,
		PRESENTBOX2 = 32,
		PRESENTBOX3 = 33,
		INCREASE = 34,
		GROWN_UP0 = 35,
		GROWN_UP1 = 36,
		GROWN_UP2 = 37,
		PAIR0_PARTS = 38,
		PAIR0_COMPLETE = 39,
		PAIR1_PARTS = 40,
		PAIR1_COMPLETE = 41,
		KEY0 = 42,
		KEY1 = 43,
		KEY2 = 44,
		KEY3 = 45,
		BOSS0 = 46,
		FIND0 = 47,
		FIND1 = 48,
		FIND2 = 49,
		FIND3 = 50,
		WARP_IN = 51,
		WARP_OUT = 52,
		SKILL_BALL = 53,
		PRESENTBOX4 = 54,
		PRESENTBOX5 = 55,
		PRESENTBOX6 = 56,
		PRESENTBOX7 = 57,
		PRESENTBOX8 = 58,
		PRESENTBOX9 = 59,
		PRESENTBOX10 = 60,
		PRESENTBOX11 = 61,
		PRESENTBOX12 = 62,
		PRESENTBOX13 = 63,
		PRESENTBOX14 = 64,
		PRESENTBOX15 = 65,
		PRESENTBOX16 = 66,
		PRESENTBOX17 = 67,
		PRESENTBOX18 = 68,
		PRESENTBOX19 = 69,
		PRESENTBOX20 = 70,
		PRESENTBOX21 = 71,
		PRESENTBOX22 = 72,
		PRESENTBOX23 = 73,
		KEY4 = 74,
		KEY5 = 75,
		KEY6 = 76,
		KEY7 = 77,
		ORB_WAVE_V0 = 78,
		ORB_WAVE_V1 = 79,
		ORB_WAVE_V2 = 80,
		ORB_WAVE_H0 = 81,
		ORB_WAVE_H1 = 82,
		ORB_WAVE_H2 = 83,
		ORB_BOMB0 = 84,
		ORB_BOMB1 = 85,
		ORB_BOMB2 = 86,
		ORB_BOSSDAMAGE0 = 87,
		ORB_BOSSDAMAGE1 = 88,
		ORB_BOSSDAMAGE2 = 89,
		DIANA = 90,
		DIANA_GOAL_L = 91,
		DIANA_GOAL_R = 92,
		DIANA_ROUTE = 93,
		FIXED_FLOWER0 = 94,
		FIXED_FLOWER1 = 95,
		MOVABLE_FLOWER0 = 96,
		MOVABLE_FLOWER1 = 97,
		TALISMAN0_0 = 98,
		TALISMAN1_0 = 99,
		TALISMAN2_0 = 100,
		TALISMAN0_1 = 101,
		TALISMAN1_1 = 102,
		TALISMAN2_1 = 103,
		TALISMAN0_2 = 104,
		TALISMAN1_2 = 105,
		TALISMAN2_2 = 106,
		TALISMAN0_3 = 107,
		TALISMAN1_3 = 108,
		TALISMAN2_3 = 109,
		STAR = 110,
		KEY8 = 111,
		MAXNUM = 112,
		WAVE = 1000,
		BOMB = 1001,
		RAINBOW = 1002,
		WAVE_WAVE = 1003,
		BOMB_BOMB = 1004,
		RAINBOW_RAINBOW = 1005,
		WAVE_BOMB = 1006,
		BOMB_RAINBOW = 1007,
		WAVE_RAINBOW = 1008,
		PAINT = 1009,
		BOMB_PAINT = 1010,
		WAVE_PAINT = 1011,
		PAINT_RAINBOW = 1012,
		PAINT_PAINT = 1013,
		SPECIAL_MAXNUM = 1014
	}

	public enum DIFFICULTY_MODE
	{
		NORMAL = 0,
		COLOR_MODIFY_POSI = 1,
		COLOR_MODIFY_NEGA = 2,
		INITIAL_MATCH_H = 3,
		INITIAL_SPLIT_H = 4,
		INITIAL_MATCH_V = 5,
		INITIAL_SPLIT_V = 6,
		FILL_MATCH = 7,
		FILL_SPLIT = 8,
		MAXNUM = 9
	}

	public enum EFFECT_OVERRIDE
	{
		NORMAL = 0,
		DISABLE = 1,
		WAVE_WAVE = 2,
		WAVE_BOMB = 3,
		WAVE_RAINBOW = 4,
		BOMB_BOMB = 5,
		BOMB_RAINBOW = 6,
		RAINBOW_RAINBOW = 7,
		BOMB_PAINT = 8,
		WAVE_PAINT = 9,
		PAINT_RAINBOW = 10,
		PAINT_PAINT = 11
	}

	public enum BOSS_ATTACK
	{
		NONE = 0,
		NORMAL = 1,
		CHANGE_COLORS = 2,
		SPAWN_PLATE = 3,
		SPAWN_FAMILIAR = 4
	}

	public enum ITEMGET_TYPE
	{
		NONE = 0,
		FREE = 1,
		PAID = 2
	}

	public enum ITEM_CATEGORY
	{
		NONE = 0,
		CONSUME = 1,
		BOOSTER = 2,
		FREE_BOOSTER = 3,
		ACCESSORY = 4,
		COMPANION = 5,
		EVENT = 6,
		WALLPAPER = 7,
		WALLPAPER_PIECE = 8,
		STORY_DEMO = 9,
		SKIN = 10,
		TUTORIAL = 11,
		STAGE_OPEN = 12,
		STAGE_OPEN_BUNDLE = 13,
		ROADBLOCK_REQ = 14,
		ROADBLOCK_RES = 15,
		FRIEND_REQ = 16,
		FRIEND_RES = 17,
		STAGEHELP_REQ = 18,
		STAGEHELP_RES = 19,
		PRESENT_HEART = 20,
		PRESENT_BOOSTER = 21,
		PRESENT_GOODJOB = 22,
		RELEASE_TIMECHEAT = 23,
		ADV_FIGURE = 24,
		ADV_CONSUME = 25,
		ADV_COIN = 26,
		ADV_STAGE_CLEAR = 27,
		ADV_STAGE_BUNDLE_CLEAR = 28,
		ADV_DUNGEON_CLEAR = 29,
		ADV_DUNGEON_BUNDLE_CLEAR = 30,
		ADV_FRIEND_MEDAL = 31,
		BINGO_TICKET = 32,
		LABYRINTH_JEWEL = 33,
		MESSAGE_CARD = 34
	}

	public enum CONSUME_ID
	{
		NONE = 0,
		HEART = 1,
		HEART_EX = 2,
		MAIN_CURRENCY = 3,
		MAIN_CURRENCY_EX = 4,
		SUB_CURRENCY = 5,
		SUB_CURRENCY_EX = 6,
		ROADBLOCK_TICKET = 7,
		ROADBLOCK_KEY = 8,
		HEART_PIECE = 9,
		GROWUP = 10,
		GROWUP_PIECE = 11,
		GOODJOB = 12,
		TOTAL_GOODJOB = 13,
		TROPHY = 14,
		TOTAL_TROPHY = 15,
		ROADBLOCK_KEY_R = 16,
		ROADBLOCK_KEY_S = 17,
		ROADBLOCK_KEY_SS = 18,
		ROADBLOCK_KEY_STARS = 19,
		OLDEVENT_KEY = 20,
		ROADBLOCK_KEY_GF00 = 21,
		MAX = 22
	}

	public enum ADV_CONSUME_ID
	{
		NONE = 0,
		POWER_P = 1,
		SKILL_LEVELUP = 2,
		GASYA_TICKET_NORMAL = 3,
		GASYA_TICKET_PREMIUM = 4,
		GASYA_TICKET_PREMIUM_SUB = 5
	}

	public enum ITEM_ADD_KIND
	{
		NONE = 0,
		ADD = 1,
		SET = 2
	}

	public enum BOOSTER_KIND
	{
		NONE = 0,
		WAVE_BOMB = 1,
		TAP_BOMB2 = 2,
		RAINBOW = 3,
		ADD_SCORE = 4,
		SKILL_CHARGE = 5,
		SELECT_ONE = 6,
		SELECT_COLLECT = 7,
		COLOR_CRUSH = 8,
		BLOCK_CRUSH = 9,
		MUGEN_HEART15 = 10,
		MUGEN_HEART30 = 11,
		MUGEN_HEART60 = 12,
		PAIR_MAKER = 13,
		ALL_CRUSH = 14,
		MAX = 15
	}

	public enum UNLOCK_TYPE
	{
		NONE = 0,
		STAR = 1,
		BOX = 2,
		INIT = 3,
		SCORE = 4
	}

	public enum STAGE_ENTER
	{
		NONE = 0,
		CHARA = 1
	}

	public enum PRESS
	{
		NONE = 0,
		GUESS = 1,
		HIT = 2
	}

	public enum DIALOG_PRODUCT
	{
		NONE = 0,
		EVENT_DIALOG = 1,
		TROPHY_DIALOG = 2,
		LOSE_DIALOG = 3
	}

	public enum TUTORIAL_INDEX
	{
		NONE = -1,
		MAP_LEVEL1 = 0,
		LEVEL1_0 = 1,
		LEVEL1_1 = 2,
		LEVEL1_2 = 3,
		LEVEL1_3 = 4,
		LEVEL1_4 = 5,
		LEVEL1_5 = 6,
		LEVEL1_6 = 7,
		LEVEL1_7 = 8,
		LEVEL1_WIN1 = 9,
		LEVEL1_WIN2 = 10,
		LEVEL1_WIN3 = 11,
		LEVEL1_WIN4 = 12,
		MAP_LEVEL2_1 = 13,
		MAP_LEVEL2_2 = 14,
		LEVEL2_0 = 15,
		LEVEL2_1 = 16,
		LEVEL2_2 = 17,
		LEVEL2_3 = 18,
		LEVEL2_4 = 19,
		LEVEL2_5 = 20,
		MAP_LEVEL3_1 = 21,
		MAP_LEVEL3_2 = 22,
		LEVEL3_0 = 23,
		LEVEL3_1 = 24,
		LEVEL3_2 = 25,
		LEVEL3_3 = 26,
		MAP_LEVEL4_0 = 27,
		MAP_LEVEL4_1 = 28,
		MAP_LEVEL4_2 = 29,
		LEVEL4_0 = 30,
		LEVEL4_1 = 31,
		LEVEL4_2 = 32,
		LEVEL4_3 = 33,
		LEVEL4_4 = 34,
		LEVEL5_0 = 35,
		LEVEL5_1 = 36,
		LEVEL5_2 = 37,
		LEVEL5_3 = 38,
		LEVEL5_4 = 39,
		LEVEL6_0 = 40,
		LEVEL6_1 = 41,
		LEVEL6_2 = 42,
		LEVEL7_0 = 43,
		LEVEL7_1 = 44,
		LEVEL7_2 = 45,
		LEVEL7_3 = 46,
		MAP_LEVEL8_0 = 47,
		MAP_LEVEL8_1 = 48,
		MAP_LEVEL8_2 = 49,
		MAP_LEVEL8_3 = 50,
		LEVEL8_0 = 51,
		LEVEL8_1 = 52,
		LEVEL8_2 = 53,
		LEVEL8_3 = 54,
		MAP_LEVEL9_0 = 55,
		MAP_LEVEL9_1 = 56,
		MAP_LEVEL9_1_2 = 57,
		MAP_LEVEL9_2 = 58,
		MAP_LEVEL9_3 = 59,
		MAP_LEVEL9_4 = 60,
		MAP_LEVEL9_5 = 61,
		MAP_LEVEL9_6 = 62,
		MAP_LEVEL9_7 = 63,
		LEVEL9_0 = 64,
		LEVEL9_1 = 65,
		LEVEL9_2 = 66,
		LEVEL9_3 = 67,
		LEVEL9_4 = 68,
		LEVEL9_5 = 69,
		LEVEL9_6 = 70,
		LEVEL9_7 = 71,
		MAP_LEVEL10_0 = 72,
		MAP_LEVEL10_1 = 73,
		MAP_LEVEL10_2 = 74,
		MAP_LEVEL10_3 = 75,
		LEVEL10_0 = 76,
		LEVEL10_1 = 77,
		LEVEL10_2 = 78,
		LEVEL10_3 = 79,
		LEVEL10_4 = 80,
		LEVEL11_0 = 81,
		LEVEL11_1 = 82,
		LEVEL11_2 = 83,
		MAP_LEVEL11_0 = 84,
		MAP_LEVEL11_1 = 85,
		MAP_LEVEL11_4 = 86,
		EPILOGUE = 87,
		MAP_LEVEL12_0 = 88,
		MAP_LEVEL12_1 = 89,
		LEVEL13_0 = 90,
		LEVEL13_1 = 91,
		MAP_LEVEL13_0 = 92,
		MAP_LEVEL13_1 = 93,
		MAP_LEVEL14_0 = 94,
		MAP_LEVEL14_1 = 95,
		MAP_LEVEL14_2 = 96,
		LEVEL15_0 = 97,
		LEVEL15_1 = 98,
		MAP_LEVEL16_0 = 99,
		MAP_LEVEL16_1 = 100,
		LEVEL16_0 = 101,
		LEVEL16_1 = 102,
		MAP_LEVEL17_0 = 103,
		MAP_LEVEL17_1 = 104,
		MAP_LEVEL17_2 = 105,
		LEVEL18_0 = 106,
		LEVEL18_1 = 107,
		LEVEL21_0 = 108,
		LEVEL21_1 = 109,
		LEVEL24_0 = 110,
		LEVEL26_0 = 111,
		MAP_LEVEL31_0 = 112,
		MAP_LEVEL31_1 = 113,
		MAP_LEVEL31_2 = 114,
		LEVEL31_0 = 115,
		LEVEL31_1 = 116,
		LEVEL31_2 = 117,
		LEVEL48_0 = 118,
		LEVEL48_1 = 119,
		LEVEL61_0 = 120,
		LEVEL61_1 = 121,
		LEVEL62_0 = 122,
		LEVEL62_1 = 123,
		LEVEL62_2 = 124,
		LEVEL62_3 = 125,
		LEVEL76_0 = 126,
		LEVEL76_1 = 127,
		SELECT_ONE_0 = 128,
		SELECT_ONE_1 = 129,
		SELECT_ONE_2 = 130,
		SELECT_ONE_3 = 131,
		SELECT_COLLECT_0 = 132,
		SELECT_COLLECT_1 = 133,
		SELECT_COLLECT_2 = 134,
		SELECT_COLLECT_3 = 135,
		COLOR_CRUSH_0 = 136,
		COLOR_CRUSH_1 = 137,
		COLOR_CRUSH_2 = 138,
		COLOR_CRUSH_3 = 139,
		FRIEND_HELP_0 = 140,
		MAP_LEVEL_NAMECHANGE0 = 141,
		MAP_LEVEL_NAMECHANGE1 = 142,
		MAP_LEVEL_NAMECHANGE2 = 143,
		MAP_LEVEL_TUT_EVENT_0 = 144,
		MAP_LEVEL_TUT_EVENT_1 = 145,
		MAP_LEVEL_TUT_EVENT_2 = 146,
		LEVEL106_0 = 147,
		LEVEL106_1 = 148,
		LEVEL109_0 = 149,
		MAP_LEVEL_TUT_SHOP_0 = 150,
		MAP_LEVEL_TUT_SHOP_1 = 151,
		LEVEL136_0 = 152,
		LEVEL167_0 = 153,
		LEVEL167_1 = 154,
		LEVEL167_2 = 155,
		LEVEL167_3 = 156,
		R_SEASON_NOTICE = 157,
		RATING_F_MARS = 158,
		RATING_2 = 159,
		LEVEL197_0 = 160,
		STAGE_SKIP_0 = 161,
		STAGE_SKIP_1 = 162,
		STAGE_SKIP_2 = 163,
		STAGE_SKIP_3 = 164,
		TROPHY_GUIDE_LOSE = 165,
		TROPHY_GUIDE_WIN = 166,
		TROPHY_TUT_0 = 167,
		TROPHY_TUT_1 = 168,
		TROPHY_TUT_2 = 169,
		TROPHY_TUT_3 = 170,
		MAP_SEASON_SELECT_0 = 171,
		MAP_SEASON_SELECT_1 = 172,
		MAP_SERIES_NOTICE_R = 173,
		MAP_SERIES_NOTICE_S = 174,
		MAP_SERIES_NOTICE_SS = 175,
		MAP_SERIES_NOTICE_STARS = 176,
		LEVEL32R_0 = 177,
		LEVEL32R_1 = 178,
		LEVEL32R_2 = 179,
		LEVEL32R_3 = 180,
		MAP_LEVEL_TUT_INFINITY_0 = 181,
		MAP_LEVEL_TUT_INFINITY_1 = 182,
		LEVEL62R_0 = 183,
		LEVEL62R_1 = 184,
		LEVEL62R_2 = 185,
		LEVEL62R_3 = 186,
		LEVEL62R_4 = 187,
		LEVEL1S_0 = 188,
		LEVEL1S_1 = 189,
		MAP_LEVEL22_0 = 190,
		MAP_LEVEL22_1 = 191,
		MAP_LEVEL22_2 = 192,
		ADV_INITIAL_HOME_1_0 = 193,
		ADV_INITIAL_HOME_1_1 = 194,
		ADV_INITIAL_HOME_1_2 = 195,
		ADV_INITIAL_HOME_1_3 = 196,
		ADV_INITIAL_HOME_1_4 = 197,
		ADV_INITIAL_HOME_1_5 = 198,
		ADV_INITIAL_PUZZLE_1_0 = 199,
		ADV_INITIAL_PUZZLE_1_1 = 200,
		ADV_INITIAL_PUZZLE_1_2 = 201,
		ADV_INITIAL_PUZZLE_1_3 = 202,
		ADV_INITIAL_PUZZLE_1_4 = 203,
		ADV_INITIAL_PUZZLE_1_5 = 204,
		ADV_INITIAL_PUZZLE_1_6 = 205,
		ADV_INITIAL_PUZZLE_1_7 = 206,
		ADV_INITIAL_PUZZLE_1_8 = 207,
		ADV_INITIAL_PUZZLE_1_9 = 208,
		ADV_INITIAL_PUZZLE_2_0 = 209,
		ADV_INITIAL_PUZZLE_2_1 = 210,
		ADV_INITIAL_PUZZLE_2_2 = 211,
		ADV_INITIAL_PUZZLE_2_3 = 212,
		ADV_INITIAL_PUZZLE_3_0 = 213,
		ADV_INITIAL_HOME_2_0 = 214,
		ADV_INITIAL_HOME_2_1 = 215,
		ADV_INITIAL_COMPLETE = 216,
		ADV_PLAY_1_0 = 217,
		ADV_PLAY_1_1 = 218,
		ADV_PLAY_1_2 = 219,
		ADV_PLAY_1_3 = 220,
		ADV_PUZZLE_ATTRIBUTE_1_0 = 221,
		ADV_PUZZLE_ATTRIBUTE_1_1 = 222,
		ADV_PUZZLE_ATTRIBUTE_1_2 = 223,
		ADV_PUZZLE_ATTRIBUTE_1_3 = 224,
		ADV_PUZZLE_ATTRIBUTE_1_4 = 225,
		ADV_PUZZLE_FEVER_1_0 = 226,
		ADV_PUZZLE_FEVER_1_1 = 227,
		ADV_PUZZLE_FEVER_1_2 = 228,
		ADV_PUZZLE_FEVER_1_3 = 229,
		ADV_PUZZLE_FEVER_1_4 = 230,
		ADV_GASHA_1_0 = 231,
		ADV_GASHA_1_1 = 232,
		ADV_GASHA_2_0 = 233,
		ADV_GASHA_2_1 = 234,
		ADV_GASHA_2_2 = 235,
		ADV_GASHA_2_3 = 236,
		ADV_GASHA_3_0 = 237,
		ADV_GASHA_3_1 = 238,
		ADV_GASHA_3_2 = 239,
		ADV_GASHA_3_3 = 240,
		ADV_GASHA_COMPLETE = 241,
		ADV_TEAM_1_0 = 242,
		ADV_TEAM_1_1 = 243,
		ADV_TEAM_1_2 = 244,
		ADV_TEAM_1_3 = 245,
		ADV_TEAM_1_4 = 246,
		ADV_TEAM_DETAIL_1_0 = 247,
		ADV_TEAM_DETAIL_1_1 = 248,
		ADV_TEAM_DETAIL_1_2 = 249,
		ADV_TEAM_DETAIL_1_3 = 250,
		ADV_TEAM_DETAIL_1_4 = 251,
		ADV_TEAM_DETAIL_1_5 = 252,
		ADV_TEAM_DETAIL_1_6 = 253,
		ADV_TEAM_DETAIL_1_7 = 254,
		ADV_TEAM_DETAIL_1_8 = 255,
		ADV_TEAM_DETAIL_1_9 = 256,
		ADV_TEAM_DETAIL_1_10 = 257,
		ADV_TEAM_DETAIL_COMPLETE = 258,
		LEVEL76S_0 = 259,
		LEVEL76S_1 = 260,
		LEVEL76S_2 = 261,
		LEVEL76S_3 = 262,
		LEVEL76S_4 = 263,
		LEVEL76S_5 = 264,
		ADV_PUZZLE_HEAL_1_0 = 265,
		ADV_PUZZLE_HEAL_1_1 = 266,
		ADV_PUZZLE_HEAL_1_2 = 267,
		MAP_LEVEL_TUT_ADVEVENT_0 = 268,
		MAP_LEVEL_TUT_ADVEVENT_1 = 269,
		MAP_LEVEL_TUT_ADVEVENT_2 = 270,
		LEVEL137S_0 = 271,
		LEVEL137S_1 = 272,
		ADV_BONUS_ENEMY_COMPLETE = 273,
		STARTUP_7000 = 274,
		STARTUP_8000 = 275,
		STARTUP_8010 = 276,
		STARTUP_8020 = 277,
		STARTUP_8030 = 278,
		STARTUP_8040 = 279,
		STARTUP_8050 = 280,
		STARTUP_9000 = 281,
		STARTUP_9010 = 282,
		STARTUP_9020 = 283,
		MAP_TUT_BONUS_0_0 = 284,
		MAP_TUT_BONUS_0_1 = 285,
		MAP_TUT_BONUS_0_2 = 286,
		MAP_TUT_BONUS_1_0 = 287,
		MAP_TUT_BONUS_1_1 = 288,
		MAP_TUT_BONUS_1_2 = 289,
		MAP_TUT_BONUS_1_3 = 290,
		LEVEL7SS_0 = 291,
		LEVEL7SS_1 = 292,
		LEVEL7SS_2 = 293,
		LEVEL7SS_3 = 294,
		LEVEL23SS_0 = 295,
		LEVEL23SS_1 = 296,
		LEVEL47SS_0 = 297,
		OLDEVENT_0 = 298,
		OLDEVENT_1 = 299,
		OLDEVENT_2 = 300,
		LEVEL107SS_0 = 301,
		LEVEL107SS_1 = 302,
		LEVEL107SS_2 = 303,
		LEVEL107SS_3 = 304,
		LEVEL47StarS_0 = 305,
		LEVEL47StarS_1 = 306,
		LEVEL47StarS_2 = 307,
		LEVEL47StarS_3 = 308,
		LEVEL47StarS_4 = 309,
		JEWEL_0 = 310,
		JEWEL_1 = 311,
		JEWELCHANCE_0 = 312,
		JEWELCHANCE_1 = 313,
		JEWELCHANCE_2 = 314,
		LEVEL107StarS_0 = 315,
		LEVEL107StarS_1 = 316,
		STARTUP_9001 = 317,
		STARTUP_9002 = 318,
		STARTUP_9003 = 319,
		STARTUP_9011 = 320,
		STARTUP_9012 = 321,
		STARTUP_9021 = 322,
		STARTUP_9022 = 323,
		STARTUP_9023 = 324,
		TALISMAN_0 = 325,
		TALISMAN_1 = 326,
		TALISMAN_2 = 327,
		TALISMAN_3 = 328,
		TALISMAN_4 = 329,
		TALISMAN_5 = 330,
		MAP_SERIES_NOTICE_GF00 = 331,
		MAP_TUT_BONUS_2_0 = 332,
		MAP_TUT_BONUS_2_1 = 333,
		MAP_TUT_BONUS_2_2 = 334
	}

	public enum MUGEN_HEART_STATUS
	{
		NONE = 0,
		START_WAIT = 1,
		TIME_OVER_PUZZLE = 2,
		START = 3,
		LAST_CHECK = 4,
		LAST_CHECK_WAIT = 5,
		LAST_CHECK_END = 6,
		TIME_OVER = 7,
		END_WAIT = 8
	}

	public enum SKILL_TYPE : byte
	{
		DEFAULT = 0,
		SELECT_TARGET = 1
	}

	public class SkillFuncData
	{
		public SKILL_TYPE skillType;

		public string coroutineName;

		public List<ByteVector2> area;

		public int param0;

		public int param1;

		public int param2;

		public int param3;

		public byte addMoves;

		public SkillFuncData(SKILL_TYPE _skillType, string _coroutine, List<ByteVector2> _area, int _param0, int _param1, int _param2, int _param3, byte _addMoves)
		{
			skillType = _skillType;
			coroutineName = _coroutine;
			area = _area;
			param0 = _param0;
			param1 = _param1;
			param2 = _param2;
			param3 = _param3;
			addMoves = _addMoves;
		}

		~SkillFuncData()
		{
			coroutineName = null;
			area = null;
		}
	}

	public static class Purchase
	{
		public static string LICENSE_KEY
		{
			get
			{
				if (!GameMain.USE_DEBUG_TIER)
				{
					return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjiSCzle5LCYjSc2eWYcFNBAzac79zYLICzjhF1nuhenlXE31Y8872/NRIPtt4siyJAd2ojrA1MKquz1CEcrAfalWbRmezvr/GkxOo/eMC/gx/eu+/oM0CeiXWIpdjD8mb6K6Z0gnXXA8naPnELH938G2LrQ8ZJ//NU1aspevD0qS8GjpUSo4A022mCLpsVxSavJ2EyGgGzFOIC51UXT6n7JPHizUs0WJEBV08pG5/X/YQWpm9yt9sguFfqJTYH68ACgWo9LQuE+BX6XXHFBJjzgK1xehPPlLgmSpAbnJvuLaoyviN6rGO5AEMRBMxHug0/HroBbOEMevrargrukEewIDAQAB";
				}
				return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlWZRXgwAVkz2LhFJLtkSiwowAPuNfoDc61ayawBq2HqaDK3y+s8XlaxscOHJd0Fg01j/mVBN0DhpfPbUvOAUHthhhAbluzdZZa/dCquZmYV8efCtyuMz6AvXIML4zwXnnX5XmHlZj937BC6tkxYhMK7N1WuG0yIKvXEoNEk37uAFLsjDHYTA4IO+XbzGDkq2hF4LmNSnReLyjrBbd39L4vvp11Wnrkx6J2wmTqboCDnprAEx6EiFUEnidgSCiMFpOiFWilro5oUA0x94zgYXvRcu1f+p2ZdFfmbRgMZCnQ/cudmS9BJhPU3rzh37F0L7ENSwdku0MBgN9mFnC2EtkQIDAQAB";
			}
		}
	}

	public struct TutorialStartData
	{
		public bool messageDisp;

		public string messageKey;

		public Vector3 messagePosPortrait;

		public Vector3 messagePosLandscape;

		public bool showSkip;

		public bool tapWait;

		public int stageNumber;

		public TUTORIAL_INDEX nextIndex;

		public TUTORIAL_INDEX lastIndex;

		public bool skipRule;

		public string l2dKey;

		public string l2dMotionKey;

		public string l2dExpressionKey;

		public float l2dScale;

		public float l2dOffsetPosY;

		public TutorialStartData(int sn, bool md, string mk, Vector3 pp, Vector3 pl, bool ss, bool tw, TUTORIAL_INDEX ni, TUTORIAL_INDEX li, bool sr, string _l2dkey, string _l2dmotion, string _l2dexpression, float _scale = 1f, float _posy = 0f)
		{
			messageDisp = md;
			messageKey = mk;
			messagePosPortrait = pp;
			messagePosLandscape = pl;
			showSkip = ss;
			tapWait = tw;
			stageNumber = sn;
			nextIndex = ni;
			lastIndex = li;
			skipRule = sr;
			l2dKey = _l2dkey;
			l2dMotionKey = _l2dmotion;
			l2dExpressionKey = _l2dexpression;
			l2dScale = _scale;
			l2dOffsetPosY = _posy;
		}
	}

	public enum TutorialCharExpression
	{
		SMILE = 0
	}

	public enum CONTINUE_ADDITIONALITEM
	{
		NONE = 0,
		SKILL_CHARGE = 1,
		SELECT_ONE = 2,
		WAVE_BOMB = 3,
		RAINBOW = 4,
		TAP_BOMB = 5,
		COLOR_CRUSH = 6
	}

	public struct ContinueStepData
	{
		public int price;

		public int addMove;

		public float addTime;

		public CONTINUE_ADDITIONALITEM item;

		public int num;

		public ContinueStepData(int p, int move, float time, CONTINUE_ADDITIONALITEM i, int n)
		{
			price = p;
			addMove = move;
			addTime = time;
			item = i;
			num = n;
		}
	}

	public static class GPAchievements
	{
		public enum KIND
		{
			MAKEUP_MASTER = 0,
			MARSHAL_FIGHTER5 = 1,
			SKILL_BEGINNER_1 = 2,
			SKILL_USER_10 = 3,
			SKILL_FIGHTER_30 = 4,
			SKILL_MASTER_100 = 5,
			FRIEND_FIRST_1 = 6,
			FRIEND_FRIENDLY_10 = 7,
			FRIEND_CONNECT_20 = 8,
			FRIEND_GENIUS_30 = 9,
			SENDHEART_MESSAGE_1 = 10,
			FRIENDHELP_SEND_1 = 11,
			FRIENDHELP_SUCCESS_1 = 12,
			COMBO_BEGINNER_10 = 13,
			COMBO_HUNTER_20 = 14,
			COMBO_CHASER_30 = 15,
			COMBO_MASTER_50 = 16,
			SCORE_BEGINNER_10000 = 17,
			SCORE_HUNTER_25000 = 18,
			SCORE_CHASER_50000 = 19,
			SCORE_MASTER_100000 = 20,
			TECHNICAL_BREAKER_3000 = 21,
			ACCESSORY_COLLECTOR_500 = 22,
			PIECE_COLLECTOR_50000 = 23,
			SILVER_MILLENNIUM_1000 = 24,
			FIND_LUNA_1000 = 25,
			PUNISHED_200 = 26
		}

		public static string GetKey(KIND _kind)
		{
			bool flag = true;
			flag = false;
			switch (_kind)
			{
			case KIND.MAKEUP_MASTER:
				return (!flag) ? "CgkIkvTlr5EWEAIQAA" : "CgkI6L_y9bcGEAIQAA";
			case KIND.MARSHAL_FIGHTER5:
				return (!flag) ? "CgkIkvTlr5EWEAIQAQ" : "CgkI6L_y9bcGEAIQAQ";
			case KIND.SKILL_BEGINNER_1:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQAg";
			case KIND.SKILL_USER_10:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQAw";
			case KIND.SKILL_FIGHTER_30:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQBA";
			case KIND.SKILL_MASTER_100:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQBQ";
			case KIND.FRIEND_FIRST_1:
				return (!flag) ? "CgkIkvTlr5EWEAIQAg" : "CgkI6L_y9bcGEAIQBg";
			case KIND.FRIEND_FRIENDLY_10:
				return (!flag) ? "CgkIkvTlr5EWEAIQAw" : "CgkI6L_y9bcGEAIQBw";
			case KIND.FRIEND_CONNECT_20:
				return (!flag) ? "CgkIkvTlr5EWEAIQBA" : "CgkI6L_y9bcGEAIQCA";
			case KIND.FRIEND_GENIUS_30:
				return (!flag) ? "CgkIkvTlr5EWEAIQBQ" : "CgkI6L_y9bcGEAIQCQ";
			case KIND.SENDHEART_MESSAGE_1:
				return (!flag) ? "CgkIkvTlr5EWEAIQBg" : "CgkI6L_y9bcGEAIQCg";
			case KIND.FRIENDHELP_SEND_1:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQCw";
			case KIND.FRIENDHELP_SUCCESS_1:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQDA";
			case KIND.COMBO_BEGINNER_10:
				return (!flag) ? "CgkIkvTlr5EWEAIQBw" : "CgkI6L_y9bcGEAIQDQ";
			case KIND.COMBO_HUNTER_20:
				return (!flag) ? "CgkIkvTlr5EWEAIQCA" : "CgkI6L_y9bcGEAIQDg";
			case KIND.COMBO_CHASER_30:
				return (!flag) ? "CgkIkvTlr5EWEAIQCQ" : "CgkI6L_y9bcGEAIQDw";
			case KIND.COMBO_MASTER_50:
				return (!flag) ? "CgkIkvTlr5EWEAIQCg" : "CgkI6L_y9bcGEAIQEA";
			case KIND.SCORE_BEGINNER_10000:
				return (!flag) ? "CgkIkvTlr5EWEAIQCw" : "CgkI6L_y9bcGEAIQEQ";
			case KIND.SCORE_HUNTER_25000:
				return (!flag) ? "CgkIkvTlr5EWEAIQDA" : "CgkI6L_y9bcGEAIQEg";
			case KIND.SCORE_CHASER_50000:
				return (!flag) ? "CgkIkvTlr5EWEAIQDQ" : "CgkI6L_y9bcGEAIQEw";
			case KIND.SCORE_MASTER_100000:
				return (!flag) ? "CgkIkvTlr5EWEAIQDg" : "CgkI6L_y9bcGEAIQFA";
			case KIND.TECHNICAL_BREAKER_3000:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQFQ";
			case KIND.ACCESSORY_COLLECTOR_500:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQFg";
			case KIND.PIECE_COLLECTOR_50000:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQFw";
			case KIND.SILVER_MILLENNIUM_1000:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQGA";
			case KIND.FIND_LUNA_1000:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQGQ";
			case KIND.PUNISHED_200:
				return (!flag) ? string.Empty : "CgkI6L_y9bcGEAIQGg";
			default:
				return string.Empty;
			}
		}
	}

	public const int ADV_ENTER_STAGENO = 21;

	public const int ADV_ENTER_CHARAID_MERCURY = 1;

	public const int ADV_ENTER_ACCESSORYID_MERCURY = 1;

	public const int ADV_ENTER_CHARAID_MARS = 2;

	public const int ADV_ENTER_ACCESSORYID_MARS = 2;

	public const float ADV_GRAY_SCREEN_DEFAULT_TIME = 0.25f;

	public const float ADV_DEF_TWEEN_TIME = 0.25f;

	public const int ADV_TUTORIAL_DUNGEON_DATA = 0;

	public const int ADV_TUTORIAL_STAGE_DATA = 0;

	public const int ADV_PROLOGUE_DUNGEON_DATA = 60000;

	public const int ADV_PROLOGUE_STAGE_1 = 60001;

	public const int ADV_PROLOGUE_STAGE_2 = 60002;

	public const int ADV_PROLOGUE_STAGE_3 = 60003;

	public const int ADV_BONUS_ENEMY_TUTORIAL_STAGE = 40035;

	public const int ADV_GLOBAL_FADE_SORT_ODER = 410;

	public const int ADV_GLOBALMENU_SORT_ODER = 400;

	public const int ADV_SUB_FADE_SORT_ODER = 390;

	public const int ADV_SUB_SORT_ODER = 300;

	public const int ADV_SUB_BG_Z = -2;

	public const int ADV_SUB_FIGURE_Z = -5;

	public const int ADV_SUB_GACHAEFFECT_Z = -5;

	public const float ADVPUZZLE_PR_BG_X = 0f;

	public const float ADVPUZZLE_PR_BG_Y = -185f;

	public const float ADVPUZZLE_LS_BG_X = 0f;

	public const float ADVPUZZLE_LS_BG_Y = 0f;

	public const float ADVPUZZLE_BG_Z = 20f;

	public const float ADV_PUZZLE_FRAME_Z = 19.5f;

	public const float ADVPUZZLE_CHAR_Z = 23f;

	public const float ADVPUZZLE_ENEMY_Z = 26f;

	public const int ADV_DEPTH_CHARACTERUI = 40;

	public const int ADV_DEPTH_ENEMYUI = 40;

	public const int ADV_DEPTH_GAMEUI = 40;

	public const int ADV_DEPTH_SYSTEMUI = 50;

	public const int ADV_DEPTH_DIALOG = 60;

	public const float DAMAGE_PIECE_MUL = 10f;

	public const float CHARGE_BASE_VALUE = 20f;

	public const float DEFFENCE_MUL_1 = 2.5f;

	public const float DEFFENCE_MUL_2 = 0.5f;

	public const float DEFFENCE_MUL_1_E = 2.2f;

	public const float DEFFENCE_MUL_2_E = 0.25f;

	public const float ATTACK_MUL_1_E = 1.2f;

	public const float ATTACK_MULRATEMAX_E = 1.5f;

	public const float ATTACK_MUL_LIMIT_E = 10000f;

	public const float ADV_CONTINUE_RECOVER_PER = 1f;

	public const int ADV_CREATE_GIMMICK_TILES_NUM = 21;

	public const int ADV_STAGEMAX_TILEKIND_NUM = 5;

	public const int SPECIAL_SOUL_CHARGECOUNT = 2;

	public const float ADV_ACTIONTIME = 5f;

	public const float ADV_FEVERTIME = 10f;

	public const int ADV_MAX_ENEMY_NUM = 3;

	public const int ADV_RATIO_WEIGHT = 1000;

	public const int ADV_MAX_PARTY_MEMBER = 5;

	public const int ADV_LEADER_POS_INDEX = 2;

	public const int ADV_LVL_UPDATE_NUM = 30;

	public const float ADV_SKILL_CHARGE = 0.37f;

	public const int ADV_SKILLTILE_CHARGE = -1;

	public const int ADV_FEVER_PIECE_NUM = 4;

	public const int ADV_FEVER_START_RAINBOW = 0;

	public const int ADV_FEVER_START_BOMB = 20;

	public const int ADV_FEVER_START_WAVE_V = 40;

	public const int ADV_FEVER_START_WAVE_H = 40;

	public const float ADV_FEVER_SPAWN_SPECIAL_PIECE = 10f;

	public const int ADV_FEVER_SPAWN_RAINBOW = 5;

	public const int ADV_FEVER_SPAWN_BOMB = 25;

	public const int ADV_FEVER_SPAWN_WAVE_V = 35;

	public const int ADV_FEVER_SPAWN_WAVE_H = 35;

	public const int ADV_ENEMY_PARTS_MAX = 8;

	public const int ADV_POWER_P_0 = 1000;

	public const int ADV_POWER_P_1 = 5000;

	public const int ADV_POWER_P_2 = 10000;

	public const int ADV_TUTORIAL_REWARDID_EXP = 150385;

	public const int ADV_TUTORIAL_REWARDID_SKILL = 150420;

	public const int RATING_STANDARD_HP_MIN = 450;

	public const int RATING_STANDARD_ATK_MIN = 230;

	public const int RATING_STANDARD_DEF_MIN = 230;

	public const int RATING_STANDARD_HP_MAX = 550;

	public const int RATING_STANDARD_ATK_MAX = 500;

	public const int RATING_STANDARD_DEF_MAX = 500;

	public const float RATING_HP_ADJUST_RATIO = 1f;

	public const float RATING_ATK_ADJUST_RATIO = 1f;

	public const float RATING_DEF_ADJUST_RATIO = 0.3f;

	public const int ATTACK_HP_COST_MIN = 1000;

	public const int ATTACK_HP_COST_MAX = 5000;

	public const float ATTACK_HP_MAGNI_RATE = 0.0001f;

	public const int DUMMY_UUID = 100;

	public const int DUMMY_HIVEID = 83;

	public const string DUMMY_UDID = "53CE2F77-3448-45EE-A339-FAA0A11B3C43";

	public const int OLD_EVENT_LOWER_ID = 200;

	public const int OLD_EVENT_GATE_ID = 1000;

	public const short Version = 1290;

	public const short AppVersion = 1290;

	public const short AdvVersion = 1190;

	public const short AdvLocalMTVersion = 1290;

	public const short SrvVersion = 1000;

	public const short ClientDataVersion = 1000;

	public const short DataFormatVersion = 10150;

	public const short ResourceDLFormatVersion = 1;

	public const float LONG_SIDE_SCREEN_SIZE = 1136f;

	public const float LONG_SIDE_SCREEN_SIZE_SHORT = 950f;

	public const float LONG_SIDE_SCREEN_SIZE_MAX = 1384f;

	public const float SHORT_SIDE_SCREEN_SIZE = 639f;

	public const int FADE_SCREEN_X = 1704;

	public const int FADE_SCREEN_Y = 1704;

	public const int FADE_SCREEN_RENDER_Q = 4000;

	public const int FADE_SCREEN_SORT_ODER = 500;

	public const float GRAY_SCREEN_ALPHA = 0.5f;

	public const float GRAY_SCREEN_DARK_ALPHA = 0.8f;

	public const float GRAY_SCREEN_PERFECTDARK_ALPHA = 1f;

	public const float GRAY_SCREEN_DEFAULT_TIME = 1f;

	public const float GRAY_SCREEN_SHORT_TIME = 0.5f;

	public const int MAP_FRAME_ANCHORED_POSITION_OFFSET_Y = 123;

	public const float MAP_SIZE = 1136f;

	public const float MAP_AVATAR_SCALE = 0.15f;

	public const int MAP_ROADBLOCK_KEY_MAX = 3;

	public const int LABYRINTH_STAGE_MAX = 9;

	public const int LABYRINTH_START_STAGENO = 10000;

	public const int LABYRINTH_GOAL_STAGENO = 20000;

	public const int CONVERT_HEART_PIECE_NUM = 15;

	public const int CONVERT_GROWUP_PIECE_NUM = 24;

	public const int MAX_WALLPAPER_PIECE_NUM = 9;

	public const int ALL_WALLPAPER_PIECE = 511;

	public const float INGREDIENT_SPAWN_RATE = 0.5f;

	public const int PAIR_PRESENCE_MAX = 7;

	public const int THROUGH_WALL0_CRUSH_SCORE = 250;

	public const int THROUGH_WALL1_CRUSH_SCORE = 250;

	public const int THROUGH_WALL2_CRUSH_SCORE = 250;

	public const int BROKEN_WALL0_CRUSH_SCORE = 30;

	public const int BROKEN_WALL1_CRUSH_SCORE = 30;

	public const int BROKEN_WALL2_CRUSH_SCORE = 30;

	public const int MOVABLE_WALL0_CRUSH_SCORE = 30;

	public const int MOVABLE_WALL1_CRUSH_SCORE = 30;

	public const int MOVABLE_WALL2_CRUSH_SCORE = 30;

	public const int CAPTURE0_CRUSH_SCORE = 30;

	public const int CAPTURE1_CRUSH_SCORE = 30;

	public const int CAPTURE2_CRUSH_SCORE = 30;

	public const int WATER_CRUSH_SCORE = 30;

	public const int INGREDIENT_DROP_SCORE = 2500;

	public const int PRESENTBOX_DROP_SCORE = 0;

	public const int GROWN_UP_CRUSH_SCORE = 500;

	public const int GROWN_ORB_CRUSH_SCORE = 500;

	public const int PEARL_CRUSH_SCORE = 1000;

	public const int DLFILELIST_GET_SPAN = 15;

	public const float IDLE_TIME = 4f;

	public const int DEFAULT_MAP_LIFE_FONTSIZE = 21;

	public const int COMPANION_LEVEL_5 = 5;

	public const int CAMPAIGN_CHECK_FREQ = 10;

	public const int TUTORIAL_VERSION = 1000;

	public const int ADV_TUTORIAL_MOON_ID = 1;

	public const int ADV_TUTORIAL_MERCURY_ID = 6;

	public const int ADV_TUTORIAL_MARS_ID = 11;

	public const int OLD_EVENT_OPEN_COST_KEY = 1;

	public const int OLD_EVENT_OPEN_COST_SHOPID = 93;

	public const int OLD_EVENT_CONTINNUE_COST_SHOPID = 94;

	public const int COMPANION_DEFAULT_INDEX = 0;

	public const int RESOURCEDL_MAX_RETRY = 5;

	public const string TRANSFER_SAME_OS = "0";

	public const string TRANSFER_ANDROID_TO_IOS = "1";

	public const string TRANSFER_IOS_TO_ANDROID = "2";

	public const int TRANSFER_MATRIX_IDX_FROM = 0;

	public const int TRANSFER_MATRIX_IDX_TO = 1;

	public const int TRANSFER_MATRIX_IDX_RELATION = 2;

	public static Color DEFAULT_ADV_MESSAGE_COLOR;

	public static Color ADV_FILTER_COLOR;

	public static Color ADV_SKILL_DESC_COLOR;

	public static float ADV_HUD_Z;

	public static float ADVPUZZLE_PORTRAIT_ORIGN_Y;

	public static float ADVPUZZLE_LANDSCAPE_ORIGN_X;

	public static float ADVPUZZLE_LANDSCAPE_ORIGN_Y;

	public static Vector3 ADVPUZZLE_CHAR_OFFSETPOS_PR;

	public static Vector3 ADVPUZZLE_CHAR_OFFSETPOS_LS;

	public static Vector3 ADVPUZZLE_ENEMY_OFFSETPOS_PR;

	public static Vector3 ADVPUZZLE_ENEMY_OFFSETPOS_LS;

	public static float ADV_SOUL_Z;

	public static float ADV_DEVICE_SCALE_MAX;

	public static readonly Vector3 PUZZLEEFFECT_OFFSET;

	public static readonly Vector3 PUZZLEEFFECT_OVERTILE_OFFSET;

	public static float TG_FRAME_TIME;

	public static int ADV_ADDHP_AMOUNT;

	public static short ADV_ADDHP_BONUS_FREQ;

	public static int ADV_ADDHP_BONUS_NUM;

	public static string[] AdvKindImageTbl;

	public static int[] ADV_FEVER_START_WEIGHT_LIST;

	public static TILE_KIND[] ADV_FEVER_START_TILE_KIND_LIST;

	public static TILE_FORM[] ADV_FEVER_START_TILE_FORM_LIST;

	public static int[] ADV_FEVER_SPAWN_WEIGHT_LIST;

	public static TILE_FORM[] ADV_FEVER_SPAWN_TILE_FORM_LIST;

	public static string DEBUG_LOAD_PLAYER_FILE;

	public static string DEBUG_UUID;

	public static SERIES SeriesGaidenMax;

	public static readonly SERIES[] SeriesCampaignSeries;

	public static List<ByteVector2> SKILL_SAILOR_MOON_00_AREA;

	public static List<ByteVector2> SKILL_SAILOR_MOON_01_AREA;

	public static List<ByteVector2> SKILL_REI2_AREA;

	public static List<ByteVector2> SKILL_MINAKO2_00_AREA;

	public static List<ByteVector2> SKILL_MINAKO2_01_AREA;

	public static List<ByteVector2> SKILL_SAILOR_MOON2_00_AREA;

	public static List<ByteVector2> SKILL_SAILOR_MOON2_01_AREA;

	public static List<ByteVector2> SKILL_SAILOR_PLUTO_00_AREA;

	public static List<ByteVector2> SKILL_SAILOR_PLUTO_01_AREA;

	public static List<ByteVector2> SKILL_SAILOR_NEPTUNE_00_AREA;

	public static List<ByteVector2> SKILL_SAILOR_NEPTUNE_01_AREA;

	public static List<ByteVector2> SKILL_SAILOR_URANUS_00_AREA;

	public static List<ByteVector2> SKILL_SAILOR_URANUS_01_AREA;

	public static List<ByteVector2> SKILL_SAILOR_MOON3_00_AREA;

	public static List<ByteVector2> SKILL_SAILOR_MOON3_01_AREA;

	public static List<ByteVector2> SKILL_MICHIRU2_AREA;

	public static List<ByteVector2> SKILL_PRINCESS_SERENITY_00_AREA;

	public static List<ByteVector2> SKILL_PRINCESS_SERENITY_01_AREA;

	public static List<ByteVector2> SKILL_S_SAILOR_MOON_00_AREA;

	public static List<ByteVector2> SKILL_S_SAILOR_MOON_01_AREA;

	public static List<ByteVector2> SKILL_TAIKI2_00_AREA;

	public static List<ByteVector2> SKILL_TAIKI2_01_AREA;

	public static List<ByteVector2> SKILL_TUXEDO_00_AREA;

	public static List<ByteVector2> SKILL_TUXEDO_01_AREA;

	public static List<ByteVector2> SKILL_BLACK_LADY_AREA;

	public static List<ByteVector2> SKILL_HELIOS_00_AREA;

	public static List<ByteVector2> SKILL_HELIOS_01_AREA;

	public static List<ByteVector2> SKILL_USAGIWEDING_AREA;

	public static List<ByteVector2> SKILL_SAILORMOON_ORIGIN_AREA;

	public static List<ByteVector2> SKILL_S_SAILORPLUTO_00_AREA;

	public static List<ByteVector2> SKILL_S_SAILORPLUTO_01_AREA;

	public static List<ByteVector2> SKILL_PRINCEDEMAND_AREA;

	public static List<ByteVector2> SKILL_STARHEALER_AREA;

	public static List<ByteVector2> SKILL_S_SAILORNEPTUNE_AREA;

	public static List<ByteVector2> SKILL_S_COSMOS_AREA;

	public static List<SkillFuncData> SKILL_FUNC_DATA;

	public static float BOOSTER_SKILL_CHARGE_RATIO;

	public static readonly string VersionStr;

	public static readonly string AppVersionStr;

	public static readonly string SrvVersionStr;

	public static readonly string URLVersion;

	public static readonly string ClientDataVersionStr;

	public static readonly string DataFormatVersionStr;

	public static readonly string ResourceDLFormatVersionStr;

	public static string CMB_ISMDJP_BundleID;

	public static string CMB_ASMDJP_BundleID;

	public static string CMB_ISMDWW_BundleID;

	public static string CMB_ASMDWW_BundleID;

	public static string BNE_ISMDJP_BundleID;

	public static string BNE_ASMDJP_BundleID;

	public static string BNE_ISMDWW_BundleID;

	public static string BNE_ASMDWW_BundleID;

	public static readonly Dictionary<SERIES, string> SeriesPrefix;

	public static readonly Dictionary<SERIES, string> SeriesLanguage;

	public static readonly Dictionary<SERIES, string> SeriesMapRouteData;

	public static readonly Dictionary<SERIES, SERIES> NextSeries;

	public static string[] CharacterIconLevelImageTbl;

	public static string[] CharacterIconLevelMaxImageTbl;

	public static string[] CharacterViewLevelImageTbl;

	public static string[] CharacterViewLevelMaxImageTbl;

	public static Color DEFAULT_MESSAGE_COLOR;

	public static Color DEFAULT_DIALOG_TITLE_COLOR;

	public static float DIALOG_BASE_Z;

	public static int DIALOG_BASE_DEPTH;

	public static bool HEART_BREAK_RETURN;

	public static float PARTNER_BG_Z;

	public static float PUZZLE_BG_Z;

	public static float PUZZLE_FRAME_Z;

	public static float PUZZLE_TILE_Z;

	public static float PUZZLE_TILE_EFFECT_Z;

	public static float PUZZLE_TILE_SCORE_Z;

	public static float HUD_Z;

	public static float FADE_BASE_Z;

	public static float MAP_BASE_Z;

	public static float MAP_SIGN_Z;

	public static float MAP_LINE_Z;

	public static float MAP_ENTITY_Z;

	public static float MAP_BUTTON_Z;

	public static float MAP_SILHOUETTE_Z;

	public static float MAP_FRIEND_Z;

	public static float MAP_AVATAR_Z;

	public static float MAP_MOVABLE_Z;

	public static float MAP_END_Z;

	public static float MAP_LENSFLARE_Z;

	public static readonly List<int> LabyrinthStageList;

	public static int ATTRIBUTE_LAYER_NUM;

	public static float DEVICE_SCALE_MAX;

	public static float TILE_SCALE_DEFAULT;

	public static float TILE_PITCH_H_DEFAULT;

	public static float TILE_PITCH_V_DEFAULT;

	public static float TILE_HITCHECK_H_DEFAULT;

	public static float TILE_HITCHECK_V_DEFAULT;

	public static float TILE_SCALE;

	public static float TILE_PITCH_H;

	public static float TILE_PITCH_V;

	public static float TILE_HITCHECK_H;

	public static float TILE_HITCHECK_V;

	public static float PUZZLE_PORTRAIT_ORIGN_Y;

	public static float PUZZLE_LANDSCAPE_ORIGN_X;

	public static Vector2 PUZZLE_PORTRAIT_FRAME_SIZE;

	public static Vector2 PUZZLE_LANDSCAPE_FRAME_SIZE;

	public static float TILE_WIDTH;

	public static float TILE_HEIGHT;

	public static float FALL_RESIST_TIME;

	public static int TARGET_FRAMERATE;

	public static float GRAVITY;

	public static float FALL_INIT_SPEED_RATIO;

	public static float MOVE_FINISH_CHECK_TIMER;

	public static float SWAP_SPEED;

	public static float SWAPRET_SPEED;

	public static float GATHER_CHECK_WAIT_TIMER;

	public static float FALL_SPEED_MAX;

	public static int SHUFFLE_TRY_MAXCOUNT;

	public static float CONTINUE_EXTEND_TIME;

	public static int CONTINUE_EXTEND_MOVE;

	public static int MAX_SCORE;

	public static int MAX_MOVES;

	public static float MAX_TIMER;

	public static int TILE_CLASH_SCORE;

	public static int TILE_COMBO_SCORE;

	public static int TILE_EFFECT_CRUSH_SCORE;

	public static int PAINT_CLASH_SCORE;

	public static int CLEAR_BONUS_SCORE;

	public static int CLEAR_BONUS_SCORE_UP;

	public static float ADDTIME_TILE_SEC;

	public static int ADDMOVE_TILE_MOVES;

	public static int ADD_TIMER_COUNT;

	public static int[] TILE_GENERATE_RATIO;

	public static float BOMB_B_STAY_TIME;

	public static float WAVE_BOMB_FIRST_CROSS_TIME;

	public static float WAVE_BOMB_SECOND_CROSS_TIME;

	public static float EXPLOSION_SCALE_RANDOM;

	public static float EXPLOSION_DELAY_RANDOM;

	public static int CLEAR_BONUS_TILE_NUM0;

	public static int CLEAR_BONUS_TILE_NUM1;

	public static IntVector2[] DIR_OFS;

	public static DIR[] RETURN_DIR;

	public static int InputName;

	public static int InputId;

	public static int RENOTIFICATION;

	public static Color DEFAULT_MAP_LIFE_COLOR;

	public static Color DEFAULT_MAP_GEM_COLOR;

	public static readonly int[] CompanionXPTable;

	public static Dictionary<SERIES, int[]> CharacterIntroList;

	public static List<int> CharacterIntroExcept;

	private static Vector3 TUT_MESSAGE_POS_P_L;

	private static Vector3 TUT_MESSAGE_POS_P_H;

	private static Vector3 TUT_MESSAGE_POS_L_L_L;

	private static Vector3 TUT_MESSAGE_POS_L_L_C;

	private static Vector3 TUT_MESSAGE_POS_L_C_R;

	private static Vector3 TUT_MAP_MESSAGE_POS_P_L;

	private static Vector3 TUT_MAP_MESSAGE_COCKPIT_POS_P_L;

	private static Vector3 TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L;

	private static Vector3 TUT_MAP_MESSAGE_POS_R_L;

	private static Vector3 TUT_MAP_MESSAGE_POS_PLAY;

	private static Vector3 TUT_MAP_MESSAGE_COCKPIT_POS_R_L;

	private static Vector3 TUT_MAP_MESSAGE_POS_L_L;

	private static Vector3 TUT_MAP_MESSAGE_POS_R_P;

	private static Vector3 TUT_MAP_MESSAGE_GROW_POS_R_P1;

	private static Vector3 TUT_MAP_MESSAGE_GROW_POS_R_P2;

	private static Vector3 TUT_MAP_MESSAGE_POS_L_P;

	private static Vector3 TUT_ADV_MESSAGE_POS_P_L;

	private static Vector3 TUT_ADV_MESSAGE_OPEN_POS_P_L;

	private static Vector3 TUT_ADV_MESSAGE_PUZZLE_POS_P_L;

	private static Vector3 TUT_ADV_MESSAGE_TEAM_POS_P_L;

	private static Vector3 TUT_ADV_MESSAGE_GASYA_POS_P_L;

	private static Vector3 TUT_ADV_MESSAGE_FIRST_POS_L_L;

	private static Vector3 TUT_ADV_MESSAGE_POS_L_L;

	private static Vector3 TUT_ADV_MESSAGE_OPEN_POS_L_L;

	private static Vector3 TUT_ADV_MESSAGE_PUZZLE_POS_L_L;

	private static Vector3 TUT_ADV_MESSAGE_TEAM_POS_L_L;

	private static Vector3 TUT_ADV_MESSAGE_GASYA_POS_L_L;

	public static readonly string ADV_TUTORIAL_L2DKey;

	private static float Motoki_Scale;

	private static float Motoki_OffsetY;

	public static TutorialStartData[] TUTORIAL_START_DATA;

	public static ContinueStepData[] CONTINUE_STEP;

	public static int OLD_EVENT_UNLOCKMINUTES;

	public static int OLD_EVENT_CONTINUE_ADDITIONALMINUTES;

	public static string[,] TRANSFER_MATRIX;

	public static readonly string StageNameFormat;

	public static readonly string StageNameFormatAdv;

	public static readonly int MaxStageNo;

	public static readonly Dictionary<BOOSTER_KIND, int> MugenHeartLimitTimeData;

	public static readonly Dictionary<int, BOOSTER_KIND> MugenHeartTimeToKindData;

	public static readonly Dictionary<int, BOOSTER_KIND> MugenHeartIdToKindData;

	public static readonly string HASH_SALT;

	public static float GetADVActionTime
	{
		get
		{
			return 5f;
		}
	}

	public static float GetADVFeverTime
	{
		get
		{
			return 10f;
		}
	}

	public static int ADV_FEVER_START_TOTAL
	{
		get
		{
			return 100;
		}
	}

	public static int ADV_FEVER_SPAWN_TOTAL
	{
		get
		{
			return 100;
		}
	}

	static Def()
	{
		DEBUG_LOAD_PLAYER_FILE = string.Empty;
		DEBUG_UUID = string.Empty;
		SeriesGaidenMax = SERIES.SM_GF00;
		SeriesCampaignSeries = new SERIES[6]
		{
			SERIES.SM_FIRST,
			SERIES.SM_R,
			SERIES.SM_S,
			SERIES.SM_SS,
			SERIES.SM_STARS,
			SERIES.SM_GF00
		};
		SKILL_SAILOR_MOON_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(0, 0),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1)
		};
		SKILL_SAILOR_MOON_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(0, 0),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2),
			new ByteVector2(0, -3),
			new ByteVector2(0, 2),
			new ByteVector2(0, 3),
			new ByteVector2(-2, 0),
			new ByteVector2(-3, 0),
			new ByteVector2(2, 0),
			new ByteVector2(3, 0)
		};
		SKILL_REI2_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1)
		};
		SKILL_MINAKO2_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-3, 3),
			new ByteVector2(-2, 2),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 0),
			new ByteVector2(1, 1),
			new ByteVector2(2, 2),
			new ByteVector2(3, 3)
		};
		SKILL_MINAKO2_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-3, 3),
			new ByteVector2(-3, 2),
			new ByteVector2(-2, 2),
			new ByteVector2(-2, 1),
			new ByteVector2(-1, 1),
			new ByteVector2(-1, 0),
			new ByteVector2(0, -1),
			new ByteVector2(1, 0),
			new ByteVector2(1, 1),
			new ByteVector2(2, 1),
			new ByteVector2(2, 2),
			new ByteVector2(3, 2),
			new ByteVector2(3, 3)
		};
		SKILL_SAILOR_MOON2_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, 2),
			new ByteVector2(2, 2),
			new ByteVector2(2, -2),
			new ByteVector2(-2, -2)
		};
		SKILL_SAILOR_MOON2_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, 2),
			new ByteVector2(2, 2),
			new ByteVector2(2, -2),
			new ByteVector2(-2, -2),
			new ByteVector2(-3, 3),
			new ByteVector2(3, 3),
			new ByteVector2(3, -3),
			new ByteVector2(-3, -3)
		};
		SKILL_SAILOR_PLUTO_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(0, 0),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1)
		};
		SKILL_SAILOR_PLUTO_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(0, 0),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-3, 0),
			new ByteVector2(3, 0),
			new ByteVector2(0, 2),
			new ByteVector2(0, -2)
		};
		SKILL_SAILOR_NEPTUNE_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(0, 0),
			new ByteVector2(-1, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-2, 1),
			new ByteVector2(0, 1),
			new ByteVector2(2, 1),
			new ByteVector2(0, 2),
			new ByteVector2(0, -1),
			new ByteVector2(0, -2)
		};
		SKILL_SAILOR_NEPTUNE_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(0, 0),
			new ByteVector2(-1, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-2, 1),
			new ByteVector2(0, 1),
			new ByteVector2(2, 1),
			new ByteVector2(-2, 2),
			new ByteVector2(0, 2),
			new ByteVector2(2, 2),
			new ByteVector2(0, 3),
			new ByteVector2(0, -1),
			new ByteVector2(-2, -2),
			new ByteVector2(-1, -2),
			new ByteVector2(0, -2),
			new ByteVector2(1, -2),
			new ByteVector2(2, -2),
			new ByteVector2(0, -3)
		};
		SKILL_SAILOR_URANUS_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(0, 0),
			new ByteVector2(-2, 1),
			new ByteVector2(0, 1),
			new ByteVector2(2, 1),
			new ByteVector2(-1, 2),
			new ByteVector2(1, 2),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1)
		};
		SKILL_SAILOR_URANUS_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(0, 0),
			new ByteVector2(-2, 1),
			new ByteVector2(0, 1),
			new ByteVector2(2, 1),
			new ByteVector2(-2, 2),
			new ByteVector2(-1, 2),
			new ByteVector2(0, 2),
			new ByteVector2(1, 2),
			new ByteVector2(2, 2),
			new ByteVector2(-2, 3),
			new ByteVector2(0, 3),
			new ByteVector2(2, 3),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, -2),
			new ByteVector2(2, -2),
			new ByteVector2(-1, -3),
			new ByteVector2(0, -3),
			new ByteVector2(1, -3)
		};
		SKILL_SAILOR_MOON3_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(0, -2),
			new ByteVector2(0, 2)
		};
		SKILL_SAILOR_MOON3_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(0, -2),
			new ByteVector2(0, 2),
			new ByteVector2(-3, 1),
			new ByteVector2(3, -1),
			new ByteVector2(-1, -3),
			new ByteVector2(1, 3)
		};
		SKILL_MICHIRU2_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1)
		};
		SKILL_PRINCESS_SERENITY_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-2, 0),
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2)
		};
		SKILL_PRINCESS_SERENITY_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-3, 0),
			new ByteVector2(-2, 0),
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(2, 0),
			new ByteVector2(3, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(0, 3),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2),
			new ByteVector2(0, -3)
		};
		SKILL_S_SAILOR_MOON_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-2, 0),
			new ByteVector2(0, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-3, 1),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(3, 1),
			new ByteVector2(0, 2),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2)
		};
		SKILL_S_SAILOR_MOON_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-2, 0),
			new ByteVector2(0, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-3, 1),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(3, 1),
			new ByteVector2(-3, 2),
			new ByteVector2(0, 2),
			new ByteVector2(3, 2),
			new ByteVector2(-2, 3),
			new ByteVector2(-1, 3),
			new ByteVector2(1, 3),
			new ByteVector2(2, 3),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2)
		};
		SKILL_TAIKI2_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(2, 2),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, -2)
		};
		SKILL_TAIKI2_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(2, 1),
			new ByteVector2(-2, 2),
			new ByteVector2(1, 2),
			new ByteVector2(2, 2),
			new ByteVector2(-2, -1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, -2),
			new ByteVector2(-1, -2),
			new ByteVector2(2, -2)
		};
		SKILL_TUXEDO_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(-1, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2)
		};
		SKILL_TUXEDO_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-3, 0),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(3, 0),
			new ByteVector2(-1, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(0, 3),
			new ByteVector2(-1, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2),
			new ByteVector2(0, -3)
		};
		SKILL_BLACK_LADY_AREA = new List<ByteVector2>
		{
			new ByteVector2(-2, 1),
			new ByteVector2(-1, 1),
			new ByteVector2(1, 1),
			new ByteVector2(2, 1),
			new ByteVector2(-1, 2),
			new ByteVector2(0, 2),
			new ByteVector2(1, 2),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(1, -1)
		};
		SKILL_HELIOS_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2)
		};
		SKILL_HELIOS_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(0, 4),
			new ByteVector2(-4, 0),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(4, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2),
			new ByteVector2(0, -4)
		};
		SKILL_USAGIWEDING_AREA = new List<ByteVector2>
		{
			new ByteVector2(-3, 1),
			new ByteVector2(0, 1),
			new ByteVector2(3, 1),
			new ByteVector2(-2, 2),
			new ByteVector2(-1, 2),
			new ByteVector2(1, 2),
			new ByteVector2(2, 2),
			new ByteVector2(-3, 0),
			new ByteVector2(3, 0),
			new ByteVector2(-2, -1),
			new ByteVector2(2, -1),
			new ByteVector2(-1, -2),
			new ByteVector2(1, -2),
			new ByteVector2(0, -3)
		};
		SKILL_SAILORMOON_ORIGIN_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2)
		};
		SKILL_S_SAILORPLUTO_00_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(-3, 0),
			new ByteVector2(-2, 0),
			new ByteVector2(2, 0),
			new ByteVector2(3, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2)
		};
		SKILL_S_SAILORPLUTO_01_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(0, 2),
			new ByteVector2(0, 3),
			new ByteVector2(-3, 0),
			new ByteVector2(-2, 0),
			new ByteVector2(-1, 0),
			new ByteVector2(1, 0),
			new ByteVector2(2, 0),
			new ByteVector2(3, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(0, -2),
			new ByteVector2(0, -3)
		};
		SKILL_PRINCEDEMAND_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-2, 2),
			new ByteVector2(2, 2),
			new ByteVector2(-3, 3),
			new ByteVector2(3, 3),
			new ByteVector2(-4, 4),
			new ByteVector2(4, 4),
			new ByteVector2(-5, 5),
			new ByteVector2(5, 5),
			new ByteVector2(-6, 6),
			new ByteVector2(6, 6),
			new ByteVector2(-7, 7),
			new ByteVector2(7, 7),
			new ByteVector2(-8, 8),
			new ByteVector2(8, 8),
			new ByteVector2(0, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, -2),
			new ByteVector2(2, -2),
			new ByteVector2(-3, -3),
			new ByteVector2(3, -3),
			new ByteVector2(-4, -4),
			new ByteVector2(4, -4),
			new ByteVector2(-5, -5),
			new ByteVector2(5, -5),
			new ByteVector2(-6, -6),
			new ByteVector2(6, -6),
			new ByteVector2(-7, -7),
			new ByteVector2(7, -7),
			new ByteVector2(-8, -8),
			new ByteVector2(8, -8)
		};
		SKILL_STARHEALER_AREA = new List<ByteVector2>
		{
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(-2, 2),
			new ByteVector2(2, 2),
			new ByteVector2(-3, 3),
			new ByteVector2(3, 3),
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(-2, -2),
			new ByteVector2(2, -2),
			new ByteVector2(-3, -3),
			new ByteVector2(3, -3)
		};
		SKILL_S_SAILORNEPTUNE_AREA = new List<ByteVector2>
		{
			new ByteVector2(-2, 1),
			new ByteVector2(0, 1),
			new ByteVector2(2, 1),
			new ByteVector2(-2, 2),
			new ByteVector2(0, 2),
			new ByteVector2(2, 2),
			new ByteVector2(0, 3),
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(0, -1),
			new ByteVector2(-2, -2),
			new ByteVector2(-1, -2),
			new ByteVector2(0, -2),
			new ByteVector2(1, -2),
			new ByteVector2(2, -2),
			new ByteVector2(0, -3)
		};
		SKILL_S_COSMOS_AREA = new List<ByteVector2>
		{
			new ByteVector2(-2, 1),
			new ByteVector2(-1, 1),
			new ByteVector2(0, 1),
			new ByteVector2(1, 1),
			new ByteVector2(2, 1),
			new ByteVector2(-1, 2),
			new ByteVector2(1, 2),
			new ByteVector2(0, 3),
			new ByteVector2(-3, 0),
			new ByteVector2(-1, 0),
			new ByteVector2(0, 0),
			new ByteVector2(1, 0),
			new ByteVector2(3, 0),
			new ByteVector2(-2, -1),
			new ByteVector2(-1, -1),
			new ByteVector2(0, -1),
			new ByteVector2(1, -1),
			new ByteVector2(2, -1),
			new ByteVector2(-1, -2),
			new ByteVector2(1, -2),
			new ByteVector2(0, -3)
		};
		SKILL_FUNC_DATA = new List<SkillFuncData>
		{
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 4, 2, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 2, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTryLuckBreakLineH", null, 50, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTryLuckBreakLineH", null, 75, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillNone", null, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillNone", null, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillNone", null, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillNone", null, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_REI2_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_REI2_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTryLuckBreakLineH", null, 50, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTryLuckBreakLineH", null, 75, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak2SailorV0", SKILL_MINAKO2_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak2SailorV1", SKILL_MINAKO2_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON2_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON2_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTryLuckBreakLineV", null, 40, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTryLuckBreakLineV", null, 65, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_PLUTO_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_PLUTO_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorPaint", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorPaint", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorCrush", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorCrush", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_URANUS_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_URANUS_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_NEPTUNE_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_NEPTUNE_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomBreakLineH", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomBreakLineH", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_MICHIRU2_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_MICHIRU2_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomBreakLineH", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomBreakLineH", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON3_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON3_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.SELECT_TARGET, "SkillSwap", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.SELECT_TARGET, "SkillSwap", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILOR_MOON_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILOR_MOON_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillNone", null, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillNone", null, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenHaruka0", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenHaruka1", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenMichiru0", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenMichiru1", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_PRINCESS_SERENITY_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_PRINCESS_SERENITY_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 1, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 2, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorCrush", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorCrush", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomWave", null, 1, 2, 40, 60, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomWave", null, 2, 3, 75, 25, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_TAIKI2_00_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_TAIKI2_01_AREA, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformAreaColor", SKILL_HELIOS_00_AREA, -1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformAreaColor", SKILL_HELIOS_01_AREA, -1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreakAndTransformBomb", SKILL_TUXEDO_00_AREA, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreakAndTransformBomb", SKILL_TUXEDO_01_AREA, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillBreakLineVH", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreakFailed", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillBreakLineWaveBomb", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreakAndTransformWave", SKILL_BLACK_LADY_AREA, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreakAndTransformWave", SKILL_BLACK_LADY_AREA, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 2, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 2, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformAreaColor", SKILL_USAGIWEDING_AREA, -1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformAreaColor", SKILL_USAGIWEDING_AREA, -1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorPaint", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorPaint", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrossBreak", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrossBreak", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 8, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 9, 6, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreakAndTransformBomb", SKILL_SAILORMOON_ORIGIN_AREA, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreakAndTransformBomb", SKILL_SAILORMOON_ORIGIN_AREA, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILORPLUTO_00_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILORPLUTO_01_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomRandomBreakLineH", null, 2, 3, 80, 20, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomRandomBreakLineH", null, 2, 3, 30, 70, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomWave", null, 1, 2, 40, 60, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomWave", null, 2, 3, 75, 25, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenUsagi0", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenUsagi1", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenHotaru0", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenHotaru1", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak2", SKILL_PRINCEDEMAND_AREA, 129, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak2", SKILL_PRINCEDEMAND_AREA, 130, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaint", null, 1, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_STARHEALER_AREA, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_STARHEALER_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomBreakLineH", null, 2, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomBreakLineH", null, 2, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILORNEPTUNE_AREA, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILORNEPTUNE_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorChange", null, 9, 3, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorChange", null, 9, 3, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 5, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorChange", null, 9, 3, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorChange", null, 9, 3, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 2, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWaveOrBomb", null, 2, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_URANUS_01_AREA, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_URANUS_01_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformWave", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 7, 8, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 9, 6, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILOR_MOON_01_AREA, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_SAILOR_MOON_01_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaintAndWave", null, 1, 1, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaintAndWave", null, 1, 1, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomRandomBreakLineH", null, 2, 3, 80, 20, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomRandomBreakLineH", null, 2, 3, 20, 80, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBombAndWave", null, 2, 2, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBombAndWave", null, 2, 2, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrossBreak", null, 2, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrossBreak", null, 3, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON3_01_AREA, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_SAILOR_MOON3_01_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorCrush", null, 0, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorCrush", null, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformBomb", null, 2, 0, 0, 0, 3),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_COSMOS_AREA, 0, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak", SKILL_S_COSMOS_AREA, 0, 0, 0, 0, 3),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 9, 6, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 9, 6, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenUsagi2_0", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillHalloweenUsagi2_1", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformRainbow", null, 1, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak2", SKILL_PRINCEDEMAND_AREA, 129, 0, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillGimmickBreak2", SKILL_PRINCEDEMAND_AREA, 130, 0, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorChange", null, 9, 3, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomColorChange", null, 9, 3, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaintAndBomb", null, 1, 1, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillTransformPaintAndBomb", null, 1, 1, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 9, 6, 0, 0, 1),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillRandomCrush", null, 9, 6, 0, 0, 2),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0),
			new SkillFuncData(SKILL_TYPE.DEFAULT, "SkillShuffle", null, 0, 0, 0, 0, 0)
		};
		BOOSTER_SKILL_CHARGE_RATIO = 1.1f;
		VersionStr = string.Empty + (short)1290;
		AppVersionStr = string.Empty + (short)1290;
		SrvVersionStr = string.Empty + (short)1000;
		URLVersion = "v" + (short)1000;
		ClientDataVersionStr = "cv" + (short)1000;
		DataFormatVersionStr = string.Empty + (short)10150;
		ResourceDLFormatVersionStr = string.Empty + (short)1;
		CMB_ISMDJP_BundleID = "com.bij.DevelopSample";
		CMB_ASMDJP_BundleID = "jp.co.capdev001.smd";
		CMB_ISMDWW_BundleID = "com.bij.SailorMoonDropsWW";
		CMB_ASMDWW_BundleID = "jp.co.capdev001.smdww";
		BNE_ISMDJP_BundleID = "jp.co.bandainamcogames.BNGI0223";
		BNE_ASMDJP_BundleID = "com.bandainamcoent.sailormoondrops";
		BNE_ISMDWW_BundleID = "jp.co.bandainamcoent.BNEI0264";
		BNE_ASMDWW_BundleID = "com.bandainamcoent.sailormoondropsww";
		SeriesPrefix = new Dictionary<SERIES, string>
		{
			{
				SERIES.SM_FIRST,
				"F"
			},
			{
				SERIES.SM_R,
				"R"
			},
			{
				SERIES.SM_S,
				"S"
			},
			{
				SERIES.SM_SS,
				"SS"
			},
			{
				SERIES.SM_STARS,
				"STARS"
			},
			{
				SERIES.SM_EV,
				"EV"
			},
			{
				SERIES.SM_GF00,
				"GF00"
			},
			{
				SERIES.SM_ADV,
				"ADV"
			}
		};
		SeriesLanguage = new Dictionary<SERIES, string>
		{
			{
				SERIES.SM_FIRST,
				"SeasonTitle_00"
			},
			{
				SERIES.SM_R,
				"SeasonTitle_01"
			},
			{
				SERIES.SM_S,
				"SeasonTitle_02"
			},
			{
				SERIES.SM_SS,
				"SeasonTitle_03"
			},
			{
				SERIES.SM_STARS,
				"SeasonTitle_04"
			},
			{
				SERIES.SM_EV,
				string.Empty
			},
			{
				SERIES.SM_GF00,
				"SeasonTitle_10"
			},
			{
				SERIES.SM_ADV,
				"Adventure"
			}
		};
		SeriesMapRouteData = new Dictionary<SERIES, string>
		{
			{
				SERIES.SM_FIRST,
				"Map_First"
			},
			{
				SERIES.SM_R,
				"Map_R"
			},
			{
				SERIES.SM_S,
				"Map_S"
			},
			{
				SERIES.SM_SS,
				"Map_SS"
			},
			{
				SERIES.SM_STARS,
				"Map_STARS"
			},
			{
				SERIES.SM_GF00,
				"Map_GF00"
			}
		};
		NextSeries = new Dictionary<SERIES, SERIES>
		{
			{
				SERIES.SM_FIRST,
				SERIES.SM_R
			},
			{
				SERIES.SM_R,
				SERIES.SM_S
			},
			{
				SERIES.SM_S,
				SERIES.SM_SS
			},
			{
				SERIES.SM_SS,
				SERIES.SM_STARS
			},
			{
				SERIES.SM_STARS,
				SERIES.SM_GF00
			},
			{
				SERIES.SM_GF00,
				SERIES.SM_EV
			}
		};
		CharacterIconLevelImageTbl = new string[6] { "lv_1", "lv_1", "lv_2", "lv_3", "lv_4", "lv_5_0" };
		CharacterIconLevelMaxImageTbl = new string[6] { "lv_1", "lv_1", "lv_2", "lv_3_max", "lv_4", "lv_5_0" };
		CharacterViewLevelImageTbl = new string[6] { "lv_1_2", "lv_1_2", "lv_2_2", "lv_3_2", "lv_4_2", "lv_5_2" };
		CharacterViewLevelMaxImageTbl = new string[6] { "lv_1_2", "lv_1_2", "lv_2_2", "lv_3_2_max", "lv_4_2", "lv_5_2" };
		DEFAULT_MESSAGE_COLOR = new Color(0.52156866f, 37f / 85f, 14f / 51f);
		DEFAULT_DIALOG_TITLE_COLOR = new Color(0.52156866f, 37f / 85f, 14f / 51f, 1f);
		DIALOG_BASE_Z = -1f;
		DIALOG_BASE_DEPTH = 60;
		HEART_BREAK_RETURN = true;
		PARTNER_BG_Z = 50f;
		PUZZLE_BG_Z = 20f;
		PUZZLE_FRAME_Z = 1f;
		PUZZLE_TILE_Z = 10f;
		PUZZLE_TILE_EFFECT_Z = 7.5f;
		PUZZLE_TILE_SCORE_Z = -6f;
		HUD_Z = -10f;
		FADE_BASE_Z = 0f;
		MAP_BASE_Z = 20f;
		MAP_SIGN_Z = 15f;
		MAP_LINE_Z = 10f;
		MAP_ENTITY_Z = 5f;
		MAP_BUTTON_Z = 0f;
		MAP_SILHOUETTE_Z = -1f;
		MAP_FRIEND_Z = -5.5f;
		MAP_AVATAR_Z = 18f;
		MAP_MOVABLE_Z = -4.5f;
		MAP_END_Z = -5f;
		MAP_LENSFLARE_Z = -6f;
		LabyrinthStageList = new List<int> { 100, 200, 300, 400, 500, 600, 700, 800, 900 };
		ATTRIBUTE_LAYER_NUM = 2;
		DEVICE_SCALE_MAX = 1.1f;
		TILE_SCALE_DEFAULT = 1f;
		TILE_PITCH_H_DEFAULT = 68f;
		TILE_PITCH_V_DEFAULT = 68f;
		TILE_HITCHECK_H_DEFAULT = 80f;
		TILE_HITCHECK_V_DEFAULT = 80f;
		TILE_SCALE = 1f;
		TILE_PITCH_H = 68f;
		TILE_PITCH_V = 68f;
		TILE_HITCHECK_H = 80f;
		TILE_HITCHECK_V = 80f;
		PUZZLE_PORTRAIT_ORIGN_Y = -759f;
		PUZZLE_LANDSCAPE_ORIGN_X = 776f;
		PUZZLE_PORTRAIT_FRAME_SIZE = new Vector2(950f, 754f);
		PUZZLE_LANDSCAPE_FRAME_SIZE = new Vector2(720f, 950f);
		TILE_WIDTH = 36f;
		TILE_HEIGHT = 36f;
		FALL_RESIST_TIME = 0.01f;
		TARGET_FRAMERATE = 30;
		GRAVITY = 2200f;
		FALL_INIT_SPEED_RATIO = 0.01f;
		MOVE_FINISH_CHECK_TIMER = 0.1f;
		SWAP_SPEED = 450f;
		SWAPRET_SPEED = 450f;
		GATHER_CHECK_WAIT_TIMER = 0.03f;
		FALL_SPEED_MAX = -3920f;
		SHUFFLE_TRY_MAXCOUNT = 100;
		CONTINUE_EXTEND_TIME = 15f;
		CONTINUE_EXTEND_MOVE = 5;
		MAX_SCORE = 9999999;
		MAX_MOVES = 999;
		MAX_TIMER = 999f;
		TILE_CLASH_SCORE = 10;
		TILE_COMBO_SCORE = 10;
		TILE_EFFECT_CRUSH_SCORE = 10;
		PAINT_CLASH_SCORE = 10;
		CLEAR_BONUS_SCORE = 200;
		CLEAR_BONUS_SCORE_UP = 150;
		ADDTIME_TILE_SEC = 5f;
		ADDMOVE_TILE_MOVES = 2;
		ADD_TIMER_COUNT = 5;
		TILE_GENERATE_RATIO = new int[7] { 20, 20, 20, 20, 20, 20, 20 };
		BOMB_B_STAY_TIME = 1.3f;
		WAVE_BOMB_FIRST_CROSS_TIME = 0.2f;
		WAVE_BOMB_SECOND_CROSS_TIME = 0.8f;
		EXPLOSION_SCALE_RANDOM = 0.2f;
		EXPLOSION_DELAY_RANDOM = 0.1f;
		CLEAR_BONUS_TILE_NUM0 = 7;
		CLEAR_BONUS_TILE_NUM1 = 5;
		DIR_OFS = new IntVector2[8]
		{
			new IntVector2(0, 1),
			new IntVector2(1, 1),
			new IntVector2(1, 0),
			new IntVector2(1, -1),
			new IntVector2(0, -1),
			new IntVector2(-1, -1),
			new IntVector2(-1, 0),
			new IntVector2(-1, 1)
		};
		RETURN_DIR = new DIR[8]
		{
			DIR.DOWN,
			DIR.DOWN_LEFT,
			DIR.LEFT,
			DIR.UP_LEFT,
			DIR.UP,
			DIR.UP_RIGHT,
			DIR.RIGHT,
			DIR.DOWN_RIGHT
		};
		InputName = 16;
		InputId = 8;
		RENOTIFICATION = 2;
		DEFAULT_MAP_LIFE_COLOR = new Color(1f, 1f, 1f);
		DEFAULT_MAP_GEM_COLOR = new Color(7f / 15f, 22f / 51f, 0.2f);
		CompanionXPTable = new int[6] { 0, 100, 200, 300, 400, 500 };
		CharacterIntroList = new Dictionary<SERIES, int[]>
		{
			{
				SERIES.SM_FIRST,
				new int[4] { 1, 2, 3, 4 }
			},
			{
				SERIES.SM_R,
				new int[3] { 16, 15, 19 }
			},
			{
				SERIES.SM_S,
				new int[3] { 29, 26, 25 }
			},
			{
				SERIES.SM_SS,
				new int[2] { 66, 67 }
			},
			{
				SERIES.SM_STARS,
				new int[3] { 106, 105, 104 }
			},
			{
				SERIES.SM_GF00,
				new int[1]
			}
		};
		CharacterIntroExcept = new List<int> { 1, 16, 29 };
		TUT_MESSAGE_POS_P_L = new Vector3(0f, -506f, 0f);
		TUT_MESSAGE_POS_P_H = new Vector3(0f, 0f, 0f);
		TUT_MESSAGE_POS_L_L_L = new Vector3(-260f, -250f, 0f);
		TUT_MESSAGE_POS_L_L_C = new Vector3(0f, -250f, 0f);
		TUT_MESSAGE_POS_L_C_R = new Vector3(260f, 0f, 0f);
		TUT_MAP_MESSAGE_POS_P_L = new Vector3(0f, -506f, 0f);
		TUT_MAP_MESSAGE_COCKPIT_POS_P_L = new Vector3(0f, -215f, 0f);
		TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L = new Vector3(0f, -30f, 0f);
		TUT_MAP_MESSAGE_POS_R_L = new Vector3(230f, -250f, 0f);
		TUT_MAP_MESSAGE_POS_PLAY = new Vector3(-230f, -57f, 0f);
		TUT_MAP_MESSAGE_COCKPIT_POS_R_L = new Vector3(230f, 6f, 0f);
		TUT_MAP_MESSAGE_POS_L_L = new Vector3(-230f, -250f, 0f);
		TUT_MAP_MESSAGE_POS_R_P = new Vector3(320f, -50f, 0f);
		TUT_MAP_MESSAGE_GROW_POS_R_P1 = new Vector3(320f, -170f, 0f);
		TUT_MAP_MESSAGE_GROW_POS_R_P2 = new Vector3(320f, 8f, 0f);
		TUT_MAP_MESSAGE_POS_L_P = new Vector3(-230f, -100f, 0f);
		TUT_ADV_MESSAGE_POS_P_L = new Vector3(0f, -500f, 0f);
		TUT_ADV_MESSAGE_OPEN_POS_P_L = new Vector3(0f, -300f, 0f);
		TUT_ADV_MESSAGE_PUZZLE_POS_P_L = new Vector3(0f, -500f, 0f);
		TUT_ADV_MESSAGE_TEAM_POS_P_L = new Vector3(0f, -500f, 0f);
		TUT_ADV_MESSAGE_GASYA_POS_P_L = new Vector3(0f, -500f, 0f);
		TUT_ADV_MESSAGE_FIRST_POS_L_L = new Vector3(-252f, -250f, 0f);
		TUT_ADV_MESSAGE_POS_L_L = new Vector3(230f, -250f, 0f);
		TUT_ADV_MESSAGE_OPEN_POS_L_L = new Vector3(0f, -50f, 0f);
		TUT_ADV_MESSAGE_PUZZLE_POS_L_L = new Vector3(230f, -250f, 0f);
		TUT_ADV_MESSAGE_TEAM_POS_L_L = new Vector3(300f, -250f, 0f);
		TUT_ADV_MESSAGE_GASYA_POS_L_L = new Vector3(-252f, -250f, 0f);
		ADV_TUTORIAL_L2DKey = "MOTOKI";
		Motoki_Scale = 0.65f;
		Motoki_OffsetY = -101f;
		TUTORIAL_START_DATA = new TutorialStartData[335]
		{
			new TutorialStartData(100, true, "MapTut_00_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_PLAY, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL1_1, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL1_3, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL1_3, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL1_4, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL1_5, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_05", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL1_6, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_06", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL1_7, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, true, "Tut_BaseRule_07", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL1_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL1_WIN1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL1_WIN2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL1_WIN3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL1_WIN4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "MapTut_01_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_LEVEL2_2, TUTORIAL_INDEX.MAP_LEVEL2_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "MapTut_01_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL2_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "Tut_Stlype_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL2_1, TUTORIAL_INDEX.LEVEL2_5, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "Tut_Stlype_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL2_2, TUTORIAL_INDEX.LEVEL2_5, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "Tut_Stlype_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL2_3, TUTORIAL_INDEX.LEVEL2_5, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "Tut_Stlype_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL2_4, TUTORIAL_INDEX.LEVEL2_5, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "Tut_Stlype_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL2_5, TUTORIAL_INDEX.LEVEL2_5, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(200, true, "Tut_Stlype_05", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL2_5, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(300, true, "MapTut_02_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL3_2, TUTORIAL_INDEX.MAP_LEVEL3_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(300, true, "MapTut_02_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL3_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(300, true, "Tut_Popping_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL3_1, TUTORIAL_INDEX.LEVEL3_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(300, true, "Tut_Popping_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL3_2, TUTORIAL_INDEX.LEVEL3_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(300, true, "Tut_Popping_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL3_3, TUTORIAL_INDEX.LEVEL3_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(300, true, "Tut_Popping_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL3_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "TutMap_03_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_LEVEL4_1, TUTORIAL_INDEX.MAP_LEVEL4_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "TutMap_03_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_LEVEL4_2, TUTORIAL_INDEX.MAP_LEVEL4_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "TutMap_03_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL4_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "Tut_Rainbow_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL4_1, TUTORIAL_INDEX.LEVEL4_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "Tut_Rainbow_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL4_2, TUTORIAL_INDEX.LEVEL4_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "Tut_Rainbow_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL4_3, TUTORIAL_INDEX.LEVEL4_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "Tut_Rainbow_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL4_4, TUTORIAL_INDEX.LEVEL4_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(400, true, "Tut_Rainbow_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL4_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(500, true, "Tut_Paint_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL5_1, TUTORIAL_INDEX.LEVEL5_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(500, true, "Tut_Paint_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL5_2, TUTORIAL_INDEX.LEVEL5_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(500, true, "Tut_Paint_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL5_3, TUTORIAL_INDEX.LEVEL5_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(500, true, "Tut_Paint_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL5_4, TUTORIAL_INDEX.LEVEL5_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(500, true, "Tut_Paint_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL5_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(600, true, "Tut_Cross_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL6_1, TUTORIAL_INDEX.LEVEL6_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(600, true, "Tut_Cross_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL6_2, TUTORIAL_INDEX.LEVEL6_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(600, true, "Tut_Cross_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL6_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, true, "Tut_Continue_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL7_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, true, "Tut_Continue_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, true, TUTORIAL_INDEX.LEVEL7_2, TUTORIAL_INDEX.LEVEL7_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, false, string.Empty, TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, false, TUTORIAL_INDEX.LEVEL7_3, TUTORIAL_INDEX.LEVEL7_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, false, string.Empty, TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL7_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "TutMap_04_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL8_1, TUTORIAL_INDEX.MAP_LEVEL8_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "TutMap_04_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL8_2, TUTORIAL_INDEX.MAP_LEVEL8_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "TutMap_04_02", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL8_3, TUTORIAL_INDEX.MAP_LEVEL8_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "TutMap_04_03", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL8_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "Tut_Barrier_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL8_1, TUTORIAL_INDEX.LEVEL8_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "Tut_Barrier_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL8_2, TUTORIAL_INDEX.LEVEL8_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "Tut_Barrier_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL8_3, TUTORIAL_INDEX.LEVEL8_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(800, true, "Tut_Barrier_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL8_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P1, false, false, TUTORIAL_INDEX.MAP_LEVEL9_1, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P1, false, true, TUTORIAL_INDEX.MAP_LEVEL9_1_2, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_01_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P1, false, true, TUTORIAL_INDEX.MAP_LEVEL9_2, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P1, false, false, TUTORIAL_INDEX.MAP_LEVEL9_3, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_03", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P2, false, true, TUTORIAL_INDEX.MAP_LEVEL9_4, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_04", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P2, false, true, TUTORIAL_INDEX.MAP_LEVEL9_5, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_05", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P2, false, false, TUTORIAL_INDEX.MAP_LEVEL9_6, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_06", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P2, false, true, TUTORIAL_INDEX.MAP_LEVEL9_7, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "TutMap_LevelUp_07", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_GROW_POS_R_P2, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "Tut_Skill_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL9_1, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "Tut_Skill_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL9_2, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "Tut_Skill_06", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL9_3, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "Tut_Skill_07", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL9_4, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, false, string.Empty, TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, false, TUTORIAL_INDEX.LEVEL9_5, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "Tut_Skill_08", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL9_6, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "Tut_Skill_09", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL9_7, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(900, true, "Tut_Skill_10", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL9_7, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_FriendMake_00", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL10_1, TUTORIAL_INDEX.MAP_LEVEL10_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_FriendMake_01", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL10_2, TUTORIAL_INDEX.MAP_LEVEL10_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_FriendMake_03", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL10_3, TUTORIAL_INDEX.MAP_LEVEL10_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_FriendMake_04", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL10_3, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_EnemyColor_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL10_1, TUTORIAL_INDEX.LEVEL10_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_EnemyColor_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL10_2, TUTORIAL_INDEX.LEVEL10_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_EnemyColor_02-1", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL10_3, TUTORIAL_INDEX.LEVEL10_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_EnemyColor_02-2", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL10_4, TUTORIAL_INDEX.LEVEL10_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1000, true, "Tut_EnemyColor_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL10_4, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1100, true, "Tut_Fall_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL11_1, TUTORIAL_INDEX.LEVEL11_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1100, true, "Tut_Fall_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL11_2, TUTORIAL_INDEX.LEVEL11_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1100, true, "Tut_Fall_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL11_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1100, true, "TutMap_06_00b", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL11_1, TUTORIAL_INDEX.MAP_LEVEL11_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1100, true, "TutMap_06_01b", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL11_4, TUTORIAL_INDEX.MAP_LEVEL11_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1100, true, "TutMap_06_04", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL11_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.EPILOGUE, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1200, true, "TutMap_Characer_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL12_1, TUTORIAL_INDEX.MAP_LEVEL12_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1200, true, "TutMap_Characer_01", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL12_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1300, true, "Tut_Plate_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL13_1, TUTORIAL_INDEX.LEVEL13_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1300, true, "Tut_Plate_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL13_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1300, true, "TutMap_CharaChange_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_P, true, true, TUTORIAL_INDEX.MAP_LEVEL13_1, TUTORIAL_INDEX.MAP_LEVEL13_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1300, true, "TutMap_CharaChange_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_P, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL13_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1400, true, "TutMap_SubRoute_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_LEVEL14_1, TUTORIAL_INDEX.MAP_LEVEL14_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1400, true, "TutMap_SubRoute_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_LEVEL14_2, TUTORIAL_INDEX.MAP_LEVEL14_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1400, true, "TutMap_SubRoute_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL14_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1500, true, "Tut_TileDelete_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL15_1, TUTORIAL_INDEX.LEVEL15_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1500, true, "Tut_TileDelete_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL15_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1600, true, "TutMap_ItemGet00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL16_1, TUTORIAL_INDEX.MAP_LEVEL16_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1600, true, "TutMap_ItemGet01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL16_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1600, true, "Tut_ItemGet_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL16_1, TUTORIAL_INDEX.LEVEL16_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1600, true, "Tut_ItemGet_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL16_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1700, true, "TutMap_WallPaper_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL17_1, TUTORIAL_INDEX.MAP_LEVEL17_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1700, true, "TutMap_WallPaper_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL17_2, TUTORIAL_INDEX.MAP_LEVEL17_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1700, true, "TutMap_WallPaper_02", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL17_2, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1800, true, "TutMap_Key_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL18_1, TUTORIAL_INDEX.LEVEL18_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1800, true, "TutMap_Key_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL18_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(2100, true, "Tut_TimeScore_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL21_1, TUTORIAL_INDEX.LEVEL21_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(2100, true, "Tut_TimeScore_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL21_1, true, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(2400, true, "Tut_Plate2", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL24_0, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(2600, true, "Tut_Barrier2", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL26_0, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "TutMap_RB_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL31_1, TUTORIAL_INDEX.MAP_LEVEL31_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "TutMap_RB_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL31_2, TUTORIAL_INDEX.MAP_LEVEL31_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "TutMap_RB_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL31_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(3100, true, "Tut_Crystal_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL31_1, TUTORIAL_INDEX.LEVEL31_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(3100, true, "Tut_Crystal_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL31_2, TUTORIAL_INDEX.LEVEL31_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(3100, true, "Tut_Crystal_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL31_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4800, true, "Tut_Cage_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL48_1, TUTORIAL_INDEX.LEVEL48_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4800, true, "Tut_Cage_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL48_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6100, true, "Tut_HideBarrier_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL61_1, TUTORIAL_INDEX.LEVEL61_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6100, true, "Tut_HideBarrier_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL61_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Luna_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL62_1, TUTORIAL_INDEX.LEVEL62_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Luna_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL62_2, TUTORIAL_INDEX.LEVEL62_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Luna_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL62_3, TUTORIAL_INDEX.LEVEL62_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Luna_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL62_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(7600, true, "Tut_Warp_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL76_1, TUTORIAL_INDEX.LEVEL76_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(7600, true, "Tut_Warp_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL76_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_PieceClash_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.SELECT_ONE_1, TUTORIAL_INDEX.SELECT_ONE_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_PieceClash_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.SELECT_ONE_2, TUTORIAL_INDEX.SELECT_ONE_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_PieceClash_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, true, true, TUTORIAL_INDEX.SELECT_ONE_3, TUTORIAL_INDEX.SELECT_ONE_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_PieceClash_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.SELECT_ONE_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ItemCatcher_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.SELECT_COLLECT_1, TUTORIAL_INDEX.SELECT_COLLECT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ItemCatcher_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.SELECT_COLLECT_2, TUTORIAL_INDEX.SELECT_COLLECT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ItemCatcher_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, true, true, TUTORIAL_INDEX.SELECT_COLLECT_3, TUTORIAL_INDEX.SELECT_COLLECT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ItemCatcher_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.SELECT_COLLECT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ColorClash_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.COLOR_CRUSH_1, TUTORIAL_INDEX.COLOR_CRUSH_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ColorClash_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.COLOR_CRUSH_2, TUTORIAL_INDEX.COLOR_CRUSH_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ColorClash_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, true, true, TUTORIAL_INDEX.COLOR_CRUSH_3, TUTORIAL_INDEX.COLOR_CRUSH_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_ColorClash_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.COLOR_CRUSH_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_FriendHelp_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.FRIEND_HELP_0, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_NameChange_00", TUT_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE1, TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_NameChange_01", TUT_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE2, TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_NameChange_02", TUT_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL_NAMECHANGE2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Event_00", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_1, TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Event_01", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_2, TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Event_02", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL_TUT_EVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10600, true, "Tut_EnemyPlate_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL106_1, TUTORIAL_INDEX.LEVEL106_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10600, true, "Tut_EnemyPlate_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL106_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10900, true, "Tut_Barrier3", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL109_0, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Sale_01", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_TUT_SHOP_1, TUTORIAL_INDEX.MAP_LEVEL_TUT_SHOP_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Sale_02", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL_TUT_SHOP_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(13600, true, "Tut_Plate3", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL136_0, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16700, true, "Tut_Race_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL167_1, TUTORIAL_INDEX.LEVEL167_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16700, true, "Tut_Race_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL167_2, TUTORIAL_INDEX.LEVEL167_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16700, true, "Tut_Race_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL167_3, TUTORIAL_INDEX.LEVEL167_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16700, true, "Tut_Race_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL167_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_NewSeason_Notice_Desc", TUT_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.R_SEASON_NOTICE, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.RATING_F_MARS, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.RATING_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(19700, true, "Tut_Cage2", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL197_0, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_StageSkip_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_C_R, true, true, TUTORIAL_INDEX.STAGE_SKIP_1, TUTORIAL_INDEX.STAGE_SKIP_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_StageSkip_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_C_R, true, true, TUTORIAL_INDEX.STAGE_SKIP_2, TUTORIAL_INDEX.STAGE_SKIP_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_StageSkip_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_C_R, true, true, TUTORIAL_INDEX.STAGE_SKIP_3, TUTORIAL_INDEX.STAGE_SKIP_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_StageSkip_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_C_R, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STAGE_SKIP_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_C_R, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.TROPHY_GUIDE_LOSE, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_C_R, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.TROPHY_GUIDE_WIN, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Torophy_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, true, true, TUTORIAL_INDEX.TROPHY_TUT_1, TUTORIAL_INDEX.TROPHY_TUT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Torophy_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, true, true, TUTORIAL_INDEX.TROPHY_TUT_2, TUTORIAL_INDEX.TROPHY_TUT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Torophy_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, true, true, TUTORIAL_INDEX.TROPHY_TUT_3, TUTORIAL_INDEX.TROPHY_TUT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Torophy_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_C, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.TROPHY_TUT_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "SeasonSerect_00", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_SEASON_SELECT_1, TUTORIAL_INDEX.MAP_SEASON_SELECT_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "SeasonSerect_01", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_SEASON_SELECT_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_SERIES_NOTICE_R, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_SERIES_NOTICE_S, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_SERIES_NOTICE_SS, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_SERIES_NOTICE_STARS, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(3200, true, "Tut_Yoma_00", TUT_MESSAGE_POS_P_H, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL32R_1, TUTORIAL_INDEX.LEVEL32R_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(3200, true, "Tut_Yoma_01", TUT_MESSAGE_POS_P_H, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL32R_2, TUTORIAL_INDEX.LEVEL32R_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(3200, true, "Tut_Yoma_02", TUT_MESSAGE_POS_P_H, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL32R_3, TUTORIAL_INDEX.LEVEL32R_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(3200, true, "Tut_Yoma_03", TUT_MESSAGE_POS_P_H, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL32R_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_MugenHeart_01", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_TUT_INFINITY_1, TUTORIAL_INDEX.MAP_LEVEL_TUT_INFINITY_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_MugenHeart_02", TUT_MAP_MESSAGE_BAROON_SHOPBAG_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL_TUT_INFINITY_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Hug_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL62R_1, TUTORIAL_INDEX.LEVEL62R_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Hug_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL62R_2, TUTORIAL_INDEX.LEVEL62R_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Hug_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL62R_3, TUTORIAL_INDEX.LEVEL62R_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Hug_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL62R_4, TUTORIAL_INDEX.LEVEL62R_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(6200, true, "Tut_Hug_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL62R_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1, true, "Tut_MovePlus_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL1S_1, TUTORIAL_INDEX.LEVEL1S_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(1, true, "Tut_MovePlus_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL1S_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "AdvTutorial_runa00", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL22_1, TUTORIAL_INDEX.MAP_LEVEL22_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "AdvTutorial_runa01", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL22_2, TUTORIAL_INDEX.MAP_LEVEL22_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "AdvTutorial_runa02", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL22_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "AdvTutorial_motoki00", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_FIRST_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_1, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_5, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki01", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_FIRST_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_2, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_5, false, ADV_TUTORIAL_L2DKey, "motions/hit01.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki02", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_FIRST_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_3, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_5, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki03", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_FIRST_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_4, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_5, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki04", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_FIRST_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_5, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_5, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki05", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_FIRST_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_INITIAL_HOME_1_5, false, ADV_TUTORIAL_L2DKey, "motions/hit00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki06", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_1, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki07", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_2, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki08", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_3, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki09", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_4, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki10", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_5, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki18", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_6, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki19", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_7, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki20", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_8, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki11", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki12", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_1_9, false, ADV_TUTORIAL_L2DKey, "motions/hit00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki13", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_2_1, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_2_3, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki14", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, false, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_2_2, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_2_3, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, false, string.Empty, TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_2_3, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_2_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, false, string.Empty, TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_2_3, false, ADV_TUTORIAL_L2DKey, "motions/hit01.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki15", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_INITIAL_PUZZLE_3_0, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki16", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_INITIAL_HOME_2_1, TUTORIAL_INDEX.ADV_INITIAL_HOME_2_1, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki17", TUT_ADV_MESSAGE_OPEN_POS_P_L, TUT_ADV_MESSAGE_OPEN_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_INITIAL_HOME_2_1, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, false, string.Empty, TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_INITIAL_COMPLETE, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki62", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_PLAY_1_1, TUTORIAL_INDEX.ADV_PLAY_1_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki63", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_PLAY_1_2, TUTORIAL_INDEX.ADV_PLAY_1_3, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki64", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_PLAY_1_3, TUTORIAL_INDEX.ADV_PLAY_1_3, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki65", TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_PLAY_1_3, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki69", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_1, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_4, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki70", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_2, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_4, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki71", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_3, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_4, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki72", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_4, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_4, false, ADV_TUTORIAL_L2DKey, "motions/crisis00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki73", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_PUZZLE_ATTRIBUTE_1_4, false, ADV_TUTORIAL_L2DKey, "motions/hit01.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki77", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_1, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_4, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki78", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_2, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_4, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki79", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_3, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_4, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki80", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_4, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_4, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki81", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_PUZZLE_FEVER_1_4, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki41", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.ADV_GASHA_1_1, TUTORIAL_INDEX.ADV_GASHA_1_1, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki42", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_GASHA_1_1, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki43", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.ADV_GASHA_2_1, TUTORIAL_INDEX.ADV_GASHA_2_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki44", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.ADV_GASHA_2_2, TUTORIAL_INDEX.ADV_GASHA_2_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki45", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.ADV_GASHA_2_3, TUTORIAL_INDEX.ADV_GASHA_2_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki46", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_GASHA_2_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki47", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.ADV_GASHA_3_1, TUTORIAL_INDEX.ADV_GASHA_3_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki48", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.ADV_GASHA_3_2, TUTORIAL_INDEX.ADV_GASHA_3_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki49", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.ADV_GASHA_3_3, TUTORIAL_INDEX.ADV_GASHA_3_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki50", TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_GASHA_3_3, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, false, string.Empty, TUT_ADV_MESSAGE_GASYA_POS_P_L, TUT_ADV_MESSAGE_GASYA_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_GASHA_COMPLETE, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki22", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_1_1, TUTORIAL_INDEX.ADV_TEAM_1_4, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki23", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_1_2, TUTORIAL_INDEX.ADV_TEAM_1_4, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki24", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, false, TUTORIAL_INDEX.ADV_TEAM_1_3, TUTORIAL_INDEX.ADV_TEAM_1_4, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki25", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_1_4, TUTORIAL_INDEX.ADV_TEAM_1_4, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki26", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_TEAM_1_4, false, ADV_TUTORIAL_L2DKey, "motions/hit00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki27", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_1, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki28", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, false, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_2, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki29", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_3, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki30", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_4, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki31", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_5, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki32", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_6, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki33", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, false, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_7, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/hit00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki34", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_8, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki35", TUT_ADV_MESSAGE_TEAM_POS_P_L, TUT_ADV_MESSAGE_TEAM_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_9, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki36", TUT_ADV_MESSAGE_OPEN_POS_P_L, TUT_ADV_MESSAGE_OPEN_POS_L_L, false, true, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/win00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki37", TUT_ADV_MESSAGE_OPEN_POS_P_L, TUT_ADV_MESSAGE_OPEN_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_TEAM_DETAIL_1_10, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, false, string.Empty, TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_TEAM_DETAIL_COMPLETE, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(7600, true, "Tut_Familiar_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL76S_1, TUTORIAL_INDEX.LEVEL76S_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(7600, true, "Tut_Familiar_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL76S_2, TUTORIAL_INDEX.LEVEL76S_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(7600, true, "Tut_Familiar_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, false, TUTORIAL_INDEX.LEVEL76S_3, TUTORIAL_INDEX.LEVEL76S_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(7600, true, "Tut_Familiar_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL76S_4, TUTORIAL_INDEX.LEVEL76S_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(7600, true, "Tut_Familiar_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL76S_5, TUTORIAL_INDEX.LEVEL76S_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(7600, true, "Tut_Familiar_05", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL76S_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "AdvTutorial_motoki84", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_PUZZLE_HEAL_1_1, TUTORIAL_INDEX.ADV_PUZZLE_HEAL_1_2, false, ADV_TUTORIAL_L2DKey, "motions/wait00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki85", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.ADV_PUZZLE_HEAL_1_2, TUTORIAL_INDEX.ADV_PUZZLE_HEAL_1_2, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_motoki86", TUT_ADV_MESSAGE_PUZZLE_POS_P_L, TUT_ADV_MESSAGE_PUZZLE_POS_L_L, false, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_PUZZLE_HEAL_1_2, false, ADV_TUTORIAL_L2DKey, "motions/talk00.mtn", string.Empty, Motoki_Scale, Motoki_OffsetY),
			new TutorialStartData(-1, true, "AdvTutorial_runa09", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_1, TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "AdvTutorial_runa10", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_2, TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "AdvTutorial_runa11", TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_LEVEL_TUT_ADVEVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(13700, true, "Tut_EnemyFamiliar_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL137S_1, TUTORIAL_INDEX.LEVEL137S_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(13700, true, "Tut_EnemyFamiliar_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL137S_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_ADV_MESSAGE_POS_P_L, TUT_ADV_MESSAGE_POS_L_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.ADV_BONUS_ENEMY_COMPLETE, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_7000, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_8000, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_8010, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_8020, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_8030, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_8040, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_8050, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9000, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9010, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9020, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(15100, true, "Tut_BonusArea00_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_TUT_BONUS_0_1, TUTORIAL_INDEX.MAP_TUT_BONUS_0_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(15100, true, "Tut_BonusArea00_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_TUT_BONUS_0_2, TUTORIAL_INDEX.MAP_TUT_BONUS_0_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(15100, true, "Tut_BonusArea00_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_TUT_BONUS_0_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16600, true, "Tut_BonusArea01_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_TUT_BONUS_1_1, TUTORIAL_INDEX.MAP_TUT_BONUS_1_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16600, true, "Tut_BonusArea01_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_TUT_BONUS_1_2, TUTORIAL_INDEX.MAP_TUT_BONUS_1_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16600, true, "Tut_BonusArea01_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_TUT_BONUS_1_3, TUTORIAL_INDEX.MAP_TUT_BONUS_1_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(16600, true, "Tut_BonusArea01_03", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_TUT_BONUS_1_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, true, "Tut_SpecialOrb_A_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL7SS_1, TUTORIAL_INDEX.LEVEL7SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, true, "Tut_SpecialOrb_A_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL7SS_2, TUTORIAL_INDEX.LEVEL7SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, true, "Tut_SpecialOrb_A_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL7SS_3, TUTORIAL_INDEX.LEVEL7SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(700, true, "Tut_SpecialOrb_A_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL7SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(2300, true, "Tut_SpecialOrb_B_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL23SS_1, TUTORIAL_INDEX.LEVEL23SS_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(2300, true, "Tut_SpecialOrb_B_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL23SS_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4400, true, "Tut_SpecialOrb_C_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL47SS_0, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_OldEvent_B_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.OLDEVENT_1, TUTORIAL_INDEX.OLDEVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_OldEvent_B_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.OLDEVENT_2, TUTORIAL_INDEX.OLDEVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_OldEvent_B_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.OLDEVENT_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10700, true, "Tut_Goal_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL107SS_1, TUTORIAL_INDEX.LEVEL107SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10700, true, "Tut_Goal_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL107SS_2, TUTORIAL_INDEX.LEVEL107SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10700, true, "Tut_Goal_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL107SS_3, TUTORIAL_INDEX.LEVEL107SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10700, true, "Tut_Goal_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL107SS_3, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4700, true, "Tut_FlowerPearl_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL47StarS_1, TUTORIAL_INDEX.LEVEL47StarS_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4700, true, "Tut_FlowerPearl_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL47StarS_2, TUTORIAL_INDEX.LEVEL47StarS_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4700, true, "Tut_FlowerPearl_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL47StarS_3, TUTORIAL_INDEX.LEVEL47StarS_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4700, true, "Tut_FlowerPearl_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL47StarS_4, TUTORIAL_INDEX.LEVEL47StarS_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(4700, true, "Tut_FlowerPearl_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL47StarS_4, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Labyrinth_A_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.JEWEL_1, TUTORIAL_INDEX.JEWEL_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Labyrinth_A_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.JEWEL_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Labyrinth_B_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.JEWELCHANCE_1, TUTORIAL_INDEX.JEWELCHANCE_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Labyrinth_B_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.JEWELCHANCE_2, TUTORIAL_INDEX.JEWELCHANCE_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Labyrinth_B_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.JEWELCHANCE_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10700, true, "Tut_FlowerPearlBlock_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.LEVEL107StarS_1, TUTORIAL_INDEX.LEVEL107StarS_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(10700, true, "Tut_FlowerPearlBlock_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.LEVEL107StarS_1, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9001, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9002, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9003, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9011, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9012, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9021, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9022, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(100, false, string.Empty, Vector3.zero, Vector3.zero, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.STARTUP_9023, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Talisman_00", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.TALISMAN_1, TUTORIAL_INDEX.TALISMAN_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Talisman_01", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.TALISMAN_2, TUTORIAL_INDEX.TALISMAN_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Talisman_02", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.TALISMAN_3, TUTORIAL_INDEX.TALISMAN_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Talisman_03", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.TALISMAN_4, TUTORIAL_INDEX.TALISMAN_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Talisman_04", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.TALISMAN_5, TUTORIAL_INDEX.TALISMAN_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, true, "Tut_Talisman_05", TUT_MESSAGE_POS_P_L, TUT_MESSAGE_POS_L_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.TALISMAN_5, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(-1, false, string.Empty, TUT_MAP_MESSAGE_COCKPIT_POS_P_L, TUT_MAP_MESSAGE_COCKPIT_POS_R_L, false, false, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_SERIES_NOTICE_GF00, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(9100, true, "Tut_BonusArea00_00", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_TUT_BONUS_2_1, TUTORIAL_INDEX.MAP_TUT_BONUS_2_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(9100, true, "Tut_BonusArea00_01", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.MAP_TUT_BONUS_2_2, TUTORIAL_INDEX.MAP_TUT_BONUS_2_2, false, string.Empty, string.Empty, string.Empty),
			new TutorialStartData(9100, true, "Tut_BonusArea00_02", TUT_MAP_MESSAGE_POS_P_L, TUT_MAP_MESSAGE_POS_L_L, true, true, TUTORIAL_INDEX.NONE, TUTORIAL_INDEX.MAP_TUT_BONUS_2_2, false, string.Empty, string.Empty, string.Empty)
		};
		CONTINUE_STEP = new ContinueStepData[10]
		{
			new ContinueStepData(10, 5, 15f, CONTINUE_ADDITIONALITEM.NONE, 0),
			new ContinueStepData(10, 5, 15f, CONTINUE_ADDITIONALITEM.SKILL_CHARGE, 1),
			new ContinueStepData(20, 5, 15f, CONTINUE_ADDITIONALITEM.SELECT_ONE, 1),
			new ContinueStepData(20, 5, 15f, CONTINUE_ADDITIONALITEM.WAVE_BOMB, 1),
			new ContinueStepData(30, 5, 15f, CONTINUE_ADDITIONALITEM.RAINBOW, 1),
			new ContinueStepData(30, 5, 15f, CONTINUE_ADDITIONALITEM.TAP_BOMB, 1),
			new ContinueStepData(40, 15, 30f, CONTINUE_ADDITIONALITEM.RAINBOW, 1),
			new ContinueStepData(40, 30, 60f, CONTINUE_ADDITIONALITEM.COLOR_CRUSH, 1),
			new ContinueStepData(50, 30, 60f, CONTINUE_ADDITIONALITEM.RAINBOW, 2),
			new ContinueStepData(50, 30, 60f, CONTINUE_ADDITIONALITEM.COLOR_CRUSH, 1)
		};
		OLD_EVENT_UNLOCKMINUTES = 1440;
		OLD_EVENT_CONTINUE_ADDITIONALMINUTES = 720;
		TRANSFER_MATRIX = new string[8, 3]
		{
			{ "asmdjp", "asmdjp", "0" },
			{ "asmdjp", "ismdjp", "1" },
			{ "ismdjp", "asmdjp", "2" },
			{ "ismdjp", "ismdjp", "0" },
			{ "asmddr", "asmddr", "0" },
			{ "asmddr", "ismddr", "1" },
			{ "ismddr", "asmddr", "2" },
			{ "ismddr", "ismddr", "0" }
		};
		StageNameFormat = "{0}_{1:0000}_{2:00}";
		StageNameFormatAdv = "{0}_{1:00000000}_{2:00}";
		MaxStageNo = 100000;
		MugenHeartLimitTimeData = new Dictionary<BOOSTER_KIND, int>
		{
			{
				BOOSTER_KIND.MUGEN_HEART15,
				900
			},
			{
				BOOSTER_KIND.MUGEN_HEART30,
				1800
			},
			{
				BOOSTER_KIND.MUGEN_HEART60,
				3600
			}
		};
		MugenHeartTimeToKindData = new Dictionary<int, BOOSTER_KIND>
		{
			{
				900,
				BOOSTER_KIND.MUGEN_HEART15
			},
			{
				1800,
				BOOSTER_KIND.MUGEN_HEART30
			},
			{
				3600,
				BOOSTER_KIND.MUGEN_HEART60
			}
		};
		MugenHeartIdToKindData = new Dictionary<int, BOOSTER_KIND>
		{
			{
				38,
				BOOSTER_KIND.MUGEN_HEART15
			},
			{
				39,
				BOOSTER_KIND.MUGEN_HEART30
			},
			{
				40,
				BOOSTER_KIND.MUGEN_HEART60
			},
			{
				41,
				BOOSTER_KIND.MUGEN_HEART15
			},
			{
				42,
				BOOSTER_KIND.MUGEN_HEART30
			},
			{
				43,
				BOOSTER_KIND.MUGEN_HEART60
			}
		};
		HASH_SALT = "C7guJA9N";
		DEFAULT_ADV_MESSAGE_COLOR = new Color(32f / 85f, 0.16862746f, 0.34509805f);
		ADV_FILTER_COLOR = new Color(0f, 0f, 0f, 0.5f);
		ADV_SKILL_DESC_COLOR = new Color(0.5137255f, 29f / 85f, 0.4862745f);
		ADV_HUD_Z = 0f;
		ADVPUZZLE_PORTRAIT_ORIGN_Y = -735f;
		ADVPUZZLE_LANDSCAPE_ORIGN_X = 776f;
		ADVPUZZLE_LANDSCAPE_ORIGN_Y = 100f;
		ADVPUZZLE_CHAR_OFFSETPOS_PR = new Vector3(0f, 110f, 23f);
		ADVPUZZLE_CHAR_OFFSETPOS_LS = new Vector3(-360f, 0f, 23f);
		ADVPUZZLE_ENEMY_OFFSETPOS_PR = new Vector3(0f, -268f, 26f);
		ADVPUZZLE_ENEMY_OFFSETPOS_LS = new Vector3(200f, 145f, 26f);
		ADV_SOUL_Z = PUZZLE_TILE_Z - 1f;
		ADV_DEVICE_SCALE_MAX = 1.15f;
		PUZZLEEFFECT_OFFSET = new Vector3(0f, 0f, -1f);
		PUZZLEEFFECT_OVERTILE_OFFSET = PUZZLEEFFECT_OFFSET + new Vector3(0f, 0f, 0f);
		TG_FRAME_TIME = 1f / (float)TARGET_FRAMERATE;
		ADV_ADDHP_AMOUNT = 100;
		ADV_ADDHP_BONUS_FREQ = 10;
		ADV_ADDHP_BONUS_NUM = 10;
		AdvKindImageTbl = new string[6] { "null", "null", "AdvSkillLevelUp_Confirm_Title", "ConsumeItemDesc_N_Ticket", "ConsumeItemDesc_P_Ticket", "ConsumeItemDesc_P_Support" };
		ADV_FEVER_START_WEIGHT_LIST = new int[4] { 40, 40, 20, 0 };
		ADV_FEVER_START_TILE_KIND_LIST = new TILE_KIND[4]
		{
			TILE_KIND.NONE,
			TILE_KIND.NONE,
			TILE_KIND.NONE,
			TILE_KIND.SPECIAL
		};
		ADV_FEVER_START_TILE_FORM_LIST = new TILE_FORM[4]
		{
			TILE_FORM.WAVE_H,
			TILE_FORM.WAVE_V,
			TILE_FORM.BOMB,
			TILE_FORM.RAINBOW
		};
		ADV_FEVER_SPAWN_WEIGHT_LIST = new int[4] { 35, 35, 25, 5 };
		ADV_FEVER_SPAWN_TILE_FORM_LIST = new TILE_FORM[4]
		{
			TILE_FORM.WAVE_H,
			TILE_FORM.WAVE_V,
			TILE_FORM.BOMB,
			TILE_FORM.RAINBOW
		};
		InputId = 10;
	}

	public static string GetSeriesString(SERIES aSeries, int aEventId)
	{
		string text = SeriesPrefix[aSeries];
		if (aSeries == SERIES.SM_EV || aSeries == SERIES.SM_ADV)
		{
			text += aEventId;
		}
		return text;
	}

	public static bool IsEventSeries(SERIES a_series)
	{
		bool flag = false;
		if (a_series == SERIES.SM_EV)
		{
			return true;
		}
		return false;
	}

	public static bool IsGaidenSeries(SERIES a_series)
	{
		bool result = false;
		if (a_series >= SERIES.SM_GF00 && a_series <= SeriesGaidenMax)
		{
			result = true;
		}
		return result;
	}

	public static bool IsAdvSeries(SERIES a_series)
	{
		bool flag = false;
		if (a_series == SERIES.SM_ADV)
		{
			return true;
		}
		return false;
	}

	public static T GetAttribute<T>(Enum value) where T : Attribute
	{
		Type type = value.GetType();
		string name = Enum.GetName(type, value);
		T[] array = (T[])type.GetField(name).GetCustomAttributes(typeof(T), false);
		return (array == null || array.Length <= 0) ? ((T)null) : array[0];
	}

	public static STAGE_WIN_CONDITION ToWin(this STAGE_TYPE value)
	{
		StageConditionAttribute attribute = GetAttribute<StageConditionAttribute>(value);
		return attribute.Win;
	}

	public static STAGE_LOSE_CONDITION ToLose(this STAGE_TYPE value)
	{
		StageConditionAttribute attribute = GetAttribute<StageConditionAttribute>(value);
		return attribute.Lose;
	}

	public static int GetStageNo(int a_main, int a_sub)
	{
		return a_main * 100 + a_sub;
	}

	public static int GetStageNo(int a_course, int a_main, int a_sub)
	{
		return a_course * MaxStageNo + a_main * 100 + a_sub;
	}

	public static int GetStageNoForServer(int a_eventID, int a_stageno)
	{
		return a_eventID * MaxStageNo * 10 + a_stageno;
	}

	public static void SplitServerStageNo(int a_stageno, out int a_eventID, out short a_course, out int a_main, out int a_sub)
	{
		a_eventID = a_stageno / (MaxStageNo * 10);
		a_stageno %= MaxStageNo * 10;
		SplitEventStageNo(a_stageno, out a_course, out a_main, out a_sub);
	}

	public static void SplitEventStageNo(int a_stageno, out short a_course, out int a_main, out int a_sub)
	{
		if (a_stageno < MaxStageNo)
		{
			a_course = 0;
			SplitStageNo(a_stageno, out a_main, out a_sub);
		}
		else
		{
			a_course = (short)(a_stageno / MaxStageNo);
			int a_stage = a_stageno % MaxStageNo;
			SplitStageNo(a_stage, out a_main, out a_sub);
		}
	}

	public static void SplitStageNo(int a_stage, out int a_main, out int a_sub)
	{
		if (a_stage < 100)
		{
			a_main = a_stage;
			a_sub = 0;
		}
		else
		{
			a_main = a_stage / 100;
			a_sub = a_stage % 100;
		}
	}

	public static string GetDisplayStageNo(int a_main, int a_sub)
	{
		string empty = string.Empty;
		if (a_sub == 0)
		{
			return string.Format("{0}", a_main);
		}
		return string.Format("{0}-{1}", a_main, a_sub);
	}

	public static float GetRouteOrder(float a_main, float a_sub, bool a_isSub = false)
	{
		if (!a_isSub)
		{
			return a_main;
		}
		return a_main + 0.01f * a_sub;
	}

	public static float GetRouteOrder(int a_stageNo, bool a_isSub = false)
	{
		int a_main;
		int a_sub;
		SplitStageNo(a_stageNo, out a_main, out a_sub);
		return GetRouteOrder(a_main, a_sub, a_isSub);
	}

	public static int GetStageNoByRouteOrder(float order)
	{
		return (int)(order * 100.00001f);
	}
}
