public enum SsKeyCastType
{
	Int = 0,
	Float = 1,
	Bool = 2,
	Hex = 3,
	Degree = 4,
	Other = 5,
	Num = 6
}
