public class SMDailyEventSetting : SMMapStageSetting
{
	public int DailyEventNo;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteInt(DailyEventNo);
		base.Serialize(a_writer);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		DailyEventNo = a_reader.ReadInt();
		base.Deserialize(a_reader);
	}
}
