using UnityEngine;

public class InstagramAndroid : InstagramIF
{
	private static AndroidJavaObject mJavaInstance;

	private static AndroidJavaObject javaInstance
	{
		get
		{
			if (mJavaInstance == null)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.bij.bijunityplugin.MainActivity"))
				{
					mJavaInstance = androidJavaClass.CallStatic<AndroidJavaObject>("getInstagramPlugin", new object[0]);
				}
			}
			return mJavaInstance;
		}
	}

	public override void enableLogging(bool enable, bool verbose)
	{
		javaInstance.Call("enableLogging", enable, verbose);
	}

	public override void setup(string key, string secret, bool autoConnect)
	{
		javaInstance.Call("setup", base.gameObject.name, key, secret, autoConnect);
	}

	public override void onPause()
	{
	}

	public override void onResume()
	{
	}

	public override bool isCanceled()
	{
		return javaInstance.Call<bool>("isCanceled", new object[0]);
	}

	public override bool preview(string title, string message, string link, string attachUrl, string attachMime)
	{
		return javaInstance.Call<bool>("preview", new object[5] { title, message, link, attachUrl, attachMime });
	}
}
