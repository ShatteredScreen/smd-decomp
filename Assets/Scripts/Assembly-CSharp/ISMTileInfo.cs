using System.Collections.Generic;

public interface ISMTileInfo
{
	List<Def.TILE_KIND> Stocks { get; }

	int TimerRemain { get; }

	bool FallIngredient { get; }

	bool FallPresentBox { get; }

	bool FallMovableWall { get; }

	bool FallPair { get; }

	bool FallTimerBomb { get; }

	bool TileAttribute_BLANK { get; set; }

	bool TileAttribute_FIND { get; set; }

	bool TileAttribute2_FAMILIAR_SPIRIT { get; set; }

	Def.TILE_KIND FindType { get; set; }

	string FindImageName { get; set; }

	int FindSizeX { get; set; }

	int FindSizeY { get; set; }

	int FindRotate { get; set; }

	int WarpIndex { get; set; }

	int WarpOutPosX { get; set; }

	int WarpOutPosY { get; set; }

	bool MakeObstacleArea { get; }

	byte FamiliarSpiritID { get; set; }

	byte DianaWayKindNum { get; }

	void SetTimerBombParam(int a_remain);

	void SetFallParam(bool a_isIngredient, bool a_isPresentBox, bool a_isMovavleWall, bool a_isPair, bool a_isTimerBomb);

	void SetTileAttributeParam(bool a_isTileBreak);

	void SetFindParam(Def.TILE_KIND a_kind, bool a_isTileFind, int a_rotate, string a_image, int a_sizeX, int a_sizeY);

	void SetWarpParam(int a_index, int a_x, int a_y);

	void SetObstacleParam(bool a_isMakeObstacleArea);

	void SetDianaWayKind(byte a_kind);

	bool IsAttributeTile();

	bool IsOverTile();
}
