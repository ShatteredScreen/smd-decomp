using System;
using System.Collections.Generic;

public class UserBehavior
{
	public class PurchaseRecord
	{
		public long mTime;

		public short mIpaID;

		public short mCampaignId;

		public short mCampaignGid;

		public short mPrice;

		public bool IsNormal()
		{
			if (mIpaID == -99)
			{
				return false;
			}
			if (mCampaignId == -1)
			{
				return true;
			}
			return false;
		}

		public bool IsCampaign()
		{
			if (mIpaID == -99)
			{
				return false;
			}
			if (mCampaignId == -1)
			{
				return false;
			}
			return true;
		}
	}

	public class UseGemRecord
	{
		public long mTime;

		public short mGem;
	}

	public class BuyBoosterRecord
	{
		public long mTime;

		public short mShopID;

		public short mGem;
	}

	public class UseBoosterRecord
	{
		public long mTime;

		public short mKind;

		public short mNum;

		public short mSeriese;

		public short mStageCondition;
	}

	public class BuyContinueRecord
	{
		public long mTime;

		public short mNum;

		public short mSeriese;

		public short mStageCondition;
	}

	public class BuyHeartRecord
	{
		public long mTime;
	}

	public class BuyMugenHeartRecord
	{
		public long mTime;
	}

	public class BuySkipRecord
	{
		public long mTime;
	}

	public class BuyChanceRecord
	{
		public long mTime;
	}

	public class ChanceRecord
	{
		public short mType;

		public int mOfferCnt;

		public int mBuyCnt;

		public List<BuyChanceRecord> mList;
	}

	public class BuyAdvGasyaRecord
	{
		public long mTime;

		public short mNum;

		public short mSale;

		public short mGem;
	}

	public class BuyAdvMedalRecord
	{
		public long mTime;
	}

	public class BuyAdvContinueRecord
	{
		public long mTime;
	}

	public class PlayRecord
	{
		public long mTime;

		public short mSeriese;

		public short mStageCondition;

		public bool mWin;

		public short mStar;

		public short mMakeSpecial;

		public short mSwapSpecial;
	}

	private class _UseBooster
	{
		public int mKind;

		public int mCnt;
	}

	private const int mUseGemListMax = 200;

	private const int mBuyContinueListMax = 200;

	private const int mBuyHeartListMax = 200;

	private const int mBuyMHeartListMax = 200;

	private const int mBuySkipListMax = 200;

	private const int mBuyChanceListMax = 200;

	private const int mBuyAdvGasyaListMax = 200;

	private const int mBuyAdvMedalListMax = 200;

	private const int mBuyAdvContinueListMax = 200;

	private const int mUseBoosterListMax = 200;

	private const int mPlayListMax = 200;

	private static UserBehavior mInstance;

	private short[] mPriceList = new short[24]
	{
		120, 600, 1200, 2400, 4800, 8800, 1800, 360, 1800, 120,
		120, 240, 360, 480, 600, 960, 1200, 2400, 3000, 3600,
		4000, 4500, 4800, 5000
	};

	public short mVersion;

	public List<PurchaseRecord> mPurchaseList;

	public int mUseGemCnt;

	public int mUseGemAmount;

	public short mUseGemMax;

	public List<UseGemRecord> mUseGemList;

	public List<BuyBoosterRecord> mBuyBoosterList;

	public List<BuyContinueRecord> mBuyContinueList;

	public List<BuyHeartRecord> mBuyHeartList;

	public List<BuyMugenHeartRecord> mBuyMHeartList;

	public List<BuySkipRecord> mBuySkipList;

	public Dictionary<short, ChanceRecord> mChanceDict;

	public List<BuyAdvGasyaRecord> mBuyAdvGasyaList;

	public List<BuyAdvMedalRecord> mBuyAdvMedalList;

	public List<BuyAdvContinueRecord> mBuyAdvContinueList;

	public List<UseBoosterRecord> mUseBoosterList;

	public List<PlayRecord> mPlayList;

	public List<int> mStripeCountHistory;

	public Dictionary<Def.SERIES, object> mMode7Id;

	public Dictionary<Def.SERIES, object> mMode7RemainCount;

	public Dictionary<Def.SERIES, object> mMode7LastTime;

	private int _ContinueMax;

	public static UserBehavior Instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = LoadUserBehavior();
			}
			return mInstance;
		}
	}

	protected UserBehavior()
	{
	}

	public static void Save()
	{
		if (mInstance != null)
		{
			mInstance.SaveUserBehavior();
		}
	}

	private void SaveUserBehavior()
	{
		try
		{
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(Constants.USERBEHAVIOR_FILE))
			{
				mInstance.Serialize(bIJEncryptDataWriter);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	public static void Load()
	{
		UserBehavior instance = Instance;
	}

	private static UserBehavior LoadUserBehavior()
	{
		UserBehavior userBehavior = null;
		try
		{
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(Constants.USERBEHAVIOR_FILE))
			{
				userBehavior = Deserialize(bIJEncryptDataReader);
				bIJEncryptDataReader.Close();
			}
		}
		catch (Exception)
		{
		}
		if (userBehavior == null)
		{
			userBehavior = CreateUserBehavior();
		}
		return userBehavior;
	}

	private static UserBehavior CreateUserBehavior()
	{
		UserBehavior userBehavior = new UserBehavior();
		userBehavior.Initialize();
		return userBehavior;
	}

	public void Reset()
	{
		Initialize();
	}

	private void Initialize()
	{
		mVersion = 1290;
		if (mPurchaseList == null)
		{
			mPurchaseList = new List<PurchaseRecord>();
		}
		else
		{
			mPurchaseList.Clear();
		}
		mUseGemCnt = 0;
		mUseGemAmount = 0;
		mUseGemMax = 0;
		if (mUseGemList == null)
		{
			mUseGemList = new List<UseGemRecord>();
		}
		else
		{
			mUseGemList.Clear();
		}
		if (mBuyBoosterList == null)
		{
			mBuyBoosterList = new List<BuyBoosterRecord>();
		}
		else
		{
			mBuyBoosterList.Clear();
		}
		if (mBuyContinueList == null)
		{
			mBuyContinueList = new List<BuyContinueRecord>();
		}
		else
		{
			mBuyContinueList.Clear();
		}
		if (mBuyHeartList == null)
		{
			mBuyHeartList = new List<BuyHeartRecord>();
		}
		else
		{
			mBuyHeartList.Clear();
		}
		if (mBuyMHeartList == null)
		{
			mBuyMHeartList = new List<BuyMugenHeartRecord>();
		}
		else
		{
			mBuyMHeartList.Clear();
		}
		if (mBuySkipList == null)
		{
			mBuySkipList = new List<BuySkipRecord>();
		}
		else
		{
			mBuySkipList.Clear();
		}
		if (mChanceDict == null)
		{
			mChanceDict = new Dictionary<short, ChanceRecord>();
		}
		else
		{
			foreach (ChanceRecord value in mChanceDict.Values)
			{
				value.mList.Clear();
			}
			mChanceDict.Clear();
		}
		if (mBuyAdvGasyaList == null)
		{
			mBuyAdvGasyaList = new List<BuyAdvGasyaRecord>();
		}
		else
		{
			mBuyAdvGasyaList.Clear();
		}
		if (mBuyAdvMedalList == null)
		{
			mBuyAdvMedalList = new List<BuyAdvMedalRecord>();
		}
		else
		{
			mBuyAdvMedalList.Clear();
		}
		if (mBuyAdvContinueList == null)
		{
			mBuyAdvContinueList = new List<BuyAdvContinueRecord>();
		}
		else
		{
			mBuyAdvContinueList.Clear();
		}
		if (mUseBoosterList == null)
		{
			mUseBoosterList = new List<UseBoosterRecord>();
		}
		else
		{
			mUseBoosterList.Clear();
		}
		if (mPlayList == null)
		{
			mPlayList = new List<PlayRecord>();
		}
		else
		{
			mPlayList.Clear();
		}
		if (mStripeCountHistory == null)
		{
			mStripeCountHistory = new List<int>();
		}
		else
		{
			mStripeCountHistory.Clear();
		}
		if (mMode7Id == null)
		{
			mMode7Id = new Dictionary<Def.SERIES, object>();
		}
		else
		{
			mMode7Id.Clear();
		}
		if (mMode7RemainCount == null)
		{
			mMode7RemainCount = new Dictionary<Def.SERIES, object>();
		}
		else
		{
			mMode7RemainCount.Clear();
		}
		if (mMode7LastTime == null)
		{
			mMode7LastTime = new Dictionary<Def.SERIES, object>();
		}
		else
		{
			mMode7LastTime.Clear();
		}
	}

	private static UserBehavior Deserialize(BIJBinaryReader aReader)
	{
		UserBehavior userBehavior = new UserBehavior();
		userBehavior.mVersion = aReader.ReadShort();
		userBehavior.mPurchaseList = new List<PurchaseRecord>();
		int num = aReader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			long aUniversalTime = aReader.ReadLong();
			short aIapID = aReader.ReadShort();
			short aCampaignId = aReader.ReadShort();
			short aCampaignGid = aReader.ReadShort();
			short aPrice = aReader.ReadShort();
			userBehavior._addPurchaseRecord(aUniversalTime, aIapID, aCampaignId, aCampaignGid, aPrice);
		}
		userBehavior.mUseGemCnt = aReader.ReadInt();
		userBehavior.mUseGemAmount = aReader.ReadInt();
		userBehavior.mUseGemMax = aReader.ReadShort();
		userBehavior.mUseGemList = new List<UseGemRecord>();
		num = aReader.ReadInt();
		for (int j = 0; j < num; j++)
		{
			long aTime = aReader.ReadLong();
			short aGem = aReader.ReadShort();
			userBehavior._addUseGem(aTime, aGem);
		}
		userBehavior.mBuyBoosterList = new List<BuyBoosterRecord>();
		num = aReader.ReadInt();
		for (int k = 0; k < num; k++)
		{
			long aUniversalTime2 = aReader.ReadLong();
			short aShopID = aReader.ReadShort();
			short aGem2 = aReader.ReadShort();
			userBehavior._addBuyBoosterRecord(aUniversalTime2, aShopID, aGem2);
		}
		userBehavior.mBuyContinueList = new List<BuyContinueRecord>();
		num = aReader.ReadInt();
		for (int l = 0; l < num; l++)
		{
			long aUniversalTime3 = aReader.ReadLong();
			short aNum = aReader.ReadShort();
			short aSeriese = aReader.ReadShort();
			short aCondition = aReader.ReadShort();
			userBehavior._addBuyContinueRecord(aUniversalTime3, aNum, aSeriese, aCondition, true);
		}
		userBehavior.mBuyHeartList = new List<BuyHeartRecord>();
		num = aReader.ReadInt();
		for (int m = 0; m < num; m++)
		{
			long aUniversalTime4 = aReader.ReadLong();
			userBehavior._addBuyHeartRecord(aUniversalTime4);
		}
		userBehavior.mBuyMHeartList = new List<BuyMugenHeartRecord>();
		num = aReader.ReadInt();
		for (int n = 0; n < num; n++)
		{
			long aUniversalTime5 = aReader.ReadLong();
			userBehavior._addBuyMugenHeartRecord(aUniversalTime5);
		}
		userBehavior.mBuySkipList = new List<BuySkipRecord>();
		num = aReader.ReadInt();
		for (int num2 = 0; num2 < num; num2++)
		{
			long aUniversalTime6 = aReader.ReadLong();
			userBehavior._addBuySkipRecord(aUniversalTime6);
		}
		userBehavior.mChanceDict = new Dictionary<short, ChanceRecord>();
		num = aReader.ReadInt();
		for (int num3 = 0; num3 < num; num3++)
		{
			ChanceRecord chanceRecord = new ChanceRecord();
			chanceRecord.mType = aReader.ReadShort();
			chanceRecord.mOfferCnt = aReader.ReadInt();
			chanceRecord.mBuyCnt = aReader.ReadInt();
			chanceRecord.mList = new List<BuyChanceRecord>();
			int num4 = aReader.ReadInt();
			for (int num5 = 0; num5 < num4; num5++)
			{
				long aUniversalTime7 = aReader.ReadLong();
				userBehavior._addSpecialChanceRecord(chanceRecord, aUniversalTime7);
			}
			userBehavior.mChanceDict.Add(chanceRecord.mType, chanceRecord);
		}
		userBehavior.mBuyAdvGasyaList = new List<BuyAdvGasyaRecord>();
		num = aReader.ReadInt();
		for (int num6 = 0; num6 < num; num6++)
		{
			long aUniversalTime8 = aReader.ReadLong();
			short aNum2 = aReader.ReadShort();
			short aSale = aReader.ReadShort();
			short aGem3 = aReader.ReadShort();
			userBehavior._addBuyAdvGasyaRecord(aUniversalTime8, aNum2, aSale, aGem3);
		}
		userBehavior.mBuyAdvMedalList = new List<BuyAdvMedalRecord>();
		num = aReader.ReadInt();
		for (int num7 = 0; num7 < num; num7++)
		{
			long aUniversalTime9 = aReader.ReadLong();
			userBehavior._addBuyAdvMedalRecord(aUniversalTime9);
		}
		userBehavior.mBuyAdvContinueList = new List<BuyAdvContinueRecord>();
		num = aReader.ReadInt();
		for (int num8 = 0; num8 < num; num8++)
		{
			long aUniversalTime10 = aReader.ReadLong();
			userBehavior._addBuyAdvContinueRecord(aUniversalTime10);
		}
		userBehavior.mUseBoosterList = new List<UseBoosterRecord>();
		num = aReader.ReadInt();
		for (int num9 = 0; num9 < num; num9++)
		{
			long aUniversalTime11 = aReader.ReadLong();
			short aBoosterKind = aReader.ReadShort();
			short aNum3 = aReader.ReadShort();
			short aSeriese2 = aReader.ReadShort();
			short aCondition2 = aReader.ReadShort();
			userBehavior._addUseBoosterRecord(aUniversalTime11, aBoosterKind, aNum3, aSeriese2, aCondition2);
		}
		userBehavior.mPlayList = new List<PlayRecord>();
		num = aReader.ReadInt();
		for (int num10 = 0; num10 < num; num10++)
		{
			long aUniversalTime12 = aReader.ReadLong();
			short aSeriese3 = aReader.ReadShort();
			short aCondition3 = aReader.ReadShort();
			bool aWin = aReader.ReadBool();
			short aStar = aReader.ReadShort();
			short aMakeSpecial = aReader.ReadShort();
			short aSwapSpecial = aReader.ReadShort();
			userBehavior._addPlayRecord(aUniversalTime12, aSeriese3, aCondition3, aWin, aStar, aMakeSpecial, aSwapSpecial);
		}
		userBehavior.mStripeCountHistory = new List<int>();
		num = aReader.ReadInt();
		for (int num11 = 0; num11 < num; num11++)
		{
			int item = aReader.ReadInt();
			userBehavior.mStripeCountHistory.Add(item);
		}
		userBehavior.mMode7Id = new Dictionary<Def.SERIES, object>();
		num = aReader.ReadInt();
		for (int num12 = 0; num12 < num; num12++)
		{
			short key = aReader.ReadShort();
			int num13 = aReader.ReadInt();
			userBehavior.mMode7Id.Add((Def.SERIES)key, num13);
		}
		userBehavior.mMode7RemainCount = new Dictionary<Def.SERIES, object>();
		num = aReader.ReadInt();
		for (int num14 = 0; num14 < num; num14++)
		{
			short key2 = aReader.ReadShort();
			int num15 = aReader.ReadInt();
			userBehavior.mMode7RemainCount.Add((Def.SERIES)key2, num15);
		}
		userBehavior.mMode7LastTime = new Dictionary<Def.SERIES, object>();
		num = aReader.ReadInt();
		for (int num16 = 0; num16 < num; num16++)
		{
			short key3 = aReader.ReadShort();
			long num17 = aReader.ReadLong();
			DateTime dateTime;
			try
			{
				dateTime = DateTimeUtil.UnixTimeStampToDateTime(num17).ToLocalTime();
			}
			catch
			{
				dateTime = default(DateTime);
			}
			userBehavior.mMode7LastTime.Add((Def.SERIES)key3, dateTime);
		}
		return userBehavior;
	}

	private void Serialize(BIJBinaryWriter aWriter)
	{
		aWriter.WriteShort(1290);
		aWriter.WriteInt(mPurchaseList.Count);
		for (int i = 0; i < mPurchaseList.Count; i++)
		{
			aWriter.WriteLong(mPurchaseList[i].mTime);
			aWriter.WriteShort(mPurchaseList[i].mIpaID);
			aWriter.WriteShort(mPurchaseList[i].mCampaignId);
			aWriter.WriteShort(mPurchaseList[i].mCampaignGid);
			aWriter.WriteShort(mPurchaseList[i].mPrice);
		}
		aWriter.WriteInt(mUseGemCnt);
		aWriter.WriteInt(mUseGemAmount);
		aWriter.WriteShort(mUseGemMax);
		aWriter.WriteInt(mUseGemList.Count);
		for (int j = 0; j < mUseGemList.Count; j++)
		{
			aWriter.WriteLong(mUseGemList[j].mTime);
			aWriter.WriteShort(mUseGemList[j].mGem);
		}
		aWriter.WriteInt(mBuyBoosterList.Count);
		for (int k = 0; k < mBuyBoosterList.Count; k++)
		{
			aWriter.WriteLong(mBuyBoosterList[k].mTime);
			aWriter.WriteShort(mBuyBoosterList[k].mShopID);
			aWriter.WriteShort(mBuyBoosterList[k].mGem);
		}
		aWriter.WriteInt(mBuyContinueList.Count);
		for (int l = 0; l < mBuyContinueList.Count; l++)
		{
			aWriter.WriteLong(mBuyContinueList[l].mTime);
			aWriter.WriteShort(mBuyContinueList[l].mNum);
			aWriter.WriteShort(mBuyContinueList[l].mSeriese);
			aWriter.WriteShort(mBuyContinueList[l].mStageCondition);
		}
		aWriter.WriteInt(mBuyHeartList.Count);
		for (int m = 0; m < mBuyHeartList.Count; m++)
		{
			aWriter.WriteLong(mBuyHeartList[m].mTime);
		}
		aWriter.WriteInt(mBuyMHeartList.Count);
		for (int n = 0; n < mBuyMHeartList.Count; n++)
		{
			aWriter.WriteLong(mBuyMHeartList[n].mTime);
		}
		aWriter.WriteInt(mBuySkipList.Count);
		for (int num = 0; num < mBuySkipList.Count; num++)
		{
			aWriter.WriteLong(mBuySkipList[num].mTime);
		}
		aWriter.WriteInt(mChanceDict.Count);
		foreach (ChanceRecord value in mChanceDict.Values)
		{
			aWriter.WriteShort(value.mType);
			aWriter.WriteInt(value.mOfferCnt);
			aWriter.WriteInt(value.mBuyCnt);
			aWriter.WriteInt(value.mList.Count);
			for (int num2 = 0; num2 < value.mList.Count; num2++)
			{
				aWriter.WriteLong(value.mList[num2].mTime);
			}
		}
		aWriter.WriteInt(mBuyAdvGasyaList.Count);
		for (int num3 = 0; num3 < mBuyAdvGasyaList.Count; num3++)
		{
			aWriter.WriteLong(mBuyAdvGasyaList[num3].mTime);
			aWriter.WriteShort(mBuyAdvGasyaList[num3].mNum);
			aWriter.WriteShort(mBuyAdvGasyaList[num3].mSale);
			aWriter.WriteShort(mBuyAdvGasyaList[num3].mGem);
		}
		aWriter.WriteInt(mBuyAdvMedalList.Count);
		for (int num4 = 0; num4 < mBuyAdvMedalList.Count; num4++)
		{
			aWriter.WriteLong(mBuyAdvMedalList[num4].mTime);
		}
		aWriter.WriteInt(mBuyAdvContinueList.Count);
		for (int num5 = 0; num5 < mBuyAdvContinueList.Count; num5++)
		{
			aWriter.WriteLong(mBuyAdvContinueList[num5].mTime);
		}
		aWriter.WriteInt(mUseBoosterList.Count);
		for (int num6 = 0; num6 < mUseBoosterList.Count; num6++)
		{
			aWriter.WriteLong(mUseBoosterList[num6].mTime);
			aWriter.WriteShort(mUseBoosterList[num6].mKind);
			aWriter.WriteShort(mUseBoosterList[num6].mNum);
			aWriter.WriteShort(mUseBoosterList[num6].mSeriese);
			aWriter.WriteShort(mUseBoosterList[num6].mStageCondition);
		}
		aWriter.WriteInt(mPlayList.Count);
		for (int num7 = 0; num7 < mPlayList.Count; num7++)
		{
			aWriter.WriteLong(mPlayList[num7].mTime);
			aWriter.WriteShort(mPlayList[num7].mSeriese);
			aWriter.WriteShort(mPlayList[num7].mStageCondition);
			aWriter.WriteBool(mPlayList[num7].mWin);
			aWriter.WriteShort(mPlayList[num7].mStar);
			aWriter.WriteShort(mPlayList[num7].mMakeSpecial);
			aWriter.WriteShort(mPlayList[num7].mSwapSpecial);
		}
		aWriter.WriteInt(mStripeCountHistory.Count);
		for (int num8 = 0; num8 < mStripeCountHistory.Count; num8++)
		{
			aWriter.WriteInt(mStripeCountHistory[num8]);
		}
		aWriter.WriteInt(mMode7Id.Count);
		foreach (KeyValuePair<Def.SERIES, object> item in mMode7Id)
		{
			aWriter.WriteShort((short)item.Key);
			aWriter.WriteInt((int)item.Value);
		}
		aWriter.WriteInt(mMode7RemainCount.Count);
		foreach (KeyValuePair<Def.SERIES, object> item2 in mMode7RemainCount)
		{
			aWriter.WriteShort((short)item2.Key);
			aWriter.WriteInt((int)item2.Value);
		}
		aWriter.WriteInt(mMode7LastTime.Count);
		foreach (KeyValuePair<Def.SERIES, object> item3 in mMode7LastTime)
		{
			aWriter.WriteShort((short)item3.Key);
			long v = (long)DateTimeUtil.DateTimeToUnixTimeStamp(((DateTime)item3.Value).ToUniversalTime());
			aWriter.WriteLong(v);
		}
	}

	public void AddPurchase(short aIapID, short aCampaignId = -1, short aCampaignGid = -1)
	{
		long aUniversalTime = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		short aPrice = 0;
		if (aIapID < mPriceList.Length)
		{
			aPrice = mPriceList[aIapID];
		}
		_addPurchaseRecord(aUniversalTime, aIapID, aCampaignId, aCampaignGid, aPrice);
	}

	private void _addPurchaseRecord(long aUniversalTime, short aIapID, short aCampaignId, short aCampaignGid, short aPrice)
	{
		PurchaseRecord purchaseRecord = new PurchaseRecord();
		purchaseRecord.mTime = aUniversalTime;
		purchaseRecord.mIpaID = aIapID;
		purchaseRecord.mCampaignId = aCampaignId;
		purchaseRecord.mCampaignGid = aCampaignGid;
		purchaseRecord.mPrice = aPrice;
		mPurchaseList.Add(purchaseRecord);
	}

	public int GetPurchaseAmount(bool aAll = true, bool aCampaign = false)
	{
		return GetPurchaseAmount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, aAll, aCampaign);
	}

	public int GetPurchaseAmount(DateTime aStartTime, DateTime aEndTime, bool aAll = true, bool aCampaign = false)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetPurchaseAmount(aStartTime2, aEndTime2, aAll, aCampaign);
	}

	public int GetPurchaseAmount(long aStartTime, long aEndTime, bool aAll = true, bool aCampaign = false)
	{
		int num = 0;
		for (int num2 = mPurchaseList.Count - 1; num2 >= 0; num2--)
		{
			PurchaseRecord purchaseRecord = mPurchaseList[num2];
			if (purchaseRecord.mTime < aStartTime || purchaseRecord.mTime > aEndTime)
			{
				break;
			}
			if (aAll)
			{
				num += purchaseRecord.mPrice;
			}
			else if (!aCampaign)
			{
				if (purchaseRecord.IsNormal())
				{
					num += purchaseRecord.mPrice;
				}
			}
			else if (purchaseRecord.IsCampaign())
			{
				num += purchaseRecord.mPrice;
			}
		}
		return num;
	}

	public short GetPurchaseMax(bool aAll = true, bool aCampaign = false)
	{
		return GetPurchaseMax(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, aAll, aCampaign);
	}

	public short GetPurchaseMax(DateTime aStartTime, DateTime aEndTime, bool aAll = true, bool aCampaign = false)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetPurchaseMax(aStartTime2, aEndTime2, aAll, aCampaign);
	}

	public short GetPurchaseMax(long aStartTime, long aEndTime, bool aAll = true, bool aCampaign = false)
	{
		short num = 0;
		for (int num2 = mPurchaseList.Count - 1; num2 >= 0; num2--)
		{
			PurchaseRecord purchaseRecord = mPurchaseList[num2];
			if (purchaseRecord.mTime < aStartTime || purchaseRecord.mTime > aEndTime)
			{
				break;
			}
			if (purchaseRecord.mPrice > num)
			{
				if (aAll)
				{
					num = purchaseRecord.mPrice;
				}
				else if (!aCampaign)
				{
					if (purchaseRecord.IsNormal())
					{
						num = purchaseRecord.mPrice;
					}
				}
				else if (purchaseRecord.IsCampaign())
				{
					num = purchaseRecord.mPrice;
				}
			}
		}
		return num;
	}

	public int GetPurchaseCount(bool aAll = true, bool aCampaign = false)
	{
		if (aAll)
		{
			return mPurchaseList.Count;
		}
		return GetPurchaseCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, false, aCampaign);
	}

	public int GetPurchaseCount(DateTime aStartTime, DateTime aEndTime, bool aAll = true, bool aCampaign = false)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetPurchaseCount(aStartTime2, aEndTime2, aAll, aCampaign);
	}

	public int GetPurchaseCount(long aStartTime, long aEndTime, bool aAll = true, bool aCampaign = false)
	{
		int num = 0;
		for (int num2 = mPurchaseList.Count - 1; num2 >= 0; num2--)
		{
			PurchaseRecord purchaseRecord = mPurchaseList[num2];
			if (purchaseRecord.mTime < aStartTime || purchaseRecord.mTime > aEndTime)
			{
				break;
			}
			if (aAll)
			{
				num++;
			}
			else if (!aCampaign)
			{
				if (purchaseRecord.IsNormal())
				{
					num++;
				}
			}
			else if (purchaseRecord.IsCampaign())
			{
				num++;
			}
		}
		return num;
	}

	public void ConvertOptionPurchaseHistory(Options aOption)
	{
		for (int i = 0; i < aOption.PurchaseList.Count; i++)
		{
			Options.PurchaseHistory purchaseHistory = aOption.PurchaseList[i];
			short num = (short)purchaseHistory.Price;
			short num2 = -1;
			bool flag = false;
			bool flag2 = false;
			for (short num3 = 0; num3 < mPriceList.Length; num3++)
			{
				if (num3 != 9 && num == mPriceList[num3])
				{
					if (num2 == -1)
					{
						num2 = num3;
					}
					switch ((PItem.KIND)num3)
					{
					case PItem.KIND.SD0:
					case PItem.KIND.SD1:
					case PItem.KIND.SD2:
					case PItem.KIND.SD3:
					case PItem.KIND.SD4:
					case PItem.KIND.SD5:
					case PItem.KIND.SD6:
						flag = true;
						break;
					default:
						flag2 = true;
						break;
					}
				}
			}
			if (num2 != -1)
			{
				short aCampaignId = -1;
				short aCampaignGid = -1;
				if (flag && flag2)
				{
					num2 = -99;
				}
				else if (flag2)
				{
					aCampaignId = -99;
					aCampaignGid = -99;
				}
				_addPurchaseRecord(purchaseHistory.Time, num2, aCampaignId, aCampaignGid, num);
			}
		}
	}

	private void _addUseGem(long aTime, short aGem)
	{
		UseGemRecord useGemRecord = new UseGemRecord();
		useGemRecord.mTime = aTime;
		useGemRecord.mGem = aGem;
		mUseGemList.Add(useGemRecord);
		if (mUseGemList.Count > 200)
		{
			mUseGemList.RemoveAt(0);
		}
	}

	private void _useGem(long aTime, short aGem)
	{
		mUseGemCnt++;
		mUseGemAmount += aGem;
		if (aGem > mUseGemMax)
		{
			mUseGemMax = aGem;
		}
		_addUseGem(aTime, aGem);
	}

	public int GetUseGemAmount()
	{
		return mUseGemAmount;
	}

	public int GetUseGemAmount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetUseGemAmount(aStartTime2, aEndTime2);
	}

	public int GetUseGemAmount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mUseGemList.Count - 1;
		while (num2 >= 0)
		{
			UseGemRecord useGemRecord = mUseGemList[num2];
			if (useGemRecord.mTime >= aStartTime && useGemRecord.mTime <= aEndTime)
			{
				num += useGemRecord.mGem;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public short GetUseGemMax()
	{
		return mUseGemMax;
	}

	public short GetUseGemMax(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetUseGemMax(aStartTime2, aEndTime2);
	}

	public short GetUseGemMax(long aStartTime, long aEndTime)
	{
		short num = 0;
		int num2 = mUseGemList.Count - 1;
		while (num2 >= 0)
		{
			UseGemRecord useGemRecord = mUseGemList[num2];
			if (useGemRecord.mTime >= aStartTime && useGemRecord.mTime <= aEndTime)
			{
				if (useGemRecord.mGem > num)
				{
					num = useGemRecord.mGem;
				}
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public int GetUseGemCount()
	{
		return mUseGemCnt;
	}

	public int GetUseGemCount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetUseGemCount(aStartTime2, aEndTime2);
	}

	public int GetUseGemCount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mUseGemList.Count - 1;
		while (num2 >= 0)
		{
			UseGemRecord useGemRecord = mUseGemList[num2];
			if (useGemRecord.mTime >= aStartTime && useGemRecord.mTime <= aEndTime)
			{
				num++;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public void BuyBooster(short aShopID, short aGem)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuyBoosterRecord(num, aShopID, aGem);
	}

	private void _addBuyBoosterRecord(long aUniversalTime, short aShopID, short aGem)
	{
		BuyBoosterRecord buyBoosterRecord = new BuyBoosterRecord();
		buyBoosterRecord.mTime = aUniversalTime;
		buyBoosterRecord.mShopID = aShopID;
		buyBoosterRecord.mGem = aGem;
		mBuyBoosterList.Add(buyBoosterRecord);
	}

	public int GetBuyBoostCount()
	{
		return mBuyBoosterList.Count;
	}

	public int GetBuyBoostCount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyBoostCount(aStartTime2, aEndTime2);
	}

	public int GetBuyBoostCount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mBuyBoosterList.Count - 1;
		while (num2 >= 0)
		{
			BuyBoosterRecord buyBoosterRecord = mBuyBoosterList[num2];
			if (buyBoosterRecord.mTime >= aStartTime && buyBoosterRecord.mTime <= aEndTime)
			{
				num++;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public short GetBuyBoosterGemMax()
	{
		return GetBuyBoosterGemMax(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public short GetBuyBoosterGemMax(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyBoosterGemMax(aStartTime2, aEndTime2);
	}

	public short GetBuyBoosterGemMax(long aStartTime, long aEndTime)
	{
		short num = 0;
		int num2 = mBuyBoosterList.Count - 1;
		while (num2 >= 0)
		{
			BuyBoosterRecord buyBoosterRecord = mBuyBoosterList[num2];
			if (buyBoosterRecord.mTime >= aStartTime && buyBoosterRecord.mTime <= aEndTime)
			{
				if (buyBoosterRecord.mGem > num)
				{
					num = buyBoosterRecord.mGem;
				}
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public void UseBooster(Def.BOOSTER_KIND aBoosterKind, short aNum, Def.SERIES aSeriese, Def.STAGE_WIN_CONDITION aCondition)
	{
		long aUniversalTime = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_addUseBoosterRecord(aUniversalTime, aNum, (short)aBoosterKind, (short)aSeriese, (short)aCondition);
	}

	private void _addUseBoosterRecord(long aUniversalTime, short aBoosterKind, short aNum, short aSeriese, short aCondition)
	{
		UseBoosterRecord useBoosterRecord = new UseBoosterRecord();
		useBoosterRecord.mTime = aUniversalTime;
		useBoosterRecord.mKind = aBoosterKind;
		useBoosterRecord.mNum = aNum;
		useBoosterRecord.mSeriese = aSeriese;
		useBoosterRecord.mStageCondition = aCondition;
		mUseBoosterList.Add(useBoosterRecord);
		if (mUseBoosterList.Count > 200)
		{
			mUseBoosterList.RemoveAt(0);
		}
	}

	public int GetBoosterUseCount(short aBoosterKind)
	{
		return GetBoosterUseCount(aBoosterKind, DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public int GetBoosterUseCount(short aBoosterKind, DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBoosterUseCount(aBoosterKind, aStartTime2, aEndTime2);
	}

	public int GetBoosterUseCount(short aBoosterKind, long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mUseBoosterList.Count - 1;
		while (num2 >= 0)
		{
			UseBoosterRecord useBoosterRecord = mUseBoosterList[num2];
			if (useBoosterRecord.mTime >= aStartTime && useBoosterRecord.mTime <= aEndTime)
			{
				if (aBoosterKind == useBoosterRecord.mKind)
				{
					num += useBoosterRecord.mNum;
				}
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public int[] GetUseBoosterKindArray()
	{
		return GetUseBoosterKindArray(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public int[] GetUseBoosterKindArray(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetUseBoosterKindArray(aStartTime2, aEndTime2);
	}

	public int[] GetUseBoosterKindArray(long aStartTime, long aEndTime)
	{
		int[] array = new int[15];
		int num = mUseBoosterList.Count - 1;
		while (num >= 0)
		{
			UseBoosterRecord useBoosterRecord = mUseBoosterList[num];
			if (useBoosterRecord.mTime >= aStartTime && useBoosterRecord.mTime <= aEndTime)
			{
				array[useBoosterRecord.mKind] += useBoosterRecord.mNum;
				num--;
				continue;
			}
			break;
		}
		List<_UseBooster> list = new List<_UseBooster>();
		for (int i = 1; i < array.Length; i++)
		{
			if (array[i] > 0)
			{
				_UseBooster useBooster = new _UseBooster();
				useBooster.mKind = i;
				useBooster.mCnt = array[i];
				list.Add(useBooster);
			}
		}
		list.Sort((_UseBooster a, _UseBooster b) => b.mCnt - a.mCnt);
		int[] array2 = new int[list.Count];
		for (int j = 0; j < list.Count; j++)
		{
			array2[j] = list[j].mKind;
		}
		return array2;
	}

	public void BuyContinue(short aNum, short aGem, Def.SERIES aSeriese, Def.STAGE_WIN_CONDITION aCondition)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuyContinueRecord(num, aNum, (short)aSeriese, (short)aCondition);
	}

	private void _addBuyContinueRecord(long aUniversalTime, short aNum, short aSeriese, short aCondition, bool aAdd = false)
	{
		BuyContinueRecord buyContinueRecord = new BuyContinueRecord();
		buyContinueRecord.mTime = aUniversalTime;
		buyContinueRecord.mNum = aNum;
		buyContinueRecord.mSeriese = aSeriese;
		buyContinueRecord.mStageCondition = aCondition;
		BuyContinueRecord buyContinueRecord2 = null;
		if (!aAdd && mBuyContinueList.Count > 0)
		{
			buyContinueRecord2 = mBuyContinueList[mBuyContinueList.Count - 1];
		}
		if (buyContinueRecord2 != null && aNum > 1 && aSeriese == buyContinueRecord2.mSeriese && aCondition == buyContinueRecord2.mStageCondition)
		{
			mBuyContinueList[mBuyContinueList.Count - 1] = buyContinueRecord;
		}
		else
		{
			mBuyContinueList.Add(buyContinueRecord);
		}
		if (aNum > _ContinueMax)
		{
			_ContinueMax = aNum;
		}
		if (mBuyContinueList.Count > 200)
		{
			mBuyContinueList.RemoveAt(0);
		}
	}

	public int GetBuyContinueCount(Def.SERIES aSeriese = Def.SERIES.SM_ALL)
	{
		return GetBuyContinueCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, aSeriese);
	}

	public int GetBuyContinueCount(DateTime aStartTime, DateTime aEndTime, Def.SERIES aSeriese = Def.SERIES.SM_ALL)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyContinueCount(aStartTime2, aEndTime2, aSeriese);
	}

	public int GetBuyContinueCount(long aStartTime, long aEndTime, Def.SERIES aSeriese = Def.SERIES.SM_ALL)
	{
		int num = 0;
		int num2 = mBuyContinueList.Count - 1;
		while (num2 >= 0)
		{
			BuyContinueRecord buyContinueRecord = mBuyContinueList[num2];
			if (buyContinueRecord.mTime >= aStartTime && buyContinueRecord.mTime <= aEndTime)
			{
				if (aSeriese == Def.SERIES.SM_ALL)
				{
					num += buyContinueRecord.mNum;
				}
				else if (buyContinueRecord.mSeriese == (short)aSeriese)
				{
					num += buyContinueRecord.mNum;
				}
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public short GetBuyContinueMax(Def.SERIES aSeriese = Def.SERIES.SM_ALL)
	{
		return GetBuyContinueMax(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, aSeriese);
	}

	public short GetBuyContinueMax(DateTime aStartTime, DateTime aEndTime, Def.SERIES aSeriese = Def.SERIES.SM_ALL)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyContinueMax(aStartTime2, aEndTime2, aSeriese);
	}

	public short GetBuyContinueMax(long aStartTime, long aEndTime, Def.SERIES aSeriese = Def.SERIES.SM_ALL)
	{
		short num = 0;
		for (int num2 = mBuyContinueList.Count - 1; num2 >= 0; num2--)
		{
			BuyContinueRecord buyContinueRecord = mBuyContinueList[num2];
			if (buyContinueRecord.mTime < aStartTime || buyContinueRecord.mTime > aEndTime)
			{
				break;
			}
			if (buyContinueRecord.mNum > num)
			{
				if (aSeriese == Def.SERIES.SM_ALL)
				{
					num = buyContinueRecord.mNum;
				}
				else if (buyContinueRecord.mSeriese == (short)aSeriese)
				{
					num = buyContinueRecord.mNum;
				}
			}
		}
		return num;
	}

	public void BuyHeart(short aGem)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuyHeartRecord(num);
	}

	private void _addBuyHeartRecord(long aUniversalTime)
	{
		BuyHeartRecord buyHeartRecord = new BuyHeartRecord();
		buyHeartRecord.mTime = aUniversalTime;
		mBuyHeartList.Add(buyHeartRecord);
		if (mBuyHeartList.Count > 200)
		{
			mBuyHeartList.RemoveAt(0);
		}
	}

	public int GetBuyHeartCount()
	{
		return GetBuyHeartCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public int GetBuyHeartCount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyHeartCount(aStartTime2, aEndTime2);
	}

	public int GetBuyHeartCount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mBuyHeartList.Count - 1;
		while (num2 >= 0)
		{
			BuyHeartRecord buyHeartRecord = mBuyHeartList[num2];
			if (buyHeartRecord.mTime >= aStartTime && buyHeartRecord.mTime <= aEndTime)
			{
				num++;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public void BuyMugenHeart(short aGem)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuyMugenHeartRecord(num);
	}

	private void _addBuyMugenHeartRecord(long aUniversalTime)
	{
		BuyMugenHeartRecord buyMugenHeartRecord = new BuyMugenHeartRecord();
		buyMugenHeartRecord.mTime = aUniversalTime;
		mBuyMHeartList.Add(buyMugenHeartRecord);
		if (mBuyMHeartList.Count > 200)
		{
			mBuyMHeartList.RemoveAt(0);
		}
	}

	public int GetBuyMugenHeartCount()
	{
		return GetBuyMugenHeartCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public int GetBuyMugenHeartCount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyMugenHeartCount(aStartTime2, aEndTime2);
	}

	public int GetBuyMugenHeartCount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mBuyMHeartList.Count - 1;
		while (num2 >= 0)
		{
			BuyMugenHeartRecord buyMugenHeartRecord = mBuyMHeartList[num2];
			if (buyMugenHeartRecord.mTime >= aStartTime && buyMugenHeartRecord.mTime <= aEndTime)
			{
				num++;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public void BuySkip(short aGem)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuySkipRecord(num);
	}

	private void _addBuySkipRecord(long aUniversalTime)
	{
		BuySkipRecord buySkipRecord = new BuySkipRecord();
		buySkipRecord.mTime = aUniversalTime;
		mBuySkipList.Add(buySkipRecord);
		if (mBuySkipList.Count > 200)
		{
			mBuySkipList.RemoveAt(0);
		}
	}

	public int GetBuySkipCount()
	{
		return GetBuySkipCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public int GetBuySkipCount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuySkipCount(aStartTime2, aEndTime2);
	}

	public int GetBuySkipCount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mBuySkipList.Count - 1;
		while (num2 >= 0)
		{
			BuySkipRecord buySkipRecord = mBuySkipList[num2];
			if (buySkipRecord.mTime >= aStartTime && buySkipRecord.mTime <= aEndTime)
			{
				num++;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public void CountSpecialChance(short aChanceType, bool aBuy, short aGem)
	{
		ChanceRecord chanceRecord = null;
		if (mChanceDict.ContainsKey(aChanceType))
		{
			chanceRecord = mChanceDict[aChanceType];
		}
		else
		{
			chanceRecord = new ChanceRecord();
			chanceRecord.mType = aChanceType;
			chanceRecord.mOfferCnt = 0;
			chanceRecord.mBuyCnt = 0;
			chanceRecord.mList = new List<BuyChanceRecord>();
			mChanceDict.Add(aChanceType, chanceRecord);
		}
		chanceRecord.mOfferCnt++;
		if (aBuy)
		{
			long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
			chanceRecord.mBuyCnt++;
			_addSpecialChanceRecord(chanceRecord, num);
			_useGem(num, aGem);
		}
	}

	private void _addSpecialChanceRecord(ChanceRecord aRecord, long aUniversalTime)
	{
		BuyChanceRecord buyChanceRecord = new BuyChanceRecord();
		buyChanceRecord.mTime = aUniversalTime;
		aRecord.mList.Add(buyChanceRecord);
		if (aRecord.mList.Count > 200)
		{
			aRecord.mList.RemoveAt(0);
		}
	}

	public int GetBuySpecialChanceCount(short aChanceType)
	{
		if (mChanceDict.ContainsKey(aChanceType))
		{
			return mChanceDict[aChanceType].mBuyCnt;
		}
		return 0;
	}

	public int GetBuySpecialChanceCount(short aChanceType, DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuySpecialChanceCount(aChanceType, aStartTime2, aEndTime2);
	}

	public int GetBuySpecialChanceCount(short aChanceType, long aStartTime, long aEndTime)
	{
		int num = 0;
		if (mChanceDict.ContainsKey(aChanceType))
		{
			int num2 = mChanceDict[aChanceType].mList.Count - 1;
			while (num2 >= 0)
			{
				BuyChanceRecord buyChanceRecord = mChanceDict[aChanceType].mList[num2];
				if (buyChanceRecord.mTime >= aStartTime && buyChanceRecord.mTime <= aEndTime)
				{
					num++;
					num2--;
					continue;
				}
				break;
			}
		}
		return num;
	}

	public void BuyAdvGasya(short aNum, short aSale, short aGem)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuyAdvGasyaRecord(num, aNum, aSale, aGem);
	}

	private void _addBuyAdvGasyaRecord(long aUniversalTime, short aNum, short aSale, short aGem)
	{
		BuyAdvGasyaRecord buyAdvGasyaRecord = new BuyAdvGasyaRecord();
		buyAdvGasyaRecord.mTime = aUniversalTime;
		buyAdvGasyaRecord.mNum = aNum;
		buyAdvGasyaRecord.mSale = aSale;
		buyAdvGasyaRecord.mGem = aGem;
		mBuyAdvGasyaList.Add(buyAdvGasyaRecord);
		if (mBuyAdvGasyaList.Count > 200)
		{
			mBuyAdvGasyaList.RemoveAt(0);
		}
	}

	public int GetAdvGasyaGemAmount(bool aAll = true, bool aSale = false)
	{
		return GetAdvGasyaGemAmount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, aAll, aSale);
	}

	public int GetAdvGasyaGemAmount(DateTime aStartTime, DateTime aEndTime, bool aAll = true, bool aSale = false)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetAdvGasyaGemAmount(aStartTime2, aEndTime2, aAll, aSale);
	}

	public int GetAdvGasyaGemAmount(long aStartTime, long aEndTime, bool aAll = true, bool aSale = false)
	{
		int num = 0;
		for (int num2 = mBuyAdvGasyaList.Count - 1; num2 >= 0; num2--)
		{
			BuyAdvGasyaRecord buyAdvGasyaRecord = mBuyAdvGasyaList[num2];
			if (buyAdvGasyaRecord.mTime < aStartTime || buyAdvGasyaRecord.mTime > aEndTime)
			{
				break;
			}
			if (aAll)
			{
				num += buyAdvGasyaRecord.mGem;
			}
			else if (!aSale)
			{
				if (buyAdvGasyaRecord.mSale == 0)
				{
					num += buyAdvGasyaRecord.mGem;
				}
			}
			else if (buyAdvGasyaRecord.mSale > 0)
			{
				num += buyAdvGasyaRecord.mGem;
			}
		}
		return num;
	}

	public int GetBuyAdvGasyaCount(bool aAll = true, bool aSale = false)
	{
		return GetBuyAdvGasyaCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, aAll, aSale);
	}

	public int GetBuyAdvGasyaCount(DateTime aStartTime, DateTime aEndTime, bool aAll = true, bool aSale = false)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyAdvGasyaCount(aStartTime2, aEndTime2, aAll, aSale);
	}

	public int GetBuyAdvGasyaCount(long aStartTime, long aEndTime, bool aAll = true, bool aSale = false)
	{
		int num = 0;
		for (int num2 = mBuyAdvGasyaList.Count - 1; num2 >= 0; num2--)
		{
			BuyAdvGasyaRecord buyAdvGasyaRecord = mBuyAdvGasyaList[num2];
			if (buyAdvGasyaRecord.mTime < aStartTime || buyAdvGasyaRecord.mTime > aEndTime)
			{
				break;
			}
			if (aAll)
			{
				num++;
			}
			else if (!aSale)
			{
				if (buyAdvGasyaRecord.mSale == 0)
				{
					num++;
				}
			}
			else if (buyAdvGasyaRecord.mSale > 0)
			{
				num++;
			}
		}
		return num;
	}

	public int GetBuyAdvGasyaGemMax(bool aAll = true, bool aSale = false)
	{
		return GetBuyAdvGasyaGemMax(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now, aAll, aSale);
	}

	public int GetBuyAdvGasyaGemMax(DateTime aStartTime, DateTime aEndTime, bool aAll = true, bool aSale = false)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyAdvGasyaGemMax(aStartTime2, aEndTime2, aAll, aSale);
	}

	public int GetBuyAdvGasyaGemMax(long aStartTime, long aEndTime, bool aAll = true, bool aSale = false)
	{
		int num = 0;
		for (int num2 = mBuyAdvGasyaList.Count - 1; num2 >= 0; num2--)
		{
			BuyAdvGasyaRecord buyAdvGasyaRecord = mBuyAdvGasyaList[num2];
			if (buyAdvGasyaRecord.mTime < aStartTime || buyAdvGasyaRecord.mTime > aEndTime)
			{
				break;
			}
			if (buyAdvGasyaRecord.mGem > num)
			{
				if (aAll)
				{
					num = buyAdvGasyaRecord.mGem;
				}
				else if (!aSale)
				{
					if (buyAdvGasyaRecord.mSale == 0)
					{
						num = buyAdvGasyaRecord.mGem;
					}
				}
				else if (buyAdvGasyaRecord.mSale > 0)
				{
					num = buyAdvGasyaRecord.mGem;
				}
			}
		}
		return num;
	}

	public void BuyAdvMedal(short aGem)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuyAdvMedalRecord(num);
	}

	private void _addBuyAdvMedalRecord(long aUniversalTime)
	{
		BuyAdvMedalRecord buyAdvMedalRecord = new BuyAdvMedalRecord();
		buyAdvMedalRecord.mTime = aUniversalTime;
		mBuyAdvMedalList.Add(buyAdvMedalRecord);
		if (mBuyAdvMedalList.Count > 200)
		{
			mBuyAdvMedalList.RemoveAt(0);
		}
	}

	public int GetBuyAdvMedalCount()
	{
		return GetBuyAdvMedalCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public int GetBuyAdvMedalCount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyAdvMedalCount(aStartTime2, aEndTime2);
	}

	public int GetBuyAdvMedalCount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mBuyAdvMedalList.Count - 1;
		while (num2 >= 0)
		{
			BuyAdvMedalRecord buyAdvMedalRecord = mBuyAdvMedalList[num2];
			if (buyAdvMedalRecord.mTime >= aStartTime && buyAdvMedalRecord.mTime <= aEndTime)
			{
				num++;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public void BuyAdvContinue(short aGem)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_useGem(num, aGem);
		_addBuyAdvContinueRecord(num);
	}

	private void _addBuyAdvContinueRecord(long aUniversalTime)
	{
		BuyAdvContinueRecord buyAdvContinueRecord = new BuyAdvContinueRecord();
		buyAdvContinueRecord.mTime = aUniversalTime;
		mBuyAdvContinueList.Add(buyAdvContinueRecord);
		if (mBuyAdvContinueList.Count > 200)
		{
			mBuyAdvContinueList.RemoveAt(0);
		}
	}

	public int GetBuyAdvContinueCount()
	{
		return GetBuyAdvContinueCount(DateTimeUtil.UnitLocalTimeEpoch, DateTime.Now);
	}

	public int GetBuyAdvContinueCount(DateTime aStartTime, DateTime aEndTime)
	{
		long aStartTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aStartTime);
		long aEndTime2 = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aEndTime);
		return GetBuyAdvContinueCount(aStartTime2, aEndTime2);
	}

	public int GetBuyAdvContinueCount(long aStartTime, long aEndTime)
	{
		int num = 0;
		int num2 = mBuyAdvContinueList.Count - 1;
		while (num2 >= 0)
		{
			BuyAdvContinueRecord buyAdvContinueRecord = mBuyAdvContinueList[num2];
			if (buyAdvContinueRecord.mTime >= aStartTime && buyAdvContinueRecord.mTime <= aEndTime)
			{
				num++;
				num2--;
				continue;
			}
			break;
		}
		return num;
	}

	public void Play(Def.SERIES aSeriese, Def.STAGE_WIN_CONDITION aCondition, bool aWin, short aStar, short aMakeSpecial, short aSwapSpecial)
	{
		long aUniversalTime = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		_addPlayRecord(aUniversalTime, (short)aSeriese, (short)aCondition, aWin, aStar, aMakeSpecial, aSwapSpecial);
	}

	private void _addPlayRecord(long aUniversalTime, short aSeriese, short aCondition, bool aWin, short aStar, short aMakeSpecial, short aSwapSpecial)
	{
		PlayRecord playRecord = new PlayRecord();
		playRecord.mTime = aUniversalTime;
		playRecord.mSeriese = aSeriese;
		playRecord.mStageCondition = aCondition;
		playRecord.mWin = aWin;
		playRecord.mStar = aStar;
		playRecord.mMakeSpecial = aMakeSpecial;
		playRecord.mSwapSpecial = aSwapSpecial;
		mPlayList.Add(playRecord);
		if (mPlayList.Count > 200)
		{
			mPlayList.RemoveAt(0);
		}
	}
}
