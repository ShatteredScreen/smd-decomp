using System;
using System.Collections.Generic;
using System.IO;

public class BIJDLDataInfo
{
	public Dictionary<string, BIJDLData> DataDict = new Dictionary<string, BIJDLData>();

	public string Filename;

	public int Version;

	public string BaseURL;

	public int Count
	{
		get
		{
			return DataDict.Count;
		}
	}

	public BIJDLDataInfo()
	{
		Filename = null;
		BaseURL = string.Empty;
	}

	public BIJDLDataInfo(string a_filename, int a_version)
	{
		Filename = a_filename;
		Version = a_version;
	}

	public bool Save()
	{
		if (Filename == null)
		{
			throw new Exception("no filename");
		}
		try
		{
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(Filename))
			{
				bIJEncryptDataWriter.WriteInt(Version);
				bIJEncryptDataWriter.WriteUTF(BaseURL);
				List<BIJDLData> list = new List<BIJDLData>(DataDict.Values);
				int count = list.Count;
				bIJEncryptDataWriter.WriteInt(count);
				for (int i = 0; i < count; i++)
				{
					list[i].SerializeToLocalFile(bIJEncryptDataWriter, Version);
				}
			}
		}
		catch (Exception)
		{
			return false;
		}
		return true;
	}

	public bool Load(bool isOldFormat, string fileName)
	{
		if (string.IsNullOrEmpty(fileName))
		{
			fileName = Filename;
		}
		if (fileName == null)
		{
			throw new Exception("no filename");
		}
		bool flag = false;
		try
		{
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(fileName))
			{
				if (isOldFormat)
				{
					int num = bIJEncryptDataReader.ReadInt();
					for (int i = 0; i < num; i++)
					{
						BIJDLData bIJDLData = BIJDLData.DeserializeOldFormatFromLocalFile(bIJEncryptDataReader, Version);
						if (string.IsNullOrEmpty(BaseURL))
						{
							BaseURL = bIJDLData.URL.Substring(0, bIJDLData.URL.IndexOf("/DL/") + 1);
						}
						bIJDLData.URL = bIJDLData.URL.Substring(bIJDLData.URL.IndexOf("/DL/") + 1);
						DataDict.Add(bIJDLData.BaseName, bIJDLData);
					}
				}
				else
				{
					int a_savedVersion = bIJEncryptDataReader.ReadInt();
					BaseURL = bIJEncryptDataReader.ReadUTF();
					int num2 = bIJEncryptDataReader.ReadInt();
					for (int j = 0; j < num2; j++)
					{
						BIJDLData bIJDLData2 = BIJDLData.DeserializeFromLocalFile(bIJEncryptDataReader, a_savedVersion);
						DataDict.Add(bIJDLData2.BaseName, bIJDLData2);
					}
				}
				flag = true;
			}
		}
		catch (Exception)
		{
			flag = false;
		}
		if (flag)
		{
			return true;
		}
		return false;
	}

	public void Clear()
	{
		DataDict.Clear();
	}

	public bool HasAlreadyDL(string aDlPath, BIJDLData a_data)
	{
		bool flag = false;
		if (File.Exists(aDlPath + a_data.BaseName))
		{
			BIJDLData dLData = GetDLData(a_data);
			if (dLData != null && !(dLData.CRC != a_data.CRC) && dLData.DataSize == a_data.DataSize)
			{
				return true;
			}
			flag = true;
			File.Delete(aDlPath + a_data.BaseName);
		}
		else
		{
			ResourceDLUtils.dbg2("no exist in local :" + a_data.BaseName);
			flag = true;
		}
		if (flag)
		{
			RemoveDLData(a_data);
		}
		return false;
	}

	public BIJDLData GetDLData(string a_name)
	{
		BIJDLData result = null;
		BIJDLData value = null;
		if (DataDict.TryGetValue(a_name, out value) && value.HasFilename(a_name))
		{
			result = value;
		}
		return result;
	}

	public BIJDLData GetDLData(BIJDLData a_data)
	{
		return GetDLData(a_data.BaseName);
	}

	public void UpdateDLData(BIJDLData a_data)
	{
		BIJDLData dLData = GetDLData(a_data.BaseName);
		if (dLData != null)
		{
			dLData.SetData(a_data);
		}
		else
		{
			DataDict.Add(a_data.BaseName, a_data);
		}
	}

	public void RemoveDLData(BIJDLData a_data)
	{
		BIJDLData dLData = GetDLData(a_data.BaseName);
		if (dLData != null)
		{
			DataDict.Remove(a_data.BaseName);
		}
	}

	public void Dump()
	{
		string empty = string.Empty;
		empty = string.Format("Filename:{0}\nDataList Count:{1}", Filename, Count);
		List<BIJDLData> list = new List<BIJDLData>(DataDict.Values);
		for (int i = 0; i < list.Count; i++)
		{
			list[i].Dump();
		}
	}
}
