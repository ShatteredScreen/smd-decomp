public class MissionCountPuzzleAdv
{
	public int mEnemy { get; set; }

	public int mSkill { get; set; }

	public int mFever { get; set; }

	public MissionCountPuzzleAdv()
	{
		Init();
	}

	public void Init()
	{
		mEnemy = 0;
		mSkill = 0;
		mFever = 0;
	}
}
