using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabyrinthLoseDialog : LoseDialog
{
	private enum LOAD_ATLAS
	{
		HUD = 0,
		PUZZLE_CONTINUE_HUD_LABYRINTH = 1,
		PUZZLE_CONTINUE_HUD = 2,
		EVENTLABYRINTH_POINT = 3
	}

	public sealed class AtlasInfo : IDisposable
	{
		private string mKey;

		private bool mInit;

		private bool mForceNotUnload;

		private bool mDisposed;

		public ResImage Resource { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Resource == null) ? null : Resource.Image;
			}
		}

		public AtlasInfo(string key)
		{
			mKey = key;
		}

		~AtlasInfo()
		{
			Dispose(false);
		}

		public IEnumerator Load(bool not_unload = false)
		{
			if (Resource != null)
			{
				UnLoad();
			}
			if (Resource != null)
			{
				yield break;
			}
			if (Res.ResImageDict.ContainsKey(mKey) && ResourceManager.IsLoaded(mKey))
			{
				Resource = ResourceManager.LoadImage(mKey);
				if (Resource != null)
				{
					yield break;
				}
			}
			Resource = ResourceManager.LoadImageAsync(mKey);
			if (Resource.LoadState == ResourceInstance.LOADSTATE.INIT)
			{
				mInit = true;
				mForceNotUnload = not_unload;
			}
		}

		public void UnLoad()
		{
			if (mInit)
			{
				ResourceManager.UnloadImage(mKey);
			}
			mInit = false;
		}

		public bool IsDone()
		{
			return !mInit || (mInit && Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.DONE);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	private Dictionary<LOAD_ATLAS, AtlasInfo> mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();

	private Transform mJewelFrame;

	private int mAcquireJewel;

	private bool mHasJewelUpChance;

	public void Init(int acquire_jewel, bool has_jewel_up_chance, int continueCount, Def.STAGE_LOSE_REASON reason, bool tuxedoSkipMode, bool _stageskipmode = false, int _stageskipPriceIndex = 0, bool _chance_announced = false)
	{
		mAcquireJewel = acquire_jewel;
		mHasJewelUpChance = has_jewel_up_chance;
		Init(continueCount, reason, false, false, 0, _chance_announced);
	}

	public override void BuildDialog()
	{
		Vector2 vector = Util.LogScreenSize();
		if (mJelly != null)
		{
			JellySecondDialog jellySecondDialog = mJelly as JellySecondDialog;
			float a_startPos = vector.y * 1.5f;
			Vector2 bUILD_DIALOG_POSITION_OFFSET = DialogBase.BUILD_DIALOG_POSITION_OFFSET;
			jellySecondDialog.SetInitialPosition(a_startPos, bUILD_DIALOG_POSITION_OFFSET.y, 0.3f, 0.2f);
		}
		UIFont atlasFont = GameMain.LoadFont();
		GameObject parent = base.gameObject;
		Vector3 vector2 = ((Util.ScreenOrientation != ScreenOrientation.LandscapeLeft && Util.ScreenOrientation != ScreenOrientation.LandscapeRight) ? mGemFramePositionOffsetP : mGemFramePositionOffsetL);
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out data);
		JudgeContinueChance();
		string titleDescKey = Localization.Get("Labyrinth_Lose_Continue");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("Character", parent, mAtlas[LOAD_ATLAS.PUZZLE_CONTINUE_HUD_LABYRINTH].Image);
		Util.SetSpriteInfo(uISprite, "continue_chara00", mBaseDepth + 4, new Vector3(-125f, -22f, 0f), Vector3.one, false);
		Transform self = uISprite.transform;
		uISprite = Util.CreateSprite("JewelFrame", uISprite.gameObject, mAtlas[LOAD_ATLAS.PUZZLE_CONTINUE_HUD_LABYRINTH].Image);
		Util.SetSpriteInfo(uISprite, "get_panel", mBaseDepth + 1, new Vector3(0f, 89f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(190, 91);
		UILabel label = Util.CreateLabel("Caption", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(label, Localization.Get("Labyrinth_Lose_Continue_Desc_00"), mBaseDepth + 2, new Vector3(-1f, 28f), 20, 180, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(label, Def.DEFAULT_MESSAGE_COLOR);
		Util.SetLabeShrinkContent(label, 180, 20);
		label = Util.CreateLabel("Footer", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(label, Localization.Get("Num_ko"), mBaseDepth + 2, new Vector3(82f, -15f), 24, 25, 0, UIWidget.Pivot.Right);
		Util.SetLabelColor(label, Def.DEFAULT_MESSAGE_COLOR);
		Util.SetLabeShrinkContent(label, 25, 24);
		int keta = ((mAcquireJewel == 0) ? 1 : ((int)Mathf.Log10(mAcquireJewel) + 1));
		NumberImageString numberImageString = Util.CreateGameObject("Num", uISprite.gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(keta, mAcquireJewel, mBaseDepth + 3, 0.7f, UIWidget.Pivot.Center, false, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image, false, new string[10] { "result_total_num0", "result_total_num1", "result_total_num2", "result_total_num3", "result_total_num4", "result_total_num5", "result_total_num6", "result_total_num7", "result_total_num8", "result_total_num9" });
		numberImageString.SetPitch(38f);
		numberImageString.SetLocalPosition(4f, -7f);
		uISprite = Util.CreateSprite("Image", parent, mAtlas[LOAD_ATLAS.PUZZLE_CONTINUE_HUD_LABYRINTH].Image);
		Util.SetSpriteInfo(uISprite, "LC_continue_caution", mBaseDepth + 1, new Vector3(0f, 184f), Vector3.one, false);
		string empty = string.Empty;
		string empty2 = string.Empty;
		empty = "continue_item00";
		empty2 = ((mGame.mCurrentStage.LoseType != 0) ? "continue_itemText09" : "continue_itemText10");
		switch (mData.item)
		{
		case Def.CONTINUE_ADDITIONALITEM.SKILL_CHARGE:
			empty = "icon_continueItem00";
			empty2 = "continue_itemText00";
			break;
		case Def.CONTINUE_ADDITIONALITEM.SELECT_ONE:
			empty = "icon_continueItem01";
			empty2 = "continue_itemText01";
			break;
		case Def.CONTINUE_ADDITIONALITEM.WAVE_BOMB:
			empty = "icon_continueItem02";
			empty2 = "continue_itemText02";
			break;
		case Def.CONTINUE_ADDITIONALITEM.RAINBOW:
			if (mData.num == 2)
			{
				empty = "icon_continueItem06";
				empty2 = "continue_itemText11";
			}
			else
			{
				empty = "icon_continueItem03";
				empty2 = "continue_itemText03";
			}
			break;
		case Def.CONTINUE_ADDITIONALITEM.TAP_BOMB:
			empty = "icon_continueItem04";
			empty2 = "continue_itemText04";
			break;
		case Def.CONTINUE_ADDITIONALITEM.COLOR_CRUSH:
			empty = "icon_continueItem05";
			empty2 = "continue_itemText05";
			break;
		}
		string imageQuantity = string.Empty;
		if (mData.item != 0)
		{
			if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES)
			{
				switch (mData.addMove)
				{
				case 5:
					imageQuantity = "continue_itemText_move00";
					break;
				case 15:
					imageQuantity = "continue_itemText_move01";
					break;
				case 30:
					imageQuantity = "continue_itemText_move02";
					break;
				}
			}
			else if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
			{
				switch ((int)mData.addTime)
				{
				case 15:
					imageQuantity = "continue_itemText_time00";
					break;
				case 30:
					imageQuantity = "continue_itemText_time01";
					break;
				case 60:
					imageQuantity = "continue_itemText_time02";
					break;
				}
			}
		}
		float num = 1f;
		if (mData.item != 0)
		{
			GameObject gameObject = Util.CreateGameObject("Ballon", parent);
			gameObject.transform.SetLocalScale(0.8f);
			gameObject.transform.SetLocalPosition(15f, -1f);
			if (!mStageSkipMode)
			{
				if (mData.item != 0)
				{
					num = 0.725f;
				}
				MakeContinueAdditionalBalloon(gameObject, new Vector3(100f, 20f, 0f), new Vector3(num, num, 1f), imageQuantity, empty, empty2);
			}
			else if (mLoseReason == Def.STAGE_LOSE_REASON.NO_MORE_MOVES)
			{
				uISprite = Util.CreateSprite("item", gameObject, mAtlas[LOAD_ATLAS.PUZZLE_CONTINUE_HUD].Image);
				Util.SetSpriteInfo(uISprite, "chara_miss", mBaseDepth + 2, new Vector3(0f, 60f, 0f), Vector3.one, false);
			}
			else
			{
				if (mData.item != 0)
				{
					num = 0.725f;
				}
				MakeContinueAdditionalBalloon(gameObject, new Vector3(100f, 20f, 0f), new Vector3(num, num, 1f), imageQuantity, empty, empty2);
			}
		}
		else
		{
			self.SetLocalPositionX(0f);
		}
		CreateButtons();
	}

	public override IEnumerator BuildResource()
	{
		yield return StartCoroutine(base.BuildResource());
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, AtlasInfo>();
		}
		foreach (AtlasInfo info3 in mAtlas.Values)
		{
			info3.UnLoad();
		}
		mAtlas.Clear();
		foreach (int type in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			AtlasInfo info2 = new AtlasInfo(((LOAD_ATLAS)type).ToString());
			mAtlas.Add((LOAD_ATLAS)type, info2);
			StartCoroutine(info2.Load());
		}
		foreach (AtlasInfo info in mAtlas.Values)
		{
			while (!info.IsDone())
			{
				yield return null;
			}
		}
	}

	public override IEnumerator CloseProcess()
	{
		if (mAtlas != null)
		{
			foreach (AtlasInfo info in mAtlas.Values)
			{
				if (info != null)
				{
					info.UnLoad();
				}
			}
			mAtlas.Clear();
		}
		mAtlas = null;
		PlayerEventData ped;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out ped);
		if (ped != null)
		{
			PlayerStageData stageData = null;
			int eventID = mGame.mCurrentStage.EventID;
			short courseID = 0;
			int main;
			int sub;
			Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out courseID, out main, out sub);
			mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, eventID, courseID, Def.GetStageNo(main, sub), out stageData);
			int eventStage = Def.GetStageNo(courseID, main, sub);
			int _jewelget_chance = (mHasJewelUpChance ? 1 : 0);
			int _result = ((mSelectItem == SELECT_ITEM.CONTINUE || mSelectItem == SELECT_ITEM.STAGESKIP) ? 1 : 0);
			int _sts_count = mGame.mPlayer.Data.LastPlayLevelCount;
			int _stage = Def.GetStageNoForServer(mGame.mPlayer.Data.CurrentEventID, eventStage);
			ServerCram.LabyrinthEventBuyContinue(sts_cnt: (mGame.mPlayer.Data.LastPlaySeries == mGame.mCurrentStage.Series && mGame.mPlayer.Data.LastPlayLevel == _stage) ? (_sts_count + 1) : 0, cur_stage: _stage, evt_id: mGame.mCurrentStage.EventID, jewel_num: mAcquireJewel, jewelget_chance: _jewelget_chance, result: _result, total_jewel_num: ped.LabyrinthData.JewelAmount);
		}
		yield return StartCoroutine(base.CloseProcess());
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		base.OnScreenChanged(o);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if ((bool)mJewelFrame)
			{
				mJewelFrame.SetLocalPosition(-194f + mGemFramePositionOffsetP.x + 200f, (0f - mDialogFrame.localSize.y) / 2f - 30f + mGemFramePositionOffsetP.y);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if ((bool)mJewelFrame)
			{
				mJewelFrame.SetLocalPosition(mDialogFrame.localSize.x / 2f + 95f + mGemFramePositionOffsetL.x, (0f - mDialogFrame.localSize.y) / 2f + 50f + mGemFramePositionOffsetL.y + 75f);
			}
			break;
		}
	}
}
