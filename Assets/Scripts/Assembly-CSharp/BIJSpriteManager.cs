using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
public class BIJSpriteManager : MonoBehaviour
{
	private class ZOrder : IComparer<BIJSpriteObject>
	{
		public int Delta = 1;

		public int Compare(BIJSpriteObject s1, BIJSpriteObject s2)
		{
			return Delta * (int)(s1.Z - s2.Z);
		}
	}

	private static readonly float UNIT_W = 100f;

	private static readonly float UNIT_H = 100f;

	private MeshFilter mMeshFilter;

	private MeshRenderer mMeshRenderer;

	private List<BIJSpriteObject> mSprites = new List<BIJSpriteObject>();

	[SerializeField]
	[HideInInspector]
	private Camera mCamera;

	private bool mModified;

	private Material mMaterial;

	public BIJImage PackImage;

	private static ZOrder mZOrder = new ZOrder();

	[BIJExposeProperty]
	public Camera Camera
	{
		get
		{
			return mCamera;
		}
		set
		{
			mCamera = value;
		}
	}

	public bool Modified
	{
		get
		{
			return mModified;
		}
		set
		{
			mModified = value;
		}
	}

	private void Awake()
	{
		mMeshFilter = GetComponent<MeshFilter>();
		mMeshRenderer = GetComponent<MeshRenderer>();
		Init();
	}

	private void OnDestroy()
	{
		Uninit();
	}

	private void Update()
	{
		if (Modified)
		{
			UpdateMesh();
			Modified = false;
		}
	}

	public virtual void Init()
	{
		mMaterial = new Material(Shader.Find("BIJ/BIJ2DAlpha"));
		mMeshFilter.mesh = new Mesh();
		mMeshRenderer.material = mMaterial;
	}

	public virtual void Uninit()
	{
		if (Application.isEditor)
		{
			Object.DestroyImmediate(mMeshFilter.mesh);
			Object.DestroyImmediate(mMeshRenderer.material);
		}
		else
		{
			Object.Destroy(mMeshFilter.mesh);
			Object.Destroy(mMeshRenderer.material);
		}
	}

	public void Resister(BIJSpriteObject sprite)
	{
		lock (mSprites)
		{
			if (!mSprites.Contains(sprite))
			{
				mSprites.Add(sprite);
				Modified = true;
			}
		}
	}

	public void Unregister(BIJSpriteObject sprite)
	{
		lock (mSprites)
		{
			if (mSprites.Contains(sprite))
			{
				mSprites.Remove(sprite);
				Modified = true;
			}
		}
	}

	private BIJSpriteObject[] GetSprites()
	{
		lock (mSprites)
		{
			List<BIJSpriteObject> list = new List<BIJSpriteObject>(mSprites.Count);
			foreach (BIJSpriteObject mSprite in mSprites)
			{
				if (mSprite.gameObject.active)
				{
					list.Add(mSprite);
				}
			}
			return list.ToArray();
		}
	}

	private void UpdateMesh()
	{
		if (PackImage == null)
		{
			return;
		}
		if (mMeshRenderer.material.mainTexture != PackImage.Texture)
		{
			mMeshRenderer.material.mainTexture = PackImage.Texture;
			mMeshRenderer.material.mainTextureOffset = new Vector2(0f, 0f);
			mMeshRenderer.material.mainTextureScale = new Vector2(1f, 1f);
		}
		Vector3[] array = new Vector3[mSprites.Count * 4];
		Vector2[] array2 = new Vector2[mSprites.Count * 4];
		int[] array3 = new int[mSprites.Count * 6];
		BIJSpriteObject[] sprites = GetSprites();
		for (int i = 0; i < sprites.Length; i++)
		{
			BIJSpriteObject bIJSpriteObject = sprites[i];
			if (bIJSpriteObject.Index == -1 || PackImage.Contains(bIJSpriteObject.Index))
			{
				Vector2 offset = PackImage.GetOffset(bIJSpriteObject.Index);
				Vector2 size = PackImage.GetSize(bIJSpriteObject.Index);
				if (bIJSpriteObject.Flip)
				{
					array2[i * 4] = new Vector2(offset.x + size.x, offset.y);
					array2[i * 4 + 1] = new Vector2(offset.x, offset.y);
					array2[i * 4 + 2] = new Vector2(offset.x + size.x, offset.y + size.y);
					array2[i * 4 + 3] = new Vector2(offset.x, offset.y + size.y);
				}
				else
				{
					array2[i * 4] = new Vector2(offset.x, offset.y);
					array2[i * 4 + 1] = new Vector2(offset.x + size.x, offset.y);
					array2[i * 4 + 2] = new Vector2(offset.x, offset.y + size.y);
					array2[i * 4 + 3] = new Vector2(offset.x + size.x, offset.y + size.y);
				}
				if (bIJSpriteObject.AutoResize)
				{
					Vector2 pixelSize = PackImage.GetPixelSize(bIJSpriteObject.Index);
					Vector4 pixelPadding = PackImage.GetPixelPadding(bIJSpriteObject.Index);
					float num = pixelSize.x / 2f;
					float num2 = pixelSize.y / 2f;
					float num3 = 0f;
					float num4 = 0f;
					switch (bIJSpriteObject.Anchor)
					{
					case BIJSpriteObject.AnchorFlags.TopLeft:
					case BIJSpriteObject.AnchorFlags.MiddleLeft:
					case BIJSpriteObject.AnchorFlags.BottomLeft:
						num3 = num + pixelPadding.z;
						break;
					case BIJSpriteObject.AnchorFlags.TopRight:
					case BIJSpriteObject.AnchorFlags.MiddleRight:
					case BIJSpriteObject.AnchorFlags.BottomRight:
						num3 = 0f - (num + pixelPadding.x);
						break;
					}
					switch (bIJSpriteObject.Anchor)
					{
					case BIJSpriteObject.AnchorFlags.TopLeft:
					case BIJSpriteObject.AnchorFlags.TopCenter:
					case BIJSpriteObject.AnchorFlags.TopRight:
						num4 = 0f - (num2 + pixelPadding.y);
						break;
					case BIJSpriteObject.AnchorFlags.BottomLeft:
					case BIJSpriteObject.AnchorFlags.BottomCenter:
					case BIJSpriteObject.AnchorFlags.BottomRight:
						num4 = num2 + pixelPadding.w;
						break;
					}
					num3 += bIJSpriteObject.AnchorOffset.x;
					num4 += bIJSpriteObject.AnchorOffset.y;
					array[i * 4] = new Vector3((0f - num + num3) * bIJSpriteObject.Scale.x + bIJSpriteObject.Position.x, (0f - num2 + num4) * bIJSpriteObject.Scale.y + bIJSpriteObject.Position.y, 0f + bIJSpriteObject.Position.z);
					array[i * 4 + 1] = new Vector3((num + num3) * bIJSpriteObject.Scale.x + bIJSpriteObject.Position.x, (0f - num2 + num4) * bIJSpriteObject.Scale.y + bIJSpriteObject.Position.y, 0f + bIJSpriteObject.Position.z);
					array[i * 4 + 2] = new Vector3((0f - num + num3) * bIJSpriteObject.Scale.x + bIJSpriteObject.Position.x, (num2 + num4) * bIJSpriteObject.Scale.y + bIJSpriteObject.Position.y, 0f + bIJSpriteObject.Position.z);
					array[i * 4 + 3] = new Vector3((num + num3) * bIJSpriteObject.Scale.x + bIJSpriteObject.Position.x, (num2 + num4) * bIJSpriteObject.Scale.y + bIJSpriteObject.Position.y, 0f + bIJSpriteObject.Position.z);
				}
			}
			array3[i * 6] = i * 4 + 2;
			array3[i * 6 + 1] = i * 4 + 1;
			array3[i * 6 + 2] = i * 4;
			array3[i * 6 + 3] = i * 4 + 2;
			array3[i * 6 + 4] = i * 4 + 3;
			array3[i * 6 + 5] = i * 4 + 1;
		}
		mMeshFilter.mesh.vertices = array;
		mMeshFilter.mesh.uv = array2;
		mMeshFilter.mesh.triangles = array3;
		mMeshFilter.mesh.RecalculateNormals();
		mMeshFilter.mesh.RecalculateBounds();
		UpdateSubmesh(sprites);
	}

	private void UpdateSubmesh(BIJSpriteObject[] sprites)
	{
		int[] array = new int[6];
		mMeshFilter.mesh.subMeshCount = sprites.Length;
		Material[] array2 = new Material[mMeshFilter.mesh.subMeshCount];
		for (int i = 0; i < sprites.Length; i++)
		{
			array[0] = i * 4 + 2;
			array[1] = i * 4 + 1;
			array[2] = i * 4;
			array[3] = i * 4 + 2;
			array[4] = i * 4 + 3;
			array[5] = i * 4 + 1;
			mMeshFilter.mesh.SetTriangles(array, i);
			array2[i] = mMaterial;
			array2[i].mainTexture = PackImage.Texture;
			array2[i].mainTextureOffset = new Vector2(0f, 0f);
			array2[i].mainTextureScale = new Vector2(1f, 1f);
		}
		mMeshRenderer.materials = array2;
	}

	public BIJSpriteObject Contains(float x, float y)
	{
		Matrix4x4 localToWorldMatrix = base.transform.localToWorldMatrix;
		BIJSpriteObject[] sprites = GetSprites();
		for (int i = 0; i < mSprites.Count; i++)
		{
			BIJSpriteObject bIJSpriteObject = sprites[i];
			if (bIJSpriteObject.Clickable)
			{
				Vector3 position = localToWorldMatrix.MultiplyPoint(mMeshFilter.mesh.vertices[i * 4]);
				Vector3 position2 = localToWorldMatrix.MultiplyPoint(mMeshFilter.mesh.vertices[i * 4 + 3]);
				Vector3 vector = Camera.WorldToScreenPoint(position);
				Vector3 vector2 = Camera.WorldToScreenPoint(position2);
				if (vector.x <= x && vector2.x >= x && vector.y <= y && vector2.y >= y)
				{
					return bIJSpriteObject;
				}
			}
		}
		return null;
	}
}
