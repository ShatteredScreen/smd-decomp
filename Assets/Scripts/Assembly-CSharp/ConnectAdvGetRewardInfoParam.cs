public class ConnectAdvGetRewardInfoParam : ConnectParameterBase
{
	public int[] RewardIDList { get; private set; }

	public string Category { get; private set; }

	public ConnectAdvGetRewardInfoParam(int[] a_list, string a_category = "quest")
	{
		RewardIDList = new int[a_list.Length];
		for (int i = 0; i < a_list.Length; i++)
		{
			RewardIDList[i] = a_list[i];
		}
		Category = a_category;
	}
}
