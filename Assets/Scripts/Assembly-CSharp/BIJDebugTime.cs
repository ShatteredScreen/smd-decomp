using UnityEngine;

public class BIJDebugTime : MonoBehaviour
{
	private Rect mDebugTimeRect;

	private float mDebugTime = -1f;

	private void Awake()
	{
		mDebugTime = -1f;
	}

	private void Start()
	{
		int num = 30;
		mDebugTimeRect = new Rect(0f, Screen.height - num, Screen.width, num);
	}

	private void OnGUI()
	{
		if (mDebugTime >= 0f)
		{
			GUI.depth = -9;
			float num = Time.realtimeSinceStartup - mDebugTime;
			GUI.Label(mDebugTimeRect, string.Format("{0} s", num.ToString("0.00")));
		}
	}

	public void StartTime()
	{
		mDebugTime = Time.realtimeSinceStartup;
	}

	public void EndTime()
	{
		mDebugTime = -1f;
	}
}
