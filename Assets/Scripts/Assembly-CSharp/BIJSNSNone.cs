public class BIJSNSNone : BIJSNS
{
	public override SNS Kind
	{
		get
		{
			return SNS.None;
		}
	}

	public override bool IsEnabled()
	{
		return false;
	}

	public override bool IsOpEnabled(int type)
	{
		return false;
	}

	public override bool IsLogined()
	{
		return false;
	}

	public override IEventCallback CreateEventCallback(int seqno)
	{
		return new BIJSNSEventCallback(this, seqno);
	}

	public override bool ReplyEvent(int seqno, Response res)
	{
		if (IsTargetCallback(seqno))
		{
			base.CurrentRes = res;
			ChangeStep(STEP.COMM_RESULT);
			return true;
		}
		return false;
	}

	public override void ClearData(Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "ClearData not immplemented", null), userdata);
	}

	public override void Login(Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "Login not immplemented", null), userdata);
	}

	public override void Logout(Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "Logout not immplemented", null), userdata);
	}

	public override void GetUserInfo(string userId, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "GetUserInfo not immplemented", null), userdata);
	}

	public override void GetFriends(string userId, int limit, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "GetFriends not immplemented", null), userdata);
	}

	public override void FetchUserImage(string userId, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "FetchUserImage not immplemented", null), userdata);
	}

	public override void PostMessage(IBIJSNSPostData data, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "PostMessage not immplemented", null), userdata);
	}

	public override void Invite(IBIJSNSPostData data, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "Invite not immplemented", null), userdata);
	}

	public override void UploadImage(IBIJSNSPostData data, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "UploadImage not immplemented", null), userdata);
	}

	public override void UnlockAchievement(string id, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "UnlockAchievement not immplemented", null), userdata);
	}

	public override void IncrementAchievement(string id, int step, Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "IncrementAchievement not immplemented", null), userdata);
	}

	public override void ShowAchievements(Handler handler, object userdata)
	{
		CallCallback(handler, new Response(1, "ShowAchievements not immplemented", null), userdata);
	}
}
