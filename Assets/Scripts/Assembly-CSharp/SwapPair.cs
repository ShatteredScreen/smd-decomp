public class SwapPair
{
	public PuzzleTile mTile1;

	public PuzzleTile mTile2;

	public Def.DIR mMoveDir1;

	public Def.DIR mMoveDir2;

	public bool mForce;

	public bool mSwapEnd;

	public SwapPair(PuzzleTile tile1, Def.DIR dir1, PuzzleTile tile2, Def.DIR dir2, bool force = false)
	{
		mTile1 = tile1;
		mTile2 = tile2;
		mMoveDir1 = dir1;
		mMoveDir2 = dir2;
		mForce = force;
		mSwapEnd = false;
	}
}
