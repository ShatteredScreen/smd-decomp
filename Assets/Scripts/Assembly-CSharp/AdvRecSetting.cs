public class AdvRecSetting
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("adv_rec_sp")]
	public int AdvRecoverSpSec { get; set; }
}
