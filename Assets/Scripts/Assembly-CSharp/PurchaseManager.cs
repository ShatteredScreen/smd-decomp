using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;
using UnityEngine;

public class PurchaseManager : MonoBehaviour
{
	public delegate void OnBillingSupportedEventFunc();

	public delegate void OnBillingNotSupportedEventFunc(string _s0);

	public delegate void OnQueryInventorySucceededEventFunc(List<GooglePurchase> _l0, List<GoogleSkuInfo> _l1);

	public delegate void OnQueryInventoryFailedEventFunc(string _s0);

	public delegate void OnPurchaseCompleteAwaitingVerificationEventFunc(string _s0, string _s1);

	public delegate void OnPurchaseSucceededEventFunc(List<PItem> _plist);

	public delegate void OnPurchaseFailedEventFunc(string _s0);

	public delegate void OnConsumePurchaseSucceededEventFunc(GooglePurchase _gp, int _state, PItem _p);

	public delegate void OnConsumePurchaseFailedEventFunc(string _s0);

	public const bool RECEIPT_CHECK = false;

	public const int GOOGLE_PURCHASED = 0;

	public const int GOOGLE_CANCELED = 1;

	public const int GOOGLE_REFUNDED = 2;

	public static PurchaseManager instance;

	public static PurchaseLog LogData;

	public static CampaignLog CampaignLogData;

	public bool restoreMode;

	private GoogleIABManager iabMangaer;

	private bool billingSupport;

	public Dictionary<string, GoogleSkuInfo> googleSkuInfos = new Dictionary<string, GoogleSkuInfo>();

	public List<PItem> mPItems = new List<PItem>();

	[NonSerialized]
	public OnBillingSupportedEventFunc onBillingSupportedEventFunc;

	[NonSerialized]
	public OnBillingNotSupportedEventFunc onBillingNotSupportedEventFunc;

	[NonSerialized]
	public OnQueryInventorySucceededEventFunc onQueryInventorySucceededEventFunc;

	[NonSerialized]
	public OnQueryInventoryFailedEventFunc onQueryInventoryFailedEventFunc;

	[NonSerialized]
	public OnPurchaseCompleteAwaitingVerificationEventFunc onPurchaseCompleteAwaitingVerificationEventFunc;

	[NonSerialized]
	public OnPurchaseSucceededEventFunc onPurchaseSucceededEventFunc;

	[NonSerialized]
	public OnPurchaseFailedEventFunc onPurchaseFailedEventFunc;

	[NonSerialized]
	public OnConsumePurchaseSucceededEventFunc onConsumePurchaseSucceededEventFunc;

	[NonSerialized]
	public OnConsumePurchaseFailedEventFunc onConsumePurchaseFailedEventFunc;

	public static string PACKAGE_NAME
	{
		get
		{
			// Changing the package name because I can't get this to work in its default state
			// return Application.bundleIdentifier;
			return "do.you.know.any.sailors";
		}
	}

	public bool IsInitialized { get; set; }

	private void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		IsInitialized = false;
	}

	private IEnumerator Start()
	{
		iabMangaer = base.gameObject.GetComponent<GoogleIABManager>();
		InitManager();
		IsInitialized = false;
		billingSupport = false;
		if (GameMain.ALWAYS_LOG)
		{
			GoogleIAB.enableLogging(true);
		}
		GoogleIAB.init(Def.Purchase.LICENSE_KEY);
		float start = Time.realtimeSinceStartup;
		while (!IsInitialized)
		{
			yield return null;
			if (Time.realtimeSinceStartup - start > 3f)
			{
				break;
			}
		}
		IsInitialized = true;
	}

	public void SetItem()
	{
		PItemInitInfo[] array = new PItemInitInfo[24]
		{
			new PItemInitInfo(PItem.KIND.SD0, 0, 10, 120f, 0.99f, "\\120", "$0.99", "bngi0223_smd_jp001", "com.mc2.smd001", "com.bij.ds0", "BNEI0264_smd_ww001", "bnei0264_smd_ww001", "com.mc2.smdww001", "com.bij.smdww0"),
			new PItemInitInfo(PItem.KIND.SD1, 1, 55, 600f, 4.99f, "\\600", "$4.99", "bngi0223_smd_jp002", "com.mc2.smd002", "com.bij.ds1", "BNEI0264_smd_ww002", "bnei0264_smd_ww002", "com.mc2.smdww002", "com.bij.smdww1"),
			new PItemInitInfo(PItem.KIND.SD2, 2, 120, 1200f, 9.99f, "\\1,200", "$9.99", "bngi0223_smd_jp003", "com.mc2.smd003", "com.bij.ds2", "BNEI0264_smd_ww003", "bnei0264_smd_ww003", "com.mc2.smdww003", "com.bij.smdww2"),
			new PItemInitInfo(PItem.KIND.SD3, 3, 255, 2400f, 19.99f, "\\2,400", "$19.99", "bngi0223_smd_jp004", "com.mc2.smd004", "com.bij.ds3", "BNEI0264_smd_ww004", "bnei0264_smd_ww004", "com.mc2.smdww004", "com.bij.smdww3"),
			new PItemInitInfo(PItem.KIND.SD4, 4, 530, 4800f, 39.99f, "\\4,800", "$39.99", "bngi0223_smd_jp005", "com.mc2.smd005", "com.bij.ds4", "BNEI0264_smd_ww005", "bnei0264_smd_ww005", "com.mc2.smdww005", "com.bij.smdww4"),
			new PItemInitInfo(PItem.KIND.SD5, 5, 1100, 8800f, 74.99f, "\\8,800", "$74.99", "bngi0223_smd_jp006", "com.mc2.smd006", "com.bij.ds5", "BNEI0264_smd_ww006", "bnei0264_smd_ww006", "com.mc2.smdww006", "com.bij.smdww5"),
			new PItemInitInfo(PItem.KIND.SD6, 6, 185, 1800f, 14.99f, "\\1,800", "$14.99", "bngi0223_smd_jp009", "com.mc2.smd009", "com.bij.ds6", "BNEI0264_smd_ww009", "bnei0264_smd_ww009", "com.mc2.smdww009", "com.bij.smdww6"),
			new PItemInitInfo(PItem.KIND.CA0, 7, 100, 360f, 2.99f, "\\360", "$2.99", "bngi0223_smd_jp007", "com.mc2.smd007", "com.bij.campaign0", "BNEI0264_smd_ww007", "bnei0264_smd_ww007", "com.mc2.smdww007", "com.bij.smdwwcampaign0"),
			new PItemInitInfo(PItem.KIND.CA1, 8, 250, 1800f, 14.99f, "\\1,800", "$14.99", "bngi0223_smd_jp008", "com.mc2.smd008", "com.bij.campaign1", "BNEI0264_smd_ww008", "bnei0264_smd_ww008", "com.mc2.smdww008", "com.bij.smdwwcampaign1"),
			new PItemInitInfo(PItem.KIND.DUMMY0, 9, 30, 120f, 0.99f, "\\120", "$0.99", "bngi0223_smd_jpxxx", "com.mc2.smdxxx", "com.bij.dsxx", "BNEI0264_smd_wwXXX", "bnei0264_smd_wwXXX", "com.mc2.smdwwXXX", "com.bij.smdwwXX"),
			new PItemInitInfo(PItem.KIND.CA2, 9, 30, 120f, 0.99f, "\\120", "$0.99", "bngi0223_smd_jp010", "com.mc2.smd010", "com.bij.ds10", "BNEI0264_smd_ww010", "bnei0264_smd_ww010", "com.mc2.smdww010", "com.bij.smdww10"),
			new PItemInitInfo(PItem.KIND.CA3, 11, 40, 240f, 1.99f, "\\240", "$1.99", "bngi0223_smd_jp011", "com.mc2.smd011", "com.bij.ds11", "BNEI0264_smd_ww011", "bnei0264_smd_ww011", "com.mc2.smdww011", "com.bij.smdww11"),
			new PItemInitInfo(PItem.KIND.CA4, 11, 45, 360f, 2.99f, "\\360", "$2.99", "bngi0223_smd_jp012", "com.mc2.smd012", "com.bij.ds12", "BNEI0264_smd_ww012", "bnei0264_smd_ww012", "com.mc2.smdww012", "com.bij.smdww12"),
			new PItemInitInfo(PItem.KIND.CA5, 11, 60, 480f, 3.99f, "\\480", "$3.99", "bngi0223_smd_jp013", "com.mc2.smd013", "com.bij.ds13", "BNEI0264_smd_ww013", "bnei0264_smd_ww013", "com.mc2.smdww013", "com.bij.smdww13"),
			new PItemInitInfo(PItem.KIND.CA6, 11, 80, 600f, 4.99f, "\\600", "$4.99", "bngi0223_smd_jp014", "com.mc2.smd014", "com.bij.ds14", "BNEI0264_smd_ww014", "bnei0264_smd_ww014", "com.mc2.smdww014", "com.bij.smdww14"),
			new PItemInitInfo(PItem.KIND.CA7, 11, 130, 960f, 7.99f, "\\960", "$7.99", "bngi0223_smd_jp015", "com.mc2.smd015", "com.bij.ds15", "BNEI0264_smd_ww015", "bnei0264_smd_ww015", "com.mc2.smdww015", "com.bij.smdww15"),
			new PItemInitInfo(PItem.KIND.CA8, 10, 165, 1200f, 9.99f, "\\1,200", "$9.99", "bngi0223_smd_jp016", "com.mc2.smd016", "com.bij.ds16", "BNEI0264_smd_ww016", "bnei0264_smd_ww016", "com.mc2.smdww016", "com.bij.smdww16"),
			new PItemInitInfo(PItem.KIND.CA9, 11, 335, 2400f, 19.99f, "\\2,400", "$19.99", "bngi0223_smd_jp017", "com.mc2.smd017", "com.bij.ds17", "BNEI0264_smd_ww017", "bnei0264_smd_ww017", "com.mc2.smdww017", "com.bij.smdww17"),
			new PItemInitInfo(PItem.KIND.CA10, 11, 415, 3000f, 24.99f, "\\3,000", "$24.99", "bngi0223_smd_jp018", "com.mc2.smd018", "com.bij.ds18", "BNEI0264_smd_ww018", "bnei0264_smd_ww018", "com.mc2.smdww018", "com.bij.smdww18"),
			new PItemInitInfo(PItem.KIND.CA11, 11, 500, 3600f, 29.99f, "\\3,600", "$29.99", "bngi0223_smd_jp019", "com.mc2.smd019", "com.bij.ds19", "BNEI0264_smd_ww019", "bnei0264_smd_ww019", "com.mc2.smdww019", "com.bij.smdww19"),
			new PItemInitInfo(PItem.KIND.CA12, 11, 555, 4000f, 32.99f, "\\4,000", "$32.99", "bngi0223_smd_jp020", "com.mc2.smd020", "com.smd.ds20", "BNEI0264_smd_ww020", "bnei0264_smd_ww020", "com.mc2.smdww020", "com.bij.smdww20"),
			new PItemInitInfo(PItem.KIND.CA13, 11, 630, 4500f, 36.99f, "\\4,500", "$36.99", "bngi0223_smd_jp021", "com.mc2.smd021", "com.smd.ds21", "BNEI0264_smd_ww021", "bnei0264_smd_ww021", "com.mc2.smdww021", "com.bij.smdww21"),
			new PItemInitInfo(PItem.KIND.CA14, 11, 680, 4800f, 39.99f, "\\4,800", "$39.99", "bngi0223_smd_jp022", "com.mc2.smd022", "com.smd.ds22", "BNEI0264_smd_ww022", "bnei0264_smd_ww022", "com.mc2.smdww022", "com.bij.smdww22"),
			new PItemInitInfo(PItem.KIND.CA15, 11, 700, 5000f, 40.99f, "\\5,000", "$40.99", "bngi0223_smd_jp023", "com.mc2.smd023", "com.smd.ds23", "BNEI0264_smd_ww023", "bnei0264_smd_ww023", "com.mc2.smdww023", "com.bij.smdww23")
		};
		foreach (PItemInitInfo pItemInitInfo in array)
		{
			PItem pItem = new PItem();
			pItem.kind = pItemInitInfo.mKind;
			pItem.iconID = pItemInitInfo.mIconID;
			pItem.amount = pItemInitInfo.mAmount;
			pItem.cost = pItemInitInfo.GetCost();
			pItem.formattedPrice = pItemInitInfo.GetFormattedPrice();
			pItem.itemID = pItemInitInfo.GetItemID();
			pItem.storeInfo = false;
			mPItems.Add(pItem);
		}
	}

	private void OnDestroy()
	{
	}

	private void Update()
	{
	}

	public PItem GetItem(string _itemID)
	{
		if (mPItems == null)
		{
			return null;
		}
		foreach (PItem mPItem in mPItems)
		{
			if (mPItem.itemID.Equals(_itemID))
			{
				return mPItem;
			}
		}
		return null;
	}

	public PItem GetItem(int _idx)
	{
		if (mPItems == null)
		{
			return null;
		}
		if (mPItems.Count <= _idx)
		{
			return null;
		}
		return mPItems[_idx];
	}

	public int GetItemIdx(string _itemID)
	{
		if (mPItems == null)
		{
			return 0;
		}
		int num = 0;
		foreach (PItem mPItem in mPItems)
		{
			if (mPItem.itemID.Equals(_itemID))
			{
				return num;
			}
			num++;
		}
		return 0;
	}

	public bool CanMakePayments()
	{
		return billingSupport;
	}

	public void RequestProductData()
	{
		if (billingSupport)
		{
			string[] array = new string[mPItems.Count];
			for (int i = 0; i < mPItems.Count; i++)
			{
				array[i] = mPItems[i].itemID;
			}
			googleSkuInfos.Clear();
			GoogleIAB.queryInventory(array);
		}
	}

	public void RestoreTransactions()
	{
		if (billingSupport)
		{
			string[] array = new string[mPItems.Count];
			for (int i = 0; i < mPItems.Count; i++)
			{
				array[i] = mPItems[i].itemID;
			}
			GoogleIAB.queryInventory(array);
		}
	}

	public void RestoreTransactionsIOS()
	{
		restoreMode = true;
	}

	public void FinishTransactions(string _transactionIdentifier)
	{
	}

	public void FinishPendingTransactions()
	{
	}

	public void ConsumeProduct(string _s)
	{
		restoreMode = false;
		GoogleIAB.consumeProduct(_s);
	}

	public void ConsumeProducts(string[] _sl)
	{
		GoogleIAB.consumeProducts(_sl);
	}

	public bool CheckRecievedProducts()
	{
		return googleSkuInfos != null && googleSkuInfos.Count > 0;
	}

	private void OnBillingSupportedEvent()
	{
		IsInitialized = true;
		billingSupport = true;
		if (onBillingSupportedEventFunc != null)
		{
			onBillingSupportedEventFunc();
		}
	}

	private void OnBillingNotSupportedEvent(string _s0)
	{
		IsInitialized = true;
		billingSupport = false;
		if (onBillingNotSupportedEventFunc != null)
		{
			onBillingNotSupportedEventFunc(_s0);
		}
	}

	private void OnQueryInventorySucceededEvent(List<GooglePurchase> _l0, List<GoogleSkuInfo> _l1)
	{
		if (_l1 != null)
		{
			foreach (GoogleSkuInfo item2 in _l1)
			{
				PItem item = GetItem(item2.productId);
				if (item == null)
				{
					continue;
				}
				try
				{
					item.cost = item2.priceAmountMicros / 1000000;
					item.currencyCode = item2.priceCurrencyCode;
					item.formattedPrice = item2.price;
					item.storeInfo = true;
					if (GameMain.USE_DEBUG_DIALOG && SingletonMonoBehaviour<GameMain>.Instance.mDebugPurchaseStoreInfoOff && item.kind == PItem.KIND.CA0)
					{
						item.storeInfo = false;
					}
				}
				catch (Exception)
				{
				}
			}
		}
		if (onQueryInventorySucceededEventFunc != null)
		{
			onQueryInventorySucceededEventFunc(_l0, _l1);
		}
	}

	private void OnQueryInventoryFailedEvent(string _s0)
	{
		if (onQueryInventoryFailedEventFunc != null)
		{
			onQueryInventoryFailedEventFunc(_s0);
		}
	}

	private void OnPurchaseCompleteAwaitingVerificationEvent(string _purchaseData, string _signature)
	{
		bool flag = false;
		if (onPurchaseCompleteAwaitingVerificationEventFunc != null)
		{
			onPurchaseCompleteAwaitingVerificationEventFunc(_purchaseData, _signature);
		}
		if (flag)
		{
			SavePurchaseData();
		}
	}

	private void OnPurchaseSucceededEvent(GooglePurchase _gp)
	{
		int num = 0;
		bool flag = false;
		string orderId = _gp.orderId;
		string packageName = _gp.packageName;
		string productId = _gp.productId;
		long purchaseTime = _gp.purchaseTime;
		int purchaseState = (int)_gp.purchaseState;
		List<PItem> list = new List<PItem>();
		if (string.IsNullOrEmpty(packageName) || !packageName.Equals(PACKAGE_NAME))
		{
			num = 100;
		}
		PItem item = GetItem(_gp.productId);
		if (item == null)
		{
			num = 200;
		}
		if (num != 0)
		{
		}
		if (item != null)
		{
			if (!LogData.Logs.ContainsKey(_gp.orderId))
			{
				PurchaseLog.PLog pLog = new PurchaseLog.PLog();
				pLog.orderID = _gp.orderId;
				pLog.itemID = _gp.productId;
				pLog.purchaseTime = 0.0;
				pLog.receipt = _gp.originalJson;
				pLog.signature = _gp.signature;
				pLog.purchaseState = 100;
				LogData.Logs[_gp.orderId] = pLog;
				item.orderID = _gp.orderId;
				item.signature = _gp.signature;
				list.Add(item);
				flag = true;
			}
			if (onPurchaseSucceededEventFunc != null)
			{
				onPurchaseSucceededEventFunc(list);
			}
		}
		if (flag)
		{
			SavePurchaseData();
		}
	}

	private void OnPurchaseFailedEvent(string _error, int _response)
	{
		if (onPurchaseFailedEventFunc != null)
		{
			onPurchaseFailedEventFunc(_error);
		}
	}

	private void OnConsumePurchaseSucceededEvent(GooglePurchase _gp)
	{
		bool flag = false;
		int num = 0;
		PItem item = GetItem(_gp.productId);
		if (item != null)
		{
			string orderId = _gp.orderId;
			if (LogData.Logs.ContainsKey(orderId))
			{
				PurchaseLog.PLog pLog = LogData.Logs[orderId];
				pLog.purchaseState = 200;
				LogData.Logs[orderId] = pLog;
				item.signature = pLog.signature;
				flag = true;
			}
			if (onConsumePurchaseSucceededEventFunc != null)
			{
				int purchaseState = (int)_gp.purchaseState;
				onConsumePurchaseSucceededEventFunc(_gp, purchaseState, item);
			}
		}
		else
		{
			num = 200;
			OnConsumePurchaseFailedEvent("purchase failed cause by error=" + num);
		}
		if (flag)
		{
			SavePurchaseData();
		}
	}

	private void OnConsumePurchaseFailedEvent(string _error)
	{
		if (onConsumePurchaseFailedEventFunc != null)
		{
			onConsumePurchaseFailedEventFunc(_error);
		}
	}

	public void InitManager()
	{
		restoreMode = false;
		GoogleIABManager.billingSupportedEvent += OnBillingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent += OnBillingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent += OnQueryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent += OnQueryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += OnPurchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent += OnPurchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent += OnPurchaseFailedEvent;
		GoogleIABManager.consumePurchaseSucceededEvent += OnConsumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent += OnConsumePurchaseFailedEvent;
	}

	public void CleanupManager()
	{
		GoogleIABManager.billingSupportedEvent -= OnBillingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent -= OnBillingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent -= OnQueryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent -= OnQueryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent -= OnPurchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent -= OnPurchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent -= OnPurchaseFailedEvent;
		GoogleIABManager.consumePurchaseSucceededEvent -= OnConsumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent -= OnConsumePurchaseFailedEvent;
	}

	public static void LoadPurchaseData()
	{
		bool flag = false;
		string pURCHASE_FILE = Constants.PURCHASE_FILE;
		try
		{
			using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader(pURCHASE_FILE))
			{
				LogData = PurchaseLog.Deserialize(bIJEncryptDataReader);
				bIJEncryptDataReader.Close();
			}
			flag = LogData != null;
		}
		catch (Exception)
		{
			flag = false;
		}
		if (!flag)
		{
			LogData = PurchaseLog.CreateForNewPurchaseLog();
			SavePurchaseData();
		}
	}

	public static void SavePurchaseData()
	{
		try
		{
			string pURCHASE_FILE = Constants.PURCHASE_FILE;
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(pURCHASE_FILE))
			{
				LogData.Serialize(bIJEncryptDataWriter);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	public static void LoadCampaignLogData()
	{
		CampaignLogData = CampaignLog.Load();
	}

	public PItem GetItemFromKIND(PItem.KIND _kind)
	{
		foreach (PItem mPItem in mPItems)
		{
			if (mPItem.kind == _kind)
			{
				return mPItem;
			}
		}
		return null;
	}

	public PItem.KIND GetItemKind(string iapname)
	{
		foreach (PItem mPItem in mPItems)
		{
			if (mPItem.ItemID == iapname)
			{
				return mPItem.kind;
			}
		}
		return PItem.KIND.SD0;
	}

	public string GetPriceText(string _itemID)
	{
		string result = string.Empty;
		PItem item = GetItem(_itemID);
		if (item != null)
		{
			result = item.FormattedPrice;
		}
		return result;
	}

	public string GetPriceText(int _idx)
	{
		string result = string.Empty;
		PItem item = GetItem(_idx);
		if (item != null)
		{
			result = item.FormattedPrice;
		}
		return result;
	}

	public float GetPrice(int _idx)
	{
		float result = 0f;
		PItem item = GetItem(_idx);
		if (item != null)
		{
			result = item.cost;
		}
		return result;
	}

	public string GetCurrencyCode(int _idx)
	{
		string result = string.Empty;
		PItem item = GetItem(_idx);
		if (item != null)
		{
			result = item.CurrencyCode;
		}
		return result;
	}

	public bool IsGotStoreInfo(string _itemID)
	{
		PItem item = GetItem(_itemID);
		if (item == null)
		{
			return false;
		}
		if (!item.storeInfo)
		{
			return false;
		}
		return true;
	}
}
