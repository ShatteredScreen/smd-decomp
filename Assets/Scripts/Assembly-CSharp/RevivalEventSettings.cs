using System;

public class RevivalEventSettings : SeasonEventSettings, ICloneable
{
	[MiniJSONAlias("open_gem")]
	public int OpenGem { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public new RevivalEventSettings Clone()
	{
		return MemberwiseClone() as RevivalEventSettings;
	}
}
