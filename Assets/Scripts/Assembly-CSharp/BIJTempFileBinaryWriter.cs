using System;
using System.IO;
using System.Text;

public class BIJTempFileBinaryWriter : BIJBinaryWriter
{
	private FileStream mStream;

	public BIJTempFileBinaryWriter(string name)
		: this(name, Encoding.UTF8, false)
	{
	}

	public BIJTempFileBinaryWriter(string name, Encoding encoding, bool append)
	{
		mStream = null;
		Writer = null;
		try
		{
			FileMode mode = ((!append) ? FileMode.Create : FileMode.Append);
			mPath = System.IO.Path.Combine(GetBasePath(), name);
			string directoryName = System.IO.Path.GetDirectoryName(mPath);
			if (!string.IsNullOrEmpty(directoryName) && !Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			mStream = new FileStream(mPath, mode);
			Writer = new BinaryWriter(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	internal static string GetBasePath()
	{
		return BIJUnity.getTempPath();
	}

	public override void Close()
	{
		base.Close();
		try
		{
			mStream.Close();
		}
		catch (Exception)
		{
		}
		mStream = null;
	}
}
