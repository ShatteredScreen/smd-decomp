public class RatioChangeType2
{
	[MiniJSONAlias("Moves")]
	public int Moves { get; set; }

	public RatioChangeType2()
	{
		Moves = 10;
	}
}
