using UnityEngine;

public class ConfirmDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum CONFIRM_DIALOG_STYLE
	{
		OK_ONLY = 0,
		YES_NO = 1,
		CLOSEBUTTON = 2,
		OK_CANCEL = 3,
		CLOSE_ONLY = 4,
		RETRY_CLOSE = 5,
		RETRY_ONLY = 6,
		CONFIRM_CLOSE = 7,
		CONFIRM_ONLY = 8,
		TIMER = 9,
		IMAGE_CHOICE = 10,
		MANUAL_CLOSE = 11
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected CONFIRM_DIALOG_STYLE mConfirmStyle;

	protected DIALOG_SIZE mDialogSize;

	protected string mTitle;

	private string mDescription;

	private float mCloseTimer = 1f;

	private string mImagePOSIData = string.Empty;

	private string mImageNEGAData = string.Empty;

	protected int ReservedSortingOrder;

	protected bool mButtonSeEnable = true;

	private UILabel mDescLabel;

	private Color mDescColor = Color.white;

	private bool mChangeDescColor;

	protected bool mRequestCreateButton = true;

	private bool mCreateButton;

	protected OnDialogClosed mCallback;

	protected bool mIsUseTitle = true;

	public void SetUseTitle(bool a_flg)
	{
		mIsUseTitle = a_flg;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (!GetBusy() && mConfirmStyle == CONFIRM_DIALOG_STYLE.TIMER)
		{
			mCloseTimer -= Time.deltaTime;
			if (mCloseTimer <= 0f || mGame.mBackKeyTrg)
			{
				Close();
			}
		}
	}

	public override void BuildDialog()
	{
		if (ReservedSortingOrder > 0)
		{
			mPanel.sortingOrder = ReservedSortingOrder;
			mPanel.depth = ReservedSortingOrder;
		}
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(mDialogSize);
		if (mIsUseTitle)
		{
			InitDialogTitle(mTitle);
		}
		float y = -110f;
		switch (mDialogSize)
		{
		case DIALOG_SIZE.SMALL:
			y = 16f;
			break;
		case DIALOG_SIZE.MEDIUM:
			y = 50f;
			break;
		case DIALOG_SIZE.LARGE:
			y = 60f;
			break;
		case DIALOG_SIZE.XLARGE:
			y = 60f;
			break;
		}
		mDescLabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(mDescLabel, mDescription, mBaseDepth + 4, new Vector3(0f, y, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(mDescLabel);
		mDescLabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		mDescLabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		mDescLabel.spacingY = 6;
		if (mChangeDescColor)
		{
			mDescLabel.color = mDescColor;
		}
		if (mRequestCreateButton)
		{
			TryCreateButton();
		}
	}

	public void Init(string title, string desc, CONFIRM_DIALOG_STYLE style, bool openSeEnable = true, bool createButton = true)
	{
		Init(title, desc, style, DIALOG_SIZE.MEDIUM, mImagePOSIData, mImageNEGAData, createButton);
		SetOpenSeEnable(openSeEnable);
	}

	public void Init(string title, string desc, CONFIRM_DIALOG_STYLE style, DIALOG_SIZE size, string ImagePOSI = "", string ImageNEGA = "", bool createButton = true)
	{
		mTitle = title;
		mDescription = desc;
		mConfirmStyle = style;
		mDialogSize = size;
		mImagePOSIData = ImagePOSI;
		mImageNEGAData = ImageNEGA;
		mRequestCreateButton = createButton;
	}

	public bool TryCreateButton()
	{
		if (mCreateButton)
		{
			return false;
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		float y = -280f;
		switch (mDialogSize)
		{
		case DIALOG_SIZE.SMALL:
			y = -40f;
			break;
		case DIALOG_SIZE.MEDIUM:
			y = -90f;
			break;
		case DIALOG_SIZE.LARGE:
			y = -210f;
			break;
		case DIALOG_SIZE.XLARGE:
			y = -370f;
			break;
		}
		switch (mConfirmStyle)
		{
		case CONFIRM_DIALOG_STYLE.OK_ONLY:
		case CONFIRM_DIALOG_STYLE.CLOSE_ONLY:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			string text = "LC_button_yes";
			if (mConfirmStyle == CONFIRM_DIALOG_STYLE.CLOSE_ONLY)
			{
				text = "LC_button_close";
			}
			Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(0f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.CLOSEBUTTON:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			string text2 = "LC_button_close";
			if (mConfirmStyle == CONFIRM_DIALOG_STYLE.CLOSE_ONLY)
			{
				text2 = "LC_button_close";
			}
			Util.SetImageButtonInfo(button, text2, text2, text2, mBaseDepth + 4, new Vector3(0f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.RETRY_CLOSE:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_retry", "LC_button_retry", "LC_button_retry", mBaseDepth + 4, new Vector3(-130f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 4, new Vector3(130f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.RETRY_ONLY:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_retry", "LC_button_retry", "LC_button_retry", mBaseDepth + 4, new Vector3(0f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.CONFIRM_CLOSE:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_check", "LC_button_check", "LC_button_check", mBaseDepth + 4, new Vector3(-130f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_close", "LC_button_close", "LC_button_close", mBaseDepth + 4, new Vector3(130f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.CONFIRM_ONLY:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_check", "LC_button_check", "LC_button_check", mBaseDepth + 4, new Vector3(0f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.IMAGE_CHOICE:
		{
			float num = 130f;
			if (mImagePOSIData == string.Empty || mImageNEGAData == string.Empty)
			{
				num = 0f;
			}
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, mImagePOSIData, mImagePOSIData, mImagePOSIData, mBaseDepth + 4, new Vector3(0f - num, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, mImageNEGAData, mImageNEGAData, mImageNEGAData, mBaseDepth + 4, new Vector3(num, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		default:
		{
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(130f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			break;
		}
		case CONFIRM_DIALOG_STYLE.TIMER:
		case CONFIRM_DIALOG_STYLE.MANUAL_CLOSE:
			break;
		}
		mCreateButton = true;
		return true;
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (mButtonSeEnable)
			{
				mGame.PlaySe("SE_POSITIVE", -1);
			}
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			if (mButtonSeEnable)
			{
				mGame.PlaySe("SE_NEGATIVE", -1);
			}
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetCloseTimer(float timer)
	{
		mCloseTimer = timer;
	}

	public void SetButtonSeEnable(bool enable)
	{
		mButtonSeEnable = enable;
	}

	public void SetDescColor(Color color)
	{
		if (mDescLabel != null)
		{
			mDescLabel.color = color;
			return;
		}
		mDescColor = color;
		mChangeDescColor = true;
	}

	public void SetSortOrder(int a_order)
	{
		ReservedSortingOrder = a_order;
	}

	public SELECT_ITEM GetSelectItem()
	{
		return mSelectItem;
	}
}
