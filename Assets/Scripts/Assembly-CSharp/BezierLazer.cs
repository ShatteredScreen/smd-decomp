using UnityEngine;

public class BezierLazer : LazerInstanceBase
{
	private float mTime;

	private BezierCurve mCurve;

	public BezierLazer(Vector2 beginPoint, Vector2 endPoint, Vector2 controlPoint1, Vector2 controlPoint2, float time, int length, float width, string spriteName)
		: base(length, width, spriteName, STYLE.LAZER)
	{
		mCurve = new BezierCurve(beginPoint, endPoint, controlPoint1, controlPoint2);
		mTime = time;
	}

	public override void Update(float deltaTime)
	{
		if (mRatio >= 1f)
		{
			mLength--;
			if (mLength == 0)
			{
				Finish();
			}
			return;
		}
		mRatio += deltaTime / mTime;
		if (mRatio > 1f)
		{
			mRatio = 1f;
		}
		if (mLength < mMaxLength)
		{
			mLength++;
		}
		mTrail[mTrailPtr] = mCurve.GetPosition(mRatio);
		mForePoint = mTrail[mTrailPtr];
		mTrailPtr = (mTrailPtr + 1) % mMaxLength;
	}
}
