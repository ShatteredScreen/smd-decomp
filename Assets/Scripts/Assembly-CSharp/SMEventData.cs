using System.Collections.Generic;
using System.Linq;

public class SMEventData
{
	public string Filename = string.Empty;

	public Def.SERIES Series;

	public SortedDictionary<int, SMSeasonEventSetting> InSessionEventList = new SortedDictionary<int, SMSeasonEventSetting>();

	public SortedDictionary<int, SMRevivalEventSetting> RevivalEventList = new SortedDictionary<int, SMRevivalEventSetting>();

	public SMEventData()
	{
	}

	public SMEventData(string a_filename)
	{
		Filename = a_filename;
	}

	public virtual void Serialize(BIJFileBinaryWriter sw)
	{
		sw.WriteInt((int)Series);
		sw.WriteInt(InSessionEventList.Count);
		List<int> list = InSessionEventList.Keys.ToList();
		list.Sort();
		foreach (int item in list)
		{
			SMSeasonEventSetting sMSeasonEventSetting = InSessionEventList[item];
			sMSeasonEventSetting.Serialize(sw);
		}
		sw.WriteInt(RevivalEventList.Count);
		list = RevivalEventList.Keys.ToList();
		list.Sort();
		foreach (int item2 in list)
		{
			SMRevivalEventSetting sMRevivalEventSetting = RevivalEventList[item2];
			sMRevivalEventSetting.Serialize(sw);
		}
	}

	public virtual void Deserialize(BIJBinaryReader a_reader)
	{
		Series = (Def.SERIES)a_reader.ReadInt();
		int num = a_reader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			SMSeasonEventSetting sMSeasonEventSetting = new SMSeasonEventSetting();
			sMSeasonEventSetting.Deserialize(a_reader);
			AddInSessionEvent(sMSeasonEventSetting);
		}
		num = a_reader.ReadInt();
		for (int j = 0; j < num; j++)
		{
			SMRevivalEventSetting sMRevivalEventSetting = new SMRevivalEventSetting();
			sMRevivalEventSetting.Deserialize(a_reader);
			AddRevivalEvent(sMRevivalEventSetting);
		}
	}

	public void AddInSessionEvent(SMSeasonEventSetting a_data)
	{
		if (!InSessionEventList.ContainsKey(a_data.EventID))
		{
			InSessionEventList.Add(a_data.EventID, a_data);
		}
		else
		{
			InSessionEventList[a_data.EventID] = a_data;
		}
	}

	public void AddRevivalEvent(SMRevivalEventSetting a_data)
	{
		if (!RevivalEventList.ContainsKey(a_data.EventID))
		{
			RevivalEventList.Add(a_data.EventID, a_data);
		}
		else
		{
			RevivalEventList[a_data.EventID] = a_data;
		}
	}
}
