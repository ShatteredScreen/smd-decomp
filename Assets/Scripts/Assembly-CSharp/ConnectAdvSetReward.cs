public class ConnectAdvSetReward : ConnectBase
{
	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		ConnectAdvSetRewardParam connectAdvSetRewardParam = mParam as ConnectAdvSetRewardParam;
		if (connectAdvSetRewardParam == null)
		{
			BIJLog.E("Type Error Parameter:" + GetType().Name + "<=" + mParam.GetType().Name);
			SetServerResponse(1, -1);
			return;
		}
		MakeConnectGUID();
		if (!Server.AdvSetReward(connectAdvSetRewardParam.RewardIDList, connectAdvSetRewardParam.MailRewardIDList, mConnectGUID, (base.RetryCount > 0) ? 1 : 0, connectAdvSetRewardParam.Category))
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetAdvSetRewardFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
