using System;
using System.Reflection;
using System.Text;

public class GiftItem : ICloneable, IComparable
{
	public const ulong GIFT_ID_NONE = 0uL;

	private static ulong mGIFT_ID_SEED;

	public GiftItemData Data;

	public Def.ITEM_CATEGORY GiftItemCategory
	{
		get
		{
			return EnumHelper.FromObject(Data.ItemCategory, Def.ITEM_CATEGORY.NONE);
		}
	}

	public int CreateTimeEpoch
	{
		get
		{
			return (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(Data.CreateTime);
		}
		set
		{
			Data.CreateTime = DateTimeUtil.UnixTimeStampToDateTime(value, true);
		}
	}

	public DateTime RemoteTime
	{
		get
		{
			return Data.GetRemoteTimeToLocal();
		}
	}

	[MiniJSONAlias("getcount")]
	public int RemoteCount { get; set; }

	public GiftItem(GiftMailType aType = GiftMailType.LOCAL)
	{
		Data = new GiftItemData();
		Data.GiftSource = aType;
		Data.GiftID = GenerateGiftID(Data.GiftSource);
		Data.Quantity = 1;
		Data.CreateTime = new DateTime(0L);
		Data.RemoteTimeEpoch = 0;
	}

	public GiftItem(GiftItemData a_data)
	{
		SetData(a_data, true);
		Data.GiftID = GenerateGiftID(Data.GiftSource, Data.GiftID);
		if (a_data.Quantity == 0)
		{
			Data.Quantity = 1;
		}
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as GiftItem);
	}

	public static string GenerateGiftID(GiftMailType _src, string _gift_id = null)
	{
		string result = _gift_id;
		if (string.IsNullOrEmpty(_gift_id))
		{
			mGIFT_ID_SEED++;
			if (mGIFT_ID_SEED == 0L)
			{
				mGIFT_ID_SEED++;
			}
			result = string.Empty + mGIFT_ID_SEED;
		}
		else
		{
			ulong result2 = 0uL;
			if (ulong.TryParse(_gift_id, out result2))
			{
				mGIFT_ID_SEED = Math.Max(mGIFT_ID_SEED, result2);
			}
		}
		return result;
	}

	public GiftItem Clone()
	{
		return MemberwiseClone() as GiftItem;
	}

	public int CompareTo(GiftItem _obj)
	{
		if (_obj == null)
		{
			return int.MaxValue;
		}
		return -(CreateTimeEpoch - _obj.CreateTimeEpoch);
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[GiftItem: {0}]", stringBuilder.ToString());
	}

	public void CopyTo(object _obj)
	{
		CopyTo(_obj as GiftItem);
	}

	public void CopyTo(GiftItem _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(GiftItem);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}

	public void SetData(GiftItemData a_ref, bool a_idOver = false)
	{
		string giftID = string.Empty;
		if (!a_idOver && Data != null)
		{
			giftID = Data.GiftID;
		}
		Data = a_ref.Clone();
		if (!a_idOver)
		{
			Data.GiftID = giftID;
		}
	}

	public GiftMailType GetGiftSource()
	{
		return Data.GiftSource;
	}
}
