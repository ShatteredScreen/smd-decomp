using System.Collections.Generic;
using System.Text;

namespace live2d.framework
{
	public class L2DPhysics
	{
		private List<PhysicsHair> physicsList;

		private long startTimeMSec;

		public L2DPhysics()
		{
			physicsList = new List<PhysicsHair>();
			startTimeMSec = UtSystem.getUserTimeMSec();
		}

		private void addParam(PhysicsHair phisics)
		{
			physicsList.Add(phisics);
		}

		public void updateParam(ALive2DModel model)
		{
			long time = UtSystem.getUserTimeMSec() - startTimeMSec;
			for (int i = 0; i < physicsList.Count; i++)
			{
				physicsList[i].update(model, time);
			}
		}

		public static L2DPhysics load(byte[] buf)
		{
			return load(Encoding.GetEncoding("UTF-8").GetString(buf));
		}

		public static L2DPhysics load(string buf)
		{
			return load(buf.ToCharArray());
		}

		public static L2DPhysics load(char[] buf)
		{
			L2DPhysics l2DPhysics = new L2DPhysics();
			Value value = Json.parseFromBytes(buf);
			List<Value> vector = value.get("physics_hair").getVector(null);
			int count = vector.Count;
			for (int i = 0; i < count; i++)
			{
				Value value2 = vector[i];
				PhysicsHair physicsHair = new PhysicsHair();
				Value value3 = value2.get("setup");
				float baseLengthM = value3.get("length").toFloat();
				float airRegistance = value3.get("regist").toFloat();
				float mass = value3.get("mass").toFloat();
				physicsHair.setup(baseLengthM, airRegistance, mass);
				List<Value> vector2 = value2.get("src").getVector(null);
				int count2 = vector2.Count;
				for (int j = 0; j < count2; j++)
				{
					Value value4 = vector2[j];
					string paramID = value4.get("id").toString();
					PhysicsHair.Src srcType = PhysicsHair.Src.SRC_TO_X;
					switch (value4.get("ptype").toString())
					{
					case "x":
						srcType = PhysicsHair.Src.SRC_TO_X;
						break;
					case "y":
						srcType = PhysicsHair.Src.SRC_TO_Y;
						break;
					case "angle":
						srcType = PhysicsHair.Src.SRC_TO_G_ANGLE;
						break;
					default:
						UtDebug.error("live2d", "Invalid value. PhysicsHair.Src");
						break;
					}
					float scale = value4.get("scale").toFloat();
					float weight = value4.get("weight").toFloat();
					physicsHair.addSrcParam(srcType, paramID, scale, weight);
				}
				List<Value> vector3 = value2.get("targets").getVector(null);
				int count3 = vector3.Count;
				for (int k = 0; k < count3; k++)
				{
					Value value5 = vector3[k];
					string paramID2 = value5.get("id").toString();
					PhysicsHair.Target targetType = PhysicsHair.Target.TARGET_FROM_ANGLE;
					string text = value5.get("ptype").toString();
					if (text == "angle")
					{
						targetType = PhysicsHair.Target.TARGET_FROM_ANGLE;
					}
					else if (text == "angle_v")
					{
						targetType = PhysicsHair.Target.TARGET_FROM_ANGLE_V;
					}
					else
					{
						UtDebug.error("live2d", "Invalid value. PhysicsHair.Target");
					}
					float scale2 = value5.get("scale").toFloat();
					float weight2 = value5.get("weight").toFloat();
					physicsHair.addTargetParam(targetType, paramID2, scale2, weight2);
				}
				l2DPhysics.addParam(physicsHair);
			}
			return l2DPhysics;
		}
	}
}
