public class ConnectAdvSetRewardParam : ConnectParameterBase
{
	public int[] RewardIDList { get; private set; }

	public int[] MailRewardIDList { get; private set; }

	public string Category { get; private set; }

	public ConnectAdvSetRewardParam(int[] a_list, int[] a_maillist, string a_category = "quest")
	{
		RewardIDList = new int[a_list.Length];
		for (int i = 0; i < a_list.Length; i++)
		{
			RewardIDList[i] = a_list[i];
		}
		MailRewardIDList = new int[a_maillist.Length];
		for (int j = 0; j < a_maillist.Length; j++)
		{
			MailRewardIDList[j] = a_maillist[j];
		}
		Category = a_category;
	}
}
