using UnityEngine;

public class BIJPointf
{
	public static readonly BIJPointf EMPTY = new BIJPointf(0f, 0f);

	public float X { get; set; }

	public float Y { get; set; }

	public BIJPointf()
		: this(0f, 0f)
	{
	}

	public BIJPointf(BIJPointf p)
	{
		X = p.X;
		Y = p.Y;
	}

	public BIJPointf(float x, float y)
	{
		X = x;
		Y = y;
	}

	public override string ToString()
	{
		return string.Format("({0,0}, {1,0})", X, Y);
	}

	public static float Dot(BIJPointf a, BIJPointf b)
	{
		return a.X * b.X + a.Y * b.Y;
	}

	public static float Cross(BIJPointf a, BIJPointf b)
	{
		return a.X * b.Y - b.X * a.Y;
	}

	public static bool IsInsideTriangle(BIJPointf p, BIJPointf a, BIJPointf b, BIJPointf c)
	{
		if (Cross(p - a, b - a) < 0f)
		{
			return false;
		}
		if (Cross(p - b, c - b) < 0f)
		{
			return false;
		}
		if (Cross(p - c, a - c) < 0f)
		{
			return false;
		}
		return true;
	}

	public static float Distance(BIJPointf a, BIJPointf b)
	{
		return Mathf.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
	}

	public static float DistanceCmpValue(BIJPointf a, BIJPointf b)
	{
		return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
	}

	public static BIJPointf Normal(BIJPointf a)
	{
		float num = Mathf.Sqrt(a.X * a.X + a.Y * a.Y);
		return new BIJPointf(a.X / num, a.Y / num);
	}

	public virtual void Serialize(BIJBinaryWriter data)
	{
		data.WriteFloat(X);
		data.WriteFloat(Y);
	}

	public static BIJPointf Deserialize(BIJBinaryReader data)
	{
		float x = data.ReadFloat();
		float y = data.ReadFloat();
		return new BIJPointf(x, y);
	}

	public static explicit operator Vector2(BIJPointf rhs)
	{
		return new Vector2(rhs.X, rhs.Y);
	}

	public static BIJPointf operator +(BIJPointf a, BIJPointf b)
	{
		return new BIJPointf(a.X + b.X, a.Y + b.Y);
	}

	public static BIJPointf operator -(BIJPointf a, BIJPointf b)
	{
		return new BIJPointf(a.X - b.X, a.Y - b.Y);
	}
}
