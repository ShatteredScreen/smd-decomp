using UnityEngine;

public class ScrollWindow : MonoBehaviour
{
	private class AdvGashaMenuButtonInstance : MonoBehaviour
	{
		public delegate void OnMenuButtonPushed(int i);

		public UITexture texture;

		private OnMenuButtonPushed mCallback;

		public int index;

		public bool FirstButtonCP;

		public int AdditionalHeight { get; private set; }

		public int ImgHeight
		{
			get
			{
				int num = 0;
				if (texture != null)
				{
					num += texture.height;
				}
				return num + AdditionalHeight;
			}
		}

		public static AdvGashaMenuButtonInstance make(GameObject _parent, float _posY, int _Countindex, int _CallbackIndex, int _Space, int _baseDepth, Texture2D _textureData, OnMenuButtonPushed _mCallback, int _textNum, BIJImage _Atlas)
		{
			GameObject gameObject = Util.CreateGameObject("sw_menu" + _Countindex, _parent);
			AdvGashaMenuButtonInstance advGashaMenuButtonInstance = gameObject.AddComponent<AdvGashaMenuButtonInstance>();
			advGashaMenuButtonInstance.texture = Util.CreateJellyTextureButton("sw_button" + _Countindex, advGashaMenuButtonInstance.gameObject, _textureData);
			Util.SetTextureButtonCallback(advGashaMenuButtonInstance.texture, advGashaMenuButtonInstance, "OnMenuButtonCallBack", UIButtonMessage.Trigger.OnClick);
			if (_Countindex == 0)
			{
				advGashaMenuButtonInstance.transform.localPosition = new Vector3(0f, _posY, 0f);
			}
			else
			{
				advGashaMenuButtonInstance.transform.localPosition = new Vector3(0f, _posY - (float)(advGashaMenuButtonInstance.texture.height / 2), 0f);
			}
			advGashaMenuButtonInstance.index = _CallbackIndex;
			advGashaMenuButtonInstance.mCallback = _mCallback;
			UIDragScrollView uIDragScrollView = advGashaMenuButtonInstance.texture.gameObject.AddComponent<UIDragScrollView>();
			uIDragScrollView.scrollView = _parent.transform.parent.gameObject.GetComponent<UIScrollView>();
			bool flag = false;
			if (0 < _textNum && _Atlas != null)
			{
				LoopAnime loopAnime = Util.CreateLoopAnime("Campaign_anim", "ADV_GASYA_CAMPAIGN_ANIM", advGashaMenuButtonInstance.gameObject, new Vector3(-advGashaMenuButtonInstance.texture.width / 2 - 2, -advGashaMenuButtonInstance.texture.height / 2 + 250 - 42, 0f), new Vector3(0.95f, 0.95f, 1f), 0f, 0f, true, null);
				UISsSpriteP uISsSpriteP = Util.CreateUISsSpriteP("CampaignShine_anim", advGashaMenuButtonInstance.gameObject, "ADV_GASYA_CAMPAIGN_SHINE", new Vector3(0f, 0f, 0f), Vector3.one, _baseDepth + 10);
				uISsSpriteP.PlayCount = 0;
			}
			if (_Countindex != 0)
			{
				if (flag)
				{
					GameObject gameObject2 = Util.CreateGameObject("sw_space" + _Countindex, gameObject);
					BoxCollider boxCollider = gameObject2.AddComponent<BoxCollider>();
					boxCollider.size = new Vector3(advGashaMenuButtonInstance.texture.width, _Space + 20, 0f);
					gameObject2.transform.localPosition = new Vector3(0f, advGashaMenuButtonInstance.texture.height / 2 + _Space / 2 + 10, 0f);
					UIDragScrollView uIDragScrollView2 = gameObject2.gameObject.AddComponent<UIDragScrollView>();
					uIDragScrollView2.scrollView = _parent.transform.parent.gameObject.GetComponent<UIScrollView>();
					gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y - 20f, gameObject.transform.localPosition.z);
				}
				else
				{
					GameObject gameObject3 = Util.CreateGameObject("sw_space" + _Countindex, gameObject);
					BoxCollider boxCollider2 = gameObject3.AddComponent<BoxCollider>();
					boxCollider2.size = new Vector3(advGashaMenuButtonInstance.texture.width, _Space, 0f);
					gameObject3.transform.localPosition = new Vector3(0f, advGashaMenuButtonInstance.texture.height / 2 + _Space / 2, 0f);
					UIDragScrollView uIDragScrollView3 = gameObject3.gameObject.AddComponent<UIDragScrollView>();
					uIDragScrollView3.scrollView = _parent.transform.parent.gameObject.GetComponent<UIScrollView>();
				}
			}
			else if (flag)
			{
				advGashaMenuButtonInstance.FirstButtonCP = true;
			}
			return advGashaMenuButtonInstance;
		}

		public static AdvGashaMenuButtonInstance make(GameObject _parent, float _posY, int _Countindex, int _CallbackIndex, int _Space, int _baseDepth, Texture2D _textureData, OnMenuButtonPushed _mCallback, int _textNum, BIJImage _Atlas, string localized_footer_msg)
		{
			AdvGashaMenuButtonInstance advGashaMenuButtonInstance = make(_parent, _posY, _Countindex, _CallbackIndex, _Space, _baseDepth, _textureData, _mCallback, _textNum, _Atlas);
			if (!string.IsNullOrEmpty(localized_footer_msg))
			{
				Vector2 vector = ((!(advGashaMenuButtonInstance.texture != null)) ? Vector2.zero : new Vector2(advGashaMenuButtonInstance.texture.width, advGashaMenuButtonInstance.texture.height));
				int num = 5;
				int num2 = 24;
				int num3 = 36;
				UISprite uISprite = Util.CreateSprite("sw_footer", advGashaMenuButtonInstance.gameObject, _Atlas);
				Util.SetSpriteInfo(uISprite, "oldevent_panel_timelimit", _baseDepth + 1, new Vector3(0f, 0f - (vector.y * 0.5f + (float)num + (float)num3 * 0.5f)), Vector3.one, false);
				uISprite.type = UIBasicSprite.Type.Sliced;
				uISprite.SetDimensions((int)vector.x, num3);
				UILabel label = Util.CreateLabel("label", uISprite.gameObject, GameMain.LoadFont());
				Util.SetLabelInfo(label, localized_footer_msg, _baseDepth + 2, Vector3.zero, num2, 0, 0, UIWidget.Pivot.Center);
				Util.SetLabelColor(label, new Color32(byte.MaxValue, 240, 113, byte.MaxValue));
				Util.SetLabeShrinkContent(label, (int)vector.x, num2);
				advGashaMenuButtonInstance.AdditionalHeight = num + num3;
			}
			return advGashaMenuButtonInstance;
		}

		public int GetImgHeight()
		{
			return texture.height;
		}

		private void OnMenuButtonCallBack()
		{
			mCallback(index);
		}
	}

	public delegate void OnScrollMenuButtonPushed(int i);

	private float mWidth;

	private float mHeight;

	private int mBaseDepth;

	public GameObject RootGo;

	public GameObject ScrollWindowRoot;

	private GameObject DragRoot;

	private int mSpace;

	public GameObject sbGo;

	public bool scrollbarEnable;

	private OnScrollMenuButtonPushed mCallback;

	private int mCount;

	private float BeforeBottom;

	public bool mFirstButtonCP;

	public GameObject firstButtonGo
	{
		get
		{
			return GameObject.Find("sw_button0");
		}
	}

	public static ScrollWindow make(GameObject _parent, int _baseDepth, Vector3 _rootPos, float _width, float _height, int _Space)
	{
		GameObject gameObject = Util.CreateGameObject("ScrollWindowRoot", _parent);
		gameObject.transform.localPosition = _rootPos;
		ScrollWindow scrollWindow = gameObject.AddComponent<ScrollWindow>();
		scrollWindow.RootGo = gameObject;
		scrollWindow.mBaseDepth = _baseDepth;
		scrollWindow.mWidth = _width;
		scrollWindow.mHeight = _height;
		scrollWindow.mSpace = _Space;
		scrollWindow.ScrollWindowRoot = Util.CreateGameObject("ScrollWindow", gameObject);
		UIScrollView uIScrollView = scrollWindow.ScrollWindowRoot.AddComponent<UIScrollView>();
		uIScrollView.movement = UIScrollView.Movement.Vertical;
		uIScrollView.disableDragIfFits = true;
		UIPanel component = uIScrollView.GetComponent<UIPanel>();
		component.sortingOrder = scrollWindow.mBaseDepth;
		component.depth = scrollWindow.mBaseDepth;
		component.clipping = UIDrawCall.Clipping.SoftClip;
		component.baseClipRegion = new Vector4(0f, 0f, scrollWindow.mWidth, scrollWindow.mHeight);
		component.clipSoftness = new Vector2(0f, 4f);
		scrollWindow.DragRoot = Util.CreateGameObject("List", scrollWindow.ScrollWindowRoot);
		UIDragScrollView uIDragScrollView = scrollWindow.DragRoot.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = uIScrollView;
		return scrollWindow;
	}

	public void CreateScrollBar(BIJImage _Atlas, string _ScrollCursorImg, string _ScrollBackImg)
	{
		scrollbarEnable = true;
		sbGo = Util.CreateGameObject("Scroll", RootGo);
		UIScrollBar uIScrollBar = sbGo.AddComponent<UIScrollBar>();
		int num = 0;
		float num2 = 14f;
		Vector3 localPosition = new Vector3(mWidth / 2f + num2, 0f, 0f);
		num = (int)mHeight;
		uIScrollBar.value = 0f;
		uIScrollBar.gameObject.transform.localPosition = localPosition;
		UISprite uISprite = Util.CreateSprite("scrollCursor", sbGo, _Atlas);
		Util.SetSpriteInfo(uISprite, _ScrollCursorImg, mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 20;
		uISprite.height = num;
		UISprite uISprite2 = Util.CreateSprite("scrollBack", sbGo, _Atlas);
		Util.SetSpriteInfo(uISprite2, _ScrollBackImg, mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.width = 20;
		uISprite2.height = num;
		uIScrollBar.fillDirection = UIProgressBar.FillDirection.TopToBottom;
		uIScrollBar.foregroundWidget = uISprite;
		uIScrollBar.backgroundWidget = uISprite2;
		UIScrollView component = ScrollWindowRoot.GetComponent<UIScrollView>();
		component.verticalScrollBar = uIScrollBar;
	}

	public void setCallBack(OnScrollMenuButtonPushed _mCallback)
	{
		mCallback = _mCallback;
	}

	private void OnMenuButtonPushed(int index)
	{
		if (mCallback != null)
		{
			mCallback(index);
		}
	}

	public void SetEnableButtonAction(bool buttonflg)
	{
		for (int i = 0; i < mCount; i++)
		{
			GameObject gameObject = GameObject.Find("sw_button" + i);
			if (gameObject != null)
			{
				BoxCollider component = gameObject.GetComponent<BoxCollider>();
				if (component != null)
				{
					component.enabled = buttonflg;
				}
			}
			gameObject = GameObject.Find("sw_space" + i);
			if (gameObject != null)
			{
				BoxCollider component = gameObject.GetComponent<BoxCollider>();
				if (component != null)
				{
					component.enabled = buttonflg;
				}
			}
		}
	}

	public void AddMenuButton(int _CallbackIndex, Texture2D _textureData, int _textNum = 0, BIJImage _Atlas = null, string localized_footer_msg = null)
	{
		if (!(_textureData == null))
		{
			float posY = 0f;
			if (mCount != 0)
			{
				posY = BeforeBottom - (float)mSpace;
			}
			AdvGashaMenuButtonInstance advGashaMenuButtonInstance = null;
			advGashaMenuButtonInstance = ((!string.IsNullOrEmpty(localized_footer_msg)) ? AdvGashaMenuButtonInstance.make(DragRoot, posY, mCount, _CallbackIndex, mSpace, mBaseDepth, _textureData, OnMenuButtonPushed, _textNum, _Atlas, localized_footer_msg) : AdvGashaMenuButtonInstance.make(DragRoot, posY, mCount, _CallbackIndex, mSpace, mBaseDepth, _textureData, OnMenuButtonPushed, _textNum, _Atlas));
			GameObject gameObject = advGashaMenuButtonInstance.texture.transform.parent.gameObject;
			if (mCount == 0)
			{
				gameObject.transform.localPosition = new Vector3(0f, gameObject.transform.localPosition.y + (mHeight / 2f - (float)(advGashaMenuButtonInstance.GetImgHeight() / 2) - (float)advGashaMenuButtonInstance.AdditionalHeight - 4f), 0f);
			}
			mCount++;
			BeforeBottom = advGashaMenuButtonInstance.transform.localPosition.y - (float)(advGashaMenuButtonInstance.GetImgHeight() / 2) - (float)advGashaMenuButtonInstance.AdditionalHeight;
			if (advGashaMenuButtonInstance.FirstButtonCP)
			{
				mFirstButtonCP = true;
			}
		}
	}
}
