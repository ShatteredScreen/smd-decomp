using System.Collections.Generic;

public interface IPlayer
{
	int HiveId { get; }

	int UUID { get; }

	string NickName { get; }

	string SearchID { get; }

	int SeriesToS { get; }

	int LevelToS { get; }

	int CompanionIDToS { get; }

	int CompanionLevelToS { get; }

	int MasterCurrency { get; }

	int SecondaryCurrency { get; }

	int Experience { get; }

	int ClearSeriesToS { get; }

	int ClearStageNoToS { get; }

	int LastSeriesToS { get; }

	int LastStageNoToS { get; }

	long TotalScoreToS { get; }

	long TotalStarToS { get; }

	long TotalTrophyToS { get; }

	long ServerTimeToS { get; }

	long PlayDays { get; }

	long IntervalDays { get; }

	long ContinuousDays { get; }

	List<PlayStageParam> PlayStage { get; }

	int AdvLastStage { get; }
}
