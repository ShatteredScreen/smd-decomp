public class RequestBase : ParameterObject<RequestBase>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	public RequestBase()
	{
		UDID = Network.BNIDPublishID;
		OpenUDID = Network.OpenUDID;
		UniqueID = Network.BNIDAuthID;
	}
}
