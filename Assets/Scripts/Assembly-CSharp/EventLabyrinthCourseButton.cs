using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventLabyrinthCourseButton : MapStageButton
{
	public enum StatusChangeType
	{
		NONE = 0,
		TO_UNLOCK = 1
	}

	public const int FRAME_LOCK = 0;

	public const int FRAME_UNLOCK = 0;

	public const int FRAME_UNLOCK_LOOP = 50;

	private Color mColor;

	private LabyrinthCourseInfoObject mCourseInfo;

	protected short mCourseID;

	private StatusChangeType mButtonChangeType;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public void SetStatusTypeReserved(StatusChangeType a_flg)
	{
		mButtonChangeType = a_flg;
	}

	protected override void UpdateButton_StateOn()
	{
		float num = mButtonReactionScaleX;
		float num2 = mButtonReactionScaleY;
		float num3 = mButtonReactionSpeed;
		float num4 = mMaxSizeAngle;
		mButtonReactionScaleX = -0.4f;
		mButtonReactionScaleY = -0.4f;
		num3 = 480f;
		num4 = 90f;
		base.UpdateButton_StateOn();
		mButtonReactionScaleX = num;
		mButtonReactionScaleY = num2;
		mButtonReactionSpeed = num3;
		mMaxSizeAngle = num4;
	}

	protected override void UpdateButton_StateOff()
	{
		float num = mButtonReactionScaleX;
		float num2 = mButtonReactionScaleY;
		mButtonReactionScaleX = -0.4f;
		mButtonReactionScaleY = -0.4f;
		base.UpdateButton_StateOff();
		mButtonReactionScaleX = num;
		mButtonReactionScaleY = num2;
	}

	protected override void UpdateButton_StateLockOn()
	{
		float num = mButtonReactionScaleX;
		float num2 = mButtonReactionScaleY;
		float num3 = mButtonReactionSpeed;
		float num4 = mMaxSizeAngle;
		mButtonReactionScaleX = -0.4f;
		mButtonReactionScaleY = -0.4f;
		num3 = 640f;
		num4 = 90f;
		base.UpdateButton_StateLockOn();
		mButtonReactionScaleX = num;
		mButtonReactionScaleY = num2;
		mButtonReactionSpeed = num3;
		mMaxSizeAngle = num4;
	}

	protected override void UpdateButton_StateLockOff()
	{
		float num = mButtonReactionScaleX;
		float num2 = mButtonReactionScaleY;
		mButtonReactionScaleX = -0.4f;
		mButtonReactionScaleY = -0.4f;
		base.UpdateButton_StateLockOff();
		mButtonReactionScaleX = num;
		mButtonReactionScaleY = num2;
	}

	public override void Init(Vector3 pos, float scale, int stageNo, byte starNum, Player.STAGE_STATUS mode, BIJImage atlas, string a_animeKey, bool a_hasItem = false, int displayNo = -1)
	{
		mUseColliderReciprocal = true;
		base.DEFAULT_SCALE = 1f;
		mAtlas = atlas;
		mBaseScale = scale;
		mStarNum = starNum;
		mStageNo = stageNo;
		mHasItem = a_hasItem;
		DefaultAnimeKey = a_animeKey;
		DisplayNumber = displayNo;
		bool flag = false;
		bool force = false;
		if (mMode != mode)
		{
			force = true;
		}
		mMode = mode;
		mColliderSize = 300;
		string defaultAnimeKey = DefaultAnimeKey;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out data);
		SMMapPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		SMEventPageData sMEventPageData = currentEventPageData as SMEventPageData;
		Vector3 pos2 = new Vector3(0f, 20f, -5f);
		if (mCourseInfo == null)
		{
			mCourseInfo = Util.CreateGameObject("Info" + stageNo, base.gameObject).AddComponent<LabyrinthCourseInfoObject>();
			flag = true;
		}
		short a_stage = 0;
		short a_star = 0;
		data.GetEventClearStarCount(mCourseID, out a_stage, out a_star);
		if (a_stage > 9)
		{
			a_stage = 9;
		}
		if (a_star > 27)
		{
			a_star = 27;
		}
		mCourseInfo.Init(pos2, stageNo, sMEventPageData, mode == Player.STAGE_STATUS.UNLOCK || mode == Player.STAGE_STATUS.CLEAR, a_stage, a_star);
		mColor = Color.white;
		int a_main;
		int a_sub;
		Def.SplitStageNo(mStageNo, out a_main, out a_sub);
		int num = -1;
		bool flag2 = false;
		switch (mode)
		{
		case Player.STAGE_STATUS.LOCK:
			num = 0;
			flag2 = true;
			break;
		default:
			if (mButtonChangeType == StatusChangeType.TO_UNLOCK)
			{
				num = 0;
				flag2 = true;
			}
			else
			{
				num = 50;
			}
			break;
		}
		if (flag && mode == Player.STAGE_STATUS.LOCK && mFukidashiDisplayFlg)
		{
			BIJImage image = ResourceManager.LoadImage("EVENTLABYRINTH").Image;
			int a_main2;
			int a_sub2;
			Def.SplitStageNo(mStageNo, out a_main2, out a_sub2);
			SMEventCourseSetting sMEventCourseSetting = sMEventPageData.EventCourseList[a_main2 - 1];
			int openAccessoryID = sMEventCourseSetting.OpenAccessoryID;
			List<SMEventRewardSetting> list = new List<SMEventRewardSetting>(sMEventPageData.EventRewardList.Values);
			int num2 = 0;
			int num3 = 0;
			if (data.LabyrinthData != null)
			{
				num3 = data.LabyrinthData.JewelAmount;
			}
			foreach (SMEventRewardSetting item4 in list)
			{
				if (item4.RewardID == openAccessoryID)
				{
					num2 = item4.TotalNum - num3;
					if (num2 < 0)
					{
						num2 = 0;
					}
					break;
				}
			}
			CHANGE_PART_INFO[] ketaPartsInfo = GetKetaPartsInfo(num2);
			SetFukidashi(true, "LABYRINTH_COURSEBUTTON_FUKIDASHI", new Vector3(120f, -80f, -0.5f), ketaPartsInfo, image, true);
		}
		ChangeAnime(defaultAnimeKey, null, force, 1);
		_sprite.transform.localScale = new Vector3(mBaseScale, mBaseScale, 1f);
		_sprite.transform.localPosition = pos;
		_sprite.AnimFrame = num;
		_sprite.PlayAtStart = false;
		if (flag2)
		{
			_sprite.AnimationFinished = null;
			_sprite.Pause();
		}
		else
		{
			_sprite.AnimationFinished = OnAnimationFinished;
			_sprite.Play();
		}
		mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
		if (!mFirstInitialized)
		{
			EventDelegate item = new EventDelegate(this, "OnButtonPushed");
			mEventTrigger.onPress.Add(item);
			EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
			mEventTrigger.onRelease.Add(item2);
			EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
			mEventTrigger.onClick.Add(item3);
		}
		mFirstInitialized = true;
	}

	public void OnAnimationFinished(SsSprite sprite)
	{
		sprite.AnimFrame = 50f;
		sprite.Play();
	}

	private CHANGE_PART_INFO[] GetKetaPartsInfo(int a_num)
	{
		int num = a_num;
		if (num < 0)
		{
			num = 0;
		}
		List<int> list = new List<int>();
		if (num > 0)
		{
			while (num > 0)
			{
				list.Add(num % 10);
				num /= 10;
			}
		}
		else
		{
			list.Add(0);
		}
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[15]
		{
			new CHANGE_PART_INFO
			{
				partName = "one_1",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "ten_1",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "ten_2",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "hundred_1",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "hundred_2",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "hundred_3",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "thousand_1",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "thousand_2",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "thousand_3",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "thousand_4",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "ten_thousand_1",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "ten_thousand_2",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "ten_thousand_3",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "ten_thousand_4",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "ten_thousand_5",
				spriteName = "null"
			}
		};
		int num2 = 0;
		string text = string.Empty;
		switch (list.Count)
		{
		case 1:
			num2 = 0;
			text = "one_";
			break;
		case 2:
			num2 = 1;
			text = "ten_";
			break;
		case 3:
			num2 = 3;
			text = "hundred_";
			break;
		case 4:
			num2 = 6;
			text = "thousand_";
			break;
		case 5:
			num2 = 10;
			text = "ten_thousand_";
			break;
		}
		for (int i = 0; i < list.Count; i++)
		{
			CHANGE_PART_INFO cHANGE_PART_INFO = array[num2 + i];
			cHANGE_PART_INFO.partName = text + (i + 1);
			cHANGE_PART_INFO.spriteName = "event_open_num" + list[i];
			array[num2 + i] = cHANGE_PART_INFO;
		}
		return array;
	}

	public override void SetDebugInfo(SMMapStageSetting a_setting)
	{
		if (a_setting == null)
		{
		}
	}

	public override void SetColor(Color col)
	{
	}

	public IEnumerator UnlockCourse()
	{
		SetStatusTypeReserved(StatusChangeType.NONE);
		SetFukidashi(false, string.Empty, string.Empty);
		yield return new WaitForSeconds(0.3f);
		_sprite.AnimFrame = 0f;
		_sprite.AnimationFinished = OnAnimationFinished;
		_sprite.Play();
		mGame.PlaySe("SE_LABYRINTH_COURSE_RELEASE", 2);
		SsPart part = _sprite.GetPart("course");
		if (part == null)
		{
			yield break;
		}
		bool animeFinished = false;
		SsPart.KeyframeCallback handler2 = null;
		handler2 = delegate(SsPart a_part, SsAttrValueInterface a_val)
		{
			SsUserDataKeyValue ssUserDataKeyValue = a_val as SsUserDataKeyValue;
			if (ssUserDataKeyValue.String.CompareTo("CLEAR_FINISH") == 0)
			{
				a_part.OnUserDataKey -= handler2;
				animeFinished = true;
			}
		};
		part.OnUserDataKey += handler2;
		float _start = Time.realtimeSinceStartup;
		while (!animeFinished)
		{
			float timeSpan = Time.realtimeSinceStartup - _start;
			if (timeSpan > 3f)
			{
				animeFinished = true;
			}
			if (animeFinished)
			{
				break;
			}
			yield return null;
		}
		mMode = Player.STAGE_STATUS.UNLOCK;
	}

	public IEnumerator OpenFukidashi()
	{
		SMMapPageData mapdata = GameMain.GetCurrentEventPageData();
		SMEventPageData eventMapData = mapdata as SMEventPageData;
		PlayerEventData eventData;
		mGame.mPlayer.Data.GetMapData(mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.CurrentEventID, out eventData);
		mFukidashiDisplayFlg = true;
		BIJImage labyrinthAtlas = ResourceManager.LoadImage("EVENTLABYRINTH").Image;
		int course;
		int sub_temp;
		Def.SplitStageNo(mStageNo, out course, out sub_temp);
		SMEventCourseSetting course_setting = eventMapData.EventCourseList[course - 1];
		int accessoryID = course_setting.OpenAccessoryID;
		List<SMEventRewardSetting> rewardList = new List<SMEventRewardSetting>(eventMapData.EventRewardList.Values);
		int reward_num = 0;
		int jewel = 0;
		if (eventData.LabyrinthData != null)
		{
			jewel = eventData.LabyrinthData.JewelAmount;
		}
		foreach (SMEventRewardSetting rewardSetting in rewardList)
		{
			if (rewardSetting.RewardID == accessoryID)
			{
				reward_num = rewardSetting.TotalNum - jewel;
				if (reward_num < 0)
				{
					reward_num = 0;
				}
				break;
			}
		}
		SetFukidashi(a_info: GetKetaPartsInfo(reward_num), a_flg: true, a_anime: "LABYRINTH_COURSEBUTTON_FUKIDASHI", a_pos: new Vector3(120f, -80f, -0.5f), a_atlas: labyrinthAtlas, a_loop: true);
		yield return new WaitForSeconds(0.3f);
	}
}
