public class AdvExchangeRewards
{
	[MiniJSONAlias("reward_id")]
	public int reward_id { get; set; }

	[MiniJSONAlias("receive_type")]
	public int receive_type { get; set; }

	[MiniJSONAlias("to_mailbox")]
	public int to_mailbox { get; set; }

	[MiniJSONAlias("category")]
	public int category { get; set; }

	[MiniJSONAlias("sub_category")]
	public int sub_category { get; set; }

	[MiniJSONAlias("itemid")]
	public int itemid { get; set; }

	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }

	[MiniJSONAlias("accessory_id")]
	public int accessory_id { get; set; }

	[MiniJSONAlias("exchange_quantity")]
	public int exchange_quantity { get; set; }
}
