using System.Collections.Generic;
using UnityEngine;

public class GetSPStampDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(int i, AccessoryData a);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	public int mAfter;

	private AccessoryData mGetAccessoryData;

	public AccessoryData mData;

	private bool mSedondRound;

	private string mIconName = "null";

	private string mText = string.Empty;

	private string mLastStage = string.Empty;

	private string mImagename = string.Empty;

	private int KeyCount;

	private bool mIsGotGem;

	private int mCount;

	private int mNumberCount;

	private float mXleft;

	private float mYdown;

	private bool mStampComplete;

	private bool mOKEnable = true;

	private bool mStampEnable = true;

	private bool mStampLastEnable = true;

	private UISsSprite mUISsAnime;

	private ResSsAnimation mResSsAnime;

	private BIJImage atlas;

	private UILabel mLabelText;

	private string mHowToURL = string.Empty;

	private int mPlaySeFlg;

	private float mCounter;

	private float mStampCounter;

	private SPDailyDialog mSPDailyDialog;

	public long mStarttime;

	public long mEndtime;

	public int mSpecialEventID;

	public int mImageID;

	public int mSPNum;

	public List<int> mAccessoryList;

	public List<int> mBackPanelRGBA;

	public bool IsLastSheet { get; private set; }

	public bool IsAfterComplete { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(AccessoryData a_data)
	{
		mData = a_data;
		mCount = a_data.GetGotIDByNum();
	}

	public void Init(int num)
	{
		mCount = num;
		mSedondRound = true;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		if (mCount != 0)
		{
			mStampCounter += Time.deltaTime;
			if (mStampCounter > 0.2f)
			{
				mNumberCount++;
				if (mPlaySeFlg == 0)
				{
					mGame.PlaySe("SE_SPDAILY_STAMP", -1, 1f, 0.5f);
					mPlaySeFlg = 1;
				}
				else if (mPlaySeFlg == 1)
				{
					mGame.PlaySe("SE_SPDAILY_STAMP", 1, 1f, 0.5f);
					mPlaySeFlg = 2;
				}
				else if (mPlaySeFlg == 2)
				{
					mGame.PlaySe("SE_SPDAILY_STAMP", 3, 1f, 0.5f);
					mPlaySeFlg = 3;
				}
				else
				{
					mGame.PlaySe("SE_SPDAILY_STAMP", 2, 1f, 0.5f);
					mPlaySeFlg = 0;
				}
				mResSsAnime = ResourceManager.LoadSsAnimation("EFFECT_STAMP_DAILY");
				SsAnimation ssAnime = mResSsAnime.SsAnime;
				SsPartRes[] partList = ssAnime.PartList;
				for (int i = 0; i < partList.Length; i++)
				{
					string changeName = "icon_specialdaily_stamp0" + mImageID;
					if ("stamp".CompareTo(partList[i].Name) == 0)
					{
						Vector2[] uv;
						Vector3[] verts;
						GetTextureParts(changeName, partList[i].OriginX, partList[i].OriginY, out uv, out verts);
						partList[i].UVs = uv;
						partList[i].OrgVertices = verts;
						UISsSprite uISsSprite = Util.CreateGameObject("SPStampSSS", base.gameObject).AddComponent<UISsSprite>();
						uISsSprite.Animation = ResourceManager.LoadSsAnimation("EFFECT_STAMP_DAILY").SsAnime;
						uISsSprite.depth = mBaseDepth + 4;
						uISsSprite.transform.localPosition = new Vector3(mXleft, mYdown, 0f);
						uISsSprite.transform.localScale = new Vector3(1f, 1f, 1f);
						uISsSprite.PlayCount = 1;
						uISsSprite.Play();
					}
				}
				mXleft += 70f;
				if (mNumberCount > 5)
				{
					mYdown -= 70f;
					mXleft = -175f;
					mNumberCount = 0;
				}
				mStampCounter = 0f;
				mCount--;
				if (-175f >= mYdown)
				{
					mStampComplete = true;
					mCount = 0;
				}
			}
		}
		if ((mCount == 0 && mData != null) || (mCount == 0 && mSedondRound))
		{
			mCounter += Time.deltaTime;
			if (mOKEnable && mCounter > 1.1f)
			{
				if (mStampComplete)
				{
					mGame.PlaySe("SE_DIALY_COMPLETE_STAMP", -1);
					mGame.PlaySe("VOICE_029", 2);
					mUISsAnime = Util.CreateGameObject("CompAnime", base.gameObject).AddComponent<UISsSprite>();
					mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_STAMP_COMP").SsAnime;
					mUISsAnime.depth = mBaseDepth + 4;
					mUISsAnime.transform.localPosition = new Vector3(0f, 8f, 0f);
					mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
					mUISsAnime.PlayCount = 1;
					mUISsAnime.Play();
					mStampComplete = false;
					mCounter = -2f;
					IsAfterComplete = true;
				}
				else
				{
					string text = "LC_button_ok2";
					UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
					Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
					Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
					mOKEnable = false;
					button = Util.CreateJellyImageButton("howtoButton", base.gameObject, atlas);
					Util.SetImageButtonInfo(button, "LC_button_howto150", "LC_button_howto150", "LC_button_howto150", mBaseDepth + 3, new Vector3(180f, -227f, 0f), Vector3.one);
					Util.AddImageButtonMessage(button, this, "OnHowToPushed", UIButtonMessage.Trigger.OnClick);
				}
			}
		}
		else if (mCount == 0 && mOKEnable)
		{
			string text2 = "LC_button_ok2";
			UIButton button2 = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
			Util.SetImageButtonInfo(button2, text2, text2, text2, mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button2, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
			button2 = Util.CreateJellyImageButton("howtoButton", base.gameObject, atlas);
			Util.SetImageButtonInfo(button2, "LC_button_howto150", "LC_button_howto150", "LC_button_howto150", mBaseDepth + 3, new Vector3(180f, -227f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button2, this, "OnHowToPushed", UIButtonMessage.Trigger.OnClick);
			mOKEnable = false;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		atlas = ResourceManager.LoadImage("HUD").Image;
		if (mGame.EventProfile.SDailyEvent != null)
		{
			mStarttime = mGame.EventProfile.SDailyEvent.StartTime;
			mEndtime = mGame.EventProfile.SDailyEvent.EndTime;
			mSpecialEventID = mGame.EventProfile.SDailyEvent.SpecialEventID;
			mHowToURL = mGame.EventProfile.SDailyEvent.HowToUrl;
			for (int i = 0; i < mGame.mSpecialDailyEventData.Count; i++)
			{
				if (mSpecialEventID.CompareTo(mGame.mSpecialDailyEventData[i].ID) == 0)
				{
					mImageID = mGame.mSpecialDailyEventData[i].ImageID;
					mBackPanelRGBA = mGame.mSpecialDailyEventData[i].BackImageRGBA;
					break;
				}
			}
		}
		mSPNum = mGame.mPlayer.GetSPDailyCount(mSpecialEventID);
		if (mGame.EventProfile.SDailyEvent != null)
		{
			mImagename = "icon_specialdaily_stamp0" + mImageID;
		}
		else
		{
			mImagename = "icon_specialdaily_stamp00";
		}
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(string.Empty);
		UILabel uILabel = Util.CreateLabel("Title", base.gameObject, atlasFont);
		string empty = string.Empty;
		int num = 1;
		int num2 = 24;
		int num3 = mSPNum - mCount;
		if (num3 < num2)
		{
			num = 1;
			if (num2 <= mSPNum)
			{
				mAfter = mSPNum - num2;
				mGetAccessoryData = mGame.GetAccessoryData(mGame.mSpecialDailyEventData[mSpecialEventID].RewardList[0].RewardID);
			}
		}
		else if (num2 <= num3 && num3 < num2 * 2)
		{
			num = 2;
			if (num2 * 2 <= mSPNum)
			{
				mAfter = mSPNum - num2 * 2;
				mGetAccessoryData = mGame.GetAccessoryData(mGame.mSpecialDailyEventData[mSpecialEventID].RewardList[1].RewardID);
			}
		}
		else if (num2 * 2 <= num3 && num3 < num2 * 3)
		{
			num = 3;
			if (num2 * 3 <= mSPNum)
			{
				mAfter = mSPNum - num2 * 3;
				mGetAccessoryData = mGame.GetAccessoryData(mGame.mSpecialDailyEventData[mSpecialEventID].RewardList[2].RewardID);
			}
		}
		else if (num2 * 3 <= num3 && num3 < num2 * 4)
		{
			num = 4;
			if (num2 * 4 <= mSPNum)
			{
				mAfter = mSPNum - num2 * 4;
				mGetAccessoryData = mGame.GetAccessoryData(mGame.mSpecialDailyEventData[mSpecialEventID].RewardList[3].RewardID);
			}
		}
		else if (num2 * 4 <= num3)
		{
			if (num2 * 5 <= mSPNum)
			{
				mAfter = 0;
				mGetAccessoryData = mGame.GetAccessoryData(mGame.mSpecialDailyEventData[mSpecialEventID].RewardList[4].RewardID);
			}
			num = 5;
			IsLastSheet = true;
		}
		mText = string.Format(Localization.Get("SDaily_DescA_" + mSpecialEventID));
		mText = mText + "\n" + string.Format(Localization.Get("SDaily_DescB_" + mSpecialEventID + "_" + (num - 1)));
		Util.SetLabelInfo(uILabel, string.Format(Localization.Get("SDaily_Title_" + mSpecialEventID), num), mBaseDepth + 135, new Vector3(41f, 289f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.4392157f, 22f / 51f, 0.1764706f, 1f);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UISprite sprite = Util.CreateSprite("TitleIcon", base.gameObject, image);
		Util.SetSpriteInfo(sprite, mImagename, mBaseDepth + 135, new Vector3(-139f, 302f, 0f), Vector3.one, false);
		UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, atlas);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, -4f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 325);
		uISprite.type = UIBasicSprite.Type.Sliced;
		GameObject gameObject = Util.CreateGameObject("FrameRoot", base.gameObject);
		float num4 = -175f;
		float num5 = 105f;
		for (int j = 0; 4 > j; j++)
		{
			for (int k = 0; 6 > k; k++)
			{
				uISprite = Util.CreateSprite("StampBack", gameObject.gameObject, atlas);
				Util.SetSpriteInfo(uISprite, "dailyEvent_stamppanel", mBaseDepth + 2, new Vector3(num4, num5, 0f), Vector3.one, false);
				if (mBackPanelRGBA == null)
				{
					uISprite.color = new Color(0.95686275f, 81f / 85f, 0.8784314f);
				}
				else
				{
					uISprite.color = new Color((float)mBackPanelRGBA[0] / 255f, (float)mBackPanelRGBA[1] / 255f, (float)mBackPanelRGBA[2] / 255f);
				}
				uISprite.SetDimensions(66, 66);
				num4 += 70f;
			}
			num4 = -175f;
			num5 -= 70f;
		}
		AccessoryData accessoryData = mGame.GetAccessoryData(mGame.mSpecialDailyEventData[mSpecialEventID].RewardList[num - 1].RewardID);
		if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.BOOSTER)
		{
			Def.BOOSTER_KIND a_kind = Def.BOOSTER_KIND.NONE;
			int a_num;
			accessoryData.GetBooster(out a_kind, out a_num);
			BoosterData boosterData = mGame.mBoosterData[(int)a_kind];
			uISprite = Util.CreateSprite("item", gameObject.gameObject, atlas);
			Util.SetSpriteInfo(uISprite, boosterData.iconNameOff, mBaseDepth + 2, new Vector3(175f, -105f, 0f), Vector3.one, false);
			uISprite.SetDimensions(66, 66);
		}
		else if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			uISprite = Util.CreateSprite("item", gameObject.gameObject, atlas);
			Util.SetSpriteInfo(uISprite, "icon_currency_jem", mBaseDepth + 2, new Vector3(175f, -105f, 0f), Vector3.one, false);
			uISprite.SetDimensions(66, 66);
			UILabel uILabel2 = Util.CreateLabel("boosterNum", uISprite.gameObject, atlasFont);
			int gotIDByNum = accessoryData.GetGotIDByNum();
			Util.SetLabelInfo(uILabel2, Localization.Get("Xtext"), mBaseDepth + 3, new Vector3(-15.5f, -22f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
			uILabel2.color = new Color(41f / 85f, 37f / 85f, 11f / 85f, 1f);
			NumberImageString numberImageString = Util.CreateGameObject("Number", uILabel2.gameObject).AddComponent<NumberImageString>();
			numberImageString.Init(2, gotIDByNum, mBaseDepth + 3, 0.53f, UIWidget.Pivot.Center, false, atlas);
			numberImageString.transform.localPosition = new Vector3(25f, 1.4f, 0f);
			numberImageString.SetPitch(30f);
		}
		mXleft = -175f;
		mYdown = 105f;
		if (mData != null || mSedondRound)
		{
			int num6 = mCount;
			int num7 = mSPNum - num2 * (num - 1);
			num7 -= num6;
			mXleft = -175f;
			mYdown = 105f;
			mNumberCount = 0;
			for (int l = 0; num7 > l; l++)
			{
				mNumberCount++;
				uISprite = Util.CreateSprite("IconImage", gameObject.gameObject, atlas);
				Util.SetSpriteInfo(uISprite, mImagename, mBaseDepth + 3, new Vector3(mXleft, mYdown, 0f), Vector3.one, false);
				uISprite.SetDimensions(60, 60);
				mXleft += 70f;
				if (mNumberCount > 5)
				{
					mYdown -= 70f;
					mXleft = -175f;
					mNumberCount = 0;
				}
				if (l == 23)
				{
					break;
				}
			}
			UILabel uILabel3 = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel3, mText, mBaseDepth + 2, new Vector3(0f, 196f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel3.color = new Color(7f / 15f, 39f / 85f, 0.2f);
		}
		else
		{
			int num8 = mCount;
			mXleft = -175f;
			mYdown = 105f;
			int num9 = mSPNum - num2 * (num - 1);
			if (num9 >= 24)
			{
				num9 = 24;
			}
			mNumberCount = 0;
			for (int m = 0; num9 > m; m++)
			{
				mNumberCount++;
				uISprite = Util.CreateSprite("IconImage", gameObject.gameObject, atlas);
				Util.SetSpriteInfo(uISprite, mImagename, mBaseDepth + 3, new Vector3(mXleft, mYdown, 0f), Vector3.one, false);
				uISprite.SetDimensions(60, 60);
				mXleft += 70f;
				if (mNumberCount > 5)
				{
					mYdown -= 70f;
					mXleft = -175f;
					mNumberCount = 0;
				}
			}
			UILabel uILabel4 = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel4, mText, mBaseDepth + 2, new Vector3(0f, 196f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel4.color = new Color(7f / 15f, 39f / 85f, 0.2f);
		}
		mGame.PlaySe("SE_ACCESSORY_UNLOCK", -1);
	}

	public void OnHowToPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSPDailyDialog = Util.CreateGameObject("SPDailyDialog", base.ParentGameObject).AddComponent<SPDailyDialog>();
			mSPDailyDialog.Init(mHowToURL);
			mSPDailyDialog.SetCallback(OnSPDailyDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnSPDailyDialogClosed(SPDailyDialog.SELECT_ITEM item)
	{
		mSPDailyDialog = null;
		mState.Reset(STATE.MAIN, true);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mAfter, mGetAccessoryData);
		}
	}

	public override void OnBackKeyPress()
	{
		if (!mOKEnable)
		{
			OnClosePushed();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}

	private void GetTextureParts(string changeName, int originX, int originY, out Vector2[] uv, out Vector3[] verts)
	{
		Vector2 offset = atlas.GetOffset(changeName);
		Vector2 size = atlas.GetSize(changeName);
		float x = offset.x;
		float y = offset.y;
		float x2 = offset.x + size.x;
		float y2 = offset.y + size.y;
		uv = new Vector2[4]
		{
			new Vector2(x, y2),
			new Vector2(x2, y2),
			new Vector2(x2, y),
			new Vector2(x, y)
		};
		Vector4 pixelPadding = atlas.GetPixelPadding(changeName);
		size = atlas.GetPixelSize(changeName);
		offset = new Vector2(originX, originY);
		offset.x += pixelPadding.x;
		offset.y += pixelPadding.y;
		size.x -= pixelPadding.x + pixelPadding.z;
		size.y -= pixelPadding.y + pixelPadding.w;
		x = 0f - offset.x;
		y = 0f - (size.y - offset.y);
		x2 = size.x - offset.x;
		y2 = offset.y;
		float z = 0f;
		verts = new Vector3[4]
		{
			new Vector3(x, y2, z),
			new Vector3(x2, y2, z),
			new Vector3(x2, y, z),
			new Vector3(x, y, z)
		};
	}
}
