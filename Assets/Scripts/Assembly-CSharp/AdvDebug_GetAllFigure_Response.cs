using System;
using System.Collections.Generic;

public class AdvDebug_GetAllFigure_Response : ICloneable
{
	[MiniJSONAlias("figures")]
	public List<AdvFigureData> FigureDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvDebug_GetAllFigure_Response Clone()
	{
		return MemberwiseClone() as AdvDebug_GetAllFigure_Response;
	}
}
