using System.Collections.Generic;

public class AdvGetMasterTableDataProfile_Request : RequestBase
{
	[MiniJSONAlias("masters")]
	public List<AdvMasterDataInfo> MasterTableInfoDataList { get; set; }
}
