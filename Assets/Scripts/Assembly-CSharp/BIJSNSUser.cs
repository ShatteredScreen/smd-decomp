using System;

public class BIJSNSUser : BIJSNSData<BIJSNSUser>, ICloneable, ICopyable, IBIJSNSUser
{
	public string ID { get; set; }

	public string Name { get; set; }

	public string Gender { get; set; }
}
