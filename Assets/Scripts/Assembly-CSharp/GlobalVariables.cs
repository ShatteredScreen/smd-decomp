using System;
using System.Collections.Generic;

public class GlobalVariables
{
	private static GlobalVariables mInstance;

	public int DEBUG_MAP_LEVEL;

	public int mSkinIndex;

	public int mCompanionIndex;

	public bool mLastCompanionSetting = true;

	public bool mEventMode;

	public bool mOldEventMode;

	public bool mOldEventOpen;

	public bool mOldEventExtend;

	public bool mAdvMode;

	public bool mAdvEventMode;

	public int mAdvEventId;

	public int mEventId;

	public string mEventName;

	public int mEventSkinIndex;

	public int mEventCompanionIndex;

	public bool mReplay;

	public bool mUseBooster0;

	public bool mUseBooster1;

	public bool mUseBooster2;

	public bool mUseBooster3;

	public bool mUseBooster4;

	public bool mUseBooster5;

	public List<Def.BOOSTER_KIND> mUseBoosters = new List<Def.BOOSTER_KIND>();

	public List<Def.BOOSTER_KIND> mNoSendUseBoosters = new List<Def.BOOSTER_KIND>();

	public bool DoLoginBonusServerCheck;

	public DateTime AchievementCheckTime;

	public FriendHelpPlay PlayingFriendHelp;

	public bool AddedNewFriend;

	public bool ConnectRetryFlg;

	public bool FirstBootFlg = true;

	public bool NoticeFirstBootFlg = true;

	public bool AdvNoticeFirstBootFlg = true;

	public int AutoPopupStageNo;

	public bool TrophyGuideLoseFlg;

	public bool TrophyGuideWinCompFlg;

	public GameMain.GiftUIMode FirstGiftTab = GameMain.GiftUIMode.NONE;

	public List<AccessoryData> mPresentBoxList = new List<AccessoryData>();

	public bool mColorModifyMode;

	public bool mColorNegativeModifyMode;

	public bool mIsStageClearStar3;

	public List<SearchUserData> RandomUserList = new List<SearchUserData>();

	public DateTime LastRandomUserGetTime = DateTimeUtil.UnitLocalTimeEpoch;

	public List<MyFaceBookFriend> FaceBookUserList = new List<MyFaceBookFriend>();

	public Dictionary<string, string> FBNameData = new Dictionary<string, string>();

	public List<int> AlreadyConnectAccessoryList = new List<int>();

	public List<int> AlreadyConnectAdvAccessoryList = new List<int>();

	public bool IsTitleReturnMaintenance;

	public List<int> CanAcquireAccessoryIdListForWin = new List<int>();

	public bool IsDailyChallengePuzzle;

	public int SelectedDailyChallengeSheetNo = -1;

	public int SelectedDailyChallengeStageNo = -1;

	public DailyChallengeEventSettings SelectedDailyChallengeSetting;

	public List<int> AcquiredCampaignId = new List<int>();

	public Dictionary<int, GlobalLabyrinthEventData> LabyrinthDataList = new Dictionary<int, GlobalLabyrinthEventData>();

	public bool IsSelectedEventTimeOut;

	public byte AddMovesCampaignAmount;

	public static GlobalVariables Instance
	{
		get
		{
			if (mInstance == null)
			{
				Reset();
			}
			return mInstance;
		}
	}

	public static void Reset()
	{
		mInstance = null;
		mInstance = new GlobalVariables();
	}

	public void SetLabyrinthData(int a_eventID, GlobalLabyrinthEventData a_data)
	{
		LabyrinthDataList[a_eventID] = a_data;
	}

	public GlobalLabyrinthEventData GetLabyrinthData(int a_eventID)
	{
		GlobalLabyrinthEventData globalLabyrinthEventData = null;
		if (LabyrinthDataList.ContainsKey(a_eventID))
		{
			return LabyrinthDataList[a_eventID];
		}
		return new GlobalLabyrinthEventData();
	}
}
