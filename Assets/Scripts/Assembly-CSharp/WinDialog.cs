using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class WinDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		MAP = 0,
		REPLAY = 1,
		NEXT = 2,
		STARGET = 3
	}

	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		STAR = 3,
		GET = 4,
		GET_ACCESSORY = 5,
		BOX_OPEN = 6,
		BOX_WAIT = 7,
		CHECK_STARGET = 8,
		WAIT_STARGET = 9
	}

	public enum MODE
	{
		NORMAL = 0,
		STARGET = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private PuzzleManager mManager;

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	protected MODE mMode;

	private OnDialogClosed mCallback;

	private float mStarYpos;

	private UISprite mGetIcon;

	private int mBoxSearchIndex;

	private int mOpenBoxIndex;

	private int mStarNum;

	private SpecialSpawnData[] mCollectItems;

	private UISprite[] mCollectItemSprites;

	private PostDialog mPostDialog;

	private UISsSprite mThirdStarEffect;

	private ResScriptableObject mResSpecialChance;

	private UIButton mCamera;

	private bool mImageSaveFinished;

	private bool mImageSaveError;

	private bool mPermissionCheckFinished;

	private bool mPermissionAllowed;

	public override void Start()
	{
		mManager = GameObject.Find("PuzzleManager").GetComponent<PuzzleManager>();
		base.Start();
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			mMode = MODE.STARGET;
		}
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			return;
		}
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (mGame.IsDailyChallengePuzzle)
			{
				mState.Change(STATE.MAIN);
			}
			else if (mManager.mScore >= mGame.mCurrentStage.BronzeScore || mMode == MODE.STARGET)
			{
				mState.Change(STATE.STAR);
			}
			else
			{
				mState.Change(STATE.GET);
			}
			break;
		case STATE.MAIN:
			if (mGame.mDebugRepeatPlayMode)
			{
				mGame.mPlayer.AddLifeCount(1);
				OnReplayPushed(null);
			}
			break;
		case STATE.STAR:
			if (mState.IsChanged())
			{
				StartCoroutine(StarEffect());
			}
			break;
		case STATE.GET:
			if (mState.IsChanged())
			{
				StartCoroutine(GetEffect());
			}
			break;
		case STATE.CHECK_STARGET:
		{
			if (!mState.IsChanged())
			{
				break;
			}
			bool flag2 = false;
			SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
			if (currentEventPageData != null && currentEventPageData.EventSetting.EventType == Def.EVENT_TYPE.SM_STAR && mMode != MODE.STARGET)
			{
				if (mStarNum == 2)
				{
					int num2 = 0;
					short a_course;
					int a_main;
					int a_sub;
					Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
					int stageNo = Def.GetStageNo(a_main, a_sub);
					PlayerClearData _psd = null;
					if (mGame.mPlayer.GetStageClearData(mGame.mCurrentStage.Series, mGame.mCurrentStage.EventID, a_course, stageNo, out _psd))
					{
						num2 = _psd.GotStars;
					}
					if (num2 < 3)
					{
						List<SMMapStageSetting> mapStages = currentEventPageData.GetMapStages(9);
						for (int num3 = mapStages.Count - 1; num3 >= 0; num3--)
						{
							if (mapStages[num3].mStageNo < 9900)
							{
								mapStages.RemoveAt(num3);
							}
						}
						if (mapStages.Count > 0)
						{
							flag2 = true;
						}
					}
				}
				if (GameMain.USE_DEBUG_DIALOG)
				{
					ResImage resImage = ResourceManager.LoadImage("PUZZLE_HUD");
					UIButton button = Util.CreateJellyImageButton("ButtonDbgStarget", base.gameObject, resImage.Image);
					Util.SetImageButtonInfo(button, "scorestar", "scorestar", "scorestar", mBaseDepth + 6, new Vector3(0f, -140f, 0f), new Vector3(2f, 2f, 0f));
					Util.AddImageButtonMessage(button, this, "OnDbgStargetPushed", UIButtonMessage.Trigger.OnClick);
				}
			}
			if (flag2)
			{
				mResSpecialChance = ResourceManager.LoadScriptableObjectAsync("SPECIAL_CHANCE");
				mState.Change(STATE.WAIT_STARGET);
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
			break;
		}
		case STATE.WAIT_STARGET:
			if (mResSpecialChance == null)
			{
				mState.Change(STATE.MAIN);
				break;
			}
			if (mGame.mSegmentProfile.ChanceList != null)
			{
				bool flag = false;
				DateTime dateTime = DateTimeUtil.Now();
				for (int i = 0; i < mGame.mSegmentProfile.ChanceList.Count; i++)
				{
					ChanceCommInfo common = mGame.SegmentProfile.ChanceList[i].Common;
					if (dateTime < common.StartTime || common.EndTime < dateTime || mGame.SegmentProfile.ChanceList[i].Data == null)
					{
						continue;
					}
					for (int j = 0; j < mGame.mSegmentProfile.ChanceList[i].Data.Count; j++)
					{
						if (mGame.mSegmentProfile.ChanceList[i].Data[j].Type - 1 == 5 && mGame.mSegmentProfile.ChanceList[i].Data[j].IsEnable)
						{
							flag = true;
							break;
						}
					}
					if (flag)
					{
						break;
					}
				}
				if (!flag)
				{
					ResourceManager.UnloadScriptableObject("SPECIAL_CHANCE");
					mState.Change(STATE.MAIN);
					break;
				}
			}
			if (mResSpecialChance.LoadState == ResourceInstance.LOADSTATE.DONE)
			{
				if (mGame.mPlayer.Data.ChanceCount == null)
				{
					mGame.mPlayer.Data.ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
				}
				if (!mGame.mPlayer.Data.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.STAR_GET))
				{
					mGame.mPlayer.Data.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.STAR_GET, 0);
				}
				Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> chanceCount;
				Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> dictionary = (chanceCount = mGame.mPlayer.Data.ChanceCount);
				SpecialChanceConditionSet.CHANCE_MODE key;
				SpecialChanceConditionSet.CHANCE_MODE key2 = (key = SpecialChanceConditionSet.CHANCE_MODE.STAR_GET);
				short num = chanceCount[key];
				dictionary[key2] = (short)(num + 1);
				mGame.Save();
				BoosterChanceDialog result = null;
				SpecialChanceConditionItem.Option option = default(SpecialChanceConditionItem.Option);
				option.GetStarCount = mStarNum;
				option.IsLose = false;
				SpecialChanceConditionItem.Option option2 = option;
				if (BoosterChanceDialog.TryMake(SpecialChanceConditionSet.CHANCE_MODE.STAR_GET, base.transform.parent.gameObject, out result, option2))
				{
					result.SetClosedEvent(OnStarGetChallengeClosed);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
				ResourceManager.UnloadScriptableObject("SPECIAL_CHANCE");
			}
			break;
		}
		if (mCamera != null)
		{
			STATE status = mState.GetStatus();
			if (status == STATE.MAIN)
			{
				if (!mCamera.gameObject.activeSelf)
				{
					SetLayoutCameraButton();
					mCamera.gameObject.SetActive(true);
				}
			}
			else if (mCamera.gameObject.activeSelf)
			{
				mCamera.gameObject.SetActive(false);
			}
		}
		mState.Update();
	}

	private IEnumerator StarEffect()
	{
		ResImage res = ResourceManager.LoadImage("HUD");
		if (mStarNum >= 1 && mMode == MODE.NORMAL)
		{
			yield return StartCoroutine(StarEffectSub(new Vector3(-30f, mStarYpos - 20f, 0f), "SE_RESULT_STAR_BRONZE", res.Image));
		}
		if (mStarNum >= 2 && mMode == MODE.NORMAL)
		{
			yield return StartCoroutine(StarEffectSub(new Vector3(90f, mStarYpos, 0f), "SE_RESULT_STAR_SILVER", res.Image));
		}
		if (mStarNum >= 3)
		{
			yield return StartCoroutine(StarEffectSub(new Vector3(210f, mStarYpos - 20f, 0f), "SE_RESULT_STAR_GOLD", res.Image, mThirdStarEffect));
		}
		mState.Change(STATE.GET);
	}

	private IEnumerator StarEffectSub(Vector3 pos, string key, BIJImage atlas, UISsSprite effect = null)
	{
		UISprite starSprite = Util.CreateSprite("Star", base.gameObject, atlas);
		Util.SetSpriteInfo(starSprite, "star_gold", mBaseDepth + 5, pos, new Vector3(0.01f, 0.01f, 1f), false);
		mGame.PlaySe(key, 3);
		float maxSize = 3f;
		float endSize = 1f;
		float targetAngle = 180f - Mathf.Asin(endSize / maxSize) * 57.29578f;
		bool loop = true;
		Util.TriFunc triFunc = Util.CreateTriFunc(0f, targetAngle, Util.TriFunc.MAXFUNC.STOP);
		while (loop)
		{
			loop = !triFunc.AddDegree(Time.deltaTime * 360f);
			float z = triFunc.degree / triFunc.maxDegree * 360f;
			float scale = triFunc.Sin() * maxSize;
			Util.SetSpriteScale(starSprite, new Vector3(scale, scale, 1f));
			starSprite.transform.localRotation = Quaternion.Euler(0f, 0f, z);
			yield return 0;
		}
		if (effect != null)
		{
			effect.enabled = true;
			effect.Play();
		}
	}

	private IEnumerator GetEffect()
	{
		for (int i = 0; i < mCollectItems.Length; i++)
		{
			yield return StartCoroutine(GetEffectSub(i));
		}
		mState.Change(STATE.CHECK_STARGET);
	}

	private IEnumerator GetEffectSub(int i)
	{
		ResImage resImageHud = ResourceManager.LoadImage("HUD");
		AccessoryData data = mGame.GetAccessoryData(mCollectItems[i].ItemIndex);
		if (mCollectItems[i].Category != Def.ITEM_CATEGORY.ACCESSORY || data == null)
		{
			yield break;
		}
		string imageName = string.Empty;
		switch (data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		{
			Def.BOOSTER_KIND boosterKind;
			int num;
			data.GetBooster(out boosterKind, out num);
			BoosterData boosterData = mGame.mBoosterData[(int)boosterKind];
			imageName = boosterData.iconBuy;
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
			imageName = "ac_icon_wallpaper_complete";
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE:
			imageName = "icon_wallpaper";
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART:
			imageName = "icon_heart";
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			imageName = "icon_dailyEvent_stamp01";
			break;
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			imageName = "icon_ranking_trophy";
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
			imageName = "icon_level";
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_F:
			imageName = "icon_level";
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			imageName = "icon_dailyEvent_stamp02";
			break;
		case AccessoryData.ACCESSORY_TYPE.SP_DAILY:
		{
			imageName = "icon_specialdaily_stamp00";
			if (mGame.EventProfile.SDailyEvent == null)
			{
				break;
			}
			int imageID = 0;
			for (int a = 0; a < mGame.mSpecialDailyEventData.Count; a++)
			{
				if (mGame.EventProfile.SDailyEvent.SpecialEventID.CompareTo(mGame.mSpecialDailyEventData[a].ID) == 0)
				{
					imageID = mGame.mSpecialDailyEventData[a].ImageID;
					break;
				}
			}
			imageName = "icon_specialdaily_stamp0" + mGame.mSpecialDailyEventData[imageID].ImageID;
			break;
		}
		}
		if (!string.IsNullOrEmpty(imageName))
		{
			UISprite effect = Util.CreateSprite("OpenEffect", base.gameObject, resImageHud.Image);
			Util.SetSpriteInfo(effect, "box_open", mBaseDepth + 5, mCollectItemSprites[i].transform.localPosition, Vector3.one, false);
			float angle = 0f;
			while (angle < 90f)
			{
				angle += Time.deltaTime * 180f;
				float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
				effect.transform.localScale = new Vector3(0.01f + ratio * 2f, 0.01f + ratio * 2f, 1f);
				effect.transform.localRotation = Quaternion.Euler(0f, 0f, angle * 1f);
				yield return 0;
			}
			BIJNGUIImage ngui_atlas = resImageHud.Image;
			mCollectItemSprites[i].atlas = ngui_atlas.Atlas;
			if (data.AccessoryType == AccessoryData.ACCESSORY_TYPE.SP_DAILY)
			{
				Util.SetSpriteImageName(mCollectItemSprites[i], imageName, new Vector3(0.93f, 0.93f, 1f), false);
				mCollectItemSprites[i].transform.localPosition += new Vector3(0f, 4f, 0f);
			}
			else
			{
				Util.SetSpriteImageName(mCollectItemSprites[i], imageName, new Vector3(1.2f, 1.2f, 1f), false);
			}
			while (angle < 180f)
			{
				angle += Time.deltaTime * 180f;
				float ratio2 = Mathf.Sin(angle * ((float)Math.PI / 180f));
				effect.transform.localScale = new Vector3(0.01f + ratio2 * 2f, 0.01f + ratio2 * 2f, 1f);
				effect.transform.localRotation = Quaternion.Euler(0f, 0f, angle * 1f);
				yield return 0;
			}
			UnityEngine.Object.Destroy(effect.gameObject);
		}
	}

	public void Init(int starNum, SpecialSpawnData[] collectItems)
	{
		mStarNum = starNum;
		mCollectItems = collectItems;
	}

	public override void BuildDialog()
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage("PUZZLE_HUD");
		ResImage resImage3 = ResourceManager.LoadImage("PUZZLE_TILE");
		UIFont atlasFont = GameMain.LoadFont();
		bool isDailyChallengePuzzle = mGame.IsDailyChallengePuzzle;
		string text;
		if (mGame.IsDailyChallengePuzzle)
		{
			text = Localization.Get("LevelInfo_Level") + mGame.SelectedDailyChallengeStageNo;
			if (mGame.mDebugDailyChallengeVisibleBaseStage)
			{
				string text2 = text;
				text = string.Concat(text2, "[sup](", mGame.mCurrentStage.Series, mGame.mCurrentStage.DisplayStageNumber, ")[/sup]");
			}
		}
		else
		{
			ISMStageData aStage;
			if (mMode == MODE.NORMAL)
			{
				aStage = mGame.mCurrentStage;
			}
			else
			{
				SMMapStageSetting aStageSetting;
				mGame.GetPreStage(out aStage, out aStageSetting);
			}
			text = ((!mGame.mEventMode) ? (Localization.Get("LevelInfo_Level") + aStage.DisplayStageNumber) : (mGame.mEventName + aStage.DisplayStageNumber));
		}
		if (isDailyChallengePuzzle)
		{
			InitDialogFrame(DIALOG_SIZE.MEDIUM, null, null, 0, 470);
		}
		else
		{
			InitDialogFrame(DIALOG_SIZE.LARGE);
		}
		InitDialogTitle(text);
		if (mCollectItems.Length == 0)
		{
			mStarYpos = 150f;
		}
		else
		{
			mStarYpos = 174f;
		}
		UISprite uISprite;
		if (!isDailyChallengePuzzle)
		{
			uISprite = Util.CreateSprite("Star", base.gameObject, resImage.Image);
			Util.SetSpriteInfo(uISprite, "star_gold_empty", mBaseDepth + 3, new Vector3(-30f, mStarYpos - 20f, 0f), Vector3.one, false);
			if (mMode == MODE.STARGET)
			{
				uISprite = Util.CreateSprite("Star", uISprite.gameObject, resImage.Image);
				Util.SetSpriteInfo(uISprite, "star_gold", mBaseDepth + 5, Vector3.zero, Vector3.one, false);
			}
			uISprite = Util.CreateSprite("Star", base.gameObject, resImage.Image);
			Util.SetSpriteInfo(uISprite, "star_gold_empty", mBaseDepth + 3, new Vector3(90f, mStarYpos, 0f), Vector3.one, false);
			if (mMode == MODE.STARGET)
			{
				uISprite = Util.CreateSprite("Star", uISprite.gameObject, resImage.Image);
				Util.SetSpriteInfo(uISprite, "star_gold", mBaseDepth + 5, Vector3.zero, Vector3.one, false);
				mThirdStarEffect = Util.CreateGameObject("ThirdSarEffect", base.gameObject).AddComponent<UISsSprite>();
				mThirdStarEffect.Animation = ResourceManager.LoadSsAnimation("EFFECT_STARGET").SsAnime;
				mThirdStarEffect.depth = mBaseDepth + 2;
				mThirdStarEffect.transform.localPosition = new Vector3(210f, mStarYpos - 20f, 0f);
				mThirdStarEffect.enabled = false;
			}
			uISprite = Util.CreateSprite("Star", base.gameObject, resImage.Image);
			Util.SetSpriteInfo(uISprite, "star_gold_empty", mBaseDepth + 3, new Vector3(210f, mStarYpos - 20f, 0f), Vector3.one, false);
		}
		float y = (isDailyChallengePuzzle ? 33f : ((mCollectItems.Length != 0) ? 110f : 60f));
		uISprite = Util.CreateSprite("Character", base.gameObject, resImage2.Image);
		Util.SetSpriteInfo(uISprite, (!isDailyChallengePuzzle) ? "chara_claer" : "daily_puzzle_chara01", mBaseDepth + 3, new Vector3(-170f, y, 0f), Vector3.one, false);
		if (mMode == MODE.NORMAL)
		{
			y = (isDailyChallengePuzzle ? 35f : ((mCollectItems.Length != 0) ? 90f : 40f));
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Score"), mBaseDepth + 3, new Vector3(90f, y, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			string[] numTblOverride = new string[10] { "num_buy_big0", "num_buy_big1", "num_buy_big2", "num_buy_big3", "num_buy_big4", "num_buy_big5", "num_buy_big6", "num_buy_big7", "num_buy_big8", "num_buy_big9" };
			y = (isDailyChallengePuzzle ? (-17f) : ((mCollectItems.Length != 0) ? 40f : (-10f)));
			NumberImageString numberImageString = Util.CreateGameObject("Score", base.gameObject).AddComponent<NumberImageString>();
			numberImageString.Init(7, mManager.mScore, mBaseDepth + 3, 0.6f, UIWidget.Pivot.Center, false, resImage.Image, false, numTblOverride);
			numberImageString.transform.localPosition = new Vector3(90f, y, 0f);
			numberImageString.SetPitch(70f);
		}
		else
		{
			uISprite = Util.CreateSprite("ThirdStarMsg", base.gameObject, resImage2.Image);
			Util.SetSpriteInfo(uISprite, "LC_starget_clear", mBaseDepth + 3, new Vector3(90f, -30f, 0f), Vector3.one, false);
		}
		UIButton uIButton;
		if (!isDailyChallengePuzzle)
		{
			uIButton = Util.CreateJellyImageButton("ButtonReplay", base.gameObject, resImage.Image);
			Util.SetImageButtonInfo(uIButton, "LC_button_retry", "LC_button_retry", "LC_button_retry", mBaseDepth + 4, new Vector3(-120f, -224f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnReplayPushed", UIButtonMessage.Trigger.OnClick);
			if (mMode == MODE.NORMAL)
			{
				bool flag = false;
				if (Def.IsEventSeries(mGame.mCurrentStage.Series))
				{
					flag = mGame.IsSeasonEventExpired(mGame.mCurrentStage.EventID);
				}
				if (flag)
				{
					UILabel uILabel2 = Util.CreateLabel("ExpiredText", base.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel2, Localization.Get("EventExpired_Title"), mBaseDepth + 5, new Vector3(-115f, -190f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
					uILabel2.color = Color.red;
					uILabel2.effectStyle = UILabel.Effect.Outline8;
					uILabel2.effectColor = Color.white;
					uILabel2.effectDistance = new Vector2(2f, 2f);
					uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
				}
				else if (mGame.mCurrentMapPageData is SMEventPageData && (mGame.mCurrentMapPageData as SMEventPageData).EventSetting != null)
				{
					Def.EVENT_TYPE eventType = (mGame.mCurrentMapPageData as SMEventPageData).EventSetting.EventType;
					if (eventType == Def.EVENT_TYPE.SM_LABYRINTH)
					{
						uIButton.defaultColor = (uIButton.pressed = (uIButton.disabledColor = (uIButton.hover = Color.gray)));
						uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
					}
				}
			}
			else
			{
				uIButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
		}
		uIButton = Util.CreateJellyImageButton("ButtonNext", base.gameObject, resImage.Image);
		Util.SetImageButtonInfo(uIButton, "LC_button_next", "LC_button_next", "LC_button_next", mBaseDepth + 4, (!isDailyChallengePuzzle) ? new Vector3(120f, -224f, 0f) : new Vector3(0f, -160f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnNextPushed", UIButtonMessage.Trigger.OnClick);
		if (!isDailyChallengePuzzle && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Post) && !mGame.mEventMode)
		{
			uIButton = Util.CreateJellyImageButton("ButtonSNS", base.gameObject, resImage.Image);
			Util.SetImageButtonInfo(uIButton, "LC_button_posting", "LC_button_posting", "LC_button_posting", mBaseDepth + 4, new Vector3(0f, -140f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnSNSPushed", UIButtonMessage.Trigger.OnClick);
		}
		float num = 400f / (float)(mCollectItems.Length + 1);
		int num2 = 1;
		mCollectItemSprites = new UISprite[mCollectItems.Length];
		for (int i = 0; i < mCollectItems.Length; i++)
		{
			float x = -200f + num * (float)num2;
			mCollectItemSprites[i] = Util.CreateSprite("Box" + i, base.gameObject, resImage3.Image);
			int kind = (int)mCollectItems[i].Kind;
			Util.SetSpriteInfo(mCollectItemSprites[i], mGame.mTileData[kind].DropImageNames[0][0], mBaseDepth + 4, new Vector3(x, -68f, 0f), new Vector3(1.2f, 1.2f, 1f), false);
			uISprite = Util.CreateSprite("BoxBase", base.gameObject, resImage.Image);
			Util.SetSpriteInfo(uISprite, "itemGet_bg", mBaseDepth + 2, new Vector3(x, -68f, 0f), Vector3.one, false);
			num2++;
		}
		mBoxSearchIndex = 0;
		if (isDailyChallengePuzzle)
		{
			UILabel uILabel = Util.CreateLabel("NoStarsLabel", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("NoStars"), mBaseDepth + 4, new Vector3(91f, -70f), 19, 0, 0, UIWidget.Pivot.Center);
			Util.SetLabelColor(uILabel, new Color32(byte.MaxValue, 65, 110, byte.MaxValue));
			uISprite = Util.CreateSprite("StageClear", base.gameObject, resImage2.Image);
			Util.SetSpriteInfo(uISprite, "daily_puzzle_clear00", mBaseDepth + 4, new Vector3(90f, 85f), Vector3.one, false);
		}
		mCamera = Util.CreateJellyImageButton("ButtonCamera", base.gameObject, resImage.Image);
		Util.SetImageButtonInfo(mCamera, "button_camera", "button_camera", "button_camera", mBaseDepth + 6, new Vector3(1384f, 1384f), Vector3.one);
		Util.AddImageButtonMessage(mCamera, this, "OnCameraPushed", UIButtonMessage.Trigger.OnClick);
		mCamera.gameObject.SetActive(false);
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
		}
		SetLayoutCameraButton();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		CloseSubDialogs();
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void Close()
	{
		CloseSubDialogs();
		base.Close();
	}

	private void CloseSubDialogs()
	{
		if (mPostDialog != null)
		{
			mPostDialog.Close();
		}
	}

	public override void OnBackKeyPress()
	{
		OnMapPushed(null);
	}

	public void OnMapPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mGame.PlaySe("VOICE_025", 2);
			mSelectItem = SELECT_ITEM.MAP;
			Close();
		}
	}

	public void OnReplayPushed(GameObject go)
	{
		if (go.GetComponent<JellyImageButton>().IsEnable() && !GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mGame.PlaySe("VOICE_024", 2);
			mSelectItem = SELECT_ITEM.REPLAY;
			Close();
		}
	}

	public void OnNextPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mGame.PlaySe("VOICE_025", 2);
			mSelectItem = SELECT_ITEM.MAP;
			Close();
		}
	}

	public void OnSNSPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			GameObject parent = base.gameObject.transform.parent.gameObject;
			mPostDialog = Util.CreateGameObject("PostDialog", parent).AddComponent<PostDialog>();
			mPostDialog.SetClosedCallback(OnPostDialogClosed);
			mPostDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	private void OnPostDialogClosed()
	{
		UnityEngine.Object.Destroy(mPostDialog.gameObject);
		mPostDialog = null;
		mState.Change(STATE.MAIN);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void OnStarGetChallengeClosed(BoosterChanceDialog.SELECT_ITEM item)
	{
		switch (item)
		{
		case BoosterChanceDialog.SELECT_ITEM.CONSUME:
		case BoosterChanceDialog.SELECT_ITEM.FIRST_CONSUME:
			mGame.PlaySe("VOICE_003", 2);
			mSelectItem = SELECT_ITEM.STARGET;
			ServerCramVariableData.mOrgStageScore = mManager.mScore;
			Close();
			break;
		case BoosterChanceDialog.SELECT_ITEM.CANCEL:
			mState.Change(STATE.MAIN);
			break;
		}
	}

	public void OnDbgStargetPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			OnStarGetChallengeClosed(BoosterChanceDialog.SELECT_ITEM.CONSUME);
		}
	}

	public void OnCameraPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			StartCoroutine(CheckIOSPermission(delegate
			{
				StartCoroutine(TakeScreenShot());
			}, delegate
			{
				ShowScreenShotErrorDialog();
			}));
		}
	}

	private void SetLayoutCameraButton()
	{
		if (mCamera != null)
		{
			Vector2 vector = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopLeft, Util.ScreenOrientation) / mJelly.GetDefaultScale();
			mCamera.gameObject.transform.localPosition = new Vector3(vector.x + 70f, vector.y - 70f);
		}
	}

	private void ShowScreenShotErrorDialog()
	{
		ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.gameObject).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("ScreenShotError_Desc"));
		confirmDialog.Init(Localization.Get("ScreenShotError_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(OnScreenShotErrorDialogClosed);
	}

	private void OnScreenShotErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
		}
		mState.Change(STATE.MAIN);
	}

	public IEnumerator TakeScreenShot()
	{
		int seqNo = 0;
		string tmpPath2 = null;
		do
		{
			tmpPath2 = Path.Combine(path2: string.Format("ScreenShot_{0}_{1}.png", DateTime.Now.ToString("yyyyMMddHHmmss"), seqNo.ToString("00")), path1: Path.Combine(BIJUnity.getTempPath(), "ss"));
			seqNo++;
			yield return 0;
		}
		while (File.Exists(tmpPath2) && seqNo < 100);
		mGame.PlaySe("SE_CAMERA_SHUTTER", -1);
		yield return StartCoroutine(mGame.mGameStateManager.mGameState.CameraFlash());
		if (string.IsNullOrEmpty(tmpPath2) || File.Exists(tmpPath2))
		{
			ShowScreenShotErrorDialog();
			yield break;
		}
		mCamera.gameObject.SetActive(false);
		yield return StartCoroutine(CaptureScreenShot(tmpPath2));
		if (!File.Exists(tmpPath2))
		{
			ShowScreenShotErrorDialog();
			yield break;
		}
		yield return StartCoroutine(CopyImageToGallery(tmpPath2));
		if (File.Exists(tmpPath2))
		{
			File.Delete(tmpPath2);
		}
		if (mImageSaveError)
		{
			ShowScreenShotErrorDialog();
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	private IEnumerator CaptureScreenShot(string path)
	{
		yield return new WaitForEndOfFrame();
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		tex.ReadPixels(new Rect(0f, 0f, width, height), 0, 0);
		tex.Apply();
		yield return 0;
		byte[] screenshot = tex.EncodeToPNG();
		using (BIJBinaryWriter _data = new BIJFileBinaryWriter(path, Encoding.UTF8, false, true))
		{
			_data.WriteRaw(screenshot, 0, screenshot.Length);
			_data.Flush();
		}
	}

	public IEnumerator CopyImageToGallery(string path)
	{
		mImageSaveFinished = false;
		mImageSaveError = false;
		if (BIJUnity.writeToPhotoAlbumAsync(path, base.gameObject, "OnSaveImageResult"))
		{
			while (!mImageSaveFinished)
			{
				yield return 0;
			}
		}
		else
		{
			mImageSaveFinished = true;
			mImageSaveError = true;
		}
	}

	public void OnSaveImageResult(string message)
	{
		mImageSaveFinished = true;
		if (string.IsNullOrEmpty(message))
		{
			mImageSaveError = false;
			return;
		}
		mImageSaveError = true;
		UnityEngine.Debug.LogError("ScreenShotStart : メッセージ開始\n" + message + "\nメッセージここまで");
	}

	private IEnumerator CheckIOSPermission(Action _OkCallback, Action _failedCallback)
	{
		mPermissionCheckFinished = false;
		mPermissionAllowed = false;
		BIJUnity.checkPermissionPhoto(base.gameObject, "OnCheckIOSPermissionResult");
		while (!mPermissionCheckFinished)
		{
			yield return null;
		}
		if (mPermissionAllowed)
		{
			if (_OkCallback != null)
			{
				_OkCallback();
			}
		}
		else if (_failedCallback != null)
		{
			_failedCallback();
		}
	}

	public void OnCheckIOSPermissionResult(string result)
	{
		if (result == "true")
		{
			mPermissionAllowed = true;
		}
		else
		{
			mPermissionAllowed = false;
		}
		mPermissionCheckFinished = true;
	}
}
