using UnityEngine;

public class EventMiniDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private OnDialogClosed mCallback;

	private string mSpriteName;

	private string mDesc;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
		}
	}

	public override void BuildDialog()
	{
		ResImage resImage = ResourceManager.LoadImage("EVENTHUD");
		ResImage resImage2 = ResourceManager.LoadImage("HUD");
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.SMALL, "DIALOG_BASE", "event_instruction_panel", 0, 240);
		if (mSpriteName != null)
		{
			UISprite sprite = Util.CreateSprite("TitleSprite", base.gameObject, resImage.Image);
			Util.SetSpriteInfo(sprite, mSpriteName, mBaseDepth + 2, new Vector3(0f, 105f, 0f), Vector3.one, false);
		}
		if (mDesc != null)
		{
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, mDesc, mBaseDepth + 3, new Vector3(0f, 31f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.white;
		}
		UIButton button = Util.CreateJellyImageButton("OKButton", base.gameObject, resImage2.Image);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 4, new Vector3(0f, -49f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(string spriteName, string desc)
	{
		mSpriteName = spriteName;
		mDesc = desc;
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		if (!GetBusy())
		{
			OnCancelPushed(null);
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
