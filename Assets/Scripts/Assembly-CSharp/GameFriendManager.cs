using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameFriendManager
{
	private enum STATE
	{
		NONE = 0,
		WAIT = 1,
		NETWORK_PROLOGUE = 2,
		NETWORK_03 = 3,
		NETWORK_04 = 4,
		NETWORK_04_00 = 5,
		NETWORK_05 = 6,
		NETWORK_EPILOGUE = 7
	}

	public enum MODE
	{
		NONE = 0,
		FORCE = 1,
		LITE = 2
	}

	private class _GetFriendsData
	{
		public List<MyFriendData> results { get; set; }
	}

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.NONE);

	private GameMain mGame;

	private SNS mSNSKind;

	private MODE mMode;

	private bool mNetworkDataModified;

	private Dictionary<int, MyFriend> mGameFriendsWork;

	private int mNewAddedGameFriendCount;

	private Queue<MyFriend> mFriendIconsWork;

	private string mErrorMessage;

	public bool IsDataModified
	{
		get
		{
			return mNetworkDataModified;
		}
	}

	public int NewAddedGameFriendCount
	{
		get
		{
			return mNewAddedGameFriendCount;
		}
	}

	public bool HasError
	{
		get
		{
			return !string.IsNullOrEmpty(mErrorMessage);
		}
	}

	public GameFriendManager(GameMain game)
		: this(game, MODE.NONE, SNS.Facebook)
	{
	}

	public GameFriendManager(GameMain game, MODE mode)
		: this(game, mode, SNS.Facebook)
	{
	}

	public GameFriendManager(GameMain game, MODE mode, SNS kind)
	{
		mGame = game;
		mSNSKind = kind;
		mMode = mode;
		if (mGame != null && (mMode == MODE.FORCE || mMode == MODE.LITE))
		{
			mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
	}

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
		try
		{
			long num = time();
		}
		catch (Exception)
		{
		}
	}

	private static void dbg(string msg)
	{
		try
		{
			long num = time();
		}
		catch (Exception)
		{
		}
	}

	public bool Start()
	{
		if (mState.GetStatus() != 0)
		{
			return false;
		}
		if (GameMain.NO_NETWORK)
		{
			return false;
		}
		if (mSNSKind != SNS.Facebook)
		{
			return false;
		}
		mGame.AddedNewFriend = false;
		mState.Reset(STATE.NETWORK_PROLOGUE, true);
		return true;
	}

	public bool IsFinished()
	{
		return mState.GetStatus() == STATE.NONE;
	}

	public void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_PROLOGUE:
			Server.GetFriendsEvent += Server_OnGetFriends;
			mErrorMessage = null;
			mNetworkDataModified = false;
			mNewAddedGameFriendCount = 0;
			mGameFriendsWork = new Dictionary<int, MyFriend>();
			mFriendIconsWork = new Queue<MyFriend>();
			mState.Change(STATE.NETWORK_03);
			break;
		case STATE.NETWORK_03:
			Network_SyncGetFriends();
			break;
		case STATE.NETWORK_04:
			Network_GetFriendIcons();
			break;
		case STATE.NETWORK_05:
			Network_GetMyIcon();
			break;
		case STATE.NETWORK_EPILOGUE:
			Server.GetFriendsEvent -= Server_OnGetFriends;
			mGameFriendsWork.Clear();
			mFriendIconsWork.Clear();
			mState.Change(STATE.NONE);
			break;
		}
		mState.Update();
	}

	private string GetSNSID(MyFriend mf)
	{
		return GetSNSID(mf, string.Empty);
	}

	private string GetSNSID(MyFriend mf, string prefix)
	{
		if (string.IsNullOrEmpty(prefix))
		{
			prefix = string.Empty;
		}
		string text = null;
		switch (mSNSKind)
		{
		case SNS.Facebook:
			return prefix + mf.Data.Fbid;
		case SNS.GameCenter:
			return prefix + mf.Data.Gcid;
		case SNS.GooglePlus:
			return prefix + mf.Data.Gpid;
		default:
			return null;
		}
	}

	private bool SetSNSID(ref MyFriend mf, string id)
	{
		if (string.IsNullOrEmpty(id))
		{
			return false;
		}
		bool result = true;
		switch (mSNSKind)
		{
		case SNS.Facebook:
			mf.Data.Fbid = id;
			break;
		case SNS.GameCenter:
			mf.Data.Gcid = id;
			break;
		case SNS.GooglePlus:
			mf.Data.Gpid = id;
			break;
		default:
			result = false;
			break;
		}
		return result;
	}

	private bool SetSNSName(ref MyFriend mf, string name)
	{
		if (string.IsNullOrEmpty(name))
		{
			return false;
		}
		bool result = true;
		switch (mSNSKind)
		{
		case SNS.Facebook:
			mf.Data.FbName = name;
			break;
		case SNS.GameCenter:
			mf.Data.GcName = name;
			break;
		case SNS.GooglePlus:
			mf.Data.GpName = name;
			break;
		default:
			result = false;
			break;
		}
		return result;
	}

	private void Network_SyncGetFriends()
	{
		STATE aStatus = STATE.NETWORK_04;
		long num = DateTimeUtil.BetweenMinutes(mGame.mPlayer.Data.LastGetFriendTime, DateTime.Now);
		long gET_FRIEND_FREQ = GameMain.GET_FRIEND_FREQ;
		log(string.Format("Network_OnSyncGetFriends : {0}/{1}", num, gET_FRIEND_FREQ));
		if (gET_FRIEND_FREQ > 0 && num < gET_FRIEND_FREQ)
		{
			log("Network_OnSyncGetFriends skip.");
			mState.Change(aStatus);
			return;
		}
		BIJSNS bIJSNS = SocialManager.Instance[mSNSKind];
		if (mGame.mPlayer.Friends.Count > 0)
		{
			List<MyFriend> list = new List<MyFriend>(mGame.mPlayer.Friends.Values);
			foreach (MyFriend item in list)
			{
				if (item.Data.UUID != 0)
				{
					mGameFriendsWork[item.Data.UUID] = item;
				}
			}
		}
		mState.Change(STATE.WAIT);
		if (!Server.GetFriends())
		{
			log("Network_OnSyncGetFriends: Server.GetFriends failed to request. skip.");
			mState.Change(aStatus);
		}
	}

	private void Server_OnGetFriends(int result, int code, string json)
	{
		STATE aStatus = STATE.NETWORK_04;
		log("Server_OnGetFriends: " + GameMain.Server_Result(result));
		if (result == 0)
		{
			try
			{
				SynchronizeFriendData(json);
				mGame.mPlayer.Data.LastGetFriendTime = DateTime.Now;
			}
			catch (Exception)
			{
			}
		}
		else
		{
			mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		mState.Change(aStatus);
	}

	private void SynchronizeFriendData(string json)
	{
		log("SynchronizeFriendData json=" + json);
		_GetFriendsData obj = new _GetFriendsData();
		new MiniJSONSerializer().Populate(ref obj, json);
		if (obj.results != null && obj.results.Count >= 0)
		{
			int num = 0;
			StringBuilder stringBuilder = new StringBuilder();
			foreach (MyFriendData result in obj.results)
			{
				num++;
				MyFriend value;
				if (mGameFriendsWork.TryGetValue(result.UUID, out value))
				{
					value.Data.UUID = result.UUID;
					value.Data.HiveId = result.HiveId;
					value.Data.Fbid = result.Fbid;
					value.Data.Gcid = result.Gcid;
					value.Data.Gpid = result.Gpid;
					value.Data.Level = result.Level;
					value.Data.Experience = result.Experience;
					value.Data.UpdateTimeEpoch = result.UpdateTimeEpoch;
					value.Data.TotalScore = result.TotalScore;
					value.Data.TotalStars = result.TotalStars;
					value.Data.TotalTrophy = result.TotalTrophy;
					value.Data.Name = result.Name;
					value.Data.Series = result.Series;
					value.Data.IconID = result.IconID;
					value.Data.IconLevel = result.IconLevel;
					value.Data.PlayStage = result.PlayStage;
					if (value.Data.PlayStage == null)
					{
						value.Data.PlayStage = new List<PlayStageParam>();
					}
					mGameFriendsWork[result.UUID] = value;
					stringBuilder.AppendLine(string.Format("#{0} update: {1}", num, value));
				}
				else
				{
					value = new MyFriend();
					value.SetData(result);
					mGameFriendsWork.Add(value.Data.UUID, value);
					if (!mGame.mPlayer.Data.NotificationNewFriends.ContainsKey(value.Data.UUID))
					{
						mGame.mPlayer.Data.NotificationNewFriends.Add(value.Data.UUID, 1);
					}
					stringBuilder.AppendLine(string.Format("#{0} new: {1}", num, value));
				}
			}
			log(string.Format("@@@ GetFriends : {0}\n{1}", num, stringBuilder.ToString()));
			List<int> list = new List<int>(mGameFriendsWork.Keys);
			List<int> list2 = new List<int>();
			for (int i = 0; i < list.Count; i++)
			{
				bool flag = false;
				foreach (MyFriendData result2 in obj.results)
				{
					if (result2.UUID == list[i])
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					list2.Add(list[i]);
				}
			}
			for (int j = 0; j < list2.Count; j++)
			{
				mGameFriendsWork.Remove(list2[j]);
			}
		}
		mNewAddedGameFriendCount = mGameFriendsWork.Count - mGame.mPlayer.Friends.Count;
		mGame.mPlayer.Friends.Clear();
		if (mGameFriendsWork.Count > 0)
		{
			foreach (MyFriend value2 in mGameFriendsWork.Values)
			{
				if (value2.Data.UUID == 0)
				{
					log("Not play the game: " + value2.Data.Name);
					continue;
				}
				mGame.mPlayer.Friends[value2.Data.UUID] = value2;
				if (mGame.mPlayer.ApplyFriends.ContainsKey(value2.Data.UUID))
				{
					mGame.mPlayer.ApplyFriends.Remove(value2.Data.UUID);
				}
			}
		}
		mGame.mPlayer.Data.GotFriends = Math.Max(mGame.mPlayer.Data.GotFriends, mGame.mPlayer.Friends.Count);
		mNetworkDataModified = true;
		mGameFriendsWork.Clear();
	}

	private void Network_GetFriendIcons()
	{
		STATE aStatus = STATE.NETWORK_05;
		if (mMode == MODE.LITE)
		{
			log("Network_OnGetFriendIcons none as litemode.");
			mState.Change(aStatus);
			return;
		}
		long num = DateTimeUtil.BetweenMinutes(mGame.mPlayer.Data.LastGetFriendIconTime, DateTime.Now);
		long gET_FRIEND_ICON_FREQ = GameMain.GET_FRIEND_ICON_FREQ;
		log(string.Format("Network_OnGetFriendIcons : {0}/{1}", num, gET_FRIEND_ICON_FREQ));
		if (gET_FRIEND_ICON_FREQ > 0 && num < gET_FRIEND_ICON_FREQ)
		{
			log("Network_OnGetFriendIcons skip.");
			mState.Change(aStatus);
			return;
		}
		if (mGame.mFbUserMngFriend == null)
		{
			mGame.mFbUserMngFriend = FBUserManager.CreateInstance();
		}
		mGame.mFbUserMngFriend.SetIconGetSpan(GameMain.GET_ONE_ICON_FREQ);
		mGame.mFbUserMngFriend.SetFBUserList(new List<MyFriend>(mGame.mPlayer.Friends.Values), OnIconGetCallback);
		if (!mGame.mFbUserMngFriend.IsAllIconComplete())
		{
			mGame.mPlayer.Data.LastGetFriendIconTime = DateTime.Now;
		}
		mState.Change(aStatus);
	}

	private void Network_GetMyIcon()
	{
		STATE aStatus = STATE.NETWORK_EPILOGUE;
		if (mMode == MODE.LITE)
		{
			log("Network_OnGetFriendIcons none as litemode.");
			mState.Change(aStatus);
			return;
		}
		FBIconManager instance = SingletonMonoBehaviour<FBIconManager>.Instance;
		instance.DeleteIconCashRequest();
		if (!GameMain.IsFacebookIconEnable() || mGame.mPlayer.Data.PlayerIcon < 10000)
		{
			log("Network_OnGetFriendIcons no use SNS icon.");
			mState.Change(aStatus);
			return;
		}
		long num = DateTimeUtil.BetweenMinutes(mGame.mPlayer.Data.LastGetMyIconTime, DateTime.Now);
		long gET_ONE_ICON_FREQ = GameMain.GET_ONE_ICON_FREQ;
		log(string.Format("Network_OnGetMyIcon : {0}/{1}", num, gET_ONE_ICON_FREQ));
		if (mGame.mPlayer.Icon != null && gET_ONE_ICON_FREQ > 0 && num < gET_ONE_ICON_FREQ)
		{
			log("Network_OnGetMyIcon skip.");
			mState.Change(aStatus);
			return;
		}
		BIJSNS sns = SocialManager.Instance[mSNSKind];
		if (!GameMain.IsSNSLogined(sns))
		{
			log("Network_OnGetMyIcon: Not logged in. skip.");
			mState.Change(aStatus);
		}
		else
		{
			instance.GetPlayerIconRequest(mGame.mOptions.Fbid, OnPlayerIconGetCallback);
			mState.Change(aStatus);
		}
	}

	public static void OnIconGetCallback(bool ret, int uuid, string fbid, Texture2D icon)
	{
		if (!ret)
		{
			return;
		}
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		if (instance.mPlayer.Friends.ContainsKey(uuid))
		{
			MyFriend myFriend = instance.mPlayer.Friends[uuid];
			if (icon != null && myFriend.Data.Fbid == fbid)
			{
				myFriend.Icon = icon;
			}
		}
	}

	public static void OnPlayerIconGetCallback(bool ret, string fbid, Texture2D icon)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		instance.mPlayer.Icon = icon;
		instance.mPlayer.Data.IconModified = DateTime.Now;
		if (instance.mPlayer.Icon != null)
		{
			instance.mPlayer.Data.LastGetMyIconTime = DateTime.Now;
		}
		instance.Save();
		GameStateManager.GAME_STATE currentState = instance.mGameStateManager.CurrentState;
		if (currentState == GameStateManager.GAME_STATE.MAP || currentState == GameStateManager.GAME_STATE.EVENT_RALLY)
		{
			GameStateSMMapBase gameStateSMMapBase = instance.mGameStateManager.mGameState as GameStateSMMapBase;
			MapAvater mAvater = gameStateSMMapBase.MapPage.mAvater;
			mAvater.SetIcon();
		}
	}
}
