using System;

public class DateTimeConverter : IMiniJSONConverter
{
	private static string ToSS(object obj)
	{
		return (obj != null) ? obj.ToString() : string.Empty;
	}

	private static T GetEnumValue<T>(object obj)
	{
		return (T)Enum.Parse(typeof(T), ToSS(obj));
	}

	public object ConvertTo(object a_obj)
	{
		return ((DateTime)a_obj).ToUniversalTime().ToString();
	}

	public object ConvertFrom(object a_json)
	{
		string text = a_json as string;
		if (text == null)
		{
			return new DateTime(0L);
		}
		try
		{
			return DateTime.Parse(text).ToLocalTime();
		}
		catch (Exception)
		{
			return new DateTime(0L);
		}
	}
}
