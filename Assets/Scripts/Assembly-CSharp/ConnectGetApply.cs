using System;
using System.Collections.Generic;

public class ConnectGetApply : ConnectBase
{
	protected class JsonSerializerTemp
	{
		public List<GetMailResponseApply> results { get; set; }
	}

	protected Server.MailType mMailType;

	protected string mJson;

	protected bool mNetworkDataModified;

	private int mApplyMax;

	private short mApplyCount;

	public void SetResponseData(Server.MailType a_kind, string a_json)
	{
		mMailType = a_kind;
		mJson = a_json;
	}

	protected override void InitServerFlg()
	{
		GameMain.mMapApplyCheckDone = false;
		GameMain.mMapApplySucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mMapApplyCheckDone = true;
		GameMain.mMapApplySucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mMapApplyCheckDone = true;
		GameMain.mMapApplySucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetApplyTime, DateTime.Now);
		long gET_APPLY_FREQ = GameMain.GET_APPLY_FREQ;
		if (gET_APPLY_FREQ > 0 && num < gET_APPLY_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			ServerFlgSucceeded();
			base.StateStart();
			return;
		}
		int mailByFriendReq = mPlayer.GetMailByFriendReq();
		if (mailByFriendReq >= 100)
		{
			ServerFlgSucceeded();
			base.StateStart();
			return;
		}
		int num = 100 - mailByFriendReq;
		int num2 = mGame.GameProfile.MailGetCount;
		if (num2 == 0)
		{
			num2 = Constants.GIFT_AMOUNT;
		}
		if (num > num2)
		{
			num = num2;
		}
		if (mParam == null)
		{
			mParam = new ConnectParameterBase();
			mParam.SetIgnoreError(true);
		}
		GameMain.mMapApplyRepeat = false;
		mApplyMax = num;
		InitServerFlg();
		if (!Server.GetFriendApply(num))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		bool flag = false;
		int num = 0;
		try
		{
			JsonSerializerTemp obj = new JsonSerializerTemp();
			new MiniJSONSerializer().Populate(ref obj, mJson);
			List<GetMailResponseApply> list = new List<GetMailResponseApply>();
			if (obj != null)
			{
				list.AddRange(obj.results);
			}
			if (list != null && list.Count > 0)
			{
				DateTime now = DateTime.Now;
				int got_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
				List<int> list2 = new List<int>();
				foreach (GetMailResponseApply item in list)
				{
					bool flag2 = true;
					GiftItemData giftItemData = item.GetGiftItemData();
					GiftItem giftItem = new GiftItem();
					giftItem.SetData(giftItemData);
					giftItem.Data.SetGiftSource(mMailType);
					if (giftItem.Data.RemoteTimeEpoch != 0)
					{
						long num2 = DateTimeUtil.BetweenDays0(giftItem.RemoteTime, now);
						long num3 = ((giftItem.GiftItemCategory != Def.ITEM_CATEGORY.FRIEND_REQ) ? Constants.GIFT_EXPIRED : Constants.GIFT_EXPIRED_FRIEND_REQ_FREQ);
						if (num2 >= num3)
						{
							num++;
							int itemCategory = giftItem.Data.ItemCategory;
							int itemKind = giftItem.Data.ItemKind;
							int itemID = giftItem.Data.ItemID;
							int quantity = giftItem.Data.Quantity;
							int lastPlaySeries = (int)SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlaySeries;
							int lastPlayLevel = SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastPlayLevel;
							ServerCram.LostGiftItem(itemCategory, itemKind, itemID, quantity, got_time, lastPlaySeries, lastPlayLevel);
							flag2 = false;
						}
					}
					if (flag2)
					{
						mPlayer.AddGift(giftItem);
						mNetworkDataModified = true;
					}
					list2.Add(giftItem.Data.RemoteID);
				}
				if (list2.Count > 0)
				{
					Server.GotApply(Server.MailType.FRIEND, list2);
				}
			}
			mPlayer.Data.LastGetApplyTime = DateTime.Now;
			if (obj != null && obj.results.Count >= mApplyMax)
			{
				if (num > 0)
				{
					int num4 = 5;
					if (mGame.GameProfile.ApplyRepeatNum > 0)
					{
						num4 = mGame.GameProfile.ApplyRepeatNum;
					}
					mApplyCount++;
					if (mApplyCount < num4)
					{
						flag = true;
					}
				}
			}
			else
			{
				mApplyCount = 0;
			}
			if (mNetworkDataModified)
			{
				mGame.Save();
			}
		}
		catch (Exception ex)
		{
			BIJLog.E("GetApply Error : " + ex);
		}
		if (!flag)
		{
			base.StateConnectFinish();
			return;
		}
		mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitTimeEpoch;
		mState.Change(STATE.START);
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
			mConnectResultKind = 3;
			base.StateConnectFinish();
			return false;
		}
		return false;
	}
}
