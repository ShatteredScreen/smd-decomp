public class AdvMasterDataInfo
{
	[MiniJSONAlias("name")]
	public string name { get; set; }

	[MiniJSONAlias("revision")]
	public int lt_revision { get; set; }
}
