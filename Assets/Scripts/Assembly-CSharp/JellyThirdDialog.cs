using UnityEngine;

public class JellyThirdDialog : JellyDialog
{
	private enum STATE
	{
		INIT = 0,
		OPEN = 1,
		CLOSE = 2,
		WAIT = 3
	}

	private STATE mState;

	protected Vector2 mLogScreen = Util.LogScreenSize();

	protected override void Awake()
	{
		mTarget = base.gameObject;
		mDelay = 0f;
		Reset();
	}

	protected override void Start()
	{
	}

	protected override void Update()
	{
		if (mDelay > 0f)
		{
			mDelay -= Time.deltaTime;
			return;
		}
		switch (mState)
		{
		case STATE.INIT:
			mAngle = 0f;
			mMoveFinished = false;
			mState = STATE.OPEN;
			break;
		case STATE.OPEN:
			if (mAngle <= 0f)
			{
				mAngle = 0f;
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			break;
		case STATE.CLOSE:
			mAngle += Time.deltaTime * 3600f;
			if (mAngle >= mLogScreen.y)
			{
				mState = STATE.WAIT;
				mMoveFinished = true;
			}
			mTarget.transform.localPosition = new Vector3(0f, mAngle, 0f);
			break;
		}
		mTarget.transform.localScale = Vector3.one;
	}

	public override void Reset()
	{
		mState = STATE.INIT;
	}

	public override void Open(bool immediate = false)
	{
		mOpenImmediate = immediate;
		mMoveFinished = false;
		mState = STATE.OPEN;
	}

	public override void Close(bool immediate = false)
	{
		mCloseImmediate = immediate;
		mAngle = 0f;
		mMoveFinished = false;
		mState = STATE.CLOSE;
	}

	public override bool IsMoveFinished()
	{
		return mMoveFinished;
	}

	public override void SetMoveFinished(bool flag)
	{
		mMoveFinished = flag;
	}

	public override void SetTarget(GameObject target)
	{
		mTarget = target;
	}

	public override void SetDelay(float delayTime)
	{
		mDelay = delayTime;
	}
}
