public class BoosterData
{
	public short index;

	public string name;

	public string desc;

	public string useMessage;

	public string useNgMessage;

	public string recommendMessage;

	public string iconNameOff;

	public string iconNameOn;

	public string iconBuy;

	public string iconPuzzleHud;

	public byte useSituation;

	public string[] preLoadSound;

	public string[] preLoadAtlas;

	public string[] preLoadAnimation;

	public string sound;

	public string anime;

	public BoosterData()
	{
		index = 0;
		name = string.Empty;
		desc = string.Empty;
		useMessage = string.Empty;
		useNgMessage = string.Empty;
		recommendMessage = string.Empty;
		iconNameOff = string.Empty;
		iconNameOn = string.Empty;
		iconBuy = string.Empty;
		iconPuzzleHud = string.Empty;
		useSituation = 0;
		preLoadSound = null;
		preLoadAtlas = null;
		preLoadAnimation = null;
		sound = string.Empty;
		anime = string.Empty;
	}

	public void deserialize(BIJBinaryReader stream)
	{
		index = stream.ReadShort();
		name = stream.ReadUTF();
		desc = stream.ReadUTF();
		useMessage = stream.ReadUTF();
		useNgMessage = stream.ReadUTF();
		recommendMessage = stream.ReadUTF();
		iconNameOff = stream.ReadUTF();
		iconNameOn = stream.ReadUTF();
		iconBuy = stream.ReadUTF();
		iconPuzzleHud = stream.ReadUTF();
		useSituation = stream.ReadByte();
		string text = stream.ReadUTF();
		string[] array = text.Split(',');
		preLoadSound = new string[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			if (string.Compare(array[i], "NONE") == 0)
			{
				preLoadSound[i] = string.Empty;
			}
			else
			{
				preLoadSound[i] = array[i];
			}
		}
		text = stream.ReadUTF();
		array = text.Split(',');
		preLoadAtlas = new string[array.Length];
		for (int j = 0; j < array.Length; j++)
		{
			if (string.Compare(array[j], "NONE") == 0)
			{
				preLoadAtlas[j] = string.Empty;
			}
			else
			{
				preLoadAtlas[j] = array[j];
			}
		}
		text = stream.ReadUTF();
		array = text.Split(',');
		preLoadAnimation = new string[array.Length];
		for (int k = 0; k < array.Length; k++)
		{
			if (string.Compare(array[k], "NONE") == 0)
			{
				preLoadAnimation[k] = string.Empty;
			}
			else
			{
				preLoadAnimation[k] = array[k];
			}
		}
		sound = stream.ReadUTF();
		anime = stream.ReadUTF();
	}

	public BoosterData Clone()
	{
		return (BoosterData)MemberwiseClone();
	}

	public void Dump()
	{
	}
}
