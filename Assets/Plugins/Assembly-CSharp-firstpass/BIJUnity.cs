using BIJUnityPlugin.Android;
using UnityEngine;

public static class BIJUnity
{
	public const int LOCAL_NOTIFICATION_DEFAULT = 0;

	public static void crash(int level)
	{
		API.BIJUnity_crash(level);
	}

	public static void setup(string _gameObject, string _callback, bool isWW)
	{
		API.BIJUnity_setup(_gameObject, _callback, Application.isEditor, isWW);
	}

	public static bool isInputEnabled()
	{
		return API.BIJUnity_isInputEnabled();
	}

	public static void enableInput(bool enabled)
	{
		API.BIJUnity_enableInput(enabled, -1);
	}

	public static void enableInput(bool enabled, int durationMills)
	{
		API.BIJUnity_enableInput(enabled, durationMills);
	}

	public static string getApplicationName()
	{
		return API.BIJUnity_getApplicationName();
	}

	public static string getBundleName()
	{
		return API.BIJUnity_getBundleName();
	}

	public static string getBundleVersion()
	{
		return API.BIJUnity_getBundleVersion();
	}

	public static string getDataPath()
	{
		return API.BIJUnity_getDataPath();
	}

	public static string getResourceDLPath(string aFolderName)
	{
		string text = getDataPath();
		if (text[text.Length - 1] != '/')
		{
			text += "/";
		}
		return text + aFolderName + "/";
	}

	public static string getTempPath()
	{
		return API.BIJUnity_getTempPath();
	}

	public static double getFreeDiskSpace(string path)
	{
		return API.BIJUnity_getFreeDiskSpace(path);
	}

	public static string getLanguageCode()
	{
		return API.BIJUnity_getLanguageCode();
	}

	public static string getCountryCode()
	{
		return API.BIJUnity_getCountryCode();
	}

	public static string getMobileCountryCode()
	{
		return API.BIJUnity_getMobileCountryCode();
	}

	public static string getCurrencyCode()
	{
		return API.BIJUnity_getCurrencyCode();
	}

	public static void setClipboardText(string text)
	{
		API.BIJUnity_setClipboardText(text);
	}

	public static string getClipboardText()
	{
		return API.BIJUnity_getClipboardText();
	}

	public static bool isAvailableRotation()
	{
		return API.BIJUnity_isAvailableRotation();
	}

	public static ScreenOrientation getStatusBarOrientation()
	{
		return (ScreenOrientation)API.BIJUnity_getStatusBarOrientation();
	}

	public static string getUnityID()
	{
		return API.BIJUnity_getUnityID();
	}

	public static string getDeviceID()
	{
		return API.BIJUnity_getDeviceID();
	}

	public static string getOriginalUniqueId()
	{
		return API.BIJUnity_getOriginalUniqueId();
	}

	public static string getIMEI()
	{
		return API.BIJUnity_getIMEI();
	}

	public static string getIMSI()
	{
		return API.BIJUnity_getIMSI();
	}

	public static string getAndroidID()
	{
		return API.BIJUnity_getAndroidID();
	}

	public static string getSerial()
	{
		return API.BIJUnity_getSerial();
	}

	public static string getDeviceModel()
	{
		return API.BIJUnity_getDeviceModel();
	}

	public static string getOpenUDID()
	{
		return API.BIJUnity_getOpenUDID();
	}

	public static string getAdvertisingID()
	{
		return API.BIJUnity_getAdvertisingID();
	}

	public static string getVendorID()
	{
		return API.BIJUnity_getVendorID();
	}

	public static string getMacAddress()
	{
		return API.BIJUnity_getMacAddress();
	}

	public static string getDeviceSerial()
	{
		return API.BIJUnity_getDeviceSerial();
	}

	public static void vibrate(long time)
	{
		API.BIJUnity_vibrate(time);
	}

	public static bool checkInstalledPackage(string packagename)
	{
		return API.BIJUnity_checkInstalledPackage(packagename);
	}

	public static bool runInstalledPackage(string packagename, string classname)
	{
		return API.BIJUnity_runInstalledPackage(packagename, classname);
	}

	public static bool invokeStore(string packagename)
	{
		return API.BIJUnity_invokeStore(packagename);
	}

	public static void showMemoryStats()
	{
		API.BIJUnity_showMemoryStats();
	}

	public static void registerMemoryStats(int time)
	{
		API.BIJUnity_registerMemoryStats(time);
	}

	public static void unregisterMemoryStats()
	{
		API.BIJUnity_unregisterMemoryStats();
	}

	public static void registerDiagnostic(int time)
	{
		API.BIJUnity_registerDiagnostic(time);
	}

	public static void unregisterDiagnostic()
	{
		API.BIJUnity_unregisterDiagnostic();
	}

	public static float getUsedMemory()
	{
		return API.BIJUnity_getUsedMemory();
	}

	public static float getBatteryPower()
	{
		return API.BIJUnity_getBatteryPower();
	}

	public static int getSignalType()
	{
		return API.BIJUnity_getSignalType();
	}

	public static float getSignalStrength()
	{
		return API.BIJUnity_getSignalStrength();
	}

	public static bool is32bitPlatform()
	{
		return API.BIJUnity_is32bitPlatform();
	}

	public static bool is64bitPlatform()
	{
		return API.BIJUnity_is64bitPlatform();
	}

	public static void scheduleLocalNotification(string message, string title, int afterSeconds)
	{
		scheduleLocalNotification(0, message, title, afterSeconds);
	}

	public static void scheduleLocalNotification(int nid, string message, string title, int afterSeconds)
	{
		API.BIJUnity_scheduleLocalNotification(nid, message, title, afterSeconds);
	}

	public static void cancelLocalNotification(int nid)
	{
		API.BIJUnity_cancelLocalNotification(nid);
	}

	public static void cancelAllLocalNotification()
	{
		API.BIJUnity_cancelAllLocalNotification();
	}

	public static void registerForPushNotifications()
	{
		API.BIJUnity_registerForPushNotifications();
	}

	public static void unregisterForPushNotifications()
	{
		API.BIJUnity_unregisterForPushNotifications();
	}

	public static string getRemoteNotificationToken()
	{
		return API.BIJUnity_getRemoteNotificationToken();
	}

	public static void enableRemoteNotification(bool enabled)
	{
		API.BIJUnity_enableRemoteNotification(enabled);
	}

	public static bool isEnableRemoteNotification()
	{
		return API.BIJUnity_isEnableRemoteNotification();
	}

	public static bool BIJUnity_invokeFacebookWithImage(string imageUrl)
	{
		return API.BIJUnity_invokeFacebookWithImage(imageUrl);
	}

	public static bool BIJUnity_invokeFacebookStore()
	{
		return API.BIJUnity_invokeFacebookStore();
	}

	public static bool invokeLINEWithMessage(string title, string message, string imageUrl)
	{
		return API.BIJUnity_invokeLINEWithMessage(title, message, imageUrl);
	}

	public static bool invokeLINEWithImage(string title, string message, string imageUrl)
	{
		return API.BIJUnity_invokeLINEWithImage(title, message, imageUrl);
	}

	public static bool invokeTwitterWithMessage(string title, string message, string imageUrl)
	{
		return API.BIJUnity_invokeTwitterWithMessage(title, message, imageUrl);
	}

	public static bool invokeTwitterOrStoreWithMessage(string title, string message, string imageUrl)
	{
		return API.BIJUnity_invokeTwitterOrStoreWithMessage(title, message, imageUrl);
	}

	public static bool invokeTwitterStore()
	{
		return API.BIJUnity_invokeTwitterStore();
	}

	public static bool invokeSMSWithMessage(string title, string message, string imageUrl)
	{
		return API.BIJUnity_invokeSMSWithMessage(title, message, imageUrl);
	}

	public static bool invokeMailWithMessageAndImage(string title, string message, string imageUrl)
	{
		return API.BIJUnity_invokeMailWithMessageAndImage(title, message, imageUrl);
	}

	public static AdInfo getAdvertisingInfo()
	{
		return API.BIJUnity_getAdvertisingInfo();
	}

	public static bool getAdvertisingInfoAsync(GameObject gameObject)
	{
		return API.BIJUnity_getAdvertisingInfoAsync(gameObject);
	}

	public static bool writeToPhotoAlbumAsync(string url, GameObject gameObject, string callback)
	{
		return API.BIJUnity_writeToPhotoAlbumAsync(url, gameObject, callback);
	}

	public static int checkPermission()
	{
		return API.BIJUnity_checkPermission();
	}

	public static void requestPermission(string gameObject, string callback)
	{
		API.BIJUnity_requestPermission(gameObject, callback);
	}

	public static int shouldShowRequestPermissionRationale()
	{
		return API.BIJUnity_shouldShowRequestPermissionRationale();
	}

	public static void openApplicationDetailsSettings()
	{
		API.BIJUnity_openApplicationDetailsSettings();
	}

	public static string getGoogleAccount()
	{
		return API.BIJUnity_getGoogleAccount();
	}

	public static void checkPermissionPhoto(GameObject _gameObject, string _callback)
	{
		API.BIJUnity_checkPermissionPhoto(_gameObject, _callback);
	}
}
