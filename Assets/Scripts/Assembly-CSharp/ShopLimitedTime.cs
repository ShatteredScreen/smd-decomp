using System;

public class ShopLimitedTime
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("id")]
	public int ShopItemID { get; set; }

	[MiniJSONAlias("limit")]
	public int ShopItemLimit { get; set; }

	[MiniJSONAlias("cnt")]
	public int ShopCount { get; set; }

	[MiniJSONAlias("mugen_heart")]
	public int MugenHeartItem { get; set; }

	[MiniJSONAlias("all_crush")]
	public int AllCrushItem { get; set; }

	[MiniJSONAlias("sale_icon_priority")]
	public int SaleIconPriority { get; set; }

	[MiniJSONAlias("sale_banner_id")]
	public int SaleBannerID { get; set; }

	[MiniJSONAlias("sale_banner_url")]
	public string SaleBannerURL { get; set; }

	[MiniJSONAlias("resale")]
	public int ResaleCount { get; set; }

	[MiniJSONAlias("invisible")]
	public bool InvisibleStoreList { get; set; }

	[MiniJSONAlias("sale_dialog_banner_url")]
	public string SaleDialogBannerURL { get; set; }

	public ShopLimitedTime()
	{
	}

	public ShopLimitedTime(long start, long end, int id, int limit, int cnt, int mugenheart, int sale_icon_priority, int allcrush)
	{
		StartTime = start;
		EndTime = end;
		ShopItemID = id;
		ShopItemLimit = limit;
		ShopCount = cnt;
		MugenHeartItem = mugenheart;
		AllCrushItem = allcrush;
		SaleIconPriority = sale_icon_priority;
	}

	public bool IsEnable()
	{
		DateTime dateTime = DateTimeUtil.Now();
		DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(StartTime, true);
		DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(EndTime, true);
		if (StartTime != EndTime && (dateTime < dateTime2 || dateTime3 < dateTime))
		{
			return false;
		}
		return true;
	}
}
