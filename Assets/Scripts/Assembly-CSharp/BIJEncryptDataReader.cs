using System;
using System.IO;
using System.Text;

public class BIJEncryptDataReader : BIJBinaryReader
{
	private MemoryStream mStream;

	public BIJEncryptDataReader(string name)
		: this(name, Encoding.UTF8)
	{
	}

	public BIJEncryptDataReader(string name, Encoding encoding)
	{
		mStream = null;
		Reader = null;
		try
		{
			mPath = System.IO.Path.Combine(getBasePath(), name);
			byte[] bytes = File.ReadAllBytes(mPath);
			byte[] buffer = CryptUtils.DecryptRJ128(bytes);
			mStream = new MemoryStream(buffer);
			Reader = new BinaryReader(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	private static string getBasePath()
	{
		return BIJUnity.getDataPath();
	}

	public override void Close()
	{
		base.Close();
		try
		{
			if (mStream != null)
			{
				mStream.Close();
			}
		}
		catch (Exception)
		{
		}
		mStream = null;
	}
}
