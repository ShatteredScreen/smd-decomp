using System.Collections.Generic;
using UnityEngine;

public class RemoveFriendConfirmDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		CLOSE = 0,
		REMOVE = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i, int remove);

	private SELECT_ITEM mSelectItem;

	private int mSelectedUser = -1;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	public string mFriendID;

	private OnDialogClosed mCallback;

	private ConfirmDialog mConfirmDialog;

	private SearchUserData mSearchData;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		string titleDescKey = Localization.Get("Reary_Destroy_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = "icon_chara00";
		string text2 = "lv_1";
		int num = 0;
		string text3 = Res.IconMapNumImageString[num];
		int index = 0;
		if (mGame.mCompanionData.Count > mSearchData.iconid)
		{
			index = mSearchData.iconid;
		}
		CompanionData companionData = mGame.mCompanionData[index];
		text = companionData.iconName;
		int iconlvl = mSearchData.iconlvl;
		text2 = "lv_" + iconlvl;
		string nickname = mSearchData.nickname;
		titleDescKey = string.Format(Localization.Get("Reary_Destroy_Desc"));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 100f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(mDialogFrame.width - 30, mDialogFrame.height - 50);
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.spacingY = 6;
		UISprite uISprite = Util.CreateSprite("Instruction", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 19f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(436, 108);
		MyFriend value;
		mGame.mPlayer.Friends.TryGetValue(mSearchData.uuid, out value);
		UISprite uISprite2;
		if (GameMain.IsFacebookIconEnable() && value.Data.IconID >= 10000)
		{
			if (value.Icon != null)
			{
				UITexture uITexture = Util.CreateGameObject("friendIcon", uISprite.gameObject).AddComponent<UITexture>();
				uITexture.depth = mBaseDepth + 3;
				uITexture.transform.localPosition = new Vector3(-165f, -9f, 0f);
				uITexture.mainTexture = value.Icon;
				uITexture.SetDimensions(65, 65);
			}
			else
			{
				uISprite2 = Util.CreateSprite("FriendIcon", uISprite.gameObject, image2);
				Util.SetSpriteInfo(uISprite2, "icon_chara_facebook", mBaseDepth + 3, new Vector3(-165f, -9f, 0f), Vector3.one, false);
				uISprite2.SetDimensions(65, 65);
			}
			index = value.Data.IconID - 10000;
			if (index >= mGame.mCompanionData.Count)
			{
				index = 0;
			}
			string iconName = mGame.mCompanionData[index].iconName;
			UISprite uISprite3 = Util.CreateSprite("Icon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite3, iconName, mBaseDepth + 4, new Vector3(-138f, 24f, 0f), Vector3.one, false);
			uISprite3.SetDimensions(50, 50);
		}
		else
		{
			index = value.Data.IconID;
			if (index >= 10000)
			{
				index -= 10000;
			}
			if (index >= mGame.mCompanionData.Count)
			{
				index = 0;
			}
			string iconName2 = mGame.mCompanionData[index].iconName;
			UISprite uISprite4 = Util.CreateSprite("Icon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite4, iconName2, mBaseDepth + 3, new Vector3(-160f, 3f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(84, 84);
			iconName2 = ((value.Data.IconLevel <= 0) ? "lv_1" : ("lv_" + value.Data.IconLevel));
			UISprite sprite = Util.CreateSprite("Lv", uISprite.gameObject, image);
			Util.SetSpriteInfo(sprite, iconName2, mBaseDepth + 4, new Vector3(-160f, -37f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(value.Data.Fbid))
			{
				string imageName = "icon_sns00_mini";
				uISprite2 = Util.CreateSprite("FacebookIcon", uISprite4.gameObject, image2);
				Util.SetSpriteInfo(uISprite2, imageName, mBaseDepth + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
			}
		}
		uISprite2 = Util.CreateSprite("Linetop", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "line00", mBaseDepth + 3, new Vector3(46f, 9f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(292, 6);
		int num2 = mSearchData.series;
		int stageNumber = 1;
		List<PlayStageParam> playStage = mSearchData.PlayStage;
		if (playStage != null && playStage.Count != 0)
		{
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == num2)
				{
					stageNumber = playStage[i].Level / 100;
					break;
				}
			}
		}
		else
		{
			num2 = 0;
			int a_main;
			int a_sub;
			Def.SplitStageNo(mSearchData.lvl, out a_main, out a_sub);
			stageNumber = a_main;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(uISprite.gameObject, mBaseDepth, num2, stageNumber, -13f, -8f);
		uILabel = Util.CreateLabel("userNameDesk", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, nickname, mBaseDepth + 3, new Vector3(46f, 26f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(290, 26);
		UIButton button = Util.CreateJellyImageButton("RemoveButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_delete", "LC_button_delete", "LC_button_delete", mBaseDepth + 3, new Vector3(0f, -90f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnRemovePushed", UIButtonMessage.Trigger.OnClick);
		CreateCancelButton("OnCancelPushed");
	}

	public void Init(SearchUserData a_userData)
	{
		mSearchData = a_userData;
		mSelectedUser = mSearchData.uuid;
	}

	public void OnRemovePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = SELECT_ITEM.REMOVE;
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem, mSelectedUser);
		}
		Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSelectItem = SELECT_ITEM.CLOSE;
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
