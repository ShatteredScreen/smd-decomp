using System.Collections;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
	public enum GAME_STATE
	{
		LOGO = 0,
		PRETITLE = 1,
		TITLE = 2,
		MAP = 3,
		PUZZLE = 4,
		EVENT_RALLY = 5,
		EVENT_STAR = 6,
		EVENT_RALLY2 = 7,
		EVENT_BINGO = 8,
		EVENT_LABYRINTH = 9,
		TRANSFER = 10,
		SUPPORT = 11,
		COLLECTION = 12,
		TROPHY = 13,
		OPTION = 14,
		OLD_EVENT_GATE = 15,
		ADVPUZZLE = 16,
		ADVMENU = 17,
		SERVICE_FINISH = 18,
		DEBUG = 19
	}

	public enum STATE
	{
		WAIT = 0,
		LOAD = 1,
		FADEIN = 2,
		MAIN = 3,
		FADEOUT = 4,
		UNLOAD = 5,
		LOADING = 6
	}

	public enum LoadingMask : byte
	{
		NONE = 0,
		LOADING = 1,
		CONNECTING = 2
	}

	private const int TIPS_SCREEN_DEPTH = 101;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	private GameMain mGame;

	public GameState mGameState;

	private GAME_STATE mCurrentGameState;

	private GAME_STATE mNextGameState;

	private GAME_STATE mPrevGameState;

	private UISprite mFadeScreen;

	private GameObject mFadeMask;

	private Mesh mMesh;

	private MeshRenderer mMeshRenderer;

	private MeshFilter mMeshFilter;

	public float mMaskInnerSize;

	private float mMaskAlpha;

	private UISprite mFadeScreenAnchor;

	private GameObject mFadeMaskAnchor;

	private Mesh mMeshAnchor;

	private MeshRenderer mMeshRendererAnchor;

	private MeshFilter mMeshFilterAnchor;

	public float mMaskInnerSizeAnchor;

	private float mMaskAlphaAnchor;

	private GameObject anchorBottomRight;

	private bool mTipsDownloadMode;

	private string mTipsAtlasKey;

	private UIPanel mTipsPanel;

	private UISprite mTipsScreenSprite;

	private GameObject mTipsMessageRoot;

	private GameObject mTipsCharacterRoot;

	private UISsSprite LoadingAnime;

	private LoadingMask CurrentLoadingMask;

	private TipsScreen mTipsScreen;

	private bool mHasTipsDisplayed;

	private static float[] posXTbl = new float[4] { -792f, -692f, 692f, 792f };

	private static float[] posYTbl = new float[4] { 792f, 692f, -692f, -792f };

	private static float[] uOfsTbl = new float[4] { 0f, 0.05f, 0.95f, 1f };

	private static float[] vOfsTbl = new float[4] { 1f, 0.95f, 0.05f, 0f };

	private bool mIsTipsScreenDeleting;

	private bool mIsFadeOutMaskForAnchorPlaying;

	public GAME_STATE CurrentState
	{
		get
		{
			return mCurrentGameState;
		}
	}

	public GAME_STATE PrevState
	{
		get
		{
			return mPrevGameState;
		}
	}

	public bool IsFadeout { get; set; }

	public bool IsFadeFinished { get; set; }

	public bool IsTipsScreenVisibled { get; private set; }

	private void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mGameState = CreateGameState(GAME_STATE.LOGO);
		mPrevGameState = GAME_STATE.LOGO;
		mState.Change(STATE.WAIT);
		GameObject parent = mGame.mAnchor.gameObject;
		UIPanel uIPanel = Util.CreateGameObject("FadePanel", parent).AddComponent<UIPanel>();
		uIPanel.renderQueue = UIPanel.RenderQueue.Explicit;
		uIPanel.depth = 100;
		uIPanel.startingRenderQueue = 4000;
		uIPanel.sortingOrder = 500;
		float num = Util.LogLongSideScreenSize();
		ResImage resImage = ResourceManager.LoadImage("FADE");
		mFadeScreen = Util.CreateSprite("FadeScreen", uIPanel.gameObject, resImage.Image);
		Util.SetSpriteInfo(mFadeScreen, "bg_white", 100, new Vector3(0f, 0f, -300f), Vector3.one, false);
		mFadeScreen.SetDimensions((int)num + 2, (int)num + 2);
		mFadeScreen.color = new Color(1f, 1f, 1f, 0f);
		mFadeScreen.type = UIBasicSprite.Type.Sliced;
		IsFadeout = false;
		IsFadeFinished = false;
		mMaskAlpha = 1f;
		MakeFadeMask();
		mFadeMask.SetActive(false);
		SetLayout(Util.ScreenOrientation);
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.LOAD:
			if (mGameState.isLoadFinished())
			{
				if (!mHasTipsDisplayed)
				{
					FadeIn(Color.black);
				}
				mState.Change(STATE.FADEIN);
			}
			break;
		case STATE.FADEIN:
			if (IsFadeFinished)
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.FADEOUT:
			if (IsFadeFinished)
			{
				mGameState.Unload();
				mState.Change(STATE.UNLOAD);
			}
			break;
		case STATE.UNLOAD:
			if (mGameState.isUnloadFinished())
			{
				GameMain.ClearParticle();
				Object.Destroy(mGameState.gameObject);
				mGameState = CreateGameState(mNextGameState);
				mState.Change(STATE.LOAD);
			}
			break;
		}
		mState.Update();
	}

	public void SetNextGameState(GAME_STATE nextState, bool useFadeMask = false)
	{
		mNextGameState = nextState;
		mPrevGameState = mCurrentGameState;
		mGameState.mNextGameStateKey = nextState.ToString();
		mHasTipsDisplayed = false;
		FadeOut(Color.black, useFadeMask);
		mState.Reset(STATE.FADEOUT);
	}

	private GameState CreateGameState(GAME_STATE newState)
	{
		GameState gameState = null;
		PlaySessionManager.PlayPlace place = PlaySessionManager.PlayPlace.NONE;
		switch (newState)
		{
		case GAME_STATE.LOGO:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateLogo", base.gameObject);
			gameState = gameObject.AddComponent<GameStateLogo>();
			place = PlaySessionManager.PlayPlace.NONE;
			break;
		}
		case GAME_STATE.PRETITLE:
		{
			GameObject gameObject = Util.CreateGameObject("GameStatePreTitle", base.gameObject);
			gameState = gameObject.AddComponent<GameStatePreTitle>();
			place = PlaySessionManager.PlayPlace.NONE;
			break;
		}
		case GAME_STATE.TITLE:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateTitle", base.gameObject);
			gameState = gameObject.AddComponent<GameStateTitle>();
			place = PlaySessionManager.PlayPlace.TITLE;
			break;
		}
		case GAME_STATE.MAP:
		{
			mGame.mEventMode = false;
			mGame.mOldEventMode = false;
			GameObject gameObject = Util.CreateGameObject("GameStateMap", base.gameObject);
			gameState = gameObject.AddComponent<GameStateSMMap>();
			place = PlaySessionManager.PlayPlace.MAP;
			break;
		}
		case GAME_STATE.PUZZLE:
		{
			GameObject gameObject = Util.CreateGameObject("GameStatePuzzle", base.gameObject);
			gameState = gameObject.AddComponent<GameStatePuzzle>();
			place = PlaySessionManager.PlayPlace.PUZZLE;
			break;
		}
		case GAME_STATE.EVENT_RALLY:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateEventRally", base.gameObject);
			gameState = gameObject.AddComponent<GameStateEventRally>();
			place = PlaySessionManager.PlayPlace.MAP;
			break;
		}
		case GAME_STATE.EVENT_RALLY2:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateEventRally2", base.gameObject);
			gameState = gameObject.AddComponent<GameStateEventRally2>();
			place = PlaySessionManager.PlayPlace.MAP;
			break;
		}
		case GAME_STATE.EVENT_STAR:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateEventStar", base.gameObject);
			gameState = gameObject.AddComponent<GameStateEventStar>();
			place = PlaySessionManager.PlayPlace.MAP;
			break;
		}
		case GAME_STATE.EVENT_BINGO:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateEventBingo", base.gameObject);
			gameState = gameObject.AddComponent<GameStateEventBingo>();
			place = PlaySessionManager.PlayPlace.MAP;
			break;
		}
		case GAME_STATE.EVENT_LABYRINTH:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateEventLabyrinth", base.gameObject);
			gameState = gameObject.AddComponent<GameStateEventLabyrinth>();
			place = PlaySessionManager.PlayPlace.MAP;
			break;
		}
		case GAME_STATE.TRANSFER:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateTransfer", base.gameObject);
			gameState = gameObject.AddComponent<GameStateTransfer>();
			place = PlaySessionManager.PlayPlace.TITLE;
			break;
		}
		case GAME_STATE.SUPPORT:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateSupport", base.gameObject);
			gameState = gameObject.AddComponent<GameStateSupport>();
			place = PlaySessionManager.PlayPlace.TITLE;
			break;
		}
		case GAME_STATE.COLLECTION:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateCollection", base.gameObject);
			gameState = gameObject.AddComponent<GameStateCollection>();
			place = PlaySessionManager.PlayPlace.OTHER;
			break;
		}
		case GAME_STATE.TROPHY:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateTrophy", base.gameObject);
			gameState = gameObject.AddComponent<GameStateTrophy>();
			place = PlaySessionManager.PlayPlace.OTHER;
			break;
		}
		case GAME_STATE.OPTION:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateOption", base.gameObject);
			gameState = gameObject.AddComponent<GameStateOption>();
			place = PlaySessionManager.PlayPlace.TITLE;
			break;
		}
		case GAME_STATE.OLD_EVENT_GATE:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateOldEvent", base.gameObject);
			gameState = gameObject.AddComponent<GameStateOldEvent>();
			place = PlaySessionManager.PlayPlace.OTHER;
			break;
		}
		case GAME_STATE.DEBUG:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateDebug", base.gameObject);
			gameState = gameObject.AddComponent<GameStateDebug>();
			place = PlaySessionManager.PlayPlace.NONE;
			break;
		}
		case GAME_STATE.SERVICE_FINISH:
		{
			GameObject gameObject = Util.CreateGameObject("GameStateServiceFinish", base.gameObject);
			gameState = gameObject.AddComponent<GameStateServiceFinish>();
			place = PlaySessionManager.PlayPlace.TITLE;
			break;
		}
		}
		if (gameState != null)
		{
			gameState.mGameStateKey = newState.ToString();
		}
		mCurrentGameState = newState;
		if ((bool)PlaySessionManager.Instance)
		{
			PlaySessionManager.Instance.UpdatePlayTimePlace(place);
		}
		return gameState;
	}

	public bool IsInMainState()
	{
		switch (mCurrentGameState)
		{
		case GAME_STATE.MAP:
		case GAME_STATE.PUZZLE:
		case GAME_STATE.EVENT_RALLY:
		case GAME_STATE.EVENT_STAR:
		case GAME_STATE.EVENT_RALLY2:
			return true;
		default:
			return false;
		}
	}

	public bool IsInAdvState()
	{
		GAME_STATE gAME_STATE = mCurrentGameState;
		if (gAME_STATE == GAME_STATE.ADVPUZZLE || gAME_STATE == GAME_STATE.ADVMENU)
		{
			return true;
		}
		return false;
	}

	public bool EnableBackgroundProcess()
	{
		switch (mCurrentGameState)
		{
		case GAME_STATE.MAP:
		case GAME_STATE.EVENT_RALLY:
		case GAME_STATE.EVENT_STAR:
		case GAME_STATE.EVENT_RALLY2:
		case GAME_STATE.TROPHY:
		case GAME_STATE.ADVMENU:
			return true;
		default:
			return false;
		}
	}

	public void OnScreenChanged(ScreenOrientation o)
	{
		if (mGameState != null && mGameState.isLoadFinished())
		{
			mGameState.OnScreenChanged(o);
		}
		SetLayout(o);
	}

	private void SetLayout(ScreenOrientation o)
	{
		if (mTipsScreen != null)
		{
			mTipsScreen.SetLayout(o);
		}
	}

	private void MakeFadeMask()
	{
		ResImage resImage = ResourceManager.LoadImage("FADE");
		mFadeMask = Util.CreateGameObject("FadeMask", GameObject.Find("Anchor"));
		mMeshFilter = mFadeMask.AddComponent<MeshFilter>();
		mMeshRenderer = mFadeMask.AddComponent<MeshRenderer>();
		mMesh = new Mesh();
		Material sharedMaterial = new Material(GameMain.DefaultShader);
		mMeshFilter.sharedMesh = mMesh;
		mMeshRenderer.sharedMaterial = sharedMaterial;
		mMeshRenderer.sharedMaterial.mainTexture = resImage.Image.Texture;
		mMeshRenderer.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRenderer.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		mMeshRenderer.sortingOrder = 9999;
		int num = 9;
		Vector3[] array = new Vector3[num * 4];
		Vector2[] array2 = new Vector2[num * 4];
		int[] array3 = new int[num * 6];
		Color32[] array4 = new Color32[num * 4];
		mMaskInnerSize = 568f;
		string text = "fade_atlas";
		BIJNGUIImage image = resImage.Image;
		for (int i = 0; i < num; i++)
		{
			Vector2 offset = image.GetOffset(text);
			Vector2 size = image.GetSize(text);
			float x = posXTbl[i % 3];
			float y = posYTbl[i / 3];
			float x2 = posXTbl[i % 3 + 1];
			float y2 = posYTbl[i / 3 + 1];
			array[i * 4] = new Vector3(x, y2, 0f);
			array[i * 4 + 1] = new Vector3(x2, y2, 0f);
			array[i * 4 + 2] = new Vector3(x, y, 0f);
			array[i * 4 + 3] = new Vector3(x2, y, 0f);
			float x3 = offset.x + size.x * uOfsTbl[i % 3];
			float y3 = offset.y + size.y * vOfsTbl[i / 3];
			float x4 = offset.x + size.x * uOfsTbl[i % 3 + 1];
			float y4 = offset.y + size.y * vOfsTbl[i / 3 + 1];
			array2[i * 4] = new Vector2(x3, y4);
			array2[i * 4 + 1] = new Vector2(x4, y4);
			array2[i * 4 + 2] = new Vector2(x3, y3);
			array2[i * 4 + 3] = new Vector2(x4, y3);
			array3[i * 6] = i * 4 + 2;
			array3[i * 6 + 1] = i * 4 + 1;
			array3[i * 6 + 2] = i * 4;
			array3[i * 6 + 3] = i * 4 + 2;
			array3[i * 6 + 4] = i * 4 + 3;
			array3[i * 6 + 5] = i * 4 + 1;
			array4[i * 4] = new Color(1f, 1f, 1f, mMaskAlpha);
			array4[i * 4 + 1] = new Color(1f, 1f, 1f, mMaskAlpha);
			array4[i * 4 + 2] = new Color(1f, 1f, 1f, mMaskAlpha);
			array4[i * 4 + 3] = new Color(1f, 1f, 1f, mMaskAlpha);
		}
		mMesh.vertices = array;
		mMesh.uv = array2;
		mMesh.triangles = array3;
		mMesh.colors32 = array4;
	}

	private void UpdateFadeMask()
	{
		float num = 792f;
		Vector3[] vertices = mMesh.vertices;
		vertices[0] = new Vector3(0f - num, mMaskInnerSize, 0f);
		vertices[1] = new Vector3(0f - mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[3] = new Vector3(0f - mMaskInnerSize, num, 0f);
		vertices[4] = new Vector3(0f - mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[5] = new Vector3(mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[6] = new Vector3(0f - mMaskInnerSize, num, 0f);
		vertices[7] = new Vector3(mMaskInnerSize, num, 0f);
		vertices[8] = new Vector3(mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[9] = new Vector3(num, mMaskInnerSize, 0f);
		vertices[10] = new Vector3(mMaskInnerSize, num, 0f);
		vertices[12] = new Vector3(0f - num, 0f - mMaskInnerSize, 0f);
		vertices[13] = new Vector3(0f - mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[14] = new Vector3(0f - num, mMaskInnerSize, 0f);
		vertices[15] = new Vector3(0f - mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[16] = new Vector3(0f - mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[17] = new Vector3(mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[18] = new Vector3(0f - mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[19] = new Vector3(mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[20] = new Vector3(mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[21] = new Vector3(num, 0f - mMaskInnerSize, 0f);
		vertices[22] = new Vector3(mMaskInnerSize, mMaskInnerSize, 0f);
		vertices[23] = new Vector3(num, mMaskInnerSize, 0f);
		vertices[25] = new Vector3(0f - mMaskInnerSize, 0f - num, 0f);
		vertices[26] = new Vector3(0f - num, 0f - mMaskInnerSize, 0f);
		vertices[27] = new Vector3(0f - mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[28] = new Vector3(0f - mMaskInnerSize, 0f - num, 0f);
		vertices[29] = new Vector3(mMaskInnerSize, 0f - num, 0f);
		vertices[30] = new Vector3(0f - mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[31] = new Vector3(mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[32] = new Vector3(mMaskInnerSize, 0f - num, 0f);
		vertices[34] = new Vector3(mMaskInnerSize, 0f - mMaskInnerSize, 0f);
		vertices[35] = new Vector3(num, 0f - mMaskInnerSize, 0f);
		mMesh.vertices = vertices;
		Color[] colors = mMesh.colors;
		for (int i = 0; i < colors.Length; i++)
		{
			colors[i] = new Color(1f, 1f, 1f, mMaskAlpha);
		}
		mMesh.colors = colors;
	}

	public void FadeIn()
	{
		StartCoroutine(FadeInScreen(Color.black));
	}

	private void FadeIn(Color col)
	{
		StartCoroutine(FadeInScreen(col));
	}

	private IEnumerator FadeInScreen(Color col)
	{
		IsFadeFinished = false;
		mFadeScreen.gameObject.SetActive(true);
		Util.TriFunc triFunc = Util.CreateTriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
		bool loop = true;
		while (loop)
		{
			loop = !triFunc.AddDegree(Time.deltaTime * 360f);
			mFadeScreen.color = new Color(col.r, col.g, col.b, 1f - triFunc.Sin());
			yield return 0;
		}
		mFadeScreen.gameObject.SetActive(false);
		IsFadeFinished = true;
		IsFadeout = false;
	}

	public void FadeOutMaskForStory()
	{
		StartCoroutine(FadeOutMaskDemo(Color.black));
	}

	private void FadeOut(Color col, bool useFadeMask = false)
	{
		if (useFadeMask)
		{
			StartCoroutine(FadeOutMask(col));
		}
		else
		{
			StartCoroutine(FadeOutScreen(col));
		}
	}

	private IEnumerator FadeOutScreen(Color col)
	{
		IsFadeFinished = false;
		mFadeScreen.gameObject.SetActive(true);
		Util.TriFunc triFunc = Util.CreateTriFunc(90f, 180f, Util.TriFunc.MAXFUNC.STOP);
		bool loop = true;
		while (loop)
		{
			loop = !triFunc.AddDegree(Time.deltaTime * 240f);
			mFadeScreen.color = new Color(col.r, col.g, col.b, 1f - triFunc.Sin());
			yield return 0;
		}
		IsFadeFinished = true;
		IsFadeout = true;
	}

	private IEnumerator FadeOutMask(Color col)
	{
		IsFadeFinished = false;
		mFadeMask.SetActive(true);
		mMaskAlpha = 0f;
		mMaskInnerSize = 2000f;
		Util.TriFunc triFunc = Util.CreateTriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
		mFadeScreen.gameObject.SetActive(true);
		bool loop2 = true;
		while (loop2)
		{
			mMaskAlpha += 0.1f;
			if (mMaskAlpha > 1f)
			{
				mMaskAlpha = 1f;
			}
			loop2 = !triFunc.AddDegree(Time.deltaTime * 135f);
			mMaskInnerSize = (1f - triFunc.Sin()) * 1800f + 200f;
			UpdateFadeMask();
			float alpha = triFunc.Sin();
			mFadeScreen.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		yield return new WaitForSeconds(0.5f);
		float rgb = 1f;
		loop2 = true;
		while (loop2)
		{
			rgb -= 0.1f;
			if (rgb < 0f)
			{
				rgb = 0f;
				loop2 = false;
			}
			mFadeScreen.color = new Color(rgb, rgb, rgb, 1f);
			yield return 0;
		}
		mFadeMask.SetActive(false);
		IsFadeFinished = true;
		IsFadeout = true;
	}

	private IEnumerator FadeOutMaskDemo(Color col)
	{
		ResourceManager.DisableAsyncLoad(ResourceASyncLoader.WAIT_REQUEST.WAIT_REQ_FADE);
		IsFadeFinished = false;
		bool isLoop = ResourceASyncLoader.mASyncLoadCount > 0;
		if (isLoop)
		{
			float _start = Time.realtimeSinceStartup;
			while (isLoop)
			{
				float timeSpan = Time.realtimeSinceStartup;
				if (timeSpan - _start > 3f)
				{
					break;
				}
				isLoop = ResourceASyncLoader.mASyncLoadCount > 0;
				yield return 0;
			}
		}
		mFadeMask.SetActive(true);
		mMaskAlpha = 0f;
		mMaskInnerSize = 2000f;
		Util.TriFunc triFunc = Util.CreateTriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
		mFadeScreen.gameObject.SetActive(true);
		bool loop2 = true;
		while (loop2)
		{
			mMaskAlpha += 0.1f;
			if (mMaskAlpha > 1f)
			{
				mMaskAlpha = 1f;
			}
			loop2 = !triFunc.AddDegree(Time.deltaTime * 135f);
			mMaskInnerSize = (1f - triFunc.Sin()) * 1800f + 200f;
			UpdateFadeMask();
			float alpha = triFunc.Sin();
			mFadeScreen.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		yield return new WaitForSeconds(0.5f);
		mGameState.GrayOutForce(1f);
		float rgb = 1f;
		loop2 = true;
		while (loop2)
		{
			rgb -= 0.1f;
			if (rgb < 0f)
			{
				rgb = 0f;
				loop2 = false;
			}
			mFadeScreen.color = new Color(rgb, rgb, rgb, 1f);
			yield return 0;
		}
		mFadeMask.SetActive(false);
		IsFadeFinished = true;
		IsFadeout = true;
		ResourceManager.EnableAsyncLoad(ResourceASyncLoader.WAIT_REQUEST.WAIT_REQ_FADE);
	}

	public void TipsScreenOn(byte a_mask = 0, bool downloadMode = false, bool isADV = false)
	{
		StartCoroutine(CoTipsScreenOn(a_mask, downloadMode, isADV));
	}

	public IEnumerator CoTipsScreenOn(byte a_mask, bool downloadMode, bool isADV = false)
	{
		mTipsDownloadMode = downloadMode;
		mTipsScreen = Util.CreateGameObject("TipsScreen", mGame.mAnchor.gameObject).AddComponent<TipsScreen>();
		mTipsScreen.Init(downloadMode, isADV);
		while (!mTipsScreen.IsFirstPanelLoaded())
		{
			yield return null;
		}
		yield return null;
		FadeIn(Color.black);
		mHasTipsDisplayed = true;
		IsTipsScreenVisibled = true;
		anchorBottomRight = Util.CreateAnchorWithChild("TipsAnchorBottomRight", mTipsScreen.gameObject, UIAnchor.Side.BottomRight, mGame.mCamera).gameObject;
		SetLayout(Util.ScreenOrientation);
		SetLoadingUISsAnimation(a_mask);
		if (LoadingAnime != null)
		{
			LoadingAnime.alpha = 1f;
		}
		yield return null;
	}

	public void CreateLoadingAnime()
	{
		if (LoadingAnime != null)
		{
			Object.Destroy(LoadingAnime.gameObject);
			LoadingAnime = null;
		}
		if (!(anchorBottomRight == null))
		{
			Vector3 localPosition = new Vector3(-80f, 100f, 0f);
			Vector3 one = Vector3.one;
			LoadingAnime = Util.CreateGameObject("Loading", anchorBottomRight.gameObject).AddComponent<UISsSprite>();
			LoadingAnime.Animation = ResourceManager.LoadSsAnimation("LOADING").SsAnime;
			LoadingAnime.depth = 104;
			LoadingAnime.transform.localPosition = localPosition;
			LoadingAnime.transform.localScale = one;
			LoadingAnime.PlayCount = 0;
			LoadingAnime.Play();
			UIPanel uIPanel = LoadingAnime.gameObject.AddComponent<UIPanel>();
			uIPanel.sortingOrder = 503;
			uIPanel.depth = 503;
		}
	}

	public void CreateConnectingAnime()
	{
		if (LoadingAnime != null)
		{
			Object.Destroy(LoadingAnime.gameObject);
			LoadingAnime = null;
		}
		if (!(anchorBottomRight == null))
		{
			Vector3 localPosition = new Vector3(-80f, 100f, 0f);
			Vector3 one = Vector3.one;
			LoadingAnime = Util.CreateGameObject("Connecting", anchorBottomRight.gameObject).AddComponent<UISsSprite>();
			LoadingAnime.Animation = ResourceManager.LoadSsAnimation("CONNECTING").SsAnime;
			LoadingAnime.depth = 104;
			LoadingAnime.transform.localPosition = localPosition;
			LoadingAnime.transform.localScale = one;
			LoadingAnime.PlayCount = 0;
			LoadingAnime.Play();
			UIPanel uIPanel = LoadingAnime.gameObject.AddComponent<UIPanel>();
			uIPanel.sortingOrder = 503;
			uIPanel.depth = 503;
		}
	}

	public void SetLoadingUISsAnimation(byte a_mask)
	{
		if ((uint)CurrentLoadingMask == a_mask)
		{
			return;
		}
		if (a_mask > 0)
		{
			switch (a_mask)
			{
			case 1:
				CreateLoadingAnime();
				break;
			case 2:
				CreateConnectingAnime();
				break;
			default:
				if (LoadingAnime != null)
				{
					Object.Destroy(LoadingAnime.gameObject);
					LoadingAnime = null;
				}
				break;
			}
		}
		CurrentLoadingMask = (LoadingMask)a_mask;
	}

	public void TipsScreenOff()
	{
		if (!mIsTipsScreenDeleting)
		{
			StartCoroutine(CoTipsScreenOff());
		}
	}

	private IEnumerator CoTipsScreenOff()
	{
		if (mTipsScreen == null)
		{
			IsTipsScreenVisibled = false;
			yield break;
		}
		mIsTipsScreenDeleting = true;
		if (LoadingAnime != null)
		{
			Object.Destroy(LoadingAnime.gameObject);
			LoadingAnime = null;
		}
		if (mTipsScreen != null)
		{
			mTipsScreen.SetDeleteRequest();
			while (!mTipsScreen.IsDeleted)
			{
				yield return null;
			}
			mTipsScreen.Delete();
			IsTipsScreenVisibled = false;
			Object.Destroy(mTipsScreen.gameObject);
			mTipsScreen = null;
		}
		mIsTipsScreenDeleting = false;
		CurrentLoadingMask = LoadingMask.NONE;
		yield return Resources.UnloadUnusedAssets();
	}

	private void MakeFadeMask(string a_anchorName)
	{
		ResImage resImage = ResourceManager.LoadImage("FADE");
		mFadeMaskAnchor = Util.CreateGameObject("FadeMask", GameObject.Find(a_anchorName));
		mMeshFilterAnchor = mFadeMaskAnchor.AddComponent<MeshFilter>();
		mMeshRendererAnchor = mFadeMaskAnchor.AddComponent<MeshRenderer>();
		mMeshAnchor = new Mesh();
		Material sharedMaterial = new Material(GameMain.DefaultShader);
		mMeshFilterAnchor.sharedMesh = mMeshAnchor;
		mMeshRendererAnchor.sharedMaterial = sharedMaterial;
		mMeshRendererAnchor.sharedMaterial.mainTexture = resImage.Image.Texture;
		mMeshRendererAnchor.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRendererAnchor.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		if (Util.IsWideScreen())
		{
			float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
			float num2 = 0.5625f;
			float num3 = 0.4617052f;
			float num4 = 1f + (num2 - num) / (num2 - num3) * 0.05f;
			mFadeMaskAnchor.transform.SetLocalScale(num4, num4, 1f);
		}
		mMeshRendererAnchor.sortingOrder = 9999;
		int num5 = 9;
		Vector3[] array = new Vector3[num5 * 4];
		Vector2[] array2 = new Vector2[num5 * 4];
		int[] array3 = new int[num5 * 6];
		Color32[] array4 = new Color32[num5 * 4];
		mMaskInnerSizeAnchor = 568f;
		string text = "fade_atlas";
		BIJNGUIImage image = resImage.Image;
		for (int i = 0; i < num5; i++)
		{
			Vector2 offset = image.GetOffset(text);
			Vector2 size = image.GetSize(text);
			float x = posXTbl[i % 3];
			float y = posYTbl[i / 3];
			float x2 = posXTbl[i % 3 + 1];
			float y2 = posYTbl[i / 3 + 1];
			array[i * 4] = new Vector3(x, y2, 0f);
			array[i * 4 + 1] = new Vector3(x2, y2, 0f);
			array[i * 4 + 2] = new Vector3(x, y, 0f);
			array[i * 4 + 3] = new Vector3(x2, y, 0f);
			float x3 = offset.x + size.x * uOfsTbl[i % 3];
			float y3 = offset.y + size.y * vOfsTbl[i / 3];
			float x4 = offset.x + size.x * uOfsTbl[i % 3 + 1];
			float y4 = offset.y + size.y * vOfsTbl[i / 3 + 1];
			array2[i * 4] = new Vector2(x3, y4);
			array2[i * 4 + 1] = new Vector2(x4, y4);
			array2[i * 4 + 2] = new Vector2(x3, y3);
			array2[i * 4 + 3] = new Vector2(x4, y3);
			array3[i * 6] = i * 4 + 2;
			array3[i * 6 + 1] = i * 4 + 1;
			array3[i * 6 + 2] = i * 4;
			array3[i * 6 + 3] = i * 4 + 2;
			array3[i * 6 + 4] = i * 4 + 3;
			array3[i * 6 + 5] = i * 4 + 1;
			array4[i * 4] = new Color(1f, 1f, 1f, mMaskAlphaAnchor);
			array4[i * 4 + 1] = new Color(1f, 1f, 1f, mMaskAlphaAnchor);
			array4[i * 4 + 2] = new Color(1f, 1f, 1f, mMaskAlphaAnchor);
			array4[i * 4 + 3] = new Color(1f, 1f, 1f, mMaskAlphaAnchor);
		}
		mMeshAnchor.vertices = array;
		mMeshAnchor.uv = array2;
		mMeshAnchor.triangles = array3;
		mMeshAnchor.colors32 = array4;
	}

	private void UpdateFadeMaskAnchor()
	{
		float num = 668f;
		Vector3[] vertices = mMeshAnchor.vertices;
		vertices[0] = new Vector3(0f - num, mMaskInnerSizeAnchor, 0f);
		vertices[1] = new Vector3(0f - mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[3] = new Vector3(0f - mMaskInnerSizeAnchor, num, 0f);
		vertices[4] = new Vector3(0f - mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[5] = new Vector3(mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[6] = new Vector3(0f - mMaskInnerSizeAnchor, num, 0f);
		vertices[7] = new Vector3(mMaskInnerSizeAnchor, num, 0f);
		vertices[8] = new Vector3(mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[9] = new Vector3(num, mMaskInnerSizeAnchor, 0f);
		vertices[10] = new Vector3(mMaskInnerSizeAnchor, num, 0f);
		vertices[12] = new Vector3(0f - num, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[13] = new Vector3(0f - mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[14] = new Vector3(0f - num, mMaskInnerSizeAnchor, 0f);
		vertices[15] = new Vector3(0f - mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[16] = new Vector3(0f - mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[17] = new Vector3(mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[18] = new Vector3(0f - mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[19] = new Vector3(mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[20] = new Vector3(mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[21] = new Vector3(num, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[22] = new Vector3(mMaskInnerSizeAnchor, mMaskInnerSizeAnchor, 0f);
		vertices[23] = new Vector3(num, mMaskInnerSizeAnchor, 0f);
		vertices[25] = new Vector3(0f - mMaskInnerSizeAnchor, 0f - num, 0f);
		vertices[26] = new Vector3(0f - num, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[27] = new Vector3(0f - mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[28] = new Vector3(0f - mMaskInnerSizeAnchor, 0f - num, 0f);
		vertices[29] = new Vector3(mMaskInnerSizeAnchor, 0f - num, 0f);
		vertices[30] = new Vector3(0f - mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[31] = new Vector3(mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[32] = new Vector3(mMaskInnerSizeAnchor, 0f - num, 0f);
		vertices[34] = new Vector3(mMaskInnerSizeAnchor, 0f - mMaskInnerSizeAnchor, 0f);
		vertices[35] = new Vector3(num, 0f - mMaskInnerSizeAnchor, 0f);
		mMeshAnchor.vertices = vertices;
		Color[] colors = mMeshAnchor.colors;
		for (int i = 0; i < colors.Length; i++)
		{
			colors[i] = new Color(1f, 1f, 1f, mMaskAlphaAnchor);
		}
		mMeshAnchor.colors = colors;
	}

	public void FadeOutMaskForAnchor(string a_anchorName)
	{
		if (!mIsFadeOutMaskForAnchorPlaying)
		{
			MakeFadeMask(a_anchorName);
			UIPanel uIPanel = Util.CreateGameObject("FadePanel", mFadeMaskAnchor).AddComponent<UIPanel>();
			uIPanel.renderQueue = UIPanel.RenderQueue.Explicit;
			uIPanel.depth = 100;
			uIPanel.startingRenderQueue = 4000;
			uIPanel.sortingOrder = 500;
			ResImage resImage = ResourceManager.LoadImage("FADE");
			mFadeScreenAnchor = Util.CreateSprite("FadeScreen", uIPanel.gameObject, resImage.Image);
			Util.SetSpriteInfo(mFadeScreenAnchor, "bg_white", 100, new Vector3(0f, 0f, -300f), Vector3.one, false);
			mFadeScreenAnchor.SetDimensions(1138, 1138);
			mFadeScreenAnchor.color = new Color(1f, 1f, 1f, 0f);
			mFadeScreenAnchor.type = UIBasicSprite.Type.Sliced;
			StartCoroutine(FadeOutMaskAnchor(Color.red));
			mIsFadeOutMaskForAnchorPlaying = true;
		}
	}

	private IEnumerator FadeOutMaskAnchor(Color col)
	{
		ResourceManager.DisableAsyncLoad(ResourceASyncLoader.WAIT_REQUEST.WAIT_REQ_FADE);
		IsFadeFinished = false;
		bool isLoop = ResourceASyncLoader.mASyncLoadCount > 0;
		if (isLoop)
		{
			mFadeMaskAnchor.SetActive(false);
			float _start = Time.realtimeSinceStartup;
			while (isLoop)
			{
				float timeSpan = Time.realtimeSinceStartup;
				if (timeSpan - _start > 3f)
				{
					break;
				}
				isLoop = ResourceASyncLoader.mASyncLoadCount > 0;
				yield return 0;
			}
		}
		mFadeMaskAnchor.SetActive(true);
		mMaskAlphaAnchor = 0f;
		mMaskInnerSizeAnchor = 2000f;
		Util.TriFunc triFunc = Util.CreateTriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
		mFadeScreenAnchor.gameObject.SetActive(true);
		bool loop2 = true;
		while (loop2)
		{
			mMaskAlphaAnchor += 0.1f;
			if (mMaskAlphaAnchor > 1f)
			{
				mMaskAlphaAnchor = 1f;
			}
			loop2 = !triFunc.AddDegree(Time.deltaTime * 135f);
			mMaskInnerSizeAnchor = (1f - triFunc.Sin()) * 1800f + 200f;
			UpdateFadeMaskAnchor();
			float alpha = triFunc.Sin();
			mFadeScreenAnchor.color = new Color(1f, 1f, 1f, alpha);
			yield return 0;
		}
		yield return new WaitForSeconds(0.5f);
		float rgb = 1f;
		loop2 = true;
		while (loop2)
		{
			rgb -= 0.1f;
			if (rgb < 0f)
			{
				rgb = 0f;
				loop2 = false;
			}
			mFadeScreenAnchor.color = new Color(rgb, rgb, rgb, 1f);
			yield return 0;
		}
		IsFadeFinished = true;
		IsFadeout = true;
		ResourceManager.EnableAsyncLoad(ResourceASyncLoader.WAIT_REQUEST.WAIT_REQ_FADE);
		mIsFadeOutMaskForAnchorPlaying = false;
	}
}
