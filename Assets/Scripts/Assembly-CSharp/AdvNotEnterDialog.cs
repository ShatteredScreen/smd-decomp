using UnityEngine;

public class AdvNotEnterDialog : DialogBase
{
	public enum SELECT_TYPE
	{
		STAGE_NO_CLEAR = 0,
		MAINTENANCE_ADV = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed();

	private SELECT_TYPE mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = null;
		text = ((mSelectItem != 0) ? Localization.Get("GameCenter_Error_Title") : Localization.Get("GameCenter_Error_Title01"));
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(text);
		text = ((mSelectItem != 0) ? Localization.Get("GameCenter_Error_Desc00B") : Localization.Get("GameCenter_Error_Desc00"));
		UILabel uILabel = Util.CreateLabel("Text1", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(0f, 123f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
		uILabel.spacingY = 6;
		text = Localization.Get("GameCenter_Error_Desc01");
		uILabel = Util.CreateLabel("Text2", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(0f, -50f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel, Color.red);
		uILabel.spacingY = 6;
		string text2 = "LC_button_close";
		UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, image);
		Util.SetImageButtonInfo(button, text2, text2, text2, mBaseDepth + 3, new Vector3(0f, -218f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(SELECT_TYPE type)
	{
		mSelectItem = type;
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
