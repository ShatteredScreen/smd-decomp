using System.Collections.Generic;

public class GetSNSFriendList : ParameterObject<GetSNSFriendList>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("targets")]
	[MiniJSONArray(typeof(string))]
	public List<SNSFriendData> Target { get; set; }

	public GetSNSFriendList()
	{
		Target = new List<SNSFriendData>();
	}

	public void SetTarget(List<string> a_list, SNS kind)
	{
		for (int i = 0; i < a_list.Count; i++)
		{
			SNSFriendData sNSFriendData = new SNSFriendData();
			if (kind == SNS.Facebook)
			{
				sNSFriendData.fbid = a_list[i];
				Target.Add(sNSFriendData);
			}
		}
	}
}
