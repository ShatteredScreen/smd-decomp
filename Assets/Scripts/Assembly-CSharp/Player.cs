using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Player : ICloneable, IPlayer
{
	public enum STAGE_STATUS
	{
		NOOPEN = 0,
		LOCK = 1,
		UNLOCK = 2,
		CLEAR = 3
	}

	public enum NOTIFICATION_STATUS
	{
		NONE = 0,
		NOTIFY = 1,
		NOTIFIED = 2
	}

	private const string ICON_PATH_FORMAT = "my.dat";

	public const string PPKEY_INFINITELIFE_DURATION = "InfiniteLifeDuration";

	public const string PPKEY_INFINITELIFE_REMAIN_HEART = "InfiniteLifeReaminHeart";

	public const string PPKEY_INFINITELIFE_PREV_WINCOUNT = "InfiniteLifePrevWinCount";

	public const string PPKEY_INFINITELIFE_PREV_LOSECOUNT = "InfiniteLifePrevLoseCount";

	public PlayerData Data;

	public List<int> StageOpenList = new List<int>();

	public List<int> StageUnlockList = new List<int>();

	public List<int> AvatarMoveList = new List<int>();

	public List<int> LineOpenList = new List<int>();

	public List<int> LineUnlockList = new List<int>();

	public bool ShowSignAnimation;

	public bool ShowClearAnimation;

	public bool ShowCompleteAnimation;

	public List<int> StageClearList = new List<int>();

	public List<int> UnlockedRBList = new List<int>();

	public Dictionary<int, int> mCompanionXPTemp = new Dictionary<int, int>();

	private int mClearLevelCache = -1;

	private int mTotalStarsCache = -1;

	private long mTotalHighScoreCache = -1L;

	private long mTotalTrophyCache = -1L;

	public Texture2D mIcon;

	public DateTime? ShowSaleIconTime;

	public bool mGetSupport;

	public bool mGetCampaign;

	public bool mSumError;

	public bool mIsNewPlayer;

	private bool mIsExtendLifeUse;

	public DateTime mLastLoginTime;

	public IList<GiftItem> mModifiedGifts;

	public Dictionary<Def.SERIES, IDictionary<int, PlayerClearData>> StageClearData;

	public Dictionary<Def.SERIES, IDictionary<int, PlayerStageData>> StageChallengeData;

	public IDictionary<int, MyFriend> Friends;

	public IDictionary<string, GiftItem> Gifts;

	public IDictionary<int, MyFriend> ApplyFriends;

	public IDictionary<int, PlayerShopData> PurchaseShop;

	public IDictionary<int, PlayerSendData> SendInfo;

	public IDictionary<int, TrophyExchangeInfo> TrophyExchangeDict;

	public bool mIsUpdateFirstStartup;

	public int ClearStageToS;

	public long ServerTime;

	public AdvSaveData AdvSaveData = AdvSaveData.InitAdvData();

	private int mTotalClearStageCache = -1;

	private int nowAdvMaxStamina = -1;

	public int UUID
	{
		get
		{
			return Data.UUID;
		}
	}

	public string NickName
	{
		get
		{
			return Data.NickName;
		}
		set
		{
			Data.NickName = value;
		}
	}

	public int HiveId
	{
		get
		{
			return Data.HiveId;
		}
		set
		{
			Data.HiveId = value;
		}
	}

	public string SearchID
	{
		get
		{
			return Data.SearchID;
		}
		set
		{
			Data.SearchID = value;
		}
	}

	public int LevelToS
	{
		get
		{
			return GetServerLevel((Def.SERIES)SeriesToS);
		}
	}

	public int SeriesToS
	{
		get
		{
			if (Def.IsEventSeries(Data.CurrentSeries) || Def.IsAdvSeries(Data.CurrentSeries))
			{
				if (!Def.IsEventSeries(Data.PreviousSeries) && !Def.IsAdvSeries(Data.PreviousSeries))
				{
					return (int)Data.PreviousSeries;
				}
				return 0;
			}
			return (int)Data.CurrentSeries;
		}
	}

	public int CompanionIDToS
	{
		get
		{
			return Data.PlayerIcon;
		}
	}

	public int CompanionLevelToS
	{
		get
		{
			int num = CompanionIDToS;
			if (num >= 10000)
			{
				num -= 10000;
			}
			if (num < 0 || !IsCompanionUnlock(num))
			{
				return 0;
			}
			return GetCompanionLevel(num);
		}
	}

	public int MasterCurrency
	{
		get
		{
			return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 3);
		}
	}

	public int SecondaryCurrency
	{
		get
		{
			return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 4);
		}
	}

	public int Experience
	{
		get
		{
			return (int)Math.Floor(Data.TotalPlayTime);
		}
	}

	public int ClearSeriesToS
	{
		get
		{
			return (int)Data.CurrentSeries;
		}
	}

	public int ClearStageNoToS
	{
		get
		{
			return ClearStageToS;
		}
	}

	public int LastSeriesToS
	{
		get
		{
			return (int)Data.LastPlaySeries;
		}
	}

	public int LastStageNoToS
	{
		get
		{
			return Data.LastPlayLevel;
		}
	}

	public long TotalScoreToS
	{
		get
		{
			return TotalHighScore;
		}
	}

	public long TotalStarToS
	{
		get
		{
			return TotalStars;
		}
	}

	public long TotalTrophyToS
	{
		get
		{
			return TotalTrophy;
		}
	}

	public long ServerTimeToS
	{
		get
		{
			return ServerTime;
		}
		set
		{
			ServerTime = value;
		}
	}

	public long PlayDays
	{
		get
		{
			return Data.BootCount;
		}
	}

	public long IntervalDays
	{
		get
		{
			return Data.IntervalCount;
		}
	}

	public long ContinuousDays
	{
		get
		{
			return Data.ContinuousCount;
		}
	}

	public List<PlayStageParam> PlayStage
	{
		get
		{
			List<PlayStageParam> list = new List<PlayStageParam>();
			GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
			List<Def.SERIES> list2 = new List<Def.SERIES>(Def.SeriesMapRouteData.Keys);
			list2.Sort((Def.SERIES a, Def.SERIES b) => a - b);
			foreach (Def.SERIES item in list2)
			{
				if (!Def.IsEventSeries(item) && !Def.IsAdvSeries(item) && instance.mGameProfile.IsSeriesAvailable(item))
				{
					int serverLevel = GetServerLevel(item);
					if (serverLevel >= 100)
					{
						PlayStageParam playStageParam = new PlayStageParam();
						playStageParam.Series = (int)item;
						playStageParam.Level = serverLevel;
						list.Add(playStageParam);
					}
					else
					{
						PlayStageParam playStageParam2 = new PlayStageParam();
						playStageParam2.Series = (int)item;
						playStageParam2.Level = 100;
						list.Add(playStageParam2);
					}
				}
			}
			return list;
		}
	}

	public int AdvLastStage
	{
		get
		{
			List<int> list = AdvStageCheck();
			if (list.Count <= 0)
			{
				return 0;
			}
			list.Sort();
			list.Reverse();
			return list[0];
		}
	}

	public int AdvAreaStageCount
	{
		get
		{
			List<int> list = AdvStageCheck();
			if (list.Count <= 0)
			{
				return 0;
			}
			return list.Count;
		}
	}

	public int MaxLifeCount
	{
		get
		{
			return Data.MaxNormalLifeCount + Data.MaxExtendLifeCount;
		}
	}

	public int LifeCount
	{
		get
		{
			int itemCount = GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1);
			int itemCount2 = GetItemCount(Def.ITEM_CATEGORY.CONSUME, 2);
			return itemCount + itemCount2;
		}
	}

	public int Dollar
	{
		get
		{
			int itemCount = GetItemCount(Def.ITEM_CATEGORY.CONSUME, 3);
			int itemCount2 = GetItemCount(Def.ITEM_CATEGORY.CONSUME, 4);
			return itemCount + itemCount2;
		}
	}

	public int RoadBlockTicket
	{
		get
		{
			return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 7);
		}
		set
		{
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 7, value);
		}
	}

	public int Level
	{
		get
		{
			PlayerMapData data;
			Data.GetMapData(Data.CurrentSeries, out data);
			if (data == null)
			{
				return Def.GetStageNo(1, 0);
			}
			return data.PlayableMaxLevel;
		}
	}

	public int ClearLevel
	{
		get
		{
			if (mClearLevelCache != -1)
			{
				return mClearLevelCache;
			}
			int num = 0;
			if (StageClearData != null && StageClearData.Count > 0)
			{
				IDictionary<int, PlayerClearData> dictionary = StageClearData[Data.CurrentSeries];
				foreach (KeyValuePair<int, PlayerClearData> item in dictionary)
				{
					PlayerClearData value = item.Value;
					if (value != null)
					{
						int a_main;
						int a_sub;
						Def.SplitStageNo(value.StageNo, out a_main, out a_sub);
						num = Math.Max(num, a_main);
					}
				}
			}
			mClearLevelCache = num;
			return num;
		}
	}

	public int TotalStars
	{
		get
		{
			if (mTotalStarsCache != -1)
			{
				return mTotalStarsCache;
			}
			int num = 0;
			if (StageClearData != null && StageClearData.Count > 0)
			{
				foreach (KeyValuePair<Def.SERIES, IDictionary<int, PlayerClearData>> stageClearDatum in StageClearData)
				{
					foreach (KeyValuePair<int, PlayerClearData> item in stageClearDatum.Value)
					{
						PlayerClearData value = item.Value;
						if (value != null)
						{
							num += value.GotStars;
						}
					}
				}
			}
			if (Data.PlayerEventDataList.Count > 0)
			{
				foreach (PlayerEventData playerEventData in Data.PlayerEventDataList)
				{
					foreach (KeyValuePair<short, PlayerMapData> courseDatum in playerEventData.CourseData)
					{
						foreach (PlayerClearData stageClearDatum2 in courseDatum.Value.StageClearData)
						{
							if (stageClearDatum2 != null)
							{
								num += stageClearDatum2.GotStars;
							}
						}
					}
				}
			}
			mTotalStarsCache = num;
			return num;
		}
	}

	public long TotalHighScore
	{
		get
		{
			if (mTotalHighScoreCache != -1)
			{
				return mTotalHighScoreCache;
			}
			long num = 0L;
			if (StageClearData != null && StageClearData.Count > 0)
			{
				foreach (KeyValuePair<Def.SERIES, IDictionary<int, PlayerClearData>> stageClearDatum in StageClearData)
				{
					foreach (KeyValuePair<int, PlayerClearData> item in stageClearDatum.Value)
					{
						PlayerClearData value = item.Value;
						if (value != null)
						{
							num += value.HightScore;
						}
					}
				}
			}
			if (Data.PlayerEventDataList.Count > 0)
			{
				foreach (PlayerEventData playerEventData in Data.PlayerEventDataList)
				{
					foreach (KeyValuePair<short, PlayerMapData> courseDatum in playerEventData.CourseData)
					{
						foreach (PlayerClearData stageClearDatum2 in courseDatum.Value.StageClearData)
						{
							if (stageClearDatum2 != null)
							{
								num += stageClearDatum2.HightScore;
							}
						}
					}
				}
			}
			mTotalHighScoreCache = num;
			return num;
		}
	}

	public long TotalTrophy
	{
		get
		{
			if (mTotalTrophyCache != -1)
			{
				return mTotalTrophyCache;
			}
			return mTotalTrophyCache = GetTotalTrophy();
		}
	}

	public Texture2D Icon
	{
		get
		{
			return mIcon;
		}
		set
		{
			mIcon = value;
		}
	}

	public string LastGiftID
	{
		get
		{
			if (Gifts == null || Gifts.Count < 1)
			{
				return null;
			}
			List<GiftItem> list = new List<GiftItem>(Gifts.Values);
			list.Sort();
			return list[0].Data.GiftID;
		}
	}

	public float WinRate
	{
		get
		{
			if (Data.TotalWin + Data.TotalLose < 1)
			{
				return 0f;
			}
			double num = (double)Data.TotalWin / (double)(Data.TotalWin + Data.TotalLose);
			return Convert.ToSingle(num * 100.0);
		}
	}

	public string IconFileName
	{
		get
		{
			return string.Format("my.dat", UUID);
		}
	}

	public PlayerMapData CurrentMapData
	{
		get
		{
			PlayerMapData data = null;
			if (Def.IsEventSeries(Data.CurrentSeries))
			{
				PlayerEventData data2;
				Data.GetMapData(Data.CurrentSeries, Data.CurrentEventID, out data2);
				if (data2 != null)
				{
					data = ((data2.CourseID >= 0) ? data2.CourseData[data2.CourseID] : null);
				}
			}
			else if (Def.IsAdvSeries(Data.CurrentSeries))
			{
				data = null;
			}
			else
			{
				Data.GetMapData(Data.CurrentSeries, out data);
			}
			return data;
		}
	}

	public int CurrentLevel
	{
		get
		{
			int result = -1;
			PlayerMapData currentMapData = CurrentMapData;
			if (currentMapData != null)
			{
				result = currentMapData.CurrentLevel;
			}
			return result;
		}
	}

	public int PlayableMaxLevel
	{
		get
		{
			int result = -1;
			PlayerMapData currentMapData = CurrentMapData;
			if (currentMapData != null)
			{
				result = currentMapData.PlayableMaxLevel;
			}
			return result;
		}
	}

	public int OpenNoticeLevel
	{
		get
		{
			int result = -1;
			PlayerMapData currentMapData = CurrentMapData;
			if (currentMapData != null)
			{
				result = currentMapData.OpenNoticeLevel;
			}
			return result;
		}
	}

	public int LastClearedLevel
	{
		get
		{
			int result = -1;
			PlayerMapData currentMapData = CurrentMapData;
			if (currentMapData != null)
			{
				result = currentMapData.LastClearedLevel;
			}
			return result;
		}
	}

	public int NextRoadBlockLevel
	{
		get
		{
			int result = 0;
			PlayerMapData currentMapData = CurrentMapData;
			if (currentMapData != null)
			{
				result = currentMapData.NextRoadBlockLevel;
			}
			return result;
		}
	}

	public int RoadBlockReachLevel
	{
		get
		{
			int result = 0;
			PlayerMapData currentMapData = CurrentMapData;
			if (currentMapData != null)
			{
				result = currentMapData.RoadBlockReachLevel;
			}
			return result;
		}
	}

	public DateTime RoadBlockReachTime
	{
		get
		{
			DateTime result = DateTimeUtil.UnitLocalTimeEpoch;
			PlayerMapData currentMapData = CurrentMapData;
			if (currentMapData != null)
			{
				result = currentMapData.RoadBlockReachTime;
			}
			return result;
		}
	}

	public int HeartUpPiece
	{
		get
		{
			return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 9);
		}
		set
		{
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 9, value);
		}
	}

	public int GrowUpPiece
	{
		get
		{
			return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 11);
		}
		set
		{
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 11, value);
		}
	}

	public int GrowUp
	{
		get
		{
			return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 10);
		}
		set
		{
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 10, value);
		}
	}

	public int TotalClearStage
	{
		get
		{
			if (mTotalClearStageCache != -1)
			{
				return mTotalClearStageCache;
			}
			mTotalClearStageCache = 0;
			for (Def.SERIES sERIES = Def.SERIES.SM_FIRST; sERIES <= Def.SeriesGaidenMax; sERIES++)
			{
				PlayerMapData data;
				Data.GetMapData(sERIES, out data);
				if (data != null)
				{
					mTotalClearStageCache += data.GetClearedMainStage();
				}
			}
			return mTotalClearStageCache;
		}
	}

	public int AdvMaxStamina
	{
		get
		{
			if (TotalClearStage > 0 && TotalClearStage > Constants.ADV_CLEARSTAGE_BORDER)
			{
				return Constants.ADV_FIRST_STAMINA + (TotalClearStage - Constants.ADV_CLEARSTAGE_BORDER) / Constants.ADV_CONVERT_RATE;
			}
			return Constants.ADV_FIRST_STAMINA;
		}
	}

	public int AdvStamina
	{
		get
		{
			return AdvSaveData.mRestStamina;
		}
	}

	public Player()
	{
		InitWork();
	}

	public void SetUUID(int a_uuid)
	{
		Data.UUID = a_uuid;
	}

	public List<int> AdvStageCheck()
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		List<AdvDungeonData> list = new List<AdvDungeonData>();
		List<AdvDungeonData> dungeonAllList = instance.GetDungeonAllList();
		for (int i = 0; i < dungeonAllList.Count; i++)
		{
			if (dungeonAllList[i].IsUnlock())
			{
				list.Add(dungeonAllList[i]);
			}
		}
		list.Sort();
		List<int> list2 = new List<int>();
		for (int j = 0; j < list.Count; j++)
		{
			List<AdvStageData> advStageDataListFromDungeon = instance.GetAdvStageDataListFromDungeon(list[j].index);
			if (advStageDataListFromDungeon == null)
			{
				continue;
			}
			advStageDataListFromDungeon.Sort();
			for (int k = 0; k < advStageDataListFromDungeon.Count; k++)
			{
				if (advStageDataListFromDungeon[k].IsUnlock())
				{
					list2.Add(advStageDataListFromDungeon[k].index);
				}
			}
		}
		return list2;
	}

	public void OnSerialize()
	{
		if (Data == null)
		{
			BIJLog.E("ERROR Data is null!!!!!");
			return;
		}
		Data.FriendData.Clear();
		foreach (KeyValuePair<int, MyFriend> friend in Friends)
		{
			MyFriend value = friend.Value;
			value.Data.Version = 1290;
			Data.FriendData.Add(value.Data);
			value.SaveIcon();
		}
		Data.GiftData.Clear();
		foreach (KeyValuePair<string, GiftItem> gift in Gifts)
		{
			GiftItem value2 = gift.Value;
			value2.Data.Version = 1290;
			Data.GiftData.Add(value2.Data);
		}
		Data.ApplyFriendData.Clear();
		foreach (KeyValuePair<int, MyFriend> applyFriend in ApplyFriends)
		{
			MyFriend value3 = applyFriend.Value;
			value3.Data.Version = 1290;
			Data.ApplyFriendData.Add(value3.Data);
		}
		Data.ShopPurchaseNumData.Clear();
		foreach (KeyValuePair<int, PlayerShopData> item in PurchaseShop)
		{
			PlayerShopData value4 = item.Value;
			value4.Version = 1290;
			Data.ShopPurchaseNumData.Add(value4);
		}
		Data.GameSendData.Clear();
		foreach (KeyValuePair<int, PlayerSendData> item2 in SendInfo)
		{
			PlayerSendData value5 = item2.Value;
			value5.Version = 1290;
			Data.GameSendData.Add(value5);
		}
		Data.TrophyExchangeInfoList.Clear();
		foreach (KeyValuePair<int, TrophyExchangeInfo> item3 in TrophyExchangeDict)
		{
			TrophyExchangeInfo value6 = item3.Value;
			Data.TrophyExchangeInfoList.Add(value6);
		}
		Data.OnSerialize(this);
	}

	public void OnDeserialized()
	{
		if (Data == null)
		{
			BIJLog.E("Load Failed!!!!!");
			Data = new PlayerData();
			Data.CreateNewData();
		}
		if (Data.FriendData == null)
		{
			Data.FriendData = new List<MyFriendData>();
		}
		Friends.Clear();
		foreach (MyFriendData friendDatum in Data.FriendData)
		{
			MyFriend myFriend = new MyFriend();
			myFriend.SetData(friendDatum);
			Friends.Add(friendDatum.UUID, myFriend);
			myFriend.LoadIcon();
		}
		if (Data.GiftData == null)
		{
			Data.GiftData = new List<GiftItemData>();
		}
		Gifts.Clear();
		foreach (GiftItemData giftDatum in Data.GiftData)
		{
			GiftItem value = new GiftItem(giftDatum);
			Gifts.Add(giftDatum.GiftID, value);
		}
		if (Data.ApplyFriendData == null)
		{
			Data.ApplyFriendData = new List<MyFriendData>();
		}
		ApplyFriends.Clear();
		foreach (MyFriendData applyFriendDatum in Data.ApplyFriendData)
		{
			MyFriend myFriend2 = new MyFriend();
			myFriend2.SetData(applyFriendDatum);
			ApplyFriends.Add(applyFriendDatum.UUID, myFriend2);
		}
		if (Data.ShopPurchaseNumData == null)
		{
			Data.ShopPurchaseNumData = new List<PlayerShopData>();
		}
		PurchaseShop.Clear();
		foreach (PlayerShopData shopPurchaseNumDatum in Data.ShopPurchaseNumData)
		{
			PurchaseShop.Add(shopPurchaseNumDatum.ShopItemID, shopPurchaseNumDatum);
		}
		if (Data.GameSendData == null)
		{
			Data.GameSendData = new List<PlayerSendData>();
		}
		SendInfo.Clear();
		DateTime now = DateTime.Now;
		foreach (PlayerSendData gameSendDatum in Data.GameSendData)
		{
			bool flag = DateTimeUtil.BetweenDays0(gameSendDatum.LastHeartSendTime, now) >= Constants.SEND_HEART_FREQ;
			bool flag2 = DateTimeUtil.BetweenDays0(gameSendDatum.LastFriendHelpRequestTime, now) >= Constants.SEND_FRIENDHELP_FREQ;
			bool flag3 = DateTimeUtil.BetweenDays0(gameSendDatum.LastRoadBlockRequestTime, now) >= Constants.SEND_ROADBLOCK_FREQ;
			if (!flag || !flag2 || !flag3)
			{
				SendInfo.Add(gameSendDatum.UUID, gameSendDatum);
			}
		}
		if (Data.TrophyExchangeInfoList == null)
		{
			Data.TrophyExchangeInfoList = new List<TrophyExchangeInfo>();
		}
		TrophyExchangeDict.Clear();
		foreach (TrophyExchangeInfo trophyExchangeInfo in Data.TrophyExchangeInfoList)
		{
			TrophyExchangeDict.Add(trophyExchangeInfo.PrizeID, trophyExchangeInfo);
		}
		Player p = this;
		Data.OnDeserialized(ref p);
	}

	public void Serialize(BIJBinaryWriter data)
	{
		OnSerialize();
		SerializeSub(data);
	}

	public void SerializeSub(BIJBinaryWriter data)
	{
		Data.SerializeToBinary(data);
		mIsUpdateFirstStartup = false;
		if (!(Icon != null))
		{
			return;
		}
		try
		{
			using (BIJTempFileBinaryWriter bIJTempFileBinaryWriter = new BIJTempFileBinaryWriter(IconFileName))
			{
				bIJTempFileBinaryWriter.WriteAll(Icon.EncodeToPNG());
			}
		}
		catch (Exception)
		{
		}
	}

	public static Player Deserialize(BIJBinaryReader data)
	{
		Player player = DeserializeSub(data);
		player.OnDeserialized();
		if (player.mIsUpdateFirstStartup && player.Data.AppVersion < 1060)
		{
			player.CheckRoadBlockKeyHolding();
		}
		return player;
	}

	public static Player DeserializeSub(BIJBinaryReader data)
	{
		Player player = new Player();
		player.InitWork();
		player.Data.DeserializeFromBinary(data, 1290);
		if (player.Data.AppVersion != 1290)
		{
			player.mIsUpdateFirstStartup = true;
		}
		player.mIsNewPlayer = false;
		player.mLastLoginTime = player.Data.LastPlayTime;
		Texture2D texture2D = null;
		try
		{
			texture2D = new Texture2D(1, 1, TextureFormat.ARGB4444, false);
			bool flag = false;
			using (BIJTempFileBinaryReader bIJTempFileBinaryReader = new BIJTempFileBinaryReader(player.IconFileName))
			{
				flag = texture2D.LoadImage(bIJTempFileBinaryReader.ReadAll());
			}
			if (flag)
			{
				player.Icon = texture2D;
			}
			else
			{
				player.Icon = null;
				player.Data.IconModified = DateTimeUtil.UnitLocalTimeEpoch;
				if (texture2D != null)
				{
					texture2D = null;
				}
			}
		}
		catch (Exception)
		{
			player.Icon = null;
			player.Data.IconModified = DateTimeUtil.UnitLocalTimeEpoch;
			if (texture2D != null)
			{
				texture2D = null;
			}
		}
		return player;
	}

	public long GetRankingTotalScore()
	{
		return TotalHighScore;
	}

	public long GetRankingTotalStar()
	{
		return TotalStars;
	}

	public long GetRankingTotalTrophy()
	{
		return TotalTrophy;
	}

	public static string GetDefaultAppName()
	{
		return SingletonMonoBehaviour<Network>.Instance.AppName;
	}

	public object Clone()
	{
		return MemberwiseClone();
	}

	private void InitWork()
	{
		mGetSupport = true;
		mGetCampaign = true;
		mSumError = false;
		mIsNewPlayer = true;
		mIsExtendLifeUse = false;
		mLastLoginTime = DateTimeUtil.UnitLocalTimeEpoch;
		mModifiedGifts = new List<GiftItem>();
		mIsUpdateFirstStartup = false;
		Data = new PlayerData();
		StageClearData = new Dictionary<Def.SERIES, IDictionary<int, PlayerClearData>>();
		foreach (Def.SERIES key in Def.SeriesPrefix.Keys)
		{
			StageClearData.Add(key, new ThreadSafeDictionary<int, PlayerClearData>());
		}
		StageChallengeData = new Dictionary<Def.SERIES, IDictionary<int, PlayerStageData>>();
		foreach (Def.SERIES key2 in Def.SeriesPrefix.Keys)
		{
			StageChallengeData.Add(key2, new ThreadSafeDictionary<int, PlayerStageData>());
		}
		Friends = new ThreadSafeDictionary<int, MyFriend>();
		Gifts = new ThreadSafeDictionary<string, GiftItem>();
		ApplyFriends = new ThreadSafeDictionary<int, MyFriend>();
		PurchaseShop = new ThreadSafeDictionary<int, PlayerShopData>();
		SendInfo = new ThreadSafeDictionary<int, PlayerSendData>();
		TrophyExchangeDict = new ThreadSafeDictionary<int, TrophyExchangeInfo>();
	}

	public static Player CreateForNewPlayer()
	{
		Player player = new Player();
		player.Data.CreateNewData();
		player.SetPartnerNotify(0, 0);
		player.SetPartnerNotified(0, 0);
		player.Icon = null;
		player.mIsNewPlayer = true;
		return player;
	}

	public bool GetSumErrorFlag()
	{
		return Data.SumError;
	}

	public bool HasTimeCheatPenalty()
	{
		bool result = false;
		if (Data.TimeCheatPenalty > 0.0)
		{
			result = true;
		}
		return result;
	}

	public int GetServerLevel(Def.SERIES a_series)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data == null)
		{
			return Def.GetStageNo(1, 0);
		}
		if (data.NextRoadBlockLevel == Def.GetStageNo(999, 0))
		{
			int num = data.PlayableMaxLevel;
			if (num >= data.MaxLevel)
			{
				num = data.MaxLevel;
			}
			return num;
		}
		if (data.NextRoadBlockLevel < data.PlayableMaxLevel)
		{
			return data.NextRoadBlockLevel;
		}
		return data.PlayableMaxLevel;
	}

	public void SetCurrentLevel(Def.SERIES a_series, int a_stageNo)
	{
		if (Def.IsEventSeries(a_series) || Def.IsAdvSeries(a_series))
		{
			BIJLog.E("Event Series is not able to use this function!!");
			return;
		}
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			data.CurrentLevel = a_stageNo;
			Data.SetMapData(a_series, data);
		}
	}

	public void SetCurrentLevel(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageno)
	{
		if (!Def.IsEventSeries(a_series) || Def.IsAdvSeries(a_series))
		{
			BIJLog.E("Normal Series is not able to use this function!!");
			return;
		}
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			if (playerMapData != null)
			{
				playerMapData.CurrentLevel = a_stageno;
				data.CourseData[a_courseID] = playerMapData;
			}
			Data.SetMapData(a_eventID, data);
		}
	}

	public void SetOpenNoticeLevel(Def.SERIES a_series, int a_stageNo)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			data.OpenNoticeLevel = a_stageNo;
			Data.SetMapData(a_series, data);
		}
	}

	public void SetOpenNoticeLevel(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageno)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			if (playerMapData != null)
			{
				playerMapData.OpenNoticeLevel = a_stageno;
				data.CourseData[a_courseID] = playerMapData;
			}
			Data.SetMapData(a_eventID, data);
		}
	}

	public void SetPlayableMaxLevel(Def.SERIES a_series, int a_stageNo)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			data.PlayableMaxLevel = a_stageNo;
			Data.SetMapData(a_series, data);
		}
	}

	public void SetPlayableMaxLevel(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageno)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			if (playerMapData != null)
			{
				playerMapData.PlayableMaxLevel = a_stageno;
				data.CourseData[a_courseID] = playerMapData;
			}
			Data.SetMapData(a_eventID, data);
		}
	}

	public void SetRoadBlockReachLevel(Def.SERIES a_series, int a_stageNo)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			data.RoadBlockReachLevel = a_stageNo;
			Data.SetMapData(a_series, data);
		}
	}

	public void SetRoadBlockReachLevel(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageno)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			if (playerMapData != null)
			{
				playerMapData.RoadBlockReachLevel = a_stageno;
				data.CourseData[a_courseID] = playerMapData;
			}
			Data.SetMapData(a_eventID, data);
		}
	}

	public void SetNextRoadBlockLevel(Def.SERIES a_series, int a_stageNo)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			data.NextRoadBlockLevel = a_stageNo;
			Data.SetMapData(a_series, data);
		}
	}

	public void SetNextRoadBlockLevel(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageno)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			if (playerMapData != null)
			{
				playerMapData.NextRoadBlockLevel = a_stageno;
				data.CourseData[a_courseID] = playerMapData;
			}
			Data.SetMapData(a_eventID, data);
		}
	}

	public int GetItemCount(Def.ITEM_CATEGORY a_category, string a_key)
	{
		if (a_category == Def.ITEM_CATEGORY.TUTORIAL)
		{
			byte value;
			if (!Data.CompleteTutorials.TryGetValue(a_key, out value))
			{
				return 0;
			}
			return value;
		}
		return 0;
	}

	public int GetItemCount(Def.ITEM_CATEGORY a_category, int a_id)
	{
		int value2;
		switch (a_category)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			if (!Data.ConsumeItems.TryGetValue(a_id, out value2))
			{
				return 0;
			}
			break;
		case Def.ITEM_CATEGORY.BOOSTER:
			if (!Data.Boosters.TryGetValue((Def.BOOSTER_KIND)a_id, out value2))
			{
				return 0;
			}
			break;
		case Def.ITEM_CATEGORY.FREE_BOOSTER:
			if (!Data.FreeBoosters.TryGetValue((Def.BOOSTER_KIND)a_id, out value2))
			{
				return 0;
			}
			break;
		case Def.ITEM_CATEGORY.ACCESSORY:
		{
			byte value5;
			if (!Data.Accessories.TryGetValue(a_id, out value5))
			{
				return 0;
			}
			return value5;
		}
		case Def.ITEM_CATEGORY.COMPANION:
		{
			byte value6;
			if (!Data.Companions.TryGetValue(a_id, out value6))
			{
				return 0;
			}
			return value6;
		}
		case Def.ITEM_CATEGORY.EVENT:
		{
			byte value3;
			if (!Data.EventItems.TryGetValue(a_id, out value3))
			{
				return 0;
			}
			return value3;
		}
		case Def.ITEM_CATEGORY.WALLPAPER:
		{
			byte value7;
			if (!Data.WallPapers.TryGetValue(a_id, out value7))
			{
				return 0;
			}
			return value7;
		}
		case Def.ITEM_CATEGORY.WALLPAPER_PIECE:
			if (!Data.WallPaper_Pieces.TryGetValue(a_id, out value2))
			{
				return 0;
			}
			break;
		case Def.ITEM_CATEGORY.STORY_DEMO:
		{
			byte value4;
			if (!Data.CompleteStories.TryGetValue(a_id, out value4))
			{
				return 0;
			}
			return value4;
		}
		case Def.ITEM_CATEGORY.SKIN:
		{
			byte value;
			if (!Data.Skins.TryGetValue(a_id, out value))
			{
				return 0;
			}
			return value;
		}
		default:
			return 0;
		}
		return value2;
	}

	public void SetItemCount(Def.ITEM_CATEGORY a_category, string a_key, int a_value)
	{
		if (a_category == Def.ITEM_CATEGORY.TUTORIAL)
		{
			Data.CompleteTutorials[a_key] = ((a_value > 0) ? ((byte)1) : ((byte)0));
		}
	}

	public void SetItemCount(Def.ITEM_CATEGORY a_category, int a_id, int a_value)
	{
		switch (a_category)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			Data.ConsumeItems[a_id] = a_value;
			break;
		case Def.ITEM_CATEGORY.BOOSTER:
			Data.Boosters[(Def.BOOSTER_KIND)a_id] = a_value;
			break;
		case Def.ITEM_CATEGORY.FREE_BOOSTER:
			Data.FreeBoosters[(Def.BOOSTER_KIND)a_id] = a_value;
			break;
		case Def.ITEM_CATEGORY.ACCESSORY:
			Data.Accessories[a_id] = ((a_value > 0) ? ((byte)1) : ((byte)0));
			break;
		case Def.ITEM_CATEGORY.COMPANION:
			Data.Companions[a_id] = ((a_value > 0) ? ((byte)1) : ((byte)0));
			break;
		case Def.ITEM_CATEGORY.EVENT:
			Data.EventItems[a_id] = ((a_value > 0) ? ((byte)1) : ((byte)0));
			break;
		case Def.ITEM_CATEGORY.WALLPAPER:
			Data.WallPapers[a_id] = ((a_value > 0) ? ((byte)1) : ((byte)0));
			break;
		case Def.ITEM_CATEGORY.WALLPAPER_PIECE:
			Data.WallPaper_Pieces[a_id] = a_value;
			break;
		case Def.ITEM_CATEGORY.STORY_DEMO:
			Data.CompleteStories[a_id] = ((a_value > 0) ? ((byte)1) : ((byte)0));
			break;
		case Def.ITEM_CATEGORY.SKIN:
			Data.Skins[a_id] = ((a_value > 0) ? ((byte)1) : ((byte)0));
			break;
		}
	}

	public int AddItemCount(Def.ITEM_CATEGORY a_category, int a_id, int a_value)
	{
		int itemCount = GetItemCount(a_category, a_id);
		itemCount += a_value;
		SetItemCount(a_category, a_id, itemCount);
		return itemCount;
	}

	public int SubItemCount(Def.ITEM_CATEGORY a_category, int a_id, int a_value)
	{
		int itemCount = GetItemCount(a_category, a_id);
		itemCount -= a_value;
		SetItemCount(a_category, a_id, itemCount);
		return itemCount;
	}

	public void SetDollar(int num, bool puschaseDollar)
	{
		if (puschaseDollar)
		{
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 3, num);
		}
		else
		{
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 4, num);
		}
	}

	public void AddDollar(int num, bool puschaseDollar)
	{
		if (Dollar + num > Constants.HAVE_LIMIT_SD)
		{
			num = Constants.HAVE_LIMIT_SD - Dollar;
		}
		if (puschaseDollar)
		{
			AddItemCount(Def.ITEM_CATEGORY.CONSUME, 3, num);
		}
		else
		{
			AddItemCount(Def.ITEM_CATEGORY.CONSUME, 4, num);
		}
	}

	public bool SubDollar(int num)
	{
		if (Dollar < num)
		{
			return false;
		}
		int num2 = SubItemCount(Def.ITEM_CATEGORY.CONSUME, 4, num);
		if (num2 < 0)
		{
			AddItemCount(Def.ITEM_CATEGORY.CONSUME, 3, num2);
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 4, 0);
		}
		return true;
	}

	public void AddRoadBlockTicket(int num)
	{
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 7, num);
	}

	public void SubRoadBlockTicket(int num)
	{
		SubItemCount(Def.ITEM_CATEGORY.CONSUME, 7, num);
	}

	public int GetRoadBlockKey(Def.SERIES a_series)
	{
		int result = 0;
		Def.CONSUME_ID roadBlockConsumeID = GetRoadBlockConsumeID(a_series);
		if (roadBlockConsumeID == Def.CONSUME_ID.NONE)
		{
			return result;
		}
		return GetItemCount(Def.ITEM_CATEGORY.CONSUME, (int)roadBlockConsumeID);
	}

	public void SetRoadBlockKey(Def.SERIES a_series, int a_num)
	{
		Def.CONSUME_ID roadBlockConsumeID = GetRoadBlockConsumeID(a_series);
		if (roadBlockConsumeID != 0)
		{
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, (int)roadBlockConsumeID, a_num);
		}
	}

	private Def.CONSUME_ID GetRoadBlockConsumeID(Def.SERIES a_type)
	{
		Def.CONSUME_ID result = Def.CONSUME_ID.NONE;
		switch (a_type)
		{
		case Def.SERIES.SM_FIRST:
			result = Def.CONSUME_ID.ROADBLOCK_KEY;
			break;
		case Def.SERIES.SM_R:
			result = Def.CONSUME_ID.ROADBLOCK_KEY_R;
			break;
		case Def.SERIES.SM_S:
			result = Def.CONSUME_ID.ROADBLOCK_KEY_S;
			break;
		case Def.SERIES.SM_SS:
			result = Def.CONSUME_ID.ROADBLOCK_KEY_SS;
			break;
		case Def.SERIES.SM_STARS:
			result = Def.CONSUME_ID.ROADBLOCK_KEY_STARS;
			break;
		case Def.SERIES.SM_GF00:
			result = Def.CONSUME_ID.ROADBLOCK_KEY_GF00;
			break;
		}
		return result;
	}

	public void AddRoadBlockKey(int num, Def.SERIES a_type)
	{
		Def.CONSUME_ID roadBlockConsumeID = GetRoadBlockConsumeID(a_type);
		if (roadBlockConsumeID != 0)
		{
			AddItemCount(Def.ITEM_CATEGORY.CONSUME, (int)roadBlockConsumeID, num);
		}
	}

	public void SubRoadBlockKey(int num, Def.SERIES a_type)
	{
		Def.CONSUME_ID roadBlockConsumeID = GetRoadBlockConsumeID(a_type);
		if (roadBlockConsumeID != 0)
		{
			SubItemCount(Def.ITEM_CATEGORY.CONSUME, (int)roadBlockConsumeID, num);
		}
	}

	public void UpdateRoadBlockRequest(int a_uuid)
	{
		DateTime now = DateTime.Now;
		if (SendInfo.ContainsKey(a_uuid))
		{
			SendInfo[a_uuid].LastRoadBlockRequestTime = now;
			return;
		}
		PlayerSendData playerSendData = new PlayerSendData();
		playerSendData.UUID = a_uuid;
		playerSendData.LastRoadBlockRequestTime = now;
		SendInfo.Add(a_uuid, playerSendData);
	}

	public void CheckRoadBlockKeyHolding()
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		List<Def.SERIES> list = new List<Def.SERIES>(Def.SeriesMapRouteData.Keys);
		list.Sort();
		foreach (Def.SERIES item in list)
		{
			if (Def.IsEventSeries(item) || Def.IsAdvSeries(item))
			{
				continue;
			}
			PlayerMapData data;
			Data.GetMapData(item, out data);
			if (data == null)
			{
				continue;
			}
			int nextRoadBlockLevel = data.NextRoadBlockLevel;
			int a_main;
			int a_sub;
			Def.SplitStageNo(nextRoadBlockLevel, out a_main, out a_sub);
			int num = 0;
			List<AccessoryData> roadBlockAccessory = instance.GetRoadBlockAccessory(a_main, item);
			for (int i = 0; i < roadBlockAccessory.Count; i++)
			{
				if (IsAccessoryUnlock(roadBlockAccessory[i].Index))
				{
					num++;
				}
			}
			if (num > 0)
			{
				int roadBlockKey = GetRoadBlockKey(item);
				if (num > roadBlockKey)
				{
					AddRoadBlockKey(num - roadBlockKey, item);
				}
			}
		}
	}

	public void AddHeartPiece(int num)
	{
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 9, num);
	}

	public void SubHeartPiece(int num)
	{
		SubItemCount(Def.ITEM_CATEGORY.CONSUME, 9, num);
	}

	public void AddGrowupPiece(int num)
	{
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 11, num);
	}

	public void SubGrowupPiece(int num)
	{
		SubItemCount(Def.ITEM_CATEGORY.CONSUME, 11, num);
	}

	public void AddGrowup(int num)
	{
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 10, num);
	}

	public void SubGrowup(int num)
	{
		SubItemCount(Def.ITEM_CATEGORY.CONSUME, 10, num);
	}

	public void AddSPDailyPiece(int id, int num)
	{
		SPDailyAddItemCount(id, num);
	}

	public void SPDailyAddItemCount(int id, int num)
	{
		PlayerSPDailyData value;
		if (Data.SpecialDaily.TryGetValue(id, out value))
		{
			Data.SpecialDaily[id].Num += num;
			return;
		}
		value = new PlayerSPDailyData();
		value.SPDailyID = id;
		value.Num = num;
		Data.SpecialDaily.Add(id, value);
	}

	public int GetSPDailyCount(int id)
	{
		int num = 0;
		PlayerSPDailyData value;
		if (Data.SpecialDaily.TryGetValue(id, out value))
		{
			return value.Num;
		}
		return 0;
	}

	public int GetTotalGoodJob()
	{
		return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 13);
	}

	public int GetGoodJob()
	{
		return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 12);
	}

	public void AddGoodJob(int num)
	{
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 12, num);
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 13, num);
	}

	public void SubGoodJob(int num)
	{
		SubItemCount(Def.ITEM_CATEGORY.CONSUME, 12, num);
	}

	public int GetTotalTrophy()
	{
		return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 15);
	}

	public int GetTrophy()
	{
		return GetItemCount(Def.ITEM_CATEGORY.CONSUME, 14);
	}

	public void AddTrophy(int num)
	{
		mTotalTrophyCache = -1L;
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 14, num);
		AddItemCount(Def.ITEM_CATEGORY.CONSUME, 15, num);
	}

	public void SubTrophy(int num)
	{
		mTotalTrophyCache = -1L;
		SubItemCount(Def.ITEM_CATEGORY.CONSUME, 14, num);
	}

	public void AddLifeCount(int num)
	{
		int num2 = AddItemCount(Def.ITEM_CATEGORY.CONSUME, 1, num);
		if (num2 > Data.MaxNormalLifeCount)
		{
			num2 = AddItemCount(Def.ITEM_CATEGORY.CONSUME, 2, num2 - Data.MaxNormalLifeCount);
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 1, Data.MaxNormalLifeCount);
			if (num2 > Data.MaxExtendLifeCount)
			{
				SetItemCount(Def.ITEM_CATEGORY.CONSUME, 2, Data.MaxExtendLifeCount);
			}
		}
	}

	public bool SubLifeCount(int num)
	{
		if (Data.mMugenHeart >= Def.MUGEN_HEART_STATUS.START)
		{
			if (Data.mMugenHeart != Def.MUGEN_HEART_STATUS.TIME_OVER)
			{
				return false;
			}
			Data.mMugenHeart = Def.MUGEN_HEART_STATUS.TIME_OVER_PUZZLE;
		}
		bool result = true;
		int num2 = SubItemCount(Def.ITEM_CATEGORY.CONSUME, 2, num);
		if (num2 < 0)
		{
			if (GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) >= Data.MaxNormalLifeCount)
			{
				DateTime now = DateTime.Now;
				if (Data.OldestLifeUseTime.CompareTo(now) <= 0)
				{
					Data.OldestLifeUseTime = now;
				}
			}
			AddItemCount(Def.ITEM_CATEGORY.CONSUME, 1, GetItemCount(Def.ITEM_CATEGORY.CONSUME, 2));
			SetItemCount(Def.ITEM_CATEGORY.CONSUME, 2, 0);
			result = false;
			if (GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) < 0)
			{
				SetItemCount(Def.ITEM_CATEGORY.CONSUME, 1, 0);
			}
		}
		return result;
	}

	public void AddLifeCountAtPuzzleEnd()
	{
		if (mIsExtendLifeUse)
		{
			AddLifeCount(1);
		}
		else if (GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) < Data.MaxNormalLifeCount)
		{
			AddLifeCount(1);
		}
	}

	public void SubLifeCountAtPuzzleStart()
	{
		mIsExtendLifeUse = SubLifeCount(1);
	}

	public void UpdateLifeCount(float deltaTime)
	{
		if (Data.TimeCheatPenalty > 0.0)
		{
			SuppressTimeCheater();
		}
		if (GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) >= Data.MaxNormalLifeCount)
		{
			return;
		}
		long num;
		do
		{
			DateTime dateTime = Data.OldestLifeUseTime.AddSeconds(Constants.LIFE_RECOVER_SEC);
			num = DateTimeUtil.BetweenSeconds(DateTime.Now, dateTime);
			if (num <= 0)
			{
				if (GetItemCount(Def.ITEM_CATEGORY.CONSUME, 1) < Data.MaxNormalLifeCount)
				{
					AddLifeCount(1);
				}
				Data.OldestLifeUseTime = dateTime;
			}
		}
		while (num <= 0);
	}

	public void UpdateMugenHeartState()
	{
		if (Data.mMugenHeart == Def.MUGEN_HEART_STATUS.START)
		{
			if (SingletonMonoBehaviour<GameMain>.Instance.GetMugenHeartEndSec() <= 0)
			{
				Data.mMugenHeart = Def.MUGEN_HEART_STATUS.LAST_CHECK;
			}
		}
		else if (Data.mMugenHeart == Def.MUGEN_HEART_STATUS.LAST_CHECK_END)
		{
			if (SingletonMonoBehaviour<GameMain>.Instance.GetMugenHeartEndSec() <= 0)
			{
				Data.mMugenHeart = Def.MUGEN_HEART_STATUS.TIME_OVER;
			}
			else
			{
				Data.mMugenHeart = Def.MUGEN_HEART_STATUS.START;
			}
		}
	}

	public void SaveInfiniteHeartStartState()
	{
		PlayerPrefs.SetInt("InfiniteLifeDuration", Data.mUseMugenHeartSetTime);
		PlayerPrefs.SetInt("InfiniteLifeReaminHeart", LifeCount);
		PlayerPrefs.SetInt("InfiniteLifePrevWinCount", (int)Data.TotalWin);
		PlayerPrefs.SetInt("InfiniteLifePrevLoseCount", (int)Data.TotalLose);
		PlayerPrefs.Save();
	}

	public void SendCramInfiniteHeartEnd()
	{
		if (PlayerPrefs.HasKey("InfiniteLifeDuration") && PlayerPrefs.HasKey("InfiniteLifeReaminHeart") && PlayerPrefs.HasKey("InfiniteLifePrevWinCount") && PlayerPrefs.HasKey("InfiniteLifePrevLoseCount"))
		{
			int num = (int)(Data.TotalWin - PlayerPrefs.GetInt("InfiniteLifePrevWinCount"));
			int num2 = (int)(Data.TotalLose - PlayerPrefs.GetInt("InfiniteLifePrevLoseCount"));
			int playcount = num + num2;
			int @int = PlayerPrefs.GetInt("InfiniteLifeDuration");
			int lastPlaySeries = (int)Data.LastPlaySeries;
			int lastPlayLevel = Data.LastPlayLevel;
			int int2 = PlayerPrefs.GetInt("InfiniteLifeReaminHeart");
			ServerCram.InfiniteHeartEnd(playcount, num, num2, @int, lastPlaySeries, lastPlayLevel, int2);
		}
		PlayerPrefs.DeleteKey("InfiniteLifeDuration");
		PlayerPrefs.DeleteKey("InfiniteLifeReaminHeart");
		PlayerPrefs.DeleteKey("InfiniteLifePrevWinCount");
		PlayerPrefs.DeleteKey("InfiniteLifePrevLoseCount");
		PlayerPrefs.Save();
	}

	public bool CheckTimeCheatNoPenalty()
	{
		DateTime now = DateTime.Now;
		double totalSeconds = (Data.LastPlayTime - now).TotalSeconds;
		if (totalSeconds < Constants.LOCAL_TIMECHEAT_MARGIN)
		{
			return false;
		}
		return true;
	}

	public bool CheckTimeCheatPenalty()
	{
		DateTime now = DateTime.Now;
		double totalSeconds = (Data.LastPlayTime - now).TotalSeconds;
		if (totalSeconds < Constants.LOCAL_TIMECHEAT_MARGIN)
		{
			return false;
		}
		double val = Math.Min(totalSeconds, Constants.LOCAL_TIMECHEAT_PENALTY);
		Data.OldTimeCheatPenalty = Data.TimeCheatPenalty;
		Data.TimeCheatPenalty = Math.Max(Data.TimeCheatPenalty, val);
		Data.LastLocalTimeCheatCheckTime = now;
		Data.OldestLifeUseTime = now.AddSeconds(Data.TimeCheatPenalty - GameMain.mServerTimeDiff);
		return true;
	}

	public void SuppressTimeCheater()
	{
		DateTime now = DateTime.Now;
		double totalSeconds = (now - Data.LastLocalTimeCheatCheckTime).TotalSeconds;
		if (Data.TimeCheatPenalty > 0.0 && totalSeconds > 0.0)
		{
			Data.OldTimeCheatPenalty = Data.TimeCheatPenalty;
			Data.TimeCheatPenalty -= totalSeconds;
			if (Data.TimeCheatPenalty < 0.0)
			{
				Data.TimeCheatPenalty = 0.0;
			}
			Data.LastLocalTimeCheatCheckTime = now;
		}
	}

	public void ResetTimeCheaterPenalty(int minutes)
	{
		if (HasTimeCheatPenalty())
		{
			Data.OldTimeCheatPenalty = Data.TimeCheatPenalty;
			Data.TimeCheatPenalty = 0.0;
			Data.LastLocalTimeCheatCheckTime = DateTime.Now;
		}
		DateTime now = DateTime.Now;
		if (minutes == 1)
		{
			minutes = 30;
		}
		now = now.AddSeconds(-Constants.LIFE_RECOVER_SEC).AddMinutes(minutes);
		Data.OldestLifeUseTime = now;
	}

	public int GetBoosterNum(Def.BOOSTER_KIND kind)
	{
		return GetPaidBoosterNum(kind) + GetFreeBoosterNum(kind);
	}

	public bool SubBoosterNum(Def.BOOSTER_KIND kind, int subNum, out Def.ITEMGET_TYPE a_type)
	{
		a_type = Def.ITEMGET_TYPE.NONE;
		int boosterNum = GetBoosterNum(kind);
		if (boosterNum < subNum)
		{
			return false;
		}
		a_type = Def.ITEMGET_TYPE.FREE;
		int num = SubFreeBoosterNum(kind, subNum);
		if (num < 0)
		{
			a_type = Def.ITEMGET_TYPE.PAID;
			AddPaidBoosterNum(kind, num);
			SetFreeBoosterNum(kind, 0);
		}
		return true;
	}

	public int GetPaidBoosterNum(Def.BOOSTER_KIND kind)
	{
		return GetItemCount(Def.ITEM_CATEGORY.BOOSTER, (int)kind);
	}

	public void SetPaidBoosterNum(Def.BOOSTER_KIND kind, int num)
	{
		SetItemCount(Def.ITEM_CATEGORY.BOOSTER, (int)kind, num);
	}

	public void AddPaidBoosterNum(Def.BOOSTER_KIND kind, int addNum)
	{
		int paidBoosterNum = GetPaidBoosterNum(kind);
		paidBoosterNum += addNum;
		SetPaidBoosterNum(kind, paidBoosterNum);
	}

	public int SubPaidBoosterNum(Def.BOOSTER_KIND kind, int subNum)
	{
		int paidBoosterNum = GetPaidBoosterNum(kind);
		paidBoosterNum -= subNum;
		int result = paidBoosterNum;
		if (paidBoosterNum < 0)
		{
			paidBoosterNum = 0;
		}
		SetPaidBoosterNum(kind, paidBoosterNum);
		return result;
	}

	public int GetFreeBoosterNum(Def.BOOSTER_KIND kind)
	{
		return GetItemCount(Def.ITEM_CATEGORY.FREE_BOOSTER, (int)kind);
	}

	public void SetFreeBoosterNum(Def.BOOSTER_KIND kind, int num)
	{
		SetItemCount(Def.ITEM_CATEGORY.FREE_BOOSTER, (int)kind, num);
	}

	public void AddFreeBoosterNum(Def.BOOSTER_KIND kind, int addNum)
	{
		int freeBoosterNum = GetFreeBoosterNum(kind);
		freeBoosterNum += addNum;
		SetFreeBoosterNum(kind, freeBoosterNum);
	}

	public int SubFreeBoosterNum(Def.BOOSTER_KIND kind, int subNum)
	{
		int freeBoosterNum = GetFreeBoosterNum(kind);
		freeBoosterNum -= subNum;
		int result = freeBoosterNum;
		if (freeBoosterNum < 0)
		{
			freeBoosterNum = 0;
		}
		SetFreeBoosterNum(kind, freeBoosterNum);
		return result;
	}

	public bool IsAllCompanionLevelMAX()
	{
		bool result = true;
		foreach (KeyValuePair<int, byte> companion in Data.Companions)
		{
			if (companion.Value > 0)
			{
				int companionLevel = GetCompanionLevel(companion.Key);
				if (companionLevel < Def.CompanionXPTable.Length - 1)
				{
					result = false;
					break;
				}
			}
		}
		return result;
	}

	public bool IsCompanionUnlock(int index)
	{
		if (GetItemCount(Def.ITEM_CATEGORY.COMPANION, index) > 0)
		{
			return true;
		}
		return false;
	}

	public void ResetCompanion(int index)
	{
		if (IsCompanionUnlock(index))
		{
			SetItemCount(Def.ITEM_CATEGORY.COMPANION, index, 0);
		}
	}

	public void UnlockCompanion(int index)
	{
		SetItemCount(Def.ITEM_CATEGORY.COMPANION, index, 1);
		SetPartnerNotify(index, 0);
	}

	public void SetUseCompanionCount(int a_index, int a_count)
	{
		int value;
		if (Data.UseCompanionCount.TryGetValue(a_index, out value))
		{
			Data.UseCompanionCount[a_index] = value;
		}
		else
		{
			Data.UseCompanionCount[a_index] = 0;
		}
	}

	public void AddUseCompanionCount(int a_index)
	{
		int value;
		if (Data.UseCompanionCount.TryGetValue(a_index, out value))
		{
			Data.UseCompanionCount[a_index] = value + 1;
		}
		else
		{
			Data.UseCompanionCount[a_index] = 1;
		}
	}

	public int GetCompanionLevel(int a_companion, bool a_maxLevelCorrect = true)
	{
		int num = 1;
		int value;
		if (!Data.CompanionXPs.TryGetValue(a_companion, out value))
		{
			value = 0;
		}
		for (int i = 1; i < Def.CompanionXPTable.Length; i++)
		{
			int num2 = Def.CompanionXPTable[i];
			if (value < num2)
			{
				break;
			}
			num++;
		}
		if (a_maxLevelCorrect && num > Def.CompanionXPTable.Length - 1)
		{
			num = Def.CompanionXPTable.Length - 1;
		}
		return num;
	}

	public void SetCompanionLevel(int a_companion, int a_level)
	{
		Data.CompanionXPs[a_companion] = Def.CompanionXPTable[a_level - 1];
	}

	public void SetCompanionLevelTemp(int a_companion, int a_level)
	{
		mCompanionXPTemp.Clear();
		mCompanionXPTemp.Add(a_companion, Def.CompanionXPTable[a_level]);
	}

	public int GetCompanionLevelTemp(int key)
	{
		int value = 1;
		mCompanionXPTemp.TryGetValue(key, out value);
		if (value >= 100)
		{
			value /= 100;
		}
		return value;
	}

	public void SetCompanionLevelTempClear()
	{
		mCompanionXPTemp.Clear();
	}

	public bool SetCompanionLevelTempData()
	{
		if (mCompanionXPTemp.Count == 0)
		{
			return false;
		}
		return true;
	}

	public int AddCompanionLevel(int a_companion, int a_add = 1)
	{
		int result = 1;
		for (int i = 0; i < a_add; i++)
		{
			int num = 0;
			int value;
			if (!Data.CompanionXPs.TryGetValue(a_companion, out value))
			{
				value = 0;
			}
			int companionLevel = GetCompanionLevel(a_companion);
			if (companionLevel == Def.CompanionXPTable.Length - 1)
			{
				result = companionLevel;
				break;
			}
			int num2 = Def.CompanionXPTable[companionLevel];
			num = num2 - value;
			result = AddCompanionXP(a_companion, num);
		}
		return result;
	}

	public int AddCompanionXP(int a_companion, int a_xp, bool a_round = true)
	{
		int companionLevel = GetCompanionLevel(a_companion);
		int value;
		if (!Data.CompanionXPs.TryGetValue(a_companion, out value))
		{
			value = 0;
			Data.CompanionXPs[a_companion] = 0;
		}
		value += a_xp;
		for (int i = companionLevel; i < Def.CompanionXPTable.Length; i++)
		{
			int num = Def.CompanionXPTable[i];
			if (value >= num)
			{
				SetPartnerNotify(a_companion, i - 1);
				if (a_round)
				{
					value = num;
					break;
				}
			}
		}
		if (value >= Def.CompanionXPTable[Def.CompanionXPTable.Length - 1])
		{
			value = Def.CompanionXPTable[Def.CompanionXPTable.Length - 1];
		}
		Data.CompanionXPs[a_companion] = value;
		return GetCompanionLevel(a_companion);
	}

	public void ClearCompanionXP(int a_companion)
	{
		int value;
		if (Data.CompanionXPs.TryGetValue(a_companion, out value))
		{
			Data.CompanionXPs[a_companion] = 0;
		}
	}

	public void RemoveCompanion(int a_index)
	{
		SetItemCount(Def.ITEM_CATEGORY.COMPANION, a_index, 0);
		ClearCompanionXP(a_index);
	}

	public bool IsSkinUnlock(int index)
	{
		if (GetItemCount(Def.ITEM_CATEGORY.SKIN, index) > 0)
		{
			return true;
		}
		return false;
	}

	public void UnlockSkin(int index)
	{
		SetItemCount(Def.ITEM_CATEGORY.SKIN, index, 1);
	}

	public bool IsAccessoryUnlock(int index)
	{
		if (GetItemCount(Def.ITEM_CATEGORY.ACCESSORY, index) > 0)
		{
			return true;
		}
		return false;
	}

	public void UnlockAccessory(int index)
	{
		SetItemCount(Def.ITEM_CATEGORY.ACCESSORY, index, 1);
	}

	public void ResetAccessory(int index)
	{
		SetItemCount(Def.ITEM_CATEGORY.ACCESSORY, index, 0);
	}

	public void ResetAccessory(AccessoryData data)
	{
		SetItemCount(Def.ITEM_CATEGORY.ACCESSORY, data.Index, 0);
		switch (data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.PARTNER:
			ResetCompanion(data.GetCompanionID());
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
			ResetWallPaper(data.GetWallPaperIndex());
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE:
			ResetWallPaperPiece(data.GetWallPaperIndex(), data.GetWallPaperPieceIndex());
			break;
		}
	}

	public bool AddAccessoryFromGift(AccessoryData a_data)
	{
		return AddAccessory(a_data, true, true);
	}

	private bool CheckEnableReceive(AccessoryData a_data)
	{
		if (a_data == null || a_data.AccessoryType == AccessoryData.ACCESSORY_TYPE.NONE)
		{
			return false;
		}
		if (!a_data.IsInfinityReceive && IsAccessoryUnlock(a_data.Index))
		{
			if (a_data.IsOnceReceive)
			{
				return false;
			}
			if (a_data.IsMultiReceive)
			{
				return false;
			}
		}
		return true;
	}

	public bool AddAccessory(AccessoryData a_data, bool a_add = true, bool a_directadd = false, int a_getType = 7, GiftMailType custom_mail_type = GiftMailType.GIFT)
	{
		int a_num = 0;
		int num = 0;
		if (a_add && !CheckEnableReceive(a_data))
		{
			return false;
		}
		bool flag = a_add;
		bool flag2 = true;
		int num2 = 0;
		int itemKind = 0;
		int num3 = 0;
		switch (a_data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.PARTNER:
		{
			int gotIDByNum = a_data.GetGotIDByNum();
			num = gotIDByNum;
			a_num = 1;
			if (!IsCompanionUnlock(a_data.GetGotIDByNum()))
			{
				if (!a_add)
				{
					break;
				}
				GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
				instance.SetPartnerCollectionAttention(a_data);
				UnlockCompanion(gotIDByNum);
				instance.SucceedBaseCompanionLevel(gotIDByNum);
				if (!a_directadd)
				{
					break;
				}
				int character_ID = gotIDByNum;
				int lastPlaySeries3 = (int)instance.mPlayer.Data.LastPlaySeries;
				int lastPlayLevel3 = instance.mPlayer.Data.LastPlayLevel;
				int lastPlaySeries4 = (int)instance.mPlayer.AdvSaveData.LastPlaySeries;
				int lastPlayLevel4 = instance.mPlayer.AdvSaveData.LastPlayLevel;
				int origin_lvl = 0;
				int companionLevel = instance.mPlayer.GetCompanionLevel(gotIDByNum);
				int place = 99;
				string text = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
				string text2 = string.Empty;
				for (int i = 0; i < instance.mCompanionData.Count; i++)
				{
					byte b = 0;
					if (instance.mPlayer.IsCompanionUnlock(i))
					{
						b = (byte)instance.mPlayer.GetCompanionLevel(i);
					}
					if (b > 9 && b > 35)
					{
						b = 0;
					}
					text2 += text.Substring(b, 1);
				}
				ServerCram.CharacterLevelUP(character_ID, lastPlaySeries3, lastPlayLevel3, lastPlaySeries4, lastPlayLevel4, origin_lvl, companionLevel, text2, place);
			}
			else
			{
				flag2 = false;
			}
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
			a_num = a_data.GetGotIDByNum();
			if (a_add)
			{
				if (a_directadd)
				{
					AddGrowup(a_num);
					int lastPlaySeries11 = (int)Data.LastPlaySeries;
					int lastPlayLevel11 = Data.LastPlayLevel;
					int lastPlaySeries12 = (int)AdvSaveData.LastPlaySeries;
					int lastPlayLevel12 = AdvSaveData.LastPlayLevel;
					int advLastStage5 = AdvLastStage;
					int advMaxStamina5 = AdvMaxStamina;
					ServerCram.ArcadeGetItem(1, lastPlaySeries11, lastPlayLevel11, lastPlaySeries12, lastPlayLevel12, 1, 10, 1, a_getType, advLastStage5, advMaxStamina5);
				}
				else
				{
					num2 = 1;
					itemKind = 10;
					num3 = a_num;
				}
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_F:
			a_num = 1;
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			a_num = a_data.GetGotIDByNum();
			if (a_add)
			{
				if (a_directadd)
				{
					AddGrowupPiece(a_num);
					int lastPlaySeries7 = (int)Data.LastPlaySeries;
					int lastPlayLevel7 = Data.LastPlayLevel;
					int lastPlaySeries8 = (int)AdvSaveData.LastPlaySeries;
					int lastPlayLevel8 = AdvSaveData.LastPlayLevel;
					int advLastStage3 = AdvLastStage;
					int advMaxStamina3 = AdvMaxStamina;
					ServerCram.ArcadeGetItem(a_num, lastPlaySeries7, lastPlayLevel7, lastPlaySeries8, lastPlayLevel8, 1, 11, 1, a_getType, advLastStage3, advMaxStamina3);
				}
				else
				{
					num2 = 1;
					itemKind = 11;
					num3 = a_num;
				}
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		{
			Def.BOOSTER_KIND a_kind = Def.BOOSTER_KIND.NONE;
			a_data.GetBooster(out a_kind, out a_num);
			num = (int)a_kind;
			if (a_add)
			{
				if (a_directadd)
				{
					AddFreeBoosterNum(a_kind, a_num);
					break;
				}
				num2 = 3;
				itemKind = (int)a_kind;
				num3 = a_num;
			}
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.HEART:
			a_num = a_data.GetGotIDByNum();
			if (a_add)
			{
				if (a_directadd)
				{
					AddLifeCount(a_num);
					int lastPlaySeries9 = (int)Data.LastPlaySeries;
					int lastPlayLevel9 = Data.LastPlayLevel;
					int lastPlaySeries10 = (int)AdvSaveData.LastPlaySeries;
					int lastPlayLevel10 = AdvSaveData.LastPlayLevel;
					int advLastStage4 = AdvLastStage;
					int advMaxStamina4 = AdvMaxStamina;
					ServerCram.ArcadeGetItem(a_num, lastPlaySeries9, lastPlayLevel9, lastPlaySeries10, lastPlayLevel10, 1, 2, 1, a_getType, advLastStage4, advMaxStamina4);
				}
				else
				{
					num2 = 1;
					itemKind = 2;
					num3 = a_num;
				}
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			a_num = a_data.GetGotIDByNum();
			if (a_add)
			{
				if (a_directadd)
				{
					AddHeartPiece(a_num);
					int lastPlaySeries5 = (int)Data.LastPlaySeries;
					int lastPlayLevel5 = Data.LastPlayLevel;
					int lastPlaySeries6 = (int)AdvSaveData.LastPlaySeries;
					int lastPlayLevel6 = AdvSaveData.LastPlayLevel;
					int advLastStage2 = AdvLastStage;
					int advMaxStamina2 = AdvMaxStamina;
					ServerCram.ArcadeGetItem(a_num, lastPlaySeries5, lastPlayLevel5, lastPlaySeries6, lastPlayLevel6, 1, 9, 1, a_getType, advLastStage2, advMaxStamina2);
				}
				else
				{
					num2 = 1;
					itemKind = 9;
					num3 = a_num;
				}
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.SP_DAILY:
		{
			int dailyID = a_data.DailyID;
			a_num = a_data.GetGotIDByNum();
			if (a_add)
			{
				AddSPDailyPiece(dailyID, a_num);
			}
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			a_num = a_data.GetGotIDByNum();
			if (a_add)
			{
				if (a_directadd)
				{
					AddTrophy(a_num);
					break;
				}
				num2 = 1;
				itemKind = 14;
				num3 = a_num;
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY:
			a_num = 1;
			if (a_add)
			{
				AddRoadBlockKey(a_num, Def.SERIES.SM_FIRST);
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_R:
			a_num = 1;
			if (a_add)
			{
				AddRoadBlockKey(a_num, Def.SERIES.SM_R);
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_S:
			a_num = 1;
			if (a_add)
			{
				AddRoadBlockKey(a_num, Def.SERIES.SM_S);
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_SS:
			a_num = 1;
			if (a_add)
			{
				AddRoadBlockKey(a_num, Def.SERIES.SM_SS);
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_STARS:
			a_num = 1;
			if (a_add)
			{
				AddRoadBlockKey(a_num, Def.SERIES.SM_STARS);
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_GF00:
			a_num = 1;
			if (a_add)
			{
				AddRoadBlockKey(a_num, Def.SERIES.SM_GF00);
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
		{
			int wallPaperIndex = a_data.GetWallPaperIndex();
			if (!IsWallPaperUnlock(wallPaperIndex))
			{
				a_num = 1;
				if (a_add)
				{
					SetCollectionNotify(a_data.Index);
					AddWallPaper(wallPaperIndex);
					List<AccessoryData> wallPaperPieces = SingletonMonoBehaviour<GameMain>.Instance.GetWallPaperPieces(a_data.GetWallPaperIndex());
					for (int j = 0; j < wallPaperPieces.Count; j++)
					{
						UnlockAccessory(wallPaperPieces[j].Index);
					}
					Data.UnlockAccessoriesNoticeCount = Data.Accessories.Count;
				}
			}
			else
			{
				flag2 = false;
			}
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE:
			if (!HasWallPaperPiece(a_data.GetWallPaperIndex(), a_data.GetWallPaperPieceIndex()))
			{
				a_num = 1;
				if (a_add)
				{
					SetCollectionNotify(a_data.Index);
					AddWallPaperPiece(a_data.GetWallPaperIndex(), a_data.GetWallPaperPieceIndex());
				}
			}
			else
			{
				flag2 = false;
			}
			break;
		case AccessoryData.ACCESSORY_TYPE.ADV_ITEM:
			AddAdvAccessoryConnect(a_data.Index);
			break;
		case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
			a_num = a_data.GetGotIDByNum();
			if (a_add)
			{
				if (a_directadd)
				{
					AddItemCount(Def.ITEM_CATEGORY.CONSUME, 20, a_num);
					int lastPlaySeries = (int)Data.LastPlaySeries;
					int lastPlayLevel = Data.LastPlayLevel;
					int lastPlaySeries2 = (int)AdvSaveData.LastPlaySeries;
					int lastPlayLevel2 = AdvSaveData.LastPlayLevel;
					int advLastStage = AdvLastStage;
					int advMaxStamina = AdvMaxStamina;
					ServerCram.ArcadeGetItem(a_num, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 20, 1, a_getType, advLastStage, advMaxStamina);
				}
				else
				{
					num2 = 1;
					itemKind = 20;
					num3 = a_num;
				}
			}
			break;
		default:
			BIJLog.E("No Implemented in AddAccessory : " + a_data.AccessoryType);
			flag2 = false;
			break;
		case AccessoryData.ACCESSORY_TYPE.GEM:
		case AccessoryData.ACCESSORY_TYPE.ERB_KEY:
		case AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY:
			break;
		}
		if (num2 > 0 && num3 > 0)
		{
			GiftMailType aType = GiftMailType.LOCAL_MAP;
			if (Def.IsEventSeries(Data.CurrentSeries))
			{
				aType = GiftMailType.LOCAL_EVENT;
			}
			else if (Def.IsAdvSeries(Data.CurrentSeries))
			{
				aType = GiftMailType.LOCAL_ADV;
			}
			if (custom_mail_type != 0)
			{
				aType = custom_mail_type;
			}
			GiftItem giftItem = new GiftItem(aType);
			giftItem.Data.ItemCategory = num2;
			giftItem.Data.ItemKind = itemKind;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = num3;
			AddGift(giftItem);
		}
		if (a_add && flag2)
		{
			AccessoryData.ACCESSORY_TYPE accessoryType = a_data.AccessoryType;
			if (accessoryType != AccessoryData.ACCESSORY_TYPE.GEM && accessoryType != AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
			{
				int index = a_data.Index;
				int cur_series = (int)Data.CurrentSeries;
				if (SingletonMonoBehaviour<GameMain>.Instance.IsDailyChallengePuzzle)
				{
					cur_series = 200;
				}
				ServerCram.GetAccessory(index, cur_series, a_getType);
			}
		}
		if (flag)
		{
			UnlockAccessory(a_data.Index);
		}
		return flag2;
	}

	public bool AddAccessory_AllInGift(AccessoryData a_data, int a_EventID = 0, int a_getType = 15)
	{
		int a_num = 0;
		if (!CheckEnableReceive(a_data))
		{
			return false;
		}
		bool flag = true;
		bool flag2 = true;
		int num = 0;
		int itemKind = 0;
		int num2 = 0;
		switch (a_data.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
			a_num = a_data.GetGotIDByNum();
			num = 1;
			itemKind = 10;
			num2 = a_num;
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			a_num = a_data.GetGotIDByNum();
			num = 1;
			itemKind = 11;
			num2 = a_num;
			break;
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		{
			Def.BOOSTER_KIND a_kind = Def.BOOSTER_KIND.NONE;
			a_data.GetBooster(out a_kind, out a_num);
			num = 3;
			itemKind = (int)a_kind;
			num2 = a_num;
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.HEART:
			a_num = a_data.GetGotIDByNum();
			num = 1;
			itemKind = 2;
			num2 = a_num;
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			a_num = a_data.GetGotIDByNum();
			num = 1;
			itemKind = 9;
			num2 = a_num;
			break;
		case AccessoryData.ACCESSORY_TYPE.PARTNER:
			flag = false;
			break;
		case AccessoryData.ACCESSORY_TYPE.BINGO_TICKET:
			a_num = a_data.GetGotIDByNum();
			num = 32;
			itemKind = a_EventID;
			num2 = a_num;
			break;
		case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
			a_num = a_data.GetGotIDByNum();
			num = 1;
			itemKind = 20;
			num2 = a_num;
			break;
		default:
			BIJLog.E("No Implemented in AddAccessory : " + a_data.AccessoryType);
			flag = false;
			break;
		case AccessoryData.ACCESSORY_TYPE.GEM:
		case AccessoryData.ACCESSORY_TYPE.ADV_ITEM:
		case AccessoryData.ACCESSORY_TYPE.BINGO_KEY:
		case AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY:
			break;
		}
		if (flag)
		{
			if (num > 0 && num2 > 0)
			{
				GiftMailType aType = GiftMailType.LOCAL_MAP;
				if (Def.IsEventSeries(Data.CurrentSeries))
				{
					aType = GiftMailType.LOCAL_EVENT;
				}
				else if (Def.IsAdvSeries(Data.CurrentSeries))
				{
					aType = GiftMailType.LOCAL_ADV;
				}
				GiftItem giftItem = new GiftItem(aType);
				giftItem.Data.ItemCategory = num;
				giftItem.Data.ItemKind = itemKind;
				giftItem.Data.ItemID = 1;
				giftItem.Data.Quantity = num2;
				AddGift(giftItem);
			}
			if (flag2)
			{
				int index = a_data.Index;
				int cur_series = (int)Data.CurrentSeries;
				if (SingletonMonoBehaviour<GameMain>.Instance.IsDailyChallengePuzzle)
				{
					cur_series = 200;
				}
				ServerCram.GetAccessory(index, cur_series, a_getType);
			}
			UnlockAccessory(a_data.Index);
		}
		return flag;
	}

	public bool AddAccessoryConnect(int a_accessoryID)
	{
		if (Data.AccessoryConnect.Contains(a_accessoryID))
		{
			return false;
		}
		Data.AccessoryConnect.Add(a_accessoryID);
		return true;
	}

	public bool SubAccessoryConnect(AccessoryData a_accessory)
	{
		if (a_accessory == null)
		{
			return true;
		}
		int index = a_accessory.Index;
		if (a_accessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
		{
			UnlockAccessory(index);
		}
		Data.AccessoryConnect.Remove(index);
		return Data.AccessoryConnect.Count == 0;
	}

	public bool AddAdvAccessoryConnect(int a_accessoryID)
	{
		if (Data.AdvAccessoryConnect.Contains(a_accessoryID))
		{
			return false;
		}
		Data.AdvAccessoryConnect.Add(a_accessoryID);
		return true;
	}

	public bool SubAdvAccessoryConnect(AccessoryData a_accessory)
	{
		if (a_accessory == null)
		{
			return true;
		}
		int index = a_accessory.Index;
		if (a_accessory.AccessoryType == AccessoryData.ACCESSORY_TYPE.ADV_ITEM)
		{
			UnlockAccessory(index);
		}
		Data.AdvAccessoryConnect.Remove(index);
		return Data.AdvAccessoryConnect.Count == 0;
	}

	public void AddWallPaper(int a_id)
	{
		AddItemCount(Def.ITEM_CATEGORY.WALLPAPER, a_id, 1);
	}

	public bool IsWallPaperUnlock(int a_id)
	{
		return GetItemCount(Def.ITEM_CATEGORY.WALLPAPER, a_id) > 0;
	}

	public void ResetWallPaper(int a_id)
	{
		if (IsWallPaperUnlock(a_id))
		{
			SetItemCount(Def.ITEM_CATEGORY.WALLPAPER, a_id, 0);
			if (Data.WallPaper_Pieces.ContainsKey(a_id))
			{
				Data.WallPaper_Pieces[a_id] = 0;
			}
		}
	}

	public void AddWallPaperPiece(int a_id, int a_piece)
	{
		int num = 1 << a_piece;
		if (Data.WallPaper_Pieces.ContainsKey(a_id))
		{
			Dictionary<int, int> wallPaper_Pieces;
			Dictionary<int, int> dictionary = (wallPaper_Pieces = Data.WallPaper_Pieces);
			int key;
			int key2 = (key = a_id);
			key = wallPaper_Pieces[key];
			dictionary[key2] = key | num;
		}
		else
		{
			Data.WallPaper_Pieces.Add(a_id, num);
		}
	}

	public bool HasWallPaperPiece(int a_id, int a_piece)
	{
		int num = 1 << a_piece;
		if (Data.WallPaper_Pieces.ContainsKey(a_id))
		{
			int num2 = Data.WallPaper_Pieces[a_id];
			return (num2 & num) == 1;
		}
		return false;
	}

	public bool IsCompleteWallPaper(int a_id)
	{
		if (Data.WallPaper_Pieces.ContainsKey(a_id))
		{
			int num = Data.WallPaper_Pieces[a_id];
			return num == 511;
		}
		return false;
	}

	public void ResetWallPaperPiece(int a_id, int a_piece)
	{
		int num = 1 << a_piece;
		if (Data.WallPaper_Pieces.ContainsKey(a_id))
		{
			Dictionary<int, int> wallPaper_Pieces;
			Dictionary<int, int> dictionary = (wallPaper_Pieces = Data.WallPaper_Pieces);
			int key;
			int key2 = (key = a_id);
			key = wallPaper_Pieces[key];
			dictionary[key2] = key - num;
		}
	}

	public bool IsTutorialCompleted(string index)
	{
		if (GetItemCount(Def.ITEM_CATEGORY.TUTORIAL, index) > 0)
		{
			return true;
		}
		return false;
	}

	public bool TutorialComplete(string index)
	{
		if (IsTutorialCompleted(index))
		{
			return false;
		}
		SetItemCount(Def.ITEM_CATEGORY.TUTORIAL, index, 1);
		return true;
	}

	public void SetTutorialSkip(bool aSkip)
	{
		Data.TutorialsSkipFlg = aSkip;
	}

	public bool IsTutorialSkipped()
	{
		return Data.TutorialsSkipFlg;
	}

	public void ResetTutorial(string index)
	{
		SetItemCount(Def.ITEM_CATEGORY.TUTORIAL, index, 0);
	}

	public bool IsStoryCompleted(int index)
	{
		if (GetItemCount(Def.ITEM_CATEGORY.STORY_DEMO, index) > 0)
		{
			return true;
		}
		return false;
	}

	public void StoryComplete(int index)
	{
		SetItemCount(Def.ITEM_CATEGORY.STORY_DEMO, index, 1);
	}

	public void StoryLock(int index)
	{
		SetItemCount(Def.ITEM_CATEGORY.STORY_DEMO, index, 0);
	}

	public bool IsStoryCompletedInEvent(int a_eventID, int a_index)
	{
		PlayerEventData data;
		Data.GetMapData(Def.SERIES.SM_EV, a_eventID, out data);
		if (data != null)
		{
			return data.HasShownDemo((short)a_index);
		}
		return false;
	}

	public void StoryComplete(int a_eventID, int a_index)
	{
		PlayerEventData data;
		Data.GetMapData(Def.SERIES.SM_EV, a_eventID, out data);
		if (data != null)
		{
			data.ShowDemo((short)a_index);
		}
		StoryComplete(a_index);
	}

	public bool GetStageClearData(Def.SERIES a_series, int stageNo, out PlayerClearData _psd)
	{
		_psd = null;
		IDictionary<int, PlayerClearData> value;
		bool flag = StageClearData.TryGetValue(a_series, out value);
		if (flag)
		{
			flag = value.TryGetValue(stageNo, out _psd);
		}
		return flag;
	}

	public bool GetStageClearData(Def.SERIES a_series, int a_eventID, int a_stageNo, out PlayerClearData _psd)
	{
		bool result = false;
		_psd = null;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			result = GetStageClearData(a_series, a_eventID, data.CourseID, a_stageNo, out _psd);
		}
		return result;
	}

	public bool GetStageClearData(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageNo, out PlayerClearData _psd)
	{
		_psd = null;
		bool result = false;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			if (playerMapData != null)
			{
				for (int i = 0; i < playerMapData.StageClearData.Count; i++)
				{
					if (playerMapData.StageClearData[i].StageNo == a_stageNo)
					{
						_psd = playerMapData.StageClearData[i];
						result = true;
						break;
					}
				}
			}
		}
		return result;
	}

	public void SetStageClearData(Def.SERIES a_series, PlayerEventClearData _psd, bool remove = false)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, _psd.EventID, out data);
		if (data == null)
		{
			return;
		}
		PlayerMapData playerMapData = data.CourseData[_psd.CourseID];
		if (playerMapData == null)
		{
			return;
		}
		for (int i = 0; i < playerMapData.StageClearData.Count; i++)
		{
			if (playerMapData.StageClearData[i].StageNo == _psd.StageNo)
			{
				if (remove)
				{
					playerMapData.StageClearData.Remove(_psd);
				}
				else
				{
					playerMapData.StageClearData[i] = _psd;
				}
				return;
			}
		}
		playerMapData.StageClearData.Add(_psd);
	}

	public bool AddStageClearData(PlayerClearData _psd)
	{
		PlayerClearData playerClearData = _psd.Clone();
		PlayerClearData _psd2 = null;
		bool flag = false;
		if (GetStageClearData(_psd.Series, _psd.StageNo, out _psd2) && _psd2 != null)
		{
			flag = _psd2.Update(playerClearData);
			if (flag)
			{
				playerClearData = _psd2;
			}
		}
		else
		{
			flag = true;
		}
		if (flag)
		{
			StageClearData[playerClearData.Series][playerClearData.StageNo] = playerClearData;
			mClearLevelCache = -1;
			mTotalStarsCache = -1;
			mTotalHighScoreCache = -1L;
			mTotalClearStageCache = -1;
			return true;
		}
		return false;
	}

	public bool AddStageClearData(PlayerEventClearData _psd)
	{
		PlayerEventClearData playerEventClearData = _psd.Clone();
		PlayerClearData _psd2 = null;
		bool flag = false;
		if (GetStageClearData(_psd.Series, _psd.EventID, _psd.CourseID, _psd.StageNo, out _psd2) && _psd2 != null)
		{
			flag = _psd2.Update(playerEventClearData);
			if (flag)
			{
				playerEventClearData = _psd2 as PlayerEventClearData;
			}
		}
		else
		{
			flag = true;
		}
		if (flag)
		{
			SetStageClearData(_psd.Series, playerEventClearData);
			mClearLevelCache = -1;
			mTotalStarsCache = -1;
			mTotalHighScoreCache = -1L;
			mTotalClearStageCache = -1;
			return true;
		}
		return false;
	}

	public void RemoveStageClearData(PlayerClearData _psd)
	{
		PlayerClearData _psd2 = null;
		if (GetStageClearData(_psd.Series, _psd.StageNo, out _psd2))
		{
			StageClearData[_psd.Series].Remove(_psd.StageNo);
			mClearLevelCache = -1;
			mTotalStarsCache = -1;
			mTotalHighScoreCache = -1L;
		}
	}

	public void RemoveStageClearData(PlayerEventClearData _psd)
	{
		PlayerClearData _psd2 = null;
		if (GetStageClearData(_psd.Series, _psd.EventID, _psd.CourseID, _psd.StageNo, out _psd2))
		{
			SetStageClearData(_psd.Series, _psd, true);
			mClearLevelCache = -1;
			mTotalStarsCache = -1;
			mTotalHighScoreCache = -1L;
		}
	}

	public bool GetStageChallengeData(Def.SERIES a_series, int a_stageNo, out PlayerStageData _psd)
	{
		_psd = null;
		IDictionary<int, PlayerStageData> value;
		bool flag = StageChallengeData.TryGetValue(a_series, out value);
		if (flag)
		{
			flag = value.TryGetValue(a_stageNo, out _psd);
		}
		return flag;
	}

	public bool GetStageChallengeData(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageNo, out PlayerStageData _psd)
	{
		_psd = null;
		bool result = false;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		PlayerMapData value;
		if (data != null && data.CourseData.TryGetValue(a_courseID, out value))
		{
			for (int i = 0; i < value.StageChallengeData.Count; i++)
			{
				if (value.StageChallengeData[i].StageNo == a_stageNo)
				{
					_psd = value.StageChallengeData[i];
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public void UpdateStageChallengeData(Def.SERIES a_series, int stageNo, bool a_isWon)
	{
		PlayerStageData _psd;
		if (!GetStageChallengeData(a_series, stageNo, out _psd))
		{
			_psd = new PlayerStageData();
			_psd.Series = a_series;
			_psd.StageNo = stageNo;
		}
		_psd.Update(a_isWon);
		StageChallengeData[_psd.Series][_psd.StageNo] = _psd;
	}

	public void UpdateStageChallegeData(Def.SERIES a_series, int a_eventID, short a_courseID, int stageNo, bool a_isWon)
	{
		PlayerStageData _psd;
		if (!GetStageChallengeData(a_series, a_eventID, a_courseID, stageNo, out _psd))
		{
			PlayerEventStageData playerEventStageData = new PlayerEventStageData();
			playerEventStageData.Series = a_series;
			playerEventStageData.EventID = a_eventID;
			playerEventStageData.CourseID = a_courseID;
			playerEventStageData.StageNo = stageNo;
			_psd = playerEventStageData;
		}
		PlayerEventStageData playerEventStageData2 = _psd as PlayerEventStageData;
		playerEventStageData2.Update(a_isWon);
		SetEventStageChallengeData(playerEventStageData2.Series, playerEventStageData2);
	}

	public void SetMapStageChallengeData(Def.SERIES a_series, PlayerStageData _psd, bool remove = false)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data == null)
		{
			return;
		}
		for (int i = 0; i < data.StageChallengeData.Count; i++)
		{
			if (data.StageChallengeData[i].StageNo == _psd.StageNo)
			{
				if (remove)
				{
					data.StageChallengeData.Remove(_psd);
				}
				else
				{
					data.StageChallengeData[i] = _psd;
				}
				return;
			}
		}
		data.StageChallengeData.Add(_psd);
	}

	public void SetEventStageChallengeData(Def.SERIES a_series, PlayerEventStageData _psd, bool remove = false)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, _psd.EventID, out data);
		if (data == null)
		{
			return;
		}
		PlayerMapData playerMapData = data.CourseData[_psd.CourseID];
		if (playerMapData == null)
		{
			return;
		}
		for (int i = 0; i < playerMapData.StageChallengeData.Count; i++)
		{
			if (playerMapData.StageChallengeData[i].StageNo == _psd.StageNo)
			{
				if (remove)
				{
					playerMapData.StageChallengeData.Remove(_psd);
				}
				else
				{
					playerMapData.StageChallengeData[i] = _psd;
				}
				return;
			}
		}
		playerMapData.StageChallengeData.Add(_psd);
	}

	public void RemoveStageChallengeData(PlayerStageData _psd)
	{
		PlayerStageData _psd2 = null;
		if (GetStageChallengeData(_psd.Series, _psd.StageNo, out _psd2))
		{
			StageChallengeData[_psd.Series].Remove(_psd.StageNo);
		}
	}

	public void RemoveStageChallengeData(PlayerEventStageData _psd)
	{
		PlayerStageData _psd2 = null;
		if (GetStageChallengeData(_psd.Series, _psd.StageNo, out _psd2))
		{
			SetEventStageChallengeData(_psd.Series, _psd, true);
		}
	}

	public void OpenStageStatus(Def.SERIES a_series, int a_stageNo)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			STAGE_STATUS value;
			if (!data.StageStatus.TryGetValue(a_stageNo, out value))
			{
				data.StageStatus.Add(a_stageNo, STAGE_STATUS.LOCK);
			}
			else if (value == STAGE_STATUS.NOOPEN)
			{
				data.StageStatus[a_stageNo] = STAGE_STATUS.LOCK;
			}
		}
		Data.SetMapData(a_series, data);
	}

	public void OpenStageStatus(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageNo)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data == null)
		{
			return;
		}
		PlayerMapData playerMapData = data.CourseData[a_courseID];
		if (playerMapData != null)
		{
			STAGE_STATUS value;
			if (!playerMapData.StageStatus.TryGetValue(a_stageNo, out value))
			{
				playerMapData.StageStatus.Add(a_stageNo, STAGE_STATUS.LOCK);
			}
			else if (value == STAGE_STATUS.NOOPEN)
			{
				playerMapData.StageStatus[a_stageNo] = STAGE_STATUS.LOCK;
			}
		}
	}

	public void SetStageStatus(Def.SERIES a_series, int a_stageNo, STAGE_STATUS a_status)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			STAGE_STATUS value;
			if (data.StageStatus.TryGetValue(a_stageNo, out value))
			{
				data.StageStatus[a_stageNo] = a_status;
			}
			else
			{
				data.StageStatus.Add(a_stageNo, a_status);
			}
		}
		Data.SetMapData(a_series, data);
	}

	public void SetStageStatus(Def.SERIES a_series, int a_eventID, int a_stageNo, STAGE_STATUS a_status)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			SetStageStatus(a_series, a_eventID, data.CourseID, a_stageNo, a_status);
		}
	}

	public void SetStageStatus(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageNo, STAGE_STATUS a_status)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data == null)
		{
			return;
		}
		PlayerMapData playerMapData = data.CourseData[a_courseID];
		if (playerMapData != null)
		{
			STAGE_STATUS value;
			if (playerMapData.StageStatus.TryGetValue(a_stageNo, out value))
			{
				playerMapData.StageStatus[a_stageNo] = a_status;
			}
			else
			{
				playerMapData.StageStatus.Add(a_stageNo, a_status);
			}
			data.CourseData[a_courseID] = playerMapData;
		}
		Data.SetMapData(a_eventID, data);
	}

	public void SetClearStageInfo(int a_stage)
	{
		PlayerMapData data;
		Data.GetMapData(Data.CurrentSeries, out data);
		data.LastClearedLevel = a_stage;
		Data.SetMapData(Data.CurrentSeries, data);
		SetTargetStage(a_stage);
		if (PlayableMaxLevel <= a_stage)
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(a_stage, out a_main, out a_sub);
			if (a_sub == 0)
			{
				SetPlayableMaxLevel(Data.CurrentSeries, Def.GetStageNo(a_main + 1, 0));
			}
		}
	}

	public void SetClearStageFromTemporary(int a_stage)
	{
		PlayerMapData data;
		Data.GetMapData(Data.CurrentSeries, out data);
		data.LastClearedLevel = a_stage;
		Data.SetMapData(Data.CurrentSeries, data);
		SetTargetStage(a_stage);
	}

	public void SetClearStageInfoForSupport(Def.SERIES a_series, int a_stage)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		data.LastClearedLevel = a_stage;
		Data.SetMapData(a_series, data);
		if (data.PlayableMaxLevel <= a_stage)
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(a_stage, out a_main, out a_sub);
			if (a_sub == 0)
			{
				SetPlayableMaxLevel(a_series, Def.GetStageNo(a_main + 1, 0));
			}
		}
	}

	public void SetClearEventStageInfo(int a_stage)
	{
		PlayerMapData playerMapData = null;
		PlayerEventData data = null;
		Data.GetMapData(Data.CurrentSeries, Data.CurrentEventID, out data);
		playerMapData = data.CourseData[data.CourseID];
		playerMapData.LastClearedLevel = a_stage;
		data.CourseData[data.CourseID] = playerMapData;
		Data.SetMapData(Data.CurrentEventID, data);
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stage, out a_main, out a_sub);
		int stageNoForServer = Def.GetStageNoForServer(Data.CurrentEventID, Def.GetStageNo(data.CourseID, a_main, a_sub));
		SetTargetStage(stageNoForServer);
		if (PlayableMaxLevel <= a_stage && a_sub == 0)
		{
			SetPlayableMaxLevel(Data.CurrentSeries, Data.CurrentEventID, data.CourseID, Def.GetStageNo(a_main + 1, 0));
		}
	}

	public void SetClearEventStageInfoForSupport(Def.SERIES a_series, int a_stage, int a_eventID, short a_course)
	{
		PlayerMapData playerMapData = null;
		PlayerEventData data = null;
		Data.GetMapData(a_series, a_eventID, out data);
		playerMapData = data.CourseData[a_course];
		playerMapData.LastClearedLevel = a_stage;
		data.CourseData[a_course] = playerMapData;
		Data.SetMapData(a_eventID, data);
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stage, out a_main, out a_sub);
		int stageNoForServer = Def.GetStageNoForServer(a_eventID, Def.GetStageNo(a_course, a_main, a_sub));
		if (playerMapData.PlayableMaxLevel <= a_stage && a_sub == 0)
		{
			SetPlayableMaxLevel(a_series, a_eventID, a_course, Def.GetStageNo(a_main + 1, 0));
		}
	}

	public void SetTargetStage(int a_stageNo)
	{
		ClearStageToS = a_stageNo;
	}

	public STAGE_STATUS GetStageStatus(Def.SERIES a_series, int a_stageNo)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		STAGE_STATUS value;
		if (data.StageStatus.TryGetValue(a_stageNo, out value))
		{
			return value;
		}
		return STAGE_STATUS.NOOPEN;
	}

	public STAGE_STATUS GetStageStatus(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageNo)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			STAGE_STATUS value;
			if (playerMapData != null && playerMapData.StageStatus.TryGetValue(a_stageNo, out value))
			{
				return value;
			}
		}
		return STAGE_STATUS.NOOPEN;
	}

	public void SetLineStatus(Def.SERIES a_series, int a_stageNo, STAGE_STATUS a_status)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		if (data != null)
		{
			STAGE_STATUS value;
			if (data.LineStatus.TryGetValue(a_stageNo, out value))
			{
				data.LineStatus[a_stageNo] = a_status;
			}
			else
			{
				data.LineStatus.Add(a_stageNo, a_status);
			}
			Data.SetMapData(a_series, data);
		}
	}

	public void SetLineStatus(Def.SERIES a_series, int a_eventID, short a_courseID, int a_stageNo, STAGE_STATUS a_status)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data == null)
		{
			return;
		}
		PlayerMapData playerMapData = data.CourseData[a_courseID];
		if (playerMapData != null)
		{
			STAGE_STATUS value;
			if (playerMapData.LineStatus.TryGetValue(a_stageNo, out value))
			{
				playerMapData.LineStatus[a_stageNo] = a_status;
			}
			else
			{
				playerMapData.LineStatus.Add(a_stageNo, a_status);
			}
			data.CourseData[a_courseID] = playerMapData;
		}
		Data.SetMapData(a_eventID, data);
	}

	public STAGE_STATUS GetLineStatus(Def.SERIES a_series, int a_stageNo)
	{
		PlayerMapData data;
		Data.GetMapData(a_series, out data);
		STAGE_STATUS value;
		if (data != null && data.LineStatus.TryGetValue(a_stageNo, out value))
		{
			return value;
		}
		return STAGE_STATUS.NOOPEN;
	}

	public STAGE_STATUS GetLineStatus(int a_eventID, short a_courseID, int a_stageNo)
	{
		PlayerEventData data;
		Data.GetMapData(Data.CurrentSeries, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			STAGE_STATUS value;
			if (playerMapData != null && playerMapData.LineStatus.TryGetValue(a_stageNo, out value))
			{
				return value;
			}
		}
		return STAGE_STATUS.NOOPEN;
	}

	public void UpdateRestoreCount()
	{
		if (Data.LastRestoreTransactionTime.Day != DateTime.Now.Day)
		{
			Data.RestoreTransactionCount = 0;
		}
	}

	public bool CheckRestoreCount()
	{
		if (Data.RestoreTransactionCount >= 6)
		{
			return false;
		}
		return true;
	}

	public void AddRestoreCount()
	{
		Data.RestoreTransactionCount++;
		Data.LastRestoreTransactionTime = DateTime.Now;
	}

	public void AddGift(GiftItem gift)
	{
		if (gift == null)
		{
			return;
		}
		gift.Data.CreateTime = DateTime.Now;
		List<string> list = FindReplaceGiftID(gift);
		if (list != null && list.Count > 0)
		{
			foreach (string item in list)
			{
				if (!string.IsNullOrEmpty(item))
				{
					Gifts.Remove(item);
				}
			}
		}
		Gifts[gift.Data.GiftID] = gift;
		bool flag = true;
		if (gift.Data.MessageCardID != -1)
		{
			flag = false;
			GiftItem giftItem = null;
			foreach (KeyValuePair<string, GiftItem> gift2 in Gifts)
			{
				if (gift2.Value.Data.ItemCategory == 34 && gift2.Value.Data.MessageCardID == gift.Data.MessageCardID)
				{
					giftItem = gift2.Value;
					break;
				}
			}
			if (giftItem != null && giftItem.Data.MessageCardPresentCount == SingletonMonoBehaviour<GameMain>.Instance.GetMessageCardPresentList(giftItem.Data.MessageCardID).Count)
			{
				flag = true;
			}
		}
		if (flag)
		{
			mModifiedGifts.Add(gift);
		}
		Data.GotGifts++;
	}

	public void RemoveGift(GiftItem gift)
	{
		if (gift != null && Gifts.Count >= 1)
		{
			Gifts.Remove(gift.Data.GiftID);
			mModifiedGifts.Remove(gift);
		}
	}

	private List<string> FindReplaceGiftID(GiftItem gift)
	{
		List<string> list = new List<string>();
		if (Gifts.Count < 1)
		{
			return list;
		}
		if (gift.Data.FromID == 0)
		{
			return list;
		}
		if (gift.GiftItemCategory == Def.ITEM_CATEGORY.CONSUME)
		{
			if (gift.Data.ItemKind == 2)
			{
				foreach (KeyValuePair<string, GiftItem> gift2 in Gifts)
				{
					string key = gift2.Key;
					GiftItem value = gift2.Value;
					if (!string.IsNullOrEmpty(key) && value != null && value.GiftItemCategory == gift.GiftItemCategory && value.Data.ItemKind == gift.Data.ItemKind && gift.Data.FromID.CompareTo(value.Data.FromID) == 0)
					{
						list.Add(key);
					}
				}
			}
		}
		else if (gift.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ || gift.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_RES)
		{
			foreach (KeyValuePair<string, GiftItem> gift3 in Gifts)
			{
				string key2 = gift3.Key;
				GiftItem value2 = gift3.Value;
				if (!string.IsNullOrEmpty(key2) && value2 != null && value2.GiftItemCategory == gift.GiftItemCategory && gift.Data.FromID == value2.Data.FromID && gift.Data.ItemKind == value2.Data.ItemKind && gift.Data.ItemID == value2.Data.ItemID)
				{
					list.Add(key2);
				}
			}
		}
		else if (gift.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ || gift.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_RES)
		{
			foreach (KeyValuePair<string, GiftItem> gift4 in Gifts)
			{
				string key3 = gift4.Key;
				GiftItem value3 = gift4.Value;
				if (!string.IsNullOrEmpty(key3) && value3 != null && value3.GiftItemCategory == gift.GiftItemCategory && gift.Data.FromID == value3.Data.FromID)
				{
					list.Add(key3);
				}
			}
		}
		else if (gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ || gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_RES)
		{
			foreach (KeyValuePair<string, GiftItem> gift5 in Gifts)
			{
				string key4 = gift5.Key;
				GiftItem value4 = gift5.Value;
				if (!string.IsNullOrEmpty(key4) && value4 != null && value4.GiftItemCategory == gift.GiftItemCategory && gift.Data.FromID == value4.Data.FromID && gift.Data.ItemKind == value4.Data.ItemKind && gift.Data.ItemID == value4.Data.ItemID)
				{
					list.Add(key4);
				}
			}
		}
		else if (gift.GiftItemCategory == Def.ITEM_CATEGORY.PRESENT_HEART || gift.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL)
		{
			foreach (KeyValuePair<string, GiftItem> gift6 in Gifts)
			{
				string key5 = gift6.Key;
				GiftItem value5 = gift6.Value;
				if (!string.IsNullOrEmpty(key5) && value5 != null && value5.GiftItemCategory == gift.GiftItemCategory && gift.Data.FromID.CompareTo(value5.Data.FromID) == 0)
				{
					list.Add(key5);
				}
			}
		}
		return list;
	}

	public List<GiftItem> GetModifiedGifts()
	{
		return new List<GiftItem>(mModifiedGifts);
	}

	public bool HasModifiedGifts()
	{
		bool result = false;
		if (mModifiedGifts != null && mModifiedGifts.Count > 0)
		{
			result = true;
		}
		return result;
	}

	public void ClearModifiedGifts()
	{
		mModifiedGifts.Clear();
	}

	public void RemoveModifiedGift(GiftItem giftItem)
	{
		mModifiedGifts.Remove(giftItem);
	}

	public int GetMailByFriendReq()
	{
		int num = 0;
		List<GiftItem> list = new List<GiftItem>(Gifts.Values);
		return list.FindAll((GiftItem p) => p.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ || p.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ).Count;
	}

	public void ResetFriends()
	{
		ResetFriends(true);
	}

	public void ResetFriends(bool clearIcons)
	{
		Friends.Clear();
		if (!clearIcons)
		{
			return;
		}
		string iconBasePath = MyFriend.IconBasePath;
		if (Directory.Exists(iconBasePath))
		{
			try
			{
				Directory.Delete(iconBasePath, true);
			}
			catch (Exception)
			{
			}
		}
	}

	public void RemoveFriend(MyFriend friend)
	{
		if (friend != null && Friends.Count >= 1)
		{
			Friends.Remove(friend.Data.UUID);
			ApplyFriends.Remove(friend.Data.UUID);
			Data.NotificationNewFriends.Remove(friend.Data.UUID);
			if (friend.Icon != null)
			{
				friend.RemoveIcon();
			}
		}
	}

	public void UpdateSendHeart(int a_uuid)
	{
		DateTime now = DateTime.Now;
		if (SendInfo.ContainsKey(a_uuid))
		{
			SendInfo[a_uuid].LastHeartSendTime = now;
			return;
		}
		PlayerSendData playerSendData = new PlayerSendData();
		playerSendData.UUID = a_uuid;
		playerSendData.LastHeartSendTime = now;
		SendInfo.Add(a_uuid, playerSendData);
	}

	public bool SendFriendHelpStatus()
	{
		bool result = true;
		long num = DateTimeUtil.BetweenDays0(Data.LastSentFriendHelpTime, DateTime.Now);
		long num2 = Constants.SEND_FRIENDHELP_FREQ;
		if (num2 > 0 && num < num2)
		{
			result = false;
		}
		if (Friends.Count == 0)
		{
			result = false;
		}
		return result;
	}

	public List<MyFriendData> GetSendableFriendHelp()
	{
		List<MyFriendData> list = new List<MyFriendData>();
		for (int i = 0; i < Data.FriendData.Count; i++)
		{
			MyFriendData myFriendData = Data.FriendData[i];
			bool flag = true;
			DateTime fdt = DateTimeUtil.UnitLocalTimeEpoch;
			if (SendInfo.ContainsKey(myFriendData.UUID))
			{
				fdt = SendInfo[myFriendData.UUID].LastFriendHelpRequestTime;
			}
			long num = DateTimeUtil.BetweenDays0(fdt, DateTime.Now);
			long num2 = Constants.SEND_FRIENDHELP_FREQ;
			if (num2 > 0 && num < num2)
			{
				flag = false;
			}
			if (flag)
			{
				list.Add(myFriendData);
			}
		}
		return list;
	}

	public bool IsValidFriendHelp(int a_series, int a_stage)
	{
		PlayerMapData data;
		Data.GetMapData((Def.SERIES)a_series, out data);
		if (data != null && data.PlayableMaxLevel >= a_stage)
		{
			return true;
		}
		return false;
	}

	public void UpdateFriendHelp(int a_uuid)
	{
		DateTime now = DateTime.Now;
		if (SendInfo.ContainsKey(a_uuid))
		{
			SendInfo[a_uuid].LastFriendHelpRequestTime = now;
			return;
		}
		PlayerSendData playerSendData = new PlayerSendData();
		playerSendData.UUID = a_uuid;
		playerSendData.LastFriendHelpRequestTime = now;
		SendInfo.Add(a_uuid, playerSendData);
	}

	public NOTIFICATION_STATUS GetCollectionNotifyStatus(int index)
	{
		byte value;
		if (!Data.NotificationCollections.TryGetValue(index, out value))
		{
			return NOTIFICATION_STATUS.NONE;
		}
		return (NOTIFICATION_STATUS)value;
	}

	public void SetCollectionNotify(int index)
	{
		Data.NotificationCollections[index] = 1;
	}

	public void CollectionNotified(int index)
	{
		Data.NotificationCollections[index] = 2;
	}

	public NOTIFICATION_STATUS GetNewFriendNotifyStatus(int uuid)
	{
		byte value;
		if (!Data.NotificationNewFriends.TryGetValue(uuid, out value))
		{
			return NOTIFICATION_STATUS.NONE;
		}
		return (NOTIFICATION_STATUS)value;
	}

	public void SetNewFriendNotify(int uuid)
	{
		Data.NotificationNewFriends[uuid] = 1;
	}

	public void NewFriendNotified(int uuid)
	{
		Data.NotificationNewFriends[uuid] = 2;
	}

	public void SetPartnerNotify(int index, int level)
	{
		if (level <= 15)
		{
			if (Data.NotificationNewPartners.ContainsKey(index))
			{
				Dictionary<int, int> notificationNewPartners;
				Dictionary<int, int> dictionary = (notificationNewPartners = Data.NotificationNewPartners);
				int key;
				int key2 = (key = index);
				key = notificationNewPartners[key];
				dictionary[key2] = key | (1 << (level & 0x1F));
			}
			else
			{
				Data.NotificationNewPartners[index] = 1 << level;
			}
		}
	}

	public void SetPartnerNotified(int index, int level)
	{
		if (level <= 15)
		{
			Dictionary<int, int> notificationNewPartners;
			Dictionary<int, int> dictionary = (notificationNewPartners = Data.NotificationNewPartners);
			int key;
			int key2 = (key = index);
			key = notificationNewPartners[key];
			dictionary[key2] = key | (1 << ((level + 16) & 0x1F));
		}
	}

	public bool IsPartnerNotify(int index, int level)
	{
		if (level > 15)
		{
			return false;
		}
		return (Data.NotificationNewPartners[index] & (1 << level)) == 1 && (Data.NotificationNewPartners[index] & (1 << level + 16)) == 0;
	}

	public void SetUserBirthday(int year, int month, int day)
	{
		Data.UserBirthDay = string.Format("{0}/{1}/{2}", year, month, day);
	}

	public void GetUserBirthdayDate(out int year, out int month, out int day)
	{
		year = 0;
		month = 0;
		day = 0;
		string[] array = Data.UserBirthDay.Split('/');
		if (array.Length == 3)
		{
			year = int.Parse(array[0]);
			month = int.Parse(array[1]);
			day = int.Parse(array[2]);
		}
	}

	public DateTime GetUserBirthday()
	{
		int year;
		int month;
		int day;
		GetUserBirthdayDate(out year, out month, out day);
		return new DateTime(year, month, day);
	}

	public void BoughtItem(ShopItemDetail a_item)
	{
		if (a_item == null)
		{
			return;
		}
		foreach (KeyValuePair<ShopItemDetail.ItemKind, int> bundleItem in a_item.BundleItems)
		{
			AddBoughtItem(bundleItem.Key, bundleItem.Value);
		}
	}

	public void AddBoughtItem(ShopItemDetail.ItemKind a_kind, int num)
	{
		switch (a_kind)
		{
		case ShopItemDetail.ItemKind.WAVE_BOMB:
			AddPaidBoosterNum(Def.BOOSTER_KIND.WAVE_BOMB, num);
			break;
		case ShopItemDetail.ItemKind.TAP_BOMB2:
			AddPaidBoosterNum(Def.BOOSTER_KIND.TAP_BOMB2, num);
			break;
		case ShopItemDetail.ItemKind.RAINBOW:
			AddPaidBoosterNum(Def.BOOSTER_KIND.RAINBOW, num);
			break;
		case ShopItemDetail.ItemKind.ADD_SCORE:
			AddPaidBoosterNum(Def.BOOSTER_KIND.ADD_SCORE, num);
			break;
		case ShopItemDetail.ItemKind.SKILL_CHARGE:
			AddPaidBoosterNum(Def.BOOSTER_KIND.SKILL_CHARGE, num);
			break;
		case ShopItemDetail.ItemKind.SELECT_ONE:
			AddPaidBoosterNum(Def.BOOSTER_KIND.SELECT_ONE, num);
			break;
		case ShopItemDetail.ItemKind.SELECT_COLLECT:
			AddPaidBoosterNum(Def.BOOSTER_KIND.SELECT_COLLECT, num);
			break;
		case ShopItemDetail.ItemKind.COLOR_CRUSH:
			AddPaidBoosterNum(Def.BOOSTER_KIND.COLOR_CRUSH, num);
			break;
		case ShopItemDetail.ItemKind.BLOCK_CRUSH:
			AddPaidBoosterNum(Def.BOOSTER_KIND.BLOCK_CRUSH, num);
			break;
		case ShopItemDetail.ItemKind.GROWUP:
			AddGrowup(num);
			break;
		case ShopItemDetail.ItemKind.HEART:
			AddLifeCount(num);
			break;
		case ShopItemDetail.ItemKind.MUGEN_HEART15:
			AddPaidBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15, num);
			break;
		case ShopItemDetail.ItemKind.MUGEN_HEART30:
			AddPaidBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30, num);
			break;
		case ShopItemDetail.ItemKind.MUGEN_HEART60:
			AddPaidBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60, num);
			break;
		case ShopItemDetail.ItemKind.PAIR_MAKER:
			AddPaidBoosterNum(Def.BOOSTER_KIND.PAIR_MAKER, num);
			break;
		case ShopItemDetail.ItemKind.ALL_CRUSH:
			AddPaidBoosterNum(Def.BOOSTER_KIND.ALL_CRUSH, num);
			break;
		case ShopItemDetail.ItemKind.CONTINUE:
		case ShopItemDetail.ItemKind.ROADBLOCK:
		case ShopItemDetail.ItemKind.STAGESKIP:
			break;
		}
	}

	public void TrophyItem(TrophyItemDetail a_item)
	{
		if (a_item == null)
		{
			return;
		}
		foreach (KeyValuePair<TrophyItemDetail.ItemKind, int> bundleItem in a_item.BundleItems)
		{
			AddTrophyItem(bundleItem.Key, bundleItem.Value);
		}
	}

	public void AddTrophyItem(TrophyItemDetail.ItemKind a_kind, int num)
	{
		int num2 = 0;
		int itemKind = 0;
		int num3 = num;
		switch (a_kind)
		{
		case TrophyItemDetail.ItemKind.WAVE_BOMB:
			num2 = 3;
			itemKind = 1;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.TAP_BOMB2:
			num2 = 3;
			itemKind = 2;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.RAINBOW:
			num2 = 3;
			itemKind = 3;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.ADD_SCORE:
			num2 = 3;
			itemKind = 4;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.SKILL_CHARGE:
			num2 = 3;
			itemKind = 5;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.SELECT_ONE:
			num2 = 3;
			itemKind = 6;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.SELECT_COLLECT:
			num2 = 3;
			itemKind = 7;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.COLOR_CRUSH:
			num2 = 3;
			itemKind = 8;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.BLOCK_CRUSH:
			num2 = 3;
			itemKind = 9;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.ROADBLOCK_TICKET:
			num2 = 1;
			itemKind = 7;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.PAIR_MAKER:
			num2 = 3;
			itemKind = 13;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.OLDEVENTKEY:
			num2 = 1;
			itemKind = 20;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.HEART:
			num2 = 1;
			itemKind = 2;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.GROWUP:
			num2 = 1;
			itemKind = 10;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.GROWUPPEACE:
			num2 = 1;
			itemKind = 11;
			num3 = num;
			break;
		case TrophyItemDetail.ItemKind.HEARTPEACE:
			num2 = 1;
			itemKind = 9;
			num3 = num;
			break;
		}
		if (num2 > 0 && num3 > 0)
		{
			GiftMailType aType = GiftMailType.LOCAL_TROPHY;
			GiftItem giftItem = new GiftItem(aType);
			giftItem.Data.ItemCategory = num2;
			giftItem.Data.ItemKind = itemKind;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = num3;
			AddGift(giftItem);
		}
	}

	public void AddTrophyItem(int itemID, int countID)
	{
		if (TrophyExchangeDict.ContainsKey(itemID))
		{
			TrophyExchangeInfo trophyExchangeInfo = TrophyExchangeDict[itemID];
			if (countID <= trophyExchangeInfo.SettingID)
			{
				trophyExchangeInfo.ExchangeCount++;
				TrophyExchangeDict[itemID] = trophyExchangeInfo;
				trophyExchangeInfo.ExchangeTotalCount++;
			}
			else
			{
				trophyExchangeInfo.SettingID = countID;
				trophyExchangeInfo.ExchangeCount = 1;
				TrophyExchangeDict[itemID] = trophyExchangeInfo;
				trophyExchangeInfo.ExchangeTotalCount++;
			}
		}
		else
		{
			TrophyExchangeInfo trophyExchangeInfo2 = new TrophyExchangeInfo();
			trophyExchangeInfo2.PrizeID = itemID;
			trophyExchangeInfo2.ExchangeCount = 1;
			trophyExchangeInfo2.SettingID = countID;
			TrophyExchangeDict[itemID] = trophyExchangeInfo2;
			trophyExchangeInfo2.ExchangeTotalCount++;
		}
	}

	public int ItemCountCheck(int itemID)
	{
		int num = 0;
		if (TrophyExchangeDict.ContainsKey(itemID))
		{
			TrophyExchangeInfo trophyExchangeInfo = TrophyExchangeDict[itemID];
			return trophyExchangeInfo.ExchangeCount;
		}
		return 0;
	}

	public int ItemTotalCountCheck(int itemID)
	{
		int num = 0;
		if (TrophyExchangeDict.ContainsKey(itemID))
		{
			TrophyExchangeInfo trophyExchangeInfo = TrophyExchangeDict[itemID];
			return trophyExchangeInfo.ExchangeTotalCount;
		}
		return 0;
	}

	public void RemoveTrophyPurchaseItem(int itemID)
	{
		if (TrophyExchangeDict.ContainsKey(itemID))
		{
			TrophyExchangeDict.Remove(itemID);
		}
	}

	public int IsEnableTrophySettingItem(int itemID, int limit, int countID)
	{
		int num = 0;
		if (limit == 0)
		{
			return num - 1;
		}
		if (TrophyExchangeDict.ContainsKey(itemID))
		{
			TrophyExchangeInfo trophyExchangeInfo = TrophyExchangeDict[itemID];
			if (countID > trophyExchangeInfo.SettingID)
			{
				trophyExchangeInfo.ExchangeCount = 0;
				trophyExchangeInfo.SettingID = countID;
				TrophyExchangeDict[itemID] = trophyExchangeInfo;
				return limit;
			}
			if (trophyExchangeInfo.ExchangeCount < limit)
			{
				return limit - trophyExchangeInfo.ExchangeCount;
			}
			return 0;
		}
		return limit;
	}

	public void AddPurchaseItem(int itemID, int countID)
	{
		if (PurchaseShop.ContainsKey(itemID))
		{
			PlayerShopData playerShopData = PurchaseShop[itemID];
			if (playerShopData.ShopCount == countID)
			{
				playerShopData.ShopPurchaseItemNum++;
				PurchaseShop[itemID] = playerShopData;
			}
			else
			{
				playerShopData.ShopCount = countID;
				playerShopData.ShopPurchaseItemNum = 1;
				PurchaseShop[itemID] = playerShopData;
			}
		}
		else
		{
			PlayerShopData playerShopData2 = new PlayerShopData();
			playerShopData2.ShopItemID = itemID;
			playerShopData2.ShopPurchaseItemNum = 1;
			playerShopData2.ShopCount = countID;
			PurchaseShop[itemID] = playerShopData2;
		}
	}

	public void RemovePurchaseItem(int itemID)
	{
		if (PurchaseShop.ContainsKey(itemID))
		{
			PurchaseShop.Remove(itemID);
		}
	}

	public bool IsEnablePurchaseItem(int itemID, int limit, int countID)
	{
		bool result = false;
		if (limit == 0)
		{
			return true;
		}
		DateTime? showSaleIconTime = ShowSaleIconTime;
		DateTime now = ((!showSaleIconTime.HasValue) ? DateTimeUtil.Now() : ShowSaleIconTime.Value);
		if (!SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.ShopItemList.Any((ShopLimitedTime _) => _.ShopItemID == itemID && _.ShopCount == countID && DateTimeUtil.UnixTimeStampToDateTime(_.StartTime, true) < now && DateTimeUtil.UnixTimeStampToDateTime(_.EndTime, true) > now))
		{
			return result;
		}
		if (PurchaseShop.ContainsKey(itemID))
		{
			PlayerShopData playerShopData = PurchaseShop[itemID];
			if (playerShopData.ShopCount != countID)
			{
				playerShopData.ShopPurchaseItemNum = 0;
				playerShopData.ShopCount = countID;
				PurchaseShop[itemID] = playerShopData;
				return true;
			}
			if (playerShopData.ShopPurchaseItemNum < limit)
			{
				return true;
			}
			return false;
		}
		return true;
	}

	public List<SearchUserData> GetStrayUserList(List<SearchUserData> a_list)
	{
		List<SearchUserData> list = new List<SearchUserData>();
		foreach (SearchUserData item in a_list)
		{
			if (!Friends.ContainsKey(item.uuid) && !ApplyFriends.ContainsKey(item.uuid))
			{
				list.Add(item);
			}
		}
		return list;
	}

	public List<MyFaceBookFriend> GetFacebookUserList(List<MyFaceBookFriend> a_list)
	{
		List<MyFaceBookFriend> list = new List<MyFaceBookFriend>();
		foreach (MyFaceBookFriend item in a_list)
		{
			if (!Friends.ContainsKey(item.UUID) && !ApplyFriends.ContainsKey(item.UUID))
			{
				list.Add(item);
			}
		}
		return list;
	}

	public void SetEvent(int a_eventID, short a_maxCourse, Def.EVENT_TYPE a_eventType)
	{
		Data.PreviousSeries = Data.CurrentSeries;
		if (Def.IsEventSeries(Data.PreviousSeries) || Def.IsAdvSeries(Data.PreviousSeries))
		{
			Data.PreviousSeries = Def.SERIES.SM_FIRST;
		}
		Data.CurrentSeries = Def.SERIES.SM_EV;
		Data.CurrentEventID = a_eventID;
		CreateEventData(a_eventID, a_maxCourse, a_eventType);
	}

	public void CreateEventData(int a_eventID, short a_maxCourse, Def.EVENT_TYPE a_eventType)
	{
		PlayerEventData data;
		Data.GetMapData(Def.SERIES.SM_EV, a_eventID, out data);
		if (data == null)
		{
			data = new PlayerEventData();
			data.NewCreate(a_eventID, a_maxCourse, a_eventType);
			Data.SetMapData(a_eventID, data);
		}
	}

	public void SetAdventure()
	{
		if (Def.IsEventSeries(Data.CurrentSeries) || Def.IsAdvSeries(Data.CurrentSeries))
		{
			if (Def.IsEventSeries(Data.PreviousSeries) || Def.IsAdvSeries(Data.PreviousSeries))
			{
				Data.PreviousSeries = Def.SERIES.SM_FIRST;
			}
		}
		else
		{
			Data.PreviousSeries = Data.CurrentSeries;
		}
		Data.CurrentSeries = Def.SERIES.SM_ADV;
		Data.CurrentEventID = -1;
	}

	public void SetNextSeries(Def.SERIES a_series)
	{
		Data.CurrentSeries = a_series;
		Data.CurrentEventID = -1;
		Data.PreviousSeries = Def.SERIES.SM_FIRST;
	}

	public void CheckCourseClear(Def.SERIES a_series, int a_eventID, short a_courseID, List<int> stages)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data == null || data.CourseClearFlag.ContainsKey(a_courseID))
		{
			return;
		}
		foreach (int stage in stages)
		{
			PlayerClearData _psd;
			if (!GetStageClearData(a_series, a_eventID, a_courseID, stage, out _psd))
			{
				return;
			}
		}
		ShowSignAnimation = true;
		ShowClearAnimation = true;
		Data.SetMapData(a_eventID, data);
	}

	public int CheckCourseComplete(Def.SERIES a_series, int a_eventID, short a_courseID, List<int> stages)
	{
		int num = 0;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			if (data.CourseCompleteFlag.ContainsKey(a_courseID))
			{
				return num;
			}
			bool flag = true;
			foreach (int stage in stages)
			{
				PlayerClearData _psd;
				if (GetStageClearData(a_series, a_eventID, a_courseID, stage, out _psd))
				{
					num += _psd.GotStars;
					if (_psd.GotStars < 3)
					{
						flag = false;
					}
				}
				else
				{
					flag = false;
				}
			}
			if (flag)
			{
				ShowCompleteAnimation = true;
			}
			Data.SetMapData(a_eventID, data);
		}
		return num;
	}

	public int GetEventGotStars(Def.SERIES a_series, int a_eventID)
	{
		int num = 0;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			foreach (short key in data.CourseData.Keys)
			{
				num += GetEventGotStars(a_series, a_eventID, key);
			}
		}
		return num;
	}

	public int GetEventGotStars(Def.SERIES a_series, int a_eventID, short a_courseID)
	{
		int num = 0;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null && data.CourseData.ContainsKey(a_courseID))
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			foreach (PlayerClearData stageClearDatum in playerMapData.StageClearData)
			{
				num += stageClearDatum.GotStars;
			}
		}
		return num;
	}

	public int GetEventStageClearCount(Def.SERIES a_series, int a_eventID)
	{
		int num = 0;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			foreach (short key in data.CourseData.Keys)
			{
				num += GetEventStageClearCount(a_series, a_eventID, key);
			}
		}
		return num;
	}

	public int GetEventStageClearCount(Def.SERIES a_series, int a_eventID, short a_courseID)
	{
		int num = 0;
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null && data.CourseData.ContainsKey(a_courseID))
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			foreach (PlayerClearData stageClearDatum in playerMapData.StageClearData)
			{
				num++;
			}
		}
		return num;
	}

	public void UnlockEventRoadBlock(Def.SERIES a_series, int a_eventID, short a_courseID, int a_rbID)
	{
		PlayerEventData data;
		Data.GetMapData(a_series, a_eventID, out data);
		if (data != null)
		{
			PlayerMapData playerMapData = data.CourseData[a_courseID];
			if (playerMapData != null)
			{
				playerMapData.UpdateUnlockedRoadBlock(a_rbID);
			}
			Data.SetMapData(a_eventID, data);
		}
	}

	public int GetContinueOfferOldEventID()
	{
		PlayerOldEventData playerOldEventData = null;
		DateTime dateTime = DateTimeUtil.Now();
		foreach (KeyValuePair<short, PlayerOldEventData> item in Data.OldEventDataDict)
		{
			if (!(item.Value.CloseTime > dateTime) && !item.Value.ExtendedOfferDone)
			{
				if (playerOldEventData == null)
				{
					playerOldEventData = item.Value;
				}
				else if (playerOldEventData.CloseTime > item.Value.CloseTime)
				{
					playerOldEventData = item.Value;
				}
			}
		}
		if (playerOldEventData == null)
		{
			return -1;
		}
		return playerOldEventData.EventID;
	}

	public void AddAdvStamina(int num)
	{
		AdvSaveData.mRestStamina += num;
	}

	public bool SubAdvStamina(int num)
	{
		if (AdvStamina < num)
		{
			return false;
		}
		if (AdvStamina >= AdvMaxStamina && AdvStamina - num < AdvMaxStamina)
		{
			AdvSaveData.mOldestAdvStaminaUseTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		}
		AdvSaveData.mRestStamina -= num;
		return true;
	}

	public void UpdateAdvStaminaCount()
	{
		if (AdvSaveData == null || AdvMaxStamina <= AdvStamina || !GameMain.IsMedalRecover)
		{
			return;
		}
		int num = 0;
		if (SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile != null && SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.AdvRecSettings != null)
		{
			bool flag = true;
			flag = false;
			DateTime dateTime = DateTimeUtil.UnixTimeStampToDateTime(SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.AdvRecSettings.StartTime, flag);
			DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.AdvRecSettings.EndTime, flag);
			if (dateTime <= DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0) && DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0) < dateTime2 && SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.AdvRecSettings.AdvRecoverSpSec > 0 && SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.AdvRecSettings.AdvRecoverSpSec < Constants.ADV_STAMINA_RECOVER_SEC)
			{
				num = SingletonMonoBehaviour<GameMain>.Instance.SegmentProfile.AdvRecSettings.AdvRecoverSpSec;
			}
		}
		if (nowAdvMaxStamina < AdvMaxStamina)
		{
			if (nowAdvMaxStamina == AdvStamina)
			{
				AdvSaveData.mOldestAdvStaminaUseTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
			}
			nowAdvMaxStamina = AdvMaxStamina;
		}
		DateTime dateTime3 = AdvSaveData.mOldestAdvStaminaUseTime.AddSeconds(Constants.ADV_STAMINA_RECOVER_SEC - num);
		while (DateTimeUtil.BetweenSeconds(DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0), dateTime3) <= 0)
		{
			if (AdvStamina < AdvMaxStamina)
			{
				AddAdvStamina(1);
				AdvSaveData.mOldestAdvStaminaUseTime = dateTime3;
			}
			else
			{
				AdvSaveData.mOldestAdvStaminaUseTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0);
			}
			dateTime3 = AdvSaveData.mOldestAdvStaminaUseTime.AddSeconds(Constants.ADV_STAMINA_RECOVER_SEC - num);
		}
	}
}
