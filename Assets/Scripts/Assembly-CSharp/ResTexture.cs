using UnityEngine;

public class ResTexture : ResourceInstance
{
	public Texture2D Tex;

	public bool isExist;

	public ResTexture(string name)
		: base(name)
	{
		ResourceType = TYPE.TEXTURE;
		Tex = null;
		isExist = false;
	}
}
