using System.Collections.Generic;

public class CampaignCheckResponse : ParameterObject<CampaignCheckResponse>
{
	public List<CampaignData> iap_campaign_data { get; set; }

	public long next_iap_campaign_starttime { get; set; }

	public long server_time { get; set; }

	public int code { get; set; }
}
