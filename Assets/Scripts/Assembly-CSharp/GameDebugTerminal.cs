using System;
using System.IO;
using System.Text;

public class GameDebugTerminal
{
	private enum STATE
	{
		NONE = 0,
		WAIT = 1,
		NETWORK_PROLOGUE = 2,
		NETWORK_01 = 3,
		NETWORK_EPILOGUE = 4
	}

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.NONE);

	private bool mSuccess;

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
	}

	private static void dbg(string msg)
	{
	}

	public bool SuccessFlg()
	{
		return mSuccess;
	}

	public bool Start()
	{
		if (mState.GetStatus() != 0)
		{
			return false;
		}
		mState.Reset(STATE.NETWORK_PROLOGUE, true);
		return true;
	}

	public bool IsFinished()
	{
		return mState.GetStatus() == STATE.NONE;
	}

	public void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_PROLOGUE:
			Server.DebugTerminalRegistration += Server_DebugTerminal;
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			Network_SyncTerminal();
			break;
		case STATE.NETWORK_EPILOGUE:
			Server.DebugTerminalRegistration -= Server_DebugTerminal;
			mState.Change(STATE.NONE);
			break;
		}
		mState.Update();
	}

	private void Network_SyncTerminal()
	{
		mState.Change(STATE.WAIT);
		STATE aStatus = STATE.NETWORK_EPILOGUE;
		if (!Server.DebugTerminal())
		{
			log("Network_OnSyncGetFriends: Server.GetFriends failed to request. skip.");
			mState.Change(aStatus);
		}
	}

	private void Server_DebugTerminal(int result, int code, Stream resStream)
	{
		log("Server_OnGetFriends: " + GameMain.Server_Result(result));
		if (result == 0)
		{
			try
			{
				using (StreamReader streamReader = new StreamReader(resStream, Encoding.UTF8))
				{
					string text = streamReader.ReadToEnd();
				}
			}
			catch (Exception)
			{
			}
			mSuccess = true;
			mState.Change(STATE.NETWORK_EPILOGUE);
		}
		else
		{
			mSuccess = false;
			mState.Change(STATE.NETWORK_EPILOGUE);
		}
	}
}
