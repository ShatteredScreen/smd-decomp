using System;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/DailyChallenge/Reward List Setting")]
public class DailyChallengeRewardListSetting : ScriptableObject
{
	[Serializable]
	private sealed class RewardInfo
	{
		[SerializeField]
		private int mID;

		[SerializeField]
		private int mAccessoryID;

		public int ID
		{
			get
			{
				return mID;
			}
			set
			{
				mID = value;
			}
		}

		public int AccessoryID
		{
			get
			{
				return mAccessoryID;
			}
			set
			{
				mAccessoryID = value;
			}
		}
	}

	[SerializeField]
	private RewardInfo[] mData;

	public int[] IDs
	{
		get
		{
			return (mData == null) ? null : mData.Select((RewardInfo n) => n.ID).ToArray();
		}
	}

	public int Count
	{
		get
		{
			return (mData != null) ? mData.Length : 0;
		}
	}

	public void Set(DailyChallengeRewardData data)
	{
		if (data != null && data.AccessoryID.HasValue)
		{
			if (mData == null)
			{
				mData = new RewardInfo[0];
			}
			if (!mData.Any((RewardInfo n) => n.ID == data.ID))
			{
				Array.Resize(ref mData, mData.Length + 1);
			}
			mData[mData.Length - 1] = new RewardInfo
			{
				ID = data.ID,
				AccessoryID = data.AccessoryID.Value
			};
			mData = mData.OrderBy((RewardInfo n) => n.ID).ToArray();
		}
	}

	public int IndexOf(int accessory_id)
	{
		if (mData == null)
		{
			return -1;
		}
		for (int i = 0; i < mData.Length; i++)
		{
			if (mData[i] != null && mData[i].AccessoryID == accessory_id)
			{
				return i;
			}
		}
		return -1;
	}

	public int GetAccessoryID(int id)
	{
		if (mData != null)
		{
			RewardInfo[] array = mData;
			foreach (RewardInfo rewardInfo in array)
			{
				if (rewardInfo != null && rewardInfo.ID == id)
				{
					return rewardInfo.AccessoryID;
				}
			}
		}
		return -1;
	}
}
