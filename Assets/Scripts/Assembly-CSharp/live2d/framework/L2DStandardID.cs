namespace live2d.framework
{
	public class L2DStandardID
	{
		public const string HIT_AREA_PRE_FIX = "D_REF";

		public const string HIT_AREA_HEAD = "D_REF_HEAD";

		public const string HIT_AREA_BODY = "D_REF_BODY";

		public const string PARTS_ID_CORE = "PARTS_01_CORE";

		public const string PARTS_ARM_PREFIX = "PARTS_01_ARM_";

		public const string PARTS_ARM_L_PREFIX = "PARTS_01_ARM_L_";

		public const string PARTS_ARM_R_PREFIX = "PARTS_01_ARM_R_";

		public const string PARAM_ANGLE_X = "PARAM_ANGLE_X";

		public const string PARAM_ANGLE_Y = "PARAM_ANGLE_Y";

		public const string PARAM_ANGLE_Z = "PARAM_ANGLE_Z";

		public const string PARAM_EYE_L_OPEN = "PARAM_EYE_L_OPEN";

		public const string PARAM_EYE_R_OPEN = "PARAM_EYE_R_OPEN";

		public const string PARAM_EYE_L_SMILE = "PARAM_EYE_L_SMILE";

		public const string PARAM_EYE_R_SMILE = "PARAM_EYE_R_SMILE";

		public const string PARAM_EYE_BALL_X = "PARAM_EYE_BALL_X";

		public const string PARAM_EYE_BALL_Y = "PARAM_EYE_BALL_Y";

		public const string PARAM_EYE_BALL_FORM = "PARAM_EYE_BALL_FORM";

		public const string PARAM_BROW_L_X = "PARAM_BROW_L_X";

		public const string PARAM_BROW_L_Y = "PARAM_BROW_L_Y";

		public const string PARAM_BROW_L_ANGLE = "PARAM_BROW_L_ANGLE";

		public const string PARAM_BROW_L_FORM = "PARAM_BROW_L_FORM";

		public const string PARAM_BROW_R_X = "PARAM_BROW_R_X";

		public const string PARAM_BROW_R_Y = "PARAM_BROW_R_Y";

		public const string PARAM_BROW_R_ANGLE = "PARAM_BROW_R_ANGLE";

		public const string PARAM_BROW_R_FORM = "PARAM_BROW_R_FORM";

		public const string PARAM_MOUTH_OPEN_Y = "PARAM_MOUTH_OPEN_Y";

		public const string PARAM_MOUTH_FORM = "PARAM_MOUTH_FORM";

		public const string PARAM_SMILE = "PARAM_SMILE";

		public const string PARAM_TERE = "PARAM_TERE";

		public const string PARAM_BODY_ANGLE_X = "PARAM_BODY_ANGLE_X";

		public const string PARAM_BODY_ANGLE_Z = "PARAM_BODY_ANGLE_Z";

		public const string PARAM_BREATH = "PARAM_BREATH";

		public const string PARAM_HAIR_FRONT = "PARAM_HAIR_FRONT";

		public const string PARAM_HAIR_SIDE = "PARAM_HAIR_SIDE";

		public const string PARAM_HAIR_BACK = "PARAM_HAIR_BACK";

		public const string PARAM_HAIR_FUWA = "PARAM_HAIR_FUWA";

		public const string PARAM_SHOULDER_X = "PARAM_SHOULDER_X";

		public const string PARAM_BUST_X = "PARAM_BUST_X";

		public const string PARAM_BUST_Y = "PARAM_BUST_Y";

		public const string PARAM_BASE_X = "PARAM_BASE_X";

		public const string PARAM_BASE_Y = "PARAM_BASE_Y";

		public const string PARAM_NONE = "NONE:";
	}
}
