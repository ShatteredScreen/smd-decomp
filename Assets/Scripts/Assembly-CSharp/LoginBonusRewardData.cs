using System;

public class LoginBonusRewardData : ICloneable
{
	public bool IsReceived
	{
		get
		{
			return IsReceivedStatus == 1;
		}
	}

	public string GetLoginBonusIcon
	{
		get
		{
			return SMDImageUtil.GetLoginBonusIconKey(Category, SubCategory, Quantity);
		}
	}

	[MiniJSONAlias("reward_id")]
	public int RewardID { get; set; }

	[MiniJSONAlias("bonus_id")]
	public int BonusID { get; set; }

	[MiniJSONAlias("category")]
	public int Category { get; set; }

	[MiniJSONAlias("sub_category")]
	public int SubCategory { get; set; }

	[MiniJSONAlias("quantity")]
	public int Quantity { get; set; }

	[MiniJSONAlias("is_received")]
	public int IsReceivedStatus { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public LoginBonusRewardData Clone()
	{
		return MemberwiseClone() as LoginBonusRewardData;
	}
}
