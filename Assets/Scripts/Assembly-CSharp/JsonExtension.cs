using System;
using System.Collections.Generic;
using System.IO;

public static class JsonExtension
{
	public static readonly Dictionary<Type, IMiniJSONConverter> DEFAULT_MAPPER;

	static JsonExtension()
	{
		DEFAULT_MAPPER = new Dictionary<Type, IMiniJSONConverter>();
		DEFAULT_MAPPER[typeof(SMGridData)] = new GridDataConverter();
		DEFAULT_MAPPER[typeof(DateTime)] = new DateTimeConverter();
		DEFAULT_MAPPER[typeof(PlayerData)] = new SMJsonDataConverter<PlayerData>();
		DEFAULT_MAPPER[typeof(PlayerMapData)] = new SMJsonDataConverter<PlayerMapData>();
		DEFAULT_MAPPER[typeof(MyFriendData)] = new SMJsonDataConverter<MyFriendData>();
		DEFAULT_MAPPER[typeof(GiftItemData)] = new SMJsonDataConverter<GiftItemData>();
		DEFAULT_MAPPER[typeof(PlayerClearData)] = new SMJsonDataConverter<PlayerClearData>();
		DEFAULT_MAPPER[typeof(List<PlayerData>)] = new SMPlayerDataListConverter();
		DEFAULT_MAPPER[typeof(List<MyFriendData>)] = new SMMyFriendDataListConverter();
		DEFAULT_MAPPER[typeof(List<GiftItemData>)] = new SMGiftItemDataListConverter();
		DEFAULT_MAPPER[typeof(List<PlayerClearData>)] = new SMPlayerStageDataListConverter();
	}

	public static void LoadFromJson(this SMStageDataImpl data, string json, int reqVersion)
	{
		try
		{
			new MiniJSONSerializer(DEFAULT_MAPPER).Populate(ref data, json);
		}
		catch (Exception)
		{
		}
		int formatVersion = data.FormatVersion;
		data.OnLoaded();
	}

	public static void LoadFromJsonFile(this SMStageDataImpl data, string path, int reqVersion)
	{
		using (StreamReader streamReader = new StreamReader(path))
		{
			string json = streamReader.ReadToEnd();
			data.LoadFromJson(json, reqVersion);
			streamReader.Close();
		}
	}

	public static void LoadFromJson(this Player p, string json, int reqVersion)
	{
		try
		{
			new MiniJSONSerializer(DEFAULT_MAPPER).Populate(ref p.Data, json);
		}
		catch (Exception ex)
		{
			BIJLog.E("Error Load From Json:" + ex.Message);
		}
	}

	public static void LoadFromJsonFile(this Player p, string path, int reqVersion)
	{
		using (StreamReader streamReader = new StreamReader(path))
		{
			string json = streamReader.ReadToEnd();
			p.LoadFromJson(json, reqVersion);
			streamReader.Close();
		}
	}

	public static void SaveToJson(this SMStageDataImpl data, out string json, int reqVersion)
	{
		data.FormatVersion = reqVersion;
		json = new MiniJSONSerializer(DEFAULT_MAPPER).Serialize(data);
		data.OnSaved();
	}

	public static void SaveToJsonFile(this SMStageDataImpl data, string path, int reqVersion)
	{
		using (StreamWriter streamWriter = new StreamWriter(path))
		{
			string json = null;
			data.SaveToJson(out json, reqVersion);
			streamWriter.Write(json);
			streamWriter.Flush();
			streamWriter.Close();
		}
	}

	public static void SaveToJson(this Player p, out string json, int reqVersion)
	{
		p.Data.Version = (short)reqVersion;
		json = new MiniJSONSerializer(DEFAULT_MAPPER).Serialize(p.Data);
	}

	public static void SaveToJsonFile(this Player p, string path, int reqVersion)
	{
		using (StreamWriter streamWriter = new StreamWriter(path))
		{
			string json = null;
			p.SaveToJson(out json, reqVersion);
			streamWriter.Write(json);
			streamWriter.Flush();
			streamWriter.Close();
		}
	}
}
