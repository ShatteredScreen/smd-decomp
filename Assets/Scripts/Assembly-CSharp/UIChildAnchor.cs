using UnityEngine;

public class UIChildAnchor : MonoBehaviour
{
	private GameMain mGame;

	private UIAnchor.Side mSide;

	private ScreenOrientation mScreenDir;

	public static float PORTRAIT_SAFESIZE_TOP = 74.995f;

	public static float PORTRAIT_SAFESIZE_BOTTOM = 57.95f;

	public static float PORTRAIT_SAFESIZE_MARGIN = 27.271f;

	public static float LANDSCAPE_SAFESIZE_SIDE = 74.995f;

	public static float LANDSCAPE_SAFESIZE_BOTTOM = 35.793f;

	public static float LANDSCAPE_SAFESIZE_MARGIN = 34.089f;

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mSide = UIAnchor.Side.Center;
		GameObject gameObject = base.gameObject.transform.parent.gameObject;
		UIAnchor component = gameObject.GetComponent<UIAnchor>();
		if (component != null)
		{
			mSide = component.side;
		}
		UpdatePos(true);
	}

	public void OnDestroy()
	{
		if (base.transform.parent.gameObject != null)
		{
			Object.Destroy(base.transform.parent.gameObject);
		}
	}

	private void Update()
	{
		UpdatePos(false);
	}

	private void UpdatePos(bool force)
	{
		if (mScreenDir != Util.ScreenOrientation || force)
		{
			mScreenDir = Util.ScreenOrientation;
			Vector2 vector = CalculateOffset(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, mSide, Util.ScreenOrientation);
			base.gameObject.transform.localPosition = new Vector3(vector.x, vector.y, 0f);
		}
	}

	private static Vector2 CalculateOffset(int manualHeight, int manualWidth, UIAnchor.Side side, ScreenOrientation o)
	{
		float x = 0f;
		float y = 0f;
		float num = 248f;
		float num2 = 0f;
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			float num4 = (float)manualHeight - 1136f;
			num2 = num4 / num;
			switch (side)
			{
			case UIAnchor.Side.Bottom:
				y = PORTRAIT_SAFESIZE_BOTTOM * num2;
				break;
			case UIAnchor.Side.BottomLeft:
				y = PORTRAIT_SAFESIZE_BOTTOM * num2;
				x = PORTRAIT_SAFESIZE_MARGIN * num2;
				break;
			case UIAnchor.Side.BottomRight:
				y = PORTRAIT_SAFESIZE_BOTTOM * num2;
				x = PORTRAIT_SAFESIZE_MARGIN * num2 * -1f;
				break;
			case UIAnchor.Side.Left:
				x = PORTRAIT_SAFESIZE_MARGIN * num2;
				break;
			case UIAnchor.Side.Right:
				x = PORTRAIT_SAFESIZE_MARGIN * num2 * -1f;
				break;
			case UIAnchor.Side.Top:
				y = PORTRAIT_SAFESIZE_TOP * num2 * -1f;
				break;
			case UIAnchor.Side.TopLeft:
				y = PORTRAIT_SAFESIZE_TOP * num2 * -1f;
				x = PORTRAIT_SAFESIZE_MARGIN * num2;
				break;
			case UIAnchor.Side.TopRight:
				y = PORTRAIT_SAFESIZE_TOP * num2 * -1f;
				x = PORTRAIT_SAFESIZE_MARGIN * num2 * -1f;
				break;
			}
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			float num3 = (float)manualWidth - 1136f;
			num2 = num3 / num;
			switch (side)
			{
			case UIAnchor.Side.Bottom:
				y = LANDSCAPE_SAFESIZE_BOTTOM * num2;
				break;
			case UIAnchor.Side.BottomLeft:
				y = LANDSCAPE_SAFESIZE_BOTTOM * num2;
				x = (LANDSCAPE_SAFESIZE_SIDE + LANDSCAPE_SAFESIZE_MARGIN) * num2;
				break;
			case UIAnchor.Side.BottomRight:
				y = LANDSCAPE_SAFESIZE_BOTTOM * num2;
				x = (LANDSCAPE_SAFESIZE_SIDE + LANDSCAPE_SAFESIZE_MARGIN) * num2 * -1f;
				break;
			case UIAnchor.Side.Left:
				x = (LANDSCAPE_SAFESIZE_SIDE + LANDSCAPE_SAFESIZE_MARGIN) * num2;
				break;
			case UIAnchor.Side.Right:
				x = (LANDSCAPE_SAFESIZE_SIDE + LANDSCAPE_SAFESIZE_MARGIN) * num2 * -1f;
				break;
			case UIAnchor.Side.TopLeft:
				x = (LANDSCAPE_SAFESIZE_SIDE + LANDSCAPE_SAFESIZE_MARGIN) * num2;
				break;
			case UIAnchor.Side.TopRight:
				x = (LANDSCAPE_SAFESIZE_SIDE + LANDSCAPE_SAFESIZE_MARGIN) * num2 * -1f;
				break;
			}
			break;
		}
		}
		return new Vector2(x, y);
	}

	public static Vector2 CalculateScreenPos(int manualHeight, int manualWidth, UIAnchor.Side side, ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize() / 2f;
		float x = 0f;
		float y = 0f;
		switch (side)
		{
		case UIAnchor.Side.Bottom:
			y = 0f - vector.y;
			break;
		case UIAnchor.Side.BottomLeft:
			x = 0f - vector.x;
			y = 0f - vector.y;
			break;
		case UIAnchor.Side.BottomRight:
			x = vector.x;
			y = 0f - vector.y;
			break;
		case UIAnchor.Side.Left:
			x = 0f - vector.x;
			break;
		case UIAnchor.Side.Right:
			x = vector.x;
			break;
		case UIAnchor.Side.Top:
			y = vector.y;
			break;
		case UIAnchor.Side.TopLeft:
			x = 0f - vector.x;
			y = vector.y;
			break;
		case UIAnchor.Side.TopRight:
			x = vector.x;
			y = vector.y;
			break;
		}
		Vector2 vector2 = new Vector2(x, y) + CalculateOffset(manualHeight, manualWidth, side, o);
		return new Vector2(vector2.x / vector.x, vector2.y / vector.y);
	}

	public static Vector2 CalculateLocalPos(int manualHeight, int manualWidth, UIAnchor.Side side, ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize() / 2f;
		Vector2 vector2 = CalculateScreenPos(manualHeight, manualWidth, side, o);
		return new Vector2(vector2.x * vector.x, vector2.y * vector.y);
	}
}
