public interface ITierData
{
	int ItemNo { get; }

	string ItemID { get; }

	int Amount { get; }

	double Cost { get; }

	string CurrencyCode { get; }

	string FormattedPrice { get; }
}
