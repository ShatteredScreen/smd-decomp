using System;
using System.Collections.Generic;

public class AdvSetRewardProfile_Response : ICloneable
{
	[MiniJSONAlias("receive_rewards")]
	public List<AdvRewardData> RewardDataList { get; set; }

	[MiniJSONAlias("mailbox_rewards")]
	public List<AdvRewardData> MailRewardDataList { get; set; }

	[MiniJSONAlias("received_rewards")]
	public List<AdvRewardData> Received_List { get; set; }

	[MiniJSONAlias("figure_exchange")]
	public List<AdvFigureExchange> FigureExchangeList { get; set; }

	[MiniJSONAlias("figures")]
	public List<AdvFigureData> FigureDataList { get; set; }

	[MiniJSONAlias("items")]
	public List<AdvItemData> ItemDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvSetRewardProfile_Response Clone()
	{
		return MemberwiseClone() as AdvSetRewardProfile_Response;
	}
}
