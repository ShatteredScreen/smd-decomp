using System;
using System.Collections.Generic;

public class PurchaseLog : ICloneable
{
	public class PLog
	{
		public enum State
		{
			NONE = 0,
			PURCHASED = 100,
			COMPLETED = 200,
			UNKNOWN = 999
		}

		public string itemID;

		public string orderID;

		public double purchaseTime;

		public string signature;

		public string receipt;

		public int purchaseState;

		public override string ToString()
		{
			return string.Format("[PLog<{0}> itemID={1} orderID={2} purchaseTime={3} purchaseState={4} signature={5} receipt={6}]", ToStateString(purchaseState), itemID, orderID, purchaseTime, purchaseState, signature, receipt);
		}

		private static string ToStateString(int _purchaseState)
		{
			switch (_purchaseState)
			{
			case 0:
				return "NONE";
			case 100:
				return "PURCHASED";
			case 200:
				return "COMPLETED";
			case 999:
				return "UNKNOWN";
			default:
				return "Unknown";
			}
		}
	}

	public IDictionary<string, PLog> Logs { get; private set; }

	protected PurchaseLog()
	{
		Logs = new Dictionary<string, PLog>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public PurchaseLog Clone()
	{
		return MemberwiseClone() as PurchaseLog;
	}

	public static PurchaseLog CreateForNewPurchaseLog()
	{
		return new PurchaseLog();
	}

	public void Serialize(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		PLog[] array = new PLog[Logs.Count];
		Logs.Values.CopyTo(array, 0);
		data.WriteInt(array.Length);
		foreach (PLog pLog in array)
		{
			data.WriteUTF(pLog.itemID);
			data.WriteUTF(pLog.orderID);
			data.WriteDouble(pLog.purchaseTime);
			data.WriteUTF(pLog.signature);
			data.WriteInt(pLog.purchaseState);
			data.WriteUTF(pLog.receipt);
		}
	}

	public static PurchaseLog Deserialize(BIJBinaryReader data)
	{
		short num = data.ReadShort();
		PurchaseLog purchaseLog = new PurchaseLog();
		int num2 = data.ReadInt();
		for (int i = 0; i < num2; i++)
		{
			PLog pLog = new PLog();
			pLog.itemID = data.ReadUTF();
			pLog.orderID = data.ReadUTF();
			pLog.purchaseTime = data.ReadDouble();
			pLog.signature = data.ReadUTF();
			pLog.purchaseState = data.ReadInt();
			pLog.receipt = data.ReadUTF();
			if (!string.IsNullOrEmpty(pLog.orderID))
			{
				purchaseLog.Logs[pLog.orderID] = pLog;
			}
		}
		return purchaseLog;
	}

	public bool HasPLog()
	{
		return Logs.Count > 0;
	}

	public PLog GetLastOnePLog()
	{
		List<string> list = new List<string>(Logs.Keys);
		if (list.Count == 0)
		{
			return null;
		}
		return Logs[list[0]];
	}

	public void PurchaseComplete()
	{
		Logs.Clear();
	}

	public void SetLastOnePLogStatus(PLog.State state)
	{
		List<string> list = new List<string>(Logs.Keys);
		if (list.Count != 0)
		{
			Logs[list[0]].purchaseState = (int)state;
		}
	}
}
