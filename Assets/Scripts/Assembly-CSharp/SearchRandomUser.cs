public class SearchRandomUser : ParameterObject<SearchRandomUser>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int Stage { get; set; }

	[MiniJSONAlias("range")]
	public int Range { get; set; }

	[MiniJSONAlias("cnt")]
	public int Count { get; set; }
}
