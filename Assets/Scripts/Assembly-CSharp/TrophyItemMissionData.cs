using UnityEngine;

public class TrophyItemMissionData
{
	public GameObject itemGo;

	public GameObject boardImage;

	public TrophyItemMissionListItem data;

	public SMVerticalListItem item;

	public SMVerticalListItem lastItem;
}
