using System.Collections;
using System.Text;
using UnityEngine;

public class EULADialog : DialogBase
{
	public enum STATE
	{
		OPEN = 0,
		CLOSE = 1,
		WAIT = 2
	}

	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum WEBVIEW_STATE
	{
		NONE = 0,
		PROGRESS = 1,
		ERROR = 2
	}

	public enum MODE
	{
		EULA_ACCEPT = 0,
		EULA = 1,
		AS = 2,
		ASCT = 3,
		PRIVACY = 4
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private const string CB_Webview_onError = "Webview_onError";

	private const string CB_Webview_onClose = "Webview_onClose";

	private const string CB_Webview_onPageStarted = "Webview_onPageStarted";

	private const string CB_Webview_onPageFinished = "Webview_onPageFinished";

	private const string CB_Webview_onProgress = "Webview_onProgress";

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.OPEN);

	private SELECT_ITEM mSelectItem;

	private WEBVIEW_STATE mWebViewState;

	private MODE mMode;

	private string mURL;

	private WebViewObject mWebViewObject;

	private bool mLoadURLCall;

	private string mLoadURL;

	private string mFallbackURL;

	private Vector2 mWebViewOrign;

	private Vector2 mWebViewSize;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.OPEN:
			if (!GetBusy())
			{
				LoadURL(mURL, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.WAIT:
			if (mMode != 0 && mGame.mBackKeyTrg)
			{
				OnBackKeyPress();
			}
			break;
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		if (mMode == MODE.EULA_ACCEPT)
		{
			ResImage resTitleImage = ResourceManager.LoadImageAsync("TITLE");
			while (resTitleImage.LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
	}

	public override void BuildDialog()
	{
		StringBuilder stringBuilder = new StringBuilder();
		string key = string.Empty;
		switch (mMode)
		{
		case MODE.EULA_ACCEPT:
			stringBuilder.Append(mGame.GameProfile.ToSTopURL);
			key = "Rules_Title00";
			break;
		case MODE.EULA:
			stringBuilder.Append(mGame.GameProfile.ToSURL);
			key = "Rules_Title00";
			break;
		case MODE.AS:
			stringBuilder.Append(mGame.GameProfile.AoSURL);
			key = "Rules_Title01";
			break;
		case MODE.ASCT:
			stringBuilder.Append(mGame.GameProfile.AoSCTURL);
			key = "Rules_Title02";
			break;
		case MODE.PRIVACY:
			stringBuilder.Append(mGame.GameProfile.PrivacyURL);
			key = "Rules_Title03_En";
			break;
		}
		mURL = stringBuilder.ToString();
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(Localization.Get(key));
		if (mMode == MODE.EULA_ACCEPT)
		{
			BIJImage image = ResourceManager.LoadImage("TITLE").Image;
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_consent", "LC_button_consent", "LC_button_consent", mBaseDepth + 4, new Vector3(-130f, -230f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
			Util.SetImageButtonInfo(button, "LC_button_disagree", "LC_button_disagree", "LC_button_disagree", mBaseDepth + 4, new Vector3(130f, -230f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			mWebViewOrign = new Vector2(0f, 25f) + DialogBase.BUILD_DIALOG_POSITION_OFFSET;
			mWebViewSize = new Vector2(530f * JellyDialog.DIALOG_SCALE, 410f * JellyDialog.DIALOG_SCALE);
		}
		else
		{
			mWebViewOrign = new Vector2(0f, -20f) + DialogBase.BUILD_DIALOG_POSITION_OFFSET;
			mWebViewSize = new Vector2(530f * JellyDialog.DIALOG_SCALE, 500f * JellyDialog.DIALOG_SCALE);
			CreateCancelButton("OnNegativePushed");
		}
		GameObject gameObject = Util.CreateGameObject("WebView", base.gameObject);
		mWebViewObject = gameObject.AddComponent<WebViewObject>();
		mWebViewObject.Init(OnWebViewObjectCallback);
		mWebViewObject.SetVisibility(false);
		UpdateWebViewMargin();
	}

	public void Init(MODE mode)
	{
		mMode = mode;
	}

	private void OnWebViewObjectCallback(string message)
	{
		if (string.Compare("Webview_onPageStarted", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.PROGRESS;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
			}
		}
		else if (string.Compare("Webview_onPageFinished", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS || mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.NONE;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(true);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onProgress", message) == 0)
		{
			if (mWebViewState != WEBVIEW_STATE.PROGRESS)
			{
			}
		}
		else if (string.Compare("Webview_onError", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS)
			{
				mWebViewState = WEBVIEW_STATE.ERROR;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onClose", message) == 0 && mWebViewState == WEBVIEW_STATE.NONE)
		{
			Close();
		}
	}

	private void UpdateWebViewMargin()
	{
		Vector2 vector = Util.LogScreenSize();
		float num = 1f;
		num = mJelly.GetDefaultScale();
		Vector2 vector2 = new Vector2(vector.x - mWebViewSize.x * num, vector.y - mWebViewSize.y * num);
		int left = (int)(Util.ToDevX(vector2.x / 2f) + Util.ToDevX(mWebViewOrign.x));
		int top = (int)(Util.ToDevY(vector2.y / 2f) - Util.ToDevY(mWebViewOrign.y));
		int right = (int)(Util.ToDevX(vector2.x / 2f) - Util.ToDevX(mWebViewOrign.x));
		int bottom = (int)(Util.ToDevY(vector2.y / 2f) + Util.ToDevY(mWebViewOrign.y));
		if (mWebViewObject != null)
		{
			mWebViewObject.SetMargins(left, top, right, bottom);
		}
	}

	private void LoadURL(string url, string fallback)
	{
		if (mWebViewObject != null)
		{
			mWebViewObject.SetExternalSpawnMode(0);
			mLoadURLCall = true;
			mWebViewState = WEBVIEW_STATE.NONE;
			mLoadURL = url;
			mFallbackURL = fallback;
			mWebViewObject.LoadURL(url);
		}
	}

	public override void OnOpenFinished()
	{
		UpdateWebViewMargin();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			Close();
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		UpdateWebViewMargin();
	}

	public void SetCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
