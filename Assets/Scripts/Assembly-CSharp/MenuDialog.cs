using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuDialog : DialogBase
{
	public enum SELECT_MENU
	{
		SERIES = 0,
		TROPHY = 1,
		COLLECTION = 2,
		MYPAGE = 3,
		RANKING = 4,
		GROWUP = 5,
		NOTICE = 6,
		OPTION = 7,
		TITLE = 8,
		CANCEL = 9
	}

	public delegate void OnDialogClosed(SELECT_MENU i);

	private SELECT_MENU mSelectItem;

	private Dictionary<SELECT_MENU, JellyImageButton> mButtons = new Dictionary<SELECT_MENU, JellyImageButton>();

	private string mTitle;

	private string mDescription;

	private OnDialogClosed mCallback;

	private UISprite mCheatTimeBG;

	public override void Start()
	{
		base.Start();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("DIALOG_BASE").Image;
		InitDialogFrame(DIALOG_SIZE.LARGE, "DIALOG_BASE", "menu_instruction_panel");
		InitDialogTitle(mTitle);
		CreateMenuButton(SELECT_MENU.SERIES, base.gameObject, mBaseDepth + 4, new Vector3(-183f, 153f, 0f), "button_mapmove");
		CreateMenuButton(SELECT_MENU.TROPHY, base.gameObject, mBaseDepth + 4, new Vector3(0f, 153f, 0f), "button_trophy");
		CreateMenuButton(SELECT_MENU.COLLECTION, base.gameObject, mBaseDepth + 4, new Vector3(183f, 153f, 0f), "button_collection");
		CreateMenuButton(SELECT_MENU.MYPAGE, base.gameObject, mBaseDepth + 4, new Vector3(-165f, -75f, 0f), "button_friend");
		CreateMenuButton(SELECT_MENU.RANKING, base.gameObject, mBaseDepth + 4, new Vector3(0f, -75f, 0f), "button_ranking");
		CreateMenuButton(SELECT_MENU.GROWUP, base.gameObject, mBaseDepth + 4, new Vector3(165f, -75f, 0f), "button_LvUP");
		CreateMenuButton(SELECT_MENU.NOTICE, base.gameObject, mBaseDepth + 4, new Vector3(-165f, -210f, 0f), "button_information");
		CreateMenuButton(SELECT_MENU.OPTION, base.gameObject, mBaseDepth + 4, new Vector3(0f, -210f, 0f), "button_option");
		CreateMenuButton(SELECT_MENU.TITLE, base.gameObject, mBaseDepth + 4, new Vector3(165f, -210f, 0f), "button_exit02");
		UISprite sprite = Util.CreateSprite("Line", base.gameObject, image2);
		Util.SetSpriteInfo(sprite, "menu_line", mBaseDepth + 3, new Vector3(0f, 24f, 0f), Vector3.one, false);
		Attention attention = Util.CreateGameObject("Attention", mButtons[SELECT_MENU.TROPHY].gameObject).AddComponent<Attention>();
		attention.Init(1f, mBaseDepth + 5, new Vector3(40f, -50f, 0f), image, "icon_exclamation");
		attention.SetAttentionEnable(IsAttentionEnable(SELECT_MENU.TROPHY));
		attention = Util.CreateGameObject("Attention", mButtons[SELECT_MENU.COLLECTION].gameObject).AddComponent<Attention>();
		attention.Init(1f, mBaseDepth + 5, new Vector3(40f, -50f, 0f), image, "icon_exclamation");
		attention.SetAttentionEnable(IsAttentionEnable(SELECT_MENU.COLLECTION));
		mButtons[SELECT_MENU.GROWUP].SetButtonEnable(mGame.mPlayer.GrowUp >= 1);
		mCheatTimeBG = Util.CreateSprite("CheatTimeBG", base.gameObject, image);
		Util.SetSpriteInfo(mCheatTimeBG, "buy_panel", mBaseDepth + 2, new Vector3(0f, -280f, 0f), Vector3.one, false);
		mCheatTimeBG.type = UIBasicSprite.Type.Sliced;
		mCheatTimeBG.SetDimensions(380, 32);
		NGUITools.SetActive(mCheatTimeBG.gameObject, false);
		UIFont atlasFont = GameMain.LoadFont();
		UILabel uILabel = Util.CreateLabel("CheatTime", mCheatTimeBG.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 3, new Vector3(0f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.9254902f, 0.02745098f, 0.38039216f);
		uILabel.effectDistance = new Vector2(2f, 2f);
		uILabel.fontSize = 22;
		StartCoroutine(UpdateCheat(uILabel));
		CreateCancelButton("OnCancelPushed");
	}

	protected void CreateMenuButton(SELECT_MENU kind, GameObject parent, int depth, Vector3 position, string spriteName)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIButton uIButton = Util.CreateJellyImageButton("Button_" + (int)kind, base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, spriteName, spriteName, spriteName, depth, position, Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnMenuButtonPushed", UIButtonMessage.Trigger.OnClick);
		mButtons[kind] = uIButton.gameObject.GetComponent<JellyImageButton>();
	}

	protected bool IsAttentionEnable(SELECT_MENU kind)
	{
		switch (kind)
		{
		case SELECT_MENU.COLLECTION:
		{
			for (int i = 0; i < mGame.mVisualCollectionData.Count; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					if (mGame.mPlayer.GetCollectionNotifyStatus(mGame.mVisualCollectionData[i].piceIds[j]) == Player.NOTIFICATION_STATUS.NOTIFY)
					{
						return true;
					}
				}
			}
			List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
			for (int k = 0; k < accessoryByType.Count; k++)
			{
				if (mGame.mPlayer.GetCollectionNotifyStatus(accessoryByType[k].Index) == Player.NOTIFICATION_STATUS.NOTIFY)
				{
					return true;
				}
			}
			break;
		}
		case SELECT_MENU.TROPHY:
			return MissionManager.EnableGetReward();
		}
		return false;
	}

	private IEnumerator UpdateCheat(UILabel CheatTime)
	{
		while (!(mCheatTimeBG == null))
		{
			long totalSec = mGame.GetLifeRecoverSec();
			if (totalSec >= 6000)
			{
				NGUITools.SetActive(mCheatTimeBG.gameObject, true);
				long Hour = totalSec / 3600;
				long Min = totalSec % 3600 / 60;
				string str = string.Format(arg2: totalSec % 60, format: Localization.Get("MyPage_PenaltyTime"), arg0: Hour, arg1: Min);
				Util.SetLabelText(CheatTime, str);
			}
			else
			{
				NGUITools.SetActive(mCheatTimeBG.gameObject, false);
			}
			yield return null;
		}
	}

	public void Init(string title, string desc, bool openSeEnable = true, bool CollectAttention = false)
	{
		Init(title, desc, DIALOG_SIZE.MEDIUM);
		SetOpenSeEnable(openSeEnable);
	}

	public void Init(string title, string desc, DIALOG_SIZE size)
	{
		mTitle = title;
		mDescription = desc;
	}

	public void OnMenuButtonPushed(GameObject go)
	{
		if (GetBusy())
		{
			return;
		}
		JellyImageButton component = go.GetComponent<JellyImageButton>();
		if (component.IsEnable())
		{
			string[] array = go.name.Split('_');
			SELECT_MENU sELECT_MENU = (SELECT_MENU)int.Parse(array[1]);
			mSelectItem = sELECT_MENU;
			if (mCheatTimeBG != null)
			{
				Object.Destroy(mCheatTimeBG.gameObject);
				mCheatTimeBG = null;
			}
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mSelectItem = SELECT_MENU.CANCEL;
			if (mCheatTimeBG != null)
			{
				Object.Destroy(mCheatTimeBG.gameObject);
				mCheatTimeBG = null;
			}
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		mButtons.Clear();
		Object.Destroy(base.gameObject);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
