using System.Collections.Generic;

public class SeriesMaxLevel
{
	[MiniJSONAlias("series")]
	public int SeriesInt { get; set; }

	[MiniJSONAlias("maxlvl")]
	public int MaxLevel { get; set; }

	[MiniJSONAlias("use")]
	public int Available { get; set; }

	[MiniJSONAlias("schedule")]
	public List<SeriesMaxLevelSchedule> Schedule { get; set; }

	public SeriesMaxLevel()
	{
		SeriesInt = 0;
		MaxLevel = 100;
		Available = 0;
		Schedule = new List<SeriesMaxLevelSchedule>
		{
			new SeriesMaxLevelSchedule()
		};
	}
}
