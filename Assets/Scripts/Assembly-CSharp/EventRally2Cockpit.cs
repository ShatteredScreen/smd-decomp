using System.Collections;
using UnityEngine;

public class EventRally2Cockpit : EventCockpit
{
	protected const float SaleIconRally2BasePosY_P = 172f;

	protected const float SaleIconRally2BasePosY_L = 172f;

	protected const float SaleIconRally2PlaceInterval = 110f;

	public readonly string mNumImagePrefix = "event2_num";

	public readonly string mNumImageTPrefix = "event2_num";

	protected UIButton mCollectionButton;

	protected UIButton mCourseNext;

	protected string mNextImage;

	protected bool mPreserveImageSetNext;

	protected UIButton mCourseBack;

	protected string mBackImage;

	protected bool mPreserveImageSetBack;

	protected bool mButtonSE;

	protected int mStarNumMax;

	protected int mStarNumCurrent;

	protected bool mStarNumUpdate;

	protected UISprite mNum0;

	protected UISprite mNum1;

	protected UISprite mNum2;

	protected UISprite mNum3;

	protected UISprite mNum4;

	protected UISprite mNum5;

	protected string mStarNumImage0 = "event2_num0";

	protected string mStarNumImage1 = "event2_num0";

	protected string mStarNumImage2 = "event2_num0";

	protected string mStarNumImage3 = "event2_num0";

	protected string mStarNumImage4 = "event2_num0";

	protected string mStarNumImage5 = "event2_num0";

	protected bool mNextIconUpdate;

	protected UISprite mNextIcon;

	protected string mNextIconName = "next_icon00";

	protected UISprite mNext;

	protected string mNextName = "LC_next";

	protected bool mCourseUpdate;

	protected UISprite mCourseNumPanel1;

	protected string mCourseNum = "event2_num1";

	protected UISprite mCoursePanel1;

	protected UISprite mCoursePanel2;

	protected UISprite mCoursePanel3;

	protected UISprite mCoursePanel4;

	protected UISprite mCoursePanel5;

	protected string mCourseImage1 = "course03";

	protected string mCourseImage2 = "course03";

	protected string mCourseImage3 = "course03";

	protected string mCourseImage4 = "course03";

	protected string mCourseImage5 = "course03";

	protected UILabel mExitBack;

	protected UILabel mGiftLabel;

	protected Color mExitGiftButtonColor = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);

	protected ParticleSystem mPS;

	protected UIButton mRewardButton;

	protected UISprite mLine;

	protected GameObject mFooterRoot;

	protected UISprite mFooterSpriteL;

	protected UISprite mFooterSpriteR;

	protected override void Update()
	{
		if (mGSM == null)
		{
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			CreateCockpitStatus(40);
			CreateCockpitItems(40);
			CreateCockpitDebug(40, -90);
			CreateSaleItem(40);
			CreateMugenHeart(40);
			if (!mButtonEnableFlg)
			{
				SetEnableButtonAction(false);
				mButtonEnableFlg = true;
			}
			mState.Change(STATE.MAIN);
			break;
		case STATE.OPEN:
			if (mState.IsChanged() && mOpenCallback != null)
			{
				mOpenCallback(false);
			}
			break;
		case STATE.MAIN:
			UpdateSaleFlg();
			if (base.mShopflg)
			{
				base.ShopBagEnable = true;
			}
			else
			{
				base.ShopBagEnable = false;
			}
			UpdateMugenHeartIconFlg();
			if (base.mMugenflg)
			{
				base.ShopMugenHeartEnable = true;
			}
			else
			{
				base.ShopMugenHeartEnable = false;
			}
			break;
		case STATE.MOVE:
			if (!mState.IsChanged())
			{
				break;
			}
			switch (mState.GetPreStatus())
			{
			case STATE.OPEN:
				if (mCloseCallback != null)
				{
					mCloseCallback(true);
				}
				break;
			case STATE.CLOSE:
				if (mOpenCallback != null)
				{
					mOpenCallback(true);
				}
				break;
			}
			break;
		}
		mState.Update();
		UpdateUIPanel();
		UpdateHUD();
		UpdateDrawFlag();
		UpdateDrawOptionFlag();
		UpdateStatus();
	}

	protected override Vector3 ShopBugLocation()
	{
		return (!base.mMugenflg) ? new Vector3(-70f, 172f) : new Vector3(-70f, 282f);
	}

	protected override Vector3 MugenHeartLocation()
	{
		return new Vector3(-70f, 172f);
	}

	protected override Vector3 AllCrushIconLocation()
	{
		int num = 0;
		if (base.mShopflg)
		{
			num++;
		}
		if (base.mMugenflg)
		{
			num++;
		}
		Vector3 result;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			result = new Vector3(-70f, 172f + 110f * (float)num);
		}
		else
		{
			float num2 = 0f;
			if (num > 1)
			{
				num = 0;
				num2 = -112f;
			}
			result = new Vector3(-70f + num2, 172f + 110f * (float)num);
		}
		return result;
	}

	public void LineCreate()
	{
		Vector2 vector = Util.LogScreenSize();
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		if (mLine != null)
		{
			Object.Destroy(mLine.gameObject);
			mLine = null;
		}
		mLine = Util.CreateSprite("Line", mAnchorT.gameObject, image);
		Util.SetSpriteInfo(mLine, "line01", 42, new Vector3(0f, -134f, 0f), Vector3.one, false);
		mLine.type = UIBasicSprite.Type.Sliced;
		mLine.SetDimensions((int)vector.x - 20, 60);
	}

	protected override void CreateCockpitStatus(int baseDepth)
	{
		base.CreateCockpitStatus(baseDepth);
		Vector2 vector = Util.LogScreenSize();
		SMEventPageData eventPageData = GameMain.GetEventPageData(mGame.mPlayer.Data.CurrentEventID);
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		BIJImage image2 = ResourceManager.LoadImage(eventPageData.EventSetting.MapAtlasKey).Image;
		int currentEventID = mGame.mPlayer.Data.CurrentEventID;
		SMSeasonEventSetting sMSeasonEventSetting = mGame.mEventData.InSessionEventList[currentEventID];
		BIJImage image3 = ResourceManager.LoadImage(sMSeasonEventSetting.BannerAtlas).Image;
		UISprite sprite = Util.CreateSprite("EventTitle", mAnchorT.gameObject, image3);
		Util.SetSpriteInfo(sprite, eventPageData.EventSetting.MapEventTitle, baseDepth + 2, new Vector3(0f, -120f, 0f), Vector3.one, false);
		LineCreate();
		mRewardButton = Util.CreateJellyImageButton("RewardButton", mAnchorTL.gameObject, image);
		Util.SetImageButtonInfo(mRewardButton, "LC_button_ｌist", "LC_button_ｌist", "LC_button_ｌist", baseDepth + 3, new Vector3(55f, -225f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mRewardButton, mGSM, "OnRewardPushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mRewardButton);
		float num = 64f;
		float num2 = 23f;
		float y = -183f;
		UISprite sprite2 = Util.CreateSprite("star", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(sprite2, "event01_star", mBasedepth + 2, new Vector3(num - 34f, y, 0f), Vector3.one, false);
		mNum0 = Util.CreateSprite("num0", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum0, mStarNumImage0, mBasedepth + 2, new Vector3(num, y, 0f), Vector3.one, false);
		num += num2;
		mNum1 = Util.CreateSprite("num1", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum1, mStarNumImage1, mBasedepth + 2, new Vector3(num, y, 0f), Vector3.one, false);
		num += num2;
		mNum2 = Util.CreateSprite("num2", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum2, mStarNumImage2, mBasedepth + 2, new Vector3(num, y, 0f), Vector3.one, false);
		num += num2;
		UISprite sprite3 = Util.CreateSprite("Slash", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(sprite3, "event02_num_slash", mBasedepth + 2, new Vector3(num, y, 0f), Vector3.one, false);
		num += num2 - 4f;
		mNum3 = Util.CreateSprite("num3", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum3, mStarNumImage3, mBasedepth + 2, new Vector3(num, y, 0f), Vector3.one, false);
		num += num2;
		mNum4 = Util.CreateSprite("num4", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum4, mStarNumImage4, mBasedepth + 2, new Vector3(num, y, 0f), Vector3.one, false);
		num += num2;
		mNum5 = Util.CreateSprite("num5", mAnchorTL.gameObject, image);
		Util.SetSpriteInfo(mNum5, mStarNumImage5, mBasedepth + 2, new Vector3(num, y, 0f), Vector3.one, false);
		num += num2;
		float y2 = -180f;
		UISprite sprite4 = Util.CreateSprite("CoursePanel", mAnchorTR.gameObject, image);
		Util.SetSpriteInfo(sprite4, "LC_course01", baseDepth + 2, new Vector3(-310f, y2, 0f), Vector3.one, false);
		mCourseNumPanel1 = Util.CreateSprite("CoursePanelnum", mAnchorTR.gameObject, image);
		Util.SetSpriteInfo(mCourseNumPanel1, mCourseNum, baseDepth + 2, new Vector3(-255f, y2, 0f), Vector3.one, false);
		float num3 = 0f;
		float num4 = 45f;
		mCoursePanel1 = Util.CreateSprite("Course1", mAnchorTR.gameObject, image);
		Util.SetSpriteInfo(mCoursePanel1, mCourseImage1, baseDepth + 2, new Vector3(-215f + num3, y2, 0f), Vector3.one, false);
		num3 += num4;
		mCoursePanel2 = Util.CreateSprite("Course2", mAnchorTR.gameObject, image);
		Util.SetSpriteInfo(mCoursePanel2, mCourseImage2, baseDepth + 2, new Vector3(-215f + num3, y2, 0f), Vector3.one, false);
		num3 += num4;
		mCoursePanel3 = Util.CreateSprite("Course3", mAnchorTR.gameObject, image);
		Util.SetSpriteInfo(mCoursePanel3, mCourseImage3, baseDepth + 2, new Vector3(-215f + num3, y2, 0f), Vector3.one, false);
		num3 += num4;
		mCoursePanel4 = Util.CreateSprite("Course4", mAnchorTR.gameObject, image);
		Util.SetSpriteInfo(mCoursePanel4, mCourseImage4, baseDepth + 2, new Vector3(-215f + num3, y2, 0f), Vector3.one, false);
		num3 += num4;
		mCoursePanel5 = Util.CreateSprite("Course5", mAnchorTR.gameObject, image);
		Util.SetSpriteInfo(mCoursePanel5, mCourseImage5, baseDepth + 2, new Vector3(-215f + num3, y2, 0f), Vector3.one, false);
		num3 += num4;
	}

	protected override void CreateCockpitItems(int baseDepth)
	{
		base.CreateCockpitItems(baseDepth);
		GameObject parent = mAnchorB.gameObject;
		float num = 0f;
		float num2 = 170f;
		mFooterRoot = Util.CreateGameObject("FooterParent", parent);
		mFooterSpriteR = Util.CreateSprite("StatusR", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(mFooterSpriteR, "menubar_frame", baseDepth, new Vector3(-2f, num - num2, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mFooterSpriteL = Util.CreateSprite("StatusL", mFooterRoot, HudAtlas);
		Util.SetSpriteInfo(mFooterSpriteL, "menubar_frame", baseDepth, new Vector3(2f, num - num2, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		GameObject parent2 = mAnchorBL.gameObject;
		BIJImage image = ResourceManager.LoadImage("EVENTHUD").Image;
		Vector3 localPosition = new Vector3(271f, 55f);
		SMEventPageData eventPageData = GameMain.GetEventPageData(mGame.mPlayer.Data.CurrentEventID);
		if (eventPageData.EventSetting.AdvButtonFlg == 1 && mGame.EnableAdvEnter())
		{
			localPosition = new Vector3(379f, 55f);
		}
		mCollectionButton = Util.CreateJellyImageButton("Episode", parent2, image);
		Util.SetImageButtonInfo(mCollectionButton, "LC_button_episode", "LC_button_episode", "LC_button_episode", baseDepth + 2, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mCollectionButton, mGSM, "OnCollectionPushed", UIButtonMessage.Trigger.OnClick);
		mCockpitButtons.Add(mCollectionButton);
		mCollectionButton.transform.localPosition = localPosition;
		SetLayout(Util.ScreenOrientation);
	}

	public override void SetLayout(ScreenOrientation o)
	{
		base.SetLayout(o);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = Vector3.zero;
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mFooterRoot != null)
			{
				mFooterRoot.transform.localPosition = new Vector3(0f, -200f, 0f);
			}
			break;
		}
	}

	public void StarCurrent(int MaxStar, int NumCurrentStar)
	{
		mStarNumMax = MaxStar;
		mStarNumCurrent = NumCurrentStar;
		mStarNumImage2 = mNumImagePrefix + NumCurrentStar % 10;
		NumCurrentStar /= 10;
		mStarNumImage1 = mNumImagePrefix + NumCurrentStar % 10;
		NumCurrentStar /= 10;
		mStarNumImage0 = mNumImagePrefix + NumCurrentStar % 10;
		NumCurrentStar /= 10;
		mStarNumImage5 = mNumImageTPrefix + MaxStar % 10;
		MaxStar /= 10;
		mStarNumImage4 = mNumImageTPrefix + MaxStar % 10;
		MaxStar /= 10;
		mStarNumImage3 = mNumImageTPrefix + MaxStar % 10;
		MaxStar /= 10;
		mStarNumUpdate = true;
		mNextIconUpdate = true;
	}

	public void DisplayCourseNextButton(bool flg = false, bool a_se = false)
	{
		if (flg)
		{
			mNextImage = "LC_course_next";
			mButtonSE = a_se;
		}
		else
		{
			if (mPS != null)
			{
				Object.Destroy(mPS.gameObject);
				mPS = null;
			}
			mNextImage = "null";
		}
		mPreserveImageSetNext = true;
	}

	public void DisplayCourseBackButton(bool flg = false, bool a_se = false)
	{
		if (flg)
		{
			mBackImage = "LC_course_back";
			mButtonSE = a_se;
		}
		else
		{
			mBackImage = "null";
		}
		mPreserveImageSetBack = true;
	}

	public void SetCourse(int a_current, int a_playable)
	{
		mCourseUpdate = true;
		mCourseImage1 = "course03";
		mCourseImage2 = "course03";
		mCourseImage3 = "course03";
		mCourseImage4 = "course03";
		mCourseImage5 = "course03";
		if (a_playable != 5)
		{
			mCourseImage1 = "course05";
			mCourseImage2 = "course05";
			mCourseImage3 = "course05";
			mCourseImage4 = "course03";
			mCourseImage5 = "course03";
		}
		else
		{
			mCourseImage1 = "course05";
			mCourseImage2 = "course05";
			mCourseImage3 = "course05";
			mCourseImage4 = "course05";
			mCourseImage5 = "course05";
		}
		switch (a_current)
		{
		case 0:
			mCourseImage1 = "course04";
			if (mExitBack != null)
			{
				mExitBack.color = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
				mGiftLabel.color = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
			}
			mExitGiftButtonColor = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
			break;
		case 1:
			mCourseImage2 = "course04";
			if (mExitBack != null)
			{
				mExitBack.color = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
				mGiftLabel.color = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
			}
			mExitGiftButtonColor = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
			break;
		case 2:
			mCourseImage3 = "course04";
			if (mExitBack != null)
			{
				mExitBack.color = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
				mGiftLabel.color = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
			}
			mExitGiftButtonColor = new Color(4f / 15f, 4f / 15f, 4f / 15f, 1f);
			break;
		case 3:
			mCourseImage4 = "course04";
			if (mExitBack != null)
			{
				mExitBack.color = Color.white;
				mGiftLabel.color = Color.white;
			}
			mExitGiftButtonColor = Color.white;
			break;
		case 4:
			mCourseImage5 = "course04";
			if (mExitBack != null)
			{
				mExitBack.color = Color.white;
				mGiftLabel.color = Color.white;
			}
			mExitGiftButtonColor = Color.white;
			break;
		}
		mCourseNum = "event2_num" + (a_current + 1);
	}

	private IEnumerator UIButtonIdleAnimation(UIButton a_button)
	{
		yield return 0;
	}

	private IEnumerator ShowButtonSprite(UIButton a_button, string a_image)
	{
		Util.SetImageButtonGraphic(a_button, a_image, a_image, a_image);
		float scale2 = 3f;
		float interval = 0.2f;
		float t = 0f;
		while (true)
		{
			t += Time.deltaTime / interval;
			scale2 = Mathf.Lerp(3f, 1f, t);
			a_button.gameObject.transform.localScale = new Vector3(scale2, scale2, 1f);
			if (scale2 <= 1f)
			{
				break;
			}
			yield return 0;
		}
		if (mButtonSE)
		{
			mGame.PlaySe("SE_OPENOPTION", -1);
			mButtonSE = false;
		}
	}

	private void UpdateHUD()
	{
		if (mStarNumUpdate && mNum0 != null && mNum1 != null && mNum2 != null && mNum3 != null && mNum4 != null && mNum5 != null)
		{
			mStarNumUpdate = false;
			Util.SetSpriteImageName(mNum0, mStarNumImage0, Vector3.one, false);
			Util.SetSpriteImageName(mNum1, mStarNumImage1, Vector3.one, false);
			Util.SetSpriteImageName(mNum2, mStarNumImage2, Vector3.one, false);
			Util.SetSpriteImageName(mNum3, mStarNumImage3, Vector3.one, false);
			Util.SetSpriteImageName(mNum4, mStarNumImage4, Vector3.one, false);
			Util.SetSpriteImageName(mNum5, mStarNumImage5, Vector3.one, false);
		}
		if (mCourseUpdate && mCoursePanel1 != null && mCoursePanel2 != null && mCoursePanel3 != null && mCoursePanel4 != null && mCoursePanel4 != null && mCourseNumPanel1 != null)
		{
			mCourseUpdate = false;
			Util.SetSpriteImageName(mCoursePanel1, mCourseImage1, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel2, mCourseImage2, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel3, mCourseImage3, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel4, mCourseImage4, Vector3.one, false);
			Util.SetSpriteImageName(mCoursePanel5, mCourseImage5, Vector3.one, false);
			Util.SetSpriteImageName(mCourseNumPanel1, mCourseNum, Vector3.one, false);
		}
	}
}
