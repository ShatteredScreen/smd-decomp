using System;

public class SsInterpolation
{
	public static float Linear(float cur, float start, float end)
	{
		return start + (end - start) * cur;
	}

	public static int Linear(float cur, int istart, int iend)
	{
		float num = istart;
		float num2 = iend;
		return (int)(num + (num2 - num) * cur);
	}

	public static float Hermite(float fTime, float fStartV, float fEndV, float fSParamV, float fEParamV)
	{
		float num = fTime;
		if (num > 1f)
		{
			num = 1f;
		}
		if (num < 0f)
		{
			num = 0f;
		}
		float num2 = fTime * fTime;
		float num3 = num2 * fTime;
		return (2f * num3 - 3f * num2 + 1f) * fStartV + (-2f * num3 + 3f * num2) * fEndV + (num3 - 2f * num2 + fTime) * (fSParamV - fStartV) + (num3 - num2) * (fEParamV - fEndV);
	}

	public static int Hermite(float fTime, int fStartV, int fEndV, float fSParamV, float fEParamV)
	{
		return (int)Hermite(fTime, (float)fStartV, (float)fEndV, fSParamV, fEParamV);
	}

	public static float Bezier(float fTime, float fStartT, float fStartV, float fEndT, float fEndV, float fSParamT, float fSParamV, float fEParamT, float fEParamV)
	{
		float num = fTime;
		if (num > 1f)
		{
			num = 1f;
		}
		if (num < 0f)
		{
			num = 0f;
		}
		float num2 = (fEndT - fStartT) * fTime + fStartT;
		float num3 = 0.5f;
		float num4 = 0.5f;
		float num5;
		float num6;
		float num7;
		for (int i = 0; i < 8; i++)
		{
			num5 = 1f - num3;
			num6 = num5 * num5;
			num7 = num6 * num5;
			float num8 = num7 * fStartT + 3f * num6 * num3 * (fSParamT + fStartT) + 3f * num5 * num3 * num3 * (fEParamT + fEndT) + num3 * num3 * num3 * fEndT;
			num4 /= 2f;
			num3 = ((!(num8 > num2)) ? (num3 + num4) : (num3 - num4));
		}
		num5 = 1f - num3;
		num6 = num5 * num5;
		num7 = num6 * num5;
		return num7 * fStartV + 3f * num6 * num3 * (fSParamV + fStartV) + 3f * num5 * num3 * num3 * (fEParamV + fEndV) + num3 * num3 * num3 * fEndV;
	}

	public static float Interpolate<T>(SsCurveParams curve, float time, T startValue, T endValue, int startTime, int endTime)
	{
		switch (curve.Type)
		{
		case SsInterpolationType.Linear:
			return Linear(time, Convert.ToSingle(startValue), Convert.ToSingle(endValue));
		case SsInterpolationType.Hermite:
			return Hermite(time, Convert.ToSingle(startValue), Convert.ToSingle(endValue), curve.StartV, curve.EndV);
		case SsInterpolationType.Bezier:
			return Bezier(time, startTime, Convert.ToSingle(startValue), endTime, Convert.ToSingle(endValue), curve.StartT, curve.StartV, curve.EndT, curve.EndV);
		default:
			return Convert.ToSingle(startValue);
		}
	}

	public static int Interpolate(SsCurveParams curve, float time, int startValue, int endValue, int startTime, int endTime)
	{
		return (int)Interpolate(curve, time, (float)startValue, (float)endValue, startTime, endTime);
	}

	public static object InterpolateKeyValue(SsKeyFrameInterface prevKey, SsKeyFrameInterface nextKey, int time)
	{
		if (prevKey.Curve.IsNone)
		{
			return prevKey.ObjectValue;
		}
		int time2 = prevKey.Time;
		int time3 = nextKey.Time;
		float time4 = 0f;
		if (time2 < time3)
		{
			time4 = (float)(time - time2) / (float)(time3 - time2);
		}
		SsInterpolatable ssInterpolatable = prevKey.ObjectValue as SsInterpolatable;
		if (ssInterpolatable == null)
		{
			return Interpolate(prevKey.Curve, time4, prevKey.ObjectValue, nextKey.ObjectValue, time2, time3);
		}
		return ssInterpolatable.GetInterpolated(prevKey.Curve, time4, (SsInterpolatable)prevKey.ObjectValue, (SsInterpolatable)nextKey.ObjectValue, time2, time3);
	}
}
