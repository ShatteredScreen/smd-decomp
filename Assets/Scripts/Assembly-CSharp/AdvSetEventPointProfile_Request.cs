public class AdvSetEventPointProfile_Request : RequestBase
{
	[MiniJSONAlias("event_id")]
	public short EventID { get; set; }

	[MiniJSONAlias("quantity")]
	public int Quantity { get; set; }

	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }

	[MiniJSONAlias("is_retry")]
	public int IsRetry { get; set; }
}
