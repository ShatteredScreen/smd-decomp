public class MissionCountFigure
{
	public const int LEVEL_MISSION_NUM = 8;

	public const int SKILL_MISSION_NUM = 3;

	public int mNum { get; set; }

	public int[] mLevel { get; set; }

	public int[] mSkill { get; set; }

	public MissionCountFigure()
	{
		mLevel = new int[8];
		mSkill = new int[3];
		Init();
	}

	public void Init()
	{
		mNum = 0;
		for (int i = 0; i < 8; i++)
		{
			mLevel[i] = 0;
		}
		for (int j = 0; j < 3; j++)
		{
			mSkill[j] = 0;
		}
	}
}
