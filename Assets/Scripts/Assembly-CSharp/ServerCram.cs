using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using UnityEngine;

public class ServerCram : MonoBehaviour
{
	public enum CRAM_ID
	{
		CRAM_ID_1_TUT = 0,
		CRAM_ID_2_BYH = 1,
		CRAM_ID_3_CNT = 2,
		CRAM_ID_4_RRB = 3,
		CRAM_ID_5_URB = 4,
		CRAM_ID_6_SGI = 5,
		CRAM_ID_7_CUS = 6,
		CRAM_ID_8_CUT = 7,
		CRAM_ID_9_CUU = 8,
		CRAM_ID_10_WIN = 9,
		CRAM_ID_11_PST = 10,
		CRAM_ID_12_EWIN = 11,
		CRAM_ID_13_LGI = 12,
		CRAM_ID_14_BDC = 13,
		CRAM_ID_15_FHLP = 14,
		CRAM_ID_16_FWIN = 15,
		CRAM_ID_17_GBST = 16,
		CRAM_ID_18_GACCE = 17,
		CRAM_ID_19_UBST = 18,
		CRAM_ID_20_CTUT = 19,
		CRAM_ID_21_CLU = 20,
		CRAM_ID_22_DWIN = 21,
		CRAM_ID_23_PNR = 22,
		CRAM_ID_24_DEWIN = 23,
		CRAM_ID_25_DAU = 24,
		CRAM_ID_26_DAYWIN = 25,
		CRAM_ID_27_SKST = 26,
		CRAM_ID_28_RTM = 27,
		CRAM_ID_29_GTRP = 28,
		CRAM_ID_30_EXCTRP = 29,
		CRAM_ID_31_INFH = 30,
		CRAM_ID_32_LBR = 31,
		CRAM_ID_33_AGFI = 32,
		CRAM_ID_34_ACLU = 33,
		CRAM_ID_35_ACSLU = 34,
		CRAM_ID_36_APL = 35,
		CRAM_ID_37_AWIN = 36,
		CRAM_ID_38_ACNT = 37,
		CRAM_ID_39_AEWIN = 38,
		CRAM_ID_40_AGACCE = 39,
		CRAM_ID_41_ADAU = 40,
		CRAM_ID_42_MDAU = 41,
		CRAM_ID_43_ATUT = 42,
		CRAM_ID_44_AGR = 43,
		CRAM_ID_45_ABYM = 44,
		CRAM_ID_46_AAPP = 45,
		CRAM_ID_47_LBCK = 46,
		CRAM_ID_48_APLP = 47,
		CRAM_ID_49_AEWINP = 48,
		CRAM_ID_50_AERP = 49,
		CRAM_ID_51_AECDP = 50,
		CRAM_ID_52_AEGPP = 51,
		CRAM_ID_53_AECUP = 52,
		CRAM_ID_54_GDG = 53,
		CRAM_ID_55_TIA = 54,
		CRAM_ID_56_EAS = 55,
		CRAM_ID_57_GEMCP = 56,
		CRAM_ID_58_TDL = 57,
		CRAM_ID_59_SSC = 58,
		CRAM_ID_60_DPM = 59,
		CRAM_ID_61_EBNG = 60,
		CRAM_ID_62_EBLTY = 61,
		CRAM_ID_63_OEENT = 62,
		CRAM_ID_64_OEOPN = 63,
		CRAM_ID_65_OEGDG = 64,
		CRAM_ID_66_DCWIN = 65,
		CRAM_ID_67_GEMGD = 66,
		CRAM_ID_68_GEMPC = 67,
		CRAM_ID_69_IPN = 68,
		CRAM_ID_70_DTD = 69,
		CRAM_ID_71_DTS = 70,
		CRAM_ID_72_DRB = 71,
		CRAM_ID_73_OEWIN = 72,
		CRAM_ID_74_DCST = 73,
		CRAM_ID_75_DTR = 74,
		CRAM_ID_76_LVRC = 75,
		CRAM_ID_77_ELCNT = 76,
		CRAM_ID_78_ELRSLT = 77,
		CRAM_ID_79_ELCR = 78,
		CRAM_ID_80_ELGJ = 79,
		CRAM_ID_81_ELRWD = 80,
		CRAM_ID_82_SGWIN = 81,
		CRAM_ID_MAX = 82,
		CRAM_ID_MIN = 0,
		CRAM_ID_INVALID = -1
	}

	private class CramData
	{
		public CRAM_ID _id;

		public int _sendId;

		public bool _isMandatory;

		public DateTime _time;

		public string _data;
	}

	private class CRAMRequest
	{
		public int _sendId;

		public bool _isMandatory;

		public HttpAsync _httpAsync;

		public DateTime _time;
	}

	public class Sender : MonoBehaviour
	{
		private enum STATE
		{
			INIT = 0,
			IDLE = 1,
			WAIT_LIST_ADD = 2,
			SEND = 3,
			DIE = 4,
			DYING = 5
		}

		private STATE mState;

		private ServerCram mParent;

		private CramData mSendData;

		private int mWaitCnt;

		public static Sender CreateInstance(ServerCram parent)
		{
			GameObject gameObject = new GameObject("ServerCramSender");
			Sender sender = gameObject.AddComponent<Sender>();
			sender.mParent = parent;
			return sender;
		}

		private void Awake()
		{
			mState = STATE.INIT;
			mSendData = null;
		}

		private void Start()
		{
			lock (mParent.mDataListLock)
			{
				mParent.LoadCramData();
			}
		}

		public void Kill()
		{
			mState = STATE.DIE;
		}

		public void Update()
		{
			switch (mState)
			{
			case STATE.INIT:
				if (!GameMain.IsLoaded && !mSendStart)
				{
					break;
				}
				if (SingletonMonoBehaviour<GameMain>.Instance.mPlayer.GetSumErrorFlag())
				{
					lock (mParent.mDataListLock)
					{
						CheatUserSum();
					}
				}
				mState = STATE.IDLE;
				break;
			case STATE.IDLE:
				lock (mParent.mDataListLock)
				{
					for (int i = 0; i < mParent.mDataList.Count; i++)
					{
						CramData cramData = mParent.mDataList[i];
						if (cramData._sendId < 0)
						{
							mSendData = cramData;
							mWaitCnt = 0;
							mState = STATE.WAIT_LIST_ADD;
							break;
						}
					}
					break;
				}
			case STATE.WAIT_LIST_ADD:
				lock (mParent.mDataListLock)
				{
					for (int j = 0; j < mParent.mDataList.Count; j++)
					{
						CramData cramData2 = mParent.mDataList[j];
						if (cramData2._sendId < 0 && mSendData != cramData2 && mSendData._id != cramData2._id)
						{
							mState = STATE.SEND;
							break;
						}
					}
				}
				mWaitCnt++;
				if (mWaitCnt >= 10)
				{
					mState = STATE.SEND;
				}
				break;
			case STATE.SEND:
			{
				bool flag = false;
				try
				{
					flag = mParent.PostRequest();
				}
				catch (Exception e)
				{
					BIJLog.E("ServerCram PostRequest error.", e);
					flag = false;
				}
				if (flag)
				{
					mState = STATE.IDLE;
				}
				break;
			}
			case STATE.DIE:
				UnityEngine.Object.Destroy(base.gameObject);
				mState = STATE.DYING;
				break;
			case STATE.DYING:
				break;
			}
		}
	}

	private const int POST_MAX = 20;

	private const int BULK_POST_MAX = 10;

	private static bool[] MandatoryTable = new bool[82]
	{
		false, true, true, false, true, false, false, false, false, false,
		false, false, false, true, false, false, true, true, true, true,
		true, false, false, false, false, false, true, true, true, true,
		true, true, true, true, true, true, true, true, true, true,
		false, false, false, true, true, true, false, true, true, true,
		true, true, true, true, true, true, true, false, true, true,
		true, true, true, true, true, true, true, true, true, true,
		true, true, false, true, true, true, true, false, false, false,
		false, false
	};

	private static string[] phpTable = new string[82]
	{
		".tut", ".byh", ".cnt", ".rrb", ".urb", ".sgi", ".cus", ".cut", ".cuu", ".win",
		".pst", ".ewin", ".lgi", ".bdc", ".fhlp", ".fwin", ".gbst", ".gacce", ".ubst", ".ctut",
		".clu", ".dwin", ".pnr", ".dewin", ".dau", ".daywin", ".skst", ".rtm", ".gtrp", ".exctrp",
		".infh", ".lbr", ".agfi", ".aclu", ".acslu", ".apl", ".awin", ".acnt", ".aewin", ".agacce",
		".adau", ".mdau", ".atut", ".agr", ".abym", ".aapp", ".lbck", ".aplp", ".aewinp", ".aerp",
		".aecdp", ".aegpp", ".aecup", ".gdg", ".tia", ".eas", ".gemcp", ".tdl", ".ssc", ".dpm",
		".ebng", ".eblty", ".oeent", ".oeopn", ".oegdg", ".dcwin", ".gemgd", ".gempc", ".ipn", ".dtd",
		".dts", ".drb", ".oewin", ".dcst", ".dtr", ".lvrc", ".elcnt", ".elrslt", ".elcr", ".elgj",
		".elrwd", ".sgwin"
	};

	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	private static object mInstanceLock = new object();

	private static ServerCram mInstance = null;

	private List<CramData> mDataList = new List<CramData>();

	private readonly object mDataListLock = new object();

	private static int mSendId = 1;

	private static bool mSaveAtRequest = true;

	private Queue<CRAMRequest> mRequests = new Queue<CRAMRequest>();

	private readonly object mRequestsLock = new object();

	private ManualResetEvent mRequestsNotify = new ManualResetEvent(false);

	private Thread mThread;

	private ManualResetEvent mThreadLock;

	private volatile bool mIsRunning;

	private Sender mSender;

	private static bool mSendStart = false;

	private bool isTerminating;

	private static List<int[]> mAdvFigureDataBuffer = new List<int[]>();

	public static ServerCram Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					mInstance = (ServerCram)gameObject.GetComponent(typeof(ServerCram));
				}
				return mInstance;
			}
		}
	}

	public static int Timeout { get; set; }

	private static string BaseCramURL
	{
		get
		{
			return SingletonMonoBehaviour<Network>.Instance.CRAM_URL;
		}
	}

	private static string BaseCramKPIURL
	{
		get
		{
			return SingletonMonoBehaviour<Network>.Instance.CRAM_KPI_URL;
		}
	}

	public static bool IsNetworking
	{
		get
		{
			return false;
		}
	}

	public static bool IsCramSendable
	{
		get
		{
			if (string.IsNullOrEmpty(BaseCramURL))
			{
				return false;
			}
			if (InternetReachability.Status == NetworkReachability.NotReachable)
			{
				return false;
			}
			return true;
		}
	}

	public static bool TutorialState(int status, int cur_series, int cur_stage, int tut_v, int skip)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_1_TUT, status, cur_series, cur_stage, tut_v, skip);
	}

	public static bool BuyHeart(int cost, int amount, int place, int cur_series, int cur_stage)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_2_BYH, cost, amount, place, cur_series, cur_stage);
	}

	public static bool BuyContinue(int cost, int add_move, float add_time, int cond, int step_item, int step_item_num, int sts_cnt, int cur_series, int cur_stage, int stage_win, int stage_lose, float stage_win_rate, long purchase_count)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_3_CNT, cost, add_move, add_time, cond, step_item, step_item_num, sts_cnt, cur_series, cur_stage, stage_win, stage_lose, stage_win_rate, purchase_count);
	}

	public static bool ReachRoadblock(int tgt_stage, int cur_time, int cur_series)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_4_RRB, tgt_stage, cur_time, cur_series);
	}

	public static bool UnlockRoadblock(int tgt_stage, int unlock_by, int cur_time, int reach_time, int cur_series)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_5_URB, tgt_stage, unlock_by, cur_time, reach_time, cur_series);
	}

	public static bool SendGiftITEM(int friend_num, int item_kind, int item_id, int type)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_6_SGI, friend_num, item_kind, item_id, type);
	}

	public static bool CheatUserSum()
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_7_CUS);
	}

	public static bool CheatUserSut(long remote_time, long cur_time, long local_time, int cause)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_8_CUT, remote_time, cur_time, local_time, cause);
	}

	public static bool CheatUserCuu()
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_9_CUU);
	}

	public static bool WinRate(int tgt_stage, int cur_series, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int gotstars, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int make_stripe, int make_pop, int make_rainbow, int make_paint, int make_lunap, int erase_stripe, int erase_pop, int erase_rainbow, int erase_paint, int erase_lunap, int swap_stripe_stripe, int swap_stripe_pop, int swap_stripe_rainbow, int swap_stripe_paint, int swap_pop_pop, int swap_pop_rainbow, int swap_pop_paint, int swap_rainbow_rainbow, int swap_rainbow_paint, int swap_paint_paint)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_10_WIN, tgt_stage, cur_series, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, gotstars, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, make_stripe, make_pop, make_rainbow, make_paint, make_lunap, erase_stripe, erase_pop, erase_rainbow, erase_paint, erase_lunap, swap_stripe_stripe, swap_stripe_pop, swap_stripe_rainbow, swap_stripe_paint, swap_pop_pop, swap_pop_rainbow, swap_pop_paint, swap_rainbow_rainbow, swap_rainbow_paint, swap_paint_paint);
	}

	public static bool PlayTime(long st, long et, long pst, string device_id, string osver, string ot, string fbid, string gcid, string gpid, string twid, string gpgsid, long startup_count, long play_count, int first_purchase, long purchase_count, long total_win, long total_lose, float total_win_rate, int rootHack, int ic, string lvl_array)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_11_PST, st, et, pst, device_id, osver, ot, fbid, gcid, gpid, twid, gpgsid, startup_count, play_count, first_purchase, purchase_count, total_win, total_lose, total_win_rate, rootHack, ic, lvl_array);
	}

	public static bool EventWinRate(int tgt_stage, int evt_id, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int gotstars, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int make_stripe, int make_pop, int make_rainbow, int make_paint, int make_lunap, int erase_stripe, int erase_pop, int erase_rainbow, int erase_paint, int erase_lunap, int swap_stripe_stripe, int swap_stripe_pop, int swap_stripe_rainbow, int swap_stripe_paint, int swap_pop_pop, int swap_pop_rainbow, int swap_pop_paint, int swap_rainbow_rainbow, int swap_rainbow_paint, int swap_paint_paint)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_12_EWIN, tgt_stage, evt_id, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, gotstars, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, make_stripe, make_pop, make_rainbow, make_paint, make_lunap, erase_stripe, erase_pop, erase_rainbow, erase_paint, erase_lunap, swap_stripe_stripe, swap_stripe_pop, swap_stripe_rainbow, swap_stripe_paint, swap_pop_pop, swap_pop_rainbow, swap_pop_paint, swap_rainbow_rainbow, swap_rainbow_paint, swap_paint_paint);
	}

	public static bool LostGiftItem(int gift_kind, int item_kind, int item_id, int amount, int got_time, int cur_series, int cur_stage)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_13_LGI, gift_kind, item_kind, item_id, amount, got_time, cur_series, cur_stage);
	}

	public static bool BackupDataConfirm(int reason, int select_item, string path, int size, int local_ver, int local_series, int local_lvl, string local_lvl_array, int remote_ver, int remote_series, int remote_lvl, string remote_lvl_array, int cur_series, int cur_stage, int heart)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_14_BDC, reason, select_item, path, size, local_ver, local_series, local_lvl, local_lvl_array, remote_ver, remote_series, remote_lvl, remote_lvl_array, cur_series, cur_stage, heart);
	}

	public static bool SendFriendHelp(int cur_series, int cur_stage, int stage_stars, int stage_win, int stage_lose, float stage_win_rate)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_15_FHLP, cur_series, cur_stage, stage_stars, stage_win, stage_lose, stage_win_rate);
	}

	public static bool FriendWinRate(int tgt_stage, int cur_series, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int gotstars, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int friendid)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_16_FWIN, tgt_stage, cur_series, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, gotstars, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, friendid);
	}

	public static bool GetBoost(int amount, int cost, int cur_series, int cur_stage, int get_type, int kind, string boost, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, long purchase_count)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_17_GBST, amount, cost, cur_series, cur_stage, get_type, kind, boost, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, purchase_count);
	}

	public static bool GetAccessory(int accessory_id, int cur_series, int get_type)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_18_GACCE, accessory_id, cur_series, get_type);
	}

	public static bool UseItem(int amount, int amount_free, int amount_paid, int tgt_stage, int cur_series, int use_type, int kind, int won, long play_count, string boost)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_19_UBST, amount, amount_free, amount_paid, tgt_stage, cur_series, use_type, kind, won, play_count, boost);
	}

	public static bool TutorialComplete(int skip)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_20_CTUT, skip);
	}

	public static bool CharacterLevelUP(int Character_ID, int cur_series, int cur_stage, int cur_aseries, int cur_astage, int origin_lvl, int update_lvl, string Character_status, int place)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_21_CLU, Character_ID, cur_series, cur_stage, cur_aseries, cur_astage, origin_lvl, update_lvl, Character_status, place);
	}

	public static bool DebugWinRate(int tgt_stage, int cur_series, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int gotstars, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int clear_score, int clear_stars, int limit_moves)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_22_DWIN, tgt_stage, cur_series, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, gotstars, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, clear_score, clear_stars, limit_moves);
	}

	public static bool PushNotificationReturn(int NotificationId)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_23_PNR, NotificationId);
	}

	public static bool DebugEventWinRate(int tgt_stage, int evt_id, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int gotstars, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int clear_score, int clear_stars, int limit_moves)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_24_DEWIN, tgt_stage, evt_id, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, gotstars, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, clear_score, clear_stars, limit_moves);
	}

	public static bool DailyActiveUser(long st, string device_id, string osver, long startup_count, long play_count, int first_purchase, long purchase_count, float purchase_amount, string currency_code, string boost, long buy_heart_count, long buy_boost_count, long buy_continue_count, long buy_rb_count, string lvl_array)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_25_DAU, st, device_id, osver, startup_count, play_count, first_purchase, purchase_count, purchase_amount, currency_code, boost, buy_heart_count, buy_boost_count, buy_continue_count, buy_rb_count, lvl_array);
	}

	public static bool DailyEventWinRate(int tgt_stage, int cur_series, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int gotstars, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int accessory_id)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_26_DAYWIN, tgt_stage, cur_series, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, gotstars, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, accessory_id);
	}

	public static bool SkipStage(int cost, int cond, int sts_lose, int status, int cur_series, int cur_stage, int stage_win, int stage_lose, float stage_win_rate)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_27_SKST, cost, cond, sts_lose, status, cur_series, cur_stage, stage_win, stage_lose, stage_win_rate);
	}

	public static bool ReachTrophyMission(int mission_id, int mission_lvl, long purchase_count)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_28_RTM, mission_id, mission_lvl, purchase_count);
	}

	public static bool GetTrophy(int get_num, int total_num, int get_type, int sub_type, long purchase_count)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_29_GTRP, get_num, total_num, get_type, sub_type, purchase_count);
	}

	public static bool ExchangeTrophy(int item_id, int use_num, int total_num, int exchange_cnt, int total_exchange_cnt, long purchase_count)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_30_EXCTRP, item_id, use_num, total_num, exchange_cnt, total_exchange_cnt, purchase_count);
	}

	public static bool InfiniteHeartEnd(int playcount, int win, int lose, int duration, int cur_series, int cur_stage, int heart)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_31_INFH, playcount, win, lose, duration, cur_series, cur_stage, heart);
	}

	public static bool LoginBonusReceive(int cur_series, int cur_event, int bonus_id, string login_category, int sheet, int login_daycount, int item_reward_id, int item_bonus_id, int item_category, int item_sub_category, int item_quantity)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_32_LBR, cur_series, cur_event, bonus_id, login_category, sheet, login_daycount, item_reward_id, item_bonus_id, item_category, item_sub_category, item_quantity);
	}

	public static bool ArcadeGetFigure(int Character_ID, int place, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_33_AGFI, Character_ID, place, maxlvl, max_stamina);
	}

	public static bool ArcadeCharacterLevelUP(int Character_ID, int use_xp, int cur_series, int cur_stage, int cur_aseries, int cur_astage, int origin_lvl, int update_lvl, int get_type, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_34_ACLU, Character_ID, use_xp, cur_series, cur_stage, cur_aseries, cur_astage, origin_lvl, update_lvl, get_type, maxlvl, max_stamina);
	}

	public static bool ArcadeCharacterSkillLevelUP(int Character_ID, int use_skillitem, int cur_series, int cur_stage, int cur_aseries, int cur_astage, int origin_lvl, int update_lvl, int get_type, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_35_ACSLU, Character_ID, use_skillitem, cur_series, cur_stage, cur_aseries, cur_astage, origin_lvl, update_lvl, get_type, maxlvl, max_stamina);
	}

	public static bool ArcadePlay(int tgt_stage, int tgt_fmt, int tgt_rev, string tgt_seg, int evt_id, int use_stamina, int unique_id, int totalhp, int totalattack, int totaldefense, string use_charaID, string use_charaLvl, string use_charaSLvl, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_36_APL, tgt_stage, tgt_fmt, tgt_rev, tgt_seg, evt_id, use_stamina, unique_id, totalhp, totalattack, totaldefense, use_charaID, use_charaLvl, use_charaSLvl, maxlvl, max_stamina);
	}

	public static bool ArcadeWinRate(int tgt_stage, int tgt_fmt, int tgt_rev, string tgt_seg, int evt_id, int won, int resulttype, int ptime, int pcnt, int fever_count, int max_combo, int stamina, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int current_wave, long total_enemyhp, string use_charaID, string use_charaMaxDamage, string use_charaSkill, int unique_id, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_37_AWIN, tgt_stage, tgt_fmt, tgt_rev, tgt_seg, evt_id, won, resulttype, ptime, pcnt, fever_count, max_combo, stamina, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, current_wave, total_enemyhp, use_charaID, use_charaMaxDamage, use_charaSkill, unique_id, maxlvl, max_stamina);
	}

	public static bool ArcadeBuyContinue(int cost, int add_hp, int sts_cnt, int fever_count, int evt_id, int cur_aseries, int cur_astage, int stage_win, int stage_lose, float stage_win_rate, long purchase_count, int wave_num, int unique_id, int total_enemyhp, string enemyID, string enemyHP, string enemyATK, string enemyDEF, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_38_ACNT, cost, add_hp, sts_cnt, fever_count, evt_id, cur_aseries, cur_astage, stage_win, stage_lose, stage_win_rate, purchase_count, wave_num, unique_id, total_enemyhp, enemyID, enemyHP, enemyATK, enemyDEF, maxlvl, max_stamina);
	}

	public static bool ArcadeEventWinRate(int tgt_stage, int tgt_fmt, int tgt_rev, string tgt_seg, int evt_id, int won, int resulttype, int ptime, int pcnt, int fever_count, int max_combo, int stamina, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int current_wave, long total_enemyhp, string use_charaID, string use_charaMaxDamage, string use_charaSkill, int unique_id, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_39_AEWIN, tgt_stage, tgt_fmt, tgt_rev, tgt_seg, evt_id, won, resulttype, ptime, pcnt, fever_count, max_combo, stamina, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, current_wave, total_enemyhp, use_charaID, use_charaMaxDamage, use_charaSkill, unique_id, maxlvl, max_stamina);
	}

	public static bool ArcadeGetAccessory(int accessory_id, int cur_aseries, int cur_astage, int evt_id, int get_type, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_40_AGACCE, accessory_id, cur_aseries, cur_astage, evt_id, get_type, maxlvl, max_stamina);
	}

	public static bool ArcadeDailyActiveUser(long st, string device_id, string osver, long startup_count, long play_count, int first_purchase, long purchase_count, float purchase_amount, string currency_code, string abooster, long buy_medal_count, long buy_item_count, long buy_continue_count, long buy_gasya_count, int figure_num, int aplayd, int aintervald, int acontinuousd, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_41_ADAU, st, device_id, osver, startup_count, play_count, first_purchase, purchase_count, purchase_amount, currency_code, abooster, buy_medal_count, buy_item_count, buy_continue_count, buy_gasya_count, figure_num, aplayd, aintervald, acontinuousd, maxlvl, max_stamina);
	}

	public static bool MainDailyActiveUser(long st, string device_id, string osver, long startup_count, long play_count, int first_purchase, long purchase_count, float purchase_amount, string currency_code, string boost, long buy_heart_count, long buy_boost_count, long buy_continue_count, long buy_rb_count, string lvl_array)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_42_MDAU, st, device_id, osver, startup_count, play_count, first_purchase, purchase_count, purchase_amount, currency_code, boost, buy_heart_count, buy_boost_count, buy_continue_count, buy_rb_count, lvl_array);
	}

	public static bool ArcadeTutorialState(int status, int cur_aseries, int cur_astage, int tut_v, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_43_ATUT, status, cur_aseries, cur_astage, tut_v, maxlvl, max_stamina);
	}

	public static bool ArcadeGasyaResult(string chara_result, int gasya_id, int cost_type, int cost, int total_chara, int gasya_type, int chara_conflict, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_44_AGR, chara_result, gasya_id, cost_type, cost, total_chara, gasya_type, chara_conflict, maxlvl, max_stamina);
	}

	public static bool ArcadeBuyMedal(int cost, int amount, int cur_aseries, int cur_astage, int place, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_45_ABYM, cost, amount, cur_aseries, cur_astage, place, maxlvl, max_stamina);
	}

	public static bool ArcadeGetItem(int quantity, int cur_series, int cur_stage, int cur_aseries, int cur_astage, int category, int sub_category, int itemid, int get_type, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_46_AAPP, quantity, cur_series, cur_stage, cur_aseries, cur_astage, category, sub_category, itemid, get_type, maxlvl, max_stamina);
	}

	public static bool LinkBannerClick(int transition, int set_position, string img_name)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_47_LBCK, transition, set_position, img_name);
	}

	public static bool ArcadePlayPoint(int tgt_stage, int tgt_fmt, int tgt_rev, string tgt_seg, int evt_id, int use_stamina, int unique_id, int totalhp, int totalattack, int totaldefense, string use_charaID, string use_charaLvl, string use_charaSLvl, int course_id, int bonus_point_per, int bonus_chara_num, float bonus_enemy_rate, float bonus_enemy_rate_bonus, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_48_APLP, tgt_stage, tgt_fmt, tgt_rev, tgt_seg, evt_id, use_stamina, unique_id, totalhp, totalattack, totaldefense, use_charaID, use_charaLvl, use_charaSLvl, course_id, bonus_point_per, bonus_chara_num, bonus_enemy_rate, bonus_enemy_rate_bonus, maxlvl, max_stamina);
	}

	public static bool ArcadeEventWinRatePoint(int tgt_stage, int tgt_fmt, int tgt_rev, string tgt_seg, int evt_id, int won, int resulttype, int ptime, int pcnt, int fever_count, int max_combo, int stamina, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int current_wave, long total_enemyhp, string use_charaID, string use_charaMaxDamage, string use_charaSkill, int unique_id, int course_id, int has_bonus_enemy, int appear_bonus_enemy, int course_clear_num, int appear_bonus_enemy_num, int maxlvl, int max_stamina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_49_AEWINP, tgt_stage, tgt_fmt, tgt_rev, tgt_seg, evt_id, won, resulttype, ptime, pcnt, fever_count, max_combo, stamina, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, current_wave, total_enemyhp, use_charaID, use_charaMaxDamage, use_charaSkill, unique_id, course_id, has_bonus_enemy, appear_bonus_enemy, course_clear_num, appear_bonus_enemy_num, maxlvl, max_stamina);
	}

	public static bool ArcadeEventResetPoint(int evt_id, int course_id, int reset_num, int cur_aseries, int last_astage, int place, int lastarea_clear_num, int maxlvl, int max_statmina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_50_AERP, evt_id, course_id, reset_num, cur_aseries, last_astage, place, lastarea_clear_num, maxlvl, max_statmina);
	}

	public static bool ArcadeEventChallengeDialogPoint(int evt_id, int course_id, int cur_aseries, int last_astage, int result, int display_time, int maxlvl, int max_statmina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_51_AECDP, evt_id, course_id, cur_aseries, last_astage, result, display_time, maxlvl, max_statmina);
	}

	public static bool ArcadeEventGetPointPoint(int evt_id, int course_id, int cur_aseries, int cur_astage, int base_point, int defeat_enemy_point, int subtotal_point, int figure_point, int course_clear_point, int total_point, int bonus_point_per, int bonus_chara_num, int total_event_point, int maxlvl, int max_statmina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_52_AEGPP, evt_id, course_id, cur_aseries, cur_astage, base_point, defeat_enemy_point, subtotal_point, figure_point, course_clear_point, total_point, bonus_point_per, bonus_chara_num, total_event_point, maxlvl, max_statmina);
	}

	public static bool ArcadeEventCourseUnlockPoint(int evt_id, int course_id, int reset_num, int total_event_point, int appear_bonus_enemy, int maxlvl, int max_statmina)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_53_AECUP, evt_id, course_id, reset_num, total_event_point, appear_bonus_enemy, maxlvl, max_statmina);
	}

	public static bool GuidanceDialog(int cur_series, int cur_stage, int cur_aseries, int cur_astage, int source_mode, int source_evt_id, int dest_mode, int result, int display_count)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_54_GDG, cur_series, cur_stage, cur_aseries, cur_astage, source_mode, source_evt_id, dest_mode, result, display_count);
	}

	public static bool TapIconArcade(int evt_id, int cur_series, int cur_stage, int max_stage, int won, int try_num, int lose_num, int lose_continuous_num, int get_character, int heart, int result_stars)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_55_TIA, evt_id, cur_series, cur_stage, max_stage, won, try_num, lose_num, lose_continuous_num, get_character, heart, result_stars);
	}

	public static bool EventAppealStart(int evt_id, int appeal_type, int result, int skip, int skip_time, int play_time, int display_count)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_56_EAS, evt_id, appeal_type, result, skip, skip_time, play_time, display_count);
	}

	public static bool GemCampaign(int campaign_id, int purchase_user, int user_type, int purchase_time, int remain_time, int campaign_end)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_57_GEMCP, campaign_id, purchase_user, user_type, purchase_time, remain_time, campaign_end);
	}

	public static bool TapDeepLink(int transition, int page_no, int info_place, string banner_id, int trans_subid, int info_mode)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_58_TDL, transition, page_no, info_place, banner_id, trans_subid, info_mode);
	}

	public static bool SaleSpecialChance(int chance_id, int chance_type, int evt_id, int cur_series, int cur_stage, int display_count, int sale_num, int offer_num, int user_segment, int result, int first_purchase, int purchase_amount, string currency_code)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_59_SSC, chance_id, chance_type, evt_id, cur_series, cur_stage, display_count, sale_num, offer_num, user_segment, result, first_purchase, purchase_amount, currency_code);
	}

	public static bool DisPlayMode(int mode, string device_id, int place)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_60_DPM, mode, device_id, place);
	}

	public static bool EventBingo(int cur_stage, int cur_series, int evt_id, int sheet_no, int bng_no, int evt_bng, int evt_stars)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_61_EBNG, cur_stage, cur_series, evt_id, sheet_no, bng_no, evt_bng, evt_stars);
	}

	public static bool EventBingoLottery(int evt_id, int sheet_no, int old_stg, int new_stg, int use_item, int use_amount, int evt_bng, int evt_stars, int cur_stage, int cur_series, int heart, int ticket)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_62_EBLTY, evt_id, sheet_no, old_stg, new_stg, use_item, use_amount, evt_bng, evt_stars, cur_stage, cur_series, heart, ticket);
	}

	public static bool OldEvTopEnter(int enter_count, int character_num_sailor_base, int character_num_sailor_expand, int character_num_other_base, int character_num_other_expand, int key_num, int open_count_all, int key_count_all, int gem_count_all)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_63_OEENT, enter_count, character_num_sailor_base, character_num_sailor_expand, character_num_other_base, character_num_other_expand, key_num, open_count_all, key_count_all, gem_count_all);
	}

	public static bool OldEvOpen(int evt_id, int where_open, int offer_type, int use_item, int character_num_sailor_base, int character_num_sailor_expand, int character_num_other_base, int character_num_other_expand, int key_num, int use_mc, int use_sc, int use_key, int get_character, int stage_num, int evt_stars)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_64_OEOPN, evt_id, where_open, offer_type, use_item, character_num_sailor_base, character_num_sailor_expand, character_num_other_base, character_num_other_expand, key_num, use_mc, use_sc, use_key, get_character, stage_num, evt_stars);
	}

	public static bool OldEvGuidanceDialog(int result, int dest_mode, int enter_count, int character_num_sailor_base, int character_num_sailor_expand, int character_num_other_base, int character_num_other_expand, int key_num, int open_count_all, int key_count_all, int gem_count_all)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_65_OEGDG, result, dest_mode, enter_count, character_num_sailor_base, character_num_sailor_expand, character_num_other_base, character_num_other_expand, key_num, open_count_all, key_count_all, gem_count_all);
	}

	public static bool DailyChallengeWinRate(int tgt_stage, int cur_series, int evt_id, int sheet_no, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int make_stripe, int make_pop, int make_rainbow, int make_paint, int make_lunap, int erase_stripe, int erase_pop, int erase_rainbow, int erase_paint, int erase_lunap, int swap_stripe_stripe, int swap_stripe_pop, int swap_stripe_rainbow, int swap_stripe_paint, int swap_pop_pop, int swap_pop_rainbow, int swap_pop_paint, int swap_rainbow_rainbow, int swap_rainbow_paint, int swap_paint_paint)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_66_DCWIN, tgt_stage, cur_series, evt_id, sheet_no, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, make_stripe, make_pop, make_rainbow, make_paint, make_lunap, erase_stripe, erase_pop, erase_rainbow, erase_paint, erase_lunap, swap_stripe_stripe, swap_stripe_pop, swap_stripe_rainbow, swap_stripe_paint, swap_pop_pop, swap_pop_rainbow, swap_pop_paint, swap_rainbow_rainbow, swap_rainbow_paint, swap_paint_paint);
	}

	public static bool GemGuidanceDialog(int campaign_group_id, int iapId, int offer_start_time, int offer_num, int result)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_67_GEMGD, campaign_group_id, iapId, offer_start_time, offer_num, result);
	}

	public static bool GemPurchase(int campaign_group_id, int iapId, int remain_time, int campaign_end, int place, int offer_start_time, int offer_num, int sale_num)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_68_GEMPC, campaign_group_id, iapId, remain_time, campaign_end, place, offer_start_time, offer_num, sale_num);
	}

	public static bool ItemPossessionNumber(string item_num, string item_num_adv, string booster_num, string lvl_array, string lvl_array_adv)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_69_IPN, item_num, item_num_adv, booster_num, lvl_array, lvl_array_adv);
	}

	public static bool DataTransferDialog(int reason, string local_path, string local_path_adv, int local_size, int local_size_adv, int local_ver, int local_ver_adv, string local_searchid, int local_uuid, int local_bhiveid, string local_lvl_array, string local_lvl_array_adv, int local_xp, int local_xp_adv, int local_cur_series, int local_cur_stage, int local_cur_aseries, int local_cur_astage, int local_heart, int local_medal, string authkey)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_70_DTD, reason, local_path, local_path_adv, local_size, local_size_adv, local_ver, local_ver_adv, local_searchid, local_uuid, local_bhiveid, local_lvl_array, local_lvl_array_adv, local_xp, local_xp_adv, local_cur_series, local_cur_stage, local_cur_aseries, local_cur_astage, local_heart, local_medal, authkey);
	}

	public static bool DataTransferSelect(int reason, int result)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_71_DTS, reason, result);
	}

	public static bool DataRollBack(int savedata_kind, int remote_file_status, string local_path, string local_path_adv, int local_size, int local_size_adv, int local_ver, int local_ver_adv, string local_lvl_array, string local_lvl_array_adv, int local_xp, int local_xp_adv, int local_cur_series, int local_cur_stage, int local_cur_aseries, int local_cur_astage, int local_heart, int local_medal, int remote_size, int remote_size_adv, int remote_ver, int remote_ver_adv, string remote_lvl_array, string remote_lvl_array_adv, int remote_xp, int remote_xp_adv, int remote_cur_series, int remote_cur_stage, int remote_cur_aseries, int remote_cur_astage, int remote_heart, int remote_medal)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_72_DRB, savedata_kind, remote_file_status, local_path, local_path_adv, local_size, local_size_adv, local_ver, local_ver_adv, local_lvl_array, local_lvl_array_adv, local_xp, local_xp_adv, local_cur_series, local_cur_stage, local_cur_aseries, local_cur_astage, local_heart, local_medal, remote_size, remote_size_adv, remote_ver, remote_ver_adv, remote_lvl_array, remote_lvl_array_adv, remote_xp, remote_xp_adv, remote_cur_series, remote_cur_stage, remote_cur_aseries, remote_cur_astage, remote_heart, remote_medal);
	}

	public static bool OldEventWinRate(int tgt_stage, int evt_id, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int score, int gotstars, int pmoves, int ptime, int pcnt, int max_combo, int heart, int last_login, int game_start, int first_purchase, long purchase_count, long startup_count, long play_count, long total_win, long total_lose, float total_win_rate, int stage_win, int stage_lose, float stage_win_rate, int Character_ID, int Character_LVL, int Skill, int make_stripe, int make_pop, int make_rainbow, int make_paint, int make_lunap, int erase_stripe, int erase_pop, int erase_rainbow, int erase_paint, int erase_lunap, int swap_stripe_stripe, int swap_stripe_pop, int swap_stripe_rainbow, int swap_stripe_paint, int swap_pop_pop, int swap_pop_rainbow, int swap_pop_paint, int swap_rainbow_rainbow, int swap_rainbow_paint, int swap_paint_paint)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_73_OEWIN, tgt_stage, evt_id, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, score, gotstars, pmoves, ptime, pcnt, max_combo, heart, last_login, game_start, first_purchase, purchase_count, startup_count, play_count, total_win, total_lose, total_win_rate, stage_win, stage_lose, stage_win_rate, Character_ID, Character_LVL, Skill, make_stripe, make_pop, make_rainbow, make_paint, make_lunap, erase_stripe, erase_pop, erase_rainbow, erase_paint, erase_lunap, swap_stripe_stripe, swap_stripe_pop, swap_stripe_rainbow, swap_stripe_paint, swap_pop_pop, swap_pop_rainbow, swap_pop_paint, swap_rainbow_rainbow, swap_rainbow_paint, swap_paint_paint);
	}

	public static bool DifficultyChangeStart(int mode_id, int cur_series, int cur_stage, int segment_type)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_74_DCST, mode_id, cur_series, cur_stage, segment_type);
	}

	public static bool DataTransferResult(int reason, string local_path, string local_path_adv, int local_size, int local_size_adv, int local_ver, int local_ver_adv, string local_searchid, int local_uuid, int local_bhiveid, string local_lvl_array, string local_lvl_array_adv, int local_xp, int local_xp_adv, int local_cur_series, int local_cur_stage, int local_cur_aseries, int local_cur_astage, int local_heart, int local_medal, int remote_size, int remote_size_adv, int remote_ver, int remote_ver_adv, string remote_searchid, int remote_uuid, int remote_bhiveid, string remote_lvl_array, string remote_lvl_array_adv, int remote_xp, int remote_xp_adv, int remote_cur_series, int remote_cur_stage, int remote_cur_aseries, int remote_cur_astage, int remote_heart, int remote_medal)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_75_DTR, reason, local_path, local_path_adv, local_size, local_size_adv, local_ver, local_ver_adv, local_searchid, local_uuid, local_bhiveid, local_lvl_array, local_lvl_array_adv, local_xp, local_xp_adv, local_cur_series, local_cur_stage, local_cur_aseries, local_cur_astage, local_heart, local_medal, remote_size, remote_size_adv, remote_ver, remote_ver_adv, remote_searchid, remote_uuid, remote_bhiveid, remote_lvl_array, remote_lvl_array_adv, remote_xp, remote_xp_adv, remote_cur_series, remote_cur_stage, remote_cur_aseries, remote_cur_astage, remote_heart, remote_medal);
	}

	public static bool LevelRaiseCheck(int Character_ID, int old_level, int new_level, int old_levelup_num, int new_levelup_num, int decision_time)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_76_LVRC, Character_ID, old_level, new_level, old_levelup_num, new_levelup_num, decision_time);
	}

	public static bool LabyrinthEventBuyContinue(int cur_stage, int evt_id, int jewel_num, int sts_cnt, int jewelget_chance, int result, int total_jewel_num)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_77_ELCNT, cur_stage, evt_id, jewel_num, sts_cnt, jewelget_chance, result, total_jewel_num);
	}

	public static bool EventLabyrinthResult(int cur_stage, int evt_id, int jewel_num, int jewel_num_puzzle, int jewel_num_bouns, int jewel_num_doubleup, int jewel_num_stageclear, int total_jewel_num, int jewelget_chance, int doubleup_chance, int cost, float rate, int total_doubleup_num)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_78_ELRSLT, cur_stage, evt_id, jewel_num, jewel_num_puzzle, jewel_num_bouns, jewel_num_doubleup, jewel_num_stageclear, total_jewel_num, jewelget_chance, doubleup_chance, cost, rate, total_doubleup_num);
	}

	public static bool EventLabyrinthCourseReset(int course_id, int cur_series, int cur_stage, int evt_id, int reason, int total_jewel_num)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_79_ELCR, course_id, cur_series, cur_stage, evt_id, reason, total_jewel_num);
	}

	public static bool EventLabyrinthGetJewel(int course_id, int cur_series, int cur_stage, int evt_id, int place, int jewel_num, int jewel_num_puzzle, int jewel_num_bouns, int jewel_num_doubleup, int jewel_num_stageclear, int jewel_num_courseclear, int jewel_num_support, int jewel_num_stageallclear, int total_jewel_num)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_80_ELGJ, course_id, cur_series, cur_stage, evt_id, place, jewel_num, jewel_num_puzzle, jewel_num_bouns, jewel_num_doubleup, jewel_num_stageclear, jewel_num_courseclear, jewel_num_support, jewel_num_stageallclear, total_jewel_num);
	}

	public static bool EventLabyrinthGetReward(int accessory_id, int evt_id, int course1_goal_count, int course2_goal_count, int course3_goal_count, int course4_goal_count, int course5_goal_count, int course6_goal_count, int course7_goal_count, int course8_goal_count, int course9_goal_count, int course10_goal_count, int course11_goal_count, int course12_goal_count, int course13_goal_count, int total_stageclear_count, int total_doubleup_num, int total_jewel_num)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_81_ELRWD, accessory_id, evt_id, course1_goal_count, course2_goal_count, course3_goal_count, course4_goal_count, course5_goal_count, course6_goal_count, course7_goal_count, course8_goal_count, course9_goal_count, course10_goal_count, course11_goal_count, course12_goal_count, course13_goal_count, total_stageclear_count, total_doubleup_num, total_jewel_num);
	}

	public static bool StarGetWinRate(int tgt_stage, int origin_stage, int evt_id, int tgt_fmt, int tgt_rev, string tgt_seg, string tgt_mode, int won, int resulttype, int origin_score, int pmoves, int ptime, int pcnt, int max_combo, int first_purchase, int Character_ID, int Character_LVL, int Skill)
	{
		return ServerCramSend(CRAM_ID.CRAM_ID_82_SGWIN, tgt_stage, origin_stage, evt_id, tgt_fmt, tgt_rev, tgt_seg, tgt_mode, won, resulttype, origin_score, pmoves, ptime, pcnt, max_combo, first_purchase, Character_ID, Character_LVL, Skill);
	}

	public static string GetCramParams(CRAM_ID idx, params object[] args)
	{
		string text = string.Empty;
		switch (idx)
		{
		case CRAM_ID.CRAM_ID_1_TUT:
			text = text + ", \"status\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"cur_stage\":" + args[2].ToString();
			text = text + ", \"tut_v\":" + args[3].ToString();
			text = text + ", \"skip\":" + args[4].ToString();
			break;
		case CRAM_ID.CRAM_ID_2_BYH:
			text = text + ", \"cost\":" + args[0].ToString();
			text = text + ", \"amount\":" + args[1].ToString();
			text = text + ", \"place\":" + args[2].ToString();
			text = text + ", \"cur_series\":" + args[3].ToString();
			text = text + ", \"cur_stage\":" + args[4].ToString();
			break;
		case CRAM_ID.CRAM_ID_3_CNT:
			text = text + ", \"cost\":" + args[0].ToString();
			text = text + ", \"add_move\":" + args[1].ToString();
			text = text + ", \"add_time\":" + args[2].ToString();
			text = text + ", \"cond\":" + args[3].ToString();
			text = text + ", \"step_item\":" + args[4].ToString();
			text = text + ", \"step_item_num\":" + args[5].ToString();
			text = text + ", \"sts_cnt\":" + args[6].ToString();
			text = text + ", \"cur_series\":" + args[7].ToString();
			text = text + ", \"cur_stage\":" + args[8].ToString();
			text = text + ", \"stage_win\":" + args[9].ToString();
			text = text + ", \"stage_lose\":" + args[10].ToString();
			text = text + ", \"stage_win_rate\":" + args[11].ToString();
			text = text + ", \"purchase_count\":" + args[12].ToString();
			break;
		case CRAM_ID.CRAM_ID_4_RRB:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"cur_time\":" + args[1].ToString();
			text = text + ", \"cur_series\":" + args[2].ToString();
			break;
		case CRAM_ID.CRAM_ID_5_URB:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"unlock_by\":" + args[1].ToString();
			text = text + ", \"cur_time\":" + args[2].ToString();
			text = text + ", \"reach_time\":" + args[3].ToString();
			text = text + ", \"cur_series\":" + args[4].ToString();
			break;
		case CRAM_ID.CRAM_ID_6_SGI:
			text = text + ", \"friend_num\":" + args[0].ToString();
			text = text + ", \"item_kind\":" + args[1].ToString();
			text = text + ", \"item_id\":" + args[2].ToString();
			text = text + ", \"type\":" + args[3].ToString();
			break;
		case CRAM_ID.CRAM_ID_8_CUT:
			text = text + ", \"remote_time\":" + args[0].ToString();
			text = text + ", \"cur_time\":" + args[1].ToString();
			text = text + ", \"local_time\":" + args[2].ToString();
			text = text + ", \"cause\":" + args[3].ToString();
			break;
		case CRAM_ID.CRAM_ID_10_WIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"tgt_fmt\":" + args[2].ToString();
			text = text + ", \"tgt_rev\":" + args[3].ToString();
			text = text + ", \"tgt_seg\":\"" + args[4].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[5].ToString() + "\"";
			text = text + ", \"won\":" + args[6].ToString();
			text = text + ", \"resulttype\":" + args[7].ToString();
			text = text + ", \"score\":" + args[8].ToString();
			text = text + ", \"gotstars\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"make_stripe\":" + args[30].ToString();
			text = text + ", \"make_pop\":" + args[31].ToString();
			text = text + ", \"make_rainbow\":" + args[32].ToString();
			text = text + ", \"make_paint\":" + args[33].ToString();
			text = text + ", \"make_lunap\":" + args[34].ToString();
			text = text + ", \"erase_stripe\":" + args[35].ToString();
			text = text + ", \"erase_pop\":" + args[36].ToString();
			text = text + ", \"erase_rainbow\":" + args[37].ToString();
			text = text + ", \"erase_paint\":" + args[38].ToString();
			text = text + ", \"erase_lunap\":" + args[39].ToString();
			text = text + ", \"swap_stripe_stripe\":" + args[40].ToString();
			text = text + ", \"swap_stripe_pop\":" + args[41].ToString();
			text = text + ", \"swap_stripe_rainbow\":" + args[42].ToString();
			text = text + ", \"swap_stripe_paint\":" + args[43].ToString();
			text = text + ", \"swap_pop_pop\":" + args[44].ToString();
			text = text + ", \"swap_pop_rainbow\":" + args[45].ToString();
			text = text + ", \"swap_pop_paint\":" + args[46].ToString();
			text = text + ", \"swap_rainbow_rainbow\":" + args[47].ToString();
			text = text + ", \"swap_rainbow_paint\":" + args[48].ToString();
			text = text + ", \"swap_paint_paint\":" + args[49].ToString();
			break;
		case CRAM_ID.CRAM_ID_11_PST:
			text = text + ", \"st\":" + args[0].ToString();
			text = text + ", \"et\":" + args[1].ToString();
			text = text + ", \"pst\":" + args[2].ToString();
			text = text + ", \"device_id\":\"" + args[3].ToString() + "\"";
			text = text + ", \"osver\":\"" + args[4].ToString() + "\"";
			text = text + ", \"ot\":\"" + args[5].ToString() + "\"";
			text = text + ", \"fbid\":\"" + args[6].ToString() + "\"";
			text = text + ", \"gcid\":\"" + args[7].ToString() + "\"";
			text = text + ", \"gpid\":\"" + args[8].ToString() + "\"";
			text = text + ", \"twid\":\"" + args[9].ToString() + "\"";
			text = text + ", \"gpgsid\":\"" + args[10].ToString() + "\"";
			text = text + ", \"startup_count\":" + args[11].ToString();
			text = text + ", \"play_count\":" + args[12].ToString();
			text = text + ", \"first_purchase\":" + args[13].ToString();
			text = text + ", \"purchase_count\":" + args[14].ToString();
			text = text + ", \"total_win\":" + args[15].ToString();
			text = text + ", \"total_lose\":" + args[16].ToString();
			text = text + ", \"total_win_rate\":" + args[17].ToString();
			text = text + ", \"rootHack\":" + args[18].ToString();
			text = text + ", \"ic\":" + args[19].ToString();
			text = text + ", \"lvl_array\":\"" + args[20].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_12_EWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"evt_id\":" + args[1].ToString();
			text = text + ", \"tgt_fmt\":" + args[2].ToString();
			text = text + ", \"tgt_rev\":" + args[3].ToString();
			text = text + ", \"tgt_seg\":\"" + args[4].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[5].ToString() + "\"";
			text = text + ", \"won\":" + args[6].ToString();
			text = text + ", \"resulttype\":" + args[7].ToString();
			text = text + ", \"score\":" + args[8].ToString();
			text = text + ", \"gotstars\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"make_stripe\":" + args[30].ToString();
			text = text + ", \"make_pop\":" + args[31].ToString();
			text = text + ", \"make_rainbow\":" + args[32].ToString();
			text = text + ", \"make_paint\":" + args[33].ToString();
			text = text + ", \"make_lunap\":" + args[34].ToString();
			text = text + ", \"erase_stripe\":" + args[35].ToString();
			text = text + ", \"erase_pop\":" + args[36].ToString();
			text = text + ", \"erase_rainbow\":" + args[37].ToString();
			text = text + ", \"erase_paint\":" + args[38].ToString();
			text = text + ", \"erase_lunap\":" + args[39].ToString();
			text = text + ", \"swap_stripe_stripe\":" + args[40].ToString();
			text = text + ", \"swap_stripe_pop\":" + args[41].ToString();
			text = text + ", \"swap_stripe_rainbow\":" + args[42].ToString();
			text = text + ", \"swap_stripe_paint\":" + args[43].ToString();
			text = text + ", \"swap_pop_pop\":" + args[44].ToString();
			text = text + ", \"swap_pop_rainbow\":" + args[45].ToString();
			text = text + ", \"swap_pop_paint\":" + args[46].ToString();
			text = text + ", \"swap_rainbow_rainbow\":" + args[47].ToString();
			text = text + ", \"swap_rainbow_paint\":" + args[48].ToString();
			text = text + ", \"swap_paint_paint\":" + args[49].ToString();
			break;
		case CRAM_ID.CRAM_ID_13_LGI:
			text = text + ", \"gift_kind\":" + args[0].ToString();
			text = text + ", \"item_kind\":" + args[1].ToString();
			text = text + ", \"item_id\":" + args[2].ToString();
			text = text + ", \"amount\":" + args[3].ToString();
			text = text + ", \"got_time\":" + args[4].ToString();
			text = text + ", \"cur_series\":" + args[5].ToString();
			text = text + ", \"cur_stage\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_14_BDC:
			text = text + ", \"reason\":" + args[0].ToString();
			text = text + ", \"select_item\":" + args[1].ToString();
			text = text + ", \"path\":\"" + args[2].ToString() + "\"";
			text = text + ", \"size\":" + args[3].ToString();
			text = text + ", \"local_ver\":" + args[4].ToString();
			text = text + ", \"local_series\":" + args[5].ToString();
			text = text + ", \"local_lvl\":" + args[6].ToString();
			text = text + ", \"local_lvl_array\":\"" + args[7].ToString() + "\"";
			text = text + ", \"remote_ver\":" + args[8].ToString();
			text = text + ", \"remote_series\":" + args[9].ToString();
			text = text + ", \"remote_lvl\":" + args[10].ToString();
			text = text + ", \"remote_lvl_array\":\"" + args[11].ToString() + "\"";
			text = text + ", \"cur_series\":" + args[12].ToString();
			text = text + ", \"cur_stage\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			break;
		case CRAM_ID.CRAM_ID_15_FHLP:
			text = text + ", \"cur_series\":" + args[0].ToString();
			text = text + ", \"cur_stage\":" + args[1].ToString();
			text = text + ", \"stage_stars\":" + args[2].ToString();
			text = text + ", \"stage_win\":" + args[3].ToString();
			text = text + ", \"stage_lose\":" + args[4].ToString();
			text = text + ", \"stage_win_rate\":" + args[5].ToString();
			break;
		case CRAM_ID.CRAM_ID_16_FWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"tgt_fmt\":" + args[2].ToString();
			text = text + ", \"tgt_rev\":" + args[3].ToString();
			text = text + ", \"tgt_seg\":\"" + args[4].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[5].ToString() + "\"";
			text = text + ", \"won\":" + args[6].ToString();
			text = text + ", \"resulttype\":" + args[7].ToString();
			text = text + ", \"score\":" + args[8].ToString();
			text = text + ", \"gotstars\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"friendid\":" + args[30].ToString();
			break;
		case CRAM_ID.CRAM_ID_17_GBST:
			text = text + ", \"amount\":" + args[0].ToString();
			text = text + ", \"cost\":" + args[1].ToString();
			text = text + ", \"cur_series\":" + args[2].ToString();
			text = text + ", \"cur_stage\":" + args[3].ToString();
			text = text + ", \"get_type\":" + args[4].ToString();
			text = text + ", \"kind\":" + args[5].ToString();
			text = text + ", \"boost\":\"" + args[6].ToString() + "\"";
			text = text + ", \"total_win\":" + args[7].ToString();
			text = text + ", \"total_lose\":" + args[8].ToString();
			text = text + ", \"total_win_rate\":" + args[9].ToString();
			text = text + ", \"stage_win\":" + args[10].ToString();
			text = text + ", \"stage_lose\":" + args[11].ToString();
			text = text + ", \"stage_win_rate\":" + args[12].ToString();
			text = text + ", \"purchase_count\":" + args[13].ToString();
			break;
		case CRAM_ID.CRAM_ID_18_GACCE:
			text = text + ", \"accessory_id\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"get_type\":" + args[2].ToString();
			break;
		case CRAM_ID.CRAM_ID_19_UBST:
			text = text + ", \"amount\":" + args[0].ToString();
			text = text + ", \"amount_free\":" + args[1].ToString();
			text = text + ", \"amount_paid\":" + args[2].ToString();
			text = text + ", \"tgt_stage\":" + args[3].ToString();
			text = text + ", \"cur_series\":" + args[4].ToString();
			text = text + ", \"use_type\":" + args[5].ToString();
			text = text + ", \"kind\":" + args[6].ToString();
			text = text + ", \"won\":" + args[7].ToString();
			text = text + ", \"play_count\":" + args[8].ToString();
			text = text + ", \"boost\":\"" + args[9].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_20_CTUT:
			text = text + ", \"skip\":" + args[0].ToString();
			break;
		case CRAM_ID.CRAM_ID_21_CLU:
			text = text + ", \"Character_ID\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"cur_stage\":" + args[2].ToString();
			text = text + ", \"cur_aseries\":" + args[3].ToString();
			text = text + ", \"cur_astage\":" + args[4].ToString();
			text = text + ", \"origin_lvl\":" + args[5].ToString();
			text = text + ", \"update_lvl\":" + args[6].ToString();
			text = text + ", \"Character_status\":\"" + args[7].ToString() + "\"";
			text = text + ", \"place\":" + args[8].ToString();
			break;
		case CRAM_ID.CRAM_ID_22_DWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"tgt_fmt\":" + args[2].ToString();
			text = text + ", \"tgt_rev\":" + args[3].ToString();
			text = text + ", \"tgt_seg\":\"" + args[4].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[5].ToString() + "\"";
			text = text + ", \"won\":" + args[6].ToString();
			text = text + ", \"resulttype\":" + args[7].ToString();
			text = text + ", \"score\":" + args[8].ToString();
			text = text + ", \"gotstars\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"clear_score\":" + args[30].ToString();
			text = text + ", \"clear_stars\":" + args[31].ToString();
			text = text + ", \"limit_moves\":" + args[32].ToString();
			break;
		case CRAM_ID.CRAM_ID_23_PNR:
			text = text + ", \"NotificationId\":" + args[0].ToString();
			break;
		case CRAM_ID.CRAM_ID_24_DEWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"evt_id\":" + args[1].ToString();
			text = text + ", \"tgt_fmt\":" + args[2].ToString();
			text = text + ", \"tgt_rev\":" + args[3].ToString();
			text = text + ", \"tgt_seg\":\"" + args[4].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[5].ToString() + "\"";
			text = text + ", \"won\":" + args[6].ToString();
			text = text + ", \"resulttype\":" + args[7].ToString();
			text = text + ", \"score\":" + args[8].ToString();
			text = text + ", \"gotstars\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"clear_score\":" + args[30].ToString();
			text = text + ", \"clear_stars\":" + args[31].ToString();
			text = text + ", \"limit_moves\":" + args[32].ToString();
			break;
		case CRAM_ID.CRAM_ID_25_DAU:
			text = text + ", \"st\":" + args[0].ToString();
			text = text + ", \"device_id\":\"" + args[1].ToString() + "\"";
			text = text + ", \"osver\":\"" + args[2].ToString() + "\"";
			text = text + ", \"startup_count\":" + args[3].ToString();
			text = text + ", \"play_count\":" + args[4].ToString();
			text = text + ", \"first_purchase\":" + args[5].ToString();
			text = text + ", \"purchase_count\":" + args[6].ToString();
			text = text + ", \"purchase_amount\":" + args[7].ToString();
			text = text + ", \"currency_code\":\"" + args[8].ToString() + "\"";
			text = text + ", \"boost\":\"" + args[9].ToString() + "\"";
			text = text + ", \"buy_heart_count\":" + args[10].ToString();
			text = text + ", \"buy_boost_count\":" + args[11].ToString();
			text = text + ", \"buy_continue_count\":" + args[12].ToString();
			text = text + ", \"buy_rb_count\":" + args[13].ToString();
			text = text + ", \"lvl_array\":\"" + args[14].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_26_DAYWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"tgt_fmt\":" + args[2].ToString();
			text = text + ", \"tgt_rev\":" + args[3].ToString();
			text = text + ", \"tgt_seg\":\"" + args[4].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[5].ToString() + "\"";
			text = text + ", \"won\":" + args[6].ToString();
			text = text + ", \"resulttype\":" + args[7].ToString();
			text = text + ", \"score\":" + args[8].ToString();
			text = text + ", \"gotstars\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"accessory_id\":" + args[30].ToString();
			break;
		case CRAM_ID.CRAM_ID_27_SKST:
			text = text + ", \"cost\":" + args[0].ToString();
			text = text + ", \"cond\":" + args[1].ToString();
			text = text + ", \"sts_lose\":" + args[2].ToString();
			text = text + ", \"status\":" + args[3].ToString();
			text = text + ", \"cur_series\":" + args[4].ToString();
			text = text + ", \"cur_stage\":" + args[5].ToString();
			text = text + ", \"stage_win\":" + args[6].ToString();
			text = text + ", \"stage_lose\":" + args[7].ToString();
			text = text + ", \"stage_win_rate\":" + args[8].ToString();
			break;
		case CRAM_ID.CRAM_ID_28_RTM:
			text = text + ", \"mission_id\":" + args[0].ToString();
			text = text + ", \"mission_lvl\":" + args[1].ToString();
			text = text + ", \"purchase_count\":" + args[2].ToString();
			break;
		case CRAM_ID.CRAM_ID_29_GTRP:
			text = text + ", \"get_num\":" + args[0].ToString();
			text = text + ", \"total_num\":" + args[1].ToString();
			text = text + ", \"get_type\":" + args[2].ToString();
			text = text + ", \"sub_type\":" + args[3].ToString();
			text = text + ", \"purchase_count\":" + args[4].ToString();
			break;
		case CRAM_ID.CRAM_ID_30_EXCTRP:
			text = text + ", \"item_id\":" + args[0].ToString();
			text = text + ", \"use_num\":" + args[1].ToString();
			text = text + ", \"total_num\":" + args[2].ToString();
			text = text + ", \"exchange_cnt\":" + args[3].ToString();
			text = text + ", \"total_exchange_cnt\":" + args[4].ToString();
			text = text + ", \"purchase_count\":" + args[5].ToString();
			break;
		case CRAM_ID.CRAM_ID_31_INFH:
			text = text + ", \"playcount\":" + args[0].ToString();
			text = text + ", \"win\":" + args[1].ToString();
			text = text + ", \"lose\":" + args[2].ToString();
			text = text + ", \"duration\":" + args[3].ToString();
			text = text + ", \"cur_series\":" + args[4].ToString();
			text = text + ", \"cur_stage\":" + args[5].ToString();
			text = text + ", \"heart\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_32_LBR:
			text = text + ", \"cur_series\":" + args[0].ToString();
			text = text + ", \"cur_event\":" + args[1].ToString();
			text = text + ", \"bonus_id\":" + args[2].ToString();
			text = text + ", \"login_category\":\"" + args[3].ToString() + "\"";
			text = text + ", \"sheet\":" + args[4].ToString();
			text = text + ", \"login_daycount\":" + args[5].ToString();
			text = text + ", \"item_reward_id\":" + args[6].ToString();
			text = text + ", \"item_bonus_id\":" + args[7].ToString();
			text = text + ", \"item_category\":" + args[8].ToString();
			text = text + ", \"item_sub_category\":" + args[9].ToString();
			text = text + ", \"item_quantity\":" + args[10].ToString();
			break;
		case CRAM_ID.CRAM_ID_33_AGFI:
			text = text + ", \"Character_ID\":" + args[0].ToString();
			text = text + ", \"place\":" + args[1].ToString();
			text = text + ", \"maxlvl\":" + args[2].ToString();
			text = text + ", \"max_stamina\":" + args[3].ToString();
			break;
		case CRAM_ID.CRAM_ID_34_ACLU:
			text = text + ", \"Character_ID\":" + args[0].ToString();
			text = text + ", \"use_xp\":" + args[1].ToString();
			text = text + ", \"cur_series\":" + args[2].ToString();
			text = text + ", \"cur_stage\":" + args[3].ToString();
			text = text + ", \"cur_aseries\":" + args[4].ToString();
			text = text + ", \"cur_astage\":" + args[5].ToString();
			text = text + ", \"origin_lvl\":" + args[6].ToString();
			text = text + ", \"update_lvl\":" + args[7].ToString();
			text = text + ", \"get_type\":" + args[8].ToString();
			text = text + ", \"maxlvl\":" + args[9].ToString();
			text = text + ", \"max_stamina\":" + args[10].ToString();
			break;
		case CRAM_ID.CRAM_ID_35_ACSLU:
			text = text + ", \"Character_ID\":" + args[0].ToString();
			text = text + ", \"use_skillitem\":" + args[1].ToString();
			text = text + ", \"cur_series\":" + args[2].ToString();
			text = text + ", \"cur_stage\":" + args[3].ToString();
			text = text + ", \"cur_aseries\":" + args[4].ToString();
			text = text + ", \"cur_astage\":" + args[5].ToString();
			text = text + ", \"origin_lvl\":" + args[6].ToString();
			text = text + ", \"update_lvl\":" + args[7].ToString();
			text = text + ", \"get_type\":" + args[8].ToString();
			text = text + ", \"maxlvl\":" + args[9].ToString();
			text = text + ", \"max_stamina\":" + args[10].ToString();
			break;
		case CRAM_ID.CRAM_ID_36_APL:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"tgt_fmt\":" + args[1].ToString();
			text = text + ", \"tgt_rev\":" + args[2].ToString();
			text = text + ", \"tgt_seg\":\"" + args[3].ToString() + "\"";
			text = text + ", \"evt_id\":" + args[4].ToString();
			text = text + ", \"use_stamina\":" + args[5].ToString();
			text = text + ", \"unique_id\":" + args[6].ToString();
			text = text + ", \"totalhp\":" + args[7].ToString();
			text = text + ", \"totalattack\":" + args[8].ToString();
			text = text + ", \"totaldefense\":" + args[9].ToString();
			text = text + ", \"use_charaID\":\"" + args[10].ToString() + "\"";
			text = text + ", \"use_charaLvl\":\"" + args[11].ToString() + "\"";
			text = text + ", \"use_charaSLvl\":\"" + args[12].ToString() + "\"";
			text = text + ", \"maxlvl\":" + args[13].ToString();
			text = text + ", \"max_stamina\":" + args[14].ToString();
			break;
		case CRAM_ID.CRAM_ID_37_AWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"tgt_fmt\":" + args[1].ToString();
			text = text + ", \"tgt_rev\":" + args[2].ToString();
			text = text + ", \"tgt_seg\":\"" + args[3].ToString() + "\"";
			text = text + ", \"evt_id\":" + args[4].ToString();
			text = text + ", \"won\":" + args[5].ToString();
			text = text + ", \"resulttype\":" + args[6].ToString();
			text = text + ", \"ptime\":" + args[7].ToString();
			text = text + ", \"pcnt\":" + args[8].ToString();
			text = text + ", \"fever_count\":" + args[9].ToString();
			text = text + ", \"max_combo\":" + args[10].ToString();
			text = text + ", \"stamina\":" + args[11].ToString();
			text = text + ", \"last_login\":" + args[12].ToString();
			text = text + ", \"game_start\":" + args[13].ToString();
			text = text + ", \"first_purchase\":" + args[14].ToString();
			text = text + ", \"purchase_count\":" + args[15].ToString();
			text = text + ", \"startup_count\":" + args[16].ToString();
			text = text + ", \"play_count\":" + args[17].ToString();
			text = text + ", \"total_win\":" + args[18].ToString();
			text = text + ", \"total_lose\":" + args[19].ToString();
			text = text + ", \"total_win_rate\":" + args[20].ToString();
			text = text + ", \"stage_win\":" + args[21].ToString();
			text = text + ", \"stage_lose\":" + args[22].ToString();
			text = text + ", \"stage_win_rate\":" + args[23].ToString();
			text = text + ", \"current_wave\":" + args[24].ToString();
			text = text + ", \"total_enemyhp\":" + args[25].ToString();
			text = text + ", \"use_charaID\":\"" + args[26].ToString() + "\"";
			text = text + ", \"use_charaMaxDamage\":\"" + args[27].ToString() + "\"";
			text = text + ", \"use_charaSkill\":\"" + args[28].ToString() + "\"";
			text = text + ", \"unique_id\":" + args[29].ToString();
			text = text + ", \"maxlvl\":" + args[30].ToString();
			text = text + ", \"max_stamina\":" + args[31].ToString();
			break;
		case CRAM_ID.CRAM_ID_38_ACNT:
			text = text + ", \"cost\":" + args[0].ToString();
			text = text + ", \"add_hp\":" + args[1].ToString();
			text = text + ", \"sts_cnt\":" + args[2].ToString();
			text = text + ", \"fever_count\":" + args[3].ToString();
			text = text + ", \"evt_id\":" + args[4].ToString();
			text = text + ", \"cur_aseries\":" + args[5].ToString();
			text = text + ", \"cur_astage\":" + args[6].ToString();
			text = text + ", \"stage_win\":" + args[7].ToString();
			text = text + ", \"stage_lose\":" + args[8].ToString();
			text = text + ", \"stage_win_rate\":" + args[9].ToString();
			text = text + ", \"purchase_count\":" + args[10].ToString();
			text = text + ", \"wave_num\":" + args[11].ToString();
			text = text + ", \"unique_id\":" + args[12].ToString();
			text = text + ", \"total_enemyhp\":" + args[13].ToString();
			text = text + ", \"enemyID\":\"" + args[14].ToString() + "\"";
			text = text + ", \"enemyHP\":\"" + args[15].ToString() + "\"";
			text = text + ", \"enemyATK\":\"" + args[16].ToString() + "\"";
			text = text + ", \"enemyDEF\":\"" + args[17].ToString() + "\"";
			text = text + ", \"maxlvl\":" + args[18].ToString();
			text = text + ", \"max_stamina\":" + args[19].ToString();
			break;
		case CRAM_ID.CRAM_ID_39_AEWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"tgt_fmt\":" + args[1].ToString();
			text = text + ", \"tgt_rev\":" + args[2].ToString();
			text = text + ", \"tgt_seg\":\"" + args[3].ToString() + "\"";
			text = text + ", \"evt_id\":" + args[4].ToString();
			text = text + ", \"won\":" + args[5].ToString();
			text = text + ", \"resulttype\":" + args[6].ToString();
			text = text + ", \"ptime\":" + args[7].ToString();
			text = text + ", \"pcnt\":" + args[8].ToString();
			text = text + ", \"fever_count\":" + args[9].ToString();
			text = text + ", \"max_combo\":" + args[10].ToString();
			text = text + ", \"stamina\":" + args[11].ToString();
			text = text + ", \"last_login\":" + args[12].ToString();
			text = text + ", \"game_start\":" + args[13].ToString();
			text = text + ", \"first_purchase\":" + args[14].ToString();
			text = text + ", \"purchase_count\":" + args[15].ToString();
			text = text + ", \"startup_count\":" + args[16].ToString();
			text = text + ", \"play_count\":" + args[17].ToString();
			text = text + ", \"total_win\":" + args[18].ToString();
			text = text + ", \"total_lose\":" + args[19].ToString();
			text = text + ", \"total_win_rate\":" + args[20].ToString();
			text = text + ", \"stage_win\":" + args[21].ToString();
			text = text + ", \"stage_lose\":" + args[22].ToString();
			text = text + ", \"stage_win_rate\":" + args[23].ToString();
			text = text + ", \"current_wave\":" + args[24].ToString();
			text = text + ", \"total_enemyhp\":" + args[25].ToString();
			text = text + ", \"use_charaID\":\"" + args[26].ToString() + "\"";
			text = text + ", \"use_charaMaxDamage\":\"" + args[27].ToString() + "\"";
			text = text + ", \"use_charaSkill\":\"" + args[28].ToString() + "\"";
			text = text + ", \"unique_id\":" + args[29].ToString();
			text = text + ", \"maxlvl\":" + args[30].ToString();
			text = text + ", \"max_stamina\":" + args[31].ToString();
			break;
		case CRAM_ID.CRAM_ID_40_AGACCE:
			text = text + ", \"accessory_id\":" + args[0].ToString();
			text = text + ", \"cur_aseries\":" + args[1].ToString();
			text = text + ", \"cur_astage\":" + args[2].ToString();
			text = text + ", \"evt_id\":" + args[3].ToString();
			text = text + ", \"get_type\":" + args[4].ToString();
			text = text + ", \"maxlvl\":" + args[5].ToString();
			text = text + ", \"max_stamina\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_41_ADAU:
			text = text + ", \"st\":" + args[0].ToString();
			text = text + ", \"device_id\":\"" + args[1].ToString() + "\"";
			text = text + ", \"osver\":\"" + args[2].ToString() + "\"";
			text = text + ", \"startup_count\":" + args[3].ToString();
			text = text + ", \"play_count\":" + args[4].ToString();
			text = text + ", \"first_purchase\":" + args[5].ToString();
			text = text + ", \"purchase_count\":" + args[6].ToString();
			text = text + ", \"purchase_amount\":" + args[7].ToString();
			text = text + ", \"currency_code\":\"" + args[8].ToString() + "\"";
			text = text + ", \"abooster\":\"" + args[9].ToString() + "\"";
			text = text + ", \"buy_medal_count\":" + args[10].ToString();
			text = text + ", \"buy_item_count\":" + args[11].ToString();
			text = text + ", \"buy_continue_count\":" + args[12].ToString();
			text = text + ", \"buy_gasya_count\":" + args[13].ToString();
			text = text + ", \"figure_num\":" + args[14].ToString();
			text = text + ", \"aplayd\":" + args[15].ToString();
			text = text + ", \"aintervald\":" + args[16].ToString();
			text = text + ", \"acontinuousd\":" + args[17].ToString();
			text = text + ", \"maxlvl\":" + args[18].ToString();
			text = text + ", \"max_stamina\":" + args[19].ToString();
			break;
		case CRAM_ID.CRAM_ID_42_MDAU:
			text = text + ", \"st\":" + args[0].ToString();
			text = text + ", \"device_id\":\"" + args[1].ToString() + "\"";
			text = text + ", \"osver\":\"" + args[2].ToString() + "\"";
			text = text + ", \"startup_count\":" + args[3].ToString();
			text = text + ", \"play_count\":" + args[4].ToString();
			text = text + ", \"first_purchase\":" + args[5].ToString();
			text = text + ", \"purchase_count\":" + args[6].ToString();
			text = text + ", \"purchase_amount\":" + args[7].ToString();
			text = text + ", \"currency_code\":\"" + args[8].ToString() + "\"";
			text = text + ", \"boost\":\"" + args[9].ToString() + "\"";
			text = text + ", \"buy_heart_count\":" + args[10].ToString();
			text = text + ", \"buy_boost_count\":" + args[11].ToString();
			text = text + ", \"buy_continue_count\":" + args[12].ToString();
			text = text + ", \"buy_rb_count\":" + args[13].ToString();
			text = text + ", \"lvl_array\":\"" + args[14].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_43_ATUT:
			text = text + ", \"status\":" + args[0].ToString();
			text = text + ", \"cur_aseries\":" + args[1].ToString();
			text = text + ", \"cur_astage\":" + args[2].ToString();
			text = text + ", \"tut_v\":" + args[3].ToString();
			text = text + ", \"maxlvl\":" + args[4].ToString();
			text = text + ", \"max_stamina\":" + args[5].ToString();
			break;
		case CRAM_ID.CRAM_ID_44_AGR:
			text = text + ", \"chara_result\":\"" + args[0].ToString() + "\"";
			text = text + ", \"gasya_id\":" + args[1].ToString();
			text = text + ", \"cost_type\":" + args[2].ToString();
			text = text + ", \"cost\":" + args[3].ToString();
			text = text + ", \"total_chara\":" + args[4].ToString();
			text = text + ", \"gasya_type\":" + args[5].ToString();
			text = text + ", \"chara_conflict\":" + args[6].ToString();
			text = text + ", \"maxlvl\":" + args[7].ToString();
			text = text + ", \"max_stamina\":" + args[8].ToString();
			break;
		case CRAM_ID.CRAM_ID_45_ABYM:
			text = text + ", \"cost\":" + args[0].ToString();
			text = text + ", \"amount\":" + args[1].ToString();
			text = text + ", \"cur_aseries\":" + args[2].ToString();
			text = text + ", \"cur_astage\":" + args[3].ToString();
			text = text + ", \"place\":" + args[4].ToString();
			text = text + ", \"maxlvl\":" + args[5].ToString();
			text = text + ", \"max_stamina\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_46_AAPP:
			text = text + ", \"quantity\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"cur_stage\":" + args[2].ToString();
			text = text + ", \"cur_aseries\":" + args[3].ToString();
			text = text + ", \"cur_astage\":" + args[4].ToString();
			text = text + ", \"category\":" + args[5].ToString();
			text = text + ", \"sub_category\":" + args[6].ToString();
			text = text + ", \"itemid\":" + args[7].ToString();
			text = text + ", \"get_type\":" + args[8].ToString();
			text = text + ", \"maxlvl\":" + args[9].ToString();
			text = text + ", \"max_stamina\":" + args[10].ToString();
			break;
		case CRAM_ID.CRAM_ID_47_LBCK:
			text = text + ", \"transition\":" + args[0].ToString();
			text = text + ", \"set_position\":" + args[1].ToString();
			text = text + ", \"img_name\":\"" + args[2].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_48_APLP:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"tgt_fmt\":" + args[1].ToString();
			text = text + ", \"tgt_rev\":" + args[2].ToString();
			text = text + ", \"tgt_seg\":\"" + args[3].ToString() + "\"";
			text = text + ", \"evt_id\":" + args[4].ToString();
			text = text + ", \"use_stamina\":" + args[5].ToString();
			text = text + ", \"unique_id\":" + args[6].ToString();
			text = text + ", \"totalhp\":" + args[7].ToString();
			text = text + ", \"totalattack\":" + args[8].ToString();
			text = text + ", \"totaldefense\":" + args[9].ToString();
			text = text + ", \"use_charaID\":\"" + args[10].ToString() + "\"";
			text = text + ", \"use_charaLvl\":\"" + args[11].ToString() + "\"";
			text = text + ", \"use_charaSLvl\":\"" + args[12].ToString() + "\"";
			text = text + ", \"course_id\":" + args[13].ToString();
			text = text + ", \"bonus_point_per\":" + args[14].ToString();
			text = text + ", \"bonus_chara_num\":" + args[15].ToString();
			text = text + ", \"bonus_enemy_rate\":" + args[16].ToString();
			text = text + ", \"bonus_enemy_rate_bonus\":" + args[17].ToString();
			text = text + ", \"maxlvl\":" + args[18].ToString();
			text = text + ", \"max_stamina\":" + args[19].ToString();
			break;
		case CRAM_ID.CRAM_ID_49_AEWINP:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"tgt_fmt\":" + args[1].ToString();
			text = text + ", \"tgt_rev\":" + args[2].ToString();
			text = text + ", \"tgt_seg\":\"" + args[3].ToString() + "\"";
			text = text + ", \"evt_id\":" + args[4].ToString();
			text = text + ", \"won\":" + args[5].ToString();
			text = text + ", \"resulttype\":" + args[6].ToString();
			text = text + ", \"ptime\":" + args[7].ToString();
			text = text + ", \"pcnt\":" + args[8].ToString();
			text = text + ", \"fever_count\":" + args[9].ToString();
			text = text + ", \"max_combo\":" + args[10].ToString();
			text = text + ", \"stamina\":" + args[11].ToString();
			text = text + ", \"last_login\":" + args[12].ToString();
			text = text + ", \"game_start\":" + args[13].ToString();
			text = text + ", \"first_purchase\":" + args[14].ToString();
			text = text + ", \"purchase_count\":" + args[15].ToString();
			text = text + ", \"startup_count\":" + args[16].ToString();
			text = text + ", \"play_count\":" + args[17].ToString();
			text = text + ", \"total_win\":" + args[18].ToString();
			text = text + ", \"total_lose\":" + args[19].ToString();
			text = text + ", \"total_win_rate\":" + args[20].ToString();
			text = text + ", \"stage_win\":" + args[21].ToString();
			text = text + ", \"stage_lose\":" + args[22].ToString();
			text = text + ", \"stage_win_rate\":" + args[23].ToString();
			text = text + ", \"current_wave\":" + args[24].ToString();
			text = text + ", \"total_enemyhp\":" + args[25].ToString();
			text = text + ", \"use_charaID\":\"" + args[26].ToString() + "\"";
			text = text + ", \"use_charaMaxDamage\":\"" + args[27].ToString() + "\"";
			text = text + ", \"use_charaSkill\":\"" + args[28].ToString() + "\"";
			text = text + ", \"unique_id\":" + args[29].ToString();
			text = text + ", \"course_id\":" + args[30].ToString();
			text = text + ", \"has_bonus_enemy\":" + args[31].ToString();
			text = text + ", \"appear_bonus_enemy\":" + args[32].ToString();
			text = text + ", \"course_clear_num\":" + args[33].ToString();
			text = text + ", \"appear_bonus_enemy_num\":" + args[34].ToString();
			text = text + ", \"maxlvl\":" + args[35].ToString();
			text = text + ", \"max_stamina\":" + args[36].ToString();
			break;
		case CRAM_ID.CRAM_ID_50_AERP:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"course_id\":" + args[1].ToString();
			text = text + ", \"reset_num\":" + args[2].ToString();
			text = text + ", \"cur_aseries\":" + args[3].ToString();
			text = text + ", \"last_astage\":" + args[4].ToString();
			text = text + ", \"place\":" + args[5].ToString();
			text = text + ", \"lastarea_clear_num\":" + args[6].ToString();
			text = text + ", \"maxlvl\":" + args[7].ToString();
			text = text + ", \"max_statmina\":" + args[8].ToString();
			break;
		case CRAM_ID.CRAM_ID_51_AECDP:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"course_id\":" + args[1].ToString();
			text = text + ", \"cur_aseries\":" + args[2].ToString();
			text = text + ", \"last_astage\":" + args[3].ToString();
			text = text + ", \"result\":" + args[4].ToString();
			text = text + ", \"display_time\":" + args[5].ToString();
			text = text + ", \"maxlvl\":" + args[6].ToString();
			text = text + ", \"max_statmina\":" + args[7].ToString();
			break;
		case CRAM_ID.CRAM_ID_52_AEGPP:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"course_id\":" + args[1].ToString();
			text = text + ", \"cur_aseries\":" + args[2].ToString();
			text = text + ", \"cur_astage\":" + args[3].ToString();
			text = text + ", \"base_point\":" + args[4].ToString();
			text = text + ", \"defeat_enemy_point\":" + args[5].ToString();
			text = text + ", \"subtotal_point\":" + args[6].ToString();
			text = text + ", \"figure_point\":" + args[7].ToString();
			text = text + ", \"course_clear_point\":" + args[8].ToString();
			text = text + ", \"total_point\":" + args[9].ToString();
			text = text + ", \"bonus_point_per\":" + args[10].ToString();
			text = text + ", \"bonus_chara_num\":" + args[11].ToString();
			text = text + ", \"total_event_point\":" + args[12].ToString();
			text = text + ", \"maxlvl\":" + args[13].ToString();
			text = text + ", \"max_statmina\":" + args[14].ToString();
			break;
		case CRAM_ID.CRAM_ID_53_AECUP:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"course_id\":" + args[1].ToString();
			text = text + ", \"reset_num\":" + args[2].ToString();
			text = text + ", \"total_event_point\":" + args[3].ToString();
			text = text + ", \"appear_bonus_enemy\":" + args[4].ToString();
			text = text + ", \"maxlvl\":" + args[5].ToString();
			text = text + ", \"max_statmina\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_54_GDG:
			text = text + ", \"cur_series\":" + args[0].ToString();
			text = text + ", \"cur_stage\":" + args[1].ToString();
			text = text + ", \"cur_aseries\":" + args[2].ToString();
			text = text + ", \"cur_astage\":" + args[3].ToString();
			text = text + ", \"source_mode\":" + args[4].ToString();
			text = text + ", \"source_evt_id\":" + args[5].ToString();
			text = text + ", \"dest_mode\":" + args[6].ToString();
			text = text + ", \"result\":" + args[7].ToString();
			text = text + ", \"display_count\":" + args[8].ToString();
			break;
		case CRAM_ID.CRAM_ID_55_TIA:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"cur_stage\":" + args[2].ToString();
			text = text + ", \"max_stage\":" + args[3].ToString();
			text = text + ", \"won\":" + args[4].ToString();
			text = text + ", \"try_num\":" + args[5].ToString();
			text = text + ", \"lose_num\":" + args[6].ToString();
			text = text + ", \"lose_continuous_num\":" + args[7].ToString();
			text = text + ", \"get_character\":" + args[8].ToString();
			text = text + ", \"heart\":" + args[9].ToString();
			text = text + ", \"result_stars\":" + args[10].ToString();
			break;
		case CRAM_ID.CRAM_ID_56_EAS:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"appeal_type\":" + args[1].ToString();
			text = text + ", \"result\":" + args[2].ToString();
			text = text + ", \"skip\":" + args[3].ToString();
			text = text + ", \"skip_time\":" + args[4].ToString();
			text = text + ", \"play_time\":" + args[5].ToString();
			text = text + ", \"display_count\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_57_GEMCP:
			text = text + ", \"campaign_id\":" + args[0].ToString();
			text = text + ", \"purchase_user\":" + args[1].ToString();
			text = text + ", \"user_type\":" + args[2].ToString();
			text = text + ", \"purchase_time\":" + args[3].ToString();
			text = text + ", \"remain_time\":" + args[4].ToString();
			text = text + ", \"campaign_end\":" + args[5].ToString();
			break;
		case CRAM_ID.CRAM_ID_58_TDL:
			text = text + ", \"transition\":" + args[0].ToString();
			text = text + ", \"page_no\":" + args[1].ToString();
			text = text + ", \"info_place\":" + args[2].ToString();
			text = text + ", \"banner_id\":\"" + args[3].ToString() + "\"";
			text = text + ", \"trans_subid\":" + args[4].ToString();
			text = text + ", \"info_mode\":" + args[5].ToString();
			break;
		case CRAM_ID.CRAM_ID_59_SSC:
			text = text + ", \"chance_id\":" + args[0].ToString();
			text = text + ", \"chance_type\":" + args[1].ToString();
			text = text + ", \"evt_id\":" + args[2].ToString();
			text = text + ", \"cur_series\":" + args[3].ToString();
			text = text + ", \"cur_stage\":" + args[4].ToString();
			text = text + ", \"display_count\":" + args[5].ToString();
			text = text + ", \"sale_num\":" + args[6].ToString();
			text = text + ", \"offer_num\":" + args[7].ToString();
			text = text + ", \"user_segment\":" + args[8].ToString();
			text = text + ", \"result\":" + args[9].ToString();
			text = text + ", \"first_purchase\":" + args[10].ToString();
			text = text + ", \"purchase_amount\":" + args[11].ToString();
			text = text + ", \"currency_code\":\"" + args[12].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_60_DPM:
			text = text + ", \"mode\":" + args[0].ToString();
			text = text + ", \"device_id\":\"" + args[1].ToString() + "\"";
			text = text + ", \"place\":" + args[2].ToString();
			break;
		case CRAM_ID.CRAM_ID_61_EBNG:
			text = text + ", \"cur_stage\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"evt_id\":" + args[2].ToString();
			text = text + ", \"sheet_no\":" + args[3].ToString();
			text = text + ", \"bng_no\":" + args[4].ToString();
			text = text + ", \"evt_bng\":" + args[5].ToString();
			text = text + ", \"evt_stars\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_62_EBLTY:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"sheet_no\":" + args[1].ToString();
			text = text + ", \"old_stg\":" + args[2].ToString();
			text = text + ", \"new_stg\":" + args[3].ToString();
			text = text + ", \"use_item\":" + args[4].ToString();
			text = text + ", \"use_amount\":" + args[5].ToString();
			text = text + ", \"evt_bng\":" + args[6].ToString();
			text = text + ", \"evt_stars\":" + args[7].ToString();
			text = text + ", \"cur_stage\":" + args[8].ToString();
			text = text + ", \"cur_series\":" + args[9].ToString();
			text = text + ", \"heart\":" + args[10].ToString();
			text = text + ", \"ticket\":" + args[11].ToString();
			break;
		case CRAM_ID.CRAM_ID_63_OEENT:
			text = text + ", \"enter_count\":" + args[0].ToString();
			text = text + ", \"character_num_sailor_base\":" + args[1].ToString();
			text = text + ", \"character_num_sailor_expand\":" + args[2].ToString();
			text = text + ", \"character_num_other_base\":" + args[3].ToString();
			text = text + ", \"character_num_other_expand\":" + args[4].ToString();
			text = text + ", \"key_num\":" + args[5].ToString();
			text = text + ", \"open_count_all\":" + args[6].ToString();
			text = text + ", \"key_count_all\":" + args[7].ToString();
			text = text + ", \"gem_count_all\":" + args[8].ToString();
			break;
		case CRAM_ID.CRAM_ID_64_OEOPN:
			text = text + ", \"evt_id\":" + args[0].ToString();
			text = text + ", \"where_open\":" + args[1].ToString();
			text = text + ", \"offer_type\":" + args[2].ToString();
			text = text + ", \"use_item\":" + args[3].ToString();
			text = text + ", \"character_num_sailor_base\":" + args[4].ToString();
			text = text + ", \"character_num_sailor_expand\":" + args[5].ToString();
			text = text + ", \"character_num_other_base\":" + args[6].ToString();
			text = text + ", \"character_num_other_expand\":" + args[7].ToString();
			text = text + ", \"key_num\":" + args[8].ToString();
			text = text + ", \"use_mc\":" + args[9].ToString();
			text = text + ", \"use_sc\":" + args[10].ToString();
			text = text + ", \"use_key\":" + args[11].ToString();
			text = text + ", \"get_character\":" + args[12].ToString();
			text = text + ", \"stage_num\":" + args[13].ToString();
			text = text + ", \"evt_stars\":" + args[14].ToString();
			break;
		case CRAM_ID.CRAM_ID_65_OEGDG:
			text = text + ", \"result\":" + args[0].ToString();
			text = text + ", \"dest_mode\":" + args[1].ToString();
			text = text + ", \"enter_count\":" + args[2].ToString();
			text = text + ", \"character_num_sailor_base\":" + args[3].ToString();
			text = text + ", \"character_num_sailor_expand\":" + args[4].ToString();
			text = text + ", \"character_num_other_base\":" + args[5].ToString();
			text = text + ", \"character_num_other_expand\":" + args[6].ToString();
			text = text + ", \"key_num\":" + args[7].ToString();
			text = text + ", \"open_count_all\":" + args[8].ToString();
			text = text + ", \"key_count_all\":" + args[9].ToString();
			text = text + ", \"gem_count_all\":" + args[10].ToString();
			break;
		case CRAM_ID.CRAM_ID_66_DCWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"evt_id\":" + args[2].ToString();
			text = text + ", \"sheet_no\":" + args[3].ToString();
			text = text + ", \"tgt_rev\":" + args[4].ToString();
			text = text + ", \"tgt_seg\":\"" + args[5].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[6].ToString() + "\"";
			text = text + ", \"won\":" + args[7].ToString();
			text = text + ", \"resulttype\":" + args[8].ToString();
			text = text + ", \"score\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"make_stripe\":" + args[30].ToString();
			text = text + ", \"make_pop\":" + args[31].ToString();
			text = text + ", \"make_rainbow\":" + args[32].ToString();
			text = text + ", \"make_paint\":" + args[33].ToString();
			text = text + ", \"make_lunap\":" + args[34].ToString();
			text = text + ", \"erase_stripe\":" + args[35].ToString();
			text = text + ", \"erase_pop\":" + args[36].ToString();
			text = text + ", \"erase_rainbow\":" + args[37].ToString();
			text = text + ", \"erase_paint\":" + args[38].ToString();
			text = text + ", \"erase_lunap\":" + args[39].ToString();
			text = text + ", \"swap_stripe_stripe\":" + args[40].ToString();
			text = text + ", \"swap_stripe_pop\":" + args[41].ToString();
			text = text + ", \"swap_stripe_rainbow\":" + args[42].ToString();
			text = text + ", \"swap_stripe_paint\":" + args[43].ToString();
			text = text + ", \"swap_pop_pop\":" + args[44].ToString();
			text = text + ", \"swap_pop_rainbow\":" + args[45].ToString();
			text = text + ", \"swap_pop_paint\":" + args[46].ToString();
			text = text + ", \"swap_rainbow_rainbow\":" + args[47].ToString();
			text = text + ", \"swap_rainbow_paint\":" + args[48].ToString();
			text = text + ", \"swap_paint_paint\":" + args[49].ToString();
			break;
		case CRAM_ID.CRAM_ID_67_GEMGD:
			text = text + ", \"campaign_group_id\":" + args[0].ToString();
			text = text + ", \"iapId\":" + args[1].ToString();
			text = text + ", \"offer_start_time\":" + args[2].ToString();
			text = text + ", \"offer_num\":" + args[3].ToString();
			text = text + ", \"result\":" + args[4].ToString();
			break;
		case CRAM_ID.CRAM_ID_68_GEMPC:
			text = text + ", \"campaign_group_id\":" + args[0].ToString();
			text = text + ", \"iapId\":" + args[1].ToString();
			text = text + ", \"remain_time\":" + args[2].ToString();
			text = text + ", \"campaign_end\":" + args[3].ToString();
			text = text + ", \"place\":" + args[4].ToString();
			text = text + ", \"offer_start_time\":" + args[5].ToString();
			text = text + ", \"offer_num\":" + args[6].ToString();
			text = text + ", \"sale_num\":" + args[7].ToString();
			break;
		case CRAM_ID.CRAM_ID_69_IPN:
			text = text + ", \"item_num\":\"" + args[0].ToString() + "\"";
			text = text + ", \"item_num_adv\":\"" + args[1].ToString() + "\"";
			text = text + ", \"booster_num\":\"" + args[2].ToString() + "\"";
			text = text + ", \"lvl_array\":\"" + args[3].ToString() + "\"";
			text = text + ", \"lvl_array_adv\":\"" + args[4].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_70_DTD:
			text = text + ", \"reason\":" + args[0].ToString();
			text = text + ", \"local_path\":\"" + args[1].ToString() + "\"";
			text = text + ", \"local_path_adv\":\"" + args[2].ToString() + "\"";
			text = text + ", \"local_size\":" + args[3].ToString();
			text = text + ", \"local_size_adv\":" + args[4].ToString();
			text = text + ", \"local_ver\":" + args[5].ToString();
			text = text + ", \"local_ver_adv\":" + args[6].ToString();
			text = text + ", \"local_searchid\":\"" + args[7].ToString() + "\"";
			text = text + ", \"local_uuid\":" + args[8].ToString();
			text = text + ", \"local_bhiveid\":" + args[9].ToString();
			text = text + ", \"local_lvl_array\":\"" + args[10].ToString() + "\"";
			text = text + ", \"local_lvl_array_adv\":\"" + args[11].ToString() + "\"";
			text = text + ", \"local_xp\":" + args[12].ToString();
			text = text + ", \"local_xp_adv\":" + args[13].ToString();
			text = text + ", \"local_cur_series\":" + args[14].ToString();
			text = text + ", \"local_cur_stage\":" + args[15].ToString();
			text = text + ", \"local_cur_aseries\":" + args[16].ToString();
			text = text + ", \"local_cur_astage\":" + args[17].ToString();
			text = text + ", \"local_heart\":" + args[18].ToString();
			text = text + ", \"local_medal\":" + args[19].ToString();
			text = text + ", \"authkey\":\"" + args[20].ToString() + "\"";
			break;
		case CRAM_ID.CRAM_ID_71_DTS:
			text = text + ", \"reason\":" + args[0].ToString();
			text = text + ", \"result\":" + args[1].ToString();
			break;
		case CRAM_ID.CRAM_ID_72_DRB:
			text = text + ", \"savedata_kind\":" + args[0].ToString();
			text = text + ", \"remote_file_status\":" + args[1].ToString();
			text = text + ", \"local_path\":\"" + args[2].ToString() + "\"";
			text = text + ", \"local_path_adv\":\"" + args[3].ToString() + "\"";
			text = text + ", \"local_size\":" + args[4].ToString();
			text = text + ", \"local_size_adv\":" + args[5].ToString();
			text = text + ", \"local_ver\":" + args[6].ToString();
			text = text + ", \"local_ver_adv\":" + args[7].ToString();
			text = text + ", \"local_lvl_array\":\"" + args[8].ToString() + "\"";
			text = text + ", \"local_lvl_array_adv\":\"" + args[9].ToString() + "\"";
			text = text + ", \"local_xp\":" + args[10].ToString();
			text = text + ", \"local_xp_adv\":" + args[11].ToString();
			text = text + ", \"local_cur_series\":" + args[12].ToString();
			text = text + ", \"local_cur_stage\":" + args[13].ToString();
			text = text + ", \"local_cur_aseries\":" + args[14].ToString();
			text = text + ", \"local_cur_astage\":" + args[15].ToString();
			text = text + ", \"local_heart\":" + args[16].ToString();
			text = text + ", \"local_medal\":" + args[17].ToString();
			text = text + ", \"remote_size\":" + args[18].ToString();
			text = text + ", \"remote_size_adv\":" + args[19].ToString();
			text = text + ", \"remote_ver\":" + args[20].ToString();
			text = text + ", \"remote_ver_adv\":" + args[21].ToString();
			text = text + ", \"remote_lvl_array\":\"" + args[22].ToString() + "\"";
			text = text + ", \"remote_lvl_array_adv\":\"" + args[23].ToString() + "\"";
			text = text + ", \"remote_xp\":" + args[24].ToString();
			text = text + ", \"remote_xp_adv\":" + args[25].ToString();
			text = text + ", \"remote_cur_series\":" + args[26].ToString();
			text = text + ", \"remote_cur_stage\":" + args[27].ToString();
			text = text + ", \"remote_cur_aseries\":" + args[28].ToString();
			text = text + ", \"remote_cur_astage\":" + args[29].ToString();
			text = text + ", \"remote_heart\":" + args[30].ToString();
			text = text + ", \"remote_medal\":" + args[31].ToString();
			break;
		case CRAM_ID.CRAM_ID_73_OEWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"evt_id\":" + args[1].ToString();
			text = text + ", \"tgt_fmt\":" + args[2].ToString();
			text = text + ", \"tgt_rev\":" + args[3].ToString();
			text = text + ", \"tgt_seg\":\"" + args[4].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[5].ToString() + "\"";
			text = text + ", \"won\":" + args[6].ToString();
			text = text + ", \"resulttype\":" + args[7].ToString();
			text = text + ", \"score\":" + args[8].ToString();
			text = text + ", \"gotstars\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"heart\":" + args[14].ToString();
			text = text + ", \"last_login\":" + args[15].ToString();
			text = text + ", \"game_start\":" + args[16].ToString();
			text = text + ", \"first_purchase\":" + args[17].ToString();
			text = text + ", \"purchase_count\":" + args[18].ToString();
			text = text + ", \"startup_count\":" + args[19].ToString();
			text = text + ", \"play_count\":" + args[20].ToString();
			text = text + ", \"total_win\":" + args[21].ToString();
			text = text + ", \"total_lose\":" + args[22].ToString();
			text = text + ", \"total_win_rate\":" + args[23].ToString();
			text = text + ", \"stage_win\":" + args[24].ToString();
			text = text + ", \"stage_lose\":" + args[25].ToString();
			text = text + ", \"stage_win_rate\":" + args[26].ToString();
			text = text + ", \"Character_ID\":" + args[27].ToString();
			text = text + ", \"Character_LVL\":" + args[28].ToString();
			text = text + ", \"Skill\":" + args[29].ToString();
			text = text + ", \"make_stripe\":" + args[30].ToString();
			text = text + ", \"make_pop\":" + args[31].ToString();
			text = text + ", \"make_rainbow\":" + args[32].ToString();
			text = text + ", \"make_paint\":" + args[33].ToString();
			text = text + ", \"make_lunap\":" + args[34].ToString();
			text = text + ", \"erase_stripe\":" + args[35].ToString();
			text = text + ", \"erase_pop\":" + args[36].ToString();
			text = text + ", \"erase_rainbow\":" + args[37].ToString();
			text = text + ", \"erase_paint\":" + args[38].ToString();
			text = text + ", \"erase_lunap\":" + args[39].ToString();
			text = text + ", \"swap_stripe_stripe\":" + args[40].ToString();
			text = text + ", \"swap_stripe_pop\":" + args[41].ToString();
			text = text + ", \"swap_stripe_rainbow\":" + args[42].ToString();
			text = text + ", \"swap_stripe_paint\":" + args[43].ToString();
			text = text + ", \"swap_pop_pop\":" + args[44].ToString();
			text = text + ", \"swap_pop_rainbow\":" + args[45].ToString();
			text = text + ", \"swap_pop_paint\":" + args[46].ToString();
			text = text + ", \"swap_rainbow_rainbow\":" + args[47].ToString();
			text = text + ", \"swap_rainbow_paint\":" + args[48].ToString();
			text = text + ", \"swap_paint_paint\":" + args[49].ToString();
			break;
		case CRAM_ID.CRAM_ID_74_DCST:
			text = text + ", \"mode_id\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"cur_stage\":" + args[2].ToString();
			text = text + ", \"segment_type\":" + args[3].ToString();
			break;
		case CRAM_ID.CRAM_ID_75_DTR:
			text = text + ", \"reason\":" + args[0].ToString();
			text = text + ", \"local_path\":\"" + args[1].ToString() + "\"";
			text = text + ", \"local_path_adv\":\"" + args[2].ToString() + "\"";
			text = text + ", \"local_size\":" + args[3].ToString();
			text = text + ", \"local_size_adv\":" + args[4].ToString();
			text = text + ", \"local_ver\":" + args[5].ToString();
			text = text + ", \"local_ver_adv\":" + args[6].ToString();
			text = text + ", \"local_searchid\":\"" + args[7].ToString() + "\"";
			text = text + ", \"local_uuid\":" + args[8].ToString();
			text = text + ", \"local_bhiveid\":" + args[9].ToString();
			text = text + ", \"local_lvl_array\":\"" + args[10].ToString() + "\"";
			text = text + ", \"local_lvl_array_adv\":\"" + args[11].ToString() + "\"";
			text = text + ", \"local_xp\":" + args[12].ToString();
			text = text + ", \"local_xp_adv\":" + args[13].ToString();
			text = text + ", \"local_cur_series\":" + args[14].ToString();
			text = text + ", \"local_cur_stage\":" + args[15].ToString();
			text = text + ", \"local_cur_aseries\":" + args[16].ToString();
			text = text + ", \"local_cur_astage\":" + args[17].ToString();
			text = text + ", \"local_heart\":" + args[18].ToString();
			text = text + ", \"local_medal\":" + args[19].ToString();
			text = text + ", \"remote_size\":" + args[20].ToString();
			text = text + ", \"remote_size_adv\":" + args[21].ToString();
			text = text + ", \"remote_ver\":" + args[22].ToString();
			text = text + ", \"remote_ver_adv\":" + args[23].ToString();
			text = text + ", \"remote_searchid\":\"" + args[24].ToString() + "\"";
			text = text + ", \"remote_uuid\":" + args[25].ToString();
			text = text + ", \"remote_bhiveid\":" + args[26].ToString();
			text = text + ", \"remote_lvl_array\":\"" + args[27].ToString() + "\"";
			text = text + ", \"remote_lvl_array_adv\":\"" + args[28].ToString() + "\"";
			text = text + ", \"remote_xp\":" + args[29].ToString();
			text = text + ", \"remote_xp_adv\":" + args[30].ToString();
			text = text + ", \"remote_cur_series\":" + args[31].ToString();
			text = text + ", \"remote_cur_stage\":" + args[32].ToString();
			text = text + ", \"remote_cur_aseries\":" + args[33].ToString();
			text = text + ", \"remote_cur_astage\":" + args[34].ToString();
			text = text + ", \"remote_heart\":" + args[35].ToString();
			text = text + ", \"remote_medal\":" + args[36].ToString();
			break;
		case CRAM_ID.CRAM_ID_76_LVRC:
			text = text + ", \"Character_ID\":" + args[0].ToString();
			text = text + ", \"old_level\":" + args[1].ToString();
			text = text + ", \"new_level\":" + args[2].ToString();
			text = text + ", \"old_levelup_num\":" + args[3].ToString();
			text = text + ", \"new_levelup_num\":" + args[4].ToString();
			text = text + ", \"decision_time\":" + args[5].ToString();
			break;
		case CRAM_ID.CRAM_ID_77_ELCNT:
			text = text + ", \"cur_stage\":" + args[0].ToString();
			text = text + ", \"evt_id\":" + args[1].ToString();
			text = text + ", \"jewel_num\":" + args[2].ToString();
			text = text + ", \"sts_cnt\":" + args[3].ToString();
			text = text + ", \"jewelget_chance\":" + args[4].ToString();
			text = text + ", \"result\":" + args[5].ToString();
			text = text + ", \"total_jewel_num\":" + args[6].ToString();
			break;
		case CRAM_ID.CRAM_ID_78_ELRSLT:
			text = text + ", \"cur_stage\":" + args[0].ToString();
			text = text + ", \"evt_id\":" + args[1].ToString();
			text = text + ", \"jewel_num\":" + args[2].ToString();
			text = text + ", \"jewel_num_puzzle\":" + args[3].ToString();
			text = text + ", \"jewel_num_bouns\":" + args[4].ToString();
			text = text + ", \"jewel_num_doubleup\":" + args[5].ToString();
			text = text + ", \"jewel_num_stageclear\":" + args[6].ToString();
			text = text + ", \"total_jewel_num\":" + args[7].ToString();
			text = text + ", \"jewelget_chance\":" + args[8].ToString();
			text = text + ", \"doubleup_chance\":" + args[9].ToString();
			text = text + ", \"cost\":" + args[10].ToString();
			text = text + ", \"rate\":" + args[11].ToString();
			text = text + ", \"total_doubleup_num\":" + args[12].ToString();
			break;
		case CRAM_ID.CRAM_ID_79_ELCR:
			text = text + ", \"course_id\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"cur_stage\":" + args[2].ToString();
			text = text + ", \"evt_id\":" + args[3].ToString();
			text = text + ", \"reason\":" + args[4].ToString();
			text = text + ", \"total_jewel_num\":" + args[5].ToString();
			break;
		case CRAM_ID.CRAM_ID_80_ELGJ:
			text = text + ", \"course_id\":" + args[0].ToString();
			text = text + ", \"cur_series\":" + args[1].ToString();
			text = text + ", \"cur_stage\":" + args[2].ToString();
			text = text + ", \"evt_id\":" + args[3].ToString();
			text = text + ", \"place\":" + args[4].ToString();
			text = text + ", \"jewel_num\":" + args[5].ToString();
			text = text + ", \"jewel_num_puzzle\":" + args[6].ToString();
			text = text + ", \"jewel_num_bouns\":" + args[7].ToString();
			text = text + ", \"jewel_num_doubleup\":" + args[8].ToString();
			text = text + ", \"jewel_num_stageclear\":" + args[9].ToString();
			text = text + ", \"jewel_num_courseclear\":" + args[10].ToString();
			text = text + ", \"jewel_num_support\":" + args[11].ToString();
			text = text + ", \"jewel_num_stageallclear\":" + args[12].ToString();
			text = text + ", \"total_jewel_num\":" + args[13].ToString();
			break;
		case CRAM_ID.CRAM_ID_81_ELRWD:
			text = text + ", \"accessory_id\":" + args[0].ToString();
			text = text + ", \"evt_id\":" + args[1].ToString();
			text = text + ", \"course1_goal_count\":" + args[2].ToString();
			text = text + ", \"course2_goal_count\":" + args[3].ToString();
			text = text + ", \"course3_goal_count\":" + args[4].ToString();
			text = text + ", \"course4_goal_count\":" + args[5].ToString();
			text = text + ", \"course5_goal_count\":" + args[6].ToString();
			text = text + ", \"course6_goal_count\":" + args[7].ToString();
			text = text + ", \"course7_goal_count\":" + args[8].ToString();
			text = text + ", \"course8_goal_count\":" + args[9].ToString();
			text = text + ", \"course9_goal_count\":" + args[10].ToString();
			text = text + ", \"course10_goal_count\":" + args[11].ToString();
			text = text + ", \"course11_goal_count\":" + args[12].ToString();
			text = text + ", \"course12_goal_count\":" + args[13].ToString();
			text = text + ", \"course13_goal_count\":" + args[14].ToString();
			text = text + ", \"total_stageclear_count\":" + args[15].ToString();
			text = text + ", \"total_doubleup_num\":" + args[16].ToString();
			text = text + ", \"total_jewel_num\":" + args[17].ToString();
			break;
		case CRAM_ID.CRAM_ID_82_SGWIN:
			text = text + ", \"tgt_stage\":" + args[0].ToString();
			text = text + ", \"origin_stage\":" + args[1].ToString();
			text = text + ", \"evt_id\":" + args[2].ToString();
			text = text + ", \"tgt_fmt\":" + args[3].ToString();
			text = text + ", \"tgt_rev\":" + args[4].ToString();
			text = text + ", \"tgt_seg\":\"" + args[5].ToString() + "\"";
			text = text + ", \"tgt_mode\":\"" + args[6].ToString() + "\"";
			text = text + ", \"won\":" + args[7].ToString();
			text = text + ", \"resulttype\":" + args[8].ToString();
			text = text + ", \"origin_score\":" + args[9].ToString();
			text = text + ", \"pmoves\":" + args[10].ToString();
			text = text + ", \"ptime\":" + args[11].ToString();
			text = text + ", \"pcnt\":" + args[12].ToString();
			text = text + ", \"max_combo\":" + args[13].ToString();
			text = text + ", \"first_purchase\":" + args[14].ToString();
			text = text + ", \"Character_ID\":" + args[15].ToString();
			text = text + ", \"Character_LVL\":" + args[16].ToString();
			text = text + ", \"Skill\":" + args[17].ToString();
			break;
		}
		return text;
	}

	private static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	private static void log(string msg)
	{
	}

	private static void dbg(string msg)
	{
	}

	private void Awake()
	{
		mThread = new Thread(_ThreadMain);
	}

	private void Start()
	{
		mIsRunning = true;
		mThread.Start();
		mSender = Sender.CreateInstance(this);
	}

	private void OnDestroy()
	{
		log("[TERM]@@@ ServerCram OnDestroy: " + isTerminating);
		if (!isTerminating)
		{
			isTerminating = true;
			Term();
		}
	}

	private void Term()
	{
		int num = 0;
		do
		{
			mIsRunning = false;
			mRequestsNotify.Set();
			mThread.Join(100);
		}
		while (mThread.IsAlive && num++ < 3);
		if (mThread.IsAlive)
		{
			mThread.Abort();
		}
		lock (mRequestsLock)
		{
			foreach (CRAMRequest mRequest in mRequests)
			{
				mRequest._httpAsync.Abort();
			}
			mRequests.Clear();
		}
		if (mSender != null)
		{
			mSender.Kill();
		}
	}

	private int GetRequestsWaitTimeMillis()
	{
		return -1;
	}

	private int GetResponsesWaitTimeMillis()
	{
		return 10;
	}

	private void _ThreadMain()
	{
		while (mIsRunning)
		{
			dbg("CRAM Request Queue - START");
			try
			{
				mRequestsNotify.WaitOne(GetRequestsWaitTimeMillis());
				mRequestsNotify.Reset();
				dbg("CRAM Request Queue - PROCESS");
				CRAMRequest cRAMRequest = null;
				do
				{
					cRAMRequest = null;
					dbg("CRAM Request Queue - REQUEST 0");
					lock (mRequestsLock)
					{
						if (mRequests.Count > 0)
						{
							cRAMRequest = mRequests.Dequeue();
						}
					}
					dbg("CRAM Request Queue - REQUEST 1");
					if (cRAMRequest != null)
					{
						int num = 0;
						try
						{
							dbg(string.Format("@@@CRAM Request({0})={1}", cRAMRequest._httpAsync.SeqNo, cRAMRequest._httpAsync.Request.RequestUri));
							mThreadLock = new ManualResetEvent(false);
							cRAMRequest._time = DateTime.Now;
							cRAMRequest._httpAsync.GetResponse(delegate(HttpAsync httpAsync)
							{
								CRAMRequest cRAMRequest2 = httpAsync.UserData as CRAMRequest;
								if (cRAMRequest2 != null)
								{
									log(string.Format("@@@CRAM Result({0})={1}    {2} ({3}sec)", cRAMRequest2._httpAsync.SeqNo, cRAMRequest2._httpAsync.Result, cRAMRequest2._httpAsync.Request.RequestUri, ((DateTime.Now - cRAMRequest2._time).TotalMilliseconds / 1000.0).ToString("0.000")));
								}
								mThreadLock.Set();
							}, cRAMRequest);
							bool flag = false;
							while (!mThreadLock.WaitOne(GetResponsesWaitTimeMillis()))
							{
								if (!mIsRunning)
								{
									flag = true;
									break;
								}
							}
							if (flag)
							{
								num = 4;
							}
							else
							{
								log("ServerCram SendId=" + cRAMRequest._sendId + " httpAsync.Result=" + cRAMRequest._httpAsync.Result);
								switch (cRAMRequest._httpAsync.Result)
								{
								case HttpAsync.RESULT.SUCCESS:
								{
									HttpWebResponse httpWebResponse = cRAMRequest._httpAsync.Response as HttpWebResponse;
									if (httpWebResponse != null)
									{
										log(string.Format("(#{0}): HTTP Result={1}   (len={2})", cRAMRequest._httpAsync.SeqNo, httpWebResponse.StatusCode, httpWebResponse.ContentLength));
										log("ServerCram SendId=" + cRAMRequest._sendId + " StatusCode=" + httpWebResponse.StatusCode);
										num = ((httpWebResponse.StatusCode != HttpStatusCode.OK) ? 5 : 0);
									}
									else
									{
										log(string.Format("(#{0}): HTTP Result=NO RESPONSE", cRAMRequest._httpAsync.SeqNo));
										num = 1;
									}
									break;
								}
								case HttpAsync.RESULT.NOTFOUND:
									log(string.Format("(#{0}): HTTP Result=NOTFOUND", cRAMRequest._httpAsync.SeqNo));
									num = 5;
									break;
								case HttpAsync.RESULT.ERROR:
									log(string.Format("(#{0}): HTTP Result=ERROR", cRAMRequest._httpAsync.SeqNo));
									num = 1;
									break;
								case HttpAsync.RESULT.TIMEOUT:
									log(string.Format("(#{0}): HTTP Result=TIMEOUT", cRAMRequest._httpAsync.SeqNo));
									num = 3;
									break;
								case HttpAsync.RESULT.ABORT:
									log(string.Format("(#{0}): HTTP Result=ABORT", cRAMRequest._httpAsync.SeqNo));
									num = 6;
									break;
								default:
									log(string.Format("(#{0}): HTTP Result=UNKNOWN ERROR", cRAMRequest._httpAsync.SeqNo));
									num = 1;
									break;
								}
							}
						}
						catch (Exception)
						{
							num = 1;
						}
						dbg(string.Format("@@@CRAM End({0})", cRAMRequest._httpAsync.SeqNo));
						lock (mDataListLock)
						{
							if (num != 0 && cRAMRequest._isMandatory)
							{
								for (int i = 0; i < mDataList.Count; i++)
								{
									CramData cramData = mDataList[i];
									if (cRAMRequest._sendId == cramData._sendId)
									{
										cramData._sendId = -1;
									}
								}
							}
							else
							{
								for (int num2 = mDataList.Count - 1; num2 >= 0; num2--)
								{
									CramData cramData = mDataList[num2];
									if (cRAMRequest._sendId == cramData._sendId)
									{
										mDataList.RemoveAt(num2);
									}
								}
								if (cRAMRequest._isMandatory)
								{
									SaveCramData();
								}
							}
						}
						if (num != 0 && num != 4)
						{
							Thread.Sleep(180000);
						}
					}
					else
					{
						dbg("CRAM Request Queue - EMPTY");
					}
					dbg("CRAM Request Queue - REQUEST 2");
				}
				while (cRAMRequest != null);
			}
			catch (Exception)
			{
			}
			dbg("CRAM Request Queue - E N D");
		}
	}

	private bool PostRequest()
	{
		int num = 0;
		lock (mRequestsLock)
		{
			num = mRequests.Count;
		}
		if (num >= 20)
		{
			return false;
		}
		CramData data = null;
		StringBuilder stringBuilder = new StringBuilder();
		lock (mDataListLock)
		{
			if (mDataList.Count > 0)
			{
				if (string.IsNullOrEmpty(BaseCramURL))
				{
					mDataList.Clear();
					SaveCramData();
				}
				for (int i = 0; i < mDataList.Count; i++)
				{
					CramData cramData = mDataList[i];
					if (cramData._sendId < 0)
					{
						data = cramData;
						break;
					}
				}
				if (data != null)
				{
					stringBuilder.Append("[");
					string value = string.Empty;
					int num2 = 0;
					for (int j = 0; j < mDataList.Count; j++)
					{
						CramData cramData = mDataList[j];
						if (data._id == cramData._id && cramData._sendId < 0)
						{
							cramData._sendId = mSendId;
							stringBuilder.Append(value);
							stringBuilder.Append(cramData._data);
							value = ",";
							num2++;
							if (num2 >= 10)
							{
								break;
							}
						}
					}
					stringBuilder.Append("]");
				}
			}
		}
		if (stringBuilder.Length == 0)
		{
			return true;
		}
		StringBuilder stringBuilder2 = new StringBuilder();
		string arg = "behavior";
		if (data._id == CRAM_ID.CRAM_ID_10_WIN)
		{
			arg = "win";
		}
		else if (data._id == CRAM_ID.CRAM_ID_11_PST)
		{
			arg = "pst";
		}
		else if (data._id == CRAM_ID.CRAM_ID_25_DAU)
		{
			arg = "dau";
		}
		stringBuilder2.Append(string.Format(BaseCramURL, arg));
		string value2 = "?tag=" + SingletonMonoBehaviour<Network>.Instance.AppName + phpTable[(int)data._id];
		stringBuilder2.Append(value2);
		bool result = Network.CreateHttpPostAsyncJson(stringBuilder2.ToString(), stringBuilder.ToString(), Timeout, delegate(HttpAsync httpAsync)
		{
			CRAMRequest item = new CRAMRequest
			{
				_sendId = data._sendId,
				_isMandatory = data._isMandatory,
				_httpAsync = httpAsync
			};
			lock (mRequestsLock)
			{
				mRequests.Enqueue(item);
			}
			log(string.Concat("ServerCram CreateHttpPostAsyncJson ID=", data._id, " SendId=", data._sendId));
			mRequestsNotify.Set();
		});
		mSendId++;
		mSendId &= int.MaxValue;
		return result;
	}

	private bool AddRequestData(CRAM_ID id, string data)
	{
		log(string.Concat("ServerCram AddRequestData ID=", id, " data=", data));
		CramData cramData = new CramData();
		bool result = false;
		if (cramData != null)
		{
			cramData._id = id;
			cramData._isMandatory = MandatoryTable[(int)id];
			cramData._sendId = -1;
			cramData._time = DateTime.Now;
			cramData._data = data;
			lock (mDataListLock)
			{
				if (id == CRAM_ID.CRAM_ID_11_PST)
				{
					mDataList.Insert(0, cramData);
				}
				else
				{
					mDataList.Add(cramData);
				}
				if (cramData._isMandatory && mSaveAtRequest)
				{
					SaveCramData();
				}
			}
			if (id == CRAM_ID.CRAM_ID_11_PST)
			{
				PostRequest();
			}
			result = true;
		}
		return result;
	}

	public void SaveCramData()
	{
		log("SaveCramData start");
		try
		{
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter("bi.dat"))
			{
				int num = 0;
				for (int i = 0; i < mDataList.Count; i++)
				{
					if (mDataList[i]._isMandatory)
					{
						num++;
					}
				}
				bIJEncryptDataWriter.WriteInt(num);
				for (int j = 0; j < mDataList.Count; j++)
				{
					CramData cramData = mDataList[j];
					if (cramData._isMandatory)
					{
						bIJEncryptDataWriter.WriteInt((int)cramData._id);
						bIJEncryptDataWriter.WriteDateTime(cramData._time);
						bIJEncryptDataWriter.WriteUTF(cramData._data);
					}
				}
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
		log("SaveCramData end");
	}

	public void LoadCramData()
	{
		log("LoadCramData start");
		try
		{
			string path = Path.Combine(BIJUnity.getDataPath(), "bi.dat");
			if (File.Exists(path))
			{
				using (BIJEncryptDataReader bIJEncryptDataReader = new BIJEncryptDataReader("bi.dat"))
				{
					int num = bIJEncryptDataReader.ReadInt();
					for (int i = 0; i < num; i++)
					{
						CramData cramData = new CramData();
						cramData._id = (CRAM_ID)bIJEncryptDataReader.ReadInt();
						cramData._time = bIJEncryptDataReader.ReadDateTime();
						cramData._data = bIJEncryptDataReader.ReadUTF();
						cramData._isMandatory = MandatoryTable[(int)cramData._id];
						cramData._sendId = -1;
						mDataList.Add(cramData);
					}
					bIJEncryptDataReader.Close();
				}
			}
		}
		catch (Exception)
		{
		}
		log("LoadCramData end");
	}

	public static void SendStart()
	{
		mSendStart = true;
	}

	public static bool ServerCramSend(CRAM_ID idx, params object[] args)
	{
		if (!IsCramSendable)
		{
			return false;
		}
		if (idx < CRAM_ID.CRAM_ID_1_TUT || idx >= CRAM_ID.CRAM_ID_MAX)
		{
			return false;
		}
		if (SingletonMonoBehaviour<Network>.Instance.HiveId == 0)
		{
			return false;
		}
		string cRAMCommonParams = Network.GetCRAMCommonParams();
		string cramParams = GetCramParams(idx, args);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("{");
		stringBuilder.Append(cRAMCommonParams);
		if (cramParams.Length > 0)
		{
			stringBuilder.Append(cramParams);
		}
		stringBuilder.Append("}");
		bool flag = false;
		try
		{
			return Instance.AddRequestData(idx, stringBuilder.ToString());
		}
		catch (Exception e)
		{
			BIJLog.E(string.Concat("AddRequestData(", idx, ") error."), e);
			return false;
		}
	}

	public static string GetBoosterNumString()
	{
		Player mPlayer = SingletonMonoBehaviour<GameMain>.Instance.mPlayer;
		int[] array = new int[16]
		{
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.WAVE_BOMB),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.TAP_BOMB2),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.RAINBOW),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.ADD_SCORE),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.SKILL_CHARGE),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.SELECT_ONE),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.SELECT_COLLECT),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.COLOR_CRUSH),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.BLOCK_CRUSH),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.PAIR_MAKER),
			mPlayer.GetBoosterNum(Def.BOOSTER_KIND.ALL_CRUSH),
			0,
			0,
			0,
			0,
			0
		};
		string[] array2 = new string[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			array2[i] = string.Empty + array[i];
		}
		return string.Join(",", array2);
	}

	public static string GetLevelArrayString(Player aPlayer)
	{
		List<PlayStageParam> playStage = aPlayer.PlayStage;
		int[] array = new int[8];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = 0;
		}
		for (int j = 0; j < playStage.Count; j++)
		{
			PlayStageParam playStageParam = playStage[j];
			int num = playStageParam.Series;
			if (Def.IsGaidenSeries((Def.SERIES)playStageParam.Series))
			{
				num -= 5;
			}
			array[num] = playStageParam.Level;
		}
		string[] array2 = new string[array.Length];
		for (int k = 0; k < array.Length; k++)
		{
			array2[k] = string.Empty + array[k];
		}
		return string.Join(",", array2);
	}

	public static void SendDau(bool aSendMain)
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(PlaySessionManager.Instance.StartPlaySessionTime);
		if (num < 0)
		{
			num = 0L;
		}
		string deviceModel = Network.DeviceModel;
		string operatingSystem = SystemInfo.operatingSystem;
		Player mPlayer = SingletonMonoBehaviour<GameMain>.Instance.mPlayer;
		long startupCount = mPlayer.Data.StartupCount;
		long play_count = mPlayer.Data.TotalWin + mPlayer.Data.TotalLose;
		Options mOptions = SingletonMonoBehaviour<GameMain>.Instance.mOptions;
		int first_purchase = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.FirstPurchasedTime);
		long purchaseCount = mOptions.PurchaseCount;
		float purchaseAmount = mOptions.PurchaseAmount;
		string currencyCode = mOptions.CurrencyCode;
		string boosterNumString = GetBoosterNumString();
		long buyHeartCount = mOptions.BuyHeartCount;
		long buyBoostCount = mOptions.BuyBoostCount;
		long buyContinueCount = mOptions.BuyContinueCount;
		long buyUnlockRBCount = mOptions.BuyUnlockRBCount;
		string levelArrayString = GetLevelArrayString(mPlayer);
		DailyActiveUser(num, deviceModel, operatingSystem, startupCount, play_count, first_purchase, purchaseCount, purchaseAmount, currencyCode, boosterNumString, buyHeartCount, buyBoostCount, buyContinueCount, buyUnlockRBCount, levelArrayString);
		if (aSendMain)
		{
			MainDailyActiveUser(num, deviceModel, operatingSystem, startupCount, play_count, first_purchase, purchaseCount, purchaseAmount, currencyCode, boosterNumString, buyHeartCount, buyBoostCount, buyContinueCount, buyUnlockRBCount, levelArrayString);
		}
	}

	public static void SendAdvDau()
	{
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(PlaySessionManager.Instance.StartPlaySessionTime);
		if (num < 0)
		{
			num = 0L;
		}
		string deviceModel = Network.DeviceModel;
		string operatingSystem = SystemInfo.operatingSystem;
		Player mPlayer = SingletonMonoBehaviour<GameMain>.Instance.mPlayer;
		long startupCount = mPlayer.Data.StartupCount;
		long playCount = mPlayer.AdvSaveData.PlayCount;
		Options mOptions = SingletonMonoBehaviour<GameMain>.Instance.mOptions;
		int first_purchase = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.FirstPurchasedTime);
		long purchaseCount = mOptions.PurchaseCount;
		float purchaseAmount = mOptions.PurchaseAmount;
		string currencyCode = mOptions.CurrencyCode;
		int[] array = new int[32];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = 0;
		}
		string abooster = StringUtils.ConvertNumberListToCommaSeparateString(array);
		long buyAdvMedalCount = mOptions.BuyAdvMedalCount;
		long buyAdvBoosterCount = mOptions.BuyAdvBoosterCount;
		long buyAdvContinueCount = mOptions.BuyAdvContinueCount;
		long buyAdvGasyaCount = mOptions.BuyAdvGasyaCount;
		int count = mPlayer.AdvSaveData.CharacterDictionary.Count;
		int aplayd = (int)mPlayer.AdvSaveData.BootCount;
		int aintervald = (int)mPlayer.AdvSaveData.IntervalCount;
		int acontinuousd = (int)mPlayer.AdvSaveData.ContinuousCount;
		int advLastStage = mPlayer.AdvLastStage;
		int advMaxStamina = mPlayer.AdvMaxStamina;
		ArcadeDailyActiveUser(num, deviceModel, operatingSystem, startupCount, playCount, first_purchase, purchaseCount, purchaseAmount, currencyCode, abooster, buyAdvMedalCount, buyAdvBoosterCount, buyAdvContinueCount, buyAdvGasyaCount, count, aplayd, aintervald, acontinuousd, advLastStage, advMaxStamina);
	}

	public static void BufferingArcadeData_ACLU(int Character_ID, int use_xp, int origin_lvl, int update_lvl, int get_type)
	{
		BufferingArcadeData(CRAM_ID.CRAM_ID_34_ACLU, Character_ID, use_xp, origin_lvl, update_lvl, get_type);
	}

	public static void BufferingArcadeData_ACSLU(int Character_ID, int use_skillitem, int origin_lvl, int update_lvl, int get_type)
	{
		BufferingArcadeData(CRAM_ID.CRAM_ID_35_ACSLU, Character_ID, use_skillitem, origin_lvl, update_lvl, get_type);
	}

	public static void BufferingArcadeData_AGACCE(int accessory_id, int evt_id, int get_type)
	{
		BufferingArcadeData(CRAM_ID.CRAM_ID_40_AGACCE, accessory_id, evt_id, get_type);
	}

	public static void BufferingArcadeData_AAPP(int quantity, int category, int sub_category, int itemid, int get_type)
	{
		BufferingArcadeData(CRAM_ID.CRAM_ID_46_AAPP, quantity, category, sub_category, itemid, get_type);
	}

	private static void BufferingArcadeData(CRAM_ID aID, int aParam1, int aParam2, int aParam3, int aParam4 = 0, int aParam5 = 0)
	{
		int[] item = new int[6]
		{
			(int)aID,
			aParam1,
			aParam2,
			aParam3,
			aParam4,
			aParam5
		};
		mAdvFigureDataBuffer.Add(item);
	}

	public static void SendBufferingArcadeData(int aSeries, int aStage, int aASeries, int cur_aAStage, int aMaxlvl, int aMaxStamina)
	{
		if (mAdvFigureDataBuffer.Count <= 0)
		{
			return;
		}
		mAdvFigureDataBuffer.Sort((int[] a, int[] b) => a[0] - b[0]);
		mSaveAtRequest = false;
		while (mAdvFigureDataBuffer.Count > 0 && mAdvFigureDataBuffer[0][0] == 33)
		{
			int[] array = mAdvFigureDataBuffer[0];
			ArcadeCharacterLevelUP(array[1], array[2], aSeries, aStage, aASeries, cur_aAStage, array[3], array[4], array[5], aMaxlvl, aMaxStamina);
			mAdvFigureDataBuffer.RemoveAt(0);
		}
		while (mAdvFigureDataBuffer.Count > 0 && mAdvFigureDataBuffer[0][0] == 34)
		{
			int[] array2 = mAdvFigureDataBuffer[0];
			ArcadeCharacterSkillLevelUP(array2[1], array2[2], aSeries, aStage, aASeries, cur_aAStage, array2[3], array2[4], array2[5], aMaxlvl, aMaxStamina);
			mAdvFigureDataBuffer.RemoveAt(0);
		}
		while (mAdvFigureDataBuffer.Count > 0 && mAdvFigureDataBuffer[0][0] == 39)
		{
			int[] array3 = mAdvFigureDataBuffer[0];
			ArcadeGetAccessory(array3[1], aASeries, cur_aAStage, array3[2], array3[3], aMaxlvl, aMaxStamina);
			mAdvFigureDataBuffer.RemoveAt(0);
		}
		while (mAdvFigureDataBuffer.Count > 0 && mAdvFigureDataBuffer[0][0] == 45)
		{
			int[] array4 = mAdvFigureDataBuffer[0];
			ArcadeGetItem(array4[1], aSeries, aStage, aASeries, cur_aAStage, array4[2], array4[3], array4[4], array4[5], aMaxlvl, aMaxStamina);
			mAdvFigureDataBuffer.RemoveAt(0);
		}
		mAdvFigureDataBuffer.Clear();
		mSaveAtRequest = true;
		lock (Instance.mDataListLock)
		{
			Instance.SaveCramData();
		}
	}

	public static void GetSaveDataInfo(Player aPlayer, PlayerData aSaveData, ref int ver, ref string lvl_array, ref int xp, ref int cur_series, ref int cur_stage, ref int heart, ref int medal)
	{
		ver = aSaveData.Version;
		lvl_array = GetLevelArrayString(aPlayer);
		xp = aPlayer.Experience;
		cur_series = (int)aSaveData.LastPlaySeries;
		cur_stage = aSaveData.LastPlayLevel;
		if (cur_stage == 0)
		{
			cur_stage = 100;
		}
		heart = aPlayer.LifeCount;
		medal = aPlayer.AdvMaxStamina;
	}

	public static void GetAdvSaveDataInfo(Player aPlayer, AdvSaveData aAdvSaveData, ref int ver, ref string lvl_array, ref int xp, ref int cur_series, ref int cur_stage)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		ver = aAdvSaveData.mAdvSaveVersion;
		int[] array = new int[6];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = 0;
		}
		array[0] = instance.GetMaxPlayableStageID(aAdvSaveData, 0);
		array[1] = instance.GetMaxPlayableStageID(aAdvSaveData, 1);
		array[2] = instance.GetMaxPlayableStageID(aAdvSaveData, 2);
		string[] array2 = new string[array.Length];
		for (int j = 0; j < array.Length; j++)
		{
			array2[j] = string.Empty + array[j];
		}
		lvl_array = string.Join(",", array2);
		xp = (int)Math.Floor(aAdvSaveData.TotalPlayTime);
		cur_series = (int)aAdvSaveData.LastPlaySeries;
		cur_stage = aAdvSaveData.LastPlayLevel;
	}

	public static void SendDataTransferDialog(int aReason, Player aOldPlayer, PlayerData aOldSaveData, AdvSaveData aOldAdvSaveData, string aOldAuthKey)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		string local_path = instance.mPlayerFilePath.Replace('\\', '/');
		string local_path_adv = instance.mAdvFilePath.Replace('\\', '/');
		int local_size = (int)instance.mPlayerFileSize;
		int local_size_adv = (int)instance.mAdvFileSize;
		int ver = 0;
		int ver2 = 0;
		string local_searchid = string.Empty;
		int local_uuid = 0;
		int local_bhiveid = 0;
		string lvl_array = string.Empty;
		string lvl_array2 = string.Empty;
		int xp = 0;
		int xp2 = 0;
		int cur_series = 0;
		int cur_stage = 0;
		int cur_series2 = 0;
		int cur_stage2 = 0;
		int heart = 0;
		int medal = 0;
		string authkey = string.Empty;
		if (aOldSaveData != null)
		{
			local_searchid = ServerCramVariableData.mLocalSearchID;
			local_uuid = ServerCramVariableData.mLocalUUID;
			local_bhiveid = ServerCramVariableData.mLocalHiveId;
			GetSaveDataInfo(aOldPlayer, aOldSaveData, ref ver, ref lvl_array, ref xp, ref cur_series, ref cur_stage, ref heart, ref medal);
		}
		if (aOldAdvSaveData != null)
		{
			GetAdvSaveDataInfo(aOldPlayer, aOldAdvSaveData, ref ver2, ref lvl_array2, ref xp2, ref cur_series2, ref cur_stage2);
		}
		if (aOldAuthKey != null)
		{
			authkey = aOldAuthKey;
		}
		DataTransferDialog(aReason, local_path, local_path_adv, local_size, local_size_adv, ver, ver2, local_searchid, local_uuid, local_bhiveid, lvl_array, lvl_array2, xp, xp2, cur_series, cur_stage, cur_series2, cur_stage2, heart, medal, authkey);
	}

	public static void SendDataRollBack(int aKind, int aRemoteState, Player aPlayer, AdvSaveData aAdvSaveData, Player aBackupPlayer, AdvSaveData aAdvBackupData)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		string local_path = instance.mPlayerFilePath.Replace('\\', '/');
		string local_path_adv = instance.mAdvFilePath.Replace('\\', '/');
		int local_size = (int)instance.mPlayerFileSize;
		int local_size_adv = (int)instance.mAdvFileSize;
		int ver = 0;
		int ver2 = 0;
		string lvl_array = string.Empty;
		string lvl_array2 = string.Empty;
		int xp = 0;
		int xp2 = 0;
		int cur_series = 0;
		int cur_stage = 0;
		int cur_series2 = 0;
		int cur_stage2 = 0;
		int heart = 0;
		int medal = 0;
		int remote_size = (int)ServerCramVariableData.mBackupFileSize;
		int remote_size_adv = (int)ServerCramVariableData.mBackupAdvFileSize;
		int ver3 = 0;
		int ver4 = 0;
		string lvl_array3 = string.Empty;
		string lvl_array4 = string.Empty;
		int xp3 = 0;
		int xp4 = 0;
		int cur_series3 = 0;
		int cur_stage3 = 0;
		int cur_series4 = 0;
		int cur_stage4 = 0;
		int heart2 = 0;
		int medal2 = 0;
		if (aPlayer.Data != null)
		{
			GetSaveDataInfo(aPlayer, aPlayer.Data, ref ver, ref lvl_array, ref xp, ref cur_series, ref cur_stage, ref heart, ref medal);
		}
		if (aAdvSaveData != null)
		{
			GetAdvSaveDataInfo(aPlayer, aAdvSaveData, ref ver2, ref lvl_array2, ref xp2, ref cur_series2, ref cur_stage2);
		}
		if (aBackupPlayer != null && aBackupPlayer.Data != null)
		{
			GetSaveDataInfo(aBackupPlayer, aBackupPlayer.Data, ref ver3, ref lvl_array3, ref xp3, ref cur_series3, ref cur_stage3, ref heart2, ref medal2);
		}
		if (aAdvBackupData != null)
		{
			GetAdvSaveDataInfo(aBackupPlayer, aAdvBackupData, ref ver4, ref lvl_array4, ref xp4, ref cur_series4, ref cur_stage4);
		}
		DataRollBack(aKind, aRemoteState, local_path, local_path_adv, local_size, local_size_adv, ver, ver2, lvl_array, lvl_array2, xp, xp2, cur_series, cur_stage, cur_series2, cur_stage2, heart, medal, remote_size, remote_size_adv, ver3, ver4, lvl_array3, lvl_array4, xp3, xp4, cur_series3, cur_stage3, cur_series4, cur_stage4, heart2, medal2);
	}

	public static void SendDataTransferResult(int aReason, Player aPlayer, Player aTransferPlayer)
	{
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		string local_path = instance.mPlayerFilePath.Replace('\\', '/');
		string local_path_adv = instance.mAdvFilePath.Replace('\\', '/');
		int local_size = (int)instance.mPlayerFileSize;
		int local_size_adv = (int)instance.mAdvFileSize;
		string mPrevSearchID = instance.mServerCramVariableData.mPrevSearchID;
		int mPrevUUID = instance.mServerCramVariableData.mPrevUUID;
		int mPrevHiveId = instance.mServerCramVariableData.mPrevHiveId;
		int ver = 0;
		int ver2 = 0;
		string lvl_array = string.Empty;
		string lvl_array2 = string.Empty;
		int xp = 0;
		int xp2 = 0;
		int cur_series = 0;
		int cur_stage = 0;
		int cur_series2 = 0;
		int cur_stage2 = 0;
		int heart = 0;
		int medal = 0;
		int remote_size = (int)ServerCramVariableData.mTransferFileSize;
		int remote_size_adv = (int)ServerCramVariableData.mTransferAdvFileSize;
		string searchID = aTransferPlayer.Data.SearchID;
		int uUID = aTransferPlayer.Data.UUID;
		int hiveId = aTransferPlayer.Data.HiveId;
		int ver3 = 0;
		int ver4 = 0;
		string lvl_array3 = string.Empty;
		string lvl_array4 = string.Empty;
		int xp3 = 0;
		int xp4 = 0;
		int cur_series3 = 0;
		int cur_stage3 = 0;
		int cur_series4 = 0;
		int cur_stage4 = 0;
		int heart2 = 0;
		int medal2 = 0;
		if (aPlayer.Data != null)
		{
			GetSaveDataInfo(aPlayer, aPlayer.Data, ref ver, ref lvl_array, ref xp, ref cur_series, ref cur_stage, ref heart, ref medal);
		}
		if (aPlayer.AdvSaveData != null)
		{
			GetAdvSaveDataInfo(aPlayer, aPlayer.AdvSaveData, ref ver2, ref lvl_array2, ref xp2, ref cur_series2, ref cur_stage2);
		}
		if (aTransferPlayer.Data != null)
		{
			GetSaveDataInfo(aTransferPlayer, aTransferPlayer.Data, ref ver3, ref lvl_array3, ref xp3, ref cur_series3, ref cur_stage3, ref heart2, ref medal2);
		}
		if (aTransferPlayer.AdvSaveData != null)
		{
			GetAdvSaveDataInfo(aTransferPlayer, aTransferPlayer.AdvSaveData, ref ver4, ref lvl_array4, ref xp4, ref cur_series4, ref cur_stage4);
		}
		DataTransferResult(aReason, local_path, local_path_adv, local_size, local_size_adv, ver, ver2, mPrevSearchID, mPrevUUID, mPrevHiveId, lvl_array, lvl_array2, xp, xp2, cur_series, cur_stage, cur_series2, cur_stage2, heart, medal, remote_size, remote_size_adv, ver3, ver4, searchID, uUID, hiveId, lvl_array3, lvl_array4, xp3, xp4, cur_series3, cur_stage3, cur_series4, cur_stage4, heart2, medal2);
	}
}
