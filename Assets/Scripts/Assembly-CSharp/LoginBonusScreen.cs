using System;
using System.Collections;
using UnityEngine;

public class LoginBonusScreen : MonoBehaviour
{
	public enum STATE
	{
		INIT = 0,
		IN = 1,
		STAMP = 2,
		WAIT_OK_BUTTON = 3,
		OUT = 4,
		WAIT = 5,
		WAIT_INIT = 6
	}

	public delegate void OnLoginBonusFinished();

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT_INIT);

	private GameMain mGame;

	private Camera mMainCamera;

	private Camera mSubCamera;

	private UIPanel mLoginBonusPanel;

	private GameObject mAnchorBottom;

	private GameObject mAnchorTopRight;

	private GameObject mAnchorBottomLeft;

	private GameObject mStampRoot;

	private GameObject mStampFrameRoot;

	private GameObject mMessageRoot;

	private bool mLaceMove;

	private UISprite mBg;

	private UIButton mOKButton;

	private GameObject mStampTarget;

	private string mIconName;

	private string mQuantity;

	private int mCategory;

	private int mSubCategory;

	private WWW mWww;

	private UITexture mBannerTexture;

	private Live2DRender mL2DRender;

	private UITexture mL2DTexture;

	private Live2DInstance mL2DInst;

	private GetLoginBonusResponse mLoginBonusResponce;

	public OnLoginBonusFinished OnFinishedCallback = delegate
	{
	};

	private static int STAMP_NUM_H = 5;

	private static float STAMP_PITCH_H = 100f;

	private static float STAMP_PITCH_V = 100f;

	private static int BG_DEPTH;

	private static int UI_DEPTH = 10;

	protected ScreenOrientation mOrientation;

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Start()
	{
		GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.DEMO_CAMERA));
		gameObject.transform.parent = mGame.mCamera.transform.parent;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		mMainCamera = mGame.mCamera;
		mSubCamera = gameObject.GetComponent<Camera>();
		mSubCamera.eventMask = mSubCamera.gameObject.layer;
		mLoginBonusPanel = Util.CreateGameObject("LoginBonusPanel", mSubCamera.gameObject).AddComponent<UIPanel>();
		mLoginBonusPanel.depth = mGame.mDialogManager.GrayScreenDepth + 1;
		mLoginBonusPanel.sortingOrder = mGame.mDialogManager.GrayScreenDepth + 1;
		mAnchorBottom = Util.CreateAnchorWithChild("AnchorBottom", mLoginBonusPanel.gameObject, UIAnchor.Side.Bottom, mSubCamera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("AnchorTopRight", mLoginBonusPanel.gameObject, UIAnchor.Side.TopRight, mSubCamera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("AnchorBottomLeft", mLoginBonusPanel.gameObject, UIAnchor.Side.BottomLeft, mSubCamera).gameObject;
		StartCoroutine(InitializeAsync());
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			StateINIT();
			break;
		case STATE.IN:
			StateIN();
			break;
		case STATE.STAMP:
			StateSTAMP();
			break;
		case STATE.WAIT_OK_BUTTON:
			if (mGame.mBackKeyTrg)
			{
				OnCancelPushed(null);
			}
			break;
		case STATE.OUT:
			StateOUT();
			break;
		case STATE.WAIT_INIT:
			return;
		}
		mState.Update();
		if (mOrientation != Util.ScreenOrientation)
		{
			SetLayout(Util.ScreenOrientation);
			mOrientation = Util.ScreenOrientation;
		}
	}

	private void StateINIT()
	{
		BIJImage image = ResourceManager.LoadImage("LOGIN_BONUS_HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		mBg = Util.CreateSprite("Bg", mLoginBonusPanel.gameObject, image);
		Util.SetSpriteInfo(mBg, "lb_back", BG_DEPTH, Vector3.zero, Vector3.one, false);
		mBg.type = UIBasicSprite.Type.Tiled;
		mStampRoot = Util.CreateGameObject("StampRoot", mLoginBonusPanel.gameObject);
		mStampFrameRoot = Util.CreateGameObject("StampRoot", mStampRoot);
		mMessageRoot = Util.CreateGameObject("MessageRoot", mLoginBonusPanel.gameObject);
		int count = mLoginBonusResponce.RewardList.Count;
		int num = count / STAMP_NUM_H;
		if (count % STAMP_NUM_H != 0)
		{
			num++;
		}
		float num2 = ((float)num / 2f - 0.5f) * STAMP_PITCH_V;
		num2 -= 24f;
		UISprite uISprite;
		if (DateTimeUtil.BetweenDays(mLoginBonusResponce.StartTime, mLoginBonusResponce.EndTime) < 100)
		{
			uISprite = Util.CreateSprite("TotalFrame", mStampRoot, image);
			Util.SetSpriteInfo(uISprite, "panel_total", UI_DEPTH + 1, new Vector3(0f, num2 + 64f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(500, 30);
			UILabel uILabel = Util.CreateLabel("TotalDay", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty, UI_DEPTH + 2, Vector3.zero, 24, 0, 0, UIWidget.Pivot.Center);
			uILabel.text = Util.MakeLText("LoginBonus_Desc", mLoginBonusResponce.StartTime.Month, mLoginBonusResponce.StartTime.Day, mLoginBonusResponce.StartTime.Hour, mLoginBonusResponce.StartTime.Minute, mLoginBonusResponce.EndTime.Month, mLoginBonusResponce.EndTime.Day, mLoginBonusResponce.EndTime.Hour, mLoginBonusResponce.EndTime.Minute);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(430, 24);
			uILabel.color = Color.white;
		}
		float num3 = ((float)STAMP_NUM_H / 2f - 0.5f) * STAMP_PITCH_H * -1f;
		if (count < STAMP_NUM_H)
		{
			num3 += (float)(STAMP_NUM_H - count) / 2f * STAMP_PITCH_H;
		}
		float num4 = 0f;
		mStampTarget = null;
		GameObject parent;
		for (int i = 0; i < count; i++)
		{
			uISprite = Util.CreateSprite("DayFrame", mStampFrameRoot, image);
			Util.SetSpriteInfo(uISprite, "panel_day", UI_DEPTH + 1, new Vector3(num3, num2, 0f), Vector3.one, false);
			parent = uISprite.gameObject;
			string getLoginBonusIcon = mLoginBonusResponce.RewardList[i].GetLoginBonusIcon;
			uISprite = Util.CreateSprite("Icon", parent, image);
			Util.SetSpriteInfo(uISprite, getLoginBonusIcon, UI_DEPTH + 2, Vector3.zero, new Vector3(0.6f, 0.6f, 1f), false);
			UILabel uILabel = Util.CreateLabel("Num", parent, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty, UI_DEPTH + 3, Vector3.zero, 20, 0, 0, UIWidget.Pivot.Center);
			if (mLoginBonusResponce.RewardList[i].Category == 25 && mLoginBonusResponce.RewardList[i].SubCategory == 1)
			{
				uILabel.text = string.Empty + mLoginBonusResponce.RewardList[i].Quantity;
				uILabel.transform.localPosition = new Vector3(0f, -32f, 0f);
				uISprite.transform.localPosition = new Vector3(0f, 6f, 0f);
			}
			else
			{
				uILabel.text = Util.MakeLText("Gift_Number", mLoginBonusResponce.RewardList[i].Quantity);
				uILabel.transform.localPosition = new Vector3(20f, -30f, 0f);
			}
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.effectStyle = UILabel.Effect.Outline;
			uILabel.effectDistance = new Vector2(2f, 2f);
			uILabel.effectColor = Color.white;
			if (mLoginBonusResponce.RewardList[i].IsReceived)
			{
				if (i == mLoginBonusResponce.LoginDayCount - 1)
				{
					mStampTarget = parent;
					mIconName = getLoginBonusIcon;
					mQuantity = uILabel.text;
					mCategory = mLoginBonusResponce.RewardList[i].Category;
					mSubCategory = mLoginBonusResponce.RewardList[i].SubCategory;
				}
				else
				{
					uISprite = Util.CreateSprite("Stamp", parent, image);
					Util.SetSpriteInfo(uISprite, "login_clearstamp", UI_DEPTH + 4, Vector3.zero, new Vector3(1f, 1f, 1f), false);
				}
			}
			num4 += 1f;
			num3 += STAMP_PITCH_H;
			if (num4 >= (float)STAMP_NUM_H)
			{
				num4 = 0f;
				num2 -= STAMP_PITCH_V;
				num3 = ((float)STAMP_NUM_H / 2f - 0.5f) * STAMP_PITCH_H * -1f;
			}
		}
		if (count < STAMP_NUM_H)
		{
			float num5 = 1f + (float)(STAMP_NUM_H - count) * 0.1f;
			float num6 = (float)(STAMP_NUM_H - count) * 12f;
			mStampFrameRoot.transform.localScale = new Vector3(num5, num5, 1f);
			mStampFrameRoot.transform.localPosition = new Vector3(0f, 0f - num6, 0f);
		}
		uISprite = Util.CreateSprite("MessageFrame", mMessageRoot, image);
		Util.SetSpriteInfo(uISprite, "panel_day", UI_DEPTH, new Vector3(0f, 160f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(500, 100);
		parent = uISprite.gameObject;
		uISprite = Util.CreateSprite("Message", parent, image);
		Util.SetSpriteInfo(uISprite, "panel_text00", UI_DEPTH + 1, Vector3.zero, Vector3.one, false);
		if (count > 1)
		{
			uISprite.transform.localPosition = new Vector3(0f, 14f, 0f);
			uISprite = Util.CreateSprite("Message", parent, image);
			Util.SetSpriteInfo(uISprite, "panel_text01", UI_DEPTH + 1, new Vector3(0f, -14f, 0f), Vector3.one, false);
		}
		StartCoroutine(CoCreateTitleBanner());
		StartCoroutine(CoCreateCharacter());
		mOrientation = Util.ScreenOrientation;
		SetLayout(mOrientation);
		mLoginBonusPanel.transform.localScale = new Vector3(1.3f, 1.3f, 1f);
		mState.Change(STATE.IN);
	}

	private void StateIN()
	{
		if (mState.IsChanged())
		{
			StartCoroutine(CoIn());
		}
	}

	private void StateSTAMP()
	{
		if (mState.IsChanged())
		{
			StartCoroutine(CoStamp());
		}
	}

	private void StateOUT()
	{
		if (mState.IsChanged())
		{
			StartCoroutine(CoOut());
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (mBg != null)
			{
				mBg.transform.localScale = new Vector3(1.26f, 1.26f, 1f);
			}
			if (mStampRoot != null)
			{
				mStampRoot.transform.localPosition = new Vector3(0f, 210f, 0f);
			}
			if (mMessageRoot != null)
			{
				mMessageRoot.transform.localPosition = new Vector3(0f, -240f, 0f);
			}
			if (mOKButton != null)
			{
				mOKButton.transform.localPosition = new Vector3(0f, 52f, 0f);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mBg != null)
			{
				mBg.transform.localScale = new Vector3(1.26f, 1f, 1f);
			}
			if (mStampRoot != null)
			{
				mStampRoot.transform.localPosition = new Vector3(-280f, 0f, 0f);
			}
			if (mMessageRoot != null)
			{
				mMessageRoot.transform.localPosition = new Vector3(280f, 0f, 0f);
			}
			if (mOKButton != null)
			{
				if (Util.IsWideScreen())
				{
					mOKButton.transform.localPosition = new Vector3(134f, 52f, 0f);
				}
				else
				{
					mOKButton.transform.localPosition = new Vector3(0f, 52f, 0f);
				}
			}
			break;
		}
		if (mBg != null)
		{
			Vector2 vector = Util.LogScreenSize();
			float num = vector.y / 910f;
			mBg.transform.localScale = new Vector3(num, num, 1f);
			mBg.SetDimensions((int)(vector.x / num), (int)(vector.y / num));
		}
	}

	private IEnumerator InitializeAsync()
	{
		yield return null;
		ResImage resImage = ResourceManager.LoadImageAsync("LOGIN_BONUS_HUD");
		while (resImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		mState.Reset(STATE.INIT, true);
	}

	private IEnumerator CoCreateTitleBanner()
	{
		string url = mLoginBonusResponce.ImageURL;
		mWww = new WWW(url);
		while (!mWww.isDone && mWww.error == null)
		{
			yield return 0;
		}
		if (mWww.error == null)
		{
			Texture2D texture = mWww.texture;
			mBannerTexture = Util.CreateGameObject("Texture", mStampRoot).AddComponent<UITexture>();
			mBannerTexture.mainTexture = texture;
			mBannerTexture.depth = UI_DEPTH + 1;
			mBannerTexture.pivot = UIWidget.Pivot.Top;
			mBannerTexture.SetDimensions(texture.width, texture.height);
			mBannerTexture.transform.localPosition = new Vector3(0f, 310f, 0f);
		}
	}

	private IEnumerator CoCreateCharacter()
	{
		mL2DRender = mGame.CreateLive2DRender(568, 568);
		yield return 0;
		mL2DInst = Util.CreateGameObject("Char", mL2DRender.gameObject).AddComponent<Live2DInstance>();
		mL2DInst.Init(mLoginBonusResponce.CompanionName, Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER, Color.white, true);
		while (!mL2DInst.IsLoadFinished())
		{
			yield return 0;
		}
		mL2DInst.StartMotion(mLoginBonusResponce.CompanionBeforeMotionName, true);
		mL2DRender.ResisterInstance(mL2DInst);
		yield return 0;
		if (mMessageRoot == null)
		{
			yield return 0;
		}
		mL2DTexture = Util.CreateGameObject("Texture", mMessageRoot).AddComponent<UITexture>();
		mL2DTexture.mainTexture = mL2DRender.RenderTexture;
		mL2DTexture.SetDimensions(568, 568);
		mL2DTexture.transform.localPosition = new Vector3(140f, -40f, 0f);
		mL2DTexture.depth = UI_DEPTH + 5;
		float scale = mLoginBonusResponce.CompanionScale;
		mL2DTexture.transform.localScale = new Vector3(scale, scale, 1f);
	}

	private IEnumerator LaceRotate(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		BIJImage imageLoginBonusHud = ResourceManager.LoadImage("LOGIN_BONUS_HUD").Image;
		UISprite sprite = Util.CreateSprite("Lace", parent, imageLoginBonusHud);
		Util.SetSpriteInfo(sprite, "lb_lace", depth, pos, scale, false);
		float angle = 0f;
		while (mLaceMove)
		{
			angle += Time.deltaTime * speed;
			sprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
		UnityEngine.Object.Destroy(sprite.gameObject);
	}

	private IEnumerator CoIn()
	{
		mGame.PlaySe("SE_DIALOG_OPEN", -1);
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 360f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float scale = 1f + (1f - Mathf.Sin(angle * ((float)Math.PI / 180f))) * 0.3f;
			mLoginBonusPanel.transform.localScale = new Vector3(scale, scale, 1f);
			yield return 0;
		}
		mLaceMove = true;
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, BG_DEPTH + 1, new Vector3(30f, -110f, 0f), new Vector3(1f, 1f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, BG_DEPTH + 1, new Vector3(-140f, -10f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, BG_DEPTH + 1, new Vector3(190f, -50f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, BG_DEPTH + 1, new Vector3(-20f, 60f, 0f), new Vector3(1f, 1f, 1f), 15f));
		mState.Change(STATE.STAMP);
	}

	private IEnumerator CoOut()
	{
		yield return new WaitForSeconds(0.2f);
		UnityEngine.Object.Destroy(mOKButton.gameObject);
		mOKButton = null;
		mLaceMove = false;
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 360f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float scale = 1f + Mathf.Sin(angle * ((float)Math.PI / 180f)) * 0.3f;
			mLoginBonusPanel.transform.localScale = new Vector3(scale, scale, 1f);
			yield return 0;
		}
		OnFinishedCallback();
		if (mSubCamera != null)
		{
			UnityEngine.Object.Destroy(mSubCamera.gameObject);
			mSubCamera = null;
		}
		if (mLoginBonusPanel != null)
		{
			UnityEngine.Object.Destroy(mLoginBonusPanel.gameObject);
			mLoginBonusPanel = null;
		}
		if (mL2DRender != null)
		{
			UnityEngine.Object.Destroy(mL2DRender.gameObject);
			mL2DRender = null;
		}
		if (mWww != null)
		{
			mWww.Dispose();
			mWww = null;
		}
		ResourceManager.UnloadImage("LOGIN_BONUS_HUD");
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private IEnumerator CoStamp()
	{
		if (mStampTarget == null)
		{
			yield break;
		}
		yield return new WaitForSeconds(0.2f);
		BIJImage imageLoginBonusHud = ResourceManager.LoadImage("LOGIN_BONUS_HUD").Image;
		BIJImage imageHud = ResourceManager.LoadImage("HUD").Image;
		UIFont font = GameMain.LoadFont();
		UISprite sprite4 = Util.CreateSprite("Stamp", mStampTarget, imageLoginBonusHud);
		Util.SetSpriteInfo(sprite4, "login_clearstamp", UI_DEPTH + 5, Vector3.zero, new Vector3(1f, 1f, 1f), false);
		bool skip = false;
		float angle4 = 0f;
		while (angle4 < 90f)
		{
			angle4 += Time.deltaTime * 90f;
			if (angle4 > 90f)
			{
				angle4 = 90f;
			}
			float scale3 = 1f + (1f - Mathf.Sin(angle4 * ((float)Math.PI / 180f))) * 2f;
			float alpha3 = Mathf.Sin(angle4 * ((float)Math.PI / 180f));
			sprite4.transform.localScale = new Vector3(scale3, scale3, 1f);
			sprite4.transform.localRotation = Quaternion.Euler(0f, 0f, 90f - angle4);
			sprite4.color = new Color(1f, 1f, 1f, alpha3);
			if (mGame.mTouchDownTrg)
			{
				sprite4.transform.localScale = Vector3.one;
				sprite4.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
				sprite4.color = Color.white;
				skip = true;
				break;
			}
			yield return 0;
		}
		mGame.PlaySe("SE_RANKUP1", -1);
		sprite4 = Util.CreateSprite("Stamp", mStampTarget, imageLoginBonusHud);
		Util.SetSpriteInfo(sprite4, "login_clearstamp", UI_DEPTH + 4, Vector3.zero, new Vector3(1f, 1f, 1f), false);
		angle4 = 0f;
		while (angle4 < 90f)
		{
			angle4 += Time.deltaTime * 180f;
			if (angle4 > 90f)
			{
				angle4 = 90f;
			}
			float scale2 = 1f + Mathf.Sin(angle4 * ((float)Math.PI / 180f));
			float alpha2 = 1f - Mathf.Sin(angle4 * ((float)Math.PI / 180f));
			sprite4.transform.localScale = new Vector3(scale2, scale2, 1f);
			sprite4.color = new Color(1f, 1f, 1f, alpha2);
			if (mGame.mTouchDownTrg || skip)
			{
				sprite4.transform.localScale = Vector3.one;
				sprite4.color = Color.clear;
				skip = true;
				break;
			}
			yield return 0;
		}
		UISprite uISprite = Util.CreateSprite("PresentLace", mMessageRoot, imageLoginBonusHud);
		UISprite lace = uISprite;
		Util.SetSpriteInfo(lace, "present_lace", UI_DEPTH + 1, new Vector3(-130f, 0f, 0f), Vector3.one, false);
		sprite4 = Util.CreateSprite("PresentMessage", lace.gameObject, imageLoginBonusHud);
		Util.SetSpriteInfo(sprite4, "next_present", UI_DEPTH + 2, new Vector3(0f, 60f, 0f), Vector3.one, false);
		angle4 = 0f;
		while (angle4 < 90f)
		{
			angle4 += Time.deltaTime * 360f;
			if (angle4 > 90f)
			{
				angle4 = 90f;
			}
			float scale = 1f + (1f - Mathf.Sin(angle4 * ((float)Math.PI / 180f)));
			float alpha = Mathf.Sin(angle4 * ((float)Math.PI / 180f));
			lace.transform.localScale = new Vector3(scale, scale, 1f);
			lace.color = new Color(1f, 1f, 1f, alpha);
			if (mGame.mTouchDownTrg || skip)
			{
				lace.transform.localScale = Vector3.one;
				lace.color = Color.white;
				skip = true;
				break;
			}
			yield return 0;
		}
		if (!skip)
		{
			yield return new WaitForSeconds(0.2f);
		}
		mGame.PlaySe("SE_ACCESSORY_UNLOCK", -1);
		float spriteBaseScale = 1f;
		sprite4 = Util.CreateSprite("Icon", lace.gameObject, imageLoginBonusHud);
		Util.SetSpriteInfo(sprite4, mIconName, UI_DEPTH + 2, Vector3.zero, Vector3.one, false);
		UILabel label = Util.CreateLabel("Num", sprite4.gameObject, font);
		Util.SetLabelInfo(label, string.Empty, UI_DEPTH + 3, Vector3.zero, 36, 0, 0, UIWidget.Pivot.Center);
		if (mCategory == 25 && mSubCategory == 1)
		{
			label.transform.localPosition = new Vector3(0f, -68f, 0f);
			sprite4.transform.localScale = new Vector3(0.8f, 0.8f, 0f);
			spriteBaseScale = 0.8f;
		}
		else
		{
			label.transform.localPosition = new Vector3(40f, -30f, 0f);
			sprite4.transform.localPosition = new Vector3(-10f, -10f, 0f);
		}
		label.text = mQuantity;
		label.color = Def.DEFAULT_MESSAGE_COLOR;
		label.effectStyle = UILabel.Effect.Outline;
		label.effectDistance = new Vector2(2f, 2f);
		label.effectColor = Color.white;
		angle4 = 0f;
		while (angle4 < 90f)
		{
			angle4 += Time.deltaTime * 90f;
			if (angle4 > 90f)
			{
				angle4 = 90f;
			}
			float effectScale = (1f - Mathf.Sin(angle4 * ((float)Math.PI / 180f))) * 0.5f;
			float x = Mathf.Sin(angle4 * 8f * ((float)Math.PI / 180f));
			float y = 0f - x;
			sprite4.transform.localScale = new Vector3(spriteBaseScale + x * effectScale, spriteBaseScale + y * effectScale, 1f);
			if (mGame.mTouchDownTrg || skip)
			{
				sprite4.transform.localScale = new Vector3(spriteBaseScale, spriteBaseScale, 1f);
				skip = true;
				break;
			}
			yield return 0;
		}
		Util.CreateUISsSprite("Effect", sprite4.gameObject, "EFFECT_BUTTON_SHINE", new Vector3(0f, 0f, 0f), new Vector3(0.5f, 0.5f, 1f), UI_DEPTH + 4);
		if (mL2DInst != null)
		{
			while (!mL2DInst.IsLoadFinished())
			{
				yield return 0;
			}
			mL2DInst.StartMotion(mLoginBonusResponce.CompanionAfterMotionName, true);
		}
		mGame.PlaySe("VOICE_017", 2);
		if (!skip)
		{
			yield return new WaitForSeconds(0.3f);
		}
		mOKButton = Util.CreateJellyImageButton("ButtonOK", mAnchorBottom.gameObject, imageHud);
		Util.SetImageButtonInfo(mOKButton, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", UI_DEPTH + 1, new Vector3(0f, 52f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mOKButton, this, "OnOkPushed", UIButtonMessage.Trigger.OnClick);
		SetLayout(mOrientation);
		mState.Change(STATE.WAIT_OK_BUTTON);
	}

	public void init(GetLoginBonusResponse loginBonusResponce, OnLoginBonusFinished proc)
	{
		mLoginBonusResponce = loginBonusResponce;
		OnFinishedCallback = proc;
		mGame.DeleteLocalFile(Constants.LOGINBONUS_BACKUP_FILE);
	}

	private void OnOkPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.WAIT_OK_BUTTON)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mState.Change(STATE.OUT);
		}
	}

	private void OnCancelPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.WAIT_OK_BUTTON)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mState.Change(STATE.OUT);
		}
	}
}
