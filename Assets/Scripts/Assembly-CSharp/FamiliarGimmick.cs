using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FamiliarGimmick : MonoBehaviour
{
	public enum STATE
	{
		LOAD = 0,
		CREATE = 1,
		ENTER = 2,
		DEPLOY = 3,
		STAY = 4,
		MOVE = 5,
		DAMAGE = 6,
		DEAD = 7,
		DEAD_FINISHED = 8
	}

	public struct FAMILIAR_SETTING
	{
		public string idle0;

		public string idle1;

		public string hit0;

		public string hit1;

		public string dead;

		public string smoke;

		public short life;

		public IntVector2[] smokeArea;

		public FAMILIAR_SETTING(string _idle0, string _idle1, string _hit0, string _hit1, string _dead, string _smoke, short _life, IntVector2[] _smokeArea)
		{
			idle0 = _idle0;
			idle1 = _idle1;
			hit0 = _hit0;
			hit1 = _hit1;
			dead = _dead;
			smoke = _smoke;
			life = _life;
			smokeArea = _smokeArea;
		}
	}

	public delegate void MoveFinishedDelegate(int listIndex);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD);

	private GameMain mGame;

	private PuzzleManager mPuzzle;

	private SsSprite mSprite;

	private List<SsSprite> mSmokeList = new List<SsSprite>();

	private int mIndex;

	private int mLifeMax;

	private int mLife;

	private IntVector2 mTilePos;

	private bool mDamaged;

	private bool mSmokeDeploy;

	public static FAMILIAR_SETTING[] FamiliarSetting = new FAMILIAR_SETTING[1]
	{
		new FAMILIAR_SETTING("GIMMICK_FAMILIAR_IDLE0", "GIMMICK_FAMILIAR_IDLE1", "GIMMICK_FAMILIAR_HIT0", "GIMMICK_FAMILIAR_HIT1", "GIMMICK_FAMILIAR_DEAD", "GIMMICK_FAMILIAR_SMOKE", 2, new IntVector2[3]
		{
			new IntVector2(-1, 0),
			new IntVector2(0, 0),
			new IntVector2(1, 0)
		})
	};

	private void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.CREATE:
			CreateSprites();
			Enter();
			break;
		case STATE.DAMAGE:
			if (!mSprite.IsPlaying())
			{
				Stay();
			}
			break;
		case STATE.DEAD:
			if (!mSprite.IsPlaying())
			{
				mState.Change(STATE.DEAD_FINISHED);
			}
			break;
		}
		mState.Update();
	}

	public void Init(int familiarIndex, IntVector2 tilePos, PuzzleManager puzzle)
	{
		mPuzzle = puzzle;
		mIndex = familiarIndex;
		mLifeMax = FamiliarSetting[mIndex].life;
		mLife = mLifeMax;
		mTilePos = tilePos;
		mDamaged = false;
		mSmokeDeploy = false;
		StartCoroutine(LoadAnimation());
	}

	private IEnumerator LoadAnimation()
	{
		List<ResourceInstance> resList = new List<ResourceInstance>
		{
			ResourceManager.LoadSsAnimationAsync(FamiliarSetting[mIndex].idle0),
			ResourceManager.LoadSsAnimationAsync(FamiliarSetting[mIndex].idle1),
			ResourceManager.LoadSsAnimationAsync(FamiliarSetting[mIndex].hit0),
			ResourceManager.LoadSsAnimationAsync(FamiliarSetting[mIndex].hit1),
			ResourceManager.LoadSsAnimationAsync(FamiliarSetting[mIndex].dead),
			ResourceManager.LoadSsAnimationAsync(FamiliarSetting[mIndex].smoke)
		};
		while (true)
		{
			bool wait = false;
			foreach (ResourceInstance res in resList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
		mState.Change(STATE.CREATE);
	}

	private void CreateSprites()
	{
		mSprite = Util.CreateGameObject("Body", base.gameObject).AddComponent<SsSprite>();
		Vector2 defaultCoords = mPuzzle.GetDefaultCoords(mTilePos);
		mSprite.transform.localPosition = new Vector3(defaultCoords.x, defaultCoords.y, 5f);
		mSprite.Scale = new Vector3(0.8f, 0.8f, 1f);
		Vector3 localPosition = mSprite.transform.localPosition;
		for (int i = 0; i < FamiliarSetting[mIndex].smokeArea.Length; i++)
		{
			SsSprite ssSprite = Util.CreateGameObject("Smoke", base.gameObject).AddComponent<SsSprite>();
			ChangeAnime(ssSprite, FamiliarSetting[mIndex].smoke, 0);
			defaultCoords = mPuzzle.GetDefaultCoords(mTilePos + FamiliarSetting[mIndex].smokeArea[i]);
			ssSprite.transform.localPosition = new Vector3(defaultCoords.x, defaultCoords.y, localPosition.z + 1f);
			ssSprite.gameObject.SetActive(false);
			mSmokeList.Add(ssSprite);
		}
		Stay();
	}

	private void Stay()
	{
		switch (mLife)
		{
		case 1:
			ChangeAnime(mSprite, FamiliarSetting[mIndex].idle1, 0);
			break;
		case 2:
			ChangeAnime(mSprite, FamiliarSetting[mIndex].idle0, 0);
			break;
		}
		mState.Change(STATE.STAY);
	}

	private void Enter()
	{
		StartCoroutine(CoEnter());
		mState.Change(STATE.ENTER);
	}

	private IEnumerator CoEnter()
	{
		yield return 0;
		Stay();
	}

	private void Deploy()
	{
		Vector3 localPosition = mSprite.transform.localPosition;
		for (int i = 0; i < mSmokeList.Count; i++)
		{
			Vector2 defaultCoords = mPuzzle.GetDefaultCoords(mTilePos + FamiliarSetting[mIndex].smokeArea[i]);
			mSmokeList[i].transform.localPosition = new Vector3(defaultCoords.x, defaultCoords.y, localPosition.z + 1f);
			if (mPuzzle.GetBackground(mTilePos + FamiliarSetting[mIndex].smokeArea[i]))
			{
				StartCoroutine(CoSmokeOpen(mSmokeList[i]));
			}
		}
		mSmokeDeploy = true;
	}

	private IEnumerator CoSmokeOpen(SsSprite sprite)
	{
		yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 0.2f));
		sprite.gameObject.SetActive(true);
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 90f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float scale = Mathf.Sin((float)Math.PI / 180f * angle) * 0.8f;
			sprite.transform.localScale = new Vector3(scale, scale, 1f);
			yield return 0;
		}
	}

	private IEnumerator CoSmokeClose(SsSprite sprite)
	{
		if (!sprite.gameObject.activeSelf)
		{
			yield break;
		}
		yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 0.2f));
		float angle = 90f;
		while (angle > 0f)
		{
			angle -= Time.deltaTime * 90f;
			if (angle < 0f)
			{
				angle = 0f;
			}
			float scale = Mathf.Sin((float)Math.PI / 180f * angle);
			sprite.transform.localScale = new Vector3(scale, scale, 1f);
			yield return 0;
		}
		sprite.gameObject.SetActive(false);
	}

	public bool Move()
	{
		if (mDamaged)
		{
			mDamaged = false;
			StartCoroutine(CoMove());
			mState.Reset(STATE.MOVE);
			return true;
		}
		if (!mSmokeDeploy)
		{
			Deploy();
		}
		return false;
	}

	private IEnumerator CoMove()
	{
		yield return new WaitForSeconds(1f);
		IntVector2 targetTilePos = null;
		PuzzleTile[] tiles = mPuzzle.GetRandomColorTiles(true);
		PuzzleTile[] array = tiles;
		foreach (PuzzleTile targetTile in array)
		{
			ISMTileInfo info = mGame.mCurrentStage.GetTileInfo(targetTile.TilePos.x, targetTile.TilePos.y);
			if (info == null || !info.MakeObstacleArea)
			{
				continue;
			}
			bool hit = false;
			for (int i = 0; i < mSmokeList.Count; i++)
			{
				if (mPuzzle.AllFamiliarHitCheck(targetTile.TilePos + FamiliarSetting[mIndex].smokeArea[i]))
				{
					hit = true;
					break;
				}
			}
			if (!hit)
			{
				targetTilePos = targetTile.TilePos;
				break;
			}
		}
		if (targetTilePos != null && mTilePos != targetTilePos)
		{
			mTilePos = targetTilePos;
			Vector3 fromPos = mSprite.transform.localPosition;
			Vector2 targetPos = mPuzzle.GetDefaultCoords(targetTilePos);
			Vector3 distance = new Vector3(targetPos.x, targetPos.y, fromPos.z) - fromPos;
			mGame.PlaySe("SE_FAMILIAR_MOVE", -1);
			float angle = 0f;
			while (angle < 90f)
			{
				angle += Time.deltaTime * 180f;
				if (angle > 90f)
				{
					angle = 90f;
				}
				Vector3 v = distance * Mathf.Sin((float)Math.PI / 180f * angle);
				mSprite.transform.localPosition = fromPos + v;
				yield return 0;
			}
		}
		Deploy();
		yield return new WaitForSeconds(1f);
		Stay();
	}

	private void Damage()
	{
		StartCoroutine(CoDamage());
		mGame.PlaySe("SE_FAMILIAR_DAMAGE", -1);
		mState.Change(STATE.DAMAGE);
	}

	private IEnumerator CoDamage()
	{
		for (int i = 0; i < mSmokeList.Count; i++)
		{
			StartCoroutine(CoSmokeClose(mSmokeList[i]));
		}
		mSmokeDeploy = false;
		int num = mLife + 1;
		if (num != 1 && num == 2)
		{
			ChangeAnime(mSprite, FamiliarSetting[mIndex].hit0, 1);
		}
		else
		{
			ChangeAnime(mSprite, FamiliarSetting[mIndex].hit1, 1);
		}
		while (!mSprite.IsAnimationFinished())
		{
			yield return 0;
		}
		if (mLife <= 0)
		{
			Dead();
		}
		else
		{
			Stay();
		}
	}

	private void Dead()
	{
		mGame.PlaySe("SE_FAMILIAR_DEAD", -1);
		ChangeAnime(mSprite, FamiliarSetting[mIndex].dead, 1);
		mState.Reset(STATE.DEAD, true);
	}

	private void ChangeAnime(SsSprite sprite, string key, int playCount)
	{
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(key);
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		sprite.Animation = ssAnime;
		sprite.ResetAnimationStatus();
		sprite.Play();
		sprite.PlayCount = playCount;
	}

	public bool OnCrush(IntVector2 tilePos, bool force = false)
	{
		if (mDamaged && !force)
		{
			return false;
		}
		if (HitCheck(tilePos))
		{
			mDamaged = true;
			mLife--;
			Damage();
			if (mLife <= 0)
			{
				return true;
			}
		}
		return false;
	}

	public bool HitCheck(IntVector2 tilePos)
	{
		for (int i = 0; i < FamiliarSetting[mIndex].smokeArea.Length; i++)
		{
			IntVector2 intVector = mTilePos + FamiliarSetting[mIndex].smokeArea[i];
			if (intVector.x == tilePos.x && intVector.y == tilePos.y)
			{
				return true;
			}
		}
		return false;
	}

	public bool IsStay()
	{
		if (mState.GetStatus() == STATE.STAY)
		{
			return true;
		}
		return false;
	}
}
