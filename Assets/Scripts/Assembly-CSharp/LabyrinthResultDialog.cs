using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class LabyrinthResultDialog : DialogBase
{
	private enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		SHOW_BOX = 2,
		COUNT_UP_JEWEL_1 = 3,
		POP_DOUBLE_UP_CHANCE_DIALOG = 4,
		SHOW_DOUBLE_UP_CHANCE = 5,
		COUNT_UP_JEWEL_2 = 6,
		OPEN_FIRST_CLEAR_BOX = 7,
		COUNT_UP_JEWEL_3_0 = 8,
		COUNT_UP_JEWEL_3_1 = 9,
		COUNT_UP_JEWEL_3_2 = 10,
		SHOW_CLOSE_BUTTON = 11,
		SHOW_OK_BUTTON = 12
	}

	private enum LOAD_ATLAS
	{
		HUD = 0,
		DIALOG_BASE = 1,
		PUZZLE_CONTINUE_HUD_LABYRINTH = 2,
		EVENTLABYRINTH_POINT = 3,
		EVENTLABYRINTH_REWARD = 4
	}

	private enum LOAD_SOUND
	{
		SE_POINTEV_ADD_0 = 0,
		SE_GC_CAPSULEOPEN_NORMAL = 1,
		SE_ORB_BREAK = 2,
		SE_DIALY_COMPLETE_STAMP = 3,
		SE_LABYRINTH_COURSE_CLEAR = 4,
		SE_MAP_GET_RBKEY = 5
	}

	public enum DISPLAY_TYPE
	{
		RESULT = 0,
		COURSE_CLEAR = 1,
		STAGE_ALL_CLEAR = 2
	}

	public enum SELECT_ITEM
	{
		OnClosed = 0,
		OnUpdatePossess = 1
	}

	private enum DOUBLE_UP_CHANCE_STATE
	{
		NoOffer = 0,
		Cancel = 1,
		Consume = 2
	}

	public enum CALLBACK_BREAKDOWN
	{
		IN_PUZZLE = 0,
		COURSE_BONUS = 1,
		DOUBLE_UP_CHANCE = 2,
		FIRST_CLEAR = 3
	}

	public sealed class ResourceInfo<T> : IDisposable where T : ResourceInstance
	{
		private bool mInit;

		private bool mForceNotUnload;

		private bool mDisposed;

		public string Key { get; private set; }

		public T Resource { get; private set; }

		public BIJImage Image
		{
			get
			{
				return (Resource == null || typeof(T) != typeof(ResImage)) ? null : (Resource as ResImage).Image;
			}
		}

		public ResourceInfo(string key)
		{
			Key = key;
		}

		~ResourceInfo()
		{
			Dispose(false);
		}

		public void Load(bool not_unload = false)
		{
			if (Resource != null)
			{
				UnLoad();
			}
			if (ResourceManager.IsLoaded(Key))
			{
				if (typeof(T) == typeof(ResImage) && Res.ResImageDict.ContainsKey(Key))
				{
					Resource = ResourceManager.LoadImage(Key) as T;
				}
				if (Resource != null)
				{
					return;
				}
			}
			if (typeof(T) == typeof(ResImage))
			{
				Resource = ResourceManager.LoadImageAsync(Key) as T;
			}
			else if (typeof(T) == typeof(ResSound))
			{
				Resource = ResourceManager.LoadSoundAsync(Key) as T;
			}
			if (Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.INIT)
			{
				mInit = true;
				mForceNotUnload = not_unload;
			}
		}

		public void UnLoad()
		{
			if (mInit)
			{
				Resource = (T)null;
				if (typeof(T) == typeof(ResImage))
				{
					ResourceManager.UnloadImage(Key);
				}
				else if (typeof(T) == typeof(ResSound))
				{
					ResourceManager.UnloadSound(Key);
				}
			}
			mInit = false;
		}

		public bool IsDone()
		{
			return Resource != null && Resource.LoadState == ResourceInstance.LOADSTATE.DONE;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!mDisposed)
			{
				if (disposing)
				{
				}
				UnLoad();
				mDisposed = true;
			}
		}
	}

	private sealed class CallBackEvent : UnityEvent<int, KeyValuePair<CALLBACK_BREAKDOWN, int>[]>
	{
	}

	private sealed class LoadAtlasComparer : IEqualityComparer<LOAD_ATLAS>
	{
		public bool Equals(LOAD_ATLAS x, LOAD_ATLAS y)
		{
			return x == y;
		}

		public int GetHashCode(LOAD_ATLAS arg)
		{
			return (int)arg;
		}
	}

	private sealed class LoadSoundComparer : IEqualityComparer<LOAD_SOUND>
	{
		public bool Equals(LOAD_SOUND x, LOAD_SOUND y)
		{
			return x == y;
		}

		public int GetHashCode(LOAD_SOUND arg)
		{
			return (int)arg;
		}
	}

	private sealed class BreakDownComparer : IEqualityComparer<CALLBACK_BREAKDOWN>
	{
		public bool Equals(CALLBACK_BREAKDOWN x, CALLBACK_BREAKDOWN y)
		{
			return x == y;
		}

		public int GetHashCode(CALLBACK_BREAKDOWN arg)
		{
			return (int)arg;
		}
	}

	private const float TANGENT_UP = 1.5708f;

	private GameObject mGameObject;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	private Dictionary<LOAD_ATLAS, ResourceInfo<ResImage>> mAtlas = new Dictionary<LOAD_ATLAS, ResourceInfo<ResImage>>(new LoadAtlasComparer());

	private Dictionary<LOAD_SOUND, ResourceInfo<ResSound>> mSound = new Dictionary<LOAD_SOUND, ResourceInfo<ResSound>>(new LoadSoundComparer());

	private PlayerEventData mPlayerEventData;

	private SMEventPageData mEventPageData;

	private GameObject mJewelUpChanceRoot;

	private Transform mJewelFrame;

	private UILabel mLabelPossessJewel;

	private UISprite[] mSpriteFirstClear;

	private UISprite mSpriteFirstClearHeader;

	private int mAcquireJewel;

	private int mTotalAcquireJewel;

	private int mPrevTotalJewel;

	private int mFixedAdditionalJewel = -1;

	private NumberImageString mImageAcquireJewel;

	private DISPLAY_TYPE mDisplayType;

	private bool mHasJewelUpChance;

	private int mContinueCount;

	private bool mIsFirstClear;

	private PartsChangedSprite mBoxAnime;

	private Transform mBoxAnimeSyncDummy;

	private GameObject mBoxAnimeDemoCamera;

	private Dictionary<SELECT_ITEM, CallBackEvent> mCallback;

	private GaugeSprite mGauge;

	private GameObject mInputArea;

	private bool mIsSkip;

	private GameState mGameState;

	private DOUBLE_UP_CHANCE_STATE mDoubleUpChanceState;

	private GameObject mAcquireCopy;

	private Dictionary<CALLBACK_BREAKDOWN, int> mBreakDownData = new Dictionary<CALLBACK_BREAKDOWN, int>(new BreakDownComparer());

	private UIButton mButtonOK;

	public GameObject GameObject
	{
		get
		{
			if (!mGameObject)
			{
				mGameObject = base.gameObject;
			}
			return mGameObject;
		}
	}

	protected bool IsBusy
	{
		get
		{
			return GetBusy() || mState.GetStatus() != STATE.MAIN;
		}
	}

	public bool IsDoubleUpChance { get; private set; }

	public override void Awake()
	{
		base.Awake();
		foreach (int value in Enum.GetValues(typeof(CALLBACK_BREAKDOWN)))
		{
			mBreakDownData.Add((CALLBACK_BREAKDOWN)value, 0);
		}
	}

	public override void Update()
	{
		base.Update();
		if (mState.IsChanged())
		{
			switch (mState.GetStatus())
			{
			case STATE.SHOW_BOX:
				StartCoroutine(AnimationBoxAsync());
				break;
			case STATE.COUNT_UP_JEWEL_1:
				switch (mDisplayType)
				{
				case DISPLAY_TYPE.COURSE_CLEAR:
					mInputArea.SetActive(false);
					mState.Change(STATE.COUNT_UP_JEWEL_3_1);
					break;
				case DISPLAY_TYPE.STAGE_ALL_CLEAR:
					mInputArea.SetActive(false);
					mState.Change(STATE.COUNT_UP_JEWEL_3_2);
					break;
				case DISPLAY_TYPE.RESULT:
					if (mAcquireJewel == 0 && !mIsFirstClear)
					{
						mState.Change(STATE.SHOW_CLOSE_BUTTON);
						break;
					}
					if (mAcquireJewel == 0)
					{
						mState.Change(STATE.OPEN_FIRST_CLEAR_BOX);
						break;
					}
					mIsSkip = false;
					mInputArea.SetActive(true);
					StartCoroutine(AnimationJewelCountUpAsync(GetMultipliedCourseBonus(), 1f, false, delegate
					{
						short a_course2;
						int a_main2;
						int a_sub2;
						Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course2, out a_main2, out a_sub2);
						SMEventLabyrinthStageSetting labyrinthStageSetting2 = mEventPageData.GetLabyrinthStageSetting(a_main2, a_sub2, a_course2);
						mInputArea.SetActive(false);
						if (mPlayerEventData != null && mEventPageData != null && mEventPageData.EventCourseList != null && mEventPageData.EventCourseList.ContainsKey(mPlayerEventData.CourseID) && mAcquireJewel >= mEventPageData.EventCourseList[mPlayerEventData.CourseID].JewelUpChanceCondition && (mHasJewelUpChance || mContinueCount >= 1 || IsNextRewardCharacter()))
						{
							mIsSkip = false;
							mState.Change(STATE.POP_DOUBLE_UP_CHANCE_DIALOG);
						}
						else if (mIsFirstClear)
						{
							mState.Change(STATE.OPEN_FIRST_CLEAR_BOX);
						}
						else
						{
							mState.Change(STATE.COUNT_UP_JEWEL_3_0);
						}
					}));
					break;
				}
				break;
			case STATE.POP_DOUBLE_UP_CHANCE_DIALOG:
			{
				if (mGameState != null && mBoxAnimeDemoCamera != null)
				{
					mGameState.SetParentGrayScreen(mBoxAnimeDemoCamera.transform, 10, true);
				}
				JewelUpChanceDialog dialog = Util.CreateGameObject("DoubleUpChanceDialog", (!mBoxAnimeDemoCamera) ? GameObject.transform.parent.gameObject : mBoxAnimeDemoCamera).AddComponent<JewelUpChanceDialog>();
				dialog.SetClosedEvent(delegate(JewelUpChanceDialog.SELECT_ITEM i)
				{
					mGameState.RestoreGrayScreenParent();
					if (i == JewelUpChanceDialog.SELECT_ITEM.CONSUME)
					{
						mDoubleUpChanceState = DOUBLE_UP_CHANCE_STATE.Consume;
						IsDoubleUpChance = true;
						mState.Change(STATE.SHOW_DOUBLE_UP_CHANCE);
					}
					else
					{
						mDoubleUpChanceState = DOUBLE_UP_CHANCE_STATE.Cancel;
						IsDoubleUpChance = false;
						if (mIsFirstClear)
						{
							mState.Change(STATE.OPEN_FIRST_CLEAR_BOX);
						}
						else
						{
							mState.Change(STATE.COUNT_UP_JEWEL_3_0);
						}
					}
					UnityEngine.Object.Destroy(dialog.gameObject);
					dialog = null;
				});
				dialog.Init(GetMultipliedCourseBonus());
				break;
			}
			case STATE.SHOW_DOUBLE_UP_CHANCE:
				mIsSkip = false;
				mInputArea.SetActive(true);
				StartCoroutine(AnimationJewelUpChanceAsync());
				break;
			case STATE.COUNT_UP_JEWEL_2:
				StartCoroutine(AnimationJewelCountUpAsync(0, 2f, false, delegate
				{
					if (mIsFirstClear)
					{
						mState.Change(STATE.OPEN_FIRST_CLEAR_BOX);
					}
					else
					{
						mState.Change(STATE.COUNT_UP_JEWEL_3_0);
					}
				}));
				break;
			case STATE.OPEN_FIRST_CLEAR_BOX:
				StartCoroutine(AnimationFirstClearBoxAsync(mIsSkip));
				break;
			case STATE.COUNT_UP_JEWEL_3_0:
			{
				short a_course;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
				SMEventLabyrinthStageSetting labyrinthStageSetting = mEventPageData.GetLabyrinthStageSetting(a_main, a_sub, a_course);
				if (labyrinthStageSetting != null && mIsFirstClear)
				{
					if (!mIsSkip)
					{
						mInputArea.SetActive(true);
					}
					StartCoroutine(AnimationJewelCountUpAsync(labyrinthStageSetting.ItemAmount, 1f, mIsSkip, delegate
					{
						mState.Change(STATE.SHOW_CLOSE_BUTTON);
					}));
				}
				else
				{
					mState.Change(STATE.SHOW_CLOSE_BUTTON);
				}
				break;
			}
			case STATE.COUNT_UP_JEWEL_3_1:
			case STATE.COUNT_UP_JEWEL_3_2:
				if (mFixedAdditionalJewel == -1)
				{
					mState.Change(STATE.MAIN);
					break;
				}
				StartCoroutine(AnimationJewelCountUpAsync(mFixedAdditionalJewel, 1f, false, delegate
				{
					mState.Change(STATE.SHOW_OK_BUTTON);
				}));
				break;
			case STATE.SHOW_CLOSE_BUTTON:
				mInputArea.SetActive(false);
				if (mButtonOK != null)
				{
					NGUITools.SetActive(mButtonOK.gameObject, true);
				}
				mState.Change(STATE.MAIN);
				break;
			case STATE.SHOW_OK_BUTTON:
			{
				mInputArea.SetActive(false);
				string text = "LC_button_ok2";
				UIButton button = Util.CreateJellyImageButton("OK Button", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
				Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 1, new Vector3(0f, -230f), Vector3.one);
				Util.SetImageButtonMessage(button, this, "OnOkButton_Click", UIButtonMessage.Trigger.OnClick);
				mState.Change(STATE.MAIN);
				break;
			}
			}
		}
		mState.Update();
	}

	private void LateUpdate()
	{
		if ((bool)mBoxAnimeSyncDummy && (bool)mBoxAnime)
		{
			mBoxAnime.SetPosition(mBoxAnimeSyncDummy.position.x, mBoxAnimeSyncDummy.position.y);
			if (mBoxAnime.gameObject.activeSelf != mBoxAnimeSyncDummy.gameObject.activeInHierarchy)
			{
				mBoxAnime.gameObject.SetActive(mBoxAnimeSyncDummy.gameObject.activeInHierarchy);
			}
		}
	}

	public void Init(int acquire_jewel, int prev_total_jewel, int continue_count, bool has_jewel_up_chance, GameState game_state, DISPLAY_TYPE type)
	{
		Init(-1, prev_total_jewel, game_state, type);
		mAcquireJewel = acquire_jewel;
		mContinueCount = continue_count;
		mHasJewelUpChance = has_jewel_up_chance;
	}

	public void Init(int fixed_acquire_jewel, int prev_total_jewel, GameState game_state, DISPLAY_TYPE type)
	{
		mPrevTotalJewel = prev_total_jewel;
		mDisplayType = type;
		mFixedAdditionalJewel = fixed_acquire_jewel;
		mGameState = game_state;
	}

	public override void BuildDialog()
	{
		UISprite uISprite = null;
		UILabel uILabel = null;
		UIFont atlasFont = GameMain.LoadFont();
		UIGrid uIGrid = null;
		NumberImageString numberImageString = null;
		GameObject gameObject = null;
		Vector3 vector = ((Util.ScreenOrientation != ScreenOrientation.LandscapeLeft && Util.ScreenOrientation != ScreenOrientation.LandscapeRight) ? mGemFramePositionOffsetP : mGemFramePositionOffsetL);
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out mPlayerEventData);
		mEventPageData = GameMain.GetEventPageData(mGame.mPlayer.Data.CurrentEventID);
		int num = ((mPlayerEventData.CourseID < 0) ? 10 : mEventPageData.EventCourseList[mPlayerEventData.CourseID].CourseBonusRate);
		int multipliedCourseBonus = GetMultipliedCourseBonus(2f);
		SMEventLabyrinthStageSetting sMEventLabyrinthStageSetting = null;
		if (mDisplayType == DISPLAY_TYPE.RESULT)
		{
			short a_course;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
			sMEventLabyrinthStageSetting = mEventPageData.GetLabyrinthStageSetting(a_main, a_sub, a_course);
			int stageNo = Def.GetStageNo(a_main, a_sub);
			mIsFirstClear = mPlayerEventData.LabyrinthData.ClearStageProcess(a_course, stageNo, true, false);
			multipliedCourseBonus = GetMultipliedCourseBonus(2f, sMEventLabyrinthStageSetting.ItemAmount);
		}
		else if (mDisplayType == DISPLAY_TYPE.COURSE_CLEAR || mDisplayType == DISPLAY_TYPE.STAGE_ALL_CLEAR)
		{
			multipliedCourseBonus = mFixedAdditionalJewel;
		}
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(string.Empty);
		float num2 = 0f;
		gameObject = mTitleBoard.gameObject;
		switch (mDisplayType)
		{
		case DISPLAY_TYPE.RESULT:
		case DISPLAY_TYPE.COURSE_CLEAR:
			num2 = 145f;
			break;
		case DISPLAY_TYPE.STAGE_ALL_CLEAR:
			num2 = 185f;
			break;
		}
		uISprite = Util.CreateSprite("OrnamentL", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
		Util.SetSpriteInfo(uISprite, "LC_result_jewel00", mBaseDepth + 1, new Vector3(0f - num2, 0f), Vector3.one, false);
		uISprite = Util.CreateSprite("OrnamentR", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
		Util.SetSpriteInfo(uISprite, "LC_result_jewel01", mBaseDepth + 1, new Vector3(num2, 0f), Vector3.one, false);
		uISprite = Util.CreateSprite("TitleSprite", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
		UISprite sprite = uISprite;
		StringBuilder stringBuilder = new StringBuilder("LC_result_title");
		int num3 = (int)mDisplayType;
		Util.SetSpriteInfo(sprite, stringBuilder.Append(num3.ToString("00")).ToString(), mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		uISprite.SetLocalPositionY(-1f);
		GaugeSprite.Config config = new GaugeSprite.Config();
		uISprite = Util.CreateSprite("GaugeFrame", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
		Util.SetSpriteInfo(uISprite, "event_instruction_panel01", mBaseDepth + 1, new Vector3(0f, -79f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(330, 63);
		gameObject = uISprite.gameObject;
		config.Frame = uISprite;
		uISprite = Util.CreateSprite("Body", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
		Util.SetSpriteInfo(uISprite, "event_gauge02", mBaseDepth + 2, new Vector3(-160f, 2f), Vector3.one, false, UIWidget.Pivot.Left);
		uISprite.type = UIBasicSprite.Type.Filled;
		uISprite.fillDirection = UIBasicSprite.FillDirection.Horizontal;
		uISprite.SetDimensions(262, 33);
		config.Body = uISprite;
		config.Icon = Util.CreateGameObject("Item", gameObject);
		config.Icon.transform.SetLocalPosition(133f, 1f);
		config.IconSize = new IntVector2(55, 55);
		config.IconDepth = mBaseDepth + 3;
		uILabel = Util.CreateLabel("Label", gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, "0", mBaseDepth + 4, new Vector3(-143f, -17f), 21, 200, 0, UIWidget.Pivot.Left);
		Util.SetLabelColor(uILabel, Color.white);
		Util.SetLabeShrinkContent(uILabel, 200, 21);
		uILabel.effectStyle = UILabel.Effect.Outline8;
		uILabel.effectDistance = new Vector2(2f, 2f);
		uILabel.effectColor = Def.DEFAULT_MESSAGE_COLOR;
		config.Label = uILabel;
		uISprite = Util.CreateSprite("Complete", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
		Util.SetSpriteInfo(uISprite, "oldevent_Complete", mBaseDepth + 5, new Vector3(-22f, 5f), Vector3.one, false);
		config.Complete = uISprite;
		config.MaxHorizontalDimension = 262;
		config.MinHorizontalDimension = 22;
		config.LocalizeKey = "Labyrinth_CharacterIncentive_02";
		config.IsShowNonNextReward = true;
		config.AchieveSeKey = LOAD_SOUND.SE_MAP_GET_RBKEY.ToString();
		config.CompleteIconAtlas = mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image;
		config.CompleteIconKey = "event_instruction_ALLGET";
		mGauge = new GaugeSprite(config, mEventPageData.EventRewardList.ToArray());
		mGauge.SetValue(mPrevTotalJewel);
		switch (mDisplayType)
		{
		case DISPLAY_TYPE.COURSE_CLEAR:
			uISprite = Util.CreateSprite("ItemFrame", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetSpriteInfo(uISprite, "instruction_panel12", mBaseDepth + 1, new Vector3(0f, 107f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(505, 105);
			mBoxAnime = CreateLoopSsSprite(GameStateEventLabyrinth.RewardItemGetAnimationFormat, true, uISprite.gameObject, mBaseDepth + 2, out mBoxAnimeDemoCamera, new CHANGE_PART_INFO
			{
				partName = "change_B_TBox_getA",
				spriteName = "null"
			}, new CHANGE_PART_INFO
			{
				partName = "change_B_TBox_getB",
				spriteName = "null"
			}, new CHANGE_PART_INFO
			{
				partName = "change_B_jewel_mix",
				spriteName = "null"
			}, new CHANGE_PART_INFO
			{
				partName = "change_B_jewel_add",
				spriteName = "null"
			}, new CHANGE_PART_INFO
			{
				partName = "change_A_jewel_mix",
				spriteName = "dummy1"
			}, new CHANGE_PART_INFO
			{
				partName = "change_A_jewel_add",
				spriteName = "dummy1"
			});
			mBoxAnime._sprite.AnimationFinished = delegate
			{
				mBoxAnime._sprite.AnimFrame = 50f;
				mBoxAnime._sprite.Play();
			};
			mBoxAnime.SetLocalScale(0.65f, 0.65f, 1f);
			mBoxAnime.gameObject.SetActive(false);
			mBoxAnimeSyncDummy = Util.CreateGameObject("SyncDummy", uISprite.gameObject).transform;
			mBoxAnimeSyncDummy.gameObject.SetActive(false);
			break;
		case DISPLAY_TYPE.STAGE_ALL_CLEAR:
			uISprite = Util.CreateSprite("ItemFrame", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetSpriteInfo(uISprite, "instruction_panel12", mBaseDepth + 1, new Vector3(0f, 107f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(505, 105);
			mBoxAnime = CreateLoopSsSprite(GameStateEventLabyrinth.RewardItemGetAnimationFormat, true, uISprite.gameObject, mBaseDepth + 2, out mBoxAnimeDemoCamera, new CHANGE_PART_INFO
			{
				partName = "change_B_TBox_getA",
				spriteName = "adv_TBox2_getA"
			}, new CHANGE_PART_INFO
			{
				partName = "change_B_TBox_getB",
				spriteName = "adv_TBox2_getB"
			}, new CHANGE_PART_INFO
			{
				partName = "change_B_jewel_mix",
				spriteName = "icon_jewel"
			}, new CHANGE_PART_INFO
			{
				partName = "change_B_jewel_add",
				spriteName = "icon_jewel"
			}, new CHANGE_PART_INFO
			{
				partName = "change_A_jewel_mix",
				spriteName = "null"
			}, new CHANGE_PART_INFO
			{
				partName = "change_A_jewel_add",
				spriteName = "null"
			});
			mBoxAnime._sprite.AnimationFinished = delegate
			{
				mBoxAnime._sprite.AnimFrame = 50f;
				mBoxAnime._sprite.Play();
			};
			mBoxAnime.SetLocalScale(0.65f, 0.65f, 1f);
			mBoxAnime.gameObject.SetActive(false);
			mBoxAnimeSyncDummy = Util.CreateGameObject("SyncDummy", uISprite.gameObject).transform;
			mBoxAnimeSyncDummy.gameObject.SetActive(false);
			mBoxAnimeSyncDummy.SetLocalPosition(0f, 5f);
			break;
		}
		int h = 378;
		float y = -12f;
		DISPLAY_TYPE dISPLAY_TYPE = mDisplayType;
		if (dISPLAY_TYPE == DISPLAY_TYPE.COURSE_CLEAR || dISPLAY_TYPE == DISPLAY_TYPE.STAGE_ALL_CLEAR)
		{
			y = -72f;
			h = 228;
		}
		uISprite = Util.CreateSprite("InnerFrame", GameObject, mAtlas[LOAD_ATLAS.DIALOG_BASE].Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, y), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(510, h);
		GameObject parent = uISprite.gameObject;
		switch (mDisplayType)
		{
		case DISPLAY_TYPE.RESULT:
		{
			uISprite = Util.CreateSprite("Silhouette", parent, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "result_silhouette", mBaseDepth + 2, new Vector3(111f, -10f), Vector3.one, false);
			uISprite.SetDimensions(272, 344);
			gameObject = Util.CreateGameObject("Jewel", parent);
			gameObject.transform.SetLocalPosition(0f, 140f);
			gameObject.transform.SetLocalScale(0.9f, 0.9f, 1f);
			uILabel = Util.CreateLabel("Label", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Labyrinth_Result_00"), mBaseDepth + 3, new Vector3(-245f, 0f), 31, 300, 0, UIWidget.Pivot.Left);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			Util.SetLabeShrinkContent(uILabel, 300, 31);
			int keta = ((mAcquireJewel == 0) ? 1 : ((int)Mathf.Log10(mAcquireJewel) + 1));
			numberImageString = Util.CreateGameObject("Num", gameObject).AddComponent<NumberImageString>();
			numberImageString.Init(keta, mAcquireJewel, mBaseDepth + 3, 1f, UIWidget.Pivot.Right, false, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image, false, new string[10] { "result_num0", "result_num1", "result_num2", "result_num3", "result_num4", "result_num5", "result_num6", "result_num7", "result_num8", "result_num9" });
			numberImageString.SetPitch(31f);
			numberImageString.SetLocalPosition(235f, 4f);
			if (mPlayerEventData != null)
			{
				gameObject = Util.CreateGameObject("CourseBonus", parent);
				gameObject.transform.SetLocalPosition(0f, 100f);
				gameObject.transform.SetLocalScale(0.9f, 0.9f, 1f);
				uILabel = Util.CreateLabel("Label", gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, Localization.Get("Labyrinth_Result_01"), mBaseDepth + 3, new Vector3(-245f, 0f), 31, 300, 0, UIWidget.Pivot.Left);
				Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
				Util.SetLabeShrinkContent(uILabel, 300, 31);
				uIGrid = Util.CreateGameObject("Num", gameObject).AddComponent<UIGrid>();
				uIGrid.cellWidth = 21f;
				uIGrid.pivot = UIWidget.Pivot.Right;
				uIGrid.SetLocalPosition(220f, 3f);
				gameObject = uIGrid.gameObject;
				int num7 = num;
				uISprite = Util.CreateSprite("Decimal", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
				Util.SetSpriteInfo(uISprite, new StringBuilder("result_num").Append(num7 % 10).ToString(), mBaseDepth + 6, Vector3.zero, Vector3.one, false);
				uISprite.transform.SetAsFirstSibling();
				uISprite = Util.CreateSprite("Dot", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
				Util.SetSpriteInfo(uISprite, "result_num_period", mBaseDepth + 5, Vector3.zero, Vector3.one, false);
				uISprite.transform.SetAsFirstSibling();
				num7 /= 10;
				uISprite = Util.CreateSprite("Integer", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
				Util.SetSpriteInfo(uISprite, new StringBuilder("result_num").Append(num7).ToString(), mBaseDepth + 4, Vector3.zero, Vector3.one, false);
				uISprite.transform.SetAsFirstSibling();
				Util.CreateGameObject("Dummy", gameObject).transform.SetAsFirstSibling();
				uISprite = Util.CreateSprite("Cross", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
				Util.SetSpriteInfo(uISprite, "result_num_times", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
				uISprite.transform.SetAsFirstSibling();
			}
			mJewelUpChanceRoot = Util.CreateGameObject("JewelUpChance", parent);
			mJewelUpChanceRoot.transform.SetLocalPosition(0f, 27f);
			mJewelUpChanceRoot.transform.SetLocalScale(0.9f, 0.9f, 1f);
			NGUITools.SetActive(mJewelUpChanceRoot, false);
			uISprite = Util.CreateSprite("Image", mJewelUpChanceRoot, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "LC_result_chance", mBaseDepth + 3, new Vector3(-128f, 0f), Vector3.one, false);
			uIGrid = Util.CreateGameObject("Num", mJewelUpChanceRoot).AddComponent<UIGrid>();
			uIGrid.cellWidth = 25f;
			uIGrid.pivot = UIWidget.Pivot.Right;
			uIGrid.SetLocalPosition(220f, -2f);
			gameObject = uIGrid.gameObject;
			uISprite = Util.CreateSprite("Cross", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "result_jewelup_num_times", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
			Util.CreateGameObject("Dummy", gameObject);
			uISprite = Util.CreateSprite("Integer", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "result_jewelup_num2", mBaseDepth + 4, Vector3.zero, Vector3.one, false);
			uISprite = Util.CreateSprite("Dot", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "result_jewelup_num_period", mBaseDepth + 5, Vector3.zero, Vector3.one, false);
			uISprite = Util.CreateSprite("Decimal", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "result_jewelup_num0", mBaseDepth + 6, Vector3.zero, Vector3.one, false);
			gameObject = Util.CreateGameObject("FirstClearReward", parent);
			gameObject.transform.SetLocalPosition(0f, -53f);
			gameObject.transform.SetLocalScale(0.9f, 0.9f, 1f);
			uILabel = Util.CreateLabel("Text", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Labyrinth_Get_Treasure_Title"), mBaseDepth + 3, new Vector3(-188f, 0f), 31, 260, 0, UIWidget.Pivot.Left);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			Util.SetLabeShrinkContent(uILabel, 260, 31);
			uILabel.spacingX = -1;
			if (mIsFirstClear)
			{
				if (sMEventLabyrinthStageSetting != null)
				{
					short boxKind = sMEventLabyrinthStageSetting.BoxKind;
					mBoxAnime = CreateLoopSsSprite(GameStateEventLabyrinth.RewardItemGetAnimationFormat, true, gameObject, mBaseDepth + 5, out mBoxAnimeDemoCamera, new CHANGE_PART_INFO
					{
						partName = "change_B_TBox_getA",
						spriteName = string.Format("adv_TBox{0}_getA", boxKind)
					}, new CHANGE_PART_INFO
					{
						partName = "change_B_TBox_getB",
						spriteName = string.Format("adv_TBox{0}_getB", boxKind)
					}, new CHANGE_PART_INFO
					{
						partName = "change_B_jewel_mix",
						spriteName = "icon_jewel"
					}, new CHANGE_PART_INFO
					{
						partName = "change_B_jewel_add",
						spriteName = "icon_jewel"
					}, new CHANGE_PART_INFO
					{
						partName = "change_A_jewel_mix",
						spriteName = "null"
					}, new CHANGE_PART_INFO
					{
						partName = "change_A_jewel_add",
						spriteName = "null"
					});
					mBoxAnime._sprite.AnimFrame = 5f;
					mBoxAnime.SetLocalScale(0.45f, 0.45f, 1f);
					mBoxAnime.SetLocalPosition(-205f, -110f);
					mBoxAnimeSyncDummy = Util.CreateGameObject("SyncDummy", gameObject).transform;
					mBoxAnimeSyncDummy.SetLocalPosition(-225f, 10f);
				}
				uIGrid = Util.CreateGameObject("Num", gameObject).AddComponent<UIGrid>();
				uIGrid.cellWidth = 31f;
				uIGrid.pivot = UIWidget.Pivot.Right;
				uIGrid.SetLocalPosition(220f, -3f);
				gameObject = uIGrid.gameObject;
				if (sMEventLabyrinthStageSetting != null)
				{
					int num8 = ((sMEventLabyrinthStageSetting.ItemAmount == 0) ? 1 : ((int)Mathf.Log10(sMEventLabyrinthStageSetting.ItemAmount) + 1));
					mSpriteFirstClear = new UISprite[num8];
					for (int j = 0; j < num8; j++)
					{
						mSpriteFirstClear[j] = Util.CreateSprite("Num" + j, gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
						Util.SetSpriteInfo(mSpriteFirstClear[j], "null", mBaseDepth + 6, Vector3.zero, Vector3.one, false);
						mSpriteFirstClear[j].transform.SetAsFirstSibling();
					}
					mSpriteFirstClearHeader = Util.CreateSprite("Cross", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
					Util.SetSpriteInfo(mSpriteFirstClearHeader, "null", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
					mSpriteFirstClearHeader.SetLocalEulerAnglesZ(45f);
					mSpriteFirstClearHeader.transform.SetAsFirstSibling();
				}
			}
			else
			{
				if (sMEventLabyrinthStageSetting != null)
				{
					short boxKind2 = sMEventLabyrinthStageSetting.BoxKind;
					uISprite = Util.CreateSprite("Box", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_REWARD].Image);
					Util.SetSpriteInfo(uISprite, string.Format("adv_TBox{0}_getB", boxKind2), mBaseDepth + 5, new Vector3(-225f, 8f), Vector3.one, false);
				}
				uISprite = Util.CreateSprite("Already", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
				Util.SetSpriteInfo(uISprite, "LC_result_clear00", mBaseDepth + 3, new Vector3(180f, -3f), Vector3.one, false);
			}
			uISprite = Util.CreateSprite("Separator", GameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "result_line00", mBaseDepth + 3, new Vector3(0f, -100f), Vector3.one, false);
			gameObject = Util.CreateGameObject("TotalAcquire", parent);
			gameObject.transform.SetLocalPosition(0f, -140f);
			uILabel = Util.CreateLabel("Label", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Labyrinth_Result_02"), mBaseDepth + 3, new Vector3(-225f, 0f), 35, 300, 0, UIWidget.Pivot.Left);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			Util.SetLabeShrinkContent(uILabel, 215, 35);
			float num9 = 41f;
			int num10 = ((multipliedCourseBonus == 0) ? 1 : ((int)Mathf.Log10(multipliedCourseBonus) + 1));
			mImageAcquireJewel = Util.CreateGameObject("Num", gameObject).AddComponent<NumberImageString>();
			mImageAcquireJewel.Init(num10, 0, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, false, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image, false, new string[10] { "result_total_num0", "result_total_num1", "result_total_num2", "result_total_num3", "result_total_num4", "result_total_num5", "result_total_num6", "result_total_num7", "result_total_num8", "result_total_num9" });
			mImageAcquireJewel.SetPitch(num9);
			mImageAcquireJewel.SetLocalPosition(192f - num9 * 0.5f * (float)(num10 - 1), 8f);
			string text = "LC_button_ok2";
			mButtonOK = Util.CreateJellyImageButton("Button", GameObject, mAtlas[LOAD_ATLAS.HUD].Image);
			Util.SetImageButtonInfo(mButtonOK, text, text, text, mBaseDepth + 2, new Vector3(0f, -240f), Vector3.one);
			Util.AddImageButtonMessage(mButtonOK, this, "OnOkButton_Click", UIButtonMessage.Trigger.OnClick);
			NGUITools.SetActive(mButtonOK.gameObject, false);
			break;
		}
		case DISPLAY_TYPE.COURSE_CLEAR:
		case DISPLAY_TYPE.STAGE_ALL_CLEAR:
		{
			string key = null;
			switch (mDisplayType)
			{
			case DISPLAY_TYPE.COURSE_CLEAR:
				key = "Labyrinth_Course_Clear_Title";
				break;
			case DISPLAY_TYPE.STAGE_ALL_CLEAR:
				key = "Labyrinth_AllCrear_Desc";
				break;
			}
			gameObject = Util.CreateGameObject("ClearReward", parent);
			gameObject.transform.SetLocalPosition(0f, 58f);
			uILabel = Util.CreateLabel("Text", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Format(Localization.Get(key), mPlayerEventData.CourseID + 1), mBaseDepth + 3, new Vector3(-225f, 0f), 31, 260, 0, UIWidget.Pivot.Left);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			Util.SetLabeShrinkContent(uILabel, 300, 31);
			uILabel.spacingX = -1;
			uIGrid = Util.CreateGameObject("Num", gameObject).AddComponent<UIGrid>();
			uIGrid.cellWidth = 31f;
			uIGrid.pivot = UIWidget.Pivot.Right;
			uIGrid.SetLocalPosition(210f, -3f);
			gameObject = uIGrid.gameObject;
			if (mFixedAdditionalJewel != -1)
			{
				int num4 = ((mFixedAdditionalJewel == 0) ? 1 : ((int)Mathf.Log10(mFixedAdditionalJewel) + 1));
				mSpriteFirstClear = new UISprite[num4];
				for (int i = 0; i < num4; i++)
				{
					mSpriteFirstClear[i] = Util.CreateSprite("Num" + i, gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
					Util.SetSpriteInfo(mSpriteFirstClear[i], "null", mBaseDepth + 6, Vector3.zero, Vector3.one, false);
					mSpriteFirstClear[i].transform.SetAsFirstSibling();
				}
				mSpriteFirstClearHeader = Util.CreateSprite("Cross", gameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
				Util.SetSpriteInfo(mSpriteFirstClearHeader, "null", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
				mSpriteFirstClearHeader.SetLocalEulerAnglesZ(45f);
				mSpriteFirstClearHeader.transform.SetAsFirstSibling();
			}
			uISprite = Util.CreateSprite("Separator", GameObject, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image);
			Util.SetSpriteInfo(uISprite, "result_line00", mBaseDepth + 3, new Vector3(0f, -78f), Vector3.one, false);
			gameObject = Util.CreateGameObject("TotalAcquire", parent);
			gameObject.transform.SetLocalPosition(0f, -59f);
			uILabel = Util.CreateLabel("Label", gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("Labyrinth_Result_02"), mBaseDepth + 3, new Vector3(-225f, 0f), 35, 300, 0, UIWidget.Pivot.Left);
			Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
			Util.SetLabeShrinkContent(uILabel, 215, 35);
			float num5 = 41f;
			int num6 = ((multipliedCourseBonus == 0) ? 1 : ((int)Mathf.Log10(multipliedCourseBonus) + 1));
			mImageAcquireJewel = Util.CreateGameObject("Num", gameObject).AddComponent<NumberImageString>();
			mImageAcquireJewel.Init(num6, 0, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, false, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image, false, new string[10] { "result_total_num0", "result_total_num1", "result_total_num2", "result_total_num3", "result_total_num4", "result_total_num5", "result_total_num6", "result_total_num7", "result_total_num8", "result_total_num9" });
			mImageAcquireJewel.SetPitch(num5);
			mImageAcquireJewel.SetLocalPosition(192f - num5 * 0.5f * (float)(num6 - 1), 8f);
			break;
		}
		}
		mInputArea = Util.CreateImageButton("SkipArea", GameObject, mAtlas[LOAD_ATLAS.HUD].Image).gameObject;
		UIButton component = mInputArea.GetComponent<UIButton>();
		Util.SetImageButtonMessage(component, this, "OnSkipArea_Clicked", UIButtonMessage.Trigger.OnClick);
		component.hover = (component.pressed = (component.disabledColor = Color.white));
		uISprite = mInputArea.GetComponent<UISprite>();
		Util.SetImageButtonInfo(component, "null", "null", "null", mBaseDepth + 100, Vector3.zero, Vector3.one);
		uISprite.SetDimensions(1136, 1136);
		uISprite.ResizeCollider();
		mInputArea.SetActive(false);
		SetLayout(Util.ScreenOrientation);
	}

	private void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if ((bool)mJewelFrame)
			{
				mJewelFrame.SetLocalPosition(194f + mGemFramePositionOffsetP.x, (0f - mDialogFrame.localSize.y) / 2f - 30f + mGemFramePositionOffsetP.y);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if ((bool)mJewelFrame)
			{
				mJewelFrame.SetLocalPosition(mDialogFrame.localSize.x / 2f + 95f + mGemFramePositionOffsetL.x, (0f - mDialogFrame.localSize.y) / 2f + 50f + mGemFramePositionOffsetL.y);
			}
			break;
		}
	}

	private void ChangeAnime(PartsChangedSprite sprite, params CHANGE_PART_INFO[] change_parts)
	{
		if (change_parts == null)
		{
			return;
		}
		Dictionary<int, CHANGE_PART_INFO> dictionary = new Dictionary<int, CHANGE_PART_INFO>();
		for (int i = 0; i < change_parts.Length; i++)
		{
			dictionary.Add(change_parts[i].partName.GetHashCode(), change_parts[i]);
		}
		SsPartRes[] partResList = sprite._sprite.PartResList;
		for (int j = 0; j < partResList.Length; j++)
		{
			CHANGE_PART_INFO value;
			if (partResList[j].IsRoot || !dictionary.TryGetValue(partResList[j].Name.GetHashCode(), out value))
			{
				continue;
			}
			Vector2 offset = mAtlas[LOAD_ATLAS.EVENTLABYRINTH_REWARD].Image.GetOffset(value.spriteName);
			Vector2 size = mAtlas[LOAD_ATLAS.EVENTLABYRINTH_REWARD].Image.GetSize(value.spriteName);
			float x = offset.x;
			float y = offset.y;
			float x2 = offset.x + size.x;
			float y2 = offset.y + size.y;
			partResList[j].UVs = new Vector2[4]
			{
				new Vector2(x, y2),
				new Vector2(x2, y2),
				new Vector2(x2, y),
				new Vector2(x, y)
			};
			Vector4 pixelPadding = mAtlas[LOAD_ATLAS.EVENTLABYRINTH_REWARD].Image.GetPixelPadding(value.spriteName);
			size = mAtlas[LOAD_ATLAS.EVENTLABYRINTH_REWARD].Image.GetPixelSize(value.spriteName);
			if (value.enableOffset)
			{
				offset = value.centerOffset;
			}
			else
			{
				offset = new Vector2(partResList[j].OriginX, partResList[j].OriginY);
				int num = partResList[j].PicArea.Right - partResList[j].PicArea.Left;
				int num2 = partResList[j].PicArea.Bottom - partResList[j].PicArea.Top;
				if (num == 1 && num2 == 1)
				{
					offset.x = size.x / 2f;
					offset.y = size.y / 2f;
				}
			}
			offset.x += pixelPadding.x;
			offset.y += pixelPadding.y;
			size.x -= pixelPadding.x + pixelPadding.z;
			size.y -= pixelPadding.y + pixelPadding.w;
			x = 0f - offset.x;
			y = 0f - (size.y - offset.y);
			x2 = size.x - offset.x;
			y2 = offset.y;
			float z = 0f;
			partResList[j].OrgVertices = new Vector3[4]
			{
				new Vector3(x, y2, z),
				new Vector3(x2, y2, z),
				new Vector3(x2, y, z),
				new Vector3(x, y, z)
			};
		}
		UnityEngine.Debug.LogError("Change Parts");
	}

	private PartsChangedSprite CreateLoopSsSprite(string key, bool is_start_pause, GameObject parent, int depth, out GameObject demo_camera, params CHANGE_PART_INFO[] change_parts)
	{
		demo_camera = UnityEngine.Object.Instantiate(GameMain.GetOriginalPrefabGOs(Res.PREFAB.DEMO_CAMERA)) as GameObject;
		demo_camera.transform.parent = mGame.mCamera.transform.parent;
		demo_camera.transform.SetLocalPosition(Vector3.zero);
		demo_camera.transform.SetLocalScale(Vector3.one);
		Camera component = demo_camera.GetComponent<Camera>();
		component.depth = 1f;
		PartsChangedSprite partsChangedSprite = Util.CreateGameObject("Animation", component.gameObject).AddComponent<PartsChangedSprite>();
		partsChangedSprite.Atlas = mAtlas[LOAD_ATLAS.EVENTLABYRINTH_REWARD].Image;
		partsChangedSprite.ChangeAnime("LABYRINTHREWARD_ITEMGET", change_parts, true, 1, null);
		partsChangedSprite.mPlayCount = 1;
		partsChangedSprite._sprite.DestroyAtEnd = false;
		partsChangedSprite._sprite.Play();
		if (is_start_pause)
		{
			partsChangedSprite._sprite.PlayAtStart = false;
			partsChangedSprite._sprite.Pause();
		}
		return partsChangedSprite;
	}

	private int GetMultipliedCourseBonus(float ratio = 1f, int additional = 0)
	{
		int num = ((mPlayerEventData.CourseID < 0) ? 10 : mEventPageData.EventCourseList[mPlayerEventData.CourseID].CourseBonusRate);
		return Util.CalcBonusToInt(mAcquireJewel, (float)num / 10f, ratio, additional);
	}

	private float GetTargetAchieveRatio(int target)
	{
		if (mEventPageData == null || mEventPageData.EventRewardList == null)
		{
			return 1f;
		}
		int num = mEventPageData.EventRewardList.Keys.FirstOrDefault((int n) => GetMultipliedCourseBonus() < n);
		int num2 = mEventPageData.EventRewardList.Keys.LastOrDefault((int n) => GetMultipliedCourseBonus() >= n);
		return (float)(target - num2) / (float)(num - num2);
	}

	private bool IsNextRewardCharacter()
	{
		if (mEventPageData == null || mEventPageData.EventRewardList == null)
		{
			return false;
		}
		int key = mEventPageData.EventRewardList.Keys.FirstOrDefault((int n) => mPrevTotalJewel < n);
		if (!mEventPageData.EventRewardList.ContainsKey(key))
		{
			return false;
		}
		SMEventRewardSetting sMEventRewardSetting = mEventPageData.EventRewardList[key];
		if (sMEventRewardSetting == null)
		{
			return false;
		}
		if (sMEventRewardSetting.IsGCItem != 0)
		{
			AdvRewardListData advRewardListData = SingletonMonoBehaviour<GameMain>.Instance.mAdvRewardListDict[sMEventRewardSetting.RewardID];
			if (advRewardListData != null && advRewardListData.Category == 24)
			{
				return true;
			}
		}
		else
		{
			AccessoryData accessoryData = SingletonMonoBehaviour<GameMain>.Instance.GetAccessoryData(sMEventRewardSetting.RewardID);
			if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
			{
				return true;
			}
		}
		return false;
	}

	public override IEnumerator BuildResource()
	{
		if (mAtlas == null)
		{
			mAtlas = new Dictionary<LOAD_ATLAS, ResourceInfo<ResImage>>();
		}
		foreach (int type2 in Enum.GetValues(typeof(LOAD_ATLAS)))
		{
			if (mAtlas.ContainsKey((LOAD_ATLAS)type2) && mAtlas[(LOAD_ATLAS)type2] != null)
			{
				mAtlas[(LOAD_ATLAS)type2].UnLoad();
				mAtlas.Remove((LOAD_ATLAS)type2);
			}
			ResourceInfo<ResImage> info4 = new ResourceInfo<ResImage>(((LOAD_ATLAS)type2).ToString());
			mAtlas.Add((LOAD_ATLAS)type2, info4);
			info4.Load();
		}
		if (mSound == null)
		{
			mSound = new Dictionary<LOAD_SOUND, ResourceInfo<ResSound>>();
		}
		foreach (int type in Enum.GetValues(typeof(LOAD_SOUND)))
		{
			if (mSound.ContainsKey((LOAD_SOUND)type) && mSound[(LOAD_SOUND)type] != null)
			{
				mSound[(LOAD_SOUND)type].UnLoad();
				mSound.Remove((LOAD_SOUND)type);
			}
			ResourceInfo<ResSound> info3 = new ResourceInfo<ResSound>(((LOAD_SOUND)type).ToString());
			mSound.Add((LOAD_SOUND)type, info3);
			info3.Load();
		}
		foreach (ResourceInfo<ResImage> info2 in mAtlas.Values)
		{
			while (!info2.IsDone())
			{
				yield return null;
			}
		}
		foreach (ResourceInfo<ResSound> info in mSound.Values)
		{
			while (!info.IsDone())
			{
				yield return null;
			}
		}
		mState.Change(STATE.WAIT);
	}

	private IEnumerator AnimationBoxAsync()
	{
		if ((mDisplayType == DISPLAY_TYPE.COURSE_CLEAR || mDisplayType == DISPLAY_TYPE.STAGE_ALL_CLEAR) && !mBoxAnime.gameObject.activeSelf)
		{
			mBoxAnime.gameObject.SetActive(true);
			mBoxAnimeSyncDummy.gameObject.SetActive(true);
			mBoxAnime._sprite.Play();
			switch (mDisplayType)
			{
			case DISPLAY_TYPE.STAGE_ALL_CLEAR:
				mGame.PlaySe(LOAD_SOUND.SE_GC_CAPSULEOPEN_NORMAL.ToString(), -1);
				break;
			case DISPLAY_TYPE.COURSE_CLEAR:
				mGame.PlaySe(LOAD_SOUND.SE_LABYRINTH_COURSE_CLEAR.ToString(), -1);
				break;
			}
			yield return new WaitForSeconds(1f);
		}
		mState.Change(STATE.COUNT_UP_JEWEL_1);
	}

	private IEnumerator AnimationJewelCountUpAsync(int add_value, float mul_value, bool is_immediate, UnityAction on_finish = null)
	{
		int flagCount = 3;
		if (mDisplayType == DISPLAY_TYPE.RESULT)
		{
			if (mState.GetStatus() == STATE.COUNT_UP_JEWEL_3_0)
			{
				flagCount++;
			}
		}
		else if (mDisplayType == DISPLAY_TYPE.COURSE_CLEAR || mDisplayType == DISPLAY_TYPE.STAGE_ALL_CLEAR)
		{
			flagCount++;
		}
		BitArray flags = new BitArray(flagCount);
		List<Coroutine> routine = new List<Coroutine>();
		float delay = 0f;
		if (mDisplayType == DISPLAY_TYPE.RESULT && mState.GetStatus() == STATE.COUNT_UP_JEWEL_2)
		{
			delay = 0.25f;
		}
		int add = Mathf.CeilToInt((float)(mTotalAcquireJewel + add_value) * mul_value) - mTotalAcquireJewel;
		int reserveGaugeValue = mGauge.Value + add;
		if (!is_immediate)
		{
			mGame.PlaySe(LOAD_SOUND.SE_DIALY_COMPLETE_STAMP.ToString(), -1);
			routine.Add(StartCoroutine(AnimationPossessJewelAsync(add, false, delegate
			{
				flags.Set(0, true);
			})));
			if (mGauge != null)
			{
				mGauge.Animation(add, 1f, delegate
				{
					flags.Set(1, true);
				});
			}
			else
			{
				flags.Set(1, true);
			}
			routine.Add(StartCoroutine(AnimationAcquireJewelAsync(add, false, delay, delegate
			{
				flags.Set(2, true);
			})));
			if (mDisplayType == DISPLAY_TYPE.RESULT)
			{
				if (mState.GetStatus() == STATE.COUNT_UP_JEWEL_3_0)
				{
					routine.Add(StartCoroutine(AnimationFirstClearCountAsync(add, false, delegate
					{
						flags.Set(3, true);
					})));
				}
			}
			else if (mDisplayType == DISPLAY_TYPE.COURSE_CLEAR || mDisplayType == DISPLAY_TYPE.STAGE_ALL_CLEAR)
			{
				routine.Add(StartCoroutine(AnimationFirstClearCountAsync(add, false, delegate
				{
					flags.Set(3, true);
				})));
			}
			InvokeCallback(SELECT_ITEM.OnUpdatePossess);
		}
		while (true)
		{
			int i = 0;
			while (true)
			{
				if (i < flags.Length)
				{
					if (flags[i])
					{
						i++;
						continue;
					}
					if (mDisplayType != 0 || (!mIsSkip && !is_immediate))
					{
						break;
					}
					foreach (Coroutine rtn in routine)
					{
						if (rtn != null)
						{
							StopCoroutine(rtn);
						}
					}
					mGauge.StopAnimation();
					routine.Add(StartCoroutine(AnimationPossessJewelAsync(add, true, delegate
					{
						flags.Set(0, true);
					})));
					if (mGauge != null)
					{
						mGauge.SetValue(reserveGaugeValue);
					}
					routine.Add(StartCoroutine(AnimationAcquireJewelAsync(add, true, delay, delegate
					{
						flags.Set(2, true);
					})));
					if (mDisplayType == DISPLAY_TYPE.RESULT)
					{
						if (mState.GetStatus() == STATE.COUNT_UP_JEWEL_3_0)
						{
							routine.Add(StartCoroutine(AnimationFirstClearCountAsync(add, true, delegate
							{
								flags.Set(3, true);
							})));
						}
					}
					else if (mDisplayType == DISPLAY_TYPE.COURSE_CLEAR || mDisplayType == DISPLAY_TYPE.STAGE_ALL_CLEAR)
					{
						routine.Add(StartCoroutine(AnimationFirstClearCountAsync(add, true, delegate
						{
							flags.Set(3, true);
						})));
					}
					InvokeCallback(SELECT_ITEM.OnUpdatePossess);
					if ((bool)mLabelPossessJewel)
					{
						mLabelPossessJewel.SetLocalScale(1f, 1f, 1f);
					}
					if ((bool)mAcquireCopy)
					{
						UnityEngine.Object.Destroy(mAcquireCopy);
					}
					mAcquireCopy = null;
				}
				mPrevTotalJewel += add;
				mTotalAcquireJewel += add;
				if (on_finish != null)
				{
					on_finish();
				}
				yield break;
			}
			yield return null;
		}
	}

	private IEnumerator AnimationFirstClearCountAsync(int add, bool is_skip, UnityAction on_finish = null)
	{
		if (mSpriteFirstClearHeader != null)
		{
			Util.SetSpriteImageName(mSpriteFirstClearHeader, "result_num_times", Vector3.one, false);
		}
		if (is_skip)
		{
			if (mSpriteFirstClear != null)
			{
				int value2 = add;
				int idx2 = 0;
				while (value2 > 0)
				{
					if (idx2 >= 0 && idx2 < mSpriteFirstClear.Length)
					{
						Util.SetSpriteImageName(mSpriteFirstClear[idx2], new StringBuilder("result_num").Append(value2 % 10).ToString(), Vector3.one, false);
					}
					value2 /= 10;
					idx2++;
				}
			}
		}
		else
		{
			AnimationCurve animNum = AnimationCurve.Linear(0f, 0f, 1f, add);
			float time = 0f;
			while (time <= 1f)
			{
				int idx = 0;
				int value = (int)animNum.Evaluate(time);
				if (mSpriteFirstClear != null)
				{
					while (value > 0)
					{
						if (idx >= 0 && idx < mSpriteFirstClear.Length)
						{
							Util.SetSpriteImageName(mSpriteFirstClear[idx], new StringBuilder("result_num").Append(value % 10).ToString(), Vector3.one, false);
						}
						value /= 10;
						idx++;
					}
				}
				yield return null;
				float tmp = time;
				time += Time.deltaTime;
				if (tmp < 1f && time > 1f)
				{
					time = 1f;
				}
			}
		}
		if (on_finish != null)
		{
			on_finish();
		}
	}

	private IEnumerator AnimationAcquireJewelAsync(int add, bool is_skip, float after_math = 0f, UnityAction on_finish = null)
	{
		if (mImageAcquireJewel != null)
		{
			float NUM_ANIMATION_TIME = ((!is_skip) ? 1f : 0f);
			float FADE_ANIMATION_TIME = ((!is_skip) ? 0.5f : 0f);
			AnimationCurve animNum = AnimationCurve.Linear(0f, mTotalAcquireJewel, NUM_ANIMATION_TIME, mTotalAcquireJewel + add);
			int value = 0;
			float time = 0f;
			while (time <= NUM_ANIMATION_TIME)
			{
				value = Mathf.CeilToInt(animNum.Evaluate(time));
				mImageAcquireJewel.SetNum(value);
				yield return null;
				float tmp = time;
				time += Time.deltaTime;
				if (tmp < NUM_ANIMATION_TIME && time > NUM_ANIMATION_TIME)
				{
					time = NUM_ANIMATION_TIME;
				}
			}
			if (!is_skip)
			{
				float pitch = 41f;
				int digit = ((value == 0) ? 1 : ((int)Mathf.Log10(value) + 1));
				NumberImageString numImage = Util.CreateGameObject("Num", mImageAcquireJewel.gameObject).AddComponent<NumberImageString>();
				numImage.Init(digit, value, mBaseDepth + 7, 1f, UIWidget.Pivot.Center, false, mAtlas[LOAD_ATLAS.EVENTLABYRINTH_POINT].Image, false, new string[10] { "result_total_num0", "result_total_num1", "result_total_num2", "result_total_num3", "result_total_num4", "result_total_num5", "result_total_num6", "result_total_num7", "result_total_num8", "result_total_num9" });
				numImage.SetPitch(pitch);
				UISprite[] numSprites = numImage.GetComponentsInChildren<UISprite>(true);
				mAcquireCopy = numImage.gameObject;
				AnimationCurve animScale = AnimationCurve.Linear(0f, 1f, FADE_ANIMATION_TIME, 2f);
				AnimationCurve animAlpha = AnimationCurve.Linear(0f, 1f, FADE_ANIMATION_TIME, 0f);
				time = 0f;
				while (time <= FADE_ANIMATION_TIME)
				{
					numImage.SetLocalScale(animScale.Evaluate(time));
					float alpha = animAlpha.Evaluate(time);
					if (numSprites != null)
					{
						UISprite[] array = numSprites;
						foreach (UISprite sprite in array)
						{
							if (sprite != null)
							{
								sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);
							}
						}
					}
					yield return null;
					float tmp2 = time;
					time += Time.deltaTime;
					if (tmp2 < FADE_ANIMATION_TIME && time > FADE_ANIMATION_TIME)
					{
						time = FADE_ANIMATION_TIME;
					}
				}
				UnityEngine.Object.Destroy(numImage.gameObject);
				numImage = null;
				numSprites = null;
			}
		}
		if (after_math > 0f && !is_skip)
		{
			yield return new WaitForSeconds(after_math);
		}
		if (on_finish != null)
		{
			on_finish();
		}
	}

	private IEnumerator AnimationPossessJewelAsync(int add, bool is_skip, UnityAction on_finish = null)
	{
		if (is_skip)
		{
			if (mLabelPossessJewel != null)
			{
				Util.SetLabelText(mLabelPossessJewel, (mPrevTotalJewel + add).ToString("#"));
			}
		}
		else if ((bool)mLabelPossessJewel)
		{
			AnimationCurve animNum = new AnimationCurve(new Keyframe(0f, mPrevTotalJewel, -1.5708f, 1.5708f), new Keyframe(1.5f, mPrevTotalJewel + add, 0f, 0f));
			AnimationCurve animScale = new AnimationCurve();
			for (int i = 0; i < 2; i++)
			{
				animScale.AddKey((float)i * 0.25f, 1f + (float)(i % 2) * 0.125f);
			}
			WrapMode preWrapMode = (animScale.postWrapMode = WrapMode.PingPong);
			animScale.preWrapMode = preWrapMode;
			float time = 0f;
			while (time <= 1.5f)
			{
				int value = (int)animNum.Evaluate(time);
				Util.SetLabelText(mLabelPossessJewel, value.ToString("#"));
				mLabelPossessJewel.SetLocalScale(animScale.Evaluate(time));
				yield return null;
				float tmp = time;
				time += Time.deltaTime;
				if (tmp < 1.5f && time > 1.5f)
				{
					time = 1.5f;
				}
			}
			mGame.PlaySe(LOAD_SOUND.SE_POINTEV_ADD_0.ToString(), -1);
			yield return new WaitForSeconds(0.5f);
		}
		if (on_finish != null)
		{
			on_finish();
		}
	}

	private IEnumerator AnimationJewelUpChanceAsync()
	{
		if ((bool)mJewelUpChanceRoot)
		{
			AnimationCurve animScale = AnimationCurve.Linear(0f, 2f, 0.25f, 0.9f);
			AnimationCurve animAlpha = AnimationCurve.Linear(0f, 0f, 0.25f, 1f);
			NGUITools.SetActive(mJewelUpChanceRoot, true);
			UISprite[] sprites = mJewelUpChanceRoot.GetComponentsInChildren<UISprite>(true);
			float time = 0f;
			while (time <= 0.25f)
			{
				mJewelUpChanceRoot.transform.SetLocalScale(animScale.Evaluate(time));
				float alpha = animAlpha.Evaluate(time);
				if (sprites != null)
				{
					UISprite[] array = sprites;
					foreach (UISprite sprite in array)
					{
						if (sprite != null)
						{
							sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);
						}
					}
				}
				yield return null;
				float tmp = time;
				time += Time.deltaTime;
				if (tmp < 0.25f && time > 0.25f)
				{
					time = 0.25f;
				}
			}
			mGame.PlaySe(LOAD_SOUND.SE_ORB_BREAK.ToString(), -1);
			yield return new WaitForSeconds(0.5f);
		}
		mState.Change(STATE.COUNT_UP_JEWEL_2);
	}

	private IEnumerator AnimationFirstClearBoxAsync(bool is_skip)
	{
		if (mBoxAnime != null)
		{
			if (is_skip)
			{
				mBoxAnime._sprite.AnimFrame = 50f;
			}
			mBoxAnime._sprite.AnimationFinished = delegate
			{
				mBoxAnime._sprite.AnimFrame = 50f;
				mBoxAnime._sprite.Play();
			};
			mBoxAnime._sprite.Play();
			mGame.PlaySe(LOAD_SOUND.SE_GC_CAPSULEOPEN_NORMAL.ToString(), -1);
			float time = 0.5f;
			while (!is_skip)
			{
				float num;
				time = (num = time - Time.deltaTime);
				if (!(num > 0f))
				{
					break;
				}
				yield return null;
			}
		}
		mState.Change(STATE.COUNT_UP_JEWEL_3_0);
	}

	private void OnCencelButton_Click(GameObject go)
	{
		if (!IsBusy)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void OnOkButton_Click(GameObject go)
	{
		if (!IsBusy)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
		mState.Change(STATE.SHOW_BOX);
	}

	public override void OnCloseFinished()
	{
		if (mAtlas != null)
		{
			foreach (ResourceInfo<ResImage> value in mAtlas.Values)
			{
				value.UnLoad();
			}
			mAtlas.Clear();
		}
		if (mSound != null)
		{
			foreach (ResourceInfo<ResSound> value2 in mSound.Values)
			{
				value2.UnLoad();
			}
			mSound.Clear();
		}
		if (mGauge != null)
		{
			mGauge.Dispose();
		}
		if ((bool)mBoxAnimeDemoCamera)
		{
			GameMain.SafeDestroy(mBoxAnimeDemoCamera);
		}
		mBoxAnimeDemoCamera = null;
		mAtlas = null;
		mSound = null;
		mGauge = null;
		if (mDisplayType == DISPLAY_TYPE.RESULT)
		{
			PlayerStageData _psd = null;
			int eventID = mGame.mCurrentStage.EventID;
			short a_course = 0;
			int a_main;
			int a_sub;
			Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
			SMEventLabyrinthStageSetting labyrinthStageSetting = mEventPageData.GetLabyrinthStageSetting(a_main, a_sub, a_course);
			mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, eventID, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
			int stageNo = Def.GetStageNo(a_course, a_main, a_sub);
			int stageNoForServer = Def.GetStageNoForServer(mGame.mCurrentStage.EventID, stageNo);
			int eventID2 = mGame.mCurrentStage.EventID;
			int num = mAcquireJewel;
			int num2 = GetMultipliedCourseBonus() - mAcquireJewel;
			int num3 = (IsDoubleUpChance ? (GetMultipliedCourseBonus(2f) - (num2 + num)) : 0);
			int num4 = ((mIsFirstClear && labyrinthStageSetting != null) ? labyrinthStageSetting.ItemAmount : 0);
			int num5 = num + num2 + num3 + num4;
			int jewelget_chance = (mHasJewelUpChance ? 1 : 0);
			int doubleup_chance = (int)mDoubleUpChanceState;
			int cost = 3;
			int total_doubleup_num = (mGame.mServerCramVariableData.Labyrinth.ContainsKey(eventID2) ? mGame.mServerCramVariableData.Labyrinth[eventID2].DoubleUpChanceCount : 0);
			int total_jewel_num = 0;
			int labyrinthBonusItemID = mGame.SegmentProfile.LabyrinthBonusItemID;
			if (mGame.mShopItemData.ContainsKey(labyrinthBonusItemID))
			{
				cost = mGame.mShopItemData[labyrinthBonusItemID].GemAmount;
			}
			if (mPlayerEventData != null && mPlayerEventData.LabyrinthData != null)
			{
				total_jewel_num = mPlayerEventData.LabyrinthData.JewelAmount + num5;
			}
			ServerCram.EventLabyrinthResult(stageNoForServer, eventID2, num5, num, num2, num3, num4, total_jewel_num, jewelget_chance, doubleup_chance, cost, 2f, total_doubleup_num);
			mBreakDownData[CALLBACK_BREAKDOWN.IN_PUZZLE] = num;
			mBreakDownData[CALLBACK_BREAKDOWN.COURSE_BONUS] = num2;
			mBreakDownData[CALLBACK_BREAKDOWN.DOUBLE_UP_CHANCE] = num3;
			mBreakDownData[CALLBACK_BREAKDOWN.FIRST_CLEAR] = num4;
		}
		InvokeCallback(SELECT_ITEM.OnClosed);
	}

	public override void OnBackKeyPress()
	{
		DISPLAY_TYPE dISPLAY_TYPE = mDisplayType;
		if (dISPLAY_TYPE == DISPLAY_TYPE.COURSE_CLEAR || dISPLAY_TYPE == DISPLAY_TYPE.STAGE_ALL_CLEAR)
		{
			OnOkButton_Click(null);
		}
		else
		{
			OnCencelButton_Click(null);
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		base.OnScreenChanged(o);
		SetLayout(o);
	}

	private void OnSkipArea_Clicked(GameObject go)
	{
		mIsSkip = true;
	}

	private void ExecCallbackMethod(UnityAction<UnityAction<int, KeyValuePair<CALLBACK_BREAKDOWN, int>[]>> method, params UnityAction<int, KeyValuePair<CALLBACK_BREAKDOWN, int>[]>[] calls)
	{
		if (method == null || calls == null)
		{
			return;
		}
		foreach (UnityAction<int, KeyValuePair<CALLBACK_BREAKDOWN, int>[]> unityAction in calls)
		{
			if (unityAction != null)
			{
				method(unityAction);
			}
		}
	}

	private CallBackEvent GetCallback(SELECT_ITEM type, bool force_create)
	{
		if (mCallback == null && force_create)
		{
			mCallback = new Dictionary<SELECT_ITEM, CallBackEvent>();
		}
		if (mCallback != null && !mCallback.ContainsKey(type) && force_create)
		{
			mCallback.Add(type, new CallBackEvent());
		}
		return (mCallback == null || !mCallback.ContainsKey(type)) ? null : mCallback[type];
	}

	private void InvokeCallback(SELECT_ITEM type)
	{
		CallBackEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.Invoke(mTotalAcquireJewel, mBreakDownData.ToArray());
		}
	}

	public void AddCallback(SELECT_ITEM type, params UnityAction<int, KeyValuePair<CALLBACK_BREAKDOWN, int>[]>[] calls)
	{
		if (calls != null)
		{
			CallBackEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.AddListener, calls);
			}
		}
	}

	public void RemoveCallback(SELECT_ITEM type, params UnityAction<int, KeyValuePair<CALLBACK_BREAKDOWN, int>[]>[] calls)
	{
		if (calls != null)
		{
			CallBackEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.RemoveListener, calls);
			}
		}
	}

	public void ClearCallback(SELECT_ITEM type)
	{
		CallBackEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.RemoveAllListeners();
		}
	}

	public new void ClearCallback()
	{
		foreach (int value in Enum.GetValues(typeof(SELECT_ITEM)))
		{
			ClearCallback((SELECT_ITEM)value);
		}
	}

	public void SetCallback(SELECT_ITEM type, params UnityAction<int, KeyValuePair<CALLBACK_BREAKDOWN, int>[]>[] calls)
	{
		ClearCallback(type);
		AddCallback(type, calls);
	}
}
