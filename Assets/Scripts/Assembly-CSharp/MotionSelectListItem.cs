public class MotionSelectListItem : SMVerticalListItem
{
	public int motionIndex;

	public bool enable = true;

	public int level;

	public string name = string.Empty;

	public CompanionData.MOTION motion = CompanionData.MOTION.STAY0;

	public MotionSelectListItem(int _motionIndex, string _name, CompanionData.MOTION _motion, int _level, bool _enable)
	{
		motionIndex = _motionIndex;
		enable = _enable;
		level = _level;
		name = _name;
		motion = _motion;
	}
}
