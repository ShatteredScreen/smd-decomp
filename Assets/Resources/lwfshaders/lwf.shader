Shader "LWF" {
Properties {
 _Color ("Color", Color) = (1,1,1,1)
 _AdditionalColor ("AdditionalColor", Color) = (0,0,0,0)
 _MainTex ("Texture", 2D) = "white" { }
 BlendModeSrc ("BlendModeSrc", Float) = 0
 BlendModeDst ("BlendModeDst", Float) = 0
 BlendEquation ("BlendEquation", Float) = 0
}
	//DummyShaderTextExporter
	
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard fullforwardshadows
#pragma target 3.0
		sampler2D _MainTex;
		struct Input
		{
			float2 uv_MainTex;
		};
		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
		}
		ENDCG
	}
}