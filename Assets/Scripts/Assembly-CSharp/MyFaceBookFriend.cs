using System;

public class MyFaceBookFriend : SMVerticalListItem, ICloneable, IComparable
{
	private string mName;

	private string mFbName;

	private string mGcName;

	private string mGpName;

	public MyFriendData Data;

	public int Version { get; set; }

	[MiniJSONAlias("uuid")]
	public int UUID { get; set; }

	[MiniJSONAlias("nickname")]
	public string Name
	{
		get
		{
			return mName;
		}
		set
		{
			mName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	[MiniJSONAlias("hiveid")]
	public string HiveId { get; set; }

	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int Level { get; set; }

	[MiniJSONAlias("xp")]
	public int Experience { get; set; }

	[MiniJSONAlias("fbid")]
	public string Fbid { get; set; }

	public string FbName
	{
		get
		{
			return mFbName;
		}
		set
		{
			mFbName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	[MiniJSONAlias("gcid")]
	public string Gcid { get; set; }

	public string GcName
	{
		get
		{
			return mGcName;
		}
		set
		{
			mGcName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	[MiniJSONAlias("gpid")]
	public string Gpid { get; set; }

	public string GpName
	{
		get
		{
			return mGpName;
		}
		set
		{
			mGpName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	[MiniJSONAlias("time")]
	public int UpdateTimeEpoch { get; set; }

	public DateTime IconModified { get; set; }

	public DateTime LastHeartSendTime { get; set; }

	public DateTime LastRoadBlockRequestTime { get; set; }

	public DateTime LastFriendHelpRequestTime { get; set; }

	[MiniJSONAlias("total_score")]
	public long TotalScore { get; set; }

	[MiniJSONAlias("total_star")]
	public long TotalStars { get; set; }

	[MiniJSONAlias("total_trophy")]
	public long TotalTrophy { get; set; }

	[MiniJSONAlias("iconid")]
	public int IconID { get; set; }

	[MiniJSONAlias("iconlvl")]
	public int IconLevel { get; set; }

	public string Gender { get; set; }

	public MyFaceBookFriend()
	{
		Data = new MyFriendData();
		Data.Version = 1290;
		Data.LastHeartSendTime = DateTimeUtil.UnitLocalTimeEpoch;
		Data.LastRoadBlockRequestTime = DateTimeUtil.UnitLocalTimeEpoch;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as SearchUserData);
	}

	public MyFaceBookFriend Clone()
	{
		return MemberwiseClone() as MyFaceBookFriend;
	}

	public int CompareTo(SearchUserData _obj)
	{
		if (_obj == null)
		{
			return -1;
		}
		return _obj.lvl - Level;
	}
}
