using UnityEngine;

public class BIJUICamera : MonoBehaviour
{
	public Vector2 UIScreenSize;

	public Vector2 transGameToUI;

	public void initCamera(Vector2 _baseScreenSize)
	{
		float num = _baseScreenSize.x / _baseScreenSize.y;
		float num2 = 1f;
		float num3;
		float num4;
		if (Screen.width > Screen.height)
		{
			num3 = (float)Screen.width / (float)Screen.height;
			num4 = 1f;
		}
		else
		{
			num4 = (float)Screen.height / (float)Screen.width;
			num3 = 1f;
		}
		transGameToUI.x = num3 / num;
		transGameToUI.y = num4 / num2;
		UIScreenSize = new Vector2(Screen.width * 2, Screen.height * 2);
	}

	public Vector2 transGameToUI_V2(Vector2 _vec)
	{
		return new Vector2(_vec.x * transGameToUI.x, _vec.y * transGameToUI.y);
	}

	public Vector3 transGameToUI_V3(Vector3 _vec)
	{
		return new Vector3(_vec.x * transGameToUI.x, _vec.y * transGameToUI.y, _vec.z);
	}
}
