public class TierCampaignListItem : TierSelectListItem
{
	public CampaignData mCampaignData;

	public bool mTimeOut;

	public UILabel mRemainTime;

	public UIButton mBuyButton;
}
