public class SpecialSpawnData
{
	public Def.TILE_KIND Kind;

	public Def.ITEM_CATEGORY Category;

	public int ItemIndex;

	public Def.UNLOCK_TYPE SpawnCondition;

	public int CountDown;

	public bool SpawnFinised;

	public SpecialSpawnData(Def.TILE_KIND kind, Def.ITEM_CATEGORY category, int itemID, Def.UNLOCK_TYPE spawnCondition, int count)
	{
		Kind = kind;
		Category = category;
		ItemIndex = itemID;
		SpawnCondition = spawnCondition;
		CountDown = count;
		SpawnFinised = false;
	}

	public override string ToString()
	{
		return string.Format("[SpawnCondition kind={0} category={1} itemindex={2} condition={3} count={4} finished={5}]", Kind, Category, ItemIndex, SpawnCondition, CountDown, SpawnFinised);
	}
}
