using System.Collections.Generic;
using UnityEngine;

public class SsAssetDatabase : MonoBehaviour
{
	public const string fileName = "SpriteStudioDatabase";

	public const string filePath = "Assets/SpriteStudioDatabase.prefab";

	[HideInInspector]
	public float ScaleFactor = 1f;

	[HideInInspector]
	public bool AngleCurveParamAsRadian = true;

	public List<SsAnimation> animeList = new List<SsAnimation>();

	public static SsAssetDatabase Instance;

	public static void CreateNewObject()
	{
		GameObject gameObject = new GameObject("SpriteStudioDatabase");
		gameObject.AddComponent<SsAssetDatabase>();
	}

	private void OnEnable()
	{
		if (!Instance)
		{
			GameObject gameObject = GameObject.Find("SpriteStudioDatabase");
			Instance = gameObject.GetComponent<SsAssetDatabase>();
			if (!Instance)
			{
				UnityEngine.Debug.Log("Not found SpriteStudioDatabase in this scene");
				return;
			}
		}
		animeList = Instance.animeList;
	}

	public SsAnimation[] GetAnimeArray()
	{
		return animeList.ToArray();
	}

	public void AddAnime(SsAnimation anm)
	{
	}

	public SsAnimation GetAnime(string name)
	{
		foreach (SsAnimation anime in animeList)
		{
			if (anime.name == name)
			{
				return anime;
			}
		}
		return null;
	}

	public void CleanupAnimeList()
	{
		animeList.RemoveAll((SsAnimation e) => e == null);
	}
}
