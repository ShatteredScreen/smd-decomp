using UnityEngine;

public class ResImage : ResourceInstance
{
	private UIAtlas mAtlas;

	public GameObject mPrefab;

	public UIAtlas Atlas
	{
		get
		{
			return mAtlas;
		}
		set
		{
			if (mAtlas != null)
			{
				mAtlas.spriteMaterial = null;
			}
			if (value == null)
			{
				if (Image != null)
				{
					Image.Atlas = null;
					Image = null;
				}
				mAtlas = null;
				return;
			}
			Image = value.gameObject.GetComponent<BIJNGUIImage>();
			if (Image == null)
			{
				Image = value.gameObject.AddComponent<BIJNGUIImage>();
			}
			Image.Atlas = value;
			mAtlas = value;
		}
	}

	public BIJNGUIImage Image { get; protected set; }

	public ResImage(string name)
		: base(name)
	{
		ResourceType = TYPE.IMAGE;
	}

	~ResImage()
	{
		if (mAtlas != null)
		{
			mAtlas.spriteMaterial = null;
		}
	}
}
