using System;

public class DailyChallengeEventSettings : ICloneable, IComparable
{
	[MiniJSONAlias("id")]
	public int DailyChallengeID { get; set; }

	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("sheet")]
	public string SheetName { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object obj)
	{
		return CompareTo(obj as DailyChallengeEventSettings);
	}

	public DailyChallengeEventSettings Clone()
	{
		return MemberwiseClone() as DailyChallengeEventSettings;
	}

	public int CompareTo(DailyChallengeEventSettings obj)
	{
		if (obj == null)
		{
			return int.MaxValue;
		}
		return (int)(StartTime - obj.StartTime);
	}
}
