using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMMapPart : MonoBehaviour
{
	public struct MovableObjectSpawn
	{
		public string AnimeKey;

		public Vector3 Position;

		public float AngleRange;

		public BIJImage Atlas;
	}

	public struct EnvParticle
	{
		public ParticleSystem PS;

		public float InitScale;

		public Vector3 InitPos;

		public bool UseScreenEdge;
	}

	protected const string MAP_IMAGE_NAME = "normal_map_{0:00}";

	protected float mMovableObjectSpawnTime;

	protected GameMain mGame;

	protected GameStateSMMapBase mGameState;

	protected Camera mCamera;

	protected int mMapIndex = -1;

	protected SMMapPage mSMMapPage;

	protected SMMapPageData mSMMapPageData;

	protected SMMapBG mBGImage;

	protected List<MapAccessory> mMapAccessoryList = new List<MapAccessory>();

	protected List<MapEntity> mMapEntityList = new List<MapEntity>();

	protected List<MapRoadBlock> mMapRBList = new List<MapRoadBlock>();

	protected List<EnvParticle> mMapEmitterList = new List<EnvParticle>();

	protected List<MapMovableObject> mMapMovableObjectList = new List<MapMovableObject>();

	protected List<MapSign> mMapSignList = new List<MapSign>();

	protected List<EventRoadBlock> mEventRBList = new List<EventRoadBlock>();

	protected List<MapMask> mMapMaskList = new List<MapMask>();

	protected List<ChangeMapButton> mChangeMapList = new List<ChangeMapButton>();

	protected List<MovableObjectSpawn> mMovableObjectSpawnList = new List<MovableObjectSpawn>();

	protected List<MapSNSIcon> mFriendIconList = new List<MapSNSIcon>();

	protected List<MapSNSIcon> mStrayUserList = new List<MapSNSIcon>();

	protected List<SsMapEntity> mStrayUserEntityList;

	protected MapAccessory.OnPushed mMapAccessoryCallback;

	protected MapRoadBlock.OnPushed mMapRoadBlockCallback;

	protected MapSNSIcon.OnPushed mMapStrayUserCallback;

	protected ChangeMapButton.OnPushed mMapChangeCallback;

	protected Vector3 mMapScale;

	protected bool mIsAvailable;

	protected Def.SERIES mSeries;

	protected LoopAnime mMapEndAnime;

	public bool IsActiveLoading;

	protected List<ResourceInstance> mLoadAsyncList = new List<ResourceInstance>();

	public bool IsActivating;

	public MapSign GetMapSign()
	{
		MapSign result = null;
		if (mMapSignList.Count > 0)
		{
			result = mMapSignList[0];
		}
		return result;
	}

	protected virtual void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mCamera = Camera.main;
	}

	protected virtual void Start()
	{
	}

	public void SetGameState(GameStateSMMapBase a_map, SMMapPage a_page, SMMapPageData a_data)
	{
		mGameState = a_map;
		mSMMapPage = a_page;
		mSMMapPageData = a_data;
	}

	protected virtual void OnDestroy()
	{
		if (mBGImage != null)
		{
			Object.Destroy(mBGImage.gameObject);
		}
		mBGImage = null;
	}

	protected virtual void Update()
	{
		if (mMovableObjectSpawnList.Count <= 0)
		{
			return;
		}
		mMovableObjectSpawnTime += Time.deltaTime;
		if (!(mMovableObjectSpawnTime > 5f) || mMapMovableObjectList.Count >= 10)
		{
			return;
		}
		mMovableObjectSpawnTime = 0f;
		int num = Random.Range(0, mMovableObjectSpawnList.Count);
		MovableObjectSpawn movableObjectSpawn = mMovableObjectSpawnList[num];
		MapMovableObject mapMovableObject = Util.CreateGameObject("MovableObject", base.gameObject).AddComponent<MapMovableObject>();
		float a_deg = 0f;
		bool flag = true;
		if (movableObjectSpawn.AngleRange > 0f)
		{
			a_deg = Random.Range((0f - movableObjectSpawn.AngleRange) / 2f, movableObjectSpawn.AngleRange / 2f);
			flag = false;
		}
		bool flag2 = true;
		Vector3 scale = Vector3.one;
		if (flag)
		{
			if (Random.Range(0, 10) > 5)
			{
				flag2 = false;
			}
			else
			{
				float num2 = Random.Range(0.5f, 1f);
				scale = new Vector3(num2, num2, 1f);
			}
		}
		if (flag2)
		{
			string movableAnimeKey = GetMovableAnimeKey(mSeries, mMapIndex, movableObjectSpawn.AnimeKey);
			float a_offset = mSMMapPage.transform.localPosition.y + 1136f * (float)mMapIndex;
			mapMovableObject.Init(0, movableObjectSpawn.AnimeKey, movableObjectSpawn.Atlas, movableObjectSpawn.Position.x, movableObjectSpawn.Position.y, scale, a_deg, num, mMapIndex, mSeries, flag, movableAnimeKey, a_offset);
			mapMovableObject.SetDisappearedCallback(OnMovableObjectDisappearedCallback);
			mMapMovableObjectList.Add(mapMovableObject);
		}
	}

	public virtual void Init(Def.SERIES a_series, int a_index, Vector3 a_mapScale, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadblockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB, bool a_isAvailable, bool useBg = true)
	{
		mSeries = a_series;
		mMapScale = a_mapScale;
		BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
		mMapIndex = a_index;
		mBGImage = Util.CreateGameObject("BG" + (a_index + 1), base.gameObject).AddComponent<SMMapBG>();
		mBGImage.Active(image, "null", Vector3.zero, mMapScale);
		mMapAccessoryCallback = a_accessoryCB;
		mMapRoadBlockCallback = a_roadblockCB;
		mMapStrayUserCallback = a_snsCB;
		mMapChangeCallback = a_mapChangeCB;
		mIsAvailable = a_isAvailable;
	}

	public virtual void Init(Def.SERIES a_series, int a_index, Vector3 a_mapScale, MapAccessory.OnPushed a_accessoryCB, bool a_isAvailable)
	{
		mSeries = a_series;
		mMapScale = a_mapScale;
		BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
		mMapIndex = a_index;
		mBGImage = null;
		mMapAccessoryCallback = a_accessoryCB;
		mMapRoadBlockCallback = null;
		mMapStrayUserCallback = null;
		mIsAvailable = a_isAvailable;
	}

	public MapAccessory TutorialData(List<SsMapEntity> a_accessoryList, List<SsMapEntity> a_mapEntityList, List<SsMapEntity> a_mapRBList, List<SsMapEntity> a_mapEmitterList, List<SsMapEntity> a_mapMovableList, List<SsMapEntity> a_strayUserList, bool a_inPlayer, AccessoryData data)
	{
		MapAccessory mapAccessory = null;
		SsMapEntity ssMapEntity = ((data.Index != 160) ? a_accessoryList[1] : a_accessoryList[0]);
		return CreateMapTutorialAccessory(ssMapEntity.MapOffsetCounter, string.Empty, data, null, ssMapEntity.Position, ssMapEntity.Scale);
	}

	public void LoadPartsAnimeAsync(SMMapSsData a_mapSsData)
	{
		mLoadAsyncList.Clear();
		List<SsMapEntity> accessoryListInMapIndex = a_mapSsData.GetAccessoryListInMapIndex(mMapIndex);
		List<SsMapEntity> mapEntityListInMapIndex = a_mapSsData.GetMapEntityListInMapIndex(mMapIndex);
		List<SsMapEntity> mapRBListInMapIndex = a_mapSsData.GetMapRBListInMapIndex(mMapIndex);
		List<SsMapEntity> mapERBListInMapIndex = a_mapSsData.GetMapERBListInMapIndex(mMapIndex);
		List<SsMapEntity> mapEmitterListInMapIndex = a_mapSsData.GetMapEmitterListInMapIndex(mMapIndex);
		List<SsMapEntity> mapMoveObjectListInMapIndex = a_mapSsData.GetMapMoveObjectListInMapIndex(mMapIndex);
		List<SsMapEntity> mapStrayUserListInMapIndex = a_mapSsData.GetMapStrayUserListInMapIndex(mMapIndex);
		List<SsMapEntity> mapSilhouetteListInMapIndex = a_mapSsData.GetMapSilhouetteListInMapIndex(mMapIndex);
		List<SsMapEntity> mapSignListInMapIndex = a_mapSsData.GetMapSignListInMapIndex(mMapIndex);
		string mapImageKey = GetMapImageKey();
		ResImage item = ResourceManager.LoadImageAsync(mapImageKey);
		mLoadAsyncList.Add(item);
		string mapAccessoryAnimeKey = GetMapAccessoryAnimeKey(mSeries, mMapIndex);
		ResSsAnimation item2 = ResourceManager.LoadSsAnimationAsync(mapAccessoryAnimeKey, true);
		ResSsAnimation item3 = ResourceManager.LoadSsAnimationAsync(mapAccessoryAnimeKey + "_TAP", true);
		ResSsAnimation item4 = ResourceManager.LoadSsAnimationAsync(mapAccessoryAnimeKey + "_TAPLOCK", true);
		ResSsAnimation item5 = ResourceManager.LoadSsAnimationAsync(mapAccessoryAnimeKey + "_LOCK", true);
		mLoadAsyncList.Add(item2);
		mLoadAsyncList.Add(item3);
		mLoadAsyncList.Add(item4);
		mLoadAsyncList.Add(item5);
		for (int i = 0; i < mapEntityListInMapIndex.Count; i++)
		{
			string animeKey = mapEntityListInMapIndex[i].AnimeKey;
			ResSsAnimation item6 = ResourceManager.LoadSsAnimationAsync(animeKey, true);
			mLoadAsyncList.Add(item6);
		}
		ResSsAnimation item7 = ResourceManager.LoadSsAnimationAsync("MAP_ROADBLOCK", true);
		ResSsAnimation item8 = ResourceManager.LoadSsAnimationAsync("MAP_ROADBLOCK_TAP", true);
		ResSsAnimation item9 = ResourceManager.LoadSsAnimationAsync("MAP_ROADBLOCK_UNLOCK", true);
		ResSsAnimation item10 = ResourceManager.LoadSsAnimationAsync("MAP_ROADBLOCK_UNLOCKED", true);
		mLoadAsyncList.Add(item7);
		mLoadAsyncList.Add(item8);
		mLoadAsyncList.Add(item9);
		mLoadAsyncList.Add(item10);
		if (mapSignListInMapIndex.Count > 0)
		{
			string key = GetSignAnimeKey(mSeries, mMapIndex) + "_LOCK";
			mLoadAsyncList.Add(ResourceManager.LoadSsAnimationAsync(key, true));
			key = GetSignAnimeKey(mSeries, mMapIndex) + "_UNLOCK";
			mLoadAsyncList.Add(ResourceManager.LoadSsAnimationAsync(key, true));
			key = GetSignAnimeKey(mSeries, mMapIndex) + "_UNLOCK_IDLE";
			mLoadAsyncList.Add(ResourceManager.LoadSsAnimationAsync(key, true));
		}
		for (int j = 0; j < mapMoveObjectListInMapIndex.Count; j++)
		{
			SsMapEntity ssMapEntity = mapMoveObjectListInMapIndex[j];
			string movableAnimeKey = GetMovableAnimeKey(mSeries, mMapIndex, ssMapEntity.AnimeKey);
			ResSsAnimation item11 = ResourceManager.LoadSsAnimationAsync(movableAnimeKey, true);
			mLoadAsyncList.Add(item11);
		}
		for (int k = 0; k < mapSilhouetteListInMapIndex.Count; k++)
		{
			string key2 = "SILHOUETTE_" + mapSilhouetteListInMapIndex[k].AnimeKey;
			ResSsAnimation item12 = ResourceManager.LoadSsAnimationAsync(key2, true);
			mLoadAsyncList.Add(item12);
		}
	}

	public virtual GameObject IntroRoadblock()
	{
		GameObject result = null;
		if (mMapRBList.Count == 0)
		{
			return result;
		}
		return mMapRBList[0].gameObject;
	}

	public virtual IEnumerator Active(SMMapSsData a_mapSsData, int a_mapIndex, bool a_inPlayer)
	{
		IsActivating = true;
		LoadPartsAnimeAsync(a_mapSsData);
		float _start = Time.realtimeSinceStartup;
		for (int i4 = 0; i4 < mLoadAsyncList.Count; i4++)
		{
			while (mLoadAsyncList[i4].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		mLoadAsyncList.Clear();
		BIJImage atlasCommon = ResourceManager.LoadImage("MAPCOMMON").Image;
		List<SsMapEntity> accessoryList = a_mapSsData.GetAccessoryListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapEntityList = a_mapSsData.GetMapEntityListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapRBList = a_mapSsData.GetMapRBListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapERBList = a_mapSsData.GetMapERBListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapEmitterList = a_mapSsData.GetMapEmitterListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapMovableList = a_mapSsData.GetMapMoveObjectListInMapIndex(a_mapIndex);
		List<SsMapEntity> strayUserList = a_mapSsData.GetMapStrayUserListInMapIndex(a_mapIndex);
		List<SsMapEntity> silhouetteList = a_mapSsData.GetMapSilhouetteListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapSignList = a_mapSsData.GetMapSignListInMapIndex(a_mapIndex);
		List<SsMapEntity> mapChangeList = a_mapSsData.GetMapChangeListInMapIndex(a_mapIndex);
		IsActiveLoading = true;
		mStrayUserEntityList = strayUserList;
		NGUITools.SetActive(base.gameObject, true);
		string map_key = GetMapImageKey();
		string accessoryAnimeKey = GetMapAccessoryAnimeKey(mSeries, mMapIndex);
		_start = Time.realtimeSinceStartup;
		for (int i3 = 0; i3 < mapEmitterList.Count; i3++)
		{
			SsMapEntity entity = mapEmitterList[i3];
			EnvParticle accesory = CreateMapEmitter(entity.MapOffsetCounter, entity.AnimeKey, entity.Position, entity.Scale, entity.Name.StartsWith("eps_"));
			mMapEmitterList.Add(accesory);
		}
		_start = Time.realtimeSinceStartup;
		BIJImage atlasMap = ResourceManager.LoadImage(map_key).Image;
		if (mBGImage != null)
		{
			mBGImage.Active(atlasMap, string.Format("normal_map_{0:00}", mMapIndex + 1), new Vector3(0f, 0f, Def.MAP_BASE_Z), mMapScale);
		}
		_start = Time.realtimeSinceStartup;
		for (int i2 = 0; i2 < accessoryList.Count; i2++)
		{
			SsMapEntity entity2 = accessoryList[i2];
			if (entity2.EnableDisplay)
			{
				MapAccessory accessory = CreateMapAccessory(data: mGame.GetAccessoryData(mGame.mPlayer.Data.CurrentSeries, entity2.AnimeKey), counter: entity2.MapOffsetCounter, animeName: string.Empty, atlas: atlasCommon, pos: entity2.Position, scale: entity2.Scale);
				mMapAccessoryList.Add(accessory);
			}
		}
		_start = Time.realtimeSinceStartup;
		for (int n = 0; n < mapEntityList.Count; n++)
		{
			SsMapEntity entity3 = mapEntityList[n];
			MapEntity accesory2 = CreateMapEntity(entity3.MapOffsetCounter, entity3.AnimeKey, atlasMap, entity3.Position, entity3.Scale);
			mMapEntityList.Add(accesory2);
		}
		_start = Time.realtimeSinceStartup;
		int nextRoadBlockLevel = mGame.mPlayer.NextRoadBlockLevel;
		for (int m = 0; m < mapRBList.Count; m++)
		{
			SsMapEntity entity4 = mapRBList[m];
			float order = float.Parse(entity4.AnimeKey);
			bool isUnlocked = true;
			int roadLevel = Def.GetStageNoByRouteOrder(order);
			if (nextRoadBlockLevel <= roadLevel)
			{
				isUnlocked = false;
			}
			SMMapPageData mapData = GameMain.GetSeriesMapPageData(mGame.mPlayer.Data.CurrentSeries);
			SMRoadBlockSetting setting = null;
			int mainStage;
			int subStage;
			Def.SplitStageNo(roadLevel, out mainStage, out subStage);
			foreach (KeyValuePair<string, SMRoadBlockSetting> roadBlockData in mapData.RoadBlockDataList)
			{
				SMRoadBlockSetting s = roadBlockData.Value;
				if (s.mStageNo == mainStage)
				{
					setting = s;
					break;
				}
			}
			if (setting != null)
			{
				MapRoadBlock accessory2 = CreateRoadBlock(entity4.MapOffsetCounter, entity4.AnimeKey, setting, atlasCommon, entity4.Position, entity4.Scale, isUnlocked, entity4.Angle);
				mMapRBList.Add(accessory2);
			}
		}
		if (mapChangeList.Count > 0)
		{
			_start = Time.realtimeSinceStartup;
			for (int l = 0; l < mapChangeList.Count; l++)
			{
				SsMapEntity entity8 = mapChangeList[l];
				SMMapPageData mapData2 = GameMain.GetSeriesMapPageData(mGame.mPlayer.Data.CurrentSeries);
				List<SMChapterSetting> chapterList = new List<SMChapterSetting>(mapData2.ChapterDataList.Values);
				chapterList.Sort((SMChapterSetting a, SMChapterSetting b) => a.ChapterID - b.ChapterID);
				ChangeMapButton accessory4 = CreateChangeMap(entity8.MapOffsetCounter, entity8.AnimeKey, chapterList[chapterList.Count - 1], atlasCommon, entity8.Position, entity8.Scale, false, entity8.Angle);
				mChangeMapList.Add(accessory4);
			}
		}
		if (mapSignList.Count > 0)
		{
			_start = Time.realtimeSinceStartup;
			for (int k = 0; k < mapSignList.Count; k++)
			{
				SsMapEntity entity7 = mapSignList[k];
				MapSign accessory3 = CreateSign(entity7.MapOffsetCounter, entity7.AnimeKey, atlasMap, entity7.Position, entity7.Scale);
				mMapSignList.Add(accessory3);
			}
		}
		if (mapMovableList.Count > 0)
		{
			_start = Time.realtimeSinceStartup;
			for (int j = 0; j < mapMovableList.Count; j++)
			{
				SsMapEntity entity6 = mapMovableList[j];
				MovableObjectSpawn spawn = CreateMovableObjectSpawn(entity6.MapOffsetCounter, entity6.AnimeKey, atlasCommon, entity6.Position, entity6.Angle);
				mMovableObjectSpawnList.Add(spawn);
			}
		}
		_start = Time.realtimeSinceStartup;
		MakeStrayUser(a_inPlayer);
		if (!mIsAvailable && mMapEndAnime == null)
		{
			_start = Time.realtimeSinceStartup;
			float offset = 0f;
			if (mSeries == Def.SERIES.SM_FIRST && (mMapIndex == 6 || mMapIndex == 9))
			{
				offset = 230f;
			}
			mMapEndAnime = Util.CreateLoopAnime("MapEnd" + (mMapIndex + 1), "MAP_END", base.gameObject, new Vector3(0f, offset, Def.MAP_END_Z), Vector3.one, 0f, 0f, false, null);
		}
		if (silhouetteList.Count > 0)
		{
			_start = Time.realtimeSinceStartup;
			for (int i = 0; i < silhouetteList.Count; i++)
			{
				SsMapEntity entity5 = silhouetteList[i];
				if (entity5.EnableDisplay)
				{
					string animName = "SILHOUETTE_" + entity5.AnimeKey;
					Vector3 pos = entity5.Position;
					pos.z = Def.MAP_SILHOUETTE_Z;
					MapEntity accesory3 = CreateMapSilhouette(entity5.MapOffsetCounter, animName, atlasMap, pos, entity5.Scale, entity5.AnimeKey);
					mMapEntityList.Add(accesory3);
				}
			}
		}
		IsActiveLoading = false;
		IsActivating = false;
	}

	public virtual void Deactive()
	{
		BIJImage image = ResourceManager.LoadImage("MAPCOMMON").Image;
		if (mBGImage != null)
		{
			mBGImage.Active(image, "null", Vector3.zero, mMapScale);
		}
		for (int i = 0; i < mMapAccessoryList.Count; i++)
		{
			MapAccessory mono = mMapAccessoryList[i];
			GameMain.SafeDestroy(ref mono);
		}
		mMapAccessoryList.Clear();
		for (int j = 0; j < mMapEntityList.Count; j++)
		{
			MapEntity mono2 = mMapEntityList[j];
			GameMain.SafeDestroy(ref mono2);
		}
		mMapEntityList.Clear();
		for (int k = 0; k < mMapRBList.Count; k++)
		{
			MapRoadBlock mono3 = mMapRBList[k];
			GameMain.SafeDestroy(ref mono3);
		}
		mMapRBList.Clear();
		for (int l = 0; l < mChangeMapList.Count; l++)
		{
			ChangeMapButton changeMapButton = mChangeMapList[l];
			Object.Destroy(changeMapButton.gameObject);
		}
		mChangeMapList.Clear();
		for (int m = 0; m < mMapEmitterList.Count; m++)
		{
			Object.Destroy(mMapEmitterList[m].PS.gameObject);
		}
		mMapEmitterList.Clear();
		for (int n = 0; n < mMapMovableObjectList.Count; n++)
		{
			MapMovableObject mapMovableObject = mMapMovableObjectList[n];
			Object.Destroy(mapMovableObject.gameObject);
		}
		mMapMovableObjectList.Clear();
		mMovableObjectSpawnList.Clear();
		for (int num = 0; num < mStrayUserList.Count; num++)
		{
			MapSNSIcon mapSNSIcon = mStrayUserList[num];
			Object.Destroy(mapSNSIcon.gameObject);
		}
		mStrayUserList.Clear();
		for (int num2 = 0; num2 < mMapSignList.Count; num2++)
		{
			MapSign mapSign = mMapSignList[num2];
			Object.Destroy(mapSign.gameObject);
		}
		mMapSignList.Clear();
		if (mMapEndAnime != null)
		{
			GameMain.SafeDestroy(mMapEndAnime.gameObject);
			mMapEndAnime = null;
		}
		NGUITools.SetActive(base.gameObject, false);
	}

	public virtual string GetMapImageKey()
	{
		string arg = Def.SeriesPrefix[mSeries];
		return string.Format("MAP_PAGE_{0}_ATLAS_{1}", arg, mMapIndex + 1);
	}

	public virtual string GetMapAccessoryAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string arg = Def.SeriesPrefix[a_series];
		return string.Format("MAP_{0}_ACCESSORY", arg, a_mapIndex + 1);
	}

	public virtual string GetButtonAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string arg = Def.SeriesPrefix[a_series];
		return string.Format("MAP_{0}_STAGEBUTTON", arg, a_mapIndex + 1);
	}

	public virtual string GetLineAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string arg = Def.SeriesPrefix[a_series];
		return string.Format("MAP_{0}_LINE", arg, a_mapIndex + 1);
	}

	public virtual string GetSignAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string arg = Def.SeriesPrefix[a_series];
		return string.Format("MAP_{0}_SIGN_{1}", arg, a_mapIndex + 1);
	}

	public virtual string GetMaskAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string arg = Def.SeriesPrefix[a_series];
		return string.Format("MAP_{0}_MASK_{1}", arg, a_mapIndex + 1);
	}

	public virtual string GetMovableAnimeKey(Def.SERIES a_series, int a_mapIndex, string a_name)
	{
		return string.Format("MAP_MOVE_{0}_{1}_", Def.SeriesPrefix[a_series], a_mapIndex + 1) + a_name;
	}

	public virtual string GetEpisodeSilhouetteAnimeKey(Def.SERIES a_series, int a_mapIndex)
	{
		string arg = Def.SeriesPrefix[a_series];
		return string.Format("SILHOUETTE_{0}", arg, a_mapIndex + 1);
	}

	public bool ContainsEntity(Vector3 a_worldPos)
	{
		foreach (MapRoadBlock mMapRB in mMapRBList)
		{
			if (mMapRB.ContainsPoint(a_worldPos))
			{
				return true;
			}
		}
		foreach (MapAccessory mMapAccessory in mMapAccessoryList)
		{
			if (mMapAccessory.ContainsPoint(a_worldPos))
			{
				return true;
			}
		}
		return false;
	}

	public bool OpenAccessory(string a_layer)
	{
		foreach (MapAccessory mMapAccessory in mMapAccessoryList)
		{
			if (mMapAccessory.mData.AnimeLayerName == a_layer)
			{
				mMapAccessory.OpenAnime();
				return true;
			}
		}
		return false;
	}

	public bool OpenRoadBlock(int a_roadStage)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_roadStage, out a_main, out a_sub);
		foreach (MapRoadBlock mMapRB in mMapRBList)
		{
			if (mMapRB.mData.mStageNo == a_main)
			{
				mMapRB.OpenAnime();
				return true;
			}
		}
		return false;
	}

	public virtual bool UnlockRoadBlock(int a_roadStage)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_roadStage, out a_main, out a_sub);
		foreach (MapRoadBlock mMapRB in mMapRBList)
		{
			if (mMapRB.mData.mStageNo == a_main)
			{
				mMapRB.UnlockAnime();
				return true;
			}
		}
		return false;
	}

	public void ChangeRoadBlock(int a_roadStage)
	{
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_roadStage, out a_main, out a_sub);
		foreach (MapRoadBlock mMapRB in mMapRBList)
		{
			if (mMapRB.mData.mStageNo == a_main)
			{
				mMapRB.ChangeMode();
				break;
			}
		}
	}

	public virtual void OpenNewAreaHide()
	{
		mIsAvailable = true;
		if (mMapEndAnime != null)
		{
			Object.Destroy(mMapEndAnime.gameObject);
			mMapEndAnime = null;
		}
	}

	public virtual void OpenNewArea()
	{
		float y = mMapEndAnime.transform.localPosition.y;
		OpenNewAreaHide();
		Util.CreateOneShotAnime("MapEndUnlock" + (mMapIndex + 1), "MAP_END_UNLOCK", base.gameObject, new Vector3(0f, y, Def.MAP_END_Z), Vector3.one, 0f, 0f);
	}

	public void OnMovableObjectDisappearedCallback(MapMovableObject a_object)
	{
		MapMovableObject mapMovableObject = mMapMovableObjectList.Find((MapMovableObject a) => a == a_object);
		if (mapMovableObject != null)
		{
			mMapMovableObjectList.Remove(mapMovableObject);
			Object.Destroy(mapMovableObject.gameObject);
			mapMovableObject = null;
		}
	}

	public MapAccessory CreateMapTutorialAccessory(int counter, string animeName, AccessoryData data, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		bool a_alreadyHas = false;
		if (mGame.mPlayer.IsAccessoryUnlock(data.Index))
		{
			a_alreadyHas = true;
		}
		MapAccessory mapAccessory = Util.CreateGameObject("Accessory" + data.Index, base.gameObject).AddComponent<MapAccessory>();
		mapAccessory.Init(counter, animeName, data, atlas, pos.x, pos.y, scale, a_alreadyHas, mMapIndex, mSeries, true, GetMapAccessoryAnimeKey(mSeries, mMapIndex));
		mapAccessory.SetPushedCallback(mMapAccessoryCallback);
		return mapAccessory;
	}

	public MapAccessory CreateMapAccessory(int counter, string animeName, AccessoryData data, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		bool a_alreadyHas = false;
		if (mGame.mPlayer.IsAccessoryUnlock(data.Index))
		{
			a_alreadyHas = true;
		}
		MapAccessory mapAccessory = Util.CreateGameObject("Accessory" + data.Index, base.gameObject).AddComponent<MapAccessory>();
		mapAccessory.Init(counter, animeName, data, atlas, pos.x, pos.y, scale, a_alreadyHas, mMapIndex, mSeries, GetMapAccessoryAnimeKey(mSeries, mMapIndex));
		mapAccessory.SetPushedCallback(mMapAccessoryCallback);
		if (GameMain.USE_DEBUG_DIALOG)
		{
			mapAccessory.SetDebugInfo(data);
		}
		return mapAccessory;
	}

	public MapEntity CreateMapEntity(int counter, string animeName, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		MapEntity mapEntity = Util.CreateGameObject("Entity" + counter, base.gameObject).AddComponent<MapEntity>();
		mapEntity.Init(counter, animeName, atlas, pos.x, pos.y, pos.z, scale);
		return mapEntity;
	}

	public MapSilhouette CreateMapSilhouette(int counter, string animeName, BIJImage atlas, Vector3 pos, Vector3 scale, string pngname)
	{
		MapSilhouette mapSilhouette = Util.CreateGameObject("Silhouette" + counter, base.gameObject).AddComponent<MapSilhouette>();
		mapSilhouette.Init(counter, animeName, atlas, pos.x, pos.y, pos.z, scale, pngname);
		return mapSilhouette;
	}

	public MapSilhouette CreateEpisodeSilhouette(int counter, string animeName, BIJImage atlas, Vector3 pos, Vector3 scale, string pngname)
	{
		MapSilhouette mapSilhouette = Util.CreateGameObject("Silhouette" + counter, base.gameObject).AddComponent<MapSilhouette>();
		mapSilhouette.InitEpisode(counter, animeName, atlas, pos.x, pos.y, pos.z, scale, pngname);
		return mapSilhouette;
	}

	public virtual MapRoadBlock CreateRoadBlock(int counter, string animeName, SMRoadBlockSetting a_data, BIJImage atlas, Vector3 pos, Vector3 scale, bool a_isUnlocked, float a_angle)
	{
		MapRoadBlock mapRoadBlock = Util.CreateGameObject("RB" + counter, base.gameObject).AddComponent<MapRoadBlock>();
		mapRoadBlock.Init(counter, animeName, a_data, atlas, pos.x, pos.y, scale, a_isUnlocked, a_angle);
		mapRoadBlock.SetPushedCallback(mMapRoadBlockCallback);
		return mapRoadBlock;
	}

	public virtual ChangeMapButton CreateChangeMap(int counter, string animeName, SMChapterSetting a_data, BIJImage atlas, Vector3 pos, Vector3 scale, bool a_isUnlocked, float a_angle)
	{
		ChangeMapButton changeMapButton = Util.CreateGameObject("RB" + counter, base.gameObject).AddComponent<ChangeMapButton>();
		changeMapButton.Init(counter, animeName, a_data, atlas, pos.x, pos.y, scale, a_isUnlocked, a_angle);
		changeMapButton.SetPushedCallback(mMapChangeCallback);
		return changeMapButton;
	}

	protected void SetParticleParameter(ref EnvParticle a_ep)
	{
		Vector2 vector = Util.LogScreenSize();
		Vector3 localPosition = a_ep.PS.gameObject.transform.localPosition;
		float num = a_ep.InitPos.x;
		float initScale = a_ep.InitScale;
		float startSize = 1f;
		float num2 = 0f;
		switch (Util.ScreenOrientation)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			num2 = 1136f / (float)Screen.height * (float)Screen.width / 2f;
			startSize = initScale;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			num2 = 1136f / (float)Screen.width * (float)Screen.width / 2f / mGame.mDeviceRatioScale;
			startSize = initScale * mGame.mDeviceRatioScale * 1.5f;
			break;
		}
		if (a_ep.UseScreenEdge)
		{
			if (num > 0f)
			{
				num = num2;
			}
			else if (num < 0f)
			{
				num = 0f - num2;
			}
		}
		a_ep.PS.startSize = startSize;
		a_ep.PS.gameObject.transform.localPosition = new Vector3(num, localPosition.y, localPosition.z);
		a_ep.PS.Clear();
		if (a_ep.UseScreenEdge)
		{
			a_ep.PS.Emit(5);
		}
		a_ep.PS.Play();
	}

	public EnvParticle CreateMapEmitter(int counter, string animeName, Vector3 pos, Vector3 scale, bool useScreenEdge = false)
	{
		ParticleSystem component = GameMain.GetParticle("Particles/" + animeName).GetComponent<ParticleSystem>();
		component.gameObject.transform.parent = base.transform;
		component.gameObject.layer = base.gameObject.layer;
		float startSize = component.startSize;
		component.gameObject.transform.localPosition = new Vector3(pos.x, pos.y, Def.MAP_BASE_Z - 2f);
		component.gameObject.transform.localScale = Vector3.one;
		EnvParticle a_ep = default(EnvParticle);
		a_ep.PS = component;
		a_ep.InitScale = startSize;
		a_ep.InitPos = pos;
		a_ep.UseScreenEdge = useScreenEdge;
		SetParticleParameter(ref a_ep);
		return a_ep;
	}

	public MovableObjectSpawn CreateMovableObjectSpawn(int counter, string animeName, BIJImage atlas, Vector3 pos, float angle)
	{
		MovableObjectSpawn result = default(MovableObjectSpawn);
		result.Position = pos;
		result.AnimeKey = animeName;
		result.Atlas = atlas;
		result.AngleRange = angle;
		return result;
	}

	public MapSign CreateSign(int counter, string animeName, BIJImage atlas, Vector3 pos, Vector3 scale)
	{
		MapSign mapSign = Util.CreateGameObject("Sign" + mMapIndex, base.gameObject).AddComponent<MapSign>();
		mapSign.Init(counter, animeName, atlas, pos.x, pos.y, scale, mMapIndex, GetSignAnimeKey(mSeries, mMapIndex));
		return mapSign;
	}

	public MapMask CreateMask(int counter, string animeName, BIJImage atlas, Vector3 pos, Vector3 scale, bool isLock)
	{
		MapMask mapMask = Util.CreateGameObject("Mask" + mMapIndex, base.gameObject).AddComponent<MapMask>();
		mapMask.Init(counter, animeName, atlas, pos.x, pos.y, scale, mMapIndex, GetMaskAnimeKey(mSeries, mMapIndex), isLock);
		return mapMask;
	}

	public void SetEnable(bool a_flg)
	{
		for (int i = 0; i < mMapAccessoryList.Count; i++)
		{
			MapAccessory mapAccessory = mMapAccessoryList[i];
			mapAccessory.SetEnable(a_flg);
		}
		for (int j = 0; j < mMapRBList.Count; j++)
		{
			MapRoadBlock mapRoadBlock = mMapRBList[j];
			mapRoadBlock.SetEnable(a_flg);
		}
		for (int k = 0; k < mStrayUserList.Count; k++)
		{
			MapSNSIcon mapSNSIcon = mStrayUserList[k];
			mapSNSIcon.SetEnable(a_flg);
		}
	}

	public void RemoveStrayUser()
	{
		for (int i = 0; i < mStrayUserList.Count; i++)
		{
			MapSNSIcon mapSNSIcon = mStrayUserList[i];
			Object.Destroy(mapSNSIcon.gameObject);
		}
		mStrayUserList.Clear();
	}

	public MapEntity RemoveSilhouette(string a_layer)
	{
		string strB = "SILHOUETTE_" + a_layer;
		for (int i = 0; i < mMapEntityList.Count; i++)
		{
			MapEntity mapEntity = mMapEntityList[i];
			if (mapEntity.AnimeKey.CompareTo(strB) == 0)
			{
				return mapEntity;
			}
		}
		return null;
	}

	public MapEntity RemoveEpisodeSilhouette(string a_id)
	{
		for (int i = 0; i < mMapEntityList.Count; i++)
		{
			MapSilhouette mapSilhouette = mMapEntityList[i] as MapSilhouette;
			if (mapSilhouette != null && mapSilhouette.UniqueID.CompareTo(a_id) == 0)
			{
				return mapSilhouette;
			}
		}
		return null;
	}

	public void OnMapSNSPushed(GameObject go)
	{
		if (mMapStrayUserCallback != null)
		{
			mMapStrayUserCallback(go);
		}
	}

	public void MakeStrayUser(bool a_inPlayer)
	{
		bool flag = a_inPlayer;
		if (mGame.AllStray)
		{
			flag = true;
		}
		if (!flag)
		{
			return;
		}
		for (int i = 0; i < mStrayUserList.Count; i++)
		{
			MapSNSIcon mapSNSIcon = mStrayUserList[i];
			Object.Destroy(mapSNSIcon.gameObject);
		}
		mStrayUserList.Clear();
		if (!mGame.AllStray)
		{
			if (!flag || mStrayUserEntityList == null || mStrayUserEntityList.Count <= 0)
			{
				return;
			}
			int index = 0;
			float num = 100000000f;
			for (int j = 0; j < mStrayUserEntityList.Count; j++)
			{
				Vector3 b = mSMMapPage.mAvater.Position - new Vector3(0f, 1136f * (float)mMapIndex, 0f);
				float num2 = Vector3.Distance(mStrayUserEntityList[j].Position, b);
				if (num > num2)
				{
					num = num2;
					index = j;
				}
			}
			SsMapEntity ssMapEntity = mStrayUserEntityList[index];
			MapSNSIcon mapSNSIcon2 = Util.CreateGameObject("StrayUser", base.gameObject).AddComponent<MapSNSIcon>();
			MapSNSSetting setting = default(MapSNSSetting);
			setting.IconID = -1;
			setting.Icon = null;
			mapSNSIcon2.Init(setting, new Vector3(ssMapEntity.Position.x, ssMapEntity.Position.y, Def.MAP_FRIEND_Z));
			mapSNSIcon2.SetPushedCallback(OnMapSNSPushed);
			mStrayUserList.Add(mapSNSIcon2);
		}
		else if (mStrayUserEntityList != null)
		{
			for (int k = 0; k < mStrayUserEntityList.Count; k++)
			{
				SsMapEntity ssMapEntity2 = mStrayUserEntityList[k];
				MapSNSIcon mapSNSIcon3 = Util.CreateGameObject("StrayUser", base.gameObject).AddComponent<MapSNSIcon>();
				MapSNSSetting setting2 = default(MapSNSSetting);
				setting2.IconID = -1;
				setting2.Icon = null;
				mapSNSIcon3.Init(setting2, new Vector3(ssMapEntity2.Position.x, ssMapEntity2.Position.y, Def.MAP_FRIEND_Z));
				mapSNSIcon3.SetPushedCallback(OnMapSNSPushed);
				mStrayUserList.Add(mapSNSIcon3);
			}
		}
	}

	public virtual void SetLayout(bool a_isLandscape)
	{
		for (int i = 0; i < mMapEmitterList.Count; i++)
		{
			EnvParticle a_ep = mMapEmitterList[i];
			SetParticleParameter(ref a_ep);
			mMapEmitterList[i] = a_ep;
		}
	}
}
