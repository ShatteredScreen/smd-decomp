using UnityEngine;

public class EvenAdvGuideDialog : DialogBase
{
	public delegate void OnDialogClosed();

	private OnDialogClosed mCallback;

	private string mSpriteName;

	private string mTitle;

	private string mDesc;

	private string mDesc2;

	private new string mLoadImageAtlas;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
		}
	}

	public override void BuildDialog()
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		ResImage resImage2 = ResourceManager.LoadImage(mLoadImageAtlas);
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(mTitle);
		if (mSpriteName != null)
		{
			UISprite sprite = Util.CreateSprite("TitleSprite", base.gameObject, resImage2.Image);
			Util.SetSpriteInfo(sprite, mSpriteName, mBaseDepth + 2, new Vector3(0f, 150f, 0f), Vector3.one, false);
		}
		if (mDesc != null)
		{
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, mDesc, mBaseDepth + 3, new Vector3(0f, -8f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.spacingY = 6;
		}
		if (mDesc2 != null)
		{
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, mDesc2, mBaseDepth + 3, new Vector3(0f, -115f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.red;
			uILabel.spacingY = 6;
		}
		UIButton button = Util.CreateJellyImageButton("ButtonClose", base.gameObject, resImage.Image);
		string text = "LC_button_close";
		Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 4, new Vector3(0f, -225f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(string spriteName, string title, string desc, string desc2, string LoadImageAtlas = "EVENTHUD")
	{
		mSpriteName = spriteName;
		mTitle = title;
		mDesc = desc;
		mDesc2 = desc2;
		mLoadImageAtlas = LoadImageAtlas;
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		if (!GetBusy())
		{
			OnCancelPushed(null);
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
