using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

public class SearchUserData : SMVerticalListItem, ICloneable, IComparable
{
	private string mNickName;

	public int uuid { get; set; }

	public string nickname
	{
		get
		{
			return mNickName;
		}
		set
		{
			mNickName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public int iconid { get; set; }

	public int emoid { get; set; }

	public int iconlvl { get; set; }

	public int series { get; set; }

	public int lvl { get; set; }

	public string fbid { get; set; }

	[MiniJSONAlias("updatetime")]
	public int UpdateTimeEpoch { get; set; }

	[MiniJSONAlias("play_stage")]
	public List<PlayStageParam> PlayStage { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as SearchUserData);
	}

	public SearchUserData Clone()
	{
		return MemberwiseClone() as SearchUserData;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[SearchUserData: {0}]", stringBuilder.ToString());
	}

	public void CopyTo(StageRankingData _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(StageRankingData);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}

	public int CompareTo(SearchUserData _obj)
	{
		if (_obj == null)
		{
			return -1;
		}
		return _obj.lvl - lvl;
	}
}
