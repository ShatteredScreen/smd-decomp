using System.IO;

public class ConnectAdvGetGameData : ConnectBase
{
	private Stream mResStream;

	private bool mIsSetResponseData;

	public void SetResponseData(Stream a_stream)
	{
		mResStream = a_stream;
		mIsSetResponseData = true;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (!Server.AdvGetSaveData())
		{
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetAdvGetGameDataFromResponse(mResStream);
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result == 0)
		{
			if (!mIsSetResponseData)
			{
				BIJLog.E("No set response Data:" + GetType().Name);
				base.StateConnectFinish();
			}
		}
		else if (!flag)
		{
			ShowErrorDialog(mErrorDialogStyle);
		}
		return false;
	}
}
