using System;
using System.Collections.Generic;

public static class EnumHelper
{
	private static string PathCombine(string a, string b)
	{
		char[] separator = new char[2] { '/', '\\' };
		string[] array = a.Split(separator, StringSplitOptions.RemoveEmptyEntries);
		string[] array2 = b.Split(separator, StringSplitOptions.RemoveEmptyEntries);
		List<string> list = new List<string>(array.Length + array2.Length);
		list.AddRange(array);
		list.AddRange(array2);
		return string.Join("/", list.ToArray());
	}

	public static T FromString<T>(string enum_value_string)
	{
		return (T)Enum.Parse(typeof(T), enum_value_string);
	}

	public static T FromString<T>(string enum_value_string, T default_value)
	{
		try
		{
			return (T)Enum.Parse(typeof(T), enum_value_string);
		}
		catch (Exception)
		{
			return default_value;
		}
	}

	public static T FromObject<T>(object enum_value)
	{
		return (T)Enum.ToObject(typeof(T), enum_value);
	}

	public static T FromObject<T>(object enum_value, T default_value)
	{
		try
		{
			return (T)Enum.ToObject(typeof(T), enum_value);
		}
		catch (Exception)
		{
			return default_value;
		}
	}

	public static T GetAttribute<T>(Enum value) where T : Attribute
	{
		Type type = value.GetType();
		string name = Enum.GetName(type, value);
		T[] array = (T[])type.GetField(name).GetCustomAttributes(typeof(T), false);
		return (array == null || array.Length <= 0) ? ((T)null) : array[0];
	}
}
