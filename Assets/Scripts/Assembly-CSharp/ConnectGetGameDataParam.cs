public class ConnectGetGameDataParam : ConnectParameterBase
{
	public bool UseHive { get; private set; }

	public int HiveID { get; private set; }

	public int UUID { get; private set; }

	public ConnectGetGameDataParam(int a_hiveID, int a_uuid, bool a_useHive = false)
	{
		UseHive = a_useHive;
		HiveID = a_hiveID;
		UUID = a_uuid;
	}
}
