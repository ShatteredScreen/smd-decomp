using System;

public class GiftListItem : SMVerticalListItem, IComparable
{
	public bool selected;

	public GiftItem gift;

	public MyFriend friend;

	public int sendCategory;

	public int sendkind;

	public DateTime? expired;

	public UILabel message;

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as GiftListItem);
	}

	public int CompareTo(GiftListItem _obj)
	{
		try
		{
			if (_obj == null)
			{
				return int.MinValue;
			}
			if (gift != null && _obj.gift != null)
			{
				return -(gift.CreateTimeEpoch - _obj.gift.CreateTimeEpoch);
			}
			if (friend != null && _obj.friend != null)
			{
				return -(friend.Data.UpdateTimeEpoch - _obj.friend.Data.UpdateTimeEpoch);
			}
		}
		catch (Exception)
		{
		}
		return int.MinValue;
	}

	public static GiftListItem Make(GiftItem a_gift, MyFriend a_friend, DateTime? expired)
	{
		GiftListItem giftListItem = new GiftListItem();
		giftListItem.gift = a_gift;
		giftListItem.friend = a_friend;
		giftListItem.expired = expired;
		return giftListItem;
	}

	public override void Update()
	{
		base.Update();
		if (!(message != null) || gift == null || gift.Data.ItemCategory != 18)
		{
			return;
		}
		int num = 0;
		int num2 = 0;
		if (expired.HasValue)
		{
			DateTime value = expired.Value;
			TimeSpan timeSpan = value - DateTime.Now;
			if (timeSpan.TotalSeconds > 0.0)
			{
				num = timeSpan.Minutes;
				num2 = timeSpan.Seconds;
			}
		}
		message.text = Util.MakeLText("FriendHelpBeRequest01") + "\n" + Util.MakeLText("FriendHelpBeRequest_Time", num, num2);
	}
}
