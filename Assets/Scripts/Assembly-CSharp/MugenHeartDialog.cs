using UnityEngine;

public class MugenHeartDialog : DialogBase
{
	public enum SELECT_STATUS
	{
		MUGEN_START = 0,
		MUGEN_END = 1
	}

	public delegate void OnDialogClosed(SELECT_STATUS i);

	private SELECT_STATUS mStatus;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey;
		string text;
		string text2;
		if (mStatus == SELECT_STATUS.MUGEN_START)
		{
			titleDescKey = Localization.Get("Mugen_Heart_Start_Title");
			text = string.Format(Localization.Get("Mugen_Heart_Start_Desc"), mGame.mPlayer.Data.mUseMugenHeartSetTime / 60);
			text2 = "LC_button_ok2";
		}
		else
		{
			titleDescKey = Localization.Get("Mugen_Heart_Expired_Title");
			text = Localization.Get("Mugen_Heart_Expired_Desc");
			text2 = "LC_button_close";
		}
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 67f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 104);
		uISprite.type = UIBasicSprite.Type.Sliced;
		UISprite sprite = Util.CreateSprite("icon", uISprite.gameObject, image);
		Util.SetSpriteInfo(sprite, "icon_heartinfinite", mBaseDepth + 3, new Vector3(0f, -3f, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(0f, -32f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect2(uILabel);
		UIButton button = Util.CreateJellyImageButton("Button", base.gameObject, image);
		Util.SetImageButtonInfo(button, text2, text2, text2, mBaseDepth + 3, new Vector3(0f, -117f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
	}

	public void Init(SELECT_STATUS i)
	{
		mStatus = i;
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			string key = ((mStatus != 0) ? "SE_NEGATIVE" : "SE_POSITIVE");
			mGame.PlaySe(key, -1);
			Close();
		}
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mStatus);
		}
		Object.Destroy(base.gameObject);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
