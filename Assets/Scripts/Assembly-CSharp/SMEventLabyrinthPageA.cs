using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEventLabyrinthPageA : SMMapPage
{
	protected int mEventID;

	protected short mCourseID;

	private bool mIsFirstLockStage = true;

	private bool mDoneActiveCamera;

	public int EventID
	{
		get
		{
			return mEventID;
		}
	}

	public short CourseID
	{
		get
		{
			return mCourseID;
		}
	}

	protected override void Update()
	{
		base.Update();
	}

	public override IEnumerator UnloadResource()
	{
		SMEventPageData eventPageData = mPageData as SMEventPageData;
		ResourceManager.UnloadResourceByAssetBundleName(ResourceManager.GetAssetBundleNameByResourceKey(eventPageData.EventSetting.MapAtlasKey));
		yield return Resources.UnloadUnusedAssets();
	}

	public IEnumerator Init(Def.SERIES a_series, int a_eventID, short a_courseID, int a_maxLevel, OnLevelPushed a_lvlPushedCB, MapAccessory.OnPushed a_accessoryCB, MapRoadBlock.OnPushed a_roadBlockCB, MapSNSIcon.OnPushed a_snsCB, ChangeMapButton.OnPushed a_mapChangeCB, SMMapPageData a_data)
	{
		mPageData = a_data;
		mSeries = a_series;
		mEventID = a_eventID;
		mCourseID = a_courseID;
		if (mSeries != mPageData.Series)
		{
		}
		SMEventPageData eventPageData = a_data as SMEventPageData;
		mMaxModulePage = mPageData.GetMap(a_courseID).ModuleMaxPageNum;
		mLevelPushedCallback = a_lvlPushedCB;
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		yield return StartCoroutine(LoadRouteData(a_series, mGame.GetMapSize(eventPageData.EventSetting.EventType), a_eventID, a_courseID));
		mMaxPage = NewMapSsDataEntity.GetMapIndex(a_maxLevel) + 1;
		int available = Def.GetStageNo(mPageData.GetMap(a_courseID).AvailableMaxLevel, 0);
		mMaxAvailablePage = NewMapSsDataEntity.GetMapIndex(available) + 1;
		int moduleLevel = Def.GetStageNo(mPageData.GetMap(a_courseID).ModuleMaxLevel, 0);
		mMaxModulePageFromMaxLevel = NewMapSsDataEntity.GetMapIndex(moduleLevel) + 1;
		bool useBg = true;
		mMapGameObject = new GameObject[mMaxModulePage];
		mMapParts = new SMMapPart[mMaxModulePage];
		for (int m = 0; m < mMaxModulePage; m++)
		{
			mMapGameObject[m] = Util.CreateGameObject("MapGO" + (m + 1), base.gameObject);
			mMapParts[m] = mMapGameObject[m].AddComponent<SMEventLabyrinthPartA>();
			mMapParts[m].SetGameState(mGameState, this, eventPageData);
			SMEventLabyrinthPartA eventPart = mMapParts[m] as SMEventLabyrinthPartA;
			eventPart.SetCourseID(a_courseID);
			Vector3 scale;
			if (!NewMapSsDataEntity.MapScaleList.TryGetValue(m, out scale))
			{
				scale = Vector3.one;
			}
			mMapParts[m].Init(mSeries, m, scale, a_accessoryCB, a_roadBlockCB, a_snsCB, a_mapChangeCB, true, useBg);
			Vector3 pos = new Vector3(0f, 0f + 1136f * (float)m, Def.MAP_BASE_Z - 0.1f * (float)m);
			mMapGameObject[m].gameObject.transform.localPosition = pos;
		}
		asyncList.Clear();
		SMMapRoute<float>.RouteNode n2 = NewMapSsDataEntity.MapRoute.Root;
		SMMapRoute<float>.RouteNode p2 = null;
		bool isFirst2 = true;
		int maxstagenum = 0;
		while (n2 != null)
		{
			if (n2.mIsStage)
			{
				int stageNo2 = (int)n2.Value;
				int mainNo = stageNo2;
				if (maxstagenum < stageNo2)
				{
					maxstagenum = stageNo2;
				}
				stageNo2 = Def.GetStageNo(stageNo2, 0);
				int mapIndex;
				GameObject go = ((!NewMapSsDataEntity.MapRouteMapIndex.TryGetValue(n2, out mapIndex)) ? base.gameObject : mMapGameObject[mapIndex]);
				SMMapPart part = mMapParts[mapIndex];
				if (isFirst2)
				{
					isFirst2 = false;
					string loadanimeKey = part.GetButtonAnimeKey(mSeries, mapIndex);
					for (int j = 0; j < eventPageData.EventCourseList.Count; j++)
					{
						asyncList.Add(ResourceManager.LoadSsAnimationAsync(loadanimeKey + (j + 1), true));
					}
					for (int i = 0; i < asyncList.Count; i++)
					{
						while (asyncList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
						{
							yield return 0;
						}
					}
				}
				Vector3 pos2 = n2.Position;
				pos2.z = Def.MAP_BUTTON_Z;
				EventLabyrinthCourseButton button = CreateStageButton(a_animeKey: part.GetButtonAnimeKey(mSeries, mapIndex) + mainNo, a_stageno: stageNo2, a_pos: pos2, a_parent: go);
				mStageButtonList.Add(button);
				mMapButtons.Add(stageNo2.ToString(), button.gameObject);
			}
			n2 = n2.NextMain;
		}
		if (NewMapSsDataEntity.MapLineList.Count > 0)
		{
			asyncList.Clear();
			isFirst2 = true;
			n2 = NewMapSsDataEntity.MapLineRoute.Root;
			p2 = null;
			while (n2 != null)
			{
				if (n2.Name.ToString().Contains("Line"))
				{
					int stageNo4 = (int)n2.Value;
					if (stageNo4 == maxstagenum)
					{
						n2 = n2.NextMain;
						continue;
					}
					stageNo4 = Def.GetStageNo(stageNo4, 0);
					int mapIndex2;
					GameObject go2 = ((!NewMapSsDataEntity.MapLineRouteMapIndex.TryGetValue(n2, out mapIndex2)) ? base.gameObject : mMapGameObject[mapIndex2]);
					SMMapPart part2 = mMapParts[mapIndex2];
					if (part2 != null)
					{
						string animeKey = part2.GetLineAnimeKey(mSeries, mapIndex2);
						if (isFirst2)
						{
							isFirst2 = false;
							asyncList.Add(ResourceManager.LoadSsAnimationAsync(animeKey, true));
							for (int l = 0; l < asyncList.Count; l++)
							{
								while (asyncList[l].LoadState != ResourceInstance.LOADSTATE.DONE)
								{
									yield return 0;
								}
							}
							asyncList.Clear();
						}
						List<SsMapEntity> list = NewMapSsDataEntity.GetMapLineListInMapIndex(mapIndex2);
						SsMapEntity e = list.Find((SsMapEntity x) => x.AnimeKey.CompareTo(n2.Value.ToString()) == 0);
						EventLabyrinthCourseLine button2 = CreateLine(e.MapOffsetCounter, e.AnimeKey, e.Position, e.Scale, e.Angle, mapIndex2, go2, animeKey);
						mLineList.Add(button2);
						mLineDict.Add(button2.StageNo, button2);
					}
				}
				n2 = n2.NextMain;
			}
		}
		for (int k = 0; k < mMaxModulePage; k++)
		{
			NGUITools.SetActive(mMapGameObject[k].gameObject, false);
		}
		yield return 0;
	}

	protected override IEnumerator LoadRouteData(Def.SERIES a_series, float a_mapSize, int a_eventID = -1, short a_courseID = -1)
	{
		bool useBin = true;
		if (a_series < Def.SERIES.SM_EV)
		{
			useBin = true;
		}
		else if (a_series == Def.SERIES.SM_EV)
		{
			useBin = false;
		}
		if (useBin)
		{
			NewMapSsDataEntity = mGame.GetMapRouteData(a_series, a_eventID, a_courseID, a_mapSize);
			if (NewMapSsDataEntity == null)
			{
				BIJLog.E(string.Concat("ROUTE LOAD ERROR!!!", a_series, ": ", a_eventID, " ", a_courseID));
			}
		}
		if (NewMapSsDataEntity != null)
		{
			yield break;
		}
		List<ResSsAnimation> asyncList = new List<ResSsAnimation>();
		string prefix = Def.SeriesPrefix[mSeries] + a_eventID;
		string routeKey = string.Format("MAP_PAGE_{0}_ANIMATION_{1}", prefix, a_courseID);
		for (int j = 0; j < mMaxModulePage; j++)
		{
			string anime_key = routeKey + "_" + (j + 1);
			ResSsAnimation entityAnime = ResourceManager.LoadSsAnimationAsync(anime_key, true);
			mLoadAsyncList.Add(entityAnime);
		}
		for (int i = 0; i < mLoadAsyncList.Count; i++)
		{
			while (mLoadAsyncList[i].LoadState != ResourceInstance.LOADSTATE.DONE)
			{
				yield return 0;
			}
		}
		NewMapSsDataEntity = new SMMapSsData();
		NewMapSsDataEntity.MapSize(a_mapSize);
		if (!string.IsNullOrEmpty(routeKey))
		{
			NewMapSsDataEntity.Load(mMaxModulePage, mSeries, routeKey);
		}
		else
		{
			NewMapSsDataEntity.Load(mMaxModulePage, mSeries);
		}
	}

	public EventLabyrinthCourseButton CreateStageButton(int a_stageno, Vector3 a_pos, GameObject a_parent, bool a_isSub = false, string a_animeKey = "")
	{
		SMEventPageData sMEventPageData = mPageData as SMEventPageData;
		BIJImage image = ResourceManager.LoadImage(sMEventPageData.EventSetting.MapAtlasKey).Image;
		int a_main;
		int a_sub;
		Def.SplitStageNo(a_stageno, out a_main, out a_sub);
		string text = (a_isSub ? ("Course_" + a_main + "-" + a_sub) : ("Course_" + a_main));
		Player.STAGE_STATUS sTAGE_STATUS = Player.STAGE_STATUS.LOCK;
		string a_animeKey2 = string.Empty;
		if (string.IsNullOrEmpty(a_animeKey))
		{
			string.Format(GameStateEventLabyrinth.FixedSpriteAnimationNameFormat, EventID);
		}
		else
		{
			a_animeKey2 = a_animeKey;
		}
		float scale = 1f;
		PlayerEventData data;
		mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, sMEventPageData.EventID, out data);
		PlayerEventLabyrinthData labyrinthData = data.LabyrinthData;
		sTAGE_STATUS = labyrinthData.CourseStageStatus[(short)a_stageno];
		bool fukidashiDisplayFlg = false;
		if (sTAGE_STATUS == Player.STAGE_STATUS.LOCK && mIsFirstLockStage)
		{
			mIsFirstLockStage = false;
			fukidashiDisplayFlg = true;
		}
		EventLabyrinthCourseButton eventLabyrinthCourseButton = Util.CreateGameObject(text, a_parent).AddComponent<EventLabyrinthCourseButton>();
		eventLabyrinthCourseButton.SetCourseID((short)(a_main - 1));
		eventLabyrinthCourseButton.SetUseCurrentAnime(true);
		eventLabyrinthCourseButton.SetFukidashiDisplayFlg(fukidashiDisplayFlg);
		eventLabyrinthCourseButton.Init(a_pos, scale, a_stageno, 0, sTAGE_STATUS, image, a_animeKey2);
		eventLabyrinthCourseButton.SetGameState(mGameState);
		eventLabyrinthCourseButton.SetPushedCallback(base.OnStageButtonPushed);
		return eventLabyrinthCourseButton;
	}

	public new EventLabyrinthCourseLine CreateLine(int counter, string animeName, Vector3 pos, Vector3 scale, float a_angle, int a_mapIndex, GameObject a_parent, string animeKey)
	{
		SMEventPageData currentEventPageData = GameMain.GetCurrentEventPageData();
		BIJImage image = ResourceManager.LoadImage(currentEventPageData.EventSetting.MapAtlasKey).Image;
		EventLabyrinthCourseLine eventLabyrinthCourseLine = Util.CreateGameObject("MapLine" + counter, a_parent).AddComponent<EventLabyrinthCourseLine>();
		eventLabyrinthCourseLine.SetCourseID(mCourseID);
		eventLabyrinthCourseLine.Init(counter, animeName, image, a_mapIndex, pos.x, pos.y, scale, a_angle, animeKey);
		return eventLabyrinthCourseLine;
	}

	public override IEnumerator ActiveObjectInCamera(float y)
	{
		if (IsActiveObject)
		{
			yield break;
		}
		List<SMMapPart> atvList = new List<SMMapPart>();
		IsActiveObject = true;
		for (int j = 0; j < mMapGameObject.Length; j++)
		{
			bool isNowActive = NGUITools.GetActive(mMapGameObject[j]);
			StartCoroutine(mMapParts[j].Active(NewMapSsDataEntity, j, false));
			atvList.Add(mMapParts[j]);
		}
		while (true)
		{
			bool endflag = true;
			int i = 0;
			for (int imax = atvList.Count; i < imax; i++)
			{
				if (atvList[i].IsActivating)
				{
					endflag = false;
					break;
				}
			}
			if (endflag)
			{
				break;
			}
			yield return null;
		}
		IsActiveObject = false;
	}

	public IEnumerator UnlockCourse(short a_unlock)
	{
		EventLabyrinthCourseButton course = GetStageButton(a_unlock) as EventLabyrinthCourseButton;
		if (course == null)
		{
			BIJLog.E("course is null");
			yield break;
		}
		int linemain;
		int linesub;
		Def.SplitStageNo(a_unlock, out linemain, out linesub);
		int lineNo = Def.GetStageNo(linemain - 1, 0);
		EventLabyrinthCourseLine line = GetLine(lineNo) as EventLabyrinthCourseLine;
		if (line == null)
		{
			BIJLog.E("line is null");
			yield break;
		}
		StartCoroutine(course.UnlockCourse());
		yield return new WaitForSeconds(1.33f);
		yield return StartCoroutine(line.UnlockCourse());
		int nextCourseNo = Def.GetStageNo(linemain + 1, 0);
		EventLabyrinthCourseButton nextCourse = GetStageButton(nextCourseNo) as EventLabyrinthCourseButton;
		if (nextCourse != null && nextCourse.mMode == Player.STAGE_STATUS.LOCK)
		{
			yield return StartCoroutine(nextCourse.OpenFukidashi());
		}
	}
}
