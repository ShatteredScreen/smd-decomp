using System.Collections.Generic;

public sealed class ChanceConditionInfo
{
	[MiniJSONAlias("cid")]
	public int ChanceID { get; set; }

	[MiniJSONAlias("id")]
	public int ID { get; set; }

	[MiniJSONAlias("sp_show")]
	public int SpecialShow { get; set; }

	[MiniJSONAlias("item_id")]
	public List<List<int>> ItemID { get; set; }

	public bool IsSpecialShow
	{
		get
		{
			return SpecialShow != 0;
		}
	}

	public ChanceConditionInfo()
	{
		ID = 0;
		SpecialShow = 0;
		ItemID = new List<List<int>>();
	}
}
