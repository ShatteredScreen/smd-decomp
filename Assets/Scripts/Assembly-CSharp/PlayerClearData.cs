using System;

public class PlayerClearData : SMJsonData, ICloneable
{
	public int Version { get; set; }

	public Def.SERIES Series { get; set; }

	public int StageNo { get; set; }

	public int HightScore { get; set; }

	public int GotStars { get; set; }

	public PlayerClearData()
	{
		Version = 1290;
		Series = Def.SERIES.SM_FIRST;
		StageNo = 0;
		HightScore = 0;
		GotStars = 0;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override bool Equals(object obj)
	{
		PlayerClearData playerClearData = obj as PlayerClearData;
		if (playerClearData == null)
		{
			return false;
		}
		if (playerClearData.Series != Series)
		{
			return false;
		}
		if (playerClearData.HightScore == HightScore && playerClearData.GotStars == GotStars)
		{
			return true;
		}
		return false;
	}

	public PlayerClearData Clone()
	{
		return MemberwiseClone() as PlayerClearData;
	}

	public bool Update(PlayerClearData _rhs)
	{
		if (_rhs == null)
		{
			return false;
		}
		if (_rhs.Series != Series)
		{
			return false;
		}
		bool result = false;
		if (HightScore < _rhs.HightScore)
		{
			HightScore = _rhs.HightScore;
			result = true;
		}
		if (GotStars < _rhs.GotStars)
		{
			GotStars = _rhs.GotStars;
			return true;
		}
		return result;
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(Version);
		data.WriteByte((byte)Series);
		data.WriteInt(StageNo);
		data.WriteInt(HightScore);
		data.WriteInt(GotStars);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		Version = data.ReadInt();
		Series = (Def.SERIES)data.ReadByte();
		StageNo = data.ReadInt();
		HightScore = data.ReadInt();
		GotStars = data.ReadInt();
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerClearData obj = new PlayerClearData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
			Version = obj.Version;
			Series = obj.Series;
			StageNo = obj.StageNo;
			HightScore = obj.HightScore;
			GotStars = obj.GotStars;
		}
		catch (Exception)
		{
		}
	}
}
