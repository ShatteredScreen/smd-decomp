using System;
using System.Collections.Generic;
using UnityEngine;

public class MyPageDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3
	}

	public enum MODE
	{
		STATUS = 0,
		COMPANION = 1,
		SKIN = 2
	}

	public delegate void OnDialogClosed();

	public delegate void OnLoginPushed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private UILabel mTotalPlayTime;

	private GameObject mPenaltyLabel;

	private UILabel mPenaltyTime;

	private MyPageItemList mMyPageItemList;

	private MyPageItem mSelectedItem;

	private UIButton mTabButtonStatus;

	private UIButton mTabButtonCompanion;

	private UIButton mTabButtonSkin;

	private UIButton mAchievements;

	private GameObject mObjectRoot;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mSNSCallCount;

	private PostDialog mPostDialog;

	private OnLoginPushed mLoginCallback;

	public MODE Mode { get; set; }

	public override void Start()
	{
		Mode = MODE.STATUS;
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (Mode == MODE.STATUS)
			{
				UpdateTotalPlayTime();
				UpdatePenaltyTime();
			}
			break;
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				DoSNS(_sns);
			}, null);
			break;
		}
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		string titleDescKey = Localization.Get("MyPage_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UISprite titleBoard = GetTitleBoard();
		titleBoard.spriteName = "88";
		MODE mode = Mode;
		ChangeMode(mode);
		mTabButtonStatus = Util.CreateImageButton("TabA", base.gameObject, image);
		Util.SetImageButtonInfo(mTabButtonStatus, "20", "20", "20", mBaseDepth + 1, new Vector3(-175f, -610f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonStatus, this, "OnStatusTabPushed", UIButtonMessage.Trigger.OnClick);
		mTabButtonCompanion = Util.CreateImageButton("TabB", base.gameObject, image);
		Util.SetImageButtonInfo(mTabButtonCompanion, "17", "17", "17", mBaseDepth + 1, new Vector3(0f, -610f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonCompanion, this, "OnCompanionTabPushed", UIButtonMessage.Trigger.OnClick);
		ActivateTab(mode);
		CreateCancelButton("OnCancelPushed");
	}

	private void UpdateTotalPlayTime()
	{
		int num = (int)mGame.mPlayer.Data.TotalPlayTime;
		int num2 = num / 3600;
		num %= 3600;
		int num3 = num / 60;
		int num4 = num % 60;
		string text = string.Format(Localization.Get("Profile_TimeFormat"), num2, num3, num4);
		mTotalPlayTime.text = text;
	}

	private void UpdatePenaltyTime()
	{
		int num = (int)mGame.mPlayer.Data.TimeCheatPenalty;
		bool active = num > 0;
		mPenaltyLabel.SetActive(active);
		int num2 = num / 3600;
		num %= 3600;
		int num3 = num / 60;
		int num4 = num % 60;
		string text = string.Format(Localization.Get("Profile_TimeFormat"), num2, num3, num4);
		mPenaltyTime.text = text;
	}

	private void ChangeMode(MODE mode)
	{
		Mode = mode;
		if (mObjectRoot != null)
		{
			UnityEngine.Object.Destroy(mObjectRoot.gameObject);
			mObjectRoot = null;
			mMyPageItemList = null;
		}
		mObjectRoot = Util.CreateGameObject("Root", base.gameObject);
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont font = GameMain.LoadFont();
		switch (mode)
		{
		case MODE.STATUS:
			BuildStatus(image, image, font);
			break;
		case MODE.COMPANION:
		{
			List<KeyValuePair<int, byte>> list = new List<KeyValuePair<int, byte>>(mGame.mPlayer.Data.Companions);
			list.Sort(CompareKeyValuePair);
			List<MyPageItem> list2 = new List<MyPageItem>();
			foreach (KeyValuePair<int, byte> item in list)
			{
				MyPageItemList.MakeAddItem(list2, MyPageItem.KIND.COMPANION, mGame.mCompanionData[item.Key]);
			}
			mMyPageItemList = MyPageItemList.Make(mObjectRoot, this, list2, 0f, -300f, mBaseDepth + 10);
			break;
		}
		case MODE.SKIN:
			break;
		}
	}

	private void BuildStatus(BIJImage atlas, BIJImage atlasUI, UIFont font)
	{
		UISprite uISprite = Util.CreateSprite("PersonalData", mObjectRoot, atlas);
		Util.SetSpriteInfo(uISprite, "319", mBaseDepth + 1, new Vector3(0f, -160f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(520, 220);
		UISprite uISprite2 = Util.CreateSprite("Level", uISprite.gameObject, atlasUI);
		Util.SetSpriteInfo(uISprite2, "76", mBaseDepth + 2, new Vector3(-195f, 40f, 0f), Vector3.one, false);
		NumberImageString numberImageString = Util.CreateGameObject("Number", uISprite2.gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(3, mGame.mPlayer.Level, mBaseDepth + 3, 0.8f, UIWidget.Pivot.Center, false, atlas);
		numberImageString.transform.localPosition = new Vector3(0f, -5f, 0f);
		numberImageString.SetColor(Def.DEFAULT_MESSAGE_COLOR);
		UISprite uISprite3 = Util.CreateSprite("Star", uISprite.gameObject, atlasUI);
		Util.SetSpriteInfo(uISprite3, "77", mBaseDepth + 2, new Vector3(200f, 50f, 0f), new Vector3(0.95f, 0.95f, 1f), false);
		NumberImageString numberImageString2 = Util.CreateGameObject("Number", uISprite3.gameObject).AddComponent<NumberImageString>();
		numberImageString2.Init(3, (int)mGame.mPlayer.GetRankingTotalStar(), mBaseDepth + 3, 0.8f, UIWidget.Pivot.Center, false, atlas);
		numberImageString2.transform.localPosition = new Vector3(0f, -10f, 0f);
		numberImageString2.SetColor(Def.DEFAULT_MESSAGE_COLOR);
		UILabel uILabel = Util.CreateLabel("Name", uISprite.gameObject, font);
		string text = ((!string.IsNullOrEmpty(mGame.mOptions.FbName)) ? mGame.mOptions.FbName : "NO NAME");
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 2, new Vector3(40f, 70f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(200, 40);
		UILabel uILabel2 = Util.CreateLabel("TotalScore", uISprite.gameObject, font);
		Util.SetLabelInfo(uILabel2, string.Format(Localization.Get("Profile_OverallScoreFormat"), 0), mBaseDepth + 2, new Vector3(140f, 35f, 0f), 24, 0, 0, UIWidget.Pivot.Right);
		uILabel2.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel2.SetDimensions(200, 30);
		UISprite uISprite4 = Util.CreateSprite("Frame", uISprite.gameObject, atlas);
		Util.SetSpriteInfo(uISprite4, "225", mBaseDepth + 3, new Vector3(-105f, 50f, 0f), Vector3.one, false);
		uISprite4.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
		if (mGame.mPlayer.Icon != null)
		{
			UITexture uITexture = Util.CreateGameObject("Icon", uISprite4.gameObject).AddComponent<UITexture>();
			uITexture.depth = mBaseDepth + 2;
			uITexture.transform.localPosition = Vector3.zero;
			uITexture.transform.localScale = Vector3.one;
			uITexture.mainTexture = mGame.mPlayer.Icon;
		}
		else
		{
			string imageName = "200";
			if (string.Compare("male", mGame.mOptions.Gender, StringComparison.OrdinalIgnoreCase) == 0)
			{
				imageName = "175";
			}
			else if (string.Compare("female", mGame.mOptions.Gender, StringComparison.OrdinalIgnoreCase) == 0)
			{
				imageName = "199";
			}
			UISprite sprite = Util.CreateSprite("Icon", uISprite4.gameObject, atlas);
			Util.SetSpriteInfo(sprite, imageName, mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		}
		UISprite uISprite5 = Util.CreateSprite("Line", uISprite.gameObject, atlas);
		Util.SetSpriteInfo(uISprite5, "171", mBaseDepth + 3, new Vector3(0f, -5f, 0f), Vector3.one, false);
		uISprite5.SetDimensions(465, 10);
		UISprite uISprite6 = Util.CreateSprite("Partner", uISprite.gameObject, atlas);
		Util.SetSpriteInfo(uISprite6, mGame.mCompanionData[mGame.mPlayer.Data.SelectedCompanion].iconName, mBaseDepth + 1, new Vector3(-170f, -50f, 0f), Vector3.one, false);
		uISprite6.transform.localScale = new Vector3(0.6f, 0.6f, 1f);
		UISprite sprite2 = Util.CreateSprite("Selected", uISprite6.gameObject, atlasUI);
		Util.SetSpriteInfo(sprite2, "14", mBaseDepth + 2, new Vector3(0f, -55f, 0f), Vector3.one, false);
		UILabel uILabel3 = Util.CreateLabel("TotalScore", uISprite.gameObject, font);
		Util.SetLabelInfo(uILabel3, mGame.mCompanionData[mGame.mPlayer.Data.SelectedCompanion].name, mBaseDepth + 2, new Vector3(46f, -56f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel3.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel3.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel3.SetDimensions(330, 40);
		UISprite uISprite7 = CreateBoard(mObjectRoot, atlas, font, "PlayData", "Profile_PlayDataTitle", new Vector3(0f, -365f, 0f), mBaseDepth + 1, Color.white, 190);
		float posY = 35f;
		PlayDataLabel playDataLabel = new PlayDataLabel(uISprite7.gameObject, font, "ComboNum", "Profile_ComboNum", "Profile_ComboNumFormat", mGame.mPlayer.Data.MaxCombo, 999L, 0L, ref posY, 27f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		PlayDataLabel playDataLabel2 = new PlayDataLabel(uISprite7.gameObject, font, "HighScore", "Profile_HighScore", "Profile_HighScoreFormat", mGame.mPlayer.Data.MaxScore, 999L, 0L, ref posY, 27f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		PlayDataLabel playDataLabel3 = new PlayDataLabel(uISprite7.gameObject, font, "PlayTime", "Profile_PlayTime", "Profile_TimeFormat", 0L, 0L, 0L, ref posY, 27f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		playDataLabel3.GetScoreLabel(ref mTotalPlayTime);
		UpdateTotalPlayTime();
		PlayDataLabel playDataLabel4 = new PlayDataLabel(uISprite7.gameObject, font, "PlayNum", "Profile_PlayNum", "Profile_PlayNumFormat", mGame.mPlayer.Data.TotalWin + mGame.mPlayer.Data.TotalLose, 0L, 0L, ref posY, 27f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		PlayDataLabel playDataLabel5 = new PlayDataLabel(uISprite7.gameObject, font, "PlayNum", "Profile_RoadBlockTicket", "Profile_RoadBlockTicketFormat", mGame.mPlayer.RoadBlockTicket, 0L, 0L, ref posY, 27f, mBaseDepth + 2, Def.DEFAULT_MESSAGE_COLOR);
		posY -= 10f;
		PlayDataLabel playDataLabel6 = new PlayDataLabel(uISprite7.gameObject, font, "PenaltyTime", "Profile_PenaltyTime", "Profile_TimeFormat", 0L, 0L, 0L, ref posY, 27f, mBaseDepth + 2, Color.red);
		playDataLabel6.GetScoreLabel(ref mPenaltyTime);
		playDataLabel6.GetRootObject(ref mPenaltyLabel);
		UpdatePenaltyTime();
		float y = -525f;
		float x = 0f;
		mAchievements = Util.CreateJellyImageButton("Achievements", mObjectRoot, atlas);
		Util.SetImageButtonInfo(mAchievements, "261", "261", "261", mBaseDepth + 1, new Vector3(x, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(mAchievements, this, "OnAchievementsPushed", UIButtonMessage.Trigger.OnClick);
		UILabel uILabel4 = Util.CreateLabel("GooglePlusDesc", mAchievements.gameObject, font);
		string text2 = Localization.Get("GooglePlus_Achievement");
		Util.SetLabelInfo(uILabel4, text2, mBaseDepth + 2, new Vector3(20f, 0f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel4.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel4.SetDimensions(155, 50);
	}

	private UISprite CreateBoard(GameObject parent, BIJImage atlas, UIFont font, string ObjName, string LocName, Vector3 pos, int depth, Color clr, int height = 160)
	{
		UISprite uISprite = Util.CreateSprite(ObjName, parent, atlas);
		Util.SetSpriteInfo(uISprite, "149", depth, pos, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(520, height);
		uISprite.color = clr;
		UILabel uILabel = Util.CreateLabel("Title", uISprite.gameObject, font);
		Util.SetLabelInfo(uILabel, Localization.Get(LocName), depth + 1, new Vector3(0f, (float)height / 2f - 15f - 15f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.SetDimensions(450, 30);
		return uISprite;
	}

	public void OnPostButtonPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			GameObject parent = base.gameObject.transform.parent.gameObject;
			mPostDialog = Util.CreateGameObject("PostDialog", parent).AddComponent<PostDialog>();
			mPostDialog.SetClosedCallback(OnPostDialogClosed);
			mPostDialog.SetBaseDepth(mBaseDepth + 70);
			mPostDialog.PostType = PostDialog.POST_TYPE.CHARLIE00;
			mState.Reset(STATE.WAIT, true);
		}
	}

	private void OnPostDialogClosed()
	{
		UnityEngine.Object.Destroy(mPostDialog.gameObject);
		mPostDialog = null;
		mState.Change(STATE.MAIN);
	}

	public void SetFacebookLoginCallback(OnLoginPushed callback)
	{
		mLoginCallback = callback;
	}

	public void OnFacebookButtonPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.WAIT && mLoginCallback != null)
		{
			mLoginCallback();
		}
	}

	private static int CompareKeyValuePair(KeyValuePair<int, byte> a, KeyValuePair<int, byte> b)
	{
		return a.Key - b.Key;
	}

	public override void OnOpenFinished()
	{
	}

	public override void Close()
	{
		base.Close();
		if (mMyPageItemList != null)
		{
			GameMain.SafeDestroy(mMyPageItemList.gameObject);
			mMyPageItemList = null;
		}
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void ActivateTab(MODE mode)
	{
		switch (mode)
		{
		case MODE.STATUS:
			Util.SetImageButtonGraphic(mTabButtonStatus, "20", "20", "20");
			Util.SetImageButtonGraphic(mTabButtonCompanion, "17", "17", "17");
			mTabButtonStatus.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 1;
			mTabButtonCompanion.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			break;
		case MODE.COMPANION:
			Util.SetImageButtonGraphic(mTabButtonStatus, "21", "21", "21");
			Util.SetImageButtonGraphic(mTabButtonCompanion, "16", "16", "16");
			mTabButtonStatus.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			mTabButtonCompanion.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 1;
			break;
		case MODE.SKIN:
			Util.SetImageButtonGraphic(mTabButtonStatus, "21", "21", "21");
			Util.SetImageButtonGraphic(mTabButtonCompanion, "17", "17", "17");
			mTabButtonStatus.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			mTabButtonCompanion.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			break;
		}
	}

	public void OnStatusTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && Mode != 0)
		{
			mGame.PlaySe("SE_PUSH", -1);
			MODE mode = MODE.STATUS;
			ActivateTab(mode);
			ChangeMode(mode);
		}
	}

	public void OnCompanionTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && Mode != MODE.COMPANION)
		{
			mGame.PlaySe("SE_PUSH", -1);
			MODE mode = MODE.COMPANION;
			ActivateTab(mode);
			ChangeMode(mode);
		}
	}

	public void OnSkinTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && Mode != MODE.SKIN)
		{
			mGame.PlaySe("SE_PUSH", -1);
			MODE mode = MODE.SKIN;
			ActivateTab(mode);
			ChangeMode(mode);
		}
	}

	public void OnCompanionChangePushed(GameObject go)
	{
		if (GetBusy())
		{
			return;
		}
		MyPageItem myPageItem = mMyPageItemList.FindListItem(go);
		if (myPageItem == null || mGame.mPlayer.Data.SelectedCompanion == myPageItem.companionData.index)
		{
			return;
		}
		mGame.PlaySe("SE_RECEIVE_GIFT", -1);
		mGame.mPlayer.Data.SelectedCompanion = myPageItem.companionData.index;
		foreach (MyPageItem item in mMyPageItemList.items)
		{
			if (item.companionData.index == myPageItem.companionData.index)
			{
				item.SetSelect(true);
			}
			else
			{
				item.SetSelect(false);
			}
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void OnAchievementsPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			BIJSNS sns = SocialManager.Instance[SNS.GooglePlus];
			mSNSCallCount = 0;
			DoSNS(sns);
		}
	}

	private void DoSNS(BIJSNS _sns)
	{
		mSNSCallCount++;
		try
		{
			mState.Change(STATE.WAIT);
			mSNS = _sns;
			if (_sns.IsOpEnabled(2) && !_sns.IsLogined())
			{
				if (mSNSCallCount > 1)
				{
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.NETWORK_LOGIN00);
				}
			}
			else if (!GameMain.ShowAchievements(_sns, delegate
			{
				mState.Change(STATE.MAIN);
			}, null))
			{
				mState.Change(STATE.MAIN);
			}
		}
		catch (Exception)
		{
			mState.Change(STATE.MAIN);
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		EnableList(false);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void EnableList(bool enabled)
	{
	}
}
