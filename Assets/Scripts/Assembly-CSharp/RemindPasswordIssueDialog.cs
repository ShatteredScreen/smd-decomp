using System.Collections;
using UnityEngine;

public class RemindPasswordIssueDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private BIJImage mTitleAtlas;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public void Init(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public override IEnumerator BuildResource()
	{
		ResImage resTitleImage = ResourceManager.LoadImageAsync("TITLE");
		while (resTitleImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mTitleAtlas = resTitleImage.Image;
	}

	public override void BuildDialog()
	{
		base.BuildDialog();
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.LARGE, null, null, 0, 450);
		InitDialogTitle(Localization.Get("Transfer_RemindPassword_Title"));
		UILabel uILabel = Util.CreateLabel("Desc02", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Transfer_RemindPassword_Desc02"), mBaseDepth + 1, new Vector3(0f, 90f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.spacingY = 8;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.color = new Color32(198, 0, 0, byte.MaxValue);
		uILabel.SetDimensions(560, 200);
		uILabel = Util.CreateLabel("Desc03", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Transfer_RemindPassword_Desc03"), mBaseDepth + 1, new Vector3(0f, -40f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.spacingY = 8;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.SetDimensions(560, 200);
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, mTitleAtlas);
		Util.SetImageButtonInfo(button, "LC_button_issue", "LC_button_issue", "LC_button_issue", mBaseDepth + 4, new Vector3(0f, -150f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}
}
