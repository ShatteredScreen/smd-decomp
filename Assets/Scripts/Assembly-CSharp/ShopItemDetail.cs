using System.Collections.Generic;

public class ShopItemDetail
{
	public enum ItemKind
	{
		NONE = 0,
		WAVE_BOMB = 1,
		TAP_BOMB2 = 2,
		RAINBOW = 3,
		ADD_SCORE = 4,
		SKILL_CHARGE = 5,
		SELECT_ONE = 6,
		SELECT_COLLECT = 7,
		COLOR_CRUSH = 8,
		BLOCK_CRUSH = 9,
		MUGEN_HEART15 = 10,
		MUGEN_HEART30 = 11,
		MUGEN_HEART60 = 12,
		PAIR_MAKER = 13,
		ALL_CRUSH = 14,
		HEART = 15,
		CONTINUE = 16,
		ROADBLOCK = 17,
		STAGESKIP = 18,
		GROWUP = 19,
		PREMIUM_TICKET = 20,
		PREMIUM_SUB_TICKET = 21,
		SCORE_UP_RATIO_1_5 = 22,
		SCORE_UP_RATIO_2_0 = 23,
		SCORE_UP_RATIO_2_5 = 24,
		SCORE_UP_RATIO_3_0 = 25,
		CONTINUE_UP_M2_T6 = 26,
		STAR_GET = 27
	}

	public int NONE { get; set; }

	[MiniJSONAlias("stripe_popping")]
	public int WAVE_BOMB { get; set; }

	[MiniJSONAlias("lunaP_double")]
	public int TAP_BOMB2 { get; set; }

	[MiniJSONAlias("rainbow")]
	public int RAINBOW { get; set; }

	[MiniJSONAlias("score")]
	public int ADD_SCORE { get; set; }

	[MiniJSONAlias("skill")]
	public int SKILL_CHARGE { get; set; }

	[MiniJSONAlias("piece_crush")]
	public int SELECT_ONE { get; set; }

	[MiniJSONAlias("itemcatch")]
	public int SELECT_COLLECT { get; set; }

	[MiniJSONAlias("color_crush")]
	public int COLOR_CRUSH { get; set; }

	[MiniJSONAlias("block_crush")]
	public int BLOCK_CRUSH { get; set; }

	[MiniJSONAlias("infinity_heart15")]
	public int MUGEN_HEART15 { get; set; }

	[MiniJSONAlias("infinity_heart30")]
	public int MUGEN_HEART30 { get; set; }

	[MiniJSONAlias("infinity_heart60")]
	public int MUGEN_HEART60 { get; set; }

	[MiniJSONAlias("heart")]
	public int HEART { get; set; }

	[MiniJSONAlias("continue")]
	public int CONTINUE { get; set; }

	[MiniJSONAlias("roadblock")]
	public int ROADBLOCK { get; set; }

	[MiniJSONAlias("stageskip")]
	public int STAGESKIP { get; set; }

	[MiniJSONAlias("growup")]
	public int GROWUP { get; set; }

	[MiniJSONAlias("pair_maker")]
	public int PAIR_MAKER { get; set; }

	[MiniJSONAlias("premium_ticket")]
	public int PREMIUM_TICKET { get; set; }

	[MiniJSONAlias("premium_sub_ticket")]
	public int PREMIUM_SUB_TICKET { get; set; }

	[MiniJSONAlias("all_crush")]
	public int ALL_CRUSH { get; set; }

	[MiniJSONAlias("score_chance15")]
	public int SCORE_UP_RATIO_1_5 { get; set; }

	[MiniJSONAlias("score_chance20")]
	public int SCORE_UP_RATIO_2_0 { get; set; }

	[MiniJSONAlias("score_chance25")]
	public int SCORE_UP_RATIO_2_5 { get; set; }

	[MiniJSONAlias("score_chance30")]
	public int SCORE_UP_RATIO_3_0 { get; set; }

	[MiniJSONAlias("doubleup_chance20")]
	public int DOUBLE_UP_RATIO_2_0 { get; set; }

	[MiniJSONAlias("continue_chance2_6")]
	public int CONTINUE_UP_M2_T6 { get; set; }

	[MiniJSONAlias("starget")]
	public int STAR_GET { get; set; }

	public Dictionary<ItemKind, int> BundleItems { get; protected set; }

	public ShopItemDetail()
	{
		BundleItems = new Dictionary<ItemKind, int>();
	}

	public bool IsSetBundle()
	{
		return BundleItems.Count > 1;
	}

	public void SetItem()
	{
		if (WAVE_BOMB > 0)
		{
			BundleItems.Add(ItemKind.WAVE_BOMB, WAVE_BOMB);
		}
		if (TAP_BOMB2 > 0)
		{
			BundleItems.Add(ItemKind.TAP_BOMB2, TAP_BOMB2);
		}
		if (RAINBOW > 0)
		{
			BundleItems.Add(ItemKind.RAINBOW, RAINBOW);
		}
		if (ADD_SCORE > 0)
		{
			BundleItems.Add(ItemKind.ADD_SCORE, ADD_SCORE);
		}
		if (SKILL_CHARGE > 0)
		{
			BundleItems.Add(ItemKind.SKILL_CHARGE, SKILL_CHARGE);
		}
		if (SELECT_ONE > 0)
		{
			BundleItems.Add(ItemKind.SELECT_ONE, SELECT_ONE);
		}
		if (SELECT_COLLECT > 0)
		{
			BundleItems.Add(ItemKind.SELECT_COLLECT, SELECT_COLLECT);
		}
		if (COLOR_CRUSH > 0)
		{
			BundleItems.Add(ItemKind.COLOR_CRUSH, COLOR_CRUSH);
		}
		if (HEART > 0)
		{
			BundleItems.Add(ItemKind.HEART, HEART);
		}
		if (GROWUP > 0)
		{
			BundleItems.Add(ItemKind.GROWUP, GROWUP);
		}
		if (MUGEN_HEART15 > 0)
		{
			BundleItems.Add(ItemKind.MUGEN_HEART15, MUGEN_HEART15);
		}
		if (MUGEN_HEART30 > 0)
		{
			BundleItems.Add(ItemKind.MUGEN_HEART30, MUGEN_HEART30);
		}
		if (MUGEN_HEART60 > 0)
		{
			BundleItems.Add(ItemKind.MUGEN_HEART60, MUGEN_HEART60);
		}
		if (BLOCK_CRUSH > 0)
		{
			BundleItems.Add(ItemKind.BLOCK_CRUSH, BLOCK_CRUSH);
		}
		if (PAIR_MAKER > 0)
		{
			BundleItems.Add(ItemKind.PAIR_MAKER, PAIR_MAKER);
		}
		if (PREMIUM_TICKET > 0)
		{
			BundleItems.Add(ItemKind.PREMIUM_TICKET, PREMIUM_TICKET);
		}
		if (PREMIUM_SUB_TICKET > 0)
		{
			BundleItems.Add(ItemKind.PREMIUM_SUB_TICKET, PREMIUM_SUB_TICKET);
		}
		if (ALL_CRUSH > 0)
		{
			BundleItems.Add(ItemKind.ALL_CRUSH, ALL_CRUSH);
		}
		if (SCORE_UP_RATIO_1_5 > 0)
		{
			BundleItems.Add(ItemKind.SCORE_UP_RATIO_1_5, SCORE_UP_RATIO_1_5);
		}
		if (SCORE_UP_RATIO_2_0 > 0)
		{
			BundleItems.Add(ItemKind.SCORE_UP_RATIO_2_0, SCORE_UP_RATIO_2_0);
		}
		if (SCORE_UP_RATIO_2_5 > 0)
		{
			BundleItems.Add(ItemKind.SCORE_UP_RATIO_2_5, SCORE_UP_RATIO_2_5);
		}
		if (SCORE_UP_RATIO_3_0 > 0)
		{
			BundleItems.Add(ItemKind.SCORE_UP_RATIO_3_0, SCORE_UP_RATIO_3_0);
		}
		if (CONTINUE_UP_M2_T6 > 0)
		{
			BundleItems.Add(ItemKind.CONTINUE_UP_M2_T6, CONTINUE_UP_M2_T6);
		}
		if (STAR_GET > 0)
		{
			BundleItems.Add(ItemKind.STAR_GET, STAR_GET);
		}
	}

	public void SetDefault(int a_default)
	{
		List<ItemKind> list = new List<ItemKind>(BundleItems.Keys);
		if (list.Count == 1)
		{
			BundleItems[list[0]] = a_default;
		}
	}

	public bool IsBoosterInclude(Def.BOOSTER_KIND a_kind)
	{
		bool result = false;
		switch (a_kind)
		{
		case Def.BOOSTER_KIND.ADD_SCORE:
			if (BundleItems.ContainsKey(ItemKind.ADD_SCORE) && BundleItems[ItemKind.ADD_SCORE] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.BLOCK_CRUSH:
			if (BundleItems.ContainsKey(ItemKind.BLOCK_CRUSH) && BundleItems[ItemKind.BLOCK_CRUSH] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.COLOR_CRUSH:
			if (BundleItems.ContainsKey(ItemKind.COLOR_CRUSH) && BundleItems[ItemKind.COLOR_CRUSH] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.ALL_CRUSH:
			if (BundleItems.ContainsKey(ItemKind.ALL_CRUSH) && BundleItems[ItemKind.ALL_CRUSH] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.PAIR_MAKER:
			if (BundleItems.ContainsKey(ItemKind.PAIR_MAKER) && BundleItems[ItemKind.PAIR_MAKER] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.RAINBOW:
			if (BundleItems.ContainsKey(ItemKind.RAINBOW) && BundleItems[ItemKind.RAINBOW] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.SELECT_COLLECT:
			if (BundleItems.ContainsKey(ItemKind.SELECT_COLLECT) && BundleItems[ItemKind.SELECT_COLLECT] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.SELECT_ONE:
			if (BundleItems.ContainsKey(ItemKind.SELECT_ONE) && BundleItems[ItemKind.SELECT_ONE] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.SKILL_CHARGE:
			if (BundleItems.ContainsKey(ItemKind.SKILL_CHARGE) && BundleItems[ItemKind.SKILL_CHARGE] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.TAP_BOMB2:
			if (BundleItems.ContainsKey(ItemKind.TAP_BOMB2) && BundleItems[ItemKind.TAP_BOMB2] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.WAVE_BOMB:
			if (BundleItems.ContainsKey(ItemKind.WAVE_BOMB) && BundleItems[ItemKind.WAVE_BOMB] > 0)
			{
				result = true;
			}
			break;
		}
		return result;
	}

	public bool IsMugenHeart(Def.BOOSTER_KIND a_kind)
	{
		bool result = false;
		switch (a_kind)
		{
		case Def.BOOSTER_KIND.MUGEN_HEART15:
			if (BundleItems.ContainsKey(ItemKind.MUGEN_HEART15) && BundleItems[ItemKind.MUGEN_HEART15] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.MUGEN_HEART30:
			if (BundleItems.ContainsKey(ItemKind.MUGEN_HEART30) && BundleItems[ItemKind.MUGEN_HEART30] > 0)
			{
				result = true;
			}
			break;
		case Def.BOOSTER_KIND.MUGEN_HEART60:
			if (BundleItems.ContainsKey(ItemKind.MUGEN_HEART60) && BundleItems[ItemKind.MUGEN_HEART60] > 0)
			{
				result = true;
			}
			break;
		}
		return result;
	}
}
