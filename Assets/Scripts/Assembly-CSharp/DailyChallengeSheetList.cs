using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/DailyChallenge/Sheet List")]
public class DailyChallengeSheetList : ScriptableObject
{
	[SerializeField]
	private List<DailyChallengeSheet> mSheet;

	private List<DailyChallengeSheet> Sheet
	{
		get
		{
			if (mSheet == null)
			{
				mSheet = new List<DailyChallengeSheet>();
			}
			return mSheet;
		}
	}

	public string[] SheetNames
	{
		get
		{
			return (Sheet == null) ? null : (from n in Sheet
				where !string.IsNullOrEmpty(n.SheetName)
				select n.SheetName).ToArray();
		}
	}

	public void AddSheet(DailyChallengeSheet sheet)
	{
		Sheet.Add(sheet);
	}

	public DailyChallengeSheet GetSheet(string sheet_name)
	{
		return Sheet.FirstOrDefault((DailyChallengeSheet n) => n.SheetName == sheet_name);
	}
}
