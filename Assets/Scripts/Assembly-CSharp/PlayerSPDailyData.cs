using System;

public class PlayerSPDailyData : SMJsonData, ICloneable
{
	public int Version { get; set; }

	public int SPDailyID { get; set; }

	public int Num { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		data.WriteInt(SPDailyID);
		data.WriteInt(Num);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		short num = 1040;
		if (reqVersion >= 1050)
		{
			num = data.ReadShort();
		}
		SPDailyID = data.ReadInt();
		Num = data.ReadInt();
	}

	public override string SerializeToJson()
	{
		return string.Empty;
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerMapData obj = new PlayerMapData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public PlayerEventData Clone()
	{
		return MemberwiseClone() as PlayerEventData;
	}
}
