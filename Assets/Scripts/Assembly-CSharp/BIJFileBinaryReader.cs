using System;
using System.IO;
using System.Text;

public class BIJFileBinaryReader : BIJBinaryReader
{
	private FileStream mStream;

	private bool mCloseOnDelete;

	public BIJFileBinaryReader(string path)
		: this(path, Encoding.UTF8, false)
	{
	}

	public BIJFileBinaryReader(string path, Encoding encoding)
		: this(path, encoding, false)
	{
	}

	public BIJFileBinaryReader(string path, Encoding encoding, bool closeOnDelete)
	{
		mStream = null;
		mCloseOnDelete = closeOnDelete;
		Reader = null;
		try
		{
			mPath = path;
			mStream = new FileStream(mPath, FileMode.Open);
			Reader = new BinaryReader(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	public override void Close()
	{
		base.Close();
		if (mStream != null)
		{
			try
			{
				mStream.Close();
			}
			catch (Exception)
			{
			}
			mStream = null;
		}
		if (!mCloseOnDelete)
		{
			return;
		}
		try
		{
			if (!string.IsNullOrEmpty(mPath) && File.Exists(mPath))
			{
				File.Delete(mPath);
			}
		}
		catch (Exception)
		{
		}
	}
}
