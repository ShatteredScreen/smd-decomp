using UnityEngine;

public class SkillMessageDialog : DialogBase
{
	public delegate void OnDialogClosed(bool cancel);

	private OnDialogClosed mCallback;

	private string mName;

	private string mDesc;

	private bool mCanceled;

	private PuzzleTile mSkillBall;

	private GameObject mAreaNaviRoot;

	private ByteVector2[] mArea;

	private Def.SKILL_TYPE mStartType;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
		}
	}

	public override void BuildDialog()
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.SMALL, "DIALOG_BASE", "instruction_panel11", 430);
		UILabel uILabel = Util.CreateLabel("Name", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, mName, mBaseDepth + 3, new Vector3(0f, 54f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		int num = mName.Split('\n').Length;
		uILabel.SetDimensions(380, num * 22);
		uILabel.color = new Color(66f / 85f, 0f, 0f, 1f);
		uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, mDesc, mBaseDepth + 3, new Vector3(0f, 0f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
		uILabel.SetDimensions(420, 52);
		uILabel.color = new Color(0.2509804f, 0.20784314f, 6f / 85f, 1f);
		if (mStartType == Def.SKILL_TYPE.DEFAULT)
		{
			float num2 = -60f;
			num2 = -67f;
			UIButton uIButton = Util.CreateJellyImageButton("Button", base.gameObject, resImage.Image);
			Util.SetImageButtonInfo(uIButton, "LC_button_invocation", "LC_button_invocation", "LC_button_invocation", mBaseDepth + 3, new Vector3(0f, num2, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnUsePushed", UIButtonMessage.Trigger.OnClick);
			if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.LEVEL9_4)
			{
				mGame.mTutorialManager.AddAllowedButton(uIButton.gameObject);
				mGame.mTutorialManager.SetTutorialFinger(base.gameObject, uIButton.gameObject, Def.DIR.NONE);
			}
		}
		CreateCancelButton("OnCancelPushed");
		mAreaNaviRoot = Util.CreateGameObject("AreaNaviRoot", mSkillBall.Sprite.transform.parent.gameObject);
		if (mArea != null)
		{
			for (int i = 0; i < mArea.Length; i++)
			{
				Vector3 pos = mSkillBall.Sprite.transform.localPosition + new Vector3(Def.TILE_PITCH_H * (float)mArea[i].x, Def.TILE_PITCH_V * (float)mArea[i].y, -2f);
				Util.CreateLoopAnime("Area", "EFFECT_SKILL_AREA_NAVI", mAreaNaviRoot, pos, Vector3.one, 0f, 0f, false, null);
			}
		}
		SetLayout(Util.ScreenOrientation);
	}

	public void Init(string name, string desc, PuzzleTile skillBall, ByteVector2[] area, Def.SKILL_TYPE startType)
	{
		mName = name;
		mDesc = desc;
		mCanceled = false;
		mSkillBall = skillBall;
		mArea = area;
		mStartType = startType;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		SetLayout(o);
	}

	private void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			base.gameObject.transform.localPosition = new Vector3(0f, 200f, 0f);
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			Vector2 vector = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, o);
			base.gameObject.transform.localPosition = new Vector3(-348f, vector.y + 94f, 0f);
			break;
		}
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mCanceled);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mCanceled = true;
			Object.Destroy(mAreaNaviRoot);
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void OnUsePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mCanceled = false;
			Object.Destroy(mAreaNaviRoot);
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
