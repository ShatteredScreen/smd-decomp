public interface IStageInfo
{
	int ClearScore { get; }

	int ClearTime { get; }

	byte[] GetMetaData();
}
