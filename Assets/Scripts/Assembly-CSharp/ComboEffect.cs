using System;
using UnityEngine;

public class ComboEffect : MonoBehaviour
{
	private enum STATE
	{
		HIDE = 0,
		UP = 1,
		STAY = 2,
		FIX = 3
	}

	private enum CORNER
	{
		TL = 0,
		TR = 1,
		BR = 2,
		BL = 3
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.HIDE);

	private UISprite mTitle;

	private UISprite[] mNum = new UISprite[3];

	private static int NUM_WIDTH = 36;

	private int mNumBuf;

	private float mSizeProgression;

	private float mAngle;

	private float mFixStayTimer;

	private ParticleSystem mPS;

	private float mPSAngle;

	private float mPSAngleDelta = 1f;

	private GameObject mAnchorT;

	private GameObject mAnchorB;

	private GameObject[] mAnchors;

	private static readonly string[] NumNameTbl = new string[10] { "combo0", "combo1", "combo2", "combo3", "combo4", "combo5", "combo6", "combo7", "combo8", "combo9" };

	private void Awake()
	{
		GameObject particle = GameMain.GetParticle("Particles/ComboParticle");
		particle.transform.parent = base.transform;
		particle.layer = base.gameObject.layer;
		particle.transform.localScale = Vector3.one;
		mPS = particle.GetComponent<ParticleSystem>();
		mPS.Stop();
	}

	private void Start()
	{
		int num = 49;
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_HUD");
		mTitle = Util.CreateSprite("Combo", base.gameObject, resImage.Image);
		Util.SetSpriteInfo(mTitle, "combo_text", num + 1, new Vector3(-80f, 0f, 0f), Vector3.one, false);
		mTitle.enabled = false;
		mTitle.autoResizeBoxCollider = false;
		for (int i = 0; i < 3; i++)
		{
			mNum[i] = Util.CreateSprite("Num" + i, base.gameObject, resImage.Image);
			Util.SetSpriteInfo(mNum[i], "115", num + 1, new Vector3(92f - (float)i * 36f, 0f, 0f), Vector3.one, false);
			mNum[i].enabled = false;
			mNum[i].autoResizeBoxCollider = false;
		}
		mNumBuf = 999;
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.UP:
		{
			mSizeProgression += Time.deltaTime * 720f;
			if (mSizeProgression > 360f)
			{
				mSizeProgression = 360f;
				mState.Change(STATE.STAY);
			}
			float num = 0.5f;
			if (mSizeProgression > 180f)
			{
				num = 0.1f;
			}
			float num2 = Mathf.Sin(mSizeProgression % 180f * ((float)Math.PI / 180f)) * num;
			base.transform.localScale = new Vector3(1f + num2, 1f + num2, 1f);
			break;
		}
		case STATE.FIX:
			mFixStayTimer -= Time.deltaTime;
			if (mFixStayTimer < 0f)
			{
				ChangeNumber(0);
				mState.Change(STATE.HIDE);
			}
			break;
		}
		mState.Update();
	}

	private void ChangeNumber(int num)
	{
		if (num > 999)
		{
			num = 999;
		}
		int num2 = num;
		int num3 = 0;
		int num4 = mTitle.width;
		while (num2 > 0)
		{
			Util.SetSpriteImageName(mNum[num3], NumNameTbl[num2 % 10], Vector3.one, false);
			num3++;
			num4 += NUM_WIDTH;
			num2 /= 10;
		}
		int num5 = -num4 / 2;
		int num6 = num5 + num4;
		mTitle.transform.localPosition = new Vector3(num5 + mTitle.width / 2, 0f, 0f);
		mTitle.enabled = false;
		mNum[0].enabled = false;
		mNum[1].enabled = false;
		mNum[2].enabled = false;
		if (num > 1)
		{
			for (int i = 0; i < num3; i++)
			{
				mNum[i].enabled = true;
				mNum[i].transform.localPosition = new Vector3(num6 - NUM_WIDTH / 2, 0f, 0f);
				num6 -= NUM_WIDTH;
			}
			if (num3 > 0)
			{
				mTitle.enabled = true;
			}
		}
	}

	public void OnCombo(int comboNum)
	{
		ChangeNumber(comboNum);
		if (comboNum > 1)
		{
			mSizeProgression = 0f;
			mAngle = UnityEngine.Random.Range(-20, 20);
			base.transform.localRotation = Quaternion.Euler(0f, 0f, mAngle);
			mPS.Emit(Mathf.Clamp(mNumBuf, 0, 8) * 2);
			mState.Change(STATE.UP);
		}
	}

	public void OnComboFinished()
	{
		mFixStayTimer = 1.5f;
		mState.Change(STATE.FIX);
	}
}
