using UnityEngine;
using UnityEngine.Rendering;

public class SimpleTexture : MonoBehaviour
{
	public enum PIVOT
	{
		CENTER = 0,
		TOP = 1,
		TOPRIGHT = 2,
		RIGHT = 3,
		BOTTOMRIGHT = 4,
		BOTTOM = 5,
		BOTTOMLEFT = 6,
		LEFT = 7,
		TOPLEFT = 8
	}

	protected PIVOT mPivot;

	protected Mesh mMesh;

	protected Material mMaterial;

	protected MeshRenderer mMeshRenderer;

	protected MeshFilter mMeshFilter;

	public Texture mTexture;

	public Rect mUv;

	public Vector2 mSize;

	protected Color mInitialColor = Color.white;

	private bool mDisableStart;

	protected virtual void Start()
	{
		if (mMeshFilter == null)
		{
			mMeshFilter = base.gameObject.AddComponent<MeshFilter>();
		}
		if (mMeshRenderer == null)
		{
			mMeshRenderer = base.gameObject.AddComponent<MeshRenderer>();
		}
		mMesh = new Mesh();
		mMaterial = new Material(GameMain.DefaultShader);
		mMeshFilter.sharedMesh = mMesh;
		mMeshRenderer.sharedMaterial = mMaterial;
		mMeshRenderer.sharedMaterial.mainTexture = mTexture;
		mMeshRenderer.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRenderer.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		mMeshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		mMeshRenderer.shadowCastingMode = ShadowCastingMode.Off;
		SetTexture();
		if (mDisableStart)
		{
			base.gameObject.SetActive(false);
		}
	}

	protected virtual void SetTexture()
	{
		if (mMesh != null)
		{
			Object.Destroy(mMesh);
		}
		mMesh = new Mesh();
		mMaterial = new Material(GameMain.DefaultShader);
		mMeshFilter.sharedMesh = mMesh;
		mMeshRenderer.sharedMaterial = mMaterial;
		mMeshRenderer.sharedMaterial.mainTexture = mTexture;
		mMeshRenderer.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRenderer.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		mMeshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		mMeshRenderer.shadowCastingMode = ShadowCastingMode.Off;
		UpdateVertices();
	}

	protected void UpdateVertices()
	{
		Vector3[] array = new Vector3[4];
		Vector2[] array2 = new Vector2[4];
		int[] array3 = new int[6];
		Color32[] array4 = new Color32[4];
		float x = 0f;
		float y = 0f;
		float x2 = 1f;
		float y2 = 1f;
		switch (mPivot)
		{
		case PIVOT.CENTER:
			x = (0f - mSize.x) / 2f;
			x2 = mSize.x / 2f;
			y2 = (0f - mSize.y) / 2f;
			y = mSize.y / 2f;
			break;
		case PIVOT.TOP:
			x = (0f - mSize.x) / 2f;
			x2 = mSize.x / 2f;
			y2 = 0f - mSize.y;
			y = 0f;
			break;
		case PIVOT.TOPRIGHT:
			x = 0f - mSize.x;
			x2 = 0f;
			y2 = 0f - mSize.y;
			y = 0f;
			break;
		case PIVOT.RIGHT:
			x = 0f - mSize.x;
			x2 = 0f;
			y2 = (0f - mSize.y) / 2f;
			y = mSize.y / 2f;
			break;
		case PIVOT.BOTTOMRIGHT:
			x = 0f - mSize.x;
			x2 = 0f;
			y2 = 0f;
			y = mSize.y;
			break;
		case PIVOT.BOTTOM:
			x = (0f - mSize.x) / 2f;
			x2 = mSize.x / 2f;
			y2 = 0f;
			y = mSize.y;
			break;
		case PIVOT.BOTTOMLEFT:
			x = 0f;
			x2 = mSize.x;
			y2 = 0f;
			y = mSize.y;
			break;
		case PIVOT.LEFT:
			x = 0f;
			x2 = mSize.x;
			y2 = (0f - mSize.y) / 2f;
			y = mSize.y / 2f;
			break;
		case PIVOT.TOPLEFT:
			x = 0f;
			x2 = mSize.x;
			y2 = 0f - mSize.y;
			y = 0f;
			break;
		}
		array[0] = new Vector3(x, y2, 0f);
		array[1] = new Vector3(x2, y2, 0f);
		array[2] = new Vector3(x, y, 0f);
		array[3] = new Vector3(x2, y, 0f);
		float xMin = mUv.xMin;
		float yMax = mUv.yMax;
		float xMax = mUv.xMax;
		float yMin = mUv.yMin;
		array2[0] = new Vector2(xMin, yMin);
		array2[1] = new Vector2(xMax, yMin);
		array2[2] = new Vector2(xMin, yMax);
		array2[3] = new Vector2(xMax, yMax);
		array3[0] = 2;
		array3[1] = 1;
		array3[2] = 0;
		array3[3] = 2;
		array3[4] = 3;
		array3[5] = 1;
		array4[0] = mInitialColor;
		array4[1] = mInitialColor;
		array4[2] = mInitialColor;
		array4[3] = mInitialColor;
		mMesh.vertices = array;
		mMesh.uv = array2;
		mMesh.triangles = array3;
		mMesh.colors32 = array4;
	}

	protected virtual void OnDestroy()
	{
		if (mMeshFilter != null && mMeshFilter.mesh != null)
		{
			Object.Destroy(mMeshFilter.mesh);
		}
		if (mMeshRenderer != null && mMeshRenderer.sharedMaterial != null)
		{
			Object.Destroy(mMeshRenderer.sharedMaterial);
		}
		if (mMesh != null)
		{
			Object.Destroy(mMesh);
			mMesh = null;
		}
		if (mMaterial != null)
		{
			Object.Destroy(mMaterial);
			mMaterial = null;
		}
		mTexture = null;
	}

	protected virtual void Update()
	{
	}

	public void Init(Texture tex, Vector2 size, Rect uv, PIVOT pivot, bool disableStart = false)
	{
		mTexture = tex;
		mSize = size;
		mUv = uv;
		mPivot = pivot;
		mDisableStart = disableStart;
	}

	public void Init(BIJImage image, string key, PIVOT pivot, bool disableStart = false)
	{
		Vector2 pixelSize = image.GetPixelSize(key);
		Vector4 pixelPadding = image.GetPixelPadding(key);
		Vector2 offset = image.GetOffset(key);
		Vector2 size = image.GetSize(key);
		mTexture = image.Texture;
		mSize = pixelSize;
		mPivot = pivot;
		float x = offset.x;
		float y = offset.y;
		float x2 = size.x;
		float y2 = size.y;
		mUv = new Rect(x, y, x2, y2);
		mDisableStart = disableStart;
	}

	public void SetInitialColor(Color col)
	{
		mInitialColor = col;
	}

	public void SetColor(Color col)
	{
		if (!(mMesh == null))
		{
			Color[] colors = mMesh.colors;
			for (int i = 0; i < colors.Length; i++)
			{
				colors[i] = col;
			}
			mMesh.colors = colors;
		}
	}

	public void Resize(Vector2 size, Rect uv, PIVOT pivot)
	{
		mSize = size;
		mUv = uv;
		mPivot = pivot;
		UpdateVertices();
	}
}
