public enum SsKeyAttr
{
	PosX = 0,
	PosY = 1,
	Angle = 2,
	ScaleX = 3,
	ScaleY = 4,
	Trans = 5,
	Prio = 6,
	FlipH = 7,
	FlipV = 8,
	Hide = 9,
	PartsCol = 10,
	PartsPal = 11,
	Vertex = 12,
	User = 13,
	Sound = 14,
	ImageOffsetX = 15,
	ImageOffsetY = 16,
	ImageOffsetW = 17,
	ImageOffsetH = 18,
	OriginOffsetX = 19,
	OriginOffsetY = 20,
	Num = 21
}
