using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

public static class StringUtils
{
	public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
	{
		foreach (T item in items)
		{
			action(item);
		}
	}

	public static string ToString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string keyValueSeparator, string sequenceSeparator)
	{
		if (dictionary == null || dictionary.Count < 1)
		{
			return string.Empty;
		}
		StringBuilder buf = new StringBuilder();
		dictionary.ForEach(delegate(KeyValuePair<TKey, TValue> x)
		{
			buf.AppendFormat("{0}{1}{2}{3}", x.Key.ToString(), keyValueSeparator, x.Value.ToString(), sequenceSeparator);
		});
		return buf.ToString(0, buf.Length - sequenceSeparator.Length);
	}

	public static string ToString<T>(this IList<T> list, string sequenceSeparator)
	{
		if (list == null || list.Count < 1)
		{
			return string.Empty;
		}
		StringBuilder buf = new StringBuilder();
		list.ForEach(delegate(T x)
		{
			buf.AppendFormat("{0}{1}", x.ToString(), sequenceSeparator);
		});
		return buf.ToString(0, buf.Length - sequenceSeparator.Length);
	}

	public static string[] ParseExact(this string data, string format)
	{
		return data.ParseExact(format, false);
	}

	public static string[] ParseExact(this string data, string format, bool ignoreCase)
	{
		string[] values;
		if (data.TryParseExact(format, out values, ignoreCase))
		{
			return values;
		}
		throw new ArgumentException("Format not compatible with value.");
	}

	public static bool TryExtract(this string data, string format, out string[] values)
	{
		return data.TryParseExact(format, out values, false);
	}

	public static bool TryParseExact(this string data, string format, out string[] values, bool ignoreCase)
	{
		int num = 0;
		format = Regex.Escape(format).Replace("\\{", "{");
		num = 0;
		while (true)
		{
			string text = string.Format("{{{0}}}", num);
			if (!format.Contains(text))
			{
				break;
			}
			format = format.Replace(text, string.Format("(?'group{0}'.*)", num));
			num++;
		}
		RegexOptions options = (ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None);
		Match match = new Regex(format, options).Match(data);
		if (num != match.Groups.Count - 1)
		{
			values = new string[0];
			return false;
		}
		values = new string[num];
		for (int i = 0; i < num; i++)
		{
			values[i] = match.Groups[string.Format("group{0}", i)].Value;
		}
		return true;
	}

	public static void CheckPlural(int _num, ref string _str)
	{
		if (_num > 1)
		{
			_str += "s";
		}
	}

	public static string RemoveEmoji(string aOrg)
	{
		return ReplaceEmoji(aOrg, '\0');
	}

	public static string ReplaceEmojiToAsterisk(string aOrg)
	{
		return ReplaceEmoji(aOrg, '*');
	}

	public static string ReplaceEmoji(string aOrg, char aSubstitute)
	{
		string text = string.Empty;
		if (string.IsNullOrEmpty(aOrg))
		{
			return text;
		}
		char[] array = aOrg.ToCharArray();
		char c = '\0';
		char c2 = '\0';
		bool flag = false;
		for (int i = 0; i < array.Length; i++)
		{
			c = array[i];
			UnicodeCategory unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
			if (unicodeCategory == UnicodeCategory.Surrogate)
			{
				if (c >= '\ud800' && c <= '\udbff')
				{
					flag = true;
				}
				else if (c >= '\udc00' && c <= '\udfff')
				{
					if (flag)
					{
						c2 = aSubstitute;
					}
					flag = false;
				}
				c = '\0';
			}
			else
			{
				flag = false;
				if (unicodeCategory == UnicodeCategory.NonSpacingMark || unicodeCategory == UnicodeCategory.EnclosingMark)
				{
					c = '\0';
					if (c2 != 0)
					{
						c2 = aSubstitute;
					}
				}
				else if (CharUtil.IsLetterlikeSymbol(c))
				{
					c = aSubstitute;
				}
				else if (!CharUtil.IsHiragana(c) && !CharUtil.IsKatakana(c) && !CharUtil.IsKanji(c) && !CharUtil.IsNumber(c) && !CharUtil.IsLatinLetter(c) && !CharUtil.IsAsciiPrintable(c))
				{
					c = aSubstitute;
				}
			}
			if (c2 != 0)
			{
				text += c2;
			}
			c2 = c;
		}
		if (c != 0)
		{
			text += c;
		}
		return text;
	}

	public static string ConvertNumberListToCommaSeparateString(int[] aList)
	{
		string[] array = new string[aList.Length];
		for (int i = 0; i < aList.Length; i++)
		{
			array[i] = string.Empty + aList[i];
		}
		return string.Join(",", array);
	}
}
