using UnityEngine;

public class GrowthPush
{
	public enum Environment
	{
		Unknown = 0,
		Development = 1,
		Production = 2
	}

	private AndroidJavaObject growthPush;

	private static GrowthPush instance = new GrowthPush();

	private GrowthPush()
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.growthpush.GrowthPush"))
		{
			growthPush = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]);
		}
	}

	public static GrowthPush GetInstance()
	{
		return instance;
	}

	public void Initialize(string applicationId, string credentialId, Environment environment)
	{
		Initialize(applicationId, credentialId, environment, true);
	}

	public void Initialize(string applicationId, string credentialId, Environment environment, bool adInfoEnable)
	{
		Initialize(applicationId, credentialId, environment, adInfoEnable, null);
	}

	public void Initialize(string applicationId, string credentialId, Environment environment, bool adInfoEnable, string channelId)
	{
		using (AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.growthpush.model.Environment"))
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject static2 = androidJavaClass2.GetStatic<AndroidJavaObject>((environment != Environment.Production) ? "development" : "production");
			growthPush.Call("initialize", @static, applicationId, credentialId, static2, adInfoEnable, channelId);
		}
	}

	public void RequestDeviceToken(string senderId)
	{
		growthPush.Call("requestRegistrationId", senderId);
	}

	public void RequestDeviceToken()
	{
	}

	public string GetDeviceToken()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
		return growthPush.Call<string>("registerGCM", new object[1] { @static });
	}

	public void SetDeviceToken(string deviceToken)
	{
	}

	public void ClearBadge()
	{
	}

	public void SetTag(string name)
	{
		SetTag(name, string.Empty);
	}

	public void SetTag(string name, string value)
	{
		growthPush.Call("setTag", name, value);
	}

	public void TrackEvent(string name)
	{
		TrackEvent(name, string.Empty);
	}

	public void TrackEvent(string name, string value)
	{
		growthPush.Call("trackEvent", name, value);
	}

	public void TrackEvent(string name, string value, string gameObject, string methodName)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.growthpush.ShowMessageHandlerWrapper"))
		{
			androidJavaClass.CallStatic("trackEvent", name, value, gameObject, methodName);
		}
	}

	public void RenderMessage(string uuid)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.growthpush.ShowMessageHandlerWrapper"))
		{
			androidJavaClass.CallStatic("renderMessage", uuid);
		}
	}

	public void SetChannelId(string channelId)
	{
		growthPush.Call("setChannelId", channelId);
	}

	public void DeleteDefaultNotificationChannel()
	{
		growthPush.Call("deleteDefaultNotificationChannel");
	}

	public void SetBaseUrl(string baseUrl)
	{
		AndroidJavaObject androidJavaObject = growthPush.Call<AndroidJavaObject>("getHttpClient", new object[0]);
		androidJavaObject.Call("setBaseUrl", baseUrl);
	}
}
