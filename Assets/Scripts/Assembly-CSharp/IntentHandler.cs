using System;
using UnityEngine;

public class IntentHandler
{
	private AndroidJavaObject intentHandler;

	private static IntentHandler instance = new IntentHandler();

	public IntentHandler()
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.growthbeat.intenthandler.IntentHandlerWrapper"))
		{
			intentHandler = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]);
		}
	}

	private void RunBlockOnThread(Action runBlock)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			@static.Call("runOnUiThread", new AndroidJavaRunnable(runBlock.Invoke));
		}
	}

	public static IntentHandler GetInstance()
	{
		return instance;
	}

	public void ClearIntentHandlers()
	{
		RunBlockOnThread(delegate
		{
			intentHandler.Call("clearIntentHandlers");
		});
	}

	public void AddNoopIntentHandler()
	{
		RunBlockOnThread(delegate
		{
			intentHandler.Call("addNoopIntentHandler");
		});
	}

	public void AddUrlIntentHandler()
	{
		RunBlockOnThread(delegate
		{
			intentHandler.Call("addUrlIntentHandler");
		});
	}

	public void AddCustomIntentHandler(string gameObjectName, string methodName)
	{
		RunBlockOnThread(delegate
		{
			intentHandler.Call("addCustomIntentHandler", gameObjectName, methodName);
		});
	}
}
