using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GiftFullDialog : DialogBase
{
	public enum STATE
	{
		LIST_INIT_WAIT = 0,
		MAIN = 1,
		WAIT = 2,
		SHOW_DETAIL_WAIT = 3,
		SUPPORT_STAGE_RECEIVE_WAIT = 4,
		SUPPORT_STAGE_BUNDLE_RECEIVE_WAIT = 5,
		NETWORK_LOGIN00 = 6,
		NETWORK_LOGIN01 = 7,
		NETWORK_00_00 = 8,
		NETWORK_00_01 = 9,
		NETWORK_01_00 = 10,
		NETWORK_01_01 = 11,
		NETWORK_02_00 = 12,
		NETWORK_02_01 = 13,
		NETWORK_03_00 = 14,
		NETWORK_03_01 = 15,
		NETWORK_ADV_COMPLETE_MAIL = 16,
		NETWORK_ADV_COMPLETE_MAIL_WAIT = 17,
		NETWORK_04_00 = 18,
		NETWORK_04_01 = 19,
		NETWORK_04_02 = 20,
		NETWORK_05_00 = 21,
		NETWORK_05_01 = 22,
		CURRENCY_BUNDLE_WAIT = 23,
		CURRENCY_BUNDLE_SUCCESS = 24,
		CURRENCY_BUNDLE_FAILURE = 25
	}

	private enum LIST_TYPE
	{
		RECEIVE_ALL = 0,
		RECEIVE_HEART = 1,
		RECEIVE_MEDAL = 2,
		RECEIVE_OTHER = 3,
		SEND = 4,
		RESCUE = 5
	}

	private sealed class ListTypeComparer : IEqualityComparer<LIST_TYPE>
	{
		public bool Equals(LIST_TYPE a, LIST_TYPE b)
		{
			return a == b;
		}

		public int GetHashCode(LIST_TYPE arg)
		{
			return (int)arg;
		}
	}

	private sealed class AdvNetworkInfo
	{
		public sealed class ConnectInfo
		{
			private struct MailTarget
			{
				public int ID;

				public GameObject GameObject;
			}

			private int mBundleConnectCount = 10;

			private List<MailTarget> mMailObject = new List<MailTarget>();

			private List<KeyValuePair<int, GiftMailType>> mCharacterInfo = new List<KeyValuePair<int, GiftMailType>>();

			public int[] MailID
			{
				get
				{
					int[] array = new int[mMailObject.Count];
					for (int i = 0; i < mMailObject.Count; i++)
					{
						array[i] = mMailObject[i].ID;
					}
					return array;
				}
			}

			public KeyValuePair<int, GiftMailType>[] CharacterInfo
			{
				get
				{
					List<KeyValuePair<int, GiftMailType>> list = new List<KeyValuePair<int, GiftMailType>>();
					KeyValuePair<int, GiftMailType> pair;
					foreach (KeyValuePair<int, GiftMailType> item in mCharacterInfo)
					{
						pair = item;
						if (!list.Any((KeyValuePair<int, GiftMailType> n) => n.Key == pair.Key && n.Value == pair.Value))
						{
							list.Add(pair);
						}
					}
					return list.ToArray();
				}
			}

			public ConnectInfo(int bundle_connect_count)
			{
				mBundleConnectCount = bundle_connect_count;
			}

			public bool Add(int mail_id, GameObject receive_item_go)
			{
				if (mMailObject == null)
				{
					mMailObject = new List<MailTarget>();
				}
				if (mMailObject.Count >= mBundleConnectCount)
				{
					return false;
				}
				mMailObject.Add(new MailTarget
				{
					ID = mail_id,
					GameObject = receive_item_go
				});
				return true;
			}

			public bool Add(int mail_id, int character_id, GiftMailType gift_mail_type, GameObject receive_item_go)
			{
				if (!Add(mail_id, receive_item_go))
				{
					return false;
				}
				mCharacterInfo.Add(new KeyValuePair<int, GiftMailType>(character_id, gift_mail_type));
				return true;
			}

			public bool Remove(GameObject receive_item_go)
			{
				if (mMailObject == null)
				{
					return false;
				}
				for (int i = 0; i < mMailObject.Count; i++)
				{
					if (!(mMailObject[i].GameObject != receive_item_go))
					{
						MailTarget mailTarget = mMailObject[i];
						mMailObject.RemoveAt(i);
						return true;
					}
				}
				return false;
			}

			public GameObject GetReceiveItemGo(int index)
			{
				return (mMailObject != null && index >= 0 && index < mMailObject.Count) ? mMailObject[index].GameObject : null;
			}
		}

		private int mBundleConnectCount = 10;

		private List<ConnectInfo> mConnectInfo;

		public int Count
		{
			get
			{
				return (mConnectInfo != null) ? mConnectInfo.Count : 0;
			}
		}

		public ConnectInfo this[int i]
		{
			get
			{
				return (mConnectInfo == null || i < 0 || i >= mConnectInfo.Count) ? null : mConnectInfo[i];
			}
		}

		public AdvNetworkInfo(int bundle_connect_count)
		{
			mBundleConnectCount = bundle_connect_count;
		}

		public void Add(int mail_id, GameObject receive_item_go)
		{
			if (mConnectInfo == null)
			{
				mConnectInfo = new List<ConnectInfo>();
			}
			foreach (ConnectInfo item in mConnectInfo)
			{
				if (item.Add(mail_id, receive_item_go))
				{
					return;
				}
			}
			ConnectInfo connectInfo = new ConnectInfo(mBundleConnectCount);
			connectInfo.Add(mail_id, receive_item_go);
			mConnectInfo.Add(connectInfo);
		}

		public void Add(int mail_id, int character_id, GiftMailType gift_mail_type, GameObject receive_item_go)
		{
			if (mConnectInfo == null)
			{
				mConnectInfo = new List<ConnectInfo>();
			}
			foreach (ConnectInfo item in mConnectInfo)
			{
				if (item.Add(mail_id, character_id, gift_mail_type, receive_item_go))
				{
					return;
				}
			}
			ConnectInfo connectInfo = new ConnectInfo(mBundleConnectCount);
			connectInfo.Add(mail_id, character_id, gift_mail_type, receive_item_go);
			mConnectInfo.Add(connectInfo);
		}

		public void Clear()
		{
			if (mConnectInfo != null)
			{
				mConnectInfo.Clear();
				mConnectInfo = null;
			}
		}

		public void Remove(int index)
		{
			if (mConnectInfo != null && index >= 0 && index < mConnectInfo.Count)
			{
				mConnectInfo.RemoveAt(index);
			}
		}

		public void Remove(GameObject receive_item_go)
		{
			if (mConnectInfo == null)
			{
				return;
			}
			foreach (ConnectInfo item in mConnectInfo)
			{
				if (item.Remove(receive_item_go))
				{
					if (item.MailID.Length <= 0)
					{
						mConnectInfo.Remove(item);
					}
					break;
				}
			}
		}
	}

	private enum FIGURE_SKILL_LEVEL_TYPE : byte
	{
		MAX = 0,
		OLD = 1,
		NEW = 2
	}

	private sealed class AdvConnectFigureInfo
	{
		public int MaxSkillLevel { get; set; }

		public int NewSkillLevel { get; set; }

		public int OldSkillLevel { get; set; }

		public int ExpPoint { get; set; }

		public int SkillExpPoint { get; set; }

		public bool IsOverFlowLevel()
		{
			return (NewSkillLevel == MaxSkillLevel && ExpPoint > 0) || SkillExpPoint > 0;
		}
	}

	private enum MULTI_RECEIVE_STATE
	{
		SUCCESS = 0,
		FAILED = 1,
		CONNECT = 2
	}

	private struct ItemListInfoConfig
	{
		public struct OrientationConfig
		{
			public int MaxPercentLine;

			public float Width;

			public float Height;
		}

		public OrientationConfig Portrait;

		public OrientationConfig Landscape;

		public int ScrollSpeed;

		public int Height;

		public int MaxDisplayCount;

		public SMVerticalListInfo CreateVerticalListInfo(float portrait_height_offset)
		{
			return new SMVerticalListInfo(Landscape.MaxPercentLine, Landscape.Width, Landscape.Height, Portrait.MaxPercentLine, Portrait.Width, Portrait.Height + portrait_height_offset, ScrollSpeed, Height, MaxDisplayCount);
		}
	}

	private struct ItemListConfig
	{
		public List<SMVerticalListItem> ListData;

		public bool IsToday;
	}

	private struct ScrollListConfig
	{
		public SMVerticalList ScrollData;

		public bool IsCreated;

		public bool IsToday;
	}

	private struct SubcurrencyInfo
	{
		public int RemoteID;

		public int Quantity;
	}

	public struct ReceiveGiftConfig
	{
		public bool IsInvisibleDialog;

		public bool IsNotResetList;

		public bool IsNotResetAllList;

		public bool IsNotCheckState;

		public bool IsCheckTodayLabelOnly;

		public static ReceiveGiftConfig Create()
		{
			ReceiveGiftConfig result = default(ReceiveGiftConfig);
			result.IsInvisibleDialog = false;
			result.IsNotResetList = false;
			result.IsNotResetAllList = false;
			result.IsNotCheckState = false;
			result.IsCheckTodayLabelOnly = false;
			return result;
		}
	}

	private struct ChangeModeConfig
	{
		public enum CreateMode
		{
			AlreadyChange = 0,
			Force = 1,
			SubOnly = 2,
			TodayLabelOnly = 3
		}

		public CreateMode Mode;

		public float ScrollValue;

		public static ChangeModeConfig Create()
		{
			ChangeModeConfig result = default(ChangeModeConfig);
			result.Mode = CreateMode.AlreadyChange;
			result.ScrollValue = -1f;
			return result;
		}
	}

	public delegate void OnDialogClosed(bool stageOpen, bool friendhelp, bool friendhelpSuccess, bool advChara, bool advTicket);

	private const float WIDE_SCREEN_BODY_PORTRAIT_OFFSET = -12.5f;

	private const int WIDE_SCREEN_BODY_HEIGHT_OFFSET = 100;

	private const float INNER_FRAME_FLUCT_HEIGHT = 193f;

	private int mReceiveSCErrorCode = -1;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LIST_INIT_WAIT);

	private Dictionary<int, bool> mNetworkConnectDone = new Dictionary<int, bool>();

	public STATE StateStatus;

	public STATE OverwriteState = STATE.WAIT;

	private OnDialogClosed mCallback;

	private SMVerticalListInfo mGiftItemListInfo;

	private SMVerticalList mGiftItemList;

	private Dictionary<LIST_TYPE, ScrollListConfig> mTempItemList = new Dictionary<LIST_TYPE, ScrollListConfig>(new ListTypeComparer());

	private GiftListItem[] mSelectedGiftItem;

	private List<GiftListItem> mSuccessSelectedGiftItem = new List<GiftListItem>();

	private List<GiftListItem> mAllGiftItems = new List<GiftListItem>();

	private List<IconCheckData> mIconCheckList = new List<IconCheckData>();

	private GameObject mDialogCenter;

	private Transform mHeader;

	private Transform mFooter;

	private Transform mBody;

	private List<UISprite> mLaceList = new List<UISprite>();

	private UISprite mBackGround;

	private UISprite mTitleBarR;

	private UISprite mTitleBarL;

	private UISprite mTitleBarBG;

	private UILabel mTitle;

	private int mTitleSize = 38;

	private int mTodayLimitSize = 20;

	private UIButton mBackButton;

	private UIButton mTabButtonAll;

	private UIButton mTabButtonHeart;

	private UIButton mTabButtonMedal;

	private UIButton mTabButtonOther;

	private GameObject mAnchorBottomLeft;

	private GameObject mAnchorTopRight;

	private UILabel mTodayLimitLabel;

	private Transform mNoteTransform;

	private UIButton mTabButtonReceive;

	private UIButton mTabButtonSend;

	private UIButton mTabButtonRescue;

	private GetStampDialog mGetStampDialog;

	private GetAccessoryDialog mGetAccessoryDialog;

	private GrowupPartnerSelectDialog mGrowupDialog;

	private GrowupPartnerConfirmDialog mGrowupPartnerConfirmtDialog;

	private GrowupResultDialog mGrowupResultDialog;

	private Attention mAttentionReceive;

	private Attention mAttentionSend;

	private Attention mAttentionRescue;

	private float mAttentionReceiveCount;

	private float mAttentionSendCount;

	private float mAttentionRescueCount;

	private bool mAttentionSaveFlgReceive;

	private bool mAttentionSaveFlgSend;

	private bool mAttentionSaveFlgRescue;

	private UIButton mSendHeartAllFriend;

	private UISprite mSendHeartAllFriendSprite;

	private UISprite mNoSendHeartAllFriend;

	private UIButton mReceiveItem;

	private UISprite mReceiveItemSprite;

	private UISprite mNoReceiveItem;

	private UIPanel mDemoBlockRaycaster;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private ScrollBoxDialog mReceiveAllResultDialog;

	private ScrollBoxDialog mReceiveAllNoticeDialog;

	private int mSNSCallCount;

	private float mBasePosY;

	private string mImageTab0;

	private string mImageTab1;

	private string mImageTab2;

	private string mImageTab3;

	private string mImageTab4;

	private string mImageTab5;

	private string mImageTab6;

	private bool mLaceMove;

	private GameMain.GiftListMode PrevListMode;

	private bool mStageOpenGiftReceived;

	private bool mFriendHelpApply;

	private bool mFriendHelpSuccess;

	private bool mAdvCharaReceived;

	private bool mAdvTicketReceived;

	private FBUserManager mFBUserManager;

	private SMDTools mSmdToolsSeries = new SMDTools();

	private int mReceivedStageNo;

	private int mReceivedStar;

	private Def.SERIES mReceivedSeries;

	private bool mSupportStageLoaded;

	private float mWideScreenBodyPortraitOffset;

	private int mWideScreenBodyHeightOffset;

	private AdvNetworkInfo mAdvNetworkInfo;

	private Dictionary<string, AdvConnectFigureInfo> mAdvConnectFigureInfo = new Dictionary<string, AdvConnectFigureInfo>();

	private string mGuID;

	private GameObject AdvReceivetItemGo;

	private int mailId;

	private int characterId;

	private GiftMailType mGiftMailType;

	private bool mIsSingleAdvReceive;

	private bool mIsMultiReceive;

	private MULTI_RECEIVE_STATE mMultiReceiveState = MULTI_RECEIVE_STATE.CONNECT;

	private Dictionary<string, int> mMultiReceiveItemCount = new Dictionary<string, int>();

	private List<ScrollBoxDialog.CreateConfig.DescriptionInfo> mCompleteNotifyDesc = new List<ScrollBoxDialog.CreateConfig.DescriptionInfo>();

	private ResImage mAtlasMailBox;

	private bool IsOperating = true;

	private bool IsTmpOperating = true;

	private int WaitOperatingCount;

	private bool mIsNetworkAdvMail;

	private bool mIsNetworkSupportMail;

	private bool[] mIsTodayLimit = new bool[Enum.GetNames(typeof(LIST_TYPE)).Length];

	private StoryDemoManager mStoryDemo;

	public GameMain.GiftUIMode Mode { get; set; }

	public GameMain.GiftListMode ListMode { get; set; }

	private float WideScreenBodyPoratraitOffset
	{
		get
		{
			if (mWideScreenBodyPortraitOffset == 0f && Util.IsWideScreen())
			{
				float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num2 = 0.5625f;
				float num3 = 0.4617052f;
				mWideScreenBodyPortraitOffset = (num2 - num) / (num2 - num3) * -12.5f;
			}
			return mWideScreenBodyPortraitOffset;
		}
	}

	private int WideScreenBodyHeightOffset
	{
		get
		{
			if (mWideScreenBodyHeightOffset == 0 && Util.IsWideScreen())
			{
				float num = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
				float num2 = 0.5625f;
				float num3 = 0.4617052f;
				mWideScreenBodyHeightOffset = (int)((num2 - num) / (num2 - num3) * 100f);
			}
			return mWideScreenBodyHeightOffset;
		}
	}

	private AdvNetworkInfo AdvNetwork
	{
		get
		{
			if (mAdvNetworkInfo == null)
			{
				mAdvNetworkInfo = new AdvNetworkInfo(mGame.GameProfile.GiftReceiveMax);
			}
			return mAdvNetworkInfo;
		}
	}

	private void Server_OnGiveGift(int result, int code)
	{
		if (result == 0)
		{
			mState.Reset(STATE.NETWORK_00_00, true);
		}
		else
		{
			mState.Reset(STATE.NETWORK_00_01, true);
		}
	}

	private void Server_OnGiveGiftAll(int result, int code)
	{
		if (result == 0)
		{
			mState.Reset(STATE.NETWORK_02_00, true);
		}
		else
		{
			mState.Reset(STATE.NETWORK_02_01, true);
		}
	}

	private void Server_OnRegisterFriend(int result, int code)
	{
		if (result == 0)
		{
			mState.Reset(STATE.NETWORK_01_00, true);
		}
		else if (code == 15000)
		{
			mConfirmDialog = Util.CreateGameObject("LimitFriendDialog", base.gameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Approval_Friend_Title"), Localization.Get("Friend_Request_Result_Full"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mState.Reset(STATE.WAIT);
		}
		else
		{
			mState.Reset(STATE.NETWORK_01_01, true);
		}
	}

	private void Server_OnBuySC(int result, object res_obj)
	{
		SCChargeResponse sCChargeResponse = res_obj as SCChargeResponse;
		if (result == 0)
		{
			if (sCChargeResponse != null)
			{
				int mc = sCChargeResponse.mc;
				int sc = sCChargeResponse.sc;
				long server_time = sCChargeResponse.server_time;
				mGame.mPlayer.ServerTime = server_time;
				mGame.mPlayer.SetDollar(mc, true);
				mGame.mPlayer.SetDollar(sc, false);
				if (mState.GetStatus() == STATE.CURRENCY_BUNDLE_WAIT)
				{
					mState.Reset(STATE.CURRENCY_BUNDLE_SUCCESS, true);
				}
				else
				{
					mState.Reset(STATE.NETWORK_03_00, true);
				}
			}
			else if (mState.GetStatus() == STATE.CURRENCY_BUNDLE_WAIT)
			{
				mState.Reset(STATE.CURRENCY_BUNDLE_FAILURE, true);
			}
			else
			{
				mState.Reset(STATE.NETWORK_03_01, true);
			}
		}
		else
		{
			if (sCChargeResponse != null)
			{
				mReceiveSCErrorCode = sCChargeResponse.code;
			}
			if (mState.GetStatus() == STATE.CURRENCY_BUNDLE_WAIT)
			{
				mState.Reset(STATE.CURRENCY_BUNDLE_FAILURE, true);
			}
			else
			{
				mState.Reset(STATE.NETWORK_03_01, true);
			}
		}
	}

	private void SendRegisterFriendSuccess()
	{
		mGame.AddedNewFriend = true;
		SendGiftSuccess();
	}

	private void SendRegisterFriendFail()
	{
		SendGiftFail();
	}

	private void RecieveSCSuccess()
	{
		if (mSelectedGiftItem != null)
		{
			for (int i = 0; i < mSelectedGiftItem.Length; i++)
			{
				GiftListItem giftListItem = mSelectedGiftItem[i];
				int sendCategory = giftListItem.sendCategory;
				int sendkind = giftListItem.sendkind;
				int num = 0;
				if (giftListItem.gift != null)
				{
					num = giftListItem.gift.Data.ItemID;
				}
				Def.ITEM_CATEGORY iTEM_CATEGORY = (Def.ITEM_CATEGORY)sendCategory;
				if (iTEM_CATEGORY == Def.ITEM_CATEGORY.CONSUME && sendkind == 4)
				{
					mGame.mPlayer.RemoveGift(giftListItem.gift);
				}
				int index = mGiftItemList.items.IndexOf(giftListItem);
				mGiftItemList.RemoveListItem(index);
				ChangeModeConfig config = ChangeModeConfig.Create();
				config.Mode = ChangeModeConfig.CreateMode.SubOnly;
				ChangeMode(Mode, config);
				DateTime now = DateTime.Now;
				int num2 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
				int num3 = 1;
				int num4 = 0;
				int currentLevel = mGame.mPlayer.CurrentLevel;
				int lifeCount = mGame.mPlayer.LifeCount;
				int totalStars = mGame.mPlayer.TotalStars;
				iTEM_CATEGORY = (Def.ITEM_CATEGORY)sendCategory;
				if (iTEM_CATEGORY != Def.ITEM_CATEGORY.CONSUME || giftListItem.gift != null)
				{
				}
			}
			mSelectedGiftItem = null;
			mGame.Save();
		}
		mState.Change(STATE.MAIN);
	}

	private void ReceiveSCFail(bool is_show_dialog = true)
	{
		string desc = string.Empty;
		if (mSelectedGiftItem != null)
		{
			for (int i = 0; i < mSelectedGiftItem.Length; i++)
			{
				GiftListItem giftListItem = mSelectedGiftItem[i];
				int sendCategory = giftListItem.sendCategory;
				int sendkind = giftListItem.sendkind;
				int num = mReceiveSCErrorCode;
				Def.ITEM_CATEGORY iTEM_CATEGORY = (Def.ITEM_CATEGORY)sendCategory;
				desc = ((iTEM_CATEGORY != Def.ITEM_CATEGORY.CONSUME) ? string.Format(Localization.Get("Gift_Generic_Error_Desc")) : ((sendkind != 4) ? string.Format(Localization.Get("Gift_Generic_Error_Desc")) : string.Format(Localization.Get("Gift_Generic_Error_Desc"))));
			}
		}
		mReceiveSCErrorCode = -1;
		mSelectedGiftItem = null;
		mState.Reset(STATE.WAIT, true);
		EnableList(false);
		if (is_show_dialog)
		{
			GameObject parent = base.gameObject.transform.parent.gameObject;
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("Gift_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
			mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	private void SendGiftSuccess()
	{
		GiftListItem giftListItem = mSelectedGiftItem[0];
		int sendCategory = giftListItem.sendCategory;
		int sendkind = giftListItem.sendkind;
		int item_id = 0;
		if (giftListItem.gift != null)
		{
			item_id = giftListItem.gift.Data.ItemID;
		}
		switch ((Def.ITEM_CATEGORY)sendCategory)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			switch (sendkind)
			{
			}
			break;
		}
		mSelectedGiftItem = null;
		int index = mGiftItemList.items.IndexOf(giftListItem);
		mGiftItemList.RemoveListItem(index);
		if (mGiftItemList.GetListItemCount() == 0)
		{
			GameMain.GiftUIMode mode = Mode;
			if (mode == GameMain.GiftUIMode.SEND)
			{
				if (mSendHeartAllFriend != null && (bool)mSendHeartAllFriend.gameObject && mSendHeartAllFriend.gameObject.activeSelf)
				{
					mSendHeartAllFriend.gameObject.SetActive(false);
				}
				if (mNoSendHeartAllFriend != null && (bool)mNoSendHeartAllFriend.gameObject && !mNoSendHeartAllFriend.gameObject.activeSelf)
				{
					mNoSendHeartAllFriend.gameObject.SetActive(true);
					mNoSendHeartAllFriend.depth = 52;
				}
			}
		}
		DateTime now = DateTime.Now;
		int num = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
		int num2 = 1;
		int num3 = 0;
		int currentLevel = mGame.mPlayer.CurrentLevel;
		int lifeCount = mGame.mPlayer.LifeCount;
		int totalStars = mGame.mPlayer.TotalStars;
		switch ((Def.ITEM_CATEGORY)sendCategory)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			if (giftListItem.friend != null)
			{
				int uUID = giftListItem.friend.Data.UUID;
				mGame.AchievementFriendHeart(uUID);
			}
			break;
		case Def.ITEM_CATEGORY.FRIEND_RES:
			if (giftListItem.gift != null)
			{
				mGame.mPlayer.RemoveGift(giftListItem.gift);
			}
			break;
		case Def.ITEM_CATEGORY.ROADBLOCK_RES:
			if (giftListItem.gift != null)
			{
				int num6 = 0;
				int num7 = 0;
				int uUID = giftListItem.gift.Data.FromID;
				string text = null;
				num3 = 0;
				num6 = giftListItem.gift.Data.ItemKind;
				num7 = giftListItem.gift.Data.ItemID;
				mGame.AchievementFriendRoadblock(uUID);
				mGame.mPlayer.RemoveGift(giftListItem.gift);
			}
			break;
		case Def.ITEM_CATEGORY.STAGEHELP_RES:
			if (giftListItem.gift != null)
			{
				int num4 = 0;
				int num5 = 0;
				int uUID = giftListItem.gift.Data.FromID;
				string text = null;
				num3 = 0;
				num4 = giftListItem.gift.Data.ItemKind;
				num5 = giftListItem.gift.Data.ItemID;
				mGame.mPlayer.RemoveGift(giftListItem.gift);
			}
			break;
		case Def.ITEM_CATEGORY.PRESENT_HEART:
		{
			mGame.mPlayer.UpdateSendHeart(giftListItem.friend.Data.UUID);
			int uUID = giftListItem.friend.Data.UUID;
			mGame.AchievementFriendHeart(uUID);
			int count = mGame.mPlayer.Friends.Count;
			ServerCram.SendGiftITEM(count, sendkind, item_id, 0);
			break;
		}
		}
		mGame.Save();
		mState.Change(STATE.MAIN);
	}

	private void SendGiftFail()
	{
		GiftListItem giftListItem = mSelectedGiftItem[0];
		int sendCategory = giftListItem.sendCategory;
		int sendkind = giftListItem.sendkind;
		string desc;
		switch ((Def.ITEM_CATEGORY)sendCategory)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			desc = ((sendkind != 1 && sendkind != 2) ? string.Format(Localization.Get("Gift_Generic_Error_Desc")) : string.Format(Localization.Get("Gift_Heart_Error_Desc")));
			break;
		case Def.ITEM_CATEGORY.FRIEND_RES:
			desc = string.Format(Localization.Get("Gift_Friend_Error_Desc"));
			break;
		case Def.ITEM_CATEGORY.ROADBLOCK_RES:
			desc = string.Format(Localization.Get("Gift_RoadBlock_Error_Desc"));
			break;
		case Def.ITEM_CATEGORY.STAGEHELP_RES:
			desc = string.Format(Localization.Get("Gift_FriendHelp_Error_Desc"));
			break;
		case Def.ITEM_CATEGORY.PRESENT_HEART:
			desc = string.Format(Localization.Get("Gift_Heart_Error_Desc"));
			break;
		default:
			desc = string.Format(Localization.Get("Gift_Generic_Error_Desc"));
			break;
		}
		mSelectedGiftItem = null;
		giftListItem = null;
		mState.Reset(STATE.WAIT, true);
		EnableList(false);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("Gift_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
	}

	private void SendGiftAllSuccess()
	{
		GiftListItem giftListItem = mSelectedGiftItem[0];
		int sendCategory = giftListItem.sendCategory;
		int sendkind = giftListItem.sendkind;
		int item_id = 0;
		if (giftListItem.gift != null)
		{
			item_id = giftListItem.gift.Data.ItemID;
		}
		switch ((Def.ITEM_CATEGORY)sendCategory)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			switch (sendkind)
			{
			}
			break;
		case Def.ITEM_CATEGORY.PRESENT_HEART:
		{
			int count = mGame.mPlayer.Friends.Count;
			ServerCram.SendGiftITEM(count, sendkind, item_id, 1);
			break;
		}
		}
		mSelectedGiftItem = null;
		for (int i = 0; i < mAllGiftItems.Count; i++)
		{
			GiftListItem giftListItem2 = mAllGiftItems[i];
			mGiftItemList.RemoveListItem(giftListItem2);
			DateTime now = DateTime.Now;
			int num = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
			int num2 = 1;
			int num3 = 0;
			int currentLevel = mGame.mPlayer.CurrentLevel;
			int lifeCount = mGame.mPlayer.LifeCount;
			int totalStars = mGame.mPlayer.TotalStars;
			switch ((Def.ITEM_CATEGORY)sendCategory)
			{
			case Def.ITEM_CATEGORY.CONSUME:
				if (giftListItem2.friend != null)
				{
					int uUID = giftListItem2.friend.Data.UUID;
					mGame.AchievementFriendHeart(uUID);
					mGame.mPlayer.UpdateSendHeart(uUID);
				}
				break;
			case Def.ITEM_CATEGORY.FRIEND_RES:
				if (giftListItem2.gift != null)
				{
					mGame.mPlayer.RemoveGift(giftListItem2.gift);
				}
				break;
			case Def.ITEM_CATEGORY.ROADBLOCK_RES:
				if (giftListItem2.gift != null)
				{
					int num6 = 0;
					int uUID = giftListItem2.gift.Data.FromID;
					string text = null;
					num3 = 0;
					num6 = giftListItem.gift.Data.ItemKind;
					mGame.AchievementFriendRoadblock(uUID);
					mGame.mPlayer.RemoveGift(giftListItem2.gift);
				}
				break;
			case Def.ITEM_CATEGORY.STAGEHELP_RES:
				if (giftListItem.gift != null)
				{
					int num4 = 0;
					int num5 = 0;
					int uUID = giftListItem.gift.Data.FromID;
					string text = null;
					num3 = 0;
					num4 = giftListItem.gift.Data.ItemKind;
					num5 = giftListItem.gift.Data.ItemID;
					mGame.mPlayer.RemoveGift(giftListItem.gift);
				}
				break;
			case Def.ITEM_CATEGORY.PRESENT_HEART:
			{
				mGame.mPlayer.UpdateSendHeart(giftListItem2.friend.Data.UUID);
				int uUID = giftListItem2.friend.Data.UUID;
				mGame.AchievementFriendHeart(uUID);
				break;
			}
			}
		}
		if (mGiftItemList.GetListItemCount() == 0)
		{
			GameMain.GiftUIMode mode = Mode;
			if (mode == GameMain.GiftUIMode.SEND)
			{
				if (mSendHeartAllFriend != null && (bool)mSendHeartAllFriend.gameObject && mSendHeartAllFriend.gameObject.activeSelf)
				{
					mSendHeartAllFriend.gameObject.SetActive(false);
				}
				if (mNoSendHeartAllFriend != null && (bool)mNoSendHeartAllFriend.gameObject && !mNoSendHeartAllFriend.gameObject.activeSelf)
				{
					mNoSendHeartAllFriend.gameObject.SetActive(true);
					mNoSendHeartAllFriend.depth = 52;
				}
			}
		}
		mGame.Save();
		mState.Change(STATE.MAIN);
	}

	private void SendGiftAllFail()
	{
		GiftListItem giftListItem = mSelectedGiftItem[0];
		int sendCategory = giftListItem.sendCategory;
		int sendkind = giftListItem.sendkind;
		string desc;
		switch ((Def.ITEM_CATEGORY)sendCategory)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			desc = ((sendkind != 1 && sendkind != 2) ? string.Format(Localization.Get("Gift_Generic_Error_Desc")) : string.Format(Localization.Get("Gift_Heart_Error_Desc")));
			break;
		case Def.ITEM_CATEGORY.FRIEND_RES:
			desc = string.Format(Localization.Get("Gift_Friend_Error_Desc"));
			break;
		case Def.ITEM_CATEGORY.ROADBLOCK_RES:
			desc = string.Format(Localization.Get("Gift_RoadBlock_Error_Desc"));
			break;
		case Def.ITEM_CATEGORY.STAGEHELP_RES:
			desc = string.Format(Localization.Get("Gift_FriendHelp_Error_Desc"));
			break;
		case Def.ITEM_CATEGORY.PRESENT_HEART:
			desc = string.Format(Localization.Get("Gift_Heart_Error_Desc"));
			break;
		default:
			desc = string.Format(Localization.Get("Gift_Generic_Error_Desc"));
			break;
		}
		mSelectedGiftItem = null;
		giftListItem = null;
		mState.Reset(STATE.WAIT, true);
		EnableList(false);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("Gift_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		if (mSendHeartAllFriend != null)
		{
			mSendHeartAllFriend.GetComponent<JellyImageButton>().SetButtonEnable(true);
		}
	}

	private void Network_ResponseFriendRequest(GiftListItem a_item)
	{
		int fromID = a_item.gift.Data.FromID;
		if (!Server.RegisterFriend(fromID))
		{
			SendRegisterFriendFail();
		}
	}

	private void Network_ResponseRoadBlockRequest(GiftListItem a_item)
	{
		int fromID = a_item.gift.Data.FromID;
		GiftItemData giftItemData = new GiftItemData();
		giftItemData.FromID = mGame.mPlayer.UUID;
		giftItemData.Message = mGame.mPlayer.NickName;
		giftItemData.ItemCategory = 15;
		giftItemData.ItemKind = a_item.gift.Data.ItemKind;
		giftItemData.ItemID = a_item.gift.Data.ItemID;
		giftItemData.Quantity = 1;
		if (!Server.GiveGift(giftItemData, fromID))
		{
			SendGiftFail();
		}
	}

	private void DoSNS(BIJSNS _sns)
	{
		for (int i = 0; i < mSelectedGiftItem.Length; i++)
		{
			mSNSCallCount++;
			try
			{
				mState.Reset(STATE.WAIT, true);
				mSNS = _sns;
				if (_sns.IsOpEnabled(2) && !_sns.IsLogined())
				{
					if (mSNSCallCount > 1)
					{
						SNSError(_sns);
					}
					else
					{
						mState.Change(STATE.NETWORK_LOGIN00);
					}
					break;
				}
				GiftItemData giftItemData = new GiftItemData();
				giftItemData.FromID = mGame.mPlayer.UUID;
				giftItemData.Message = mGame.mOptions.FbName;
				int fromID = mSelectedGiftItem[i].gift.Data.FromID;
				giftItemData.ItemCategory = 15;
				giftItemData.ItemKind = mSelectedGiftItem[i].gift.Data.ItemKind;
				giftItemData.ItemID = 0;
				if (!Server.GiveGift(giftItemData, fromID))
				{
					SendGiftFail();
					break;
				}
			}
			catch (Exception)
			{
				mState.Change(STATE.MAIN);
			}
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Reset(STATE.WAIT, true);
		EnableList(false);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public override void Start()
	{
		Mode = GameMain.GiftUIMode.RECEIVE;
		Server.GiveGiftEvent += Server_OnGiveGift;
		Server.RegisterFriendEvent += Server_OnRegisterFriend;
		Server.GiveGiftAllEvent += Server_OnGiveGiftAll;
		ServerIAP.SubcurrencyChargeEvent += Server_OnBuySC;
		if (Mode == GameMain.GiftUIMode.NONE)
		{
			Mode = GameMain.GiftUIMode.RECEIVE;
		}
		mFBUserManager = SingletonMonoBehaviour<GameMain>.Instance.mFbUserMngGift;
		if (mFBUserManager != null)
		{
			mFBUserManager.SetPriority(true);
		}
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (OverwriteState != STATE.WAIT)
		{
			mState.Change(OverwriteState);
			OverwriteState = STATE.WAIT;
		}
		StateStatus = mState.GetStatus();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (StateStatus)
		{
		case STATE.LIST_INIT_WAIT:
			if (mGiftItemList != null && mGiftItemList.IsContentsCreated)
			{
				mGiftItemList.OnScreenChanged(Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown);
				if (mState.GetPreStatus() == STATE.SHOW_DETAIL_WAIT)
				{
					mState.Change(STATE.SHOW_DETAIL_WAIT);
				}
				else if (mGetStampDialog != null || mGetAccessoryDialog != null || mGrowupDialog != null || mGrowupPartnerConfirmtDialog != null || mGrowupResultDialog != null || mConfirmDialog != null || mReceiveAllResultDialog != null || mReceiveAllNoticeDialog != null)
				{
					mState.Change(STATE.WAIT);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}
			break;
		case STATE.MAIN:
		{
			if (mIconCheckList.Count <= 0)
			{
				break;
			}
			BIJImage image = ResourceManager.LoadImage("ICON").Image;
			int num = mBaseDepth + 5;
			for (int num2 = mIconCheckList.Count - 1; num2 >= 0; num2--)
			{
				IconCheckData iconCheckData = mIconCheckList[num2];
				FBUserData.STATE iconStatus = mFBUserManager.GetIconStatus(iconCheckData.uuid);
				if (iconStatus == FBUserData.STATE.SUCCESS || iconStatus == FBUserData.STATE.FAILURE)
				{
					if (iconCheckData.loadingIcon != null)
					{
						UnityEngine.Object.Destroy(iconCheckData.loadingIcon);
					}
					Texture2D icon = mFBUserManager.GetIcon(iconCheckData.uuid);
					if (icon != null)
					{
						UITexture uITexture = Util.CreateGameObject("Icon", iconCheckData.boardImage.gameObject).AddComponent<UITexture>();
						uITexture.depth = num + 2;
						uITexture.transform.localPosition = new Vector3(-225f, -18f, 0f);
						uITexture.mainTexture = icon;
						uITexture.SetDimensions(65, 65);
						int num3 = iconCheckData.iconid - 10000;
						if (num3 >= mGame.mCompanionData.Count)
						{
							num3 = 0;
						}
						string iconName = mGame.mCompanionData[num3].iconName;
						UISprite uISprite = Util.CreateSprite("CharaIcon", uITexture.gameObject, image);
						Util.SetSpriteInfo(uISprite, iconName, num + 4, new Vector3(32f, 32f, 0f), Vector3.one, false);
						uISprite.SetDimensions(50, 50);
					}
					else
					{
						string imageName = "icon_chara_facebook";
						UISprite uISprite2 = Util.CreateSprite("Icon", iconCheckData.boardImage.gameObject, image);
						Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-225f, -18f, 0f), Vector3.one, false);
						uISprite2.SetDimensions(65, 65);
						int num4 = iconCheckData.iconid - 10000;
						if (num4 >= mGame.mCompanionData.Count)
						{
							num4 = 0;
						}
						string iconName2 = mGame.mCompanionData[num4].iconName;
						UISprite uISprite3 = Util.CreateSprite("CharaIcon", uISprite2.gameObject, image);
						Util.SetSpriteInfo(uISprite3, iconName2, num + 4, new Vector3(32f, 32f, 0f), Vector3.one, false);
						uISprite3.SetDimensions(50, 50);
					}
					mIconCheckList.RemoveAt(num2);
				}
			}
			break;
		}
		case STATE.SUPPORT_STAGE_RECEIVE_WAIT:
			if (!mSupportStageLoaded)
			{
				break;
			}
			mSupportStageLoaded = false;
			mGame.DebugCheatStageClear_Main(mReceivedSeries, mReceivedStageNo, mReceivedStar);
			mGame.Save();
			if (mReceivedSeries == Def.SERIES.SM_EV)
			{
				if (CheckDisplayDialog(mReceivedStageNo))
				{
					ConfirmDialog test = Util.CreateGameObject("ConfirmDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
					test.Init(Localization.Get("MailBOX_GetJwel_Title"), Localization.Get("MailBOX_GetJwel_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
					test.SetClosedCallback(delegate
					{
						test = null;
						mState.Change(STATE.MAIN);
					});
					test.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component = test.GetComponent<BoxCollider>();
						if ((bool)component)
						{
							component.size = new Vector3(1384f, 1384f, 1f);
						}
					});
					mState.Change(STATE.WAIT);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.SUPPORT_STAGE_BUNDLE_RECEIVE_WAIT:
			if (!mSupportStageLoaded)
			{
				break;
			}
			mSupportStageLoaded = false;
			mGame.DebugCheatStageClearBundle_Main(mReceivedSeries, mReceivedStageNo, mReceivedStar, true);
			mGame.Save();
			if (mReceivedSeries == Def.SERIES.SM_EV)
			{
				if (CheckDisplayDialog(mReceivedStageNo))
				{
					ConfirmDialog test2 = Util.CreateGameObject("ConfirmDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
					test2.Init(Localization.Get("MailBOX_GetJwel_Title"), Localization.Get("MailBOX_GetJwel_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
					test2.SetClosedCallback(delegate
					{
						test2 = null;
						mState.Change(STATE.MAIN);
					});
					test2.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component4 = test2.GetComponent<BoxCollider>();
						if ((bool)component4)
						{
							component4.size = new Vector3(1384f, 1384f, 1f);
						}
					});
					mState.Change(STATE.WAIT);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						mGame.SaveOptions();
					}
					SNSError(_sns);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns2, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				DoSNS(_sns2);
			}, null);
			break;
		}
		case STATE.NETWORK_00_00:
			SendGiftSuccess();
			break;
		case STATE.NETWORK_00_01:
			SendGiftFail();
			break;
		case STATE.NETWORK_01_00:
			SendRegisterFriendSuccess();
			break;
		case STATE.NETWORK_01_01:
			SendRegisterFriendFail();
			break;
		case STATE.NETWORK_02_00:
			SendGiftAllSuccess();
			break;
		case STATE.NETWORK_02_01:
			SendGiftAllFail();
			break;
		case STATE.NETWORK_03_00:
			RecieveSCSuccess();
			break;
		case STATE.NETWORK_03_01:
			ReceiveSCFail();
			break;
		case STATE.NETWORK_04_00:
			if (mIsNetworkAdvMail)
			{
				break;
			}
			if (mGame.mPlayer.Gifts.Count < Constants.GIFT_AMOUNT)
			{
				if (GameMain.AdvGetMailCount >= Constants.GIFT_AMOUNT)
				{
					mConfirmDialog = Util.CreateGameObject("RedialDialog", base.gameObject).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("MailBOX_ReAc_Title"), Localization.Get("MailBOX_ReAc_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, true, false);
					mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component3 = mConfirmDialog.GetComponent<BoxCollider>();
						if ((bool)component3)
						{
							component3.size = new Vector3(1384f, 1384f, 1f);
						}
						mState.Change(STATE.NETWORK_04_01);
					});
					mIsNetworkAdvMail = true;
					break;
				}
				mIsNetworkSupportMail = false;
				mState.Reset(STATE.NETWORK_05_00, true);
				goto case STATE.NETWORK_05_00;
			}
			ChangeMode(Mode, new ChangeModeConfig
			{
				Mode = ChangeModeConfig.CreateMode.Force
			});
			mAdvConnectFigureInfo.Clear();
			break;
		case STATE.NETWORK_04_01:
		{
			int is_retry = 0;
			if (mGuID == null)
			{
				mGuID = Guid.NewGuid().ToString();
			}
			else
			{
				is_retry = 1;
			}
			mGame.Network_AdvGetMail(mGuID, is_retry, true);
			mState.Change(STATE.NETWORK_04_02);
			break;
		}
		case STATE.NETWORK_04_02:
		{
			if (!GameMain.mAdvGetMailCheckDone)
			{
				break;
			}
			mIsNetworkAdvMail = false;
			if (GameMain.mAdvGetMailSucceed)
			{
				mIsNetworkSupportMail = false;
				mState.Change(STATE.NETWORK_05_00);
				mAdvConnectFigureInfo.Clear();
				break;
			}
			Server.ErrorCode mAdvGetMailErrorCode = GameMain.mAdvGetMailErrorCode;
			if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 19);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 19, OnConnectErrorRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		}
		case STATE.NETWORK_05_00:
		{
			if (mIsNetworkSupportMail)
			{
				break;
			}
			int custom_receive_count = Constants.GIFT_AMOUNT - mGame.mPlayer.Gifts.Count;
			if (mGame.mPlayer.Gifts.Count < Constants.GIFT_AMOUNT && GameMain.MapSupportRequestCount == GameMain.MapSupportReceiveCount && mGame.MapNetwork_SupportCheck(custom_receive_count))
			{
				if (mConfirmDialog == null)
				{
					mConfirmDialog = Util.CreateGameObject("RedialDialog", base.gameObject).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("MailBOX_ReAc_Title"), Localization.Get("MailBOX_ReAc_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, true, false);
					mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component2 = mConfirmDialog.GetComponent<BoxCollider>();
						if ((bool)component2)
						{
							component2.size = new Vector3(1384f, 1384f, 1f);
						}
						mState.Change(STATE.NETWORK_05_01);
					});
					mIsNetworkSupportMail = true;
				}
				else
				{
					mIsNetworkSupportMail = true;
					mState.Change(STATE.NETWORK_05_01);
				}
			}
			else if (mConfirmDialog != null)
			{
				if (mConfirmDialog.TryCreateButton())
				{
					mConfirmDialog.SetCallback(CALLBACK_STATE.OnCloseFinished, delegate
					{
						ChangeMode(Mode, new ChangeModeConfig
						{
							Mode = ChangeModeConfig.CreateMode.Force
						});
						mAdvConnectFigureInfo.Clear();
					});
					mIsNetworkSupportMail = true;
				}
				else
				{
					mConfirmDialog.Close();
					ChangeMode(Mode, new ChangeModeConfig
					{
						Mode = ChangeModeConfig.CreateMode.Force
					});
					mAdvConnectFigureInfo.Clear();
				}
			}
			else
			{
				ChangeMode(Mode, new ChangeModeConfig
				{
					Mode = ChangeModeConfig.CreateMode.Force
				});
				mAdvConnectFigureInfo.Clear();
			}
			break;
		}
		case STATE.NETWORK_05_01:
			if (!GameMain.mMapSupportCheckDone)
			{
				break;
			}
			mIsNetworkSupportMail = false;
			if (GameMain.mMapSupportSucceed)
			{
				if (mConfirmDialog != null)
				{
					if (mConfirmDialog.TryCreateButton())
					{
						mConfirmDialog.SetCallback(CALLBACK_STATE.OnCloseFinished, delegate
						{
							ChangeMode(Mode, new ChangeModeConfig
							{
								Mode = ChangeModeConfig.CreateMode.Force
							});
						});
					}
					else
					{
						mConfirmDialog.Close();
						ChangeMode(Mode, new ChangeModeConfig
						{
							Mode = ChangeModeConfig.CreateMode.Force
						});
					}
				}
				else
				{
					ChangeMode(Mode, new ChangeModeConfig
					{
						Mode = ChangeModeConfig.CreateMode.Force
					});
				}
			}
			else
			{
				Server.ErrorCode mAdvGetMailErrorCode = GameMain.mAdvGetMailErrorCode;
				if (mAdvGetMailErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 19);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 19, OnConnectErrorRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = mAtlasMailBox.Image;
		UIFont atlasFont = GameMain.LoadFont();
		SetJellyTargetScale(1f);
		Vector2 vector = Util.LogScreenSize();
		float num = vector.y * 0.5f;
		float num2 = 0f - num;
		if (Util.IsWideScreen())
		{
			Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopLeft, mOrientation);
			Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomRight, mOrientation);
			num = vector2.y;
			num2 = vector3.y;
		}
		GameObject parent = base.gameObject;
		string empty = string.Empty;
		float num3 = 0f;
		GameObject gameObject = Util.CreateGameObject("Header", parent);
		GameObject gameObject2 = Util.CreateGameObject("Footer", parent);
		GameObject gameObject3 = Util.CreateGameObject("Body", parent);
		mHeader = gameObject.transform;
		mFooter = gameObject2.transform;
		mBody = gameObject3.transform;
		mBackGround = Util.CreateSprite("BackGround", parent, image2);
		Util.SetSpriteInfo(mBackGround, "characolle_back00", mBaseDepth, new Vector3(0f, 0f, 200f), Vector3.one, false, UIWidget.Pivot.Bottom);
		mBackGround.type = UIBasicSprite.Type.Tiled;
		mBackGround.SetDimensions(1386, 910);
		Vector3 localPosition = mBackGround.transform.localPosition;
		mBackGround.transform.localPosition = localPosition;
		mBackGround.transform.localScale = new Vector3(1f, 1.5230769f, 1f);
		mAnchorBottomLeft = Util.CreateAnchorWithChild("CollectionAnchorBottomLeft", parent, UIAnchor.Side.BottomLeft, component).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("CollectionAnchorTopRight", parent, UIAnchor.Side.TopRight, component).gameObject;
		mLaceMove = true;
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, mBaseDepth + 1, new Vector3(30f, -110f, 0f), Vector3.one, 15f));
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, mBaseDepth + 1, new Vector3(-140f, -10f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(190f, -50f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(-20f, 60f, 0f), Vector3.one, 15f));
		float num4 = num + 50f;
		mTitleBarR = Util.CreateSprite("TitleBarR", gameObject, image);
		Util.SetSpriteInfo(mTitleBarR, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, num4 + 123f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBarL = Util.CreateSprite("TitleBarL", gameObject, image);
		Util.SetSpriteInfo(mTitleBarL, "menubar_frame", mBaseDepth + 2, new Vector3(2f, num4 + 123f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		num4 = num - 28f;
		mTitle = Util.CreateLabel("Title", gameObject, atlasFont);
		Util.SetLabelInfo(mTitle, string.Empty, mBaseDepth + 3, Vector3.zero, mTitleSize, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(mTitle, Localization.Get("Gift_Title"));
		mTitle.transform.localPosition = new Vector3(0f, num4, 0f);
		num4 = num2 + 117f;
		mTodayLimitLabel = Util.CreateLabel("TodayLimit", gameObject2, atlasFont);
		Util.SetLabelInfo(mTodayLimitLabel, string.Empty, 49, Vector3.zero, mTodayLimitSize, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(mTodayLimitLabel, Localization.Get("MailBOX_Info"));
		mTodayLimitLabel.transform.localPosition = new Vector3(14f, num4, 0f);
		mTodayLimitLabel.color = new Color32(242, 55, 117, byte.MaxValue);
		mTodayLimitLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		mTodayLimitLabel.SetDimensions(465, 20);
		empty = "button_back";
		num3 = 70f;
		mBackButton = Util.CreateJellyImageButton("BackButton", gameObject2, image);
		Util.SetImageButtonInfo(mBackButton, empty, empty, empty, mBaseDepth + 3, Vector3.zero, Vector3.one);
		Util.AddImageButtonMessage(mBackButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
		mBackButton.transform.localPosition = new Vector3((0f - vector.x) * 0.5f + num3, (0f - vector.y) * 0.5f + 80f);
		InitDialogFrame(DIALOG_SIZE.XLARGE, null, null, 600, 865);
		mDialogFrame.transform.SetParent(mBody.transform);
		mDialogFrame.transform.localPosition = new Vector3(-6f, -5f);
		string text = string.Format(Localization.Get("Com_Notice"));
		mDialogCenter = Util.CreateGameObject("GridRoot", gameObject3);
		GameMain.GiftUIMode mode = Mode;
		UILabel uILabel = Util.CreateLabel("Note", gameObject3, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 1, Vector3.zero, 21, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(uILabel, Localization.Get("Com_Notice"));
		Util.SetLabelColor(uILabel, Def.DEFAULT_MESSAGE_COLOR);
		mNoteTransform = uILabel.transform;
		mNoteTransform.localPosition = new Vector3(0f, 320f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
		mTabButtonAll = Util.CreateImageButton("TabAll", gameObject3, image2);
		Util.SetImageButtonInfo(mTabButtonAll, mImageTab3, mImageTab3, mImageTab3, mBaseDepth + 2, new Vector3(-140f, 365f, 0f), Vector3.one);
		Util.SetImageButtonMessage(mTabButtonAll, this, "OnAllTabPushed", UIButtonMessage.Trigger.OnClick);
		Util.SetImageButtonColor(mTabButtonAll, Color.white);
		mTabButtonHeart = Util.CreateImageButton("TabHeart", gameObject3, image2);
		Util.SetImageButtonInfo(mTabButtonHeart, mImageTab4, mImageTab4, mImageTab4, mBaseDepth + 2, new Vector3(-15f, 365f, 0f), Vector3.one);
		Util.SetImageButtonMessage(mTabButtonHeart, this, "OnHeartTabPushed", UIButtonMessage.Trigger.OnClick);
		Util.SetImageButtonColor(mTabButtonHeart, Color.gray);
		mTabButtonOther = Util.CreateImageButton("TabOther", gameObject3, image2);
		Util.SetImageButtonInfo(mTabButtonOther, mImageTab6, mImageTab6, mImageTab6, mBaseDepth + 2, new Vector3(110f, 365f, 0f), Vector3.one);
		Util.SetImageButtonMessage(mTabButtonOther, this, "OnOtherTabPushed", UIButtonMessage.Trigger.OnClick);
		Util.SetImageButtonColor(mTabButtonOther, Color.gray);
		mTabButtonReceive = Util.CreateImageButton("TabA", gameObject3, image2);
		Util.SetImageButtonInfo(mTabButtonReceive, mImageTab0, mImageTab0, mImageTab0, mBaseDepth - 3, new Vector3(-175f, 445f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonReceive, this, "OnReceiveTabPushed", UIButtonMessage.Trigger.OnClick);
		mTabButtonSend = Util.CreateImageButton("TabB", gameObject3, image2);
		Util.SetImageButtonInfo(mTabButtonSend, mImageTab1, mImageTab1, mImageTab1, mBaseDepth - 3, new Vector3(-10f, 445f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonSend, this, "OnSendTabPushed", UIButtonMessage.Trigger.OnClick);
		mTabButtonRescue = Util.CreateImageButton("TabC", gameObject3, image2);
		Util.SetImageButtonInfo(mTabButtonRescue, mImageTab2, mImageTab2, mImageTab2, mBaseDepth - 3, new Vector3(155f, 445f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonRescue, this, "OnRescueTabPushed", UIButtonMessage.Trigger.OnClick);
		mAttentionReceive = Util.CreateGameObject("TabAAttention", mTabButtonReceive.gameObject).AddComponent<Attention>();
		mAttentionReceive.Init(1f, mBaseDepth + 2, new Vector3(66f, 5f, 0f), image, "icon_exclamation");
		mAttentionReceive.SetAttentionEnable(false);
		mAttentionSend = Util.CreateGameObject("TabBAttention", mTabButtonSend.gameObject).AddComponent<Attention>();
		mAttentionSend.Init(1f, mBaseDepth + 2, new Vector3(66f, 5f, 0f), image, "icon_exclamation");
		mAttentionSend.SetAttentionEnable(false);
		mAttentionRescue = Util.CreateGameObject("TabCAttention", mTabButtonRescue.gameObject).AddComponent<Attention>();
		mAttentionRescue.Init(1f, mBaseDepth + 2, new Vector3(66f, 5f, 0f), image, "icon_exclamation");
		mAttentionRescue.SetAttentionEnable(false);
		mReceiveItem = Util.CreateJellyImageButton("ReceiveItem", gameObject2, image2);
		Util.SetImageButtonInfo(mReceiveItem, "LC_button_collect", "LC_button_collect", "LC_button_collect", 53, new Vector3(0f, 0f - vector.y + 68f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mReceiveItem, this, "OnReceiveItemPushed", UIButtonMessage.Trigger.OnClick);
		mReceiveItemSprite = mReceiveItem.GetComponent<UISprite>();
		mNoReceiveItem = Util.CreateSprite("NoReceiveItem", gameObject2, image2);
		Util.SetSpriteInfo(mNoReceiveItem, "LC_button_collect", 53, new Vector3(0f, 0f - vector.y + 68f, 0f), Vector3.one, false);
		mNoReceiveItem.color = Color.gray;
		mSendHeartAllFriend = Util.CreateJellyImageButton("SendAll", gameObject2, image2);
		Util.SetImageButtonInfo(mSendHeartAllFriend, "LC_button_sendAll", "LC_button_sendAll", "LC_button_sendAll", 53, new Vector3(0f, 0f - vector.y + 68f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mSendHeartAllFriend, this, "OnSendAllPushed", UIButtonMessage.Trigger.OnClick);
		mSendHeartAllFriendSprite = mSendHeartAllFriend.GetComponent<UISprite>();
		mNoSendHeartAllFriend = Util.CreateSprite("NoSendAll", gameObject2, image2);
		Util.SetSpriteInfo(mNoSendHeartAllFriend, "LC_button_sendAll", 53, new Vector3(0f, 0f - vector.y + 68f, 0f), Vector3.one, false);
		mNoSendHeartAllFriend.color = Color.gray;
		CheckShowAttention();
		if (mGame.CheckShowGiftDialog(GameMain.GiftUIMode.RECEIVE) != GameMain.GiftUIMode.NONE)
		{
			Mode = GameMain.GiftUIMode.RECEIVE;
		}
		if (mGame.CheckShowGiftDialog(GameMain.GiftUIMode.SEND) != GameMain.GiftUIMode.NONE)
		{
			Mode = GameMain.GiftUIMode.SEND;
		}
		if (mGame.CheckShowGiftDialog(GameMain.GiftUIMode.RESCUE) != GameMain.GiftUIMode.NONE)
		{
			Mode = GameMain.GiftUIMode.RESCUE;
		}
		ListMode = GameMain.GiftListMode.ALL;
		ActivateTab(Mode);
		ActivateTopTab(ListMode);
		SetLayout(Util.ScreenOrientation);
		mStageOpenGiftReceived = false;
	}

	private void CheckShowAttention()
	{
		if (mGame.CheckShowGiftDialog(GameMain.GiftUIMode.RECEIVE) != GameMain.GiftUIMode.NONE)
		{
			NGUITools.SetActive(mAttentionReceive.gameObject, true);
			mAttentionSaveFlgReceive = true;
		}
		else
		{
			NGUITools.SetActive(mAttentionReceive.gameObject, false);
			if (mAttentionSaveFlgReceive)
			{
				mAttentionSaveFlgReceive = false;
				mGame.Save();
			}
		}
		if (mGame.CheckShowGiftDialog(GameMain.GiftUIMode.SEND) != GameMain.GiftUIMode.NONE)
		{
			NGUITools.SetActive(mAttentionSend.gameObject, true);
			mAttentionSaveFlgSend = true;
		}
		else
		{
			NGUITools.SetActive(mAttentionSend.gameObject, false);
			if (mAttentionSaveFlgSend)
			{
				mAttentionSaveFlgSend = false;
				mGame.Save();
			}
		}
		if (mGame.CheckShowGiftDialog(GameMain.GiftUIMode.RESCUE) != GameMain.GiftUIMode.NONE)
		{
			NGUITools.SetActive(mAttentionRescue.gameObject, true);
			mAttentionSaveFlgRescue = true;
			return;
		}
		NGUITools.SetActive(mAttentionRescue.gameObject, false);
		if (mAttentionSaveFlgRescue)
		{
			mAttentionSaveFlgRescue = false;
			mGame.Save();
		}
	}

	public override void Close()
	{
		base.Close();
		if (mGiftItemList != null)
		{
			GameMain.SafeDestroy(mGiftItemList.gameObject);
			mGiftItemList = null;
		}
		if (Mode == GameMain.GiftUIMode.RECEIVE)
		{
			mGame.ClearModifiedGifts(GameMain.GiftUIMode.RECEIVE);
		}
		else if (Mode == GameMain.GiftUIMode.RESCUE)
		{
			mGame.ClearModifiedGifts(GameMain.GiftUIMode.RESCUE);
		}
		if (mAttentionSaveFlgRescue || mAttentionSaveFlgSend || mAttentionSaveFlgReceive)
		{
			mGame.Save();
		}
	}

	private void SendHeartAll()
	{
		GiftItem giftItem = new GiftItem();
		giftItem.Data.FromID = mGame.mPlayer.UUID;
		giftItem.Data.Message = mGame.mPlayer.NickName;
		giftItem.Data.ItemCategory = 20;
		giftItem.Data.ItemKind = 2;
		giftItem.Data.ItemID = 1;
		giftItem.Data.Quantity = 1;
		mSelectedGiftItem = new GiftListItem[1];
		mSelectedGiftItem[0] = new GiftListItem();
		mSelectedGiftItem[0].gift = giftItem;
		mSelectedGiftItem[0].sendCategory = giftItem.Data.ItemCategory;
		mSelectedGiftItem[0].sendkind = giftItem.Data.ItemKind;
		List<int> list = new List<int>();
		for (int i = 0; i < mGiftItemList.items.Count; i++)
		{
			if (!(mGiftItemList.items[i] is SMVerticalListItem) || mGiftItemList.items[i] is GiftListItem)
			{
				GiftListItem giftListItem = mGiftItemList.items[i] as GiftListItem;
				list.Add(giftListItem.friend.Data.UUID);
				giftListItem.sendCategory = giftItem.Data.ItemCategory;
				giftListItem.sendkind = giftItem.Data.ItemKind;
				mAllGiftItems.Add(giftListItem);
			}
		}
		mState.Reset(STATE.WAIT, true);
		if (!Server.GiveGiftAll(giftItem.Data, list))
		{
			SendGiftAllFail();
			return;
		}
		if (mSendHeartAllFriend != null)
		{
		}
		if (mSendHeartAllFriend != null && (bool)mSendHeartAllFriend.gameObject && mSendHeartAllFriend.gameObject.activeSelf)
		{
			mSendHeartAllFriend.gameObject.SetActive(false);
		}
		if (mNoSendHeartAllFriend != null && (bool)mNoSendHeartAllFriend.gameObject && !mNoSendHeartAllFriend.gameObject.activeSelf)
		{
			mNoSendHeartAllFriend.gameObject.SetActive(true);
			mNoSendHeartAllFriend.depth = 52;
		}
	}

	private void ChangeMode(GameMain.GiftUIMode mode, ChangeModeConfig config)
	{
		if (Mode != mode)
		{
			if (Mode == GameMain.GiftUIMode.RECEIVE)
			{
				mGame.ClearModifiedGifts(GameMain.GiftUIMode.RECEIVE);
			}
			else if (Mode == GameMain.GiftUIMode.RESCUE)
			{
				mGame.ClearModifiedGifts(GameMain.GiftUIMode.RESCUE);
			}
		}
		CheckShowAttention();
		LIST_TYPE lIST_TYPE = LIST_TYPE.RECEIVE_ALL;
		LIST_TYPE key = LIST_TYPE.RECEIVE_ALL;
		switch (mode)
		{
		case GameMain.GiftUIMode.RECEIVE:
			switch (ListMode)
			{
			case GameMain.GiftListMode.ALL:
				key = LIST_TYPE.RECEIVE_ALL;
				break;
			case GameMain.GiftListMode.HEART:
				key = LIST_TYPE.RECEIVE_HEART;
				break;
			case GameMain.GiftListMode.MEDAL:
				key = LIST_TYPE.RECEIVE_MEDAL;
				break;
			case GameMain.GiftListMode.OTHER:
				key = LIST_TYPE.RECEIVE_OTHER;
				break;
			}
			break;
		case GameMain.GiftUIMode.SEND:
			key = LIST_TYPE.SEND;
			break;
		case GameMain.GiftUIMode.RESCUE:
			key = LIST_TYPE.RESCUE;
			break;
		}
		switch (Mode)
		{
		case GameMain.GiftUIMode.RECEIVE:
			switch (PrevListMode)
			{
			case GameMain.GiftListMode.ALL:
				lIST_TYPE = LIST_TYPE.RECEIVE_ALL;
				break;
			case GameMain.GiftListMode.HEART:
				lIST_TYPE = LIST_TYPE.RECEIVE_HEART;
				break;
			case GameMain.GiftListMode.MEDAL:
				lIST_TYPE = LIST_TYPE.RECEIVE_MEDAL;
				break;
			case GameMain.GiftListMode.OTHER:
				lIST_TYPE = LIST_TYPE.RECEIVE_OTHER;
				break;
			}
			break;
		case GameMain.GiftUIMode.SEND:
			lIST_TYPE = LIST_TYPE.SEND;
			break;
		case GameMain.GiftUIMode.RESCUE:
			lIST_TYPE = LIST_TYPE.RESCUE;
			break;
		}
		BitArray bitArray = new BitArray(9, true);
		BitArray bitArray2 = new BitArray(9, false);
		Mode = mode;
		PrevListMode = ListMode;
		switch (config.Mode)
		{
		case ChangeModeConfig.CreateMode.TodayLabelOnly:
		{
			bitArray.SetAll(false);
			bitArray[4] = true;
			bool flag4 = false;
			foreach (int value10 in Enum.GetValues(typeof(LIST_TYPE)))
			{
				mIsTodayLimit[value10] = false;
			}
			switch (mode)
			{
			case GameMain.GiftUIMode.RECEIVE:
			{
				if (mGame.mPlayer.Gifts.Count <= 0)
				{
					break;
				}
				foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
				{
					GiftItem value6 = gift.Value;
					if (value6.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ || value6.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_RES || value6.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ || value6.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ || !mGame.CheckCorrectReceiveGiftItem(value6))
					{
						continue;
					}
					DateTime dateTime2;
					if (value6.GiftItemCategory == Def.ITEM_CATEGORY.PRESENT_HEART || (value6.GiftItemCategory == Def.ITEM_CATEGORY.CONSUME && (value6.Data.ItemKind == 2 || value6.Data.ItemKind == 1)) || (value6.GiftItemCategory == Def.ITEM_CATEGORY.FREE_BOOSTER && (value6.Data.ItemKind == 10 || value6.Data.ItemKind == 11 || value6.Data.ItemKind == 12)))
					{
						if (ListMode != 0 && ListMode != GameMain.GiftListMode.HEART)
						{
							continue;
						}
						double value7 = ((value6.GiftItemCategory != Def.ITEM_CATEGORY.PRESENT_HEART) ? ((double)Constants.GIFT_EXPIRED) : ((double)Constants.FRIENDHEART_GIFT_EXPIRED));
						dateTime2 = DateTimeUtil.GetDate(value6.Data.CreateTime).AddDays(value7);
					}
					else if (value6.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL || (value6.GiftItemCategory == Def.ITEM_CATEGORY.ADV_COIN && value6.Data.ItemKind == 1))
					{
						if (ListMode != 0 && ListMode != GameMain.GiftListMode.MEDAL)
						{
							continue;
						}
						double value8 = ((value6.GiftItemCategory != Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL) ? ((double)Constants.GIFT_EXPIRED) : ((double)Constants.FRIENDHEART_GIFT_EXPIRED));
						dateTime2 = DateTimeUtil.GetDate(value6.Data.CreateTime).AddDays(value8);
					}
					else
					{
						if (ListMode != 0 && ListMode != GameMain.GiftListMode.OTHER)
						{
							continue;
						}
						dateTime2 = DateTimeUtil.GetDate(value6.Data.CreateTime).AddDays(Constants.GIFT_EXPIRED);
					}
					TimeSpan timeSpan2 = dateTime2 - DateTime.Now;
					if (!flag4 && timeSpan2.TotalDays <= 1.0 && timeSpan2.TotalSeconds >= 0.0)
					{
						flag4 = true;
					}
				}
				LIST_TYPE lIST_TYPE6 = LIST_TYPE.RECEIVE_ALL;
				switch (ListMode)
				{
				case GameMain.GiftListMode.ALL:
					lIST_TYPE6 = LIST_TYPE.RECEIVE_ALL;
					break;
				case GameMain.GiftListMode.HEART:
					lIST_TYPE6 = LIST_TYPE.RECEIVE_HEART;
					break;
				case GameMain.GiftListMode.MEDAL:
					lIST_TYPE6 = LIST_TYPE.RECEIVE_MEDAL;
					break;
				case GameMain.GiftListMode.OTHER:
					lIST_TYPE6 = LIST_TYPE.RECEIVE_OTHER;
					break;
				}
				mIsTodayLimit[(int)lIST_TYPE6] = flag4;
				if (flag4 && mTodayLimitLabel != null)
				{
					bitArray2[4] = true;
					mTodayLimitLabel.depth = 52;
				}
				break;
			}
			case GameMain.GiftUIMode.SEND:
				mIsTodayLimit[4] = false;
				break;
			case GameMain.GiftUIMode.RESCUE:
				mIsTodayLimit[5] = false;
				break;
			}
			goto case ChangeModeConfig.CreateMode.SubOnly;
		}
		case ChangeModeConfig.CreateMode.SubOnly:
		{
			if (mTempItemList != null)
			{
				List<LIST_TYPE> list4 = new List<LIST_TYPE>();
				foreach (KeyValuePair<LIST_TYPE, ScrollListConfig> mTempItem in mTempItemList)
				{
					if (!(mTempItem.Value.ScrollData == null) && !(mTempItem.Value.ScrollData == mGiftItemList))
					{
						GameMain.SafeDestroy(mTempItem.Value.ScrollData.gameObject);
						list4.Add(mTempItem.Key);
					}
				}
				foreach (LIST_TYPE item in list4)
				{
					mIsTodayLimit[(int)item] = false;
					mTempItemList.Remove(item);
				}
			}
			foreach (int value11 in Enum.GetValues(typeof(GameMain.GiftUIMode)))
			{
				if (Mode == GameMain.GiftUIMode.NONE || Mode == GameMain.GiftUIMode.RESCUE || Mode == GameMain.GiftUIMode.SEND)
				{
					continue;
				}
				switch (value11)
				{
				case 0:
					foreach (int value12 in Enum.GetValues(typeof(GameMain.GiftListMode)))
					{
						StartCoroutine(Routine_CreateBackgroundTabItem((GameMain.GiftUIMode)value11, (GameMain.GiftListMode)value12));
					}
					break;
				default:
					StartCoroutine(Routine_CreateBackgroundTabItem((GameMain.GiftUIMode)value11));
					break;
				case -1:
					break;
				}
			}
			Vector3 zero4 = Vector3.zero;
			switch (mode)
			{
			case GameMain.GiftUIMode.RECEIVE:
				bitArray2[2] = mGiftItemList.items.Count > 0;
				bitArray2[3] = mGiftItemList.items.Count <= 0;
				bitArray2[5] = true;
				bitArray2[6] = true;
				bitArray2[7] = true;
				bitArray2[8] = true;
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					zero4.y = 90f;
				}
				else
				{
					zero4.y = 320f;
				}
				break;
			case GameMain.GiftUIMode.SEND:
				bitArray2[0] = mGiftItemList.GetListItemCount() > 0;
				bitArray2[1] = mGiftItemList.GetListItemCount() <= 0;
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					zero4.y = 150f;
				}
				else
				{
					zero4.y = 380f;
				}
				break;
			case GameMain.GiftUIMode.RESCUE:
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					zero4.y = 150f;
				}
				else
				{
					zero4.y = 380f;
				}
				break;
			}
			LIST_TYPE lIST_TYPE7 = LIST_TYPE.RECEIVE_OTHER;
			switch (ListMode)
			{
			case GameMain.GiftListMode.ALL:
				lIST_TYPE7 = LIST_TYPE.RECEIVE_ALL;
				break;
			case GameMain.GiftListMode.HEART:
				lIST_TYPE7 = LIST_TYPE.RECEIVE_HEART;
				break;
			case GameMain.GiftListMode.MEDAL:
				lIST_TYPE7 = LIST_TYPE.RECEIVE_MEDAL;
				break;
			case GameMain.GiftListMode.OTHER:
				lIST_TYPE7 = LIST_TYPE.RECEIVE_OTHER;
				break;
			}
			bitArray2[4] = mIsTodayLimit[(int)lIST_TYPE7];
			break;
		}
		case ChangeModeConfig.CreateMode.AlreadyChange:
		{
			if (!mTempItemList.ContainsKey(key) || mTempItemList[key].ScrollData == null)
			{
				goto case ChangeModeConfig.CreateMode.Force;
			}
			if (mGiftItemList != null && mGiftItemList.gameObject.activeSelf)
			{
				mGiftItemList.gameObject.SetActive(false);
			}
			if (mTempItemList.ContainsKey(lIST_TYPE))
			{
				mTempItemList[lIST_TYPE] = new ScrollListConfig
				{
					ScrollData = mGiftItemList,
					IsToday = mIsTodayLimit[(int)lIST_TYPE],
					IsCreated = true
				};
			}
			else
			{
				mTempItemList.Add(lIST_TYPE, new ScrollListConfig
				{
					ScrollData = mGiftItemList,
					IsToday = mIsTodayLimit[(int)lIST_TYPE],
					IsCreated = true
				});
			}
			bool flag = !mTempItemList[key].IsCreated;
			mGiftItemList = mTempItemList[key].ScrollData;
			mGiftItemList.gameObject.SetActive(true);
			if (mGiftItemList.transform.childCount != 0)
			{
				mGiftItemList.OnScreenChanged(Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown);
				mGiftItemList.ScrollSet((config.ScrollValue != -1f) ? config.ScrollValue : 0f);
			}
			else
			{
				mGiftItemList.OnBeforeCreate = delegate
				{
					mGiftItemList.portrait = Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown;
					mGiftItemList.OnScreenChanged(Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown);
					mGiftItemList.ScrollSet((config.ScrollValue != -1f) ? config.ScrollValue : 0f);
				};
			}
			Vector3 zero = Vector3.zero;
			Vector2 vector = Util.LogScreenSize();
			Vector2 zero2 = Vector2.zero;
			zero2 = ((!(vector.x > vector.y)) ? new Vector2(vector.y, vector.x) : new Vector2(vector.x, vector.y));
			float num = zero2.y / (zero2.x / 16f * 9f);
			switch (mode)
			{
			case GameMain.GiftUIMode.RECEIVE:
			{
				bitArray2[2] = mGiftItemList.items.Count > 0;
				bitArray2[3] = mGiftItemList.items.Count <= 0;
				bitArray2[5] = true;
				bitArray2[6] = true;
				bitArray2[7] = true;
				bitArray2[8] = true;
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					zero.y = 90f + 193f * (num - 1f);
				}
				else
				{
					zero.y = 320f + (float)WideScreenBodyHeightOffset * 0.5f;
				}
				LIST_TYPE lIST_TYPE2 = LIST_TYPE.RECEIVE_OTHER;
				switch (ListMode)
				{
				case GameMain.GiftListMode.ALL:
					lIST_TYPE2 = LIST_TYPE.RECEIVE_ALL;
					break;
				case GameMain.GiftListMode.HEART:
					lIST_TYPE2 = LIST_TYPE.RECEIVE_HEART;
					break;
				case GameMain.GiftListMode.MEDAL:
					lIST_TYPE2 = LIST_TYPE.RECEIVE_MEDAL;
					break;
				case GameMain.GiftListMode.OTHER:
					lIST_TYPE2 = LIST_TYPE.RECEIVE_OTHER;
					break;
				}
				bitArray2[4] = mIsTodayLimit[(int)lIST_TYPE2];
				break;
			}
			case GameMain.GiftUIMode.SEND:
				bitArray2[0] = ((!flag) ? (mGiftItemList.GetListItemCount() > 0) : (mGiftItemList.items.Count > 0));
				bitArray2[1] = ((!flag) ? (mGiftItemList.GetListItemCount() <= 0) : (mGiftItemList.items.Count <= 0));
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					zero.y = 150f + 193f * (num - 1f);
				}
				else
				{
					zero.y = 380f + (float)WideScreenBodyHeightOffset * 0.5f;
				}
				bitArray2[4] = mIsTodayLimit[4];
				break;
			case GameMain.GiftUIMode.RESCUE:
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					zero.y = 150f + 193f * (num - 1f);
				}
				else
				{
					zero.y = 380f + (float)WideScreenBodyHeightOffset * 0.5f;
				}
				bitArray2[4] = mIsTodayLimit[5];
				break;
			}
			if (mNoteTransform != null)
			{
				mNoteTransform.localPosition = zero;
			}
			break;
		}
		case ChangeModeConfig.CreateMode.Force:
		{
			if (mGiftItemList != null)
			{
				UnityEngine.Object.Destroy(mGiftItemList.gameObject);
				mGiftItemList = null;
			}
			if (mTempItemList != null)
			{
				foreach (ScrollListConfig value13 in mTempItemList.Values)
				{
					if (value13.ScrollData != null)
					{
						GameMain.SafeDestroy(value13.ScrollData.gameObject);
					}
				}
			}
			mTempItemList.Clear();
			foreach (int value14 in Enum.GetValues(typeof(LIST_TYPE)))
			{
				mIsTodayLimit[value14] = false;
			}
			List<SMVerticalListItem> list = new List<SMVerticalListItem>();
			List<GiftListItem> list2 = new List<GiftListItem>();
			List<GiftListItem> list3 = new List<GiftListItem>();
			Vector2 vector2 = Util.LogScreenSize();
			int num2 = -44;
			int num3 = 0;
			Vector2 zero3 = Vector2.zero;
			zero3 = ((!(vector2.x > vector2.y)) ? new Vector2(vector2.y, vector2.x) : new Vector2(vector2.x, vector2.y));
			float num4 = zero3.y / (zero3.x / 16f * 9f);
			bool flag2 = false;
			switch (mode)
			{
			case GameMain.GiftUIMode.RECEIVE:
				num3 = -10;
				num2 -= 5;
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					mNoteTransform.localPosition = new Vector3(0f, 90f + 193f * (num4 - 1f), 0f);
				}
				else
				{
					mNoteTransform.localPosition = new Vector3(0f, 320f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
				}
				mGiftItemListInfo = new SMVerticalListInfo(2, 1006f, 230f + 193f * (num4 - 1f), 1, 530f, 700 + WideScreenBodyHeightOffset, 1, 172);
				if (mGame.mPlayer.Gifts.Count > 0)
				{
					foreach (KeyValuePair<string, GiftItem> gift2 in mGame.mPlayer.Gifts)
					{
						GiftItem value2 = gift2.Value;
						if (value2.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ || value2.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_RES || value2.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ || value2.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ || !mGame.CheckCorrectReceiveGiftItem(value2) || !MessageCardDisplayCheck(value2))
						{
							continue;
						}
						DateTime dateTime;
						if (value2.GiftItemCategory == Def.ITEM_CATEGORY.PRESENT_HEART || (value2.GiftItemCategory == Def.ITEM_CATEGORY.CONSUME && (value2.Data.ItemKind == 2 || value2.Data.ItemKind == 1)) || (value2.GiftItemCategory == Def.ITEM_CATEGORY.FREE_BOOSTER && (value2.Data.ItemKind == 10 || value2.Data.ItemKind == 11 || value2.Data.ItemKind == 12)))
						{
							if (ListMode != 0 && ListMode != GameMain.GiftListMode.HEART)
							{
								continue;
							}
							double value3 = ((value2.GiftItemCategory != Def.ITEM_CATEGORY.PRESENT_HEART) ? ((double)Constants.GIFT_EXPIRED) : ((double)Constants.FRIENDHEART_GIFT_EXPIRED));
							dateTime = DateTimeUtil.GetDate(value2.Data.CreateTime).AddDays(value3);
						}
						else if (value2.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL || (value2.GiftItemCategory == Def.ITEM_CATEGORY.ADV_COIN && value2.Data.ItemKind == 1))
						{
							if (ListMode != 0 && ListMode != GameMain.GiftListMode.MEDAL)
							{
								continue;
							}
							double value4 = ((value2.GiftItemCategory != Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL) ? ((double)Constants.GIFT_EXPIRED) : ((double)Constants.FRIENDHEART_GIFT_EXPIRED));
							dateTime = DateTimeUtil.GetDate(value2.Data.CreateTime).AddDays(value4);
						}
						else
						{
							if (ListMode != 0 && ListMode != GameMain.GiftListMode.OTHER)
							{
								continue;
							}
							dateTime = DateTimeUtil.GetDate(value2.Data.CreateTime).AddDays(Constants.GIFT_EXPIRED);
						}
						TimeSpan timeSpan = dateTime - DateTime.Now;
						if (!flag2 && timeSpan.TotalDays <= 1.0 && timeSpan.TotalSeconds >= 0.0)
						{
							flag2 = true;
						}
						if (timeSpan.TotalSeconds >= 0.0)
						{
							list.Add(GiftListItem.Make(value2, null, dateTime));
						}
					}
					LIST_TYPE lIST_TYPE4 = LIST_TYPE.RECEIVE_ALL;
					switch (ListMode)
					{
					case GameMain.GiftListMode.ALL:
						lIST_TYPE4 = LIST_TYPE.RECEIVE_ALL;
						break;
					case GameMain.GiftListMode.HEART:
						lIST_TYPE4 = LIST_TYPE.RECEIVE_HEART;
						break;
					case GameMain.GiftListMode.MEDAL:
						lIST_TYPE4 = LIST_TYPE.RECEIVE_MEDAL;
						break;
					case GameMain.GiftListMode.OTHER:
						lIST_TYPE4 = LIST_TYPE.RECEIVE_OTHER;
						break;
					}
					mIsTodayLimit[(int)lIST_TYPE4] = flag2;
					if (flag2 && mTodayLimitLabel != null)
					{
						bitArray2[4] = true;
						mTodayLimitLabel.depth = 52;
					}
					if (list.Count > 0)
					{
						bitArray2[2] = true;
						if (mReceiveItemSprite != null)
						{
							mReceiveItemSprite.depth = 52;
						}
					}
					else if ((bool)mNoReceiveItem)
					{
						bitArray2[3] = true;
						mNoReceiveItem.depth = 52;
					}
					list.Sort();
				}
				else if ((bool)mNoReceiveItem.gameObject)
				{
					bitArray2[3] = true;
					mNoReceiveItem.depth = 52;
				}
				bitArray2[5] = true;
				bitArray2[6] = true;
				bitArray2[7] = true;
				bitArray2[8] = true;
				break;
			case GameMain.GiftUIMode.SEND:
			{
				num2 = -2;
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					num2 -= 10;
					mNoteTransform.localPosition = new Vector3(0f, 150f + 193f * (num4 - 1f), 0f);
				}
				else
				{
					num2 -= 8;
					mNoteTransform.localPosition = new Vector3(0f, 380f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
				}
				mGiftItemListInfo = new SMVerticalListInfo(2, 1006f, 300f + 193f * (num4 - 1f), 1, 530f, 750 + WideScreenBodyHeightOffset, 1, 172);
				bool flag3 = false;
				List<MyFriend> heartSendableFriends = mGame.GetHeartSendableFriends();
				if (heartSendableFriends != null && heartSendableFriends.Count > 0)
				{
					foreach (MyFriend item2 in heartSendableFriends)
					{
						if (item2.Data.UUID != mGame.mPlayer.UUID)
						{
							list.Add(GiftListItem.Make(null, item2, null));
						}
					}
					flag3 = true;
				}
				BIJImage image = ResourceManager.LoadImage("HUD").Image;
				if (flag3)
				{
					bitArray2[0] = true;
					if (mSendHeartAllFriendSprite != null)
					{
						mSendHeartAllFriendSprite.depth = 52;
					}
				}
				else if ((bool)mNoSendHeartAllFriend)
				{
					bitArray2[1] = true;
					mNoSendHeartAllFriend.depth = 52;
				}
				mIsTodayLimit[4] = false;
				list.Sort();
				break;
			}
			case GameMain.GiftUIMode.RESCUE:
			{
				num2 = -2;
				if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeLeft)
				{
					num2 -= 10;
					mNoteTransform.localPosition = new Vector3(0f, 150f + 193f * (num4 - 1f), 0f);
				}
				else
				{
					num2 -= 8;
					mNoteTransform.localPosition = new Vector3(0f, 380f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
				}
				mGiftItemListInfo = new SMVerticalListInfo(2, 1006f, 300f + 193f * (num4 - 1f), 1, 530f, 750 + WideScreenBodyHeightOffset, 1, 172, 30);
				if (mGame.mPlayer.Gifts.Count <= 0)
				{
					break;
				}
				foreach (KeyValuePair<string, GiftItem> gift3 in mGame.mPlayer.Gifts)
				{
					GiftItem value = gift3.Value;
					if (mGame.CheckCorrectRescueGiftItem(value))
					{
						if (value.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ)
						{
							list2.Add(GiftListItem.Make(value, null, null));
						}
						else if (value.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ)
						{
							list3.Add(GiftListItem.Make(value, null, null));
						}
						else if (value.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ)
						{
							list.Add(GiftListItem.Make(value, null, value.Data.GetRemoteTimeToLocal().AddMinutes(Constants.FRIENDHELP_EXPIRED)));
						}
					}
				}
				mIsTodayLimit[5] = false;
				list2.Sort(GiftItemSort);
				list3.Sort(GiftItemSort);
				for (int i = 0; i < list2.Count; i++)
				{
					list.Add(list2[i]);
				}
				for (int j = 0; j < list3.Count; j++)
				{
					list.Add(list3[j]);
				}
				break;
			}
			}
			num3 -= 10;
			bool portrait = Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown;
			mGiftItemList = SMVerticalList.Make(mDialogCenter, this, mGiftItemListInfo, list, 0f, 0f, mBaseDepth + 1, portrait, UIScrollView.Movement.Vertical, true);
			Vector3 localPosition = mGiftItemList.transform.localPosition;
			localPosition.x = num3;
			localPosition.y = num2;
			mGiftItemList.transform.localPosition = localPosition;
			mGiftItemList.ScrollBarPositionOffsetPortrait = new Vector3(-5f, 0f);
			mGiftItemList.ScrollBarPositionOffsetLandscape = new Vector3(13f, 0f);
			mGiftItemList.OnBeforeCreate = delegate
			{
				if (config.ScrollValue != -1f)
				{
					mGiftItemList.ScrollSet(config.ScrollValue);
				}
			};
			if (mode == GameMain.GiftUIMode.SEND)
			{
				mGiftItemList.OnCreateItem = OnCreateItem_Friend;
			}
			else
			{
				mGiftItemList.OnCreateItem = OnCreateItem_Gift;
			}
			if (mTempItemList.ContainsKey(key))
			{
				mTempItemList[key] = new ScrollListConfig
				{
					ScrollData = mGiftItemList,
					IsToday = flag2,
					IsCreated = false
				};
			}
			else
			{
				mTempItemList.Add(key, new ScrollListConfig
				{
					ScrollData = mGiftItemList,
					IsToday = flag2,
					IsCreated = false
				});
			}
			mState.Change(STATE.LIST_INIT_WAIT);
			foreach (int value15 in Enum.GetValues(typeof(GameMain.GiftUIMode)))
			{
				if (Mode == GameMain.GiftUIMode.NONE || (Mode == GameMain.GiftUIMode.RESCUE && value15 == (int)Mode) || (Mode == GameMain.GiftUIMode.SEND && value15 == (int)Mode))
				{
					continue;
				}
				switch (value15)
				{
				case 0:
					foreach (int value16 in Enum.GetValues(typeof(GameMain.GiftListMode)))
					{
						StartCoroutine(Routine_CreateBackgroundTabItem((GameMain.GiftUIMode)value15, (GameMain.GiftListMode)value16));
					}
					break;
				default:
					StartCoroutine(Routine_CreateBackgroundTabItem((GameMain.GiftUIMode)value15));
					break;
				case -1:
					break;
				}
			}
			break;
		}
		}
		if (bitArray[0] && mSendHeartAllFriend != null && (bool)mSendHeartAllFriend.gameObject && mSendHeartAllFriend.gameObject.activeSelf != bitArray2[0])
		{
			mSendHeartAllFriend.gameObject.SetActive(bitArray2[0]);
		}
		if (bitArray[1] && mNoSendHeartAllFriend != null && (bool)mNoSendHeartAllFriend.gameObject && mNoSendHeartAllFriend.gameObject.activeSelf != bitArray2[1])
		{
			mNoSendHeartAllFriend.gameObject.SetActive(bitArray2[1]);
		}
		if (bitArray[2] && mReceiveItem != null && (bool)mReceiveItem.gameObject && mReceiveItem.gameObject.activeSelf != bitArray2[2])
		{
			mReceiveItem.gameObject.SetActive(bitArray2[2]);
		}
		if (bitArray[3] && mNoReceiveItem != null && (bool)mNoReceiveItem.gameObject && mNoReceiveItem.gameObject.activeSelf != bitArray2[3])
		{
			mNoReceiveItem.gameObject.SetActive(bitArray2[3]);
		}
		if (bitArray[4] && mTodayLimitLabel != null && (bool)mTodayLimitLabel.gameObject && mTodayLimitLabel.gameObject.activeSelf != bitArray2[4])
		{
			mTodayLimitLabel.gameObject.SetActive(bitArray2[4]);
		}
		if (bitArray[5] && mTabButtonAll != null && (bool)mTabButtonAll.gameObject && mTabButtonAll.gameObject.activeSelf != bitArray2[5])
		{
			mTabButtonAll.gameObject.SetActive(bitArray2[5]);
		}
		if (bitArray[6] && mTabButtonHeart != null && (bool)mTabButtonHeart.gameObject && mTabButtonHeart.gameObject.activeSelf != bitArray2[6])
		{
			mTabButtonHeart.gameObject.SetActive(bitArray2[6]);
		}
		if (bitArray[7] && mTabButtonMedal != null && (bool)mTabButtonMedal.gameObject && mTabButtonMedal.gameObject.activeSelf != bitArray2[7])
		{
			mTabButtonMedal.gameObject.SetActive(bitArray2[7]);
		}
		if (bitArray[8] && mTabButtonOther != null && (bool)mTabButtonOther.gameObject && mTabButtonOther.gameObject.activeSelf != bitArray2[8])
		{
			mTabButtonOther.gameObject.SetActive(bitArray2[8]);
		}
	}

	private void ChangeListMode(GameMain.GiftListMode mode)
	{
		ListMode = mode;
		ChangeMode(GameMain.GiftUIMode.RECEIVE, ChangeModeConfig.Create());
	}

	private void ActivateTopTab(GameMain.GiftListMode mode)
	{
		switch (mode)
		{
		case GameMain.GiftListMode.ALL:
			Util.SetImageButtonColor(mTabButtonAll, Color.white);
			Util.SetImageButtonColor(mTabButtonHeart, Color.gray);
			Util.SetImageButtonColor(mTabButtonOther, Color.gray);
			break;
		case GameMain.GiftListMode.HEART:
			Util.SetImageButtonColor(mTabButtonAll, Color.gray);
			Util.SetImageButtonColor(mTabButtonHeart, Color.white);
			Util.SetImageButtonColor(mTabButtonOther, Color.gray);
			break;
		case GameMain.GiftListMode.MEDAL:
			Util.SetImageButtonColor(mTabButtonAll, Color.gray);
			Util.SetImageButtonColor(mTabButtonHeart, Color.gray);
			Util.SetImageButtonColor(mTabButtonOther, Color.gray);
			break;
		case GameMain.GiftListMode.OTHER:
			Util.SetImageButtonColor(mTabButtonAll, Color.gray);
			Util.SetImageButtonColor(mTabButtonHeart, Color.gray);
			Util.SetImageButtonColor(mTabButtonOther, Color.white);
			break;
		}
	}

	private void ActivateTab(GameMain.GiftUIMode mode)
	{
		switch (mode)
		{
		case GameMain.GiftUIMode.RECEIVE:
			Util.SetImageButtonColor(mTabButtonReceive, Color.white);
			Util.SetImageButtonColor(mTabButtonSend, Color.gray);
			Util.SetImageButtonColor(mTabButtonRescue, Color.gray);
			break;
		case GameMain.GiftUIMode.SEND:
			Util.SetImageButtonColor(mTabButtonReceive, Color.gray);
			Util.SetImageButtonColor(mTabButtonSend, Color.white);
			Util.SetImageButtonColor(mTabButtonRescue, Color.gray);
			break;
		case GameMain.GiftUIMode.RESCUE:
			Util.SetImageButtonColor(mTabButtonReceive, Color.gray);
			Util.SetImageButtonColor(mTabButtonSend, Color.gray);
			Util.SetImageButtonColor(mTabButtonRescue, Color.white);
			break;
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void EnableList(bool enabled)
	{
		if (!(mGiftItemList == null))
		{
			NGUITools.SetActive(mGiftItemList.gameObject, enabled);
		}
	}

	private void GetGiftItemDisplayData(GiftItem gift, out string spriteName, out string buttonImage, out string message, out BIJImage atlas)
	{
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		buttonImage = string.Empty;
		spriteName = string.Empty;
		atlas = image2;
		message = string.Empty;
		GiftItemData data = gift.Data;
		switch (gift.GiftItemCategory)
		{
		case Def.ITEM_CATEGORY.CONSUME:
			GetConsumeItemDisplayData((Def.CONSUME_ID)data.ItemKind, data.Quantity, data.ItemID == 2, out spriteName, out message);
			buttonImage = "LC_button_receive";
			break;
		case Def.ITEM_CATEGORY.ACCESSORY:
			GetAccessoryDisplayData(data.ItemKind, data.Quantity, out spriteName, out message, out atlas);
			buttonImage = "LC_button_receive";
			break;
		case Def.ITEM_CATEGORY.BOOSTER:
		case Def.ITEM_CATEGORY.FREE_BOOSTER:
			GetBoosterDisplayData(data.ItemKind, data.Quantity, data.ItemID == 2, out spriteName, out message);
			buttonImage = "LC_button_receive";
			break;
		case Def.ITEM_CATEGORY.COMPANION:
		{
			CompanionData companionData = null;
			int itemKind2 = data.ItemKind;
			if (mGame.mCompanionData.Count > itemKind2 && !mGame.mPlayer.IsCompanionUnlock(itemKind2))
			{
				companionData = mGame.mCompanionData[itemKind2];
				message = Localization.Get(companionData.name);
				atlas = image;
				spriteName = companionData.iconName;
				buttonImage = "LC_button_receive";
			}
			break;
		}
		case Def.ITEM_CATEGORY.WALLPAPER:
			message = Localization.Get("ConsumeItemDesc_WallPaper") + " " + Localization.Get("Purchase_mul") + " " + data.Quantity;
			atlas = image2;
			spriteName = "ac_icon_wallpaper_complete";
			buttonImage = "LC_button_receive";
			break;
		case Def.ITEM_CATEGORY.WALLPAPER_PIECE:
			message = Localization.Get("WallpaperPiece") + " " + Localization.Get("Purchase_mul") + " " + data.Quantity;
			atlas = image2;
			spriteName = "icon_wallpaper";
			buttonImage = "LC_button_receive";
			break;
		case Def.ITEM_CATEGORY.FRIEND_REQ:
			message = Util.MakeLText("FriendBeRequest");
			buttonImage = "LC_button_approval";
			break;
		case Def.ITEM_CATEGORY.FRIEND_RES:
			break;
		case Def.ITEM_CATEGORY.STAGEHELP_REQ:
			break;
		case Def.ITEM_CATEGORY.STAGEHELP_RES:
			message = Util.MakeLText("FriendHelp_Result00");
			buttonImage = "LC_button_ok";
			break;
		case Def.ITEM_CATEGORY.ROADBLOCK_REQ:
			message = Util.MakeLText("RoadBrockBeRequest");
			buttonImage = "LC_button_approval";
			break;
		case Def.ITEM_CATEGORY.ROADBLOCK_RES:
			break;
		case Def.ITEM_CATEGORY.STAGE_OPEN:
		case Def.ITEM_CATEGORY.STAGE_OPEN_BUNDLE:
		{
			Def.SERIES itemKind = (Def.SERIES)data.ItemKind;
			string key = Def.SeriesLanguage[itemKind];
			string empty = string.Empty;
			string arg = string.Empty;
			int itemID = data.ItemID;
			int a_main;
			int a_sub;
			if (Def.IsEventSeries(itemKind))
			{
				int a_eventID;
				short a_course;
				if (gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGE_OPEN)
				{
					Def.SplitServerStageNo(itemID, out a_eventID, out a_course, out a_main, out a_sub);
				}
				else
				{
					Def.SplitServerStageNo(itemID * 100, out a_eventID, out a_course, out a_main, out a_sub);
				}
				empty = ((!mGame.mEventData.InSessionEventList.ContainsKey(a_eventID)) ? Localization.Get("EventPage") : Localization.Get(mGame.mEventData.InSessionEventList[a_eventID].Name));
				string text = string.Empty;
				SMEventPageData eventPageData = GameMain.GetEventPageData(a_eventID);
				if (eventPageData != null)
				{
					SMMapStageSetting mapStage = eventPageData.GetMapStage(a_main, a_sub, a_course);
					arg = ((mapStage == null) ? (string.Empty + a_main) : mapStage.DisplayStageNo);
					switch (eventPageData.EventSetting.EventType)
					{
					case Def.EVENT_TYPE.SM_BINGO:
						text = Localization.Get("Sheet") + (a_course + 1) + " ";
						break;
					default:
						text = string.Format(Localization.Get("SeasonEvent_CourseNum"), a_course + 1) + " ";
						break;
					case Def.EVENT_TYPE.SM_STAR:
						break;
					}
				}
				arg = text + string.Format(Localization.Get("Win_Title"), arg);
			}
			else
			{
				empty = Localization.Get(key);
				if (gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGE_OPEN)
				{
					Def.SplitStageNo(itemID, out a_main, out a_sub);
				}
				else if (gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGE_OPEN_BUNDLE)
				{
					a_main = itemID;
					a_sub = 0;
				}
				else
				{
					a_main = 1;
					a_sub = 0;
				}
				arg = string.Empty + a_main;
				if (a_sub > 0)
				{
					arg = arg + "-" + a_sub;
				}
				arg = string.Format(Localization.Get("Win_Title"), arg);
			}
			empty = empty + "\n" + arg;
			message = empty;
			buttonImage = "LC_button_receive";
			break;
		}
		case Def.ITEM_CATEGORY.PRESENT_BOOSTER:
			buttonImage = "LC_button_receive";
			break;
		case Def.ITEM_CATEGORY.PRESENT_GOODJOB:
			buttonImage = "LC_button_ok";
			break;
		case Def.ITEM_CATEGORY.PRESENT_HEART:
		{
			spriteName = "icon_heart";
			buttonImage = "LC_button_receive";
			string _str = Localization.Get("Gift_ItemNameHeart");
			StringUtils.CheckPlural(data.Quantity, ref _str);
			message = _str + " " + Localization.Get("Purchase_mul") + " " + data.Quantity;
			break;
		}
		case Def.ITEM_CATEGORY.RELEASE_TIMECHEAT:
			buttonImage = "LC_button_receive";
			break;
		case Def.ITEM_CATEGORY.BINGO_TICKET:
		{
			int quantity2 = data.Quantity;
			spriteName = "icon_event_ticket00";
			buttonImage = "LC_button_receive";
			string reLotteryTicketName = GetReLotteryTicketName(data.ItemKind, quantity2);
			message = reLotteryTicketName + " " + Localization.Get("Purchase_mul") + " " + quantity2;
			break;
		}
		case Def.ITEM_CATEGORY.LABYRINTH_JEWEL:
		{
			int quantity = data.Quantity;
			spriteName = "icon_currency_jewel";
			buttonImage = "LC_button_receive";
			message = GetLabyrinthItemName(data.ItemKind, quantity, "Labyrinth_Jwel");
			break;
		}
		case Def.ITEM_CATEGORY.MESSAGE_CARD:
			spriteName = string.Empty;
			buttonImage = "LC_button_receive";
			message = Localization.Get("MessageCard");
			break;
		case Def.ITEM_CATEGORY.EVENT:
		case Def.ITEM_CATEGORY.STORY_DEMO:
		case Def.ITEM_CATEGORY.SKIN:
		case Def.ITEM_CATEGORY.TUTORIAL:
		case Def.ITEM_CATEGORY.ADV_FIGURE:
		case Def.ITEM_CATEGORY.ADV_CONSUME:
		case Def.ITEM_CATEGORY.ADV_COIN:
		case Def.ITEM_CATEGORY.ADV_STAGE_CLEAR:
		case Def.ITEM_CATEGORY.ADV_STAGE_BUNDLE_CLEAR:
		case Def.ITEM_CATEGORY.ADV_DUNGEON_CLEAR:
		case Def.ITEM_CATEGORY.ADV_DUNGEON_BUNDLE_CLEAR:
		case Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL:
			break;
		}
	}

	private void GetAccessoryDisplayData(int a_id, int a_quantity, out string spriteName, out string message, out BIJImage atlas)
	{
		AccessoryData accessoryData = null;
		spriteName = string.Empty;
		message = string.Empty;
		atlas = ResourceManager.LoadImage("HUD").Image;
		if (mGame.mAccessoryData.Count <= a_id)
		{
			return;
		}
		accessoryData = mGame.mAccessoryData[a_id];
		switch (accessoryData.AccessoryType)
		{
		case AccessoryData.ACCESSORY_TYPE.PARTNER:
		{
			CompanionData companionData = null;
			if (!mGame.mPlayer.IsCompanionUnlock(a_id))
			{
				int index = int.Parse(accessoryData.GotID);
				companionData = mGame.mCompanionData[index];
				message = Localization.Get(companionData.name);
				spriteName = companionData.iconName;
				atlas = ResourceManager.LoadImage("ICON").Image;
			}
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.BOOSTER:
		{
			Def.BOOSTER_KIND a_kind = Def.BOOSTER_KIND.NONE;
			int a_num = 0;
			accessoryData.GetBooster(out a_kind, out a_num);
			BoosterData boosterData = mGame.mBoosterData[(int)a_kind];
			message = Localization.Get(boosterData.name) + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = boosterData.iconBuy;
			break;
		}
		case AccessoryData.ACCESSORY_TYPE.GEM:
			message = Localization.Get("Gift_ItemNameSD") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_currency_jem";
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART:
			message = Localization.Get("Gift_ItemNameHeart") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_heart";
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY:
			message = Localization.Get("KeyItemInfo_RibbonKey_Title") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_loadblock_key";
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_R:
			message = Localization.Get("KeyItemInfo_RibbonKey_Title") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_loadblock_key_R";
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_S:
			message = Localization.Get("KeyItemInfo_RibbonKey_Title") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_loadblock_key_S";
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_SS:
			message = Localization.Get("KeyItemInfo_RibbonKey_Title") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_loadblock_key_SS";
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_STARS:
			message = Localization.Get("KeyItemInfo_RibbonKey_Title") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_loadblock_key_STARS";
			break;
		case AccessoryData.ACCESSORY_TYPE.RB_KEY_GF00:
			message = Localization.Get("KeyItemInfo_RibbonKey_Title") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_loadblock_key_GF00";
			break;
		case AccessoryData.ACCESSORY_TYPE.ERB_KEY:
			message = Localization.Get("ConsumeItemDesc_EVRoadBlockKey") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_eventroadblock_key";
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER:
			message = Localization.Get("ConsumeItemDesc_WallPaper") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "ac_icon_wallpaper_complete";
			break;
		case AccessoryData.ACCESSORY_TYPE.WALL_PAPER_PIECE:
			message = Localization.Get("WallpaperPiece") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_wallpaper";
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP:
			message = Localization.Get("ConsumeItemDesc_Growup") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_level";
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_F:
			message = Localization.Get("ConsumeItemDesc_Growup") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_level";
			break;
		case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
			message = Localization.Get("ConsumeItemDesc_GrowupPiece") + " " + Localization.Get("Purchase_mul") + " " + accessoryData.GetGotIDByNum() * a_quantity;
			spriteName = "icon_dailyEvent_stamp00";
			break;
		case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
			message = Localization.Get("ConsumeItemDesc_HeartPiece") + " " + Localization.Get("Purchase_mul") + " " + accessoryData.GetGotIDByNum() * a_quantity;
			spriteName = "icon_dailyEvent_stamp01";
			break;
		case AccessoryData.ACCESSORY_TYPE.TROPHY:
			message = Localization.Get("Ranking_TrophyNum") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_ranking_trophy";
			break;
		case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
			message = Localization.Get("ConsumeItemDesc_OldEventKey") + " " + Localization.Get("Purchase_mul") + " " + a_quantity;
			spriteName = "icon_old_event_key";
			break;
		case AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY:
			message = GetLabyrinthItemName(accessoryData.EventID, a_quantity, "Labyrinth_Course_Key");
			spriteName = "icon_eventroadblock_key02";
			break;
		case AccessoryData.ACCESSORY_TYPE.SP_DAILY:
		case AccessoryData.ACCESSORY_TYPE.REVIVAL_EVENT:
		case AccessoryData.ACCESSORY_TYPE.ADV_ITEM:
		case AccessoryData.ACCESSORY_TYPE.BINGO_KEY:
		case AccessoryData.ACCESSORY_TYPE.BINGO_TICKET:
			break;
		}
	}

	private void GetConsumeItemDisplayData(Def.CONSUME_ID a_id, int a_quantity, bool a_set, out string spriteName, out string message)
	{
		spriteName = string.Empty;
		message = string.Empty;
		bool flag = true;
		string text = string.Empty;
		string _str = string.Empty;
		if (!a_set)
		{
			text = Localization.Get("Purchase_mul") + " ";
		}
		switch (a_id)
		{
		case Def.CONSUME_ID.HEART:
		case Def.CONSUME_ID.HEART_EX:
			spriteName = "icon_heart";
			_str = Localization.Get("Gift_ItemNameHeart");
			break;
		case Def.CONSUME_ID.HEART_PIECE:
			spriteName = "icon_dailyEvent_stamp01";
			_str = Localization.Get("ConsumeItemDesc_HeartPiece");
			break;
		case Def.CONSUME_ID.GROWUP:
			spriteName = "icon_level";
			_str = Localization.Get("ConsumeItemDesc_Growup");
			break;
		case Def.CONSUME_ID.GROWUP_PIECE:
			spriteName = "icon_dailyEvent_stamp00";
			_str = Localization.Get("ConsumeItemDesc_GrowupPiece");
			break;
		case Def.CONSUME_ID.MAIN_CURRENCY:
		case Def.CONSUME_ID.MAIN_CURRENCY_EX:
			spriteName = "icon_currency_jem";
			_str = Localization.Get("Gift_ItemNameSD");
			break;
		case Def.CONSUME_ID.ROADBLOCK_KEY:
			spriteName = "icon_loadblock_key";
			_str = Localization.Get("KeyItemInfo_RibbonKey_Title");
			break;
		case Def.CONSUME_ID.ROADBLOCK_KEY_R:
			spriteName = "icon_loadblock_key_R";
			_str = Localization.Get("KeyItemInfo_RibbonKey_Title");
			break;
		case Def.CONSUME_ID.ROADBLOCK_KEY_S:
			spriteName = "icon_loadblock_key_S";
			_str = Localization.Get("KeyItemInfo_RibbonKey_Title");
			break;
		case Def.CONSUME_ID.ROADBLOCK_KEY_SS:
			spriteName = "icon_loadblock_key_SS";
			_str = Localization.Get("KeyItemInfo_RibbonKey_Title");
			break;
		case Def.CONSUME_ID.ROADBLOCK_KEY_STARS:
			spriteName = "icon_loadblock_key_STARS";
			_str = Localization.Get("KeyItemInfo_RibbonKey_Title");
			break;
		case Def.CONSUME_ID.ROADBLOCK_KEY_GF00:
			spriteName = "icon_loadblock_key_GF00";
			_str = Localization.Get("KeyItemInfo_RibbonKey_Title");
			break;
		case Def.CONSUME_ID.ROADBLOCK_TICKET:
			spriteName = "icon_ticket";
			_str = Localization.Get("ConsumeItemDesc_RoadBlockTicket");
			break;
		case Def.CONSUME_ID.TROPHY:
			spriteName = "icon_ranking_trophy";
			_str = Localization.Get("Ranking_TrophyNum");
			flag = false;
			break;
		case Def.CONSUME_ID.OLDEVENT_KEY:
			spriteName = SMDImageUtil.GetHUDIconKey(1, (int)a_id, a_quantity);
			_str = Localization.Get("ConsumeItemDesc_OldEventKey");
			break;
		}
		if (flag)
		{
			StringUtils.CheckPlural(a_quantity, ref _str);
		}
		message = _str + " " + text + a_quantity;
	}

	private void GetBoosterDisplayData(int a_id, int a_quantity, bool a_set, out string spriteName, out string message)
	{
		spriteName = string.Empty;
		message = string.Empty;
		if (mGame.mBoosterData.Count > a_id)
		{
			string text = string.Empty;
			if (!a_set)
			{
				text = Localization.Get("Purchase_mul") + " ";
			}
			BoosterData boosterData = mGame.mBoosterData[a_id];
			message = Localization.Get(boosterData.name) + " " + text + a_quantity;
			spriteName = boosterData.iconBuy;
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		float num = vector.y * 0.5f;
		float num2 = 0f - num;
		float num3 = vector.x * 0.5f;
		float num4 = 0f - num3;
		float num5 = 70f;
		float y = -10f;
		if (Util.IsWideScreen())
		{
			Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopLeft, o);
			Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.BottomRight, o);
			num = vector2.y;
			num2 = vector3.y;
			num4 = vector2.x;
			num3 = vector3.x;
			y = 0f;
		}
		Vector3 localPosition = mBackGround.transform.localPosition;
		localPosition.y = (0f - vector.y) * 0.5f - 2f;
		mBackGround.transform.localPosition = localPosition;
		mBackGround.transform.localScale = new Vector3(1f, (Util.LogScreenSize().y + 2f) / 910f, 1f);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mBody.SetLocalPositionY(WideScreenBodyPoratraitOffset);
			mTitleBarR.transform.localPosition = new Vector3(-2f, num + 50f + 123f);
			mTitleBarL.transform.localPosition = new Vector3(2f, num + 50f + 123f);
			mTitle.transform.localPosition = new Vector3(0f, num - 28f);
			mTodayLimitLabel.transform.localPosition = new Vector3(14f, num2 + 117f);
			mBackButton.transform.localPosition = new Vector3(num4 + num5, num2 + 80f);
			mReceiveItem.transform.localPosition = new Vector3(0f, num2 + 68f);
			mNoReceiveItem.transform.localPosition = new Vector3(0f, num2 + 68f);
			mSendHeartAllFriend.transform.localPosition = new Vector3(0f, num2 + 68f);
			mNoSendHeartAllFriend.transform.localPosition = new Vector3(0f, num2 + 68f);
			mTabButtonReceive.transform.localPosition = new Vector3(-175f, 450f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
			mTabButtonSend.transform.localPosition = new Vector3(-10f, 450f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
			mTabButtonRescue.transform.localPosition = new Vector3(155f, 450f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
			mTabButtonAll.transform.localPosition = new Vector3(-140f, 365f + (float)WideScreenBodyHeightOffset * 0.5f);
			mTabButtonHeart.transform.localPosition = new Vector3(-15f, 365f + (float)WideScreenBodyHeightOffset * 0.5f);
			mTabButtonOther.transform.localPosition = new Vector3(110f, 365f + (float)WideScreenBodyHeightOffset * 0.5f);
			mAttentionReceive.transform.localPosition = new Vector3(66f, 5f, 0f);
			mAttentionSend.transform.localPosition = new Vector3(66f, 5f, 0f);
			mAttentionRescue.transform.localPosition = new Vector3(66f, 5f, 0f);
			mImageTab0 = "LC_mail_tabtop00";
			mImageTab1 = "LC_mail_tabtop01";
			mImageTab2 = "LC_mail_tabtop02";
			mImageTab3 = "LC_button_mail_all";
			mImageTab4 = "LC_button_mail_heart";
			mImageTab5 = "LC_button_mail_medal";
			mImageTab6 = "LC_button_mail_other";
			Util.SetImageButtonGraphic(mTabButtonReceive, mImageTab0, mImageTab0, mImageTab0);
			Util.SetImageButtonGraphic(mTabButtonSend, mImageTab1, mImageTab1, mImageTab1);
			Util.SetImageButtonGraphic(mTabButtonRescue, mImageTab2, mImageTab2, mImageTab2);
			Util.SetImageButtonGraphic(mTabButtonAll, mImageTab3, mImageTab3, mImageTab3);
			Util.SetImageButtonGraphic(mTabButtonHeart, mImageTab4, mImageTab4, mImageTab4);
			Util.SetImageButtonGraphic(mTabButtonOther, mImageTab6, mImageTab6, mImageTab6);
			mBasePosY = -29f;
			mDialogFrame.SetDimensions(600, 865 + WideScreenBodyHeightOffset);
			mDialogFrame.transform.localPosition = new Vector3(0f, -5f);
			if ((bool)mGiftItemList)
			{
				mGiftItemList.OnScreenChanged(true);
			}
			if ((bool)mNoteTransform)
			{
				switch (Mode)
				{
				case GameMain.GiftUIMode.RECEIVE:
					mNoteTransform.localPosition = new Vector3(0f, 320f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
					break;
				case GameMain.GiftUIMode.SEND:
				case GameMain.GiftUIMode.RESCUE:
					mNoteTransform.localPosition = new Vector3(0f, 380f + (float)WideScreenBodyHeightOffset * 0.5f, 0f);
					break;
				}
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			Vector2 zero = Vector2.zero;
			zero = ((!(vector.x > vector.y)) ? new Vector2(vector.y, vector.x) : new Vector2(vector.x, vector.y));
			float num6 = zero.y / (zero.x / 16f * 9f);
			mBody.SetLocalPositionY(y);
			mTitleBarR.transform.localPosition = new Vector3(-2f, num + 50f + 123f);
			mTitleBarL.transform.localPosition = new Vector3(2f, num + 50f + 123f);
			mTitle.transform.localPosition = new Vector3(0f, num - 28f);
			mTodayLimitLabel.transform.localPosition = new Vector3(14f, num2 + 117f);
			mBackButton.transform.localPosition = new Vector3(num4 + num5, num2 + 80f);
			mReceiveItem.transform.localPosition = new Vector3(0f, num2 + 68f);
			mNoReceiveItem.transform.localPosition = new Vector3(0f, num2 + 68f);
			mSendHeartAllFriend.transform.localPosition = new Vector3(0f, num2 + 68f);
			mNoSendHeartAllFriend.transform.localPosition = new Vector3(0f, num2 + 68f);
			float y2 = 210f + 193f * (num6 - 1f);
			mTabButtonReceive.transform.localPosition = new Vector3(-190f, y2, 0f);
			mTabButtonSend.transform.localPosition = new Vector3(-20f, y2, 0f);
			mTabButtonRescue.transform.localPosition = new Vector3(150f, y2, 0f);
			y2 = 135f + 193f * (num6 - 1f);
			mTabButtonAll.transform.localPosition = new Vector3(-140f, y2);
			mTabButtonHeart.transform.localPosition = new Vector3(-15f, y2);
			mTabButtonOther.transform.localPosition = new Vector3(110f, y2);
			mAttentionReceive.transform.localPosition = new Vector3(66f, 5f, 0f);
			mAttentionSend.transform.localPosition = new Vector3(66f, 5f, 0f);
			mAttentionRescue.transform.localPosition = new Vector3(66f, 5f, 0f);
			mImageTab0 = "LC_mail_tabtop00";
			mImageTab1 = "LC_mail_tabtop01";
			mImageTab2 = "LC_mail_tabtop02";
			mImageTab3 = "LC_button_mail_all";
			mImageTab4 = "LC_button_mail_heart";
			mImageTab5 = "LC_button_mail_medal";
			mImageTab6 = "LC_button_mail_other";
			Util.SetImageButtonGraphic(mTabButtonReceive, mImageTab0, mImageTab0, mImageTab0);
			Util.SetImageButtonGraphic(mTabButtonSend, mImageTab1, mImageTab1, mImageTab1);
			Util.SetImageButtonGraphic(mTabButtonRescue, mImageTab2, mImageTab2, mImageTab2);
			Util.SetImageButtonGraphic(mTabButtonAll, mImageTab3, mImageTab3, mImageTab3);
			Util.SetImageButtonGraphic(mTabButtonHeart, mImageTab4, mImageTab4, mImageTab4);
			Util.SetImageButtonGraphic(mTabButtonOther, mImageTab6, mImageTab6, mImageTab6);
			mBasePosY = 50f;
			y2 = num6 * 375f;
			mDialogFrame.SetDimensions(1110, (int)y2);
			mDialogFrame.transform.localPosition = new Vector3(0f, 0f);
			if ((bool)mGiftItemList)
			{
				if (mGiftItemList.items.Count % 2 != 0 && (mAdvNetworkInfo == null || mAdvNetworkInfo.Count == 0))
				{
					ChangeMode(Mode, new ChangeModeConfig
					{
						Mode = ChangeModeConfig.CreateMode.Force
					});
				}
				else
				{
					mGiftItemList.OnScreenChanged(false);
				}
			}
			if ((bool)mNoteTransform)
			{
				switch (Mode)
				{
				case GameMain.GiftUIMode.RECEIVE:
					mNoteTransform.localPosition = new Vector3(0f, 90f + 193f * (num6 - 1f), 0f);
					break;
				case GameMain.GiftUIMode.SEND:
				case GameMain.GiftUIMode.RESCUE:
					mNoteTransform.localPosition = new Vector3(0f, 150f + 193f * (num6 - 1f), 0f);
					break;
				}
			}
			break;
		}
		}
	}

	private void AddMultiReceiveItemCount(string item_name, int count)
	{
		if (mMultiReceiveItemCount.ContainsKey(item_name))
		{
			Dictionary<string, int> dictionary;
			Dictionary<string, int> dictionary2 = (dictionary = mMultiReceiveItemCount);
			string key;
			string key2 = (key = item_name);
			int num = dictionary[key];
			dictionary2[key2] = num + count;
		}
		else
		{
			mMultiReceiveItemCount.Add(item_name, count);
		}
	}

	private void SetCompleteNoticeData(string disp_name, bool enable_next_separator = true)
	{
		string[] array = disp_name.Split('\n');
		if (array != null)
		{
			string[] array2 = array;
			foreach (string message in array2)
			{
				mCompleteNotifyDesc.Add(new ScrollBoxDialog.CreateConfig.DescriptionInfo
				{
					Message = message,
					Color = Def.DEFAULT_MESSAGE_COLOR,
					IgnoreNextSeparator = !enable_next_separator
				});
			}
		}
	}

	public void PermissionToOperate(bool flag)
	{
		if (WaitOperatingCount > 0 || IsOperating == flag)
		{
			return;
		}
		IsOperating = flag;
		Collider[] componentsInChildren = GetComponentsInChildren<Collider>(true);
		Collider[] array = componentsInChildren;
		foreach (Collider collider in array)
		{
			if (collider != null)
			{
				collider.enabled = flag;
			}
		}
	}

	private bool IsAdvItem(int category)
	{
		bool flag = false;
		switch (category)
		{
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
			return true;
		default:
			return false;
		}
	}

	public int GiftItemSort(GiftListItem a, GiftListItem b)
	{
		int num = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(a.gift.Data.CreateTime);
		int num2 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(b.gift.Data.CreateTime);
		if (num2 > num)
		{
			return 1;
		}
		if (num > num2)
		{
			return -1;
		}
		if (b.gift.Data.FromID > a.gift.Data.FromID)
		{
			return 1;
		}
		if (a.gift.Data.FromID > b.gift.Data.FromID)
		{
			return -1;
		}
		return 0;
	}

	private int GetCramGetType(GiftItem gift)
	{
		int result = 0;
		switch (gift.GetGiftSource())
		{
		case GiftMailType.LOCAL:
			result = 2;
			break;
		case GiftMailType.LOCAL_EVENT:
			result = 3;
			break;
		case GiftMailType.REWARD:
			result = 4;
			break;
		case GiftMailType.GIFT:
			result = 6;
			break;
		case GiftMailType.LOCAL_MAP:
			result = 7;
			break;
		case GiftMailType.LOCAL_TROPHY:
			result = 13;
			break;
		case GiftMailType.SUPPORT:
			result = 99;
			break;
		}
		return result;
	}

	private string GetReLotteryTicketName(int eventID, int num)
	{
		string _str = Localization.Get("ReLotTicket");
		StringUtils.CheckPlural(num, ref _str);
		if (mGame.mEventData.InSessionEventList.ContainsKey(eventID))
		{
			SMSeasonEventSetting sMSeasonEventSetting = mGame.mEventData.InSessionEventList[eventID];
			_str = _str + "\n(" + Localization.Get(sMSeasonEventSetting.Name) + ")";
		}
		return _str;
	}

	private string GetLabyrinthItemName(int eventID, int num, string itemname)
	{
		string _str = Localization.Get(itemname);
		StringUtils.CheckPlural(num, ref _str);
		_str = _str + " " + Localization.Get("Purchase_mul") + " " + num;
		if (mGame.mEventData.InSessionEventList.ContainsKey(eventID))
		{
			SMSeasonEventSetting sMSeasonEventSetting = mGame.mEventData.InSessionEventList[eventID];
			_str = _str + "\n(" + Localization.Get(sMSeasonEventSetting.Name) + ")";
		}
		return _str;
	}

	public override IEnumerator BuildResource()
	{
		ResImage image = ResourceManager.LoadImageAsync("MAIL_BOX");
		while (image == null || image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		mAtlasMailBox = image;
	}

	private IEnumerator LaceRotate(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		BIJImage resImageCollection = mAtlasMailBox.Image;
		UISprite sprite = Util.CreateSprite("Lace", parent, resImageCollection);
		Util.SetSpriteInfo(sprite, "characolle_back_lace02", depth, pos, scale, false);
		mLaceList.Add(sprite);
		float angle = 0f;
		while (mLaceMove)
		{
			angle += Time.deltaTime * speed;
			sprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	private IEnumerator Routine_AnimationSpriteAlpha(UISprite sprite, float time, float delay, float end, float start = -1f)
	{
		if (sprite == null)
		{
			yield break;
		}
		while (delay > 0f)
		{
			delay -= Time.deltaTime;
			yield return null;
		}
		if (start == -1f)
		{
			start = sprite.color.a;
		}
		start = Mathf.Clamp01(start);
		end = Mathf.Clamp01(end);
		if (time <= 0f)
		{
			Color color2 = sprite.color;
			color2.a = end;
			sprite.color = color2;
			yield break;
		}
		float curTime = 0f;
		while (true)
		{
			Color color = sprite.color;
			color.a = Mathf.Lerp(start, end, curTime / time);
			sprite.color = color;
			if (curTime >= time)
			{
				break;
			}
			curTime += Time.deltaTime;
			if (curTime > time)
			{
				curTime = time;
			}
			yield return null;
		}
	}

	private IEnumerator MoveGiftDialog(bool hideMode)
	{
		int i = 0;
		if (!hideMode)
		{
			i = -2000;
		}
		while (true)
		{
			if (hideMode)
			{
				i -= 200;
				mBody.localPosition = new Vector3(i, mBody.localPosition.y, mBody.localPosition.z);
				mHeader.localPosition = new Vector3(mHeader.localPosition.x, (float)(-i) * 0.1f, mHeader.localPosition.z);
				mFooter.localPosition = new Vector3(mFooter.localPosition.x, (float)i * 0.1f, mFooter.localPosition.z);
				if (i <= -2000)
				{
					mDialogCenter.SetActive(false);
					mDialogCenter.SetActive(true);
					StartCoroutine(Routine_AnimationSpriteAlpha(mBackGround, 0.2f, 0f, 0f, -1f));
					{
						foreach (UISprite lace in mLaceList)
						{
							StartCoroutine(Routine_AnimationSpriteAlpha(lace, 0.2f, 0f, 0f, -1f));
						}
						yield break;
					}
				}
			}
			else
			{
				i += 200;
				mBody.localPosition = new Vector3(i, mBody.localPosition.y, mBody.localPosition.z);
				mHeader.localPosition = new Vector3(mHeader.localPosition.x, (float)(-i) * 0.1f, mHeader.localPosition.z);
				mFooter.localPosition = new Vector3(mFooter.localPosition.x, (float)i * 0.1f, mFooter.localPosition.z);
				if (i >= 0)
				{
					break;
				}
			}
			yield return 0;
		}
		mDialogCenter.SetActive(false);
		mDialogCenter.SetActive(true);
	}

	private IEnumerator LoadSupportStage(Def.SERIES a_series, int a_stage)
	{
		if (Def.IsAdvSeries(a_series))
		{
			mState.Change(STATE.MAIN);
		}
		else if (!Def.IsEventSeries(a_series))
		{
			mSupportStageLoaded = true;
		}
		else
		{
			mSupportStageLoaded = true;
		}
		yield break;
	}

	private IEnumerator Routine_CommunicateSubcurrencyCharge(Action<bool> callback, bool already_connect_success, params KeyValuePair<GiftListItem, SubcurrencyInfo>[] id_num_pair)
	{
		if (WaitOperatingCount == 0)
		{
			IsTmpOperating = IsOperating;
		}
		WaitOperatingCount++;
		IsOperating = false;
		bool result = true;
		if (id_num_pair != null || id_num_pair.Length != 0)
		{
			mState.Reset(STATE.CURRENCY_BUNDLE_WAIT, true);
			int offset = 0;
			int totalNums = 0;
			int connectNum;
			for (; offset < id_num_pair.Length; offset += connectNum)
			{
				connectNum = mGame.GameProfile.GiftReceiveMax;
				if (connectNum <= 0)
				{
					connectNum = 1;
				}
				List<int> remoteIDs = new List<int>();
				List<GiftListItem> selectGiftItem = new List<GiftListItem>();
				for (int i = offset; i < offset + connectNum && i < id_num_pair.Length; i++)
				{
					remoteIDs.Add(id_num_pair[i].Value.RemoteID);
					selectGiftItem.Add(id_num_pair[i].Key);
				}
				mSelectedGiftItem = selectGiftItem.ToArray();
				while (ServerIAP.IsNetworking)
				{
					yield return null;
				}
				mState.Reset(STATE.CURRENCY_BUNDLE_WAIT, true);
				if (!ServerIAP.SubcurrencyCharge(remoteIDs.ToArray()))
				{
					result = false;
					ReceiveSCFail(false);
					break;
				}
				while (mState.GetStatus() == STATE.CURRENCY_BUNDLE_WAIT)
				{
					yield return null;
				}
				switch (mState.GetStatus())
				{
				case STATE.CURRENCY_BUNDLE_SUCCESS:
				{
					mSuccessSelectedGiftItem.AddRange(mSelectedGiftItem);
					for (int j = offset; j < offset + connectNum; j++)
					{
						if (j < id_num_pair.Length)
						{
							totalNums += id_num_pair[j].Value.Quantity;
						}
					}
					break;
				}
				case STATE.CURRENCY_BUNDLE_FAILURE:
					result = false;
					break;
				}
				mSelectedGiftItem = null;
			}
			if (totalNums > 0)
			{
				AddMultiReceiveItemCount(Localization.Get("Gift_ItemNameSD"), totalNums);
			}
		}
		mSelectedGiftItem = mSuccessSelectedGiftItem.ToArray();
		if (mSelectedGiftItem != null && mSelectedGiftItem.Length > 0)
		{
			RecieveSCSuccess();
		}
		mState.Reset(STATE.WAIT, true);
		if (callback != null)
		{
			callback(result && already_connect_success);
		}
		if (--WaitOperatingCount <= 0)
		{
			IsOperating = IsTmpOperating;
		}
	}

	private IEnumerator Routine_ReceiveAdvItem(Action callback = null)
	{
		for (int i = AdvNetwork[0].MailID.Length - 1; i >= 0; i--)
		{
			while (ServerIAP.IsNetworking)
			{
				yield return null;
			}
			bool isResetList = AdvNetwork.Count > 0 || i != 0;
			OnReceiveGiftPushed(AdvNetwork[0].GetReceiveItemGo(i), new ReceiveGiftConfig
			{
				IsInvisibleDialog = true,
				IsNotResetList = isResetList,
				IsNotResetAllList = isResetList,
				IsNotCheckState = true
			});
			yield return null;
		}
		if (AdvNetwork.Count > 0)
		{
			mState.Change(STATE.NETWORK_ADV_COMPLETE_MAIL);
		}
		else
		{
			mMultiReceiveState = MULTI_RECEIVE_STATE.SUCCESS;
			mIsMultiReceive = false;
		}
		if (callback != null)
		{
			callback();
		}
	}

	private IEnumerator Routine_WaitCommunicateAdvItem(Action<bool> callback = null)
	{
		bool result = true;
		while (mMultiReceiveState != 0)
		{
			if (mMultiReceiveState == MULTI_RECEIVE_STATE.FAILED)
			{
				result = false;
				break;
			}
			yield return null;
		}
		AdvNetwork.Clear();
		mIsMultiReceive = false;
		if (callback != null)
		{
			callback(result);
		}
	}

	private IEnumerator Routine_ResumeBackgroundAlpha(Action finished_call)
	{
		foreach (UISprite lace in mLaceList)
		{
			StartCoroutine(Routine_AnimationSpriteAlpha(lace, 0.2f, 0f, 1f, -1f));
		}
		yield return StartCoroutine(Routine_AnimationSpriteAlpha(mBackGround, 0.2f, 0f, 1f, -1f));
		if (finished_call != null)
		{
			finished_call();
		}
	}

	private IEnumerator Routine_CreateBackgroundTabItem(GameMain.GiftUIMode mode, GameMain.GiftListMode? list_mode = null)
	{
		ItemListInfoConfig config = default(ItemListInfoConfig);
		Vector2 position = new Vector2(-10f, -44f);
		Dictionary<LIST_TYPE, ItemListConfig> listDic = new Dictionary<LIST_TYPE, ItemListConfig>(new ListTypeComparer());
		Vector2 screenSize = Util.LogScreenSize();
		Vector2 sz = Vector2.zero;
		sz = ((!(screenSize.x > screenSize.y)) ? new Vector2(screenSize.y, screenSize.x) : new Vector2(screenSize.x, screenSize.y));
		float asp = sz.y / (sz.x / 16f * 9f);
		switch (mode)
		{
		case GameMain.GiftUIMode.RECEIVE:
		{
			if ((ListMode == list_mode.GetValueOrDefault() && list_mode.HasValue && Mode == GameMain.GiftUIMode.RECEIVE) || list_mode == GameMain.GiftListMode.NONE)
			{
				break;
			}
			List<SMVerticalListItem> list3 = new List<SMVerticalListItem>();
			bool isToday = false;
			LIST_TYPE listType = LIST_TYPE.RECEIVE_ALL;
			if (list_mode.HasValue)
			{
				switch (list_mode.Value)
				{
				case GameMain.GiftListMode.ALL:
					listType = LIST_TYPE.RECEIVE_ALL;
					break;
				case GameMain.GiftListMode.HEART:
					listType = LIST_TYPE.RECEIVE_HEART;
					break;
				case GameMain.GiftListMode.MEDAL:
					listType = LIST_TYPE.RECEIVE_MEDAL;
					break;
				case GameMain.GiftListMode.OTHER:
					listType = LIST_TYPE.RECEIVE_OTHER;
					break;
				}
			}
			position = new Vector2(-20f, -49f);
			config.Landscape = new ItemListInfoConfig.OrientationConfig
			{
				MaxPercentLine = 2,
				Width = 1006f,
				Height = 230f + 193f * (asp - 1f)
			};
			config.Portrait = new ItemListInfoConfig.OrientationConfig
			{
				MaxPercentLine = 1,
				Width = 530f,
				Height = 700f
			};
			config.ScrollSpeed = 1;
			config.Height = 172;
			if (mGame.mPlayer.Gifts.Count > 0)
			{
				foreach (KeyValuePair<string, GiftItem> gift3 in mGame.mPlayer.Gifts)
				{
					GiftItem gift2 = gift3.Value;
					if (gift2.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ || gift2.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_RES || gift2.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ || gift2.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ || !mGame.CheckCorrectReceiveGiftItem(gift2) || !MessageCardDisplayCheck(gift2))
					{
						continue;
					}
					bool isExclusion = false;
					double day = -1.0;
					if (gift2.GiftItemCategory == Def.ITEM_CATEGORY.PRESENT_HEART || (gift2.GiftItemCategory == Def.ITEM_CATEGORY.CONSUME && (gift2.Data.ItemKind == 2 || gift2.Data.ItemKind == 1)) || (gift2.GiftItemCategory == Def.ITEM_CATEGORY.FREE_BOOSTER && (gift2.Data.ItemKind == 10 || gift2.Data.ItemKind == 11 || gift2.Data.ItemKind == 12)))
					{
						if ((list_mode.GetValueOrDefault() != 0 || !list_mode.HasValue) && list_mode != GameMain.GiftListMode.HEART)
						{
							isExclusion = true;
						}
						else
						{
							day = ((gift2.GiftItemCategory != Def.ITEM_CATEGORY.PRESENT_HEART) ? ((double)Constants.GIFT_EXPIRED) : ((double)Constants.FRIENDHEART_GIFT_EXPIRED));
						}
					}
					else if (gift2.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL || (gift2.GiftItemCategory == Def.ITEM_CATEGORY.ADV_COIN && gift2.Data.ItemKind == 1))
					{
						if ((list_mode.GetValueOrDefault() != 0 || !list_mode.HasValue) && list_mode != GameMain.GiftListMode.MEDAL)
						{
							isExclusion = true;
						}
						else
						{
							day = ((gift2.GiftItemCategory != Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL) ? ((double)Constants.GIFT_EXPIRED) : ((double)Constants.FRIENDHEART_GIFT_EXPIRED));
						}
					}
					else if ((list_mode.GetValueOrDefault() != 0 || !list_mode.HasValue) && list_mode != GameMain.GiftListMode.OTHER)
					{
						isExclusion = true;
					}
					else
					{
						day = Constants.GIFT_EXPIRED;
					}
					if (!isExclusion)
					{
						DateTime expired = DateTimeUtil.GetDate(gift2.Data.CreateTime).AddDays(day);
						LIST_TYPE ltype = LIST_TYPE.RECEIVE_ALL;
						if (list_mode.HasValue)
						{
							switch (list_mode.Value)
							{
							case GameMain.GiftListMode.ALL:
								ltype = LIST_TYPE.RECEIVE_ALL;
								break;
							case GameMain.GiftListMode.HEART:
								ltype = LIST_TYPE.RECEIVE_HEART;
								break;
							case GameMain.GiftListMode.MEDAL:
								ltype = LIST_TYPE.RECEIVE_MEDAL;
								break;
							case GameMain.GiftListMode.OTHER:
								ltype = LIST_TYPE.RECEIVE_OTHER;
								break;
							}
						}
						TimeSpan diff = expired - DateTime.Now;
						if (!isToday && diff.TotalDays <= 1.0 && diff.TotalSeconds >= 0.0)
						{
							isToday = true;
						}
						if (diff.TotalSeconds >= 0.0)
						{
							list3.Add(GiftListItem.Make(gift2, null, expired));
						}
						mIsTodayLimit[(int)ltype] = isToday;
					}
					yield return null;
				}
			}
			list3.Sort();
			if (listDic.ContainsKey(listType))
			{
				listDic[listType] = new ItemListConfig
				{
					ListData = list3,
					IsToday = isToday
				};
			}
			else
			{
				listDic.Add(listType, new ItemListConfig
				{
					ListData = list3,
					IsToday = isToday
				});
			}
			break;
		}
		case GameMain.GiftUIMode.SEND:
		{
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				position.y = -22f;
			}
			else
			{
				position.y = -20f;
			}
			config.Landscape = new ItemListInfoConfig.OrientationConfig
			{
				MaxPercentLine = 2,
				Width = 1006f,
				Height = 300f + 193f * (asp - 1f)
			};
			config.Portrait = new ItemListInfoConfig.OrientationConfig
			{
				MaxPercentLine = 1,
				Width = 530f,
				Height = 750f
			};
			config.ScrollSpeed = 1;
			config.Height = 172;
			List<SMVerticalListItem> list2 = new List<SMVerticalListItem>();
			List<MyFriend> sendableFriends = mGame.GetHeartSendableFriends();
			if (sendableFriends != null && sendableFriends.Count > 0)
			{
				foreach (MyFriend friend in sendableFriends)
				{
					if (friend.Data.UUID != mGame.mPlayer.UUID)
					{
						list2.Add(GiftListItem.Make(null, friend, null));
						yield return null;
					}
				}
			}
			mIsTodayLimit[4] = false;
			list2.Sort();
			if (listDic.ContainsKey(LIST_TYPE.SEND))
			{
				listDic[LIST_TYPE.SEND] = new ItemListConfig
				{
					ListData = list2,
					IsToday = false
				};
			}
			else
			{
				listDic.Add(LIST_TYPE.SEND, new ItemListConfig
				{
					ListData = list2,
					IsToday = false
				});
			}
			break;
		}
		case GameMain.GiftUIMode.RESCUE:
		{
			if (Util.ScreenOrientation == ScreenOrientation.LandscapeRight || Util.ScreenOrientation == ScreenOrientation.LandscapeRight)
			{
				position.y = -22f;
			}
			else
			{
				position.y = -20f;
			}
			config.Landscape = new ItemListInfoConfig.OrientationConfig
			{
				MaxPercentLine = 2,
				Width = 1006f,
				Height = 300f + 193f * (asp - 1f)
			};
			config.Portrait = new ItemListInfoConfig.OrientationConfig
			{
				MaxPercentLine = 1,
				Width = 530f,
				Height = 750f
			};
			config.ScrollSpeed = 1;
			config.Height = 172;
			config.MaxDisplayCount = 30;
			List<SMVerticalListItem> list = new List<SMVerticalListItem>();
			if (mGame.mPlayer.Gifts.Count > 0)
			{
				List<GiftListItem> listRoadblock = new List<GiftListItem>();
				List<GiftListItem> listFriendReq = new List<GiftListItem>();
				foreach (KeyValuePair<string, GiftItem> gift4 in mGame.mPlayer.Gifts)
				{
					GiftItem gift = gift4.Value;
					if (mGame.CheckCorrectRescueGiftItem(gift))
					{
						if (gift.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_REQ)
						{
							listRoadblock.Add(GiftListItem.Make(gift, null, null));
						}
						else if (gift.GiftItemCategory == Def.ITEM_CATEGORY.FRIEND_REQ)
						{
							listFriendReq.Add(GiftListItem.Make(gift, null, null));
						}
						else if (gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ)
						{
							list.Add(GiftListItem.Make(gift, null, gift.Data.GetRemoteTimeToLocal().AddMinutes(Constants.FRIENDHEART_GIFT_EXPIRED)));
						}
						yield return null;
					}
				}
				listRoadblock.Sort(GiftItemSort);
				listFriendReq.Sort(GiftItemSort);
				list.AddRange(listRoadblock.ToArray());
				list.AddRange(listFriendReq.ToArray());
			}
			mIsTodayLimit[5] = false;
			if (listDic.ContainsKey(LIST_TYPE.RESCUE))
			{
				listDic[LIST_TYPE.RESCUE] = new ItemListConfig
				{
					ListData = list,
					IsToday = false
				};
			}
			else
			{
				listDic.Add(LIST_TYPE.RESCUE, new ItemListConfig
				{
					ListData = list,
					IsToday = false
				});
			}
			break;
		}
		}
		foreach (KeyValuePair<LIST_TYPE, ItemListConfig> info in listDic)
		{
			SMVerticalList giftList = SMVerticalList.Make(_portrait: Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown, _parent: mDialogCenter, _messageTaget: this, listInfo: config.CreateVerticalListInfo(WideScreenBodyHeightOffset), _items: info.Value.ListData, _posX: 0f, _posY: 0f, _baseDepth: mBaseDepth + 1, _movement: UIScrollView.Movement.Vertical, _listupdate: true);
			Vector3 pos = giftList.transform.localPosition;
			pos.x = position.x;
			pos.y = position.y;
			giftList.transform.localPosition = pos;
			giftList.ScrollBarPositionOffsetPortrait = new Vector3(-5f, 0f);
			giftList.ScrollBarPositionOffsetLandscape = new Vector3(13f, 0f);
			if (mode == GameMain.GiftUIMode.SEND)
			{
				giftList.OnCreateItem = OnCreateItem_Friend;
			}
			else
			{
				giftList.OnCreateItem = OnCreateItem_Gift;
			}
			giftList.gameObject.SetActive(false);
			if (mTempItemList.ContainsKey(info.Key))
			{
				mTempItemList[info.Key] = new ScrollListConfig
				{
					ScrollData = giftList,
					IsToday = info.Value.IsToday,
					IsCreated = false
				};
			}
			else
			{
				mTempItemList.Add(info.Key, new ScrollListConfig
				{
					ScrollData = giftList,
					IsToday = info.Value.IsToday,
					IsCreated = false
				});
			}
			yield return null;
		}
	}

	public override void OnDestroy()
	{
		Server.GiveGiftEvent -= Server_OnGiveGift;
		Server.RegisterFriendEvent -= Server_OnRegisterFriend;
		Server.GiveGiftAllEvent -= Server_OnGiveGiftAll;
		ServerIAP.SubcurrencyChargeEvent -= Server_OnBuySC;
		if (mFBUserManager != null)
		{
			mFBUserManager.SetPriority(false);
		}
		ResourceManager.UnloadResource(mAtlasMailBox, "MAIL_BOX");
		base.OnDestroy();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	public override void OnOpenFinished()
	{
		ChangeMode(Mode, new ChangeModeConfig
		{
			Mode = ChangeModeConfig.CreateMode.Force
		});
		mGame.FirstGiftTab = GameMain.GiftUIMode.NONE;
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mStageOpenGiftReceived, mFriendHelpApply, mFriendHelpSuccess, mAdvCharaReceived, mAdvTicketReceived);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private void OnDetailClosing()
	{
	}

	private void OnApplyPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		GiftListItem giftListItem = mGiftItemList.FindListItem(go.transform.parent.gameObject) as GiftListItem;
		if (giftListItem == null)
		{
			return;
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		if (giftListItem.gift != null)
		{
			bool flag = false;
			switch ((Def.ITEM_CATEGORY)giftListItem.gift.Data.ItemCategory)
			{
			case Def.ITEM_CATEGORY.FRIEND_REQ:
				if (mGame.mPlayer.Friends.Count >= Constants.GAME_FRIEND_MAX)
				{
					mConfirmDialog = Util.CreateGameObject("LimitFriendDialog", base.gameObject).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("Approval_Friend_Title"), Localization.Get("Friend_Request_Result_Mine_Full"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSEBUTTON);
					mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
					mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component = mConfirmDialog.GetComponent<BoxCollider>();
						if ((bool)component)
						{
							component.size = new Vector3(1384f, 1384f, 1f);
						}
					});
					EnableList(false);
					mState.Reset(STATE.WAIT);
					return;
				}
				giftListItem.sendCategory = 17;
				mSelectedGiftItem = new GiftListItem[1] { giftListItem };
				flag = true;
				Network_ResponseFriendRequest(giftListItem);
				break;
			case Def.ITEM_CATEGORY.ROADBLOCK_REQ:
				giftListItem.sendCategory = 15;
				mSelectedGiftItem = new GiftListItem[1] { giftListItem };
				flag = true;
				Network_ResponseRoadBlockRequest(giftListItem);
				break;
			case Def.ITEM_CATEGORY.STAGEHELP_REQ:
			{
				bool flag2 = false;
				if (giftListItem.expired.HasValue)
				{
					DateTime value = giftListItem.expired.Value;
					if ((value - DateTime.Now).Seconds < 0)
					{
						flag2 = true;
					}
				}
				else
				{
					flag2 = true;
				}
				if (flag2)
				{
					mGame.mPlayer.RemoveGift(giftListItem.gift);
					int index2 = mGiftItemList.FindListIndexOf(go.transform.parent.gameObject);
					mGiftItemList.RemoveListItem(index2);
					mConfirmDialog = Util.CreateGameObject("FriendHelpExpired", base.ParentGameObject).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("FriendHelp_TimeLimit_Title"), Localization.Get("FriendHelp_TimeLimit_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
					mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
					mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component2 = mConfirmDialog.GetComponent<BoxCollider>();
						if ((bool)component2)
						{
							component2.size = new Vector3(1384f, 1384f, 1f);
						}
					});
					EnableList(false);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.SetFriendHelp(giftListItem.gift);
					mFriendHelpApply = true;
					Close();
				}
				break;
			}
			case Def.ITEM_CATEGORY.STAGEHELP_RES:
			{
				PlayerClearData playerClearData = new PlayerClearData();
				playerClearData.Series = (Def.SERIES)giftListItem.gift.Data.ItemKind;
				playerClearData.StageNo = giftListItem.gift.Data.ItemID;
				playerClearData.HightScore = 0;
				playerClearData.GotStars = 0;
				mGame.mPlayer.AddStageClearData(playerClearData);
				mGame.mPlayer.SetClearStageInfo(giftListItem.gift.Data.ItemID);
				mGame.mPlayer.RemoveGift(giftListItem.gift);
				int index = mGiftItemList.FindListIndexOf(go.transform.parent.gameObject);
				mGiftItemList.RemoveListItem(index);
				mGame.Save();
				mFriendHelpSuccess = true;
				Close();
				break;
			}
			}
			if (flag)
			{
				mState.Reset(STATE.WAIT, true);
			}
		}
		else if (giftListItem.friend == null)
		{
		}
	}

	private void OnRefusePushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		GiftListItem giftListItem = mGiftItemList.FindListItem(go.transform.parent.gameObject) as GiftListItem;
		if (giftListItem == null)
		{
			return;
		}
		mGame.PlaySe("SE_NEGATIVE", -1);
		if (giftListItem.gift != null)
		{
			switch ((Def.ITEM_CATEGORY)giftListItem.gift.Data.ItemCategory)
			{
			case Def.ITEM_CATEGORY.FRIEND_REQ:
			{
				int index2 = mGiftItemList.FindListIndexOf(go.transform.parent.gameObject);
				mGiftItemList.RemoveListItem(index2);
				if (giftListItem.gift != null)
				{
					mGame.mPlayer.RemoveGift(giftListItem.gift);
					mGame.Save();
				}
				break;
			}
			case Def.ITEM_CATEGORY.STAGEHELP_REQ:
			{
				int index = mGiftItemList.FindListIndexOf(go.transform.parent.gameObject);
				mGiftItemList.RemoveListItem(index);
				if (giftListItem.gift != null)
				{
					mGame.mPlayer.RemoveGift(giftListItem.gift);
					mGame.Save();
				}
				break;
			}
			case Def.ITEM_CATEGORY.FRIEND_RES:
				break;
			}
		}
		else if (giftListItem.friend == null)
		{
		}
	}

	public void OnFigureDetailPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		GiftListItem giftListItem = mGiftItemList.FindListItem(go) as GiftListItem;
		if (giftListItem == null)
		{
			return;
		}
		if (giftListItem.gift.Data.ItemCategory == 34)
		{
			List<GiftItem> messageCardPresentList = mGame.GetMessageCardPresentList(giftListItem.gift.Data.MessageCardID);
			if (messageCardPresentList.Count > 0)
			{
				ScrollBoxDialog.CreateConfig createConfig = default(ScrollBoxDialog.CreateConfig);
				createConfig.Title = Localization.Get("MailBOX_MessageCard_Detail_Title00");
				createConfig.DescriptionSize = 21;
				createConfig.DisableOpneSE = false;
				createConfig.DisableButtonSE = false;
				createConfig.Style = ScrollBoxDialog.CONFIRM_DIALOG_STYLE.OK_ONLY;
				createConfig.Size = DIALOG_SIZE.MEDIUM;
				createConfig.VisibleSeparator = false;
				ScrollBoxDialog.CreateConfig config = createConfig;
				List<ScrollBoxDialog.CreateConfig.DescriptionInfo> list = new List<ScrollBoxDialog.CreateConfig.DescriptionInfo>();
				foreach (GiftItem item in messageCardPresentList)
				{
					string spriteName = string.Empty;
					string buttonImage = string.Empty;
					string message = string.Empty;
					BIJImage atlas = null;
					GetGiftItemDisplayData(item, out spriteName, out buttonImage, out message, out atlas);
					list.Add(new ScrollBoxDialog.CreateConfig.DescriptionInfo
					{
						Message = message,
						Color = Def.DEFAULT_MESSAGE_COLOR
					});
				}
				config.Description = list.ToArray();
				ScrollBoxDialog dialog = Util.CreateGameObject("DetailsDialog", base.ParentGameObject).AddComponent<ScrollBoxDialog>();
				dialog.Init(config);
				dialog.SetClosedCallback(delegate
				{
					mState.Reset(STATE.MAIN, true);
				});
				dialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
				{
					BoxCollider component = dialog.GetComponent<BoxCollider>();
					if ((bool)component)
					{
						component.size = new Vector3(1384f, 1384f, 1f);
					}
				});
			}
		}
		mState.Reset(STATE.SHOW_DETAIL_WAIT);
	}

	public override void OnBackKeyPress()
	{
		if (IsOperating && mReceiveAllNoticeDialog == null && mReceiveAllResultDialog == null)
		{
			OnCancelPushed(null);
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void OnSendGiftPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		GiftListItem giftListItem = mGiftItemList.FindListItem(go.transform.parent.gameObject) as GiftListItem;
		if (giftListItem != null)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			giftListItem.sendCategory = 20;
			giftListItem.sendkind = 2;
			mSelectedGiftItem = new GiftListItem[1];
			mSelectedGiftItem[0] = giftListItem;
			mState.Reset(STATE.WAIT, true);
			GiftItemData giftItemData = new GiftItemData();
			giftItemData.FromID = mGame.mPlayer.UUID;
			giftItemData.Message = mGame.mPlayer.NickName;
			giftItemData.ItemCategory = 20;
			giftItemData.ItemKind = 2;
			giftItemData.ItemID = 1;
			giftItemData.Quantity = 1;
			GiftItem giftItem = new GiftItem();
			giftItem.Data = giftItemData;
			giftListItem.gift = giftItem;
			int uUID = mSelectedGiftItem[0].friend.Data.UUID;
			if (!Server.GiveGift(giftItemData, uUID))
			{
				SendGiftFail();
			}
		}
	}

	public void OnReceiveGiftPushed_Direct(GameObject go)
	{
		OnReceiveGiftPushed(go, new ReceiveGiftConfig
		{
			IsNotResetList = true,
			IsCheckTodayLabelOnly = true
		});
	}

	public void OnReceiveGiftPushed(GameObject go, ReceiveGiftConfig config)
	{
		if (GetBusy() || (!config.IsNotCheckState && mState.GetStatus() != STATE.MAIN))
		{
			return;
		}
		GiftListItem giftListItem = mGiftItemList.FindListItem(go.transform.parent.gameObject) as GiftListItem;
		if (giftListItem == null)
		{
			return;
		}
		if (IsAdvItem(giftListItem.gift.Data.ItemCategory))
		{
			if (AdvNetwork.Count == 0)
			{
				if (giftListItem.gift.Data.ItemCategory == 24)
				{
					AdvNetwork.Add(giftListItem.gift.Data.RemoteID, giftListItem.gift.Data.ItemKind, giftListItem.gift.GetGiftSource(), go);
				}
				else
				{
					AdvNetwork.Add(giftListItem.gift.Data.RemoteID, go);
				}
				mIsSingleAdvReceive = true;
				mState.Change(STATE.NETWORK_ADV_COMPLETE_MAIL);
				return;
			}
			AdvNetwork.Remove(go);
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		bool flag = true;
		bool flag2 = false;
		switch (giftListItem.gift.Data.ItemCategory)
		{
		case 1:
			if (giftListItem.gift.Data.ItemKind == 3 || giftListItem.gift.Data.ItemKind == 4 || giftListItem.gift.Data.ItemKind == 5 || giftListItem.gift.Data.ItemKind == 6)
			{
			}
			if (giftListItem.gift.Data.ItemKind == 4)
			{
				flag = false;
				mSelectedGiftItem = new GiftListItem[1];
				mSelectedGiftItem[0] = giftListItem;
				mSelectedGiftItem[0].sendCategory = giftListItem.gift.Data.ItemCategory;
				mSelectedGiftItem[0].sendkind = giftListItem.gift.Data.ItemKind;
				if (!ServerIAP.SubcurrencyCharge(giftListItem.gift.Data.RemoteID))
				{
					ReceiveSCFail();
					return;
				}
				mState.Reset(STATE.WAIT, true);
			}
			else if (giftListItem.gift.Data.ItemKind == 1 || giftListItem.gift.Data.ItemKind == 2)
			{
				if (mGame.mPlayer.Data.mMugenHeart != 0 || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60))
				{
					if (!config.IsInvisibleDialog)
					{
						mConfirmDialog = Util.CreateGameObject("NowMugenHeartDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
						mConfirmDialog.Init(Localization.Get("Send_heart_MaxError_Title"), Localization.Get("MugenHeart_GetMailNG"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
						mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
						mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
						{
							BoxCollider component5 = mConfirmDialog.GetComponent<BoxCollider>();
							if ((bool)component5)
							{
								component5.size = new Vector3(1384f, 1384f, 1f);
							}
						});
					}
					mState.Reset(STATE.WAIT, true);
					flag = false;
					flag2 = true;
				}
				else
				{
					if (giftListItem.gift.Data.ItemID == 0)
					{
						break;
					}
					if (mGame.mPlayer.LifeCount + giftListItem.gift.Data.Quantity > mGame.mPlayer.MaxLifeCount)
					{
						int num5 = mGame.mPlayer.MaxLifeCount - mGame.mPlayer.LifeCount;
						mGame.mPlayer.AddLifeCount(num5);
						giftListItem.gift.Data.Quantity -= num5;
						if (!config.IsInvisibleDialog)
						{
							mConfirmDialog = Util.CreateGameObject("OverHeartDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
							mConfirmDialog.Init(Localization.Get("Send_heart_MaxError_Title"), Localization.Get("Send_heart_MaxError_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
							mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
							mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
							{
								BoxCollider component4 = mConfirmDialog.GetComponent<BoxCollider>();
								if ((bool)component4)
								{
									component4.size = new Vector3(1384f, 1384f, 1f);
								}
							});
						}
						mState.Reset(STATE.WAIT, true);
						UILabel[] componentsInChildren2 = go.transform.parent.GetComponentsInChildren<UILabel>(true);
						UILabel[] array2 = componentsInChildren2;
						foreach (UILabel uILabel2 in array2)
						{
							if (uILabel2.name.Equals("Message"))
							{
								string _str2 = Localization.Get("Gift_ItemNameHeart");
								StringUtils.CheckPlural(giftListItem.gift.Data.Quantity, ref _str2);
								Util.SetLabelText(uILabel2, _str2 + " " + Localization.Get("Purchase_mul") + " " + giftListItem.gift.Data.Quantity);
							}
						}
						flag = false;
						flag2 = true;
					}
					else
					{
						mGame.mPlayer.AddLifeCount(giftListItem.gift.Data.Quantity);
					}
				}
			}
			else if (giftListItem.gift.Data.ItemKind == 10)
			{
				mGame.mPlayer.AddGrowup(giftListItem.gift.Data.Quantity);
				int quantity2 = giftListItem.gift.Data.Quantity;
				int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
				int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
				int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
				int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
				int advLastStage = mGame.mPlayer.AdvLastStage;
				int advMaxStamina = mGame.mPlayer.AdvMaxStamina;
				int get_type = 4;
				if (giftListItem.gift.GetGiftSource() == GiftMailType.SUPPORT)
				{
					get_type = 99;
				}
				else if (giftListItem.gift.GetGiftSource() == GiftMailType.FROM_ADV || giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_ADV)
				{
					get_type = 55;
				}
				else if (giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_EVENT)
				{
					get_type = 3;
				}
				else if (giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_MAP)
				{
					get_type = 2;
				}
				else
				{
					int cramGetType = GetCramGetType(giftListItem.gift);
					if (cramGetType > 0)
					{
						get_type = cramGetType;
					}
				}
				ServerCram.ArcadeGetItem(quantity2, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 10, 1, get_type, advLastStage, advMaxStamina);
				int num7 = 0;
				List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
				for (int num8 = 0; num8 < playStage.Count; num8++)
				{
					if (playStage[num8].Series == 0)
					{
						num7 = playStage[num8].Level;
						break;
					}
				}
				if (num7 <= 800)
				{
					break;
				}
				if (!config.IsInvisibleDialog)
				{
					mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", base.gameObject).AddComponent<GrowupPartnerSelectDialog>();
					mGrowupDialog.Init(null, false, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
					mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
					mGrowupDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component3 = mGrowupDialog.GetComponent<BoxCollider>();
						if ((bool)component3)
						{
							component3.size = new Vector3(1384f, 1384f, 1f);
						}
					});
				}
				mState.Reset(STATE.WAIT, true);
			}
			else if (giftListItem.gift.Data.ItemKind == 11)
			{
				int quantity3 = giftListItem.gift.Data.Quantity;
				mGame.mPlayer.AddGrowupPiece(quantity3);
				if (!config.IsInvisibleDialog)
				{
					mGetStampDialog = Util.CreateGameObject("GetStampDialog", base.gameObject).AddComponent<GetStampDialog>();
					mGetStampDialog.Init(null, quantity3, true, false, true, quantity3, GetCramGetType(giftListItem.gift));
					mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
					mGetStampDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component2 = mGetStampDialog.GetComponent<BoxCollider>();
						if ((bool)component2)
						{
							component2.size = new Vector3(1384f, 1384f, 1f);
						}
					});
				}
				mState.Reset(STATE.WAIT, true);
			}
			else if (giftListItem.gift.Data.ItemKind == 9)
			{
				int quantity4 = giftListItem.gift.Data.Quantity;
				mGame.mPlayer.AddHeartPiece(quantity4);
				if (!config.IsInvisibleDialog)
				{
					mGetStampDialog = Util.CreateGameObject("GetStampDialog", base.gameObject).AddComponent<GetStampDialog>();
					mGetStampDialog.Init(null, quantity4, true, false, false, quantity4, GetCramGetType(giftListItem.gift));
					mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
					mGetStampDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component = mGetStampDialog.GetComponent<BoxCollider>();
						if ((bool)component)
						{
							component.size = new Vector3(1384f, 1384f, 1f);
						}
					});
				}
				mState.Reset(STATE.WAIT, true);
			}
			else if (giftListItem.gift.Data.ItemKind == 8)
			{
				mGame.mPlayer.AddRoadBlockKey(giftListItem.gift.Data.Quantity, Def.SERIES.SM_FIRST);
			}
			else if (giftListItem.gift.Data.ItemKind == 16)
			{
				mGame.mPlayer.AddRoadBlockKey(giftListItem.gift.Data.Quantity, Def.SERIES.SM_R);
			}
			else if (giftListItem.gift.Data.ItemKind == 17)
			{
				mGame.mPlayer.AddRoadBlockKey(giftListItem.gift.Data.Quantity, Def.SERIES.SM_S);
			}
			else if (giftListItem.gift.Data.ItemKind == 18)
			{
				mGame.mPlayer.AddRoadBlockKey(giftListItem.gift.Data.Quantity, Def.SERIES.SM_SS);
			}
			else if (giftListItem.gift.Data.ItemKind == 19)
			{
				mGame.mPlayer.AddRoadBlockKey(giftListItem.gift.Data.Quantity, Def.SERIES.SM_STARS);
			}
			else if (giftListItem.gift.Data.ItemKind == 21)
			{
				mGame.mPlayer.AddRoadBlockKey(giftListItem.gift.Data.Quantity, Def.SERIES.SM_GF00);
			}
			else if (giftListItem.gift.Data.ItemKind == 7)
			{
				mGame.mPlayer.AddRoadBlockTicket(giftListItem.gift.Data.Quantity);
			}
			else if (giftListItem.gift.Data.ItemKind == 14)
			{
				mGame.mPlayer.AddTrophy(giftListItem.gift.Data.Quantity);
			}
			else if (giftListItem.gift.Data.ItemKind == 20)
			{
				mGame.mPlayer.AddItemCount(Def.ITEM_CATEGORY.CONSUME, 20, giftListItem.gift.Data.Quantity);
				int cramGetType2 = GetCramGetType(giftListItem.gift);
				ServerCram.ArcadeGetItem(giftListItem.gift.Data.Quantity, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 20, 1, cramGetType2, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
			}
			break;
		case 3:
		{
			Def.BOOSTER_KIND bOOSTER_KIND = EnumHelper.FromObject(giftListItem.gift.Data.ItemKind, Def.BOOSTER_KIND.NONE);
			if (bOOSTER_KIND == Def.BOOSTER_KIND.NONE)
			{
				break;
			}
			if ((bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART15 || bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART30 || bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART60) && (mGame.mPlayer.Data.mMugenHeart != 0 || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60)))
			{
				if (!config.IsInvisibleDialog)
				{
					mConfirmDialog = Util.CreateGameObject("NowMugenHeartDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("Send_heart_MaxError_Title"), Localization.Get("MugenHeart_GetMailNG"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
					mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
					mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component6 = mConfirmDialog.GetComponent<BoxCollider>();
						if ((bool)component6)
						{
							component6.size = new Vector3(1384f, 1384f, 1f);
						}
					});
				}
				mState.Reset(STATE.WAIT, true);
				flag = false;
				flag2 = true;
			}
			else
			{
				mGame.mPlayer.AddFreeBoosterNum(bOOSTER_KIND, giftListItem.gift.Data.Quantity);
			}
			break;
		}
		case 4:
		{
			AccessoryData accessoryData2 = mGame.GetAccessoryData(giftListItem.gift.Data.ItemKind);
			mGame.mPlayer.AddAccessoryFromGift(accessoryData2);
			if (accessoryData2.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER)
			{
				List<AccessoryData> wallPaperPieces = mGame.GetWallPaperPieces(accessoryData2.GetWallPaperIndex());
				for (int i = 0; i < wallPaperPieces.Count; i++)
				{
					mGame.mPlayer.AddAccessoryFromGift(wallPaperPieces[i]);
				}
			}
			mGame.mPlayer.Data.UnlockAccessoriesNoticeCount = mGame.mPlayer.Data.Accessories.Count;
			mStageOpenGiftReceived = true;
			break;
		}
		case 5:
		{
			List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
			for (int k = 0; k < accessoryByType.Count; k++)
			{
				if (accessoryByType[k].GetCompanionID() == giftListItem.gift.Data.ItemKind)
				{
					mGame.mPlayer.AddAccessoryFromGift(accessoryByType[k]);
					break;
				}
			}
			mGame.AchievementPartnerUnlock();
			mStageOpenGiftReceived = true;
			break;
		}
		case 7:
		{
			AccessoryData accessoryWallPaperData = mGame.GetAccessoryWallPaperData(giftListItem.gift.Data.ItemKind);
			mGame.mPlayer.AddAccessory(accessoryWallPaperData);
			List<AccessoryData> wallPaperPieces2 = mGame.GetWallPaperPieces(accessoryWallPaperData.GetWallPaperIndex());
			mGame.mPlayer.SetCollectionNotify(wallPaperPieces2[0].Index);
			break;
		}
		case 8:
		{
			AccessoryData accessoryWallPaperPieceData = mGame.GetAccessoryWallPaperPieceData(giftListItem.gift.Data.ItemKind, giftListItem.gift.Data.ItemID);
			VisualCollectionData visualCollectionData = mGame.mVisualCollectionData[accessoryWallPaperPieceData.IsFlip];
			int num4 = 0;
			for (int l = 0; l < visualCollectionData.piceIds.Length; l++)
			{
				if (mGame.mPlayer.IsAccessoryUnlock(visualCollectionData.piceIds[l]))
				{
					num4++;
				}
			}
			mGame.mPlayer.AddAccessory(accessoryWallPaperPieceData);
			if (visualCollectionData.piceIds.Length - 1 != num4)
			{
				break;
			}
			num4 = 0;
			for (int m = 0; m < visualCollectionData.piceIds.Length; m++)
			{
				if (mGame.mPlayer.IsAccessoryUnlock(visualCollectionData.piceIds[m]))
				{
					num4++;
				}
			}
			if (visualCollectionData.piceIds.Length != num4)
			{
				break;
			}
			mGame.mPlayer.SetCollectionNotify(visualCollectionData.accessoryId);
			if (!config.IsInvisibleDialog)
			{
				mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", base.ParentGameObject).AddComponent<GetAccessoryDialog>();
				mGetAccessoryDialog.Init(true);
				mGetAccessoryDialog.SetClosedCallback(OnWallPeperCompClosed);
				mGetAccessoryDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
				{
					BoxCollider component7 = mGetAccessoryDialog.GetComponent<BoxCollider>();
					if ((bool)component7)
					{
						component7.size = new Vector3(1384f, 1384f, 1f);
					}
				});
			}
			mState.Reset(STATE.WAIT, true);
			break;
		}
		case 10:
			mGame.mPlayer.UnlockSkin(giftListItem.gift.Data.ItemKind);
			break;
		case 12:
		{
			Def.SERIES a_series = EnumHelper.FromObject(giftListItem.gift.Data.ItemKind, Def.SERIES.SM_FIRST);
			int itemID = giftListItem.gift.Data.ItemID;
			int quantity6 = giftListItem.gift.Data.Quantity;
			if (Def.IsEventSeries(a_series))
			{
				mReceivedStageNo = itemID;
				mReceivedStar = quantity6;
				mReceivedSeries = a_series;
			}
			else
			{
				mReceivedStageNo = itemID;
				mReceivedStar = quantity6;
				mReceivedSeries = a_series;
			}
			mSupportStageLoaded = false;
			StartCoroutine(LoadSupportStage(a_series, itemID));
			mState.Reset(STATE.SUPPORT_STAGE_RECEIVE_WAIT, true);
			mStageOpenGiftReceived = true;
			break;
		}
		case 13:
		{
			Def.SERIES a_series2 = EnumHelper.FromObject(giftListItem.gift.Data.ItemKind, Def.SERIES.SM_FIRST);
			int itemID2 = giftListItem.gift.Data.ItemID;
			int quantity7 = giftListItem.gift.Data.Quantity;
			if (Def.IsEventSeries(a_series2))
			{
				mReceivedStageNo = itemID2;
				mReceivedStar = quantity7;
				mReceivedSeries = a_series2;
			}
			else
			{
				mReceivedStageNo = Def.GetStageNo(itemID2, 0);
				mReceivedStar = quantity7;
				mReceivedSeries = a_series2;
			}
			mSupportStageLoaded = false;
			StartCoroutine(LoadSupportStage(a_series2, mReceivedStageNo));
			mState.Reset(STATE.SUPPORT_STAGE_BUNDLE_RECEIVE_WAIT, true);
			mStageOpenGiftReceived = true;
			break;
		}
		case 20:
			if (giftListItem.gift.Data.ItemKind != 1 && giftListItem.gift.Data.ItemKind != 2)
			{
				break;
			}
			if (mGame.mPlayer.LifeCount + giftListItem.gift.Data.Quantity > mGame.mPlayer.MaxLifeCount)
			{
				int num3 = mGame.mPlayer.MaxLifeCount - mGame.mPlayer.LifeCount;
				mGame.mPlayer.AddLifeCount(num3);
				giftListItem.gift.Data.Quantity -= num3;
				if (!config.IsInvisibleDialog)
				{
					mConfirmDialog = Util.CreateGameObject("OverHeartDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
					mConfirmDialog.Init(Localization.Get("Send_heart_MaxError_Title"), Localization.Get("Send_heart_MaxError_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
					mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
					mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component8 = mConfirmDialog.GetComponent<BoxCollider>();
						if ((bool)component8)
						{
							component8.size = new Vector3(1384f, 1384f, 1f);
						}
					});
				}
				mState.Reset(STATE.WAIT, true);
				UILabel[] componentsInChildren = go.transform.parent.GetComponentsInChildren<UILabel>(true);
				UILabel[] array = componentsInChildren;
				foreach (UILabel uILabel in array)
				{
					if (uILabel.name.Equals("Message"))
					{
						string _str = Localization.Get("Gift_ItemNameHeart");
						StringUtils.CheckPlural(giftListItem.gift.Data.Quantity, ref _str);
						Util.SetLabelText(uILabel, _str + " " + Localization.Get("Purchase_mul") + " " + giftListItem.gift.Data.Quantity);
					}
				}
				flag = false;
				flag2 = true;
			}
			else
			{
				mGame.mPlayer.AddLifeCount(giftListItem.gift.Data.Quantity);
			}
			break;
		case 32:
		{
			int itemKind2 = giftListItem.gift.Data.ItemKind;
			int quantity5 = giftListItem.gift.Data.Quantity;
			if (!mGame.mPlayer.Data.mBingoEventDataDict.ContainsKey(itemKind2))
			{
				mGame.mPlayer.Data.MakeBingoEventData(itemKind2);
			}
			mGame.mPlayer.Data.mBingoEventDataDict[itemKind2].mReLotteryTicket += quantity5;
			break;
		}
		case 33:
		{
			int itemKind = giftListItem.gift.Data.ItemKind;
			int quantity = giftListItem.gift.Data.Quantity;
			bool flag3 = false;
			SMEventPageData eventPageData = GameMain.GetEventPageData(itemKind);
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, itemKind, out data);
			if (data == null)
			{
				mGame.mPlayer.CreateEventData(itemKind, (short)eventPageData.EventCourseList.Count, eventPageData.EventSetting.EventType);
				mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, itemKind, out data);
			}
			if (eventPageData != null && eventPageData.EventRewardList != null)
			{
				int jewel = data.LabyrinthData.JewelAmount;
				int num = quantity;
				while (num > 0)
				{
					int num2 = eventPageData.EventRewardList.Keys.FirstOrDefault((int n) => jewel < n);
					if (num2 == 0 || jewel + num < num2 || !eventPageData.EventRewardList.ContainsKey(num2))
					{
						break;
					}
					SMEventRewardSetting sMEventRewardSetting = eventPageData.EventRewardList[num2];
					if (sMEventRewardSetting == null)
					{
						break;
					}
					if (sMEventRewardSetting.IsGCItem == 0)
					{
						AccessoryData accessoryData = mGame.GetAccessoryData(sMEventRewardSetting.RewardID);
						if (accessoryData != null && accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY)
						{
							flag3 = true;
							num -= num2 - jewel;
							jewel = num2;
							continue;
						}
						flag3 = false;
						break;
					}
					flag3 = false;
					break;
				}
			}
			mGame.MakeMapInitializeData(Def.SERIES.SM_EV, itemKind);
			mGame.AddEventPoint(itemKind, quantity);
			if (data != null && data.LabyrinthData != null)
			{
				int a_eventID = 9999;
				if (Def.IsEventSeries(mGame.mPlayer.Data.LastPlaySeries))
				{
					short a_course;
					int a_main;
					int a_sub;
					Def.SplitServerStageNo(mGame.mPlayer.Data.LastPlayLevel, out a_eventID, out a_course, out a_main, out a_sub);
				}
				ServerCram.EventLabyrinthGetJewel(-1, (int)mGame.mPlayer.Data.CurrentSeries, mGame.mPlayer.Data.LastPlayLevel, a_eventID, 3, quantity, 0, 0, 0, 0, 0, quantity, 0, data.LabyrinthData.JewelAmount);
			}
			GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(itemKind);
			if (labyrinthData == null)
			{
				break;
			}
			labyrinthData = GlobalVariables.Instance.GetLabyrinthData(itemKind);
			if (labyrinthData == null || labyrinthData.RewardAccessoryDataList == null)
			{
				break;
			}
			if (!config.IsInvisibleDialog && !flag3)
			{
				mConfirmDialog = Util.CreateGameObject("GetLabyrinthRewardDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("MailBOX_GetJwel_Title"), Localization.Get("MailBOX_GetJwel_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
				mConfirmDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
				{
					BoxCollider component9 = mConfirmDialog.GetComponent<BoxCollider>();
					if ((bool)component9)
					{
						component9.size = new Vector3(1384f, 1384f, 1f);
					}
				});
				mState.Reset(STATE.WAIT, true);
				config.IsCheckTodayLabelOnly = false;
			}
			labyrinthData.RewardAccessoryDataList.RemoveAll((AccessoryData a) => a != null);
			labyrinthData.RewardAccessoryDataList = null;
			GlobalVariables.Instance.SetLabyrinthData(itemKind, labyrinthData);
			break;
		}
		case 34:
		{
			StartStoryDemo(giftListItem.gift.Data.MessageCardDemoID);
			List<GiftItem> messageCardPresentList = mGame.GetMessageCardPresentList(giftListItem.gift.Data.MessageCardID);
			foreach (GiftItem item in messageCardPresentList)
			{
				item.Data.CreateTime = DateTime.Now;
			}
			break;
		}
		}
		bool flag4 = false;
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_04_00:
		case STATE.NETWORK_04_01:
		case STATE.NETWORK_04_02:
		case STATE.NETWORK_05_00:
		case STATE.NETWORK_05_01:
			flag4 = true;
			break;
		}
		if (flag)
		{
			mGame.mPlayer.RemoveGift(giftListItem.gift);
			int index = mGiftItemList.FindListIndexOf(go.transform.parent.gameObject);
			mGiftItemList.RemoveListItem(index);
			if (giftListItem.gift.Data.ItemCategory == 4 || giftListItem.gift.Data.ItemCategory == 3)
			{
				int num9 = 0;
				switch (giftListItem.gift.GetGiftSource())
				{
				case GiftMailType.LOCAL:
					num9 = 2;
					break;
				case GiftMailType.LOCAL_EVENT:
					num9 = 3;
					break;
				case GiftMailType.REWARD:
					num9 = 4;
					break;
				case GiftMailType.GIFT:
					num9 = 6;
					break;
				case GiftMailType.LOCAL_MAP:
					num9 = 7;
					break;
				case GiftMailType.FROM_ADV:
					num9 = 55;
					break;
				case GiftMailType.SUPPORT:
					num9 = 99;
					break;
				}
				if (num9 == 0)
				{
					num9 = GetCramGetType(giftListItem.gift);
				}
				Def.BOOSTER_KIND a_kind = Def.BOOSTER_KIND.NONE;
				int a_num = 0;
				if (giftListItem.gift.Data.ItemCategory == 4)
				{
					int itemKind3 = giftListItem.gift.Data.ItemKind;
					if (itemKind3 != 0)
					{
						switch (num9)
						{
						}
					}
					AccessoryData accessoryData3 = mGame.GetAccessoryData(itemKind3);
					if (accessoryData3.AccessoryType == AccessoryData.ACCESSORY_TYPE.BOOSTER)
					{
						accessoryData3.GetBooster(out a_kind, out a_num);
					}
				}
				else if (giftListItem.gift.Data.ItemCategory == 3)
				{
					a_kind = (Def.BOOSTER_KIND)giftListItem.gift.Data.ItemKind;
					a_num = giftListItem.gift.Data.Quantity;
				}
				if (a_kind != 0 && num9 != 0)
				{
					mGame.SendGetBoost(a_num, 0, num9, a_kind);
				}
			}
			if (giftListItem.gift.GetGiftSource() == GiftMailType.SUPPORT)
			{
				if (giftListItem.gift.Data.ItemCategory == 4)
				{
					AccessoryData accessoryData4 = mGame.mAccessoryData[giftListItem.gift.Data.ItemKind];
					if (accessoryData4.AccessoryType == AccessoryData.ACCESSORY_TYPE.TROPHY)
					{
						int gotIDByNum = accessoryData4.GetGotIDByNum();
						ServerCram.GetTrophy(gotIDByNum, mGame.mPlayer.GetTrophy(), 99, giftListItem.gift.Data.ItemKind, mGame.mOptions.PurchaseCount);
					}
				}
				else if (giftListItem.gift.Data.ItemCategory == 1 && giftListItem.gift.Data.ItemKind == 14)
				{
					ServerCram.GetTrophy(giftListItem.gift.Data.Quantity, mGame.mPlayer.GetTrophy(), 99, -1, mGame.mOptions.PurchaseCount);
				}
			}
			else if (giftListItem.gift.GetGiftSource() == GiftMailType.REWARD)
			{
				if (giftListItem.gift.Data.ItemKind == 14)
				{
					ServerCram.GetTrophy(giftListItem.gift.Data.Quantity, mGame.mPlayer.GetTrophy(), 5, -1, mGame.mOptions.PurchaseCount);
				}
			}
			else if (giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_EVENT && giftListItem.gift.Data.ItemKind == 14)
			{
				ServerCram.GetTrophy(giftListItem.gift.Data.Quantity, mGame.mPlayer.GetTrophy(), 5, -1, mGame.mOptions.PurchaseCount);
			}
			if (giftListItem.gift.Data.ItemCategory == 4)
			{
				int itemKind4 = giftListItem.gift.Data.ItemKind;
				AccessoryData accessoryData5 = mGame.GetAccessoryData(itemKind4);
				int gotIDByNum2 = accessoryData5.GetGotIDByNum();
				AdvRewardListData rewardListData = mGame.GetRewardListData(gotIDByNum2);
				if (rewardListData != null && rewardListData.Category == 25)
				{
					int lastPlaySeries3 = (int)mGame.mPlayer.Data.LastPlaySeries;
					int lastPlayLevel3 = mGame.mPlayer.Data.LastPlayLevel;
					int lastPlaySeries4 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
					int lastPlayLevel4 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
					int advLastStage2 = mGame.mPlayer.AdvLastStage;
					int advMaxStamina2 = mGame.mPlayer.AdvMaxStamina;
					int get_type2 = 12;
					if (giftListItem.gift.GetGiftSource() == GiftMailType.SUPPORT)
					{
						get_type2 = 99;
					}
					ServerCram.ArcadeGetItem((int)rewardListData.num, lastPlaySeries3, lastPlayLevel3, lastPlaySeries4, lastPlayLevel4, rewardListData.Category, rewardListData.Kind, rewardListData.ItemID, get_type2, advLastStage2, advMaxStamina2);
				}
			}
			else if (giftListItem.gift.Data.ItemCategory == 25 || giftListItem.gift.Data.ItemCategory == 26 || giftListItem.gift.Data.ItemCategory == 31 || giftListItem.gift.Data.ItemCategory == 20 || (giftListItem.gift.Data.ItemCategory == 1 && giftListItem.gift.Data.ItemKind == 1) || (giftListItem.gift.Data.ItemCategory == 1 && giftListItem.gift.Data.ItemKind == 2) || (giftListItem.gift.Data.ItemCategory == 1 && giftListItem.gift.Data.ItemKind == 7))
			{
				int num10 = 0;
				num10 = ((giftListItem.gift.GetGiftSource() == GiftMailType.REWARD) ? 4 : ((giftListItem.gift.GetGiftSource() == GiftMailType.GIFT) ? 6 : ((giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL) ? 2 : ((giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_EVENT) ? 3 : ((giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_MAP) ? 7 : ((giftListItem.gift.GetGiftSource() == GiftMailType.ADV || giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_ADV || giftListItem.gift.GetGiftSource() == GiftMailType.FROM_ADV) ? 55 : ((giftListItem.gift.GetGiftSource() != GiftMailType.SUPPORT) ? GetCramGetType(giftListItem.gift) : 99)))))));
				int lastPlaySeries5 = (int)mGame.mPlayer.Data.LastPlaySeries;
				int lastPlayLevel5 = mGame.mPlayer.Data.LastPlayLevel;
				int lastPlaySeries6 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
				int lastPlayLevel6 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
				int advLastStage3 = mGame.mPlayer.AdvLastStage;
				int advMaxStamina3 = mGame.mPlayer.AdvMaxStamina;
				ServerCram.ArcadeGetItem(giftListItem.gift.Data.Quantity, lastPlaySeries5, lastPlayLevel5, lastPlaySeries6, lastPlayLevel6, giftListItem.gift.Data.ItemCategory, giftListItem.gift.Data.ItemKind, giftListItem.gift.Data.ItemID, num10, advLastStage3, advMaxStamina3);
			}
			mGame.Save();
			int num11 = 0;
			if (mGiftItemList != null)
			{
				num11 = mGiftItemList.GetListItemCount();
			}
			if (num11 == 0)
			{
				if (mReceiveItem != null && (bool)mReceiveItem.gameObject && mReceiveItem.gameObject.activeSelf)
				{
					mReceiveItem.gameObject.SetActive(false);
				}
				if (mNoReceiveItem != null && (bool)mNoReceiveItem.gameObject && !mNoReceiveItem.gameObject.activeSelf)
				{
					mNoReceiveItem.gameObject.SetActive(true);
				}
			}
			if (!flag4 && num11 == 0 && mGame.mPlayer.Gifts.Count == 0 && GameMain.MapSupportRequestCount == GameMain.MapSupportReceiveCount)
			{
				mState.Reset(STATE.NETWORK_04_00, true);
			}
			else if (!config.IsNotResetAllList)
			{
				ChangeModeConfig config2 = ChangeModeConfig.Create();
				if (config.IsCheckTodayLabelOnly)
				{
					config2.Mode = ChangeModeConfig.CreateMode.TodayLabelOnly;
				}
				else
				{
					config2.Mode = ChangeModeConfig.CreateMode.Force;
				}
				ChangeMode(Mode, config2);
			}
		}
		else
		{
			if (!flag2)
			{
				return;
			}
			mGame.Save();
			int num12 = 0;
			if (mGiftItemList != null)
			{
				num12 = mGiftItemList.GetListItemCount();
			}
			if (!flag4 && mGiftItemList != null && num12 == 0 && GameMain.MapSupportRequestCount == GameMain.MapSupportReceiveCount)
			{
				mState.Reset(STATE.NETWORK_04_00, true);
			}
			else
			{
				if (config.IsNotResetAllList)
				{
					return;
				}
				ChangeModeConfig config3 = ChangeModeConfig.Create();
				if (!config.IsNotResetList || ((Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight) && num12 == 1))
				{
					if (config.IsCheckTodayLabelOnly)
					{
						config3.Mode = ChangeModeConfig.CreateMode.TodayLabelOnly;
					}
					else
					{
						config3.Mode = ChangeModeConfig.CreateMode.Force;
					}
				}
				else if (config.IsCheckTodayLabelOnly)
				{
					config3.Mode = ChangeModeConfig.CreateMode.TodayLabelOnly;
				}
				else
				{
					config3.Mode = ChangeModeConfig.CreateMode.SubOnly;
				}
				ChangeMode(Mode, config3);
			}
		}
	}

	private void OnReceiveItemPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN || mGiftItemList.GetListItemCount() <= 0)
		{
			return;
		}
		mState.Reset(STATE.WAIT);
		mIsSingleAdvReceive = false;
		mIsMultiReceive = true;
		mReceiveAllNoticeDialog = null;
		mReceiveAllResultDialog = null;
		ScrollBoxDialog.CreateConfig dialogConfig = new ScrollBoxDialog.CreateConfig
		{
			Title = Localization.Get("MailBOX_Receive_Result_Title00"),
			DescriptionSize = 21,
			DisableOpneSE = false,
			DisableButtonSE = false,
			Style = ScrollBoxDialog.CONFIRM_DIALOG_STYLE.OK_ONLY,
			Size = DIALOG_SIZE.MEDIUM,
			VisibleSeparator = false
		};
		List<ScrollBoxDialog.CreateConfig.DescriptionInfo> receiveItemDesc = new List<ScrollBoxDialog.CreateConfig.DescriptionInfo>();
		bool isOverflowItem = false;
		bool isSupportItem = false;
		bool isInfinityHeart = false;
		bool flag = false;
		bool isMessageCard = false;
		List<KeyValuePair<GiftListItem, SubcurrencyInfo>> subcurrencyIds = new List<KeyValuePair<GiftListItem, SubcurrencyInfo>>();
		mMultiReceiveItemCount.Clear();
		mCompleteNotifyDesc.Clear();
		AdvNetwork.Clear();
		Func<GiftMailType, int> fnGetGiftSourceType = delegate(GiftMailType fn_mail_type)
		{
			switch (fn_mail_type)
			{
			case GiftMailType.LOCAL:
				return 2;
			case GiftMailType.LOCAL_EVENT:
				return 3;
			case GiftMailType.REWARD:
				return 4;
			case GiftMailType.GIFT:
				return 6;
			case GiftMailType.LOCAL_MAP:
				return 7;
			case GiftMailType.ADV:
			case GiftMailType.FROM_ADV:
			case GiftMailType.LOCAL_ADV:
				return 55;
			case GiftMailType.SUPPORT:
				return 99;
			case GiftMailType.LOCAL_TROPHY:
				return 13;
			default:
				return 0;
			}
		};
		mGame.PlaySe("SE_POSITIVE", -1);
		List<Transform> list = new List<Transform>();
		List<KeyValuePair<Transform, DateTime>> list2 = new List<KeyValuePair<Transform, DateTime>>();
		List<Transform> list3 = new List<Transform>();
		for (int num = mGiftItemList.mList.transform.childCount - 1; num >= 0; num--)
		{
			Transform child = mGiftItemList.mList.transform.GetChild(num);
			if (!(child == null))
			{
				Transform transform = child.FindChild("GiftBoard");
				if (!(transform == null))
				{
					GiftListItem giftListItem = mGiftItemList.FindListItem(transform.gameObject) as GiftListItem;
					if (giftListItem != null)
					{
						DateTime? expired = giftListItem.expired;
						if (expired.HasValue)
						{
							list2.Add(new KeyValuePair<Transform, DateTime>(transform, giftListItem.expired.Value));
						}
						else
						{
							list3.Add(transform);
						}
					}
				}
			}
		}
		list = (from _ in list2
			orderby _.Value
			select _.Key).Concat(list3).ToList();
		List<GiftListItem> list4 = new List<GiftListItem>();
		for (int j = 0; j < list.Count; j++)
		{
			Transform transform2 = list[j];
			if (transform2 == null)
			{
				continue;
			}
			GiftListItem item = mGiftItemList.FindListItem(transform2.gameObject) as GiftListItem;
			if (item == null)
			{
				continue;
			}
			GiftMailType mailType = item.gift.GetGiftSource();
			int num2 = fnGetGiftSourceType(mailType);
			if (num2 == 99)
			{
				isSupportItem = true;
			}
			else if (item.gift.Data.ItemCategory == 34)
			{
				isMessageCard = true;
			}
			else
			{
				if (item.gift.Data.MessageCardID > -1 && IsMessageCardAvalable(item.gift))
				{
					continue;
				}
				bool flag2 = true;
				bool flag3 = false;
				bool flag4 = false;
				switch ((Def.ITEM_CATEGORY)item.gift.Data.ItemCategory)
				{
				case Def.ITEM_CATEGORY.CONSUME:
					switch ((Def.CONSUME_ID)item.gift.Data.ItemKind)
					{
					case Def.CONSUME_ID.MAIN_CURRENCY_EX:
					{
						flag2 = false;
						GiftListItem giftListItem2 = item;
						giftListItem2.sendCategory = item.gift.Data.ItemCategory;
						giftListItem2.sendkind = item.gift.Data.ItemKind;
						list4.Add(giftListItem2);
						subcurrencyIds.Add(new KeyValuePair<GiftListItem, SubcurrencyInfo>(giftListItem2, new SubcurrencyInfo
						{
							RemoteID = item.gift.Data.RemoteID,
							Quantity = item.gift.Data.Quantity
						}));
						break;
					}
					case Def.CONSUME_ID.HEART:
					case Def.CONSUME_ID.HEART_EX:
						if (mGame.mPlayer.Data.mMugenHeart != 0 || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60))
						{
							flag2 = false;
							flag3 = true;
							isInfinityHeart = true;
							flag = true;
						}
						else if (item.gift.Data.ItemID == 0)
						{
							flag4 = true;
						}
						else if (mGame.mPlayer.LifeCount + item.gift.Data.Quantity > mGame.mPlayer.MaxLifeCount)
						{
							int num3 = mGame.mPlayer.MaxLifeCount - mGame.mPlayer.LifeCount;
							mGame.mPlayer.AddLifeCount(num3);
							item.gift.Data.Quantity -= num3;
							AddMultiReceiveItemCount(Localization.Get("Gift_ItemNameHeart"), num3);
							flag2 = false;
							flag3 = true;
							isOverflowItem = true;
							flag = true;
						}
						else
						{
							AddMultiReceiveItemCount(Localization.Get("Gift_ItemNameHeart"), item.gift.Data.Quantity);
							mGame.mPlayer.AddLifeCount(item.gift.Data.Quantity);
						}
						break;
					case Def.CONSUME_ID.GROWUP:
					{
						mGame.mPlayer.AddGrowup(item.gift.Data.Quantity);
						int quantity2 = item.gift.Data.Quantity;
						int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
						int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
						int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
						int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
						int advLastStage = mGame.mPlayer.AdvLastStage;
						int advMaxStamina = mGame.mPlayer.AdvMaxStamina;
						int get_type = 4;
						if (mailType == GiftMailType.SUPPORT)
						{
							get_type = 99;
						}
						ServerCram.ArcadeGetItem(quantity2, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 10, 1, get_type, advLastStage, advMaxStamina);
						int num5 = 0;
						List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
						for (int k = 0; k < playStage.Count; k++)
						{
							if (playStage[k].Series == 0)
							{
								num5 = playStage[k].Level;
								break;
							}
						}
						if (num5 > 800)
						{
							AddMultiReceiveItemCount(Localization.Get("ConsumeItemDesc_Growup"), item.gift.Data.Quantity);
						}
						break;
					}
					case Def.CONSUME_ID.GROWUP_PIECE:
						AddMultiReceiveItemCount(Localization.Get("ConsumeItemDesc_GrowupPiece"), item.gift.Data.Quantity);
						mGame.mPlayer.AddGrowupPiece(item.gift.Data.Quantity);
						if (mGame.mPlayer.GrowUpPiece >= 24)
						{
							SetCompleteNoticeData(Localization.Get("MailBOX_Receive_Result_Desc_GrowUp_00") + "\n" + string.Format(Localization.Get("MailBOX_Receive_Result_Desc_GrowUp_01"), mGame.mPlayer.GrowUpPiece / 24));
							int num4 = 0;
							while (mGame.mPlayer.GrowUpPiece >= 24)
							{
								mGame.mPlayer.SubGrowupPiece(24);
								num4++;
							}
							mGame.mPlayer.AddGrowup(num4);
							mGame.Save();
							ServerCram.ArcadeGetItem(num4, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 10, 1, 30, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
						}
						ServerCram.ArcadeGetItem(item.gift.Data.Quantity, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 11, 1, GetCramGetType(item.gift), mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
						break;
					case Def.CONSUME_ID.HEART_PIECE:
						AddMultiReceiveItemCount(Localization.Get("ConsumeItemDesc_HeartPiece"), item.gift.Data.Quantity);
						mGame.mPlayer.AddHeartPiece(item.gift.Data.Quantity);
						if (mGame.mPlayer.HeartUpPiece >= 15)
						{
							SetCompleteNoticeData(Localization.Get("MailBOX_Receive_Result_Desc_Heart_00") + "\n" + string.Format(Localization.Get("MailBOX_Receive_Result_Desc_Heart_01"), mGame.mPlayer.HeartUpPiece / 15));
							mGame.mPlayer.SubHeartPiece(15);
							GiftItem giftItem = new GiftItem();
							giftItem.Data.Message = Localization.Get("Gift_RatingReward");
							giftItem.Data.ItemCategory = 1;
							giftItem.Data.ItemKind = 1;
							giftItem.Data.ItemID = 1;
							giftItem.Data.Quantity = 1;
							mGame.mPlayer.AddGift(giftItem);
							ServerCram.ArcadeGetItem(1, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 2, 1, 30, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
						}
						ServerCram.ArcadeGetItem(item.gift.Data.Quantity, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 9, 1, GetCramGetType(item.gift), mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
						break;
					case Def.CONSUME_ID.ROADBLOCK_KEY:
						AddMultiReceiveItemCount(Localization.Get("KeyItemInfo_RibbonKey_Title"), item.gift.Data.Quantity);
						mGame.mPlayer.AddRoadBlockKey(item.gift.Data.Quantity, Def.SERIES.SM_FIRST);
						break;
					case Def.CONSUME_ID.ROADBLOCK_KEY_R:
						AddMultiReceiveItemCount(Localization.Get("KeyItemInfo_RibbonKey_Title"), item.gift.Data.Quantity);
						mGame.mPlayer.AddRoadBlockKey(item.gift.Data.Quantity, Def.SERIES.SM_R);
						break;
					case Def.CONSUME_ID.ROADBLOCK_KEY_S:
						AddMultiReceiveItemCount(Localization.Get("KeyItemInfo_RibbonKey_Title"), item.gift.Data.Quantity);
						mGame.mPlayer.AddRoadBlockKey(item.gift.Data.Quantity, Def.SERIES.SM_S);
						break;
					case Def.CONSUME_ID.ROADBLOCK_KEY_SS:
						AddMultiReceiveItemCount(Localization.Get("KeyItemInfo_RibbonKey_Title"), item.gift.Data.Quantity);
						mGame.mPlayer.AddRoadBlockKey(item.gift.Data.Quantity, Def.SERIES.SM_SS);
						break;
					case Def.CONSUME_ID.ROADBLOCK_KEY_STARS:
						AddMultiReceiveItemCount(Localization.Get("KeyItemInfo_RibbonKey_Title"), item.gift.Data.Quantity);
						mGame.mPlayer.AddRoadBlockKey(item.gift.Data.Quantity, Def.SERIES.SM_STARS);
						break;
					case Def.CONSUME_ID.ROADBLOCK_KEY_GF00:
						AddMultiReceiveItemCount(Localization.Get("KeyItemInfo_RibbonKey_Title"), item.gift.Data.Quantity);
						mGame.mPlayer.AddRoadBlockKey(item.gift.Data.Quantity, Def.SERIES.SM_GF00);
						break;
					case Def.CONSUME_ID.ROADBLOCK_TICKET:
						AddMultiReceiveItemCount(Localization.Get("ConsumeItemDesc_RoadBlockTicket"), item.gift.Data.Quantity);
						mGame.mPlayer.AddRoadBlockTicket(item.gift.Data.Quantity);
						break;
					case Def.CONSUME_ID.TROPHY:
						AddMultiReceiveItemCount(Localization.Get("Ranking_TrophyNum"), item.gift.Data.Quantity);
						mGame.mPlayer.AddTrophy(item.gift.Data.Quantity);
						break;
					case Def.CONSUME_ID.OLDEVENT_KEY:
					{
						AddMultiReceiveItemCount(Localization.Get("ConsumeItemDesc_OldEventKey"), item.gift.Data.Quantity);
						mGame.mPlayer.AddItemCount(Def.ITEM_CATEGORY.CONSUME, 20, item.gift.Data.Quantity);
						int cramGetType = GetCramGetType(item.gift);
						ServerCram.ArcadeGetItem(item.gift.Data.Quantity, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, 20, 1, cramGetType, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
						break;
					}
					}
					break;
				case Def.ITEM_CATEGORY.FREE_BOOSTER:
				{
					Def.BOOSTER_KIND bOOSTER_KIND = EnumHelper.FromObject(item.gift.Data.ItemKind, Def.BOOSTER_KIND.NONE);
					if (bOOSTER_KIND != 0 && (bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART15 || bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART30 || bOOSTER_KIND == Def.BOOSTER_KIND.MUGEN_HEART60) && (mGame.mPlayer.Data.mMugenHeart != 0 || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60)))
					{
						flag2 = false;
						flag3 = true;
						flag4 = true;
						isInfinityHeart = true;
						flag = true;
					}
					else if (!flag4)
					{
						switch (bOOSTER_KIND)
						{
						case Def.BOOSTER_KIND.MUGEN_HEART15:
						case Def.BOOSTER_KIND.MUGEN_HEART30:
						case Def.BOOSTER_KIND.MUGEN_HEART60:
							AddMultiReceiveItemCount(Localization.Get("Mugen_Heart"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.ADD_SCORE:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_ADD_SCORE"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.COLOR_CRUSH:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_COLOR_CRUSH"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.RAINBOW:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_RAINOBOW"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.SELECT_COLLECT:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_SELECT_COLLECT"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.SELECT_ONE:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_SELECT_ONE"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.SKILL_CHARGE:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_SKILL_CHARGE"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.TAP_BOMB2:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_TAP_BOMB2"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.WAVE_BOMB:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_WAVE_BOMB"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.BLOCK_CRUSH:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_BLOCK_CRUSH"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.PAIR_MAKER:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_PAIR_MAKER"), item.gift.Data.Quantity);
							break;
						case Def.BOOSTER_KIND.ALL_CRUSH:
							AddMultiReceiveItemCount(Localization.Get("BoosterName_ALL_CRUSH"), item.gift.Data.Quantity);
							break;
						}
						mGame.mPlayer.AddFreeBoosterNum(bOOSTER_KIND, item.gift.Data.Quantity);
					}
					break;
				}
				case Def.ITEM_CATEGORY.ACCESSORY:
				{
					AccessoryData accessoryData = mGame.GetAccessoryData(item.gift.Data.ItemKind);
					mGame.mPlayer.AddAccessoryFromGift(accessoryData);
					if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.WALL_PAPER)
					{
						List<AccessoryData> wallPaperPieces = mGame.GetWallPaperPieces(accessoryData.GetWallPaperIndex());
						for (int n = 0; n < wallPaperPieces.Count; n++)
						{
							mGame.mPlayer.AddAccessoryFromGift(wallPaperPieces[n]);
						}
					}
					mGame.mPlayer.Data.UnlockAccessoriesNoticeCount = mGame.mPlayer.Data.Accessories.Count;
					mStageOpenGiftReceived = true;
					break;
				}
				case Def.ITEM_CATEGORY.COMPANION:
				{
					List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
					for (int num7 = 0; num7 < accessoryByType.Count; num7++)
					{
						if (accessoryByType[num7].GetCompanionID() == item.gift.Data.ItemKind)
						{
							mGame.mPlayer.AddAccessoryFromGift(accessoryByType[num7]);
							break;
						}
					}
					mGame.AchievementPartnerUnlock();
					mStageOpenGiftReceived = true;
					break;
				}
				case Def.ITEM_CATEGORY.WALLPAPER:
				{
					AccessoryData accessoryWallPaperData = mGame.GetAccessoryWallPaperData(item.gift.Data.ItemKind);
					mGame.mPlayer.AddAccessory(accessoryWallPaperData);
					List<AccessoryData> wallPaperPieces2 = mGame.GetWallPaperPieces(accessoryWallPaperData.GetWallPaperIndex());
					mGame.mPlayer.SetCollectionNotify(wallPaperPieces2[0].Index);
					SetCompleteNoticeData(Localization.Get("MailBOX_Receive_Result_Desc_WallPaper_00"));
					AddMultiReceiveItemCount(Localization.Get("ConsumeItemDesc_WallPaper"), item.gift.Data.Quantity);
					break;
				}
				case Def.ITEM_CATEGORY.WALLPAPER_PIECE:
				{
					AccessoryData accessoryWallPaperPieceData = mGame.GetAccessoryWallPaperPieceData(item.gift.Data.ItemKind, item.gift.Data.ItemID);
					VisualCollectionData visualCollectionData = mGame.mVisualCollectionData[accessoryWallPaperPieceData.IsFlip];
					int num6 = 0;
					for (int l = 0; l < visualCollectionData.piceIds.Length; l++)
					{
						if (mGame.mPlayer.IsAccessoryUnlock(visualCollectionData.piceIds[l]))
						{
							num6++;
						}
					}
					mGame.mPlayer.AddAccessory(accessoryWallPaperPieceData);
					if (visualCollectionData.piceIds.Length - 1 == num6)
					{
						num6 = 0;
						for (int m = 0; m < visualCollectionData.piceIds.Length; m++)
						{
							if (mGame.mPlayer.IsAccessoryUnlock(visualCollectionData.piceIds[m]))
							{
								num6++;
							}
						}
						if (visualCollectionData.piceIds.Length == num6)
						{
							SetCompleteNoticeData(Localization.Get("MailBOX_Receive_Result_Desc_WallPaper_00"));
							mGame.mPlayer.SetCollectionNotify(visualCollectionData.accessoryId);
						}
					}
					AddMultiReceiveItemCount(Localization.Get("WallpaperPiece"), item.gift.Data.Quantity);
					break;
				}
				case Def.ITEM_CATEGORY.SKIN:
					mGame.mPlayer.UnlockSkin(item.gift.Data.ItemKind);
					break;
				case Def.ITEM_CATEGORY.PRESENT_HEART:
					if (mGame.mPlayer.Data.mMugenHeart != 0 || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30) || 0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60))
					{
						flag2 = false;
						flag3 = true;
						isInfinityHeart = true;
						flag = true;
					}
					else if (item.gift.Data.ItemKind == 1 || item.gift.Data.ItemKind == 2)
					{
						if (mGame.mPlayer.LifeCount + item.gift.Data.Quantity > mGame.mPlayer.MaxLifeCount)
						{
							int num8 = mGame.mPlayer.MaxLifeCount - mGame.mPlayer.LifeCount;
							mGame.mPlayer.AddLifeCount(num8);
							item.gift.Data.Quantity -= num8;
							AddMultiReceiveItemCount(Localization.Get("Gift_ItemNameHeart"), num8);
							flag2 = false;
							flag3 = true;
							isOverflowItem = true;
							flag = true;
						}
						else
						{
							AddMultiReceiveItemCount(Localization.Get("Gift_ItemNameHeart"), item.gift.Data.Quantity);
							mGame.mPlayer.AddLifeCount(item.gift.Data.Quantity);
						}
					}
					break;
				case Def.ITEM_CATEGORY.LABYRINTH_JEWEL:
				{
					int itemKind2 = item.gift.Data.ItemKind;
					int quantity3 = item.gift.Data.Quantity;
					mGame.MakeMapInitializeData(Def.SERIES.SM_EV, itemKind2);
					mGame.AddEventPoint(itemKind2, quantity3);
					AddMultiReceiveItemCount(Localization.Get("Labyrinth_Jwel"), quantity3);
					GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(itemKind2);
					if (labyrinthData == null)
					{
						break;
					}
					labyrinthData = GlobalVariables.Instance.GetLabyrinthData(itemKind2);
					if (labyrinthData != null && labyrinthData.RewardAccessoryDataList != null)
					{
						StringBuilder stringBuilder = new StringBuilder();
						stringBuilder.AppendLine("MailBOX_Receive_Result_Desc_Jwel_00");
						stringBuilder.AppendLine("MailBOX_Receive_Result_Desc_Jwel_01");
						SetCompleteNoticeData(stringBuilder.ToString());
						labyrinthData.RewardAccessoryDataList.RemoveAll((AccessoryData a) => a != null);
						labyrinthData.RewardAccessoryDataList = null;
						GlobalVariables.Instance.SetLabyrinthData(itemKind2, labyrinthData);
					}
					break;
				}
				case Def.ITEM_CATEGORY.BINGO_TICKET:
				{
					int itemKind = item.gift.Data.ItemKind;
					int quantity = item.gift.Data.Quantity;
					if (!mGame.mPlayer.Data.mBingoEventDataDict.ContainsKey(itemKind))
					{
						mGame.mPlayer.Data.MakeBingoEventData(itemKind);
					}
					mGame.mPlayer.Data.mBingoEventDataDict[itemKind].mReLotteryTicket += quantity;
					AddMultiReceiveItemCount(GetReLotteryTicketName(itemKind, quantity), quantity);
					break;
				}
				}
				if (flag2)
				{
					mGame.mPlayer.RemoveGift(item.gift);
					int index = mGiftItemList.FindListIndexOf(transform2.gameObject);
					mGiftItemList.RemoveListItem(index);
					Def.ITEM_CATEGORY itemCategory = (Def.ITEM_CATEGORY)item.gift.Data.ItemCategory;
					if (itemCategory == Def.ITEM_CATEGORY.FREE_BOOSTER || itemCategory == Def.ITEM_CATEGORY.ACCESSORY)
					{
						int num9 = fnGetGiftSourceType(mailType);
						Def.BOOSTER_KIND a_kind = Def.BOOSTER_KIND.NONE;
						int a_num = 0;
						switch ((Def.ITEM_CATEGORY)item.gift.Data.ItemCategory)
						{
						case Def.ITEM_CATEGORY.ACCESSORY:
						{
							AccessoryData accessoryData2 = mGame.GetAccessoryData(item.gift.Data.ItemKind);
							if (accessoryData2.AccessoryType == AccessoryData.ACCESSORY_TYPE.BOOSTER)
							{
								accessoryData2.GetBooster(out a_kind, out a_num);
							}
							break;
						}
						case Def.ITEM_CATEGORY.BOOSTER:
							a_kind = (Def.BOOSTER_KIND)item.gift.Data.ItemKind;
							a_num = item.gift.Data.Quantity;
							break;
						}
						if (num9 == 55)
						{
							num9 = 0;
						}
						if (a_kind != 0 && num9 != 0)
						{
							mGame.SendGetBoost(a_num, 0, num9, a_kind);
						}
					}
					switch (mailType)
					{
					case GiftMailType.SUPPORT:
						switch ((Def.ITEM_CATEGORY)item.gift.Data.ItemCategory)
						{
						case Def.ITEM_CATEGORY.ACCESSORY:
						{
							AccessoryData accessoryData3 = mGame.mAccessoryData[item.gift.Data.ItemKind];
							if (accessoryData3.AccessoryType == AccessoryData.ACCESSORY_TYPE.TROPHY)
							{
								ServerCram.GetTrophy(accessoryData3.GetGotIDByNum(), mGame.mPlayer.GetTrophy(), 99, item.gift.Data.ItemKind, mGame.mOptions.PurchaseCount);
							}
							break;
						}
						case Def.ITEM_CATEGORY.CONSUME:
							if (item.gift.Data.ItemKind == 14)
							{
								ServerCram.GetTrophy(item.gift.Data.Quantity, mGame.mPlayer.GetTrophy(), 99, -1, mGame.mOptions.PurchaseCount);
							}
							break;
						}
						break;
					case GiftMailType.REWARD:
						if (item.gift.Data.ItemKind == 14)
						{
							ServerCram.GetTrophy(item.gift.Data.Quantity, mGame.mPlayer.GetTrophy(), 5, -1, mGame.mOptions.PurchaseCount);
						}
						break;
					case GiftMailType.LOCAL_EVENT:
						if (item.gift.Data.ItemKind == 14)
						{
							ServerCram.GetTrophy(item.gift.Data.Quantity, mGame.mPlayer.GetTrophy(), 5, -1, mGame.mOptions.PurchaseCount);
						}
						break;
					}
					Action action = delegate
					{
						ServerCram.ArcadeGetItem(item.gift.Data.Quantity, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, item.gift.Data.ItemCategory, item.gift.Data.ItemKind, item.gift.Data.ItemID, fnGetGiftSourceType(mailType), mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
					};
					switch ((Def.ITEM_CATEGORY)item.gift.Data.ItemCategory)
					{
					case Def.ITEM_CATEGORY.PRESENT_HEART:
					case Def.ITEM_CATEGORY.ADV_CONSUME:
					case Def.ITEM_CATEGORY.ADV_COIN:
					case Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL:
						action();
						break;
					case Def.ITEM_CATEGORY.CONSUME:
					{
						int itemKind3 = item.gift.Data.ItemKind;
						if (itemKind3 == 1 || itemKind3 == 2 || itemKind3 == 7)
						{
							action();
						}
						break;
					}
					}
					mGame.Save();
				}
				else if (flag3)
				{
					mGame.Save();
				}
			}
		}
		Action<bool> fnOpenResultDialog = delegate(bool fn_is_connect_success)
		{
			if (isSupportItem)
			{
				string[] array = Localization.Get("MailBOX_Receive_Result_Error_Desc02").Split('\n');
				if (array != null)
				{
					for (int num10 = array.Length - 1; num10 >= 0; num10--)
					{
						receiveItemDesc.Insert(0, new ScrollBoxDialog.CreateConfig.DescriptionInfo
						{
							Message = array[num10],
							Color = Color.red,
							IgnoreNextSeparator = (num10 + 1 < array.Length)
						});
					}
				}
			}
			if (isInfinityHeart)
			{
				string[] array2 = Localization.Get("MailBOX_Receive_Result_Error_Desc03").Split('\n');
				if (array2 != null)
				{
					for (int num11 = array2.Length - 1; num11 >= 0; num11--)
					{
						receiveItemDesc.Insert(0, new ScrollBoxDialog.CreateConfig.DescriptionInfo
						{
							Message = array2[num11],
							Color = Color.red,
							IgnoreNextSeparator = (num11 + 1 < array2.Length)
						});
					}
				}
			}
			if (isOverflowItem)
			{
				string[] array3 = Localization.Get("MailBOX_Receive_Result_Error_Desc01").Split('\n');
				if (array3 != null)
				{
					for (int num12 = array3.Length - 1; num12 >= 0; num12--)
					{
						receiveItemDesc.Insert(0, new ScrollBoxDialog.CreateConfig.DescriptionInfo
						{
							Message = array3[num12],
							Color = Color.red,
							IgnoreNextSeparator = (num12 + 1 < array3.Length)
						});
					}
				}
			}
			if (isMessageCard)
			{
				string[] array4 = Localization.Get("MailBOX_Receive_Result_Error_Desc04").Split('\n');
				if (array4 != null)
				{
					for (int num13 = array4.Length - 1; num13 >= 0; num13--)
					{
						receiveItemDesc.Insert(0, new ScrollBoxDialog.CreateConfig.DescriptionInfo
						{
							Message = array4[num13],
							Color = Color.red,
							IgnoreNextSeparator = (num13 + 1 < array4.Length)
						});
					}
				}
			}
			if (!fn_is_connect_success)
			{
				string[] array5 = Localization.Get("MailBOX_Receive_Result_Error_Desc00").Split('\n');
				if (array5 != null)
				{
					for (int num14 = array5.Length - 1; num14 >= 0; num14--)
					{
						receiveItemDesc.Insert(0, new ScrollBoxDialog.CreateConfig.DescriptionInfo
						{
							Message = array5[num14],
							Color = Color.red,
							IgnoreNextSeparator = (num14 + 1 < array5.Length)
						});
					}
				}
			}
			foreach (KeyValuePair<string, int> item2 in mMultiReceiveItemCount)
			{
				if (item2.Value > 0)
				{
					string[] array6 = item2.Key.Split('\n');
					for (int num15 = 0; num15 < array6.Length; num15++)
					{
						if (num15 + 1 != array6.Length)
						{
							receiveItemDesc.Add(new ScrollBoxDialog.CreateConfig.DescriptionInfo
							{
								Message = array6[num15],
								Color = Def.DEFAULT_MESSAGE_COLOR
							});
						}
						else
						{
							receiveItemDesc.Add(new ScrollBoxDialog.CreateConfig.DescriptionInfo
							{
								Message = array6[num15] + string.Format(Localization.Get("Gift_Number"), item2.Value),
								Color = Def.DEFAULT_MESSAGE_COLOR
							});
						}
					}
				}
			}
			mState.Reset(STATE.WAIT);
			dialogConfig.Description = receiveItemDesc.ToArray();
			mReceiveAllResultDialog = Util.CreateGameObject("ResultDialog", base.ParentGameObject).AddComponent<ScrollBoxDialog>();
			mReceiveAllResultDialog.Init(dialogConfig);
			mReceiveAllResultDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component2 = mReceiveAllResultDialog.GetComponent<BoxCollider>();
				if ((bool)component2)
				{
					component2.size = new Vector3(1384f, 1384f, 1f);
				}
			});
			mReceiveAllResultDialog.SetClosedCallback(delegate
			{
				mIsMultiReceive = false;
				if (mAdvConnectFigureInfo != null)
				{
					foreach (KeyValuePair<string, AdvConnectFigureInfo> item3 in mAdvConnectFigureInfo)
					{
						StringBuilder str = new StringBuilder();
						Action action2 = delegate
						{
							string[] array7 = str.ToString().Split('\n');
							if (array7 != null)
							{
								for (int num16 = 0; num16 < array7.Length; num16++)
								{
									if (num16 < array7.Length - 1)
									{
										SetCompleteNoticeData(array7[num16], false);
									}
									else
									{
										SetCompleteNoticeData(array7[num16]);
									}
								}
							}
							str.Remove(0, str.Length);
						};
						bool flag5 = false;
						if (item3.Value.NewSkillLevel > item3.Value.OldSkillLevel)
						{
							str.AppendLine(Localization.Get("MailBOX_Receive_Result_Desc_SkillUp_00"));
							str.AppendLine(string.Format(Localization.Get("MailBOX_Receive_Result_Desc_SkillUp_01"), item3.Key));
							str.AppendLine(string.Format(Localization.Get("MailBOX_Receive_Result_Desc_SkillUp_02"), item3.Value.OldSkillLevel, item3.Value.NewSkillLevel));
							flag5 = true;
						}
						if (item3.Value.IsOverFlowLevel())
						{
							if (flag5)
							{
								action2();
							}
							str.AppendLine(Localization.Get("AdvOmakeDesc01"));
							str.AppendLine(string.Format(Localization.Get("MailBOX_Receive_Result_Desc_SkillUp_01"), item3.Key));
							str.Append("   ").Append(Localization.Get("AdvMail_item_25_01")).Append(" ")
								.AppendLine(string.Format(Localization.Get("AdvEvPointRewardPoint00"), item3.Value.ExpPoint));
							if (item3.Value.SkillExpPoint > 0)
							{
								str.Append("   ").Append(Localization.Get("AdvMail_item_25_02")).AppendLine(string.Format(Localization.Get("Adv_Unit_SKILLUPITEM00"), item3.Value.SkillExpPoint));
							}
							flag5 = true;
						}
						if (flag5)
						{
							action2();
						}
					}
				}
				if (mCompleteNotifyDesc != null && mCompleteNotifyDesc.Count != 0)
				{
					mReceiveAllNoticeDialog = Util.CreateGameObject("NotifyDialog", base.ParentGameObject).AddComponent<ScrollBoxDialog>();
					dialogConfig = new ScrollBoxDialog.CreateConfig
					{
						Title = Localization.Get("MailBOX_Receive_Result_Title01"),
						DescriptionSize = 21,
						DisableOpneSE = false,
						DisableButtonSE = false,
						Style = ScrollBoxDialog.CONFIRM_DIALOG_STYLE.OK_ONLY,
						Size = DIALOG_SIZE.MEDIUM,
						Description = mCompleteNotifyDesc.ToArray(),
						VisibleSeparator = true
					};
					mReceiveAllNoticeDialog.Init(dialogConfig);
					mReceiveAllNoticeDialog.SetClosedCallback(delegate
					{
						mState.Reset(STATE.NETWORK_04_00, true);
						mReceiveAllNoticeDialog = null;
					});
					mReceiveAllNoticeDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
					{
						BoxCollider component = mReceiveAllNoticeDialog.GetComponent<BoxCollider>();
						if ((bool)component)
						{
							component.size = new Vector3(1384f, 1384f, 1f);
						}
					});
				}
				else
				{
					mState.Reset(STATE.NETWORK_04_00, true);
				}
				mReceiveAllResultDialog = null;
			});
		};
		if (AdvNetwork.Count > 0)
		{
			mState.Change(STATE.NETWORK_ADV_COMPLETE_MAIL);
			mMultiReceiveState = MULTI_RECEIVE_STATE.CONNECT;
			StartCoroutine(Routine_WaitCommunicateAdvItem(delegate(bool fn_connect_result)
			{
				if (subcurrencyIds.Count <= 0)
				{
					fnOpenResultDialog(fn_connect_result);
				}
				else
				{
					StartCoroutine(Routine_CommunicateSubcurrencyCharge(fnOpenResultDialog, fn_connect_result, subcurrencyIds.ToArray()));
				}
			}));
		}
		else if (subcurrencyIds.Count <= 0)
		{
			fnOpenResultDialog(true);
		}
		else
		{
			StartCoroutine(Routine_CommunicateSubcurrencyCharge(fnOpenResultDialog, true, subcurrencyIds.ToArray()));
		}
	}

	private void OnSendAllPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && Mode == GameMain.GiftUIMode.SEND && mGiftItemList.items.Count >= 1)
		{
			mAllGiftItems.Clear();
			mGame.PlaySe("SE_POSITIVE", -1);
			SendHeartAll();
		}
	}

	private void OnCreateItem_Gift(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		string imageName = string.Empty;
		string empty = string.Empty;
		GiftListItem giftListItem = item as GiftListItem;
		UISprite uISprite3 = Util.CreateSprite("Back", itemGO, image2);
		Util.SetSpriteInfo(uISprite3, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite3.width = (int)mGiftItemList.mList.cellWidth;
		uISprite3.height = (int)mGiftItemList.mList.cellHeight;
		Collider component = uISprite3.transform.parent.GetComponent<Collider>();
		if ((bool)component)
		{
			component.enabled = IsOperating;
		}
		if (giftListItem == null)
		{
			return;
		}
		GiftItemData data = giftListItem.gift.Data;
		string imageName2 = "instruction_panel5";
		if (giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ || giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_RES)
		{
			imageName2 = "instruction_panel5_2";
		}
		uISprite = Util.CreateSprite("GiftBoard", itemGO, image2);
		Util.SetSpriteInfo(uISprite, imageName2, num, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(528, 159);
		uISprite.transform.localScale = new Vector3(0.925f, 0.925f);
		empty = data.Message;
		switch (giftListItem.gift.GetGiftSource())
		{
		case GiftMailType.GIFT:
		case GiftMailType.FRIEND:
			empty = Localization.Get("Gift_From") + " " + empty;
			break;
		case GiftMailType.LOCAL:
		case GiftMailType.LOCAL_MAP:
			empty = Localization.Get("GetItemMail_Title");
			break;
		case GiftMailType.LOCAL_EVENT:
		case GiftMailType.LOCAL_ADV:
			empty = Localization.Get("GetItemMail_Title_03");
			break;
		case GiftMailType.LOCAL_TROPHY:
			empty = Localization.Get("GetItemMail_Title_06");
			break;
		}
		if (!string.IsNullOrEmpty(empty))
		{
			string[] array = empty.Split('_');
			empty = array[0];
		}
		UILabel uILabel = Util.CreateLabel("FromName", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, empty, num + 1, new Vector3(0f, 60f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(510, 26);
		MyFriend value = giftListItem.friend;
		bool flag = false;
		if (giftListItem.gift.GetGiftSource() == GiftMailType.GIFT)
		{
			int num2 = 0;
			bool flag2 = false;
			if (mGame.mPlayer.Friends.TryGetValue(data.FromID, out value))
			{
				num2 = value.Data.IconID;
			}
			if (num2 >= 10000)
			{
				num2 -= 10000;
				flag2 = true;
			}
			if (num2 >= mGame.mCompanionData.Count)
			{
				num2 = 0;
			}
			if (value != null)
			{
				imageName = mGame.mCompanionData[num2].iconName;
				uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
				if (GameMain.IsFacebookIconEnable() && flag2)
				{
					Util.SetSpriteInfo(uISprite2, imageName, num + 3, new Vector3(-197f, 16f, 0f), Vector3.one, false);
					uISprite2.SetDimensions(50, 50);
					if (value.Icon != null)
					{
						UITexture uITexture = Util.CreateGameObject("Tex", uISprite.gameObject).AddComponent<UITexture>();
						uITexture.depth = num + 2;
						uITexture.transform.localPosition = new Vector3(-225f, -18f, 0f);
						uITexture.mainTexture = value.Icon;
						uITexture.SetDimensions(65, 65);
					}
					else
					{
						UISprite uISprite4 = Util.CreateSprite("Unknown", uISprite.gameObject, image);
						Util.SetSpriteInfo(uISprite4, "icon_chara_facebook", num + 1, new Vector3(-225f, -18f, 0f), Vector3.one, false);
						uISprite4.SetDimensions(65, 65);
					}
				}
				else
				{
					Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-213f, -10f, 0f), Vector3.one, false);
					uISprite2.SetDimensions(84, 84);
					imageName = "lv_" + value.Data.IconLevel;
					UISprite uISprite4 = Util.CreateSprite("Level", uISprite.gameObject, image);
					Util.SetSpriteInfo(uISprite4, imageName, num + 3, new Vector3(-213f, -47f, 0f), Vector3.one, false);
					if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(value.Data.Fbid))
					{
						string imageName3 = "icon_sns00_mini";
						uISprite4 = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
						Util.SetSpriteInfo(uISprite4, imageName3, num + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
					}
				}
			}
			else
			{
				imageName = "icon_chara_facebook";
				flag = true;
			}
		}
		else if (giftListItem.gift.GetGiftSource() == GiftMailType.SUPPORT || giftListItem.gift.GetGiftSource() == GiftMailType.REWARD || giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL || giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_EVENT || giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_MAP || giftListItem.gift.GetGiftSource() == GiftMailType.FROM_ADV || giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_TROPHY)
		{
			imageName = ((data.ItemCategory != 34) ? "icon_mailbox_acceptance00" : "icon_mailbox_acceptance02");
			flag = true;
		}
		else if (giftListItem.gift.GetGiftSource() == GiftMailType.LOCAL_ADV || giftListItem.gift.GetGiftSource() == GiftMailType.ADV)
		{
			imageName = "icon_mailbox_acceptance01";
			flag = true;
		}
		else
		{
			int num3 = data.IconID;
			bool flag3 = false;
			if (num3 >= 10000)
			{
				num3 -= 10000;
				flag3 = true;
			}
			if (num3 >= mGame.mCompanionData.Count)
			{
				num3 = 0;
			}
			Texture2D texture2D = null;
			if (GameMain.IsFacebookIconEnable() && flag3)
			{
				switch (mFBUserManager.GetIconStatus(data.FromID))
				{
				case FBUserData.STATE.DEFAULT:
				case FBUserData.STATE.FAILURE:
				{
					imageName = "icon_chara_facebook";
					uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
					Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-225f, -18f, 0f), Vector3.one, false);
					uISprite2.SetDimensions(65, 65);
					string iconName3 = mGame.mCompanionData[num3].iconName;
					UISprite uISprite7 = Util.CreateSprite("CharaIcon", uISprite2.gameObject, image);
					Util.SetSpriteInfo(uISprite7, iconName3, num + 4, new Vector3(32f, 32f, 0f), Vector3.one, false);
					uISprite7.SetDimensions(50, 50);
					break;
				}
				case FBUserData.STATE.LOADING:
				{
					imageName = "icon_chara_facebook02";
					uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
					Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-225f, -18f, 0f), Vector3.one, false);
					uISprite2.SetDimensions(65, 65);
					IconCheckData iconCheckData = new IconCheckData();
					iconCheckData.boardImage = uISprite;
					iconCheckData.loadingIcon = uISprite2;
					iconCheckData.uuid = data.FromID;
					iconCheckData.iconid = data.IconID;
					mIconCheckList.Add(iconCheckData);
					break;
				}
				case FBUserData.STATE.SUCCESS:
					texture2D = mFBUserManager.GetIcon(data.FromID);
					if (texture2D != null)
					{
						UITexture uITexture2 = Util.CreateGameObject("Icon", uISprite.gameObject).AddComponent<UITexture>();
						uITexture2.depth = num + 2;
						uITexture2.transform.localPosition = new Vector3(-225f, -18f, 0f);
						uITexture2.mainTexture = texture2D;
						uITexture2.SetDimensions(65, 65);
						string iconName = mGame.mCompanionData[num3].iconName;
						UISprite uISprite5 = Util.CreateSprite("CharaIcon", uITexture2.gameObject, image);
						Util.SetSpriteInfo(uISprite5, iconName, num + 4, new Vector3(32f, 32f, 0f), Vector3.one, false);
						uISprite5.SetDimensions(50, 50);
					}
					else
					{
						imageName = "icon_chara_facebook";
						uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
						Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-225f, -18f, 0f), Vector3.one, false);
						uISprite2.SetDimensions(65, 65);
						string iconName2 = mGame.mCompanionData[num3].iconName;
						UISprite uISprite6 = Util.CreateSprite("CharaIcon", uISprite2.gameObject, image);
						Util.SetSpriteInfo(uISprite6, iconName2, num + 4, new Vector3(32f, 32f, 0f), Vector3.one, false);
						uISprite6.SetDimensions(50, 50);
					}
					break;
				}
			}
			else
			{
				imageName = mGame.mCompanionData[num3].iconName;
				uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
				Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-213f, -10f, 0f), Vector3.one, false);
				uISprite2.SetDimensions(84, 84);
				int iconLevel = data.IconLevel;
				UISprite sprite = Util.CreateSprite("CharaLv", uISprite.gameObject, image2);
				Util.SetSpriteInfo(sprite, "lv_" + iconLevel, num + 4, new Vector3(-213f, -48f, 0f), Vector3.one, false);
				if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(data.Fbid))
				{
					string imageName4 = "icon_sns00_mini";
					UISprite sprite2 = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
					Util.SetSpriteInfo(sprite2, imageName4, num + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
				}
			}
		}
		if (flag)
		{
			uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, imageName, num + 2, new Vector3(-213f, -10f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(84, 84);
		}
		string buttonImage = string.Empty;
		imageName = string.Empty;
		BIJImage atlas = image2;
		string message = string.Empty;
		GetGiftItemDisplayData(giftListItem.gift, out imageName, out buttonImage, out message, out atlas);
		bool flag4 = false;
		if (giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGEHELP_REQ)
		{
			int num4 = 0;
			int num5 = 0;
			if (giftListItem.expired.HasValue)
			{
				DateTime value2 = giftListItem.expired.Value;
				TimeSpan timeSpan = value2 - DateTime.Now;
				if (timeSpan.Seconds < 0)
				{
					flag4 = true;
				}
				else
				{
					num4 = timeSpan.Minutes;
					num5 = timeSpan.Seconds;
				}
			}
			else
			{
				flag4 = true;
			}
			if (!flag4)
			{
				message = Util.MakeLText("FriendHelpBeRequest01") + "\n";
				message += Util.MakeLText("FriendHelpBeRequest_Time", num4, num5);
				buttonImage = "LC_button_play2";
			}
			else
			{
				message = Util.MakeLText("FriendHelp_Result01_Desc");
				buttonImage = "LC_button_ok";
			}
		}
		if (data.UsePlayerInfo)
		{
			UISprite uISprite8 = Util.CreateSprite("Linetop", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite8, "line00", num + 3, new Vector3(41f, -12f, 0f), Vector3.one, false);
			uISprite8.type = UIBasicSprite.Type.Sliced;
			uISprite8.SetDimensions(418, 6);
			if (giftListItem.gift.GiftItemCategory != Def.ITEM_CATEGORY.FRIEND_REQ)
			{
				uILabel = Util.CreateLabel("Message", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, message, num + 3, new Vector3(48f, -3f, 0f), 22, 0, 0, UIWidget.Pivot.Bottom);
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
				int series = data.Series;
				int a_main;
				int a_sub;
				Def.SplitStageNo(data.SeriesStage, out a_main, out a_sub);
				int stageNumber = a_main;
				mSmdToolsSeries.SMDSeries(uISprite.gameObject, num, series, stageNumber, -82f, -30f);
			}
			else
			{
				uILabel = Util.CreateLabel("ReDays", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, message, num + 3, new Vector3(-38f, -6f, 0f), 22, 0, 0, UIWidget.Pivot.Bottom);
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
				DateTime now = DateTime.Now;
				long num6 = ((data.RemoteTimeEpoch == 0) ? DateTimeUtil.BetweenDays0(data.CreateTime, now) : DateTimeUtil.BetweenDays0(giftListItem.gift.RemoteTime, now));
				long num7 = Constants.GIFT_EXPIRED_FRIEND_REQ_FREQ;
				message = Util.MakeLText("Gift_Remaining", num7 - num6);
				uILabel = Util.CreateLabel("limit", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, message, num + 3, new Vector3(130f, -6f, 0f), 22, 0, 0, UIWidget.Pivot.Bottom);
				uILabel.color = Color.red;
				int series2 = data.Series;
				List<PlayStageParam> playStage = data.PlayStage;
				int stageNumber2 = 1;
				if (playStage != null && playStage.Count != 0)
				{
					for (int i = 0; i < playStage.Count; i++)
					{
						if (playStage[i].Series == data.Series)
						{
							stageNumber2 = playStage[i].Level / 100;
							break;
						}
					}
				}
				else
				{
					series2 = 0;
					int a_main2;
					int a_sub2;
					Def.SplitStageNo(data.SeriesStage, out a_main2, out a_sub2);
					stageNumber2 = a_main2;
				}
				mSmdToolsSeries.SMDSeries(uISprite.gameObject, num, series2, stageNumber2, -82f, -30f);
			}
			switch (giftListItem.gift.GiftItemCategory)
			{
			case Def.ITEM_CATEGORY.FRIEND_REQ:
			case Def.ITEM_CATEGORY.STAGEHELP_REQ:
				if (!flag4)
				{
					UIButton uIButton2 = Util.CreateJellyImageButton("ButtonOK", uISprite.gameObject, image2);
					Util.SetImageButtonInfo(uIButton2, buttonImage, buttonImage, buttonImage, num + 5, new Vector3(67f, -43f, 0f), Vector3.one);
					Util.AddImageButtonMessage(uIButton2, this, "OnApplyPushed", UIButtonMessage.Trigger.OnClick);
					Collider component3 = uIButton2.GetComponent<Collider>();
					if ((bool)component3)
					{
						component3.enabled = IsOperating;
					}
					UIDragScrollView uIDragScrollView2 = uIButton2.gameObject.AddComponent<UIDragScrollView>();
					uIDragScrollView2.scrollView = mGiftItemList.mListScrollView;
					UIButton uIButton3 = Util.CreateJellyImageButton("ButtonORefuse", uISprite.gameObject, image2);
					Util.SetImageButtonInfo(uIButton3, "LC_button_denial", "LC_button_denial", "LC_button_denial", num + 5, new Vector3(195f, -43f, 0f), Vector3.one);
					Util.AddImageButtonMessage(uIButton3, this, "OnRefusePushed", UIButtonMessage.Trigger.OnClick);
					giftListItem.message = uILabel;
					component3 = uIButton3.GetComponent<Collider>();
					if ((bool)component3)
					{
						component3.enabled = IsOperating;
					}
					UIDragScrollView uIDragScrollView3 = uIButton3.gameObject.AddComponent<UIDragScrollView>();
					uIDragScrollView2.scrollView = mGiftItemList.mListScrollView;
				}
				else
				{
					UIButton uIButton4 = Util.CreateJellyImageButton("ButtonOK", uISprite.gameObject, image2);
					Util.SetImageButtonInfo(uIButton4, buttonImage, buttonImage, buttonImage, num + 5, new Vector3(67f, -43f, 0f), Vector3.one);
					Util.AddImageButtonMessage(uIButton4, this, "OnApplyPushed", UIButtonMessage.Trigger.OnClick);
					Collider component4 = uIButton4.GetComponent<Collider>();
					if ((bool)component4)
					{
						component4.enabled = IsOperating;
					}
					UIDragScrollView uIDragScrollView4 = uIButton4.gameObject.AddComponent<UIDragScrollView>();
					uIDragScrollView4.scrollView = mGiftItemList.mListScrollView;
				}
				break;
			default:
			{
				UIButton uIButton = Util.CreateJellyImageButton("ButtonOK", uISprite.gameObject, image2);
				Util.SetImageButtonInfo(uIButton, buttonImage, buttonImage, buttonImage, num + 5, new Vector3(67f, -43f, 0f), Vector3.one);
				Util.AddImageButtonMessage(uIButton, this, "OnApplyPushed", UIButtonMessage.Trigger.OnClick);
				Collider component2 = uIButton.GetComponent<Collider>();
				if ((bool)component2)
				{
					component2.enabled = IsOperating;
				}
				UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
				uIDragScrollView.scrollView = mGiftItemList.mListScrollView;
				break;
			}
			}
			return;
		}
		UISprite uISprite9 = Util.CreateSprite("Linetop", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite9, "line00", num + 3, new Vector3(41f, -14f, 0f), Vector3.one, false);
		uISprite9.type = UIBasicSprite.Type.Sliced;
		uISprite9.SetDimensions(400, 6);
		if (giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.PRESENT_HEART || giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FRIEND_MEDAL)
		{
			uILabel = Util.CreateLabel("Message", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, message, num + 3, new Vector3(-111f, 11f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uISprite9 = Util.CreateSprite("RecieveIcon", uISprite.gameObject, atlas);
			Util.SetSpriteInfo(uISprite9, imageName, num + 3, new Vector3(-138f, 13f, 0f), Vector3.one, false);
			uISprite9.SetDimensions(54, 54);
			if (value != null)
			{
				int series3 = value.Data.Series;
				List<PlayStageParam> playStage2 = value.Data.PlayStage;
				int stageNumber3 = 1;
				if (playStage2 != null && playStage2.Count != 0)
				{
					for (int j = 0; j < playStage2.Count; j++)
					{
						if (playStage2[j].Series == value.Data.Series)
						{
							stageNumber3 = playStage2[j].Level / 100;
							break;
						}
					}
				}
				else
				{
					series3 = 0;
					int a_main3;
					int a_sub3;
					Def.SplitStageNo(value.Data.Level, out a_main3, out a_sub3);
					stageNumber3 = a_main3;
				}
				mSmdToolsSeries.SMDSeries(uISprite.gameObject, num, series3, stageNumber3, -73f, -30f);
			}
		}
		else
		{
			if (!string.IsNullOrEmpty(imageName))
			{
				uISprite9 = Util.CreateSprite("RecieveIcon", uISprite.gameObject, atlas);
				Util.SetSpriteInfo(uISprite9, imageName, num + 3, new Vector3(-138f, 14f, 0f), Vector3.one, false);
				uISprite9.SetDimensions(50, 50);
			}
			uILabel = Util.CreateLabel("Message", uISprite.gameObject, atlasFont);
			Vector3 zero = Vector3.zero;
			UIWidget.Pivot pivot = UIWidget.Pivot.Left;
			Vector2 zero2 = Vector2.zero;
			if (giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.ADV_FIGURE)
			{
				zero = new Vector3(-11f, 12f);
				pivot = UIWidget.Pivot.Center;
				zero2 = new Vector2(295f, 45f);
				uILabel.spacingY = 2;
			}
			else if (giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGE_OPEN || giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.STAGE_OPEN_BUNDLE)
			{
				zero = new Vector3(-160f, 12f);
				zero2 = new Vector2(295f, 56f);
				uILabel.spacingY = 0;
			}
			else if (giftListItem.gift.GiftItemCategory == Def.ITEM_CATEGORY.MESSAGE_CARD)
			{
				zero = new Vector3(-160f, 8f);
				zero2 = new Vector2(244f, 26f);
				uILabel.spacingY = 0;
			}
			else
			{
				if (message.Contains("\n"))
				{
					zero = new Vector3(-107f, 12f);
					zero2 = new Vector2(244f, 52f);
				}
				else
				{
					zero = new Vector3(-107f, 8f);
					zero2 = new Vector2(244f, 26f);
				}
				uILabel.spacingY = 0;
			}
			Util.SetLabelInfo(uILabel, message, num + 3, zero, 26, 0, 0, pivot);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions((int)zero2.x, (int)zero2.y);
		}
		DateTime? expired = giftListItem.expired;
		if (expired.HasValue)
		{
			uILabel = Util.CreateLabel("limit", uISprite.gameObject, atlasFont);
			DateTime now2 = DateTime.Now;
			long num8 = DateTimeUtil.BetweenDays0(now2, giftListItem.expired.Value);
			string empty2 = string.Empty;
			empty2 = ((num8 > 1) ? string.Format(Localization.Get("Gift_Remaining"), num8) : Localization.Get("Gift_RemainingToday"));
			Util.SetLabelInfo(uILabel, empty2, num + 3, new Vector3(243f, 11f, 0f), 22, 0, 0, UIWidget.Pivot.Right);
			uILabel.color = Color.red;
		}
		UIButton uIButton5 = Util.CreateJellyImageButton("ButtonOK", uISprite.gameObject, image2);
		Util.SetImageButtonInfo(uIButton5, buttonImage, buttonImage, buttonImage, num + 5, new Vector3(179f, -45f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton5, this, "OnReceiveGiftPushed_Direct", UIButtonMessage.Trigger.OnClick);
		Collider component5 = uIButton5.GetComponent<Collider>();
		if ((bool)component5)
		{
			component5.enabled = IsOperating;
		}
		UIDragScrollView uIDragScrollView5 = uIButton5.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView5.scrollView = mGiftItemList.mListScrollView;
		bool flag5 = false;
		switch (giftListItem.gift.GiftItemCategory)
		{
		case Def.ITEM_CATEGORY.ADV_FIGURE:
			flag5 = true;
			break;
		case Def.ITEM_CATEGORY.MESSAGE_CARD:
			if (mGame.GetMessageCardPresentList(giftListItem.gift.Data.MessageCardID).Count > 0)
			{
				flag5 = true;
			}
			break;
		}
		if (flag5)
		{
			buttonImage = "LC_button_detail";
			UIButton uIButton6 = Util.CreateJellyImageButton("ButtonDetail", uISprite.gameObject, image2);
			Util.SetImageButtonInfo(uIButton6, buttonImage, buttonImage, buttonImage, num + 5, new Vector3(40f, -45f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton6, this, "OnFigureDetailPushed", UIButtonMessage.Trigger.OnClick);
			component5 = uIButton6.GetComponent<Collider>();
			if ((bool)component5)
			{
				component5.enabled = IsOperating;
			}
		}
	}

	private void OnCreateItem_Friend(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int num = mBaseDepth + 5;
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		string empty = string.Empty;
		string empty2 = string.Empty;
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		GiftListItem giftListItem = item as GiftListItem;
		float cellWidth = mGiftItemList.mList.cellWidth;
		float cellHeight = mGiftItemList.mList.cellHeight;
		UISprite uISprite3 = Util.CreateSprite("Back", itemGO, image2);
		Util.SetSpriteInfo(uISprite3, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite3.width = (int)cellWidth;
		uISprite3.height = (int)cellHeight;
		Collider component = uISprite3.transform.parent.GetComponent<Collider>();
		if ((bool)component)
		{
			component.enabled = IsOperating;
		}
		if (giftListItem == null)
		{
			return;
		}
		uISprite = Util.CreateSprite("FriendBoard", itemGO, image2);
		Util.SetSpriteInfo(uISprite, "instruction_panel5", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(472, 159);
		uISprite.transform.localScale = new Vector3(0.925f, 0.925f);
		MyFriend friend = giftListItem.friend;
		if (friend == null)
		{
			BIJLog.E("friend is null!!");
			return;
		}
		empty2 = friend.Data.Name;
		UILabel uILabel = Util.CreateLabel("FromName", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, empty2, num + 1, new Vector3(0f, 60f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(468, 26);
		int num2 = 0;
		bool flag = false;
		if (GameMain.IsFacebookIconEnable() && friend.Data.IconID >= 10000)
		{
			if (friend.Icon != null)
			{
				UITexture uITexture = Util.CreateGameObject("Icon", uISprite.gameObject).AddComponent<UITexture>();
				uITexture.depth = num + 2;
				uITexture.transform.localPosition = new Vector3(-194f, -18f, 0f);
				uITexture.mainTexture = friend.Icon;
				uITexture.SetDimensions(65, 65);
			}
			else
			{
				UISprite uISprite4 = Util.CreateSprite("FriendIcon", uISprite.gameObject, image);
				Util.SetSpriteInfo(uISprite4, "icon_chara_facebook", num + 1, new Vector3(-194f, -18f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(65, 65);
			}
			num2 = friend.Data.IconID - 10000;
			if (num2 >= mGame.mCompanionData.Count)
			{
				num2 = 0;
			}
			empty = mGame.mCompanionData[num2].iconName;
			UISprite uISprite5 = Util.CreateSprite("RightTopIcon", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite5, empty, num + 3, new Vector3(-162f, 16f, 0f), Vector3.one, false);
			uISprite5.SetDimensions(50, 50);
			flag = true;
		}
		else
		{
			num2 = friend.Data.IconID;
			if (num2 >= 10000)
			{
				num2 -= 10000;
			}
		}
		if (!flag)
		{
			if (num2 >= mGame.mCompanionData.Count)
			{
				num2 = 0;
			}
			empty = mGame.mCompanionData[num2].iconName;
			uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, empty, num + 2, new Vector3(-186f, -10f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(84, 84);
			empty = ((friend.Data.IconLevel <= 0) ? "lv_1" : ("lv_" + friend.Data.IconLevel));
			UISprite sprite = Util.CreateSprite("Icon", uISprite.gameObject, image);
			Util.SetSpriteInfo(sprite, empty, num + 3, new Vector3(-186f, -47f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(friend.Data.Fbid))
			{
				string imageName = "icon_sns00_mini";
				UISprite sprite2 = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
				Util.SetSpriteInfo(sprite2, imageName, num + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
			}
		}
		string text = " " + Localization.Get("Purchase_mul") + " " + 1;
		uILabel = Util.CreateLabel("FriendName", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, num + 3, new Vector3(-81f, 11f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(70, 26);
		UISprite uISprite6;
		if (mGame.IsPlayAdvMode() && mGame.mGameProfile.AdvSettings.SendMedal == 1)
		{
			text = " " + Localization.Get("Purchase_mul") + " " + 1;
			uILabel = Util.CreateLabel("SendDesc2", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text, num + 3, new Vector3(54f, 11f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(70, 26);
			empty = "adv_item_coin";
			uISprite6 = Util.CreateSprite("RecieveIcon2", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite6, empty, num + 3, new Vector3(27f, 13f, 0f), Vector3.one, false);
			uISprite6.SetDimensions(54, 54);
		}
		empty = "icon_heart";
		string text2 = "LC_button_send";
		string empty3 = string.Empty;
		string text3 = string.Format(Localization.Get("MyStage_Desk"));
		uISprite6 = Util.CreateSprite("Linetop", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite6, "line00", num + 3, new Vector3(42f, -14f, 0f), Vector3.one, false);
		uISprite6.type = UIBasicSprite.Type.Sliced;
		uISprite6.SetDimensions(346, 6);
		uISprite6 = Util.CreateSprite("RecieveIcon", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite6, empty, num + 3, new Vector3(-108f, 13f, 0f), Vector3.one, false);
		uISprite6.SetDimensions(54, 54);
		if (mGame.mPlayer.GetNewFriendNotifyStatus(friend.Data.UUID) == Player.NOTIFICATION_STATUS.NOTIFY)
		{
			mGame.mPlayer.NewFriendNotified(friend.Data.UUID);
			uISprite6 = Util.CreateSprite("NewIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite6, "icon_new", num + 3, new Vector3(175f, 4f, 0f), Vector3.one, false);
		}
		int num3 = friend.Data.Series;
		List<PlayStageParam> playStage = friend.Data.PlayStage;
		int stageNumber = 1;
		if (playStage != null && playStage.Count != 0)
		{
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == num3)
				{
					stageNumber = playStage[i].Level / 100;
					break;
				}
			}
		}
		else
		{
			num3 = 0;
			int a_main;
			int a_sub;
			Def.SplitStageNo(friend.Data.Level, out a_main, out a_sub);
			stageNumber = a_main;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(uISprite.gameObject, num, num3, stageNumber, -45f, -30f);
		UIButton uIButton = Util.CreateJellyImageButton("ButtonSend", uISprite.gameObject, image2);
		Util.SetImageButtonInfo(uIButton, text2, text2, text2, num + 5, new Vector3(161f, -45f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnSendGiftPushed", UIButtonMessage.Trigger.OnClick);
		Collider component2 = uIButton.GetComponent<Collider>();
		if ((bool)component2)
		{
			component2.enabled = IsOperating;
		}
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mGiftItemList.mListScrollView;
	}

	public void OnReceiveTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && Mode != 0 && ((mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_ALL) && ListMode == GameMain.GiftListMode.ALL) || (mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_HEART) && ListMode == GameMain.GiftListMode.HEART) || (mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_MEDAL) && ListMode == GameMain.GiftListMode.MEDAL) || (mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_OTHER) && ListMode == GameMain.GiftListMode.OTHER)))
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.GiftUIMode mode = GameMain.GiftUIMode.RECEIVE;
			ActivateTab(mode);
			ChangeMode(mode, ChangeModeConfig.Create());
		}
	}

	public void OnSendTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && Mode != GameMain.GiftUIMode.SEND && mTempItemList.ContainsKey(LIST_TYPE.SEND))
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.GiftUIMode mode = GameMain.GiftUIMode.SEND;
			ActivateTab(mode);
			ChangeMode(mode, ChangeModeConfig.Create());
		}
	}

	public void OnRescueTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && Mode != GameMain.GiftUIMode.RESCUE && mTempItemList.ContainsKey(LIST_TYPE.RESCUE))
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.GiftUIMode mode = GameMain.GiftUIMode.RESCUE;
			ActivateTab(mode);
			ChangeMode(mode, ChangeModeConfig.Create());
		}
	}

	public void OnAllTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && ListMode != 0 && mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_ALL))
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.GiftListMode mode = GameMain.GiftListMode.ALL;
			ActivateTopTab(mode);
			ChangeListMode(mode);
		}
	}

	public void OnHeartTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && ListMode != GameMain.GiftListMode.HEART && mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_HEART))
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.GiftListMode mode = GameMain.GiftListMode.HEART;
			ActivateTopTab(mode);
			ChangeListMode(mode);
		}
	}

	public void OnMedalTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && ListMode != GameMain.GiftListMode.MEDAL && mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_MEDAL))
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.GiftListMode mode = GameMain.GiftListMode.MEDAL;
			ActivateTopTab(mode);
			ChangeListMode(mode);
		}
	}

	public void OnOtherTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && ListMode != GameMain.GiftListMode.OTHER && mTempItemList.ContainsKey(LIST_TYPE.RECEIVE_OTHER))
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.GiftListMode mode = GameMain.GiftListMode.OTHER;
			ActivateTopTab(mode);
			ChangeListMode(mode);
		}
	}

	private void OnConnectErrorRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		mState.Change((STATE)state);
	}

	private void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public void OnGetStampDialogClosed(GetStampDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetStampDialog != null)
		{
			UnityEngine.Object.Destroy(mGetStampDialog.gameObject);
			mGetStampDialog = null;
		}
		int num = 0;
		List<PlayStageParam> playStage = mGame.mPlayer.PlayStage;
		for (int i = 0; i < playStage.Count; i++)
		{
			if (playStage[i].Series == 0)
			{
				num = playStage[i].Level;
				break;
			}
		}
		if (a_selectItem == GetStampDialog.SELECT_ITEM.GROWUP && num > 800)
		{
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", base.gameObject).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(null, true, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mGrowupDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component3 = mGrowupDialog.GetComponent<BoxCollider>();
				if ((bool)component3)
				{
					component3.size = new Vector3(1384f, 1384f, 1f);
				}
			});
		}
		else if (a_selectItem == GetStampDialog.SELECT_ITEM.GROWUP && mGame.mPlayer.GrowUpPiece > 0)
		{
			mGetStampDialog = Util.CreateGameObject("GetStampDialog", base.gameObject).AddComponent<GetStampDialog>();
			mGetStampDialog.Init(null, 0, false, true);
			mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			mGetStampDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component2 = mGetStampDialog.GetComponent<BoxCollider>();
				if ((bool)component2)
				{
					component2.size = new Vector3(1384f, 1384f, 1f);
				}
			});
		}
		else if (mGame.mPlayer.HeartUpPiece >= 15)
		{
			mGame.mPlayer.SubHeartPiece(15);
			GiftItem giftItem = new GiftItem();
			giftItem.Data.Message = Localization.Get("Gift_RatingReward");
			giftItem.Data.ItemCategory = 1;
			giftItem.Data.ItemKind = 1;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = 1;
			mGame.mPlayer.AddGift(giftItem);
			GameMain.GiftUIMode mode = GameMain.GiftUIMode.RECEIVE;
			ChangeMode(mode, new ChangeModeConfig
			{
				Mode = ChangeModeConfig.CreateMode.Force
			});
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", base.gameObject).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init("icon_heart", Localization.Get("GetAccessory_Item_Title"), string.Format(Localization.Get("GetAccessory_HEART_Desc"), 1));
			mGetAccessoryDialog.SetClosedCallback(OnGetHeartPieaceDialogClosed);
			mGetAccessoryDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component = mGetAccessoryDialog.GetComponent<BoxCollider>();
				if ((bool)component)
				{
					component.size = new Vector3(1384f, 1384f, 1f);
				}
			});
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	public void OnGetHeartPieaceDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetAccessoryDialog != null)
		{
			UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
			mGetAccessoryDialog = null;
		}
		if (mGame.mPlayer.HeartUpPiece > 0)
		{
			mGetStampDialog = Util.CreateGameObject("GetStampDialog", base.gameObject).AddComponent<GetStampDialog>();
			mGetStampDialog.Init(null, 0, false, true, false);
			mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			mGetStampDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component = mGetStampDialog.GetComponent<BoxCollider>();
				if ((bool)component)
				{
					component.size = new Vector3(1384f, 1384f, 1f);
				}
			});
			mState.Change(STATE.WAIT);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	public void OnGrowupSelectDialogClosed(GrowupPartnerSelectDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupDialog.SelectedPartnerNo;
		bool isCloseButton = mGrowupDialog.IsCloseButton;
		mGrowupDialog.Init(false);
		AccessoryData data = mGrowupDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupDialog.gameObject);
		mGrowupDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerSelectDialog.SELECT_ITEM.PLAY:
			mGrowupPartnerConfirmtDialog = Util.CreateGameObject("GrowupPartnerConfirmDialog", base.gameObject).AddComponent<GrowupPartnerConfirmDialog>();
			mGrowupPartnerConfirmtDialog.Init(data, selectedPartnerNo, isCloseButton, accessoryType);
			mGrowupPartnerConfirmtDialog.SetClosedCallback(OnGrowupPartnerConfirmDialogClosed);
			mGrowupPartnerConfirmtDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component = mGrowupPartnerConfirmtDialog.GetComponent<BoxCollider>();
				if ((bool)component)
				{
					component.size = new Vector3(1384f, 1384f, 1f);
				}
			});
			break;
		case GrowupPartnerSelectDialog.SELECT_ITEM.CANCEL:
			if (isCloseButton && mGame.mPlayer.GrowUpPiece > 0)
			{
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", base.gameObject).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mGetStampDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
				{
					BoxCollider component2 = mGetStampDialog.GetComponent<BoxCollider>();
					if ((bool)component2)
					{
						component2.size = new Vector3(1384f, 1384f, 1f);
					}
				});
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
			break;
		default:
			mState.Change(STATE.MAIN);
			break;
		}
	}

	public void OnGrowupPartnerConfirmDialogClosed(GrowupPartnerConfirmDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupPartnerConfirmtDialog.SelectedPartnerNo;
		bool isReplayStamp = mGrowupPartnerConfirmtDialog.IsReplayStamp;
		mGrowupPartnerConfirmtDialog.Init(false);
		AccessoryData data = mGrowupPartnerConfirmtDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupPartnerConfirmtDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupPartnerConfirmtDialog.gameObject);
		mGrowupPartnerConfirmtDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerConfirmDialog.SELECT_ITEM.PLAY:
			mGrowupResultDialog = Util.CreateGameObject("GrowupResultDialog", base.gameObject).AddComponent<GrowupResultDialog>();
			mGrowupResultDialog.Init(data, selectedPartnerNo, true, accessoryType, isReplayStamp);
			mGrowupResultDialog.SetClosedCallback(OnGrowupResultDialogClosed);
			mGrowupResultDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component = mGrowupResultDialog.GetComponent<BoxCollider>();
				if ((bool)component)
				{
					component.size = new Vector3(1384f, 1384f, 1f);
				}
			});
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.CANCEL:
			if (mGame.mPlayer.GrowUp >= 1)
			{
				mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", base.gameObject).AddComponent<GrowupPartnerSelectDialog>();
				mGrowupDialog.Init(null, isReplayStamp, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
				mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
				mGrowupDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
				{
					BoxCollider component3 = mGrowupDialog.GetComponent<BoxCollider>();
					if ((bool)component3)
					{
						component3.size = new Vector3(1384f, 1384f, 1f);
					}
				});
			}
			else
			{
				if (!isReplayStamp || mGame.mPlayer.GrowUpPiece <= 0)
				{
					break;
				}
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", base.gameObject).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mGetStampDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
				{
					BoxCollider component2 = mGetStampDialog.GetComponent<BoxCollider>();
					if ((bool)component2)
					{
						component2.size = new Vector3(1384f, 1384f, 1f);
					}
				});
			}
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.BACK:
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", base.gameObject).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(data, isReplayStamp, accessoryType);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mGrowupDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component4 = mGrowupDialog.GetComponent<BoxCollider>();
				if ((bool)component4)
				{
					component4.size = new Vector3(1384f, 1384f, 1f);
				}
			});
			break;
		}
	}

	public void OnGrowupResultDialogClosed(GrowupResultDialog.SELECT_ITEM a_selectItem)
	{
		AccessoryData data = mGrowupResultDialog.Data;
		if (mGame.mPlayer.GrowUp >= 1)
		{
			bool a_close = mGrowupResultDialog != null && mGrowupResultDialog.IsReplayStamp;
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", base.gameObject).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(null, a_close, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mGrowupDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
			{
				BoxCollider component2 = mGrowupDialog.GetComponent<BoxCollider>();
				if ((bool)component2)
				{
					component2.size = new Vector3(1384f, 1384f, 1f);
				}
			});
		}
		else if (mGrowupResultDialog != null)
		{
			if (mGrowupResultDialog.IsReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				mGrowupResultDialog.Init(false);
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", base.gameObject).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
				mGetStampDialog.SetCallback(CALLBACK_STATE.OnOpenFinished, delegate
				{
					BoxCollider component = mGetStampDialog.GetComponent<BoxCollider>();
					if ((bool)component)
					{
						component.size = new Vector3(1384f, 1384f, 1f);
					}
				});
			}
			else
			{
				mState.Change(STATE.MAIN);
			}
		}
		if (mGrowupResultDialog != null)
		{
			UnityEngine.Object.Destroy(mGrowupResultDialog.gameObject);
		}
		mGrowupResultDialog = null;
	}

	public void OnWallPeperCompClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetAccessoryDialog != null)
		{
			UnityEngine.Object.Destroy(mGetAccessoryDialog.gameObject);
			mGetAccessoryDialog = null;
		}
		EnableList(true);
		mState.Change(STATE.MAIN);
	}

	private void GetCharacterDialogClosed()
	{
		OnReceiveGiftPushed(AdvNetwork[0].GetReceiveItemGo(0), new ReceiveGiftConfig
		{
			IsNotResetList = true,
			IsNotCheckState = true
		});
	}

	private bool CheckDisplayDialog(int a_stage)
	{
		bool result = false;
		int a_main = 0;
		int a_sub = 0;
		short a_course = 0;
		int a_eventID = 0;
		Def.SplitServerStageNo(a_stage, out a_eventID, out a_course, out a_main, out a_sub);
		SMEventPageData eventPageData = GameMain.GetEventPageData(a_eventID);
		if (eventPageData == null)
		{
			return false;
		}
		Def.EVENT_TYPE eventType = eventPageData.EventSetting.EventType;
		if (eventType == Def.EVENT_TYPE.SM_LABYRINTH)
		{
			GlobalLabyrinthEventData labyrinthData = GlobalVariables.Instance.GetLabyrinthData(a_eventID);
			if (labyrinthData != null && labyrinthData.RewardAccessoryDataList != null && labyrinthData.RewardAccessoryDataList.Count > 0)
			{
				result = false;
				foreach (AccessoryData rewardAccessoryData in labyrinthData.RewardAccessoryDataList)
				{
					if (rewardAccessoryData.AccessoryType != AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY)
					{
						result = true;
					}
				}
				labyrinthData.RewardAccessoryDataList.RemoveAll((AccessoryData a) => a != null);
				labyrinthData.RewardAccessoryDataList = null;
				GlobalVariables.Instance.SetLabyrinthData(a_eventID, labyrinthData);
			}
		}
		return result;
	}

	private bool IsMessageCardAvalable(GiftItem gift)
	{
		int messageCardID = gift.Data.MessageCardID;
		if (messageCardID == -1)
		{
			return false;
		}
		foreach (KeyValuePair<string, GiftItem> gift2 in mGame.mPlayer.Gifts)
		{
			if (gift2.Value.Data.ItemCategory == 34 && messageCardID == gift2.Value.Data.MessageCardID)
			{
				return true;
			}
		}
		return false;
	}

	private bool MessageCardDisplayCheck(GiftItem gift)
	{
		int messageCardID = gift.Data.MessageCardID;
		if (gift.Data.ItemCategory == 34)
		{
			if (messageCardID == -1)
			{
				return false;
			}
			int messageCardPresentCount = gift.Data.MessageCardPresentCount;
			int num = 0;
			foreach (KeyValuePair<string, GiftItem> gift2 in mGame.mPlayer.Gifts)
			{
				if (gift2.Value.Data.ItemCategory != 34 && messageCardID == gift2.Value.Data.MessageCardID)
				{
					num++;
				}
			}
			if (num != messageCardPresentCount)
			{
				return false;
			}
		}
		else
		{
			if (messageCardID == -1)
			{
				return true;
			}
			foreach (KeyValuePair<string, GiftItem> gift3 in mGame.mPlayer.Gifts)
			{
				if (gift3.Value.Data.ItemCategory == 34 && messageCardID == gift3.Value.Data.MessageCardID)
				{
					return false;
				}
			}
		}
		return true;
	}

	private void StartStoryDemo(int demoIndex)
	{
		if (demoIndex >= 0 && mGame.mStoryDemoData.Count > demoIndex)
		{
			mGame.SetMusicVolume(0f, 0);
			mStoryDemo = Util.CreateGameObject("StoryDemo", base.gameObject).AddComponent<StoryDemoManager>();
			mStoryDemo.ChangeStoryDemo(demoIndex, OnStoryDemoFinished);
			mGame.mDialogManager.SetGrayScreenDepth = GetSortingOder() + 4;
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			mDemoBlockRaycaster = Util.CreatePanel("BlockRaycaster", mBody.gameObject, 70, 70);
			UISprite uISprite = Util.CreateSprite("Sprite", mDemoBlockRaycaster.gameObject, image);
			Util.SetSpriteInfo(uISprite, "null", 70, Vector3.zero, Vector3.one, false);
			uISprite.SetDimensions(1384, 1384);
			uISprite.gameObject.AddComponent<BoxCollider>();
			uISprite.ResizeCollider();
			mState.Reset(STATE.SHOW_DETAIL_WAIT, true);
		}
	}

	private void OnStoryDemoFinished(int storyDemoIndex)
	{
		mGame.SetMusicVolume(1f, 0);
		mStoryDemo = null;
		mGame.mDialogManager.SetGrayScreenDepth = GetSortingOder() - 1;
		mGame.mPlayer.StoryComplete(storyDemoIndex);
		mGame.Save();
		if (mDemoBlockRaycaster != null)
		{
			UnityEngine.Object.Destroy(mDemoBlockRaycaster.gameObject);
		}
		ChangeMode(Mode, new ChangeModeConfig
		{
			Mode = ChangeModeConfig.CreateMode.Force
		});
		mState.Reset(STATE.MAIN, true);
	}
}
