public class AdvGrowUpFigureProfile_Request : RequestBase
{
	[MiniJSONAlias("lvlup_quantity")]
	public int LvQuantity { get; set; }

	[MiniJSONAlias("skillup_quantity")]
	public int SkillLvQuantity { get; set; }

	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }

	[MiniJSONAlias("is_retry")]
	public int IsRetry { get; set; }
}
