using System;
using System.Collections.Generic;

public class AdvGashaPlayProfile_Response : ICloneable
{
	[MiniJSONAlias("prizes")]
	public List<AdvGashaResult> GashaResultList { get; set; }

	[MiniJSONAlias("figure_exchange")]
	public List<AdvFigureExchange> FigureExchangeList { get; set; }

	[MiniJSONAlias("gasha_groups")]
	public List<AdvGashaInfo> GashaInfoList { get; set; }

	[MiniJSONAlias("figures")]
	public List<AdvFigureData> FigureDataList { get; set; }

	[MiniJSONAlias("items")]
	public List<AdvItemData> ItemDataList { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGashaPlayProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGashaPlayProfile_Response;
	}
}
