public class GetSaveDataParam : ParameterObject<GetSaveDataParam>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }
}
