public class CampaignChargeResponse : ParameterObject<CampaignChargeResponse>
{
	public long server_time { get; set; }

	public int code { get; set; }
}
