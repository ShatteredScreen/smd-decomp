using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UserDataInfo
{
	public enum DATA_STATE
	{
		NONE = 0,
		LOAD_ERROR = 1,
		LOADED = 2
	}

	private static UserDataInfo mInstance;

	private string mAuthKey;

	private string mAuthKeyConf;

	public short mVersion;

	public short mAppVersion;

	public int mTransferStep;

	public bool mShowInheriting;

	public bool mShowPassword;

	public int mResetReason;

	public Dictionary<Def.SERIES, int> ShowCharacterInfoDialogDate = new Dictionary<Def.SERIES, int>();

	public DATA_STATE mAuthKeyState;

	public DATA_STATE mDataState;

	public DATA_STATE mAdvDataState;

	public DATA_STATE mBackupDataState;

	public DATA_STATE mBackupAdvDataState;

	public static UserDataInfo Instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = new UserDataInfo();
			}
			return mInstance;
		}
	}

	public string AuthKey
	{
		get
		{
			return mAuthKey;
		}
	}

	public short mAndroidOVersion
	{
		get
		{
			return 1180;
		}
	}

	public void Load()
	{
		LoadAuthKey();
		LoadInfo();
	}

	public void Save(bool isAuthKeySave = false)
	{
		if (isAuthKeySave)
		{
			SaveAuthKey();
		}
		SaveInfo();
	}

	public void ReCreateAuthKey(bool isSave = true)
	{
		CreateAuthKey();
		if (isSave)
		{
			SaveAuthKey();
		}
	}

	public bool IsNeedInheriting()
	{
		bool result = false;
		switch (mAuthKeyState)
		{
		case DATA_STATE.NONE:
		case DATA_STATE.LOADED:
			if (mDataState == DATA_STATE.NONE || mDataState == DATA_STATE.LOAD_ERROR || (false ? true : false))
			{
				result = true;
			}
			else if (mAuthKeyState == DATA_STATE.NONE && mDataState == DATA_STATE.LOADED && SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.AppVersion >= mAndroidOVersion)
			{
				result = true;
			}
			break;
		case DATA_STATE.LOAD_ERROR:
			result = true;
			break;
		}
		return result;
	}

	public bool IsInstalled()
	{
		bool result = false;
		if (mAuthKeyState == DATA_STATE.NONE && mDataState == DATA_STATE.NONE && mAdvDataState == DATA_STATE.NONE)
		{
			result = true;
		}
		return result;
	}

	public bool IsDataDameged()
	{
		bool result = false;
		if (!IsInstalled() && IsNeedInheriting())
		{
			result = true;
		}
		return result;
	}

	private void CreateAuthKey()
	{
		mAuthKey = Guid.NewGuid().ToString("D");
		mAuthKeyConf = mAuthKey;
	}

	private void CreateInfo()
	{
		mVersion = 1290;
		mAppVersion = 1290;
		mTransferStep = 0;
		if (SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.AppVersion < mAndroidOVersion)
		{
			GameStateTransfer.STEP @int = (GameStateTransfer.STEP)PlayerPrefs.GetInt("TransferStep", 0);
			mTransferStep = (int)@int;
		}
		mShowInheriting = false;
		mShowPassword = false;
		if (SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.AppVersion < mAndroidOVersion)
		{
			mShowPassword = true;
		}
		mResetReason = -1;
	}

	private void LoadAuthKey()
	{
		string aUTHKEY_FILE = Constants.AUTHKEY_FILE;
		string text = Path.Combine(BIJDataBinaryReader.GetBasePath(), aUTHKEY_FILE);
		if (File.Exists(text))
		{
			mAuthKeyState = DATA_STATE.LOAD_ERROR;
			try
			{
				using (BIJBinaryReader data = new BIJEncryptDataReader(text))
				{
					DeserializeAuthKey(data);
					if (!string.IsNullOrEmpty(mAuthKey) && mAuthKey.Length == 36 && mAuthKey == mAuthKeyConf)
					{
						mAuthKeyState = DATA_STATE.LOADED;
					}
				}
			}
			catch (Exception)
			{
			}
		}
		else
		{
			mAuthKeyState = DATA_STATE.NONE;
		}
		if (mAuthKeyState == DATA_STATE.NONE)
		{
			CreateAuthKey();
			SaveAuthKey();
		}
	}

	private void LoadInfo()
	{
		string uDINFO_FILE = Constants.UDINFO_FILE;
		string text = Path.Combine(BIJDataBinaryReader.GetBasePath(), uDINFO_FILE);
		bool flag = false;
		if (File.Exists(text))
		{
			try
			{
				using (BIJBinaryReader data = new BIJEncryptDataReader(text))
				{
					DeserializeInfo(data);
					flag = true;
				}
			}
			catch (Exception)
			{
			}
		}
		if (!flag)
		{
			CreateInfo();
		}
	}

	private void SaveAuthKey()
	{
		try
		{
			string aUTHKEY_FILE = Constants.AUTHKEY_FILE;
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(aUTHKEY_FILE))
			{
				SerializeAuthKey(bIJEncryptDataWriter);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	private void SaveInfo()
	{
		try
		{
			string uDINFO_FILE = Constants.UDINFO_FILE;
			using (BIJEncryptDataWriter bIJEncryptDataWriter = new BIJEncryptDataWriter(uDINFO_FILE))
			{
				SerializeInfo(bIJEncryptDataWriter);
				bIJEncryptDataWriter.Close();
			}
		}
		catch (Exception)
		{
		}
	}

	private void SerializeAuthKey(BIJBinaryWriter data)
	{
		data.WriteUTF(mAuthKey);
		data.WriteUTF(mAuthKeyConf);
	}

	private void SerializeInfo(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		data.WriteShort(1290);
		data.WriteInt(mTransferStep);
		data.WriteBool(mShowInheriting);
		data.WriteBool(mShowPassword);
		data.WriteInt(mResetReason);
		data.WriteShort((short)ShowCharacterInfoDialogDate.Count);
		List<Def.SERIES> list = new List<Def.SERIES>(ShowCharacterInfoDialogDate.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			Def.SERIES sERIES = list[i];
			data.WriteShort((short)sERIES);
			data.WriteInt(ShowCharacterInfoDialogDate[sERIES]);
		}
	}

	private void DeserializeAuthKey(BIJBinaryReader data)
	{
		mAuthKey = data.ReadUTF();
		mAuthKeyConf = data.ReadUTF();
	}

	private void DeserializeInfo(BIJBinaryReader data)
	{
		mVersion = data.ReadShort();
		mAppVersion = data.ReadShort();
		mTransferStep = data.ReadInt();
		mShowInheriting = data.ReadBool();
		mShowPassword = data.ReadBool();
		mResetReason = data.ReadInt();
		if (mVersion < 1230)
		{
			return;
		}
		short num = data.ReadShort();
		for (int i = 0; i < num; i++)
		{
			Def.SERIES key = (Def.SERIES)data.ReadShort();
			int value = data.ReadInt();
			if (!ShowCharacterInfoDialogDate.ContainsKey(key))
			{
				ShowCharacterInfoDialogDate.Add(key, value);
			}
		}
	}
}
