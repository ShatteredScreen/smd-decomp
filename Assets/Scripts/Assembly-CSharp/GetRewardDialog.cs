using System.Collections.Generic;
using UnityEngine;

public class GetRewardDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	private class RewardSpriteData
	{
		public string mImgName;

		public int mNum;

		public RewardSpriteData()
		{
			mImgName = string.Empty;
			mNum = 0;
		}
	}

	public delegate void OnDialogClosed();

	public const int MAX_DISPLAY = 4;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private BIJImage atlas;

	private Vector2 mLogScreen = Util.LogScreenSize();

	private List<AccessoryData> mAccessoryDataList;

	private bool mNoGetFlg;

	private bool mFreeTicketFlg;

	private bool mIsBundle;

	private List<RewardSpriteData> mRewardSpriteDataList = new List<RewardSpriteData>();

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		mLogScreen = Util.LogScreenSize();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public void Init(List<AccessoryData> dataList, bool noGet = false, bool freeTicket = false)
	{
		mAccessoryDataList = dataList;
		mNoGetFlg = noGet;
		mFreeTicketFlg = freeTicket;
	}

	public void InitUseBundle(List<AccessoryData> a_list)
	{
		mAccessoryDataList = new List<AccessoryData>(a_list);
		mIsBundle = true;
		mNoGetFlg = false;
		mFreeTicketFlg = false;
	}

	private void MakeSpriteDataList(out string a_title, out string a_desc)
	{
		a_title = string.Empty;
		a_desc = string.Empty;
		if (mAccessoryDataList == null)
		{
			return;
		}
		for (int i = 0; i < mAccessoryDataList.Count; i++)
		{
			RewardSpriteData data = new RewardSpriteData();
			switch (mAccessoryDataList[i].AccessoryType)
			{
			case AccessoryData.ACCESSORY_TYPE.BOOSTER:
			{
				Def.BOOSTER_KIND a_kind;
				mAccessoryDataList[i].GetBooster(out a_kind, out data.mNum);
				data.mImgName = mGame.mBoosterData[(int)a_kind].iconBuy;
				break;
			}
			case AccessoryData.ACCESSORY_TYPE.ADV_ITEM:
				if (mGame.mAdvRewardListDict.ContainsKey(int.Parse(mAccessoryDataList[i].GotID)))
				{
					AdvRewardListData advRewardListData = mGame.mAdvRewardListDict[int.Parse(mAccessoryDataList[i].GotID)];
					data.mImgName = advRewardListData.IconKey;
					data.mNum = (int)advRewardListData.Value;
				}
				break;
			case AccessoryData.ACCESSORY_TYPE.GEM:
				data.mImgName = "icon_84jem";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.GROWUP:
				data.mImgName = "icon_level";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
				data.mImgName = "icon_dailyEvent_stamp02";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.HEART:
				data.mImgName = "icon_heart";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
				data.mImgName = "icon_dailyEvent_stamp01";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.BINGO_KEY:
				if (mAccessoryDataList.Count == 1)
				{
					data.mImgName = "icon_eventroadblock_key01";
					data.mNum = int.Parse(mAccessoryDataList[i].GotID);
					a_title = Localization.Get("GetSheetKey_Title");
					a_desc = Localization.Get("GetSheetKey_Desc");
				}
				break;
			case AccessoryData.ACCESSORY_TYPE.BINGO_TICKET:
				data.mImgName = "icon_event_ticket00";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				data.mImgName = SMDImageUtil.GetHUDIconKey(1, 20, data.mNum);
				break;
			case AccessoryData.ACCESSORY_TYPE.TROPHY:
				data.mImgName = "icon_ranking_trophy";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				break;
			case AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY:
				data.mImgName = "icon_eventroadblock_key02";
				data.mNum = int.Parse(mAccessoryDataList[i].GotID);
				a_title = Localization.Get("GetLabyrinthKey_Title");
				a_desc = Localization.Get("GetLabyrinthKey_Desc");
				break;
			}
			if (string.IsNullOrEmpty(data.mImgName))
			{
				continue;
			}
			bool flag = true;
			if (mIsBundle)
			{
				int num = mRewardSpriteDataList.FindIndex((RewardSpriteData a) => a.mImgName.CompareTo(data.mImgName) == 0);
				if (num > -1)
				{
					flag = false;
					mRewardSpriteDataList[num].mNum += data.mNum;
				}
			}
			if (flag)
			{
				mRewardSpriteDataList.Add(data);
			}
		}
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("GetAccessory_Item_Title");
		string text = Localization.Get("GetAccessory_Item_DescB");
		string a_title;
		string a_desc;
		MakeSpriteDataList(out a_title, out a_desc);
		if (!string.IsNullOrEmpty(a_title))
		{
			titleDescKey = a_title;
		}
		if (!string.IsNullOrEmpty(a_desc))
		{
			text = a_desc;
		}
		if (mNoGetFlg)
		{
			titleDescKey = Localization.Get("GotError_Title");
			text = Localization.Get("GotError_Desc");
		}
		else if (mFreeTicketFlg)
		{
			titleDescKey = Localization.Get("Otameshi_Ticket_Title");
			text = string.Format(Localization.Get("Otameshi_Ticket_Desc"), mRewardSpriteDataList[0].mNum);
		}
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, atlas);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 67f, 0f), Vector3.one, false);
		uISprite.SetDimensions(510, 104);
		uISprite.type = UIBasicSprite.Type.Sliced;
		if (mRewardSpriteDataList.Count > 0)
		{
			float num = 0f;
			int num2 = ((mRewardSpriteDataList.Count > 4) ? 4 : mRewardSpriteDataList.Count);
			if (num2 > 1)
			{
				num = -50 * (num2 - 1);
			}
			for (int i = 0; i < num2; i++)
			{
				UISprite uISprite2 = Util.CreateSprite("item" + i, uISprite.gameObject, atlas);
				Util.SetSpriteInfo(uISprite2, mRewardSpriteDataList[i].mImgName, mBaseDepth + 3, new Vector3(num, 0f, 0f), Vector3.one, false);
				uISprite2.SetDimensions(80, 80);
				num += 100f;
				if (mRewardSpriteDataList[i].mNum > 0)
				{
					UILabel uILabel = Util.CreateLabel("RewardNum", uISprite2.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, string.Format(Localization.Get("Gift_Number"), mRewardSpriteDataList[i].mNum), mBaseDepth + 4, new Vector3(40f, -30f, 0f), 18, 0, 0, UIWidget.Pivot.Right);
					uILabel.color = new Color(0.4f, 0.2627451f, 0.14901961f);
					uILabel.effectStyle = UILabel.Effect.Outline8;
					uILabel.effectColor = Color.white;
					uILabel.effectDistance = new Vector2(2f, 2f);
				}
			}
		}
		UILabel uILabel2 = Util.CreateLabel("Text", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel2, text, mBaseDepth + 2, new Vector3(0f, -28f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelColor(uILabel2, Def.DEFAULT_MESSAGE_COLOR);
		uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel2.SetDimensions(500, 80);
		uILabel2.spacingY = 8;
		UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -107f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		mState.Reset(STATE.WAIT, true);
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnClosePushed();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
