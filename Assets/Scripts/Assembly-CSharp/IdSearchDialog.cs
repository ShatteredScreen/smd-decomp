using System;
using System.Text;
using UnityEngine;

public class IdSearchDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		IDSEARCH = 2,
		IDSEARCH_WAIT = 3,
		IDRESULT = 4,
		WAIT = 5
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	public IdResultDialog.ID_RESULT mItem = IdResultDialog.ID_RESULT.ERROR;

	private OnDialogClosed mCallback;

	private UIInput mInputs;

	private IdResultDialog mIdResultDialog;

	private string mIDdata;

	private bool mSearchIDUserSucceed;

	private bool mSearchIDUserCheckDone;

	private SearchUserData mSearchData;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		Server.GetIDUserEvent -= Server_OnSearchIDUser;
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			Server.GetIDUserEvent += Server_OnSearchIDUser;
			mState.Change(STATE.MAIN);
			break;
		case STATE.IDSEARCH:
			Network_SearchIDUser();
			mState.Change(STATE.IDSEARCH_WAIT);
			break;
		case STATE.IDSEARCH_WAIT:
			if (mSearchIDUserCheckDone)
			{
				mState.Change(STATE.IDRESULT);
			}
			break;
		case STATE.IDRESULT:
			mIdResultDialog = Util.CreateGameObject("IdResultDialog", base.ParentGameObject).AddComponent<IdResultDialog>();
			mIdResultDialog.SetClosedCallback(OnIdResultDialogClosed);
			mIdResultDialog.SetBaseDepth(mBaseDepth + 70);
			mIdResultDialog.Init(mItem, mIDdata, mSearchData);
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("ID_Search_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		titleDescKey = string.Format(Localization.Get("ID_Search_Desk"));
		UILabel uILabel = Util.CreateLabel("ID_Search_Desk", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 80f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.spacingY = 10;
		titleDescKey = string.Format(Localization.Get("ID_Search_Desk02"));
		uILabel = Util.CreateLabel("ID_Search_Desc2", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, -54f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.red;
		uILabel.spacingY = 10;
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_search", "LC_button_search", "LC_button_search", mBaseDepth + 3, new Vector3(0f, -121f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnIdsearchPushed", UIButtonMessage.Trigger.OnClick);
		mInputs = Util.CreateInput("Input", base.gameObject);
		Util.SetInputInfo(mInputs, uIFont, 24, Def.InputId, image, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 12f, 0f), Vector3.one, 258, 60, UIWidget.Pivot.Center, 0f, string.Empty);
		Util.SetInputType(mInputs, UIInput.InputType.Standard, UIInput.KeyboardType.ASCIICapable, UIInput.Validation.Alphanumeric);
		CreateCancelButton("OnCancelPushed");
	}

	private void OnIdsearchPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mIDdata = GetInputPassword();
			if (mIDdata == mGame.mPlayer.Data.SearchID)
			{
				mItem = IdResultDialog.ID_RESULT.MYSELF;
				mIdResultDialog = Util.CreateGameObject("IdResultDialog", base.ParentGameObject).AddComponent<IdResultDialog>();
				mIdResultDialog.SetClosedCallback(OnIdResultDialogClosed);
				mIdResultDialog.SetBaseDepth(mBaseDepth + 70);
				mIdResultDialog.Init(mItem, mIDdata, mSearchData);
				mState.Change(STATE.WAIT);
			}
			else
			{
				mState.Reset(STATE.IDSEARCH, true);
			}
		}
	}

	private string GetInputPassword()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(mInputs.value);
		return stringBuilder.ToString();
	}

	public void OnIdResultDialogClosed(bool ResultFlg)
	{
		if (ResultFlg)
		{
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			UIFont font = GameMain.LoadFont();
			UnityEngine.Object.Destroy(mInputs.gameObject);
			mInputs = Util.CreateInput("Input", base.gameObject);
			Util.SetInputInfo(mInputs, font, 24, Def.InputId, image, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, 258, 60, UIWidget.Pivot.Center, 0f, string.Empty);
			Util.SetInputType(mInputs, UIInput.InputType.Standard, UIInput.KeyboardType.ASCIICapable, UIInput.Validation.Alphanumeric);
		}
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void Network_SearchIDUser()
	{
		mSearchIDUserCheckDone = false;
		mSearchIDUserSucceed = false;
		if (!Server.GetIDUser(mIDdata))
		{
			mSearchIDUserCheckDone = true;
		}
	}

	private void Server_OnSearchIDUser(int result, int code, string json)
	{
		if (result == 0)
		{
			try
			{
				if (string.IsNullOrEmpty(json))
				{
					mItem = IdResultDialog.ID_RESULT.NOTFOUND;
				}
				else
				{
					SearchUserData obj = new SearchUserData();
					new MiniJSONSerializer().Populate(ref obj, json);
					mSearchData = obj;
					if (mGame.mPlayer.Friends.ContainsKey(obj.uuid))
					{
						mItem = IdResultDialog.ID_RESULT.ALREADY;
					}
					else if (mGame.mPlayer.ApplyFriends.ContainsKey(obj.uuid))
					{
						mItem = IdResultDialog.ID_RESULT.ALREADY_APPLY;
					}
					else
					{
						mItem = IdResultDialog.ID_RESULT.HIT;
					}
				}
				mSearchIDUserSucceed = true;
			}
			catch (Exception)
			{
				mItem = IdResultDialog.ID_RESULT.ERROR;
				mSearchData = null;
			}
		}
		else if (code == 6)
		{
			mItem = IdResultDialog.ID_RESULT.NOTFOUND;
		}
		else
		{
			mItem = IdResultDialog.ID_RESULT.ERROR;
		}
		mSearchIDUserCheckDone = true;
	}
}
