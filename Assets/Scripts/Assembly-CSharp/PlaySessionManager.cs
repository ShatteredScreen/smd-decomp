using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class PlaySessionManager : MonoBehaviour
{
	public enum PlayPlace
	{
		NONE = 0,
		TITLE = 1,
		MAP = 2,
		PUZZLE = 3,
		OTHER = 4,
		ADV_MENU = 5,
		ADV_PUZZLE = 6,
		ADV_TEAM = 7,
		ADV_STAGE = 8,
		ADV_GACHA = 9
	}

	private static object mInstanceLock = new object();

	private static PlaySessionManager mInstance = null;

	public DateTime StartPlaySessionTime = DateTime.Now;

	public bool UsePlaySessionTime = true;

	public SortedDictionary<PlayPlace, double> PlayTimePlaceCount = new SortedDictionary<PlayPlace, double>();

	public PlayPlace LastPlace;

	public DateTime LastPlaceTime = new DateTime(0L);

	private bool isTerminating;

	public static readonly IDictionary<int, string> CHECK_OTHER_APPS = new SortedDictionary<int, string>
	{
		{ 5, "com.king.candycrushsaga" },
		{ 6, "com.disney.frozensaga_goo" },
		{ 7, "jp.gungho.pad" },
		{ 8, "com.facebook.katana" },
		{ 9, "com.twitter.android" },
		{ 10, "jp.naver.line.android" },
		{ 11, "com.linecorp.LGTMTM" },
		{ 12, "jp.co.mixi.monsterstrike" },
		{
			13,
			string.Empty
		},
		{ 14, "com.capcom.snoopyJP" },
		{ 15, "jp.co.happyelements.toto" },
		{ 16, "com.linecorp.LGRGS" },
		{ 17, "com.cookpad.android.activities" },
		{ 18, "com.instagram.android" },
		{ 19, "com.king.candycrushsodasaga" }
	};

	public static PlaySessionManager Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					if ((bool)gameObject)
					{
						mInstance = (PlaySessionManager)gameObject.GetComponent(typeof(PlaySessionManager));
					}
				}
				return mInstance;
			}
		}
	}

	private void OnApplicationPause(bool pause)
	{
		if (pause)
		{
			SendPlaySessionTime();
			return;
		}
		StartPlaySessionTime = DateTime.Now;
		UsePlaySessionTime = true;
		UpdatePlayTimePlace();
	}

	private void OnDestroy()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			SendPlaySessionTime();
		}
	}

	private void OnApplicationQuit()
	{
		if (!isTerminating)
		{
			isTerminating = true;
			SendPlaySessionTime();
		}
	}

	public void SendPlaySessionTime()
	{
		if (UsePlaySessionTime)
		{
			DateTime now = DateTime.Now;
			UpdatePlayTimePlace();
			long st = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(StartPlaySessionTime);
			long et = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
			long pst = (long)(now - StartPlaySessionTime).TotalSeconds;
			string text = "unknown";
			int ic = 0;
			if (InternetReachability.Status == NetworkReachability.ReachableViaLocalAreaNetwork)
			{
				ic = 1;
			}
			else if (InternetReachability.Status == NetworkReachability.ReachableViaCarrierDataNetwork)
			{
				ic = 2;
			}
			UpdatePlayTimePlace();
			string playTimePlaceString = GetPlayTimePlaceString();
			Player mPlayer = SingletonMonoBehaviour<GameMain>.Instance.mPlayer;
			Options mOptions = SingletonMonoBehaviour<GameMain>.Instance.mOptions;
			long startupCount = mPlayer.Data.StartupCount;
			long playCount = mPlayer.Data.PlayCount;
			int first_purchase = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mOptions.FirstPurchasedTime);
			long purchaseCount = mOptions.PurchaseCount;
			long totalWin = mPlayer.Data.TotalWin;
			long totalLose = mPlayer.Data.TotalLose;
			float winRate = mPlayer.WinRate;
			string levelArrayString = ServerCram.GetLevelArrayString(mPlayer);
			int rootHack = 0;
			string deviceModel = Network.DeviceModel;
			string operatingSystem = SystemInfo.operatingSystem;
			string fbid = mOptions.Fbid;
			string gcid = mOptions.Gcid;
			string gpid = mOptions.Gpid;
			string twid = mOptions.Twid;
			string gpgsName = mOptions.GpgsName;
			ServerCram.PlayTime(st, et, pst, deviceModel, operatingSystem, playTimePlaceString, fbid, gcid, gpid, twid, gpgsName, startupCount, playCount, first_purchase, purchaseCount, totalWin, totalLose, winRate, rootHack, ic, levelArrayString);
			UsePlaySessionTime = false;
			ResetPlayTimePlace();
		}
	}

	public void UpdatePlayTimePlace()
	{
		UpdatePlayTimePlace(LastPlace);
	}

	public void UpdatePlayTimePlace(PlayPlace place)
	{
		DateTime now = DateTime.Now;
		if (new DateTime(0L).CompareTo(LastPlaceTime) >= 0)
		{
			LastPlace = place;
			LastPlaceTime = now;
			return;
		}
		double value = 0.0;
		if (!PlayTimePlaceCount.TryGetValue(LastPlace, out value))
		{
			value = 0.0;
		}
		value += (now - LastPlaceTime).TotalSeconds;
		PlayTimePlaceCount[LastPlace] = value;
		LastPlace = place;
		LastPlaceTime = now;
	}

	public string GetPlayTimePlaceString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		PlayPlace[] array = Enum.GetValues(typeof(PlayPlace)) as PlayPlace[];
		for (int i = 0; i <= 3; i++)
		{
			double value = 0.0;
			if (!PlayTimePlaceCount.TryGetValue(array[i], out value))
			{
				value = 0.0;
			}
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(",");
			}
			stringBuilder.Append((long)Math.Floor(value));
		}
		string installCheckString = GetInstallCheckString();
		if (!string.IsNullOrEmpty(installCheckString))
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(",");
			}
			stringBuilder.Append(installCheckString);
		}
		for (int j = 4; j < array.Length; j++)
		{
			double value2 = 0.0;
			if (!PlayTimePlaceCount.TryGetValue(array[j], out value2))
			{
				value2 = 0.0;
			}
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(",");
			}
			stringBuilder.Append((long)Math.Floor(value2));
		}
		return stringBuilder.ToString();
	}

	public void ResetPlayTimePlace()
	{
		PlayTimePlaceCount.Clear();
		LastPlaceTime = new DateTime(0L);
	}

	public string GetInstallCheckString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (CHECK_OTHER_APPS.Count > 0)
		{
			foreach (KeyValuePair<int, string> cHECK_OTHER_APP in CHECK_OTHER_APPS)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(",");
				}
				bool flag = false;
				stringBuilder.Append((!flag) ? "0" : "1");
			}
		}
		return stringBuilder.ToString();
	}
}
