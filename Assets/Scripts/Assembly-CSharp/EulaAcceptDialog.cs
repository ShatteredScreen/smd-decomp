using System.Collections;
using UnityEngine;

public class EulaAcceptDialog : DialogBase
{
	public enum STATE
	{
		OPEN = 0,
		CLOSE = 1,
		WAIT = 2,
		EULA = 3
	}

	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.OPEN);

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private EULADialog mEULADialog;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.OPEN:
			if (!GetBusy())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		ResImage resTitleImage = ResourceManager.LoadImageAsync("TITLE");
		while (resTitleImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("TITLE").Image;
		if (SingletonMonoBehaviour<GameMain>.Instance.mPlayer.Data.LastTermServiceID == 0)
		{
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle(Localization.Get("Start_Title"));
		}
		else
		{
			InitDialogFrame(DIALOG_SIZE.LARGE, DIALOG_STYLE.NONTITLED, null);
		}
		UISprite sprite = Util.CreateSprite("Banner", base.gameObject, image);
		Util.SetSpriteInfo(sprite, "LC_start_banner01", mBaseDepth + 4, new Vector3(0f, 95f, 0f), Vector3.one, false);
		UIButton button = Util.CreateJellyImageButton("ButtonTOS", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_btn_service", "LC_btn_service", "LC_btn_service", mBaseDepth + 4, new Vector3(0f, -85f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnTosPushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonPrivacy", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_btn_privacy", "LC_btn_privacy", "LC_btn_privacy", mBaseDepth + 4, new Vector3(0f, -140f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPrivacyPushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_consent", "LC_button_consent", "LC_button_consent", mBaseDepth + 4, new Vector3(130f, -235f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_disagree", "LC_button_disagree", "LC_button_disagree", mBaseDepth + 4, new Vector3(-130f, -235f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		if (mState.GetStatus() == STATE.WAIT)
		{
			OnNegativePushed(null);
		}
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void OnTosPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			ShowEULADialog(EULADialog.MODE.EULA);
			mState.Change(STATE.EULA);
		}
	}

	public void OnPrivacyPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			ShowEULADialog(EULADialog.MODE.PRIVACY);
			mState.Change(STATE.EULA);
		}
	}

	public void ShowEULADialog(EULADialog.MODE aMode)
	{
		mEULADialog = Util.CreateGameObject("EULADialog", base.ParentGameObject).AddComponent<EULADialog>();
		mEULADialog.Init(aMode);
		mEULADialog.SetCallback(OnEULADialogClosed);
	}

	public new void OnEULADialogClosed(EULADialog.SELECT_ITEM item)
	{
		mEULADialog = null;
		mState.Change(STATE.WAIT);
	}

	public void SetCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
