using System.Collections;
using System.Collections.Generic;

public class ThreadSafeDictionary<TKey, TValue> : IEnumerable, IEnumerable<KeyValuePair<TKey, TValue>>, ICollection<KeyValuePair<TKey, TValue>>, IDictionary<TKey, TValue>
{
	protected Dictionary<TKey, TValue> _interalDict = new Dictionary<TKey, TValue>();

	protected readonly object _lock = new object();

	public ICollection<TKey> Keys
	{
		get
		{
			lock (_lock)
			{
				return _interalDict.Keys;
			}
		}
	}

	public ICollection<TValue> Values
	{
		get
		{
			lock (_lock)
			{
				return _interalDict.Values;
			}
		}
	}

	public TValue this[TKey key]
	{
		get
		{
			lock (_lock)
			{
				return _interalDict[key];
			}
		}
		set
		{
			lock (_lock)
			{
				_interalDict[key] = value;
			}
		}
	}

	public int Count
	{
		get
		{
			lock (_lock)
			{
				return _interalDict.Count;
			}
		}
	}

	public bool IsReadOnly
	{
		get
		{
			lock (_lock)
			{
				return false;
			}
		}
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return Clone().GetEnumerator();
	}

	public void Add(TKey key, TValue value)
	{
		lock (_lock)
		{
			_interalDict.Add(key, value);
		}
	}

	public bool ContainsKey(TKey key)
	{
		lock (_lock)
		{
			return _interalDict.ContainsKey(key);
		}
	}

	public bool Remove(TKey key)
	{
		lock (_lock)
		{
			return _interalDict.Remove(key);
		}
	}

	public bool TryGetValue(TKey key, out TValue value)
	{
		lock (_lock)
		{
			return _interalDict.TryGetValue(key, out value);
		}
	}

	public void Add(KeyValuePair<TKey, TValue> item)
	{
		lock (_lock)
		{
			_interalDict.Add(item.Key, item.Value);
		}
	}

	public void Clear()
	{
		lock (_lock)
		{
			_interalDict.Clear();
		}
	}

	public bool Contains(KeyValuePair<TKey, TValue> item)
	{
		lock (_lock)
		{
			TValue value;
			if (TryGetValue(item.Key, out value) && object.ReferenceEquals(value, item.Value))
			{
				return true;
			}
			return false;
		}
	}

	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
	{
		lock (_lock)
		{
			((ICollection<KeyValuePair<TKey, TValue>>)_interalDict).CopyTo(array, arrayIndex);
		}
	}

	public bool Remove(KeyValuePair<TKey, TValue> item)
	{
		lock (_lock)
		{
			return _interalDict.Remove(item.Key);
		}
	}

	public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
	{
		return Clone().GetEnumerator();
	}

	public IDictionary<TKey, TValue> Clone()
	{
		IDictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>();
		lock (_lock)
		{
			if (_interalDict.Count > 0)
			{
				foreach (KeyValuePair<TKey, TValue> item in _interalDict)
				{
					dictionary.Add(item);
				}
			}
		}
		return dictionary;
	}
}
