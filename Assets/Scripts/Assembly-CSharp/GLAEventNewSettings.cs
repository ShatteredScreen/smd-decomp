using System.Collections.Generic;

public class GLAEventNewSettings
{
	[MiniJSONAlias("event_id")]
	public short mEventID { get; set; }

	[MiniJSONAlias("help_url")]
	public string mHelpURL { get; set; }

	[MiniJSONAlias("week")]
	public List<short> mWeek { get; set; }

	[MiniJSONAlias("time")]
	public List<GLAEventNewSettingsTime> mTimeList { get; set; }

	public GLAEventNewSettings()
	{
		mWeek = new List<short>();
		mTimeList = new List<GLAEventNewSettingsTime>();
	}
}
