public class GetSaveDataOfOthersProfile : ParameterObject<GetSaveDataOfOthersProfile>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("searchid")]
	public string SearchID { get; set; }
}
