using System;

public class ConnectSegmentProfile : ConnectBase
{
	protected override void InitServerFlg()
	{
		GameMain.mSegmentProfileCheckDone = false;
		GameMain.mSegmentProfileSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mSegmentProfileCheckDone = true;
		GameMain.mSegmentProfileSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mSegmentProfileCheckDone = true;
		GameMain.mSegmentProfileSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			mGame.mSegmentProfile = new SegmentProfile();
			mGame.SetDebugSegmentInfoNoNetwork();
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetSegmentInfoTime, DateTime.Now);
		long gET_SEGMENT_INFO_FREQ = GameMain.GET_SEGMENT_INFO_FREQ;
		if (gET_SEGMENT_INFO_FREQ > 0 && num < gET_SEGMENT_INFO_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		string text = string.Empty;
		string a_datekey = string.Empty;
		if (mParam != null)
		{
			ConnectGetProfileParam connectGetProfileParam = mParam as ConnectGetProfileParam;
			if (connectGetProfileParam != null)
			{
				a_datekey = connectGetProfileParam.DebugDate;
				text = connectGetProfileParam.DebugKey;
			}
		}
		if (GameMain.DEBUG_DEBUGSERVERINFO && string.IsNullOrEmpty(text))
		{
			text = "debug";
			GameMain.GET_SEGMENT_INFO_FREQ = 240L;
		}
		InitServerFlg();
		if (!Server.GetSegmentInfo(a_datekey, text))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetSegmentProfileFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastGetSegmentInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
