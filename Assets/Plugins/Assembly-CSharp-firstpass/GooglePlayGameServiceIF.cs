using System;
using UnityEngine;

public abstract class GooglePlayGameServiceIF : MonoBehaviour
{
	private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

	public static Action<string> authenticationSucceededEvent;

	public static Action<string> authenticationFailedEvent;

	public static Action<object> loadPlayerSucceededEvent;

	public static Action<string> loadPlayerFailedEvent;

	public static Action<object> showAchievementsSucceededEvent;

	public static Action<string> showAchievementsFailedEvent;

	public void enableLogging(bool enable)
	{
		enableLogging(enable, enable);
	}

	public abstract void enableLogging(bool enable, bool verbose);

	public void setup(string clientid)
	{
		setup(string.Empty, false, clientid, string.Empty);
	}

	public abstract void setup(string appId, bool autoConnect, string clientId, string clientSecret);

	public abstract void onPause();

	public abstract void onResume();

	public abstract bool isCanceled();

	public abstract bool isSignedIn();

	public abstract void authenticate();

	public abstract void signOut();

	public abstract bool loadPlayer(string id);

	public abstract bool unlockAchievement(string id);

	public abstract bool incrementAchievement(string id, int step);

	public abstract bool showAchievements();

	public abstract bool resetAchievement(string id);

	public abstract bool resetAchievementAll();

	protected static string ToSS(object o)
	{
		return (o != null) ? o.ToString() : string.Empty;
	}

	protected static long time()
	{
		return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
	}

	protected static void log(string format, params object[] args)
	{
		try
		{
			long num = time();
			string arg = string.Format(format, args);
			UnityEngine.Debug.Log(string.Format("@@@ {0}({1}): {2}", "GPGSPlugin", num, arg));
		}
		catch (Exception)
		{
		}
	}

	protected static void dbg(string format, params object[] args)
	{
		try
		{
			long num = time();
			string arg = string.Format(format, args);
			UnityEngine.Debug.Log(string.Format("@@@ {0}({1}): {2}", "GPGSPlugin", num, arg));
		}
		catch (Exception)
		{
		}
	}

	protected void _PluginCB_onSignInSucceeded(string result)
	{
		if (authenticationSucceededEvent != null)
		{
			authenticationSucceededEvent(result);
		}
	}

	protected void _PluginCB_onSignInFailed(string result)
	{
		if (authenticationFailedEvent != null)
		{
			authenticationFailedEvent(result);
		}
	}

	protected void _PluginCB_onLoadPlayerSucceeded(string result)
	{
		try
		{
			if (loadPlayerSucceededEvent != null)
			{
				loadPlayerSucceededEvent(MiniJSON.jsonDecode(result));
			}
		}
		catch (Exception ex)
		{
			_PluginCB_onLoadPlayerFailed(ex.ToString());
		}
	}

	protected void _PluginCB_onLoadPlayerFailed(string result)
	{
		if (loadPlayerFailedEvent != null)
		{
			loadPlayerFailedEvent(result);
		}
	}

	protected void _PluginCB_onAchievemtnsSucceeded(string result)
	{
		try
		{
			if (showAchievementsSucceededEvent != null)
			{
				showAchievementsSucceededEvent(MiniJSON.jsonDecode(result));
			}
		}
		catch (Exception ex)
		{
			_PluginCB_onAchievemtnsFailed(ex.ToString());
		}
	}

	protected void _PluginCB_onAchievemtnsFailed(string result)
	{
		if (showAchievementsFailedEvent != null)
		{
			showAchievementsFailedEvent(result);
		}
	}
}
