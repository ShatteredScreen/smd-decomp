using System;
using System.Collections.Generic;
using UnityEngine;

public class FriendHelpDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		NETWORK_00 = 3,
		NETWORK_00_1 = 4,
		NETWORK_00_2 = 5
	}

	public enum SELECT_ITEM
	{
		SUCCESS = 0,
		ERROR = 1,
		NO = 2
	}

	public enum SendFriendHelpResult
	{
		SENT_OK = 0,
		SENT_NG = 1,
		NO_FRIEND = 2,
		NO_FRIENDHELP = 3
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private PuzzleManager mManager;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	public STATE CurrentState;

	private SELECT_ITEM mSelectItem;

	public int mStars;

	private OnDialogClosed mCallback;

	private bool mFriendHelp;

	private BoxCollider mColliderFriend;

	private UIButton mFriendHelpButton;

	private string mImageName = "LC_button_friendHelp_all";

	private bool mSendFriendHelpSucceed;

	private bool mSendFriendHelpCheckDone;

	private List<MyFriendData> mSendableList;

	public override void Start()
	{
		mManager = GameObject.Find("PuzzleManager").GetComponent<PuzzleManager>();
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		CurrentState = mState.GetStatus();
		switch (CurrentState)
		{
		case STATE.INIT:
			Server.GiveGiftAllEvent += Server_SendFriendHelp;
			mState.Reset(STATE.MAIN, true);
			break;
		case STATE.NETWORK_00_1:
			if (mSendFriendHelpCheckDone)
			{
				mState.Change(STATE.NETWORK_00_2);
			}
			break;
		case STATE.NETWORK_00_2:
			if (mSendFriendHelpSucceed)
			{
				mSelectItem = SELECT_ITEM.SUCCESS;
			}
			else
			{
				mSelectItem = SELECT_ITEM.ERROR;
				SendFriendHelpFail();
			}
			mState.Reset(STATE.WAIT, true);
			Close();
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("FriendHelp_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("FriendHelp_Desc00"), mBaseDepth + 3, new Vector3(0f, 60f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.spacingY = 10;
		uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("FriendHelp_Desc01"), mBaseDepth + 3, new Vector3(0f, -35f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.red;
		mFriendHelpButton = Util.CreateJellyImageButton("ButtonHelp", base.gameObject, image);
		Util.SetImageButtonInfo(mFriendHelpButton, mImageName, mImageName, mImageName, mBaseDepth + 4, new Vector3(0f, -110f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mFriendHelpButton, this, "OnFriendHelpPushed", UIButtonMessage.Trigger.OnClick);
		mColliderFriend = mFriendHelpButton.GetComponent<BoxCollider>();
		CreateCancelButton("OnBackKeyPress");
	}

	public void OnFriendHelpPushed(GameObject go)
	{
		if (!GetBusy() && CurrentState == STATE.MAIN)
		{
			Network_SendFriendHelp();
			mState.Change(STATE.NETWORK_00_1);
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Server.GiveGiftAllEvent -= Server_SendFriendHelp;
	}

	public override void OnBackKeyPress()
	{
		OnNoPushed(null);
	}

	public void OnNoPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NO;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void Network_SendFriendHelp()
	{
		mSendFriendHelpSucceed = false;
		mSendFriendHelpCheckDone = false;
		Player mPlayer = mGame.mPlayer;
		if (!mPlayer.SendFriendHelpStatus())
		{
			mSendFriendHelpCheckDone = true;
			return;
		}
		mSendableList = mPlayer.GetSendableFriendHelp();
		if (mSendableList.Count == 0)
		{
			mSendFriendHelpCheckDone = true;
			return;
		}
		List<int> list = new List<int>();
		for (int i = 0; i < mSendableList.Count; i++)
		{
			if (mSendableList[i].UUID != 0)
			{
				list.Add(mSendableList[i].UUID);
			}
		}
		GiftItem giftItem = new GiftItem();
		giftItem.Data.FromID = mGame.mPlayer.UUID;
		giftItem.Data.Message = mGame.mPlayer.NickName;
		giftItem.Data.ItemCategory = 18;
		giftItem.Data.ItemKind = (int)mGame.mCurrentStage.Series;
		giftItem.Data.ItemID = mGame.mCurrentStage.StageNumber;
		giftItem.Data.Quantity = 1;
		if (!Server.GiveGiftAll(giftItem.Data, list))
		{
			mSendFriendHelpCheckDone = true;
		}
	}

	private void Server_SendFriendHelp(int result, int code)
	{
		if (result == 0)
		{
			mSendFriendHelpSucceed = true;
			SendFriendHelpSuccess();
			int series = (int)mGame.mCurrentStage.Series;
			int stageNumber = mGame.mCurrentStage.StageNumber;
			int num = mStars;
			int num2 = 0;
			int num3 = 0;
			float num4 = 0f;
			PlayerStageData _psd;
			if (mGame.mEventMode)
			{
				int eventID = mGame.mCurrentStage.EventID;
				short a_course = 0;
				int a_main;
				int a_sub;
				Def.SplitEventStageNo(mGame.mCurrentStage.StageNumber, out a_course, out a_main, out a_sub);
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, eventID, a_course, Def.GetStageNo(a_main, a_sub), out _psd);
				int stageNo = Def.GetStageNo(a_course, a_main, a_sub);
				stageNumber = Def.GetStageNoForServer(eventID, stageNo);
			}
			else
			{
				mGame.mPlayer.GetStageChallengeData(mGame.mCurrentStage.Series, mGame.mCurrentStage.StageNumber, out _psd);
			}
			if (_psd != null)
			{
				num2 = _psd.WinCount;
				num3 = _psd.LoseCount;
				num4 = _psd.WinRate;
			}
		}
		mSendFriendHelpCheckDone = true;
	}

	private void SendFriendHelpSuccess()
	{
		mGame.mPlayer.Data.LastSentFriendHelpTime = DateTime.Now;
		for (int i = 0; i < mSendableList.Count; i++)
		{
			MyFriendData myFriendData = mSendableList[i];
			mGame.mPlayer.UpdateFriendHelp(myFriendData.UUID);
			if (mGame.mPlayer.Friends.ContainsKey(myFriendData.UUID))
			{
				mGame.mPlayer.Friends[myFriendData.UUID].SetData(myFriendData);
			}
		}
		mGame.Save();
	}

	private void SendFriendHelpFail()
	{
	}
}
