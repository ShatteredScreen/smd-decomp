using System.Collections.Generic;

public class AdvFigureExchange
{
	[MiniJSONAlias("figure_id")]
	public int figure_id { get; set; }

	[MiniJSONAlias("rewards")]
	public List<AdvExchangeRewards> ExchangeRewardList { get; set; }
}
