using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BIJUnityPlugin.Android
{
	internal static class API
	{
		private const string LOG_TAG = "AndroidAPI";

		private const string PLUGIN_CLASS = "com.bij.bijunityplugin.BIJUnity";

		private static readonly DateTime _BASE_TIME = new DateTime(2014, 1, 1);

		private static object mInstanceLock = new object();

		private static AndroidJavaObject mInstance = null;

		private static string _BIJUnity_dataPath = null;

		private static string _BIJUnity_tempPath = null;

		private static AndroidJavaObject AndroidPlugin
		{
			get
			{
				if (mInstance == null)
				{
					lock (mInstanceLock)
					{
						if (mInstance == null)
						{
							using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
							{
								using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
								{
									object[] args = new object[1] { androidJavaObject };
									AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.bij.bijunityplugin.BIJUnity");
									mInstance = androidJavaClass2.CallStatic<AndroidJavaObject>("getInstance", args);
								}
							}
						}
					}
				}
				return mInstance;
			}
		}

		private static long time()
		{
			return (long)((DateTime.Now.ToUniversalTime() - _BASE_TIME).TotalMilliseconds + 0.5);
		}

		private static void log(string format, params object[] args)
		{
		}

		private static void dbg(string format, params object[] args)
		{
		}

		[DllImport("BIJUnityPlugin")]
		public static extern void BIJUnity_dummyCrash();

		public static void BIJUnity_crash(int level)
		{
			if (level == 0)
			{
				BIJUnity_dummyCrash();
				return;
			}
			using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.bij.bijunityplugin.BIJUnity"))
			{
				UnityEngine.Debug.Log("BIJUnity_crash call 1");
				IntPtr methodID = AndroidJNI.GetMethodID(androidJavaClass.GetRawClass(), "throwRuntimeException", "()V");
				UnityEngine.Debug.Log("BIJUnity_crash call 2");
				AndroidJNI.CallVoidMethod(AndroidPlugin.GetRawObject(), methodID, new jvalue[0]);
				UnityEngine.Debug.Log("BIJUnity_crash call 3");
			}
		}

		public static void BIJUnity_setup(string _gameObject, string _callback, bool _isEditor, bool _isWW)
		{
			bool flag = AndroidPlugin.Call<bool>("setup", new object[3] { _gameObject, _callback, _isWW });
			BIJUnity_getDataPath();
			BIJUnity_getTempPath();
		}

		public static bool BIJUnity_isInputEnabled()
		{
			return AndroidPlugin.CallStatic<bool>("isInputEnabled", new object[0]);
		}

		public static void BIJUnity_enableInput(bool enabled, int durationMills)
		{
			AndroidPlugin.CallStatic("enableInput", enabled, durationMills);
		}

		public static string BIJUnity_getApplicationName()
		{
			return AndroidPlugin.Call<string>("getApplicationName", new object[0]);
		}

		public static string BIJUnity_getBundleName()
		{
			return AndroidPlugin.Call<string>("getPackageName", new object[0]);
		}

		public static string BIJUnity_getBundleVersion()
		{
			return AndroidPlugin.Call<string>("getPackageVersion", new object[0]);
		}

		public static string BIJUnity_getDataPath()
		{
			if (string.IsNullOrEmpty(_BIJUnity_dataPath))
			{
				_BIJUnity_dataPath = Application.persistentDataPath;
				if (string.IsNullOrEmpty(_BIJUnity_dataPath))
				{
					_BIJUnity_dataPath = BIJUnity_getDataPathInternal();
				}
			}
			return _BIJUnity_dataPath;
		}

		private static string BIJUnity_getDataPathInternal()
		{
			AndroidJNI.AttachCurrentThread();
			AndroidJNI.PushLocalFrame(0);
			try
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					dbg("UnityPlayer = " + androidJavaClass);
					AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
					dbg("Activity = " + @static);
					using (AndroidJavaObject androidJavaObject = @static.Call<AndroidJavaObject>("getFilesDir", new object[0]))
					{
						dbg("File = " + androidJavaObject);
						return androidJavaObject.Call<string>("getCanonicalPath", new object[0]);
					}
				}
			}
			catch (Exception ex)
			{
				log("BIJUnity_getDataPathInternal failed: " + ex.ToString());
			}
			finally
			{
				AndroidJNI.PopLocalFrame(IntPtr.Zero);
			}
			return null;
		}

		public static string BIJUnity_getTempPath()
		{
			if (string.IsNullOrEmpty(_BIJUnity_tempPath))
			{
				_BIJUnity_tempPath = Application.temporaryCachePath;
				if (string.IsNullOrEmpty(_BIJUnity_tempPath))
				{
					_BIJUnity_tempPath = BIJUnity_getTempPathInternal();
				}
			}
			return _BIJUnity_tempPath;
		}

		private static string BIJUnity_getTempPathInternal()
		{
			AndroidJNI.AttachCurrentThread();
			AndroidJNI.PushLocalFrame(0);
			try
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					dbg("UnityPlayer = " + androidJavaClass);
					AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
					dbg("Activity = " + @static);
					using (AndroidJavaObject androidJavaObject = @static.Call<AndroidJavaObject>("getCacheDir", new object[0]))
					{
						dbg("File = " + androidJavaObject);
						return androidJavaObject.Call<string>("getCanonicalPath", new object[0]);
					}
				}
			}
			catch (Exception ex)
			{
				log("BIJUnity_getTempPathInternal failed: " + ex.ToString());
			}
			finally
			{
				AndroidJNI.PopLocalFrame(IntPtr.Zero);
			}
			return null;
		}

		public static double BIJUnity_getFreeDiskSpace(string path)
		{
			return AndroidPlugin.Call<double>("getFreeDiskSpace", new object[1] { path });
		}

		public static string BIJUnity_getLanguageCode()
		{
			return AndroidPlugin.Call<string>("getLanguageCode", new object[0]);
		}

		public static string BIJUnity_getCountryCode()
		{
			return AndroidPlugin.Call<string>("getCountryCode", new object[0]);
		}

		public static string BIJUnity_getMobileCountryCode()
		{
			return AndroidPlugin.Call<string>("getMobileCountryCode", new object[0]);
		}

		public static string BIJUnity_getCurrencyCode()
		{
			return AndroidPlugin.Call<string>("getCurrencyCode", new object[0]);
		}

		public static string BIJUnity_getUnityID()
		{
			return SystemInfo.deviceUniqueIdentifier;
		}

		public static void BIJUnity_setClipboardText(string text)
		{
			AndroidPlugin.Call("setClipboardText", text, 1000L);
		}

		public static string BIJUnity_getClipboardText()
		{
			return AndroidPlugin.Call<string>("getClipboardText", new object[1] { 1000L });
		}

		public static bool BIJUnity_isAvailableRotation()
		{
			return AndroidPlugin.Call<bool>("isAvailableRotation", new object[1] { 1000L });
		}

		public static int BIJUnity_getStatusBarOrientation()
		{
			return (int)Screen.orientation;
		}

		public static string BIJUnity_getDeviceID()
		{
			return SystemInfo.deviceUniqueIdentifier;
		}

		public static string BIJUnity_getOriginalUniqueId()
		{
			return AndroidPlugin.Call<string>("getOriginalUniqueId", new object[0]);
		}

		public static string BIJUnity_getIMEI()
		{
			return AndroidPlugin.Call<string>("getIMEI", new object[0]);
		}

		public static string BIJUnity_getIMSI()
		{
			return AndroidPlugin.Call<string>("getIMSI", new object[0]);
		}

		public static string BIJUnity_getAndroidID()
		{
			return AndroidPlugin.Call<string>("getAndroidID", new object[0]);
		}

		public static string BIJUnity_getSerial()
		{
			return AndroidPlugin.Call<string>("getSIMSerial", new object[0]);
		}

		public static string BIJUnity_getDeviceModel()
		{
			return SystemInfo.deviceModel;
		}

		public static string BIJUnity_getOpenUDID()
		{
			return "android";
		}

		public static string BIJUnity_getAdvertisingID()
		{
			return "android";
		}

		public static string BIJUnity_getVendorID()
		{
			return "android";
		}

		public static string BIJUnity_getMacAddress()
		{
			string text = AndroidPlugin.Call<string>("getMacAddress", new object[0]);
			if (!string.IsNullOrEmpty(text))
			{
				text = text.ToUpper();
			}
			return text;
		}

		public static string BIJUnity_getDeviceSerial()
		{
			return AndroidPlugin.Call<string>("getDeviceSerial", new object[0]);
		}

		public static void BIJUnity_vibrate(long time)
		{
			AndroidPlugin.Call("vibrate", time);
		}

		public static bool BIJUnity_checkInstalledPackage(string packagename)
		{
			return AndroidPlugin.Call<bool>("checkInstalledPackage", new object[1] { packagename });
		}

		public static bool BIJUnity_runInstalledPackage(string packagename, string classname)
		{
			return AndroidPlugin.Call<bool>("runInstalledPackage", new object[2] { packagename, classname });
		}

		public static bool BIJUnity_invokeStore(string packagename)
		{
			return AndroidPlugin.Call<bool>("invokeStore", new object[1] { packagename });
		}

		public static void BIJUnity_showMemoryStats()
		{
			AndroidPlugin.Call("showMemoryStats");
		}

		public static void BIJUnity_registerMemoryStats(int time)
		{
			AndroidPlugin.Call("registerMemoryStats", time);
		}

		public static void BIJUnity_unregisterMemoryStats()
		{
			AndroidPlugin.Call("unregisterMemoryStats");
		}

		public static void BIJUnity_registerDiagnostic(int time)
		{
			AndroidPlugin.Call("registerDiagnostic", time);
		}

		public static void BIJUnity_unregisterDiagnostic()
		{
			AndroidPlugin.Call("unregisterDiagnostic");
		}

		public static float BIJUnity_getUsedMemory()
		{
			return AndroidPlugin.Call<float>("getUsedMemory", new object[0]);
		}

		public static float BIJUnity_getBatteryPower()
		{
			return AndroidPlugin.Call<float>("getBatteryPower", new object[0]);
		}

		public static int BIJUnity_getSignalType()
		{
			return AndroidPlugin.Call<int>("getSignalType", new object[0]);
		}

		public static float BIJUnity_getSignalStrength()
		{
			return AndroidPlugin.Call<float>("getSignalStrength", new object[0]);
		}

		[DllImport("BIJUnityPlugin")]
		public static extern bool BIJUnity_is32bitPlatform();

		[DllImport("BIJUnityPlugin")]
		public static extern bool BIJUnity_is64bitPlatform();

		public static void BIJUnity_scheduleLocalNotification(int nid, string message, string title, int afterSeconds)
		{
			AndroidPlugin.Call("scheduleLocalNotification", nid, message, title, afterSeconds);
		}

		public static void BIJUnity_cancelLocalNotification(int nid)
		{
			AndroidPlugin.Call("cancelLocalNotification", nid);
		}

		public static void BIJUnity_cancelAllLocalNotification()
		{
			AndroidPlugin.Call("cancelAllLocalNotification");
		}

		public static void BIJUnity_registerForPushNotifications()
		{
		}

		public static void BIJUnity_unregisterForPushNotifications()
		{
		}

		public static string BIJUnity_getRemoteNotificationToken()
		{
			return AndroidPlugin.Call<string>("getRemoteNotificationToken", new object[0]);
		}

		public static void BIJUnity_enableRemoteNotification(bool enabled)
		{
			AndroidPlugin.Call("enableRemoteNotification", enabled);
		}

		public static bool BIJUnity_isEnableRemoteNotification()
		{
			return AndroidPlugin.Call<bool>("isEnableRemoteNotification", new object[0]);
		}

		public static bool BIJUnity_invokeFacebookWithImage(string imageUrl)
		{
			return AndroidPlugin.Call<bool>("snsInvokeFacebookWithImage", new object[1] { imageUrl });
		}

		public static bool BIJUnity_invokeFacebookStore()
		{
			return AndroidPlugin.Call<bool>("snsInvokeFacebookStore", new object[0]);
		}

		public static bool BIJUnity_invokeLINEWithMessage(string title, string message, string imageUrl)
		{
			return AndroidPlugin.Call<bool>("snsInvokeLINEWithMessage", new object[3] { title, message, imageUrl });
		}

		public static bool BIJUnity_invokeLINEWithImage(string title, string message, string imageUrl)
		{
			return AndroidPlugin.Call<bool>("snsInvokeLINEWithImage", new object[3] { title, message, imageUrl });
		}

		public static bool BIJUnity_invokeTwitterWithMessage(string title, string message, string imageUrl)
		{
			return AndroidPlugin.Call<bool>("snsInvokeTwitterWithMessage", new object[3] { title, message, imageUrl });
		}

		public static bool BIJUnity_invokeTwitterOrStoreWithMessage(string title, string message, string imageUrl)
		{
			return AndroidPlugin.Call<bool>("snsInvokeTwitterOrStoreWithMessage", new object[3] { title, message, imageUrl });
		}

		public static bool BIJUnity_invokeTwitterStore()
		{
			return AndroidPlugin.Call<bool>("snsInvokeTwitterStore", new object[0]);
		}

		public static bool BIJUnity_invokeSMSWithMessage(string title, string message, string imageUrl)
		{
			return AndroidPlugin.Call<bool>("snsInvokeSMSWithMessage", new object[3] { title, message, imageUrl });
		}

		public static bool BIJUnity_invokeMailWithMessageAndImage(string title, string message, string imageUrl)
		{
			return AndroidPlugin.Call<bool>("snsInvokeMailWithMessageAndImage", new object[3] { title, message, imageUrl });
		}

		public static AdInfo BIJUnity_getAdvertisingInfo()
		{
			AdInfo adInfo = new AdInfo(string.Empty, false);
			using (AndroidJavaObject androidJavaObject = AndroidPlugin.Call<AndroidJavaObject>("getAdvertisingInfo", new object[0]))
			{
				return new AdInfo(androidJavaObject.Call<string>("getId", new object[0]), androidJavaObject.Call<bool>("isEnabled", new object[0]));
			}
		}

		public static bool BIJUnity_getAdvertisingInfoAsync(GameObject gameObject)
		{
			if (gameObject != null)
			{
				return AndroidPlugin.Call<bool>("acquireAdvertisingInfo", new object[1] { gameObject.name });
			}
			return false;
		}

		public static bool BIJUnity_writeToPhotoAlbumAsync(string url, GameObject gameObject, string callback)
		{
			if (gameObject != null)
			{
				return AndroidPlugin.Call<bool>("writeToPhotoAlbumAsync", new object[3] { url, gameObject.name, callback });
			}
			return false;
		}

		public static int BIJUnity_checkPermission()
		{
			return AndroidPlugin.Call<int>("checkPermission", new object[0]);
		}

		public static void BIJUnity_requestPermission(string gameObject, string callback)
		{
			AndroidPlugin.Call("requestPermission", gameObject, callback);
		}

		public static int BIJUnity_shouldShowRequestPermissionRationale()
		{
			return AndroidPlugin.Call<int>("shouldShowRequestPermissionRationale", new object[0]);
		}

		public static void BIJUnity_openApplicationDetailsSettings()
		{
			AndroidPlugin.Call("openApplicationDetailsSettings");
		}

		public static string BIJUnity_getGoogleAccount()
		{
			return AndroidPlugin.Call<string>("getGoogleAccount", new object[0]);
		}

		public static void BIJUnity_checkPermissionPhoto(GameObject gameObject, string callback)
		{
			gameObject.SendMessage(callback, "true");
		}
	}
}
