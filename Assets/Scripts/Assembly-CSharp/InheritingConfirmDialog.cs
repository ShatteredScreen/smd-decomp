using System.Collections;
using UnityEngine;

public class InheritingConfirmDialog : DialogBase
{
	public enum REASON
	{
		NONE = 0,
		STARTUP = 1,
		LOAD_ERROR = 2,
		AUTH_ERROR = 3,
		AUTHKEY_ERROR = 4
	}

	public enum SELECT_ITEM
	{
		NEWGAME = 0,
		INHERITING = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i, REASON reason, int maltaReason);

	private REASON mReason;

	private int mMaltaReason = 9999;

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private BIJImage mTitleAtlas;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public void Init(REASON reason, int maltaReason, OnDialogClosed callback)
	{
		mReason = reason;
		mMaltaReason = maltaReason;
		mCallback = callback;
	}

	public override IEnumerator BuildResource()
	{
		ResImage resTitleImage = ResourceManager.LoadImageAsync("TITLE");
		while (resTitleImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mTitleAtlas = resTitleImage.Image;
	}

	public override void BuildDialog()
	{
		base.BuildDialog();
		UIFont atlasFont = GameMain.LoadFont();
		if (mReason == REASON.STARTUP)
		{
			InitDialogFrame(DIALOG_SIZE.MEDIUM);
			UISprite sprite = Util.CreateSprite("Banner", base.gameObject, mTitleAtlas);
			Util.SetSpriteInfo(sprite, "LC_start_banner00", mBaseDepth + 1, new Vector3(0f, 135f, 0f), Vector3.one, false);
			UILabel uILabel = Util.CreateLabel("Desc01", base.gameObject, atlasFont);
			float num = 0f;
			num = -6f;
			Util.SetLabelInfo(uILabel, Localization.Get("Inheriting_Confirm_New_Desc"), mBaseDepth + 2, new Vector3(0f, num, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.spacingY = 6;
			uILabel.SetDimensions(500, 120);
			UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, mTitleAtlas);
			Util.SetImageButtonInfo(button, "LC_button_startNew", "LC_button_startNew", "LC_button_startNew", mBaseDepth + 4, new Vector3(-130f, -110f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
			UIButton button2 = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, mTitleAtlas);
			Util.SetImageButtonInfo(button2, "LC_button_TakeoverData", "LC_button_TakeoverData", "LC_button_TakeoverData", mBaseDepth + 5, new Vector3(130f, -110f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button2, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
			return;
		}
		InitDialogFrame(DIALOG_SIZE.LARGE, null, null, 0, 460);
		string text = string.Empty;
		string text2 = string.Empty;
		switch (mReason)
		{
		case REASON.NONE:
			InitDialogTitle("NONE");
			break;
		case REASON.LOAD_ERROR:
			InitDialogTitle(Localization.Get("Inheriting_Confirm_Title01"));
			text = Localization.Get("Inheriting_Confirm_Reason01");
			text2 = Localization.Get("Inheriting_Confirm_Desc01b");
			break;
		case REASON.AUTH_ERROR:
		case REASON.AUTHKEY_ERROR:
			InitDialogTitle(Localization.Get("Inheriting_Confirm_Title02"));
			text = Localization.Get("Inheriting_Confirm_Reason02");
			text2 = Localization.Get("Inheriting_Confirm_Desc01b");
			break;
		}
		float num2 = 0f;
		if (!string.IsNullOrEmpty(text))
		{
			UILabel uILabel2 = Util.CreateLabel("Reason", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel2, text, mBaseDepth + 1, new Vector3(0f, 130f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel2.color = new Color32(198, 0, 0, byte.MaxValue);
			uILabel2.SetDimensions(500, 100);
		}
		else
		{
			num2 = 70f;
		}
		UILabel uILabel3 = Util.CreateLabel("Desc01", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel3, text2, mBaseDepth + 2, new Vector3(0f, 40f + num2, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel3.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel3.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel3.SetDimensions(470, 120);
		uILabel3 = Util.CreateLabel("Desc02", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel3, Localization.Get("Inheriting_Confirm_Desc02"), mBaseDepth + 2, new Vector3(0f, -50f + num2, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel3.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel3.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel3.SetDimensions(470, 120);
		UIButton button3 = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, mTitleAtlas);
		Util.SetImageButtonInfo(button3, "LC_button_startNew", "LC_button_startNew", "LC_button_startNew", mBaseDepth + 4, new Vector3(-130f, -140f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button3, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		UIButton button4 = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, mTitleAtlas);
		Util.SetImageButtonInfo(button4, "LC_button_TakeoverData", "LC_button_TakeoverData", "LC_button_TakeoverData", mBaseDepth + 5, new Vector3(130f, -140f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button4, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem, mReason, mMaltaReason);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.NEWGAME;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.INHERITING;
			Close();
		}
	}
}
