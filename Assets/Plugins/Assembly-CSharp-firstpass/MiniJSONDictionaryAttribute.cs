using System;

public class MiniJSONDictionaryAttribute : Attribute
{
	public Type KeyType { get; set; }

	public Type ValueType { get; set; }

	public MiniJSONDictionaryAttribute(Type keyType, Type valueType)
	{
		KeyType = keyType;
		ValueType = valueType;
	}
}
