using System;
using System.Globalization;

public class DateTimeUtil
{
	public static readonly DateTime UnitTimeEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);

	public static readonly DateTime UnitLocalTimeEpoch = UnitTimeEpoch.ToLocalTime();

	public static DateTime Tomorrow
	{
		get
		{
			return DateTime.Now.AddDays(1.0);
		}
	}

	public static DateTime Yesterday
	{
		get
		{
			return DateTime.Now.AddDays(-1.0);
		}
	}

	private DateTimeUtil()
	{
	}

	public static DateTime Now()
	{
		DateTime result = DateTime.Now;
		if (!GameMain.mDebugNotUseServerTimeDiff)
		{
			result = result.AddSeconds(GameMain.mServerTimeDiff * -1.0);
		}
		return result;
	}

	public static DateTime GetDateTime(int year, int month, int day, int hour, int minute, int second, int millsecond)
	{
		return new DateTime(year, month, day, hour, minute, second, millsecond);
	}

	public static DateTime GetDate(int year, int month, int day)
	{
		return GetDateTime(year, month, day, 0, 0, 0, 0);
	}

	public static DateTime GetDate(DateTime dt)
	{
		return GetDate(dt.Year, dt.Month, dt.Day);
	}

	public static DateTime UnixTimeStampToDateTime(double unixTimeStamp, bool toLocal = false)
	{
		DateTime result = UnitTimeEpoch.AddSeconds(unixTimeStamp);
		if (toLocal)
		{
			result = result.ToLocalTime();
		}
		return result;
	}

	public static double LoacalDateTimeToUnixTimeStamp(DateTime dateTime)
	{
		return DateTimeToUnixTimeStamp(dateTime, true);
	}

	public static double DateTimeToUnixTimeStamp(DateTime dateTime, bool fromLocal = false)
	{
		if (fromLocal)
		{
			dateTime = dateTime.ToUniversalTime();
		}
		return (dateTime - UnitTimeEpoch).TotalSeconds;
	}

	public static DateTime JavaTimeStampToDateTime(double javaTimeStamp)
	{
		return UnitTimeEpoch.AddSeconds(Math.Round(javaTimeStamp / 1000.0));
	}

	public static string DateTimeToHttpDate(DateTime dateTime)
	{
		return dateTime.ToUniversalTime().ToString("s");
	}

	public static DateTime HttpDateToDateTime(string httpDate)
	{
		return DateTime.ParseExact(httpDate, "ddd, dd MMM yyyy HH:mm:ss 'GMT'", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.AssumeUniversal);
	}

	public static long BetweenDays0(DateTime fdt, DateTime tdt)
	{
		DateTime date = GetDate(fdt.Year, fdt.Month, fdt.Day);
		return (long)Math.Floor(GetDate(tdt.Year, tdt.Month, tdt.Day).Subtract(date).TotalDays);
	}

	public static long BetweenDays(DateTime fdt, DateTime tdt)
	{
		return (long)Math.Floor(tdt.Subtract(fdt).TotalDays);
	}

	public static long BetweenHours(DateTime fdt, DateTime tdt)
	{
		return (long)Math.Floor(tdt.Subtract(fdt).TotalHours);
	}

	public static long BetweenMinutes(DateTime fdt, DateTime tdt)
	{
		return (long)Math.Floor(tdt.Subtract(fdt).TotalMinutes);
	}

	public static long BetweenSeconds(DateTime fdt, DateTime tdt)
	{
		return (long)Math.Floor(tdt.Subtract(fdt).TotalSeconds);
	}

	public static long BetweenMilliseconds(DateTime fdt, DateTime tdt)
	{
		return (long)Math.Floor(tdt.Subtract(fdt).TotalMilliseconds);
	}
}
