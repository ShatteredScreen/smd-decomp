using System;
using System.Text;
using UnityEngine;

public class AgeConfirmationDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(string birthday);

	private const int INPUT_COUNT = 3;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private ConfirmDialog mConfirmDialog;

	private AgeReConfirmationDialog mAgeReConfirmationDialog;

	private string mYeardata;

	private string mMonthdata;

	private string mDaydata;

	public int mYearInt;

	public int mMonthInt;

	public int mDayint;

	private UIInput[] mInputs = new UIInput[3];

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("AgeConfimation_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		titleDescKey = string.Format(Localization.Get("AgeConfimation_Desc00"));
		UILabel uILabel = Util.CreateLabel("Desc0", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 174f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.spacingY = 10;
		titleDescKey = string.Format(Localization.Get("AgeConfimation_Desc01"));
		uILabel = Util.CreateLabel("Desc1", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 96f, 0f), 17, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.spacingY = 8;
		int num = 0;
		mInputs[num] = Util.CreateInput("Input_" + num, base.gameObject);
		Util.SetInputInfo(mInputs[num], uIFont, 24, 4, image, "instruction_panel2", mBaseDepth + 1, new Vector3(-161f, 29f, 0f), Vector3.one, 120, 60, UIWidget.Pivot.Center, 0f, "1900");
		Util.SetInputType(mInputs[num], UIInput.InputType.Standard, UIInput.KeyboardType.NumberPad, UIInput.Validation.Integer);
		titleDescKey = string.Format(Localization.Get("AgeYearDesc"));
		uILabel = Util.CreateLabel("yesrDesc", mInputs[num].gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(88f, 0f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		num++;
		mInputs[num] = Util.CreateInput("Input_" + num, base.gameObject);
		Util.SetInputInfo(mInputs[num], uIFont, 24, 2, image, "instruction_panel2", mBaseDepth + 1, new Vector3(0f, 29f, 0f), Vector3.one, 70, 60, UIWidget.Pivot.Center, 0f, "1");
		Util.SetInputType(mInputs[num], UIInput.InputType.Standard, UIInput.KeyboardType.NumberPad, UIInput.Validation.Integer);
		titleDescKey = string.Format(Localization.Get("AgeMonthDesc"));
		uILabel = Util.CreateLabel("MonthDesc", mInputs[num].gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(64f, -2f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		num++;
		mInputs[num] = Util.CreateInput("Input_" + num, base.gameObject);
		Util.SetInputInfo(mInputs[num], uIFont, 24, 2, image, "instruction_panel2", mBaseDepth + 1, new Vector3(130f, 29f, 0f), Vector3.one, 70, 60, UIWidget.Pivot.Center, 0f, "1");
		Util.SetInputType(mInputs[num], UIInput.InputType.Standard, UIInput.KeyboardType.NumberPad, UIInput.Validation.Integer);
		titleDescKey = string.Format(Localization.Get("AgeDayDesc"));
		uILabel = Util.CreateLabel("DayDesc", mInputs[num].gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(64f, -2f, 0f), 32, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		titleDescKey = string.Format(Localization.Get("AgeConfimation_Desc03"));
		uILabel = Util.CreateLabel("Desc3", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(-212f, -39f, 0f), 22, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Color.red;
		titleDescKey = string.Format(Localization.Get("AgeConfimation_Desc04"));
		uILabel = Util.CreateLabel("Desc4", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(-174f, -90f, 0f), 21, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		titleDescKey = string.Format(Localization.Get("AgeConfimation_Desc05"));
		uILabel = Util.CreateLabel("Desc5", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(-174f, -122f, 0f), 21, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		titleDescKey = string.Format(Localization.Get("AgeConfimation_Desc06"));
		uILabel = Util.CreateLabel("Desc6", base.gameObject, uIFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(-174f, -154f, 0f), 21, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		UIButton button = Util.CreateJellyImageButton("AgeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -226f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnOkPushed", UIButtonMessage.Trigger.OnClick);
		CreateCancelButton("OnCancelPushed");
	}

	private void OnOkPushed()
	{
		if (mState.GetStatus() != 0 || GetBusy())
		{
			return;
		}
		mYeardata = GetInputAge(0);
		mMonthdata = GetInputAge(1);
		mDaydata = GetInputAge(2);
		try
		{
			if (mYeardata == string.Empty)
			{
				mYeardata = "1900";
			}
			if (mMonthdata == string.Empty)
			{
				mMonthdata = "1";
			}
			if (mDaydata == string.Empty)
			{
				mDaydata = "1";
			}
			mYearInt = StringToInt(mYeardata);
			mMonthInt = StringToInt(mMonthdata);
			mDayint = StringToInt(mDaydata);
			DateTime dateTime = new DateTime(mYearInt, mMonthInt, mDayint, 0, 0, 0, DateTimeKind.Local);
			mMonthdata = string.Format("{0:00}", mMonthInt);
			mDaydata = string.Format("{0:00}", mDayint);
		}
		catch
		{
			ErrorDialog();
			return;
		}
		OnInputComp();
	}

	private void OnMapDestroyClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
	}

	private static int CharToInt(char c)
	{
		return c - 48;
	}

	private static int StringToInt(string st)
	{
		int num = 0;
		foreach (char c in st)
		{
			int num2 = CharToInt(c);
			num = num * 10 + num2;
		}
		return num;
	}

	private string GetInputAge(int age)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(mInputs[age].value);
		return stringBuilder.ToString();
	}

	public void OnIdResultDialogClosed()
	{
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mYeardata + mMonthdata + mDaydata);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && !GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void ErrorDialog()
	{
		mState.Reset(STATE.WAIT);
		ConfirmDialog confirmDialog = Util.CreateGameObject("Map_destroy_Dialog", base.gameObject).AddComponent<ConfirmDialog>();
		confirmDialog.SetClosedCallback(OnMapDestroyClosed);
		confirmDialog.Init(Localization.Get("AgeConfimationNG_Title"), Localization.Get("AgeConfimationNG_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mYeardata = string.Empty;
		mMonthdata = string.Empty;
		mDaydata = string.Empty;
	}

	private void OnInputComp()
	{
		if (mYearInt <= 1900)
		{
			ErrorDialog();
			return;
		}
		DateTime now = DateTime.Now;
		if (mYearInt > now.Year)
		{
			ErrorDialog();
			return;
		}
		if (mYearInt == now.Year)
		{
			if (mMonthInt > now.Month)
			{
				ErrorDialog();
				return;
			}
			if (mMonthInt == now.Month && mDayint > now.Day)
			{
				ErrorDialog();
				return;
			}
		}
		mState.Reset(STATE.WAIT);
		mAgeReConfirmationDialog = Util.CreateGameObject("Age2_Dialog", base.gameObject).AddComponent<AgeReConfirmationDialog>();
		mAgeReConfirmationDialog.Init(mYearInt, mMonthInt, mDayint);
		mAgeReConfirmationDialog.SetClosedCallback(OnAgeConfirmationClosed);
	}

	private void OnAgeConfirmationClosed(AgeReConfirmationDialog.SELECT_ITEM item)
	{
		if (item == AgeReConfirmationDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Reset(STATE.MAIN);
			Close();
			return;
		}
		mState.Change(STATE.MAIN);
		mYeardata = string.Empty;
		mMonthdata = string.Empty;
		mDaydata = string.Empty;
	}
}
