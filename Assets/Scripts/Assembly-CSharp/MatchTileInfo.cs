public class MatchTileInfo
{
	public IntVector2 pos;

	public Def.DIR dir;

	public int numV;

	public int numH;

	public MatchTileInfo(IntVector2 _pos, Def.DIR _dir, int _numV, int _numH)
	{
		pos = _pos;
		dir = _dir;
		numV = _numV;
		numH = _numH;
	}
}
