public class SortUtil
{
	public enum SORTKIND
	{
		ELEMENT = 0,
		LEVEL = 1,
		SKILLLEVEL = 2,
		HP = 3,
		ATK = 4,
		DEF = 5,
		RECOVER = 6,
		RARE = 7,
		NEWER = 8,
		PARTYCOST = 9,
		AIUEO = 10,
		INDEX = 11,
		RANK = 12,
		KIND = 13,
		EQUIPE = 14,
		RECCOMEND = 15,
		SERIES = 16,
		CHARACTER = 17,
		SORTKIND_NUM = 18
	}

	public static readonly string[] ButtonKeytable = new string[18]
	{
		"adv_btn_sort_attribute",
		"adv_btn_sort_lv",
		"adv_btn_sort_skilllv",
		"adv_btn_sort_hp",
		"adv_btn_sort_atk",
		"adv_btn_sort_def",
		string.Empty,
		"adv_btn_sort_rarity",
		"adv_btn_sort_aging",
		string.Empty,
		"adv_btn_sort_person",
		"adv_btn_sort_aging",
		string.Empty,
		string.Empty,
		string.Empty,
		string.Empty,
		"adv_btn_sort_series",
		"adv_btn_sort_person"
	};

	public static string GetBtnKey(SORTKIND _sortkind)
	{
		return ButtonKeytable[(int)_sortkind];
	}
}
