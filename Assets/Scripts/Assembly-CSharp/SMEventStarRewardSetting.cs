public class SMEventStarRewardSetting : SMMapPageSettingBase
{
	public int StarNum;

	public int Reward;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteInt(StarNum);
		a_writer.WriteInt(Reward);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		StarNum = a_reader.ReadInt();
		Reward = a_reader.ReadInt();
	}
}
