using System.Collections.Generic;

public class MissionData
{
	public enum SHOWGUAGE
	{
		NONE = 0,
		PUZZLE_END = 1
	}

	public int mID { get; set; }

	public string mName { get; set; }

	public string mExplanation { get; set; }

	public SHOWGUAGE mShowGuage { get; set; }

	public int mOpenID { get; set; }

	public int mMaxStep { get; set; }

	public int mOpenStep { get; set; }

	public List<MissionStep> mSteps { get; set; }

	public MissionData()
	{
		mID = 0;
		mName = string.Empty;
		mExplanation = string.Empty;
		mShowGuage = SHOWGUAGE.NONE;
		mOpenID = 0;
		mMaxStep = 0;
		mOpenStep = 0;
		mSteps = new List<MissionStep>();
	}

	~MissionData()
	{
		mSteps.Clear();
		mSteps = null;
	}

	public void Deserialize(BIJBinaryReader aReader)
	{
		mID = aReader.ReadInt();
		mName = aReader.ReadUTF();
		mExplanation = aReader.ReadUTF();
		mShowGuage = (SHOWGUAGE)aReader.ReadInt();
		mOpenID = aReader.ReadInt();
		mMaxStep = aReader.ReadInt();
		mOpenStep = aReader.ReadInt();
		for (int i = 0; i < mMaxStep; i++)
		{
			MissionStep missionStep = new MissionStep();
			missionStep.mTarget = aReader.ReadInt();
			missionStep.mReward = aReader.ReadInt();
			mSteps.Add(missionStep);
		}
	}

	public MissionData Clone()
	{
		MissionData missionData = new MissionData();
		missionData.mID = mID;
		missionData.mName = string.Copy(mName);
		missionData.mExplanation = string.Copy(mExplanation);
		missionData.mShowGuage = mShowGuage;
		missionData.mOpenID = mOpenID;
		missionData.mMaxStep = mMaxStep;
		missionData.mOpenStep = mOpenStep;
		missionData.mSteps = new List<MissionStep>();
		for (int i = 0; i < mMaxStep; i++)
		{
			MissionStep missionStep = new MissionStep();
			missionStep.mTarget = mSteps[i].mTarget;
			missionStep.mReward = mSteps[i].mReward;
			missionData.mSteps.Add(missionStep);
		}
		return missionData;
	}
}
