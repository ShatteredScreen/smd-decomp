using UnityEngine;

public class TweenDestroyOnFinish : MonoBehaviour
{
	public void OnTweenFinish()
	{
		Object.Destroy(base.gameObject);
	}
}
