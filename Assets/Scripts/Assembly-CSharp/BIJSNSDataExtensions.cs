using System.Collections;
using System.Text;

public static class BIJSNSDataExtensions
{
	public static void ToDump(this object value, StringBuilder builder)
	{
		if (value == null)
		{
			builder.Append("null");
		}
		else if (value.GetType().IsArray)
		{
			ArrayToDump(new ArrayList((ICollection)value), builder);
		}
		else if (value is string)
		{
			builder.Append(value);
		}
		else if (value is char)
		{
			builder.Append(value);
		}
		else if (value is IDictionary)
		{
			DictToDump((IDictionary)value, builder);
		}
		else if (value is IList)
		{
			ArrayToDump((IList)value, builder);
		}
		else if (value is bool && (bool)value)
		{
			builder.Append("true");
		}
		else if (value is bool && !(bool)value)
		{
			builder.Append("false");
		}
		else if (value.GetType().IsPrimitive)
		{
			builder.Append(value);
		}
		else
		{
			builder.Append(value.ToString());
		}
	}

	private static void ArrayToDump(IList value, StringBuilder builder)
	{
		if (value != null && value.Count > 0)
		{
			builder.Append("[");
			int num = 0;
			foreach (object item in value)
			{
				if (num > 0)
				{
					builder.Append(",");
				}
				item.ToDump(builder);
				num++;
			}
			builder.Append("]");
		}
		else
		{
			builder.Append("[]");
		}
	}

	private static void DictToDump(IDictionary value, StringBuilder builder)
	{
		if (value != null && value.Count > 0)
		{
			builder.Append("{");
			int num = 0;
			foreach (IDictionaryEnumerator item in value)
			{
				if (num > 0)
				{
					builder.Append(",");
				}
				item.Key.ToDump(builder);
				builder.Append("=");
				item.Value.ToDump(builder);
			}
			builder.Append("}");
		}
		else
		{
			builder.Append("{}");
		}
	}
}
