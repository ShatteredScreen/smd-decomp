using UnityEngine;

public class UISsPartPDraw : UIWidget
{
	public const float DEPTH_MUL = 0.0001f;

	private Material mMat;

	private Shader mShader;

	public Vector3[] _verts = new Vector3[4];

	public Vector2[] _uvs = new Vector2[4];

	public Vector2[] _uv2s = new Vector2[4];

	public Color[] _colors = new Color[4];

	public new int depth
	{
		get
		{
			return (int)mDepth;
		}
		set
		{
			BIJLog.E("You have to use depthf!!!!!!!!!!!!!!!");
			if (mDepth == (float)value)
			{
				return;
			}
			if (panel != null)
			{
				panel.RemoveWidget(this);
			}
			mDepth = value;
			if (panel != null)
			{
				panel.AddWidget(this);
				if (!Application.isPlaying)
				{
					panel.SortWidgets();
					panel.RebuildAllDrawCalls();
				}
			}
		}
	}

	public override Material material
	{
		get
		{
			return mMat;
		}
		set
		{
			if (mMat != value)
			{
				RemoveFromPanel();
				mShader = null;
				mMat = value;
				MarkAsChanged();
			}
		}
	}

	public override Shader shader
	{
		get
		{
			if (mMat != null)
			{
				return mMat.shader;
			}
			if (mShader == null)
			{
				mShader = Shader.Find("Unlit/Transparent Colored");
			}
			return mShader;
		}
		set
		{
			if (mShader != value)
			{
				RemoveFromPanel();
				mShader = value;
				mMat = null;
				MarkAsChanged();
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();
		useUV2 = true;
	}

	public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Vector2> uv2s, BetterList<Color32> cols)
	{
		verts.Add(_verts[0]);
		verts.Add(_verts[1]);
		verts.Add(_verts[2]);
		verts.Add(_verts[3]);
		uvs.Add(_uvs[0]);
		uvs.Add(_uvs[1]);
		uvs.Add(_uvs[2]);
		uvs.Add(_uvs[3]);
		uv2s.Add(_uv2s[0]);
		uv2s.Add(_uv2s[1]);
		uv2s.Add(_uv2s[2]);
		uv2s.Add(_uv2s[3]);
		cols.Add(_colors[0]);
		cols.Add(_colors[1]);
		cols.Add(_colors[2]);
		cols.Add(_colors[3]);
	}
}
