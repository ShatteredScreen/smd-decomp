using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipsScreen : MonoBehaviour
{
	private enum STATE
	{
		INIT = 0,
		MAIN = 1,
		END = 2,
		END_WAIT = 3
	}

	private const int MAX_DOWNLOAD_PANEL_NUM = 3;

	private const short DOWNLOAD_DISPLAY_PANEL_NUM = 3;

	private const short NORMAL_DISPLAY_PANEL_NUM = 1;

	private static Dictionary<string, string[]> TipsData = new Dictionary<string, string[]>
	{
		{
			"TIPS_000",
			new string[2] { "LoadingTipsNew01", "dl_back01" }
		},
		{
			"TIPS_001",
			new string[2] { "LoadingTipsNew00", "dl_back00" }
		},
		{
			"TIPS_002",
			new string[2] { "LoadingTipsNew02", "dl_back02" }
		},
		{
			"TIPS_003",
			new string[2] { "LoadingTipsNew03", "dl_back03" }
		},
		{
			"TIPS_004",
			new string[2] { "LoadingTipsNew04", "dl_back04" }
		},
		{
			"TIPS_005",
			new string[2] { "LoadingTipsNew05", "dl_back03" }
		},
		{
			"TIPS_006",
			new string[2] { "LoadingTipsNew06", "dl_back01" }
		},
		{
			"TIPS_007",
			new string[2] { "LoadingTipsNew07", "dl_back01" }
		},
		{
			"TIPS_008",
			new string[2] { "LoadingTipsNew08", "dl_back01" }
		},
		{
			"TIPS_009",
			new string[2] { "LoadingTipsNew09", "dl_back09" }
		},
		{
			"TIPS_010",
			new string[2] { "LoadingTipsNew10", "dl_back07" }
		},
		{
			"TIPS_011",
			new string[2] { "LoadingTipsNew11", "dl_back08" }
		},
		{
			"TIPS_012",
			new string[2] { "LoadingTipsNew12", "dl_back01" }
		},
		{
			"TIPS_013",
			new string[2] { "LoadingTipsNew13", "dl_back08" }
		},
		{
			"TIPS_014",
			new string[2] { "LoadingTipsNew14", "dl_back07" }
		},
		{
			"TIPS_015",
			new string[2] { "LoadingTipsNew15", "dl_back05" }
		},
		{
			"TIPS_016",
			new string[2] { "LoadingTipsNew16", "dl_back01" }
		},
		{
			"TIPS_017",
			new string[2] { "LoadingTipsNew17", "dl_back06" }
		}
	};

	private static string[] TipsKey = new string[18]
	{
		"TIPS_006", "TIPS_007", "TIPS_009", "TIPS_010", "TIPS_011", "TIPS_000", "TIPS_001", "TIPS_002", "TIPS_003", "TIPS_004",
		"TIPS_005", "TIPS_008", "TIPS_012", "TIPS_013", "TIPS_014", "TIPS_015", "TIPS_016", "TIPS_017"
	};

	private static string[] TipsAdvKey = new string[0];

	private GameMain mGame;

	private bool mIsInitialized;

	private bool mIsDownloadMode;

	private bool mIsAdvMode;

	private ResImage mBGImage;

	private List<ResImage> mTipsImageList = new List<ResImage>();

	private UIPanel mTipsPanel;

	private UIPanel mTipsPanelBG;

	private UISprite mTipsBGMain;

	private UISprite mTipsBGNext;

	private SlidePanel mTipsSlidePanel;

	private UIAnchor mAnchorTop;

	private UIAnchor mAnchorBottom;

	private Dictionary<string, string[]> mTipsList = new Dictionary<string, string[]>();

	private List<SlidePanelSetting> mTipsSettingList;

	private List<UIWidget> mWidgetList = new List<UIWidget>();

	private float mWidgetAlphaInEnd = 1f;

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private bool mIsDeleteRequest;

	private Util.TriFunc mDeleteTri;

	public bool IsDeleted { get; private set; }

	public void SetDeleteRequest()
	{
		mIsDeleteRequest = true;
	}

	private void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Start()
	{
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			if (mIsInitialized)
			{
				InitComponent();
				mState.Change(STATE.MAIN);
			}
			break;
		case STATE.MAIN:
			if (mIsDeleteRequest)
			{
				if (mTipsSlidePanel != null)
				{
					mTipsSlidePanel.ForceSlideUnable = true;
				}
				mDeleteTri = Util.CreateTriFunc(0f, 90f, Util.TriFunc.MAXFUNC.STOP);
				mState.Reset(STATE.END, true);
			}
			break;
		case STATE.END:
		{
			bool flag = !mDeleteTri.AddDegree(Time.deltaTime * 360f);
			mWidgetAlphaInEnd = 1f - mDeleteTri.Sin();
			for (int i = 0; i < mWidgetList.Count; i++)
			{
				if (mWidgetList[i] != null)
				{
					mWidgetList[i].color = new Color(1f, 1f, 1f, mWidgetAlphaInEnd);
				}
			}
			if (mTipsBGMain != null)
			{
				mTipsBGMain.color = new Color(1f, 1f, 1f, mWidgetAlphaInEnd);
			}
			if (mTipsBGNext != null)
			{
				mTipsBGNext.color = new Color(1f, 1f, 1f, mWidgetAlphaInEnd);
			}
			if (mTipsSlidePanel != null)
			{
				List<SlidePanelObject> slideObjectList = mTipsSlidePanel.GetSlideObjectList();
				for (int j = 0; j < slideObjectList.Count; j++)
				{
					SlidePanelObject slidePanelObject = slideObjectList[j];
					if (!(slidePanelObject != null))
					{
						continue;
					}
					List<string> list = new List<string>(slidePanelObject.mChildren.Keys);
					for (int k = 0; k < list.Count; k++)
					{
						UIWidget uIWidget = slidePanelObject.mChildren[list[k]] as UIWidget;
						if (uIWidget != null)
						{
							uIWidget.color = new Color(1f, 1f, 1f, mWidgetAlphaInEnd);
						}
					}
				}
			}
			if (!flag)
			{
				IsDeleted = true;
				mState.Reset(STATE.END_WAIT, true);
			}
			break;
		}
		}
		mState.Update();
	}

	private void InitComponent()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		mTipsPanel = Util.CreateGameObject("TipsPanel", base.gameObject).AddComponent<UIPanel>();
		mTipsPanel.renderQueue = UIPanel.RenderQueue.Explicit;
		mTipsPanel.depth = 102;
		mTipsPanel.startingRenderQueue = 4002;
		mTipsPanel.sortingOrder = 102;
		mTipsPanelBG = Util.CreateGameObject("TipsPanelBG", base.gameObject).AddComponent<UIPanel>();
		mTipsPanelBG.renderQueue = UIPanel.RenderQueue.Explicit;
		mTipsPanelBG.depth = 101;
		mTipsPanelBG.startingRenderQueue = 4001;
		mTipsPanelBG.sortingOrder = 101;
		mBGImage = ResourceManager.LoadImage("DL_BACKGROUND");
		mAnchorTop = Util.CreateAnchor("TipsAnchorTop", mTipsPanel.gameObject, UIAnchor.Side.Top, mGame.mCamera);
		mAnchorBottom = Util.CreateAnchor("TipsAnchorBottom", mTipsPanel.gameObject, UIAnchor.Side.Bottom, mGame.mCamera);
		float num = Util.LogLongSideScreenSize();
		UISprite uISprite = Util.CreateSprite("CharmTop", mAnchorTop.gameObject, mBGImage.Image);
		Util.SetSpriteInfo(uISprite, "lace00", 35, Vector3.zero, Vector3.one, false);
		uISprite.pivot = UIWidget.Pivot.Bottom;
		uISprite.transform.localPosition = Vector3.zero;
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions((int)num + 2, 54);
		uISprite.transform.localScale = new Vector3(1f, -1f, 1f);
		mWidgetList.Add(uISprite);
		uISprite = Util.CreateSprite("CharmBottom", mAnchorBottom.gameObject, mBGImage.Image);
		Util.SetSpriteInfo(uISprite, "lace00", 35, Vector3.zero, Vector3.one, false);
		uISprite.pivot = UIWidget.Pivot.Bottom;
		uISprite.transform.localPosition = Vector3.zero;
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions((int)num + 2, 54);
		mWidgetList.Add(uISprite);
		mTipsSlidePanel = Util.CreateGameObject("SlidePanel", base.gameObject).AddComponent<SlidePanel>();
		mTipsSlidePanel.Init(103, 500f, 650f);
		mTipsSlidePanel.SetMoveInitCallback(OnMoveInit);
		mTipsSlidePanel.SetMoveUpdateCallback(OnMoveUpdate);
		mTipsSlidePanel.SetMoveFinishCallback(OnMoveFinished);
		mTipsSlidePanel.SetCursorSettings("btn_banner02", mBGImage.Image, mTipsPanelBG.gameObject);
		mTipsSlidePanel.SetOrbSettings("btn_banner01", mBGImage.Image, mTipsPanelBG.gameObject);
		mTipsSlidePanel.SetOrbButtonEnable(false);
		mTipsSlidePanel.mLoopEnable = true;
		mTipsSlidePanel.mAutoMoveEnable = true;
		TipsPanelSetting tipsPanelSetting = null;
		List<string> list = new List<string>(mTipsList.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			string a_key = list[i];
			tipsPanelSetting = new TipsPanelSetting(a_key, 500, 650);
			tipsPanelSetting.SetLoadCallback(LoadTipsPanel);
			tipsPanelSetting.SetUnloadCallback(UnloadTipsPanel);
			tipsPanelSetting.SetLayoutCallback(LayoutTipsPanel);
			mTipsSlidePanel.AddSlideWindowPanel(tipsPanelSetting);
		}
		mTipsSettingList = mTipsSlidePanel.SettingList;
		tipsPanelSetting = mTipsSettingList[0] as TipsPanelSetting;
		string tipsKey = tipsPanelSetting.TipsKey;
		string imageName = TipsData[tipsKey][1];
		mTipsBGMain = Util.CreateSprite("TipsScreenMain", mTipsPanelBG.gameObject, mBGImage.Image);
		Util.SetSpriteInfo(mTipsBGMain, imageName, 29, new Vector3(0f, 0f, 0f), Vector3.one, false);
		mTipsBGMain.SetDimensions((int)num, (int)num);
		mTipsBGMain.type = UIBasicSprite.Type.Sliced;
		mTipsBGMain.color = new Color(1f, 1f, 1f, 1f);
		SlidePanel.log("tips screen InitializeComponent");
		SetLayout(Util.ScreenOrientation);
	}

	public void Init(bool a_downloadmode, bool a_isAdv)
	{
		SlidePanel.log("tips screen init. DL=" + a_downloadmode + " adv=" + a_isAdv);
		mIsDownloadMode = a_downloadmode;
		mIsAdvMode = a_isAdv;
		List<string> list = new List<string>();
		if (mTipsList.Count > 0)
		{
			List<string> list2 = new List<string>(mTipsList.Keys);
			for (int i = 0; i < list2.Count; i++)
			{
				mTipsList.Remove(list2[i]);
			}
			mTipsList.Clear();
		}
		short num = 1;
		if (mIsDownloadMode)
		{
			for (int j = 0; j < 3; j++)
			{
				list.Add(TipsKey[j]);
			}
			num = 3;
		}
		else if (mIsAdvMode)
		{
			for (int k = 0; k < TipsAdvKey.Length; k++)
			{
				if (ResourceManager.EnableLoadImage(TipsAdvKey[k]))
				{
					list.Add(TipsAdvKey[k]);
				}
			}
		}
		else
		{
			for (int l = 0; l < TipsKey.Length; l++)
			{
				if (ResourceManager.EnableLoadImage(TipsKey[l]))
				{
					list.Add(TipsKey[l]);
				}
			}
		}
		if (list.Count < num)
		{
			int num2 = 0;
			while (list.Count < num)
			{
				string item = TipsKey[num2];
				if (!list.Contains(item))
				{
					list.Add(TipsKey[num2]);
				}
				num2++;
			}
		}
		int num3 = list.Count;
		while (num3 > 1)
		{
			num3--;
			int index = Random.Range(0, num3);
			string value = list[index];
			list[index] = list[num3];
			list[num3] = value;
		}
		if (list.Count > num)
		{
			list.RemoveRange(0, list.Count - num);
		}
		if (mIsDownloadMode)
		{
			int num4 = list.IndexOf(TipsKey[0]);
			if (num4 != -1)
			{
				list.RemoveAt(num4);
			}
			else
			{
				list.RemoveAt(0);
			}
			list.Insert(0, TipsKey[0]);
		}
		for (int m = 0; m < list.Count; m++)
		{
			string text = list[m];
			mTipsList.Add(text, TipsData[text]);
			SlidePanel.log("tips screen tips=" + text);
		}
		SlidePanel.log("tips screen initialized.");
		mIsInitialized = true;
	}

	public bool IsFirstPanelLoaded()
	{
		bool result = true;
		if (mTipsSlidePanel == null)
		{
			result = false;
		}
		else if (mTipsSlidePanel.FirstPanel == null || !mTipsSlidePanel.FirstPanel.mPanelSetting.mIsLoadFinished)
		{
			result = false;
		}
		return result;
	}

	public void Delete()
	{
		if (mTipsSlidePanel != null)
		{
			mTipsSlidePanel.Delete();
			mTipsSlidePanel = null;
		}
		if (mTipsPanel != null)
		{
			Object.Destroy(mTipsPanel.gameObject);
			mTipsPanel = null;
		}
		ResourceManager.UnloadImage("DL_BACKGROUND");
	}

	public void SetLayout(ScreenOrientation o)
	{
		if (!(mTipsSlidePanel != null))
		{
			return;
		}
		if (mIsDownloadMode)
		{
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mTipsSlidePanel.SetOrbPositionY(-335f);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mTipsSlidePanel.SetOrbPositionY(-150f);
				break;
			}
		}
		else
		{
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mTipsSlidePanel.SetOrbPositionY(-335f);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mTipsSlidePanel.SetOrbPositionY(-300f);
				break;
			}
		}
		mTipsSlidePanel.SetLayout(o);
	}

	private IEnumerator LoadTipsPanel(SlidePanelObject a_panel)
	{
		TipsPanelSetting setting = a_panel.mPanelSetting as TipsPanelSetting;
		ResImage resImageTips = ResourceManager.LoadImageAsync(setting.TipsKey);
		while (resImageTips.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		string descKey = mTipsList[setting.TipsKey][0];
		UISprite sprite3 = null;
		sprite3 = Util.CreateSprite("TipsCharacter", a_panel.gameObject, resImageTips.Image);
		Util.SetSpriteInfo(sprite3, "tips", 110, Vector3.zero, Vector3.one, false);
		a_panel.mChildren.Add("TipsChara", sprite3);
		UIFont font = GameMain.LoadFont();
		sprite3 = Util.CreateSprite("TipsMessageFrame", a_panel.gameObject, mBGImage.Image);
		Util.SetSpriteInfo(sprite3, "loading_panel", 111, Vector3.zero, Vector3.one, false);
		UILabel label = Util.CreateLabel("TipsMessageLabel", sprite3.gameObject, font);
		Util.SetLabelInfo(label, Localization.Get(descKey), 112, Vector3.zero, 24, 0, 0, UIWidget.Pivot.Center);
		label.color = new Color(0.34509805f, 13f / 51f, 8f / 51f);
		label.overflowMethod = UILabel.Overflow.ShrinkContent;
		label.SetDimensions(320, 200);
		a_panel.mChildren.Add("TipsMessageFrame", sprite3);
		a_panel.mChildren.Add("TipsMessage", label);
		LayoutTipsPanel(a_panel, Util.ScreenOrientation);
		SlidePanel.log("tips screen load tips panel=" + setting.TipsKey);
		if (mTipsSlidePanel != null && mTipsSlidePanel.ForceSlideUnable)
		{
			List<string> keys = new List<string>(a_panel.mChildren.Keys);
			for (int i = 0; i < keys.Count; i++)
			{
				UIWidget widget = a_panel.mChildren[keys[i]] as UIWidget;
				if (widget != null)
				{
					widget.color = new Color(1f, 1f, 1f, mWidgetAlphaInEnd);
				}
			}
		}
		setting.mIsLoadFinished = true;
	}

	private void LayoutTipsPanel(SlidePanelObject a_panel, ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			if (a_panel.mChildren.ContainsKey("TipsMessageFrame"))
			{
				a_panel.mChildren["TipsMessageFrame"].transform.localScale = Vector3.one;
				a_panel.mChildren["TipsMessageFrame"].transform.localPosition = new Vector3(0f, -200f, 0f);
			}
			if (a_panel.mChildren.ContainsKey("TipsChara"))
			{
				a_panel.mChildren["TipsChara"].transform.localScale = Vector3.one;
				a_panel.mChildren["TipsChara"].transform.localPosition = new Vector3(0f, 120f, 0f);
			}
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			if (mIsDownloadMode)
			{
				if (a_panel.mChildren.ContainsKey("TipsMessageFrame"))
				{
					a_panel.mChildren["TipsMessageFrame"].transform.localScale = new Vector3(0.7f, 0.7f, 1f);
					a_panel.mChildren["TipsMessageFrame"].transform.localPosition = new Vector3(0f, -80f, 0f);
				}
				if (a_panel.mChildren.ContainsKey("TipsChara"))
				{
					a_panel.mChildren["TipsChara"].transform.localScale = new Vector3(0.7f, 0.7f, 1f);
					a_panel.mChildren["TipsChara"].transform.localPosition = new Vector3(0f, 130f, 0f);
				}
			}
			else
			{
				if (a_panel.mChildren.ContainsKey("TipsMessageFrame"))
				{
					a_panel.mChildren["TipsMessageFrame"].transform.localScale = Vector3.one;
					a_panel.mChildren["TipsMessageFrame"].transform.localPosition = new Vector3(0f, -194f, 0f);
				}
				if (a_panel.mChildren.ContainsKey("TipsChara"))
				{
					a_panel.mChildren["TipsChara"].transform.localScale = Vector3.one;
					a_panel.mChildren["TipsChara"].transform.localPosition = new Vector3(0f, 88f, 0f);
				}
			}
			break;
		}
	}

	private void UnloadTipsPanel(SlidePanelObject a_panel)
	{
		TipsPanelSetting tipsPanelSetting = a_panel.mPanelSetting as TipsPanelSetting;
		SlidePanel.log("tips screen unload tips panel=" + tipsPanelSetting.TipsKey);
		ResourceManager.UnloadImage(tipsPanelSetting.TipsKey);
	}

	private void OnMoveInit(int currentIndex, int targetIndex, int startPosX, int targetPosX)
	{
		TipsPanelSetting tipsPanelSetting = mTipsSettingList[targetIndex] as TipsPanelSetting;
		string tipsKey = tipsPanelSetting.TipsKey;
		string text = TipsData[tipsKey][1];
		float num = Util.LogLongSideScreenSize();
		SlidePanel.log("target id=" + targetIndex);
		SlidePanel.log("nexttips bg=" + text);
		mTipsBGNext = Util.CreateSprite("TipsScreenSub", mTipsPanelBG.gameObject, mBGImage.Image);
		Util.SetSpriteInfo(mTipsBGNext, text, 30, new Vector3(0f, 0f, 0f), Vector3.one, false);
		mTipsBGNext.SetDimensions((int)num + 2, (int)num + 2);
		mTipsBGNext.type = UIBasicSprite.Type.Sliced;
		float a = mTipsBGNext.color.a;
		float a2 = mTipsBGMain.color.a;
		if (mTipsSlidePanel != null && !mTipsSlidePanel.ForceSlideUnable)
		{
			a = ((currentIndex != targetIndex) ? 0f : 1f);
			a2 = 1f;
		}
		mTipsBGNext.color = new Color(1f, 1f, 1f, a);
		mTipsBGMain.color = new Color(1f, 1f, 1f, a2);
	}

	private void OnMoveUpdate(int currentIndex, int targetIndex, int moveX, int startX, int targetX, int slidedirecttion)
	{
		if (currentIndex == targetIndex)
		{
			mTipsBGNext.color = new Color(1f, 1f, 1f, 1f);
			return;
		}
		float a = ((float)moveX - (float)startX) / ((float)targetX - (float)startX);
		if (mTipsBGNext != null)
		{
			mTipsBGNext.color = new Color(1f, 1f, 1f, a);
		}
	}

	private void OnMoveFinished(int currentIndex)
	{
		if (mTipsBGMain != null)
		{
			Object.Destroy(mTipsBGMain.gameObject);
			mTipsBGMain = null;
		}
		mTipsBGMain = mTipsBGNext;
		mTipsBGMain.name = "TipsScreenMain";
		mTipsBGNext = null;
	}
}
