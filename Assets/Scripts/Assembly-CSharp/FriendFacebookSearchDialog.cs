using System;
using System.Collections.Generic;
using UnityEngine;

public class FriendFacebookSearchDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3,
		NETWORK_FRIENDS_RELOAD00 = 4,
		NETWORK_FRIENDS_RELOAD01 = 5,
		NETWORK_FRIENDS_RELOAD02 = 6,
		NETWORK_FRIENDS_RELOAD03 = 7
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private IdSearchDialog mIdSearchDialog;

	private UIInput mInputs;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private FacebookFriendDialog mFacebookFriendDialog;

	private string mIDdata;

	private int mSNSCallCount;

	private SNSFriendManager mFriendManager;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				if (mSNS.Kind == SNS.Facebook)
				{
					mState.Reset(STATE.NETWORK_FRIENDS_RELOAD00, true);
				}
				else
				{
					DoSNS();
				}
			}, null);
			break;
		}
		case STATE.NETWORK_FRIENDS_RELOAD00:
			Server.ManualConnecting = true;
			mFriendManager = new SNSFriendManager(mGame, SNSFriendManager.MODE.LITE);
			if (!mFriendManager.Start())
			{
				mState.Change(STATE.NETWORK_FRIENDS_RELOAD02);
			}
			else
			{
				mState.Change(STATE.NETWORK_FRIENDS_RELOAD01);
			}
			break;
		case STATE.NETWORK_FRIENDS_RELOAD01:
			if (mFriendManager == null || mFriendManager.IsFinished())
			{
				mState.Change(STATE.NETWORK_FRIENDS_RELOAD02);
			}
			else
			{
				mFriendManager.Update();
			}
			break;
		case STATE.NETWORK_FRIENDS_RELOAD02:
			if (mFriendManager != null && mFriendManager.IsDataModified)
			{
				mGame.Save();
			}
			if (mFriendManager != null && mFriendManager.NewAddedGameFriendCount > 0)
			{
				int newAddedGameFriendCount = mFriendManager.NewAddedGameFriendCount;
			}
			Server.ManualConnecting = false;
			mState.Change(STATE.NETWORK_FRIENDS_RELOAD03);
			break;
		case STATE.NETWORK_FRIENDS_RELOAD03:
			mState.Change(STATE.WAIT);
			FriendList();
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("Facebook_Search");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("Input", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 4, new Vector3(0f, 147f, 0f), Vector3.one, false);
		uISprite.SetDimensions(500, 60);
		uISprite.type = UIBasicSprite.Type.Sliced;
		UISprite uISprite2 = Util.CreateSprite("FacebookIcon", uISprite.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "icon_sns00", mBaseDepth + 4, new Vector3(-230f, 0f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(57, 57);
		if (IsFacebookLogined())
		{
			titleDescKey = mGame.mOptions.FbName;
			UILabel uILabel = Util.CreateLabel("FaceBookDesk", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 6, new Vector3(10f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(400, 28);
		}
		float num = 16f;
		float num2 = -95f;
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite, SNS.Facebook))
		{
			UIButton uIButton = Util.CreateJellyImageButton("IDsearchButton", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, num, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "DoSNS", UIButtonMessage.Trigger.OnClick);
			titleDescKey = string.Format(Localization.Get("Friend_Invite_Desk"));
			UILabel uILabel = Util.CreateLabel("IDSearchDesk", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			num += num2;
		}
		if (GameMain.IsSNSFunctionEnabled(SNS_FLAG.FriendSearch) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.FriendSearch, SNS.Facebook))
		{
			UIButton uIButton = Util.CreateJellyImageButton("FacebookButton", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, num, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "FriendRequest", UIButtonMessage.Trigger.OnClick);
			titleDescKey = string.Format(Localization.Get("Friend_Request"));
			UILabel uILabel = Util.CreateLabel("FacebookSearchDesk", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(10f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		}
		num += num2;
		CreateCancelButton("OnCancelPushed");
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private void FriendRequest()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.NETWORK_FRIENDS_RELOAD00);
		}
	}

	private void FriendList()
	{
		if (BuildDialogList(mGame.FaceBookUserList))
		{
			mFacebookFriendDialog = Util.CreateGameObject("FacebookFriendDialog", base.gameObject).AddComponent<FacebookFriendDialog>();
			mFacebookFriendDialog.Init();
			mFacebookFriendDialog.SetClosedCallback(mFacebookFriendDialogClosed);
		}
		else if (!mFriendManager.mInNetwork)
		{
			mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("FB_Friend_ErrorType00_Title"), Localization.Get("FB_Friend_ErrorType00_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		}
		else if (!mFriendManager.mFBfriend)
		{
			mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("ID_Search_NotFound_Title"), Localization.Get("ID_Search_NotFound"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		}
		else
		{
			mConfirmDialog = Util.CreateGameObject("ErrorDialog", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("FB_Friend_ErrorType01_Title"), Localization.Get("FB_Friend_ErrorType01_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		}
	}

	private bool BuildDialogList(List<MyFaceBookFriend> a_list)
	{
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		if (a_list.Count == 0)
		{
			return false;
		}
		list.Clear();
		for (int i = 0; i < a_list.Count; i++)
		{
			MyFaceBookFriend myFaceBookFriend = a_list[i];
			if (myFaceBookFriend.UUID != mGame.mPlayer.UUID && !mGame.mPlayer.Friends.ContainsKey(myFaceBookFriend.UUID) && !mGame.mPlayer.ApplyFriends.ContainsKey(myFaceBookFriend.UUID))
			{
				list.Add(myFaceBookFriend);
			}
		}
		if (list.Count == 0)
		{
			return false;
		}
		return true;
	}

	private void mFacebookFriendDialogClosed(bool a_isEmpty)
	{
		mState.Change(STATE.MAIN);
		if (a_isEmpty)
		{
			mGame.Save();
		}
	}

	private void DoSNS()
	{
		if (mState.GetStatus() != 0)
		{
			return;
		}
		BIJSNS _sns = SocialManager.Instance[SNS.Facebook];
		mSNSCallCount++;
		try
		{
			mState.Reset(STATE.WAIT, true);
			mSNS = _sns;
			if (GameMain.IsSNSNeedLogin(_sns))
			{
				if (mSNSCallCount > 1)
				{
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.NETWORK_LOGIN00);
				}
				return;
			}
			IBIJSNSPostData postData = GetPostData(_sns);
			_sns.Invite(postData, delegate(BIJSNS.Response _res, object _userdata)
			{
				if (_res != null && _res.result == 0)
				{
					if (GameMain.ProcessInviteResult(_sns, _res.detail as IBIJSNSPostData) > 0)
					{
						mGame.AchievementFriendInvite();
					}
					if (SNSSuccess(_sns))
					{
						return;
					}
				}
				else if (SNSError(_sns))
				{
					return;
				}
				mState.Change(STATE.MAIN);
			}, null);
		}
		catch (Exception)
		{
			mState.Change(STATE.MAIN);
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("Invite_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("Invite_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private bool SNSSuccess(BIJSNS _sns)
	{
		return false;
	}

	private IBIJSNSPostData GetPostData(BIJSNS _sns)
	{
		BIJSNSPostData bIJSNSPostData = new BIJSNSPostData();
		string text = ((!string.IsNullOrEmpty(Constants.INVITE_URL)) ? ("\n" + Constants.INVITE_URL) : string.Empty);
		string iNVITE_CODE = Constants.INVITE_CODE;
		string empty = string.Empty;
		string fileName = "SnoopyDrops.png";
		List<string> inviteExcludeIds = GameMain.GetInviteExcludeIds(_sns);
		switch (_sns.Kind)
		{
		case SNS.Facebook:
		case SNS.GooglePlus:
		{
			string linkURL = "\n" + string.Format(Constants.POST_URL, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = empty;
			bIJSNSPostData.Title = string.Format(Localization.Get("Invite_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("Invite_Contents_Desc"), string.Empty, string.Empty);
			bIJSNSPostData.LinkURL = linkURL;
			bIJSNSPostData.FileName = fileName;
			bIJSNSPostData.ExcludeIds = inviteExcludeIds;
			break;
		}
		default:
		{
			string arg = "\n" + string.Format(Constants.STORE_URL_IOS, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = empty;
			bIJSNSPostData.Title = string.Format(Localization.Get("Invite_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("Invite_Contents_Desc"), arg, "\n" + Constants.STORE_URL_AD);
			bIJSNSPostData.LinkURL = string.Empty;
			bIJSNSPostData.FileName = fileName;
			bIJSNSPostData.ExcludeIds = inviteExcludeIds;
			break;
		}
		}
		return bIJSNSPostData;
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
