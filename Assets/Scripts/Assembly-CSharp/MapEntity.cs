using System;
using System.Collections.Generic;
using UnityEngine;

public class MapEntity : ScSpriteObject
{
	public enum STATE
	{
		STAY = 0,
		ON = 1,
		OFF = 2,
		WAIT = 3,
		UNLOCK_WAIT = 4,
		DISPPEAR = 5
	}

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.STAY);

	protected GameMain mGame;

	protected Camera mCamera;

	public BIJImage mAtlas;

	public string DefaultAnimeKey = string.Empty;

	public string AnimeKey = string.Empty;

	public string CurrentAnimeKey = string.Empty;

	protected Vector3 mScale = Vector3.one;

	public bool HasTapAnime;

	public bool IsLockTapAnime;

	protected Dictionary<string, string> mChangeParts;

	protected bool mForceAnimeFinished;

	protected bool mCallCallback;

	protected float mStateTime;

	protected bool mDisappearFinish;

	public override void Awake()
	{
		base.Awake();
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mCamera = mGame.mCamera;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (_sprite.IsAnimationFinished())
		{
			PlayAnime(AnimeKey, true);
		}
		switch (mState.GetStatus())
		{
		case STATE.ON:
			if (!mState.IsChanged() && !mGame.mTouchCnd)
			{
				Vector3 position = new Vector3(mGame.mTouchPos.x, mGame.mTouchPos.y, 0f);
				mCallCallback = _sprite.ContainsPoint(mCamera.ScreenToWorldPoint(position), true);
				mState.Change(STATE.OFF);
			}
			break;
		case STATE.OFF:
			if (!mState.IsChanged())
			{
				mState.Change(STATE.STAY);
			}
			break;
		case STATE.DISPPEAR:
		{
			if (mState.IsChanged())
			{
				mStateTime = 0f;
				break;
			}
			mStateTime += Time.deltaTime * 540f;
			float x = 1f - Mathf.Sin(mStateTime * ((float)Math.PI / 180f));
			float num = Mathf.Sin(mStateTime * ((float)Math.PI / 180f)) * 30f;
			if (mStateTime > 90f)
			{
				mDisappearFinish = true;
				x = 0f;
				NGUITools.SetActive(base.gameObject, false);
			}
			base.gameObject.transform.localScale = new Vector3(x, base.gameObject.transform.localScale.y, base.gameObject.transform.localScale.z);
			base.gameObject.transform.localPosition = new Vector3(base.gameObject.transform.localPosition.x, base.gameObject.transform.localPosition.y + num, base.gameObject.transform.localPosition.z);
			break;
		}
		}
		mState.Update();
	}

	public void Init(int _counter, string _name, BIJImage atlas, float x, float y, float z, Vector3 scale)
	{
		DefaultAnimeKey = _name;
		AnimeKey = _name;
		mAtlas = atlas;
		mScale = scale;
		PlayAnime(AnimeKey, true);
		base._pos = new Vector3(x, y, z);
	}

	protected virtual void PlayAnime(string animeName, bool loop, bool force = false)
	{
		int playCount = ((!loop) ? 1 : 0);
		ChangeAnime(animeName, mChangeParts, force, playCount);
		_sprite.transform.localScale = mScale;
	}

	public void SetColor(Color col)
	{
		if (!(_sprite == null))
		{
			for (int i = 0; i < _sprite._colors.Length; i++)
			{
				_sprite._colors[i] = col;
			}
			_sprite._mesh.colors = _sprite._colors;
		}
	}

	public void SetAlpha(float a_alpha)
	{
		if (!(_sprite == null))
		{
			SsPart[] parts = _sprite.GetParts();
			for (int i = 0; i < parts.Length; i++)
			{
				parts[i].ForceAlpha(a_alpha);
			}
		}
	}

	public bool ContainsPoint(Vector3 worldPos)
	{
		if (!HasTapAnime)
		{
			return false;
		}
		bool flag = _sprite.ContainsPoint(worldPos, true);
		if (flag)
		{
			mState.Change(STATE.ON);
		}
		return flag;
	}

	public bool ChangeAnime(string animePath, Dictionary<string, string> changeParts, bool force, int playCount)
	{
		if (mAtlas == null)
		{
			return false;
		}
		SsAnimation ssAnime = ResourceManager.LoadSsAnimation(animePath).SsAnime;
		if (ssAnime == null)
		{
			_sprite.Animation = null;
			return true;
		}
		if (force)
		{
		}
		CurrentAnimeKey = animePath;
		if (changeParts != null)
		{
			SsPartRes[] partList = ssAnime.PartList;
			for (int i = 0; i < partList.Length; i++)
			{
				string value;
				if (!partList[i].IsRoot && changeParts.TryGetValue(partList[i].Name, out value))
				{
					string text = value;
					Vector2 offset = mAtlas.GetOffset(text);
					Vector2 size = mAtlas.GetSize(text);
					float x = offset.x;
					float y = offset.y;
					float x2 = offset.x + size.x;
					float y2 = offset.y + size.y;
					partList[i].UVs = new Vector2[4]
					{
						new Vector2(x, y2),
						new Vector2(x2, y2),
						new Vector2(x2, y),
						new Vector2(x, y)
					};
					Vector4 pixelPadding = mAtlas.GetPixelPadding(text);
					size = mAtlas.GetPixelSize(text);
					offset = new Vector2(partList[i].OriginX, partList[i].OriginY);
					offset.x = size.x / 2f;
					offset.y = size.y / 2f;
					offset.x += pixelPadding.x;
					offset.y += pixelPadding.y;
					size.x -= pixelPadding.x + pixelPadding.z;
					size.y -= pixelPadding.y + pixelPadding.w;
					x = 0f - offset.x;
					y = 0f - (size.y - offset.y);
					x2 = size.x - offset.x;
					y2 = offset.y;
					float z = 0f;
					partList[i].OrgVertices = new Vector3[4]
					{
						new Vector3(x, y2, z),
						new Vector3(x2, y2, z),
						new Vector3(x2, y, z),
						new Vector3(x, y, z)
					};
				}
			}
		}
		CleanupAnimation();
		_sprite.Animation = ssAnime;
		_sprite.UpdateAlways();
		_sprite.Play();
		_sprite.PlayCount = playCount;
		_sprite.AnimationFinished = OnAnimationFinished;
		BoxCollider component = base.gameObject.GetComponent<BoxCollider>();
		if (component != null)
		{
			UnityEngine.Object.DestroyImmediate(component);
			component = base.gameObject.AddComponent<BoxCollider>();
		}
		return true;
	}

	public void Celebrate()
	{
		if (HasTapAnime)
		{
			PlayAnime(AnimeKey + "_TAP", false);
		}
	}

	public virtual void OnAnimationFinished(SsSprite sprite)
	{
	}

	public void StartDisappear()
	{
		mDisappearFinish = false;
		mState.Change(STATE.DISPPEAR);
	}

	public bool DisappearFinished()
	{
		return mDisappearFinish;
	}
}
