using UnityEngine;

public class ParticleOnSSAnime : MonoBehaviour
{
	public delegate void OnEmitterDestroy();

	private float mAngle;

	public ParticleSystem mParticle;

	private SsSprite mTargetAnime;

	private float mDestroyTimer;

	private UIRoot uiRoot;

	private float mOfsZ = -2f;

	private string mTargetLayerName = string.Empty;

	private float mOfsY;

	private OnEmitterDestroy mEmitterDestroy;

	private void OnDestroy()
	{
		Object.Destroy(mParticle.gameObject);
		mParticle = null;
	}

	private void Start()
	{
		mAngle = 0f;
	}

	private void Update()
	{
		if (!(mParticle != null))
		{
			return;
		}
		UpdatePosition();
		if (!mParticle.IsAlive(true))
		{
			if (mEmitterDestroy != null)
			{
				mEmitterDestroy();
			}
			mTargetAnime = null;
		}
	}

	private void UpdatePosition()
	{
		if (mTargetAnime == null)
		{
			return;
		}
		uiRoot = SingletonMonoBehaviour<GameMain>.Instance.mRoot;
		SsAnimation animation = mTargetAnime.Animation;
		SsPartRes[] partList = animation.PartList;
		for (int i = 0; i < partList.Length; i++)
		{
			if (!partList[i].IsRoot && partList[i].Name.CompareTo(mTargetLayerName) == 0 && mParticle != null)
			{
				mParticle.transform.position = mTargetAnime.transform.position;
			}
		}
	}

	public void Init(SsSprite target, string particleName, float ofsZ = -2f, string a_layerName = "particleEmitter", GameObject go = null, float a_offset = 0f)
	{
		mTargetAnime = target;
		mDestroyTimer = 0f;
		mOfsY = a_offset;
		mOfsZ = ofsZ;
		mTargetLayerName = a_layerName;
		if (go != null)
		{
			base.gameObject.layer = go.layer;
		}
		mParticle = GameMain.GetParticle(particleName).GetComponent<ParticleSystem>();
		mParticle.gameObject.transform.parent = base.transform;
		mParticle.gameObject.layer = base.gameObject.layer;
		mParticle.gameObject.transform.localScale = Vector3.one;
		float startSize = mParticle.startSize;
		float startSize2 = mParticle.startSize;
		switch (Util.ScreenOrientation)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			startSize2 = startSize;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			startSize2 = startSize * SingletonMonoBehaviour<GameMain>.Instance.mDeviceRatioScale * 1.5f;
			break;
		}
		mParticle.startSize = startSize2;
		UpdatePosition();
		Play();
	}

	public void SetEmitterDestroyCallback(OnEmitterDestroy a_callback)
	{
		mEmitterDestroy = a_callback;
	}

	public void Play()
	{
		if (mParticle != null)
		{
			mParticle.Play();
		}
	}

	public void Stop()
	{
		if (mParticle != null)
		{
			mParticle.Stop();
		}
	}
}
