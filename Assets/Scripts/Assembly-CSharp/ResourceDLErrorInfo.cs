using System;
using System.Reflection;

public class ResourceDLErrorInfo
{
	public enum ERROR_CODE
	{
		NO_ERROR = 0,
		CONNECTION = 1000,
		CONNECTION_TIMEOUT = 1001,
		CONNECTION_TIMEOUT_FATAL = 1002,
		CONNECTION_CANCELED = 1003,
		CONNECTION_TIMEOUT_BACK = 1004,
		CONNECTION_ERROR_MAX = 1999,
		SERVER_ACCESS = 2000,
		SERVER_ACCESS_ERROR_MAX = 2999,
		MEMORY_ACCESS = 3000,
		MEMORY_ACCESS_ERROR_MAX = 3999,
		LOW_DISK_SPACE = 4000,
		LOW_DISK_SPACE_ERROR_MAX = 4999,
		ASSET_LOAD = 5000,
		ASSET_LOAD_ERROR_MAX = 5999,
		OTHER = 6000,
		NO_DLLIST = 6001,
		NO_DLMANAGER = 6002,
		NO_COMPLETE_DL = 6003,
		NO_SERVERDLLIST = 6004,
		OTHER_ERROR_MAX = 6999,
		MAX_ERROR_CODE = 7000
	}

	public enum PLACE
	{
		GAMESTATEPRETITLE = 0,
		DLMANAGER_MAIN = 100,
		DLMANAGER_THREADMAIN = 101,
		DLMANAGER_ONRESPONSECALLBACK = 102,
		DLMANAGER_ONREADCALLBACK = 103,
		DLMANAGER_ONTIMEOUTCALLBACK = 104,
		DLMANAGER_ONGETRESOURCEDOWNLOAD = 105,
		DLMANAGER_ONRESOURCECALLBACK = 106
	}

	public enum AFTER_ACTION
	{
		RETRY = 0,
		STOP_RETURN = 1,
		APPLICATION_QUIT = 2
	}

	public ERROR_CODE ErrorCode;

	public string Name;

	public string URL;

	public int SerialNo;

	public string CRC;

	public string SceneNameRaw;

	public bool NeedUnzip;

	public string Query;

	public string ErrorTitle;

	public string ErrorMessage;

	public ResourceDLErrorInfo(BIJDLData a_data, ERROR_CODE a_code, PLACE _place, string a_title, string a_message)
	{
		ErrorCode = a_code;
		SetMessage(_place, a_title, a_message);
		if (a_data != null)
		{
			URL = a_data.URL;
			CRC = a_data.CRC;
			NeedUnzip = a_data.NeedUnZip;
			Query = a_data.Query;
		}
	}

	public ResourceDLErrorInfo(BIJDLData a_data, ERROR_CODE a_code, PLACE a_place, string a_title, Exception a_e)
		: this(a_data, a_code, a_place, string.Empty, string.Empty)
	{
		SetMessage(a_place, a_title, string.Empty);
	}

	public string GetMessage()
	{
		return ErrorMessage;
	}

	public bool IsAssetLoadError()
	{
		bool result = false;
		if (ErrorCode >= ERROR_CODE.ASSET_LOAD && ErrorCode <= ERROR_CODE.ASSET_LOAD_ERROR_MAX)
		{
			result = true;
		}
		return result;
	}

	public AFTER_ACTION GetAfterAction()
	{
		return AFTER_ACTION.RETRY;
	}

	public void SetMessage(PLACE a_place, string a_title, string a_message)
	{
		ErrorTitle = a_title;
		ErrorMessage = a_message;
		int num = (int)a_place;
		string text = num.ToString("D3");
		if (ErrorCode >= ERROR_CODE.CONNECTION && ErrorCode < ERROR_CODE.SERVER_ACCESS)
		{
			if (ErrorTitle == string.Empty)
			{
				ErrorTitle = Localization.Get("ResourceDL_ErrorConnect_Title");
			}
			if (ErrorMessage == string.Empty)
			{
				ErrorMessage = string.Format(Localization.Get("ResourceDL_ErrorConnect"), text + (int)ErrorCode);
			}
		}
		else if (ErrorCode >= ERROR_CODE.SERVER_ACCESS && ErrorCode < ERROR_CODE.MEMORY_ACCESS)
		{
			if (ErrorTitle == string.Empty)
			{
				ErrorTitle = Localization.Get("ResourceDL_ErrorServerAccess_Title");
			}
			if (ErrorMessage == string.Empty)
			{
				ErrorMessage = string.Format(Localization.Get("ResourceDL_ErrorServerAccess"), text + (int)ErrorCode);
			}
		}
		else if (ErrorCode >= ERROR_CODE.MEMORY_ACCESS && ErrorCode < ERROR_CODE.LOW_DISK_SPACE)
		{
			if (ErrorTitle == string.Empty)
			{
				ErrorTitle = Localization.Get("ResourceDL_ErrorMemoryAccess_Title");
			}
			if (ErrorMessage == string.Empty)
			{
				ErrorMessage = string.Format(Localization.Get("ResourceDL_ErrorMemoryAccess"), text + (int)ErrorCode);
			}
		}
		else if (ErrorCode >= ERROR_CODE.LOW_DISK_SPACE && ErrorCode < ERROR_CODE.ASSET_LOAD)
		{
			if (ErrorTitle == string.Empty)
			{
				ErrorTitle = Localization.Get("ResourceDL_ErrorOutOfMemory_Title");
			}
			if (ErrorMessage == string.Empty)
			{
				ErrorMessage = string.Format(Localization.Get("ResourceDL_ErrorOutOfMemory"), text + (int)ErrorCode);
			}
		}
		else if (ErrorCode >= ERROR_CODE.ASSET_LOAD && ErrorCode < ERROR_CODE.OTHER)
		{
			if (ErrorTitle == string.Empty)
			{
				ErrorTitle = Localization.Get("ResourceDL_ErrorDataLoad_Title");
			}
			if (ErrorMessage == string.Empty)
			{
				ErrorMessage = string.Format(Localization.Get("ResourceDL_ErrorDataLoad"), text + (int)ErrorCode);
			}
		}
		else if (ErrorCode >= ERROR_CODE.OTHER && ErrorCode < ERROR_CODE.MAX_ERROR_CODE)
		{
			if (ErrorTitle == string.Empty)
			{
				ErrorTitle = Localization.Get("ResourceDL_ErrorOther_Title");
			}
			if (ErrorMessage == string.Empty)
			{
				ErrorMessage = string.Format(Localization.Get("ResourceDL_ErrorOther"), text + (int)ErrorCode);
			}
		}
	}

	public void CopyTo(ResourceDLErrorInfo _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(ResourceDLErrorInfo);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}
}
