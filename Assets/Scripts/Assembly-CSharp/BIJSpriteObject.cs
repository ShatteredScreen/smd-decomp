using System.Runtime.CompilerServices;
using UnityEngine;

[ExecuteInEditMode]
public class BIJSpriteObject : MonoBehaviour
{
	public enum AnchorFlags : byte
	{
		TopLeft = 0,
		TopCenter = 1,
		TopRight = 2,
		MiddleLeft = 3,
		MiddleCenter = 4,
		MiddleRight = 5,
		BottomLeft = 6,
		BottomCenter = 7,
		BottomRight = 8
	}

	public delegate void EventFunc(int param);

	public bool Clickable;

	[SerializeField]
	public BIJSpriteManager SpriteManager;

	[SerializeField]
	[HideInInspector]
	public AnchorFlags mAnchor = AnchorFlags.MiddleCenter;

	[HideInInspector]
	[SerializeField]
	private int mIndex;

	[SerializeField]
	[HideInInspector]
	private bool mAutoResize = true;

	[SerializeField]
	[HideInInspector]
	private Vector2 mAnchorOffset = new Vector2(0f, 0f);

	[SerializeField]
	[HideInInspector]
	private bool mFlip;

	private Transform mTransform;

	[BIJExposeProperty]
	public int Index
	{
		get
		{
			return mIndex;
		}
		set
		{
			mIndex = value;
			SetModified(true);
		}
	}

	[BIJExposeProperty]
	public bool AutoResize
	{
		get
		{
			return mAutoResize;
		}
		set
		{
			mAutoResize = value;
			SetModified(true);
		}
	}

	[BIJExposeProperty]
	public bool Flip
	{
		get
		{
			return mFlip;
		}
		set
		{
			mFlip = value;
			SetModified(true);
		}
	}

	[BIJExposeProperty]
	public AnchorFlags Anchor
	{
		get
		{
			return mAnchor;
		}
		set
		{
			mAnchor = value;
			SetModified(true);
		}
	}

	[BIJExposeProperty]
	public Vector2 AnchorOffset
	{
		get
		{
			return mAnchorOffset;
		}
		set
		{
			mAnchorOffset = value;
			SetModified(true);
		}
	}

	public Vector3 Position
	{
		get
		{
			return mTransform.position;
		}
		set
		{
			mTransform.position = value;
			SetModified(true);
		}
	}

	public float X
	{
		get
		{
			return Position.x;
		}
		set
		{
			Vector3 position = Position;
			position.x = value;
			Position = position;
		}
	}

	public float Y
	{
		get
		{
			return Position.y;
		}
		set
		{
			Vector3 position = Position;
			position.y = value;
			Position = position;
		}
	}

	public float Z
	{
		get
		{
			return Position.z;
		}
		set
		{
			Vector3 position = Position;
			position.z = value;
			Position = position;
		}
	}

	public Vector3 Rotation
	{
		get
		{
			return mTransform.localRotation.eulerAngles;
		}
		set
		{
			Quaternion identity = Quaternion.identity;
			identity.eulerAngles = value;
			mTransform.localRotation = identity;
			SetModified(true);
		}
	}

	public float Rx
	{
		get
		{
			return Rotation.x;
		}
		set
		{
			Vector3 rotation = Rotation;
			rotation.x = value;
			Rotation = rotation;
		}
	}

	public float Ry
	{
		get
		{
			return Rotation.y;
		}
		set
		{
			Vector3 rotation = Rotation;
			rotation.y = value;
			Rotation = rotation;
		}
	}

	public float Rz
	{
		get
		{
			return Rotation.z;
		}
		set
		{
			Vector3 rotation = Rotation;
			rotation.z = value;
			Rotation = rotation;
		}
	}

	public Vector3 Scale
	{
		get
		{
			return mTransform.localScale;
		}
		set
		{
			mTransform.localScale = value;
			SetModified(true);
		}
	}

	public float Sx
	{
		get
		{
			return Scale.x;
		}
		set
		{
			Vector3 scale = Scale;
			scale.x = value;
			Scale = scale;
		}
	}

	public float Sy
	{
		get
		{
			return Scale.y;
		}
		set
		{
			Vector3 scale = Scale;
			scale.y = value;
			Scale = scale;
		}
	}

	public float Sz
	{
		get
		{
			return Scale.z;
		}
		set
		{
			Vector3 scale = Scale;
			scale.z = value;
			Scale = scale;
		}
	}

	[method: MethodImpl(32)]
	public event EventFunc OnEventFunc;

	private void Awake()
	{
		mTransform = base.transform;
	}

	private void OnDestroy()
	{
	}

	private void OnEnable()
	{
		SetModified(true);
	}

	private void OnDisable()
	{
		SetModified(true);
	}

	private void SetModified(bool modified)
	{
		if (SpriteManager != null)
		{
			SpriteManager.Modified = modified;
		}
	}

	private void Start()
	{
		if (SpriteManager != null)
		{
			SpriteManager.Resister(this);
		}
	}

	public void OnEvent(int param)
	{
		if (this.OnEventFunc != null)
		{
			this.OnEventFunc(param);
		}
	}
}
