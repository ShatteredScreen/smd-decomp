using UnityEngine;

public class SDConfirmDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		INIT = 2,
		MAINTENANCE_CHECK_WAIT = 3,
		NETWORK_00 = 4,
		NETWORK_00_1 = 5,
		NETWORK_00_2 = 6,
		NETWORK_01 = 7,
		NETWORK_01_1 = 8,
		NETWORK_01_2 = 9,
		AUTHERROR_WAIT = 10
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAINTENANCE_CHECK_WAIT);

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private ConfirmDialog mErrorDialog;

	private UILabel mLabelWindow;

	public STATE StateStatus;

	public override void Start()
	{
		MaintenanceCheckOnStart(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.CLOSE, true);
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		StateStatus = mState.GetStatus();
		switch (mState.GetStatus())
		{
		case STATE.INIT:
		{
			string empty = string.Empty;
			empty = string.Format(Localization.Get("SDConfirm_DescD_En"), mGame.mPlayer.Dollar);
			mLabelWindow.text = empty;
			mState.Change(STATE.MAIN);
			break;
		}
		case STATE.MAINTENANCE_CHECK_WAIT:
			if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
			{
				mState.Change(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00:
			if (!mGame.Network_UserAuth())
			{
				if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					ShowNetworkErrorDialog();
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		case STATE.NETWORK_00_1:
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00_2);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
			}
			else
			{
				ShowNetworkErrorDialog();
			}
			break;
		case STATE.NETWORK_00_2:
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			if (!mGame.Network_GetGemCount())
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.INIT);
				}
				else
				{
					ShowNetworkErrorDialog();
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_01_1);
			}
			break;
		case STATE.NETWORK_01_1:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_01_2);
				}
				else
				{
					ShowNetworkErrorDialog();
				}
			}
			break;
		case STATE.NETWORK_01_2:
			mState.Change(STATE.INIT);
			break;
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int w = 500;
		int h = 110;
		string titleDescKey = Localization.Get("SDConfirm_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("Instruction", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 23f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(510, 100);
		titleDescKey = string.Format(Localization.Get("SDConfirm_DescA"), Constants.HAVE_LIMIT_SD);
		UILabel uILabel = Util.CreateLabel("DescA", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, 80f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.SetDimensions(w, h);
		titleDescKey = string.Empty;
		mLabelWindow = Util.CreateLabel("DescB", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(mLabelWindow, titleDescKey, mBaseDepth + 3, new Vector3(0f, -3f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(mLabelWindow);
		mLabelWindow.SetDimensions(w, h);
		float y = -100f;
		titleDescKey = string.Format(Localization.Get("SDConfirm_Notice_En"), Constants.HAVE_LIMIT_SD);
		uILabel = Util.CreateLabel("Notice", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, y, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.color = Color.red;
		uILabel.SetDimensions(w, h);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAINTENANCE_CHECK_WAIT))
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void ShowNetworkErrorDialog()
	{
		mState.Reset(STATE.WAIT, true);
		mErrorDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mErrorDialog.SetClosedCallback(OnNetworkErrorDialogClosed);
	}

	private void OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}
}
