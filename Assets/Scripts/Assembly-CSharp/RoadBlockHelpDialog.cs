using System;
using System.Collections.Generic;
using UnityEngine;

public class RoadBlockHelpDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_00_00 = 2,
		NETWORK_00_01 = 3
	}

	public enum MODE
	{
		SEND = 0,
		RECEIVE = 1
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private GameObject mDialogCenter;

	private GiftListItem mSelectedGiftItem;

	private SMVerticalListInfo mFriendListInfo;

	private SMVerticalList mFriendList;

	private List<SMVerticalListItem> mList = new List<SMVerticalListItem>();

	private SMRoadBlockSetting mRoadBlockSetting;

	private ConfirmDialog mConfirmDialog;

	private UIButton mTabButtonSend;

	private UIButton mTabButtonReceive;

	public MODE Mode { get; set; }

	public override void Start()
	{
		Server.GiveGiftEvent += Server_OnGiveGift;
		base.Start();
	}

	public override void OnDestroy()
	{
		Server.GiveGiftEvent -= Server_OnGiveGift;
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_00_00:
			OnNetwork_SendGiftSuccess();
			break;
		case STATE.NETWORK_00_01:
			OnNetwork_SendGiftFail();
			break;
		}
		mState.Update();
	}

	public void Init(SMRoadBlockSetting data)
	{
		mRoadBlockSetting = data;
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("RoadBlockHelp_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		titleDescKey = Util.MakeLText("RoadBlockHelp_Desc", mRoadBlockSetting.UnlockFriends);
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, -510f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		mDialogCenter = Util.CreateGameObject("GridRoot", base.gameObject);
		ChangeMode(MODE.SEND);
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void ChangeMode(MODE mode)
	{
		Mode = mode;
		if (mode != 0 || mGame.mPlayer.Friends.Count <= 0)
		{
			return;
		}
		DateTime now = DateTime.Now;
		foreach (KeyValuePair<int, MyFriend> friend in mGame.mPlayer.Friends)
		{
			MyFriend value = friend.Value;
			if (value == null)
			{
				continue;
			}
			int uUID = value.Data.UUID;
			DateTime fdt = DateTimeUtil.UnitLocalTimeEpoch;
			if (mGame.mPlayer.SendInfo.ContainsKey(uUID))
			{
				fdt = mGame.mPlayer.SendInfo[uUID].LastRoadBlockRequestTime;
			}
			bool flag = false;
			if (mGame.mPlayer.Gifts.Count > 0)
			{
				foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
				{
					GiftItem value2 = gift.Value;
					if (value2.GiftItemCategory == Def.ITEM_CATEGORY.ROADBLOCK_RES)
					{
						int itemKind = value2.Data.ItemKind;
						int itemID = value2.Data.ItemID;
						if (itemKind == (int)mRoadBlockSetting.Series && itemID == mRoadBlockSetting.RoadBlockLevel && value2.Data.FromID == value.Data.UUID)
						{
							flag = true;
							break;
						}
					}
				}
			}
			if (!flag && DateTimeUtil.BetweenDays0(fdt, now) >= Constants.SEND_ROADBLOCK_FREQ)
			{
				mList.Add(GiftListItem.Make(null, value, null));
			}
		}
	}

	private void ActivateTab(MODE mode)
	{
		switch (mode)
		{
		case MODE.SEND:
			Util.SetImageButtonGraphic(mTabButtonSend, "61", "61", "61");
			Util.SetImageButtonGraphic(mTabButtonReceive, "60", "60", "60");
			mTabButtonSend.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 1;
			mTabButtonReceive.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			break;
		case MODE.RECEIVE:
			Util.SetImageButtonGraphic(mTabButtonSend, "62", "62", "62");
			Util.SetImageButtonGraphic(mTabButtonReceive, "59", "59", "59");
			mTabButtonSend.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			mTabButtonReceive.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 1;
			break;
		}
	}

	private void OnCreateItem_Friend(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		GiftListItem giftListItem = item as GiftListItem;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string text = "icon_chara00";
		string text2 = "lv_1";
		if (giftListItem.friend == null)
		{
			return;
		}
		MyFriend friend = giftListItem.friend;
		int iconLevel = friend.Data.IconLevel;
		text2 = "lv_" + iconLevel;
		int a_main;
		int a_sub;
		Def.SplitStageNo(friend.Data.Level, out a_main, out a_sub);
		string text3 = string.Format(Localization.Get("Stage_Data"), a_main);
		string text4 = friend.Data.Name;
		float cellWidth = mFriendList.mList.cellWidth;
		float cellHeight = mFriendList.mList.cellHeight;
		UISprite uISprite = Util.CreateSprite("Back", itemGO, image);
		Util.SetSpriteInfo(uISprite, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		UISprite uISprite2 = Util.CreateSprite("Instruction", itemGO.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 19f, 0f), Vector3.one, false);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		uISprite2.SetDimensions(436, 108);
		int num = 0;
		if (friend.Data.IconID >= 10000)
		{
			num = friend.Data.IconID - 10000;
			if (num >= mGame.mCompanionData.Count)
			{
				num = 0;
			}
			CompanionData companionData = mGame.mCompanionData[num];
			text = companionData.iconName;
		}
		UISprite uISprite3;
		if (GameMain.IsFacebookIconEnable() && friend.Data.IconID >= 10000)
		{
			if (friend.Icon != null)
			{
				UITexture uITexture = Util.CreateGameObject("friendIcon", uISprite2.gameObject).AddComponent<UITexture>();
				uITexture.depth = mBaseDepth + 3;
				uITexture.transform.localPosition = new Vector3(-165f, -9f, 0f);
				uITexture.mainTexture = friend.Icon;
				uITexture.SetDimensions(65, 65);
			}
			else
			{
				uISprite3 = Util.CreateSprite("FriendIcon", uISprite2.gameObject, image2);
				Util.SetSpriteInfo(uISprite3, "icon_chara_facebook", mBaseDepth + 3, new Vector3(-165f, -9f, 0f), Vector3.one, false);
				uISprite3.SetDimensions(65, 65);
			}
			num = friend.Data.IconID - 10000;
			if (num >= mGame.mCompanionData.Count)
			{
				num = 0;
			}
			string iconName = mGame.mCompanionData[num].iconName;
			UISprite uISprite4 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
			Util.SetSpriteInfo(uISprite4, iconName, mBaseDepth + 4, new Vector3(-138f, 24f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(50, 50);
		}
		else
		{
			num = friend.Data.IconID;
			if (num >= 10000)
			{
				num -= 10000;
			}
			if (num >= mGame.mCompanionData.Count)
			{
				num = 0;
			}
			string iconName2 = mGame.mCompanionData[num].iconName;
			UISprite uISprite5 = Util.CreateSprite("Icon", uISprite2.gameObject, image2);
			Util.SetSpriteInfo(uISprite5, iconName2, mBaseDepth + 3, new Vector3(-156f, 3f, 0f), Vector3.one, false);
			uISprite5.SetDimensions(84, 84);
			iconName2 = ((friend.Data.IconLevel <= 0) ? "lv_1" : ("lv_" + friend.Data.IconLevel));
			UISprite sprite = Util.CreateSprite("Lv", uISprite2.gameObject, image);
			Util.SetSpriteInfo(sprite, iconName2, mBaseDepth + 4, new Vector3(-156f, -37f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && !string.IsNullOrEmpty(friend.Data.Fbid))
			{
				string imageName = "icon_sns00_mini";
				uISprite3 = Util.CreateSprite("FacebookIcon", uISprite5.gameObject, image2);
				Util.SetSpriteInfo(uISprite3, imageName, mBaseDepth + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
			}
		}
		uISprite3 = Util.CreateSprite("Linetop", uISprite2.gameObject, image);
		Util.SetSpriteInfo(uISprite3, "line00", mBaseDepth + 3, new Vector3(51f, 7f, 0f), Vector3.one, false);
		uISprite3.type = UIBasicSprite.Type.Sliced;
		uISprite3.SetDimensions(292, 6);
		UILabel uILabel = Util.CreateLabel("userNameDesc", uISprite2.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text4, mBaseDepth + 4, new Vector3(51f, 24f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(300, 26);
		int num2 = friend.Data.Series;
		int stageNumber = 1;
		List<PlayStageParam> playStage = friend.Data.PlayStage;
		if (playStage != null && playStage.Count != 0)
		{
			for (int i = 0; i < playStage.Count; i++)
			{
				if (playStage[i].Series == num2)
				{
					stageNumber = playStage[i].Level / 100;
					break;
				}
			}
		}
		else
		{
			num2 = 0;
			Def.SplitStageNo(friend.Data.Level, out a_main, out a_sub);
			stageNumber = a_main;
		}
		SMDTools sMDTools = new SMDTools();
		sMDTools.SMDSeries(uISprite2.gameObject, mBaseDepth, num2, stageNumber, -19f, -8f);
		UIButton button = Util.CreateJellyImageButton("RemoveButton", uISprite2.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_application2", "LC_button_application2", "LC_button_application2", mBaseDepth + 3, new Vector3(138f, -22f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnSendGiftPushed", UIButtonMessage.Trigger.OnClick);
	}

	public override void OnOpenFinished()
	{
		mFriendListInfo = new SMVerticalListInfo(1, 512f, 472f, 1, 512f, 472f, 1, 121);
		mList.Sort();
		mFriendList = SMVerticalList.Make(base.gameObject, this, mFriendListInfo, mList, 0f, -18f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mFriendList.OnCreateItem = OnCreateItem_Friend;
	}

	public void OnSendTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && Mode != 0)
		{
			mGame.PlaySe("SE_PUSH", -1);
			ActivateTab(MODE.SEND);
			ChangeMode(MODE.SEND);
		}
	}

	public void OnReceiveTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && Mode != MODE.RECEIVE)
		{
			mGame.PlaySe("SE_PUSH", -1);
			ActivateTab(MODE.RECEIVE);
			ChangeMode(MODE.RECEIVE);
		}
	}

	public void OnSendGiftPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		GiftListItem giftListItem = mFriendList.FindListItem(go) as GiftListItem;
		if (giftListItem != null)
		{
			mGame.PlaySe("SE_RECEIVE_GIFT", -1);
			mSelectedGiftItem = giftListItem;
			mState.Reset(STATE.WAIT, true);
			GiftItemData giftItemData = new GiftItemData();
			giftItemData.FromID = mGame.mPlayer.UUID;
			giftItemData.Message = mGame.mOptions.FbName;
			int uUID = mSelectedGiftItem.friend.Data.UUID;
			giftItemData.ItemCategory = 14;
			int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
			giftItemData.ItemKind = (int)mGame.mPlayer.Data.CurrentSeries;
			giftItemData.ItemID = roadBlockLevel;
			if (!Server.GiveGift(giftItemData, uUID))
			{
				OnNetwork_SendGiftFail();
			}
		}
	}

	private void Server_OnGiveGift(int result, int code)
	{
		if (result == 0)
		{
			mState.Reset(STATE.NETWORK_00_00, true);
		}
		else
		{
			mState.Reset(STATE.NETWORK_00_01, true);
		}
	}

	private void OnNetwork_SendGiftSuccess()
	{
		GiftListItem giftListItem = mSelectedGiftItem;
		mSelectedGiftItem = null;
		mFriendList.RemoveListItem(giftListItem);
		if (giftListItem.friend != null)
		{
			DateTime now = DateTime.Now;
			int num = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(now);
			int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
			int uUID = giftListItem.friend.Data.UUID;
			string hiveId = giftListItem.friend.Data.HiveId;
			int num2 = 1;
			DateTime dateTime = DateTimeUtil.UnitLocalTimeEpoch;
			if (mGame.mPlayer.SendInfo.ContainsKey(uUID))
			{
				dateTime = mGame.mPlayer.SendInfo[uUID].LastRoadBlockRequestTime;
			}
			int num3 = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(dateTime);
			int currentLevel = mGame.mPlayer.CurrentLevel;
			int lifeCount = mGame.mPlayer.LifeCount;
			int totalStars = mGame.mPlayer.TotalStars;
			mGame.mPlayer.UpdateRoadBlockRequest(uUID);
		}
		mGame.Save();
		mState.Change(STATE.MAIN);
	}

	private void OnNetwork_SendGiftFail()
	{
		GiftListItem item = mSelectedGiftItem;
		mSelectedGiftItem = null;
		mFriendList.RemoveListItem(item);
		item = null;
		mState.Reset(STATE.WAIT, true);
		EnableList(false);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("RoadBlockHelp_Error_Desc"));
		mConfirmDialog.Init(Localization.Get("RoadBlockHelp_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSendErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
	}

	private void OnSendErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void EnableList(bool enabled)
	{
		if (!(mFriendList == null))
		{
			NGUITools.SetActive(mFriendList.gameObject, enabled);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
