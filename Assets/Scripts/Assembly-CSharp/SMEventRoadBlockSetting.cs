public class SMEventRoadBlockSetting : SMRoadBlockSetting
{
	public int AccessoryIDOnOpened;

	public string ImageName = string.Empty;

	public string TitleKey = string.Empty;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		base.Serialize(a_writer);
		a_writer.WriteInt(AccessoryIDOnOpened);
		a_writer.WriteUTF(ImageName);
		a_writer.WriteUTF(TitleKey);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		base.Deserialize(a_reader);
		AccessoryIDOnOpened = a_reader.ReadInt();
		ImageName = a_reader.ReadUTF();
		TitleKey = a_reader.ReadUTF();
	}

	public int UnlockAccessoryID()
	{
		return UnlockFriends;
	}
}
