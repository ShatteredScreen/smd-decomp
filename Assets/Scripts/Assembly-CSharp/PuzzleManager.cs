using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : SingletonMonoBehaviour<PuzzleManager>, IPuzzleManager
{
	public enum STATE
	{
		NONE = -1,
		WAIT = 0,
		INIT = 1,
		BOOSTER = 2,
		BEFOREPLAY = 3,
		PLAY = 4,
		CHAIN = 5,
		SHUFFLE = 6,
		SHUFFLE_WAIT = 7,
		SELECT_BOOSTER_TARGET = 8,
		SELECT_SKILL_TARGET = 9,
		PAUSE = 10,
		WIN = 11,
		LOSE = 12,
		SKILLBALLTRANSFORM = 13,
		SPECIALTILECRUSH = 14,
		CLEARBONUS = 15,
		FINISH = 16,
		INCREASE_WAIT = 17
	}

	public class CrushGroupItem
	{
		public PuzzleTile tile;

		public Def.TILE_KIND kind;

		public Def.TILE_FORM form;

		public CrushGroupItem(PuzzleTile _tile, Def.TILE_KIND _kind, Def.TILE_FORM _form)
		{
			tile = _tile;
			kind = _kind;
			form = _form;
		}
	}

	public delegate void TriggerDelegate();

	public delegate void TriggerDelegate_Int(int i);

	public delegate void TriggerDelegate_Float(float f);

	public delegate void TriggerDelegate_Bool(bool b);

	public delegate bool StageStartDelegate();

	public delegate bool UseBoosterDelegate(Def.BOOSTER_KIND b, PuzzleTile targetTile, bool add_use_count);

	public delegate bool SkillTargetSelectDelegate(PuzzleTile targetTile);

	public delegate bool EffectBoosterDelegate(Def.BOOSTER_KIND b, PuzzleTile targetTile);

	public delegate bool BeforePlayDelegate();

	public delegate void OnBeginCrushDelegate(PuzzleTile tile, float delayTime);

	public delegate void OnBeginSpecialCreateDelegate(Def.TILE_FORM form);

	public delegate void OnBeginSpecialCrossDelegate(Def.TILE_KIND crossKind);

	public delegate void OnCollectDelegate(PuzzleTile tile, Vector3 worldPosition, string overWriteImage);

	public delegate bool OnComboFinishedDelegate(int comboNum);

	public delegate void OnLoseDelegate(Def.STAGE_LOSE_REASON reason);

	public delegate void OnSkillPushedDelegate(PuzzleTile targetTile);

	public delegate GameObject OnGetTutorialArrowTargetDelegate(Def.TUTORIAL_INDEX tutIndex);

	public delegate void OnTutorialTriggerDelegate(Def.TUTORIAL_INDEX tutIndex);

	public delegate void OnShakeScreenDelegate(float time);

	public delegate void OnAttackTartgetsChangedDelegate(Vector3 orignPos, Def.TILE_KIND[] targets, bool immediate);

	public delegate void OnPlacePlateDelegate(Vector3 orignPos, int plate1Num, int plate2Num, int plate3Num);

	public delegate void OnTalismanPlaceMovableWallDelegate(PuzzleTile orignTile, int plate1Num, int plate2Num, int plate3Num);

	public StageStartDelegate OnPuzzleStart = () => false;

	public UseBoosterDelegate OnBoosterUse = (Def.BOOSTER_KIND b, PuzzleTile targetTile, bool add_use_count) => false;

	public SkillTargetSelectDelegate OnSkillTargetSelect = (PuzzleTile targetTile) => false;

	public EffectBoosterDelegate OnBoosterEffectOnly = (Def.BOOSTER_KIND b, PuzzleTile targetTile) => false;

	public BeforePlayDelegate OnBeforePlay = () => false;

	public TriggerDelegate OnPuzzleHudAdjust = delegate
	{
	};

	public OnBeginCrushDelegate OnBeginCrush = delegate
	{
	};

	public OnBeginCrushDelegate OnBossDamageOrbCrush = delegate
	{
	};

	public OnBeginSpecialCreateDelegate OnSpecialCreateCountUp = delegate
	{
	};

	public OnBeginSpecialCreateDelegate OnSpecialCrushCountUp = delegate
	{
	};

	public OnBeginSpecialCrossDelegate OnSpecialCrossCountUp = delegate
	{
	};

	public OnCollectDelegate OnCollect = delegate
	{
	};

	public TriggerDelegate_Bool OnShuffle = delegate
	{
	};

	public TriggerDelegate_Int OnChangeMoves = delegate
	{
	};

	public TriggerDelegate_Float OnChangeTimer = delegate
	{
	};

	public TriggerDelegate_Int OnChangeScore = delegate
	{
	};

	public TriggerDelegate_Int OnCombo = delegate
	{
	};

	public OnComboFinishedDelegate OnComboFinished = (int comboNum) => false;

	public TriggerDelegate OnWin = delegate
	{
	};

	public TriggerDelegate OnClearBonusSpecialTileCrushed = delegate
	{
	};

	public TriggerDelegate OnClearBonusStart = delegate
	{
	};

	public TriggerDelegate_Int OnClearBonusStepUp = delegate
	{
	};

	public TriggerDelegate OnClearBonusFinished = delegate
	{
	};

	public OnLoseDelegate OnLose = delegate
	{
	};

	public BeforePlayDelegate OnSkillBallTransform = () => false;

	public TriggerDelegate OnPuzzleFinished = delegate
	{
	};

	public OnSkillPushedDelegate OnSkillPushed = delegate
	{
	};

	public OnGetTutorialArrowTargetDelegate OnGetTutorialArrowTarget = (Def.TUTORIAL_INDEX tutIndex) => null;

	public OnTutorialTriggerDelegate OnTutorialTrigger = delegate
	{
	};

	public OnShakeScreenDelegate OnShakeScreen = delegate
	{
	};

	public OnAttackTartgetsChangedDelegate OnAttackTargetsChanged = delegate
	{
	};

	public OnPlacePlateDelegate OnPlaceBrokenWall = delegate
	{
	};

	public TriggerDelegate_Int OnDianaStepRemainChanged = delegate
	{
	};

	public OnTalismanPlaceMovableWallDelegate OnTalismanPlaceMovableWall = delegate
	{
	};

	public BeforePlayDelegate OnBeforePuzzleSpecialOfferCheck = () => false;

	private bool mTutorialHighLightEnable;

	private int[][] LEVEL1_reservedTiles = new int[7][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 1, 2, 3, 0, 1, 2, 3, 0 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL2_reservedTiles = new int[9][]
	{
		new int[13]
		{
			3, 2, 1, 0, 3, 2, 1, 0, 3, 0,
			2, 0, 3
		},
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[11]
		{
			3, 2, 1, 0, 3, 2, 1, 0, 3, 2,
			1
		},
		new int[11]
		{
			0, 1, 2, 3, 0, 1, 2, 3, 1, 0,
			2
		}
	};

	private int[][] LEVEL3_reservedTiles = new int[7][]
	{
		new int[8] { 0, 3, 0, 3, 1, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 1, 2, 3, 0, 1, 3, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 3, 0, 2, 0, 3, 2, 3 },
		new int[8] { 2, 0, 0, 1, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL4_reservedTiles = new int[9][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL5_reservedTiles = new int[7][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL6_reservedTiles = new int[7][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[17]
		{
			3, 2, 1, 0, 3, 2, 1, 0, 2, 3,
			1, 1, 0, 3, 0, 2, 2
		},
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL7_reservedTiles = new int[7][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL8_reservedTiles = new int[7][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL9_reservedTiles = new int[7][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 }
	};

	private int[][] LEVEL31_reservedTiles = new int[9][]
	{
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 0, 1, 2, 3, 0, 1, 2, 3 },
		new int[8] { 3, 2, 1, 0, 3, 2, 1, 0 }
	};

	private int[][] LEVEL167_reservedTiles = new int[7][]
	{
		new int[0],
		new int[4] { 0, 2, 1, 3 },
		new int[4] { 0, 1, 3, 2 },
		new int[1] { 1 },
		new int[0],
		new int[0],
		new int[0]
	};

	private IntVector2[] LEVEL1_3_tile = new IntVector2[4]
	{
		new IntVector2(2, 5),
		new IntVector2(3, 5),
		new IntVector2(4, 5),
		new IntVector2(5, 5)
	};

	private IntVector2[] LEVEL1_4_tile = new IntVector2[4]
	{
		new IntVector2(6, 6),
		new IntVector2(6, 5),
		new IntVector2(6, 4),
		new IntVector2(6, 3)
	};

	private IntVector2[] LEVEL2_1_tile = new IntVector2[5]
	{
		new IntVector2(3, 5),
		new IntVector2(2, 5),
		new IntVector2(2, 7),
		new IntVector2(2, 6),
		new IntVector2(2, 4)
	};

	private IntVector2[] LEVEL2_2_tile = new IntVector2[4]
	{
		new IntVector2(2, 4),
		new IntVector2(2, 3),
		new IntVector2(3, 3),
		new IntVector2(4, 3)
	};

	private IntVector2[] LEVEL2_3_tile = new IntVector2[5]
	{
		new IntVector2(5, 5),
		new IntVector2(5, 4),
		new IntVector2(4, 4),
		new IntVector2(6, 4),
		new IntVector2(7, 4)
	};

	private IntVector2[] LEVEL2_4_tile = new IntVector2[4]
	{
		new IntVector2(5, 4),
		new IntVector2(4, 4),
		new IntVector2(4, 5),
		new IntVector2(4, 3)
	};

	private IntVector2[] LEVEL3_1_tile = new IntVector2[6]
	{
		new IntVector2(4, 6),
		new IntVector2(5, 6),
		new IntVector2(5, 7),
		new IntVector2(5, 8),
		new IntVector2(6, 6),
		new IntVector2(7, 6)
	};

	private IntVector2[] LEVEL3_2_tile = new IntVector2[4]
	{
		new IntVector2(5, 6),
		new IntVector2(5, 5),
		new IntVector2(5, 4),
		new IntVector2(5, 3)
	};

	private IntVector2[] LEVEL4_1_tile = new IntVector2[6]
	{
		new IntVector2(4, 7),
		new IntVector2(5, 7),
		new IntVector2(5, 9),
		new IntVector2(5, 8),
		new IntVector2(5, 6),
		new IntVector2(5, 5)
	};

	private IntVector2[] LEVEL4_2_tile = new IntVector2[2]
	{
		new IntVector2(5, 5),
		new IntVector2(4, 5)
	};

	private IntVector2[] LEVEL5_1_tile = new IntVector2[7]
	{
		new IntVector2(4, 6),
		new IntVector2(5, 6),
		new IntVector2(5, 8),
		new IntVector2(5, 7),
		new IntVector2(5, 5),
		new IntVector2(6, 6),
		new IntVector2(7, 6)
	};

	private IntVector2[] LEVEL5_2_tile = new IntVector2[2]
	{
		new IntVector2(5, 5),
		new IntVector2(5, 4)
	};

	private IntVector2[] LEVEL6_1_tile = new IntVector2[2]
	{
		new IntVector2(5, 5),
		new IntVector2(5, 4)
	};

	private IntVector2[] LEVEL8_1_tile = new IntVector2[12]
	{
		new IntVector2(2, 4),
		new IntVector2(3, 3),
		new IntVector2(3, 4),
		new IntVector2(3, 5),
		new IntVector2(4, 4),
		new IntVector2(4, 5),
		new IntVector2(5, 4),
		new IntVector2(5, 5),
		new IntVector2(6, 4),
		new IntVector2(6, 5),
		new IntVector2(7, 4),
		new IntVector2(7, 5)
	};

	private IntVector2[] LEVEL9_1_tile = new IntVector2[4]
	{
		new IntVector2(4, 9),
		new IntVector2(4, 8),
		new IntVector2(3, 8),
		new IntVector2(2, 8)
	};

	private IntVector2[] LEVEL9_2_tile = new IntVector2[1]
	{
		new IntVector2(5, 8)
	};

	private IntVector2[] LEVEL9_3_tile = new IntVector2[1]
	{
		new IntVector2(5, 8)
	};

	private IntVector2[] LEVEL11_1_tile = new IntVector2[2]
	{
		new IntVector2(3, 9),
		new IntVector2(7, 9)
	};

	private IntVector2[] LEVEL13_0_tile = new IntVector2[12]
	{
		new IntVector2(4, 4),
		new IntVector2(5, 4),
		new IntVector2(6, 4),
		new IntVector2(4, 5),
		new IntVector2(5, 5),
		new IntVector2(6, 5),
		new IntVector2(4, 6),
		new IntVector2(5, 6),
		new IntVector2(6, 6),
		new IntVector2(4, 7),
		new IntVector2(5, 7),
		new IntVector2(6, 7)
	};

	private IntVector2[] LEVEL24_0_tile = new IntVector2[9]
	{
		new IntVector2(4, 3),
		new IntVector2(5, 3),
		new IntVector2(6, 3),
		new IntVector2(4, 4),
		new IntVector2(5, 4),
		new IntVector2(6, 4),
		new IntVector2(4, 5),
		new IntVector2(5, 5),
		new IntVector2(6, 5)
	};

	private IntVector2[] LEVEL26_0_tile = new IntVector2[18]
	{
		new IntVector2(3, 2),
		new IntVector2(4, 2),
		new IntVector2(2, 3),
		new IntVector2(5, 3),
		new IntVector2(2, 4),
		new IntVector2(5, 4),
		new IntVector2(3, 5),
		new IntVector2(4, 5),
		new IntVector2(5, 5),
		new IntVector2(8, 5),
		new IntVector2(6, 6),
		new IntVector2(8, 6),
		new IntVector2(7, 7),
		new IntVector2(8, 7),
		new IntVector2(5, 8),
		new IntVector2(6, 8),
		new IntVector2(7, 8),
		new IntVector2(8, 8)
	};

	private IntVector2[] LEVEL31_1_tile = new IntVector2[8]
	{
		new IntVector2(4, 4),
		new IntVector2(3, 4),
		new IntVector2(3, 5),
		new IntVector2(3, 6),
		new IntVector2(5, 2),
		new IntVector2(2, 5),
		new IntVector2(8, 5),
		new IntVector2(5, 8)
	};

	private IntVector2[] LEVEL48_0_tile = new IntVector2[10]
	{
		new IntVector2(2, 4),
		new IntVector2(3, 4),
		new IntVector2(4, 4),
		new IntVector2(6, 4),
		new IntVector2(7, 4),
		new IntVector2(8, 4),
		new IntVector2(5, 1),
		new IntVector2(5, 2),
		new IntVector2(5, 3),
		new IntVector2(5, 7)
	};

	private IntVector2[] LEVEL61_0_tile = new IntVector2[8]
	{
		new IntVector2(3, 6),
		new IntVector2(4, 6),
		new IntVector2(3, 7),
		new IntVector2(4, 7),
		new IntVector2(6, 3),
		new IntVector2(7, 3),
		new IntVector2(6, 4),
		new IntVector2(7, 4)
	};

	private IntVector2[] LEVEL62_1_tile = new IntVector2[28]
	{
		new IntVector2(1, 3),
		new IntVector2(2, 3),
		new IntVector2(1, 4),
		new IntVector2(2, 4),
		new IntVector2(1, 5),
		new IntVector2(2, 5),
		new IntVector2(4, 3),
		new IntVector2(5, 3),
		new IntVector2(6, 3),
		new IntVector2(4, 4),
		new IntVector2(5, 4),
		new IntVector2(6, 4),
		new IntVector2(8, 3),
		new IntVector2(9, 3),
		new IntVector2(8, 4),
		new IntVector2(9, 4),
		new IntVector2(8, 5),
		new IntVector2(9, 5),
		new IntVector2(2, 7),
		new IntVector2(2, 8),
		new IntVector2(4, 7),
		new IntVector2(5, 7),
		new IntVector2(6, 7),
		new IntVector2(4, 8),
		new IntVector2(5, 8),
		new IntVector2(6, 8),
		new IntVector2(8, 7),
		new IntVector2(8, 8)
	};

	private IntVector2[] LEVEL76_0_tile = new IntVector2[8]
	{
		new IntVector2(1, 4),
		new IntVector2(2, 4),
		new IntVector2(3, 4),
		new IntVector2(4, 4),
		new IntVector2(6, 11),
		new IntVector2(7, 11),
		new IntVector2(8, 11),
		new IntVector2(9, 11)
	};

	private IntVector2[] LEVEL109_0_tile = new IntVector2[4]
	{
		new IntVector2(5, 6),
		new IntVector2(5, 7),
		new IntVector2(5, 8),
		new IntVector2(5, 9)
	};

	private IntVector2[] LEVEL136_0_tile = new IntVector2[7]
	{
		new IntVector2(2, 4),
		new IntVector2(3, 4),
		new IntVector2(4, 4),
		new IntVector2(5, 4),
		new IntVector2(6, 4),
		new IntVector2(7, 4),
		new IntVector2(8, 4)
	};

	private IntVector2[] LEVEL167_0_tile = new IntVector2[18]
	{
		new IntVector2(3, 2),
		new IntVector2(3, 3),
		new IntVector2(3, 4),
		new IntVector2(3, 5),
		new IntVector2(3, 6),
		new IntVector2(3, 7),
		new IntVector2(4, 2),
		new IntVector2(4, 7),
		new IntVector2(5, 2),
		new IntVector2(5, 7),
		new IntVector2(6, 2),
		new IntVector2(6, 7),
		new IntVector2(7, 2),
		new IntVector2(7, 3),
		new IntVector2(7, 4),
		new IntVector2(7, 5),
		new IntVector2(7, 6),
		new IntVector2(7, 7)
	};

	private IntVector2[] LEVEL167_1_tile = new IntVector2[4]
	{
		new IntVector2(4, 8),
		new IntVector2(4, 7),
		new IntVector2(4, 6),
		new IntVector2(4, 5)
	};

	private IntVector2[] LEVEL197_0_tile = new IntVector2[5]
	{
		new IntVector2(3, 5),
		new IntVector2(4, 5),
		new IntVector2(5, 5),
		new IntVector2(6, 5),
		new IntVector2(7, 5)
	};

	private IntVector2[] LEVEL32R_0_tile = new IntVector2[7]
	{
		new IntVector2(2, 1),
		new IntVector2(3, 1),
		new IntVector2(4, 1),
		new IntVector2(5, 1),
		new IntVector2(6, 1),
		new IntVector2(7, 1),
		new IntVector2(8, 1)
	};

	private IntVector2[] LEVEL62R_0_tile = new IntVector2[2]
	{
		new IntVector2(6, 9),
		new IntVector2(7, 9)
	};

	private IntVector2[] LEVEL62R_1_tile = new IntVector2[2]
	{
		new IntVector2(2, 6),
		new IntVector2(2, 5)
	};

	private IntVector2[] LEVEL1S_0_tile = new IntVector2[3]
	{
		new IntVector2(3, 8),
		new IntVector2(5, 3),
		new IntVector2(7, 8)
	};

	private IntVector2[] LEVEL76S_0_tile = new IntVector2[2]
	{
		new IntVector2(3, 7),
		new IntVector2(4, 7)
	};

	private IntVector2[] LEVEL7SS_0_tile = new IntVector2[3]
	{
		new IntVector2(3, 8),
		new IntVector2(5, 2),
		new IntVector2(7, 8)
	};

	private IntVector2[] LEVEL23SS_0_tile = new IntVector2[2]
	{
		new IntVector2(2, 9),
		new IntVector2(8, 9)
	};

	private IntVector2[] LEVEL47SS_0_tile = new IntVector2[2]
	{
		new IntVector2(2, 8),
		new IntVector2(7, 8)
	};

	private IntVector2[] LEVEL107SS_1_tile = new IntVector2[7]
	{
		new IntVector2(1, 3),
		new IntVector2(1, 7),
		new IntVector2(4, 5),
		new IntVector2(6, 9),
		new IntVector2(7, 5),
		new IntVector2(9, 4),
		new IntVector2(9, 8)
	};

	private IntVector2[] LEVEL107SS_2_tile = new IntVector2[1]
	{
		new IntVector2(4, 8)
	};

	private IntVector2[] LEVEL47StarS_0_tile = new IntVector2[2]
	{
		new IntVector2(3, 7),
		new IntVector2(7, 3)
	};

	private IntVector2[] LEVEL47StarS_1_tile = new IntVector2[2]
	{
		new IntVector2(3, 3),
		new IntVector2(7, 7)
	};

	private IntVector2[] LEVEL107StarS_0_tile = new IntVector2[8]
	{
		new IntVector2(2, 2),
		new IntVector2(2, 4),
		new IntVector2(4, 2),
		new IntVector2(4, 4),
		new IntVector2(7, 1),
		new IntVector2(7, 3),
		new IntVector2(7, 5),
		new IntVector2(7, 7)
	};

	public StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	public GameMain mGame;

	public GameObject mRoot;

	public Camera mCamera;

	public float mPlayTime;

	public int mScore;

	public int mCombo;

	public bool mFirstMove;

	protected int mCollectEffectBusy;

	protected bool mTileCrushed;

	protected bool mUserSwaped;

	protected bool mEnemyTileCrushed;

	protected bool mBeforePlayOrbCheck;

	protected bool mBeforePlayTalismanCheck = true;

	protected bool mGameFinished;

	public int mMoves;

	public float mTime;

	protected PuzzleTile mCaptureTile;

	protected Dictionary<Def.TILE_KIND, int> mSpawnCount = new Dictionary<Def.TILE_KIND, int>();

	protected Dictionary<Def.TILE_KIND, int> mExitCount = new Dictionary<Def.TILE_KIND, int>();

	protected Dictionary<Def.TILE_FORM, int> mFormRemainCount = new Dictionary<Def.TILE_FORM, int>();

	protected List<SwapPair> mSwapList = new List<SwapPair>();

	protected HashSet<PuzzleTile> mCrushList = new HashSet<PuzzleTile>();

	public PuzzleBackGrounds mPuzzleBackGrounds;

	protected short[,] mSpecialCrossFormTbl;

	protected ObjectPool<TileSprite> mTileSpritePool = new ObjectPool<TileSprite>();

	protected Def.SERIES mSeriesNumber;

	protected int mStageNumber;

	protected IntVector2 mFieldSize;

	protected IntVector2 mPlayAreaSize;

	protected List<Def.TILE_KIND> mRandomKindList;

	protected Def.STAGE_WIN_CONDITION mWinCondition;

	protected Def.STAGE_LOSE_CONDITION mLoseCondition;

	protected Def.STAGE_LOSE_REASON mLoseReason;

	protected Def.TILE_KIND[] mNormaKindArray;

	protected bool mJewelGetChance;

	public PuzzleTile[,] mTile;

	public PuzzleTile[,] mTileTemp;

	public IntVector2[,] mNum;

	public PuzzleTile[,,] mAttribute;

	public bool[,] mBackground;

	public PuzzleAttributeInfo[,] mBackgroundInfos;

	public List<PuzzleTile> mAppealTiles = new List<PuzzleTile>();

	protected float mAutoPlayCheckTime;

	protected MatchTileInfo mAutoPlayInfo;

	public List<IntVector2> mSpawnPoints = new List<IntVector2>();

	public List<Def.TILE_KIND>[] mReservedFillTiles;

	public float mMoveFinishCheckTimer;

	protected bool mCancelRequest;

	protected float mShakeTimer;

	protected float mIdleTimer;

	protected bool mShuffleRequest;

	protected bool mForceShuffleRequest;

	protected bool mShuffleFinished;

	protected int mShuffleTryCount;

	protected PuzzleTile mReservedCrushSkillBall;

	protected SMMapStageSetting mStageInfo;

	protected List<SpecialSpawnData> mSpecialSpawnList = new List<SpecialSpawnData>();

	protected List<SpecialSpawnData> mCollectItemList = new List<SpecialSpawnData>();

	protected IntVector2 mLastPairParts0GeneratePos = new IntVector2(0, 0);

	protected IntVector2 mLastOrbGeneratePos = new IntVector2(0, 0);

	protected BossGimmick mBossCharacter;

	protected List<FindGimmick> mFindGimmicks = new List<FindGimmick>();

	protected List<FamiliarGimmick> mFamiliarGimmicks = new List<FamiliarGimmick>();

	protected DianaGimmick mDianaCharacter;

	public List<OneShotAnime> mOneShotList = new List<OneShotAnime>();

	protected List<Def.BOOSTER_KIND> mUseBoostersTemp = new List<Def.BOOSTER_KIND>();

	protected List<Def.BOOSTER_KIND> mNoRegisterUseBoostersTemp = new List<Def.BOOSTER_KIND>();

	private bool mUseBeforeBoosters;

	public bool mEnablePlusChance;

	public bool mUseScoreUpChance;

	protected Def.BOOSTER_KIND mUseBoosterKind;

	public float mExpandedScoreRatio;

	public float mSkillExpandedScoreRatio;

	public int mSkillExpandedScoreRatioRemain;

	public List<LoopAnime> mBoosterTargetList = new List<LoopAnime>();

	protected Def.SKILL_TYPE mUseSkillType;

	public int mMaxCombo;

	public int mTotalMoves;

	public int mStageClearScore;

	public int mStageClearStars;

	public int mStageClearMoves;

	private Def.DIFFICULTY_MODE mDifficultyMode;

	private float mDifficultyModeWork0;

	private object[] mDifficultyModeParamArray;

	private static int uqTileCounter = 0;

	private BetterList<PuzzleTile> tempBetterList_PuzzleTile = new BetterList<PuzzleTile>();

	private static string[] comboSeTbl = new string[12]
	{
		"SE_COMBO00", "SE_COMBO01", "SE_COMBO02", "SE_COMBO03", "SE_COMBO04", "SE_COMBO05", "SE_COMBO06", "SE_COMBO07", "SE_COMBO08", "SE_COMBO09",
		"SE_COMBO10", "SE_COMBO11"
	};

	public STATE NextState { get; set; }

	public GameObject Root
	{
		get
		{
			return mRoot;
		}
	}

	public float TimeScale { get; set; }

	public bool InputEnable { get; set; }

	public bool TimerEnable { get; set; }

	public float ExpandedScoreRatio
	{
		get
		{
			return mExpandedScoreRatio;
		}
	}

	public float SkillExpandedScoreRatio
	{
		get
		{
			return mSkillExpandedScoreRatio;
		}
	}

	protected void HandleCreateBackground(int x, int y, Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		switch (kind)
		{
		case Def.TILE_KIND.THROUGH:
		case Def.TILE_KIND.WALL:
		case Def.TILE_KIND.SPAWN:
		case Def.TILE_KIND.EXIT:
		case Def.TILE_KIND.BOSS0:
		case Def.TILE_KIND.WARP_IN:
		case Def.TILE_KIND.WARP_OUT:
			return;
		}
		mBackground[y, x] = true;
		PuzzleAttributeInfo puzzleAttributeInfo = new PuzzleAttributeInfo();
		puzzleAttributeInfo.kind = Def.TILE_KIND.NORMAL;
		puzzleAttributeInfo.pos = new IntVector2(x, y);
		mBackgroundInfos[y, x] = puzzleAttributeInfo;
	}

	protected void HandleCreateAttribute(int x, int y, Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		switch (kind)
		{
		case Def.TILE_KIND.EXIT:
			PlaceAttribute(new IntVector2(x, y), CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y)));
			break;
		case Def.TILE_KIND.THROUGH:
		case Def.TILE_KIND.THROUGH_WALL0:
		case Def.TILE_KIND.THROUGH_WALL1:
		case Def.TILE_KIND.THROUGH_WALL2:
		case Def.TILE_KIND.BROKEN_WALL0:
		case Def.TILE_KIND.BROKEN_WALL1:
		case Def.TILE_KIND.BROKEN_WALL2:
		case Def.TILE_KIND.CAPTURE0:
		case Def.TILE_KIND.CAPTURE1:
		case Def.TILE_KIND.CAPTURE2:
		case Def.TILE_KIND.INCREASE:
		case Def.TILE_KIND.GROWN_UP0:
		case Def.TILE_KIND.GROWN_UP1:
		case Def.TILE_KIND.GROWN_UP2:
		case Def.TILE_KIND.WARP_IN:
		case Def.TILE_KIND.WARP_OUT:
			PlaceAttribute(new IntVector2(x, y), CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y)));
			break;
		case Def.TILE_KIND.SPAWN:
			mSpawnPoints.Add(new IntVector2(x, y));
			PlaceAttribute(new IntVector2(x, y), CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y)));
			break;
		case Def.TILE_KIND.BOSS0:
			CreateBossGimmick(kind, new IntVector2(x, y));
			break;
		case Def.TILE_KIND.DIANA_GOAL_L:
		case Def.TILE_KIND.DIANA_GOAL_R:
			PlaceAttribute(new IntVector2(x, y), CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y)));
			break;
		}
	}

	protected void HandleCreateEdge(int x, int y, Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		if (mBackground[y, x])
		{
			return;
		}
		for (int i = 0; i < 8; i++)
		{
			if (IsInField(new IntVector2(x + Def.DIR_OFS[i].x, y + Def.DIR_OFS[i].y)) && mBackground[y + Def.DIR_OFS[i].y, x + Def.DIR_OFS[i].x])
			{
				PuzzleAttributeInfo puzzleAttributeInfo = new PuzzleAttributeInfo();
				puzzleAttributeInfo.kind = Def.TILE_KIND.EDGE;
				puzzleAttributeInfo.pos = new IntVector2(x, y);
				mBackgroundInfos[y, x] = puzzleAttributeInfo;
				break;
			}
		}
	}

	protected void HandleReservedTile(int x, int y, Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		PuzzleTile puzzleTile = null;
		switch (kind)
		{
		case Def.TILE_KIND.COLOR0:
		case Def.TILE_KIND.COLOR1:
		case Def.TILE_KIND.COLOR2:
		case Def.TILE_KIND.COLOR3:
		case Def.TILE_KIND.COLOR4:
		case Def.TILE_KIND.COLOR5:
		case Def.TILE_KIND.COLOR6:
			puzzleTile = ((form != Def.TILE_FORM.RAINBOW) ? CreateTile(mRoot, kind, form, new IntVector2(x, y)) : CreateTile(mRoot, Def.TILE_KIND.SPECIAL, form, new IntVector2(x, y)));
			break;
		case Def.TILE_KIND.SPECIAL:
			puzzleTile = CreateTile(mRoot, kind, form, new IntVector2(x, y));
			break;
		case Def.TILE_KIND.INGREDIENT0:
		case Def.TILE_KIND.INGREDIENT1:
		case Def.TILE_KIND.INGREDIENT2:
		case Def.TILE_KIND.INGREDIENT3:
		case Def.TILE_KIND.PAIR0_PARTS:
		case Def.TILE_KIND.PAIR1_PARTS:
			puzzleTile = CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y));
			AddSpawnCount(kind, -1, true);
			break;
		case Def.TILE_KIND.PRESENTBOX0:
		case Def.TILE_KIND.PRESENTBOX1:
		case Def.TILE_KIND.PRESENTBOX2:
		case Def.TILE_KIND.PRESENTBOX3:
		case Def.TILE_KIND.PRESENTBOX4:
		case Def.TILE_KIND.PRESENTBOX5:
		case Def.TILE_KIND.PRESENTBOX6:
		case Def.TILE_KIND.PRESENTBOX7:
		case Def.TILE_KIND.PRESENTBOX8:
		case Def.TILE_KIND.PRESENTBOX9:
		case Def.TILE_KIND.PRESENTBOX10:
		case Def.TILE_KIND.PRESENTBOX11:
		case Def.TILE_KIND.PRESENTBOX12:
		case Def.TILE_KIND.PRESENTBOX13:
		case Def.TILE_KIND.PRESENTBOX14:
		case Def.TILE_KIND.PRESENTBOX15:
		case Def.TILE_KIND.PRESENTBOX16:
		case Def.TILE_KIND.PRESENTBOX17:
		case Def.TILE_KIND.PRESENTBOX18:
		case Def.TILE_KIND.PRESENTBOX19:
		case Def.TILE_KIND.PRESENTBOX20:
		case Def.TILE_KIND.PRESENTBOX21:
		case Def.TILE_KIND.PRESENTBOX22:
		case Def.TILE_KIND.PRESENTBOX23:
			if (!GlobalVariables.Instance.IsDailyChallengePuzzle)
			{
				puzzleTile = CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y));
			}
			break;
		case Def.TILE_KIND.MOVABLE_WALL0:
		case Def.TILE_KIND.MOVABLE_WALL1:
		case Def.TILE_KIND.MOVABLE_WALL2:
		case Def.TILE_KIND.ORB_WAVE_V0:
		case Def.TILE_KIND.ORB_WAVE_V1:
		case Def.TILE_KIND.ORB_WAVE_V2:
		case Def.TILE_KIND.ORB_WAVE_H0:
		case Def.TILE_KIND.ORB_WAVE_H1:
		case Def.TILE_KIND.ORB_WAVE_H2:
		case Def.TILE_KIND.ORB_BOMB0:
		case Def.TILE_KIND.ORB_BOMB1:
		case Def.TILE_KIND.ORB_BOMB2:
		case Def.TILE_KIND.ORB_BOSSDAMAGE0:
		case Def.TILE_KIND.ORB_BOSSDAMAGE1:
		case Def.TILE_KIND.ORB_BOSSDAMAGE2:
		case Def.TILE_KIND.FIXED_FLOWER0:
		case Def.TILE_KIND.FIXED_FLOWER1:
		case Def.TILE_KIND.MOVABLE_FLOWER0:
		case Def.TILE_KIND.MOVABLE_FLOWER1:
		case Def.TILE_KIND.TALISMAN0_0:
		case Def.TILE_KIND.TALISMAN1_0:
		case Def.TILE_KIND.TALISMAN2_0:
		case Def.TILE_KIND.STAR:
			puzzleTile = CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y));
			break;
		case Def.TILE_KIND.DIANA:
			puzzleTile = CreateTile(mRoot, kind, Def.TILE_FORM.NORMAL, new IntVector2(x, y));
			CreateDianaGimmick(new IntVector2(x, y), puzzleTile);
			break;
		case Def.TILE_KIND.THROUGH:
		case Def.TILE_KIND.WALL:
		case Def.TILE_KIND.SPAWN:
		case Def.TILE_KIND.EXIT:
			return;
		}
		if (puzzleTile != null)
		{
			PlaceTile(new IntVector2(x, y), puzzleTile);
		}
	}

	protected void HandleCreateTile(int x, int y, Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		ISMTileInfo tileInfo = mGame.mCurrentStage.GetTileInfo(x, y);
		if (tileInfo.TileAttribute_BLANK)
		{
			return;
		}
		if (tileInfo.TileAttribute2_FAMILIAR_SPIRIT)
		{
			CreateFamiliarGimmick(new IntVector2(x, y), tileInfo.FamiliarSpiritID);
		}
		PuzzleTile tile = GetTile(new IntVector2(x, y), false);
		if (tile != null || !GetBackground(new IntVector2(x, y)))
		{
			return;
		}
		PuzzleTile attribute = GetAttribute(new IntVector2(x, y));
		if (attribute != null && !attribute.isThrough())
		{
			return;
		}
		tile = null;
		int num = 0;
		while (true)
		{
			Def.TILE_KIND kind2 = RandomColorTile();
			if (tile == null)
			{
				tile = CreateTile(mRoot, kind2, Def.TILE_FORM.NORMAL, new IntVector2(x, y));
			}
			else
			{
				tile.ChangeForm(kind2, Def.TILE_FORM.NORMAL, 0f, 0f, true, false, 0, true);
			}
			PlaceTile(new IntVector2(x, y), tile);
			if (num > 100)
			{
				mShuffleRequest = true;
				break;
			}
			int candyNumH = GetCandyNumH(new IntVector2(x, y));
			int candyNumV = GetCandyNumV(new IntVector2(x, y));
			if (mDifficultyMode == Def.DIFFICULTY_MODE.INITIAL_SPLIT_H)
			{
				if (candyNumH < 2 && candyNumV < 3)
				{
					break;
				}
			}
			else if (candyNumH < 3 && candyNumV < 3)
			{
				break;
			}
			RemoveTile(new IntVector2(x, y), false);
			num++;
		}
	}

	public void InitStage(SMMapStageSetting stageInfo)
	{
		mSeriesNumber = mGame.mCurrentStage.Series;
		mStageNumber = mGame.mCurrentStage.StageNumber;
		mScore = 0;
		mCancelRequest = false;
		mGameFinished = false;
		mReservedCrushSkillBall = null;
		mStageInfo = stageInfo;
		mExpandedScoreRatio = 0f;
		mSkillExpandedScoreRatio = 0f;
		mSkillExpandedScoreRatioRemain = 0;
		mMaxCombo = 0;
		mTotalMoves = 0;
		mStageClearScore = 0;
		mStageClearStars = 0;
		mStageClearMoves = 0;
		if (mStageInfo != null && mGame.PlayingFriendHelp == null)
		{
			int count = mStageInfo.BoxSpawnCondition.Count;
			for (int i = 0; i < count; i++)
			{
				if (!mGame.mPlayer.IsAccessoryUnlock(mStageInfo.BoxID[i]))
				{
					SpecialSpawnData specialSpawnData = new SpecialSpawnData(mStageInfo.BoxSpawnTileKind[i], mStageInfo.BoxKind[i], mStageInfo.BoxID[i], mStageInfo.BoxSpawnCondition[i], mStageInfo.BoxSpawnConditionID[i]);
					if (GlobalVariables.Instance.IsDailyChallengePuzzle)
					{
						specialSpawnData.SpawnFinised = true;
					}
					else if (specialSpawnData.SpawnCondition == Def.UNLOCK_TYPE.INIT || specialSpawnData.SpawnCondition == Def.UNLOCK_TYPE.STAR)
					{
						specialSpawnData.SpawnFinised = true;
					}
					mSpecialSpawnList.Add(specialSpawnData);
				}
			}
		}
		TimerEnable = false;
		InputEnable = false;
		mFirstMove = false;
		ResetShakeTimer();
		ResetIdleTimer();
		mWinCondition = mGame.mCurrentStage.WinType;
		mLoseCondition = mGame.mCurrentStage.LoseType;
		mNormaKindArray = mGame.mCurrentStage.GetNormaKindArray();
		if (!mGame.mReplay && Def.HEART_BREAK_RETURN && mWinCondition != Def.STAGE_WIN_CONDITION.STARGET)
		{
			PlayStartHeartBreak();
		}
		switch (mLoseCondition)
		{
		case Def.STAGE_LOSE_CONDITION.MOVES:
			mMoves = mGame.mCurrentStage.LimitMoves;
			OnChangeMoves(mMoves);
			break;
		case Def.STAGE_LOSE_CONDITION.TIME:
			mTime = mGame.mCurrentStage.LimitTimeSec;
			OnChangeTimer(mTime);
			break;
		default:
			mMoves = mGame.mCurrentStage.LimitMoves;
			OnChangeMoves(mMoves);
			break;
		}
		mSpawnCount.Clear();
		mFormRemainCount[Def.TILE_FORM.ADDTIME] = mGame.mCurrentStage.BonusLimit1;
		mFormRemainCount[Def.TILE_FORM.ADDPLUS] = mGame.mCurrentStage.BonusMoveLimit1;
		mSpawnCount[Def.TILE_KIND.MOVABLE_WALL0] = mGame.mCurrentStage.FCookie1Limit;
		mSpawnCount[Def.TILE_KIND.MOVABLE_WALL1] = mGame.mCurrentStage.FCookie2Limit;
		mSpawnCount[Def.TILE_KIND.MOVABLE_WALL2] = mGame.mCurrentStage.FCookie3Limit;
		mSpawnCount[Def.TILE_KIND.ORB_WAVE_V0] = mGame.mCurrentStage.FOrb1Limit;
		mSpawnCount[Def.TILE_KIND.ORB_WAVE_H0] = mGame.mCurrentStage.FOrb2Limit;
		mSpawnCount[Def.TILE_KIND.ORB_BOMB0] = mGame.mCurrentStage.FOrb3Limit;
		mSpawnCount[Def.TILE_KIND.ORB_BOSSDAMAGE0] = mGame.mCurrentStage.FOrb4Limit;
		mFormRemainCount[Def.TILE_FORM.FOOT_STAMP] = mGame.mCurrentStage.FootPieceLimit1;
		mSpawnCount[Def.TILE_KIND.MOVABLE_FLOWER0] = mGame.mCurrentStage.FPerl00Limit1;
		mSpawnCount[Def.TILE_KIND.MOVABLE_FLOWER1] = mGame.mCurrentStage.FPerl01Limit1;
		mFormRemainCount[Def.TILE_FORM.PEARL] = mGame.mCurrentStage.FPerl02Limit1;
		mFormRemainCount[Def.TILE_FORM.JEWEL] = mGame.mCurrentStage.FJewel1Limit1;
		mFormRemainCount[Def.TILE_FORM.JEWEL3] = mGame.mCurrentStage.FJewel3Limit1;
		mFormRemainCount[Def.TILE_FORM.JEWEL5] = mGame.mCurrentStage.FJewel5Limit1;
		mFormRemainCount[Def.TILE_FORM.JEWEL9] = mGame.mCurrentStage.FJewel9Limit1;
		switch (mWinCondition)
		{
		case Def.STAGE_WIN_CONDITION.COLLECT:
			mSpawnCount[Def.TILE_KIND.INGREDIENT0] = mGame.mCurrentStage.Ingredient0;
			mSpawnCount[Def.TILE_KIND.INGREDIENT1] = mGame.mCurrentStage.Ingredient1;
			mSpawnCount[Def.TILE_KIND.INGREDIENT2] = mGame.mCurrentStage.Ingredient2;
			mSpawnCount[Def.TILE_KIND.INGREDIENT3] = mGame.mCurrentStage.Ingredient3;
			break;
		case Def.STAGE_WIN_CONDITION.EATEN:
			mSpawnCount[Def.TILE_KIND.COLOR0] = mGame.mCurrentStage.Color0;
			mSpawnCount[Def.TILE_KIND.COLOR1] = mGame.mCurrentStage.Color1;
			mSpawnCount[Def.TILE_KIND.COLOR2] = mGame.mCurrentStage.Color2;
			mSpawnCount[Def.TILE_KIND.COLOR3] = mGame.mCurrentStage.Color3;
			mSpawnCount[Def.TILE_KIND.COLOR4] = mGame.mCurrentStage.Color4;
			mSpawnCount[Def.TILE_KIND.COLOR5] = mGame.mCurrentStage.Color5;
			mSpawnCount[Def.TILE_KIND.COLOR6] = mGame.mCurrentStage.Color6;
			break;
		case Def.STAGE_WIN_CONDITION.PAIR:
			mSpawnCount[Def.TILE_KIND.PAIR0_PARTS] = mGame.mCurrentStage.PairLimit0;
			mSpawnCount[Def.TILE_KIND.PAIR1_PARTS] = 0;
			break;
		}
		mExitCount.Clear();
		mRandomKindList = new List<Def.TILE_KIND>();
		if (mGame.mCurrentStage.UseColor0)
		{
			mRandomKindList.Add(Def.TILE_KIND.COLOR0);
		}
		if (mGame.mCurrentStage.UseColor1)
		{
			mRandomKindList.Add(Def.TILE_KIND.COLOR1);
		}
		if (mGame.mCurrentStage.UseColor2)
		{
			mRandomKindList.Add(Def.TILE_KIND.COLOR2);
		}
		if (mGame.mCurrentStage.UseColor3)
		{
			mRandomKindList.Add(Def.TILE_KIND.COLOR3);
		}
		if (mGame.mCurrentStage.UseColor4)
		{
			mRandomKindList.Add(Def.TILE_KIND.COLOR4);
		}
		if (mGame.mCurrentStage.UseColor5)
		{
			mRandomKindList.Add(Def.TILE_KIND.COLOR5);
		}
		if (mGame.mCurrentStage.UseColor6)
		{
			mRandomKindList.Add(Def.TILE_KIND.COLOR6);
		}
		StageCreator stageCreator = new StageCreator(ref mGame.mCurrentStage);
		mFieldSize = stageCreator.FieldSize;
		mPlayAreaSize = new IntVector2(mGame.mCurrentStage.PlayAreaWidth, mGame.mCurrentStage.PlayAreaHeight);
		mTile = new PuzzleTile[mFieldSize.y, mFieldSize.x];
		mTileTemp = new PuzzleTile[mFieldSize.y, mFieldSize.x];
		mNum = new IntVector2[mFieldSize.y, mFieldSize.x];
		mAttribute = new PuzzleTile[Def.ATTRIBUTE_LAYER_NUM, mFieldSize.y, mFieldSize.x];
		mBackground = new bool[mFieldSize.y, mFieldSize.x];
		mBackgroundInfos = new PuzzleAttributeInfo[mFieldSize.y, mFieldSize.x];
		for (int j = 0; j < mFieldSize.y; j++)
		{
			for (int k = 0; k < mFieldSize.x; k++)
			{
				mTile[j, k] = null;
				mTileTemp[j, k] = null;
				mNum[j, k] = new IntVector2(0, 0);
			}
		}
		mTileSpritePool.Init(mFieldSize.x * mFieldSize.y * 2);
		stageCreator.HandleCreateBackground = HandleCreateBackground;
		stageCreator.HandleCreateAttribute = HandleCreateAttribute;
		stageCreator.HandleCreateEdge = HandleCreateEdge;
		stageCreator.HandleReservedTile = HandleReservedTile;
		stageCreator.HandleCreateTile = HandleCreateTile;
		for (int l = 0; l < mGame.mCurrentStage.StencilDataList.Count; l++)
		{
			CreateFindGimmick(mGame.mCurrentStage.StencilDataList[l]);
		}
		SetColorModifyMode(false);
		stageCreator.Execute();
		for (int m = 0; m < mSpecialSpawnList.Count; m++)
		{
			if (mSpecialSpawnList[m].SpawnCondition == Def.UNLOCK_TYPE.INIT)
			{
				long num = mSpecialSpawnList[m].CountDown;
				int y = (int)(num & 0xFFFF);
				int x = (int)((num & 0xFFFF0000u) >> 16);
				PuzzleTile tile = GetTile(new IntVector2(x, y), false);
				if (tile != null)
				{
					RemoveTile(tile, false);
					tile.ChangeForm(mSpecialSpawnList[m].Kind, Def.TILE_FORM.NORMAL, 0f, 0f, true, false, 0, true);
					tile.ItemCategory = mSpecialSpawnList[m].Category;
					tile.ItemIndex = mSpecialSpawnList[m].ItemIndex;
					PlaceTile(tile.TilePos, tile);
					mSpecialSpawnList[m].SpawnFinised = true;
				}
			}
		}
		mReservedFillTiles = new List<Def.TILE_KIND>[mSpawnPoints.Count];
		for (int n = 0; n < mSpawnPoints.Count; n++)
		{
			mReservedFillTiles[n] = new List<Def.TILE_KIND>();
		}
		if (mDifficultyMode == Def.DIFFICULTY_MODE.COLOR_MODIFY_POSI)
		{
			SetColorModifyMode(true);
		}
		if (mDifficultyMode == Def.DIFFICULTY_MODE.FILL_MATCH)
		{
			mDifficultyModeWork0 = 0f;
		}
		if (!mShuffleRequest)
		{
			UpdateAppealList();
		}
		int num2 = 0;
		while (mShuffleRequest && num2 < 100)
		{
			Shuffle(true);
			UpdateAppealList();
			num2++;
		}
		if (mGame.mCurrentStage.DianaWayPosList.Count > 0)
		{
			foreach (IntVector2 dianaWayPos in mGame.mCurrentStage.DianaWayPosList)
			{
				PuzzleAttributeInfo puzzleAttributeInfo = new PuzzleAttributeInfo();
				puzzleAttributeInfo.kind = Def.TILE_KIND.DIANA_ROUTE;
				puzzleAttributeInfo.pos = dianaWayPos;
				mBackgroundInfos[dianaWayPos.y, dianaWayPos.x] = puzzleAttributeInfo;
			}
		}
		if (mPuzzleBackGrounds != null)
		{
			UnityEngine.Object.Destroy(mPuzzleBackGrounds.gameObject);
		}
		mPuzzleBackGrounds = PuzzleBackGrounds.Make(mRoot, mGame, this, ResourceManager.LoadImage("PUZZLE_BG").Image, mBackgroundInfos, new Vector3(0f, 0f, 12f));
	}

	public void SetColorModifyMode(bool mode)
	{
		if (mode)
		{
			if (mWinCondition == Def.STAGE_WIN_CONDITION.EATEN)
			{
				List<Def.TILE_KIND> list = new List<Def.TILE_KIND>(mRandomKindList);
				if (mGame.mCurrentStage.Color0 > 0)
				{
					list.Remove(Def.TILE_KIND.COLOR0);
				}
				if (mGame.mCurrentStage.Color1 > 0)
				{
					list.Remove(Def.TILE_KIND.COLOR1);
				}
				if (mGame.mCurrentStage.Color2 > 0)
				{
					list.Remove(Def.TILE_KIND.COLOR2);
				}
				if (mGame.mCurrentStage.Color3 > 0)
				{
					list.Remove(Def.TILE_KIND.COLOR3);
				}
				if (mGame.mCurrentStage.Color4 > 0)
				{
					list.Remove(Def.TILE_KIND.COLOR4);
				}
				if (mGame.mCurrentStage.Color5 > 0)
				{
					list.Remove(Def.TILE_KIND.COLOR5);
				}
				if (mGame.mCurrentStage.Color6 > 0)
				{
					list.Remove(Def.TILE_KIND.COLOR6);
				}
				if (list.Count > 1)
				{
					Def.TILE_GENERATE_RATIO[(int)list[UnityEngine.Random.Range(0, list.Count - 1)]] = 5;
				}
				return;
			}
			Dictionary<Def.TILE_KIND, int> dictionary = new Dictionary<Def.TILE_KIND, int>();
			for (int i = 0; i < mRandomKindList.Count; i++)
			{
				dictionary.Add(mRandomKindList[i], 0);
			}
			for (int j = 0; j < mFieldSize.y; j++)
			{
				for (int k = 0; k < mFieldSize.x; k++)
				{
					if (mTile[j, k] != null && mTile[j, k].isColor())
					{
						Dictionary<Def.TILE_KIND, int> dictionary2;
						Dictionary<Def.TILE_KIND, int> dictionary3 = (dictionary2 = dictionary);
						Def.TILE_KIND kind;
						Def.TILE_KIND key = (kind = mTile[j, k].Kind);
						int num = dictionary2[kind];
						dictionary3[key] = num + 1;
					}
				}
			}
			int num2 = mRandomKindList.Count - 4;
			for (int l = 0; l < num2; l++)
			{
				Def.TILE_KIND tILE_KIND = mRandomKindList[0];
				int num3 = dictionary[mRandomKindList[0]];
				for (int m = 1; m < mRandomKindList.Count; m++)
				{
					if (num3 >= dictionary[mRandomKindList[m]])
					{
						tILE_KIND = mRandomKindList[m];
						num3 = dictionary[mRandomKindList[m]];
						dictionary[mRandomKindList[m]] = 9999;
					}
				}
				Def.TILE_GENERATE_RATIO[(int)tILE_KIND] = 5 + l * 5;
			}
		}
		else
		{
			for (int n = 0; n < Def.TILE_GENERATE_RATIO.Length; n++)
			{
				Def.TILE_GENERATE_RATIO[n] = 20;
			}
		}
	}

	public void SetRoot(GameObject gameObject)
	{
		mRoot = gameObject;
	}

	public void SetCamera(Camera camera)
	{
		mCamera = camera;
	}

	public void LoadSpecialCrossData(string filePath)
	{
		mSpecialCrossFormTbl = new short[19, 19];
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(filePath))
		{
			short num = bIJResourceBinaryReader.ReadShort();
			for (int i = 0; i < num; i++)
			{
				short num2 = bIJResourceBinaryReader.ReadShort();
				for (int j = 0; j < num2; j++)
				{
					string value = bIJResourceBinaryReader.ReadUTF();
					try
					{
						Def.TILE_KIND tILE_KIND = (Def.TILE_KIND)(int)Enum.Parse(typeof(Def.TILE_KIND), value);
						mSpecialCrossFormTbl[i, j] = (short)tILE_KIND;
					}
					catch (Exception)
					{
					}
				}
			}
			bIJResourceBinaryReader.Close();
		}
	}

	public void PlayStartHeartBreak()
	{
		mGame.mPlayer.SubLifeCountAtPuzzleStart();
		mGame.Save();
	}

	public int GetRandomKindCount()
	{
		int num = 0;
		if (mGame.mCurrentStage.UseColor0)
		{
			num++;
		}
		if (mGame.mCurrentStage.UseColor1)
		{
			num++;
		}
		if (mGame.mCurrentStage.UseColor2)
		{
			num++;
		}
		if (mGame.mCurrentStage.UseColor3)
		{
			num++;
		}
		if (mGame.mCurrentStage.UseColor4)
		{
			num++;
		}
		if (mGame.mCurrentStage.UseColor5)
		{
			num++;
		}
		if (mGame.mCurrentStage.UseColor6)
		{
			num++;
		}
		return num;
	}

	public void SetDifficultyMode(Def.DIFFICULTY_MODE mode, object[] paramArray)
	{
		mDifficultyMode = mode;
		mDifficultyModeParamArray = paramArray;
		if (mode == Def.DIFFICULTY_MODE.FILL_MATCH)
		{
			int randomKindCount = GetRandomKindCount();
			float num = 1f;
			if (randomKindCount <= 4)
			{
				num = 0.2f;
			}
			else if (randomKindCount <= 5)
			{
				num = 0.6f;
			}
			int num2 = (int)mDifficultyModeParamArray[0];
			int num3 = (int)mDifficultyModeParamArray[1];
			mDifficultyModeParamArray[0] = (int)((float)num2 * num);
			mDifficultyModeParamArray[1] = (int)((float)num3 * num);
		}
	}

	public Def.DIFFICULTY_MODE GetDifficultyMode()
	{
		return mDifficultyMode;
	}

	private void UpdateDifficultyMode()
	{
		Def.DIFFICULTY_MODE dIFFICULTY_MODE = mDifficultyMode;
		if (dIFFICULTY_MODE == Def.DIFFICULTY_MODE.FILL_MATCH && mDifficultyModeParamArray != null && mDifficultyModeParamArray.Length >= 2)
		{
			int num = (int)mDifficultyModeParamArray[0];
			int num2 = (int)mDifficultyModeParamArray[1];
			float num3 = 1f - (float)mMoves / (float)mGame.mCurrentStage.LimitMoves;
			mDifficultyModeWork0 = num3 * (float)(num2 - num) + (float)num;
		}
	}

	public void SpecialWaveH(IntVector2 pos)
	{
		for (int i = 0; i < mFieldSize.x; i++)
		{
			float timer = 0.1f + (float)Mathf.Abs(i - pos.x) * 0.05f;
			PuzzleTile tile = GetTile(new IntVector2(i, pos.y), false);
			if (tile != null)
			{
				BeginCrush(tile, timer, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(i, pos.y), timer, true);
		}
	}

	public void SpecialWaveV(IntVector2 pos)
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			float timer = 0.1f + (float)Mathf.Abs(i - pos.y) * 0.05f;
			PuzzleTile tile = GetTile(new IntVector2(pos.x, i), false);
			if (tile != null)
			{
				BeginCrush(tile, timer, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(pos.x, i), timer, true);
		}
	}

	public void SpecialBomb(IntVector2 pos, int size)
	{
		for (int i = -size; i <= size; i++)
		{
			for (int j = -size; j <= size; j++)
			{
				if (i != 0 || j != 0)
				{
					PuzzleTile tile = GetTile(new IntVector2(pos.x + j, pos.y + i), false);
					if (tile != null)
					{
						BeginCrush(tile, 0.5f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
					}
					BeginAttributeCrush(new IntVector2(pos.x + j, pos.y + i), 0.5f, true);
				}
			}
		}
	}

	public void SpecialBossDamage(PuzzleTile tile)
	{
		OnBossDamageOrbCrush(tile, 0f);
	}

	public void SpecialRainbow(IntVector2 pos, Def.TILE_KIND targetKind)
	{
		if (targetKind == Def.TILE_KIND.NONE)
		{
			targetKind = RandomColorTile();
		}
		Vector2 defaultCoords = GetDefaultCoords(pos);
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == targetKind)
				{
					float num = Mathf.Sqrt((j - pos.x) * (j - pos.x) + (i - pos.y) * (i - pos.y));
					if (BeginCrush(tile, 0.8f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1))
					{
						BeginAttributeCrush(tile.TilePos, 0.8f, true);
					}
					OneShotAnime oneShotAnime = Util.CreateOneShotAnime("Debris", "EFFECT_RAINBOW_DEBRIS", mRoot, new Vector3(defaultCoords.x, defaultCoords.y, -3f), new Vector3(num - 0.1f, 1f, 1f), 0f, 0.5f, false, OneShotAnime_OnFinished);
					float z = Mathf.Atan2(i - pos.y, j - pos.x) * 57.29578f;
					oneShotAnime.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, z);
					AddOneShotAnime(oneShotAnime);
				}
			}
		}
	}

	public void SpecialPaint(IntVector2 pos, Def.TILE_KIND targetKind, Def.TILE_KIND changeKind)
	{
		if (changeKind == Def.TILE_KIND.NONE)
		{
			changeKind = RandomColorTile();
		}
		if (targetKind == Def.TILE_KIND.NONE)
		{
			do
			{
				targetKind = RandomColorTile();
			}
			while (targetKind == changeKind);
		}
		string key = "EFFECT_RAINBOW_DEBRIS";
		switch (changeKind)
		{
		case Def.TILE_KIND.COLOR0:
			key = "EFFECT_PAINT_COLOR0_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR1:
			key = "EFFECT_PAINT_COLOR1_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR2:
			key = "EFFECT_PAINT_COLOR2_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR3:
			key = "EFFECT_PAINT_COLOR3_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR4:
			key = "EFFECT_PAINT_COLOR4_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR5:
			key = "EFFECT_PAINT_COLOR5_DEBRIS";
			break;
		}
		Vector2 defaultCoords = GetDefaultCoords(pos);
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == targetKind && !tile.isMove())
				{
					float num = Mathf.Sqrt((j - pos.x) * (j - pos.x) + (i - pos.y) * (i - pos.y));
					tile.ChangeForm(changeKind, Def.TILE_FORM.NONE, 0f, 0.8f, false, true, 0, true);
					OneShotAnime oneShotAnime = Util.CreateOneShotAnime("Debris", key, mRoot, new Vector3(defaultCoords.x, defaultCoords.y, -3f), new Vector3(num - 0.1f, 1f, 1f), 0f, 0.19999999f, false, OneShotAnime_OnFinished);
					float z = Mathf.Atan2(i - pos.y, j - pos.x) * 57.29578f;
					oneShotAnime.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, z);
					AddOneShotAnime(oneShotAnime);
				}
			}
		}
	}

	public void SkillPushed(PuzzleTile targetTile)
	{
		if (mGame.mTutorialManager.IsTutorialPlaying() && mGame.mTutorialManager.GetCurrentTutorial() == Def.TUTORIAL_INDEX.LEVEL9_3)
		{
			if (!mGame.mTutorialManager.IsWantMove(targetTile, Def.DIR.NONE))
			{
				return;
			}
			mGame.mTutorialManager.ClearWantMove();
			TutorialHighLightEnd();
			TutorialMessageFinish();
			TutorialStart(Def.TUTORIAL_INDEX.LEVEL9_4);
		}
		mState.Reset(STATE.WAIT, true);
		NextState = STATE.CHAIN;
		OnSkillPushed(targetTile);
	}

	public void SpecialCombo_WaveWave(IntVector2 pos)
	{
		SpecialWaveH(pos);
		SpecialWaveV(pos);
	}

	public void SpecialCombo_WaveBomb(IntVector2 pos)
	{
		for (int i = 0; i < mFieldSize.x; i++)
		{
			float timer = Def.WAVE_BOMB_FIRST_CROSS_TIME + (float)Mathf.Abs(i - pos.x) * 0.05f;
			PuzzleTile tile = GetTile(new IntVector2(i, pos.y - 1), false);
			if (tile != null)
			{
				BeginCrush(tile, timer, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(i, pos.y - 1), timer, true);
			tile = GetTile(new IntVector2(i, pos.y), false);
			if (tile != null)
			{
				BeginCrush(tile, timer, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(i, pos.y), timer, true);
			tile = GetTile(new IntVector2(i, pos.y + 1), false);
			if (tile != null)
			{
				BeginCrush(tile, timer, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(i, pos.y + 1), timer, true);
		}
		for (int j = 0; j < mFieldSize.y; j++)
		{
			float timer2 = Def.WAVE_BOMB_SECOND_CROSS_TIME + (float)Mathf.Abs(j - pos.y) * 0.05f;
			PuzzleTile tile = GetTile(new IntVector2(pos.x - 1, j), false);
			if (tile != null)
			{
				BeginCrush(tile, timer2, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(pos.x - 1, j), timer2, true);
			tile = GetTile(new IntVector2(pos.x, j), false);
			if (tile != null)
			{
				BeginCrush(tile, timer2, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(pos.x, j), timer2, true);
			tile = GetTile(new IntVector2(pos.x + 1, j), false);
			if (tile != null)
			{
				BeginCrush(tile, timer2, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(pos.x + 1, j), timer2, true);
		}
	}

	public void SpecialCombo_WaveRainbow(IntVector2 pos, Def.TILE_KIND targetKind)
	{
		int num = 1;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile == null || tile.Kind != targetKind)
				{
					continue;
				}
				if (tile.isNormalForm())
				{
					if (UnityEngine.Random.Range(0, 100) < 50)
					{
						tile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.WAVE_H, (float)num * 0.3f, 0f, false, true, 0, false);
					}
					else
					{
						tile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.WAVE_V, (float)num * 0.3f, 0f, false, true, 0, false);
					}
					num++;
				}
				else if (tile.isWave())
				{
					tile.ChangeForm(Def.TILE_KIND.NONE, tile.Form, (float)num * 0.3f, 0f, false, true, 0, false);
					num++;
				}
			}
		}
	}

	public void SpecialCombo_BombBomb(IntVector2 pos)
	{
		SpecialBomb(pos, 2);
	}

	public void SpecialCombo_BombRainbow(IntVector2 pos, Def.TILE_KIND targetKind)
	{
		int num = 1;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == targetKind)
				{
					if (tile.isNormalForm())
					{
						tile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.BOMB_B, (float)num * 0.3f, 0f, false, true, 0, false);
						num++;
					}
					else if (tile.Form == Def.TILE_FORM.BOMB)
					{
						tile.ChangeForm(Def.TILE_KIND.NONE, tile.Form, (float)num * 0.3f, 0f, false, true, 0, false);
						num++;
					}
				}
			}
		}
	}

	public void SpecialCombo_RainbowRainbow(IntVector2 pos)
	{
		Vector2 defaultCoords = GetDefaultCoords(pos);
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && !tile.isExitableTile() && !tile.isPair())
				{
					float num = Mathf.Sqrt((j - pos.x) * (j - pos.x) + (i - pos.y) * (i - pos.y));
					if (BeginCrush(tile, 0.2f * num, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1))
					{
						BeginAttributeCrush(tile.TilePos, 0.2f * num, true);
					}
					OneShotAnime oneShotAnime = Util.CreateOneShotAnime("Debris", "EFFECT_RAINBOW_DEBRIS", mRoot, new Vector3(defaultCoords.x, defaultCoords.y, -3f), new Vector3(num - 0.1f, 1f, 1f), 0f, 0.2f * num - 0.1f, false, OneShotAnime_OnFinished);
					float z = Mathf.Atan2(i - pos.y, j - pos.x) * 57.29578f;
					oneShotAnime.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, z);
					AddOneShotAnime(oneShotAnime);
				}
			}
		}
	}

	public void SpecialCombo_BombPaint(IntVector2 pos, Def.TILE_KIND paintKind, Def.TILE_KIND targetKind)
	{
		int num = 1;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == paintKind)
				{
					if (tile.isNormalForm())
					{
						tile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.BOMB_B, (float)num * 0.3f + 1f, 0f, false, true, 0, false);
						num++;
					}
					else if (tile.Form == Def.TILE_FORM.BOMB)
					{
						tile.ChangeForm(Def.TILE_KIND.NONE, tile.Form, (float)num * 0.3f + 1f, 0f, false, true, 0, false);
						num++;
					}
				}
				if (tile != null && tile.Kind == targetKind)
				{
					if (tile.isNormalForm())
					{
						tile.ChangeForm(paintKind, Def.TILE_FORM.BOMB_B, (float)num * 0.3f, 0f, false, true, 0, false);
						num++;
					}
					else if (tile.Form == Def.TILE_FORM.BOMB)
					{
						tile.ChangeForm(paintKind, tile.Form, (float)num * 0.3f, 0f, false, true, 0, false);
						num++;
					}
				}
			}
		}
	}

	public void SpecialCombo_WavePaint(IntVector2 pos, Def.TILE_KIND paintKind, Def.TILE_KIND targetKind)
	{
		int num = 1;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == paintKind)
				{
					if (tile.isNormalForm())
					{
						if (UnityEngine.Random.Range(0, 100) < 50)
						{
							tile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.WAVE_H, (float)num * 0.3f + 1f, 0f, false, true, 0, false);
						}
						else
						{
							tile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.WAVE_V, (float)num * 0.3f + 1f, 0f, false, true, 0, false);
						}
						num++;
					}
					else if (tile.isWave())
					{
						tile.ChangeForm(Def.TILE_KIND.NONE, tile.Form, (float)num * 0.3f, 0f, false, true, 0, false);
					}
				}
				if (tile == null || tile.Kind != targetKind)
				{
					continue;
				}
				if (tile.isNormalForm())
				{
					if (UnityEngine.Random.Range(0, 100) < 50)
					{
						tile.ChangeForm(paintKind, Def.TILE_FORM.WAVE_H, (float)num * 0.3f, 0f, false, true, 0, false);
					}
					else
					{
						tile.ChangeForm(paintKind, Def.TILE_FORM.WAVE_V, (float)num * 0.3f, 0f, false, true, 0, false);
					}
					num++;
				}
				else if (tile.isWave())
				{
					tile.ChangeForm(paintKind, tile.Form, (float)num * 0.3f, 0f, false, true, 0, false);
				}
			}
		}
	}

	public void SpecialCombo_PaintRainbow(IntVector2 pos, Def.TILE_KIND changeKind)
	{
		if (changeKind == Def.TILE_KIND.NONE)
		{
			changeKind = RandomColorTile();
		}
		string key = "EFFECT_RAINBOW_DEBRIS";
		switch (changeKind)
		{
		case Def.TILE_KIND.COLOR0:
			key = "EFFECT_PAINT_COLOR0_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR1:
			key = "EFFECT_PAINT_COLOR1_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR2:
			key = "EFFECT_PAINT_COLOR2_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR3:
			key = "EFFECT_PAINT_COLOR3_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR4:
			key = "EFFECT_PAINT_COLOR4_DEBRIS";
			break;
		case Def.TILE_KIND.COLOR5:
			key = "EFFECT_PAINT_COLOR5_DEBRIS";
			break;
		}
		Vector2 defaultCoords = GetDefaultCoords(pos);
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.isColor())
				{
					float num = Mathf.Sqrt((j - pos.x) * (j - pos.x) + (i - pos.y) * (i - pos.y));
					tile.ChangeForm(changeKind, Def.TILE_FORM.NONE, 0f, 0.15f * num, false, true, 0, false);
					OneShotAnime oneShotAnime = Util.CreateOneShotAnime("Debris", key, mRoot, new Vector3(defaultCoords.x, defaultCoords.y, -3f), new Vector3(num - 0.1f, 1f, 1f), 0f, Mathf.Max(0f, 0.15f * num - 0.6f), false, OneShotAnime_OnFinished);
					float z = Mathf.Atan2(i - pos.y, j - pos.x) * 57.29578f;
					oneShotAnime.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, z);
					AddOneShotAnime(oneShotAnime);
				}
			}
		}
	}

	public void SpecialCombo_PaintPaint(IntVector2 pos, Def.TILE_KIND changeKind)
	{
		SpecialCombo_PaintRainbow(pos, changeKind);
	}

	protected bool CrushAllSpecialTile()
	{
		bool result = false;
		float num = 0.1f;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null)
				{
					Def.TILE_FORM form = tile.Form;
					if (form == Def.TILE_FORM.WAVE_H || form == Def.TILE_FORM.WAVE_V || form == Def.TILE_FORM.BOMB || form == Def.TILE_FORM.RAINBOW || form == Def.TILE_FORM.PAINT)
					{
						BeginCrush(tile, num, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
						result = true;
						num += 0.05f;
					}
				}
			}
		}
		return result;
	}

	protected bool SkillBallTransferRainbow()
	{
		bool result = false;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == Def.TILE_KIND.SKILL_BALL)
				{
					tile.ChangeForm(Def.TILE_KIND.SPECIAL, Def.TILE_FORM.RAINBOW, 0f, 0f, false, true, 0, false);
					result = true;
				}
			}
		}
		return result;
	}

	protected IEnumerator ClearBonusCoroutine()
	{
		int bonusStep = 0;
		int totalBonus = Def.CLEAR_BONUS_SCORE;
		while (mMoves > 0)
		{
			OnClearBonusStepUp(bonusStep);
			PuzzleTile[] tiles = GetRandomColorTiles(true);
			int loopMax = ((bonusStep % 2 != 0) ? Def.CLEAR_BONUS_TILE_NUM1 : Def.CLEAR_BONUS_TILE_NUM0);
			if (tiles.Length < loopMax)
			{
				loopMax = tiles.Length;
			}
			for (int i = 0; i < loopMax; i++)
			{
				Def.TILE_FORM form = ((bonusStep % 2 != 0) ? Def.TILE_FORM.BOMB : ((UnityEngine.Random.Range(0, 100) >= 50) ? Def.TILE_FORM.WAVE_V : Def.TILE_FORM.WAVE_H));
				tiles[i].ChangeForm(Def.TILE_KIND.NONE, form, 0.5f, (float)i * 0.1f, false, true, totalBonus, false);
				totalBonus += Def.CLEAR_BONUS_SCORE_UP;
				mMoves--;
				OnChangeMoves(mMoves);
				if (mMoves <= 0)
				{
					break;
				}
			}
			while (!IsMoveFinishAll())
			{
				yield return 0;
			}
			while (CrushAllSpecialTile())
			{
				while (!IsMoveFinishAll())
				{
					yield return 0;
				}
			}
			bonusStep++;
			yield return new WaitForSeconds(0.5f);
		}
		yield return new WaitForSeconds(0.5f);
		OnClearBonusFinished();
		mState.Reset(STATE.FINISH, true);
	}

	public PuzzleTile[] Booster_GetTargetTiles(int num)
	{
		PuzzleTile[] array = new PuzzleTile[num];
		PuzzleTile[] colorTiles = GetColorTiles(true);
		if (colorTiles.Length < num)
		{
			return null;
		}
		List<PuzzleTile> list = new List<PuzzleTile>(colorTiles);
		for (int i = 0; i < num; i++)
		{
			int index = UnityEngine.Random.Range(0, list.Count);
			array[i] = list[index];
			list.RemoveAt(index);
		}
		return array;
	}

	public PuzzleTile[] Booster_GetBlockCrushTargetTiles(int num)
	{
		List<PuzzleTile> list = new List<PuzzleTile>();
		List<PuzzleTile> list2 = new List<PuzzleTile>();
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile attribute = GetAttribute(new IntVector2(j, i));
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (attribute != null && (attribute.isGrownUp() || attribute.isBrokenWall() || attribute.isCapture() || attribute.Kind == Def.TILE_KIND.INCREASE))
				{
					list2.Add(attribute);
				}
				if (tile != null && (tile.isFlower() || tile.isTalismanFace()))
				{
					list2.Add(tile);
				}
			}
		}
		for (int k = 0; k < num; k++)
		{
			if (list2.Count > 0)
			{
				int index = UnityEngine.Random.Range(0, list2.Count);
				list.Add(list2[index]);
				list2.RemoveAt(index);
			}
		}
		if (list.Count < num)
		{
			PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
			int num2 = num - list.Count;
			for (int l = 0; l < num2; l++)
			{
				if (l < randomColorTiles.Length)
				{
					list.Add(randomColorTiles[l]);
				}
			}
		}
		if (list.Count > 0)
		{
			return list.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile Booster_GetPairMakerTargetTile(PuzzleTile selectTile)
	{
		PuzzleTile[] pairTiles = GetPairTiles();
		PuzzleTile result = null;
		float num = 999f;
		for (int i = 0; i < pairTiles.Length; i++)
		{
			if (pairTiles[i] != selectTile)
			{
				float num2 = Mathf.Sqrt((selectTile.TilePos.x - pairTiles[i].TilePos.x) * (selectTile.TilePos.x - pairTiles[i].TilePos.x) + (selectTile.TilePos.y - pairTiles[i].TilePos.y) * (selectTile.TilePos.y - pairTiles[i].TilePos.y));
				if (num2 < num)
				{
					num = num2;
					result = pairTiles[i];
				}
			}
		}
		return result;
	}

	public void Booster_WaveBomb(PuzzleTile targetTile, int index)
	{
		if (index % 2 == 0)
		{
			if (UnityEngine.Random.Range(0, 100) < 50)
			{
				targetTile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.WAVE_H, 0f, 0f, false, true, 0, true);
			}
			else
			{
				targetTile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.WAVE_V, 0f, 0f, false, true, 0, true);
			}
		}
		else
		{
			targetTile.ChangeForm(Def.TILE_KIND.NONE, Def.TILE_FORM.BOMB, 0f, 0f, false, true, 0, true);
		}
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
	}

	public void Booster_TapBomb2(PuzzleTile targetTile)
	{
		RemoveTile(targetTile, true);
		targetTile.ChangeForm(Def.TILE_KIND.SPECIAL, Def.TILE_FORM.TAP_BOMB, 0f, 0f, false, true, 0, true);
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
	}

	public void Booster_Rainbow(PuzzleTile targetTile)
	{
		RemoveTile(targetTile, true);
		targetTile.ChangeForm(Def.TILE_KIND.SPECIAL, Def.TILE_FORM.RAINBOW, 0f, 0f, false, true, 0, true);
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
	}

	public void Booster_AddScore(float ratio)
	{
		mExpandedScoreRatio = ratio;
	}

	public void Booster_SkillCharge()
	{
	}

	public void Booster_SelectOne(PuzzleTile targetTile)
	{
		if (BeginCrush(targetTile, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1))
		{
			BeginAttributeCrush(targetTile.TilePos, 0f, true);
		}
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOOSTER_SELECT_ONE2", mRoot, pos, Vector3.one, 0f, 0f);
		if (!mFirstMove)
		{
			PlayStartAtFirstMove();
		}
	}

	public void Booster_SelectCollect(PuzzleTile targetTile)
	{
		BeginCollect(targetTile);
		if (!mFirstMove)
		{
			PlayStartAtFirstMove();
		}
	}

	public void Booster_ColorCrush(PuzzleTile targetTile)
	{
		Def.TILE_KIND kind = targetTile.Kind;
		IntVector2 tilePos = targetTile.TilePos;
		Vector3 pos = new Vector3(0f, 0f, -3f);
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == kind)
				{
					float num = Mathf.Sqrt((j - tilePos.x) * (j - tilePos.x) + (i - tilePos.y) * (i - tilePos.y));
					if (BeginCrush(tile, 0.2f * num, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1))
					{
						BeginAttributeCrush(tile.TilePos, 0.8f, true);
					}
					OneShotAnime oneShotAnime = Util.CreateOneShotAnime("Debris", "EFFECT_RAINBOW_DEBRIS", mRoot, pos, new Vector3(num - 0.1f, 1f, 1f), 0f, 0.2f * num - 0.2f, false, OneShotAnime_OnFinished);
					float z = Mathf.Atan2(i - tilePos.y, j - tilePos.x) * 57.29578f;
					oneShotAnime.gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, z);
					AddOneShotAnime(oneShotAnime);
				}
			}
		}
		if (!mFirstMove)
		{
			PlayStartAtFirstMove();
		}
	}

	public void Booster_BlockCrush(PuzzleTile targetTile)
	{
		if (targetTile.isAttributeTile())
		{
			BeginAttributeCrush(targetTile.TilePos, 0f, true);
		}
		else if (BeginCrush(targetTile, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1))
		{
			if (targetTile.isFlower() || targetTile.isTalismanFace())
			{
				BeginAttributeCrush(targetTile.TilePos, 0f, true);
			}
			else
			{
				BeginAttributeCrush(targetTile.TilePos, 0f, false);
			}
		}
		if (!mFirstMove)
		{
			PlayStartAtFirstMove();
		}
	}

	public void Booster_CrushAll(PuzzleTile targetTile)
	{
		Vector3 vector = new Vector3((float)mFieldSize.x / 2f - 0.5f, (float)mFieldSize.y / 2f - 0.5f, -2f);
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				float num = Mathf.Sqrt(((float)j - vector.x) * ((float)j - vector.x) + ((float)i - vector.y) * ((float)i - vector.y));
				if (tile != null && (!tile.isExitableTile() || tile.isTalismanFace()) && !tile.isPair() && BeginCrush(tile, 0.2f * num, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1))
				{
					BeginAttributeCrush(tile.TilePos, 0.2f * num, true);
				}
				tile = GetAttribute(new IntVector2(j, i));
				if (tile != null && (tile.isBrokenWall() || tile.isGrownUp() || tile.isCapture() || tile.Kind == Def.TILE_KIND.INCREASE))
				{
					BeginAttributeCrush(tile.TilePos, 0.2f * num, true);
				}
			}
		}
		if (!mFirstMove)
		{
			PlayStartAtFirstMove();
		}
	}

	public void Skill_Transform(PuzzleTile targetTile, Def.TILE_KIND kind, Def.TILE_FORM form)
	{
		targetTile.ChangeForm(kind, form, 0f, 0f, false, true, 0, true);
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
	}

	public void Skill_TransformWave(PuzzleTile targetTile, int num)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num2 = 0;
		while (num > 0)
		{
			Def.TILE_KIND kind = RandomColorTile();
			Def.TILE_FORM form = ((UnityEngine.Random.Range(0, 100) >= 50) ? Def.TILE_FORM.WAVE_V : Def.TILE_FORM.WAVE_H);
			Skill_Transform(targetTile, kind, form);
			if (randomColorTiles.Length <= num2)
			{
				break;
			}
			targetTile = randomColorTiles[num2];
			num--;
			num2++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_TransformRainbow(PuzzleTile targetTile, int num)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num2 = 0;
		while (num > 0)
		{
			Skill_Transform(targetTile, Def.TILE_KIND.SPECIAL, Def.TILE_FORM.RAINBOW);
			if (randomColorTiles.Length <= num2)
			{
				break;
			}
			targetTile = randomColorTiles[num2];
			num--;
			num2++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_TransformBomb(PuzzleTile targetTile, int num)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num2 = 0;
		while (num > 0)
		{
			Def.TILE_KIND kind = RandomColorTile();
			Skill_Transform(targetTile, kind, Def.TILE_FORM.BOMB);
			if (randomColorTiles.Length <= num2)
			{
				break;
			}
			targetTile = randomColorTiles[num2];
			num--;
			num2++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_TransformPaint(PuzzleTile targetTile, int num)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num2 = 0;
		while (num > 0)
		{
			Def.TILE_KIND kind = RandomColorTile();
			Skill_Transform(targetTile, kind, Def.TILE_FORM.PAINT);
			if (randomColorTiles.Length <= num2)
			{
				break;
			}
			targetTile = randomColorTiles[num2];
			num--;
			num2++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_TransformWaveOrBomb(PuzzleTile targetTile, int num)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num2 = 0;
		while (num > 0)
		{
			Def.TILE_KIND kind = RandomColorTile();
			if (UnityEngine.Random.Range(0, 100) < 50)
			{
				if (UnityEngine.Random.Range(0, 100) < 50)
				{
					targetTile.ChangeForm(kind, Def.TILE_FORM.WAVE_H, 0f, 0f, false, true, 0, true);
				}
				else
				{
					targetTile.ChangeForm(kind, Def.TILE_FORM.WAVE_V, 0f, 0f, false, true, 0, true);
				}
			}
			else
			{
				targetTile.ChangeForm(kind, Def.TILE_FORM.BOMB, 0f, 0f, false, true, 0, true);
			}
			Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
			Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
			if (randomColorTiles.Length <= num2)
			{
				break;
			}
			targetTile = randomColorTiles[num2];
			num--;
			num2++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_TransformBombAndWave(PuzzleTile targetTile, int waveNum, int bombNum)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num = 0;
		while (bombNum > 0)
		{
			Def.TILE_KIND kind = RandomColorTile();
			targetTile.ChangeForm(kind, Def.TILE_FORM.BOMB, 0f, 0f, false, true, 0, true);
			Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
			Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
			if (randomColorTiles.Length <= num)
			{
				break;
			}
			targetTile = randomColorTiles[num];
			bombNum--;
			num++;
		}
		while (waveNum > 0)
		{
			Def.TILE_KIND kind2 = RandomColorTile();
			if (UnityEngine.Random.Range(0, 100) < 50)
			{
				targetTile.ChangeForm(kind2, Def.TILE_FORM.WAVE_H, 0f, 0f, false, true, 0, true);
			}
			else
			{
				targetTile.ChangeForm(kind2, Def.TILE_FORM.WAVE_V, 0f, 0f, false, true, 0, true);
			}
			Vector3 pos2 = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
			Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos2, Vector3.one, 0f, 0f);
			if (randomColorTiles.Length <= num)
			{
				break;
			}
			targetTile = randomColorTiles[num];
			waveNum--;
			num++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_TransformPaintAndWave(PuzzleTile targetTile, int paintNum, int waveNum)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num = 0;
		while (paintNum > 0)
		{
			Def.TILE_KIND kind = RandomColorTile();
			Skill_Transform(targetTile, kind, Def.TILE_FORM.PAINT);
			Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
			Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
			if (randomColorTiles.Length <= num)
			{
				break;
			}
			targetTile = randomColorTiles[num];
			paintNum--;
			num++;
		}
		while (waveNum > 0)
		{
			Def.TILE_KIND kind2 = RandomColorTile();
			if (UnityEngine.Random.Range(0, 100) < 50)
			{
				targetTile.ChangeForm(kind2, Def.TILE_FORM.WAVE_H, 0f, 0f, false, true, 0, true);
			}
			else
			{
				targetTile.ChangeForm(kind2, Def.TILE_FORM.WAVE_V, 0f, 0f, false, true, 0, true);
			}
			Vector3 pos2 = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
			Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos2, Vector3.one, 0f, 0f);
			if (randomColorTiles.Length <= num)
			{
				break;
			}
			targetTile = randomColorTiles[num];
			waveNum--;
			num++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_TransformPaintAndBomb(PuzzleTile targetTile, int paintNum, int bombNum)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		int num = 0;
		while (paintNum > 0)
		{
			Def.TILE_KIND kind = RandomColorTile();
			Skill_Transform(targetTile, kind, Def.TILE_FORM.PAINT);
			Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
			Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos, Vector3.one, 0f, 0f);
			if (randomColorTiles.Length <= num)
			{
				break;
			}
			targetTile = randomColorTiles[num];
			paintNum--;
			num++;
		}
		while (bombNum > 0)
		{
			Def.TILE_KIND kind2 = RandomColorTile();
			targetTile.ChangeForm(kind2, Def.TILE_FORM.BOMB, 0f, 0f, false, true, 0, true);
			Vector3 pos2 = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
			Util.CreateOneShotAnime("Debris", "EFFECT_SKILL_AFTER_CHANGE", mRoot, pos2, Vector3.one, 0f, 0f);
			if (randomColorTiles.Length <= num)
			{
				break;
			}
			targetTile = randomColorTiles[num];
			bombNum--;
			num++;
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_GimmickBreak(PuzzleTile targetTile, ref List<ByteVector2> targetPositions, float delayBase = 0f)
	{
		for (int i = 0; i < targetPositions.Count; i++)
		{
			PuzzleTile tile = GetTile(new IntVector2(targetTile.TilePos.x + targetPositions[i].x, targetTile.TilePos.y + targetPositions[i].y), false);
			float timer = delayBase * (float)i;
			if (tile != null)
			{
				BeginCrush(tile, timer, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(targetTile.TilePos.x + targetPositions[i].x, targetTile.TilePos.y + targetPositions[i].y), timer, false);
		}
		mGame.PlaySe("SE_SKILL_CRUSH", 2);
	}

	public void Skill_GimmickBreak2(PuzzleTile targetTile, ref List<ByteVector2> targetPositions, float delayPitch = 0f, float delayTime = 0f)
	{
		IntVector2 intVector = new IntVector2(0, 0);
		if (targetTile != null)
		{
			intVector = targetTile.TilePos;
		}
		for (int i = 0; i < targetPositions.Count; i++)
		{
			PuzzleTile tile = GetTile(new IntVector2(intVector.x + targetPositions[i].x, intVector.y + targetPositions[i].y), false);
			float timer = delayTime + delayPitch * (float)i;
			if (tile != null)
			{
				BeginCrush(tile, timer, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1);
			}
			BeginAttributeCrush(new IntVector2(intVector.x + targetPositions[i].x, intVector.y + targetPositions[i].y), timer, true);
		}
		mGame.PlaySe("SE_SKILL_CRUSH", 2);
	}

	public void Skill_RandomCrush(PuzzleTile targetTile, int num, int randomMax)
	{
		num += UnityEngine.Random.Range(0, randomMax);
		PuzzleTile[] randomColorTiles = GetRandomColorTiles();
		if (randomColorTiles.Length < 1)
		{
			return;
		}
		if (randomColorTiles.Length < num)
		{
			num = randomColorTiles.Length;
		}
		for (int i = 0; i < num; i++)
		{
			if (BeginCrush(randomColorTiles[i], 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, Def.TILE_EFFECT_CRUSH_SCORE, Def.TILE_KIND.NONE, 1))
			{
				BeginAttributeCrush(randomColorTiles[i].TilePos, 0f, false);
			}
		}
		mGame.PlaySe("SE_SKILL_CRUSH", 2);
	}

	public void Skill_RandomColorChange(int num, int randomMax)
	{
		Def.TILE_KIND kind = RandomColorTile();
		num += UnityEngine.Random.Range(0, randomMax);
		PuzzleTile[] randomColorTiles = GetRandomColorTiles(true);
		if (randomColorTiles.Length < num)
		{
			num = randomColorTiles.Length;
		}
		for (int i = 0; i < num; i++)
		{
			Skill_Transform(randomColorTiles[i], kind, Def.TILE_FORM.NONE);
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public void Skill_ScoreRatio(int moves)
	{
		mSkillExpandedScoreRatio = 0.1f;
		mSkillExpandedScoreRatioRemain += moves;
	}

	public void Skill_BreakLineH(PuzzleTile targetTile)
	{
		IntVector2 tilePos = targetTile.TilePos;
		SpecialWaveH(tilePos);
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_BREAK_LINE", mRoot, pos, Vector3.one, 0f, 0f);
		mGame.PlaySe("SE_WAVE", 2);
	}

	public void Skill_RandomBreakLineH(int num)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles();
		int num2 = 0;
		for (int i = 0; i < randomColorTiles.Length; i++)
		{
			IntVector2 tilePos = randomColorTiles[i].TilePos;
			bool flag = true;
			for (int num3 = i - 1; num3 >= 0; num3--)
			{
				if (randomColorTiles[num3].TilePos.y == tilePos.y)
				{
					flag = false;
				}
			}
			if (flag)
			{
				SpecialWaveH(tilePos);
				Vector3 pos = randomColorTiles[i].LocalPosition + new Vector3(0f, 0f, -2f);
				Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_BREAK_LINE", mRoot, pos, Vector3.one, 0f, 0f);
				num2++;
				if (num2 >= num)
				{
					break;
				}
			}
		}
		mGame.PlaySe("SE_WAVE", 2);
	}

	public void Skill_BreakLineV(PuzzleTile targetTile)
	{
		IntVector2 tilePos = targetTile.TilePos;
		SpecialWaveV(tilePos);
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_BREAK_LINE", mRoot, pos, Vector3.one, 90f, 0f);
		mGame.PlaySe("SE_WAVE", 2);
	}

	public void Skill_RandomBreakLineV(int num)
	{
		PuzzleTile[] randomColorTiles = GetRandomColorTiles();
		for (int i = 0; i < num && i < randomColorTiles.Length; i++)
		{
			IntVector2 tilePos = randomColorTiles[i].TilePos;
			bool flag = true;
			for (int num2 = i - 1; num2 >= 0; num2--)
			{
				if (randomColorTiles[num2].TilePos.y == tilePos.y)
				{
					flag = false;
				}
			}
			if (flag)
			{
				SpecialWaveV(tilePos);
				Vector3 pos = randomColorTiles[i].LocalPosition + new Vector3(0f, 0f, -2f);
				Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_BREAK_LINE", mRoot, pos, Vector3.one, 90f, 0f);
			}
		}
		mGame.PlaySe("SE_WAVE", 2);
	}

	public void Skill_BreakLineVH(PuzzleTile targetTile)
	{
		IntVector2 tilePos = targetTile.TilePos;
		SpecialCombo_WaveWave(tilePos);
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_BREAK_LINE", mRoot, pos, Vector3.one, 0f, 0f);
		Util.CreateOneShotAnime("Effect", "EFFECT_SKILL_BREAK_LINE", mRoot, pos, Vector3.one, 90f, 0f);
		mGame.PlaySe("SE_WAVE", 2);
	}

	public void Skill_BreakLineWaveBomb(PuzzleTile targetTile)
	{
		IntVector2 tilePos = targetTile.TilePos;
		SpecialCombo_WaveBomb(tilePos);
		Vector3 pos = targetTile.LocalPosition + new Vector3(0f, 0f, -2f);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOMB_STRIPE_H", mRoot, pos, Vector3.one, 0f, Def.WAVE_BOMB_FIRST_CROSS_TIME);
		Util.CreateOneShotAnime("Effect", "EFFECT_BOMB_STRIPE_H", mRoot, pos, Vector3.one, 90f, Def.WAVE_BOMB_SECOND_CROSS_TIME);
		mGame.PlaySe("SE_WAVE_BOMB", 2);
	}

	public void Skill_Shuffle(PuzzleTile targetTile)
	{
		mForceShuffleRequest = true;
	}

	public void Skill_Swap(PuzzleTile targetTile1, PuzzleTile targetTile2)
	{
		Def.TILE_FORM form = targetTile1.Form;
		Def.TILE_KIND kind = targetTile2.Kind;
		Def.TILE_FORM form2 = targetTile2.Form;
		Def.TILE_KIND kind2 = targetTile1.Kind;
		Skill_Transform(targetTile1, kind, form2);
		Skill_Transform(targetTile2, kind2, form);
	}

	public void Skill_TransformAreaColor(PuzzleTile targetTile, ref List<ByteVector2> targetPositions, Def.TILE_KIND color = Def.TILE_KIND.NONE, float delayBase = 0f)
	{
		if (color == Def.TILE_KIND.NONE)
		{
			color = RandomColorTile();
		}
		for (int i = 0; i < targetPositions.Count; i++)
		{
			PuzzleTile tile = GetTile(new IntVector2(targetTile.TilePos.x + targetPositions[i].x, targetTile.TilePos.y + targetPositions[i].y), false);
			float num = delayBase * (float)i;
			if (tile != null && tile.isColor())
			{
				Skill_Transform(tile, color, Def.TILE_FORM.NONE);
			}
		}
		mGame.PlaySe("SE_SPECIAL_TILE", 2);
	}

	public PuzzleTile GetSkillBallTransformTarget()
	{
		if (mGame.mTutorialManager.GetNextTutorial() == Def.TUTORIAL_INDEX.LEVEL9_2)
		{
			return GetTile(new IntVector2(5, 8), false);
		}
		if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugSkillBallMakeX != -1 && mGame.mDebugSkillBallMakeY != -1)
		{
			int mDebugSkillBallMakeX = mGame.mDebugSkillBallMakeX;
			int mDebugSkillBallMakeY = mGame.mDebugSkillBallMakeY;
			PuzzleTile tile = GetTile(new IntVector2(mDebugSkillBallMakeX, mDebugSkillBallMakeY), false);
			if (tile != null)
			{
				return tile;
			}
		}
		PuzzleTile[] colorTiles = GetColorTiles(true);
		if (colorTiles.Length == 0)
		{
			return null;
		}
		if (colorTiles.Length == 1)
		{
			return colorTiles[0];
		}
		return colorTiles[UnityEngine.Random.Range(0, colorTiles.Length)];
	}

	public void SkillBallTransform(PuzzleTile targetTile)
	{
		RemoveTile(targetTile, true);
		targetTile.ChangeForm(Def.TILE_KIND.SKILL_BALL, Def.TILE_FORM.NORMAL, 0f, 0f, false, true, 0, true);
	}

	public void BossGimmick_ChangeBrokenWall(PuzzleTile targetTile, Def.TILE_KIND kind)
	{
		RemoveTile(targetTile, false);
		PlaceAttribute(targetTile.TilePos, targetTile);
		targetTile.ChangeForm(kind, Def.TILE_FORM.NONE, 0f, 0f, false, true, 0, true);
	}

	public void BossGimmick_ChangeMovableWall(PuzzleTile targetTile, Def.TILE_KIND kind)
	{
		targetTile.ChangeForm(kind, Def.TILE_FORM.NONE, 0f, 0f, false, true, 0, true);
	}

	private IEnumerator BossGimmick_PlaceFamiliar(Vector3 orignPos, int num)
	{
		OnShakeScreen(0.5f);
		bool makeFamiliar = false;
		for (int j = 0; j < num; j++)
		{
			if (mFamiliarGimmicks.Count >= mBossCharacter.mAttackParam1)
			{
				continue;
			}
			PuzzleTile targetTile = null;
			PuzzleTile[] tiles = GetRandomColorTiles(true);
			PuzzleTile[] array = tiles;
			foreach (PuzzleTile tile in array)
			{
				ISMTileInfo info = mGame.mCurrentStage.GetTileInfo(tile.TilePos.x, tile.TilePos.y);
				if (info == null || !info.MakeObstacleArea)
				{
					continue;
				}
				bool hit = false;
				for (int i = 0; i < FamiliarGimmick.FamiliarSetting[0].smokeArea.Length; i++)
				{
					if (AllFamiliarHitCheck(tile.TilePos + FamiliarGimmick.FamiliarSetting[0].smokeArea[i]))
					{
						hit = true;
						break;
					}
				}
				if (!hit)
				{
					targetTile = tile;
					break;
				}
			}
			if (targetTile != null)
			{
				yield return StartCoroutine(BossGimmickTargetEffect(orignPos, targetTile.Sprite.transform.position));
				FamiliarGimmick gm = Util.CreateGameObject("Familiar", mRoot).AddComponent<FamiliarGimmick>();
				gm.Init(0, targetTile.TilePos, this);
				mFamiliarGimmicks.Add(gm);
				makeFamiliar = true;
			}
		}
		if (makeFamiliar)
		{
			yield return new WaitForSeconds(1.5f);
		}
		else
		{
			yield return new WaitForSeconds(0.5f);
		}
		WaitStateFinished();
	}

	private IEnumerator BossGimmickTargetEffect(Vector3 orignPos, Vector3 targetPos)
	{
		OneShotParticle particle = Util.CreateOneShotParticle("Change", "Particles/PuzzleGimmick/Boss_TargetChangeA", mRoot, new Vector3(0f, 0f, -30f), new Vector3(1f, 1f, 0.1f), 3f, 0f);
		float zBuf = particle.transform.position.z;
		float angle = 0f;
		Vector3 distance = targetPos - orignPos;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 180f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			particle.transform.position = new Vector3(orignPos.x + distance.x * ratio, orignPos.y + distance.y * ratio, zBuf);
			yield return 0;
		}
	}

	private bool IncreaseEnemyGimmick()
	{
		PuzzleTile[] randomEnemyGimmicTiles = GetRandomEnemyGimmicTiles();
		if (randomEnemyGimmicTiles.Length == 0)
		{
			return false;
		}
		IntVector2[] array = new IntVector2[4]
		{
			new IntVector2(0, 1),
			new IntVector2(0, -1),
			new IntVector2(1, 0),
			new IntVector2(-1, 0)
		};
		for (int i = 0; i < 50; i++)
		{
			int num = UnityEngine.Random.Range(0, array.Length);
			int num2 = UnityEngine.Random.Range(0, array.Length);
			if (num != num2)
			{
				IntVector2 intVector = array[num];
				array[num] = array[num2];
				array[num2] = intVector;
			}
		}
		for (int j = 0; j < randomEnemyGimmicTiles.Length; j++)
		{
			IntVector2 tilePos = randomEnemyGimmicTiles[j].TilePos;
			IntVector2[] array2 = array;
			foreach (IntVector2 intVector2 in array2)
			{
				PuzzleTile attribute = GetAttribute(tilePos + intVector2);
				if (attribute != null)
				{
					continue;
				}
				attribute = GetTile(tilePos + intVector2, false);
				if (attribute != null && attribute.isColor() && attribute.isNormalForm())
				{
					ISMTileInfo tileInfo = mGame.mCurrentStage.GetTileInfo(attribute.TilePos.x, attribute.TilePos.y);
					if (tileInfo != null && tileInfo.MakeObstacleArea)
					{
						StartCoroutine(IncreaseEnemyGimmickCoroutine(attribute));
						return true;
					}
				}
			}
		}
		return false;
	}

	private IEnumerator IncreaseEnemyGimmickCoroutine(PuzzleTile targetTile)
	{
		RemoveTile(targetTile, false);
		PlaceAttribute(targetTile.TilePos, targetTile);
		targetTile.ChangeForm(Def.TILE_KIND.INCREASE, Def.TILE_FORM.NORMAL, 0f, 0f, false, true, 0, true);
		mGame.PlaySe("SE_BOSS_ATTACK", 0, 1f, 0.2f);
		yield return 0;
		while (targetTile.isMove())
		{
			yield return 0;
		}
		mState.Reset(STATE.BEFOREPLAY, true);
	}

	private bool MoveFamiliarGimmick()
	{
		bool flag = false;
		for (int i = 0; i < mFamiliarGimmicks.Count; i++)
		{
			if (mFamiliarGimmicks[i].Move())
			{
				flag = true;
			}
		}
		if (flag)
		{
			StartCoroutine(MoveFamiliarGimmickCoroutine());
		}
		return flag;
	}

	private IEnumerator MoveFamiliarGimmickCoroutine()
	{
		bool busy = true;
		while (busy)
		{
			busy = false;
			for (int i = 0; i < mFamiliarGimmicks.Count; i++)
			{
				if (!mFamiliarGimmicks[i].IsStay())
				{
					busy = true;
				}
			}
			yield return 0;
		}
		mState.Reset(STATE.BEFOREPLAY, true);
	}

	public bool AllFamiliarHitCheck(IntVector2 tilePos)
	{
		for (int i = 0; i < mFamiliarGimmicks.Count; i++)
		{
			if (mFamiliarGimmicks[i].HitCheck(tilePos))
			{
				return true;
			}
		}
		return false;
	}

	private bool BeforePlayOrbGimmick(bool flgClearOnly)
	{
		bool flag = false;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (!flgClearOnly && tile != null && tile.isOrb() && !tile.mOrbCrush)
				{
					switch (tile.Kind)
					{
					case Def.TILE_KIND.ORB_WAVE_V1:
					case Def.TILE_KIND.ORB_WAVE_V2:
					case Def.TILE_KIND.ORB_WAVE_H1:
					case Def.TILE_KIND.ORB_WAVE_H2:
					case Def.TILE_KIND.ORB_BOMB1:
					case Def.TILE_KIND.ORB_BOMB2:
					case Def.TILE_KIND.ORB_BOSSDAMAGE1:
					case Def.TILE_KIND.ORB_BOSSDAMAGE2:
						flag = true;
						tile.CreateOrbDownAnime();
						tile.ChangeForm(tile.Kind - 1, Def.TILE_FORM.NONE, 0f, 0f, false, true, 0, true);
						break;
					}
				}
				if (tile != null)
				{
					tile.setOrbCrushFlg(false);
				}
			}
		}
		if (flag)
		{
			StartCoroutine(BeforePlayGimmickWaitCoroutine());
		}
		return flag;
	}

	private IEnumerator BeforePlayGimmickWaitCoroutine()
	{
		while (!IsMoveFinishAll())
		{
			yield return 0;
		}
		mState.Reset(STATE.BEFOREPLAY, true);
	}

	private bool BeforePlayTalismanGimmick()
	{
		BIJImage image = ResourceManager.LoadImage("PUZZLE_TILE").Image;
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[4]
		{
			new CHANGE_PART_INFO
			{
				partName = "change_000",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_001",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_002",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_003",
				spriteName = "null"
			}
		};
		bool flag = false;
		bool flag2 = false;
		if (mUserSwaped)
		{
			if (mTileCrushed)
			{
				flag = true;
				flag2 = true;
			}
		}
		else if (!mTileCrushed)
		{
			flag = true;
		}
		string key = string.Empty;
		bool flag3 = false;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile == null || !tile.isTalisman())
				{
					continue;
				}
				if (flag && tile.mTalismanFaceCrush)
				{
					tile.mTalismanFaceCrush = false;
				}
				else if (flag2)
				{
					array[0].spriteName = "null";
					array[1].spriteName = "null";
					array[2].spriteName = "null";
					array[3].spriteName = "null";
					switch (tile.Kind)
					{
					case Def.TILE_KIND.TALISMAN0_0:
						array[0].spriteName = "talisman_00";
						array[1].spriteName = "talisman_00";
						key = "EFFECT_TALISMAN_CHANGE1";
						tILE_KIND = Def.TILE_KIND.TALISMAN0_1;
						break;
					case Def.TILE_KIND.TALISMAN1_0:
						array[0].spriteName = "talisman_01";
						array[1].spriteName = "talisman_01";
						key = "EFFECT_TALISMAN_CHANGE1";
						tILE_KIND = Def.TILE_KIND.TALISMAN1_1;
						break;
					case Def.TILE_KIND.TALISMAN2_0:
						array[0].spriteName = "talisman_02";
						array[1].spriteName = "talisman_02";
						key = "EFFECT_TALISMAN_CHANGE1";
						tILE_KIND = Def.TILE_KIND.TALISMAN2_1;
						break;
					case Def.TILE_KIND.TALISMAN0_1:
						array[0].spriteName = "talisman_00";
						array[1].spriteName = "talisman_00";
						key = "EFFECT_TALISMAN_CHANGE2";
						tILE_KIND = Def.TILE_KIND.TALISMAN0_2;
						break;
					case Def.TILE_KIND.TALISMAN1_1:
						array[0].spriteName = "talisman_01";
						array[1].spriteName = "talisman_01";
						key = "EFFECT_TALISMAN_CHANGE2";
						tILE_KIND = Def.TILE_KIND.TALISMAN1_2;
						break;
					case Def.TILE_KIND.TALISMAN2_1:
						array[0].spriteName = "talisman_02";
						array[1].spriteName = "talisman_02";
						key = "EFFECT_TALISMAN_CHANGE2";
						tILE_KIND = Def.TILE_KIND.TALISMAN2_2;
						break;
					case Def.TILE_KIND.TALISMAN0_2:
						array[0].spriteName = "talisman_00";
						array[2].spriteName = "gimmick_RT_orb";
						key = "EFFECT_TALISMAN_CHANGE3";
						tILE_KIND = Def.TILE_KIND.TALISMAN0_3;
						break;
					case Def.TILE_KIND.TALISMAN1_2:
						array[0].spriteName = "talisman_01";
						array[2].spriteName = "gimmick_RT_mirror";
						key = "EFFECT_TALISMAN_CHANGE3";
						tILE_KIND = Def.TILE_KIND.TALISMAN1_3;
						break;
					case Def.TILE_KIND.TALISMAN2_2:
						array[0].spriteName = "talisman_02";
						array[2].spriteName = "gimmick_RT_sword";
						key = "EFFECT_TALISMAN_CHANGE3";
						tILE_KIND = Def.TILE_KIND.TALISMAN2_3;
						break;
					}
					if (tILE_KIND != Def.TILE_KIND.NONE)
					{
						flag3 = true;
						tile.ChangeForm(tILE_KIND, Def.TILE_FORM.NONE, 0f, 0f, false, true, 0, false);
						Util.CreateOneShotAnime("Effect", key, Root, new Vector3(tile.LocalPosition.x, tile.LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, array, image);
					}
				}
			}
		}
		if (flag3)
		{
			StartCoroutine(BeforePlayGimmickWaitCoroutine());
			mGame.PlaySe("SE_BOSS_ATTACK", 0);
		}
		return flag3;
	}

	private IEnumerator BeforePlayDianaMoveWaitCoroutine()
	{
		if (mDianaCharacter != null)
		{
			while (mDianaCharacter.GetStatus() == DianaGimmick.STATE.MOVE)
			{
				yield return 0;
			}
		}
		SetFallRock(false);
		mState.Reset(STATE.CHAIN, true);
	}

	public IEnumerator PuzzkeStartTalismanTransform()
	{
		BIJImage atlas = ResourceManager.LoadImage("PUZZLE_TILE").Image;
		CHANGE_PART_INFO[] partInfo = new CHANGE_PART_INFO[4]
		{
			new CHANGE_PART_INFO
			{
				partName = "change_000",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_001",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_002",
				spriteName = "null"
			},
			new CHANGE_PART_INFO
			{
				partName = "change_003",
				spriteName = "null"
			}
		};
		for (int cy = 0; cy < mFieldSize.y; cy++)
		{
			for (int cx = 0; cx < mFieldSize.x; cx++)
			{
				PuzzleTile tile = GetTile(new IntVector2(cx, cy), false);
				if (tile != null)
				{
					if (tile.Kind == Def.TILE_KIND.TALISMAN0_0)
					{
						tile.ChangeForm(Def.TILE_KIND.TALISMAN0_3, Def.TILE_FORM.NONE, 0f, 0f, false, true, 0, false);
						partInfo[0].spriteName = "talisman_00";
						partInfo[2].spriteName = "gimmick_RT_orb";
						Util.CreateOneShotAnime("Effect", "EFFECT_TALISMAN_CHANGE3_START", Root, new Vector3(tile.LocalPosition.x, tile.LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, partInfo, atlas);
					}
					if (tile.Kind == Def.TILE_KIND.TALISMAN1_0)
					{
						tile.ChangeForm(Def.TILE_KIND.TALISMAN1_3, Def.TILE_FORM.NONE, 0f, 0f, false, true, 0, false);
						partInfo[0].spriteName = "talisman_01";
						partInfo[2].spriteName = "gimmick_RT_mirror";
						Util.CreateOneShotAnime("Effect", "EFFECT_TALISMAN_CHANGE3_START", Root, new Vector3(tile.LocalPosition.x, tile.LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, partInfo, atlas);
					}
					if (tile.Kind == Def.TILE_KIND.TALISMAN2_0)
					{
						tile.ChangeForm(Def.TILE_KIND.TALISMAN2_3, Def.TILE_FORM.NONE, 0f, 0f, false, true, 0, false);
						partInfo[0].spriteName = "talisman_02";
						partInfo[2].spriteName = "gimmick_RT_sword";
						Util.CreateOneShotAnime("Effect", "EFFECT_TALISMAN_CHANGE3_START", Root, new Vector3(tile.LocalPosition.x, tile.LocalPosition.y, Def.PUZZLE_TILE_EFFECT_Z), Vector3.one, 0f, 0f, partInfo, atlas);
					}
				}
			}
		}
		mGame.PlaySe("SE_BOSS_ATTACK", 0);
		yield return new WaitForSeconds(0.7f);
	}

	public IEnumerator JewelGetChanceCoroutine(int a_num1, int a_num3, int a_num5, int a_num9)
	{
		PuzzleTile[] tiles = GetRandomColorTiles(true);
		if (tiles.Length == 1 && tiles[0] == null)
		{
			yield break;
		}
		mGame.PlaySe("SE_JEWEL_GET_CHANCE01", 2);
		List<PuzzleTile> changedTiles = new List<PuzzleTile>();
		int cnt = 0;
		int num1 = a_num1;
		int num2 = a_num3;
		int num3 = a_num5;
		int num4 = a_num9;
		while (cnt < tiles.Length)
		{
			PuzzleTile targetTile = tiles[cnt];
			if (targetTile == null)
			{
				break;
			}
			Def.TILE_FORM next = Def.TILE_FORM.NONE;
			if (num4 > 0)
			{
				next = Def.TILE_FORM.JEWEL9;
				num4--;
			}
			else if (num3 > 0)
			{
				next = Def.TILE_FORM.JEWEL5;
				num3--;
			}
			else if (num2 > 0)
			{
				next = Def.TILE_FORM.JEWEL3;
				num2--;
			}
			else if (num1 > 0)
			{
				next = Def.TILE_FORM.JEWEL;
				num1--;
			}
			if (next == Def.TILE_FORM.NONE)
			{
				break;
			}
			Skill_Transform(targetTile, Def.TILE_KIND.NONE, next);
			changedTiles.Add(targetTile);
			cnt++;
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		BIJImage atlas = ResourceManager.LoadImage("PUZZLE_TILE").Image;
		for (int i = 0; i < changedTiles.Count; i++)
		{
			PuzzleTile tile = changedTiles[i];
			TileData.DROP_ANIME_TYPE anime_type = TileData.DROP_ANIME_TYPE.NORMAL;
			tile.Data.GetAnimeType(ref anime_type, tile.Form);
			CHANGE_PART_INFO[] info;
			tile.Data.GetChangePartsInfo(anime_type, tile.Form, out info);
			OneShotAnime anime = Util.CreateOneShotAnime(changePartsArray: new CHANGE_PART_INFO[4]
			{
				new CHANGE_PART_INFO
				{
					partName = "change_000",
					spriteName = info[0].spriteName
				},
				new CHANGE_PART_INFO
				{
					partName = "change_001",
					spriteName = info[0].spriteName
				},
				new CHANGE_PART_INFO
				{
					partName = "change_002",
					spriteName = info[1].spriteName
				},
				new CHANGE_PART_INFO
				{
					partName = "change_003",
					spriteName = info[1].spriteName
				}
			}, name: "JewelGet" + i, key: "EFFECT_JEWEL_GETCHANCE", parent: tile.Sprite.gameObject, pos: new Vector3(0f, 0f, -1f), scale: Vector3.one, rotateDeg: 0f, delay: 0f, atlas: atlas);
		}
		yield return new WaitForSeconds(0.2f);
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.JEWELCHANCE_0))
		{
			yield return new WaitForSeconds(1.2f);
		}
		mJewelGetChance = true;
	}

	private Def.TUTORIAL_INDEX TutorialStartCheck()
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = Def.TUTORIAL_INDEX.NONE;
		if (GlobalVariables.Instance.IsDailyChallengePuzzle)
		{
			return tUTORIAL_INDEX;
		}
		switch (mSeriesNumber)
		{
		case Def.SERIES.SM_FIRST:
			switch (mStageNumber)
			{
			case 100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL1_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL1_0;
				}
				break;
			case 200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL2_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL2_0;
				}
				break;
			case 300:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL3_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL3_0;
				}
				break;
			case 400:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL4_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL4_0;
				}
				break;
			case 500:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL5_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL5_0;
				}
				break;
			case 600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL6_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL6_0;
				}
				break;
			case 700:
				if (mMoves == 0)
				{
					if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL7_1))
					{
						tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL7_1;
					}
				}
				else if (mMoves != 1 && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL7_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL7_0;
				}
				break;
			case 800:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL8_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL8_0;
				}
				break;
			case 900:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL9_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL9_0;
				}
				break;
			case 1000:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL10_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL10_0;
				}
				break;
			case 1100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL11_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL11_0;
				}
				break;
			case 1300:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL13_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL13_0;
				}
				break;
			case 1500:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL15_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL15_0;
				}
				break;
			case 1600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL16_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL16_0;
				}
				break;
			case 1800:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL18_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL18_0;
				}
				break;
			case 2100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL21_0, mGame.mPlayer.IsTutorialSkipped()))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL21_0;
				}
				break;
			case 2400:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL24_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL24_0;
				}
				break;
			case 2600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL26_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL26_0;
				}
				break;
			case 3100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL31_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL31_0;
				}
				break;
			case 4800:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL48_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL48_0;
				}
				break;
			case 6100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL61_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL61_0;
				}
				break;
			case 6200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL62_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL62_0;
				}
				break;
			case 7600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL76_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL76_0;
				}
				break;
			case 10600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL106_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL106_0;
				}
				break;
			case 10900:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL109_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL109_0;
				}
				break;
			case 13600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL136_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL136_0;
				}
				break;
			case 16700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL167_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL167_0;
				}
				break;
			case 19700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL197_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL197_0;
				}
				break;
			}
			if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE || mStageNumber > 6000 || !mGame.mTutorialManager.IsInitialTutorialCompleted() || mLoseCondition != 0)
			{
				break;
			}
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.CLEAN && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.SELECT_ONE_0))
			{
				int num2 = 0;
				for (int i = 0; i < mNormaKindArray.Length; i++)
				{
					num2 += mGame.mCurrentStage.GetTargetNum(mNormaKindArray[i]) - GetExitCount(mNormaKindArray[i]);
				}
				if (num2 == 1 && CheckGotStars() > 0 && mMoves == 1)
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.SELECT_ONE_0;
				}
			}
			else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.COLLECT && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.SELECT_COLLECT_0))
			{
				int num3 = 0;
				for (int j = 0; j < mNormaKindArray.Length; j++)
				{
					num3 += mGame.mCurrentStage.GetTargetNum(mNormaKindArray[j]) - GetExitCount(mNormaKindArray[j]);
				}
				if (num3 != 1 || CheckGotStars() <= 0 || mMoves != 1)
				{
					break;
				}
				for (int k = 0; k < mFieldSize.y; k++)
				{
					for (int l = 0; l < mFieldSize.x; l++)
					{
						PuzzleTile tile = GetTile(new IntVector2(l, k), false);
						if (tile != null && tile.isIngredient())
						{
							tUTORIAL_INDEX = Def.TUTORIAL_INDEX.SELECT_COLLECT_0;
						}
					}
				}
			}
			else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.EATEN && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.COLOR_CRUSH_0))
			{
				int num4 = 0;
				int num5 = 0;
				for (int m = 0; m < mNormaKindArray.Length; m++)
				{
					num5 += mGame.mCurrentStage.GetTargetNum(mNormaKindArray[m]);
					num4 += GetExitCount(mNormaKindArray[m]);
				}
				if ((float)num4 / (float)num5 >= 0.8f && CheckGotStars() > 0 && mMoves > 0 && mMoves <= 5)
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.COLOR_CRUSH_0;
				}
			}
			break;
		case Def.SERIES.SM_R:
			switch (mStageNumber)
			{
			case 3200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL32R_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL32R_0;
				}
				break;
			case 6200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL62R_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL62R_0;
				}
				break;
			}
			break;
		case Def.SERIES.SM_S:
			switch (mStageNumber)
			{
			case 100:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL1S_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL1S_0;
				}
				break;
			case 200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL62R_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL62R_0;
				}
				break;
			case 7600:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL76S_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL76S_0;
				}
				break;
			case 13700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL137S_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL137S_0;
				}
				break;
			}
			break;
		case Def.SERIES.SM_SS:
			switch (mStageNumber)
			{
			case 200:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL62R_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL62R_0;
				}
				break;
			case 700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL7SS_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL7SS_0;
				}
				break;
			case 2300:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL23SS_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL23SS_0;
				}
				break;
			case 4700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL47SS_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL47SS_0;
				}
				break;
			case 10700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL107SS_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL107SS_0;
				}
				break;
			}
			break;
		case Def.SERIES.SM_STARS:
			switch (mStageNumber)
			{
			case 4700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL47StarS_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL47StarS_0;
				}
				break;
			case 10700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL107StarS_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL107StarS_0;
				}
				break;
			case 13700:
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.TALISMAN_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.TALISMAN_0;
				}
				break;
			}
			break;
		case Def.SERIES.SM_GF00:
		{
			int num = mStageNumber;
			break;
		}
		case Def.SERIES.SM_EV:
		{
			SMEventPageData sMEventPageData = mGame.mCurrentMapPageData as SMEventPageData;
			PlayerEventData data;
			mGame.mPlayer.Data.GetMapData(Def.SERIES.SM_EV, mGame.mPlayer.Data.CurrentEventID, out data);
			if (data.EventID == 0 && data.CourseID == 0 && mStageNumber == 200)
			{
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL31_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL31_0;
				}
			}
			else if (data.EventID == 24 && data.CourseID == 0 && mStageNumber == 200)
			{
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.LEVEL62R_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.LEVEL62R_0;
				}
			}
			else if (sMEventPageData.EventSetting.EventType == Def.EVENT_TYPE.SM_LABYRINTH)
			{
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.JEWEL_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.JEWEL_0;
				}
				else if (mJewelGetChance && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.JEWELCHANCE_0))
				{
					tUTORIAL_INDEX = Def.TUTORIAL_INDEX.JEWELCHANCE_0;
				}
			}
			else if (data.EventID == 72 && data.CourseID == 0 && mStageNumber == 500 && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.TALISMAN_0))
			{
				tUTORIAL_INDEX = Def.TUTORIAL_INDEX.TALISMAN_0;
			}
			break;
		}
		}
		return tUTORIAL_INDEX;
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
		if (mGame.mTutorialManager.IsMessageFakeClosed() && Def.TUTORIAL_START_DATA[(int)index].messageDisp)
		{
			mGame.mTutorialManager.MessageFakeOpen();
		}
		switch (index)
		{
		case Def.TUTORIAL_INDEX.LEVEL1_0:
			MakeReservedTilesList(LEVEL1_reservedTiles);
			break;
		case Def.TUTORIAL_INDEX.LEVEL1_3:
			WantMove(LEVEL1_3_tile, null, null, Def.DIR.RIGHT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL1_4:
			WantMove(LEVEL1_4_tile, null, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL1_5:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL1_6:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL1_7:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL2_0:
			MakeReservedTilesList(LEVEL2_reservedTiles);
			break;
		case Def.TUTORIAL_INDEX.LEVEL2_1:
			WantMove(LEVEL2_1_tile, null, null, Def.DIR.LEFT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL2_2:
			WantMove(LEVEL2_2_tile, null, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL2_3:
			WantMove(LEVEL2_3_tile, null, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL2_4:
			WantMove(LEVEL2_4_tile, null, null, Def.DIR.LEFT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL2_5:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL3_0:
			MakeReservedTilesList(LEVEL3_reservedTiles);
			break;
		case Def.TUTORIAL_INDEX.LEVEL3_1:
			WantMove(LEVEL3_1_tile, null, null, Def.DIR.RIGHT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL3_2:
			WantMove(LEVEL3_2_tile, null, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL4_0:
			MakeReservedTilesList(LEVEL4_reservedTiles);
			break;
		case Def.TUTORIAL_INDEX.LEVEL4_1:
			WantMove(LEVEL4_1_tile, null, null, Def.DIR.RIGHT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL4_2:
			WantMove(LEVEL4_2_tile, null, null, Def.DIR.LEFT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL5_0:
			MakeReservedTilesList(LEVEL5_reservedTiles);
			break;
		case Def.TUTORIAL_INDEX.LEVEL5_1:
			WantMove(LEVEL5_1_tile, null, null, Def.DIR.RIGHT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL5_2:
			WantMove(LEVEL5_2_tile, null, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL6_0:
			MakeReservedTilesList(LEVEL6_reservedTiles);
			break;
		case Def.TUTORIAL_INDEX.LEVEL6_1:
			WantMove(LEVEL6_1_tile, null, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL7_0:
		{
			MakeReservedTilesList(LEVEL7_reservedTiles);
			mMoves = 1;
			OnChangeMoves(mMoves);
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL7_2:
			OnTutorialTrigger(Def.TUTORIAL_INDEX.LEVEL7_2);
			break;
		case Def.TUTORIAL_INDEX.LEVEL7_3:
			OnTutorialTrigger(Def.TUTORIAL_INDEX.LEVEL7_3);
			break;
		case Def.TUTORIAL_INDEX.LEVEL8_0:
		{
			MakeReservedTilesList(LEVEL8_reservedTiles);
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL8_1:
			WantMove(LEVEL8_1_tile, LEVEL8_1_tile, null, Def.DIR.RIGHT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL9_0:
			MakeReservedTilesList(LEVEL9_reservedTiles);
			break;
		case Def.TUTORIAL_INDEX.LEVEL9_1:
			WantMove(LEVEL9_1_tile, null, null, Def.DIR.DOWN);
			OnTutorialTrigger(index);
			break;
		case Def.TUTORIAL_INDEX.LEVEL9_2:
			TutorialHighLight(LEVEL9_2_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL9_3:
			WantTap(LEVEL9_3_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL10_0:
		{
			GameObject target = mBossCharacter.gameObject;
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			mGame.mTutorialManager.SetTutorialArrowOffset(Def.TILE_PITCH_H / 2f, 0f);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL10_1:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL10_2:
		{
			GameObject target = mBossCharacter.gameObject;
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			mGame.mTutorialManager.SetTutorialArrowOffset(Def.TILE_PITCH_H * 1.25f, Def.TILE_PITCH_V * 1.4f);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL10_4:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL11_0:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL11_1:
			TutorialHighLight(LEVEL11_1_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL11_2:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL13_0:
			TutorialHighLight(null, LEVEL13_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL15_0:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL15_1:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL16_0:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL18_0:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL21_0:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL21_1:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL24_0:
			TutorialHighLight(null, LEVEL24_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL26_0:
			TutorialHighLight(LEVEL26_0_tile, LEVEL26_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL31_0:
		{
			MakeReservedTilesList(LEVEL31_reservedTiles);
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL31_1:
			WantMove(LEVEL31_1_tile, LEVEL31_1_tile, null, Def.DIR.LEFT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL31_2:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL48_0:
			TutorialHighLight(LEVEL48_0_tile, LEVEL48_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL61_0:
			TutorialHighLight(LEVEL61_0_tile, LEVEL61_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL62_1:
			TutorialHighLight(LEVEL62_1_tile, LEVEL62_1_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL76_0:
			TutorialHighLight(null, LEVEL76_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.SELECT_ONE_2:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target, true);
			mGame.mTutorialManager.SetTutorialArrowOffset(0f, 34f);
			break;
		}
		case Def.TUTORIAL_INDEX.SELECT_ONE_3:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialFinger(mGame.mAnchor.gameObject, target, Def.DIR.NONE, true);
			mGame.mTutorialManager.SetTutorialArrowOffset(0f, 63f);
			mGame.mTutorialManager.AddAllowedButton(target);
			mGame.mPlayer.AddFreeBoosterNum(Def.BOOSTER_KIND.SELECT_ONE, 1);
			break;
		}
		case Def.TUTORIAL_INDEX.SELECT_COLLECT_2:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target, true);
			mGame.mTutorialManager.SetTutorialArrowOffset(0f, 34f);
			break;
		}
		case Def.TUTORIAL_INDEX.SELECT_COLLECT_3:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialFinger(mGame.mAnchor.gameObject, target, Def.DIR.NONE, true);
			mGame.mTutorialManager.SetTutorialArrowOffset(0f, 63f);
			mGame.mTutorialManager.AddAllowedButton(target);
			mGame.mPlayer.AddFreeBoosterNum(Def.BOOSTER_KIND.SELECT_COLLECT, 1);
			break;
		}
		case Def.TUTORIAL_INDEX.COLOR_CRUSH_2:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target, true);
			mGame.mTutorialManager.SetTutorialArrowOffset(0f, 34f);
			break;
		}
		case Def.TUTORIAL_INDEX.COLOR_CRUSH_3:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialFinger(mGame.mAnchor.gameObject, target, Def.DIR.NONE, true);
			mGame.mTutorialManager.SetTutorialArrowOffset(0f, 63f);
			mGame.mTutorialManager.AddAllowedButton(target);
			mGame.mPlayer.AddFreeBoosterNum(Def.BOOSTER_KIND.COLOR_CRUSH, 1);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL106_0:
		{
			GameObject target = mBossCharacter.gameObject;
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			mGame.mTutorialManager.SetTutorialArrowOffset(Def.TILE_PITCH_H / 2f, 0f);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL106_1:
		{
			GameObject target = mBossCharacter.gameObject;
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			mGame.mTutorialManager.SetTutorialArrowOffset(Def.TILE_PITCH_H * 1.25f, Def.TILE_PITCH_V * 1.4f);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL109_0:
			TutorialHighLight(LEVEL109_0_tile, LEVEL109_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL136_0:
			TutorialHighLight(null, LEVEL136_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL167_0:
			MakeReservedTilesList(LEVEL167_reservedTiles);
			TutorialHighLight(LEVEL167_0_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL167_1:
			WantMove(LEVEL167_1_tile, null, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL197_0:
			TutorialHighLight(LEVEL197_0_tile, LEVEL197_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL32R_0:
			TutorialHighLight(LEVEL32R_0_tile, LEVEL32R_0_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL62R_1:
			WantMove(LEVEL62R_0_tile, LEVEL62R_0_tile, null, Def.DIR.RIGHT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL62R_2:
			WantMove(LEVEL62R_1_tile, LEVEL62R_1_tile, null, Def.DIR.DOWN);
			break;
		case Def.TUTORIAL_INDEX.LEVEL62R_4:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL1S_0:
			TutorialHighLight(LEVEL1S_0_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL76S_2:
			WantMove(LEVEL76S_0_tile, null, null, Def.DIR.RIGHT);
			break;
		case Def.TUTORIAL_INDEX.LEVEL137S_0:
		{
			GameObject target = mBossCharacter.gameObject;
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			mGame.mTutorialManager.SetTutorialArrowOffset(Def.TILE_PITCH_H / 2f, 0f);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL137S_1:
		{
			GameObject target = mBossCharacter.gameObject;
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			mGame.mTutorialManager.SetTutorialArrowOffset(Def.TILE_PITCH_H * 1.25f, Def.TILE_PITCH_V * 1.4f);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL7SS_0:
			TutorialHighLight(LEVEL7SS_0_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL23SS_0:
			TutorialHighLight(LEVEL23SS_0_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL47SS_0:
			TutorialHighLight(LEVEL47SS_0_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL107SS_1:
			TutorialHighLight(LEVEL107SS_1_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL107SS_2:
			TutorialHighLight(null, LEVEL107SS_2_tile, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL107SS_3:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL47StarS_0:
			TutorialHighLight(LEVEL47StarS_0_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.LEVEL47StarS_1:
			TutorialHighLight(LEVEL47StarS_1_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.JEWEL_0:
		case Def.TUTORIAL_INDEX.JEWELCHANCE_0:
		{
			List<IntVector2> list2 = new List<IntVector2>();
			PuzzleTile[] colorTiles = GetColorTiles(false, true);
			for (int j = 0; j < colorTiles.Length; j++)
			{
				if (colorTiles[j].isJewel())
				{
					list2.Add(colorTiles[j].TilePos);
				}
			}
			TutorialHighLight(list2.ToArray(), null, null);
			break;
		}
		case Def.TUTORIAL_INDEX.LEVEL107StarS_0:
			TutorialHighLight(LEVEL107StarS_0_tile, null, null);
			break;
		case Def.TUTORIAL_INDEX.TALISMAN_1:
		{
			List<IntVector2> list = new List<IntVector2>();
			PuzzleTile[] talismanFaceTiles = GetTalismanFaceTiles();
			for (int i = 0; i < talismanFaceTiles.Length; i++)
			{
				list.Add(talismanFaceTiles[i].TilePos);
			}
			TutorialHighLight(list.ToArray(), null, null);
			break;
		}
		case Def.TUTORIAL_INDEX.TALISMAN_5:
		{
			GameObject target = OnGetTutorialArrowTarget(index);
			mGame.mTutorialManager.SetTutorialArrow(mGame.mAnchor.gameObject, target);
			break;
		}
		}
		if (Def.TUTORIAL_START_DATA[(int)index].tapWait)
		{
			mState.Reset(STATE.WAIT, true);
			NextState = STATE.BEFOREPLAY;
			return;
		}
		switch (index)
		{
		case Def.TUTORIAL_INDEX.LEVEL1_3:
		case Def.TUTORIAL_INDEX.LEVEL1_4:
		case Def.TUTORIAL_INDEX.LEVEL2_1:
		case Def.TUTORIAL_INDEX.LEVEL2_2:
		case Def.TUTORIAL_INDEX.LEVEL2_3:
		case Def.TUTORIAL_INDEX.LEVEL2_4:
		case Def.TUTORIAL_INDEX.LEVEL3_1:
		case Def.TUTORIAL_INDEX.LEVEL3_2:
		case Def.TUTORIAL_INDEX.LEVEL4_1:
		case Def.TUTORIAL_INDEX.LEVEL4_2:
		case Def.TUTORIAL_INDEX.LEVEL5_1:
		case Def.TUTORIAL_INDEX.LEVEL5_2:
		case Def.TUTORIAL_INDEX.LEVEL6_1:
		case Def.TUTORIAL_INDEX.LEVEL8_1:
		case Def.TUTORIAL_INDEX.LEVEL9_1:
		case Def.TUTORIAL_INDEX.LEVEL9_3:
		case Def.TUTORIAL_INDEX.LEVEL31_1:
		case Def.TUTORIAL_INDEX.LEVEL167_1:
		case Def.TUTORIAL_INDEX.LEVEL62R_1:
		case Def.TUTORIAL_INDEX.LEVEL62R_2:
		case Def.TUTORIAL_INDEX.LEVEL76S_2:
			mState.Reset(STATE.PLAY, true);
			break;
		case Def.TUTORIAL_INDEX.LEVEL7_1:
		case Def.TUTORIAL_INDEX.LEVEL7_2:
		case Def.TUTORIAL_INDEX.LEVEL9_4:
			break;
		default:
			mState.Reset(STATE.WAIT, true);
			NextState = STATE.BEFOREPLAY;
			break;
		}
	}

	private void MakeReservedTilesList(int[][] tbl)
	{
		for (int i = 0; i < mReservedFillTiles.Length; i++)
		{
			mReservedFillTiles[i].Clear();
		}
		for (int j = 0; j < tbl.Length; j++)
		{
			if (mReservedFillTiles.Length > j)
			{
				int[] array = tbl[j];
				for (int k = 0; k < array.Length; k++)
				{
					mReservedFillTiles[j].Add((Def.TILE_KIND)array[k]);
				}
			}
		}
	}

	private void WantMove(IntVector2[] tiles, IntVector2[] attributes1, IntVector2[] attributes2, Def.DIR dir)
	{
		PuzzleTile tile = GetTile(tiles[0], false);
		PuzzleTile tile2 = GetTile(tiles[1], false);
		mGame.mTutorialManager.SetWantMove(tile, dir, tile2);
		TutorialHighLight(tiles, attributes1, attributes2);
		mGame.mTutorialManager.SetTutorialFinger(mGame.mAnchor.gameObject, tile.Sprite.gameObject, dir);
	}

	private void WantTap(IntVector2[] tiles, IntVector2[] attributes1, IntVector2[] attributes2)
	{
		PuzzleTile tile = GetTile(tiles[0], true);
		PuzzleTile tile2 = GetTile(tiles[0], true);
		mGame.mTutorialManager.SetWantMove(tile, Def.DIR.NONE, tile2);
		TutorialHighLight(tiles, attributes1, attributes2);
		mGame.mTutorialManager.SetTutorialFinger(mGame.mAnchor.gameObject, tile.Sprite.gameObject, Def.DIR.NONE);
	}

	public void TutorialHighLight(IntVector2[] tiles, IntVector2[] attributes1, IntVector2[] attributes2)
	{
		mGame.mTutorialManager.ClearHighLightTiles();
		if (tiles != null)
		{
			for (int i = 0; i < tiles.Length; i++)
			{
				PuzzleTile tile = GetTile(tiles[i], false);
				if (tile != null)
				{
					mGame.mTutorialManager.AddHighLightTile(tile);
				}
			}
		}
		if (attributes1 != null)
		{
			for (int j = 0; j < attributes1.Length; j++)
			{
				PuzzleTile tile = GetAttribute(attributes1[j], 0);
				if (tile != null)
				{
					mGame.mTutorialManager.AddHighLightTile(tile);
				}
			}
		}
		if (attributes2 != null)
		{
			for (int k = 0; k < attributes2.Length; k++)
			{
				PuzzleTile tile = GetAttribute(attributes2[k], 0);
				if (tile != null)
				{
					mGame.mTutorialManager.AddHighLightTile(tile);
				}
			}
		}
		for (int l = 0; l < mFieldSize.y; l++)
		{
			for (int m = 0; m < mFieldSize.x; m++)
			{
				if (mTile[l, m] != null)
				{
					if (mGame.mTutorialManager.IsHighLightTile(mTile[l, m]))
					{
						mTile[l, m].GrayOut(false);
					}
					else
					{
						mTile[l, m].GrayOut(true);
					}
				}
				if (mAttribute[0, l, m] != null)
				{
					if (mGame.mTutorialManager.IsHighLightTile(mAttribute[0, l, m]))
					{
						mAttribute[0, l, m].GrayOut(false);
					}
					else
					{
						mAttribute[0, l, m].GrayOut(true);
					}
				}
				if (mAttribute[1, l, m] != null)
				{
					if (mGame.mTutorialManager.IsHighLightTile(mAttribute[1, l, m]))
					{
						mAttribute[1, l, m].GrayOut(false);
					}
					else
					{
						mAttribute[1, l, m].GrayOut(true);
					}
				}
			}
		}
		mTutorialHighLightEnable = true;
	}

	public void TutorialHighLightEnd()
	{
		if (!mTutorialHighLightEnable)
		{
			return;
		}
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				if (mTile[i, j] != null)
				{
					mTile[i, j].GrayOut(false);
				}
				if (mAttribute[0, i, j] != null)
				{
					mAttribute[0, i, j].GrayOut(false);
				}
				if (mAttribute[1, i, j] != null)
				{
					mAttribute[1, i, j].GrayOut(false);
				}
			}
		}
		mTutorialHighLightEnable = false;
	}

	public void TutorialMessageFinish()
	{
		Def.TUTORIAL_INDEX currentTutorial = mGame.mTutorialManager.GetCurrentTutorial();
		if (currentTutorial != Def.TUTORIAL_INDEX.NONE)
		{
			mGame.mTutorialManager.TutorialComplete(currentTutorial);
			if (mGame.mTutorialManager.GetCurrentTutorialData().messageDisp)
			{
				mGame.mTutorialManager.MessageFakeClose(null);
			}
		}
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX index2 = mGame.mTutorialManager.TutorialComplete(index);
		switch (index)
		{
		case Def.TUTORIAL_INDEX.LEVEL7_1:
			TutorialStart(index2);
			return;
		case Def.TUTORIAL_INDEX.LEVEL11_2:
		case Def.TUTORIAL_INDEX.LEVEL13_1:
		case Def.TUTORIAL_INDEX.LEVEL24_0:
		case Def.TUTORIAL_INDEX.LEVEL26_0:
		case Def.TUTORIAL_INDEX.LEVEL48_1:
		case Def.TUTORIAL_INDEX.LEVEL61_1:
		case Def.TUTORIAL_INDEX.LEVEL62_2:
		case Def.TUTORIAL_INDEX.LEVEL76_1:
		case Def.TUTORIAL_INDEX.LEVEL109_0:
		case Def.TUTORIAL_INDEX.LEVEL136_0:
		case Def.TUTORIAL_INDEX.LEVEL197_0:
		case Def.TUTORIAL_INDEX.LEVEL32R_2:
		case Def.TUTORIAL_INDEX.LEVEL1S_0:
		case Def.TUTORIAL_INDEX.LEVEL7SS_3:
		case Def.TUTORIAL_INDEX.LEVEL23SS_1:
		case Def.TUTORIAL_INDEX.LEVEL47SS_0:
		case Def.TUTORIAL_INDEX.LEVEL107SS_2:
		case Def.TUTORIAL_INDEX.LEVEL47StarS_2:
		case Def.TUTORIAL_INDEX.JEWEL_1:
		case Def.TUTORIAL_INDEX.JEWELCHANCE_2:
		case Def.TUTORIAL_INDEX.LEVEL107StarS_1:
		case Def.TUTORIAL_INDEX.TALISMAN_2:
			TutorialHighLightEnd();
			break;
		}
		WaitStateFinished();
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			TutorialHighLightEnd();
			mGame.mTutorialManager.TutorialSkip(index);
			WaitStateFinished();
		}
	}

	protected override void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		TimeScale = 1f;
	}

	protected virtual void Start()
	{
	}

	public void Update()
	{
		float deltaTime = Time.deltaTime * TimeScale;
		int count = mOneShotList.Count;
		for (int i = 0; i < count; i++)
		{
			if (mOneShotList[i] != null)
			{
				mOneShotList[i].SetTimeScale(TimeScale);
			}
		}
		switch (mState.GetStatusAndMarkUsed())
		{
		case STATE.WAIT:
			StateWAIT(deltaTime);
			break;
		case STATE.INIT:
			StateINIT(deltaTime);
			break;
		case STATE.BEFOREPLAY:
			StateBEFOREPLAY(deltaTime);
			break;
		case STATE.PLAY:
			StatePLAY(deltaTime);
			break;
		case STATE.CHAIN:
			StateCHAIN(deltaTime);
			break;
		case STATE.SHUFFLE:
			StateSHUFFLE(deltaTime);
			break;
		case STATE.SHUFFLE_WAIT:
			StateSHUFFLE_WAIT(deltaTime);
			break;
		case STATE.SELECT_BOOSTER_TARGET:
			StateSELECT_BOOSTER_TARGET(deltaTime);
			break;
		case STATE.SELECT_SKILL_TARGET:
			StateSELECT_SKILL_TARGET(deltaTime);
			break;
		case STATE.PAUSE:
			StatePAUSE(deltaTime);
			break;
		case STATE.WIN:
			StateWIN(deltaTime);
			break;
		case STATE.LOSE:
			StateLOSE(deltaTime);
			break;
		case STATE.SKILLBALLTRANSFORM:
			StateSKILLBALLTRANSFORM(deltaTime);
			break;
		case STATE.SPECIALTILECRUSH:
			StateSPECIALTILECRUSH(deltaTime);
			break;
		case STATE.CLEARBONUS:
			StateCLEARBONUS(deltaTime);
			break;
		case STATE.FINISH:
			StateFINISH(deltaTime);
			break;
		case STATE.INCREASE_WAIT:
			StateINCREASE_WAIT(deltaTime);
			break;
		}
		mState.Update();
	}

	private void StateWAIT(float deltaTime)
	{
	}

	private void StateINIT(float deltaTime)
	{
		if (mState.IsChanged())
		{
			foreach (Def.BOOSTER_KIND mUseBooster in mGame.mUseBoosters)
			{
				mUseBoostersTemp.Add(mUseBooster);
			}
			mGame.mUseBoosters.Clear();
		}
		if (IsMoveFinishAll())
		{
			mState.Reset(STATE.BEFOREPLAY, true);
		}
		UpdateTile(deltaTime);
	}

	private void StateBEFOREPLAY(float deltaTime)
	{
		if (mCollectEffectBusy > 0)
		{
			return;
		}
		OnPuzzleHudAdjust();
		if (mBossCharacter != null && mBossCharacter.IsDeadBusy())
		{
			return;
		}
		ResetCapture();
		UpdateDifficultyMode();
		if (StageFinishCheck())
		{
			InputEnable = false;
			return;
		}
		if (!mForceShuffleRequest)
		{
			UpdateAppealList();
		}
		if (mShuffleRequest || mForceShuffleRequest)
		{
			InputEnable = false;
			mState.Change(STATE.SHUFFLE);
			return;
		}
		if (OnBeforePlay())
		{
			mState.Reset(STATE.WAIT, true);
			NextState = STATE.BEFOREPLAY;
			return;
		}
		if (mReservedCrushSkillBall != null)
		{
			BeginTapCrush(mReservedCrushSkillBall);
			mReservedCrushSkillBall = null;
			return;
		}
		if (MoveFamiliarGimmick())
		{
			mState.Change(STATE.INCREASE_WAIT);
			NextState = STATE.BEFOREPLAY;
			return;
		}
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = TutorialStartCheck();
		Def.TUTORIAL_INDEX nextTutorial = mGame.mTutorialManager.GetNextTutorial();
		if (nextTutorial != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(nextTutorial);
			return;
		}
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(tUTORIAL_INDEX);
			return;
		}
		if (mUseBoostersTemp.Count > 0)
		{
			if (OnBoosterUse(mUseBoostersTemp[0], null, true))
			{
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.BEFOREPLAY;
			}
			mUseBoostersTemp.RemoveAt(0);
			if (mUseBoostersTemp.Count == 0)
			{
				mUseBeforeBoosters = true;
			}
			return;
		}
		if (OnBeforePuzzleSpecialOfferCheck())
		{
			mState.Reset(STATE.WAIT, true);
			NextState = STATE.BEFOREPLAY;
			return;
		}
		if (mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL11_4))
		{
			if (mUseBeforeBoosters)
			{
				mUseBeforeBoosters = false;
				mEnablePlusChance = true;
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.BEFOREPLAY;
				return;
			}
			if (mGame.mNoSendUseBoosters.Count > 0)
			{
				foreach (Def.BOOSTER_KIND mNoSendUseBooster in mGame.mNoSendUseBoosters)
				{
					mNoRegisterUseBoostersTemp.Add(mNoSendUseBooster);
				}
				mGame.mNoSendUseBoosters.Clear();
			}
			if (mNoRegisterUseBoostersTemp.Count > 0)
			{
				if (OnBoosterUse(mNoRegisterUseBoostersTemp[0], null, false))
				{
					mState.Reset(STATE.WAIT, true);
					NextState = STATE.BEFOREPLAY;
				}
				mNoRegisterUseBoostersTemp.RemoveAt(0);
				return;
			}
			if (mUseScoreUpChance)
			{
				if (OnBoosterEffectOnly(Def.BOOSTER_KIND.ADD_SCORE, null))
				{
					mState.Reset(STATE.WAIT, true);
					NextState = STATE.BEFOREPLAY;
				}
				mUseScoreUpChance = false;
				return;
			}
		}
		bool flag = false;
		if (mDianaCharacter != null && mDianaCharacter.Move())
		{
			StartCoroutine(BeforePlayDianaMoveWaitCoroutine());
			SetFallRock(true);
			mState.Change(STATE.INCREASE_WAIT);
			return;
		}
		if (!mBeforePlayOrbCheck)
		{
			mBeforePlayOrbCheck = true;
			if (BeforePlayOrbGimmick(!mTileCrushed))
			{
				mState.Change(STATE.INCREASE_WAIT);
				return;
			}
		}
		if (!mBeforePlayTalismanCheck)
		{
			mBeforePlayTalismanCheck = true;
			if (BeforePlayTalismanGimmick())
			{
				mState.Change(STATE.INCREASE_WAIT);
				return;
			}
		}
		if (mWinCondition == Def.STAGE_WIN_CONDITION.DEFEAT)
		{
			if (mBossCharacter != null && mTileCrushed)
			{
				flag = mBossCharacter.AttackCountDown(1);
			}
			if (flag)
			{
				mState.Change(STATE.WAIT);
				NextState = STATE.BEFOREPLAY;
			}
			else
			{
				mState.Change(STATE.PLAY);
			}
		}
		else if (mWinCondition == Def.STAGE_WIN_CONDITION.ENEMY)
		{
			if (mTileCrushed && !mEnemyTileCrushed)
			{
				flag = IncreaseEnemyGimmick();
			}
			if (flag)
			{
				mState.Change(STATE.INCREASE_WAIT);
				NextState = STATE.BEFOREPLAY;
			}
			else
			{
				mState.Change(STATE.PLAY);
			}
		}
		else
		{
			mState.Change(STATE.PLAY);
		}
		mTileCrushed = false;
		mEnemyTileCrushed = false;
		mUserSwaped = false;
	}

	private void StatePLAY(float deltaTime)
	{
		if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugForceScore > -1)
		{
			mScore = mGame.mDebugForceScore;
			mGame.mDebugForceScore = -1;
		}
		if (mCollectEffectBusy <= 0 && StageFinishCheck())
		{
			InputEnable = false;
			return;
		}
		if (mCancelRequest)
		{
			mLoseReason = Def.STAGE_LOSE_REASON.CANCEL;
			mState.Reset(STATE.LOSE, true);
			mCancelRequest = false;
			return;
		}
		UpdateInput();
		UpdateShakeTimer();
		UpdateIdleTimer();
		if (mGame.mDebugAutoPlayMode)
		{
			CheckAutoPlayWait();
		}
		UpdateAll(deltaTime);
	}

	private void StateCHAIN(float deltaTime)
	{
		if (IsAppeal())
		{
			AppealEnd();
		}
		if (mState.IsChanged())
		{
			mBeforePlayOrbCheck = false;
			mBeforePlayTalismanCheck = false;
		}
		UpdateInput();
		UpdateAll(deltaTime);
		if (IsMoveFinishAll())
		{
			if (mGame.mTutorialManager.IsTutorialPlaying())
			{
				TutorialMessageFinish();
			}
			if (OnComboFinished(mCombo))
			{
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.BEFOREPLAY;
			}
			else
			{
				mState.Reset(STATE.BEFOREPLAY, true);
			}
			ClearCombo();
		}
	}

	private void StateSHUFFLE(float deltaTime)
	{
		UpdateAll(deltaTime);
		if (mState.IsChanged())
		{
			mShuffleTryCount = 0;
			mShuffleFinished = false;
			OnShuffle(mForceShuffleRequest);
			mShuffleRequest = false;
			mForceShuffleRequest = false;
		}
		if (mShuffleTryCount > Def.SHUFFLE_TRY_MAXCOUNT)
		{
			mShuffleFinished = true;
		}
		else if (!mShuffleFinished)
		{
			mShuffleTryCount++;
			if (Shuffle())
			{
				mShuffleFinished = true;
			}
		}
		if (mShuffleFinished)
		{
			mState.Change(STATE.SHUFFLE_WAIT);
		}
	}

	private void StateSHUFFLE_WAIT(float deltaTime)
	{
		UpdateAll(deltaTime);
		if (IsMoveFinishAll())
		{
			InputEnable = true;
			mState.Change(STATE.BEFOREPLAY);
		}
	}

	private void StateSELECT_BOOSTER_TARGET(float deltaTime)
	{
		UpdateInput();
	}

	private void StateSELECT_SKILL_TARGET(float deltaTime)
	{
		UpdateInput();
	}

	private void StatePAUSE(float deltaTime)
	{
	}

	private void StateWIN(float deltaTime)
	{
		if (IsAppeal())
		{
			AppealEnd();
		}
		mGameFinished = true;
		OnWin();
		mState.Reset(STATE.WAIT, true);
		NextState = STATE.SKILLBALLTRANSFORM;
	}

	private void StateLOSE(float deltaTime)
	{
		if (IsAppeal())
		{
			AppealEnd();
		}
		mGameFinished = true;
		OnLose(mLoseReason);
		mState.Reset(STATE.WAIT, true);
		NextState = STATE.FINISH;
	}

	private void StateSKILLBALLTRANSFORM(float deltaTime)
	{
		UpdateAll(deltaTime);
		if (IsMoveFinishAll())
		{
			if (OnSkillBallTransform())
			{
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.SKILLBALLTRANSFORM;
			}
			else
			{
				SkillBallTransferRainbow();
				mState.Reset(STATE.SPECIALTILECRUSH, true);
			}
		}
	}

	private void StateSPECIALTILECRUSH(float deltaTime)
	{
		UpdateAll(deltaTime);
		if (IsMoveFinishAll() && !CrushAllSpecialTile())
		{
			OnClearBonusSpecialTileCrushed();
			if (mLoseCondition == Def.STAGE_LOSE_CONDITION.MOVES && mMoves > 0 && mWinCondition != Def.STAGE_WIN_CONDITION.STARGET)
			{
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.CLEARBONUS;
				OnClearBonusStart();
			}
			else
			{
				OnClearBonusFinished();
				mState.Change(STATE.FINISH);
			}
		}
	}

	private void StateCLEARBONUS(float deltaTime)
	{
		if (mState.IsChanged())
		{
			StartCoroutine(ClearBonusCoroutine());
		}
		UpdateAll(deltaTime);
	}

	private void StateFINISH(float deltaTime)
	{
		if (mState.IsChanged())
		{
			OnPuzzleFinished();
		}
	}

	private void StateINCREASE_WAIT(float deltaTime)
	{
		UpdateTile(deltaTime);
		UpdateBeforePlaySwap();
	}

	public void SetWaitState(STATE nextState = STATE.NONE)
	{
		if (nextState != STATE.NONE)
		{
			NextState = nextState;
		}
		else
		{
			NextState = mState.GetStatus();
		}
		mState.Reset(STATE.WAIT, true);
	}

	public void WaitStateFinished(STATE force = STATE.NONE)
	{
		if (mState.GetStatus() == STATE.WAIT)
		{
			if (force != STATE.NONE)
			{
				NextState = force;
			}
			mState.Reset(NextState, true);
		}
	}

	protected void UpdateAll(float deltaTime)
	{
		UpdateTimer(deltaTime);
		UpdateTile(deltaTime);
		UpdateSwap();
		UpdateCrush();
		FillTileCheck();
	}

	protected void UpdateTimer(float deltaTime)
	{
		if (TimerEnable && mLoseCondition == Def.STAGE_LOSE_CONDITION.TIME)
		{
			AddTime(0f - deltaTime);
		}
		mPlayTime += deltaTime;
	}

	protected void UpdateTile(float deltaTime)
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				if (mTile[i, j] != null)
				{
					mTile[i, j].UpdateP(deltaTime);
				}
				if (mTileTemp[i, j] != null)
				{
					mTileTemp[i, j].UpdateP(deltaTime);
				}
				for (int k = 0; k < Def.ATTRIBUTE_LAYER_NUM; k++)
				{
					if (mAttribute[k, i, j] != null)
					{
						mAttribute[k, i, j].UpdateP(deltaTime);
					}
				}
			}
		}
	}

	protected void UpdateSwap()
	{
		int count = mSwapList.Count;
		for (int num = count - 1; num >= 0; num--)
		{
			PuzzleTile mTile = mSwapList[num].mTile1;
			PuzzleTile mTile2 = mSwapList[num].mTile2;
			Def.DIR mMoveDir = mSwapList[num].mMoveDir1;
			Def.DIR mMoveDir2 = mSwapList[num].mMoveDir2;
			bool mForce = mSwapList[num].mForce;
			IntVector2 intVector = null;
			IntVector2 intVector2 = null;
			IntVector2 intVector3 = null;
			IntVector2 intVector4 = null;
			bool flag = true;
			bool flag2 = true;
			if (mTile != null)
			{
				intVector = mTile.TilePos;
				intVector2 = new IntVector2(intVector.x + Def.DIR_OFS[(int)mMoveDir].x, intVector.y + Def.DIR_OFS[(int)mMoveDir].y);
				if (mTile.GetState() != PuzzleTile.TILE_STATE.SWAPEND)
				{
					flag = false;
				}
			}
			if (mTile2 != null)
			{
				intVector3 = mTile2.TilePos;
				intVector4 = new IntVector2(intVector3.x + Def.DIR_OFS[(int)mMoveDir2].x, intVector3.y + Def.DIR_OFS[(int)mMoveDir2].y);
				if (mTile2.GetState() != PuzzleTile.TILE_STATE.SWAPEND)
				{
					flag2 = false;
				}
			}
			if (flag && flag2)
			{
				if (mTile != null)
				{
					PlaceTile(intVector2, mTile);
				}
				if (mTile2 != null)
				{
					PlaceTile(intVector4, mTile2);
				}
				bool flag3 = false;
				Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
				if (mTile != null && mTile2 != null)
				{
					tILE_KIND = (Def.TILE_KIND)mSpecialCrossFormTbl[(int)mTile.Form, (int)mTile2.Form];
					if (mTile.isExitableTile() || mTile2.isExitableTile())
					{
						tILE_KIND = Def.TILE_KIND.NONE;
					}
					if (mTile.Kind == Def.TILE_KIND.SKILL_BALL || mTile2.Kind == Def.TILE_KIND.SKILL_BALL)
					{
						tILE_KIND = Def.TILE_KIND.NONE;
					}
					if (mTile.isMovableWall() || mTile2.isMovableWall())
					{
						tILE_KIND = Def.TILE_KIND.NONE;
					}
					if (mTile.isOrb() || mTile2.isOrb())
					{
						tILE_KIND = Def.TILE_KIND.NONE;
					}
					if (mTile.isFlower() || mTile2.isFlower())
					{
						tILE_KIND = Def.TILE_KIND.NONE;
					}
					if (mTile.isPair() || mTile2.isPair())
					{
						tILE_KIND = Def.TILE_KIND.NONE;
						if (mTile.Kind == mTile2.Kind)
						{
							if (mTile.Kind == Def.TILE_KIND.PAIR0_PARTS)
							{
								tILE_KIND = Def.TILE_KIND.PAIR0_COMPLETE;
							}
							if (mTile.Kind == Def.TILE_KIND.PAIR1_PARTS)
							{
								tILE_KIND = Def.TILE_KIND.PAIR1_COMPLETE;
							}
						}
					}
				}
				if (tILE_KIND != Def.TILE_KIND.NONE)
				{
					AddCombo();
					mTile.stay();
					mTile2.stay();
					AddExitCount(tILE_KIND, 1);
					flag3 = true;
					mSwapList.RemoveAt(num);
					switch (tILE_KIND)
					{
					case Def.TILE_KIND.WAVE_WAVE:
						OnSpecialCrossCountUp(tILE_KIND);
						if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.WAVE_WAVE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1))
						{
							BeginAttributeCrush(intVector2, 0f, false);
						}
						BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1);
						break;
					case Def.TILE_KIND.WAVE_BOMB:
						OnSpecialCrossCountUp(tILE_KIND);
						if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.WAVE_BOMB, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1))
						{
							BeginAttributeCrush(intVector2, 0f, false);
						}
						BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1);
						break;
					case Def.TILE_KIND.WAVE_RAINBOW:
						OnSpecialCrossCountUp(tILE_KIND);
						if (mTile.Form == Def.TILE_FORM.RAINBOW)
						{
							if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.WAVE_RAINBOW, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile2.Kind, 1))
							{
								BeginAttributeCrush(intVector2, 0f, false);
							}
						}
						else if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.WAVE_RAINBOW, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile.Kind, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						break;
					case Def.TILE_KIND.BOMB_BOMB:
						OnSpecialCrossCountUp(tILE_KIND);
						if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.BOMB_BOMB, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 2))
						{
							BeginAttributeCrush(intVector2, 0f, false);
						}
						BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1);
						break;
					case Def.TILE_KIND.BOMB_RAINBOW:
						OnSpecialCrossCountUp(tILE_KIND);
						if (mTile.Form == Def.TILE_FORM.RAINBOW)
						{
							if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.BOMB_RAINBOW, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile2.Kind, 1))
							{
								BeginAttributeCrush(intVector2, 0f, false);
							}
						}
						else if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.BOMB_RAINBOW, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile.Kind, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						break;
					case Def.TILE_KIND.RAINBOW_RAINBOW:
						OnSpecialCrossCountUp(tILE_KIND);
						if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.RAINBOW_RAINBOW, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1))
						{
							BeginAttributeCrush(intVector2, 0f, false);
						}
						if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						break;
					case Def.TILE_KIND.RAINBOW:
						if (mTile.Form == Def.TILE_FORM.RAINBOW)
						{
							if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile2.Kind, 1))
							{
								BeginAttributeCrush(intVector2, 0f, false);
							}
						}
						else if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile.Kind, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						mGame.PlaySe("SE_RAINBOW", 2);
						break;
					case Def.TILE_KIND.PAINT:
						if (mTile.Form == Def.TILE_FORM.PAINT)
						{
							if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, 0, mTile2.Kind, 1))
							{
								BeginAttributeCrush(intVector2, 0f, false);
							}
						}
						else if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, 0, mTile.Kind, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						mGame.PlaySe("SE_RAINBOW", 2);
						break;
					case Def.TILE_KIND.BOMB_PAINT:
						OnSpecialCrossCountUp(tILE_KIND);
						if (mTile.Form == Def.TILE_FORM.PAINT)
						{
							if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.BOMB_PAINT, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile2.Kind, 1))
							{
								BeginAttributeCrush(intVector2, 0f, false);
							}
						}
						else if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.BOMB_PAINT, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile.Kind, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						break;
					case Def.TILE_KIND.WAVE_PAINT:
						OnSpecialCrossCountUp(tILE_KIND);
						if (mTile.Form == Def.TILE_FORM.PAINT)
						{
							if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.WAVE_PAINT, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile2.Kind, 1))
							{
								BeginAttributeCrush(intVector2, 0f, false);
							}
						}
						else if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.WAVE_PAINT, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile.Kind, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						break;
					case Def.TILE_KIND.PAINT_RAINBOW:
						OnSpecialCrossCountUp(tILE_KIND);
						if (mTile.Form == Def.TILE_FORM.PAINT)
						{
							if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.PAINT_RAINBOW, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile2.Kind, 1))
							{
								BeginAttributeCrush(intVector2, 0f, false);
							}
							BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1);
						}
						else
						{
							if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.PAINT_RAINBOW, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), mTile.Kind, 1))
							{
								BeginAttributeCrush(intVector4, 0f, false);
							}
							BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1);
						}
						break;
					case Def.TILE_KIND.PAINT_PAINT:
						OnSpecialCrossCountUp(tILE_KIND);
						if (BeginCrush(mTile, 0f, Def.EFFECT_OVERRIDE.PAINT_PAINT, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1))
						{
							BeginAttributeCrush(intVector2, 0f, false);
						}
						if (BeginCrush(mTile2, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, GetCrushScore(2), Def.TILE_KIND.NONE, 1))
						{
							BeginAttributeCrush(intVector4, 0f, false);
						}
						break;
					case Def.TILE_KIND.PAIR0_COMPLETE:
						BeginCollectPair(mTile, mTile2);
						break;
					case Def.TILE_KIND.PAIR1_COMPLETE:
						BeginCollectPair(mTile, mTile2);
						break;
					}
				}
				else
				{
					bool flag4 = false;
					bool flag5 = false;
					if (mTile != null)
					{
						flag4 = IsThreeOrMoreGather(intVector2);
					}
					if (mTile2 != null)
					{
						flag5 = IsThreeOrMoreGather(intVector4);
					}
					if (flag4 || flag5)
					{
						AddCombo();
						if (mTile != null)
						{
							mTile.stay();
						}
						if (mTile2 != null)
						{
							mTile2.stay();
						}
						mSwapList.RemoveAt(num);
						if (flag4)
						{
							CrushTile(mTile, mMoveDir);
						}
						if (flag5)
						{
							CrushTile(mTile2, mMoveDir2);
						}
						if (!mForce)
						{
							flag3 = true;
						}
					}
					else if (mForce)
					{
						if (mTile != null)
						{
							mTile.stay();
						}
						if (mTile2 != null)
						{
							mTile2.stay();
						}
						mSwapList.RemoveAt(num);
					}
					else
					{
						if (mTile != null)
						{
							mTile.move(PuzzleTile.TILE_STATE.RETURN, Def.RETURN_DIR[(int)mMoveDir]);
							RemoveTile(mTile, true);
						}
						if (mTile2 != null)
						{
							mTile2.move(PuzzleTile.TILE_STATE.RETURN, Def.RETURN_DIR[(int)mMoveDir2]);
							RemoveTile(mTile2, true);
						}
						mSwapList[num].mMoveDir1 = Def.RETURN_DIR[(int)mMoveDir];
						mSwapList[num].mMoveDir2 = Def.RETURN_DIR[(int)mMoveDir2];
						mGame.PlaySe("SE_SWAPRETURN", -1);
					}
				}
				if (flag3)
				{
					SwapComplete();
					mTileCrushed = true;
				}
			}
			else if (mTile.GetState() == PuzzleTile.TILE_STATE.RETURNEND && mTile2.GetState() == PuzzleTile.TILE_STATE.RETURNEND)
			{
				PlaceTile(intVector2, mTile);
				PlaceTile(intVector4, mTile2);
				mTile.stay();
				mTile2.stay();
				mSwapList.RemoveAt(num);
				if (IsThreeOrMoreGather(intVector2))
				{
					CrushTile(mTile, mMoveDir);
				}
				if (IsThreeOrMoreGather(intVector4))
				{
					CrushTile(mTile2, mMoveDir2);
				}
			}
		}
	}

	protected void UpdateBeforePlaySwap()
	{
		int count = mSwapList.Count;
		for (int num = count - 1; num >= 0; num--)
		{
			PuzzleTile mTile = mSwapList[num].mTile1;
			PuzzleTile mTile2 = mSwapList[num].mTile2;
			if (mTile == null && mTile2 == null)
			{
				mSwapList.RemoveAt(num);
			}
			else if (!mSwapList[num].mSwapEnd)
			{
				Def.DIR mMoveDir = mSwapList[num].mMoveDir1;
				Def.DIR mMoveDir2 = mSwapList[num].mMoveDir2;
				IntVector2 intVector = null;
				IntVector2 intVector2 = null;
				IntVector2 intVector3 = null;
				IntVector2 intVector4 = null;
				bool flag = true;
				bool flag2 = true;
				if (mTile != null)
				{
					intVector = mTile.TilePos;
					intVector2 = new IntVector2(intVector.x + Def.DIR_OFS[(int)mMoveDir].x, intVector.y + Def.DIR_OFS[(int)mMoveDir].y);
					if (mTile.GetState() != PuzzleTile.TILE_STATE.SWAPEND)
					{
						flag = false;
					}
				}
				if (mTile2 != null)
				{
					intVector3 = mTile2.TilePos;
					intVector4 = new IntVector2(intVector3.x + Def.DIR_OFS[(int)mMoveDir2].x, intVector3.y + Def.DIR_OFS[(int)mMoveDir2].y);
					if (mTile2.GetState() != PuzzleTile.TILE_STATE.SWAPEND)
					{
						flag2 = false;
					}
				}
				if (flag && flag2)
				{
					mSwapList[num].mSwapEnd = true;
					if (mTile != null)
					{
						if (mTile.Kind == Def.TILE_KIND.DIANA)
						{
							mSwapList[num].mTile1 = null;
							PlaceTile(intVector2, mTile);
							mTile.stay();
						}
						else
						{
							mTileTemp[intVector2.y, intVector2.x] = mTile;
						}
					}
					else if (mTile2 != null)
					{
						mTileTemp[intVector3.y, intVector3.x] = null;
					}
					if (mTile2 != null)
					{
						if (mTile2.Kind == Def.TILE_KIND.DIANA)
						{
							mSwapList[num].mTile2 = null;
							PlaceTile(intVector4, mTile2);
							mTile2.stay();
						}
						else
						{
							mTileTemp[intVector4.y, intVector4.x] = mTile2;
						}
					}
					else if (mTile != null)
					{
						mTileTemp[intVector.y, intVector.x] = null;
					}
				}
			}
		}
	}

	protected void UpdateCrush()
	{
		HashSet<PuzzleTile> hashSet = null;
		foreach (PuzzleTile mCrush in mCrushList)
		{
			if (mCrush.GetState() == PuzzleTile.TILE_STATE.CRUSHEND)
			{
				Def.TILE_KIND nextKind = mCrush.NextKind;
				Def.TILE_FORM nextForm = mCrush.NextForm;
				if (mCrush.isAttributeTile())
				{
					if (nextKind == Def.TILE_KIND.NONE && nextForm == Def.TILE_FORM.NORMAL)
					{
						RemoveAttribute(mCrush);
						AddExitCount(mCrush.Kind, 1);
						DestroyTile(mCrush);
						continue;
					}
					Def.TILE_KIND kind = mCrush.Kind;
					if (kind == Def.TILE_KIND.CAPTURE1 || kind == Def.TILE_KIND.CAPTURE2)
					{
						mCrush.ChangeForm(nextKind, nextForm, 0f, 0f, false, false, 0, true);
					}
					else
					{
						mCrush.ChangeForm(nextKind, nextForm, 0f, 0f, false, true, 0, true);
					}
					continue;
				}
				PlaceTile(mCrush.TilePos, mCrush);
				if (nextKind == Def.TILE_KIND.NONE && nextForm == Def.TILE_FORM.NORMAL)
				{
					RemoveTile(mCrush, false);
					switch (mCrush.Form)
					{
					case Def.TILE_FORM.WAVE_V:
					case Def.TILE_FORM.WAVE_H:
						AddExitCount(Def.TILE_KIND.WAVE, 1);
						break;
					case Def.TILE_FORM.RAINBOW:
						AddExitCount(Def.TILE_KIND.RAINBOW, 1);
						break;
					case Def.TILE_FORM.BOMB:
						AddExitCount(Def.TILE_KIND.BOMB, 1);
						break;
					case Def.TILE_FORM.BOMB_B:
						AddExitCount(Def.TILE_KIND.BOMB, 1);
						break;
					}
					if (!mCrush.isExitableTile() && mCrush.mScore > 0)
					{
						AddExitCount(mCrush.Kind, 1);
					}
					DestroyTile(mCrush);
				}
				else
				{
					RemoveTile(mCrush, true);
					AddExitCount(mCrush.Kind, 1);
					mCrush.ChangeForm(nextKind, nextForm, 0f, 0f, false, true, 0, true);
				}
			}
			else
			{
				if (hashSet == null)
				{
					hashSet = new HashSet<PuzzleTile>();
				}
				hashSet.Add(mCrush);
			}
		}
		mCrushList.Clear();
		if (hashSet != null)
		{
			mCrushList = hashSet;
		}
	}

	protected void FillTileCheck()
	{
		for (int i = 0; i < mSpawnPoints.Count; i++)
		{
			IntVector2 intVector = mSpawnPoints[i];
			int x = intVector.x;
			int y = intVector.y;
			if (mTile[y, x] != null || mTileTemp[y, x] != null)
			{
				continue;
			}
			Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
			Def.TILE_FORM tILE_FORM = Def.TILE_FORM.NORMAL;
			ISMTileInfo tileInfo = mGame.mCurrentStage.GetTileInfo(x, y);
			if (tILE_KIND == Def.TILE_KIND.NONE && tileInfo.FallIngredient && HasIngredient(x, y))
			{
				tILE_KIND = RandomIngredient();
			}
			SpecialSpawnData specialSpawnData = null;
			if (tILE_KIND == Def.TILE_KIND.NONE)
			{
				for (int j = 0; j < mSpecialSpawnList.Count; j++)
				{
					SpecialSpawnData specialSpawnData2 = mSpecialSpawnList[j];
					specialSpawnData2.CountDown--;
					if (specialSpawnData2.CountDown < 0)
					{
						specialSpawnData2.CountDown = 0;
					}
					mSpecialSpawnList[j] = specialSpawnData2;
				}
				for (int k = 0; k < mSpecialSpawnList.Count; k++)
				{
					SpecialSpawnData specialSpawnData3 = mSpecialSpawnList[k];
					if (tileInfo.FallPresentBox && specialSpawnData3.CountDown <= 0 && !specialSpawnData3.SpawnFinised)
					{
						tILE_KIND = specialSpawnData3.Kind;
						specialSpawnData3.SpawnFinised = true;
						mSpecialSpawnList[k] = specialSpawnData3;
						specialSpawnData = specialSpawnData3;
						break;
					}
				}
			}
			if (tILE_KIND == Def.TILE_KIND.NONE && tileInfo.FallIngredient && HasPair(x, y) && (mLastPairParts0GeneratePos.x != x || mLastPairParts0GeneratePos.y != y))
			{
				tILE_KIND = RandomPair();
				if (tILE_KIND != Def.TILE_KIND.NONE)
				{
					mLastPairParts0GeneratePos = new IntVector2(x, y);
				}
			}
			if (tILE_KIND == Def.TILE_KIND.NONE && HasMovableWall(x, y))
			{
				tILE_KIND = RandomMovableWall();
			}
			if (tILE_KIND == Def.TILE_KIND.NONE && tileInfo.FallIngredient && HasOrb(x, y) && (mLastOrbGeneratePos.x != x || mLastOrbGeneratePos.y != y))
			{
				tILE_KIND = RandomOrb();
				if (tILE_KIND != Def.TILE_KIND.NONE)
				{
					mLastOrbGeneratePos = new IntVector2(x, y);
				}
			}
			if (tILE_KIND == Def.TILE_KIND.NONE && tileInfo.FallIngredient && HasFlower(x, y))
			{
				tILE_KIND = RandomFlower();
			}
			if (tILE_KIND == Def.TILE_KIND.NONE)
			{
				tILE_KIND = RandomColorTileForFillTile(i, intVector);
				if (mWinCondition == Def.STAGE_WIN_CONDITION.GOAL)
				{
					if (tILE_FORM == Def.TILE_FORM.NORMAL && SpawnLotteryForm(Def.TILE_FORM.FOOT_STAMP, (int)mGame.mCurrentStage.FootPieceFreq1))
					{
						tILE_FORM = Def.TILE_FORM.FOOT_STAMP;
					}
				}
				else if (mLoseCondition == Def.STAGE_LOSE_CONDITION.TIME)
				{
					if (tILE_FORM == Def.TILE_FORM.NORMAL && SpawnLotteryForm(Def.TILE_FORM.ADDTIME, (int)mGame.mCurrentStage.BonusFreq1))
					{
						tILE_FORM = Def.TILE_FORM.ADDTIME;
					}
				}
				else if (mLoseCondition == Def.STAGE_LOSE_CONDITION.MOVES && tILE_FORM == Def.TILE_FORM.NORMAL && SpawnLotteryForm(Def.TILE_FORM.ADDPLUS, (int)mGame.mCurrentStage.BonusMove1))
				{
					tILE_FORM = Def.TILE_FORM.ADDPLUS;
				}
				if (tILE_FORM == Def.TILE_FORM.NORMAL && SpawnLotteryForm(Def.TILE_FORM.PEARL, (int)mGame.mCurrentStage.FPerl02Freq1))
				{
					tILE_FORM = Def.TILE_FORM.PEARL;
				}
				if (tILE_FORM == Def.TILE_FORM.NORMAL && HasJewel())
				{
					tILE_FORM = RandomJewel();
				}
			}
			PuzzleTile tile = CreateTile(mRoot, tILE_KIND, tILE_FORM, new IntVector2(x, y), specialSpawnData);
			PlaceTile(new IntVector2(x, y), tile);
		}
	}

	protected bool StageFinishCheck()
	{
		if (GameMain.USE_DEBUG_DIALOG)
		{
			if (mGame.mDebugForceWin)
			{
				mGame.mDebugForceWin = false;
				mState.Reset(STATE.WIN, true);
				return true;
			}
			if (mGame.mDebugForceLose)
			{
				mGame.mDebugForceLose = false;
				mState.Reset(STATE.LOSE, true);
				return true;
			}
			if (mGame.mDebugForceNoMoreMoves)
			{
				mLoseReason = Def.STAGE_LOSE_REASON.NO_MORE_MOVES;
				mGame.mDebugForceNoMoreMoves = false;
				mState.Reset(STATE.LOSE, true);
				return true;
			}
		}
		bool flag = false;
		bool flag2 = mScore >= mGame.mCurrentStage.BronzeScore;
		switch (mWinCondition)
		{
		case Def.STAGE_WIN_CONDITION.SCORE:
			flag = flag2;
			break;
		case Def.STAGE_WIN_CONDITION.COLLECT:
		case Def.STAGE_WIN_CONDITION.EATEN:
		case Def.STAGE_WIN_CONDITION.CLEAN:
		case Def.STAGE_WIN_CONDITION.SPECIAL_EATEN:
		case Def.STAGE_WIN_CONDITION.PAIR:
		case Def.STAGE_WIN_CONDITION.FIND:
		case Def.STAGE_WIN_CONDITION.GROWUP:
		case Def.STAGE_WIN_CONDITION.TALISMAN:
			flag = flag2 && IsNormaAchivementCheck();
			break;
		case Def.STAGE_WIN_CONDITION.ENEMY:
			if (flag2 && GetTileCount(Def.TILE_KIND.INCREASE) == 0)
			{
				flag = true;
			}
			break;
		case Def.STAGE_WIN_CONDITION.DEFEAT:
			if (mBossCharacter != null)
			{
				flag = flag2 && mBossCharacter.IsDead();
			}
			break;
		case Def.STAGE_WIN_CONDITION.GOAL:
			if (mDianaCharacter != null)
			{
				flag = flag2 && mDianaCharacter.IsGoal();
			}
			break;
		case Def.STAGE_WIN_CONDITION.STARGET:
			flag = IsNormaAchivementCheck();
			break;
		}
		if (flag && mLoseCondition != Def.STAGE_LOSE_CONDITION.TIME)
		{
			mState.Reset(STATE.WIN, true);
			mStageClearScore = mScore;
			mStageClearMoves = mMoves;
			mStageClearStars = CheckGotStars();
			return true;
		}
		bool flag3 = false;
		switch (mLoseCondition)
		{
		case Def.STAGE_LOSE_CONDITION.MOVES:
			flag3 = mMoves <= 0;
			mLoseReason = Def.STAGE_LOSE_REASON.MOVES;
			break;
		case Def.STAGE_LOSE_CONDITION.TIME:
			flag3 = mTime <= 0f;
			mLoseReason = Def.STAGE_LOSE_REASON.TIME;
			break;
		}
		if (flag3 && mWinCondition == Def.STAGE_WIN_CONDITION.GOAL && mDianaCharacter != null)
		{
			Def.DIR moveDir;
			mDianaCharacter.MoveCheck(out moveDir);
			if (moveDir != Def.DIR.NONE)
			{
				flag3 = false;
			}
		}
		if (mShuffleTryCount > Def.SHUFFLE_TRY_MAXCOUNT)
		{
			flag3 = true;
			mLoseReason = Def.STAGE_LOSE_REASON.NO_MORE_MOVES;
		}
		if (flag3)
		{
			Def.TUTORIAL_INDEX tUTORIAL_INDEX = TutorialStartCheck();
			if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
			{
				TutorialStart(tUTORIAL_INDEX);
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.LOSE;
				return true;
			}
			if (mLoseCondition == Def.STAGE_LOSE_CONDITION.TIME && flag)
			{
				mState.Reset(STATE.WIN, true);
				return true;
			}
			mState.Reset(STATE.LOSE, true);
			return true;
		}
		return false;
	}

	protected void ResetShakeTimer()
	{
		mShakeTimer = 2f;
	}

	protected void UpdateShakeTimer()
	{
		if (IsMoveFinishAll())
		{
			mShakeTimer -= Time.deltaTime;
			if (!(mShakeTimer > 0f) && UnityEngine.Random.Range(0, 60) == 0)
			{
				ResetShakeTimer();
				ShakeStart();
			}
		}
	}

	protected void ShakeStart()
	{
		Def.TILE_KIND tILE_KIND = RandomColorTile();
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == tILE_KIND)
				{
					tile.Shake();
				}
			}
		}
	}

	protected void ResetIdleTimer()
	{
		mIdleTimer = 4f;
	}

	protected void UpdateIdleTimer()
	{
		if (!mGame.mTutorialManager.IsTutorialPlaying() && IsMoveFinishAll() && !(mIdleTimer <= 0f))
		{
			mIdleTimer -= Time.deltaTime;
			if (!(mIdleTimer > 0f))
			{
				AppealStart();
			}
		}
	}

	protected void AppealStart()
	{
		if (mAppealTiles.Count <= 0)
		{
			return;
		}
		foreach (PuzzleTile mAppealTile in mAppealTiles)
		{
			mAppealTile.appeal();
		}
		mGame.PlaySe("SE_BEGIN_APPEAL", -1);
	}

	public bool IsAppeal()
	{
		return mAppealTiles.Count > 0;
	}

	public void AppealEnd()
	{
		foreach (PuzzleTile mAppealTile in mAppealTiles)
		{
			mAppealTile.appealEnd();
		}
		mAppealTiles.Clear();
		ResetIdleTimer();
	}

	private static int MatchTileInfoSorter(MatchTileInfo a, MatchTileInfo b)
	{
		if (a.numV + a.numH != b.numV + b.numH)
		{
			return b.numV + b.numH - (a.numV + a.numH);
		}
		return a.pos.y - b.pos.y;
	}

	protected void UpdateAppealList()
	{
		mShuffleRequest = false;
		AppealEnd();
		List<MatchTileInfo> list = new List<MatchTileInfo>();
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (UpdateAppealListSub_NullOrAttribute(tile, true) || !UpdateAppealListSub_Movable(tile) || tile.Form == Def.TILE_FORM.BOMB)
				{
					continue;
				}
				for (int k = 0; k < 8; k += 2)
				{
					IntVector2 intVector = new IntVector2(j, i) + Def.DIR_OFS[k];
					PuzzleTile tile2 = GetTile(intVector, false);
					int num = 0;
					int num2 = 0;
					if (UpdateAppealListSub_NullOrAttribute(tile2) || !UpdateAppealListSub_Movable(tile2) || tile.Kind == tile2.Kind)
					{
						continue;
					}
					for (int l = 0; l < 8; l += 2)
					{
						if (Def.RETURN_DIR[k] == (Def.DIR)l)
						{
							continue;
						}
						IntVector2 intVector2 = intVector + Def.DIR_OFS[l];
						PuzzleTile tile3 = GetTile(intVector2, false);
						if (UpdateAppealListSub_NullOrAttribute(tile3) || tile.Kind != tile3.Kind)
						{
							continue;
						}
						switch (l)
						{
						case 0:
						case 4:
							if (IsInField(intVector2.x, intVector2.y))
							{
								num += mNum[intVector2.y, intVector2.x].y;
							}
							break;
						case 2:
						case 6:
							if (IsInField(intVector2.x, intVector2.y))
							{
								num2 += mNum[intVector2.y, intVector2.x].x;
							}
							break;
						}
					}
					if (num >= 2 || num2 >= 2)
					{
						if (num < 2)
						{
							num = 0;
						}
						if (num2 < 2)
						{
							num2 = 0;
						}
						list.Add(new MatchTileInfo(new IntVector2(j, i), (Def.DIR)k, num, num2));
					}
				}
			}
		}
		if (list.Count == 0)
		{
			mShuffleRequest = true;
			return;
		}
		list.Sort(MatchTileInfoSorter);
		mAutoPlayInfo = list[0];
		if (GameMain.USE_DEBUG_DIALOG && mGame.mDebug2MatchNaviMode)
		{
			for (int m = 0; m < list.Count; m++)
			{
				mAppealTiles = UpdateAppealListSub_ChooseAppealList(list[m]);
				if (mAppealTiles.Count < 3)
				{
					break;
				}
				mAppealTiles.Clear();
			}
		}
		if (mAppealTiles.Count == 0)
		{
			for (int n = 0; n < list.Count; n++)
			{
				mAppealTiles = UpdateAppealListSub_ChooseAppealList(list[n]);
				if (mAppealTiles.Count >= 3)
				{
					break;
				}
				mAppealTiles.Clear();
			}
		}
		if (mAppealTiles.Count == 0)
		{
			mShuffleRequest = true;
		}
	}

	protected bool UpdateAppealListSub_NullOrAttribute(PuzzleTile tile, bool firstTile = false)
	{
		if (tile == null)
		{
			return true;
		}
		if (tile.isAttributeTile())
		{
			return true;
		}
		if (firstTile && tile.isExitableTile())
		{
			return true;
		}
		if (tile.Kind == Def.TILE_KIND.SPECIAL)
		{
			return true;
		}
		if (firstTile && tile.isMovableWall())
		{
			return true;
		}
		if (firstTile && tile.isPair())
		{
			return true;
		}
		if (firstTile && tile.isOrb())
		{
			return true;
		}
		if (tile.isDiana())
		{
			return true;
		}
		if (firstTile && (tile.Kind == Def.TILE_KIND.MOVABLE_FLOWER0 || tile.Kind == Def.TILE_KIND.MOVABLE_FLOWER1))
		{
			return true;
		}
		if (tile.Kind == Def.TILE_KIND.FIXED_FLOWER0 || tile.Kind == Def.TILE_KIND.FIXED_FLOWER1)
		{
			return true;
		}
		if (tile.isTalismanFace())
		{
			return true;
		}
		return false;
	}

	protected bool UpdateAppealListSub_Movable(PuzzleTile tile)
	{
		if (tile == null)
		{
			return false;
		}
		PuzzleTile attribute = GetAttribute(tile.TilePos);
		if (attribute != null && attribute.isCandyCaptor())
		{
			return false;
		}
		return true;
	}

	protected List<PuzzleTile> UpdateAppealListSub_ChooseAppealList(MatchTileInfo targetInfo)
	{
		List<PuzzleTile> list = new List<PuzzleTile>();
		PuzzleTile tile = GetTile(targetInfo.pos, false);
		list.Add(tile);
		IntVector2 intVector = targetInfo.pos + Def.DIR_OFS[(int)targetInfo.dir];
		for (int i = 0; i < 8; i += 2)
		{
			if (Def.RETURN_DIR[(int)targetInfo.dir] == (Def.DIR)i)
			{
				continue;
			}
			switch (i)
			{
			case 0:
			case 4:
				if (targetInfo.numV == 0)
				{
					continue;
				}
				break;
			case 2:
			case 6:
				if (targetInfo.numH == 0)
				{
					continue;
				}
				break;
			}
			IntVector2 pos = intVector + Def.DIR_OFS[i];
			while (true)
			{
				PuzzleTile tile2 = GetTile(pos, false);
				if (!UpdateAppealListSub_NullOrAttribute(tile2) && tile.Kind == tile2.Kind)
				{
					list.Add(tile2);
					pos += Def.DIR_OFS[i];
					continue;
				}
				break;
			}
		}
		return list;
	}

	protected bool Shuffle(bool initialShuffle = false)
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				if (initialShuffle)
				{
					ISMTile tile = mGame.mCurrentStage.GetTile(j, i);
					if (tile.Kind != Def.TILE_KIND.NONE)
					{
						continue;
					}
				}
				PuzzleTile tile2 = GetTile(new IntVector2(j, i), false);
				if (tile2 != null && tile2.isColor() && (tile2.isNormalForm() || tile2.isFootstamp() || tile2.isJewel()))
				{
					RemoveTile(tile2, true);
				}
			}
		}
		for (int k = 0; k < mFieldSize.y; k++)
		{
			for (int l = 0; l < mFieldSize.x; l++)
			{
				if (initialShuffle)
				{
					ISMTile tile3 = mGame.mCurrentStage.GetTile(l, k);
					if (tile3.Kind != Def.TILE_KIND.NONE)
					{
						continue;
					}
				}
				PuzzleTile tile4 = GetTile(new IntVector2(l, k), true);
				if (tile4 == null || !tile4.isColor() || (!tile4.isNormalForm() && !tile4.isFootstamp() && !tile4.isJewel()))
				{
					continue;
				}
				while (true)
				{
					Def.TILE_KIND kind = RandomColorTile();
					tile4.ChangeColorForShuffle(kind, 0f);
					PlaceTile(new IntVector2(l, k), tile4);
					int candyNumH = GetCandyNumH(new IntVector2(l, k));
					int candyNumV = GetCandyNumV(new IntVector2(l, k));
					if (candyNumH < 3 && candyNumV < 3)
					{
						break;
					}
					RemoveTile(tile4, true);
				}
			}
		}
		UpdateAppealList();
		if (mShuffleRequest)
		{
			return false;
		}
		for (int m = 0; m < mFieldSize.y; m++)
		{
			for (int n = 0; n < mFieldSize.x; n++)
			{
				if (initialShuffle)
				{
					ISMTile tile5 = mGame.mCurrentStage.GetTile(n, m);
					if (tile5.Kind != Def.TILE_KIND.NONE)
					{
						continue;
					}
				}
				PuzzleTile tile6 = GetTile(new IntVector2(n, m), true);
				if (tile6 != null && tile6.isColor() && (tile6.isNormalForm() || tile6.isFootstamp() || tile6.isJewel()))
				{
					if (initialShuffle)
					{
						tile6.ChangeColorForShuffle(tile6.Kind, 0.03f);
						continue;
					}
					float waitTime = Mathf.Sqrt(n * n + m * m) * 0.15f;
					tile6.ChangeColorForShuffle(tile6.Kind, waitTime);
				}
			}
		}
		return true;
	}

	protected void CheckAutoPlayWait()
	{
		mAutoPlayCheckTime -= Time.deltaTime;
		if (!mGame.mTutorialManager.IsTutorialPlaying() && IsMoveFinishAll() && mAutoPlayCheckTime <= 0f)
		{
			mAutoPlayCheckTime = UnityEngine.Random.Range(0f, 0.5f);
			if (mAutoPlayInfo != null)
			{
				PuzzleTile tile = GetTile(mAutoPlayInfo.pos, false);
				BeginFloat(tile, new Vector2(0f, 0f));
				BeginMove(tile, mAutoPlayInfo.dir);
				mAutoPlayInfo = null;
			}
		}
	}

	public void SetPlayStart()
	{
		InputEnable = true;
		if (OnPuzzleStart())
		{
			mState.Reset(STATE.WAIT, true);
			NextState = STATE.INIT;
		}
		else
		{
			mState.Reset(STATE.INIT, true);
		}
	}

	protected void PlayStartAtFirstMove()
	{
		mFirstMove = true;
		TimerEnable = true;
	}

	public void Continue(int continueCount, int additionalMoves = 0)
	{
		if (continueCount >= Def.CONTINUE_STEP.Length)
		{
			continueCount = Def.CONTINUE_STEP.Length - 1;
		}
		Def.ContinueStepData continueStepData = Def.CONTINUE_STEP[continueCount];
		if (mLoseCondition == Def.STAGE_LOSE_CONDITION.MOVES)
		{
			mMoves += continueStepData.addMove + additionalMoves;
			OnChangeMoves(mMoves);
		}
		else if (mLoseCondition == Def.STAGE_LOSE_CONDITION.TIME)
		{
			mTime += continueStepData.addTime + (float)(additionalMoves * 3);
			TimerEnable = true;
			OnChangeTimer(mTime);
		}
		if (continueStepData.item != 0)
		{
			switch (continueStepData.item)
			{
			case Def.CONTINUE_ADDITIONALITEM.SKILL_CHARGE:
				mGame.mPlayer.AddPaidBoosterNum(Def.BOOSTER_KIND.SKILL_CHARGE, continueStepData.num);
				mGame.SendGetBoost(continueStepData.num, 0, 5, Def.BOOSTER_KIND.SKILL_CHARGE);
				break;
			case Def.CONTINUE_ADDITIONALITEM.SELECT_ONE:
				mGame.mPlayer.AddPaidBoosterNum(Def.BOOSTER_KIND.SELECT_ONE, continueStepData.num);
				mGame.SendGetBoost(continueStepData.num, 0, 5, Def.BOOSTER_KIND.SELECT_ONE);
				break;
			case Def.CONTINUE_ADDITIONALITEM.WAVE_BOMB:
			{
				PuzzleTile[] colorTiles = Booster_GetTargetTiles(2);
				if (colorTiles != null)
				{
					Booster_WaveBomb(colorTiles[0], 0);
					Booster_WaveBomb(colorTiles[1], 1);
				}
				break;
			}
			case Def.CONTINUE_ADDITIONALITEM.RAINBOW:
			{
				PuzzleTile[] colorTiles = Booster_GetTargetTiles(continueStepData.num);
				if (colorTiles != null)
				{
					for (int i = 0; i < colorTiles.Length; i++)
					{
						Booster_Rainbow(colorTiles[i]);
					}
				}
				break;
			}
			case Def.CONTINUE_ADDITIONALITEM.TAP_BOMB:
			{
				PuzzleTile[] colorTiles = GetColorTiles(true);
				int num = UnityEngine.Random.Range(0, colorTiles.Length);
				colorTiles[num].ChangeForm(Def.TILE_KIND.SPECIAL, Def.TILE_FORM.TAP_BOMB, 0f, 0f, false, true, 0, true);
				break;
			}
			case Def.CONTINUE_ADDITIONALITEM.COLOR_CRUSH:
				mGame.mPlayer.AddPaidBoosterNum(Def.BOOSTER_KIND.COLOR_CRUSH, continueStepData.num);
				mGame.SendGetBoost(continueStepData.num, 0, 5, Def.BOOSTER_KIND.COLOR_CRUSH);
				break;
			}
		}
		InputEnable = true;
		mGameFinished = false;
		mState.Change(STATE.BEFOREPLAY);
		mGame.PlaySe("VOICE_022", 2);
	}

	protected void SwapComplete()
	{
		if (mLoseCondition == Def.STAGE_LOSE_CONDITION.MOVES)
		{
			AddMoves(-1);
		}
		if (mSkillExpandedScoreRatioRemain > 0)
		{
			mSkillExpandedScoreRatioRemain--;
			if (mSkillExpandedScoreRatioRemain == 0)
			{
				mSkillExpandedScoreRatio = 0f;
			}
		}
		mTotalMoves++;
	}

	public bool IsMoveFinishAll()
	{
		if (IsMoveFinishAllSub())
		{
			mMoveFinishCheckTimer += 1f;
		}
		else
		{
			mMoveFinishCheckTimer = 0f;
		}
		if (mMoveFinishCheckTimer >= 2f)
		{
			return true;
		}
		return false;
	}

	protected bool IsMoveFinishAllSub()
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile puzzleTile = mTile[i, j];
				if (puzzleTile != null && puzzleTile.isMove())
				{
					return false;
				}
				puzzleTile = mTileTemp[i, j];
				if (puzzleTile != null && puzzleTile.isMove())
				{
					return false;
				}
				for (int k = 0; k < Def.ATTRIBUTE_LAYER_NUM; k++)
				{
					puzzleTile = mAttribute[k, i, j];
					if (puzzleTile != null && puzzleTile.isMove())
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	public Def.TILE_KIND RandomColorTile(int spawnPointIndex = -1)
	{
		int count = mRandomKindList.Count;
		int num = 0;
		for (int i = 0; i < count; i++)
		{
			num += Def.TILE_GENERATE_RATIO[(int)mRandomKindList[i]];
		}
		int num2 = UnityEngine.Random.Range(0, num);
		int num3 = 0;
		for (int j = 0; j < count; j++)
		{
			num3 += Def.TILE_GENERATE_RATIO[(int)mRandomKindList[j]];
			if (num2 < num3)
			{
				return mRandomKindList[j];
			}
		}
		return mRandomKindList[0];
	}

	protected Def.TILE_KIND RandomColorTileForFillTile(int spawnPointIndex, IntVector2 pos)
	{
		if (spawnPointIndex >= 0 && mReservedFillTiles[spawnPointIndex].Count > 0)
		{
			Def.TILE_KIND result = mReservedFillTiles[spawnPointIndex][0];
			mReservedFillTiles[spawnPointIndex].RemoveAt(0);
			return result;
		}
		Def.TILE_KIND result2 = mRandomKindList[0];
		int count = mRandomKindList.Count;
		int num = 0;
		for (int i = 0; i < count; i++)
		{
			num += Def.TILE_GENERATE_RATIO[(int)mRandomKindList[i]];
		}
		int num2 = UnityEngine.Random.Range(0, num);
		int num3 = 0;
		for (int j = 0; j < count; j++)
		{
			num3 += Def.TILE_GENERATE_RATIO[(int)mRandomKindList[j]];
			if (num2 < num3)
			{
				result2 = mRandomKindList[j];
				break;
			}
		}
		if (mDifficultyMode == Def.DIFFICULTY_MODE.FILL_MATCH && !IsGameFinished() && mCombo < 20)
		{
			PuzzleTile tile = GetTile(pos + new IntVector2(0, -1), true);
			if (tile != null && tile.isColor() && UnityEngine.Random.Range(0f, 100f) < mDifficultyModeWork0)
			{
				result2 = tile.Kind;
			}
		}
		return result2;
	}

	protected void GetPoolTileSprite(GameObject parent, int num, out TileSprite[] sprites)
	{
		sprites = new TileSprite[num];
		for (int i = 0; i < num; i++)
		{
			TileSprite tileSprite = mTileSpritePool.GetObject();
			if (tileSprite == null)
			{
				tileSprite = Util.CreateGameObject("tile" + uqTileCounter, parent).AddComponent<TileSprite>();
				uqTileCounter++;
			}
			sprites[i] = tileSprite;
		}
	}

	protected PuzzleTile CreateTile(GameObject parent, Def.TILE_KIND kind, Def.TILE_FORM form, IntVector2 tilePos, SpecialSpawnData specialSpawnData = null)
	{
		TileSprite sprite = null;
		if (mGame.mTileData[(int)kind].animationType == TileData.ANIMATION_TYPE.ANIMATION)
		{
			TileSprite[] sprites;
			GetPoolTileSprite(parent, 1, out sprites);
			sprite = sprites[0];
		}
		Vector2 defaultCoords = GetDefaultCoords(tilePos);
		Def.ITEM_CATEGORY itemCategory = Def.ITEM_CATEGORY.NONE;
		int itemIndex = 0;
		if (specialSpawnData != null)
		{
			itemCategory = specialSpawnData.Category;
			itemIndex = specialSpawnData.ItemIndex;
		}
		return new PuzzleTile(kind, form, tilePos, new Vector3(defaultCoords.x, defaultCoords.y, Def.PUZZLE_TILE_Z + mGame.mTileData[(int)kind].zOffset), sprite, this, itemCategory, itemIndex);
	}

	protected void DestroyTile(PuzzleTile tile)
	{
		if (tile != null)
		{
			TileSprite sprite = tile.Sprite;
			if (sprite != null)
			{
				mTileSpritePool.PutObject(sprite);
				sprite.ToRecycle();
			}
		}
	}

	protected void CreateBossGimmick(Def.TILE_KIND kind, IntVector2 tilePos)
	{
		Vector2 defaultCoords = GetDefaultCoords(tilePos);
		mBossCharacter = Util.CreateGameObject("Boss", mRoot).AddComponent<BossGimmick>();
		mBossCharacter.transform.localPosition = new Vector3(defaultCoords.x, defaultCoords.y, Def.PUZZLE_TILE_Z);
		mBossCharacter.transform.localScale = new Vector3(Def.TILE_SCALE, Def.TILE_SCALE, 1f);
		mBossCharacter.OnShuffleTargets = delegate(Def.TILE_KIND[] newTargets, bool immediate)
		{
			OnAttackTargetsChanged(mBossCharacter.transform.position, newTargets, immediate);
		};
		mBossCharacter.OnPlaceBrokenWall = delegate(int num1, int num2, int num3)
		{
			OnPlaceBrokenWall(mBossCharacter.transform.position, num1, num2, num3);
		};
		mBossCharacter.OnPlaceFamiliar = delegate(int maxNum, int num)
		{
			StartCoroutine(BossGimmick_PlaceFamiliar(mBossCharacter.transform.position, num));
		};
		mBossCharacter.Init(ref mGame.mCurrentStage);
	}

	protected void CreateFindGimmick(StencilData data)
	{
		IntVector2 position2 = data.Position;
		Vector2 defaultCoords = GetDefaultCoords(position2);
		FindGimmick findGimmick = Util.CreateGameObject("Find", mRoot).AddComponent<FindGimmick>();
		findGimmick.transform.localPosition = new Vector3(defaultCoords.x, defaultCoords.y, Def.PUZZLE_TILE_Z + 2f);
		findGimmick.Init(ref data);
		findGimmick.OnAllCollect = delegate(FindGimmick gimmick, Vector3 position, string overWriteImage)
		{
			PuzzleTile puzzleTile = new PuzzleTile(gimmick.mKind, Def.TILE_FORM.NORMAL, new IntVector2(0, 0), Vector3.zero, null, null, Def.ITEM_CATEGORY.NONE, 0);
			OnCollect(puzzleTile, position, overWriteImage);
			mFindGimmicks.Remove(gimmick);
			AddExitCount(puzzleTile.Kind, 1);
		};
		mFindGimmicks.Add(findGimmick);
	}

	protected void CreateFamiliarGimmick(IntVector2 tilePos, int familiarId)
	{
		FamiliarGimmick familiarGimmick = Util.CreateGameObject("Familiar", mRoot).AddComponent<FamiliarGimmick>();
		familiarGimmick.Init(familiarId, tilePos, this);
		mFamiliarGimmicks.Add(familiarGimmick);
	}

	protected void CreateDianaGimmick(IntVector2 tilePos, PuzzleTile tile)
	{
		Vector2 defaultCoords = GetDefaultCoords(tilePos);
		mDianaCharacter = Util.CreateGameObject("Diana", mRoot).AddComponent<DianaGimmick>();
		mDianaCharacter.Init(tilePos, this, tile);
	}

	public void UpdateInput()
	{
		Vector2 vector = Util.LogScreenRatio();
		Vector3 position = new Vector3(mGame.mTouchPos.x, mGame.mTouchPos.y, 0f);
		Vector3 vector2 = mCamera.ScreenToWorldPoint(position);
		Vector2 vector3 = new Vector2(vector2.x * vector.y, vector2.y * vector.y);
		if (mGame.mTouchDownTrg)
		{
			bool flag = false;
			for (int i = 0; i < mFieldSize.y; i++)
			{
				for (int j = 0; j < mFieldSize.x; j++)
				{
					if (mTile[i, j] != null && mTile[i, j].Contains(vector3) && BeginFloat(mTile[i, j], vector3))
					{
						mCaptureTile = mTile[i, j];
						flag = true;
						break;
					}
				}
				if (flag)
				{
					break;
				}
			}
		}
		if (!mGame.mTouchCnd)
		{
			if (mCaptureTile != null)
			{
				EndFloat(mCaptureTile);
				mCaptureTile = null;
			}
		}
		else if (mGame.mTouchWasDrag && mCaptureTile != null)
		{
			Def.DIR dIR = mCaptureTile.OnDrag(vector3);
			if (dIR != Def.DIR.NONE)
			{
				BeginMove(mCaptureTile, dIR);
			}
		}
	}

	protected void ResetCapture()
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				if (mTile[i, j] != null)
				{
					mTile[i, j].selectFloatEnd();
				}
			}
		}
		mCaptureTile = null;
	}

	public Vector2 GetDefaultCoords(IntVector2 pos)
	{
		float num = 0f;
		if (mPlayAreaSize.x % 2 == 0)
		{
			num = Def.TILE_PITCH_H / 2f;
		}
		float num2 = (float)(-mFieldSize.x) / 2f + 0.5f;
		float num3 = (float)(-mFieldSize.y) / 2f + 0.5f;
		return new Vector2(num2 * Def.TILE_PITCH_H + (float)pos.x * Def.TILE_PITCH_H + num, num3 * Def.TILE_PITCH_V + (float)pos.y * Def.TILE_PITCH_V);
	}

	public Vector2 GetFallTargetCoords(IntVector2 pos)
	{
		Vector2 defaultCoords = GetDefaultCoords(pos);
		PuzzleTile attribute = GetAttribute(pos);
		if (attribute != null && attribute.Kind == Def.TILE_KIND.WARP_IN)
		{
			defaultCoords += new Vector2(0f, Def.TILE_PITCH_V * 0.5f);
		}
		return defaultCoords;
	}

	public bool IsInPlayArea(IntVector2 pos)
	{
		return mBackground[pos.y, pos.x];
	}

	public bool IsInField(IntVector2 pos)
	{
		return IsInField(pos.x, pos.y);
	}

	public bool IsInField(int x, int y)
	{
		return x >= 0 && x < mFieldSize.x && y >= 0 && y < mFieldSize.y;
	}

	public PuzzleTile GetAttribute(IntVector2 pos, int layer = -1)
	{
		if (!IsInField(pos))
		{
			return null;
		}
		if (layer != -1)
		{
			return mAttribute[layer, pos.y, pos.x];
		}
		layer = 0;
		return mAttribute[layer, pos.y, pos.x];
	}

	public bool GetBackground(IntVector2 pos)
	{
		if (!IsInField(pos))
		{
			return false;
		}
		return mBackground[pos.y, pos.x];
	}

	public PuzzleTile GetTile(IntVector2 pos, bool checkForTemp)
	{
		if (!IsInField(pos))
		{
			return null;
		}
		PuzzleTile attribute = GetAttribute(pos);
		if (attribute != null && attribute.Kind == Def.TILE_KIND.SPAWN)
		{
			return null;
		}
		PuzzleTile puzzleTile = mTile[pos.y, pos.x];
		if (puzzleTile == null && checkForTemp)
		{
			puzzleTile = mTileTemp[pos.y, pos.x];
		}
		return puzzleTile;
	}

	public PuzzleTile GetTileAllTiles(IntVector2 pos, bool checkForTemp)
	{
		if (!IsInField(pos))
		{
			return null;
		}
		PuzzleTile puzzleTile = mTile[pos.y, pos.x];
		if (puzzleTile == null && checkForTemp)
		{
			puzzleTile = mTileTemp[pos.y, pos.x];
		}
		return puzzleTile;
	}

	public Def.TILE_KIND GetCandyKind(IntVector2 pos)
	{
		PuzzleTile tile = GetTile(pos, false);
		if (tile == null)
		{
			return Def.TILE_KIND.NONE;
		}
		return tile.Kind;
	}

	public bool IsThreeOrMoreGather(IntVector2 pos)
	{
		PuzzleTile tile = GetTile(pos, false);
		if (tile != null && !tile.isColor())
		{
			return false;
		}
		if (GetCandyNumV(pos) >= 3 || GetCandyNumH(pos) >= 3)
		{
			return true;
		}
		return false;
	}

	public int GetCandyNumH(IntVector2 pos)
	{
		if (!IsInField(pos))
		{
			return 0;
		}
		return mNum[pos.y, pos.x].x;
	}

	public int GetCandyNumV(IntVector2 pos)
	{
		if (!IsInField(pos))
		{
			return 0;
		}
		return mNum[pos.y, pos.x].y;
	}

	public void SetCandyNumH(IntVector2 pos, int num)
	{
		if (IsInField(pos))
		{
			mNum[pos.y, pos.x].x = num;
		}
	}

	public void SetCandyNumV(IntVector2 pos, int num)
	{
		if (IsInField(pos))
		{
			mNum[pos.y, pos.x].y = num;
		}
	}

	public PuzzleTile[] GetColorTiles(bool freeTileOnly, bool allForm)
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				IntVector2 pos = new IntVector2(j, i);
				PuzzleTile tile = GetTile(pos, false);
				if (tile == null || !tile.isColor() || (!allForm && !tile.isNormalForm()))
				{
					continue;
				}
				if (freeTileOnly)
				{
					PuzzleTile attribute = GetAttribute(pos);
					if (attribute == null || (attribute != null && !attribute.isCapture()))
					{
						betterList.Add(tile);
					}
				}
				else
				{
					betterList.Add(tile);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile[] GetColorTiles(bool freeTileOnly)
	{
		return GetColorTiles(freeTileOnly, false);
	}

	public PuzzleTile[] GetRandomColorTiles(bool freeTileOnly = false)
	{
		PuzzleTile[] colorTiles = GetColorTiles(freeTileOnly);
		for (int i = 0; i < 500; i++)
		{
			int num = UnityEngine.Random.Range(0, colorTiles.Length);
			int num2 = UnityEngine.Random.Range(0, colorTiles.Length);
			if (num != num2)
			{
				PuzzleTile puzzleTile = colorTiles[num];
				colorTiles[num] = colorTiles[num2];
				colorTiles[num2] = puzzleTile;
			}
		}
		return colorTiles;
	}

	public PuzzleTile[] GetGimmicTiles()
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				IntVector2 pos = new IntVector2(j, i);
				PuzzleTile attribute = GetAttribute(pos);
				if (attribute != null && (attribute.isThroughWall() || attribute.isBrokenWall() || attribute.isCapture()))
				{
					betterList.Add(attribute);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile[] GetRandomGimmicTiles()
	{
		PuzzleTile[] gimmicTiles = GetGimmicTiles();
		for (int i = 0; i < 500; i++)
		{
			int num = UnityEngine.Random.Range(0, gimmicTiles.Length);
			int num2 = UnityEngine.Random.Range(0, gimmicTiles.Length);
			if (num != num2)
			{
				PuzzleTile puzzleTile = gimmicTiles[num];
				gimmicTiles[num] = gimmicTiles[num2];
				gimmicTiles[num2] = puzzleTile;
			}
		}
		return gimmicTiles;
	}

	public PuzzleTile[] GetEnemyGimmicTiles()
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile attribute = GetAttribute(new IntVector2(j, i));
				if (attribute != null && attribute.Kind == Def.TILE_KIND.INCREASE)
				{
					betterList.Add(attribute);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile[] GetRandomEnemyGimmicTiles()
	{
		PuzzleTile[] enemyGimmicTiles = GetEnemyGimmicTiles();
		for (int i = 0; i < 500; i++)
		{
			int num = UnityEngine.Random.Range(0, enemyGimmicTiles.Length);
			int num2 = UnityEngine.Random.Range(0, enemyGimmicTiles.Length);
			if (num != num2)
			{
				PuzzleTile puzzleTile = enemyGimmicTiles[num];
				enemyGimmicTiles[num] = enemyGimmicTiles[num2];
				enemyGimmicTiles[num2] = puzzleTile;
			}
		}
		return enemyGimmicTiles;
	}

	public PuzzleTile GetSkillBall()
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				if (tile != null && tile.Kind == Def.TILE_KIND.SKILL_BALL)
				{
					return tile;
				}
			}
		}
		return null;
	}

	public PuzzleTile[] GetExitableTiles()
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), true);
				if (tile != null && tile.isExitableTile() && !tile.isTalismanFace())
				{
					betterList.Add(tile);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile[] GetPairTiles()
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), true);
				if (tile != null && tile.isPair())
				{
					betterList.Add(tile);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile[] GetFootstampTiles()
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), true);
				if (tile != null && tile.isFootstamp())
				{
					betterList.Add(tile);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile[] GetMovableWallTiles()
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), true);
				if (tile != null && tile.isMovableWall())
				{
					betterList.Add(tile);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public PuzzleTile[] GetTalismanFaceTiles()
	{
		tempBetterList_PuzzleTile.Clear();
		BetterList<PuzzleTile> betterList = tempBetterList_PuzzleTile;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), true);
				if (tile != null && tile.isTalismanFace())
				{
					betterList.Add(tile);
				}
			}
		}
		if (betterList.size > 0)
		{
			return betterList.ToArray();
		}
		return new PuzzleTile[0];
	}

	public IntVector2 SearchFallTarget(IntVector2 pos, bool exitable)
	{
		IntVector2 intVector = null;
		intVector = HitCheckForFall(pos, exitable);
		if (intVector == null)
		{
			intVector = HitCheckForSideFall(pos, exitable);
		}
		return intVector;
	}

	public IntVector2 HitCheckForFall(IntVector2 pos, bool exitable)
	{
		PuzzleTile attribute = GetAttribute(new IntVector2(pos.x, pos.y));
		if (attribute != null && attribute.isCandyCaptor())
		{
			return null;
		}
		int num = -1;
		while (!HitCheckForFallSub(new IntVector2(pos.x, pos.y + num), exitable))
		{
			attribute = GetAttribute(new IntVector2(pos.x, pos.y + num));
			if (attribute != null && attribute.Kind == Def.TILE_KIND.THROUGH)
			{
				num--;
				continue;
			}
			return new IntVector2(pos.x, pos.y + num);
		}
		return null;
	}

	public bool HitCheckForFallSub(IntVector2 pos, bool exitable)
	{
		if (!IsInField(pos))
		{
			return true;
		}
		if (mTile[pos.y, pos.x] != null)
		{
			return true;
		}
		if (mTileTemp[pos.y, pos.x] != null)
		{
			return true;
		}
		PuzzleTile attribute;
		if ((attribute = GetAttribute(new IntVector2(pos.x, pos.y))) != null)
		{
			if (attribute.Kind == Def.TILE_KIND.WARP_IN)
			{
				ISMTileInfo tileInfo = mGame.mCurrentStage.GetTileInfo(pos.x, pos.y);
				return HitCheckForFallSub(new IntVector2(tileInfo.WarpOutPosX, tileInfo.WarpOutPosY - 1), exitable);
			}
			if (exitable && attribute.Kind == Def.TILE_KIND.EXIT)
			{
				return false;
			}
			if (!attribute.isThrough())
			{
				return true;
			}
			if (attribute.isCapture())
			{
				return true;
			}
		}
		else if (!mBackground[pos.y, pos.x])
		{
			return true;
		}
		return false;
	}

	public IntVector2 HitCheckForSideFall(IntVector2 pos, bool exitable)
	{
		PuzzleTile attribute = GetAttribute(new IntVector2(pos.x, pos.y));
		if (attribute != null && attribute.isCandyCaptor())
		{
			return null;
		}
		if (attribute != null && attribute.Kind == Def.TILE_KIND.SPAWN)
		{
			return null;
		}
		int num = -1;
		int num2 = -1;
		while (!HitCheckForSideFallSub(new IntVector2(pos.x + num2, pos.y + num), exitable))
		{
			attribute = GetAttribute(new IntVector2(pos.x + num2, pos.y + num));
			if (attribute != null && attribute.Kind == Def.TILE_KIND.THROUGH)
			{
				num--;
				continue;
			}
			return new IntVector2(pos.x + num2, pos.y + num);
		}
		num = -1;
		num2 = 1;
		while (!HitCheckForSideFallSub(new IntVector2(pos.x + num2, pos.y + num), exitable))
		{
			attribute = GetAttribute(new IntVector2(pos.x + num2, pos.y + num));
			if (attribute != null && attribute.Kind == Def.TILE_KIND.THROUGH)
			{
				num--;
				continue;
			}
			return new IntVector2(pos.x + num2, pos.y + num);
		}
		return null;
	}

	public bool HitCheckForSideFallSub(IntVector2 pos, bool exitable)
	{
		if (HitCheckForFallSub(pos, false))
		{
			return true;
		}
		for (int i = 1; pos.y + i < mFieldSize.y; i++)
		{
			PuzzleTile puzzleTile = mTile[pos.y + i, pos.x];
			if (puzzleTile == null)
			{
				puzzleTile = mTileTemp[pos.y + i, pos.x];
			}
			PuzzleTile attribute = GetAttribute(new IntVector2(pos.x, pos.y + i));
			bool flag = mBackground[pos.y + i, pos.x];
			if (puzzleTile != null)
			{
				if (!puzzleTile.isMovable())
				{
					break;
				}
				if (attribute == null)
				{
					if (!flag)
					{
						break;
					}
					return true;
				}
				if (!attribute.isCandyCaptor())
				{
					return true;
				}
			}
			if (attribute != null && (attribute.Kind == Def.TILE_KIND.SPAWN || attribute.Kind == Def.TILE_KIND.WARP_OUT))
			{
				return true;
			}
			if ((attribute != null && (!attribute.isThrough() || attribute.isCandyCaptor())) || (!flag && attribute == null))
			{
				break;
			}
		}
		return false;
	}

	public void PlaceTile(IntVector2 tilePos, PuzzleTile tile)
	{
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		if (GetCandyKind(new IntVector2(tilePos.x, tilePos.y + 1)) == tile.Kind)
		{
			num = GetCandyNumV(new IntVector2(tilePos.x, tilePos.y + 1));
		}
		if (GetCandyKind(new IntVector2(tilePos.x, tilePos.y - 1)) == tile.Kind)
		{
			num2 = GetCandyNumV(new IntVector2(tilePos.x, tilePos.y - 1));
		}
		if (GetCandyKind(new IntVector2(tilePos.x - 1, tilePos.y)) == tile.Kind)
		{
			num3 = GetCandyNumH(new IntVector2(tilePos.x - 1, tilePos.y));
		}
		if (GetCandyKind(new IntVector2(tilePos.x + 1, tilePos.y)) == tile.Kind)
		{
			num4 = GetCandyNumH(new IntVector2(tilePos.x + 1, tilePos.y));
		}
		int num5 = num + num2 + 1;
		int num6 = num3 + num4 + 1;
		PuzzleTile attribute = GetAttribute(new IntVector2(tilePos.x, tilePos.y));
		if (attribute != null && attribute.Kind == Def.TILE_KIND.SPAWN)
		{
			num5 = 1;
			num6 = 1;
			num = 0;
			num2 = 0;
			num3 = 0;
			num4 = 0;
		}
		for (int i = 1; i <= num; i++)
		{
			SetCandyNumV(new IntVector2(tilePos.x, tilePos.y + i), num5);
		}
		for (int j = 1; j <= num2; j++)
		{
			SetCandyNumV(new IntVector2(tilePos.x, tilePos.y - j), num5);
		}
		for (int k = 1; k <= num3; k++)
		{
			SetCandyNumH(new IntVector2(tilePos.x - k, tilePos.y), num6);
		}
		for (int l = 1; l <= num4; l++)
		{
			SetCandyNumH(new IntVector2(tilePos.x + l, tilePos.y), num6);
		}
		mTile[tilePos.y, tilePos.x] = tile;
		mTileTemp[tilePos.y, tilePos.x] = null;
		tile.TilePos = tilePos;
		SetCandyNumV(tilePos, num5);
		SetCandyNumH(tilePos, num6);
	}

	public PuzzleTile RemoveTile(PuzzleTile candy, bool removeToTemp)
	{
		IntVector2 tilePos = candy.TilePos;
		return RemoveTile(tilePos, removeToTemp);
	}

	public PuzzleTile RemoveTile(IntVector2 pos, bool removeToTemp)
	{
		PuzzleTile puzzleTile = mTile[pos.y, pos.x];
		if (puzzleTile == null)
		{
			puzzleTile = mTileTemp[pos.y, pos.x];
		}
		mTile[pos.y, pos.x] = null;
		mTileTemp[pos.y, pos.x] = null;
		if (removeToTemp && puzzleTile != null)
		{
			mTileTemp[pos.y, pos.x] = puzzleTile;
		}
		SetCandyNumV(pos, 0);
		SetCandyNumH(pos, 0);
		if (puzzleTile == null)
		{
			return null;
		}
		Def.TILE_KIND kind = puzzleTile.Kind;
		short num = 0;
		int i;
		for (i = 1; GetCandyKind(new IntVector2(pos.x, pos.y + i)) == kind; i++)
		{
			num++;
		}
		for (i--; i > 0; i--)
		{
			SetCandyNumV(new IntVector2(pos.x, pos.y + i), num);
		}
		num = 0;
		i = -1;
		while (GetCandyKind(new IntVector2(pos.x, pos.y + i)) == kind)
		{
			num++;
			i--;
		}
		for (i++; i < 0; i++)
		{
			SetCandyNumV(new IntVector2(pos.x, pos.y + i), num);
		}
		num = 0;
		i = -1;
		while (GetCandyKind(new IntVector2(pos.x + i, pos.y)) == kind)
		{
			num++;
			i--;
		}
		for (i++; i < 0; i++)
		{
			SetCandyNumH(new IntVector2(pos.x + i, pos.y), num);
		}
		num = 0;
		for (i = 1; GetCandyKind(new IntVector2(pos.x + i, pos.y)) == kind; i++)
		{
			num++;
		}
		for (i--; i > 0; i--)
		{
			SetCandyNumH(new IntVector2(pos.x + i, pos.y), num);
		}
		return puzzleTile;
	}

	public void MoveTempCandy(PuzzleTile candy, IntVector2 toPos)
	{
		IntVector2 tilePos = candy.TilePos;
		mTileTemp[tilePos.y, tilePos.x] = null;
		mTileTemp[toPos.y, toPos.x] = candy;
	}

	public void PlaceAttribute(IntVector2 pos, PuzzleTile attr)
	{
		if (mAttribute[0, pos.y, pos.x] == null)
		{
			mAttribute[0, pos.y, pos.x] = attr;
		}
		else if (mGame.mTileData[(int)attr.Kind].attributePriority <= mGame.mTileData[(int)mAttribute[0, pos.y, pos.x].Kind].attributePriority)
		{
			mAttribute[1, pos.y, pos.x] = attr;
		}
		else
		{
			mAttribute[1, pos.y, pos.x] = mAttribute[0, pos.y, pos.x];
			mAttribute[0, pos.y, pos.x] = attr;
		}
		attr.TilePos = pos;
	}

	public void RemoveAttribute(PuzzleTile attr)
	{
		IntVector2 tilePos = attr.TilePos;
		if (mAttribute[0, tilePos.y, tilePos.x] == attr)
		{
			mAttribute[0, tilePos.y, tilePos.x] = mAttribute[1, tilePos.y, tilePos.x];
			mAttribute[1, tilePos.y, tilePos.x] = null;
		}
		else if (mAttribute[1, tilePos.y, tilePos.x] == attr)
		{
			mAttribute[1, tilePos.y, tilePos.x] = null;
		}
	}

	public void CrushTile(PuzzleTile tile, Def.DIR moveDir)
	{
		IntVector2 tilePos = tile.TilePos;
		List<CrushGroupItem> list = new List<CrushGroupItem>();
		CrushGroupItem crushGroupItem = new CrushGroupItem(null, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL);
		CrushTileSearch(tilePos, true, true, crushGroupItem, list);
		int crushScore = GetCrushScore(list.Count);
		int i = 0;
		for (int count = list.Count; i < count; i++)
		{
			CrushGroupItem crushGroupItem2 = list[i];
			if (GetTile(crushGroupItem2.tile.TilePos, false) == null)
			{
				continue;
			}
			if (crushGroupItem.tile == crushGroupItem2.tile)
			{
				if (BeginCrush(crushGroupItem2.tile, 0f, Def.EFFECT_OVERRIDE.NORMAL, crushGroupItem2.kind, crushGroupItem2.form, crushScore, Def.TILE_KIND.NONE, 1))
				{
					BeginAttributeCrush(crushGroupItem2.tile.TilePos, 0f, false);
				}
			}
			else if (BeginCrush(crushGroupItem2.tile, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, crushScore, Def.TILE_KIND.NONE, 1))
			{
				BeginAttributeCrush(crushGroupItem2.tile.TilePos, 0f, false);
			}
		}
	}

	public void CrushTileSearch(IntVector2 pos, bool searchV, bool searchH, CrushGroupItem specialCandy, List<CrushGroupItem> crushGroup)
	{
		PuzzleTile tile = GetTile(pos, false);
		int candyNumV = GetCandyNumV(pos);
		int candyNumH = GetCandyNumH(pos);
		Def.TILE_FORM tILE_FORM = Def.TILE_FORM.NORMAL;
		Def.TILE_KIND kind = Def.TILE_KIND.NONE;
		if (candyNumV >= 5 || candyNumH >= 5)
		{
			tILE_FORM = Def.TILE_FORM.RAINBOW;
			kind = Def.TILE_KIND.SPECIAL;
		}
		else if (candyNumV >= 3 && candyNumH >= 3)
		{
			tILE_FORM = ((candyNumV + candyNumH - 1 < 6) ? Def.TILE_FORM.BOMB : Def.TILE_FORM.PAINT);
		}
		else if (candyNumV >= 4 && candyNumH < 4)
		{
			tILE_FORM = Def.TILE_FORM.WAVE_H;
		}
		else if (candyNumH >= 4 && candyNumV < 4)
		{
			tILE_FORM = Def.TILE_FORM.WAVE_V;
		}
		if (tILE_FORM != 0)
		{
			if (specialCandy.tile == null)
			{
				specialCandy.tile = tile;
				specialCandy.kind = kind;
				specialCandy.form = tILE_FORM;
			}
			else if (specialCandy.form == Def.TILE_FORM.WAVE_H || specialCandy.form == Def.TILE_FORM.WAVE_V)
			{
				if (tILE_FORM == Def.TILE_FORM.BOMB || tILE_FORM == Def.TILE_FORM.RAINBOW || tILE_FORM == Def.TILE_FORM.PAINT)
				{
					specialCandy.tile = tile;
					specialCandy.kind = kind;
					specialCandy.form = tILE_FORM;
				}
			}
			else if (specialCandy.form == Def.TILE_FORM.BOMB)
			{
				if (tILE_FORM == Def.TILE_FORM.RAINBOW || tILE_FORM == Def.TILE_FORM.PAINT)
				{
					specialCandy.tile = tile;
					specialCandy.kind = kind;
					specialCandy.form = tILE_FORM;
				}
			}
			else if (specialCandy.form == Def.TILE_FORM.RAINBOW && tILE_FORM == Def.TILE_FORM.PAINT)
			{
				specialCandy.tile = tile;
				specialCandy.kind = kind;
				specialCandy.form = tILE_FORM;
			}
		}
		crushGroup.Add(new CrushGroupItem(tile, kind, tILE_FORM));
		if (searchV && candyNumV >= 3)
		{
			bool searchV2 = false;
			bool searchH2 = true;
			if (!searchH)
			{
				searchH2 = false;
			}
			int i;
			for (i = 1; GetCandyKind(new IntVector2(pos.x, pos.y + i)) == tile.Kind; i++)
			{
				CrushTileSearch(new IntVector2(pos.x, pos.y + i), searchV2, searchH2, specialCandy, crushGroup);
			}
			i = -1;
			while (GetCandyKind(new IntVector2(pos.x, pos.y + i)) == tile.Kind)
			{
				CrushTileSearch(new IntVector2(pos.x, pos.y + i), searchV2, searchH2, specialCandy, crushGroup);
				i--;
			}
		}
		if (searchH && candyNumH >= 3)
		{
			bool searchV2 = true;
			bool searchH2 = false;
			if (!searchV)
			{
				searchV2 = false;
			}
			int i = -1;
			while (GetCandyKind(new IntVector2(pos.x + i, pos.y)) == tile.Kind)
			{
				CrushTileSearch(new IntVector2(pos.x + i, pos.y), searchV2, searchH2, specialCandy, crushGroup);
				i--;
			}
			for (i = 1; GetCandyKind(new IntVector2(pos.x + i, pos.y)) == tile.Kind; i++)
			{
				CrushTileSearch(new IntVector2(pos.x + i, pos.y), searchV2, searchH2, specialCandy, crushGroup);
			}
		}
	}

	public bool BeginFloat(PuzzleTile tile, Vector2 orignPos)
	{
		PuzzleTile attribute = GetAttribute(tile.TilePos);
		if (attribute != null && attribute.Kind == Def.TILE_KIND.SPAWN)
		{
			return false;
		}
		if (mState.GetStatus() == STATE.SELECT_BOOSTER_TARGET)
		{
			bool flag = false;
			switch (mUseBoosterKind)
			{
			case Def.BOOSTER_KIND.SELECT_ONE:
				if (tile.isColor() || tile.Form == Def.TILE_FORM.RAINBOW)
				{
					flag = true;
				}
				break;
			case Def.BOOSTER_KIND.SELECT_COLLECT:
				if (tile.isExitableTile() && !tile.isTalismanFace())
				{
					flag = true;
				}
				break;
			case Def.BOOSTER_KIND.COLOR_CRUSH:
				if (tile.isColor())
				{
					flag = true;
				}
				break;
			case Def.BOOSTER_KIND.PAIR_MAKER:
				if (tile.isPair() && BoosterUsableCheck(Def.BOOSTER_KIND.PAIR_MAKER))
				{
					flag = true;
				}
				break;
			}
			if (flag && OnBoosterUse(mUseBoosterKind, tile, true))
			{
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.CHAIN;
			}
			return false;
		}
		if (mState.GetStatus() == STATE.SELECT_SKILL_TARGET)
		{
			bool flag2 = false;
			Def.SKILL_TYPE sKILL_TYPE = mUseSkillType;
			if (sKILL_TYPE != 0 && sKILL_TYPE == Def.SKILL_TYPE.SELECT_TARGET && !tile.isGrayOut())
			{
				flag2 = true;
			}
			if (flag2 && OnSkillTargetSelect(tile))
			{
				mState.Reset(STATE.WAIT, true);
				NextState = STATE.CHAIN;
			}
			return false;
		}
		if (attribute != null && attribute.isCandyCaptor())
		{
			return false;
		}
		return tile.selectFloat(orignPos);
	}

	public void EndFloat(PuzzleTile tile)
	{
		tile.selectFloatEnd();
	}

	public void BeginMove(PuzzleTile tile1, Def.DIR dir)
	{
		if (!IsMovable() || !mGame.mTutorialManager.IsWantMove(tile1, dir))
		{
			return;
		}
		mGame.mTutorialManager.ClearWantMove();
		TutorialHighLightEnd();
		mGame.mTutorialManager.DeleteTutorialArrow();
		mGame.mTutorialManager.MessageFakeClose(null);
		IntVector2 tilePos = tile1.TilePos;
		IntVector2 pos = new IntVector2(tilePos.x + Def.DIR_OFS[(int)dir].x, tilePos.y + Def.DIR_OFS[(int)dir].y);
		PuzzleTile attribute = GetAttribute(tilePos);
		if ((attribute != null && attribute.Kind == Def.TILE_KIND.SPAWN) || (attribute != null && attribute.isCandyCaptor()))
		{
			return;
		}
		attribute = GetAttribute(pos);
		if (attribute != null && attribute.isCandyCaptor())
		{
			return;
		}
		PuzzleTile tile2 = GetTile(pos, false);
		if (tile2 != null && tile2.isMovable() && !tile2.isMove())
		{
			tile1.move(PuzzleTile.TILE_STATE.SWAP, dir);
			tile2.move(PuzzleTile.TILE_STATE.SWAP, Def.RETURN_DIR[(int)dir]);
			RemoveTile(tile1, true);
			RemoveTile(tile2, true);
			SwapPair item = new SwapPair(tile1, dir, tile2, Def.RETURN_DIR[(int)dir]);
			mSwapList.Add(item);
			if (mState.GetStatus() != STATE.CHAIN)
			{
				mState.Reset(STATE.CHAIN, true);
				mUserSwaped = true;
			}
			mGame.PlaySe("SE_SWAP", -1);
			if (!mFirstMove)
			{
				PlayStartAtFirstMove();
			}
		}
	}

	public void BeginMoveDiana(PuzzleTile tile1, Def.DIR dir)
	{
		IntVector2 tilePos = tile1.TilePos;
		IntVector2 intVector = new IntVector2(tilePos.x + Def.DIR_OFS[(int)dir].x, tilePos.y + Def.DIR_OFS[(int)dir].y);
		PuzzleTile attribute = GetAttribute(tilePos);
		if ((attribute != null && attribute.Kind == Def.TILE_KIND.SPAWN) || (attribute != null && attribute.isCandyCaptor()))
		{
			return;
		}
		attribute = GetAttribute(intVector);
		if (attribute != null && attribute.isCandyCaptor())
		{
			return;
		}
		PuzzleTile tile2 = GetTile(intVector, true);
		if (tile2 != null)
		{
			int count = mSwapList.Count;
			for (int num = count - 1; num >= 0; num--)
			{
				SwapPair swapPair = mSwapList[num];
				if (swapPair.mTile1 != null && swapPair.mTile1.Equals(tile2))
				{
					tile2.TilePos = intVector;
					swapPair.mTile1 = null;
				}
				if (swapPair.mTile2 != null && swapPair.mTile2.Equals(tile2))
				{
					tile2.TilePos = intVector;
					swapPair.mTile2 = null;
				}
			}
			tile2.move(PuzzleTile.TILE_STATE.SWAP, Def.RETURN_DIR[(int)dir]);
			RemoveTile(tile2, true);
		}
		tile1.move(PuzzleTile.TILE_STATE.SWAP, dir);
		RemoveTile(tile1, true);
		SwapPair item = new SwapPair(tile1, dir, tile2, Def.RETURN_DIR[(int)dir], true);
		mSwapList.Add(item);
		mGame.PlaySe("SE_SWAP", 1);
	}

	public int GetCrushScore(int matchNum)
	{
		if (matchNum > 5)
		{
			matchNum = 5;
		}
		int num = mCombo;
		if (num < 1)
		{
			num = 1;
		}
		return (num - 1) * Def.TILE_COMBO_SCORE + matchNum * Def.TILE_CLASH_SCORE;
	}

	public bool BeginCrush(PuzzleTile tile, float timer, Def.EFFECT_OVERRIDE effectOverride, Def.TILE_KIND nextKind, Def.TILE_FORM nextForm, int score, Def.TILE_KIND rainbowTargetKind, int bombSize, bool tapTrigger = false, bool forceCrush = false)
	{
		if (tile == null)
		{
			return false;
		}
		if (tile.isDiana())
		{
			return false;
		}
		if (tile.isTapTriggerTile() && !tapTrigger)
		{
			return false;
		}
		if (tile.isExitableTile() && !tile.isTalismanFace())
		{
			return true;
		}
		if (tile.isPair() && !forceCrush)
		{
			return false;
		}
		PuzzleTile attribute = GetAttribute(tile.TilePos);
		if (attribute != null && (attribute.Kind == Def.TILE_KIND.CAPTURE1 || attribute.Kind == Def.TILE_KIND.CAPTURE2))
		{
			return true;
		}
		if (tile.crush(timer, effectOverride, score, nextKind, nextForm, rainbowTargetKind, bombSize))
		{
			RemoveTile(tile, true);
			mCrushList.Add(tile);
			if (mState.GetStatus() == STATE.PLAY)
			{
				mState.Reset(STATE.CHAIN, true);
			}
			if (score <= 0)
			{
				return true;
			}
			if (tile.Form == Def.TILE_FORM.BOMB && effectOverride == Def.EFFECT_OVERRIDE.NORMAL)
			{
				return true;
			}
			OnBeginCrush(tile, timer);
			CrushFamiliarGimmickTile(tile.TilePos);
			return true;
		}
		return false;
	}

	public void BeginAttributeCrush(IntVector2 pos, float timer, bool effectCrush)
	{
		PuzzleTile attribute = GetAttribute(pos);
		if (attribute != null && attribute.AttributeCrush(timer, false))
		{
			mCrushList.Add(attribute);
			OnBeginCrush(attribute, timer);
			if (attribute.Kind == Def.TILE_KIND.INCREASE)
			{
				mEnemyTileCrushed = true;
			}
			PuzzleTile attribute2 = GetAttribute(pos, 0);
			PuzzleTile attribute3 = GetAttribute(pos, 1);
			bool flag = true;
			if ((attribute2 != null && !object.ReferenceEquals(attribute2, attribute)) || (attribute3 != null && !object.ReferenceEquals(attribute3, attribute)))
			{
				flag = false;
			}
			if (flag && attribute.NextKind == Def.TILE_KIND.NONE)
			{
				CrushFindGimmickTile(pos);
				CrushFamiliarGimmickTile(pos);
			}
			else if (flag && (attribute.NextKind == Def.TILE_KIND.CAPTURE0 || attribute.NextKind == Def.TILE_KIND.GROWN_UP0 || attribute.NextKind == Def.TILE_KIND.GROWN_UP1))
			{
				CrushFamiliarGimmickTile(pos);
			}
		}
		if (effectCrush)
		{
			return;
		}
		for (int i = 0; i < 8; i += 2)
		{
			IntVector2 intVector = new IntVector2(pos.x + Def.DIR_OFS[i].x, pos.y + Def.DIR_OFS[i].y);
			attribute = GetAttribute(intVector);
			if (attribute != null && attribute.AttributeCrush(timer, true))
			{
				mCrushList.Add(attribute);
				OnBeginCrush(attribute, timer);
				CrushFamiliarGimmickTile(intVector);
				if (attribute.Kind == Def.TILE_KIND.INCREASE)
				{
					mEnemyTileCrushed = true;
				}
				PuzzleTile attribute2 = GetAttribute(intVector, 0);
				PuzzleTile attribute3 = GetAttribute(intVector, 1);
				bool flag2 = true;
				if ((attribute2 != null && !object.ReferenceEquals(attribute2, attribute)) || (attribute3 != null && !object.ReferenceEquals(attribute3, attribute)))
				{
					flag2 = false;
				}
				if (flag2 && attribute.NextKind == Def.TILE_KIND.NONE)
				{
					CrushFindGimmickTile(intVector);
				}
			}
			attribute = GetTile(new IntVector2(pos.x + Def.DIR_OFS[i].x, pos.y + Def.DIR_OFS[i].y), false);
			if (attribute != null && attribute.isMovableWall() && attribute.AttributeCrush(timer, true))
			{
				RemoveTile(attribute, true);
				mCrushList.Add(attribute);
				OnBeginCrush(attribute, timer);
				CrushFamiliarGimmickTile(intVector);
			}
			if (attribute != null && attribute.isOrb() && attribute.AttributeCrush(timer, true))
			{
				RemoveTile(attribute, true);
				mCrushList.Add(attribute);
				OnBeginCrush(attribute, timer);
				CrushFamiliarGimmickTile(intVector);
			}
			if (attribute != null && attribute.isFlower() && attribute.AttributeCrush(timer, true))
			{
				RemoveTile(attribute, true);
				mCrushList.Add(attribute);
				OnBeginCrush(attribute, timer);
				CrushFamiliarGimmickTile(intVector);
			}
			if (attribute != null && attribute.isTalismanFace() && attribute.AttributeCrush(timer, true))
			{
				RemoveTile(attribute, true);
				mCrushList.Add(attribute);
				OnBeginCrush(attribute, timer);
				CrushFamiliarGimmickTile(intVector);
			}
		}
	}

	public void BeginTapCrush(PuzzleTile tile)
	{
		if (tile != null)
		{
			bool flag = BeginCrush(tile, 0f, Def.EFFECT_OVERRIDE.NORMAL, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, 0, Def.TILE_KIND.NONE, 1, true);
			if (tile.Form == Def.TILE_FORM.TAP_BOMB && flag)
			{
				BeginAttributeCrush(tile.TilePos, 0f, true);
			}
			if (!mFirstMove)
			{
				PlayStartAtFirstMove();
			}
		}
	}

	public void ReserveSkillBallCrush(PuzzleTile tile)
	{
		if (tile != null)
		{
			mReservedCrushSkillBall = tile;
		}
	}

	public bool IsCollectable(PuzzleTile candy)
	{
		if (candy != null && candy.isExitableTile())
		{
			IntVector2 tilePos = candy.TilePos;
			PuzzleTile attribute = GetAttribute(candy.TilePos);
			if (!IsInPlayArea(tilePos) || (attribute != null && attribute.Kind == Def.TILE_KIND.EXIT))
			{
				return true;
			}
		}
		return false;
	}

	public void BeginCollect(PuzzleTile tile)
	{
		int score = 0;
		if (tile.isExitableTile())
		{
			score = ((!tile.isPresentBox()) ? 2500 : 0);
		}
		RemoveTile(tile, false);
		tile.CreateScoreEffect(0.1f, score);
		OnCollect(tile, tile.Position, string.Empty);
		AddExitCount(tile.Kind, 1);
		DestroyTile(tile);
	}

	public void BeginCollectPair(PuzzleTile tile1, PuzzleTile tile2)
	{
		BeginCrush(tile1, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, 0, Def.TILE_KIND.NONE, 1, false, true);
		BeginCrush(tile2, 0f, Def.EFFECT_OVERRIDE.DISABLE, Def.TILE_KIND.NONE, Def.TILE_FORM.NORMAL, 0, Def.TILE_KIND.NONE, 1, false, true);
		AddExitCount(tile1.Kind, 1);
		AddExitCount(tile2.Kind, 1);
		PuzzleTile tile3 = new PuzzleTile(Def.TILE_KIND.PAIR0_COMPLETE, Def.TILE_FORM.NORMAL, new IntVector2(0, 0), Vector3.zero, null, null, Def.ITEM_CATEGORY.NONE, 0);
		OnCollect(tile3, tile1.Position, string.Empty);
		tile1.CreateScoreEffect(1.6f, 2500);
	}

	public void AddMoves(int deltaMoves)
	{
		if (!mGameFinished)
		{
			mMoves += deltaMoves;
			if (mMoves > Def.MAX_MOVES)
			{
				mMoves = Def.MAX_MOVES;
			}
			else if (mMoves < 0)
			{
				mMoves = 0;
			}
			OnChangeMoves(mMoves);
		}
	}

	public void AddTime(float deltaTime)
	{
		if (!mGameFinished)
		{
			mTime += deltaTime;
			if (mTime > Def.MAX_TIMER)
			{
				mTime = Def.MAX_TIMER;
			}
			else if (mTime < 0f)
			{
				mTime = 0f;
			}
			OnChangeTimer(mTime);
		}
	}

	public void AddHp(float deltaHP)
	{
	}

	public void AddScore(int deltaScore)
	{
		mScore += deltaScore;
		if (mScore > Def.MAX_SCORE)
		{
			mScore = Def.MAX_SCORE;
		}
		else if (mScore < 0)
		{
			mScore = 0;
		}
		OnChangeScore(mScore);
	}

	public void AddCombo()
	{
		mCombo++;
		int num = mCombo;
		if (num > 11)
		{
			num = 11;
		}
		mGame.PlaySe(comboSeTbl[num], 1);
		if (mMaxCombo < mCombo)
		{
			mMaxCombo = mCombo;
		}
		OnCombo(mCombo);
	}

	public void ClearCombo()
	{
		mCombo = 0;
	}

	public void AddSpecialCreateCount(Def.TILE_FORM form)
	{
		OnSpecialCreateCountUp(form);
	}

	public void AddSpecialCrushCount(Def.TILE_FORM form)
	{
		OnSpecialCrushCountUp(form);
	}

	public void Pause()
	{
		if (mState.GetStatus() != STATE.PAUSE)
		{
			InputEnable = false;
			TimerEnable = false;
			mState.Reset(STATE.PAUSE, true);
		}
	}

	public void PauseEnd(STATE nextState = STATE.BEFOREPLAY)
	{
		if (mState.GetStatus() == STATE.PAUSE)
		{
			InputEnable = true;
			if (mFirstMove)
			{
				TimerEnable = true;
			}
			mState.Reset(nextState, true);
		}
	}

	private void SetFallRock(bool flg)
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				if (mTile[i, j] != null)
				{
					mTile[i, j].SetFallRock(flg);
				}
				if (mTileTemp[i, j] != null)
				{
					mTileTemp[i, j].SetFallRock(flg);
				}
				for (int k = 0; k < Def.ATTRIBUTE_LAYER_NUM; k++)
				{
					if (mAttribute[k, i, j] != null)
					{
						mAttribute[k, i, j].SetFallRock(flg);
					}
				}
			}
		}
	}

	public bool IsItemUsable()
	{
		if (!IsMoveFinishAllSub())
		{
			return false;
		}
		if (mState.GetStatus() != STATE.PLAY)
		{
			return false;
		}
		if (mLoseCondition == Def.STAGE_LOSE_CONDITION.TIME && mTime <= 0f)
		{
			return false;
		}
		return true;
	}

	public bool IsItemUsableTapItem()
	{
		return IsItemUsable();
	}

	public bool IsPlaying()
	{
		if (mState.GetStatus() == STATE.PLAY || mState.GetStatus() == STATE.CHAIN)
		{
			return true;
		}
		return false;
	}

	public bool IsPausing()
	{
		if (mState.GetStatus() == STATE.PAUSE)
		{
			return true;
		}
		return false;
	}

	public bool IsMovable()
	{
		if (!InputEnable)
		{
			return false;
		}
		switch (mLoseCondition)
		{
		case Def.STAGE_LOSE_CONDITION.MOVES:
			if (mMoves <= 0)
			{
				return false;
			}
			break;
		case Def.STAGE_LOSE_CONDITION.TIME:
			if (mTime <= 0f)
			{
				return false;
			}
			break;
		default:
			if (mMoves <= 0)
			{
				return false;
			}
			break;
		}
		return true;
	}

	public bool IsGameFinished()
	{
		return mGameFinished;
	}

	public void BeginBoosterTargetSelect(Def.BOOSTER_KIND kind)
	{
		mUseBoosterKind = kind;
		mState.Reset(STATE.WAIT, true);
		StartCoroutine(BeginBoosterTargetSelectWait(kind));
	}

	private IEnumerator BeginBoosterTargetSelectWait(Def.BOOSTER_KIND kind)
	{
		yield return new WaitForSeconds(0.2f);
		mState.Reset(STATE.SELECT_BOOSTER_TARGET, true);
		AppealEnd();
		switch (kind)
		{
		case Def.BOOSTER_KIND.SELECT_COLLECT:
		{
			for (int cy3 = 0; cy3 < mFieldSize.y; cy3++)
			{
				for (int cx3 = 0; cx3 < mFieldSize.x; cx3++)
				{
					PuzzleTile tile8 = GetTile(new IntVector2(cx3, cy3), false);
					if (tile8 != null && (!tile8.isExitableTile() || tile8.isTalismanFace()))
					{
						tile8.GrayOut(true);
					}
					tile8 = GetAttribute(new IntVector2(cx3, cy3), 0);
					if (tile8 != null && (!tile8.isExitableTile() || tile8.isTalismanFace()))
					{
						tile8.GrayOut(true);
					}
					tile8 = GetAttribute(new IntVector2(cx3, cy3), 1);
					if (tile8 != null && (!tile8.isExitableTile() || tile8.isTalismanFace()))
					{
						tile8.GrayOut(true);
					}
				}
			}
			break;
		}
		case Def.BOOSTER_KIND.PAIR_MAKER:
		{
			for (int cy2 = 0; cy2 < mFieldSize.y; cy2++)
			{
				for (int cx2 = 0; cx2 < mFieldSize.x; cx2++)
				{
					PuzzleTile tile5 = GetTile(new IntVector2(cx2, cy2), false);
					if (tile5 != null && !tile5.isPair())
					{
						tile5.GrayOut(true);
					}
					tile5 = GetAttribute(new IntVector2(cx2, cy2), 0);
					if (tile5 != null && !tile5.isPair())
					{
						tile5.GrayOut(true);
					}
					tile5 = GetAttribute(new IntVector2(cx2, cy2), 1);
					if (tile5 != null && !tile5.isPair())
					{
						tile5.GrayOut(true);
					}
				}
			}
			break;
		}
		case Def.BOOSTER_KIND.BLOCK_CRUSH:
		{
			for (int cy = 0; cy < mFieldSize.y; cy++)
			{
				for (int cx = 0; cx < mFieldSize.x; cx++)
				{
					PuzzleTile tile2 = GetTile(new IntVector2(cx, cy), false);
					if (tile2 != null && !tile2.isFlower() && !tile2.isTalismanFace())
					{
						tile2.GrayOut(true);
					}
					tile2 = GetAttribute(new IntVector2(cx, cy), 0);
					if (tile2 != null && !tile2.isGrownUp() && !tile2.isBrokenWall() && !tile2.isCapture() && tile2.Kind != Def.TILE_KIND.INCREASE)
					{
						tile2.GrayOut(true);
					}
				}
			}
			break;
		}
		}
	}

	public void TargetSelectEnd()
	{
		mState.Reset(STATE.PLAY, true);
	}

	public void TargetSelectEffectEnd()
	{
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				if (mTile[i, j] != null)
				{
					mTile[i, j].GrayOut(false);
				}
				if (mAttribute[0, i, j] != null)
				{
					mAttribute[0, i, j].GrayOut(false);
				}
				if (mAttribute[1, i, j] != null)
				{
					mAttribute[1, i, j].GrayOut(false);
				}
			}
		}
		UpdateAppealList();
	}

	public bool BoosterUsableCheck(Def.BOOSTER_KIND boosterKind)
	{
		switch (boosterKind)
		{
		case Def.BOOSTER_KIND.SELECT_COLLECT:
		{
			PuzzleTile[] exitableTiles = GetExitableTiles();
			if (exitableTiles.Length < 1)
			{
				return false;
			}
			break;
		}
		case Def.BOOSTER_KIND.PAIR_MAKER:
		{
			PuzzleTile[] pairTiles = GetPairTiles();
			if (pairTiles.Length < 2)
			{
				return false;
			}
			break;
		}
		}
		return true;
	}

	public void BeginSkillTargetSelect(Def.SKILL_TYPE type)
	{
		mUseSkillType = type;
		mState.Reset(STATE.SELECT_SKILL_TARGET, true);
		AppealEnd();
		if (type != Def.SKILL_TYPE.SELECT_TARGET)
		{
			return;
		}
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				PuzzleTile tile = GetTile(new IntVector2(j, i), false);
				PuzzleTile attribute = GetAttribute(new IntVector2(j, i));
				if (tile != null)
				{
					if (attribute != null && attribute.isCandyCaptor())
					{
						tile.GrayOut(true);
					}
					else if (!tile.isColor() && tile.Form != Def.TILE_FORM.RAINBOW)
					{
						tile.GrayOut(true);
					}
				}
				tile = GetAttribute(new IntVector2(j, i), 0);
				if (tile != null)
				{
					tile.GrayOut(true);
				}
				tile = GetAttribute(new IntVector2(j, i), 1);
				if (tile != null)
				{
					tile.GrayOut(true);
				}
			}
		}
	}

	public int GetTileCount(Def.TILE_KIND kind)
	{
		int num = 0;
		for (int i = 0; i < mFieldSize.y; i++)
		{
			for (int j = 0; j < mFieldSize.x; j++)
			{
				if (mTile[i, j] != null && mTile[i, j].Kind == kind)
				{
					num++;
				}
				if (mTileTemp[i, j] != null && mTileTemp[i, j].Kind == kind)
				{
					num++;
				}
				if (mAttribute[0, i, j] != null && mAttribute[0, i, j].Kind == kind)
				{
					num++;
				}
				if (mAttribute[1, i, j] != null && mAttribute[1, i, j].Kind == kind)
				{
					num++;
				}
			}
		}
		return num;
	}

	public int GetSpawnCount(Def.TILE_KIND kind)
	{
		int value = 0;
		mSpawnCount.TryGetValue(kind, out value);
		return value;
	}

	protected int AddSpawnCount(Def.TILE_KIND kind, int num, bool removeIfZero = false)
	{
		int spawnCount = GetSpawnCount(kind);
		spawnCount += num;
		mSpawnCount[kind] = spawnCount;
		if (spawnCount < 0 && removeIfZero)
		{
			mSpawnCount.Remove(kind);
		}
		return spawnCount;
	}

	public int GetExitCount(Def.TILE_KIND kind)
	{
		int value = 0;
		mExitCount.TryGetValue(kind, out value);
		return value;
	}

	public int AddExitCount(Def.TILE_KIND kind, int num, bool removeIfZero = false)
	{
		switch (kind)
		{
		case Def.TILE_KIND.TALISMAN0_1:
		case Def.TILE_KIND.TALISMAN0_2:
			kind = Def.TILE_KIND.TALISMAN0_0;
			break;
		case Def.TILE_KIND.TALISMAN1_1:
		case Def.TILE_KIND.TALISMAN1_2:
			kind = Def.TILE_KIND.TALISMAN1_0;
			break;
		case Def.TILE_KIND.TALISMAN2_1:
		case Def.TILE_KIND.TALISMAN2_2:
			kind = Def.TILE_KIND.TALISMAN2_0;
			break;
		}
		int exitCount = GetExitCount(kind);
		exitCount += num;
		mExitCount[kind] = exitCount;
		if (exitCount < 0 && removeIfZero)
		{
			mExitCount.Remove(kind);
		}
		return exitCount;
	}

	public int GetSpawnCountForm(Def.TILE_FORM form)
	{
		int value = 0;
		mFormRemainCount.TryGetValue(form, out value);
		return value;
	}

	protected int AddSpawnCountForm(Def.TILE_FORM form, int num, bool removeIfZero = false)
	{
		int spawnCountForm = GetSpawnCountForm(form);
		spawnCountForm += num;
		mFormRemainCount[form] = spawnCountForm;
		if (spawnCountForm < 0 && removeIfZero)
		{
			mFormRemainCount.Remove(form);
		}
		return spawnCountForm;
	}

	protected bool IsNormaAchivementCheck()
	{
		for (int i = 0; i < mNormaKindArray.Length; i++)
		{
			if (mGame.mCurrentStage.GetTargetNum(mNormaKindArray[i]) > GetExitCount(mNormaKindArray[i]))
			{
				return false;
			}
		}
		return true;
	}

	protected bool HasIngredient(int x, int y)
	{
		if (mWinCondition == Def.STAGE_WIN_CONDITION.COLLECT && (GetSpawnCount(Def.TILE_KIND.INGREDIENT0) > 0 || GetSpawnCount(Def.TILE_KIND.INGREDIENT1) > 0 || GetSpawnCount(Def.TILE_KIND.INGREDIENT2) > 0 || GetSpawnCount(Def.TILE_KIND.INGREDIENT3) > 0))
		{
			PuzzleTile attribute = GetAttribute(new IntVector2(x, y - 1));
			if (attribute == null || attribute.isThrough())
			{
				return true;
			}
		}
		return false;
	}

	protected bool IsIngredientSpawnable(int max, int remain, int dropped)
	{
		if (max > 0 && remain > 0 && remain + dropped >= max)
		{
			return true;
		}
		return false;
	}

	protected Def.TILE_KIND RandomIngredient(bool subtract = true)
	{
		if (!Util.CommonLottery(50, 100))
		{
			return Def.TILE_KIND.NONE;
		}
		Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
		int[] array = new int[4];
		if (IsIngredientSpawnable(mGame.mCurrentStage.Ingredient0, GetSpawnCount(Def.TILE_KIND.INGREDIENT0), GetExitCount(Def.TILE_KIND.INGREDIENT0)))
		{
			array[0] = 1;
		}
		if (IsIngredientSpawnable(mGame.mCurrentStage.Ingredient1, GetSpawnCount(Def.TILE_KIND.INGREDIENT1), GetExitCount(Def.TILE_KIND.INGREDIENT1)))
		{
			array[1] = 1;
		}
		if (IsIngredientSpawnable(mGame.mCurrentStage.Ingredient2, GetSpawnCount(Def.TILE_KIND.INGREDIENT2), GetExitCount(Def.TILE_KIND.INGREDIENT2)))
		{
			array[2] = 1;
		}
		if (IsIngredientSpawnable(mGame.mCurrentStage.Ingredient3, GetSpawnCount(Def.TILE_KIND.INGREDIENT3), GetExitCount(Def.TILE_KIND.INGREDIENT3)))
		{
			array[3] = 1;
		}
		int num = Util.CommonLottery(array);
		if (num != -1)
		{
			tILE_KIND = (Def.TILE_KIND)(8 + num);
			if (subtract)
			{
				AddSpawnCount(tILE_KIND, -1);
			}
		}
		return tILE_KIND;
	}

	protected bool HasPair(int x, int y)
	{
		if (mWinCondition == Def.STAGE_WIN_CONDITION.PAIR && GetSpawnCount(Def.TILE_KIND.PAIR0_PARTS) > 0)
		{
			PuzzleTile attribute = GetAttribute(new IntVector2(x, y - 1));
			if (attribute == null || attribute.isThrough())
			{
				return true;
			}
		}
		return false;
	}

	protected Def.TILE_KIND RandomPair(bool subtract = true)
	{
		if (!Util.CommonLottery(50, 100))
		{
			return Def.TILE_KIND.NONE;
		}
		Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
		if (mGame.mCurrentStage.PairLimit0 - GetSpawnCount(Def.TILE_KIND.PAIR0_PARTS) - GetExitCount(Def.TILE_KIND.PAIR0_PARTS) < 7)
		{
			tILE_KIND = Def.TILE_KIND.PAIR0_PARTS;
			if (subtract)
			{
				AddSpawnCount(tILE_KIND, -1);
			}
		}
		return tILE_KIND;
	}

	protected bool HasMovableWall(int x, int y)
	{
		if (GetSpawnCount(Def.TILE_KIND.MOVABLE_WALL0) + GetSpawnCount(Def.TILE_KIND.MOVABLE_WALL1) + GetSpawnCount(Def.TILE_KIND.MOVABLE_WALL2) > 0)
		{
			PuzzleTile attribute = GetAttribute(new IntVector2(x, y - 1));
			if (attribute == null || attribute.isThrough())
			{
				return true;
			}
		}
		return false;
	}

	protected Def.TILE_KIND RandomMovableWall(bool subtract = true)
	{
		Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
		int[] array = new int[4];
		if (GetSpawnCount(Def.TILE_KIND.MOVABLE_WALL0) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FCookie1Freq, 100))
		{
			array[0] = 1;
		}
		if (GetSpawnCount(Def.TILE_KIND.MOVABLE_WALL1) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FCookie2Freq, 100))
		{
			array[1] = 1;
		}
		if (GetSpawnCount(Def.TILE_KIND.MOVABLE_WALL2) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FCookie3Freq, 100))
		{
			array[2] = 1;
		}
		int num = Util.CommonLottery(array);
		if (num != -1)
		{
			tILE_KIND = (Def.TILE_KIND)(24 + num);
			if (subtract)
			{
				AddSpawnCount(tILE_KIND, -1);
			}
		}
		return tILE_KIND;
	}

	protected bool HasOrb(int x, int y)
	{
		if (GetSpawnCount(Def.TILE_KIND.ORB_WAVE_V0) + GetSpawnCount(Def.TILE_KIND.ORB_WAVE_H0) + GetSpawnCount(Def.TILE_KIND.ORB_BOMB0) + GetSpawnCount(Def.TILE_KIND.ORB_BOSSDAMAGE0) > 0)
		{
			PuzzleTile attribute = GetAttribute(new IntVector2(x, y - 1));
			if (attribute == null || attribute.isThrough())
			{
				return true;
			}
		}
		return false;
	}

	protected Def.TILE_KIND RandomOrb(bool subtract = true)
	{
		Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
		int[] array = new int[4];
		if (GetSpawnCount(Def.TILE_KIND.ORB_WAVE_V0) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FOrb1Freq, 100))
		{
			array[0] = 1;
		}
		if (GetSpawnCount(Def.TILE_KIND.ORB_WAVE_H0) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FOrb2Freq, 100))
		{
			array[1] = 1;
		}
		if (GetSpawnCount(Def.TILE_KIND.ORB_BOMB0) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FOrb3Freq, 100))
		{
			array[2] = 1;
		}
		if (GetSpawnCount(Def.TILE_KIND.ORB_BOSSDAMAGE0) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FOrb4Freq, 100))
		{
			array[3] = 1;
		}
		int num = Util.CommonLottery(array);
		if (num != -1)
		{
			tILE_KIND = (Def.TILE_KIND)(78 + 3 * num);
			if (subtract)
			{
				AddSpawnCount(tILE_KIND, -1);
			}
		}
		return tILE_KIND;
	}

	protected bool HasFlower(int x, int y)
	{
		if (GetSpawnCount(Def.TILE_KIND.MOVABLE_FLOWER0) + GetSpawnCount(Def.TILE_KIND.MOVABLE_FLOWER1) > 0)
		{
			PuzzleTile attribute = GetAttribute(new IntVector2(x, y - 1));
			if (attribute == null || attribute.isThrough())
			{
				return true;
			}
		}
		return false;
	}

	protected Def.TILE_KIND RandomFlower(bool subtract = true)
	{
		Def.TILE_KIND tILE_KIND = Def.TILE_KIND.NONE;
		int[] array = new int[2];
		if (GetSpawnCount(Def.TILE_KIND.MOVABLE_FLOWER0) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FPerl00Freq1, 100))
		{
			array[0] = 1;
		}
		if (GetSpawnCount(Def.TILE_KIND.MOVABLE_FLOWER1) > 0 && Util.CommonLottery((int)mGame.mCurrentStage.FPerl01Freq1, 100))
		{
			array[1] = 1;
		}
		int num = Util.CommonLottery(array);
		if (num != -1)
		{
			tILE_KIND = (Def.TILE_KIND)(96 + num);
			if (subtract)
			{
				AddSpawnCount(tILE_KIND, -1);
			}
		}
		return tILE_KIND;
	}

	protected bool SpawnLotteryForm(Def.TILE_FORM form, int freq)
	{
		if (GetSpawnCountForm(form) > 0 && Util.CommonLottery(freq, 100))
		{
			AddSpawnCountForm(form, -1);
			return true;
		}
		return false;
	}

	protected bool HasJewel()
	{
		if (GetSpawnCountForm(Def.TILE_FORM.JEWEL) + GetSpawnCountForm(Def.TILE_FORM.JEWEL3) + GetSpawnCountForm(Def.TILE_FORM.JEWEL5) + GetSpawnCountForm(Def.TILE_FORM.JEWEL9) > 0)
		{
			return true;
		}
		return false;
	}

	protected Def.TILE_FORM RandomJewel(bool subtract = true)
	{
		if (!Util.CommonLottery((int)mGame.mCurrentStage.FJewelFreq1, 100))
		{
			return Def.TILE_FORM.NORMAL;
		}
		Def.TILE_FORM tILE_FORM = Def.TILE_FORM.NORMAL;
		if (Util.CommonLottery(1, 2))
		{
			int[] array = new int[4];
			if (GetSpawnCountForm(Def.TILE_FORM.JEWEL) > 0)
			{
				array[0] = 1;
			}
			if (GetSpawnCountForm(Def.TILE_FORM.JEWEL3) > 0)
			{
				array[1] = 1;
			}
			if (GetSpawnCountForm(Def.TILE_FORM.JEWEL5) > 0)
			{
				array[2] = 1;
			}
			if (GetSpawnCountForm(Def.TILE_FORM.JEWEL9) > 0)
			{
				array[3] = 1;
			}
			switch (Util.CommonLottery(array))
			{
			case 0:
				tILE_FORM = Def.TILE_FORM.JEWEL;
				break;
			case 1:
				tILE_FORM = Def.TILE_FORM.JEWEL3;
				break;
			case 2:
				tILE_FORM = Def.TILE_FORM.JEWEL5;
				break;
			case 3:
				tILE_FORM = Def.TILE_FORM.JEWEL9;
				break;
			}
		}
		else if (GetSpawnCountForm(Def.TILE_FORM.JEWEL9) > 0)
		{
			tILE_FORM = Def.TILE_FORM.JEWEL9;
		}
		else if (GetSpawnCountForm(Def.TILE_FORM.JEWEL5) > 0)
		{
			tILE_FORM = Def.TILE_FORM.JEWEL5;
		}
		else if (GetSpawnCountForm(Def.TILE_FORM.JEWEL3) > 0)
		{
			tILE_FORM = Def.TILE_FORM.JEWEL3;
		}
		else if (GetSpawnCountForm(Def.TILE_FORM.JEWEL) > 0)
		{
			tILE_FORM = Def.TILE_FORM.JEWEL;
		}
		if (tILE_FORM != 0 && subtract)
		{
			AddSpawnCountForm(tILE_FORM, -1);
		}
		return tILE_FORM;
	}

	public int CheckGotStars()
	{
		int num = 0;
		if (mScore >= mGame.mCurrentStage.GoldScore)
		{
			num++;
		}
		if (mScore >= mGame.mCurrentStage.SilverScore)
		{
			num++;
		}
		if (mScore >= mGame.mCurrentStage.BronzeScore)
		{
			num++;
		}
		return num;
	}

	public void SetCollectEffectBusy(bool enable)
	{
		if (enable)
		{
			mCollectEffectBusy++;
		}
		else
		{
			mCollectEffectBusy--;
		}
	}

	public void AddCollectItemList(Def.TILE_KIND kind, Def.ITEM_CATEGORY category, int itemIndex)
	{
		SpecialSpawnData item = new SpecialSpawnData(kind, category, itemIndex, Def.UNLOCK_TYPE.NONE, 0);
		mCollectItemList.Add(item);
	}

	public SpecialSpawnData[] GetCollectedItemList()
	{
		return mCollectItemList.ToArray();
	}

	public void CancelRequest()
	{
		mCancelRequest = true;
	}

	public Vector3 GetBossGimmickPos()
	{
		if (mBossCharacter == null)
		{
			return Vector3.zero;
		}
		return mBossCharacter.GetCenterPos();
	}

	public bool AddBossDamage(Def.TILE_KIND kind)
	{
		if (mBossCharacter == null)
		{
			return false;
		}
		int amount = 10;
		if (kind == Def.TILE_KIND.ORB_BOSSDAMAGE2)
		{
			amount = 30;
		}
		return mBossCharacter.AddDamage(amount);
	}

	public bool IsBossGimmickWeak()
	{
		if (mBossCharacter == null)
		{
			return true;
		}
		return mBossCharacter.IsWeak();
	}

	public bool IsBossGimmickDead()
	{
		if (mBossCharacter == null)
		{
			return true;
		}
		return mBossCharacter.IsDead();
	}

	public bool IsBossGimmickDeadBusy()
	{
		if (mBossCharacter == null)
		{
			return true;
		}
		return mBossCharacter.IsDeadBusy();
	}

	protected void CrushFindGimmickTile(IntVector2 tilePos)
	{
		for (int i = 0; i < mFindGimmicks.Count; i++)
		{
			mFindGimmicks[i].OnAttributeCrush(tilePos);
		}
	}

	protected void CrushFamiliarGimmickTile(IntVector2 tilePos)
	{
		for (int num = mFamiliarGimmicks.Count - 1; num >= 0; num--)
		{
			if (mFamiliarGimmicks[num].OnCrush(tilePos))
			{
				mFamiliarGimmicks.RemoveAt(num);
			}
		}
	}

	public Vector3 GetDianaGimmickPos()
	{
		if (mDianaCharacter == null)
		{
			return Vector3.zero;
		}
		return mDianaCharacter.GetCenterPos();
	}

	public void AddDianaStep(int amount)
	{
		if (!(mDianaCharacter == null))
		{
			mDianaCharacter.AddStepCount(amount);
		}
	}

	public bool IsDianaGimmickChance()
	{
		if (mDianaCharacter == null)
		{
			return false;
		}
		return mDianaCharacter.IsChance();
	}

	public bool IsDianaGimmickGoal()
	{
		if (mDianaCharacter == null)
		{
			return false;
		}
		return mDianaCharacter.IsGoal();
	}

	public void AddOneShotAnime(OneShotAnime a_anime)
	{
		mOneShotList.Add(a_anime);
	}

	public void OneShotAnime_OnFinished(OneShotAnime a_anime)
	{
		if (a_anime != null)
		{
			mOneShotList.Remove(a_anime);
			GameMain.SafeDestroy(ref a_anime);
		}
		mOneShotList.RemoveAll((OneShotAnime p) => p == null);
	}

	public virtual void MakeHitNum(Vector3 _pos)
	{
	}
}
