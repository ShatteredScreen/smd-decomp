using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class BIJZoomCamera : MonoBehaviour
{
	private const int OS_ANDROID = 0;

	private const int OS_IOS = 1;

	private const int OS_OTHER = 2;

	private int m_OSflag;

	public float minOrthographicSize = 100f;

	public float maxOrthographicSize = 1600f;

	public float zoomValue = 1f;

	public float minPosX = -10000f;

	public float maxPosX = 10000f;

	public float minPosY = -10000f;

	public float maxPosY = 10000f;

	public bool limitflag = true;

	public float frictionValue = 8f;

	public float frictionStopSpeed = 3f;

	public bool autoHandleInput = true;

	private Vector2 m_oldMousePosition = new Vector2(0f, 0f);

	private float m_oldTouchDist;

	private float m_deltaTouchDist;

	private Vector2 m_touchCenterPosition = new Vector2(0f, 0f);

	private Vector2 m_MoveSpeed = new Vector2(0f, 0f);

	private bool m_afterPinch;

	private Camera m_camera;

	private Vector4 m_clipping = new Vector4(0f, 0f, 0f, 0f);

	private bool m_clippingUpdate = true;

	public Vector4 ClippingRect
	{
		get
		{
			return m_clipping;
		}
	}

	public static BIJZoomCamera Instance
	{
		get
		{
			return UnityEngine.Object.FindObjectOfType(typeof(BIJZoomCamera)) as BIJZoomCamera;
		}
	}

	[method: MethodImpl(32)]
	public static event Action<Camera, Vector4> UpdateClippingEvent;

	private void Awake()
	{
		m_camera = base.gameObject.GetComponent<Camera>();
	}

	private void Start()
	{
		if (SystemInfo.operatingSystem.Contains("Android"))
		{
			m_OSflag = 0;
		}
		else if (SystemInfo.operatingSystem.Contains("iPhone"))
		{
			m_OSflag = 1;
		}
		else
		{
			m_OSflag = 2;
		}
		base.gameObject.transform.position = checkLimitPosition(base.gameObject.transform.position);
	}

	private void Update()
	{
		UpdateMousePos();
		if (autoHandleInput)
		{
			HandleInput();
		}
		UpdateMovePosition();
	}

	public void UpdateMousePos()
	{
		if (m_OSflag == 2)
		{
			if (Input.GetMouseButtonDown(0))
			{
				m_oldMousePosition = Input.mousePosition;
				m_MoveSpeed = new Vector2(0f, 0f);
			}
			return;
		}
		if (Input.touchCount == 0)
		{
			m_afterPinch = false;
		}
		if (Input.touchCount == 1 && !m_afterPinch)
		{
			Touch touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				m_oldMousePosition = touch.position;
				m_MoveSpeed = new Vector2(0f, 0f);
			}
		}
		if (Input.touchCount >= 2)
		{
			m_MoveSpeed = new Vector2(0f, 0f);
			Touch touch2 = Input.GetTouch(0);
			Touch touch3 = Input.GetTouch(1);
			if ((touch2.phase == TouchPhase.Began) | (touch3.phase == TouchPhase.Began))
			{
				m_oldTouchDist = Mathf.Sqrt((touch2.position.x - touch3.position.x) * (touch2.position.x - touch3.position.x) + (touch2.position.y - touch3.position.y) * (touch2.position.y - touch3.position.y));
			}
		}
	}

	public void HandleInput()
	{
		if (m_OSflag == 2)
		{
			if (Input.GetMouseButtonDown(0))
			{
				m_oldMousePosition = Input.mousePosition;
			}
			else if (Input.GetMouseButton(0))
			{
				Vector3 mousePosition = Input.mousePosition;
				mousePosition.z = 10f;
				Vector3 vector = base.gameObject.GetComponent<Camera>().ScreenToWorldPoint(mousePosition);
				mousePosition = m_oldMousePosition;
				mousePosition.z = 10f;
				Vector3 vector2 = base.gameObject.GetComponent<Camera>().ScreenToWorldPoint(mousePosition);
				vector -= vector2;
				m_MoveSpeed = new Vector2(0f - vector.x, 0f - vector.y);
				m_oldMousePosition = Input.mousePosition;
			}
			float axis = Input.GetAxis("Mouse ScrollWheel");
			if (axis != 0f)
			{
				m_MoveSpeed = new Vector2(0f, 0f);
				float num = (0f - axis) * 100f * zoomValue;
				Camera component = base.gameObject.GetComponent<Camera>();
				component.orthographicSize += num;
				if (component.orthographicSize < minOrthographicSize)
				{
					component.orthographicSize = minOrthographicSize;
				}
				else if (component.orthographicSize > maxOrthographicSize)
				{
					component.orthographicSize = maxOrthographicSize;
				}
				float x = (Input.mousePosition.x - (float)(Screen.width / 2)) / (float)(Screen.width / 2) * num * 1.9f;
				float y = (Input.mousePosition.y - (float)(Screen.height / 2)) / (float)(Screen.height / 2) * num * 1f;
				SubPosition(new Vector3(x, y, 0f));
			}
			return;
		}
		if (Input.touchCount == 0)
		{
			m_afterPinch = false;
		}
		if (Input.touchCount == 1 && !m_afterPinch)
		{
			Touch touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				m_oldMousePosition = touch.position;
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				Vector3 position = touch.position;
				position.z = 10f;
				Vector3 vector3 = base.gameObject.GetComponent<Camera>().ScreenToWorldPoint(position);
				position = m_oldMousePosition;
				position.z = 10f;
				Vector3 vector4 = base.gameObject.GetComponent<Camera>().ScreenToWorldPoint(position);
				vector3 -= vector4;
				m_MoveSpeed = new Vector2(0f - vector3.x, 0f - vector3.y);
				m_oldMousePosition = touch.position;
			}
		}
		if (Input.touchCount < 2)
		{
			return;
		}
		m_MoveSpeed = new Vector2(0f, 0f);
		Touch touch2 = Input.GetTouch(0);
		Touch touch3 = Input.GetTouch(1);
		if ((touch2.phase == TouchPhase.Began) | (touch3.phase == TouchPhase.Began))
		{
			m_oldTouchDist = Mathf.Sqrt((touch2.position.x - touch3.position.x) * (touch2.position.x - touch3.position.x) + (touch2.position.y - touch3.position.y) * (touch2.position.y - touch3.position.y));
		}
		if (touch2.phase == TouchPhase.Moved && touch3.phase == TouchPhase.Moved)
		{
			float num2 = Mathf.Sqrt((touch2.position.x - touch3.position.x) * (touch2.position.x - touch3.position.x) + (touch2.position.y - touch3.position.y) * (touch2.position.y - touch3.position.y));
			m_deltaTouchDist = num2 - m_oldTouchDist;
			m_touchCenterPosition = new Vector2((touch2.position.x + touch3.position.x) / 2f, (touch2.position.y + touch3.position.y) / 2f);
			float num3 = (0f - m_deltaTouchDist) * zoomValue;
			Camera component2 = base.gameObject.GetComponent<Camera>();
			float orthographicSize = component2.orthographicSize;
			bool flag = false;
			orthographicSize += num3;
			component2.orthographicSize += num3;
			if (orthographicSize < minOrthographicSize)
			{
				orthographicSize = minOrthographicSize;
				flag = true;
			}
			else if (orthographicSize > maxOrthographicSize)
			{
				orthographicSize = maxOrthographicSize;
				flag = true;
			}
			component2.orthographicSize = orthographicSize;
			if (!flag)
			{
				float x2 = (m_touchCenterPosition.x - (float)(Screen.width / 2)) / (float)(Screen.width / 2) * num3 * 1.9f;
				float y2 = (m_touchCenterPosition.y - (float)(Screen.height / 2)) / (float)(Screen.height / 2) * num3 * 1f;
				SubPosition(new Vector3(x2, y2, 0f));
			}
			m_oldTouchDist = num2;
			m_afterPinch = true;
		}
	}

	public void DoMove(Vector2 pos, Vector2 delta)
	{
		m_MoveSpeed = new Vector2(0f - delta.x, 0f - delta.y);
		UpdateMovePosition();
	}

	public void DoZoom(Vector2 pos, float delta)
	{
		float num = (0f - delta) * zoomValue;
		Camera component = base.gameObject.GetComponent<Camera>();
		float orthographicSize = component.orthographicSize;
		bool flag = false;
		orthographicSize += num;
		component.orthographicSize += num;
		if (orthographicSize < minOrthographicSize)
		{
			orthographicSize = minOrthographicSize;
			flag = true;
		}
		else if (orthographicSize > maxOrthographicSize)
		{
			orthographicSize = maxOrthographicSize;
			flag = true;
		}
		component.orthographicSize = orthographicSize;
		if (!flag)
		{
			float x = (pos.x - (float)(Screen.width / 2)) / (float)(Screen.width / 2) * num * 1.9f;
			float y = (pos.y - (float)(Screen.height / 2)) / (float)(Screen.height / 2) * num * 1f;
			SubPosition(new Vector3(x, y, 0f));
		}
	}

	private void AddPosition(Vector3 adder)
	{
		Vector3 vec = new Vector3(base.gameObject.transform.position.x, base.gameObject.transform.position.y, base.gameObject.transform.position.z);
		vec += adder;
		vec = checkLimitPosition(vec);
		base.gameObject.transform.position = vec;
	}

	private void SubPosition(Vector3 adder)
	{
		Vector3 vec = new Vector3(base.gameObject.transform.position.x, base.gameObject.transform.position.y, base.gameObject.transform.position.z);
		vec -= adder;
		vec = checkLimitPosition(vec);
		base.gameObject.transform.position = vec;
	}

	public Vector3 checkLimitPosition(Vector3 _vec)
	{
		if (limitflag)
		{
			Camera component = base.gameObject.GetComponent<Camera>();
			float orthographicSize = component.orthographicSize;
			float num = orthographicSize * component.aspect;
			if (_vec.x < minPosX + num)
			{
				_vec.x = minPosX + num;
			}
			else if (_vec.x > maxPosX - num)
			{
				_vec.x = maxPosX - num;
			}
			if (_vec.y < minPosY + orthographicSize)
			{
				_vec.y = minPosY + orthographicSize;
			}
			else if (_vec.y > maxPosY - orthographicSize)
			{
				_vec.y = maxPosY - orthographicSize;
			}
		}
		return _vec;
	}

	private void UpdateMovePosition()
	{
		Vector3 vec = new Vector3(base.gameObject.transform.position.x, base.gameObject.transform.position.y, base.gameObject.transform.position.z);
		vec += new Vector3(m_MoveSpeed.x, m_MoveSpeed.y, 0f);
		vec = checkLimitPosition(vec);
		Vector3 position = base.gameObject.transform.position;
		base.gameObject.transform.position = vec;
		m_clippingUpdate = true;
		m_MoveSpeed.x -= m_MoveSpeed.x / frictionValue;
		m_MoveSpeed.y -= m_MoveSpeed.y / frictionValue;
		frictionStopSpeed = 3f;
		if (Mathf.Abs(m_MoveSpeed.x) <= frictionStopSpeed)
		{
			m_MoveSpeed.x = 0f;
		}
		if (Mathf.Abs(m_MoveSpeed.y) <= frictionStopSpeed)
		{
			m_MoveSpeed.y = 0f;
		}
	}

	public void CallUpdateClippingRect()
	{
		if (BIJZoomCamera.UpdateClippingEvent != null)
		{
			BIJZoomCamera.UpdateClippingEvent(m_camera, m_clipping);
		}
	}

	private void OnPreCull()
	{
		try
		{
			if (m_clippingUpdate)
			{
				Vector3 vector = m_camera.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
				Vector3 vector2 = m_camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f));
				m_clipping.x = vector.x;
				m_clipping.y = vector.y;
				m_clipping.z = vector2.x;
				m_clipping.w = vector2.y;
				Shader.SetGlobalVector("_Clip", m_clipping);
				CallUpdateClippingRect();
				m_clippingUpdate = false;
			}
		}
		catch (Exception)
		{
		}
	}

	private void OnPreRender()
	{
	}

	private void OnPostRender()
	{
	}

	private void OnRenderObject()
	{
	}
}
