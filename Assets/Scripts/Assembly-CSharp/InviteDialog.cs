using System;
using System.Collections.Generic;
using UnityEngine;

public class InviteDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3,
		NETWORK_FRIENDS_RELOAD00 = 4,
		NETWORK_FRIENDS_RELOAD01 = 5,
		NETWORK_FRIENDS_RELOAD02 = 6,
		NETWORK_FRIENDS_RELOAD03 = 7
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private Dictionary<GameObject, SNS> mButtons = new Dictionary<GameObject, SNS>();

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mSNSCallCount;

	private SNSFriendManager mFriendManager;

	public new void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				if (mSNS.Kind == SNS.Facebook)
				{
					mState.Reset(STATE.NETWORK_FRIENDS_RELOAD00, true);
				}
				else
				{
					DoSNS(_sns);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_FRIENDS_RELOAD00:
			Server.ManualConnecting = true;
			mFriendManager = new SNSFriendManager(mGame, SNSFriendManager.MODE.LITE);
			if (!mFriendManager.Start())
			{
				mState.Change(STATE.NETWORK_FRIENDS_RELOAD02);
			}
			else
			{
				mState.Change(STATE.NETWORK_FRIENDS_RELOAD01);
			}
			break;
		case STATE.NETWORK_FRIENDS_RELOAD01:
			if (mFriendManager == null || mFriendManager.IsFinished())
			{
				mState.Change(STATE.NETWORK_FRIENDS_RELOAD02);
			}
			else
			{
				mFriendManager.Update();
			}
			break;
		case STATE.NETWORK_FRIENDS_RELOAD02:
			if (mFriendManager != null && mFriendManager.IsDataModified)
			{
				mGame.Save();
			}
			if (mFriendManager != null && mFriendManager.NewAddedGameFriendCount > 0)
			{
				int newAddedGameFriendCount = mFriendManager.NewAddedGameFriendCount;
			}
			Server.ManualConnecting = false;
			mState.Change(STATE.NETWORK_FRIENDS_RELOAD03);
			break;
		case STATE.NETWORK_FRIENDS_RELOAD03:
			DoSNS(mSNS);
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("Invite_Title");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		titleDescKey = string.Format(Localization.Get("Invite_Desc"));
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, -70f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		mButtons.Clear();
		int num = 0;
		Vector3 zero = Vector3.zero;
		SNS[] array = new SNS[7]
		{
			SNS.Facebook,
			SNS.GooglePlus,
			SNS.GameCenter,
			SNS.LINE,
			SNS.Twitter,
			SNS.Instagram,
			SNS.Mail
		};
		Dictionary<SNS, bool> dictionary = new Dictionary<SNS, bool>();
		int num2 = 0;
		SNS[] array2 = array;
		foreach (SNS sNS in array2)
		{
			bool flag = GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite, sNS) && SocialManager.Instance[sNS].IsEnabled() && SocialManager.Instance[sNS].IsOpEnabled(8);
			switch (sNS)
			{
			case SNS.Facebook:
				flag = true && flag;
				break;
			case SNS.GooglePlus:
				flag = true && flag;
				break;
			case SNS.Twitter:
				flag = true && flag;
				break;
			case SNS.Instagram:
				flag = true && flag;
				break;
			}
			dictionary[sNS] = flag;
			if (flag)
			{
				num2++;
			}
		}
		float num3 = 70f;
		float num4 = -130f;
		float num5 = -70f;
		int num6 = 3;
		float num7 = -166f;
		float num8 = 166f;
		float num9 = 0f;
		foreach (KeyValuePair<SNS, bool> item in dictionary)
		{
			SNS key = item.Key;
			if (item.Value)
			{
				string text;
				switch (key)
				{
				case SNS.Facebook:
					text = "icon_sns00";
					break;
				case SNS.GooglePlus:
					text = "icon_sns05";
					break;
				case SNS.LINE:
					text = "icon_sns02";
					break;
				case SNS.Twitter:
					text = "icon_sns03";
					break;
				case SNS.Instagram:
					text = "icon_sns01";
					break;
				case SNS.Mail:
					text = "icon_sns04";
					break;
				default:
					continue;
				}
				int num10 = num % num6;
				int num11 = num / num6;
				zero.x = num7 + (float)num10 * num8 + ((num11 != num2 / num6) ? 0f : num9);
				zero.y = num3 + (float)num11 * num4;
				num++;
				UIButton uIButton = Util.CreateJellyImageButton(string.Concat(string.Empty, key, "Button"), base.gameObject, image);
				Util.SetImageButtonInfo(uIButton, text, text, text, mBaseDepth + 3, zero, Vector3.one);
				Util.AddImageButtonMessage(uIButton, this, "OnSNSButtonPushed", UIButtonMessage.Trigger.OnClick);
				mButtons[uIButton.gameObject] = key;
				zero.y += num5;
				titleDescKey = string.Format(Localization.Get("Invite_By_" + key));
				uILabel = Util.CreateLabel(string.Concat(string.Empty, key, "Label"), uIButton.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, -62f), 20, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
				uILabel.MakePixelPerfect();
			}
		}
		CreateCancelButton("OnCancelPushed");
	}

	public void OnSNSButtonPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		SNS value = SNS.None;
		if (mButtons.TryGetValue(go, out value))
		{
			BIJSNS bIJSNS = SocialManager.Instance[value];
			if (bIJSNS != null)
			{
				mSNSCallCount = 0;
				DoSNS(bIJSNS);
			}
		}
	}

	private IBIJSNSPostData GetPostData(BIJSNS _sns)
	{
		BIJSNSPostData bIJSNSPostData = new BIJSNSPostData();
		string text = ((!string.IsNullOrEmpty(Constants.INVITE_URL)) ? ("\n" + Constants.INVITE_URL) : string.Empty);
		string iNVITE_CODE = Constants.INVITE_CODE;
		string empty = string.Empty;
		string fileName = "SnoopyDrops.png";
		List<string> inviteExcludeIds = GameMain.GetInviteExcludeIds(_sns);
		switch (_sns.Kind)
		{
		case SNS.Facebook:
		{
			string linkURL2 = string.Format(Constants.POST_URL, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = empty;
			bIJSNSPostData.Title = string.Format(Localization.Get("Invite_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("Invite_Contents_Desc"), string.Empty, string.Empty);
			bIJSNSPostData.LinkURL = linkURL2;
			bIJSNSPostData.FileName = fileName;
			bIJSNSPostData.ExcludeIds = inviteExcludeIds;
			break;
		}
		case SNS.GooglePlus:
		{
			string linkURL = "\n" + string.Format(Constants.POST_URL, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = empty;
			bIJSNSPostData.Title = string.Format(Localization.Get("Invite_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("Invite_Contents_Desc"), string.Empty, string.Empty);
			bIJSNSPostData.LinkURL = linkURL;
			bIJSNSPostData.FileName = fileName;
			bIJSNSPostData.ExcludeIds = inviteExcludeIds;
			break;
		}
		default:
		{
			string arg = "\n" + string.Format(Constants.STORE_URL_IOS, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = empty;
			bIJSNSPostData.Title = string.Format(Localization.Get("Invite_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("Invite_Contents_Desc"), arg, "\n" + Constants.STORE_URL_AD);
			bIJSNSPostData.LinkURL = string.Empty;
			bIJSNSPostData.FileName = fileName;
			bIJSNSPostData.ExcludeIds = inviteExcludeIds;
			break;
		}
		}
		return bIJSNSPostData;
	}

	private void DoSNS(BIJSNS _sns)
	{
		mSNSCallCount++;
		try
		{
			mState.Reset(STATE.WAIT, true);
			mSNS = _sns;
			if (GameMain.IsSNSNeedLogin(_sns))
			{
				if (mSNSCallCount > 1)
				{
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.NETWORK_LOGIN00);
				}
				return;
			}
			IBIJSNSPostData postData = GetPostData(_sns);
			_sns.Invite(postData, delegate(BIJSNS.Response _res, object _userdata)
			{
				if (_res != null && _res.result == 0)
				{
					if (GameMain.ProcessInviteResult(_sns, _res.detail as IBIJSNSPostData) > 0)
					{
						mGame.AchievementFriendInvite();
					}
					if (SNSSuccess(_sns))
					{
						return;
					}
				}
				else if (SNSError(_sns))
				{
					return;
				}
				mState.Change(STATE.MAIN);
			}, null);
		}
		catch (Exception)
		{
			mState.Change(STATE.MAIN);
		}
	}

	private bool SNSSuccess(BIJSNS _sns)
	{
		return false;
	}

	private void OnSNSSuccessDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		Close();
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Reset(STATE.WAIT, true);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("Invite_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("Invite_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		if (mCallback != null)
		{
			mCallback();
		}
	}

	public override void Close()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
