using System;

public class MiniJSONAliasAttribute : Attribute
{
	public string Alias { get; set; }

	public MiniJSONAliasAttribute(string alias)
	{
		Alias = alias;
	}
}
