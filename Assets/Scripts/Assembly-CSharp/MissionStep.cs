public class MissionStep
{
	public int mTarget { get; set; }

	public int mReward { get; set; }

	public MissionStep()
	{
		mTarget = 0;
		mReward = 0;
	}

	public MissionStep(int aTarget, int aReward)
	{
		mTarget = aTarget;
		mReward = aReward;
	}
}
