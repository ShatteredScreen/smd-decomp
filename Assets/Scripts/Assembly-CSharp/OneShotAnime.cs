using System.Collections.Generic;
using UnityEngine;

public class OneShotAnime : PartsChangedSprite
{
	public delegate void UserKeyHandler(string a_key);

	public delegate void AnimationCallback(OneShotAnime anime);

	private GameMain mGame;

	protected bool mAutoDestroy;

	protected float mDelayTime;

	protected CHANGE_PART_INFO[] mChangePartsArray;

	protected string mSoundTriggerPartName = string.Empty;

	protected Dictionary<string, string> mSoundTriggerDict;

	protected string mUserKeyTriggerPartName = string.Empty;

	protected Dictionary<string, UserKeyHandler> mUserKeyTriggerDict;

	public AnimationCallback AnimationFinished;

	public override void OnDestroy()
	{
		mChangePartsArray = null;
		base.OnDestroy();
	}

	protected new virtual void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		base.Awake();
	}

	protected new virtual void Update()
	{
		if (mDelayTime > 0f)
		{
			mDelayTime -= Time.deltaTime;
			if (mDelayTime <= 0f)
			{
				AnimeStart(mChangePartsArray);
			}
		}
	}

	public void Init(string fileName, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas, Vector3 pos, Vector3 scale, bool noPlay)
	{
		_animationName = fileName;
		mAutoDestroy = false;
		mDelayTime = 0f;
		mChangePartsArray = changePartsArray;
		Atlas = atlas;
		AnimationFinished = null;
		_sprite.AnimationFinished = SsSprite_OnAnimationFinished;
		_sprite.Position = pos;
		_sprite.Scale = scale;
		_sprite.Rotation = new Vector3(0f, 0f, 0f);
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_animationName);
		_sprite.Animation = resSsAnimation.SsAnime;
		_sprite.DestroyAtEnd = mAutoDestroy;
		_sprite.PlayCount = 1;
		if (!noPlay)
		{
			AnimeStart(changePartsArray);
			return;
		}
		_sprite.PlayAtStart = false;
		_sprite.Pause();
	}

	public void Init(string fileName, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, AnimationCallback onFinished)
	{
		Init(fileName, null, null, pos, scale, rotateDeg, delay, autoDestroy, onFinished);
	}

	public void Init(string fileName, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, AnimationCallback onFinished)
	{
		_animationName = fileName;
		mAutoDestroy = autoDestroy;
		mDelayTime = delay;
		mChangePartsArray = changePartsArray;
		Atlas = atlas;
		AnimationFinished = onFinished;
		_sprite.AnimationFinished = SsSprite_OnAnimationFinished;
		_sprite.Position = pos;
		_sprite.Scale = scale;
		_sprite.Rotation = new Vector3(0f, 0f, rotateDeg);
		if (delay == 0f)
		{
			AnimeStart(changePartsArray);
		}
	}

	public void Init(string fileName, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, SsSprite.AnimationCallback onFinished)
	{
		Init(fileName, null, null, pos, scale, rotateDeg, delay, autoDestroy, onFinished);
	}

	public void Init(string fileName, CHANGE_PART_INFO[] changePartsArray, BIJImage atlas, Vector3 pos, Vector3 scale, float rotateDeg, float delay, bool autoDestroy, SsSprite.AnimationCallback onFinished)
	{
		_animationName = fileName;
		mAutoDestroy = autoDestroy;
		mDelayTime = delay;
		mChangePartsArray = changePartsArray;
		Atlas = atlas;
		_sprite.AnimationFinished = onFinished;
		_sprite.Position = pos;
		_sprite.Scale = scale;
		_sprite.Rotation = new Vector3(0f, 0f, rotateDeg);
		if (delay == 0f)
		{
			AnimeStart(changePartsArray);
		}
	}

	protected virtual void AnimeStart(CHANGE_PART_INFO[] changePartsArray)
	{
		if (changePartsArray == null)
		{
			ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(_animationName);
			_sprite.Animation = resSsAnimation.SsAnime;
			_sprite.DestroyAtEnd = mAutoDestroy;
			_sprite.PlayCount = 1;
			_sprite.Play();
		}
		else
		{
			ChangeAnime(_animationName, changePartsArray, false, 1, _sprite.AnimationFinished);
			_sprite.DestroyAtEnd = mAutoDestroy;
		}
	}

	public bool IsAnimationFinished()
	{
		return _sprite.IsAnimationFinished();
	}

	public void RemoveSoundTrigger(string partName)
	{
		mSoundTriggerDict = null;
		SsPart part = _sprite.GetPart(partName);
		if (part != null)
		{
			part.OnUserDataKey -= OnSoundDataKey;
		}
	}

	public void AddSoundTrigger(string partName, Dictionary<string, string> dict)
	{
		mSoundTriggerPartName = partName;
		mSoundTriggerDict = dict;
		SsPart part = _sprite.GetPart(partName);
		if (part != null)
		{
			part.OnUserDataKey += OnSoundDataKey;
		}
	}

	public void OnSoundDataKey(SsPart part, SsAttrValueInterface val)
	{
		SsUserDataKeyValue ssUserDataKeyValue = val as SsUserDataKeyValue;
		string value;
		if (mSoundTriggerDict.TryGetValue(ssUserDataKeyValue.String, out value))
		{
			mGame.PlaySe(value, -1);
		}
	}

	public void RemoveUserKeyTrigger(string partName)
	{
		mUserKeyTriggerDict = null;
		SsPart part = _sprite.GetPart(partName);
		if (part != null)
		{
			part.OnUserDataKey -= OnUserDataKey;
		}
	}

	public void AddUserKeyTrigger(string partName, Dictionary<string, UserKeyHandler> dict)
	{
		if (mUserKeyTriggerPartName.CompareTo(partName) != 0)
		{
			mUserKeyTriggerDict = dict;
			SsPart part = _sprite.GetPart(partName);
			if (part != null)
			{
				part.OnUserDataKey += OnUserDataKey;
			}
		}
	}

	public void OnUserDataKey(SsPart part, SsAttrValueInterface val)
	{
		SsUserDataKeyValue ssUserDataKeyValue = val as SsUserDataKeyValue;
		UserKeyHandler value = null;
		if (mUserKeyTriggerDict.TryGetValue(ssUserDataKeyValue.String, out value) && value != null)
		{
			value(ssUserDataKeyValue.String);
		}
	}

	public void SsSprite_OnAnimationFinished(SsSprite a_sprite)
	{
		if (AnimationFinished != null)
		{
			AnimationFinished(this);
		}
	}

	public void SetTimeScale(float scale)
	{
		_sprite.Speed = scale;
	}

	public virtual void Play()
	{
		AnimeStart(mChangePartsArray);
	}

	public virtual void Pause()
	{
		_sprite.Pause();
	}

	public virtual void SkipToEnd()
	{
		_sprite.AnimFrame = _sprite.EndFrame - 1f;
	}
}
