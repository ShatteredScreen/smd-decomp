using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PlayerData : SMJsonData, ICloneable
{
	public bool SumError;

	private string mNickName;

	private int mOldEventOpenCountGem;

	private int mOldEventOpenCountKey;

	public short Version { get; set; }

	public string AppName { get; set; }

	public short AppVersion { get; set; }

	public short DataSetVersion { get; set; }

	public DateTime LastPlayTime { get; set; }

	public long StartupCount { get; set; }

	public long PlayCount { get; set; }

	public long BootCount { get; set; }

	public long IntervalCount { get; set; }

	public long ContinuousCount { get; set; }

	public DateTime LastBootTime { get; set; }

	public DateTime LastRatingDisplayTime { get; set; }

	public int LastRatingDisplayLevel { get; set; }

	public int RatingDisplayCount { get; set; }

	public int UUID { get; set; }

	public string NickName
	{
		get
		{
			return mNickName;
		}
		set
		{
			mNickName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public int HiveId { get; set; }

	public int PlayerIcon { get; set; }

	public string SearchID { get; set; }

	public int LastUsedIcon { get; set; }

	public bool LastUsedIconCancelFlg { get; set; }

	public int RestoreTransactionCount { get; set; }

	public DateTime LastRestoreTransactionTime { get; set; }

	public double TimeCheatPenalty { get; set; }

	public double OldTimeCheatPenalty { get; set; }

	public DateTime LastLocalTimeCheatCheckTime { get; set; }

	public DateTime LastGetAuthTime { get; set; }

	public DateTime LastGetGameInfoTime { get; set; }

	public DateTime LastGetSupportTime { get; set; }

	public DateTime LastGetSupportPurchaseTime { get; set; }

	public DateTime LastGetGiftTime { get; set; }

	public DateTime LastGetApplyTime { get; set; }

	public DateTime LastGetSocialTime { get; set; }

	public DateTime LastGetFriendTime { get; set; }

	public DateTime LastGetFriendIconTime { get; set; }

	public DateTime LastGetMyIconTime { get; set; }

	public DateTime LastRewardBonusTime { get; set; }

	public DateTime LastSentFriendHelpTime { get; set; }

	public DateTime LastGetSegmentInfoTime { get; set; }

	public DateTime LastGetMaintenanceInfoTime { get; set; }

	public DateTime LastLoginBonusTime { get; set; }

	public Def.SERIES CurrentSeries { get; set; }

	public Def.SERIES PreviousSeries { get; set; }

	public int CurrentEventID { get; set; }

	public List<short> LastEventIdList { get; set; }

	public List<EventEffectDispInfo> EventEffectDispList { get; set; }

	public PlayerMapData Player1stSeriesData { get; set; }

	public PlayerMapData PlayerRSeriesData { get; set; }

	public PlayerMapData PlayerSSeriesData { get; set; }

	public PlayerMapData PlayerSSSeriesData { get; set; }

	public PlayerMapData PlayerStarSSeriesData { get; set; }

	public Dictionary<Def.SERIES, PlayerMapData> PlayerGSeriesData { get; set; }

	[MiniJSONArray(typeof(PlayerEventData))]
	public List<PlayerEventData> PlayerEventDataList { get; set; }

	[MiniJSONDictionary(typeof(short), typeof(PlayerOldEventData))]
	public Dictionary<short, PlayerOldEventData> OldEventDataDict { get; set; }

	public int OldEventEnterCount { get; set; }

	public int OldEventOpenCountGem
	{
		get
		{
			return mOldEventOpenCountGem;
		}
		set
		{
			if (value > 999999)
			{
				mOldEventOpenCountGem = 999999;
			}
			else
			{
				mOldEventOpenCountGem = value;
			}
		}
	}

	public int OldEventOpenCountKey
	{
		get
		{
			return mOldEventOpenCountKey;
		}
		set
		{
			if (value > 999999)
			{
				mOldEventOpenCountKey = 999999;
			}
			else
			{
				mOldEventOpenCountKey = value;
			}
		}
	}

	public bool IsOldEventOpenGuide { get; set; }

	public int MaxNormalLifeCount { get; set; }

	public int MaxExtendLifeCount { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(int))]
	public Dictionary<int, int> ConsumeItems { get; set; }

	[MiniJSONDictionary(typeof(Def.BOOSTER_KIND), typeof(int))]
	public Dictionary<Def.BOOSTER_KIND, int> Boosters { get; set; }

	[MiniJSONDictionary(typeof(Def.BOOSTER_KIND), typeof(int))]
	public Dictionary<Def.BOOSTER_KIND, int> FreeBoosters { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> Accessories { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> Companions { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> EventItems { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> WallPapers { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(int))]
	public Dictionary<int, int> WallPaper_Pieces { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> CompleteStories { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> Skins { get; set; }

	[MiniJSONDictionary(typeof(string), typeof(byte))]
	public Dictionary<string, byte> CompleteTutorials { get; set; }

	public bool TutorialsSkipFlg { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(int))]
	public Dictionary<int, int> CompanionXPs { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> NotificationCollections { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(byte))]
	public Dictionary<int, byte> NotificationNewFriends { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(int))]
	public Dictionary<int, int> NotificationNewPartners { get; set; }

	public DateTime OldestLifeUseTime { get; set; }

	public short SelectedCompanion { get; set; }

	public short SelectedSkin { get; set; }

	public int UnlockAccessoriesNoticeCount { get; set; }

	public double TotalPlayTime { get; set; }

	[MiniJSONArray(typeof(MyFriendData))]
	public List<MyFriendData> FriendData { get; set; }

	public int GotFriends { get; set; }

	[MiniJSONArray(typeof(MyFriendData))]
	public List<MyFriendData> ApplyFriendData { get; set; }

	public DateTime LastGiftExpiredCheckTime { get; set; }

	public DateTime IconModified { get; set; }

	[MiniJSONArray(typeof(GiftItemData))]
	public List<GiftItemData> GiftData { get; set; }

	public long GotGifts { get; set; }

	public Def.SERIES LastPlaySeries { get; set; }

	public int LastPlayLevel { get; set; }

	public DateTime LastPlayLevelTime { get; set; }

	public int LastPlayLevelCount { get; set; }

	public DateTime LastPlayLevelStartTime { get; set; }

	public long TotalWin { get; set; }

	public long TotalLose { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(int))]
	public Dictionary<int, int> UseCompanionCount { get; set; }

	public long MaxCombo { get; set; }

	public long MaxScore { get; set; }

	[MiniJSONDictionary(typeof(string), typeof(DateTime))]
	public Dictionary<string, DateTime> LastInviteTime { get; set; }

	public DateTime LastColorModificationTime { get; set; }

	public DateTime LastApplyRandomUserTime { get; set; }

	public int ApplyRandomUserNum { get; set; }

	public List<int> AccessoryConnect { get; set; }

	[MiniJSONArray(typeof(PlayerShopData))]
	public List<PlayerShopData> ShopPurchaseNumData { get; set; }

	[MiniJSONArray(typeof(PlayerSendData))]
	public List<PlayerSendData> GameSendData { get; set; }

	[MiniJSONArray(typeof(TrophyExchangeInfo))]
	public List<TrophyExchangeInfo> TrophyExchangeInfoList { get; set; }

	public Dictionary<int, MissionProgress> MissionProgressDict { get; set; }

	public int ReflectMissionProgress { get; set; }

	[MiniJSONDictionary(typeof(int), typeof(PlayerSPDailyData))]
	public Dictionary<int, PlayerSPDailyData> SpecialDaily { get; set; }

	public DateTime MugenHeartUseTime { get; set; }

	public Def.MUGEN_HEART_STATUS mMugenHeart { get; set; }

	public int mUseMugenHeartSetTime { get; set; }

	public List<int> AdvAccessoryConnect { get; set; }

	public bool mCampaignDialog { get; set; }

	public Dictionary<int, PlayerBingoEventData> mBingoEventDataDict { get; set; }

	public Dictionary<int, List<DateTime>> AlreadyOpenBoosterSetDict { get; set; }

	public int LastEventInfoID { get; set; }

	public string LastEventInfoPageURL { get; set; }

	public DateTime LastEventInfoDisplayTime { get; set; }

	public int LastTermServiceID { get; set; }

	public string LastTermServiceURL { get; set; }

	public DateTime LastTermServiceDisplayTime { get; set; }

	public int LastPrivacyPolicyID { get; set; }

	public string LastPrivacyPolicyURL { get; set; }

	public DateTime LastPrivacyPolicyDisplayTime { get; set; }

	public Dictionary<int, GDPRAcceptInfo> GDPRAcceptDict { get; set; }

	public string UserBirthDay { get; set; }

	public Dictionary<SpecialChanceConditionSet.CHANCE_MODE, bool> ChanceFirstPurchaseFlag { get; set; }

	public Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> ChanceCount { get; set; }

	public Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short> ChanceDispCount { get; set; }

	public Dictionary<int, int> ChanceTotalOfferCount { get; set; }

	public Dictionary<int, int> ChanceTotalSaleCount { get; set; }

	public int UsePlusChanceBoosterCount { get; set; }

	public List<Def.BOOSTER_KIND> UseBeforeBooster { get; set; }

	public bool UseSpecialChance { get; set; }

	public bool UsePlusChance { get; set; }

	public bool UseScoreUpChance { get; set; }

	public bool UseContinueChance { get; set; }

	public bool UseStarGetChallenge { get; set; }

	public int DailyChallengeID { get; set; }

	public int DailyChallengeOpenSheetNum { get; set; }

	public Dictionary<int, int> DailyChallengeStayStage { get; set; }

	public int DailyChallengeLastSelectSheetNo { get; set; }

	public int TotalPlayableLevel
	{
		get
		{
			int num = 0;
			num += Player1stSeriesData.PlayableMaxLevel;
			num += PlayerRSeriesData.PlayableMaxLevel;
			num += PlayerSSeriesData.PlayableMaxLevel;
			num += PlayerSSSeriesData.PlayableMaxLevel;
			num += PlayerStarSSeriesData.PlayableMaxLevel;
			for (Def.SERIES sERIES = Def.SERIES.SM_GF00; sERIES <= Def.SeriesGaidenMax; sERIES++)
			{
				num += PlayerGSeriesData[sERIES].PlayableMaxLevel;
			}
			return num;
		}
	}

	public PlayerData()
	{
		ConsumeItems = new Dictionary<int, int>();
		Boosters = new Dictionary<Def.BOOSTER_KIND, int>();
		FreeBoosters = new Dictionary<Def.BOOSTER_KIND, int>();
		Accessories = new Dictionary<int, byte>();
		Companions = new Dictionary<int, byte>();
		EventItems = new Dictionary<int, byte>();
		WallPapers = new Dictionary<int, byte>();
		WallPaper_Pieces = new Dictionary<int, int>();
		CompleteStories = new Dictionary<int, byte>();
		Skins = new Dictionary<int, byte>();
		CompleteTutorials = new Dictionary<string, byte>();
		TutorialsSkipFlg = false;
		CompanionXPs = new Dictionary<int, int>();
		SpecialDaily = new Dictionary<int, PlayerSPDailyData>();
		NotificationCollections = new Dictionary<int, byte>();
		NotificationNewFriends = new Dictionary<int, byte>();
		NotificationNewPartners = new Dictionary<int, int>();
		UseCompanionCount = new Dictionary<int, int>();
		LastInviteTime = new Dictionary<string, DateTime>();
		FriendData = new List<MyFriendData>();
		GiftData = new List<GiftItemData>();
		ApplyFriendData = new List<MyFriendData>();
		PlayerEventDataList = new List<PlayerEventData>();
		OldEventDataDict = new Dictionary<short, PlayerOldEventData>();
		Player1stSeriesData = new PlayerMapData();
		PlayerRSeriesData = new PlayerMapData();
		PlayerSSeriesData = new PlayerMapData();
		PlayerSSSeriesData = new PlayerMapData();
		PlayerStarSSeriesData = new PlayerMapData();
		PlayerGSeriesData = new Dictionary<Def.SERIES, PlayerMapData>();
		for (Def.SERIES sERIES = Def.SERIES.SM_GF00; sERIES <= Def.SeriesGaidenMax; sERIES++)
		{
			PlayerGSeriesData.Add(sERIES, new PlayerMapData());
		}
		AccessoryConnect = new List<int>();
		ShopPurchaseNumData = new List<PlayerShopData>();
		GameSendData = new List<PlayerSendData>();
		TrophyExchangeInfoList = new List<TrophyExchangeInfo>();
		MissionProgressDict = new Dictionary<int, MissionProgress>();
		ReflectMissionProgress = 0;
		AdvAccessoryConnect = new List<int>();
		LastEventIdList = new List<short>();
		EventEffectDispList = new List<EventEffectDispInfo>();
		mBingoEventDataDict = new Dictionary<int, PlayerBingoEventData>();
		AlreadyOpenBoosterSetDict = new Dictionary<int, List<DateTime>>();
		ChanceFirstPurchaseFlag = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, bool>();
		ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
		ChanceDispCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
		ChanceTotalOfferCount = new Dictionary<int, int>();
		ChanceTotalSaleCount = new Dictionary<int, int>();
		UseBeforeBooster = new List<Def.BOOSTER_KIND>();
		DailyChallengeStayStage = new Dictionary<int, int>();
		GDPRAcceptDict = new Dictionary<int, GDPRAcceptInfo>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void CreateNewData()
	{
		Version = 1290;
		AppName = Player.GetDefaultAppName();
		AppVersion = 1290;
		DataSetVersion = 1000;
		LastPlayTime = DateTime.Now;
		UUID = 0;
		NickName = Localization.Get("DefaultName");
		HiveId = 0;
		PlayerIcon = 0;
		SearchID = string.Empty;
		LastUsedIcon = 0;
		LastUsedIconCancelFlg = false;
		RestoreTransactionCount = 0;
		LastRestoreTransactionTime = DateTimeUtil.Yesterday;
		TimeCheatPenalty = 0.0;
		OldTimeCheatPenalty = 0.0;
		LastLocalTimeCheatCheckTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetGameInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetGiftTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetFriendIconTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetMyIconTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastRewardBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastSentFriendHelpTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetSegmentInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
		CurrentSeries = Def.SERIES.SM_FIRST;
		LastEventIdList.Clear();
		EventEffectDispList.Clear();
		MaxNormalLifeCount = Constants.FIRST_MAX_LIFE;
		MaxExtendLifeCount = Constants.FIRST_MAX_EXTENDLIFE;
		OldestLifeUseTime = DateTimeUtil.UnitLocalTimeEpoch;
		ConsumeItems.Clear();
		ConsumeItems[1] = Constants.FIRST_LIFE;
		ConsumeItems[2] = Constants.FIRST_EXTENDLIFE;
		short num = 1060;
		num = 1071;
		if (num == 1290)
		{
			ConsumeItems[2] = Constants.FIRST_MAX_EXTENDLIFE;
		}
		ConsumeItems[3] = 0;
		ConsumeItems[4] = Constants.FIRST_SD;
		Boosters.Clear();
		FreeBoosters.Clear();
		Accessories.Clear();
		Companions.Clear();
		Companions[0] = 1;
		EventItems.Clear();
		WallPapers.Clear();
		WallPaper_Pieces.Clear();
		CompleteStories.Clear();
		Skins.Clear();
		Skins[0] = 1;
		SpecialDaily.Clear();
		SelectedSkin = 0;
		UnlockAccessoriesNoticeCount = 0;
		CompleteTutorials.Clear();
		TutorialsSkipFlg = false;
		CompanionXPs.Clear();
		NotificationCollections.Clear();
		NotificationNewFriends.Clear();
		NotificationNewPartners.Clear();
		SelectedCompanion = 0;
		TotalPlayTime = 0.0;
		FriendData.Clear();
		GotFriends = 0;
		ApplyFriendData.Clear();
		LastGiftExpiredCheckTime = DateTimeUtil.UnitLocalTimeEpoch;
		IconModified = DateTimeUtil.UnitLocalTimeEpoch;
		LastEventInfoID = 0;
		LastEventInfoPageURL = null;
		LastEventInfoDisplayTime = DateTimeUtil.UnitLocalTimeEpoch;
		GiftData.Clear();
		GotGifts = 0L;
		LastPlaySeries = Def.SERIES.SM_FIRST;
		LastPlayLevel = 0;
		LastPlayLevelTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastPlayLevelCount = 0;
		LastPlayLevelStartTime = DateTimeUtil.UnitLocalTimeEpoch;
		StartupCount = 0L;
		PlayCount = 0L;
		TotalWin = 0L;
		TotalLose = 0L;
		UseCompanionCount.Clear();
		MaxCombo = 0L;
		MaxScore = 0L;
		LastRatingDisplayTime = DateTimeUtil.UnitLocalTimeEpoch;
		LastRatingDisplayLevel = 0;
		RatingDisplayCount = 0;
		LastInviteTime.Clear();
		BootCount = 0L;
		IntervalCount = 0L;
		ContinuousCount = 0L;
		LastBootTime = DateTimeUtil.UnitLocalTimeEpoch;
		AccessoryConnect.Clear();
		ShopPurchaseNumData.Clear();
		GameSendData.Clear();
		TrophyExchangeInfoList.Clear();
		MissionManager.ClearMissionProgress(this);
		LastColorModificationTime = DateTimeUtil.UnitLocalTimeEpoch;
		MugenHeartUseTime = DateTimeUtil.UnitLocalTimeEpoch;
		mMugenHeart = Def.MUGEN_HEART_STATUS.NONE;
		mUseMugenHeartSetTime = 0;
		AdvAccessoryConnect.Clear();
		mCampaignDialog = false;
		mBingoEventDataDict.Clear();
		AlreadyOpenBoosterSetDict.Clear();
		ChanceFirstPurchaseFlag = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, bool>();
		ChanceCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
		ChanceDispCount = new Dictionary<SpecialChanceConditionSet.CHANCE_MODE, short>();
		UsePlusChanceBoosterCount = 0;
		UseBeforeBooster.Clear();
		ChanceTotalOfferCount.Clear();
		ChanceTotalSaleCount.Clear();
		IsOldEventOpenGuide = false;
		DailyChallengeID = 0;
		DailyChallengeOpenSheetNum = 0;
		DailyChallengeStayStage = new Dictionary<int, int>();
		DailyChallengeLastSelectSheetNo = 1;
		GDPRAcceptDict.Clear();
	}

	public void GetMapData(Def.SERIES a_series, out PlayerMapData data)
	{
		data = null;
		switch (a_series)
		{
		case Def.SERIES.SM_FIRST:
			data = Player1stSeriesData;
			break;
		case Def.SERIES.SM_R:
			data = PlayerRSeriesData;
			break;
		case Def.SERIES.SM_S:
			data = PlayerSSeriesData;
			break;
		case Def.SERIES.SM_SS:
			data = PlayerSSSeriesData;
			break;
		case Def.SERIES.SM_STARS:
			data = PlayerStarSSeriesData;
			break;
		case Def.SERIES.SM_GF00:
			data = PlayerGSeriesData[a_series];
			break;
		}
	}

	public void GetMapData(Def.SERIES a_series, int a_eventID, out PlayerEventData data)
	{
		data = null;
		for (int i = 0; i < PlayerEventDataList.Count; i++)
		{
			if (PlayerEventDataList[i].EventID == a_eventID)
			{
				PlayerEventData playerEventData = PlayerEventDataList[i];
				data = playerEventData;
				break;
			}
		}
	}

	public void SetMapData(Def.SERIES a_series, PlayerMapData a_data)
	{
		switch (a_series)
		{
		case Def.SERIES.SM_FIRST:
			Player1stSeriesData = a_data;
			break;
		case Def.SERIES.SM_R:
			PlayerRSeriesData = a_data;
			break;
		case Def.SERIES.SM_S:
			PlayerSSeriesData = a_data;
			break;
		case Def.SERIES.SM_SS:
			PlayerSSSeriesData = a_data;
			break;
		case Def.SERIES.SM_STARS:
			PlayerStarSSeriesData = a_data;
			break;
		case Def.SERIES.SM_GF00:
			PlayerGSeriesData[a_series] = a_data;
			break;
		}
	}

	public void SetMapData(int a_eventID, PlayerEventData a_data)
	{
		bool flag = false;
		for (int i = 0; i < PlayerEventDataList.Count; i++)
		{
			if (PlayerEventDataList[i].EventID == a_eventID)
			{
				PlayerEventDataList[i] = a_data;
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			PlayerEventDataList.Add(a_data);
		}
	}

	public bool RemoveMapData(int a_eventID, PlayerEventData a_data)
	{
		return PlayerEventDataList.Remove(a_data);
	}

	public void GetOldEventData(short a_eventID, out PlayerOldEventData data)
	{
		data = null;
		if (OldEventDataDict.ContainsKey(a_eventID))
		{
			data = OldEventDataDict[a_eventID];
		}
	}

	public void SetOldEventData(short a_eventID, PlayerOldEventData a_data)
	{
		if (OldEventDataDict.ContainsKey(a_eventID))
		{
			OldEventDataDict[a_eventID] = a_data;
		}
		else
		{
			OldEventDataDict.Add(a_eventID, a_data);
		}
	}

	public void OnSerialize(Player p)
	{
		Player1stSeriesData.StageClearData.Clear();
		foreach (KeyValuePair<int, PlayerClearData> item in p.StageClearData[Def.SERIES.SM_FIRST])
		{
			PlayerClearData value = item.Value;
			value.Version = 1290;
			Player1stSeriesData.StageClearData.Add(value);
		}
		PlayerRSeriesData.StageClearData.Clear();
		foreach (KeyValuePair<int, PlayerClearData> item2 in p.StageClearData[Def.SERIES.SM_R])
		{
			PlayerClearData value2 = item2.Value;
			value2.Version = 1290;
			PlayerRSeriesData.StageClearData.Add(value2);
		}
		PlayerSSeriesData.StageClearData.Clear();
		foreach (KeyValuePair<int, PlayerClearData> item3 in p.StageClearData[Def.SERIES.SM_S])
		{
			PlayerClearData value3 = item3.Value;
			value3.Version = 1290;
			PlayerSSeriesData.StageClearData.Add(value3);
		}
		PlayerSSSeriesData.StageClearData.Clear();
		foreach (KeyValuePair<int, PlayerClearData> item4 in p.StageClearData[Def.SERIES.SM_SS])
		{
			PlayerClearData value4 = item4.Value;
			value4.Version = 1290;
			PlayerSSSeriesData.StageClearData.Add(value4);
		}
		PlayerStarSSeriesData.StageClearData.Clear();
		foreach (KeyValuePair<int, PlayerClearData> item5 in p.StageClearData[Def.SERIES.SM_STARS])
		{
			PlayerClearData value5 = item5.Value;
			value5.Version = 1290;
			PlayerStarSSeriesData.StageClearData.Add(value5);
		}
		for (Def.SERIES sERIES = Def.SERIES.SM_GF00; sERIES <= Def.SeriesGaidenMax; sERIES++)
		{
			PlayerMapData playerMapData = PlayerGSeriesData[sERIES];
			playerMapData.StageClearData.Clear();
			foreach (KeyValuePair<int, PlayerClearData> item6 in p.StageClearData[sERIES])
			{
				PlayerClearData value6 = item6.Value;
				value6.Version = 1290;
				playerMapData.StageClearData.Add(value6);
			}
		}
		Player1stSeriesData.StageChallengeData.Clear();
		foreach (KeyValuePair<int, PlayerStageData> item7 in p.StageChallengeData[Def.SERIES.SM_FIRST])
		{
			PlayerStageData value7 = item7.Value;
			value7.Version = 1290;
			Player1stSeriesData.StageChallengeData.Add(value7);
		}
		PlayerRSeriesData.StageChallengeData.Clear();
		foreach (KeyValuePair<int, PlayerStageData> item8 in p.StageChallengeData[Def.SERIES.SM_R])
		{
			PlayerStageData value8 = item8.Value;
			value8.Version = 1290;
			PlayerRSeriesData.StageChallengeData.Add(value8);
		}
		PlayerSSeriesData.StageChallengeData.Clear();
		foreach (KeyValuePair<int, PlayerStageData> item9 in p.StageChallengeData[Def.SERIES.SM_S])
		{
			PlayerStageData value9 = item9.Value;
			value9.Version = 1290;
			PlayerSSeriesData.StageChallengeData.Add(value9);
		}
		PlayerSSSeriesData.StageChallengeData.Clear();
		foreach (KeyValuePair<int, PlayerStageData> item10 in p.StageChallengeData[Def.SERIES.SM_SS])
		{
			PlayerStageData value10 = item10.Value;
			value10.Version = 1290;
			PlayerSSSeriesData.StageChallengeData.Add(value10);
		}
		PlayerStarSSeriesData.StageChallengeData.Clear();
		foreach (KeyValuePair<int, PlayerStageData> item11 in p.StageChallengeData[Def.SERIES.SM_STARS])
		{
			PlayerStageData value11 = item11.Value;
			value11.Version = 1290;
			PlayerStarSSeriesData.StageChallengeData.Add(value11);
		}
		for (Def.SERIES sERIES2 = Def.SERIES.SM_GF00; sERIES2 <= Def.SeriesGaidenMax; sERIES2++)
		{
			PlayerMapData playerMapData2 = PlayerGSeriesData[sERIES2];
			playerMapData2.StageChallengeData.Clear();
			foreach (KeyValuePair<int, PlayerStageData> item12 in p.StageChallengeData[sERIES2])
			{
				PlayerStageData value12 = item12.Value;
				value12.Version = 1290;
				playerMapData2.StageChallengeData.Add(value12);
			}
		}
	}

	public void OnDeserialized(ref Player p)
	{
		StorePlayerClearData(ref p);
		StorePlayerChallengeData(ref p);
	}

	private void StorePlayerClearData(ref Player p)
	{
		if (Player1stSeriesData.StageClearData == null)
		{
			Player1stSeriesData.StageClearData = new List<PlayerClearData>();
		}
		p.StageClearData[Def.SERIES.SM_FIRST].Clear();
		foreach (PlayerClearData stageClearDatum in Player1stSeriesData.StageClearData)
		{
			PlayerClearData playerClearData = new PlayerClearData();
			playerClearData = stageClearDatum.Clone();
			p.StageClearData[Def.SERIES.SM_FIRST].Add(playerClearData.StageNo, playerClearData);
		}
		if (PlayerRSeriesData.StageClearData == null)
		{
			PlayerRSeriesData.StageClearData = new List<PlayerClearData>();
		}
		p.StageClearData[Def.SERIES.SM_R].Clear();
		foreach (PlayerClearData stageClearDatum2 in PlayerRSeriesData.StageClearData)
		{
			PlayerClearData playerClearData2 = new PlayerClearData();
			playerClearData2 = stageClearDatum2.Clone();
			p.StageClearData[Def.SERIES.SM_R].Add(playerClearData2.StageNo, playerClearData2);
		}
		if (PlayerSSeriesData.StageClearData == null)
		{
			PlayerSSeriesData.StageClearData = new List<PlayerClearData>();
		}
		p.StageClearData[Def.SERIES.SM_S].Clear();
		foreach (PlayerClearData stageClearDatum3 in PlayerSSeriesData.StageClearData)
		{
			PlayerClearData playerClearData3 = new PlayerClearData();
			playerClearData3 = stageClearDatum3.Clone();
			p.StageClearData[Def.SERIES.SM_S].Add(playerClearData3.StageNo, playerClearData3);
		}
		if (PlayerSSSeriesData.StageClearData == null)
		{
			PlayerSSSeriesData.StageClearData = new List<PlayerClearData>();
		}
		p.StageClearData[Def.SERIES.SM_SS].Clear();
		foreach (PlayerClearData stageClearDatum4 in PlayerSSSeriesData.StageClearData)
		{
			PlayerClearData playerClearData4 = new PlayerClearData();
			playerClearData4 = stageClearDatum4.Clone();
			p.StageClearData[Def.SERIES.SM_SS].Add(playerClearData4.StageNo, playerClearData4);
		}
		if (PlayerStarSSeriesData.StageClearData == null)
		{
			PlayerStarSSeriesData.StageClearData = new List<PlayerClearData>();
		}
		p.StageClearData[Def.SERIES.SM_STARS].Clear();
		foreach (PlayerClearData stageClearDatum5 in PlayerStarSSeriesData.StageClearData)
		{
			PlayerClearData playerClearData5 = new PlayerClearData();
			playerClearData5 = stageClearDatum5.Clone();
			p.StageClearData[Def.SERIES.SM_STARS].Add(playerClearData5.StageNo, playerClearData5);
		}
		for (Def.SERIES sERIES = Def.SERIES.SM_GF00; sERIES <= Def.SeriesGaidenMax; sERIES++)
		{
			PlayerMapData playerMapData = PlayerGSeriesData[sERIES];
			if (playerMapData.StageClearData == null)
			{
				playerMapData.StageClearData = new List<PlayerClearData>();
			}
			p.StageClearData[sERIES].Clear();
			foreach (PlayerClearData stageClearDatum6 in playerMapData.StageClearData)
			{
				PlayerClearData playerClearData6 = new PlayerClearData();
				playerClearData6 = stageClearDatum6.Clone();
				p.StageClearData[sERIES].Add(playerClearData6.StageNo, playerClearData6);
			}
		}
	}

	private void StorePlayerChallengeData(ref Player p)
	{
		if (Player1stSeriesData.StageChallengeData == null)
		{
			Player1stSeriesData.StageChallengeData = new List<PlayerStageData>();
		}
		p.StageChallengeData[Def.SERIES.SM_FIRST].Clear();
		foreach (PlayerStageData stageChallengeDatum in Player1stSeriesData.StageChallengeData)
		{
			PlayerStageData playerStageData = new PlayerStageData();
			playerStageData = stageChallengeDatum.Clone();
			p.StageChallengeData[Def.SERIES.SM_FIRST].Add(playerStageData.StageNo, playerStageData);
		}
		if (PlayerRSeriesData.StageChallengeData == null)
		{
			PlayerRSeriesData.StageChallengeData = new List<PlayerStageData>();
		}
		p.StageChallengeData[Def.SERIES.SM_R].Clear();
		foreach (PlayerStageData stageChallengeDatum2 in PlayerRSeriesData.StageChallengeData)
		{
			PlayerStageData playerStageData2 = new PlayerStageData();
			playerStageData2 = stageChallengeDatum2.Clone();
			p.StageChallengeData[Def.SERIES.SM_R].Add(playerStageData2.StageNo, playerStageData2);
		}
		if (PlayerSSeriesData.StageChallengeData == null)
		{
			PlayerSSeriesData.StageChallengeData = new List<PlayerStageData>();
		}
		p.StageChallengeData[Def.SERIES.SM_S].Clear();
		foreach (PlayerStageData stageChallengeDatum3 in PlayerSSeriesData.StageChallengeData)
		{
			PlayerStageData playerStageData3 = new PlayerStageData();
			playerStageData3 = stageChallengeDatum3.Clone();
			p.StageChallengeData[Def.SERIES.SM_S].Add(playerStageData3.StageNo, playerStageData3);
		}
		if (PlayerSSSeriesData.StageChallengeData == null)
		{
			PlayerSSSeriesData.StageChallengeData = new List<PlayerStageData>();
		}
		p.StageChallengeData[Def.SERIES.SM_SS].Clear();
		foreach (PlayerStageData stageChallengeDatum4 in PlayerSSSeriesData.StageChallengeData)
		{
			PlayerStageData playerStageData4 = new PlayerStageData();
			playerStageData4 = stageChallengeDatum4.Clone();
			p.StageChallengeData[Def.SERIES.SM_SS].Add(playerStageData4.StageNo, playerStageData4);
		}
		if (PlayerStarSSeriesData.StageChallengeData == null)
		{
			PlayerStarSSeriesData.StageChallengeData = new List<PlayerStageData>();
		}
		p.StageChallengeData[Def.SERIES.SM_STARS].Clear();
		foreach (PlayerStageData stageChallengeDatum5 in PlayerStarSSeriesData.StageChallengeData)
		{
			PlayerStageData playerStageData5 = new PlayerStageData();
			playerStageData5 = stageChallengeDatum5.Clone();
			p.StageChallengeData[Def.SERIES.SM_STARS].Add(playerStageData5.StageNo, playerStageData5);
		}
		for (Def.SERIES sERIES = Def.SERIES.SM_GF00; sERIES <= Def.SeriesGaidenMax; sERIES++)
		{
			PlayerMapData playerMapData = PlayerGSeriesData[sERIES];
			if (playerMapData.StageChallengeData == null)
			{
				playerMapData.StageChallengeData = new List<PlayerStageData>();
			}
			p.StageChallengeData[sERIES].Clear();
			foreach (PlayerStageData stageChallengeDatum6 in playerMapData.StageChallengeData)
			{
				PlayerStageData playerStageData6 = new PlayerStageData();
				playerStageData6 = stageChallengeDatum6.Clone();
				p.StageChallengeData[sERIES].Add(playerStageData6.StageNo, playerStageData6);
			}
		}
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		StringBuilder stringBuilder = new StringBuilder();
		byte[] array = null;
		data.WriteShort(1290);
		data.WriteUTF(Player.GetDefaultAppName());
		data.WriteShort(1290);
		data.WriteShort(1000);
		SerializeGeneralData(data, 1290);
		SerializeNickName(data, 1290);
		SerializePurchaseInfo(data, 1290);
		SerializeTimeCheat(data, 1290);
		SerializeConnection(data, 1290);
		SerializeGame(data, 1290);
		SerializeWebView(data, 1290);
		SerializeTermService(data, 1290);
		SerializeUserInfo(data, 1290);
		SerializeEnglishVersion(data, 1290);
		SerializeSpecialChance(data, 1290);
		SerializeOldEvent(data, 1290);
		SerializeDailyChallenge(data, 1290);
		int value;
		if (!ConsumeItems.TryGetValue(3, out value))
		{
			value = 0;
		}
		int value2;
		if (!ConsumeItems.TryGetValue(3, out value2))
		{
			value2 = 0;
		}
		stringBuilder.Remove(0, stringBuilder.Length);
		stringBuilder.Append(TotalPlayableLevel);
		stringBuilder.Append(value);
		stringBuilder.Append(value2);
		stringBuilder.Append((int)Math.Floor(TotalPlayTime));
		array = new BIJMD5(stringBuilder.ToString()).Hash;
		data.WriteByteArray(array);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		PlayerData p = this;
		StringBuilder stringBuilder = new StringBuilder();
		byte[] array = null;
		byte[] array2 = null;
		short num2 = (p.Version = data.ReadShort());
		p.AppName = data.ReadUTF();
		short appVersion = data.ReadShort();
		p.AppVersion = appVersion;
		short dataSetVersion = data.ReadShort();
		p.DataSetVersion = dataSetVersion;
		if (p.DataSetVersion != 1000)
		{
			SMStageData.ClearCache();
		}
		DeserializeGeneralData(data, ref p, num2);
		DeserializeNickName(data, ref p, num2);
		DeserializePurchaseInfo(data, ref p, num2);
		DeserializeTimeCheat(data, ref p, num2);
		DeserializeConnection(data, ref p, num2);
		DeserializeGame(data, ref p, num2);
		DeserializeWebView(data, ref p, num2);
		DeserializeTermService(data, ref p, num2);
		DeserializeUserInfo(data, ref p, num2);
		DeserializeEnglishVersion(data, ref p, num2);
		DeserializeSpecialChance(data, ref p, num2);
		if (num2 != 1230 || data.RemainSize > 0)
		{
			DeserializeOldEvent(data, ref p, num2);
			DeserializeDailyChallenge(data, ref p, num2);
			int value;
			if (!p.ConsumeItems.TryGetValue(3, out value))
			{
				value = 0;
			}
			int value2;
			if (!p.ConsumeItems.TryGetValue(3, out value2))
			{
				value2 = 0;
			}
			stringBuilder.Remove(0, stringBuilder.Length);
			stringBuilder.Append(p.TotalPlayableLevel);
			stringBuilder.Append(value);
			stringBuilder.Append(value2);
			stringBuilder.Append((int)Math.Floor(p.TotalPlayTime));
			array = new BIJMD5(stringBuilder.ToString()).Hash;
			array2 = data.ReadByteArray();
			if (array == null || array2 == null || !array2.SequenceEqual(array))
			{
				p.SumError = true;
			}
		}
	}

	private void SerializeGeneralData(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteDateTime(LastPlayTime);
		data.WriteLong(StartupCount);
		data.WriteLong(PlayCount);
		data.WriteLong(BootCount);
		data.WriteLong(IntervalCount);
		data.WriteLong(ContinuousCount);
		data.WriteDateTime(LastBootTime);
		data.WriteDateTime(LastRatingDisplayTime);
		data.WriteInt(LastRatingDisplayLevel);
		data.WriteInt(RatingDisplayCount);
	}

	private void DeserializeGeneralData(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.LastPlayTime = data.ReadDateTime();
		p.StartupCount = data.ReadLong();
		p.PlayCount = data.ReadLong();
		p.BootCount = data.ReadLong();
		p.IntervalCount = data.ReadLong();
		p.ContinuousCount = data.ReadLong();
		p.LastBootTime = data.ReadDateTime();
		p.LastRatingDisplayTime = data.ReadDateTime();
		p.LastRatingDisplayLevel = data.ReadInt();
		p.RatingDisplayCount = data.ReadInt();
	}

	private void SerializeNickName(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(UUID);
		data.WriteUTF(NickName);
		data.WriteInt(HiveId);
		data.WriteInt(PlayerIcon);
		data.WriteUTF(SearchID);
		data.WriteInt(LastUsedIcon);
		data.WriteBool(LastUsedIconCancelFlg);
	}

	private void DeserializeNickName(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.UUID = data.ReadInt();
		p.NickName = data.ReadUTF();
		p.HiveId = data.ReadInt();
		p.PlayerIcon = data.ReadInt();
		p.SearchID = data.ReadUTF();
		p.LastUsedIcon = data.ReadInt();
		p.LastUsedIconCancelFlg = data.ReadBool();
	}

	private void SerializePurchaseInfo(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(RestoreTransactionCount);
		data.WriteDateTime(LastRestoreTransactionTime);
	}

	private void DeserializePurchaseInfo(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.RestoreTransactionCount = data.ReadInt();
		p.LastRestoreTransactionTime = data.ReadDateTime();
	}

	private void SerializeTimeCheat(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteDouble(TimeCheatPenalty);
		data.WriteDouble(OldTimeCheatPenalty);
		data.WriteDateTime(LastLocalTimeCheatCheckTime);
	}

	private void DeserializeTimeCheat(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.TimeCheatPenalty = data.ReadDouble();
		p.OldTimeCheatPenalty = data.ReadDouble();
		p.LastLocalTimeCheatCheckTime = data.ReadDateTime();
	}

	private void SerializeConnection(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteDateTime(LastGetAuthTime);
		data.WriteDateTime(LastGetGameInfoTime);
		data.WriteDateTime(LastGetSupportTime);
		data.WriteDateTime(LastGetSupportPurchaseTime);
		data.WriteDateTime(LastGetGiftTime);
		data.WriteDateTime(LastGetApplyTime);
		data.WriteDateTime(LastGetFriendTime);
		data.WriteDateTime(LastGetSocialTime);
		data.WriteDateTime(LastGetFriendIconTime);
		data.WriteDateTime(LastGetMyIconTime);
		data.WriteDateTime(LastRewardBonusTime);
		data.WriteDateTime(LastSentFriendHelpTime);
		data.WriteDateTime(LastGetSegmentInfoTime);
		data.WriteDateTime(LastGetMaintenanceInfoTime);
	}

	private void DeserializeConnection(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.LastGetAuthTime = data.ReadDateTime();
		p.LastGetGameInfoTime = data.ReadDateTime();
		p.LastGetSupportTime = data.ReadDateTime();
		p.LastGetSupportPurchaseTime = data.ReadDateTime();
		p.LastGetGiftTime = data.ReadDateTime();
		p.LastGetApplyTime = data.ReadDateTime();
		p.LastGetFriendTime = data.ReadDateTime();
		p.LastGetSocialTime = data.ReadDateTime();
		p.LastGetFriendIconTime = data.ReadDateTime();
		p.LastGetMyIconTime = data.ReadDateTime();
		p.LastRewardBonusTime = data.ReadDateTime();
		p.LastSentFriendHelpTime = data.ReadDateTime();
		p.LastGetSegmentInfoTime = data.ReadDateTime();
		if (reqVersion >= 1060)
		{
			p.LastGetMaintenanceInfoTime = data.ReadDateTime();
		}
		p.LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetGameInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetGiftTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetApplyTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastRewardBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetSegmentInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		p.LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
	}

	private void SerializeGame(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt((int)CurrentSeries);
		data.WriteInt((int)PreviousSeries);
		data.WriteInt(LastEventIdList.Count);
		int i = 0;
		for (int count = LastEventIdList.Count; i < count; i++)
		{
			data.WriteShort(LastEventIdList[i]);
		}
		data.WriteInt(EventEffectDispList.Count);
		for (int j = 0; j < EventEffectDispList.Count; j++)
		{
			data.WriteShort(EventEffectDispList[j].mIsAdvEvent);
			data.WriteShort(EventEffectDispList[j].mEventId);
		}
		Player1stSeriesData.SerializeToBinary(data);
		PlayerRSeriesData.SerializeToBinary(data);
		PlayerSSeriesData.SerializeToBinary(data);
		PlayerSSSeriesData.SerializeToBinary(data);
		PlayerStarSSeriesData.SerializeToBinary(data);
		data.WriteInt(PlayerGSeriesData.Count);
		foreach (KeyValuePair<Def.SERIES, PlayerMapData> playerGSeriesDatum in PlayerGSeriesData)
		{
			data.WriteInt((int)playerGSeriesDatum.Key);
			playerGSeriesDatum.Value.SerializeToBinary(data);
		}
		data.WriteInt(MaxNormalLifeCount);
		data.WriteInt(MaxExtendLifeCount);
		data.WriteInt(ConsumeItems.Count);
		foreach (KeyValuePair<int, int> consumeItem in ConsumeItems)
		{
			data.WriteInt(consumeItem.Key);
			data.WriteInt(consumeItem.Value);
		}
		data.WriteShort((short)Boosters.Count);
		foreach (KeyValuePair<Def.BOOSTER_KIND, int> booster in Boosters)
		{
			data.WriteInt((int)booster.Key);
			data.WriteInt(booster.Value);
		}
		data.WriteShort((short)FreeBoosters.Count);
		foreach (KeyValuePair<Def.BOOSTER_KIND, int> freeBooster in FreeBoosters)
		{
			data.WriteInt((int)freeBooster.Key);
			data.WriteInt(freeBooster.Value);
		}
		data.WriteInt(Accessories.Count);
		foreach (KeyValuePair<int, byte> accessory in Accessories)
		{
			data.WriteInt(accessory.Key);
			data.WriteByte(accessory.Value);
		}
		data.WriteShort((short)Companions.Count);
		foreach (KeyValuePair<int, byte> companion in Companions)
		{
			data.WriteInt(companion.Key);
			data.WriteByte(companion.Value);
		}
		data.WriteInt(EventItems.Count);
		foreach (KeyValuePair<int, byte> eventItem in EventItems)
		{
			data.WriteInt(eventItem.Key);
			data.WriteByte(eventItem.Value);
		}
		data.WriteInt(WallPapers.Count);
		foreach (KeyValuePair<int, byte> wallPaper in WallPapers)
		{
			data.WriteInt(wallPaper.Key);
			data.WriteByte(wallPaper.Value);
		}
		data.WriteInt(WallPaper_Pieces.Count);
		foreach (KeyValuePair<int, int> wallPaper_Piece in WallPaper_Pieces)
		{
			data.WriteInt(wallPaper_Piece.Key);
			data.WriteInt(wallPaper_Piece.Value);
		}
		data.WriteInt(CompleteStories.Count);
		foreach (KeyValuePair<int, byte> completeStory in CompleteStories)
		{
			data.WriteInt(completeStory.Key);
			data.WriteByte(completeStory.Value);
		}
		data.WriteInt(Skins.Count);
		foreach (KeyValuePair<int, byte> skin in Skins)
		{
			data.WriteInt(skin.Key);
			data.WriteByte(skin.Value);
		}
		data.WriteInt(CompleteTutorials.Count);
		foreach (KeyValuePair<string, byte> completeTutorial in CompleteTutorials)
		{
			data.WriteUTF(completeTutorial.Key);
			data.WriteByte(completeTutorial.Value);
		}
		data.WriteBool(TutorialsSkipFlg);
		data.WriteShort((short)CompanionXPs.Count);
		foreach (KeyValuePair<int, int> companionXP in CompanionXPs)
		{
			data.WriteInt(companionXP.Key);
			data.WriteInt(companionXP.Value);
		}
		data.WriteInt(NotificationCollections.Count);
		foreach (KeyValuePair<int, byte> notificationCollection in NotificationCollections)
		{
			data.WriteInt(notificationCollection.Key);
			data.WriteByte(notificationCollection.Value);
		}
		data.WriteInt(NotificationNewFriends.Count);
		foreach (KeyValuePair<int, byte> notificationNewFriend in NotificationNewFriends)
		{
			data.WriteInt(notificationNewFriend.Key);
			data.WriteByte(notificationNewFriend.Value);
		}
		data.WriteInt(NotificationNewPartners.Count);
		foreach (KeyValuePair<int, int> notificationNewPartner in NotificationNewPartners)
		{
			data.WriteInt(notificationNewPartner.Key);
			data.WriteInt(notificationNewPartner.Value);
		}
		data.WriteDateTime(OldestLifeUseTime);
		data.WriteShort(SelectedCompanion);
		data.WriteShort(SelectedSkin);
		data.WriteInt(UnlockAccessoriesNoticeCount);
		data.WriteDouble(TotalPlayTime);
		data.WriteInt(FriendData.Count);
		foreach (MyFriendData friendDatum in FriendData)
		{
			friendDatum.SerializeToBinary(data);
		}
		data.WriteInt(GotFriends);
		data.WriteInt(ApplyFriendData.Count);
		foreach (MyFriendData applyFriendDatum in ApplyFriendData)
		{
			applyFriendDatum.SerializeToBinary(data);
		}
		data.WriteDateTime(LastGiftExpiredCheckTime);
		data.WriteDateTime(IconModified);
		data.WriteInt(GiftData.Count);
		foreach (GiftItemData giftDatum in GiftData)
		{
			giftDatum.SerializeToBinary(data);
		}
		data.WriteLong(GotGifts);
		data.WriteInt((int)LastPlaySeries);
		data.WriteInt(LastPlayLevel);
		data.WriteDateTime(LastPlayLevelTime);
		data.WriteInt(LastPlayLevelCount);
		data.WriteDateTime(LastPlayLevelStartTime);
		data.WriteLong(TotalWin);
		data.WriteLong(TotalLose);
		short v = (short)UseCompanionCount.Count;
		data.WriteShort(v);
		foreach (KeyValuePair<int, int> item in UseCompanionCount)
		{
			data.WriteInt(item.Key);
			data.WriteInt(item.Value);
		}
		data.WriteLong(MaxCombo);
		data.WriteLong(MaxScore);
		if (LastInviteTime == null || LastInviteTime.Count < 1)
		{
			data.WriteInt(0);
		}
		else
		{
			int count2 = LastInviteTime.Count;
			data.WriteInt(count2);
			foreach (KeyValuePair<string, DateTime> item2 in LastInviteTime)
			{
				data.WriteUTF(item2.Key);
				data.WriteDateTime(item2.Value);
			}
		}
		data.WriteDateTime(LastColorModificationTime);
		data.WriteDateTime(LastApplyRandomUserTime);
		data.WriteInt(ApplyRandomUserNum);
		data.WriteInt(PlayerEventDataList.Count);
		for (int k = 0; k < PlayerEventDataList.Count; k++)
		{
			PlayerEventData playerEventData = PlayerEventDataList[k];
			playerEventData.SerializeToBinary(data);
		}
		data.WriteInt(AccessoryConnect.Count);
		foreach (int item3 in AccessoryConnect)
		{
			data.WriteInt(item3);
		}
		data.WriteInt(ShopPurchaseNumData.Count);
		foreach (PlayerShopData shopPurchaseNumDatum in ShopPurchaseNumData)
		{
			shopPurchaseNumDatum.SerializeToBinary(data);
		}
		data.WriteInt(GameSendData.Count);
		foreach (PlayerSendData gameSendDatum in GameSendData)
		{
			gameSendDatum.SerializeToBinary(data);
		}
		data.WriteInt(TrophyExchangeInfoList.Count);
		foreach (TrophyExchangeInfo trophyExchangeInfo in TrophyExchangeInfoList)
		{
			trophyExchangeInfo.SerializeToBinary(data);
		}
		MissionManager.SaveMissionProgress(this, data);
		data.WriteInt(SpecialDaily.Count);
		foreach (KeyValuePair<int, PlayerSPDailyData> item4 in SpecialDaily)
		{
			data.WriteInt(item4.Key);
			item4.Value.SerializeToBinary(data);
		}
		data.WriteDateTime(MugenHeartUseTime);
		data.WriteInt((int)mMugenHeart);
		data.WriteInt(mUseMugenHeartSetTime);
		data.WriteInt(AdvAccessoryConnect.Count);
		foreach (int item5 in AdvAccessoryConnect)
		{
			data.WriteInt(item5);
		}
		data.WriteBool(mCampaignDialog);
		data.WriteInt(mBingoEventDataDict.Count);
		foreach (PlayerBingoEventData value in mBingoEventDataDict.Values)
		{
			value.SerializeToBinary(data);
		}
		DateTime dateTime = DateTimeUtil.Now();
		Dictionary<int, List<DateTime>> dictionary = new Dictionary<int, List<DateTime>>();
		foreach (KeyValuePair<int, List<DateTime>> item6 in AlreadyOpenBoosterSetDict)
		{
			List<DateTime> list = new List<DateTime>();
			foreach (DateTime item7 in item6.Value)
			{
				if (dateTime <= item7)
				{
					list.Add(item7);
				}
			}
			if (list.Count > 0)
			{
				dictionary.Add(item6.Key, list);
			}
		}
		data.WriteInt(dictionary.Count);
		foreach (KeyValuePair<int, List<DateTime>> item8 in dictionary)
		{
			data.WriteInt(item8.Key);
			data.WriteShort((short)item8.Value.Count);
			foreach (DateTime item9 in item8.Value)
			{
				data.WriteDateTime(item9);
			}
		}
	}

	private void DeserializeGame(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		int num = 0;
		p.CurrentSeries = (Def.SERIES)data.ReadInt();
		if (reqVersion >= 1050)
		{
			p.PreviousSeries = (Def.SERIES)data.ReadInt();
		}
		if (reqVersion >= 1020)
		{
			if (reqVersion >= 1130)
			{
				num = data.ReadInt();
				for (int i = 0; i < num; i++)
				{
					p.LastEventIdList.Add(data.ReadShort());
				}
			}
			else
			{
				int num2 = data.ReadInt();
				if (num2 != -1)
				{
					p.LastEventIdList.Add((short)num2);
				}
			}
		}
		int num3 = 1180;
		if (p.AppName.Contains("ww"))
		{
			num3 = 1160;
		}
		if (reqVersion >= num3)
		{
			num = data.ReadInt();
			for (int j = 0; j < num; j++)
			{
				EventEffectDispInfo eventEffectDispInfo = new EventEffectDispInfo();
				eventEffectDispInfo.mIsAdvEvent = data.ReadShort();
				eventEffectDispInfo.mEventId = data.ReadShort();
				p.EventEffectDispList.Add(eventEffectDispInfo);
			}
		}
		p.Player1stSeriesData.DeserializeFromBinary<PlayerClearData, PlayerStageData>(data, reqVersion);
		p.PlayerRSeriesData.DeserializeFromBinary<PlayerClearData, PlayerStageData>(data, reqVersion);
		p.PlayerSSeriesData.DeserializeFromBinary<PlayerClearData, PlayerStageData>(data, reqVersion);
		p.PlayerSSSeriesData.DeserializeFromBinary<PlayerClearData, PlayerStageData>(data, reqVersion);
		p.PlayerStarSSeriesData.DeserializeFromBinary<PlayerClearData, PlayerStageData>(data, reqVersion);
		if (reqVersion >= 1250)
		{
			num = data.ReadInt();
			for (int k = 0; k < num; k++)
			{
				Def.SERIES key = (Def.SERIES)data.ReadInt();
				if (p.PlayerGSeriesData.ContainsKey(key))
				{
					p.PlayerGSeriesData[key].DeserializeFromBinary<PlayerClearData, PlayerStageData>(data, reqVersion);
					continue;
				}
				PlayerMapData playerMapData = new PlayerMapData();
				playerMapData.DeserializeFromBinary<PlayerClearData, PlayerStageData>(data, reqVersion);
				p.PlayerGSeriesData.Add(key, playerMapData);
			}
		}
		p.MaxNormalLifeCount = data.ReadInt();
		p.MaxExtendLifeCount = data.ReadInt();
		p.ConsumeItems.Clear();
		num = data.ReadInt();
		for (int l = 0; l < num; l++)
		{
			int key2 = data.ReadInt();
			int value = data.ReadInt();
			p.ConsumeItems[key2] = value;
		}
		p.Boosters.Clear();
		num = data.ReadShort();
		for (int m = 0; m < num; m++)
		{
			int key3 = data.ReadInt();
			int value2 = data.ReadInt();
			p.Boosters[(Def.BOOSTER_KIND)key3] = value2;
		}
		p.FreeBoosters.Clear();
		num = data.ReadShort();
		for (int n = 0; n < num; n++)
		{
			int key4 = data.ReadInt();
			int value3 = data.ReadInt();
			p.FreeBoosters[(Def.BOOSTER_KIND)key4] = value3;
		}
		p.Accessories.Clear();
		num = data.ReadInt();
		for (int num4 = 0; num4 < num; num4++)
		{
			int key5 = data.ReadInt();
			byte value4 = data.ReadByte();
			p.Accessories[key5] = value4;
		}
		p.Companions.Clear();
		num = data.ReadShort();
		for (int num5 = 0; num5 < num; num5++)
		{
			int key6 = data.ReadInt();
			byte value5 = data.ReadByte();
			p.Companions[key6] = value5;
		}
		p.EventItems.Clear();
		num = data.ReadInt();
		for (int num6 = 0; num6 < num; num6++)
		{
			int key7 = data.ReadInt();
			byte value6 = data.ReadByte();
			p.EventItems[key7] = value6;
		}
		p.WallPapers.Clear();
		num = data.ReadInt();
		for (int num7 = 0; num7 < num; num7++)
		{
			int key8 = data.ReadInt();
			byte value7 = data.ReadByte();
			p.WallPapers[key8] = value7;
		}
		p.WallPaper_Pieces.Clear();
		num = data.ReadInt();
		for (int num8 = 0; num8 < num; num8++)
		{
			int key9 = data.ReadInt();
			int value8 = data.ReadInt();
			p.WallPaper_Pieces[key9] = value8;
		}
		p.CompleteStories.Clear();
		num = data.ReadInt();
		for (int num9 = 0; num9 < num; num9++)
		{
			int key10 = data.ReadInt();
			byte value9 = data.ReadByte();
			p.CompleteStories[key10] = value9;
		}
		p.Skins.Clear();
		num = data.ReadInt();
		for (int num10 = 0; num10 < num; num10++)
		{
			int key11 = data.ReadInt();
			byte value10 = data.ReadByte();
			p.Skins[key11] = value10;
		}
		p.CompleteTutorials.Clear();
		num = data.ReadInt();
		for (int num11 = 0; num11 < num; num11++)
		{
			string key12 = data.ReadUTF();
			byte value11 = data.ReadByte();
			p.CompleteTutorials[key12] = value11;
		}
		num3 = 1140;
		if (p.AppName.Contains("ww"))
		{
			num3 = 1120;
		}
		if (reqVersion >= num3)
		{
			p.TutorialsSkipFlg = data.ReadBool();
		}
		p.CompanionXPs.Clear();
		num = data.ReadShort();
		for (int num12 = 0; num12 < num; num12++)
		{
			int key13 = data.ReadInt();
			int value12 = data.ReadInt();
			p.CompanionXPs[key13] = value12;
		}
		p.NotificationCollections.Clear();
		num = data.ReadInt();
		for (int num13 = 0; num13 < num; num13++)
		{
			int key14 = data.ReadInt();
			byte value13 = data.ReadByte();
			p.NotificationCollections[key14] = value13;
		}
		p.NotificationNewFriends.Clear();
		num = data.ReadInt();
		for (int num14 = 0; num14 < num; num14++)
		{
			int key15 = data.ReadInt();
			byte value14 = data.ReadByte();
			p.NotificationNewFriends[key15] = value14;
		}
		p.NotificationNewPartners.Clear();
		num = data.ReadInt();
		for (int num15 = 0; num15 < num; num15++)
		{
			int key16 = data.ReadInt();
			int value15 = data.ReadInt();
			p.NotificationNewPartners[key16] = value15;
		}
		p.OldestLifeUseTime = data.ReadDateTime();
		p.SelectedCompanion = data.ReadShort();
		p.SelectedSkin = data.ReadShort();
		p.UnlockAccessoriesNoticeCount = data.ReadInt();
		p.TotalPlayTime = data.ReadDouble();
		p.FriendData.Clear();
		num = data.ReadInt();
		for (int num16 = 0; num16 < num; num16++)
		{
			MyFriendData myFriendData = new MyFriendData();
			myFriendData.DeserializeFromBinary(data, reqVersion);
			if (myFriendData.UUID != 0)
			{
				p.FriendData.Add(myFriendData);
			}
		}
		p.GotFriends = data.ReadInt();
		p.ApplyFriendData.Clear();
		num = data.ReadInt();
		for (int num17 = 0; num17 < num; num17++)
		{
			MyFriendData myFriendData2 = new MyFriendData();
			myFriendData2.DeserializeFromBinary(data, reqVersion);
			if (myFriendData2.UUID != 0)
			{
				p.ApplyFriendData.Add(myFriendData2);
			}
		}
		p.LastGiftExpiredCheckTime = data.ReadDateTime();
		p.IconModified = data.ReadDateTime();
		p.GiftData.Clear();
		num = data.ReadInt();
		for (int num18 = 0; num18 < num; num18++)
		{
			GiftItemData giftItemData = new GiftItemData();
			giftItemData.DeserializeFromBinary(data, reqVersion);
			if (string.IsNullOrEmpty(giftItemData.GiftID))
			{
				continue;
			}
			if (reqVersion < 1060 && giftItemData.ItemCategory == 20)
			{
				DateTime? serverTime = GameMain.ServerTime;
				if (serverTime.HasValue)
				{
					giftItemData.CreateTime = serverTime.Value;
				}
				else
				{
					giftItemData.CreateTime = DateTime.Now;
				}
			}
			p.GiftData.Add(giftItemData);
		}
		p.GotGifts = data.ReadLong();
		p.LastPlaySeries = (Def.SERIES)data.ReadInt();
		p.LastPlayLevel = data.ReadInt();
		p.LastPlayLevelTime = data.ReadDateTime();
		p.LastPlayLevelCount = data.ReadInt();
		p.LastPlayLevelStartTime = data.ReadDateTime();
		p.TotalWin = data.ReadLong();
		p.TotalLose = data.ReadLong();
		num = data.ReadShort();
		for (int num19 = 0; num19 < num; num19++)
		{
			int key17 = data.ReadInt();
			int value16 = data.ReadInt();
			p.UseCompanionCount[key17] = value16;
		}
		p.MaxCombo = data.ReadLong();
		p.MaxScore = data.ReadLong();
		p.LastInviteTime.Clear();
		int num20 = data.ReadInt();
		for (int num21 = 0; num21 < num20; num21++)
		{
			string text = data.ReadUTF();
			DateTime value17 = data.ReadDateTime();
			if (!string.IsNullOrEmpty(text))
			{
				p.LastInviteTime[text] = value17;
			}
		}
		p.LastColorModificationTime = data.ReadDateTime();
		p.LastApplyRandomUserTime = data.ReadDateTime();
		p.ApplyRandomUserNum = data.ReadInt();
		PlayerEventDataList = new List<PlayerEventData>();
		int num22 = data.ReadInt();
		for (int num23 = 0; num23 < num22; num23++)
		{
			PlayerEventData playerEventData = new PlayerEventData();
			playerEventData.DeserializeFromBinary(data, reqVersion);
			p.PlayerEventDataList.Add(playerEventData);
		}
		num = data.ReadInt();
		for (int num24 = 0; num24 < num; num24++)
		{
			p.AccessoryConnect.Add(data.ReadInt());
		}
		if (reqVersion >= 1010)
		{
			num = data.ReadInt();
			for (int num25 = 0; num25 < num; num25++)
			{
				PlayerShopData playerShopData = new PlayerShopData();
				playerShopData.DeserializeFromBinary(data, reqVersion);
				ShopPurchaseNumData.Add(playerShopData);
			}
		}
		if (reqVersion >= 1020)
		{
			num = data.ReadInt();
			for (int num26 = 0; num26 < num; num26++)
			{
				PlayerSendData playerSendData = new PlayerSendData();
				playerSendData.DeserializeFromBinary(data, reqVersion);
				p.GameSendData.Add(playerSendData);
			}
		}
		else
		{
			p.GameSendData.Clear();
			for (int num27 = 0; num27 < p.FriendData.Count; num27++)
			{
				MyFriendData f = p.FriendData[num27];
				PlayerSendData playerSendData2 = p.GameSendData.Find((PlayerSendData pp) => f.UUID == pp.UUID);
				if (playerSendData2 == null)
				{
					playerSendData2 = new PlayerSendData();
					playerSendData2.CopyFrom(f);
					p.GameSendData.Add(playerSendData2);
				}
			}
		}
		if (reqVersion >= 1050)
		{
			num = data.ReadInt();
			for (int num28 = 0; num28 < num; num28++)
			{
				TrophyExchangeInfo trophyExchangeInfo = new TrophyExchangeInfo();
				trophyExchangeInfo.DeserializeFromBinary(data, reqVersion);
				TrophyExchangeInfoList.Add(trophyExchangeInfo);
			}
		}
		MissionManager.LoadMissionProgress(this, data, reqVersion);
		if (reqVersion >= 1050)
		{
			p.SpecialDaily.Clear();
			num = data.ReadInt();
			for (int num29 = 0; num29 < num; num29++)
			{
				int key18 = data.ReadInt();
				PlayerSPDailyData playerSPDailyData = new PlayerSPDailyData();
				playerSPDailyData.DeserializeFromBinary(data, reqVersion);
				p.SpecialDaily.Add(key18, playerSPDailyData);
			}
		}
		if (reqVersion < 1060)
		{
			p.MaxExtendLifeCount = Constants.FIRST_MAX_EXTENDLIFE;
			short num30 = 1060;
			num30 = 1071;
			if (num30 == 1290)
			{
				p.ConsumeItems[1] = Constants.FIRST_MAX_LIFE;
				p.ConsumeItems[2] = Constants.FIRST_MAX_EXTENDLIFE;
			}
		}
		if (reqVersion >= 1060)
		{
			p.MugenHeartUseTime = data.ReadDateTime();
			p.mMugenHeart = (Def.MUGEN_HEART_STATUS)data.ReadInt();
			p.mUseMugenHeartSetTime = data.ReadInt();
		}
		if (reqVersion >= 1100)
		{
			num = data.ReadInt();
			for (int num31 = 0; num31 < num; num31++)
			{
				p.AdvAccessoryConnect.Add(data.ReadInt());
			}
		}
		if (reqVersion >= 1140)
		{
			mCampaignDialog = data.ReadBool();
		}
		if (reqVersion < 1150)
		{
			return;
		}
		num = data.ReadInt();
		for (int num32 = 0; num32 < num; num32++)
		{
			PlayerBingoEventData playerBingoEventData = new PlayerBingoEventData();
			playerBingoEventData.DeserializeFromBinary(data, reqVersion);
			mBingoEventDataDict[playerBingoEventData.mEventId] = playerBingoEventData;
		}
		int num33 = data.ReadInt();
		for (short num34 = 0; num34 < num33; num34++)
		{
			int key19 = data.ReadInt();
			short num35 = data.ReadShort();
			List<DateTime> list = new List<DateTime>();
			for (short num36 = 0; num36 < num35; num36++)
			{
				list.Add(data.ReadDateTime());
			}
			AlreadyOpenBoosterSetDict.Add(key19, list);
		}
	}

	private void SerializeWebView(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(LastEventInfoID);
		data.WriteUTF(LastEventInfoPageURL);
		data.WriteDateTime(LastEventInfoDisplayTime);
	}

	private static void DeserializeWebView(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.LastEventInfoID = data.ReadInt();
		p.LastEventInfoPageURL = data.ReadUTF();
		p.LastEventInfoDisplayTime = data.ReadDateTime();
	}

	private void SerializeTermService(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(LastTermServiceID);
		data.WriteUTF(LastTermServiceURL);
		data.WriteDateTime(LastTermServiceDisplayTime);
	}

	private static void DeserializeTermService(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.LastTermServiceID = data.ReadInt();
		p.LastTermServiceURL = data.ReadUTF();
		p.LastTermServiceDisplayTime = data.ReadDateTime();
	}

	private void SerializePrivacyPolicy(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(LastPrivacyPolicyID);
		data.WriteUTF(LastPrivacyPolicyURL);
		data.WriteDateTime(LastPrivacyPolicyDisplayTime);
	}

	private static void DeserializePrivacyPolicy(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.LastPrivacyPolicyID = data.ReadInt();
		p.LastPrivacyPolicyURL = data.ReadUTF();
		p.LastPrivacyPolicyDisplayTime = data.ReadDateTime();
	}

	private void SerializeUserInfo(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteUTF(UserBirthDay);
	}

	private static void DeserializeUserInfo(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		p.UserBirthDay = data.ReadUTF();
	}

	private void SerializeEnglishVersion(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteBool(true);
		SerializePrivacyPolicy(data, reqVersion);
		data.WriteInt(GDPRAcceptDict.Count);
		foreach (KeyValuePair<int, GDPRAcceptInfo> item in GDPRAcceptDict)
		{
			data.WriteInt(item.Value.mKind);
			data.WriteBool(item.Value.mAccept);
			data.WriteLong(item.Value.mDate);
		}
	}

	private static void DeserializeEnglishVersion(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		if (reqVersion < 1041 || !data.ReadBool())
		{
			return;
		}
		DeserializePrivacyPolicy(data, ref p, reqVersion);
		if (reqVersion < 1223)
		{
			return;
		}
		p.GDPRAcceptDict.Clear();
		int num = data.ReadInt();
		for (int i = 0; i < num; i++)
		{
			GDPRAcceptInfo gDPRAcceptInfo = new GDPRAcceptInfo();
			gDPRAcceptInfo.mKind = data.ReadInt();
			gDPRAcceptInfo.mAccept = data.ReadBool();
			gDPRAcceptInfo.mDate = data.ReadLong();
			if (p.GDPRAcceptDict.ContainsKey(gDPRAcceptInfo.mKind))
			{
				p.GDPRAcceptDict[gDPRAcceptInfo.mKind] = gDPRAcceptInfo;
			}
			else
			{
				p.GDPRAcceptDict.Add(gDPRAcceptInfo.mKind, gDPRAcceptInfo);
			}
		}
	}

	private void SerializeSpecialChance(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(ChanceFirstPurchaseFlag.Keys.Count);
		foreach (KeyValuePair<SpecialChanceConditionSet.CHANCE_MODE, bool> item in ChanceFirstPurchaseFlag)
		{
			data.WriteInt((int)item.Key);
			data.WriteBool(item.Value);
		}
		data.WriteInt(ChanceCount.Keys.Count);
		foreach (KeyValuePair<SpecialChanceConditionSet.CHANCE_MODE, short> item2 in ChanceCount)
		{
			data.WriteInt((int)item2.Key);
			data.WriteShort(item2.Value);
		}
		data.WriteInt(ChanceDispCount.Keys.Count);
		foreach (KeyValuePair<SpecialChanceConditionSet.CHANCE_MODE, short> item3 in ChanceDispCount)
		{
			data.WriteInt((int)item3.Key);
			data.WriteShort(item3.Value);
		}
		data.WriteInt(ChanceTotalOfferCount.Count);
		foreach (KeyValuePair<int, int> item4 in ChanceTotalOfferCount)
		{
			data.WriteInt(item4.Key);
			data.WriteInt(item4.Value);
		}
		data.WriteInt(ChanceTotalSaleCount.Count);
		foreach (KeyValuePair<int, int> item5 in ChanceTotalSaleCount)
		{
			data.WriteInt(item5.Key);
			data.WriteInt(item5.Value);
		}
	}

	private void DeserializeSpecialChance(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		if (reqVersion < 1150)
		{
			return;
		}
		if (reqVersion < 1230)
		{
			if (p.ChanceFirstPurchaseFlag.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL))
			{
				p.ChanceFirstPurchaseFlag[SpecialChanceConditionSet.CHANCE_MODE.SPECIAL] = data.ReadBool();
			}
			else
			{
				p.ChanceFirstPurchaseFlag.Add(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL, data.ReadBool());
			}
			if (p.ChanceFirstPurchaseFlag.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS))
			{
				p.ChanceFirstPurchaseFlag[SpecialChanceConditionSet.CHANCE_MODE.PLUS] = data.ReadBool();
			}
			else
			{
				p.ChanceFirstPurchaseFlag.Add(SpecialChanceConditionSet.CHANCE_MODE.PLUS, data.ReadBool());
			}
			if (p.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL))
			{
				p.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.SPECIAL] = data.ReadShort();
			}
			else
			{
				p.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL, data.ReadShort());
			}
			if (p.ChanceCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS))
			{
				p.ChanceCount[SpecialChanceConditionSet.CHANCE_MODE.PLUS] = data.ReadShort();
			}
			else
			{
				p.ChanceCount.Add(SpecialChanceConditionSet.CHANCE_MODE.PLUS, data.ReadShort());
			}
			int num = data.ReadInt();
			for (int i = 0; i < num; i++)
			{
				int key = data.ReadInt();
				int value = data.ReadInt();
				if (p.ChanceTotalOfferCount.ContainsKey(key))
				{
					p.ChanceTotalOfferCount[key] = value;
				}
				else
				{
					p.ChanceTotalOfferCount.Add(key, value);
				}
			}
			num = data.ReadInt();
			for (int j = 0; j < num; j++)
			{
				int key2 = data.ReadInt();
				int value2 = data.ReadInt();
				if (p.ChanceTotalSaleCount.ContainsKey(key2))
				{
					p.ChanceTotalSaleCount[key2] = value2;
				}
				else
				{
					p.ChanceTotalSaleCount.Add(key2, value2);
				}
			}
			if (p.ChanceDispCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL))
			{
				p.ChanceDispCount[SpecialChanceConditionSet.CHANCE_MODE.SPECIAL] = data.ReadShort();
			}
			else
			{
				p.ChanceDispCount.Add(SpecialChanceConditionSet.CHANCE_MODE.SPECIAL, data.ReadShort());
			}
			if (p.ChanceDispCount.ContainsKey(SpecialChanceConditionSet.CHANCE_MODE.PLUS))
			{
				p.ChanceDispCount[SpecialChanceConditionSet.CHANCE_MODE.PLUS] = data.ReadShort();
			}
			else
			{
				p.ChanceDispCount.Add(SpecialChanceConditionSet.CHANCE_MODE.PLUS, data.ReadShort());
			}
		}
		else
		{
			if (reqVersion == 1230 && data.RemainSize <= 0)
			{
				return;
			}
			int num2 = data.ReadInt();
			for (int k = 0; k < num2; k++)
			{
				int key3 = data.ReadInt();
				bool value3 = data.ReadBool();
				if (p.ChanceFirstPurchaseFlag.ContainsKey((SpecialChanceConditionSet.CHANCE_MODE)key3))
				{
					p.ChanceFirstPurchaseFlag[(SpecialChanceConditionSet.CHANCE_MODE)key3] = value3;
				}
				else
				{
					p.ChanceFirstPurchaseFlag.Add((SpecialChanceConditionSet.CHANCE_MODE)key3, value3);
				}
			}
			if (reqVersion == 1230 && data.RemainSize <= 0)
			{
				return;
			}
			num2 = data.ReadInt();
			for (int l = 0; l < num2; l++)
			{
				int key4 = data.ReadInt();
				short value4 = data.ReadShort();
				if (p.ChanceCount.ContainsKey((SpecialChanceConditionSet.CHANCE_MODE)key4))
				{
					p.ChanceCount[(SpecialChanceConditionSet.CHANCE_MODE)key4] = value4;
				}
				else
				{
					p.ChanceCount.Add((SpecialChanceConditionSet.CHANCE_MODE)key4, value4);
				}
			}
			if (reqVersion == 1230 && data.RemainSize <= 0)
			{
				return;
			}
			num2 = data.ReadInt();
			for (int m = 0; m < num2; m++)
			{
				int key5 = data.ReadInt();
				short value5 = data.ReadShort();
				if (p.ChanceDispCount.ContainsKey((SpecialChanceConditionSet.CHANCE_MODE)key5))
				{
					p.ChanceDispCount[(SpecialChanceConditionSet.CHANCE_MODE)key5] = value5;
				}
				else
				{
					p.ChanceDispCount.Add((SpecialChanceConditionSet.CHANCE_MODE)key5, value5);
				}
			}
			num2 = data.ReadInt();
			for (int n = 0; n < num2; n++)
			{
				int key6 = data.ReadInt();
				int value6 = data.ReadInt();
				if (p.ChanceTotalOfferCount.ContainsKey(key6))
				{
					p.ChanceTotalOfferCount[key6] = value6;
				}
				else
				{
					p.ChanceTotalOfferCount.Add(key6, value6);
				}
			}
			num2 = data.ReadInt();
			for (int num3 = 0; num3 < num2; num3++)
			{
				int key7 = data.ReadInt();
				int value7 = data.ReadInt();
				if (p.ChanceTotalSaleCount.ContainsKey(key7))
				{
					p.ChanceTotalSaleCount[key7] = value7;
				}
				else
				{
					p.ChanceTotalSaleCount.Add(key7, value7);
				}
			}
		}
	}

	private void SerializeOldEvent(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteShort((short)OldEventDataDict.Count);
		foreach (PlayerOldEventData value in OldEventDataDict.Values)
		{
			value.SerializeToBinary(data);
		}
		data.WriteInt(OldEventOpenCountGem);
		data.WriteInt(OldEventOpenCountKey);
		data.WriteBool(IsOldEventOpenGuide);
	}

	private void DeserializeOldEvent(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		if (reqVersion >= 1170)
		{
			OldEventDataDict.Clear();
			short num = data.ReadShort();
			for (int i = 0; i < num; i++)
			{
				PlayerOldEventData playerOldEventData = new PlayerOldEventData();
				playerOldEventData.DeserializeFromBinary(data, reqVersion);
				OldEventDataDict[playerOldEventData.EventID] = playerOldEventData;
			}
			OldEventOpenCountGem = data.ReadInt();
			OldEventOpenCountKey = data.ReadInt();
			if (reqVersion >= 1180)
			{
				IsOldEventOpenGuide = data.ReadBool();
			}
		}
	}

	private void SerializeDailyChallenge(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(DailyChallengeID);
		data.WriteInt(DailyChallengeOpenSheetNum);
		data.WriteShort((short)DailyChallengeStayStage.Keys.Count);
		foreach (KeyValuePair<int, int> item in DailyChallengeStayStage)
		{
			data.WriteInt(item.Key);
			data.WriteInt(item.Value);
		}
		data.WriteInt(DailyChallengeLastSelectSheetNo);
	}

	private void DeserializeDailyChallenge(BIJBinaryReader data, ref PlayerData p, int reqVersion)
	{
		if (reqVersion < 1180)
		{
			return;
		}
		p.DailyChallengeID = data.ReadInt();
		p.DailyChallengeOpenSheetNum = data.ReadInt();
		p.DailyChallengeStayStage.Clear();
		short num = data.ReadShort();
		for (short num2 = 0; num2 < num; num2++)
		{
			int key = data.ReadInt();
			int value = data.ReadInt();
			if (p.DailyChallengeStayStage.ContainsKey(key))
			{
				p.DailyChallengeStayStage[key] = value;
			}
			else
			{
				p.DailyChallengeStayStage.Add(key, value);
			}
		}
		p.DailyChallengeLastSelectSheetNo = data.ReadInt();
	}

	public override string SerializeToJson()
	{
		return string.Empty;
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
	}

	public void MakeBingoEventData(int a_eventID)
	{
		PlayerBingoEventData playerBingoEventData = new PlayerBingoEventData();
		playerBingoEventData.mEventId = a_eventID;
		for (int i = 0; i < 5; i++)
		{
			List<bool> list = new List<bool>();
			list.Clear();
			for (int j = 0; j < 8; j++)
			{
				list.Add(false);
			}
			playerBingoEventData.mReachEffectList.Add(list);
			List<bool> list2 = new List<bool>();
			list2.Clear();
			for (int k = 0; k < 8; k++)
			{
				list2.Add(false);
			}
			playerBingoEventData.mBingoEffectList.Add(list2);
			playerBingoEventData.mAllBingoEffectList.Add(false);
			playerBingoEventData.mAllStarCompEffectList.Add(false);
		}
		for (int l = 0; l < 5; l++)
		{
			playerBingoEventData.mGuideDialogList.Add(false);
		}
		mBingoEventDataDict[a_eventID] = playerBingoEventData;
	}

	public void MakeLabyrinthEventData(int a_eventID, SMEventPageData a_eventPageData)
	{
		PlayerEventData data;
		GetMapData(Def.SERIES.SM_EV, CurrentEventID, out data);
		if (data.LabyrinthData != null && data.LabyrinthData.CourseStageStatus.Count != 0)
		{
			return;
		}
		if (data.LabyrinthData == null)
		{
			data.LabyrinthData = new PlayerEventLabyrinthData();
		}
		List<int> list = new List<int>(a_eventPageData.EventCourseList.Keys);
		list.Sort();
		for (int i = 0; i < list.Count; i++)
		{
			int num = list[i];
			int stageNo = Def.GetStageNo(num + 1, 0);
			data.LabyrinthData.CourseStageStatus[(short)stageNo] = Player.STAGE_STATUS.NOOPEN;
			if (num != list.Count - 1)
			{
				data.LabyrinthData.CourseLineStatus[(short)stageNo] = Player.STAGE_STATUS.NOOPEN;
			}
		}
	}

	public PlayerData Clone()
	{
		return MemberwiseClone() as PlayerData;
	}
}
