public class AdvEventPoint
{
	[MiniJSONAlias("event_id")]
	public short event_id { get; set; }

	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }
}
