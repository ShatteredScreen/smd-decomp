using UnityEngine;

public class SPDailyDialog : DialogBase
{
	public enum STATE
	{
		OPEN = 0,
		CLOSE = 1,
		WAIT = 2
	}

	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum WEBVIEW_STATE
	{
		NONE = 0,
		PROGRESS = 1,
		ERROR = 2
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private const string CB_Webview_onError = "Webview_onError";

	private const string CB_Webview_onClose = "Webview_onClose";

	private const string CB_Webview_onPageStarted = "Webview_onPageStarted";

	private const string CB_Webview_onPageFinished = "Webview_onPageFinished";

	private const string CB_Webview_onProgress = "Webview_onProgress";

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.OPEN);

	private SELECT_ITEM mSelectItem;

	private WEBVIEW_STATE mWebViewState;

	private string mURL;

	private WebViewObject mWebViewObject;

	private bool mLoadURLCall;

	private string mLoadURL;

	private string mFallbackURL;

	private Vector2 mWebViewOrign;

	private Vector2 mWebViewSize;

	private OnDialogClosed mCallback;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.OPEN:
			if (!GetBusy())
			{
				LoadURL(mURL, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.WAIT:
			if (mGame.mBackKeyTrg)
			{
				OnBackKeyPress();
			}
			break;
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		string empty = string.Empty;
		empty = "SeasonEvent_HowTo";
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(Localization.Get(empty));
		mWebViewOrign = new Vector2(0f, -20f);
		mWebViewSize = new Vector2(530f, 500f);
		CreateCancelButton("OnNegativePushed");
		GameObject gameObject = Util.CreateGameObject("WebView", base.gameObject);
		mWebViewObject = gameObject.AddComponent<WebViewObject>();
		mWebViewObject.Init(OnWebViewObjectCallback);
		mWebViewObject.SetVisibility(false);
		UpdateWebViewMargin();
	}

	public void Init(string url)
	{
		mURL = url;
	}

	private void OnWebViewObjectCallback(string message)
	{
		if (string.Compare("Webview_onPageStarted", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.PROGRESS;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
			}
		}
		else if (string.Compare("Webview_onPageFinished", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS || mWebViewState == WEBVIEW_STATE.NONE)
			{
				mWebViewState = WEBVIEW_STATE.NONE;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(true);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onProgress", message) == 0)
		{
			if (mWebViewState != WEBVIEW_STATE.PROGRESS)
			{
			}
		}
		else if (string.Compare("Webview_onError", message) == 0)
		{
			if (mWebViewState == WEBVIEW_STATE.PROGRESS)
			{
				mWebViewState = WEBVIEW_STATE.ERROR;
				if (mWebViewObject != null)
				{
					mWebViewObject.SetVisibility(false);
				}
				mLoadURLCall = false;
			}
		}
		else if (string.Compare("Webview_onClose", message) == 0 && mWebViewState == WEBVIEW_STATE.NONE)
		{
			Close();
		}
	}

	private void UpdateWebViewMargin()
	{
		if (mWebViewObject != null)
		{
			Vector2 vector = Util.LogScreenSize();
			Vector2 vector2 = new Vector2(vector.x - mWebViewSize.x, vector.y - mWebViewSize.y);
			int left = (int)(Util.ToDevX(vector2.x / 2f) + Util.ToDevX(mWebViewOrign.x));
			int top = (int)(Util.ToDevY(vector2.y / 2f) - Util.ToDevY(mWebViewOrign.y));
			int right = (int)(Util.ToDevX(vector2.x / 2f) - Util.ToDevX(mWebViewOrign.x));
			int bottom = (int)(Util.ToDevY(vector2.y / 2f) + Util.ToDevY(mWebViewOrign.y));
			mWebViewObject.SetMargins(left, top, right, bottom);
		}
	}

	private void LoadURL(string url, string fallback)
	{
		if (mWebViewObject != null)
		{
			mLoadURLCall = true;
			mWebViewState = WEBVIEW_STATE.NONE;
			mLoadURL = url;
			mFallbackURL = fallback;
			mWebViewObject.LoadURL(url);
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			if (mWebViewObject != null)
			{
				mWebViewObject.SetVisibility(false);
			}
			Close();
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		UpdateWebViewMargin();
	}

	public void SetCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
