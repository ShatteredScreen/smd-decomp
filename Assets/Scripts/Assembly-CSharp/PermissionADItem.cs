using System;

public class PermissionADItem : SMVerticalListItem, IComparable
{
	public int Index;

	public string Text = string.Empty;

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as PermissionADItem);
	}

	public int CompareTo(PermissionADItem _obj)
	{
		try
		{
			if (_obj == null)
			{
				return int.MinValue;
			}
			return Index - _obj.Index;
		}
		catch (Exception)
		{
		}
		return int.MinValue;
	}

	public static PermissionADItem Make(int a_index, string a_text)
	{
		PermissionADItem permissionADItem = new PermissionADItem();
		permissionADItem.Index = a_index;
		permissionADItem.Text = a_text;
		return permissionADItem;
	}
}
