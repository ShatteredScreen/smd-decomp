using System;
using System.Collections.Generic;

public class AdvGetMailProfile_Response : ICloneable
{
	[MiniJSONAlias("results")]
	public List<AdvMailResult> MailResults { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGetMailProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGetMailProfile_Response;
	}
}
