using UnityEngine;

public class BezierCurve
{
	private Vector2[] points = new Vector2[4];

	public BezierCurve(Vector2 beginPoint, Vector2 endPoint, Vector2 controlPoint1, Vector2 controlPoint2)
	{
		points[0] = beginPoint;
		points[1] = controlPoint1;
		points[2] = controlPoint2;
		points[3] = endPoint;
	}

	public Vector2 GetPosition(float ratio)
	{
		Vector2[] array = new Vector2[3];
		for (int i = 0; i < 3; i++)
		{
			array[i] = Vector2.Lerp(points[i], points[i + 1], ratio);
		}
		Vector2[] array2 = new Vector2[2];
		for (int j = 0; j < 2; j++)
		{
			array2[j] = Vector2.Lerp(array[j], array[j + 1], ratio);
		}
		return Vector2.Lerp(array2[0], array2[1], ratio);
	}
}
