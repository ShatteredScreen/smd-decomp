using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateSMMapBase : GameState
{
	protected class StoryDemoTemp
	{
		public int DemoIndex;

		public int SelectStage;

		public int ClearStage;

		public bool IsStageDemo(out int stageno)
		{
			stageno = -1;
			if (SelectStage != -1)
			{
				stageno = SelectStage;
				return true;
			}
			if (ClearStage != -1)
			{
				stageno = ClearStage;
				return true;
			}
			return false;
		}
	}

	public class _GetRankingData
	{
		public int series { get; set; }

		public int stage { get; set; }

		public List<StageRankingData> results { get; set; }
	}

	public enum DLFILE_CHECK_RESULT
	{
		OK = 0,
		HASH_NG = 1,
		FILE_DESTROY = 2
	}

	protected const float SCROLL_G = 9.80665f;

	protected const float SCROLL_SPEED = 12.258312f;

	protected const float SCROLL_THRESHOLD_Y = 0f;

	protected const float DECAY_SCROLL_Y = 2f;

	protected const float MAX_SCROLL_POWER = 75f;

	public const float DEFAULT_MAP_BOUNDS_RANGE = 67f;

	public const float MAP_DISTANSE = 1136f;

	protected GameObject mAnchorTop;

	protected GameObject mAnchorTopLeft;

	protected GameObject mAnchorBottom;

	protected GameObject mAnchorBottomRight;

	protected GameObject mAnchorBottomLeft;

	protected GameObject mAnchorRight;

	protected GameObject mAnchorCenter;

	protected GameObject mMapPageRoot;

	protected bool mTipsScreenDisplayed;

	protected SMMapPage mCurrentPage;

	protected SMMapPageData mMapPageData;

	protected GameStateManager mGameState;

	protected Camera mCamera;

	protected int mMaxPage;

	protected int mMaxAvailablePage;

	protected int mMaxModulePage;

	protected bool mIsOpenNewChapter;

	protected List<StoryDemoTemp> mShowStoryDemoList = new List<StoryDemoTemp>();

	protected StoryDemoTemp mCurrentStoryDemo;

	protected int mCurrentDemoIndex = -1;

	protected static object mRankingDataLock = new object();

	protected _GetRankingData mRankingData;

	protected bool mIsPlayingBGM;

	protected Cockpit mCockpit;

	protected bool mCockpitOperation;

	protected Coroutine mActiveObjectInCameraFunc;

	protected Def.PRESS mPageMoveLock;

	protected bool mPageMoved;

	protected bool mPageMoveBacking;

	protected Vector2 mDownPos = Vector2.zero;

	protected float mAutoScrollSpeed;

	public static float s_ScrollThresholdY = 0f;

	public static float s_DecayScrollY = 2f;

	public float mScrollPowerY;

	public float mScrollPowerYTemp;

	public Vector2 MoveVector = Vector2.zero;

	public Vector2 MoveVectorOld = Vector2.zero;

	public Vector2 DragPower = Vector2.zero;

	protected float mBoundSpan;

	protected bool mIsFirstBound;

	public float MAP_BOUNDS_RANGE = 67f;

	public float Map_DeviceOffset;

	public float Map_DeviceScreenSizeY;

	protected bool mIsLimitMove;

	protected float Map_LimitOffset;

	protected ConfirmDialog mConfirmDialog;

	protected ConfirmImageDialog mConfirmImageDialog;

	public StageInfoDialog mStageInfo;

	protected StageCharaDialog mStageChara;

	protected RankUpDialog mRankUpDialog;

	protected GetPartnerDialog mGetPartnerDialog;

	protected GrowupPartnerSelectDialog mGrowupDialog;

	protected GrowupPartnerConfirmDialog mGrowupPartnerConfirmtDialog;

	protected GrowupResultDialog mGrowupResultDialog;

	protected bool mGrowupFromMenu;

	protected GetAccessoryDialog mGetAccessoryDialog;

	protected GetStampDialog mGetStampDialog;

	protected BuyHeartDialog mHeartDialog;

	protected GiftFullDialog mGiftFullDialog;

	protected SocialRankingWindow mSocialRanking;

	protected int mSocialRankingDepth;

	protected WebViewDialog mHelpDialog;

	protected AlternateRewardDialog mAlternateRewardDialog;

	protected EventIntroCharacterDialog mEventIntroCharacterDialog;

	protected AdvNotEnterDialog mAdvNotEnterDialog;

	protected EventAdvConfirmDialog mEventAdvConfirmDialog;

	protected int mDemoNoAfterRBUnlock = -1;

	protected MapAccessory mPushedAccessory;

	protected AccessoryData mAutoUnlockPartner;

	protected AccessoryData mAutoUnlockAccessory;

	protected AccessoryData mAdvAutoUnlockAccessory;

	protected List<AccessoryData> mAlterUnlockAccessory = new List<AccessoryData>();

	protected int AccessoryConnectIndex;

	protected int AdvAccessoryConnectIndex;

	public static bool mIsShowErrorDialog = false;

	public static bool mAdvIsShowErrorDialog = false;

	protected List<AccessoryData> mAutoUnlockAccessoryList = new List<AccessoryData>();

	protected List<List<AccessoryData>> mAlterUnlockAccessoryList = new List<List<AccessoryData>>();

	protected ShopFullDialog mShopFullDialog;

	protected LoginBonusScreen mLoginBonusScreen;

	public int CurrentEventID;

	protected bool mIsNextPartnerIntro;

	protected bool mHasBootConnectingError;

	protected bool mIsConnecting = true;

	protected float mConnectionTime;

	private int mWebSaleIndex = -1;

	private bool mLinkBannerOnStore;

	protected bool mFirstCheckReward = true;

	public GameObject MapPageRoot
	{
		get
		{
			return mMapPageRoot;
		}
	}

	public SMMapPage MapPage
	{
		get
		{
			return mCurrentPage;
		}
	}

	public SMMapPageData MapPageData
	{
		get
		{
			return mMapPageData;
		}
	}

	protected _GetRankingData RankingData
	{
		get
		{
			lock (mRankingDataLock)
			{
				return mRankingData;
			}
		}
		set
		{
			lock (mRankingDataLock)
			{
				mRankingData = value;
			}
		}
	}

	protected bool IsShowingMarketingScreen
	{
		get
		{
			return false;
		}
	}

	public override void Start()
	{
		base.Start();
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
		mCamera = GameObject.Find("Camera").GetComponent<Camera>();
	}

	protected virtual void Update_INIT()
	{
	}

	protected virtual void Update_WAIT_SOUND()
	{
	}

	protected virtual void Update_UNLOCK_ACTING()
	{
	}

	protected virtual void Update_MAIN()
	{
	}

	protected virtual void CheckStageClear(ref PlayerMapData a_data)
	{
		int lastClearedLevel = mGame.mPlayer.LastClearedLevel;
		if (lastClearedLevel <= 0)
		{
			return;
		}
		int a_main;
		int a_sub;
		Def.SplitStageNo(lastClearedLevel, out a_main, out a_sub);
		SMMapStageSetting mapStage = mMapPageData.GetMapStage(a_main, a_sub);
		int num = -1;
		if (mapStage != null)
		{
			int storyDemoClear = mapStage.GetStoryDemoClear();
			if (storyDemoClear != -1 && !mGame.mPlayer.IsStoryCompleted(storyDemoClear))
			{
				num = storyDemoClear;
			}
			if (mapStage.StageEnterCondition == "PARTNER_F" && mapStage.EnterPartners.Count == 1)
			{
				int num2 = mapStage.EnterPartners[0];
				if (!mGame.mPlayer.IsCompanionUnlock(num2))
				{
					AccessoryData accessoryData = null;
					List<AccessoryData> accessoryByType = mGame.GetAccessoryByType(AccessoryData.ACCESSORY_TYPE.PARTNER);
					for (int i = 0; i < accessoryByType.Count; i++)
					{
						AccessoryData accessoryData2 = accessoryByType[i];
						if (accessoryData2.GetCompanionID() == num2)
						{
							accessoryData = accessoryData2;
							break;
						}
					}
					if (accessoryData != null)
					{
						mAutoUnlockPartner = accessoryData;
						mGame.mPlayer.AddAccessory(accessoryData);
						if (mGame.SetPartnerCollectionAttention(accessoryData))
						{
							mCockpit.SetEnableMenuButtonAttention(true);
						}
					}
				}
			}
		}
		if (num != -1 && !mGame.mPlayer.IsStoryCompleted(num))
		{
			StoryDemoTemp storyDemoTemp = new StoryDemoTemp();
			storyDemoTemp.DemoIndex = num;
			storyDemoTemp.SelectStage = -1;
			storyDemoTemp.ClearStage = lastClearedLevel;
			mShowStoryDemoList.Add(storyDemoTemp);
		}
		a_data.LastClearedLevel = -1;
	}

	public virtual float ClearedStagePosition(bool a_newcheck = true)
	{
		return 0f;
	}

	protected void UpdateCurrentMapPos(bool islandscape)
	{
		float num = ClearedStagePosition(false);
		float y = mCurrentPage.transform.localPosition.y;
		if (islandscape && num > y)
		{
			num += 80f;
		}
		StartCoroutine(DirectMapMove(0f - num));
		UpdateMapPages();
	}

	protected float GetLimitMapPageY()
	{
		float map_DeviceOffset = Map_DeviceOffset;
		return 1136f * (float)(mMaxModulePage - 1) + map_DeviceOffset - Map_LimitOffset;
	}

	public virtual void UpdateMapPages()
	{
		if (mCurrentPage == null)
		{
			return;
		}
		bool flag = false;
		float x = mCurrentPage.gameObject.transform.localPosition.x;
		float num = mCurrentPage.gameObject.transform.localPosition.y + mScrollPowerY;
		float num2 = Mathf.Abs(mScrollPowerY) / mGame.mDeviceRatioScale;
		if (num2 < 2.5f)
		{
			SetScrollPower(0f);
			num2 = 0f;
		}
		float map_DeviceOffset = Map_DeviceOffset;
		float num3 = 0f - GetLimitMapPageY();
		float mAP_BOUNDS_RANGE = MAP_BOUNDS_RANGE;
		if (!mGame.mTouchWasDrag && num2 < 0.0001f)
		{
			if (!mIsFirstBound)
			{
				mIsFirstBound = true;
				mBoundSpan = 0f;
			}
			else
			{
				mBoundSpan += Time.deltaTime;
			}
			if (mBoundSpan > 0.2f)
			{
				if (num > map_DeviceOffset)
				{
					num -= mAP_BOUNDS_RANGE * 0.1f;
					flag = true;
				}
				else if (num < num3)
				{
					num += mAP_BOUNDS_RANGE * 0.1f;
					flag = true;
				}
				else
				{
					mIsFirstBound = false;
				}
			}
		}
		mIsLimitMove = false;
		if (num >= map_DeviceOffset + mAP_BOUNDS_RANGE)
		{
			num = map_DeviceOffset + mAP_BOUNDS_RANGE;
			mIsLimitMove = true;
		}
		else if (num <= num3 - mAP_BOUNDS_RANGE)
		{
			num = num3 - mAP_BOUNDS_RANGE;
			mIsLimitMove = true;
		}
		if (!flag && ((!mPageMoveBacking && num2 > 0f && num2 < 2.5f) || num2 < 0.05f))
		{
			return;
		}
		if (mIsLimitMove)
		{
			mScrollPowerY *= 0.1f;
		}
		else if (DragPower.magnitude < s_DecayScrollY)
		{
			Vector2 normalized = new Vector2(0f, mScrollPowerY).normalized;
			float num4 = mScrollPowerY;
			mScrollPowerY -= normalized.y * Time.deltaTime * 90f;
			if (num4 > 0f && mScrollPowerY < 0f)
			{
				mScrollPowerY = 0f;
			}
			else if (num4 < 0f && mScrollPowerY > 0f)
			{
				mScrollPowerY = 0f;
			}
		}
		mCurrentPage.gameObject.transform.localPosition = new Vector3(x, num, mCurrentPage.gameObject.transform.localPosition.z);
		if (!flag && !mCurrentPage.IsActiveObject)
		{
			mActiveObjectInCameraFunc = StartCoroutine(mCurrentPage.ActiveObjectInCamera(num));
		}
	}

	public int GetCurrentPageIndex(float y)
	{
		return 1;
	}

	public IEnumerator SlideMapMove(float y)
	{
		float start_pos_y = mCurrentPage.gameObject.transform.localPosition.y;
		float limit_min_y = Map_DeviceOffset;
		float limit_max_y = 0f - (1136f * (float)(mMaxModulePage - 1) + limit_min_y);
		float x = mCurrentPage.gameObject.transform.localPosition.x;
		float z = mCurrentPage.gameObject.transform.localPosition.z;
		if (y > limit_min_y)
		{
			y = limit_min_y;
		}
		else if (y < limit_max_y)
		{
			y = limit_max_y;
		}
		float progress2 = 0f;
		float angle = 0f;
		while (true)
		{
			bool finished = false;
			angle += Time.deltaTime * 90f;
			if (angle > 90f)
			{
				angle = 90f;
				finished = true;
			}
			progress2 = Mathf.Sin(angle * ((float)Math.PI / 180f));
			float current_y = Mathf.Lerp(start_pos_y, y, progress2);
			mCurrentPage.gameObject.transform.localPosition = new Vector3(x, current_y, z);
			if (finished)
			{
				break;
			}
			yield return null;
		}
	}

	public IEnumerator DirectMapMove(float y)
	{
		float limit_min_y = Map_DeviceOffset;
		float limit_max_y = 0f - (1136f * (float)(mMaxModulePage - 1) + limit_min_y);
		if (y > limit_min_y)
		{
			y = limit_min_y;
		}
		else if (y < limit_max_y)
		{
			y = limit_max_y;
		}
		mCurrentPage.gameObject.transform.localPosition = new Vector3(mCurrentPage.gameObject.transform.localPosition.x, y, mCurrentPage.gameObject.transform.localPosition.z);
		SetScrollPower(0f);
		while (mCurrentPage.IsActiveObject)
		{
			yield return 0;
		}
		mActiveObjectInCameraFunc = StartCoroutine(mCurrentPage.ActiveObjectInCamera(y));
		yield return mActiveObjectInCameraFunc;
	}

	public void SetScrollPower(float a_power)
	{
		mScrollPowerY = a_power;
	}

	protected virtual void MapMove()
	{
		if (mGame.mTouchDownTrg)
		{
			mDownPos = mGame.mTouchPos;
		}
		if (mPageMoveLock != Def.PRESS.HIT)
		{
			if (mGame.mTouchWasDrag && !mPageMoved)
			{
				Vector3 vector = mCamera.ScreenToWorldPoint(new Vector3(0f, mGame.mTouchPos.y));
				Vector3 vector2 = mCamera.ScreenToWorldPoint(new Vector3(0f, mGame.mTouchPosOld.y));
				MoveVector = (vector - vector2) * Map_DeviceScreenSizeY / 2f;
				mScrollPowerYTemp += MoveVector.y;
				mPageMoveBacking = false;
				if (Mathf.Abs(mScrollPowerYTemp) > s_ScrollThresholdY)
				{
					float num = Vector2.Dot(MoveVector, MoveVectorOld);
					if (num < 0f)
					{
						mScrollPowerYTemp = mScrollPowerY / 5f;
					}
					if (Mathf.Abs(mScrollPowerYTemp) > 75f)
					{
						mScrollPowerYTemp = ((!(mScrollPowerYTemp < 0f)) ? 75f : (-75f));
					}
					DragPower = new Vector2(0f, mScrollPowerYTemp);
					SetScrollPower(mScrollPowerYTemp);
					mScrollPowerYTemp = 0f;
					MoveVectorOld = MoveVector;
				}
			}
			else
			{
				MoveVectorOld = Vector2.zero;
			}
		}
		if (!mGame.mTouchCnd)
		{
			mPageMoved = false;
			mPageMoveLock = Def.PRESS.NONE;
		}
	}

	public virtual bool OnDrawCockpit()
	{
		return false;
	}

	public virtual bool IsEnableButton()
	{
		return false;
	}

	protected virtual bool WebviewStartCheck()
	{
		return false;
	}

	protected virtual bool GiftStartCheck()
	{
		mGame.CheckMailDeadLine();
		GiftAttentionCheck();
		return false;
	}

	protected virtual void GiftAttentionCheck()
	{
		if (mCockpit != null)
		{
			mCockpit.SetEnableMailAttention(mGame.CheckShowGiftDialog(GameMain.GiftUIMode.RECEIVE) != GameMain.GiftUIMode.NONE || mGame.CheckShowGiftDialog(GameMain.GiftUIMode.SEND) != GameMain.GiftUIMode.NONE || mGame.CheckShowGiftDialog(GameMain.GiftUIMode.RESCUE) != GameMain.GiftUIMode.NONE);
		}
	}

	protected bool MaintenanceModeStartCheck()
	{
		if (!GameMain.MaintenanceMode)
		{
			return false;
		}
		StartCoroutine(GrayOut());
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("MaintenanceMode_ReturnTitle_Desc"));
		mConfirmDialog.Init(Localization.Get("MaintenanceMode_ReturnTitle_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mConfirmDialog.SetClosedCallback(OnMaintenanceModeDialogClosed);
		mConfirmDialog.SetBaseDepth(70);
		return true;
	}

	protected virtual void OnMaintenanceModeDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mGame.IsTitleReturnMaintenance = true;
		OnExitToTitle(true);
	}

	protected virtual void OnExitToTitle(bool force)
	{
	}

	protected DLFILE_CHECK_RESULT DLFilelistHashCheck()
	{
		DLFILE_CHECK_RESULT result = DLFILE_CHECK_RESULT.OK;
		if (GameMain.USE_RESOURCEDL)
		{
			if (mGame.mDLManager.DLFilelistHashCheck())
			{
				if (!GameMain.mDLFileIntegrityCheckDone)
				{
					if (!mGame.mDLManager.CheckDLFileIntegrity())
					{
						StartCoroutine(GrayOut());
						ConfirmDialog confirmDialog = Util.CreateGameObject("MapDestroyDialog", mRoot).AddComponent<ConfirmDialog>();
						confirmDialog.SetClosedCallback(OnMapDestroyClosed);
						confirmDialog.SetSortOrder(510);
						confirmDialog.Init(Localization.Get("Map_destroy_Title"), Localization.Get("Map_destroy_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
						result = DLFILE_CHECK_RESULT.FILE_DESTROY;
					}
					else
					{
						GameMain.mDLFileIntegrityCheckDone = true;
					}
				}
			}
			else
			{
				StartCoroutine(GrayOut());
				ConfirmDialog confirmDialog2 = Util.CreateGameObject("MapDestroyUpdateDialog", mRoot).AddComponent<ConfirmDialog>();
				confirmDialog2.SetClosedCallback(OnMapDestroyUpDateClosed);
				confirmDialog2.SetSortOrder(510);
				confirmDialog2.Init(Localization.Get("Map_destroy_Update_Title"), Localization.Get("Map_destroy_Update_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				result = DLFILE_CHECK_RESULT.HASH_NG;
			}
		}
		return result;
	}

	protected virtual void OnMapDestroyUpDateClosed(ConfirmDialog.SELECT_ITEM i)
	{
	}

	protected virtual void OnMapDestroyClosed(ConfirmDialog.SELECT_ITEM i)
	{
	}

	protected virtual bool IsEventExpired(bool a_return, ConfirmDialog.OnDialogClosed a_callback, Action a_cancel_callback)
	{
		EventCockpit eventCockpit = mCockpit as EventCockpit;
		if (!mGame.IsSeasonEventExpired(CurrentEventID) && (eventCockpit == null || !eventCockpit.IsOldEventFinished))
		{
			return false;
		}
		if (a_return)
		{
			Action action = delegate
			{
				mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("ReturnMap_Desc02"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				mConfirmDialog.SetClosedCallback(a_callback);
			};
			if (eventCockpit == null || !eventCockpit.TryOpenOldEventExtensionDialog(a_cancel_callback, action))
			{
				action();
			}
		}
		else
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("EventExpired_Desc02"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(a_callback);
		}
		return true;
	}

	protected virtual bool IsEventExpired(string a_title, string a_message, ConfirmDialog.OnDialogClosed a_callback)
	{
		if (!mGame.IsSeasonEventExpired(CurrentEventID))
		{
			return false;
		}
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(a_title, a_message, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mConfirmDialog.SetClosedCallback(a_callback);
		return true;
	}

	protected virtual bool IsEventHelpExpired(ConfirmDialog.OnDialogClosed a_callback)
	{
		if (!mGame.IsSeasonEventExpired(CurrentEventID))
		{
			return false;
		}
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
		mConfirmDialog.Init(Localization.Get("EventExpired_Title"), Localization.Get("EventExpired_Desc04"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mConfirmDialog.SetClosedCallback(a_callback);
		return true;
	}

	protected virtual void RegisterServerCallback()
	{
	}

	protected virtual void UnregisterServerCallback()
	{
	}

	protected virtual void Network_OnStage()
	{
	}

	protected virtual void Network_OnStageWait()
	{
	}

	protected virtual bool SetDeviceScreen(ScreenOrientation o)
	{
		return false;
	}

	protected virtual void SetLayout(ScreenOrientation o)
	{
	}

	public virtual void CreateMugenHeartDialog(MugenHeartDialog.SELECT_STATUS i)
	{
		StartCoroutine(GrayOut());
		MugenHeartDialog mugenHeartDialog = Util.CreateGameObject("MugenHeartDialog", mRoot).AddComponent<MugenHeartDialog>();
		mugenHeartDialog.Init(i);
		mugenHeartDialog.SetClosedCallback(OnMugenDialogDialogClosed);
	}

	public virtual void StartMugenHeart()
	{
		StartCoroutine(GrayIn());
		mGame.mPlayer.SaveInfiniteHeartStartState();
	}

	private IEnumerator GetMaintenanceProfileProcess()
	{
		mGame.mPlayer.Data.LastGetMaintenanceInfoTime = DateTimeUtil.UnitLocalTimeEpoch;
		mGame.Network_GetMaintenanceProfile(string.Empty, string.Empty);
		while (!GameMain.mMaintenanceProfileCheckDone)
		{
			yield return null;
		}
		if (GameMain.mMaintenanceProfileSucceed)
		{
			if (mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.LAST_CHECK_WAIT)
			{
				mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.LAST_CHECK_END;
			}
			else
			{
				StartMugenHeart();
			}
		}
		else
		{
			mGame.CreateConnectErrorMaintenaceDialog(ConnectCommonErrorDialog.STYLE.RETRY, OnMaintenancheCheckErrorDialogClosed, 0);
		}
	}

	public virtual void OnMugenDialogDialogClosed(MugenHeartDialog.SELECT_STATUS i)
	{
		if (i == MugenHeartDialog.SELECT_STATUS.MUGEN_START)
		{
			StartCoroutine(GetMaintenanceProfileProcess());
			return;
		}
		if (!mGame.IsDailyChallengePuzzle)
		{
			StartCoroutine(GrayIn());
		}
		mGame.mPlayer.SendCramInfiniteHeartEnd();
	}

	private void OnMaintenancheCheckErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int state)
	{
		StartCoroutine(GetMaintenanceProfileProcess());
	}

	public virtual void OnMugenHeartButtonPushed(GameObject go)
	{
		StartCoroutine("GrayOut");
		BuyBoosterDialog buyBoosterDialog = Util.CreateGameObject("BoosterDialog", mRoot).AddComponent<BuyBoosterDialog>();
		buyBoosterDialog.Init(mGame.mShopItemData[mCockpit.mMugenHeartItemID], false, false, mCockpit.mMugenHeartShopCount);
		if (!Def.IsEventSeries(mGame.mPlayer.Data.CurrentSeries))
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SALE_MAP;
			buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.MAP_SALE;
		}
		else
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SALE_EVENTMAP;
			buyBoosterDialog.mPlace = BuyBoosterDialog.PLACE.EVENT_SALE;
		}
		buyBoosterDialog.SetClosedCallback(OnMugenHeartDialogClosed);
		buyBoosterDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
	}

	public virtual void OnMugenHeartDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		mCockpit.UpdateMugenHeartIconFlg();
		StartCoroutine(GrayIn());
	}

	public virtual void OnStoreBagPushed(GameObject go, bool linkBanner = false)
	{
		mLinkBannerOnStore = linkBanner;
		if (this is GameStateSMMap)
		{
			(this as GameStateSMMap).OnStorePushed(null);
			return;
		}
		StartCoroutine("GrayOut");
		mShopFullDialog = Util.CreateGameObject("ShopFullDialog", mRoot).AddComponent<ShopFullDialog>();
		mShopFullDialog.Init(ShopFullDialog.LIST_TYPE.SALE);
		mShopFullDialog.SetCallback(ShopFullDialog.EVENT_STATE.AUTH_ERROR, OnDialogAuthErrorClosed);
		mShopFullDialog.SetCallback(DialogBase.CALLBACK_STATE.OnCloseFinished, delegate
		{
			OnStoreBagDialogClosed(null, BuyBoosterDialog.SELECT_ITEM.BUY_COMPLETE);
		});
	}

	public virtual void OnStoreBagDialogClosed(ShopItemData ItemData, BuyBoosterDialog.SELECT_ITEM item)
	{
		mCockpit.UpdateSaleFlg();
		StartCoroutine(GrayIn());
	}

	public void OpenWebSaleDialog(WebSaleDialog.MODE aMode, int aIndex = -1)
	{
		mWebSaleIndex = -1;
		WebSaleDialog webSaleDialog = Util.CreateGameObject("WebSaleDialog", mRoot).AddComponent<WebSaleDialog>();
		webSaleDialog.Init(aMode, aIndex);
		webSaleDialog.SetClosedCallback(OnWebSaleDialogClosed);
		if (!Def.IsEventSeries(mGame.mPlayer.Data.CurrentSeries))
		{
			if (!mLinkBannerOnStore)
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SALE_MAP;
				webSaleDialog.mPlace = WebSaleDialog.PLACE.MAP_SALE;
			}
			else
			{
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SALE_MAP;
				webSaleDialog.mPlace = WebSaleDialog.PLACE.LINBANNER_MAP_SALE;
			}
		}
		else if (!mLinkBannerOnStore)
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SALE_EVENTMAP;
			webSaleDialog.mPlace = WebSaleDialog.PLACE.EVENT_SALE;
		}
		else
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.SALE_EVENTMAP;
			webSaleDialog.mPlace = WebSaleDialog.PLACE.LINBANNER_EVENT_SALE;
		}
	}

	public void ReOpenWebSaleDialog()
	{
		if (mWebSaleIndex == -1)
		{
			OpenWebSaleDialog(WebSaleDialog.MODE.TOP);
		}
		else
		{
			OpenWebSaleDialog(WebSaleDialog.MODE.DETAIL, mWebSaleIndex);
		}
	}

	public void OnWebSaleDialogClosed(WebSaleDialog.SELECT_ITEM item, int aIndex)
	{
		switch (item)
		{
		case WebSaleDialog.SELECT_ITEM.PURCHASE:
		{
			mWebSaleIndex = aIndex;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(mRoot);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(OnDialogAuthErrorClosed);
			return;
		}
		case WebSaleDialog.SELECT_ITEM.EULA:
		{
			mWebSaleIndex = aIndex;
			EULADialog eULADialog = Util.CreateGameObject("EULADialog", mRoot).AddComponent<EULADialog>();
			eULADialog.Init(EULADialog.MODE.ASCT);
			eULADialog.SetCallback(OnEULADialogClosed);
			return;
		}
		}
		if (aIndex > 0)
		{
			switch (item)
			{
			case WebSaleDialog.SELECT_ITEM.BUY:
			{
				int key = mGame.SegmentProfile.WebSale.DetailList[aIndex - 1];
				ShopItemData shopItemData = mGame.mShopItemData[key];
				TuxedoMaskDilog tuxedoMaskDilog = Util.CreateGameObject("TuxedDialog", mRoot).AddComponent<TuxedoMaskDilog>();
				tuxedoMaskDilog.Init(mGame.mShopItemData[shopItemData.Index]);
				tuxedoMaskDilog.SetClosedCallback(OnTuxedMaskDialogClosed);
				break;
			}
			case WebSaleDialog.SELECT_ITEM.DETAIL:
				OpenWebSaleDialog(WebSaleDialog.MODE.DETAIL, aIndex);
				break;
			case WebSaleDialog.SELECT_ITEM.NEGATIVE:
				if (!GameMain.MaintenanceMode)
				{
					OpenWebSaleDialog(WebSaleDialog.MODE.TOP);
					break;
				}
				mWebSaleIndex = -1;
				OnStoreBagDialogClosed(null, BuyBoosterDialog.SELECT_ITEM.CANCEL);
				break;
			}
		}
		else
		{
			OnStoreBagDialogClosed(null, BuyBoosterDialog.SELECT_ITEM.CANCEL);
		}
	}

	private void OnTuxedMaskDialogClosed(TuxedoMaskDilog.SELECT_ITEM i)
	{
		OpenWebSaleDialog(WebSaleDialog.MODE.TOP);
	}

	public void OnPurchaseDialogClosed()
	{
		ReOpenWebSaleDialog();
	}

	public void OnEULADialogClosed(EULADialog.SELECT_ITEM item)
	{
		ReOpenWebSaleDialog();
	}

	public virtual void OnDialogAuthErrorClosed()
	{
	}

	public virtual void DEBUG_ChangeStateToInit()
	{
	}

	public virtual void OnIndexButtonPushed(int index)
	{
	}

	public void SetAlternateReward(int a_accessory)
	{
		List<AccessoryData> alternateReward = mGame.GetAlternateReward(a_accessory);
		if (alternateReward.Count <= 0)
		{
			return;
		}
		mAlterUnlockAccessory = new List<AccessoryData>(alternateReward);
		for (int i = 0; i < mAlterUnlockAccessory.Count; i++)
		{
			AccessoryData accessoryData = mAlterUnlockAccessory[i];
			if (accessoryData.AccessoryType == AccessoryData.ACCESSORY_TYPE.GEM)
			{
				mGame.AddAccessoryConnect(accessoryData.Index);
				continue;
			}
			mGame.mPlayer.AddAccessory(accessoryData);
			mGame.mPlayer.SetCollectionNotify(accessoryData.Index);
			mCockpit.SetEnableMenuButtonAttention(true);
		}
		mGame.Save();
	}

	public virtual void CheckRewardOnEnterMap()
	{
	}

	public virtual void ReEnterMap()
	{
		throw new NotImplementedException();
	}

	public void UpdateMugenHeartStatus()
	{
		if (mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.NONE)
		{
			bool flag = false;
			if (0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART15))
			{
				mGame.mPlayer.Data.mUseMugenHeartSetTime = Def.MugenHeartLimitTimeData[Def.BOOSTER_KIND.MUGEN_HEART15];
				flag = true;
			}
			else if (0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART30))
			{
				mGame.mPlayer.Data.mUseMugenHeartSetTime = Def.MugenHeartLimitTimeData[Def.BOOSTER_KIND.MUGEN_HEART30];
				flag = true;
			}
			else if (0 < mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.MUGEN_HEART60))
			{
				mGame.mPlayer.Data.mUseMugenHeartSetTime = Def.MugenHeartLimitTimeData[Def.BOOSTER_KIND.MUGEN_HEART60];
				flag = true;
			}
			if (flag)
			{
				CreateMugenHeartDialog(MugenHeartDialog.SELECT_STATUS.MUGEN_START);
			}
		}
		else if (mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.LAST_CHECK)
		{
			mGame.mPlayer.Data.mMugenHeart = Def.MUGEN_HEART_STATUS.LAST_CHECK_WAIT;
			StartCoroutine(GetMaintenanceProfileProcess());
		}
		else if (mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.TIME_OVER || mGame.mPlayer.Data.mMugenHeart == Def.MUGEN_HEART_STATUS.TIME_OVER_PUZZLE)
		{
			CreateMugenHeartDialog(MugenHeartDialog.SELECT_STATUS.MUGEN_END);
		}
	}

	public bool CheckGuideToADV()
	{
		return false;
	}

	protected int GetPossessCompanionCramValue()
	{
		int result = 0;
		SMEventPageData sMEventPageData = mMapPageData as SMEventPageData;
		if (sMEventPageData != null)
		{
			Action<int> action = delegate(int fn_acc_id)
			{
				if (mGame.mAccessoryData[fn_acc_id].AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
				{
					int companionID = mGame.mAccessoryData[fn_acc_id].GetCompanionID();
					if (mGame.mPlayer.IsCompanionUnlock(companionID))
					{
						result++;
					}
					if (mGame.mPlayer.IsAccessoryUnlock(mGame.GetAlternateRewardIdByBaseAccessoryId(fn_acc_id)))
					{
						result += 10;
					}
				}
			};
			if (this is GameStateEventStar)
			{
				foreach (int key in sMEventPageData.EventCourseList.Keys)
				{
					List<SMRoadBlockSetting> roadBlocks = sMEventPageData.GetRoadBlocks(key);
					if (roadBlocks == null)
					{
						continue;
					}
					foreach (SMRoadBlockSetting item in roadBlocks)
					{
						if (item != null && item is SMEventRoadBlockSetting)
						{
							SMEventRoadBlockSetting sMEventRoadBlockSetting = item as SMEventRoadBlockSetting;
							if (sMEventRoadBlockSetting != null && sMEventRoadBlockSetting.AccessoryIDOnOpened != 0)
							{
								action(sMEventRoadBlockSetting.AccessoryIDOnOpened);
							}
						}
					}
				}
			}
			else if (this is GameStateEventRally)
			{
				foreach (KeyValuePair<int, SMEventCourseSetting> eventCourse in sMEventPageData.EventCourseList)
				{
					action(eventCourse.Value.ClearAccessoryID);
				}
			}
			else if (this is GameStateEventRally2)
			{
				foreach (int key2 in sMEventPageData.EventCourseList.Keys)
				{
					List<SMRoadBlockSetting> roadBlocks2 = sMEventPageData.GetRoadBlocks(key2);
					if (roadBlocks2 == null)
					{
						continue;
					}
					foreach (SMRoadBlockSetting item2 in roadBlocks2)
					{
						if (item2 != null && item2 is SMEventRoadBlockSetting)
						{
							SMEventRoadBlockSetting sMEventRoadBlockSetting2 = item2 as SMEventRoadBlockSetting;
							if (sMEventRoadBlockSetting2 != null && sMEventRoadBlockSetting2.AccessoryIDOnOpened != 0)
							{
								action(sMEventRoadBlockSetting2.AccessoryIDOnOpened);
							}
						}
					}
				}
			}
		}
		return result;
	}

	public virtual bool IsStageButtonSECall(int stage)
	{
		return false;
	}

	public virtual void OnSeriesCampaignPushed(Def.SERIES a_series)
	{
	}

	protected virtual bool EnableSocialRankingDialog()
	{
		if (mGame.mTutorialManager.IsTutorialPlaying())
		{
			return false;
		}
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
		{
			return false;
		}
		if (PurchaseFullDialog.IsShow())
		{
			return false;
		}
		return true;
	}

	protected virtual void ShowSocialRankingDialog(int aSeries, int aStage, List<StageRankingData> aRanking)
	{
		GameObject parent;
		bool screen;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			parent = mAnchorBottom.gameObject;
			screen = true;
		}
		else
		{
			parent = mAnchorRight.gameObject;
			screen = false;
		}
		mSocialRanking = Util.CreateGameObject("SocialRanking", parent).AddComponent<SocialRankingWindow>();
		if (mSocialRankingDepth == 0)
		{
			mSocialRankingDepth = SingletonMonoBehaviour<GameMain>.Instance.mDialogManager.mDepth - 3;
		}
		mSocialRanking.SetSortingOder(mSocialRankingDepth);
		mSocialRanking.Init(aSeries, aStage, aRanking, false, screen);
	}

	public void ShowSocialRankingDialog()
	{
		if (mSocialRanking != null)
		{
			UnityEngine.Object.Destroy(mSocialRanking.gameObject);
			mSocialRanking = null;
		}
		Network_OnStageWait();
	}

	public IEnumerator CloseSocialRanking(Action<bool> on_closed = null)
	{
		bool result = false;
		if (mSocialRanking != null)
		{
			bool isClosed = false;
			mSocialRanking.SetCallback(SocialRankingWindow.CALLBACK.OnClosed, delegate
			{
				isClosed = true;
			});
			mSocialRanking.Close();
			while (!isClosed)
			{
				yield return null;
			}
			result = true;
		}
		if (on_closed != null)
		{
			on_closed(result);
		}
	}
}
