using UnityEngine;

public class ParticleEmitterControl : MonoBehaviour
{
	private float mAngle;

	private SMParticle mParticle;

	private SsSprite mTargetAnime;

	private float mDestroyTimer;

	private GameObject uiRoot;

	private float mOfsZ = -2f;

	private void Awake()
	{
		uiRoot = GameObject.Find("UI Root (2D)");
	}

	private void OnDestroy()
	{
		Object.Destroy(mParticle.gameObject);
	}

	private void Start()
	{
		mAngle = 0f;
		mParticle.mParticle.Play();
	}

	private void Update()
	{
		if (mDestroyTimer > 0f)
		{
			mDestroyTimer -= Time.deltaTime;
			if (mDestroyTimer <= 0f)
			{
				mDestroyTimer = 0f;
				Object.Destroy(base.gameObject);
				return;
			}
		}
		if (mTargetAnime != null)
		{
			if (mTargetAnime.IsPlaying())
			{
				UpdatePosition();
				return;
			}
			mParticle.mParticle.Stop();
			mDestroyTimer = 3f;
			mTargetAnime = null;
		}
	}

	private void UpdatePosition()
	{
		SsAnimation animation = mTargetAnime.Animation;
		SsPartRes[] partList = animation.PartList;
		for (int i = 0; i < partList.Length; i++)
		{
			if (!partList[i].IsRoot && partList[i].Name == "particleEmitter")
			{
				float num = partList[i].PosX((int)mTargetAnime.AnimFrame);
				float num2 = 0f - partList[i].PosY((int)mTargetAnime.AnimFrame);
				mParticle.transform.localPosition = new Vector3((num + mTargetAnime.transform.localPosition.x) * uiRoot.transform.localScale.x, (num2 + mTargetAnime.transform.localPosition.y) * uiRoot.transform.localScale.y, (mTargetAnime.transform.localPosition.z + mOfsZ) * uiRoot.transform.localScale.z);
			}
		}
	}

	public void Init(SsSprite target, string particleName, float ofsZ = -2f)
	{
		mTargetAnime = target;
		mDestroyTimer = 0f;
		mOfsZ = ofsZ;
		mParticle = Util.CreateGameObject("EmitterControl", null).AddComponent<SMParticle>();
		mParticle.gameObject.layer = base.gameObject.layer;
		mParticle.LoadFromPrefab(particleName);
		mParticle.gameObject.transform.localScale = new Vector3(1f, 1f, 0.1f);
		UpdatePosition();
	}
}
