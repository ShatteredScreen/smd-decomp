using System.Collections.Generic;

public sealed class ChanceInfo
{
	[MiniJSONAlias("comm")]
	public ChanceCommInfo Common { get; set; }

	[MiniJSONAlias("chance")]
	public List<ChanceDataInfo> Data { get; set; }

	public ChanceInfo()
	{
		Common = new ChanceCommInfo();
		Data = new List<ChanceDataInfo>();
	}
}
