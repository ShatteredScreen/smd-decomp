using System;
using System.IO;
using System.Security.Cryptography;

public class BIJIOUtils
{
	private const int ZIP_LEAD_BYTES = 67324752;

	private const ushort GZIP_LEAD_BYTES = 35615;

	private const int CHECKSUM_BUFFER_SIZE = 1048576;

	internal static bool IsPkZipCompressedData(byte[] data)
	{
		return BitConverter.ToInt32(data, 0) == 67324752;
	}

	internal static bool IsGZipCompressedData(byte[] data)
	{
		return BitConverter.ToUInt16(data, 0) == 35615;
	}

	public static bool IsCompressedData(byte[] data)
	{
		return IsPkZipCompressedData(data) || IsGZipCompressedData(data);
	}

	public static bool IsCompressedFile(string path)
	{
		try
		{
			if (File.Exists(path))
			{
				using (Stream stream = File.OpenRead(path))
				{
					byte[] array = new byte[4];
					int num = stream.Read(array, 0, array.Length);
					if (num != array.Length)
					{
						return false;
					}
					return IsCompressedData(array);
				}
			}
		}
		catch (Exception)
		{
		}
		return false;
	}

	public static string GetFileChecksum(string filePath)
	{
		return GetFileChecksum(filePath, 1048576);
	}

	public static string GetFileChecksum(string filePath, int bufferSize)
	{
		if (!File.Exists(filePath))
		{
			return string.Empty;
		}
		string empty = string.Empty;
		try
		{
			using (Stream inputStream = new BufferedStream(File.OpenRead(filePath), bufferSize))
			{
				SHA256Managed sHA256Managed = new SHA256Managed();
				byte[] array = sHA256Managed.ComputeHash(inputStream);
				return BitConverter.ToString(array).Replace("-", string.Empty);
			}
		}
		catch (Exception)
		{
			return null;
		}
	}

	public static string getFileHash(string path)
	{
		string result = string.Empty;
		if (File.Exists(path))
		{
			MD5 mD = MD5.Create();
			FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
			byte[] array = mD.ComputeHash(fileStream);
			result = BitConverter.ToString(array).Replace("-", string.Empty).ToLowerInvariant();
			fileStream.Close();
		}
		return result;
	}
}
