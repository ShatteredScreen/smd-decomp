public class NWDebugTerminal : ParameterObject<NWDebugTerminal>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("bhiveid")]
	public int HiveID { get; set; }

	[MiniJSONAlias("beid")]
	public string BundleID { get; set; }
}
