using System;
using System.Reflection;
using System.Text;

public class ParameterObject<T> : ICloneable where T : class, new()
{
	[MiniJSONAlias("imsi")]
	public string IMSI { get; set; }

	[MiniJSONAlias("iccid")]
	public string ICCID { get; set; }

	[MiniJSONAlias("device_serial_id")]
	public string DeviceSerial { get; set; }

	[MiniJSONAlias("auth_key")]
	public string AuthKey { get; set; }

	public ParameterObject()
	{
		IMSI = Network.IMSI;
		ICCID = Network.Serial;
		DeviceSerial = Network.DeviceSerial;
		AuthKey = UserDataInfo.Instance.AuthKey;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public T Clone()
	{
		return MemberwiseClone() as T;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (propertyInfo.CanRead)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(',');
				}
				stringBuilder.Append(propertyInfo.Name);
				stringBuilder.Append('=');
				object value = propertyInfo.GetValue(this, null);
				if (value == null)
				{
					stringBuilder.Append(value);
				}
				else
				{
					value.ToDump(stringBuilder);
				}
			}
		}
		return string.Format("[{0}: {1}]", typeof(T).Name, stringBuilder.ToString());
	}

	public void CopyTo(T _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(T);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (!propertyInfo.CanRead)
			{
				continue;
			}
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				if (!property.CanWrite)
				{
					throw new Exception(string.Format("Not writable property {0}@{1}", typeFromHandle.Name, property.Name));
				}
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}
}
