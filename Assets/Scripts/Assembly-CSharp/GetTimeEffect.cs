using System;
using System.Collections;
using UnityEngine;

public class GetTimeEffect : MonoBehaviour
{
	private FixedSprite mSprite;

	private Color mColor;

	private float mTime;

	private float mScale;

	private float mDelayTime;

	private Vector3 mPos;

	private float mAngle;

	private int mStep;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void Init(float time, Def.TILE_FORM form, float delay)
	{
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_TILE");
		string spriteName;
		switch (form)
		{
		case Def.TILE_FORM.ADDPLUS:
			spriteName = "add2moves";
			break;
		case Def.TILE_FORM.ADDTIME:
			spriteName = "add5sec";
			break;
		default:
			spriteName = "add1000score";
			break;
		}
		mSprite = Util.CreateGameObject("Num", base.gameObject).AddComponent<FixedSprite>();
		mSprite.Init("PUZZLETILE_FIXED_SPRITE", spriteName, Vector3.zero, resImage.Image);
		mColor = new Color(1f, 1f, 1f, 1f);
		if (delay > 0f)
		{
			mSprite.enabled = false;
		}
		StartCoroutine(CoUpdate(delay));
		mStep = 0;
		mTime = 1f;
		mAngle = 0f;
		mScale = 0.9f;
		mDelayTime = delay;
		mPos = base.transform.localPosition;
	}

	private IEnumerator CoUpdate(float delayTime)
	{
		if (delayTime > 0f)
		{
			while (true)
			{
				delayTime -= Time.deltaTime;
				if (delayTime <= 0f)
				{
					break;
				}
				yield return 0;
			}
			mSprite.enabled = true;
		}
		float angle3 = 0f;
		angle3 = 0f;
		while (angle3 < 180f)
		{
			angle3 += Time.deltaTime * 450f;
			if (angle3 > 180f)
			{
				angle3 = 180f;
			}
			float y = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * 30f;
			mSprite.transform.localPosition = new Vector3(0f, y, 0f);
			float scale = Mathf.Sin(angle3 / 180f * 90f * ((float)Math.PI / 180f)) + 0.2f;
			mSprite.transform.localScale = new Vector3(scale, scale, 1f);
			yield return 0;
		}
		angle3 = 0f;
		while (angle3 < 180f)
		{
			angle3 += Time.deltaTime * 810f;
			if (angle3 > 180f)
			{
				angle3 = 180f;
			}
			float y2 = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * 10f;
			mSprite.transform.localPosition = new Vector3(0f, y2, 0f);
			yield return 0;
		}
		yield return new WaitForSeconds(1f);
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
