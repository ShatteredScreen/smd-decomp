using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBlockDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		MAINTENANCE_CHECK_START = 3,
		MAINTENANCE_CHECK_WAIT = 4,
		NETWORK_00 = 5,
		NETWORK_00_1 = 6,
		NETWORK_00_2 = 7,
		NETWORK_01 = 8,
		NETWORK_01_1 = 9,
		NETWORK_01_2 = 10,
		MAINTENANCE_CHECK_START02 = 11,
		MAINTENANCE_CHECK_WAIT02 = 12,
		NETWORK_02 = 13,
		NETWORK_02_1 = 14,
		NETWORK_02_2 = 15,
		AUTHERROR_WAIT = 16
	}

	public enum SELECT_ITEM
	{
		CLEAR = 0,
		UNLOCK_SD = 1,
		UNLOCK_KEY = 2,
		UNLOCK_FRIEND = 3,
		UNLOCK_TICKET = 4,
		CANCEL = 5
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.WAIT);

	public STATE StateStatus;

	private STATE mStateAfterNetworkError = STATE.MAIN;

	private List<string> mPreLoadAnimationKeys = new List<string>();

	private SELECT_ITEM mSelectItem;

	private SMRoadBlockSetting mRoadBlockSetting;

	private int mFriendNum;

	private int mKeyNum;

	private OnDialogClosed mCallback;

	private ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mSNSCallCount;

	private RoadBlockHelpDialog mRoadBlockHelpDialog;

	private SNSFriendManager mFriendManager;

	private Live2DRender mL2DRender;

	private UITexture mL2DTexture;

	private Live2DInstance mL2DInstLuna;

	private Live2DInstance mL2DInstArtemis;

	private ShopItemData mBoughtItem;

	private ConfirmDialog mErrorDialog;

	public SMRoadBlockSetting CurrentRoadBlockSetting
	{
		get
		{
			return mRoadBlockSetting;
		}
	}

	public override void Start()
	{
		base.Start();
	}

	public override IEnumerator BuildResource()
	{
		ResLive2DAnimation anim = ResourceManager.LoadLive2DAnimationAsync("LUNA");
		ResLive2DAnimation anim2 = ResourceManager.LoadLive2DAnimationAsync("ARTEMIS");
		mPreLoadAnimationKeys.Add("LUNA");
		mPreLoadAnimationKeys.Add("ARTEMIS");
		while (anim.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (anim2.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		yield return 0;
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			CreateGemWindow();
			mState.Change(STATE.MAIN);
			break;
		case STATE.MAINTENANCE_CHECK_START:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.RETRY, true);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT:
			if (MaintenanceCheckResult() == MAINTENANCE_RESULT.OK)
			{
				mState.Change(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00:
			if (!mGame.Network_UserAuth())
			{
				if (GameMain.mUserAuthSucceed)
				{
					mState.Change(STATE.NETWORK_01);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_00);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_00_1);
			}
			break;
		case STATE.NETWORK_00_1:
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.NETWORK_00_2);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
			}
			else
			{
				ShowNetworkErrorDialog(STATE.NETWORK_00);
			}
			break;
		case STATE.NETWORK_00_2:
			mState.Change(STATE.NETWORK_01);
			break;
		case STATE.NETWORK_01:
			if (!mGame.Network_GetGemCount())
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.INIT);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			else
			{
				mState.Change(STATE.NETWORK_01_1);
			}
			break;
		case STATE.NETWORK_01_1:
			if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.NETWORK_01_2);
				}
				else
				{
					ShowNetworkErrorDialog(STATE.NETWORK_01);
				}
			}
			break;
		case STATE.NETWORK_01_2:
			mState.Change(STATE.INIT);
			break;
		case STATE.MAINTENANCE_CHECK_START02:
			MaintenanceCheck(ConnectCommonErrorDialog.STYLE.CLOSE, ConnectCommonErrorDialog.STYLE.CLOSE);
			mState.Change(STATE.MAINTENANCE_CHECK_WAIT02);
			break;
		case STATE.MAINTENANCE_CHECK_WAIT02:
			switch (MaintenanceCheckResult())
			{
			case MAINTENANCE_RESULT.OK:
				mState.Change(STATE.NETWORK_02);
				break;
			case MAINTENANCE_RESULT.ERROR:
			case MAINTENANCE_RESULT.IN_MAINTENANCE:
				if (!GetBusy())
				{
					mState.Change(STATE.MAIN);
				}
				break;
			}
			break;
		case STATE.NETWORK_02:
		{
			bool flag = mGame.Network_BuyItem(mBoughtItem.Index);
			mGame.ConnectRetryFlg = false;
			if (!flag)
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			else
			{
				mState.Change(STATE.NETWORK_02_1);
			}
			break;
		}
		case STATE.NETWORK_02_1:
		{
			if (!GameMain.mBuyItemCheckDone)
			{
				break;
			}
			if (GameMain.mBuyItemSucceed)
			{
				mState.Change(STATE.NETWORK_02_2);
				break;
			}
			Server.ErrorCode mBuyItemErrorCode = GameMain.mBuyItemErrorCode;
			if (mBuyItemErrorCode == Server.ErrorCode.DUPLICATE_DATA)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("DuplicateDataErrorDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("Network_Error_Title"), Localization.Get("Network_Error_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
				confirmDialog.SetClosedCallback(OnDuplicateDataErrorDialogClosed);
				mState.Change(STATE.WAIT);
			}
			else
			{
				ShowNetworkErrorDialog(STATE.MAIN);
			}
			break;
		}
		case STATE.NETWORK_02_2:
		{
			mBoughtItem = null;
			int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
			int unlock_by = 0;
			int cur_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
			int reach_time = 0;
			if (mGame.mPlayer.RoadBlockReachLevel == mRoadBlockSetting.RoadBlockLevel)
			{
				reach_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mGame.mPlayer.RoadBlockReachTime);
			}
			int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
			ServerCram.UnlockRoadblock(roadBlockLevel, unlock_by, cur_time, reach_time, currentSeries);
			mGame.mOptions.BuyUnlockRBCount++;
			mGame.SaveOptions();
			mSelectItem = SELECT_ITEM.UNLOCK_SD;
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		mState.Update();
	}

	public void Init(SMRoadBlockSetting data)
	{
		mRoadBlockSetting = data;
		int level = mGame.mPlayer.Level;
		int a_main;
		int a_sub;
		Def.SplitStageNo(level, out a_main, out a_sub);
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		float r = 66f / 85f;
		PlayerMapData currentMapData = mGame.mPlayer.CurrentMapData;
		int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
		if (roadBlockLevel >= currentMapData.MaxLevel)
		{
			mState.Change(STATE.MAIN);
			mL2DRender = mGame.CreateLive2DRender(568, 568);
			mL2DInstLuna = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			mL2DInstLuna.Init("LUNA", Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
			mL2DInstLuna.StartMotion("motions/talk00.mtn", true);
			mL2DInstLuna.SetExpression("expressions/F00.exp.json");
			mL2DInstLuna.Position = new Vector3(-180f, 0f, -50f);
			mL2DRender.ResisterInstance(mL2DInstLuna);
			mL2DInstArtemis = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			mL2DInstArtemis.Init("ARTEMIS", Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
			mL2DInstArtemis.StartMotion("motions/talk00.mtn", true);
			mL2DInstArtemis.SetExpression("expressions/F01.exp.json");
			mL2DInstArtemis.Position = new Vector3(180f, 0f, -50f);
			mL2DRender.ResisterInstance(mL2DInstArtemis);
			string titleDescKey = Localization.Get("RoadBlock_TitleMax");
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle(titleDescKey);
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, Localization.Get("RoadBlock_MaxDesc"), mBaseDepth + 4, new Vector3(0f, 144f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(uILabel);
			mGame.PlaySe("VOICE_009", -1);
		}
		else
		{
			mState.Change(STATE.MAINTENANCE_CHECK_START);
			bool flag = false;
			if (mGame.mPlayer.RoadBlockTicket > 0)
			{
				flag = true;
			}
			string titleDescKey = Localization.Get("RoadBlock_Title");
			InitDialogFrame(DIALOG_SIZE.LARGE);
			InitDialogTitle(titleDescKey);
			titleDescKey = Util.MakeLText("RoadBlock_Desc", mRoadBlockSetting.UnlockFriends);
			UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, 200f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			float y = 97f;
			UIButton uIButton;
			if (!flag)
			{
				uIButton = Util.CreateJellyImageButton("SDUnlock", base.gameObject, image);
				Util.SetImageButtonInfo(uIButton, "button_roadblock", "button_roadblock", "button_roadblock", mBaseDepth + 3, new Vector3(0f, y, 0f), Vector3.one);
				Util.AddImageButtonMessage(uIButton, this, "OnSDUnlockPushed", UIButtonMessage.Trigger.OnClick);
				UISprite sprite = Util.CreateSprite("GemImage", uIButton.gameObject, image);
				Util.SetSpriteInfo(sprite, "icon_currency_jem", mBaseDepth + 4, new Vector3(-169f, 0f, 0f), Vector3.one, false);
				titleDescKey = Util.MakeLText("RoadBlock_UseSD");
				uILabel = Util.CreateLabel("UseSDText", uIButton.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 5, new Vector3(-130f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Left);
				UISprite uISprite = Util.CreateSprite("LabelFrame", uIButton.gameObject, image);
				Util.SetSpriteInfo(uISprite, "button02", mBaseDepth + 4, new Vector3(154f, 0f, 0f), Vector3.one, false);
				uILabel = Util.CreateLabel("Price", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, string.Empty + mRoadBlockSetting.UnlockSD, mBaseDepth + 5, new Vector3(0f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
				if (mRoadBlockSetting.UnlockSD > mGame.mPlayer.Dollar)
				{
					uILabel.color = new Color(r, 0.1f, 0.1f);
				}
				else
				{
					uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
				}
			}
			else
			{
				uIButton = Util.CreateJellyImageButton("LoadBlockTicketUnlock", base.gameObject, image);
				Util.SetImageButtonInfo(uIButton, "button_roadblock", "button_roadblock", "button_roadblock", mBaseDepth + 3, new Vector3(0f, y, 0f), Vector3.one);
				Util.AddImageButtonMessage(uIButton, this, "OnRBTUnlockPushed", UIButtonMessage.Trigger.OnClick);
				UISprite uISprite2 = Util.CreateSprite("GemImage", uIButton.gameObject, image);
				Util.SetSpriteInfo(uISprite2, "icon_ticket", mBaseDepth + 4, new Vector3(-169f, 0f, 0f), Vector3.one, false);
				uISprite2.SetDimensions(54, 54);
				titleDescKey = Util.MakeLText("RoadBlock_UseRBTicket");
				uILabel = Util.CreateLabel("UseRBText", uIButton.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 5, new Vector3(-130f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Left);
				UISprite uISprite3 = Util.CreateSprite("LabelFrame", uIButton.gameObject, image);
				Util.SetSpriteInfo(uISprite3, "button02", mBaseDepth + 4, new Vector3(154f, 0f, 0f), Vector3.one, false);
				uILabel = Util.CreateLabel("Price", uISprite3.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, string.Empty + mGame.mPlayer.RoadBlockTicket, mBaseDepth + 5, new Vector3(0f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
				if (mRoadBlockSetting.UnlockTicket > mGame.mPlayer.RoadBlockTicket)
				{
					uILabel.color = new Color(r, 0.1f, 0.1f);
				}
				else
				{
					uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
				}
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			}
			y = -32f;
			uIButton = Util.CreateJellyImageButton("KeyUnlock", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnKeyUnlockPushed", UIButtonMessage.Trigger.OnClick);
			UISprite uISprite4 = Util.CreateSprite("KEYImage", uIButton.gameObject, image);
			Util.SetSpriteInfo(uISprite4, mGame.GetRBKeyImageName(mRoadBlockSetting.Series), mBaseDepth + 4, new Vector3(-169f, 0f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(54, 54);
			titleDescKey = Util.MakeLText("RoadBlock_UseKey");
			uILabel = Util.CreateLabel("UseRBText", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 5, new Vector3(-130f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Left);
			UISprite uISprite5 = Util.CreateSprite("LabelFrame", uIButton.gameObject, image);
			Util.SetSpriteInfo(uISprite5, "button02", mBaseDepth + 4, new Vector3(154f, 0f, 0f), Vector3.one, false);
			mKeyNum = 0;
			mKeyNum = mGame.mPlayer.GetRoadBlockKey(mRoadBlockSetting.Series);
			uILabel = Util.CreateLabel("KeyNum", uISprite5.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty + mKeyNum + "/" + mRoadBlockSetting.UnlockStars, mBaseDepth + 5, new Vector3(0f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			if (mRoadBlockSetting.UnlockStars > mKeyNum)
			{
				uILabel.color = new Color(r, 0.1f, 0.1f);
			}
			else
			{
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			}
			y = -133f;
			uIButton = Util.CreateJellyImageButton("FriendUnlock", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, y, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnFriendUnlockPushed", UIButtonMessage.Trigger.OnClick);
			UISprite sprite2 = Util.CreateSprite("FriendImage", uIButton.gameObject, image);
			Util.SetSpriteInfo(sprite2, "icon_friend", mBaseDepth + 4, new Vector3(-169f, 0f, 0f), Vector3.one, false);
			titleDescKey = Util.MakeLText("RoadBlock_UseFriend");
			uILabel = Util.CreateLabel("UseRBText", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 5, new Vector3(-130f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Left);
			UISprite uISprite6 = Util.CreateSprite("LabelFrame", uIButton.gameObject, image);
			Util.SetSpriteInfo(uISprite6, "button02", mBaseDepth + 4, new Vector3(154f, 0f, 0f), Vector3.one, false);
			mFriendNum = 0;
			Vector3 scale = new Vector3(1f, 1f, 1f);
			int num = Mathf.Min(mRoadBlockSetting.UnlockFriends, 6);
			if (num > 0)
			{
				Vector3[] array = new Vector3[num];
				int num2 = num * 80;
				float num3 = 80f;
				float num4 = 80f;
				float num5 = 0f;
				num5 = (0f - num3) * (float)num / 2f + num3 / 2f;
				y = -220f;
				int num6 = 3;
				int num7 = 1;
				for (int i = 0; i < num6; i++)
				{
					for (int j = 0; j < num7; j++)
					{
						int num8 = num6 * j + i;
						if (num8 < array.Length)
						{
							array[num8] = new Vector3(num5 + num3 * (float)i, y + num4 * ((float)(num7 - 1) * 0.5f - (float)j), 0f);
						}
					}
				}
				for (int k = 0; k < num; k++)
				{
					UISprite sprite3 = Util.CreateSprite("FriendFrame" + k, base.gameObject, image);
					Util.SetSpriteInfo(sprite3, "icon_panel", mBaseDepth + 5, array[k], scale, false);
				}
				if (mGame.mPlayer.Gifts.Count > 0)
				{
					mFriendNum = 0;
					foreach (KeyValuePair<string, GiftItem> gift in mGame.mPlayer.Gifts)
					{
						GiftItem value = gift.Value;
						if (value == null || value.GiftItemCategory != Def.ITEM_CATEGORY.ROADBLOCK_RES)
						{
							continue;
						}
						int itemKind = value.Data.ItemKind;
						int itemID = value.Data.ItemID;
						if (itemKind != (int)mGame.mPlayer.Data.CurrentSeries || itemID != mRoadBlockSetting.RoadBlockLevel)
						{
							continue;
						}
						MyFriend value2;
						mGame.mPlayer.Friends.TryGetValue(value.Data.FromID, out value2);
						if (value2 != null)
						{
							for (int l = 0; l < value.Data.Quantity; l++)
							{
								int num9 = 0;
								UISprite sprite3 = Util.CreateSprite("Icon", base.gameObject, image2);
								if (value.Data.GiftSource == GiftMailType.SUPPORT)
								{
									Util.SetSpriteInfo(sprite3, "icon_mailbox_acceptance00", mBaseDepth + 6, array[mFriendNum], scale, false);
								}
								else
								{
									num9 = value2.Data.IconID;
									string text = "icon_chara_facebook";
									if (num9 >= 10000)
									{
										num9 -= 10000;
									}
									if (num9 >= mGame.mCompanionData.Count)
									{
										num9 = 0;
									}
									CompanionData companionData = mGame.mCompanionData[num9];
									text = companionData.iconName;
									Util.SetSpriteInfo(sprite3, text, mBaseDepth + 6, array[mFriendNum], scale, false);
								}
								sprite3.SetDimensions(66, 66);
								mFriendNum++;
								if (mFriendNum >= num)
								{
									break;
								}
							}
						}
						if (mFriendNum < num)
						{
							continue;
						}
						break;
					}
				}
			}
			uILabel = Util.CreateLabel("FriendNum", uISprite6.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty + mFriendNum + "/" + mRoadBlockSetting.UnlockFriends, mBaseDepth + 5, new Vector3(0f, -2f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			if (mRoadBlockSetting.UnlockFriends > mFriendNum)
			{
				uILabel.color = new Color(r, 0.1f, 0.1f);
			}
			else
			{
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			}
			mGame.PlaySe("VOICE_006", -1);
		}
		CreateCancelButton("OnCancelPushed");
	}

	private void TutorialStartSub_Message(Def.TUTORIAL_INDEX index)
	{
	}

	public void OnTutorialFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
	}

	public override void OnOpening()
	{
		base.OnOpening();
		if (mL2DRender != null)
		{
			mL2DTexture = Util.CreateGameObject("Texture", base.gameObject).AddComponent<UITexture>();
			mL2DTexture.mainTexture = mL2DRender.RenderTexture;
			mL2DTexture.SetDimensions(568, 568);
			mL2DTexture.transform.localPosition = new Vector3(0f, 166f, 0f);
			mL2DTexture.depth = mBaseDepth + 5;
			mL2DTexture.transform.localScale = new Vector3(1.18f, 1.18f, 1f);
		}
	}

	public override void OnOpenFinished()
	{
		PlayerMapData currentMapData = mGame.mPlayer.CurrentMapData;
		int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
		if (roadBlockLevel < currentMapData.MaxLevel && !mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL31_2))
		{
			mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.MAP_LEVEL31_0, base.ParentGameObject, OnTutorialMessageFinished, OnTutorialMessageClosed);
		}
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE && selectItem != TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			TutorialStart(tUTORIAL_INDEX);
			return;
		}
		mGame.mTutorialManager.DeleteTutorialPointArrow();
		mGame.mTutorialManager.TutorialSkip(index);
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(tUTORIAL_INDEX);
		}
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.TutorialSkip(index);
		}
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		if (mL2DInstLuna != null)
		{
			UnityEngine.Object.Destroy(mL2DInstLuna.gameObject);
			mL2DInstLuna = null;
		}
		if (mL2DInstArtemis != null)
		{
			UnityEngine.Object.Destroy(mL2DInstArtemis.gameObject);
			mL2DInstArtemis = null;
		}
		if (mL2DRender != null)
		{
			UnityEngine.Object.Destroy(mL2DTexture.gameObject);
			UnityEngine.Object.Destroy(mL2DRender.gameObject);
			mL2DTexture = null;
			mL2DRender = null;
		}
		for (int i = 0; i < mPreLoadAnimationKeys.Count; i++)
		{
			ResourceManager.UnloadLive2DAnimation(mPreLoadAnimationKeys[i]);
		}
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		UnityEngine.Object.Destroy(base.gameObject);
		yield return null;
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnPurchaseDialogClosed()
	{
		UILabel component = GameObject.Find("Price").GetComponent<UILabel>();
		if (mRoadBlockSetting.UnlockSD > mGame.mPlayer.Dollar)
		{
			component.color = new Color(1f, 0.1f, 0.1f);
		}
		else
		{
			component.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		mState.Change(STATE.MAIN);
	}

	private void PurchaseAuthError()
	{
		mState.Change(STATE.AUTHERROR_WAIT);
	}

	public void OnSDUnlockPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			if (mRoadBlockSetting.UnlockSD > mGame.mPlayer.Dollar)
			{
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
				confirmDialog.Init(Localization.Get("NoMoreSD_Title"), Localization.Get("NoMoreSD_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.YES_NO);
				confirmDialog.SetClosedCallback(OnSDConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
			else
			{
				mBoughtItem = mGame.mShopItemData[mGame.mSegmentProfile.RoadBlockItem];
				GemUseConfirmDialog gemUseConfirmDialog = Util.CreateGameObject("GemUseConfirmDialog", base.transform.parent.gameObject).AddComponent<GemUseConfirmDialog>();
				gemUseConfirmDialog.Init(mRoadBlockSetting.UnlockSD, 11);
				gemUseConfirmDialog.SetClosedCallback(OnGemUseConfirmClosed);
				mState.Reset(STATE.WAIT, true);
			}
		}
	}

	public void OnGemUseConfirmClosed(GemUseConfirmDialog.SELECT_ITEM item, int aValue)
	{
		if (item == GemUseConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			mState.Change((STATE)aValue);
			return;
		}
		mBoughtItem = null;
		mState.Change(STATE.MAIN);
	}

	public void OnSDConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (item == ConfirmDialog.SELECT_ITEM.POSITIVE)
		{
			GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.RB_MAP;
			PurchaseDialog purchaseDialog = PurchaseDialog.CreatePurchaseDialog(base.transform.parent.gameObject);
			purchaseDialog.SetBaseDepth(mBaseDepth + 10);
			purchaseDialog.SetClosedCallback(OnPurchaseDialogClosed);
			purchaseDialog.SetAuthErrorClosedCallback(PurchaseAuthError);
		}
		else
		{
			mState.Change(STATE.MAIN);
		}
	}

	public void OnRBTUnlockPushed()
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mRoadBlockSetting.UnlockTicket > mGame.mPlayer.RoadBlockTicket)
		{
			ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
			confirmDialog.Init(Localization.Get("NoMoreRBT_Title"), Localization.Get("NoMoreRBT_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			confirmDialog.SetClosedCallback(OnRBTConfirmClosed);
			confirmDialog.SetBaseDepth(mBaseDepth + 10);
			mState.Reset(STATE.WAIT, true);
			return;
		}
		mGame.mPlayer.SubRoadBlockTicket(mRoadBlockSetting.UnlockTicket);
		int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
		int unlock_by = 3;
		int cur_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		int reach_time = 0;
		if (mGame.mPlayer.RoadBlockReachLevel == mRoadBlockSetting.RoadBlockLevel)
		{
			reach_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mGame.mPlayer.RoadBlockReachTime);
		}
		int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
		ServerCram.UnlockRoadblock(roadBlockLevel, unlock_by, cur_time, reach_time, currentSeries);
		mSelectItem = SELECT_ITEM.UNLOCK_TICKET;
		Close();
	}

	public void OnRBTConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mState.Change(STATE.MAIN);
	}

	public void OnFriendUnlockPushed()
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mRoadBlockSetting.UnlockFriends > mFriendNum)
		{
			ShowRoadBlockHelpDialog();
			return;
		}
		int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
		int unlock_by = 1;
		int cur_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		int reach_time = 0;
		if (mGame.mPlayer.RoadBlockReachLevel == mRoadBlockSetting.RoadBlockLevel)
		{
			reach_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mGame.mPlayer.RoadBlockReachTime);
		}
		int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
		ServerCram.UnlockRoadblock(roadBlockLevel, unlock_by, cur_time, reach_time, currentSeries);
		mSelectItem = SELECT_ITEM.UNLOCK_FRIEND;
		Close();
	}

	public void ShowRoadBlockHelpDialog()
	{
		if (mRoadBlockHelpDialog != null)
		{
			UnityEngine.Object.Destroy(mRoadBlockHelpDialog.gameObject);
			mRoadBlockHelpDialog = null;
		}
		int count = mGame.mPlayer.Friends.Count;
		if (count < mRoadBlockSetting.UnlockFriends)
		{
			mConfirmDialog = Util.CreateGameObject("ConfirmDialog", base.gameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("RB_FriendShortageError_Title"), string.Format(Localization.Get("RB_FriendShortageError_Desc"), mRoadBlockSetting.UnlockFriends), ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
		}
		else
		{
			mRoadBlockHelpDialog = Util.CreateGameObject("HelpDialog", base.ParentGameObject).AddComponent<RoadBlockHelpDialog>();
			mRoadBlockHelpDialog.Init(mRoadBlockSetting);
			mRoadBlockHelpDialog.SetClosedCallback(OnRoadBlockHelpDialogClosed);
		}
		mState.Reset(STATE.WAIT, true);
	}

	public void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(STATE.MAIN);
	}

	public void OnRoadBlockHelpDialogClosed()
	{
		mState.Change(STATE.MAIN);
	}

	public void OnNoKeyDialogClosed()
	{
		mState.Change(STATE.MAIN);
	}

	public void OnKeyUnlockPushed()
	{
		if (GetBusy() || mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		if (mRoadBlockSetting.UnlockStars > mKeyNum)
		{
			RoadBlockNoKeyDialog roadBlockNoKeyDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<RoadBlockNoKeyDialog>();
			roadBlockNoKeyDialog.SetClosedCallback(OnNoKeyDialogClosed);
			mState.Reset(STATE.WAIT, true);
			return;
		}
		int roadBlockLevel = mRoadBlockSetting.RoadBlockLevel;
		int unlock_by = 2;
		int cur_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Now);
		int reach_time = 0;
		if (mGame.mPlayer.RoadBlockReachLevel == mRoadBlockSetting.RoadBlockLevel)
		{
			reach_time = (int)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(mGame.mPlayer.RoadBlockReachTime);
		}
		int currentSeries = (int)mGame.mPlayer.Data.CurrentSeries;
		ServerCram.UnlockRoadblock(roadBlockLevel, unlock_by, cur_time, reach_time, currentSeries);
		mSelectItem = SELECT_ITEM.UNLOCK_KEY;
		Close();
	}

	public void OnStarConfirmClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mState.Change(STATE.MAIN);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && (mState.GetStatus() == STATE.MAIN || mState.GetStatus() == STATE.MAINTENANCE_CHECK_WAIT))
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void ShowNetworkErrorDialog(STATE aNextState)
	{
		mStateAfterNetworkError = aNextState;
		mState.Reset(STATE.WAIT, true);
		mErrorDialog = Util.CreateGameObject("StoreError", base.ParentGameObject).AddComponent<ConfirmDialog>();
		string desc = Localization.Get("Network_Error_Desc01") + Localization.Get("Network_ErrorTwo_Desc");
		mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		mErrorDialog.SetClosedCallback(OnNetworkErrorDialogClosed);
	}

	private void OnNetworkErrorDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mState.Change(mStateAfterNetworkError);
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}

	private void OnDuplicateDataErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mGame.GetGemCountFreqFlag = false;
		mState.Change(STATE.NETWORK_00);
	}
}
