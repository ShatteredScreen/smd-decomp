using System.Collections.Generic;
using UnityEngine;

public class MapSNSIcon : ScSpriteObject
{
	public delegate void OnPushed(GameObject go);

	private GameMain mGame;

	private BIJImage mAtlas;

	private Color[] mOrgColors;

	private Color? mColor;

	private Vector3 Position;

	private BoxCollider mBoxCollider;

	private OnPushed mCallback;

	public int UuID;

	private List<MapSNSSetting> mSettings;

	private List<SpriteRenderer> mSpriteRenderers;

	private UIEventTrigger mEventTrigger;

	private int mColliderSize = 100;

	private bool mIsStrayUser;

	public Color? color
	{
		get
		{
			return mColor;
		}
		set
		{
			SetColor(value);
		}
	}

	public override void Start()
	{
		base.Start();
		_sprite.Position = Position;
		string key = "MAP_SNS_ICON";
		if (mIsStrayUser)
		{
			key = "MAP_STRAY_ICON";
			mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
			mEventTrigger = base.gameObject.AddComponent<UIEventTrigger>();
			mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
			EventDelegate item = new EventDelegate(this, "OnClicked");
			mEventTrigger.onClick.Add(item);
		}
		ChangeAnime(key, true, 0);
	}

	public void UpdateIcon(List<MapSNSSetting> settings)
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mAtlas = ResourceManager.LoadImage("ICON").Image;
		mSettings = new List<MapSNSSetting>(settings);
		mSpriteRenderers = new List<SpriteRenderer>();
		string key = "MAP_SNS_ICON";
		ChangeAnime(key, true, 0);
	}

	public void Init(List<MapSNSSetting> settings, Vector3 pos)
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mAtlas = ResourceManager.LoadImage("ICON").Image;
		mSettings = new List<MapSNSSetting>(settings);
		mSpriteRenderers = new List<SpriteRenderer>();
		Position = pos;
	}

	public void Init(MapSNSSetting setting, Vector3 pos)
	{
		mIsStrayUser = true;
		Init(new List<MapSNSSetting> { setting }, pos);
	}

	public void TutorialInit(Vector3 pos)
	{
		Position = pos;
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		mAtlas = null;
		if (mSettings != null)
		{
			mSettings.Clear();
		}
		mSettings = null;
		for (int i = 0; i < mSpriteRenderers.Count; i++)
		{
			Object.Destroy(mSpriteRenderers[i].gameObject);
		}
		mSpriteRenderers.Clear();
		mSpriteRenderers = null;
	}

	public void SetColor(Color? col)
	{
		mColor = col;
		if (_sprite == null)
		{
			return;
		}
		if (col.HasValue)
		{
			Color? color = mColor;
			if (color.GetValueOrDefault() != col.GetValueOrDefault() || (color.HasValue ^ col.HasValue))
			{
				for (int i = 0; i < _sprite._colors.Length; i++)
				{
					_sprite._colors[i] = col.Value;
				}
				_sprite._colorChanged = true;
				_sprite.UpdateAlways();
			}
		}
		else if (mOrgColors != null && mOrgColors.Length == _sprite._colors.Length)
		{
			for (int j = 0; j < _sprite._colors.Length; j++)
			{
				_sprite._colors[j] = mOrgColors[j];
			}
			_sprite._colorChanged = true;
			_sprite.UpdateAlways();
		}
	}

	public bool ChangeAnime(string key, bool force, int playCount)
	{
		if (mAtlas == null)
		{
			return false;
		}
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(key);
		if (resSsAnimation == null)
		{
			_sprite.Animation = null;
			return true;
		}
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		SsPartRes[] partList = ssAnime.PartList;
		for (int i = 0; i < partList.Length; i++)
		{
			if (partList[i].IsRoot)
			{
				continue;
			}
			if ("lv".CompareTo(partList[i].Name) == 0)
			{
				string text = "lv_" + mSettings[0].IconLv;
				text = "null";
				Vector2[] uv;
				Vector3[] verts;
				GetTextureParts(text, partList[i].OriginX, partList[i].OriginY, out uv, out verts);
				partList[i].UVs = uv;
				partList[i].OrgVertices = verts;
				continue;
			}
			for (int j = 0; j < 3; j++)
			{
				string text2 = string.Format("icon{0:00}", j);
				string text3 = string.Format("frame{0:00}", j);
				Vector2[] uv2;
				Vector3[] verts2;
				if (j < mSettings.Count)
				{
					if (text2.CompareTo(partList[i].Name) == 0)
					{
						MapSNSSetting mapSNSSetting = mSettings[j];
						string text4 = "icon_chara_facebook";
						int num = -1;
						num = ((mapSNSSetting.IconID < 10000) ? mapSNSSetting.IconID : (mapSNSSetting.IconID - 10000));
						if (num >= mGame.mCompanionData.Count)
						{
							num = 0;
						}
						CompanionData companionData = mGame.mCompanionData[num];
						text4 = companionData.iconName;
						GetTextureParts(text4, partList[i].OriginX, partList[i].OriginY, out uv2, out verts2);
						partList[i].UVs = uv2;
						partList[i].OrgVertices = verts2;
					}
				}
				else
				{
					if (text3.CompareTo(partList[i].Name) == 0)
					{
						GetTextureParts("null", partList[i].OriginX, partList[i].OriginY, out uv2, out verts2);
						partList[i].UVs = uv2;
						partList[i].OrgVertices = verts2;
					}
					if (text2.CompareTo(partList[i].Name) == 0)
					{
						GetTextureParts("null", partList[i].OriginX, partList[i].OriginY, out uv2, out verts2);
						partList[i].UVs = uv2;
						partList[i].OrgVertices = verts2;
					}
				}
			}
		}
		CleanupAnimation();
		_sprite.Animation = ssAnime;
		if (mColor.HasValue)
		{
			mOrgColors = new Color[_sprite._colors.Length];
			for (int k = 0; k < _sprite._colors.Length; k++)
			{
				mOrgColors[k] = _sprite._colors[k];
				_sprite._colors[k] = mColor.Value;
			}
			_sprite._colorChanged = true;
		}
		_sprite.UpdateAlways();
		_sprite.Play();
		_sprite.PlayCount = playCount;
		_sprite.Pause();
		return true;
	}

	private void GetTextureParts(string changeName, int originX, int originY, out Vector2[] uv, out Vector3[] verts)
	{
		Vector2 offset = mAtlas.GetOffset(changeName);
		Vector2 size = mAtlas.GetSize(changeName);
		float x = offset.x;
		float y = offset.y;
		float x2 = offset.x + size.x;
		float y2 = offset.y + size.y;
		uv = new Vector2[4]
		{
			new Vector2(x, y2),
			new Vector2(x2, y2),
			new Vector2(x2, y),
			new Vector2(x, y)
		};
		Vector4 pixelPadding = mAtlas.GetPixelPadding(changeName);
		size = mAtlas.GetPixelSize(changeName);
		offset = new Vector2(originX, originY);
		offset.x += pixelPadding.x;
		offset.y += pixelPadding.y;
		size.x -= pixelPadding.x + pixelPadding.z;
		size.y -= pixelPadding.y + pixelPadding.w;
		x = 0f - offset.x;
		y = 0f - (size.y - offset.y);
		x2 = size.x - offset.x;
		y2 = offset.y;
		float z = 0f;
		verts = new Vector3[4]
		{
			new Vector3(x, y2, z),
			new Vector3(x2, y2, z),
			new Vector3(x2, y, z),
			new Vector3(x, y, z)
		};
	}

	public void SetEnable(bool a_flg)
	{
		if (mBoxCollider != null)
		{
			mBoxCollider.enabled = a_flg;
		}
	}

	public void OnClicked()
	{
		if (mCallback != null)
		{
			mCallback(base.gameObject);
		}
	}

	public void SetPushedCallback(OnPushed callback)
	{
		mCallback = callback;
	}
}
