using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShotDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3
	}

	public delegate void OnDialogClosed();

	private const string II_TITLE = "instruction_panel2";

	private const string II_BACKGROUND = "instruction_panel";

	private const float LOG_SSW = 480f;

	private const float LOG_SSH = 480f;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private Dictionary<GameObject, SNS> mButtons = new Dictionary<GameObject, SNS>();

	private Dictionary<GameObject, Vector3> mButtonsPos = new Dictionary<GameObject, Vector3>();

	private Dictionary<GameObject, Vector3> mButtonsLabelPos = new Dictionary<GameObject, Vector3>();

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mSNSCallCount;

	private UISprite mBackground;

	private UISprite mHeader;

	private UILabel mTitle;

	private UISprite mTitleBarL;

	private UISprite mTitleBarR;

	private UITexture mTex;

	private UIButton mButton;

	private Vector2 mTexScale = Vector2.one;

	private float mLandscapeScaleValue = 1f;

	public string imageUrl;

	private bool isImageSave;

	private bool isImageSaveFinish;

	private bool isImageSaveError;

	private bool isSizeCheck;

	private bool mPermissionCheckFinished;

	private bool mPermissionAllowed;

	public string Path { get; set; }

	public new void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
					}
				}
				catch (Exception)
				{
				}
				DoSNS(_sns);
			}, null);
			break;
		}
		}
		mState.Update();
	}

	private void CreateCharm(Vector3 topPos, string charmImage, BIJImage atlas)
	{
		UISprite uISprite = Util.CreateSprite("Chain", mBackground.gameObject, atlas);
		Util.SetSpriteInfo(uISprite, "instruction_accessory00", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
		uISprite.pivot = UIWidget.Pivot.Top;
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions(20, 32);
		uISprite.transform.localPosition = topPos;
		uISprite = Util.CreateSprite("Charm", mBackground.gameObject, atlas);
		Util.SetSpriteInfo(uISprite, charmImage, mBaseDepth + 2, topPos += new Vector3(0f, -30f), Vector3.one, false);
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		Vector2 vector = Util.LogScreenSize();
		float num = 0f;
		float num2 = 0f;
		float num3 = vector.y / 2f;
		mLandscapeScaleValue = 1f;
		if (Util.IsWideScreen())
		{
			num3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Top, Util.ScreenOrientation).y;
			float num4 = ((Screen.width <= Screen.height) ? ((float)Screen.width / (float)Screen.height) : ((float)Screen.height / (float)Screen.width));
			float num5 = 0.5625f;
			float num6 = 0.4617052f;
			mLandscapeScaleValue = 1f - (num5 - num4) / (num5 - num6) * 0.15f;
		}
		SetJellyTargetScale(1f);
		string text = Localization.Get("ScreenShot_Title");
		UISprite uISprite = Util.CreateSprite("BackGround", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, "bg00", mBaseDepth, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Tiled;
		uISprite.SetDimensions(1386, 1386);
		string imageName = "instruction_panel";
		uISprite = Util.CreateSprite("Background", base.gameObject, image);
		Util.SetSpriteInfo(uISprite, imageName, mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Advanced;
		uISprite.fillDirection = UIBasicSprite.FillDirection.Radial360;
		uISprite.SetDimensions((int)vector.x, (int)vector.y);
		mBackground = uISprite;
		uISprite.centerType = UIBasicSprite.AdvancedType.Tiled;
		uISprite.leftType = UIBasicSprite.AdvancedType.Tiled;
		uISprite.topType = UIBasicSprite.AdvancedType.Tiled;
		uISprite.rightType = UIBasicSprite.AdvancedType.Tiled;
		uISprite.bottomType = UIBasicSprite.AdvancedType.Tiled;
		mTitleBarR = Util.CreateSprite("StatusR", base.gameObject, image);
		Util.SetSpriteInfo(mTitleBarR, "menubar_frame", mBaseDepth + 2, new Vector3(-2f, num3 + 45f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mTitleBarL = Util.CreateSprite("StatusL", base.gameObject, image);
		Util.SetSpriteInfo(mTitleBarL, "menubar_frame", mBaseDepth + 2, new Vector3(2f, num3 + 45f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		UILabel uILabel = Util.CreateLabel("Title", mTitleBarR.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 3, Vector3.zero, 38, 0, 0, UIWidget.Pivot.Center);
		Util.SetLabelText(uILabel, text);
		uILabel.gameObject.transform.localPosition = new Vector3(2f, -201f, 0f);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.color = Color.white;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(370, 38);
		uILabel.fontSize = 38;
		mTitle = uILabel;
		Texture2D texture2D = new Texture2D(0, 0, TextureFormat.ARGB32, false);
		using (BIJBinaryReader bIJBinaryReader = new BIJFileBinaryReader(Path))
		{
			texture2D.LoadImage(bIJBinaryReader.ReadAll());
		}
		float num7 = texture2D.width;
		float num8 = texture2D.height;
		float num9 = 446f;
		float num10 = 446f;
		float num11 = Mathf.Min(num9 / num7, num10 / num8);
		float x = num7 * num11;
		float num12 = num8 * num11;
		num = num - num2 / 2f - num12 / 2f - 70f;
		mTex = Util.CreateGameObject("ScreenShot", base.gameObject).AddComponent<UITexture>();
		mTex.pivot = UIWidget.Pivot.Center;
		mTex.depth = mBaseDepth + 6;
		mTex.mainTexture = texture2D;
		mTex.transform.localPosition = new Vector3(0f, num, 0f);
		mTex.transform.localScale = new Vector3(x, num12, 1f);
		mTexScale = new Vector2(x, num12);
		UISprite uISprite2 = Util.CreateSprite("MainBoard", mTex.gameObject, image);
		Util.SetSpriteInfo(uISprite2, "instruction_panel12", mBaseDepth + 4, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite2.SetDimensions(500, 480);
		uISprite2.type = UIBasicSprite.Type.Sliced;
		UISprite uISprite3 = Util.CreateSprite("BoosterBoardIn", mTex.gameObject, image);
		Util.SetSpriteInfo(uISprite3, "instruction_panel14", mBaseDepth + 5, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite3.SetDimensions(490, 470);
		uISprite3.type = UIBasicSprite.Type.Sliced;
		uISprite = Util.CreateSprite("Accessory9Top", mTex.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_accessory09", mBaseDepth + 3, new Vector3(0f, 250f, 0f), Vector3.one, false);
		uISprite = Util.CreateSprite("Accessory9Down", mTex.gameObject, image);
		Util.SetSpriteInfo(uISprite, "instruction_accessory09", mBaseDepth + 3, new Vector3(0f, -250f, 0f), Vector3.one, false);
		uISprite.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);
		text = string.Format(Localization.Get("ScreenShot_Desc"));
		uILabel = Util.CreateLabel("Desc", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(0f, -70f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		mButtons.Clear();
		mButtonsPos.Clear();
		mButtonsLabelPos.Clear();
		int num13 = 0;
		Vector3 zero = Vector3.zero;
		SNS[] array = new SNS[7]
		{
			SNS.Facebook,
			SNS.GooglePlus,
			SNS.GameCenter,
			SNS.LINE,
			SNS.Twitter,
			SNS.Instagram,
			SNS.Mail
		};
		Dictionary<SNS, bool> dictionary = new Dictionary<SNS, bool>();
		int num14 = 0;
		SNS[] array2 = array;
		foreach (SNS sNS in array2)
		{
			bool flag = GameMain.IsSNSFunctionEnabled(SNS_FLAG.Screenshot, sNS) && SocialManager.Instance[sNS].IsEnabled() && SocialManager.Instance[sNS].IsOpEnabled(9);
			switch (sNS)
			{
			case SNS.Facebook:
				flag = true && flag;
				break;
			case SNS.GooglePlus:
				flag = true && flag;
				break;
			case SNS.Twitter:
				flag = true && flag;
				break;
			case SNS.Instagram:
				flag = true && flag;
				break;
			}
			dictionary[sNS] = flag;
			if (flag)
			{
				num14++;
			}
		}
		float num15 = -180f;
		float num16 = -160f;
		float num17 = -70f;
		int num18 = 3;
		float num19 = -150f;
		float num20 = 150f;
		float num21 = 0f;
		foreach (KeyValuePair<SNS, bool> item in dictionary)
		{
			SNS key = item.Key;
			if (item.Value)
			{
				string text2;
				switch (key)
				{
				case SNS.Facebook:
					text2 = "icon_sns00";
					break;
				case SNS.GooglePlus:
					text2 = "icon_sns05";
					break;
				case SNS.LINE:
					text2 = "icon_sns02";
					break;
				case SNS.Twitter:
					text2 = "icon_sns03";
					break;
				case SNS.Instagram:
					text2 = "icon_sns01";
					break;
				case SNS.Mail:
					text2 = "icon_sns04";
					break;
				default:
					continue;
				}
				int num22 = num13 % num18;
				int num23 = num13 / num18;
				zero.x = num19 + (float)num22 * num20 + ((num23 != num14 / num18) ? 0f : num21);
				zero.y = num15 + (float)num23 * num16;
				num13++;
				UIButton uIButton = Util.CreateJellyImageButton(string.Concat(string.Empty, key, "Button"), base.gameObject, image);
				Util.SetImageButtonInfo(uIButton, text2, text2, text2, mBaseDepth + 3, zero, Vector3.one);
				Util.AddImageButtonMessage(uIButton, this, "OnSNSButtonPushed", UIButtonMessage.Trigger.OnClick);
				mButtons[uIButton.gameObject] = key;
				mButtonsPos[uIButton.gameObject] = new Vector3(zero.x - num19, zero.y - num15, 0f);
				zero.y += num17;
				text = string.Format(Localization.Get("ScreenShot_By_" + key));
				uILabel = Util.CreateLabel(string.Concat(string.Empty, key, "Label"), uIButton.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, new Vector3(0f, -70f, 0f), 16, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
				uILabel.overflowMethod = UILabel.Overflow.ResizeHeight;
				uILabel.SetDimensions(100, 32);
			}
		}
		float y = -490f;
		mButton = Util.CreateJellyImageButton("CloseButton", base.gameObject, image);
		Util.SetImageButtonInfo(mButton, "button_back", "button_back", "button_back", mBaseDepth + 4, new Vector3(0f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(mButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
		BoxCollider boxCollider = base.gameObject.AddComponent<BoxCollider>();
		boxCollider.center = new Vector3(0f, 0f, 0f);
		boxCollider.size = new Vector3(vector.x, vector.y, 1f);
		SetLayout(Util.ScreenOrientation);
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		float start_x = 0f;
		float start_y = 0f;
		float scaling = 1f;
		float num = vector.y / 2f;
		float num2 = (0f - vector.y) / 2f;
		float num3 = (0f - vector.x) / 2f;
		if (Util.IsWideScreen())
		{
			Vector2 vector2 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.TopLeft, o);
			Vector2 vector3 = UIChildAnchor.CalculateLocalPos(mGame.mRoot.manualHeight, mGame.mRoot.manualWidth, UIAnchor.Side.Bottom, o);
			num = vector2.y;
			num2 = vector3.y;
			num3 = vector2.x;
		}
		float num4 = mTex.mainTexture.width;
		float num5 = mTex.mainTexture.height;
		float num6 = 446f;
		float num7 = 446f;
		float num8 = Mathf.Min(num6 / num4, num7 / num5);
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			UISprite uISprite = mBackground;
			uISprite.SetDimensions(620, 920);
			uISprite.transform.localPosition = new Vector3(0f, 0f);
			mTex.transform.localPosition = new Vector3(0f, 170f, 0f);
			mTex.transform.SetLocalScale(1f, 1f, 1f);
			mTex.SetDimensions((int)mTexScale.x, (int)mTexScale.y);
			mButton.transform.localPosition = new Vector3(num3 + 55f, num2 + 55f, 0f);
			start_x = -150f;
			start_y = -180f;
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			UISprite uISprite = mBackground;
			uISprite.SetDimensions((int)(920f * mLandscapeScaleValue), (int)(556f * mLandscapeScaleValue));
			uISprite.transform.localPosition = new Vector3(0f, -15f);
			mTex.transform.localPosition = new Vector3(-170f * mLandscapeScaleValue, -15f, 0f);
			mTex.transform.SetLocalScale(mLandscapeScaleValue, mLandscapeScaleValue, 1f);
			mTex.SetDimensions((int)mTexScale.x, (int)mTexScale.y);
			mButton.transform.localPosition = new Vector3(num3 + 60f, num2 + 76f, 0f);
			start_x = 150f;
			start_y = 180f;
			scaling = mLandscapeScaleValue;
			break;
		}
		}
		mTitleBarR.gameObject.transform.localPosition = new Vector3(-2f, num + 45f + 123f, 0f);
		mTitleBarL.gameObject.transform.localPosition = new Vector3(2f, num + 45f + 123f, 0f);
		LayoutSNSButtons(start_x, start_y, scaling);
	}

	private void LayoutSNSButtons(float start_x, float start_y, float scaling)
	{
		int num = 0;
		int num2 = 0;
		foreach (KeyValuePair<GameObject, Vector3> mButtonsPo in mButtonsPos)
		{
			GameObject key = mButtonsPo.Key;
			Vector3 value = mButtonsPo.Value;
			if (key == null)
			{
				continue;
			}
			float z = key.transform.localPosition.z;
			if (start_x < 0f)
			{
				key.transform.localPosition = new Vector3(start_x + value.x, start_y + value.y, z);
				continue;
			}
			key.transform.localPosition = new Vector3((float)(200 + num * 155) * scaling, (float)(160 + num2 * -155) * scaling, z);
			num++;
			if (num == 2)
			{
				num2++;
			}
			num %= 2;
		}
	}

	public void OnSNSButtonPushed(GameObject go)
	{
		if (GetBusy() || mState.GetStatus() != 0)
		{
			return;
		}
		SNS value = SNS.None;
		if (!mButtons.TryGetValue(go, out value))
		{
			return;
		}
		BIJSNS _sns = SocialManager.Instance[value];
		if (_sns == null)
		{
			return;
		}
		bool flag = false;
		if (value == SNS.Twitter || value == SNS.Facebook)
		{
			flag = true;
		}
		if (flag)
		{
			PushSaveImage(delegate
			{
				mSNSCallCount = 0;
				DoSNS(_sns);
			}, delegate
			{
				BIJLog.E("image save filed");
				mSNSCallCount = 0;
				mState.Reset(STATE.WAIT);
				ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.gameObject).AddComponent<ConfirmDialog>();
				string desc = string.Format(Localization.Get("ScreenShotError_Desc"));
				confirmDialog.Init(Localization.Get("ScreenShotError_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
				confirmDialog.SetClosedCallback(delegate
				{
					mState.Change(STATE.MAIN);
				});
			});
		}
		else
		{
			mSNSCallCount = 0;
			DoSNS(_sns);
		}
	}

	private IBIJSNSPostData GetPostData(BIJSNS _sns)
	{
		BIJSNSPostData bIJSNSPostData = new BIJSNSPostData();
		string text = ((!string.IsNullOrEmpty(Constants.POST_URL)) ? ("\n" + Constants.POST_URL) : string.Empty);
		string pOST_CODE = Constants.POST_CODE;
		string fbName = mGame.mOptions.FbName;
		string empty = string.Empty;
		string path = Path;
		string fileName = "SnoopyDrops.png";
		switch (_sns.Kind)
		{
		case SNS.Facebook:
		{
			string linkURL2 = string.Format(Constants.POST_URL, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = "file://" + path;
			bIJSNSPostData.Title = string.Format(Localization.Get("ScreenShot_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("ScreenShot_Contents_Desc"), string.Empty, string.Empty);
			bIJSNSPostData.LinkURL = linkURL2;
			bIJSNSPostData.FileName = fileName;
			break;
		}
		case SNS.GooglePlus:
		{
			string linkURL = "\n" + string.Format(Constants.POST_URL, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = "file://" + path;
			bIJSNSPostData.Title = string.Format(Localization.Get("ScreenShot_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("ScreenShot_Contents_Desc"), string.Empty, string.Empty);
			bIJSNSPostData.LinkURL = linkURL;
			bIJSNSPostData.FileName = fileName;
			break;
		}
		default:
		{
			string arg = "\n" + string.Format(Constants.STORE_URL_IOS, BIJUnity.getCountryCode());
			bIJSNSPostData.ImageURL = path;
			bIJSNSPostData.Title = string.Format(Localization.Get("ScreenShot_Contents_Title"));
			bIJSNSPostData.Message = string.Format(Localization.Get("ScreenShot_Contents_Desc"), arg, "\n" + Constants.STORE_URL_AD);
			bIJSNSPostData.FileName = fileName;
			break;
		}
		}
		return bIJSNSPostData;
	}

	private void DoSNS(BIJSNS _sns)
	{
		mSNSCallCount++;
		try
		{
			mState.Reset(STATE.WAIT, true);
			mSNS = _sns;
			IBIJSNSPostData postData = GetPostData(_sns);
			_sns.UploadImage(postData, delegate(BIJSNS.Response _res, object _userdata)
			{
				if (_res != null && _res.result == 0)
				{
					if (SNSSuccess(_sns))
					{
						return;
					}
				}
				else
				{
					switch (_sns.Kind)
					{
					case SNS.Facebook:
						Application.OpenURL(mGame.GameProfile.FacebookExternalPostURL);
						if (SNSSuccess(_sns))
						{
							return;
						}
						break;
					case SNS.Twitter:
						Application.OpenURL(mGame.GameProfile.TwitterExternalPostURL);
						if (SNSSuccess(_sns))
						{
							return;
						}
						break;
					default:
						if (SNSError(_sns))
						{
							return;
						}
						break;
					}
				}
				mState.Change(STATE.MAIN);
			}, null);
		}
		catch (Exception)
		{
			mState.Change(STATE.MAIN);
		}
	}

	private bool SNSSuccess(BIJSNS _sns)
	{
		mState.Reset(STATE.WAIT, true);
		mGame.AchievementFriendPost(true);
		OnSNSSuccessDialogClosed(ConfirmDialog.SELECT_ITEM.POSITIVE);
		return true;
	}

	private void OnSNSSuccessDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		Close();
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Reset(STATE.WAIT, true);
		GameObject parent = base.gameObject.transform.parent.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("ScreenShot_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("ScreenShot_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(Closing());
	}

	private IEnumerator Closing()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		yield return 0;
		Texture _tex2 = null;
		if (mTex != null && mTex.mainTexture != null)
		{
			_tex2 = mTex.mainTexture;
			UnityEngine.Object.Destroy(mTex.mainTexture);
			mTex.mainTexture = null;
		}
		yield return Resources.UnloadUnusedAssets();
		if (mCallback != null)
		{
			mCallback();
		}
	}

	public override void Close()
	{
		if (mConfirmDialog != null)
		{
			mConfirmDialog.Close();
		}
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void PushSaveImage(Action _finishCallback, Action _failCallback)
	{
		if (!isImageSave)
		{
			StartCoroutine(CheckIOSPermission(delegate
			{
				StartCoroutine(SaveImage(_finishCallback, _failCallback));
			}, delegate
			{
				_failCallback();
			}));
		}
	}

	public IEnumerator SaveImage(Action _finishCallback, Action _failCallback, bool _dialogwait = false)
	{
		isImageSaveFinish = false;
		isImageSaveError = false;
		isImageSave = true;
		imageUrl = Path;
		if (!BIJUnity.writeToPhotoAlbumAsync(imageUrl, base.gameObject, "SaveImageResult"))
		{
			isImageSaveError = true;
		}
		else
		{
			while (!isImageSaveFinish)
			{
				yield return 0;
			}
		}
		if (isImageSaveError)
		{
			mPermissionCheckFinished = false;
			mPermissionAllowed = false;
			BIJUnity.checkPermissionPhoto(base.gameObject, "OnRequestIOSPermissionResult");
			while (!mPermissionCheckFinished)
			{
				yield return null;
			}
			if (!mPermissionAllowed)
			{
				isImageSave = false;
				if (_failCallback != null)
				{
					_failCallback();
				}
			}
			else
			{
				isImageSave = false;
				if (_failCallback != null)
				{
					_failCallback();
				}
			}
		}
		else
		{
			isImageSave = false;
			if (_finishCallback != null)
			{
				_finishCallback();
			}
		}
	}

	public void SaveImageResult(string message)
	{
		isImageSaveFinish = true;
		if (string.IsNullOrEmpty(message))
		{
			isImageSaveError = false;
			return;
		}
		isImageSaveError = true;
		UnityEngine.Debug.LogError("ScreenShotStart : メッセージ開始\n" + message + "\nメッセージここまで");
	}

	private IEnumerator CheckIOSPermission(Action _OkCallback, Action _failedCallback)
	{
		mPermissionCheckFinished = false;
		mPermissionAllowed = false;
		BIJUnity.checkPermissionPhoto(base.gameObject, "OnRequestIOSPermissionResult");
		while (!mPermissionCheckFinished)
		{
			yield return null;
		}
		if (mPermissionAllowed)
		{
			if (_OkCallback != null)
			{
				_OkCallback();
			}
		}
		else if (_failedCallback != null)
		{
			_failedCallback();
		}
	}

	public void OnRequestIOSPermissionResult(string result)
	{
		if (result == "true")
		{
			mPermissionAllowed = true;
		}
		else
		{
			mPermissionAllowed = false;
		}
		mPermissionCheckFinished = true;
	}
}
