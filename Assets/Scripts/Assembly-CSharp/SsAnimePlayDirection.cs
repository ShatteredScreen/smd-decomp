public enum SsAnimePlayDirection
{
	Forward = 0,
	Reverse = 1,
	RoundTrip = 2,
	ReverseRoundTrip = 3
}
