using UnityEngine;

public class AdvIconInstance : MonoBehaviour
{
	public enum DISPLAY_UNDER
	{
		NONE = 0,
		LEVEL = 1,
		HP = 2,
		ATK = 3,
		DEF = 4,
		RARE = 5,
		SKILL_LEVEL = 6
	}

	public static AdvIconInstance Make(GameObject _parent, string _objname, AdvCharacter _character, Vector3 _pos, int _depth, bool _drare = false, bool _dskilllv = false, DISPLAY_UNDER _dumode = DISPLAY_UNDER.NONE, bool _draggable = false)
	{
		GameObject gameObject = Util.CreateGameObject(_objname, _parent);
		return gameObject.AddComponent<AdvIconInstance>();
	}

	public static AdvIconInstance MakeWorld(GameObject _parent, string _objname, AdvCharacter _character, Vector3 _wpos, Vector3 _offsetpos, int _depth, bool _drare = false, bool _dskilllv = false, DISPLAY_UNDER _dumode = DISPLAY_UNDER.NONE, bool _draggable = false)
	{
		GameObject gameObject = Util.CreateGameObject(_objname, _parent);
		return gameObject.AddComponent<AdvIconInstance>();
	}

	public static AdvIconInstance Make(GameObject _parent, string _objname, AdvCharacterData _character, Vector3 _pos, int _depth, bool _drare = false, bool _dskilllv = false, DISPLAY_UNDER _dumode = DISPLAY_UNDER.NONE, bool _draggable = false)
	{
		GameObject gameObject = Util.CreateGameObject(_objname, _parent);
		return gameObject.AddComponent<AdvIconInstance>();
	}

	public static AdvIconInstance MakeWorld(GameObject _parent, string _objname, AdvCharacterData _character, Vector3 _wpos, Vector3 _offsetpos, int _depth, bool _drare = false, bool _dskilllv = false, DISPLAY_UNDER _dumode = DISPLAY_UNDER.NONE, bool _draggable = false)
	{
		GameObject gameObject = Util.CreateGameObject(_objname, _parent);
		return gameObject.AddComponent<AdvIconInstance>();
	}

	protected virtual void Awake()
	{
	}

	protected virtual void Start()
	{
	}

	protected virtual void OnDestroy()
	{
	}

	public virtual void Update()
	{
	}

	protected virtual void OnLoadImage()
	{
	}

	protected virtual void BuildIcon()
	{
	}

	protected virtual void EraseAttribute()
	{
	}
}
