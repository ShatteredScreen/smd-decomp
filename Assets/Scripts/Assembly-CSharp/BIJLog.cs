using System;
using System.Diagnostics;
using System.Text;
using UnityEngine;

public class BIJLog
{
	public enum Level
	{
		ERROR = 0,
		WARNING = 1,
		INFO = 2,
		DEBUG = 3,
		VERBOSE = 4
	}

	private static BIJLogManager mLogManagerInstance = null;

	private static object mInstanceLock = new object();

	private static BIJLog mInstance;

	private Level mLogLevel;

	private static StringBuilder mDebugStringBuilder = new StringBuilder();

	public static BIJLog Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					mInstance = new BIJLog();
				}
				return mInstance;
			}
		}
	}

	public Level LogLevel
	{
		get
		{
			lock (mInstanceLock)
			{
				return mLogLevel;
			}
		}
		set
		{
			lock (mInstanceLock)
			{
				mLogLevel = value;
			}
		}
	}

	private BIJLog()
	{
		if (UnityEngine.Debug.isDebugBuild)
		{
			mLogLevel = Level.VERBOSE;
		}
		else
		{
			mLogLevel = Level.INFO;
		}
	}

	public void SetLogManager(BIJLogManager a_manager)
	{
		mLogManagerInstance = a_manager;
	}

	public static bool IsEanbled(Level level)
	{
		bool flag = false;
		lock (mInstanceLock)
		{
			return level <= Instance.LogLevel;
		}
	}

	public void Output(Level level, object msg, Exception e)
	{
		if (IsEanbled(level))
		{
			StringBuilder stringBuilder = new StringBuilder((msg != null) ? msg.ToString() : string.Empty);
			if (e != null)
			{
				stringBuilder.AppendLine(string.Empty);
				stringBuilder.Append(e.Message);
				stringBuilder.AppendLine(string.Empty);
				stringBuilder.Append(e.StackTrace);
			}
			switch (level)
			{
			case Level.ERROR:
				UnityEngine.Debug.LogError(stringBuilder);
				break;
			case Level.WARNING:
				UnityEngine.Debug.LogWarning(stringBuilder);
				break;
			case Level.INFO:
				UnityEngine.Debug.Log(stringBuilder);
				break;
			default:
				UnityEngine.Debug.Log(stringBuilder);
				break;
			}
			if (mLogManagerInstance != null)
			{
				mLogManagerInstance.Entry(stringBuilder.ToString(), level.ToString() + ":\t");
			}
		}
	}

	public static void E(object msg)
	{
		E(msg, null);
	}

	public static void E(object msg, Exception e)
	{
		Instance.Output(Level.ERROR, msg, e);
	}

	[Conditional("BIJ_DEBUG")]
	public static void W(object msg)
	{
	}

	[Conditional("BIJ_DEBUG")]
	public static void W(object msg, Exception e)
	{
		Instance.Output(Level.WARNING, msg, e);
	}

	[Conditional("BIJ_DEBUG")]
	public static void I(object msg)
	{
	}

	[Conditional("BIJ_DEBUG")]
	public static void I(object msg, Exception e)
	{
		Instance.Output(Level.INFO, msg, e);
	}

	[Conditional("BIJ_DEBUG")]
	public static void D(object msg)
	{
	}

	[Conditional("BIJ_DEBUG")]
	public static void D(object msg, Exception e)
	{
		Instance.Output(Level.DEBUG, msg, e);
	}

	[Conditional("BIJ_DEBUG")]
	public static void V(object msg)
	{
	}

	[Conditional("BIJ_DEBUG")]
	public static void V(object msg, Exception e)
	{
		Instance.Output(Level.VERBOSE, msg, e);
	}

	[Conditional("BIJ_DEBUG")]
	public static void AppendDebug(object msg)
	{
		if (IsEanbled(Level.DEBUG))
		{
			mDebugStringBuilder.AppendLine(msg.ToString());
		}
	}

	[Conditional("BIJ_DEBUG")]
	public static void PublishDebug()
	{
		if (IsEanbled(Level.DEBUG))
		{
			mDebugStringBuilder.Remove(0, mDebugStringBuilder.Length);
		}
	}
}
