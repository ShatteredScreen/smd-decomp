using System;

public class CpTimeSettings : ICloneable
{
	[MiniJSONAlias("start")]
	public long StartTime { get; set; }

	[MiniJSONAlias("end")]
	public long EndTime { get; set; }

	[MiniJSONAlias("point")]
	public float Point { get; set; }

	[MiniJSONAlias("probability")]
	public float Probability { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public SpCharaSettings Clone()
	{
		return MemberwiseClone() as SpCharaSettings;
	}
}
