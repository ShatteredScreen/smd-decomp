public class ConnectGetSupportParam : ConnectParameterBase
{
	public int mCustomReceiveCount = -1;

	public ConnectGetSupportParam(int a_count)
	{
		mCustomReceiveCount = a_count;
	}
}
