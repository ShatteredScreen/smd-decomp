using UnityEngine;

public class PlayDataLabel
{
	private GameObject Root;

	private UILabel Title;

	private UILabel Score;

	public PlayDataLabel()
	{
		Root = null;
		Title = null;
		Score = null;
	}

	public PlayDataLabel(GameObject parent, UIFont font, string ObjName, string TitleLocName, string ScoreLocName, long Value1, long Value2, long Value3, ref float posY, float pitchY, int depth, Color clr, int shrink = 210)
	{
		Root = Util.CreateGameObject(ObjName, parent);
		Root.transform.localPosition = new Vector3(0f, posY, 0f);
		Title = Util.CreateLabel("Title", Root.gameObject, font);
		Util.SetLabelInfo(Title, Localization.Get(TitleLocName), depth, new Vector3(-235f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Left);
		Title.overflowMethod = UILabel.Overflow.ShrinkContent;
		Title.color = clr;
		Title.SetDimensions(shrink, (int)pitchY);
		Score = Util.CreateLabel("Score", Root.gameObject, font);
		Util.SetLabelInfo(Score, string.Format(Localization.Get(ScoreLocName), Value1, Value2, Value3), depth, new Vector3(235f, 0f, 0f), 22, 0, 0, UIWidget.Pivot.Right);
		Score.overflowMethod = UILabel.Overflow.ShrinkContent;
		Score.color = clr;
		Score.SetDimensions(260, (int)pitchY);
		posY -= pitchY;
	}

	public bool GetScoreLabel(ref UILabel label)
	{
		if (Score == null)
		{
			return false;
		}
		label = Score;
		return true;
	}

	public bool GetRootObject(ref GameObject obj)
	{
		if (Root == null)
		{
			return false;
		}
		obj = Root;
		return true;
	}
}
