using System.Collections.Generic;
using UnityEngine;

public class MapSign : MapEntity
{
	public STATE mStateStatus;

	protected new float mStateTime;

	protected int mMapIndex;

	protected bool mFirstInitialized;

	protected bool mBookedCallback;

	protected Def.SERIES mSeries;

	protected bool mIsUnlockAnimeFinished;

	protected OneShotAnime mOneShotAnime;

	public bool IsUnlockAnimeFinished
	{
		get
		{
			return mIsUnlockAnimeFinished;
		}
	}

	public override void Awake()
	{
		base.Awake();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		mStateStatus = mState.GetStatus();
		switch (mStateStatus)
		{
		case STATE.ON:
			if (mIsUnlockAnimeFinished)
			{
				PlayAnime(DefaultAnimeKey + "_UNLOCK_IDLE", true);
				if (mOneShotAnime != null)
				{
					Object.Destroy(mOneShotAnime.gameObject);
					mOneShotAnime = null;
				}
				mState.Change(STATE.STAY);
			}
			break;
		case STATE.WAIT:
			if (_sprite.IsAnimationFinished() || mForceAnimeFinished)
			{
				mForceAnimeFinished = false;
				mState.Change(STATE.STAY);
			}
			break;
		}
		mState.Update();
	}

	public virtual void Init(int _counter, string _name, BIJImage atlas, float x, float y, Vector3 scale, int a_mapIndex, string a_animeKey)
	{
		mSeries = Def.SERIES.SM_FIRST;
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mAtlas = atlas;
		mScale = scale;
		float mAP_SIGN_Z = Def.MAP_SIGN_Z;
		base._pos = new Vector3(x, y, mAP_SIGN_Z);
		bool flag = true;
		Player mPlayer = mGame.mPlayer;
		if (flag)
		{
			IsLockTapAnime = false;
			PlayAnime(AnimeKey + "_LOCK", true);
		}
		else
		{
			PlayAnime(AnimeKey + "_UNLOCK_IDLE", true);
		}
		HasTapAnime = false;
		mFirstInitialized = true;
	}

	public void UnlockAnime()
	{
		AnimeKey = DefaultAnimeKey;
		mChangeParts = new Dictionary<string, string>();
		HasTapAnime = true;
		mState.Change(STATE.ON);
		mIsUnlockAnimeFinished = false;
		mOneShotAnime = Util.CreateOneShotAnime("LineUnlock", AnimeKey + "_UNLOCK", base.gameObject, Vector3.zero, Vector3.one, 0f, 0f, false, OnUnlockAnimationFinished);
	}

	public void SetEnable(bool a_flg)
	{
	}

	public void OnUnlockAnimationFinished(SsSprite sprite)
	{
		mIsUnlockAnimeFinished = true;
	}
}
