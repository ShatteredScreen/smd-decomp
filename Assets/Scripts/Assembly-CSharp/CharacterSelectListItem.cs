public class CharacterSelectListItem : SMVerticalListItem
{
	public bool enable;

	public CompanionData data;

	public int accessoryId = -1;

	public bool showAttention;

	public byte category = byte.MaxValue;

	public static int CompareByCategoryAndOrder(CharacterSelectListItem a, CharacterSelectListItem b)
	{
		if (a.data.sortCategory == b.data.sortCategory)
		{
			return a.data.sortOder - b.data.sortOder;
		}
		return a.data.sortCategory - b.data.sortCategory;
	}
}
