public class GetGameProfile : ParameterObject<GetGameProfile>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("datekey")]
	public long DateKey { get; set; }

	[MiniJSONAlias("debugkey")]
	public string DebugKey { get; set; }
}
