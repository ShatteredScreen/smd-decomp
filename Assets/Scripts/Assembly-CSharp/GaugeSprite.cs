using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class GaugeSprite : Disposable
{
	public enum CALLBACK_TYPE
	{
		OnAcquireAllReward = 0,
		OnDispose = 1
	}

	public sealed class Config : Disposable
	{
		public UISprite Frame { get; set; }

		public UISprite Body { get; set; }

		public GameObject Icon { get; set; }

		public UILabel Label { get; set; }

		public UISprite Complete { get; set; }

		public int MaxHorizontalDimension { get; set; }

		public int MinHorizontalDimension { get; set; }

		public bool IsShowNonNextReward { get; set; }

		public IntVector2 IconSize { get; set; }

		public int IconDepth { get; set; }

		public string LocalizeKey { get; set; }

		public string AchieveSeKey { get; set; }

		public BIJImage CompleteIconAtlas { get; set; }

		public string CompleteIconKey { get; set; }

		public Config()
		{
			IconSize = new IntVector2(42, 42);
			LocalizeKey = "Labyrinth_Num_Slash";
			base.ManagedDisposeEvent = delegate
			{
				if ((bool)Label)
				{
					GameMain.SafeDestroy(Label.gameObject);
					Label = null;
				}
				if ((bool)Icon)
				{
					GameMain.SafeDestroy(Icon);
					Icon = null;
				}
				if ((bool)Body)
				{
					GameMain.SafeDestroy(Body.gameObject);
					Body = null;
				}
				if ((bool)Frame)
				{
					GameMain.SafeDestroy(Frame.gameObject);
					Frame = null;
				}
				if ((bool)Complete)
				{
					GameMain.SafeDestroy(Complete.gameObject);
					Complete = null;
				}
			};
		}

		public void Show(bool flag)
		{
			if ((bool)Frame)
			{
				Frame.enabled = flag;
			}
			if ((bool)Body)
			{
				Body.enabled = flag;
			}
			if ((bool)Icon && Icon.activeSelf != flag)
			{
				Icon.SetActive(flag);
			}
			if ((bool)Label)
			{
				Label.enabled = flag;
			}
			if ((bool)Complete)
			{
				Complete.enabled = flag;
			}
		}
	}

	private sealed class CallbackComparer : IEqualityComparer<CALLBACK_TYPE>
	{
		public bool Equals(CALLBACK_TYPE x, CALLBACK_TYPE y)
		{
			return x == y;
		}

		public int GetHashCode(CALLBACK_TYPE arg)
		{
			return (int)arg;
		}
	}

	private const float ANIM_90_DEGREE_UP = 1.5708f;

	private const float ANIM_90_DEGREE_DOWN = -1.5708f;

	private bool mDisposed;

	private Dictionary<int, SMEventRewardSetting> mReward;

	private Config mConfig;

	private UISprite mIcon;

	private AdvIconInstance mAdvIcon;

	private float mNextValue;

	private UISsSprite mGaugeEffect;

	private Coroutine mCoroutine;

	private int mCurrentValue;

	private string mSelectedAtlasName;

	private Dictionary<CALLBACK_TYPE, UnityEvent> mEvent = new Dictionary<CALLBACK_TYPE, UnityEvent>(new CallbackComparer());

	private bool mIsFirstSetValue = true;

	private bool mIsExecCompleteAnimation;

	public int Value { get; private set; }

	public bool IsInit { get; private set; }

	public bool CanExec { get; private set; }

	public bool IsAcquiredAllReward { get; private set; }

	public GaugeSprite(Config config, params KeyValuePair<int, SMEventRewardSetting>[] reward)
	{
		IsInit = true;
		CanExec = false;
		IsAcquiredAllReward = false;
		if (config == null || reward == null || reward.Length <= 0)
		{
			return;
		}
		if (config != null)
		{
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			mIcon = Util.CreateSprite("Sprite", config.Icon, image);
			Util.SetSpriteInfo(mIcon, "null", config.IconDepth, Vector3.zero, Vector3.one, false);
			mIcon.SetDimensions(config.IconSize.x, config.IconSize.y);
			if ((bool)config.Complete)
			{
				config.Complete.enabled = false;
			}
		}
		mConfig = config;
		mReward = reward.OrderBy((KeyValuePair<int, SMEventRewardSetting> n) => n.Key).ToDictionary((KeyValuePair<int, SMEventRewardSetting> k) => k.Key, (KeyValuePair<int, SMEventRewardSetting> v) => v.Value);
		base.ManagedDisposeEvent = delegate
		{
			if (mReward != null)
			{
				mReward.Clear();
			}
			if (mConfig != null)
			{
				mConfig.Dispose();
				mConfig = null;
			}
			if (mAdvIcon != null)
			{
				GameMain.SafeDestroy(mAdvIcon.gameObject);
				mAdvIcon = null;
			}
		};
		CanExec = true;
		base.ManagedDisposeEvent = delegate
		{
			InvokeCallback(CALLBACK_TYPE.OnDispose);
		};
	}

	public void SetValue(int value)
	{
		SetValue(value, true);
	}

	private void SetValue(float f_value, bool immediate)
	{
		int value = Mathf.FloorToInt(f_value);
		if (!CanExec)
		{
			return;
		}
		bool flag = value != Value;
		if (immediate)
		{
			Value = value;
		}
		mCurrentValue = value;
		if (!mReward.Keys.Any((int n) => value < n))
		{
			if (mConfig != null && !mConfig.IsShowNonNextReward)
			{
				mConfig.Show(false);
			}
			else
			{
				mConfig.Label.enabled = false;
				if (mAdvIcon != null)
				{
					GameMain.SafeDestroy(mAdvIcon.gameObject);
				}
				mAdvIcon = null;
				if (mIcon != null)
				{
					if (mConfig.CompleteIconAtlas != null && mConfig.CompleteIconAtlas is BIJNGUIImage)
					{
						mIcon.enabled = true;
						string spriteName = (string.IsNullOrEmpty(mConfig.CompleteIconKey) ? "null" : mConfig.CompleteIconKey);
						mIcon.atlas = (mConfig.CompleteIconAtlas as BIJNGUIImage).Atlas;
						mIcon.spriteName = spriteName;
					}
					else
					{
						mIcon.enabled = false;
					}
				}
				if ((bool)mConfig.Body)
				{
					if (mConfig.Body.type == UIBasicSprite.Type.Filled)
					{
						mConfig.Body.fillAmount = 1f;
					}
					else
					{
						int num = mConfig.MaxHorizontalDimension - mConfig.MinHorizontalDimension;
						mConfig.Body.SetDimensions(num + mConfig.MinHorizontalDimension, mConfig.Body.height);
					}
				}
				if ((bool)mConfig.Complete && immediate)
				{
					mConfig.Complete.enabled = true;
				}
				IsAcquiredAllReward = true;
				if (!IsAcquiredAllReward && !mIsFirstSetValue)
				{
					InvokeCallback(CALLBACK_TYPE.OnAcquireAllReward);
				}
			}
		}
		else
		{
			int num2 = mReward.Keys.LastOrDefault((int n) => value >= n);
			int num3 = mReward.Keys.FirstOrDefault((int n) => value < n);
			if (mReward.ContainsKey(num3))
			{
				if (mConfig != null)
				{
					mConfig.Show(true);
				}
				if (mNextValue != (float)num3)
				{
					if (mAdvIcon != null)
					{
						GameMain.SafeDestroy(mAdvIcon.gameObject);
					}
					mAdvIcon = null;
					if (mIcon != null && mIcon.gameObject.activeSelf)
					{
						mIcon.gameObject.SetActive(false);
					}
					if (mReward[num3].IsGCItem != 0)
					{
						AdvRewardListData advRewardListData = null;
						if (SingletonMonoBehaviour<GameMain>.Instance.mAdvRewardListDict.ContainsKey(mReward[num3].RewardID))
						{
							advRewardListData = SingletonMonoBehaviour<GameMain>.Instance.mAdvRewardListDict[mReward[num3].RewardID];
						}
						if (advRewardListData != null)
						{
							if (advRewardListData.Category != 24)
							{
								if (mIcon != null)
								{
									if (!mIcon.gameObject.activeSelf)
									{
										mIcon.gameObject.SetActive(true);
									}
									mIcon.spriteName = advRewardListData.IconKey;
									mNextValue = num3;
								}
							}
							else if (SingletonMonoBehaviour<GameMain>.Instance.mAdvCharactersDataDict.ContainsKey(advRewardListData.Kind))
							{
								AdvCharacterData advCharacterData = SingletonMonoBehaviour<GameMain>.Instance.mAdvCharactersDataDict[advRewardListData.Kind];
								if (advCharacterData != null)
								{
									mAdvIcon = AdvIconInstance.Make(mConfig.Icon, "Icon", advCharacterData, Vector3.zero, 0);
									mNextValue = num3;
								}
							}
						}
					}
					else
					{
						AccessoryData accessoryData = SingletonMonoBehaviour<GameMain>.Instance.GetAccessoryData(mReward[num3].RewardID);
						if (accessoryData != null)
						{
							string text = null;
							string text2 = "HUD";
							switch (accessoryData.AccessoryType)
							{
							case AccessoryData.ACCESSORY_TYPE.BOOSTER:
							{
								int a_num = 0;
								Def.BOOSTER_KIND a_kind;
								accessoryData.GetBooster(out a_kind, out a_num);
								if (a_kind >= Def.BOOSTER_KIND.NONE && (int)a_kind < SingletonMonoBehaviour<GameMain>.Instance.mBoosterData.Count)
								{
									text = SingletonMonoBehaviour<GameMain>.Instance.mBoosterData[(int)a_kind].iconBuy;
								}
								break;
							}
							case AccessoryData.ACCESSORY_TYPE.ADV_ITEM:
								if (SingletonMonoBehaviour<GameMain>.Instance.mAdvRewardListDict.ContainsKey(int.Parse(accessoryData.GotID)))
								{
									AdvRewardListData advRewardListData2 = SingletonMonoBehaviour<GameMain>.Instance.mAdvRewardListDict[int.Parse(accessoryData.GotID)];
									if (advRewardListData2 != null)
									{
										text = advRewardListData2.IconKey;
									}
								}
								break;
							case AccessoryData.ACCESSORY_TYPE.PARTNER:
							{
								CompanionData companionData = SingletonMonoBehaviour<GameMain>.Instance.mCompanionData[int.Parse(accessoryData.GotID)];
								if (companionData != null)
								{
									text = companionData.iconName;
									text2 = "ICON";
								}
								break;
							}
							case AccessoryData.ACCESSORY_TYPE.GEM:
								text = "icon_84jem";
								break;
							case AccessoryData.ACCESSORY_TYPE.GROWUP:
								text = "icon_level";
								break;
							case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
								text = "icon_dailyEvent_stamp02";
								break;
							case AccessoryData.ACCESSORY_TYPE.HEART:
								text = "icon_heart";
								break;
							case AccessoryData.ACCESSORY_TYPE.HEART_PIECE:
								text = "icon_dailyEvent_stamp01";
								break;
							case AccessoryData.ACCESSORY_TYPE.BINGO_KEY:
								text = "icon_eventroadblock_key01";
								break;
							case AccessoryData.ACCESSORY_TYPE.BINGO_TICKET:
								text = "icon_event_ticket00";
								break;
							case AccessoryData.ACCESSORY_TYPE.OLDEVENT_KEY:
								text = "icon_old_event_key";
								break;
							case AccessoryData.ACCESSORY_TYPE.LABYRINTH_KEY:
								text = "icon_eventroadblock_key02";
								break;
							}
							bool flag2 = mIcon.spriteName != text;
							if (!mIcon.gameObject.activeSelf)
							{
								mIcon.gameObject.SetActive(true);
							}
							if (flag2)
							{
								if (mSelectedAtlasName != text2)
								{
									BIJImage image = ResourceManager.LoadImage(text2).Image;
									mIcon.atlas = (image as BIJNGUIImage).Atlas;
									mSelectedAtlasName = text2;
								}
								mIcon.spriteName = text;
							}
							if (flag2)
							{
								mNextValue = num3;
							}
						}
					}
				}
				Util.SetLabelText(mConfig.Label, string.Format(Localization.Get(mConfig.LocalizeKey), num3 - value));
				if ((bool)mConfig.Body)
				{
					if (mConfig.Body.type == UIBasicSprite.Type.Filled)
					{
						mConfig.Body.fillAmount = (f_value - (float)num2) / (float)(num3 - num2);
					}
					else
					{
						int num4 = mConfig.MaxHorizontalDimension - mConfig.MinHorizontalDimension;
						int num5 = (int)((float)(value - num2) / (float)(num3 - num2) * (float)num4);
						mConfig.Body.SetDimensions(num5 + mConfig.MinHorizontalDimension, mConfig.Body.height);
					}
				}
				if ((bool)mConfig.Complete)
				{
					mConfig.Complete.enabled = false;
				}
			}
			else
			{
				if (mConfig != null && !mConfig.IsShowNonNextReward)
				{
					mConfig.Show(false);
				}
				else
				{
					mConfig.Label.enabled = false;
					if (mAdvIcon != null)
					{
						GameMain.SafeDestroy(mAdvIcon.gameObject);
					}
					mAdvIcon = null;
					if (mIcon != null)
					{
						if (mConfig.CompleteIconAtlas != null && mConfig.CompleteIconAtlas is BIJNGUIImage)
						{
							mIcon.enabled = true;
							string spriteName2 = (string.IsNullOrEmpty(mConfig.CompleteIconKey) ? "null" : mConfig.CompleteIconKey);
							mIcon.atlas = (mConfig.CompleteIconAtlas as BIJNGUIImage).Atlas;
							mIcon.spriteName = spriteName2;
						}
						else
						{
							mIcon.enabled = false;
						}
					}
					if ((bool)mConfig.Body)
					{
						if (mConfig.Body.type == UIBasicSprite.Type.Filled)
						{
							mConfig.Body.fillAmount = 1f;
						}
						else
						{
							int num6 = mConfig.MaxHorizontalDimension - mConfig.MinHorizontalDimension;
							mConfig.Body.SetDimensions(num6 + mConfig.MinHorizontalDimension, mConfig.Body.height);
						}
					}
				}
				if ((bool)mConfig.Complete && immediate)
				{
					mConfig.Complete.enabled = true;
				}
				IsAcquiredAllReward = true;
				if (!IsAcquiredAllReward && !mIsFirstSetValue)
				{
					InvokeCallback(CALLBACK_TYPE.OnAcquireAllReward);
				}
			}
		}
		mIsFirstSetValue = false;
	}

	public void Animation(int add, float time, UnityAction on_finish = null)
	{
		if (CanExec)
		{
			if (mCoroutine != null)
			{
				StopAnimation();
			}
			mCoroutine = mConfig.Body.StartCoroutine(AnimationAsync(add, time, on_finish));
		}
	}

	public void StopAnimation()
	{
		if (CanExec)
		{
			if (mCoroutine != null)
			{
				mConfig.Body.StopCoroutine(mCoroutine);
			}
			mCoroutine = null;
		}
	}

	private bool IsAchieveReward(int target, bool compare_current = false)
	{
		int num = mReward.Keys.LastOrDefault((int n) => ((!compare_current) ? Value : mCurrentValue) >= n);
		int num2 = mReward.Keys.LastOrDefault((int n) => target >= n);
		return num2 > num;
	}

	private IEnumerator AnimationAsync(int add, float time, UnityAction on_finish)
	{
		if (mConfig != null)
		{
			MonoBehaviour behaviour = mConfig.Body;
			if (!behaviour)
			{
				behaviour = mConfig.Frame;
			}
			if (!behaviour)
			{
				behaviour = mConfig.Complete;
			}
			if ((bool)behaviour)
			{
				float ratio = (float)add / (time * 30f);
				float value = Value;
				int prev = -1;
				while (value <= (float)(Value + add))
				{
					int i = Mathf.FloorToInt(value);
					if ((i != prev || prev == -1) && prev != -1 && IsAchieveReward(i, true))
					{
						UISprite icon = UnityEngine.Object.Instantiate(mIcon);
						icon.transform.SetParent(mIcon.transform);
						icon.transform.SetLocalPosition(0f, 0f);
						icon.transform.SetLocalScale(1f);
						behaviour.StartCoroutine(AnimationSpriteFade(icon, 0.5f, 2f, 0f, true, false, true, 0.25f, delegate
						{
							if (IsAcquiredAllReward && !mIsExecCompleteAnimation)
							{
								mIsExecCompleteAnimation = true;
							}
						}));
						if (!string.IsNullOrEmpty(mConfig.AchieveSeKey))
						{
							SingletonMonoBehaviour<GameMain>.Instance.PlaySe(mConfig.AchieveSeKey, -1);
						}
					}
					prev = i;
					SetValue(value, false);
					if (value == (float)(Value + add))
					{
						break;
					}
					yield return null;
					value += ratio;
					if (value > (float)(Value + add))
					{
						value = Value + add;
					}
				}
			}
			Value += add;
		}
		if (on_finish != null)
		{
			on_finish();
		}
	}

	private IEnumerator AnimationCompleteAsync()
	{
		while (!mIsExecCompleteAnimation)
		{
			yield return null;
		}
		if (!(mConfig.Complete == null))
		{
			MonoBehaviour behaviour = mConfig.Body;
			if (!behaviour)
			{
				behaviour = mConfig.Frame;
			}
			if (!behaviour)
			{
				behaviour = mConfig.Complete;
			}
			if ((bool)behaviour)
			{
				mConfig.Complete.enabled = true;
				mConfig.Complete.SetLocalScale(2f, 2f, 1f);
				Color color = mConfig.Complete.color;
				color.a = 0f;
				mConfig.Complete.color = color;
				behaviour.StartCoroutine(AnimationSpriteFade(mConfig.Complete, 0.25f, 1f, 1f, false, true, false, 0f));
			}
		}
	}

	private IEnumerator AnimationSpriteFade(UISprite sprite, float time, float to_scale, float to_alpha, bool is_auto_destroy, bool is_linear_alpha, bool is_aftermath_alpha, float aftermath_time, UnityAction on_finished = null)
	{
		if (sprite == null)
		{
			yield break;
		}
		float alphaStart = 0f;
		if (is_aftermath_alpha)
		{
			alphaStart += aftermath_time;
		}
		AnimationCurve animScale = AnimationCurve.Linear(0f, sprite.transform.localScale.x, time, to_scale);
		AnimationCurve animAlpha = null;
		animAlpha = ((!is_linear_alpha) ? new AnimationCurve(new Keyframe(alphaStart, sprite.color.a, 0f, 0f), new Keyframe(alphaStart + time, to_alpha, -1.5708f, 1.5708f)) : AnimationCurve.Linear(alphaStart, sprite.color.a, alphaStart + time, to_alpha));
		time += alphaStart;
		float cur = 0f;
		while (cur < time)
		{
			float scale = animScale.Evaluate(cur);
			float alpha = animAlpha.Evaluate(cur);
			Color color = sprite.color;
			color.a = alpha;
			sprite.color = color;
			sprite.SetLocalScale(scale, scale, 1f);
			yield return null;
			float tmp = cur;
			cur += Time.deltaTime;
			if (tmp < time && cur > time)
			{
				cur = time;
			}
		}
		if (is_auto_destroy)
		{
			GameMain.SafeDestroy(sprite.gameObject);
			sprite = null;
		}
		if (on_finished != null)
		{
			on_finished();
		}
	}

	private void ExecCallbackMethod(UnityAction<UnityAction> method, params UnityAction[] calls)
	{
		if (method == null || calls == null)
		{
			return;
		}
		foreach (UnityAction unityAction in calls)
		{
			if (unityAction != null)
			{
				method(unityAction);
			}
		}
	}

	private UnityEvent GetCallback(CALLBACK_TYPE type, bool force_create)
	{
		if (mEvent == null && force_create)
		{
			mEvent = new Dictionary<CALLBACK_TYPE, UnityEvent>();
		}
		if (mEvent != null && !mEvent.ContainsKey(type) && force_create)
		{
			mEvent.Add(type, new UnityEvent());
		}
		return (mEvent == null || !mEvent.ContainsKey(type)) ? null : mEvent[type];
	}

	private void InvokeCallback(CALLBACK_TYPE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.Invoke();
		}
	}

	public void AddCallback(CALLBACK_TYPE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.AddListener, calls);
			}
		}
	}

	public void RemoveCallback(CALLBACK_TYPE type, params UnityAction[] calls)
	{
		if (calls != null)
		{
			UnityEvent callback = GetCallback(type, true);
			if (callback != null)
			{
				ExecCallbackMethod(callback.RemoveListener, calls);
			}
		}
	}

	public void ClearCallback(CALLBACK_TYPE type)
	{
		UnityEvent callback = GetCallback(type, false);
		if (callback != null)
		{
			callback.RemoveAllListeners();
		}
	}

	public void ClearCallback()
	{
		foreach (int value in Enum.GetValues(typeof(CALLBACK_TYPE)))
		{
			ClearCallback((CALLBACK_TYPE)value);
		}
	}

	public void SetCallback(CALLBACK_TYPE type, params UnityAction[] calls)
	{
		ClearCallback(type);
		AddCallback(type, calls);
	}
}
