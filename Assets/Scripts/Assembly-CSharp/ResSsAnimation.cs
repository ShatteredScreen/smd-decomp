public class ResSsAnimation : ResourceInstance
{
	public SsAnimation SsAnime;

	public ResSsAnimation(string name)
		: base(name)
	{
		ResourceType = TYPE.SS_ANIMATION;
	}
}
