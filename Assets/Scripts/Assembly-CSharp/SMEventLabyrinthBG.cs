using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class SMEventLabyrinthBG : SMMapBG
{
	public override void Active(BIJImage a_atlas, string a_spriteName, Vector3 a_pos, Vector3 a_scale)
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, a_pos.z);
		mScale = a_scale;
		mScale = new Vector3(1.46f, 1.46f, 1f);
		mAtlas = a_atlas;
		mMeshFilter = GetComponent<MeshFilter>();
		mMeshRenderer = GetComponent<MeshRenderer>();
		mMesh = new Mesh();
		mMaterial = new Material(GameMain.DefaultShader);
		mMeshFilter.sharedMesh = mMesh;
		mMeshRenderer.sharedMaterial = mMaterial;
		mMeshRenderer.sharedMaterial.mainTexture = mAtlas.Texture;
		mMeshRenderer.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRenderer.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		BIJNGUIImage bIJNGUIImage = (BIJNGUIImage)mAtlas;
		int num = 1;
		Vector3[] array = new Vector3[num * 4];
		Vector2[] array2 = new Vector2[num * 4];
		int[] array3 = new int[num * 6];
		Color32[] array4 = new Color32[num * 4];
		Vector2 pixelSize = bIJNGUIImage.GetPixelSize(a_spriteName);
		Vector2 offset = bIJNGUIImage.GetOffset(a_spriteName);
		Vector2 size = bIJNGUIImage.GetSize(a_spriteName);
		float z = base.gameObject.transform.localPosition.z;
		float num2 = 1f;
		array[0] = new Vector3((a_pos.x - pixelSize.x / 2f) * mScale.x, (a_pos.y - pixelSize.y / 2f + pixelSize.y * (1f - num2)) * mScale.y, z);
		array[1] = new Vector3((a_pos.x + pixelSize.x / 2f) * mScale.x, (a_pos.y - pixelSize.y / 2f + pixelSize.y * (1f - num2)) * mScale.y, z);
		array[2] = new Vector3((a_pos.x + pixelSize.x / 2f) * mScale.x, (a_pos.y + pixelSize.y / 2f) * mScale.y, z);
		array[3] = new Vector3((a_pos.x - pixelSize.x / 2f) * mScale.x, (a_pos.y + pixelSize.y / 2f) * mScale.y, z);
		float x = offset.x;
		float num3 = offset.y + size.y * (1f - num2);
		float x2 = offset.x + size.x;
		float y = num3 + size.y * num2;
		array2[0] = new Vector2(x, num3);
		array2[1] = new Vector2(x2, num3);
		array2[2] = new Vector2(x2, y);
		array2[3] = new Vector2(x, y);
		array3[0] = 0;
		array3[1] = 1;
		array3[2] = 2;
		array3[3] = 0;
		array3[4] = 2;
		array3[5] = 3;
		array4[0] = Color.white;
		array4[1] = Color.white;
		array4[2] = Color.white;
		array4[3] = Color.white;
		mMesh.vertices = array;
		mMesh.uv = array2;
		mMesh.triangles = array3;
		mMesh.colors32 = array4;
		mMesh.RecalculateNormals();
		mMesh.RecalculateBounds();
	}
}
