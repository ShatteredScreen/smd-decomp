namespace live2d.framework
{
	public class L2DModelMatrix : L2DMatrix44
	{
		private float width;

		private float height;

		public L2DModelMatrix(float w, float h)
		{
			width = w;
			height = h;
		}

		public void setPosition(float x, float y)
		{
			translate(x, y);
		}

		public void setCenterPosition(float x, float y)
		{
			float num = width * getScaleX();
			float num2 = height * getScaleY();
			translate(x - num / 2f, y - num2 / 2f);
		}

		public void top(float y)
		{
			setY(y);
		}

		public void bottom(float y)
		{
			float num = height * getScaleY();
			translateY(y - num);
		}

		public void left(float x)
		{
			setX(x);
		}

		public void right(float x)
		{
			float num = width * getScaleX();
			translateX(x - num);
		}

		public void centerX(float x)
		{
			float num = width * getScaleX();
			translateX(x - num / 2f);
		}

		public void centerY(float y)
		{
			float num = height * getScaleY();
			translateY(y - num / 2f);
		}

		public void setX(float x)
		{
			translateX(x);
		}

		public void setY(float y)
		{
			translateY(y);
		}

		public void setHeight(float h)
		{
			float num = h / height;
			float scaleY = 0f - num;
			scale(num, scaleY);
		}

		public void setWidth(float w)
		{
			float num = w / width;
			float scaleY = 0f - num;
			scale(num, scaleY);
		}
	}
}
