using System.Collections.Generic;
using UnityEngine;

public class EventAccessory : MapAccessory
{
	protected short mCourseID;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public override void Init(int _counter, string _name, AccessoryData data, BIJImage atlas, float x, float y, Vector3 scale, bool a_alreadyHas, int a_mapIndex, Def.SERIES a_series, string a_animeName)
	{
		mSeries = a_series;
		mMapIndex = a_mapIndex;
		AnimeKey = a_animeName;
		DefaultAnimeKey = a_animeName;
		mAtlas = atlas;
		mScale = scale;
		float num = Def.MAP_BUTTON_Z - 0.1f;
		base._pos = new Vector3(x, y, num);
		mData = data;
		if (!a_alreadyHas)
		{
			mChangeParts = new Dictionary<string, string>();
			mChangeParts["icon_00"] = data.ImageName;
			mChangeParts["icon_01"] = data.ImageName;
			mChangeParts["mess"] = "ac_mess_tap";
			bool flag = true;
			Player mPlayer = mGame.mPlayer;
			int stageNo = Def.GetStageNo(data.MainStage, data.SubStage);
			if (data.UnlockCondition.CompareTo("TAP") == 0)
			{
				PlayerClearData _psd;
				if (mGame.mEventMode)
				{
					if (mPlayer.GetStageClearData(a_series, mPlayer.Data.CurrentEventID, mCourseID, stageNo, out _psd) && (data.RelationID == 0 || mGame.mPlayer.IsAccessoryUnlock(data.RelationID)))
					{
						flag = false;
					}
				}
				else if (mPlayer.GetStageClearData(a_series, stageNo, out _psd))
				{
					flag = false;
				}
			}
			if (flag)
			{
				IsLockTapAnime = true;
				mChangeParts["icon_00"] = data.ImageName;
				mChangeParts["icon_01"] = data.ImageName;
				mChangeParts["mess"] = "null";
				mScale = new Vector3(0.7f, 0.7f, 1f);
				PlayAnime(AnimeKey + "_LOCK", true);
			}
			else
			{
				PlayAnime(AnimeKey, true);
			}
			HasTapAnime = true;
			if (!mFirstInitialized)
			{
				mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
				mEventTrigger = base.gameObject.AddComponent<UIEventTrigger>();
				mBoxCollider.size = new Vector3(mColliderSize, mColliderSize, 0f);
				EventDelegate item = new EventDelegate(this, "OnButtonPushed");
				mEventTrigger.onPress.Add(item);
				EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
				mEventTrigger.onRelease.Add(item2);
				EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
				mEventTrigger.onClick.Add(item3);
			}
		}
		else
		{
			HasTapAnime = false;
		}
		mLaceAnime = Util.CreateLoopAnime("Lace_" + data.ImageName, "MAP_LACE", base.gameObject.transform.parent.gameObject, new Vector3(x, y, num + 0.01f), mScale, 0f, 0f, false, null);
		mFirstInitialized = true;
	}

	public override void Init(int _counter, string _name, AccessoryData data, BIJImage atlas, float x, float y, Vector3 scale, bool a_alreadyHas, int a_mapIndex, Def.SERIES a_series, bool tutorial, string animeName)
	{
		mSeries = a_series;
		mMapIndex = a_mapIndex;
		AnimeKey = animeName;
		DefaultAnimeKey = animeName;
		mAtlas = atlas;
		mScale = scale;
		float z = Def.MAP_BUTTON_Z - 0.1f;
		base._pos = new Vector3(x, y, z);
		mData = data;
	}
}
