namespace live2d.framework
{
	internal enum EYE_STATE
	{
		STATE_FIRST = 0,
		STATE_INTERVAL = 1,
		STATE_CLOSING = 2,
		STATE_CLOSED = 3,
		STATE_OPENING = 4
	}
}
