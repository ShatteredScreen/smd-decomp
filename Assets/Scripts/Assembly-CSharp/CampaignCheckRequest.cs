public class CampaignCheckRequest : ParameterObject<CampaignCheckRequest>
{
	public int bhiveid { get; set; }

	public int uuid { get; set; }

	public string udid { get; set; }

	public string uniq_id { get; set; }

	public int is_tutorial_clear { get; set; }

	public int is_debug { get; set; }
}
