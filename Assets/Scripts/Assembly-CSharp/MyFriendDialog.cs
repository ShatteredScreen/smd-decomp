using System;
using UnityEngine;

public class MyFriendDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		NETWORK_LOGIN00 = 3,
		NETWORK_LOGIN01 = 4,
		NETWORK_00 = 5,
		NETWORK_00_00 = 6
	}

	public delegate void OnDialogClosed(bool mHereFaceBookLogin);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	private OnDialogClosed mCallback;

	private IdSearchDialog mIdSearchDialog;

	private MyDataDialog mMyDataDialog;

	private FriendFacebookSearchDialog mFriendFacebookSearchDialog;

	private FriendSearchDialog mFriendSearchDialog;

	private MyIconSetDialog mIconSetDialog;

	private NickNameDialog mNickNameDialog;

	private IconSettingDialog mIconSettingDialog;

	private FriendListDialog mFriendListDialog;

	private UISprite mIconSprite;

	private UISprite mIconLevelSprite;

	private UISprite mFacebookIcon;

	private UILabel mNiceNameLabel;

	public bool mFaceBookLogin;

	private UILabel mUserIDLabel;

	private UISprite mSpriteFlame;

	private UITexture mTex;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private string mIconImage;

	private string mIconLv;

	private string mFbstr = "icon_sns00_mini";

	private int mMapicon;

	private string mIconMap;

	private int mStagenum;

	private string mUserName;

	private string mUserID;

	private string mUserDescID;

	private string mFriendNum;

	private MapAvater mAvatar;

	public bool Modified { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			mGame.mPlayer.Data.SearchID = string.Empty;
			if (string.IsNullOrEmpty(mGame.mPlayer.Data.SearchID) || string.IsNullOrEmpty(mGame.mPlayer.Data.NickName))
			{
				mState.Reset(STATE.NETWORK_00, true);
			}
			break;
		case STATE.NETWORK_00:
			mGame.Network_NicknameCheck();
			mState.Change(STATE.NETWORK_00_00);
			break;
		case STATE.NETWORK_00_00:
			if (!GameMain.mNickNameCheckDone)
			{
				break;
			}
			if (GameMain.mNickNameSucceed)
			{
				Player mPlayer = mGame.mPlayer;
				mUserID = string.Format(Localization.Get("UserID_Data"), mPlayer.Data.SearchID);
				mUserDescID = string.Format(Localization.Get("UserID_Desc")) + string.Format(Localization.Get("UserID_Data"), mPlayer.Data.SearchID);
				mUserName = mPlayer.Data.NickName;
				Util.SetLabelText(mNiceNameLabel, mUserName);
				Util.SetLabelText(mUserIDLabel, mUserDescID);
				mState.Change(STATE.MAIN);
			}
			else
			{
				Server.ErrorCode nickNameErrorCode = GameMain.NickNameErrorCode;
				Server.ErrorCode errorCode = nickNameErrorCode;
				if (errorCode == Server.ErrorCode.MAINTENANCE_MODE)
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 5, OnConnectErrorRetry, OnConnectErrorAbandon);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY_COUNT, 5, OnConnectErrorRetry, OnConnectErrorAbandon);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							Modified = false;
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
						mFaceBookLogin = true;
						mFriendFacebookSearchDialog = Util.CreateGameObject("FriendFacebookSearchDialog", base.ParentGameObject).AddComponent<FriendFacebookSearchDialog>();
						mFriendFacebookSearchDialog.SetClosedCallback(Dialogclosed);
						mFriendFacebookSearchDialog.SetBaseDepth(mBaseDepth + 70);
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns))
				{
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}, null);
			break;
		}
		}
		mState.Update();
	}

	public void Init(MapAvater a_avater)
	{
		mAvatar = a_avater;
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		Player mPlayer = mGame.mPlayer;
		int num = 0;
		num = mPlayer.Data.PlayerIcon;
		if (num >= 10000)
		{
			num -= 10000;
		}
		CompanionData companionData = mGame.mCompanionData[num];
		mIconImage = companionData.iconName;
		int companionLevel = mPlayer.GetCompanionLevel(num);
		if (companionLevel < companionData.maxLevel)
		{
			mIconLv = Def.CharacterIconLevelImageTbl[companionLevel];
		}
		else
		{
			mIconLv = Def.CharacterIconLevelMaxImageTbl[companionLevel];
		}
		mUserName = mGame.mPlayer.Data.NickName;
		mUserID = string.Format(Localization.Get("UserID_Data"), mPlayer.Data.SearchID);
		mUserDescID = string.Format(Localization.Get("UserID_Desc")) + string.Format(Localization.Get("UserID_Data"), mPlayer.Data.SearchID);
		mFriendNum = string.Format(Localization.Get("FriendTotalNum"), mPlayer.Friends.Count, Constants.GAME_FRIEND_MAX);
		string titleDescKey = Localization.Get("FriendPage_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		mSpriteFlame = Util.CreateSprite("Instruction", base.gameObject, image);
		Util.SetSpriteInfo(mSpriteFlame, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, 133f, 0f), Vector3.one, false);
		mSpriteFlame.type = UIBasicSprite.Type.Sliced;
		mSpriteFlame.SetDimensions(490, 177);
		if (GameMain.IsFacebookIconEnable() && mPlayer.Data.PlayerIcon >= 10000)
		{
			if (mGame.mPlayer.Icon != null)
			{
				mTex = Util.CreateGameObject("Icon", mSpriteFlame.gameObject).AddComponent<UITexture>();
				mTex.depth = mBaseDepth + 2;
				mTex.transform.localPosition = new Vector3(-194f, 20f, 0f);
				mTex.transform.localScale = Vector3.one;
				mTex.mainTexture = mGame.mPlayer.Icon;
				mTex.SetDimensions(80, 80);
			}
			else
			{
				mIconSprite = Util.CreateSprite("CharaIcon", mSpriteFlame.gameObject, image2);
				Util.SetSpriteInfo(mIconSprite, "icon_chara_facebook", mBaseDepth + 3, new Vector3(-194f, 20f, 0f), Vector3.one, false);
				mIconSprite.SetDimensions(80, 80);
			}
			mIconSprite = Util.CreateSprite("CharaIcon", mSpriteFlame.gameObject, image2);
			Util.SetSpriteInfo(mIconSprite, mIconImage, mBaseDepth + 3, new Vector3(-168f, 57f, 0f), Vector3.one, false);
			mIconSprite.SetDimensions(46, 46);
		}
		else
		{
			mIconSprite = Util.CreateSprite("CharaIcon", mSpriteFlame.gameObject, image2);
			Util.SetSpriteInfo(mIconSprite, mIconImage, mBaseDepth + 3, new Vector3(-194f, 33f, 0f), Vector3.one, false);
			mIconSprite.SetDimensions(80, 80);
			mIconLevelSprite = Util.CreateSprite("CharaLv", mSpriteFlame.gameObject, image);
			Util.SetSpriteInfo(mIconLevelSprite, mIconLv, mBaseDepth + 4, new Vector3(-194f, -6f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable() && IsFacebookLogined())
			{
				mFacebookIcon = Util.CreateSprite("FBIcon", mSpriteFlame.gameObject, image2);
				Util.SetSpriteInfo(mFacebookIcon, mFbstr, mBaseDepth + 4, new Vector3(-171f, 57f, 0f), Vector3.one, false);
			}
		}
		UISprite uISprite = Util.CreateSprite("Linetop", mSpriteFlame.gameObject, image);
		Util.SetSpriteInfo(uISprite, "line00", mBaseDepth + 3, new Vector3(46f, 13f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(376, 6);
		uISprite = Util.CreateSprite("LineBot", mSpriteFlame.gameObject, image);
		Util.SetSpriteInfo(uISprite, "line00", mBaseDepth + 3, new Vector3(46f, -51f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(376, 6);
		UIButton button = Util.CreateJellyImageButton("NameChangeButton", mSpriteFlame.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_change", "LC_button_change", "LC_button_change", mBaseDepth + 3, new Vector3(203f, 38f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnNiceNameChangePushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("IDCopyButton", mSpriteFlame.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_copy", "LC_button_copy", "LC_button_copy", mBaseDepth + 3, new Vector3(203f, -25f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnYourIDPushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("IconChangeButton", mSpriteFlame.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_change", "LC_button_change", "LC_button_change", mBaseDepth + 3, new Vector3(-194f, -45f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnIconChangePushed", UIButtonMessage.Trigger.OnClick);
		mNiceNameLabel = Util.CreateLabel("userNameDesk", mSpriteFlame.gameObject, atlasFont);
		Util.SetLabelInfo(mNiceNameLabel, mUserName, mBaseDepth + 3, new Vector3(-134f, 30f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		mNiceNameLabel.color = Def.DEFAULT_MESSAGE_COLOR;
		mNiceNameLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		mNiceNameLabel.SetDimensions(302, 26);
		mUserIDLabel = Util.CreateLabel("userIDDesk", mSpriteFlame.gameObject, atlasFont);
		Util.SetLabelInfo(mUserIDLabel, mUserDescID, mBaseDepth + 4, new Vector3(-134f, -32f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
		mUserIDLabel.color = Def.DEFAULT_MESSAGE_COLOR;
		float num2 = -12f;
		float num3 = -88f;
		button = Util.CreateJellyImageButton("IDsearchButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, num2, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnIdsearchPushed", UIButtonMessage.Trigger.OnClick);
		titleDescKey = string.Format(Localization.Get("MyData_Title"));
		UILabel label = Util.CreateLabel("IDSearchDesk", button.gameObject, atlasFont);
		Util.SetLabelInfo(label, titleDescKey, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		num2 += num3;
		button = Util.CreateJellyImageButton("FacebookButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, num2, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnFriendSearchDialogPushed", UIButtonMessage.Trigger.OnClick);
		titleDescKey = string.Format(Localization.Get("FriendSearch_Title"));
		label = Util.CreateLabel("FacebookSearchDesk", button.gameObject, atlasFont);
		Util.SetLabelInfo(label, titleDescKey, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		num2 += num3;
		button = Util.CreateJellyImageButton("FriendButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "button00", "button00", "button00", mBaseDepth + 3, new Vector3(0f, num2, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnFriendListPushed", UIButtonMessage.Trigger.OnClick);
		titleDescKey = string.Format(Localization.Get("FriendList"));
		label = Util.CreateLabel("IDSearchDesk", button.gameObject, atlasFont);
		Util.SetLabelInfo(label, titleDescKey, mBaseDepth + 4, new Vector3(0f, 0f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		CreateCancelButton("OnCancelPushed");
	}

	public void OnIdsearchPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mMyDataDialog = Util.CreateGameObject("MyDataDialog", base.ParentGameObject).AddComponent<MyDataDialog>();
			mMyDataDialog.SetClosedCallback(Dialogclosed);
			mMyDataDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	public void OnIconChangePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mIconSetDialog = Util.CreateGameObject("MyIconSetDialog", base.ParentGameObject).AddComponent<MyIconSetDialog>();
			mIconSetDialog.SetClosedCallback(OnIconSettingDialogClosed);
			mIconSetDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	public void OnNiceNameChangePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mNickNameDialog = Util.CreateGameObject("NickNameDialog", base.ParentGameObject).AddComponent<NickNameDialog>();
			mNickNameDialog.SetClosedCallback(OnNickNameClosed);
			mNickNameDialog.SetBaseDepth(mBaseDepth + 70);
		}
	}

	public void OnYourIDPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			string clipboardText = mUserID;
			BIJUnity.setClipboardText(clipboardText);
			mState.Reset(STATE.WAIT, true);
			mConfirmDialog = Util.CreateGameObject("YourIDCopy", base.ParentGameObject).AddComponent<ConfirmDialog>();
			mConfirmDialog.Init(Localization.Get("CopyID_Title"), Localization.Get("CopyID_Desc"), ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
			mConfirmDialog.SetClosedCallback(OnConfirmDialogClosed);
		}
	}

	public void OnConfirmDialogClosed(ConfirmDialog.SELECT_ITEM i)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	public void OnFacbooksearchPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSNS = SocialManager.Instance[SNS.Facebook];
			if (IsFacebookLogined())
			{
				mState.Reset(STATE.WAIT, true);
				mFriendFacebookSearchDialog = Util.CreateGameObject("FriendFacebookSearchDialog", base.ParentGameObject).AddComponent<FriendFacebookSearchDialog>();
				mFriendFacebookSearchDialog.SetClosedCallback(Dialogclosed);
				mFriendFacebookSearchDialog.SetBaseDepth(mBaseDepth + 70);
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = base.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public void Dialogclosed()
	{
		mState.Change(STATE.MAIN);
	}

	public void OnIconSettingDialogClosed(bool FacebookLogin, bool iconIdSaveflg)
	{
		mGame.Network_NicknameEdit(string.Empty);
		if (iconIdSaveflg)
		{
			mGame.Save(true, true);
		}
		if (FacebookLogin)
		{
			mState.Change(STATE.MAIN);
			mFaceBookLogin = true;
			Close();
		}
		else
		{
			mState.Change(STATE.MAIN);
			OnIconSettingDialogClosed();
		}
	}

	public void OnIconSettingDialogClosed()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		BIJImage image2 = ResourceManager.LoadImage("ICON").Image;
		Player mPlayer = mGame.mPlayer;
		int num = 0;
		num = mPlayer.Data.PlayerIcon;
		if (num >= 10000)
		{
			num -= 10000;
		}
		if (num >= 0)
		{
			CompanionData companionData = mGame.mCompanionData[num];
			mIconImage = companionData.iconName;
		}
		if (GameMain.IsFacebookIconEnable() && mPlayer.Data.PlayerIcon >= 10000)
		{
			if (mGame.mPlayer.Icon != null)
			{
				if (mTex == null)
				{
					mTex = Util.CreateGameObject("Icon", mSpriteFlame.gameObject).AddComponent<UITexture>();
					mTex.depth = mBaseDepth + 2;
					mTex.transform.localPosition = new Vector3(-194f, 20f, 0f);
					mTex.transform.localScale = Vector3.one;
					mTex.mainTexture = mGame.mPlayer.Icon;
					mTex.SetDimensions(80, 80);
				}
				if (mIconSprite != null)
				{
					mIconSprite.transform.localPosition = new Vector3(-168f, 57f, 0f);
					Util.SetSpriteImageName(mIconSprite, mIconImage, Vector3.one, false);
					mIconSprite.SetDimensions(46, 46);
				}
				if (mIconLevelSprite != null)
				{
					mIconLv = null;
					Util.SetSpriteImageName(mIconLevelSprite, mIconLv, Vector3.one, false);
				}
				if (mFacebookIcon != null)
				{
					Util.SetSpriteImageName(mFacebookIcon, null, Vector3.one, false);
					mFacebookIcon = null;
				}
			}
		}
		else
		{
			CompanionData companionData = mGame.mCompanionData[num];
			mIconImage = companionData.iconName;
			if (mIconSprite != null)
			{
				mIconSprite.transform.localPosition = new Vector3(-194f, 33f, 0f);
				mIconSprite.SetDimensions(80, 80);
				int companionLevel = mPlayer.GetCompanionLevel(num);
				if (companionLevel < companionData.maxLevel)
				{
					mIconLv = Def.CharacterIconLevelImageTbl[companionLevel];
				}
				else
				{
					mIconLv = Def.CharacterIconLevelMaxImageTbl[companionLevel];
				}
				if (mIconLevelSprite != null)
				{
					Util.SetSpriteImageName(mIconLevelSprite, mIconLv, Vector3.one, false);
				}
				else
				{
					mIconLevelSprite = Util.CreateSprite("CharaLv", mSpriteFlame.gameObject, image);
					Util.SetSpriteInfo(mIconLevelSprite, mIconLv, mBaseDepth + 4, new Vector3(-194f, -6f, 0f), Vector3.one, false);
				}
				if (mIconImage != null)
				{
					Util.SetSpriteImageName(mIconSprite, mIconImage, Vector3.one, false);
					mIconSprite.SetDimensions(80, 80);
				}
				if (GameMain.IsFacebookMiniIconEnable() && mFacebookIcon == null && IsFacebookLogined())
				{
					mFacebookIcon = Util.CreateSprite("FBIcon", mSpriteFlame.gameObject, image2);
					Util.SetSpriteInfo(mFacebookIcon, mFbstr, mBaseDepth + 4, new Vector3(-171f, 57f, 0f), Vector3.one, false);
				}
			}
			else
			{
				mIconSprite = Util.CreateSprite("CharaIcon", mSpriteFlame.gameObject, image2);
				Util.SetSpriteInfo(mIconSprite, mIconImage, mBaseDepth + 3, new Vector3(-194f, 33f, 0f), Vector3.one, false);
				mIconSprite.SetDimensions(80, 80);
				mIconLevelSprite = Util.CreateSprite("CharaLv", mSpriteFlame.gameObject, image);
				Util.SetSpriteInfo(mIconLevelSprite, mIconLv, mBaseDepth + 4, new Vector3(-194f, -6f, 0f), Vector3.one, false);
				if (GameMain.IsFacebookMiniIconEnable() && IsFacebookLogined())
				{
					mFacebookIcon = Util.CreateSprite("FBIcon", mSpriteFlame.gameObject, image2);
					Util.SetSpriteInfo(mFacebookIcon, mFbstr, mBaseDepth + 4, new Vector3(-171f, 57f, 0f), Vector3.one, false);
				}
			}
			if (mTex != null)
			{
				mTex.transform.localScale = Vector3.zero;
				mTex = null;
			}
		}
		mState.Change(STATE.MAIN);
	}

	public void OnNickNameClosed(bool FacebookLogin)
	{
		mUserName = mGame.mPlayer.Data.NickName;
		Util.SetLabelText(mNiceNameLabel, mUserName);
		mState.Change(STATE.MAIN);
		if (FacebookLogin)
		{
			mFaceBookLogin = true;
		}
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mFaceBookLogin);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.Save();
			Player mPlayer = mGame.mPlayer;
			if (mAvatar != null)
			{
				mAvatar.SetIcon();
			}
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void OnFriendListPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			if (mFriendListDialog != null)
			{
				UnityEngine.Object.Destroy(mFriendListDialog.gameObject);
				mFriendListDialog = null;
			}
			mFriendListDialog = Util.CreateGameObject("FriendListDialog", base.ParentGameObject).AddComponent<FriendListDialog>();
			mFriendListDialog.SetClosedCallback(OnFriendListClosed);
		}
	}

	private void OnFriendSearchDialogPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			if ((GameMain.IsSNSFunctionEnabled(SNS_FLAG.FriendSearch) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.FriendSearch, SNS.Facebook)) || (GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite) && GameMain.IsSNSFunctionEnabled(SNS_FLAG.Invite, SNS.Facebook)))
			{
				mFriendSearchDialog = Util.CreateGameObject("FriendSearchDialog", base.ParentGameObject).AddComponent<FriendSearchDialog>();
				mFriendSearchDialog.SetClosedCallback(OnFriendSearchClosed);
			}
			else
			{
				mIdSearchDialog = Util.CreateGameObject("IdSearchDialog", base.ParentGameObject).AddComponent<IdSearchDialog>();
				mIdSearchDialog.SetClosedCallback(Dialogclosed);
			}
		}
	}

	public void OnFriendSearchClosed()
	{
		mState.Change(STATE.MAIN);
	}

	public void OnFriendListClosed(bool a)
	{
		mState.Change(STATE.MAIN);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	private void OnConnectErrorRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorAbandon()
	{
		Close();
		mState.Reset(STATE.WAIT);
	}
}
