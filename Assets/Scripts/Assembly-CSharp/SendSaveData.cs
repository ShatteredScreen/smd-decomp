using System.Collections.Generic;

public class SendSaveData : ParameterObject<SendSaveData>
{
	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("fbid")]
	public string FBID { get; set; }

	[MiniJSONAlias("series")]
	public int Series { get; set; }

	[MiniJSONAlias("lvl")]
	public int Level { get; set; }

	[MiniJSONAlias("xp")]
	public int PlayTime { get; set; }

	[MiniJSONAlias("mc")]
	public int MainCurrency { get; set; }

	[MiniJSONAlias("sc")]
	public int SubCurrency { get; set; }

	[MiniJSONAlias("total_score")]
	public long TotalScore { get; set; }

	[MiniJSONAlias("total_star")]
	public long TotalStar { get; set; }

	[MiniJSONAlias("total_trophy")]
	public long TotalTrophy { get; set; }

	[MiniJSONAlias("play_stage")]
	public List<PlayStageParam> PlayStage { get; set; }
}
