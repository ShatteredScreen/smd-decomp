public class SMRoadBlockSetting : SMMapPageSettingBase
{
	public short RoadBlockID;

	public int UnlockStars;

	public int UnlockFriends;

	public int UnlockSD;

	public int UnlockTicket;

	public int RoadBlockLevel
	{
		get
		{
			return Def.GetStageNo(mStageNo, 0);
		}
	}

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteShort((short)mCourseNo);
		a_writer.WriteShort(RoadBlockID);
		a_writer.WriteInt(mStageNo);
		a_writer.WriteInt(mSubNo);
		a_writer.WriteUTF(OpenDemo);
		a_writer.WriteUTF(ClearDemo);
		a_writer.WriteInt(UnlockStars);
		a_writer.WriteInt(UnlockFriends);
		a_writer.WriteInt(UnlockSD);
		a_writer.WriteInt(UnlockTicket);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		mCourseNo = a_reader.ReadShort();
		RoadBlockID = a_reader.ReadShort();
		mStageNo = a_reader.ReadInt();
		mSubNo = a_reader.ReadInt();
		OpenDemo = a_reader.ReadUTF();
		ClearDemo = a_reader.ReadUTF();
		UnlockStars = a_reader.ReadInt();
		UnlockFriends = a_reader.ReadInt();
		UnlockSD = a_reader.ReadInt();
		UnlockTicket = a_reader.ReadInt();
	}
}
