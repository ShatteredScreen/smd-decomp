public class ConnectNicknameCheck : ConnectBase
{
	protected override void InitServerFlg()
	{
		GameMain.mNickNameCheckDone = false;
		GameMain.mNickNameSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mNickNameCheckDone = true;
		GameMain.mNickNameSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mNickNameCheckDone = true;
		GameMain.mNickNameSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			mGame.mUserProfile = new UserGetResponse();
			mGame.mUserProfile.NickName = "DUMMY_PLUG";
			mGame.mUserProfile.IconID = 0;
			mGame.mUserProfile.IconLvl = 1;
			mGame.mUserProfile.SearchID = "DUMMY";
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		InitServerFlg();
		if (!Server.NicknameCheck())
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		mGame.SetNicknameCheckFromResponse();
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
