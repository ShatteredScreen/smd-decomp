using System;

public class CampaignSaleEntry : IComparable
{
	public enum KIND
	{
		IAP = 0,
		BOOSTER = 1,
		HEART = 2
	}

	public enum IMAGE_NO
	{
		NONE = 0,
		BARGAIN = 1,
		MOST_POPULAR = 2,
		ONE_TIME_OFFER = 3,
		NEW = 4
	}

	[MiniJSONAlias("No")]
	public int No { get; set; }

	[MiniJSONAlias("On")]
	public int On { get; set; }

	[MiniJSONAlias("Kind")]
	public int Kind { get; set; }

	[MiniJSONAlias("StartTimeEpoch")]
	public int StartTimeEpoch { get; set; }

	[MiniJSONAlias("EndTimeEpoch")]
	public int EndTimeEpoch { get; set; }

	[MiniJSONAlias("Key")]
	public string Key { get; set; }

	[MiniJSONAlias("KeyBNE")]
	public string KeyBNE { get; set; }

	[MiniJSONAlias("ImageNo")]
	public int ImageNo { get; set; }

	[MiniJSONAlias("Text")]
	public string Text { get; set; }

	public int ShowId { get; set; }

	public DateTime StartTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(StartTimeEpoch).ToLocalTime();
		}
	}

	public DateTime EndTime
	{
		get
		{
			return DateTimeUtil.UnixTimeStampToDateTime(EndTimeEpoch).ToLocalTime();
		}
	}

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as CampaignSaleEntry);
	}

	public int CompareTo(CampaignSaleEntry _obj)
	{
		try
		{
			if (_obj != null)
			{
				return No - _obj.No;
			}
		}
		catch (Exception)
		{
		}
		return int.MaxValue;
	}

	public bool IsEnable()
	{
		return On != 0;
	}

	public bool InTime()
	{
		DateTime dateTime = DateTimeUtil.Now();
		if (StartTime <= dateTime && dateTime < EndTime)
		{
			return true;
		}
		return false;
	}
}
