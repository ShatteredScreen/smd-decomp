using System.Collections.Generic;

public class SCCheckResponse : ParameterObject<SCCheckResponse>
{
	public List<SCStatus> results { get; set; }

	public int code { get; set; }

	public int code_ex { get; set; }

	public long server_time { get; set; }
}
