public static class MarketingSettings
{
	public const bool USE_LOG = true;

	public const bool DEBUG_MODE = false;

	public const bool DISABLE_PARTYTRACK = true;

	public const bool DEBUG_PARTYTRACK = false;

	public const bool DISABLE_GROWTHPUSH = false;

	public const bool DEBUG_GROWTHPUSH = false;

	public const bool DISABLE_ADJUST = false;

	public const bool DEBUG_ADJUST = false;
}
