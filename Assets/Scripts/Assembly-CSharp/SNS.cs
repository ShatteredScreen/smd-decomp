public enum SNS
{
	None = 0,
	Facebook = 1,
	GameCenter = 2,
	GooglePlus = 3,
	LINE = 4,
	Twitter = 5,
	SMS = 6,
	Mail = 7,
	Instagram = 8,
	GooglePlayGameServices = 9
}
