using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class BIJSNSGooglePlusMobile : BIJSNSGooglePlus
{
	private const string ATTACHEMENT_FILE = "attachment.png";

	private const float ATTACHEMENT_RESIZE_SCALE = 0.5f;

	public override bool IsOpEnabled(int type)
	{
		if (!IsEnabled())
		{
			return false;
		}
		OP_TYPE oP_TYPE = OP_TYPE.NONE;
		try
		{
			oP_TYPE = (OP_TYPE)(int)Enum.ToObject(typeof(OP_TYPE), type);
		}
		catch (Exception)
		{
			return false;
		}
		switch (oP_TYPE)
		{
		case OP_TYPE.LOGIN:
		case OP_TYPE.LOGOUT:
		case OP_TYPE.GETUSERINFO:
		case OP_TYPE.GETFRIENDS:
		case OP_TYPE.FETCHUSERIMAGE:
		case OP_TYPE.POSTMESSAGE:
		case OP_TYPE.INVITE:
			return true;
		default:
			return false;
		}
	}

	public override bool IsLogined()
	{
		return GooglePlusPlugin.PluginIF.isSignedIn();
	}

	public override void Login(Handler handler, object userdata)
	{
		if (IsLogined())
		{
			CallCallback(handler, new Response(0, "Google+ already logged in", null), userdata);
			return;
		}
		int type = 2;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			GooglePlusPlugin.PluginIF.authenticate();
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}

	public override void Logout(Handler handler, object userdata)
	{
		if (!IsLogined())
		{
			CallCallback(handler, new Response(0, "Google+ already logged out", null), userdata);
			return;
		}
		GooglePlusPlugin.PluginIF.signOut();
		CallCallback(handler, new Response(0, string.Empty, null), userdata);
	}

	private bool CheckNoLogined()
	{
		return !IsLogined();
	}

	private IEnumerator BIJSNSGooglePlusMobile_PluginFailed(int seqno, string message, object result)
	{
		yield return new WaitForSeconds(0.5f);
		if (base.CurrentSeqNo == seqno)
		{
			base.CurrentRes = new Response(1, message, result);
			ChangeStep(STEP.COMM_RESULT);
		}
	}

	public override void GetUserInfo(string userId, Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google+ must logged in", null), userdata);
			return;
		}
		int type = 4;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (!GooglePlusPlugin.PluginIF.loadPerson(userId))
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGooglePlusMobile_PluginFailed(num, "Google+ load person failed: " + userId, null));
			}
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}

	public override void GetFriends(string userId, int limit, Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google+ must logged in", null), userdata);
			return;
		}
		int type = 5;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (!GooglePlusPlugin.PluginIF.loadVisiblePeople(userId))
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGooglePlusMobile_PluginFailed(num, "Google+ load visible people failed: " + userId, null));
			}
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}

	public override void FetchUserImage(string userId, Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google+ must logged in", null), userdata);
			return;
		}
		if (base.surrogateMonoBehaviour == null)
		{
			CallCallback(handler, new Response(1, "Google+ has no surrogate MonoBehaviour", null), userdata);
			return;
		}
		try
		{
			base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGooglePlus_FetchUserImage_Coroutine(userId, handler, userdata));
		}
		catch (Exception)
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}

	private IEnumerator BIJSNSGooglePlus_FetchUserImage_Coroutine(string userId, Handler handler, object userdata)
	{
		userId = BIJSNS.ToSS(userId);
		if (!GooglePlusPlugin.PluginIF.getUserImageURL(userId))
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
			yield break;
		}
		float check = Time.realtimeSinceStartup;
		string url = string.Empty;
		while (!GetUserImageURL(userId, out url))
		{
			if (Time.realtimeSinceStartup - check > 5f)
			{
				CallCallback(handler, new Response(1, "Google+ user image url request is timeout: " + userId, null), userdata);
				yield break;
			}
			yield return 0;
		}
		if (string.IsNullOrEmpty(url))
		{
			CallCallback(handler, new Response(1, "Google+ could not get user image url: " + userId, null), userdata);
			yield break;
		}
		WWW www = new WWW(url);
		yield return www;
		Texture2D tex = www.texture;
		if (IsValidTexture(ref tex))
		{
			CallCallback(handler, new Response(0, userId, tex), userdata);
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ picure download failed: " + url, null), userdata);
		}
	}

	private IEnumerator BIJSNSGooglePlusAndroid_PostMessage_Coroutine(int seqno, string title, string message, string imageurl, Handler handler, object userdata, float scale)
	{
		bool needDownload2 = false;
		string url = imageurl;
		if (string.IsNullOrEmpty(url))
		{
			needDownload2 = false;
		}
		else
		{
			if (!url.Contains("://"))
			{
				url = "file://" + url;
			}
			needDownload2 = ((!url.StartsWith("file:")) ? true : false);
		}
		if (needDownload2)
		{
			string localPath = Application.temporaryCachePath + "/attachment.png";
			if (File.Exists(localPath))
			{
				File.Delete(localPath);
			}
			WWW www = new WWW(url);
			yield return www;
			Texture2D tex = www.texture;
			if (!IsValidTexture(ref tex))
			{
				url = string.Empty;
			}
			else
			{
				if (scale > 0f)
				{
					BIJTextureScale.Bilinear(newWidth: Mathf.FloorToInt((float)tex.width * scale), newHeight: Mathf.FloorToInt((float)tex.height * scale), tex: tex);
				}
				File.WriteAllBytes(localPath, tex.EncodeToPNG());
				url = ("file://" + localPath).Replace('\\', '/');
			}
		}
		if (!GooglePlusPlugin.PluginIF.interactivePost(title, message, string.Empty, url, "application/image", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty))
		{
			base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGooglePlusMobile_PluginFailed(seqno, "Google+ failed to invoke share dialog.", null));
		}
	}

	public override void PostMessage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google+ must logged in", null), userdata);
			return;
		}
		int type = 7;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string message = BIJSNS.ToSS(postdata.Message);
			string text = BIJSNS.ToSS(postdata.ImageURL);
			string link = BIJSNS.ToSS(postdata.LinkURL);
			if (!GooglePlusPlugin.PluginIF.interactivePost(title, message, link, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty))
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGooglePlusMobile_PluginFailed(num, "Google+ failed to invoke share dialog.", null));
			}
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}

	public override void UploadImage(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google+ must logged in", null), userdata);
			return;
		}
		int type = 9;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string message = BIJSNS.ToSS(postdata.Message);
			string attachUrl = BIJSNS.ToSS(postdata.ImageURL);
			string text = BIJSNS.ToSS(postdata.LinkURL);
			if (!GooglePlusPlugin.PluginIF.interactivePost(title, message, string.Empty, attachUrl, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty))
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGooglePlusMobile_PluginFailed(num, "Google+ failed to invoke share dialog.", null));
			}
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}

	public override void Invite(IBIJSNSPostData postdata, Handler handler, object userdata)
	{
		if (CheckNoLogined())
		{
			CallCallback(handler, new Response(1, "Google+ must logged in", null), userdata);
			return;
		}
		int type = 8;
		int num = PushReq(type, handler, userdata);
		if (num != 0)
		{
			if (IsEnableSwitching())
			{
				spawn = true;
				backtogame = false;
				spawnSeqNo = num;
			}
			string title = BIJSNS.ToSS(postdata.Title);
			string message = BIJSNS.ToSS(postdata.Message);
			string text = BIJSNS.ToSS(postdata.ImageURL);
			string link = BIJSNS.ToSS(postdata.LinkURL);
			if (!GooglePlusPlugin.PluginIF.interactivePost(title, message, link, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty))
			{
				base.surrogateMonoBehaviour.StartCoroutine(BIJSNSGooglePlusMobile_PluginFailed(num, "Google+ failed to invoke share dialog.", null));
			}
		}
		else
		{
			CallCallback(handler, new Response(1, "Google+ not requested", null), userdata);
		}
	}
}
