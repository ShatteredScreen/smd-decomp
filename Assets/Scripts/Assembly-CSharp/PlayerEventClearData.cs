using System;

public class PlayerEventClearData : PlayerClearData, ICloneable
{
	public int EventID { get; set; }

	public short CourseID { get; set; }

	public int EventStageNo
	{
		get
		{
			int a_main;
			int a_sub;
			Def.SplitStageNo(base.StageNo, out a_main, out a_sub);
			return Def.GetStageNo(CourseID, a_main, a_sub);
		}
	}

	public PlayerEventClearData()
	{
		EventID = -1;
		CourseID = -1;
	}

	public PlayerEventClearData(int a_eventID, short a_course)
	{
		EventID = a_eventID;
		CourseID = a_course;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override bool Equals(object obj)
	{
		PlayerEventClearData playerEventClearData = obj as PlayerEventClearData;
		if (playerEventClearData == null)
		{
			return false;
		}
		if (playerEventClearData.Series != base.Series)
		{
			return false;
		}
		if (playerEventClearData.EventID != EventID)
		{
			return false;
		}
		if (playerEventClearData.CourseID != CourseID)
		{
			return false;
		}
		if (playerEventClearData.StageNo != base.StageNo)
		{
			return false;
		}
		if (playerEventClearData.HightScore == base.HightScore && playerEventClearData.GotStars == base.GotStars)
		{
			return true;
		}
		return false;
	}

	public new PlayerEventClearData Clone()
	{
		return MemberwiseClone() as PlayerEventClearData;
	}

	public bool Update(PlayerEventClearData _rhs)
	{
		if (_rhs == null)
		{
			return false;
		}
		if (_rhs.Series != base.Series)
		{
			return false;
		}
		if (_rhs.EventID != EventID)
		{
			return false;
		}
		if (_rhs.CourseID != CourseID)
		{
			return false;
		}
		if (_rhs.StageNo != base.StageNo)
		{
			return false;
		}
		bool result = false;
		if (base.HightScore < _rhs.HightScore)
		{
			base.HightScore = _rhs.HightScore;
			result = true;
		}
		if (base.GotStars < _rhs.GotStars)
		{
			base.GotStars = _rhs.GotStars;
			return true;
		}
		return result;
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		base.SerializeToBinary(data);
		data.WriteInt(EventID);
		data.WriteShort(CourseID);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		base.DeserializeFromBinary(data, reqVersion);
		EventID = data.ReadInt();
		CourseID = data.ReadShort();
	}

	public override string SerializeToJson()
	{
		return string.Empty;
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
	}
}
