using System;

public class AdvMaxDugeonLevelSettings : ICloneable
{
	[MiniJSONAlias("difficulty")]
	public int difficulty { get; set; }

	[MiniJSONAlias("first")]
	public int first { get; set; }

	[MiniJSONAlias("maxlvl")]
	public int maxlvl { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvMaxDugeonLevelSettings Clone()
	{
		return MemberwiseClone() as AdvMaxDugeonLevelSettings;
	}
}
