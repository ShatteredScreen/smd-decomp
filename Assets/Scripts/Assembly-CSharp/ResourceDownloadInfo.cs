using System.Collections.Generic;

public class ResourceDownloadInfo
{
	public int mFmtVer { get; set; }

	public string mBaseURL { get; set; }

	public Dictionary<string, BIJDLData> mDict { get; set; }

	public ResourceDownloadInfo()
	{
		mFmtVer = 0;
		mBaseURL = string.Empty;
		mDict = new Dictionary<string, BIJDLData>();
	}

	public void Deserialize(BIJBinaryReader a_reader)
	{
		mFmtVer = a_reader.ReadInt();
		mBaseURL = a_reader.ReadUTF();
		int num = a_reader.ReadInt();
		for (int i = 0; i < num; i++)
		{
			BIJDLData bIJDLData = new BIJDLData();
			bIJDLData.DeserializeFromServerFile(a_reader, mFmtVer);
			mDict.Add(bIJDLData.BaseName, bIJDLData);
		}
	}
}
