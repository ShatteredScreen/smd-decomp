using System;
using System.Collections.Generic;

public class Options : ICloneable, IOptions
{
	public class PurchaseHistory
	{
		public long Time { get; set; }

		public float Price { get; set; }

		public string CurrencyCode { get; set; }
	}

	public short Version;

	public short AppVersion;

	public bool BGMEnable = true;

	public float BGMVolume;

	public bool SEEnable = true;

	public float SEVolume;

	private string mFbName;

	private string mGcName;

	private string mGpName;

	private string mTwName;

	private string mGpgsName;

	public bool ServerAlreadeyFirstSavedFlag;

	public bool ServerForceSaveRequestByBackup;

	public bool UseVibe = true;

	public bool UseNotification = true;

	public bool ComboCutIn = true;

	public DateTime GameStartTime { get; set; }

	public DateTime FirstPurchasedTime { get; set; }

	public DateTime LastPurchasedTime { get; set; }

	public List<PurchaseHistory> PurchaseList { get; set; }

	public long BuyHeartCount { get; set; }

	public long BuyBoostCount { get; set; }

	public long BuyContinueCount { get; set; }

	public long BuyUnlockRBCount { get; set; }

	public float PurchaseAmount
	{
		get
		{
			float num = 0f;
			long purchaseCountStartTime = SingletonMonoBehaviour<GameMain>.Instance.GameProfile.PurchaseCountStartTime;
			long purchaseCountEndTime = SingletonMonoBehaviour<GameMain>.Instance.GameProfile.PurchaseCountEndTime;
			for (int i = 0; i < PurchaseList.Count; i++)
			{
				PurchaseHistory purchaseHistory = PurchaseList[i];
				if (purchaseHistory.Time >= purchaseCountStartTime && purchaseHistory.Time <= purchaseCountEndTime)
				{
					num += purchaseHistory.Price;
				}
			}
			return num;
		}
	}

	public string CurrencyCode
	{
		get
		{
			string result = string.Empty;
			if (PurchaseList.Count > 0)
			{
				result = PurchaseList[0].CurrencyCode;
			}
			return result;
		}
	}

	public long PurchaseCount
	{
		get
		{
			long num = 0L;
			long purchaseCountStartTime = SingletonMonoBehaviour<GameMain>.Instance.GameProfile.PurchaseCountStartTime;
			long purchaseCountEndTime = SingletonMonoBehaviour<GameMain>.Instance.GameProfile.PurchaseCountEndTime;
			for (int i = 0; i < PurchaseList.Count; i++)
			{
				PurchaseHistory purchaseHistory = PurchaseList[i];
				if (purchaseHistory.Time >= purchaseCountStartTime && purchaseHistory.Time <= purchaseCountEndTime)
				{
					num++;
				}
			}
			return num;
		}
	}

	public long TotalMasterCurrency { get; set; }

	public long TotalSecondaryCurrency { get; set; }

	public long BuyAdvMedalCount { get; set; }

	public long BuyAdvBoosterCount { get; set; }

	public long BuyAdvContinueCount { get; set; }

	public long BuyAdvGasyaCount { get; set; }

	public bool AdvSortHigher { get; set; }

	public long AdvSortKind { get; set; }

	public int NetworkState { get; set; }

	public int DLThreadNum { get; set; }

	public int LBCacheDeleteID { get; set; }

	public string Fbid { get; set; }

	public string FbName
	{
		get
		{
			return mFbName;
		}
		set
		{
			mFbName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public string Gcid { get; set; }

	public string GcName
	{
		get
		{
			return mGcName;
		}
		set
		{
			mGcName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public string Gpid { get; set; }

	public string GpName
	{
		get
		{
			return mGpName;
		}
		set
		{
			mGpName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public string Twid { get; set; }

	public string TwName
	{
		get
		{
			return mTwName;
		}
		set
		{
			mTwName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public string Gpgsid { get; set; }

	public string GpgsName
	{
		get
		{
			return mGpgsName;
		}
		set
		{
			mGpgsName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public string Gender { get; set; }

	protected Options()
	{
	}

	public void AddPurchaseHistory(DateTime aLocalTime, float aPrice, string aCurrencyCode)
	{
		if (FirstPurchasedTime == DateTimeUtil.UnitLocalTimeEpoch)
		{
			FirstPurchasedTime = aLocalTime;
		}
		LastPurchasedTime = aLocalTime;
		long aUniversalTime = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(aLocalTime);
		AddPurchaseHistory(aUniversalTime, aPrice, aCurrencyCode);
	}

	private void AddPurchaseHistory(long aUniversalTime, float aPrice, string aCurrencyCode)
	{
		PurchaseHistory purchaseHistory = new PurchaseHistory();
		purchaseHistory.Time = aUniversalTime;
		purchaseHistory.Price = aPrice;
		purchaseHistory.CurrencyCode = aCurrencyCode;
		if (PurchaseList.Count > 0 && PurchaseList[0].CurrencyCode != aCurrencyCode)
		{
			PurchaseList.Clear();
		}
		PurchaseList.Add(purchaseHistory);
	}

	public object Clone()
	{
		return MemberwiseClone();
	}

	private void InitWork()
	{
	}

	public static Options CreateForNewPlayer()
	{
		Options options = new Options();
		options.Version = 1290;
		options.AppVersion = 1290;
		options.GameStartTime = DateTime.Now;
		options.FirstPurchasedTime = DateTimeUtil.UnitLocalTimeEpoch;
		options.LastPurchasedTime = DateTimeUtil.UnitLocalTimeEpoch;
		options.PurchaseList = new List<PurchaseHistory>();
		options.BuyHeartCount = 0L;
		options.BuyBoostCount = 0L;
		options.BuyContinueCount = 0L;
		options.BuyUnlockRBCount = 0L;
		options.TotalMasterCurrency = 0L;
		options.TotalSecondaryCurrency = 0L;
		options.BuyAdvMedalCount = 0L;
		options.BuyAdvBoosterCount = 0L;
		options.BuyAdvContinueCount = 0L;
		options.BuyAdvGasyaCount = 0L;
		options.AdvSortHigher = false;
		options.AdvSortKind = 0L;
		options.NetworkState = 0;
		options.DLThreadNum = 0;
		options.LBCacheDeleteID = 0;
		options.BGMEnable = true;
		options.BGMVolume = Constants.DEF_BGM_VOL;
		options.SEEnable = true;
		options.SEVolume = Constants.DEF_SE_VOL;
		options.Fbid = string.Empty;
		options.FbName = string.Empty;
		options.Gcid = string.Empty;
		options.GcName = string.Empty;
		options.Gpid = string.Empty;
		options.GpName = string.Empty;
		options.Twid = string.Empty;
		options.TwName = string.Empty;
		options.Gpgsid = string.Empty;
		options.GpgsName = string.Empty;
		options.Gender = string.Empty;
		options.ServerAlreadeyFirstSavedFlag = false;
		options.ServerForceSaveRequestByBackup = false;
		options.UseVibe = false;
		options.UseNotification = true;
		options.ComboCutIn = true;
		return options;
	}

	private static string ToSS(object o)
	{
		if (o == null)
		{
			return string.Empty;
		}
		return o.ToString();
	}

	public void Serialize(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		data.WriteShort(1290);
		data.WriteDateTime(GameStartTime);
		data.WriteDateTime(FirstPurchasedTime);
		data.WriteDateTime(LastPurchasedTime);
		long num = (long)DateTimeUtil.LoacalDateTimeToUnixTimeStamp(DateTime.Today.AddMonths(-6));
		for (int num2 = PurchaseList.Count - 1; num2 >= 0; num2--)
		{
			if (PurchaseList[num2].Time < num)
			{
				PurchaseList.RemoveAt(num2);
			}
		}
		data.WriteInt(PurchaseList.Count);
		for (int i = 0; i < PurchaseList.Count; i++)
		{
			data.WriteLong(PurchaseList[i].Time);
			data.WriteFloat(PurchaseList[i].Price);
			data.WriteUTF(PurchaseList[i].CurrencyCode);
		}
		data.WriteLong(BuyHeartCount);
		data.WriteLong(BuyBoostCount);
		data.WriteLong(BuyContinueCount);
		data.WriteLong(BuyUnlockRBCount);
		data.WriteLong(TotalMasterCurrency);
		data.WriteLong(TotalSecondaryCurrency);
		data.WriteLong(BuyAdvMedalCount);
		data.WriteLong(BuyAdvBoosterCount);
		data.WriteLong(BuyAdvContinueCount);
		data.WriteLong(BuyAdvGasyaCount);
		data.WriteBool(AdvSortHigher);
		data.WriteLong(AdvSortKind);
		data.WriteInt(NetworkState);
		data.WriteInt(DLThreadNum);
		data.WriteInt(LBCacheDeleteID);
		data.WriteBool(BGMEnable);
		data.WriteFloat(BGMVolume);
		data.WriteBool(SEEnable);
		data.WriteFloat(SEVolume);
		data.WriteUTF(Fbid);
		data.WriteUTF(FbName);
		data.WriteUTF(Gcid);
		data.WriteUTF(GcName);
		data.WriteUTF(Gpid);
		data.WriteUTF(GpName);
		data.WriteUTF(Twid);
		data.WriteUTF(TwName);
		data.WriteUTF(Gpgsid);
		data.WriteUTF(GpgsName);
		data.WriteUTF(Gender);
		data.WriteBool(ServerAlreadeyFirstSavedFlag);
		data.WriteBool(ServerForceSaveRequestByBackup);
		data.WriteBool(UseVibe);
		data.WriteBool(UseNotification);
		data.WriteBool(ComboCutIn);
		SerializeEnglishVersion(data, 1290);
	}

	public static Options Deserialize(BIJBinaryReader data)
	{
		Options p = new Options();
		p.Version = data.ReadShort();
		p.AppVersion = data.ReadShort();
		p.GameStartTime = data.ReadDateTime();
		p.FirstPurchasedTime = data.ReadDateTime();
		p.LastPurchasedTime = data.ReadDateTime();
		p.PurchaseList = new List<PurchaseHistory>();
		if (p.Version >= 1041)
		{
			int num = data.ReadInt();
			for (int i = 0; i < num; i++)
			{
				long aUniversalTime = data.ReadLong();
				float aPrice = data.ReadFloat();
				string aCurrencyCode = data.ReadUTF();
				p.AddPurchaseHistory(aUniversalTime, aPrice, aCurrencyCode);
			}
			p.BuyHeartCount = data.ReadLong();
			p.BuyBoostCount = data.ReadLong();
			p.BuyContinueCount = data.ReadLong();
			p.BuyUnlockRBCount = data.ReadLong();
		}
		else
		{
			data.ReadLong();
		}
		p.TotalMasterCurrency = data.ReadLong();
		p.TotalSecondaryCurrency = data.ReadLong();
		if (p.Version >= 1100)
		{
			p.BuyAdvMedalCount = data.ReadLong();
			p.BuyAdvBoosterCount = data.ReadLong();
			p.BuyAdvContinueCount = data.ReadLong();
			p.BuyAdvGasyaCount = data.ReadLong();
		}
		if (p.Version >= 1110)
		{
			p.AdvSortHigher = data.ReadBool();
			p.AdvSortKind = data.ReadLong();
		}
		if (p.Version >= 1120)
		{
			p.NetworkState = data.ReadInt();
			p.DLThreadNum = data.ReadInt();
		}
		if (p.Version >= 1130)
		{
			p.LBCacheDeleteID = data.ReadInt();
		}
		p.BGMEnable = data.ReadBool();
		p.BGMVolume = data.ReadFloat();
		p.SEEnable = data.ReadBool();
		p.SEVolume = data.ReadFloat();
		p.Fbid = ToSS(data.ReadUTF());
		p.FbName = ToSS(data.ReadUTF());
		p.Gcid = ToSS(data.ReadUTF());
		p.GcName = ToSS(data.ReadUTF());
		p.Gpid = ToSS(data.ReadUTF());
		p.GpName = ToSS(data.ReadUTF());
		p.Twid = ToSS(data.ReadUTF());
		p.TwName = ToSS(data.ReadUTF());
		p.Gpgsid = ToSS(data.ReadUTF());
		p.GpgsName = ToSS(data.ReadUTF());
		p.Gender = ToSS(data.ReadUTF());
		p.ServerAlreadeyFirstSavedFlag = data.ReadBool();
		p.ServerForceSaveRequestByBackup = data.ReadBool();
		p.UseVibe = data.ReadBool();
		p.UseNotification = data.ReadBool();
		if (p.Version >= 1230)
		{
			p.ComboCutIn = data.ReadBool();
		}
		DeserializeEnglishVersion(data, ref p, p.Version);
		return p;
	}

	private void SerializeEnglishVersion(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteBool(true);
	}

	private static void DeserializeEnglishVersion(BIJBinaryReader data, ref Options p, int reqVersion)
	{
		if (reqVersion >= 1041 && data.ReadBool())
		{
		}
	}
}
