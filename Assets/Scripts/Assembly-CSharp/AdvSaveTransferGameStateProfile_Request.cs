public class AdvSaveTransferGameStateProfile_Request : RequestBase
{
	[MiniJSONAlias("fbid")]
	public string FaceBookID { get; set; }

	[MiniJSONAlias("max_quest_id")]
	public int MaxQuestID { get; set; }

	[MiniJSONAlias("xp")]
	public int Experience { get; set; }

	[MiniJSONAlias("mc")]
	public int MainCurrency { get; set; }

	[MiniJSONAlias("sc")]
	public int SubCurrency { get; set; }
}
