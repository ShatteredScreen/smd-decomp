public class AdvGashaInfoDetail
{
	[MiniJSONAlias("gasha_id")]
	public int gasha_id { get; set; }

	[MiniJSONAlias("price_id")]
	public int price_id { get; set; }

	[MiniJSONAlias("price_type")]
	public string price_type { get; set; }

	[MiniJSONAlias("item_id")]
	public int item_id { get; set; }

	[MiniJSONAlias("default_quantity")]
	public int default_quantity { get; set; }

	[MiniJSONAlias("quantity")]
	public int quantity { get; set; }

	[MiniJSONAlias("discount_type")]
	public string discount_type { get; set; }

	[MiniJSONAlias("discount_limit")]
	public int discount_limit { get; set; }

	[MiniJSONAlias("is_play")]
	public int is_play { get; set; }

	[MiniJSONAlias("play_count")]
	public int play_count { get; set; }
}
