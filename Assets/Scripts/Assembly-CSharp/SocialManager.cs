using System.Collections.Generic;
using UnityEngine;

public class SocialManager : MonoBehaviour
{
	private static object mInstanceLock = new object();

	private static SocialManager mInstance = null;

	private Dictionary<SNS, BIJSNS> mSNS = new Dictionary<SNS, BIJSNS>();

	public static SocialManager Instance
	{
		get
		{
			lock (mInstanceLock)
			{
				if (mInstance == null)
				{
					GameObject gameObject = GameObject.Find("Network");
					mInstance = (SocialManager)gameObject.GetComponent(typeof(SocialManager));
				}
				return mInstance;
			}
		}
	}

	public int Count
	{
		get
		{
			return mSNS.Count;
		}
	}

	public BIJSNS this[SNS _kind]
	{
		get
		{
			BIJSNS value = null;
			if (mSNS.TryGetValue(_kind, out value) || value != null)
			{
				return value;
			}
			return new BIJSNSNone();
		}
	}

	public bool IsNetworking
	{
		get
		{
			foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
			{
				BIJSNS value = mSN.Value;
				if (value == null || !value.IsNetworking)
				{
					continue;
				}
				return true;
			}
			return false;
		}
	}

	private void Awake()
	{
		Init(base.gameObject, this);
	}

	private void OnDestroy()
	{
		Stop();
	}

	private void OnApplicationQuit()
	{
		Term();
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			OnSuspned();
		}
		else
		{
			OnResume();
		}
	}

	public void Init(GameObject go, MonoBehaviour mb)
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		Term();
		if (go != null)
		{
			Object.DontDestroyOnLoad(go);
		}
		mSNS[SNS.Facebook] = new BIJSNSFacebook();
		mSNS[SNS.GooglePlus] = new BIJSNSGooglePlusAndroid();
		mSNS[SNS.LINE] = new BIJSNSLINE();
		mSNS[SNS.Twitter] = new BIJSNSTwitter();
		mSNS[SNS.Instagram] = new BIJSNSInstagram();
		mSNS[SNS.SMS] = new BIJSNSSMS();
		mSNS[SNS.Mail] = new BIJSNSMail();
		mSNS[SNS.GooglePlayGameServices] = new BIJSNSGPGSAndroid();
		foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
		{
			BIJSNS value = mSN.Value;
			if (value != null)
			{
				value.Init(go, mb);
			}
		}
		float num = Time.realtimeSinceStartup - realtimeSinceStartup;
	}

	public void OnSetup()
	{
		OnResume();
	}

	public void Term()
	{
		foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
		{
			BIJSNS value = mSN.Value;
			if (value != null)
			{
				value.Term();
			}
		}
		mSNS.Clear();
	}

	public void Update()
	{
		foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
		{
			BIJSNS value = mSN.Value;
			if (value != null)
			{
				value.Update();
			}
		}
	}

	public void Start()
	{
		foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
		{
			BIJSNS value = mSN.Value;
			if (value != null)
			{
				value.Start();
			}
		}
	}

	public void Stop()
	{
		foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
		{
			BIJSNS value = mSN.Value;
			if (value != null)
			{
				value.Stop();
			}
		}
	}

	public void OnResume()
	{
		foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
		{
			BIJSNS value = mSN.Value;
			if (value != null)
			{
				value.OnResume();
			}
		}
	}

	public void OnSuspned()
	{
		foreach (KeyValuePair<SNS, BIJSNS> mSN in mSNS)
		{
			BIJSNS value = mSN.Value;
			if (value != null)
			{
				value.OnSuspned();
			}
		}
	}
}
