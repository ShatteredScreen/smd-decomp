using System;

[Serializable]
public class SsUserDataKeyValue : SsAttrValueInterface
{
	public bool IsNum;

	public int Num;

	public bool IsRect;

	public SsRect Rect;

	public bool IsPoint;

	public SsPoint Point;

	public bool IsString;

	public string String;

	public SsUserDataKeyValue()
	{
	}

	public SsUserDataKeyValue(SsUserDataKeyValue r)
	{
		IsNum = r.IsNum;
		Num = r.Num;
		IsRect = r.IsRect;
		Rect = r.Rect.Clone();
		IsPoint = r.IsPoint;
		Point = r.Point.Clone();
		IsString = r.IsString;
		String = string.Copy(r.String);
	}

	public SsAttrValueInterface Clone()
	{
		return new SsUserDataKeyValue(this);
	}

	public override string ToString()
	{
		return string.Concat("IsNum: ", IsNum, ", Num: ", Num, ", IsRect: ", IsRect, ", Rect: ", Rect, ", IsPoint: ", IsPoint, ", Point: ", Point, ", IsString: ", IsString, ", String: ", String);
	}
}
