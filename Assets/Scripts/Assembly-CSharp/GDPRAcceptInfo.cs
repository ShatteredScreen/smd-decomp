public class GDPRAcceptInfo
{
	public int mKind { get; set; }

	public bool mAccept { get; set; }

	public long mDate { get; set; }

	public string mAdjustKey { get; set; }

	public GDPRAcceptInfo()
	{
		mKind = -1;
		mAccept = false;
		mDate = 0L;
		mAdjustKey = string.Empty;
	}
}
