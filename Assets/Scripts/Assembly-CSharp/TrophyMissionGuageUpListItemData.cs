using UnityEngine;

public class TrophyMissionGuageUpListItemData
{
	public GameObject itemGo;

	public GameObject boardImage;

	public TrophyMissionGuageUpListItem data;
}
