using System;

[Flags]
public enum SsSoundKeyFlags
{
	Note = 1,
	Volume = 2,
	UserData = 4
}
