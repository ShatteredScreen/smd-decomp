using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateTrophy : GameState
{
	public enum STATE
	{
		MAIN = 0,
		LOAD_WAIT = 1,
		UNLOAD_WAIT = 2,
		WAIT = 3,
		NETWORK_LOGIN00 = 4,
		NETWORK_LOGIN01 = 5,
		NETWORK_00_00 = 6,
		NETWORK_00_01 = 7,
		NETWORK_01_00 = 8,
		NETWORK_01_01 = 9,
		NETWORK_02_00 = 10,
		NETWORK_02_01 = 11
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private List<GiftListItem> mAllGiftItems = new List<GiftListItem>();

	private SMVerticalListInfo mListInfo;

	private SMVerticalList mItemList;

	private FriendItem mFriendItem;

	private List<FriendItem> mAllFriendItems = new List<FriendItem>();

	private List<MyFriend> sendableFriends;

	private List<TrophyItemListData> mTrophyItemListData = new List<TrophyItemListData>();

	private List<TrophyItemMissionData> mTrophyItemListMissionData = new List<TrophyItemMissionData>();

	private TrophyItemGaugeMove mTrophyItemGaugeMove = new TrophyItemGaugeMove();

	private List<int> mNotClearMissionID = new List<int>();

	private List<BatchClearInfo> mCollectBatchList = new List<BatchClearInfo>();

	private GameObject mTabParent;

	private UIButton mTabMission;

	private UIButton mTabCleared;

	private UIButton mTabExchange;

	private UISprite mTabMissionCheck;

	private UISprite mTabClearedCheck;

	private UISprite mTabExchangeCheck;

	private UIButton mBatchReceiveButton;

	private UISprite mSpriteTrophyIcon;

	private UISprite mSpriteTrophyText;

	private UISprite mNumFrame;

	private UISprite mSelectedTitleFrame;

	private UISprite mSelectedTitle;

	private UISprite mAttentionReceive;

	private UISprite mAttentionSend;

	private UISprite mAttentionRescue;

	private float mAttentionReceiveCount;

	private float mAttentionSendCount;

	private float mAttentionRescueCount;

	private UILabel mNumDesc;

	private UILabel mExchangeDesc;

	private UISprite mBackground;

	private UISprite mCurtain;

	private float mCurtainHeight = 230f;

	private BIJSNS mSNS;

	private int mSNSCallCount;

	private bool mAllData;

	private string mImageTab0;

	private string mImageTab1;

	private string mImageTab2;

	private string mImageCheck = "button_check";

	private UIButton mExitButton;

	private NumberImageString mNumberString;

	private bool mEffectFlg;

	private UISsSprite mUISsAnime;

	private UIPanel mUISsAnimePanel;

	private SMMapPage mCurrentPage;

	private TrophyMode Mode;

	private TrophyMode _mode;

	private ConfirmDialog mConfirmDialog;

	private GetPartnerDialog mGetPartnerDialog;

	private TrophyExchangeDialog mTrophyExchangeDialog;

	private TrophyDetailDialog mTrophyDetailDialog;

	private TrophyAlreadyPartnerDialog mTrophyAlreadyPartnerDialog;

	private TrophyDetailPartnerDialog mTrophyDetailPartnerDialog;

	private GrowupPartnerSelectDialog mGrowupDialog;

	private GrowupPartnerConfirmDialog mGrowupPartnerConfirmtDialog;

	private GrowupResultDialog mGrowupResultDialog;

	private GetStampDialog mGetStampDialog;

	private GetAccessoryDialog mGetAccessoryDialog;

	private SMVerticalComponent mComponent;

	private TrophyMissionClearDialog mTrophyMissionClearDialog;

	private ScreenOrientation mScreenOrientation = ScreenOrientation.Portrait;

	private TrophyItemMissionListItem mMissionClearSelectItem;

	private TrophyItemListItem mSelectItem;

	private GameStateManager mGameState;

	private int mBaseDepth;

	private float buttonX = 116f;

	private float buttonY = -37f;

	private bool mMissionAddFlg = true;

	private bool mMissionCollctFlg = true;

	private int mSpritePanelSize;

	private GameObject mAnchorTop;

	private GameObject mAnchorTopLeft;

	private GameObject mAnchorTopRight;

	private GameObject mAnchorBottomLeft;

	private GameObject mAnchorBottomRight;

	private bool mTrophyTutorial;

	private bool mDetailPartnerFlg;

	private bool mLaceMove;

	private bool mEffectWaitFlg;

	private bool mRotationMissionFlg;

	private bool mRotationExchangeFlg;

	private bool mTrophyChangeNumFlg;

	private float mCounter;

	public override void Start()
	{
		Mode = TrophyMode.MISSION;
		base.Start();
		mGameState = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}

	public override void Update()
	{
		base.Update();
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (mGame.mBackKeyTrg)
			{
				OnBackPushed();
			}
			break;
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				if (!mTrophyTutorial)
				{
					mState.Change(STATE.MAIN);
				}
				else
				{
					mState.Reset(STATE.WAIT, true);
				}
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		}
		mState.Update();
	}

	public void OnTutorialMessageFinished(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = ((!mGame.mTutorialManager.IsTutorialCompleted(index)) ? mGame.mTutorialManager.TutorialComplete(index) : Def.TUTORIAL_INDEX.NONE);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE && selectItem != TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			TutorialStart(tUTORIAL_INDEX);
		}
		else if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.TutorialSkip(index);
		}
		AccessoryData accessoryData = mGame.mAccessoryData[1122];
		mGame.mPlayer.AddAccessory(accessoryData, true, true);
		int gotIDByNum = accessoryData.GetGotIDByNum();
		ServerCram.GetTrophy(gotIDByNum, mGame.mPlayer.GetTrophy(), 2, 1122, mGame.mOptions.PurchaseCount);
		if (mNumberString != null)
		{
			UnityEngine.Object.Destroy(mNumberString.gameObject);
			mNumberString = null;
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			mNumberString = Util.CreateGameObject("Number", mNumFrame.gameObject).AddComponent<NumberImageString>();
			mNumberString.Init(6, mGame.mPlayer.GetTrophy(), mBaseDepth + 5, 0.7f, UIWidget.Pivot.Right, false, image);
			mNumberString.transform.localPosition = new Vector3(85f, 0f, 0f);
		}
		mState.Change(STATE.MAIN);
		mGame.Save();
	}

	private void TutorialStart(Def.TUTORIAL_INDEX index)
	{
		mGame.mTutorialManager.TutorialStart(index, mGame.mAnchor.gameObject, OnTutorialMessageClosed, OnTutorialMessageFinished);
	}

	public void OnTutorialMessageClosed(Def.TUTORIAL_INDEX index, TutorialMessageDialog.SELECT_ITEM selectItem)
	{
		Def.TUTORIAL_INDEX tUTORIAL_INDEX = mGame.mTutorialManager.TutorialComplete(index);
		if (tUTORIAL_INDEX != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialStart(tUTORIAL_INDEX);
		}
		if (selectItem == TutorialMessageDialog.SELECT_ITEM.SKIP)
		{
			mGame.mTutorialManager.DeleteTutorialPointArrow();
			mGame.mTutorialManager.TutorialSkip(index);
		}
	}

	private IEnumerator LaceRotate(GameObject parent, int depth, Vector3 pos, Vector3 scale, float speed)
	{
		ResImage resImageCollection = ResourceManager.LoadImage("TROPHY");
		UISprite sprite = Util.CreateSprite("Lace", parent, resImageCollection.Image);
		Util.SetSpriteInfo(sprite, "characolle_back_lace02", depth, pos, scale, false);
		float angle = 0f;
		while (mLaceMove)
		{
			angle += Time.deltaTime * speed;
			sprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle);
			yield return 0;
		}
	}

	public void TrophyTutorialStart()
	{
		mGame.mTutorialManager.TutorialStart(Def.TUTORIAL_INDEX.TROPHY_TUT_0, mRoot, OnTutorialMessageFinished, OnTutorialMessageClosed);
		mTrophyTutorial = false;
	}

	public override IEnumerator LoadGameState()
	{
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		mAnchorTop = Util.CreateAnchorWithChild("AnchorTop", camera.gameObject, UIAnchor.Side.Top, camera).gameObject;
		mAnchorTopLeft = Util.CreateAnchorWithChild("AnchorTopLeft", camera.gameObject, UIAnchor.Side.TopLeft, camera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("AnchorTopRight", camera.gameObject, UIAnchor.Side.TopRight, camera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("AnchorBottomLeft", camera.gameObject, UIAnchor.Side.BottomLeft, camera).gameObject;
		mAnchorBottomRight = Util.CreateAnchorWithChild("AnchorBottomRight", camera.gameObject, UIAnchor.Side.BottomRight, camera).gameObject;
		BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
		UIFont font = GameMain.LoadFont();
		ResImage resImage = ResourceManager.LoadImageAsync("TROPHY");
		while (resImage.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		BIJImage trophyAtlas = ResourceManager.LoadImage("TROPHY").Image;
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.TROPHY_TUT_3))
		{
			mTrophyTutorial = true;
		}
		ResSound resSound = ResourceManager.LoadSoundAsync("BGM_COLLECTION");
		while (resSound.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		mBackground = Util.CreateSprite("BackGround", mRoot, trophyAtlas);
		Util.SetSpriteInfo(mBackground, "trophy_back", mBaseDepth, Vector3.zero, Vector3.one, false);
		mBackground.type = UIBasicSprite.Type.Tiled;
		mLaceMove = true;
		StartCoroutine(LaceRotate(mAnchorTopRight.gameObject, mBaseDepth + 1, new Vector3(30f, -110f, 0f), new Vector3(1f, 1f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(190f, -50f, 0f), new Vector3(0.8f, 0.8f, 1f), 15f));
		StartCoroutine(LaceRotate(mAnchorBottomLeft.gameObject, mBaseDepth + 1, new Vector3(-20f, 60f, 0f), new Vector3(1f, 1f, 1f), 15f));
		Vector2 logScreen = Util.LogScreenSize();
		string imageName2 = "menubar_frame";
		UISprite sprite2 = Util.CreateSprite("TitleBarR", mAnchorTop.gameObject, atlas);
		Util.SetSpriteInfo(sprite2, "menubar_frame", mBaseDepth + 9, new Vector3(-2f, 173f, 0f), new Vector3(1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		sprite2 = Util.CreateSprite("TitleBarL", mAnchorTop.gameObject, atlas);
		Util.SetSpriteInfo(sprite2, "menubar_frame", mBaseDepth + 9, new Vector3(2f, 173f, 0f), new Vector3(-1f, -1f, 1f), false, UIWidget.Pivot.BottomLeft);
		string title = Localization.Get("Ranking_TrophyNum");
		UILabel labelTitle = Util.CreateLabel("Title", mAnchorTop.gameObject, font);
		Util.SetLabelInfo(labelTitle, title, mBaseDepth + 10, new Vector3(0f, -26f, 0f), 36, 0, 0, UIWidget.Pivot.Center);
		labelTitle.color = Color.white;
		imageName2 = "trophy_back_curtain00";
		mCurtain = Util.CreateSprite("Curtain", mAnchorTop, trophyAtlas);
		Util.SetSpriteInfo(mCurtain, imageName2, mBaseDepth + 3, Vector3.zero, Vector3.one, false);
		mCurtain.type = UIBasicSprite.Type.Tiled;
		mCurtain.SetDimensions((int)logScreen.x, (int)mCurtainHeight);
		mSpriteTrophyIcon = Util.CreateSprite("TrophyIcon", mCurtain.gameObject, trophyAtlas);
		Util.SetSpriteInfo(mSpriteTrophyIcon, "icon_trophy", mBaseDepth + 4, new Vector3(-265f, -19f, 0f), Vector3.one, false);
		mSpriteTrophyText = Util.CreateSprite("TrophyDesc", mCurtain.gameObject, trophyAtlas);
		Util.SetSpriteInfo(mSpriteTrophyText, "LC_text_trophy", mBaseDepth + 4, new Vector3(0f, -12f, 0f), Vector3.one, false);
		mNumFrame = Util.CreateSprite("NumFrame", mCurtain.gameObject, trophyAtlas);
		Util.SetSpriteInfo(mNumFrame, "panel_trophy", mBaseDepth + 4, new Vector3(74f, -44f, 0f), Vector3.one, false);
		mNumFrame.SetDimensions(210, 52);
		mNumFrame.type = UIBasicSprite.Type.Sliced;
		string str2 = string.Format(Localization.Get("TrophyPage_Nop"));
		mNumDesc = Util.CreateLabel("NumDesc", mNumFrame.gameObject, font);
		Util.SetLabelInfo(mNumDesc, str2, mBaseDepth + 4, new Vector3(-182f, -1f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		mNumDesc.color = Color.white;
		mNumberString = Util.CreateGameObject("Number", mNumFrame.gameObject).AddComponent<NumberImageString>();
		mNumberString.Init(6, mGame.mPlayer.GetTrophy(), mBaseDepth + 5, 0.7f, UIWidget.Pivot.Right, false, atlas);
		mNumberString.transform.localPosition = new Vector3(85f, 0f, 0f);
		mSelectedTitleFrame = Util.CreateSprite("SelectedTitleFrame", mRoot, trophyAtlas);
		Util.SetSpriteInfo(mSelectedTitleFrame, "panel_mission", mBaseDepth + 8, new Vector3(0f, 330f, 0f), Vector3.one, false);
		mSelectedTitleFrame.SetDimensions(315, 58);
		mSelectedTitleFrame.type = UIBasicSprite.Type.Sliced;
		mSelectedTitle = Util.CreateSprite("SelectedTitle", mSelectedTitleFrame.gameObject, trophyAtlas);
		Util.SetSpriteInfo(mSelectedTitle, "LC_text_mission", mBaseDepth + 9, new Vector3(0f, 0f, 0f), Vector3.one, false);
		str2 = string.Format(Localization.Get("TrophyPageExcgange_Desc"));
		mExchangeDesc = Util.CreateLabel("ExchangeDesc", mRoot, font);
		Util.SetLabelInfo(mExchangeDesc, str2, mBaseDepth + 4, new Vector3(0f, -61f, 0f), 21, 0, 0, UIWidget.Pivot.Center);
		mExchangeDesc.color = Color.white;
		mTabParent = Util.CreateGameObject("TabParent", mAnchorBottomRight);
		mTabParent.transform.localScale = new Vector3(0.95f, 0.95f);
		mTabMission = Util.CreateJellyImageButton("TabMission", mTabParent, trophyAtlas);
		Util.SetImageButtonInfo(mTabMission, mImageTab0, mImageTab0, mImageTab0, mBaseDepth + 1, new Vector3(-440f, 62f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabMission, this, "OnTabMissionPushed", UIButtonMessage.Trigger.OnClick);
		mTabMissionCheck = Util.CreateSprite("Check", mTabMission.gameObject, trophyAtlas);
		Util.SetSpriteInfo(mTabMissionCheck, "button_check", mBaseDepth + 8, new Vector3(-73f, -4f, 0f), Vector3.one, false);
		mTabCleared = Util.CreateJellyImageButton("TabCleared", mTabParent, trophyAtlas);
		Util.SetImageButtonInfo(mTabCleared, mImageTab1, mImageTab1, mImageTab1, mBaseDepth + 1, new Vector3(-270f, 62f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabCleared, this, "OnTabClearedPushed", UIButtonMessage.Trigger.OnClick);
		mTabClearedCheck = Util.CreateSprite("Check", mTabCleared.gameObject, trophyAtlas);
		Util.SetSpriteInfo(mTabClearedCheck, "button_check", mBaseDepth + 8, new Vector3(-73f, -4f, 0f), Vector3.one, false);
		mTabExchange = Util.CreateJellyImageButton("TabExchange", mTabParent, trophyAtlas);
		Util.SetImageButtonInfo(mTabExchange, mImageTab2, mImageTab2, mImageTab2, mBaseDepth + 1, new Vector3(-100f, 62f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabExchange, this, "OnTabExchangePushed", UIButtonMessage.Trigger.OnClick);
		mTabExchangeCheck = Util.CreateSprite("Check", mTabExchange.gameObject, trophyAtlas);
		Util.SetSpriteInfo(mTabExchangeCheck, "button_check", mBaseDepth + 8, new Vector3(-73f, -4f, 0f), Vector3.one, false);
		string btnName = "button_exit";
		if (mGameState.PrevState == GameStateManager.GAME_STATE.ADVMENU)
		{
			btnName = "button_back";
		}
		mExitButton = Util.CreateJellyImageButton("ExitButton", mAnchorBottomLeft.gameObject, atlas);
		Util.SetImageButtonInfo(mExitButton, btnName, btnName, btnName, mBaseDepth + 5, new Vector3(60f, 60f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mExitButton, this, "OnCancelPushed", UIButtonMessage.Trigger.OnClick);
		mUISsAnimePanel = Util.CreatePanel("EffectPanel", mRoot, 0, 0);
		mBatchReceiveButton = Util.CreateJellyImageButton("BatchReceiveButton", mRoot, trophyAtlas);
		Util.SetImageButtonInfo(mBatchReceiveButton, "LC_button_collect", "LC_button_collect", "LC_button_collect", mBaseDepth + 1, new Vector3(0f, -412f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mBatchReceiveButton, this, "OnBatchReceivePushed", UIButtonMessage.Trigger.OnClick);
		if (MissionManager.EnableGetReward())
		{
			mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
		}
		else
		{
			mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		_mode = Mode;
		if (GameMain.USE_DEBUG_DIALOG && !GameMain.HIDDEN_DEBUG_BUTTON)
		{
			UIButton button = Util.CreateJellyImageButton(atlas: ResourceManager.LoadImage("HUD").Image, name: "DebugButton", parent: mAnchorTopLeft.gameObject, a_type: Util.BUTTON_TYPE.NORMAL2);
			Util.SetImageButtonInfo(button, "button_debug", "button_debug", "button_debug", mBaseDepth + 20, new Vector3(30f, -256f, 0f), Vector3.one);
			Util.AddImageButtonMessage(button, this, "OnDebugPushed", UIButtonMessage.Trigger.OnClick);
		}
		ChangeMode(_mode, true);
		mGame.PlayMusic("BGM_COLLECTION", 0, true, false, true);
		mGame.EventProfile.ConvertTrophyExchangeSetting();
		StartCoroutine(StartButtonJumpCount());
		yield return 0;
	}

	public void OnDebugPushed()
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mGame.StopMusic(0, 0.2f);
			SMDDebugDialog sMDDebugDialog = Util.CreateGameObject("DebugMenu", mRoot).AddComponent<SMDDebugDialog>();
			sMDDebugDialog.SetGameState(this);
			sMDDebugDialog.SetClosedCallback(delegate
			{
				mGame.PlayMusic("BGM_PUZZLE_CHANCE", 0, true);
			});
		}
	}

	public override IEnumerator UnloadGameState()
	{
		mLaceMove = false;
		ResourceManager.UnloadSound("BGM_COLLECTION");
		if ((bool)mAnchorTop)
		{
			UnityEngine.Object.Destroy(mAnchorTop.gameObject);
		}
		if ((bool)mAnchorTopLeft)
		{
			UnityEngine.Object.Destroy(mAnchorTopLeft.gameObject);
		}
		if ((bool)mAnchorTopRight)
		{
			UnityEngine.Object.Destroy(mAnchorTopRight.gameObject);
		}
		if ((bool)mAnchorBottomLeft)
		{
			UnityEngine.Object.Destroy(mAnchorBottomLeft.gameObject);
		}
		if ((bool)mAnchorBottomRight)
		{
			UnityEngine.Object.Destroy(mAnchorBottomRight.gameObject);
		}
		mGame.UnloadNotPreloadSoundResources(true);
		ResourceManager.UnloadSsAnimationAll();
		ResourceManager.UnloadImageAll();
		yield return 0;
		UnloadGameStateFinished();
		yield return 0;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		mScreenOrientation = o;
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			float num2 = 63f;
			mTabParent.transform.parent = mAnchorBottomRight.transform;
			mTabParent.transform.localPosition = new Vector3(0f, 0f);
			if (Util.IsWideScreen())
			{
				mTabParent.transform.localScale = new Vector3(0.88f, 0.88f);
			}
			else
			{
				mTabParent.transform.localScale = new Vector3(0.95f, 0.95f);
			}
			mTabMission.transform.localPosition = new Vector3(-440f, 62f);
			mTabCleared.transform.localPosition = new Vector3(-270f, 62f);
			mTabExchange.transform.localPosition = new Vector3(-100f, 62f);
			mImageTab0 = "LC_button_mission";
			mImageTab1 = "LC_button_mission_settled";
			mImageTab2 = "LC_button_prize";
			Util.SetImageButtonGraphic(mTabMission, mImageTab0, mImageTab0, mImageTab0);
			Util.SetImageButtonGraphic(mTabCleared, mImageTab1, mImageTab1, mImageTab1);
			Util.SetImageButtonGraphic(mTabExchange, mImageTab2, mImageTab2, mImageTab2);
			mCurtain.SetDimensions((int)vector.x, (int)mCurtainHeight);
			mSpriteTrophyText.transform.localPosition = new Vector3(44f, -12f, 0f);
			mNumFrame.transform.localPosition = new Vector3(116f, -57f, 0f);
			mSpriteTrophyIcon.transform.localPosition = new Vector3(-240f, -30f, 0f);
			mSpriteTrophyIcon.transform.localScale = new Vector3(1f, 1f, 1f);
			mSelectedTitleFrame.transform.localPosition = new Vector3(0f, 330f, 0f);
			mSpritePanelSize = 315;
			mItemList.OnScreenChanged(true);
			mItemList.transform.localPosition = new Vector3(0f, -50f);
			mExchangeDesc.transform.localPosition = new Vector3(0f, mListInfo.portrait_height / 2f + mItemList.transform.localPosition.y + 15f);
			mBatchReceiveButton.transform.localPosition = new Vector3(0f, (0f - mListInfo.portrait_height) / 2f + mItemList.transform.localPosition.y - 50f, 0f);
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			float num = 343f;
			mTabParent.transform.parent = mRoot.transform;
			mTabParent.transform.localPosition = new Vector3(-300f, 0f);
			mTabParent.transform.localScale = new Vector3(0.95f, 0.95f);
			mTabMission.transform.localPosition = new Vector3(-110f, 0f);
			mTabCleared.transform.localPosition = new Vector3(95f, 0f);
			mTabExchange.transform.localPosition = new Vector3(-110f, -100f);
			mImageTab0 = "LC_button_mission";
			mImageTab1 = "LC_button_mission_settled";
			mImageTab2 = "LC_button_prize";
			Util.SetImageButtonGraphic(mTabMission, mImageTab0, mImageTab0, mImageTab0);
			Util.SetImageButtonGraphic(mTabCleared, mImageTab1, mImageTab1, mImageTab1);
			Util.SetImageButtonGraphic(mTabExchange, mImageTab2, mImageTab2, mImageTab2);
			mCurtain.SetDimensions((int)vector.x, 196);
			mSpriteTrophyText.transform.localPosition = new Vector3((float)((int)(0f - vector.x) / 2) + 413f, -40f, 0f);
			mNumFrame.transform.localPosition = new Vector3(350f, -40f, 0f);
			mSpriteTrophyIcon.transform.localPosition = new Vector3((float)((int)(0f - vector.x) / 2) + 134f, -28f, 0f);
			mSpriteTrophyIcon.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
			mSelectedTitleFrame.transform.localPosition = new Vector3(-300f, 82f, 0f);
			mSpritePanelSize = 380;
			mItemList.OnScreenChanged(false);
			mItemList.transform.localPosition = new Vector3(177f, -70f);
			mExchangeDesc.transform.localPosition = new Vector3(177f, mListInfo.landscape_height / 2f + mItemList.transform.localPosition.y + 15f);
			mBatchReceiveButton.transform.localPosition = new Vector3(185f, (0f - mListInfo.landscape_height) / 2f + mItemList.transform.localPosition.y - 24f);
			break;
		}
		}
		StartCoroutine(DownCurtain(mCurtain));
		ActivateTab(_mode);
		if (mRotationMissionFlg)
		{
			mRotationMissionFlg = false;
			mState.Change(STATE.MAIN);
		}
		if (mRotationExchangeFlg)
		{
			OnGetAnimeFinished(null);
		}
		if (mBackground != null)
		{
			Vector2 vector2 = Util.LogScreenSize();
			float num3 = vector2.y / 910f;
			mBackground.transform.localScale = new Vector3(num3, num3, 1f);
			mBackground.SetDimensions((int)(vector2.x / num3), (int)(vector2.y / num3));
		}
	}

	private void ChangeMode(TrophyMode mode, bool layout = false)
	{
		Vector2 vector = Util.LogScreenSize();
		if (mListInfo.landscape_width == 0f)
		{
			float num = vector.x;
			if (vector.y < vector.x)
			{
				num = vector.y;
			}
			mListInfo = new SMVerticalListInfo(1, 550f, num * 0.52f, 1, 550f, 640f, 1, 156);
		}
		if (!mAllData)
		{
			sendableFriends = mGame.AllFriends();
			mAllData = true;
		}
		if (Mode != mode)
		{
		}
		Mode = mode;
		if (mItemList != null)
		{
			UnityEngine.Object.Destroy(mItemList.gameObject);
			mItemList = null;
		}
		mTrophyItemListData.Clear();
		mTrophyItemListMissionData.Clear();
		List<TrophyItemMissionListItem> list = new List<TrophyItemMissionListItem>();
		List<SMVerticalListItem> list2 = new List<SMVerticalListItem>();
		List<TrophyItemListItem> list3 = new List<TrophyItemListItem>();
		List<TrophyItemListItem> list4 = new List<TrophyItemListItem>();
		List<TrophyItemListItem> list5 = new List<TrophyItemListItem>();
		List<SMVerticalListItem> list6 = new List<SMVerticalListItem>();
		List<int> list7 = new List<int>();
		switch (mode)
		{
		case TrophyMode.MISSION:
		{
			list7 = MissionManager.GetNotClearedArray();
			for (int n = 0; n < list7.Count; n++)
			{
				MissionInfo missionInfo = MissionManager.GetMissionInfo(list7[n]);
				TrophyItemMissionListItem trophyItemMissionListItem = new TrophyItemMissionListItem();
				trophyItemMissionListItem.data = missionInfo;
				trophyItemMissionListItem.id = list7[n];
				if (!missionInfo.IsAdvMission() || mGame.EnableAdvEnter())
				{
					list.Add(trophyItemMissionListItem);
				}
			}
			mNotClearMissionID = list7;
			for (int num7 = 0; num7 < list.Count; num7++)
			{
				list2.Add(list[num7]);
			}
			break;
		}
		case TrophyMode.CLEARED_MISSION:
		{
			list7 = MissionManager.GetClearedArray();
			for (int num8 = 0; num8 < list7.Count; num8++)
			{
				MissionInfo missionInfo2 = MissionManager.GetMissionInfo(list7[num8]);
				TrophyItemMissionListItem trophyItemMissionListItem2 = new TrophyItemMissionListItem();
				trophyItemMissionListItem2.data = missionInfo2;
				trophyItemMissionListItem2.id = list7[num8];
				list.Add(trophyItemMissionListItem2);
			}
			for (int num9 = 0; num9 < list.Count; num9++)
			{
				list2.Add(list[num9]);
			}
			break;
		}
		case TrophyMode.EXCHANGE:
		{
			for (int i = 0; i < mGame.mTrophyExchangeItemData.Count; i++)
			{
				if (!mGame.mTrophyExchangeItemData.ContainsKey(i) || mGame.mTrophyExchangeItemData[i].Category == 0)
				{
					continue;
				}
				TrophyExchangeItemData data = mGame.mTrophyExchangeItemData[i];
				TrophyItemListItem trophyItemListItem = new TrophyItemListItem();
				trophyItemListItem.data = data;
				int limit = 0;
				int num2 = 0;
				int num3 = 0;
				double num4 = 0.0;
				double num5 = 0.0;
				bool flag = false;
				num3 = mGame.mTrophyExchangeItemData[i].Index;
				for (int j = 0; j < mGame.EventProfile.TrophyExchangeList.Count; j++)
				{
					if (mGame.EventProfile.TrophyExchangeList[j].TrophyItemID.CompareTo(trophyItemListItem.data.Index) == 0)
					{
						num4 = mGame.EventProfile.TrophyExchangeList[j].StartTime;
						num5 = mGame.EventProfile.TrophyExchangeList[j].EndTime;
						limit = mGame.EventProfile.TrophyExchangeList[j].TrophyItemLimit;
						num2 = mGame.EventProfile.TrophyExchangeList[j].TrophyCount;
						flag = true;
					}
				}
				if (!flag)
				{
					if (mGame.mTrophyExchangeItemData[i].SaleNum == 1)
					{
						continue;
					}
					limit = mGame.mTrophyExchangeItemData[i].DefaultNum;
				}
				int num6 = (trophyItemListItem.limit = mGame.mPlayer.IsEnableTrophySettingItem(num3, limit, num2));
				trophyItemListItem.count = num2;
				DateTime dateTime = DateTimeUtil.Now();
				DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(num4, true);
				DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(num5, true);
				if (num4 == num5 || (dateTime2 < dateTime && dateTime < dateTime3))
				{
					if (mGame.mTrophyExchangeItemData[i].Category == 1)
					{
						list4.Add(trophyItemListItem);
					}
					if (mGame.mTrophyExchangeItemData[i].Category == 2 && num6 != 0)
					{
						list3.Add(trophyItemListItem);
					}
					if (mGame.mTrophyExchangeItemData[i].Category == 3)
					{
						list5.Add(trophyItemListItem);
					}
				}
			}
			list5.Sort(TrophyoOtherItemSort);
			for (int k = 0; k < list3.Count; k++)
			{
				list2.Add(list3[k]);
			}
			for (int l = 0; l < list4.Count; l++)
			{
				list2.Add(list4[l]);
			}
			for (int m = 0; m < list5.Count; m++)
			{
				list2.Add(list5[m]);
			}
			break;
		}
		}
		if (mScreenOrientation == ScreenOrientation.Portrait || mScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			mItemList = SMVerticalList.Make(mRoot, this, mListInfo, list2, 0f, 0f, mBaseDepth + 20, true, UIScrollView.Movement.Vertical);
			mItemList.transform.localPosition = new Vector3(0f, -50f);
		}
		else
		{
			mItemList = SMVerticalList.Make(mRoot, this, mListInfo, list2, 0f, 0f, mBaseDepth + 20, false, UIScrollView.Movement.Vertical);
			mItemList.transform.localPosition = new Vector3(177f, -70f);
		}
		switch (mode)
		{
		case TrophyMode.MISSION:
			mItemList.OnCreateItem = OnCreateItem_Mission;
			break;
		case TrophyMode.CLEARED_MISSION:
			mItemList.OnCreateItem = OnCreateItem_Mission_Clear;
			break;
		default:
			mItemList.OnCreateItem = OnCreateItem_Exchange;
			break;
		}
		if (layout)
		{
			mItemList.OnCreateFinish = delegate
			{
				SetLayout(Util.ScreenOrientation);
			};
		}
	}

	private void OnCreateItem_Mission(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		BIJImage image3 = ResourceManager.LoadImage("TROPHY").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		string empty = string.Empty;
		string empty2 = string.Empty;
		TrophyItemMissionData trophyItemMissionData = new TrophyItemMissionData();
		float cellWidth = mItemList.mList.cellWidth;
		float cellHeight = mItemList.mList.cellHeight;
		UISprite uISprite3 = Util.CreateSprite("Back", itemGO, image2);
		Util.SetSpriteInfo(uISprite3, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite3.width = (int)cellWidth;
		uISprite3.height = (int)cellHeight;
		TrophyItemMissionListItem trophyItemMissionListItem = (trophyItemMissionData.data = item as TrophyItemMissionListItem);
		trophyItemMissionData.itemGo = itemGO;
		trophyItemMissionData.item = item;
		trophyItemMissionData.lastItem = lastItem;
		uISprite = Util.CreateSprite("FriendBoard", uISprite3.gameObject, image3);
		Util.SetSpriteInfo(uISprite, "instruction_panel", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(490, 150);
		trophyItemMissionData.boardImage = uISprite.gameObject;
		string text = string.Format(Localization.Get(trophyItemMissionListItem.data.mData.mName), 0);
		UILabel uILabel = Util.CreateLabel("Mission_Title", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 8, new Vector3(-180f, 30f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(253, 50);
		UISprite uISprite4;
		if (trophyItemMissionListItem.data.mData.mSteps.Count > trophyItemMissionListItem.data.mProgress.mStep - 1)
		{
			uISprite4 = Util.CreateSprite("trophyNumBack", uISprite.gameObject, image3);
			Util.SetSpriteInfo(uISprite4, "trophy_panel", mBaseDepth + 8, new Vector3(140f, 33f, 0f), Vector3.one, false);
			uILabel = Util.CreateLabel("TrophyNum", uISprite4.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty + trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mReward, mBaseDepth + 9, new Vector3(21f, -7f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		}
		DialogBase.SetDialogLabelEffect2(uILabel);
		uISprite4 = Util.CreateSprite("Line", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite4, "line00", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(375, 6);
		if (mMissionAddFlg)
		{
			uISprite4 = Util.CreateSprite("redbar", uISprite.gameObject, image3);
			Util.SetSpriteInfo(uISprite4, "instruction_accessory30", mBaseDepth + 20, new Vector3(-222f, 47f, 0f), Vector3.one, false);
			uISprite4.type = UIBasicSprite.Type.Sliced;
			uISprite4.SetDimensions(102, 34);
			uISprite4.transform.localRotation = Quaternion.Euler(0f, 0f, 30f);
			text = ((trophyItemMissionListItem.data.mData.mOpenStep == 1) ? string.Format(Localization.Get("TrophyMission_Lv_nothing")) : string.Format(Localization.Get("TrophyMission_Lv"), trophyItemMissionListItem.data.mProgress.mStep));
			uILabel = Util.CreateLabel("LvNum", uISprite4.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text, mBaseDepth + 21, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Color.white;
			uILabel.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
		}
		MissionProgress missionProgress = MissionManager.GetMissionProgress(trophyItemMissionListItem.data.mData.mID);
		int num2 = missionProgress.mValue + missionProgress.mChangeValue;
		uISprite4 = Util.CreateSprite("GaugeBase", uISprite.gameObject, image3);
		Util.SetSpriteInfo(uISprite4, "gauge_trophy", mBaseDepth + 3, new Vector3(-50.3f, -28f, 0f), Vector3.one, false);
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(250, 53);
		UISprite uISprite5 = Util.CreateSprite("Fill", uISprite4.gameObject, image3);
		Util.SetSpriteInfo(uISprite5, "gauge_trophy_bar", mBaseDepth + 4, new Vector3(1f, -1f, 0f), Vector3.one, false);
		uISprite5.SetDimensions(232, 27);
		uISprite5.type = UIBasicSprite.Type.Filled;
		uISprite5.fillDirection = UIBasicSprite.FillDirection.Horizontal;
		if (mMissionAddFlg)
		{
			if (mMissionAddFlg)
			{
				if (num2 == 0)
				{
					uISprite5.fillAmount = 0f;
				}
				else if (trophyItemMissionListItem.data.mProgress.mStep <= 1)
				{
					if (trophyItemMissionListItem.data.mData.mSteps.Count <= trophyItemMissionListItem.data.mProgress.mStep - 1)
					{
						uISprite5.fillAmount = (float)num2 / (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mData.mSteps.Count - 1].mTarget;
					}
					else
					{
						uISprite5.fillAmount = (float)num2 / (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mTarget;
					}
				}
				else if (trophyItemMissionListItem.data.mData.mSteps.Count <= trophyItemMissionListItem.data.mProgress.mStep - 1)
				{
					uISprite5.fillAmount = (float)num2 / (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mData.mSteps.Count - 1].mTarget;
				}
				else
				{
					float num3 = (float)num2 - (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 2].mTarget;
					float num4 = (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mTarget - (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 2].mTarget;
					uISprite5.fillAmount = num3 / num4;
				}
			}
		}
		else
		{
			uISprite5.fillAmount = 0f;
		}
		int num5 = num2;
		if (num2 >= trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mTarget)
		{
			UIButton uIButton = Util.CreateJellyImageButton("Getbutton", uISprite.gameObject, image3);
			Util.SetImageButtonInfo(uIButton, "LC_button_acquisition", "LC_button_acquisition", "LC_button_acquisition", mBaseDepth + 9, new Vector3(155f, -36f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "MissionTrophyGet", UIButtonMessage.Trigger.OnClick);
			StartCoroutine(UpdateButtonJump(uIButton));
			SMVerticalComponent sMVerticalComponent = uIButton.gameObject.AddComponent<SMVerticalComponent>();
			sMVerticalComponent.ListItem = trophyItemMissionListItem;
			sMVerticalComponent.Index = index;
		}
		else
		{
			uISprite4 = Util.CreateSprite("GetSprite", uISprite.gameObject, image3);
			Util.SetSpriteInfo(uISprite4, "LC_button_acquisition", mBaseDepth + 9, new Vector3(155f, -36f, 0f), Vector3.one, false);
			uISprite4.color = Color.gray;
		}
		int num6 = ((trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mTarget >= num5) ? (trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mTarget - num5) : 0);
		if (mMissionAddFlg)
		{
			if (num6 == 0)
			{
				UISsSpriteP uISsSpriteP = Util.CreateGameObject("TrophyLevel", uISprite5.gameObject).AddComponent<UISsSpriteP>();
				uISsSpriteP.Animation = ResourceManager.LoadSsAnimation("EFFECT_TROPHY_GAUGE_MAX").SsAnime;
				uISsSpriteP.depth = mBaseDepth + 10;
				uISsSpriteP.transform.localScale = new Vector3(1f, 1f, 1f);
				uISsSpriteP.Play();
			}
		}
		else
		{
			num6 = trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mTarget - trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 2].mTarget;
		}
		uISprite4 = Util.CreateSprite("ato", uISprite.gameObject, image3);
		float num7 = -2f;
		int num8 = 119;
		num7 = -27f;
		num8 = 169;
		Util.SetSpriteInfo(uISprite4, "LC_gauge_trophy2", mBaseDepth + 11, new Vector3(num7, -49f, 0f), Vector3.one, false);
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(num8, 21);
		NumberImageString numberImageString = Util.CreateGameObject("Number", uISprite.gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(6, num6, mBaseDepth + 12, 0.5f, UIWidget.Pivot.Right, false, image2);
		numberImageString.transform.localPosition = new Vector3(53f, -47f, 0f);
		numberImageString.SetPitch(34f);
		if (mMissionAddFlg && mMissionCollctFlg)
		{
			mTrophyItemListMissionData.Add(trophyItemMissionData);
		}
		else if (mMissionCollctFlg && trophyItemMissionListItem.data.mData.mSteps.Count > trophyItemMissionListItem.data.mProgress.mStep - 1)
		{
			UISsSprite uISsSprite = Util.CreateGameObject("RibbonAnim", uISprite.gameObject).AddComponent<UISsSprite>();
			uISsSprite.Animation = ResourceManager.LoadSsAnimation("EFFECT_TROPHY_RIBBON").SsAnime;
			uISsSprite.depth = mBaseDepth + 15;
			uISsSprite.transform.localScale = new Vector3(1f, 1f, 1f);
			uISsSprite.PlayCount = 1;
			uISsSprite.Play();
			uISsSprite.transform.localPosition = new Vector3(-222f, 47f, 0f);
			text = ((trophyItemMissionListItem.data.mData.mOpenStep == 1) ? string.Format(Localization.Get("TrophyMission_Lv_nothing")) : string.Format(Localization.Get("TrophyMission_Lv"), trophyItemMissionListItem.data.mProgress.mStep));
			if (uISsSprite != null)
			{
				uILabel = Util.CreateLabel("LvNum", uISsSprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, string.Empty + text, mBaseDepth + 5, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = Color.white;
				uILabel.depth = mBaseDepth + 21;
				uILabel.transform.localRotation = Quaternion.Euler(0f, 0f, 30f);
				StartCoroutine(UpdateLabelJump(uILabel));
			}
			mTrophyItemListMissionData[index].boardImage = trophyItemMissionData.boardImage;
			mState.Reset(STATE.WAIT);
			mTrophyItemGaugeMove.num = (float)num2 - (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 2].mTarget;
			mTrophyItemGaugeMove.target = (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 1].mTarget - (float)trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 2].mTarget;
			mTrophyItemGaugeMove.perGauge = uISprite5;
			mTrophyItemGaugeMove.itemGO = itemGO;
			mTrophyItemGaugeMove.numberString = numberImageString;
			StartLevelUpAnime();
		}
	}

	private void StartLevelUpAnime()
	{
		StartCoroutine(MoveGauge(0f, mTrophyItemGaugeMove.num, mTrophyItemGaugeMove.target, mTrophyItemGaugeMove.perGauge, mTrophyItemGaugeMove.itemGO));
		StartCoroutine(MoveNum(0f, mTrophyItemGaugeMove.num, mTrophyItemGaugeMove.target, mTrophyItemGaugeMove.numberString));
	}

	private void OnCreateItem_Mission_Clear(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		int num = mBaseDepth + 5;
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		BIJImage image3 = ResourceManager.LoadImage("TROPHY").Image;
		UIFont atlasFont = GameMain.LoadFont();
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		string empty = string.Empty;
		string empty2 = string.Empty;
		TrophyItemMissionListItem trophyItemMissionListItem = item as TrophyItemMissionListItem;
		float cellWidth = mItemList.mList.cellWidth;
		float cellHeight = mItemList.mList.cellHeight;
		UISprite uISprite3 = Util.CreateSprite("Back", itemGO, image2);
		Util.SetSpriteInfo(uISprite3, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite3.width = (int)cellWidth;
		uISprite3.height = (int)cellHeight;
		uISprite = Util.CreateSprite("FriendBoard", uISprite3.gameObject, image3);
		Util.SetSpriteInfo(uISprite, "instruction_panel", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite.color = new Color(0.7137255f, 0.7137255f, 0.7137255f, 1f);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(490, 150);
		string text = string.Format(Localization.Get(trophyItemMissionListItem.data.mData.mName), 0);
		UILabel uILabel = Util.CreateLabel("Mission_Title", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 8, new Vector3(-180f, 30f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = new Color(0.28627452f, 23f / 85f, 23f / 85f, 1f);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(253, 50);
		UISprite uISprite4 = Util.CreateSprite("Line", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite4, "line00", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one, false);
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(375, 6);
		uISprite4 = Util.CreateSprite("redbar", uISprite.gameObject, image3);
		Util.SetSpriteInfo(uISprite4, "instruction_accessory30", mBaseDepth + 20, new Vector3(-222f, 47f, 0f), Vector3.one, false);
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(102, 34);
		uISprite4.transform.localRotation = Quaternion.Euler(0f, 0f, 30f);
		uISprite4.color = new Color(0.8901961f, 0.8901961f, 0.8901961f, 1f);
		text = ((trophyItemMissionListItem.data.mData.mOpenStep == 1) ? string.Format(Localization.Get("TrophyMission_Lv_nothing")) : string.Format(Localization.Get("TrophyMission_Lv"), trophyItemMissionListItem.data.mProgress.mStep - 1));
		if (!mMissionCollctFlg)
		{
			text = string.Format(Localization.Get("TrophyMission_Lv"), trophyItemMissionListItem.data.mData.mOpenStep);
		}
		uILabel = Util.CreateLabel("LvNum", uISprite4.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 21, new Vector3(0f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Color.white;
		uILabel.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
		uISprite4 = Util.CreateSprite("GaugeBase", uISprite.gameObject, image3);
		Util.SetSpriteInfo(uISprite4, "gauge_trophy", mBaseDepth + 3, new Vector3(-50.3f, -28f, 0f), Vector3.one, false);
		uISprite4.color = Color.gray;
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(250, 53);
		UISprite uISprite5 = Util.CreateSprite("Fill", uISprite4.gameObject, image3);
		Util.SetSpriteInfo(uISprite5, "gauge_trophy_bar", mBaseDepth + 4, new Vector3(1f, -1f, 0f), Vector3.one, false);
		uISprite5.type = UIBasicSprite.Type.Filled;
		uISprite5.SetDimensions(232, 27);
		uISprite5.fillDirection = UIBasicSprite.FillDirection.Horizontal;
		uISprite5.fillAmount = 1f;
		uISprite5.color = Color.gray;
		int num2;
		if (mMissionAddFlg && mMissionCollctFlg)
		{
			num2 = trophyItemMissionListItem.data.mData.mSteps[trophyItemMissionListItem.data.mProgress.mStep - 2].mTarget;
			uISprite4 = Util.CreateSprite("ato", uISprite.gameObject, image3);
			Util.SetSpriteInfo(uISprite4, "gauge_trophy2", mBaseDepth + 8, new Vector3(-13f, -49f, 0f), Vector3.one, false);
			uISprite4.type = UIBasicSprite.Type.Sliced;
			uISprite4.SetDimensions(140, 21);
			uISprite4.color = Color.gray;
		}
		else
		{
			float num3 = -2f;
			int num4 = 119;
			num3 = -27f;
			num4 = 169;
			uISprite4 = Util.CreateSprite("ato", uISprite.gameObject, image3);
			Util.SetSpriteInfo(uISprite4, "LC_gauge_trophy2", mBaseDepth + 8, new Vector3(num3, -49f, 0f), Vector3.one, false);
			uISprite4.type = UIBasicSprite.Type.Sliced;
			uISprite4.SetDimensions(num4, 21);
			uISprite4.color = Color.gray;
			num2 = 0;
		}
		NumberImageString numberImageString = Util.CreateGameObject("Number", uISprite.gameObject).AddComponent<NumberImageString>();
		numberImageString.Init(6, num2, mBaseDepth + 9, 0.5f, UIWidget.Pivot.Right, false, image2);
		numberImageString.transform.localPosition = new Vector3(53f, -47f, 0f);
		numberImageString.SetPitch(34f);
		numberImageString.SetColor(Color.gray);
		uISprite4 = Util.CreateSprite("trophyNumBack", uISprite.gameObject, image3);
		Util.SetSpriteInfo(uISprite4, "LC_already", mBaseDepth + 8, new Vector3(160f, 0f, 0f), Vector3.one, false);
		if (!mMissionAddFlg)
		{
			UISsSprite uISsSprite = Util.CreateGameObject("TrophyLevel", uISprite.gameObject).AddComponent<UISsSprite>();
			uISsSprite.Animation = ResourceManager.LoadSsAnimation("EFFECT_LABEL_CLEAR").SsAnime;
			uISsSprite.depth = mBaseDepth + 10;
			uISsSprite.transform.localScale = new Vector3(1f, 1f, 1f);
			uISsSprite.DestroyAtEnd = true;
			uISsSprite.PlayCount = 1;
			uISsSprite.Play();
			uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnGetLastUpAnimeFinished));
			mState.Reset(STATE.WAIT);
		}
	}

	private void OnGetLastUpAnimeFinished(UISsSprite sprite)
	{
		mState.Change(STATE.MAIN);
	}

	private void OnCreateItem_Exchange(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		TrophyItemListData trophyItemListData = new TrophyItemListData();
		UISsSprite uISsSprite = null;
		uISsSprite = Util.CreateGameObject("Shine", itemGO).AddComponent<UISsSprite>();
		uISsSprite.Animation = ResourceManager.LoadSsAnimation("EFFECT_SHINE").SsAnime;
		uISsSprite.depth = mBaseDepth + 10;
		uISsSprite.transform.localScale = new Vector3(1f, 1f, 1f);
		uISsSprite.DestroyAtEnd = true;
		uISsSprite.PlayCount = 1;
		uISsSprite.Play();
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		BIJImage image3 = ResourceManager.LoadImage("TROPHY").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int num = mBaseDepth + 5;
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		string empty = string.Empty;
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		float cellWidth = mItemList.mList.cellWidth;
		float cellHeight = mItemList.mList.cellHeight;
		UISprite uISprite3 = Util.CreateSprite("Back", itemGO, image2);
		Util.SetSpriteInfo(uISprite3, "null", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite3.width = (int)cellWidth;
		uISprite3.height = (int)cellHeight;
		TrophyItemListItem trophyItemListItem = (trophyItemListData.data = item as TrophyItemListItem);
		trophyItemListData.itemGo = itemGO;
		if (trophyItemListItem.data.ItemDetail.ACCESSORY == 0)
		{
			uISprite = Util.CreateSprite("FriendBoard", uISprite3.gameObject, image3);
			Util.SetSpriteInfo(uISprite, "instruction_panel", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(490, 150);
		}
		else
		{
			uISprite = Util.CreateSprite("FriendBoard", uISprite3.gameObject, image2);
			Util.SetSpriteInfo(uISprite, "instruction_panel11", mBaseDepth + 1, new Vector3(0f, 0f, 0f), Vector3.one, false);
			uISprite.type = UIBasicSprite.Type.Sliced;
			uISprite.SetDimensions(500, 184);
		}
		float x = -169f;
		float y = 20f;
		UISprite uISprite4 = null;
		if (trophyItemListItem.data.Category == 1)
		{
			BoosterData boosterData = mGame.mBoosterData[trophyItemListItem.data.ItemId];
			empty = boosterData.iconBuy;
			uISprite4 = Util.CreateSprite("Icon_" + trophyItemListItem.data.Index, itemGO.gameObject, image2);
			Util.SetSpriteInfo(uISprite4, empty, mBaseDepth + 8, new Vector3(x, y, 0f), Vector3.one, false);
		}
		else if (trophyItemListItem.data.Category == 2)
		{
			AccessoryData accessoryData = null;
			accessoryData = mGame.mAccessoryData[trophyItemListItem.data.ItemId];
			CompanionData companionData = null;
			int gotIDByNum = accessoryData.GetGotIDByNum();
			companionData = mGame.mCompanionData[gotIDByNum];
			empty = companionData.iconName;
			uISprite4 = Util.CreateSprite("Icon_" + trophyItemListItem.data.Index, itemGO.gameObject, image);
			Util.SetSpriteInfo(uISprite4, empty, mBaseDepth + 8, new Vector3(x, y, 0f), Vector3.one, false);
			if (mGame.mPlayer.IsCompanionUnlock(gotIDByNum))
			{
				uISprite4.color = Color.gray;
				string text = string.Format(Localization.Get("TrophyPage_Chara_Get"));
				UILabel uILabel = Util.CreateLabel("itemName", uISprite4.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text, mBaseDepth + 9, new Vector3(0f, -35f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
				uILabel.color = new Color(1f, 33f / 85f, 33f / 85f, 1f);
				uILabel.effectStyle = UILabel.Effect.Outline8;
			}
		}
		else if (trophyItemListItem.data.Category == 3)
		{
			empty = trophyItemListItem.data.IconName;
			uISprite4 = Util.CreateSprite("Icon_" + trophyItemListItem.data.Index, itemGO.gameObject, image2);
			Util.SetSpriteInfo(uISprite4, empty, mBaseDepth + 8, new Vector3(x, y, 0f), Vector3.one, false);
		}
		if (uISprite4 != null)
		{
			uISprite4.SetDimensions(80, 80);
		}
		UIButton uIButton = Util.CreateJellyImageButton("details" + trophyItemListItem.data.Index, itemGO.gameObject, image3);
		Util.SetImageButtonInfo(uIButton, "LC_button_details", "LC_button_details", "LC_button_details", mBaseDepth + 9, new Vector3(x, -46f, 0f), Vector3.one);
		Util.AddImageButtonMessage(uIButton, this, "OnDetailPushed", UIButtonMessage.Trigger.OnClick);
		SMVerticalComponent sMVerticalComponent = uIButton.gameObject.AddComponent<SMVerticalComponent>();
		sMVerticalComponent.ListItem = trophyItemListItem;
		sMVerticalComponent.Index = index;
		UIDragScrollView uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
		uIDragScrollView.scrollView = mItemList.mListScrollView;
		if (trophyItemListItem.data.Category == 2)
		{
			AccessoryData accessoryData2 = null;
			accessoryData2 = mGame.mAccessoryData[trophyItemListItem.data.ItemId];
			int gotIDByNum2 = accessoryData2.GetGotIDByNum();
			if (mGame.mPlayer.IsCompanionUnlock(gotIDByNum2))
			{
				string text2 = string.Format(Localization.Get("chara_same_Desc01"));
				UILabel uILabel = Util.CreateLabel("itemName", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text2, mBaseDepth + 8, new Vector3(-120f, 32f, 0f), 24, 0, 0, UIWidget.Pivot.Left);
				uILabel.color = new Color(10f / 51f, 0.2509804f, 0f, 1f);
				uISprite4 = Util.CreateSprite("GrowIcon" + trophyItemListItem.data.Index, uISprite.gameObject, image2);
				Util.SetSpriteInfo(uISprite4, "icon_level", mBaseDepth + 8, new Vector3(10f, 34f, 0f), Vector3.one, false);
				uISprite4.SetDimensions(67, 67);
				text2 = string.Format(Localization.Get("chara_same_Desc02"));
				uILabel = Util.CreateLabel("itemName", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text2, mBaseDepth + 8, new Vector3(45f, 32f, 0f), 24, 0, 0, UIWidget.Pivot.Left);
				uILabel.color = new Color(10f / 51f, 0.2509804f, 0f, 1f);
			}
			else
			{
				string text3 = string.Format(Localization.Get(trophyItemListItem.data.Name));
				UILabel uILabel = Util.CreateLabel("itemName", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text3, mBaseDepth + 8, new Vector3(-124f, 32f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
				uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
				uILabel.SetDimensions(333, 26);
				uILabel.color = new Color(10f / 51f, 0.2509804f, 0f, 1f);
			}
		}
		else
		{
			string text4 = string.Format(Localization.Get(trophyItemListItem.data.Name));
			UILabel uILabel = Util.CreateLabel("itemName", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text4, mBaseDepth + 8, new Vector3(-124f, 32f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
			uILabel.SetDimensions(333, 26);
		}
		uISprite4 = Util.CreateSprite("Line", uISprite.gameObject, image2);
		Util.SetSpriteInfo(uISprite4, "line00", num + 3, new Vector3(48f, 3f, 0f), Vector3.one, false);
		uISprite4.type = UIBasicSprite.Type.Sliced;
		uISprite4.SetDimensions(338, 6);
		if (trophyItemListItem.data.Category != 2)
		{
			string text5 = string.Format(Localization.Get("TrophyPage_ItemStock"), trophyItemListItem.limit);
			UILabel uILabel = Util.CreateLabel("itemNum", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text5, mBaseDepth + 8, new Vector3(0f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			uILabel.pivot = UIWidget.Pivot.Left;
			uILabel.transform.localPosition = new Vector3(-121f, -31f, 0f);
			uILabel.color = new Color(0.6627451f, 2f / 15f, 2f / 15f, 1f);
			trophyItemListData.limitLabel = uILabel;
		}
		if (trophyItemListItem.limit != 0)
		{
			if (trophyItemListItem.data.UseTrophy <= mGame.mPlayer.GetTrophy())
			{
				string funcName = "OnExchangePushed";
				if (trophyItemListItem.data.ItemDetail.ACCESSORY != 0)
				{
					AccessoryData accessoryData3 = null;
					accessoryData3 = mGame.mAccessoryData[trophyItemListItem.data.ItemDetail.ACCESSORY];
					int gotIDByNum3 = accessoryData3.GetGotIDByNum();
					if (mGame.mPlayer.IsCompanionUnlock(gotIDByNum3))
					{
						funcName = "OnDetail2Pushed";
					}
				}
				uIButton = Util.CreateJellyImageButton("ChangeButton_" + trophyItemListItem.data.Index, itemGO.gameObject, image3);
				Util.SetImageButtonInfo(uIButton, "LC_button_exchange", "LC_button_exchange", "LC_button_exchange", mBaseDepth + 8, new Vector3(buttonX, buttonY, 0f), Vector3.one);
				Util.AddImageButtonMessage(uIButton, this, funcName, UIButtonMessage.Trigger.OnClick);
				sMVerticalComponent = uIButton.gameObject.AddComponent<SMVerticalComponent>();
				sMVerticalComponent.ListItem = trophyItemListItem;
				sMVerticalComponent.Index = index;
				uIDragScrollView = uIButton.gameObject.AddComponent<UIDragScrollView>();
				uIDragScrollView.scrollView = mItemList.mListScrollView;
				UILabel uILabel = Util.CreateLabel("Price", uIButton.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, string.Empty + trophyItemListItem.data.UseTrophy, mBaseDepth + 9, new Vector3(46f, -3f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel);
				uILabel.fontSize = 24;
				trophyItemListData.button = uIButton;
				trophyItemListData.trophyNumLabel = uILabel;
			}
			else
			{
				uISprite4 = Util.CreateSprite("little" + trophyItemListItem.data.Index, itemGO.gameObject, image3);
				Util.SetSpriteInfo(uISprite4, "LC_button_exchange", mBaseDepth + 8, new Vector3(buttonX, buttonY, 0f), Vector3.one, false);
				uISprite4.color = Color.gray;
				UILabel uILabel = Util.CreateLabel("Price", uISprite4.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, string.Empty + trophyItemListItem.data.UseTrophy, mBaseDepth + 9, new Vector3(46f, -3f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel);
				uILabel.fontSize = 24;
				uILabel.color = new Color(16f / 51f, 0f, 0f, 1f);
			}
		}
		else
		{
			uISprite4 = Util.CreateSprite("SoldOut" + trophyItemListItem.data.Index, itemGO.gameObject, image3);
			Util.SetSpriteInfo(uISprite4, "LC_soldout", mBaseDepth + 8, new Vector3(buttonX, buttonY, 0f), Vector3.one, false);
		}
		mTrophyItemListData.Add(trophyItemListData);
	}

	private void OnExchangePushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mComponent = go.GetComponent<SMVerticalComponent>();
			TrophyItemListItem trophyItemListItem = mComponent.ListItem as TrophyItemListItem;
			if (trophyItemListItem.data.UseTrophy <= mGame.mPlayer.GetTrophy() && mComponent != null)
			{
				mState.Reset(STATE.WAIT, true);
				mItemList.ScrollCustomStop();
				mGame.PlaySe("SE_ITEM_BUY_SHOP", -1);
				mUISsAnime = Util.CreateGameObject("GetEffect", mComponent.gameObject).AddComponent<UISsSprite>();
				mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
				mUISsAnime.depth = mBaseDepth + 15;
				mUISsAnime.transform.localPosition = new Vector3(-138f, 26f, 0f);
				mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
				mUISsAnime.PlayCount = 1;
				mUISsAnime.Play();
				UISsSprite uISsSprite = mUISsAnime;
				uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnGetAnimeFinished));
				mRotationExchangeFlg = true;
				mEffectFlg = true;
				mSelectItem = trophyItemListItem;
				mGame.PlaySe("SE_POSITIVE", -1);
			}
		}
	}

	public void OnDetail2Pushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			StartCoroutine(GrayOut());
			mComponent = go.GetComponent<SMVerticalComponent>();
			mSelectItem = mComponent.ListItem as TrophyItemListItem;
			AccessoryData accessoryData = null;
			accessoryData = mGame.mAccessoryData[mSelectItem.data.ItemId];
			CompanionData companionData = null;
			int gotIDByNum = accessoryData.GetGotIDByNum();
			bool flag = mGame.mPlayer.IsCompanionUnlock(gotIDByNum);
			mDetailPartnerFlg = false;
			if (mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index) || flag)
			{
				mTrophyAlreadyPartnerDialog = Util.CreateGameObject("mTrophyAlreadyPartnerDialog", mRoot).AddComponent<TrophyAlreadyPartnerDialog>();
				mTrophyAlreadyPartnerDialog.Init(accessoryData, mSelectItem);
				mTrophyAlreadyPartnerDialog.SetClosedCallback(OnTrophyAlreadyPartnerDialogClosed);
			}
			else
			{
				mTrophyDetailPartnerDialog = Util.CreateGameObject("TrophyDetailPartnerDialog", mRoot).AddComponent<TrophyDetailPartnerDialog>();
				mTrophyDetailPartnerDialog.Init(accessoryData, mSelectItem);
				mTrophyDetailPartnerDialog.SetClosedCallback(OnTrophyDetailPartnerDialogClosed);
			}
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnDetailPushed(GameObject go)
	{
		if (mState.GetStatus() != 0)
		{
			return;
		}
		StartCoroutine(GrayOut());
		mComponent = go.GetComponent<SMVerticalComponent>();
		mSelectItem = mComponent.ListItem as TrophyItemListItem;
		if (mSelectItem.data.Category != 2)
		{
			mTrophyDetailDialog = Util.CreateGameObject("TrophyDetailDialog", mRoot).AddComponent<TrophyDetailDialog>();
			mTrophyDetailDialog.Init(mSelectItem);
			mTrophyDetailDialog.SetClosedCallback(OnTrophyDetailDialogClosed);
		}
		else
		{
			mDetailPartnerFlg = true;
			AccessoryData accessoryData = null;
			accessoryData = mGame.mAccessoryData[mSelectItem.data.ItemId];
			CompanionData companionData = null;
			int gotIDByNum = accessoryData.GetGotIDByNum();
			bool flag = mGame.mPlayer.IsCompanionUnlock(gotIDByNum);
			if (mGame.mPlayer.IsAccessoryUnlock(accessoryData.Index) || flag)
			{
				mTrophyAlreadyPartnerDialog = Util.CreateGameObject("mTrophyAlreadyPartnerDialog", mRoot).AddComponent<TrophyAlreadyPartnerDialog>();
				mTrophyAlreadyPartnerDialog.Init(accessoryData, mSelectItem);
				mTrophyAlreadyPartnerDialog.SetClosedCallback(OnTrophyAlreadyPartnerDialogClosed);
			}
			else
			{
				mTrophyDetailPartnerDialog = Util.CreateGameObject("TrophyDetailPartnerDialog", mRoot).AddComponent<TrophyDetailPartnerDialog>();
				mTrophyDetailPartnerDialog.Init(accessoryData, mSelectItem);
				mTrophyDetailPartnerDialog.SetClosedCallback(OnTrophyDetailPartnerDialogClosed);
			}
		}
		mState.Reset(STATE.WAIT, true);
		mItemList.ScrollCustomStop();
	}

	private void OnTrophyAlreadyPartnerDialogClosed(TrophyAlreadyPartnerDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		if (item == TrophyAlreadyPartnerDialog.SELECT_ITEM.CANCEL)
		{
			mState.Change(STATE.MAIN);
			mItemList.ScrollCustomMove();
		}
		if (item == TrophyAlreadyPartnerDialog.SELECT_ITEM.BUY)
		{
			mGame.PlaySe("SE_ITEM_BUY_SHOP", -1);
			mUISsAnime = Util.CreateGameObject("GetEffect", mComponent.gameObject).AddComponent<UISsSprite>();
			mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
			mUISsAnime.depth = mBaseDepth + 15;
			if (mDetailPartnerFlg)
			{
				mUISsAnime.transform.localPosition = new Vector3(154f, 32f, 0f);
			}
			else
			{
				mUISsAnime.transform.localPosition = new Vector3(-109f, 32f, 0f);
			}
			mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
			mUISsAnime.PlayCount = 1;
			mUISsAnime.Play();
			UISsSprite uISsSprite = mUISsAnime;
			uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnGetAnimeFinished));
			mRotationExchangeFlg = true;
		}
	}

	private void OnGetAnimeFinished(UISsSprite sprite)
	{
		mRotationExchangeFlg = false;
		StartCoroutine(GrayOut());
		if (mUISsAnime != null)
		{
			UnityEngine.Object.Destroy(mUISsAnime.gameObject);
			mUISsAnime = null;
		}
		mGame.mPlayer.AddTrophyItem(mSelectItem.data.Index, mSelectItem.count);
		mGame.mPlayer.TrophyItem(mSelectItem.data.ItemDetail);
		mGame.mPlayer.SubTrophy(mSelectItem.data.UseTrophy);
		mTrophyChangeNumFlg = true;
		if (mNumberString != null)
		{
			UnityEngine.Object.Destroy(mNumberString.gameObject);
			mNumberString = null;
			BIJImage image = ResourceManager.LoadImage("HUD").Image;
			mNumberString = Util.CreateGameObject("Number", mNumFrame.gameObject).AddComponent<NumberImageString>();
			mNumberString.Init(6, mGame.mPlayer.GetTrophy(), mBaseDepth + 5, 0.7f, UIWidget.Pivot.Right, false, image);
			mNumberString.transform.localPosition = new Vector3(85f, 0f, 0f);
		}
		if (mSelectItem.data.Category == 1)
		{
			mTrophyExchangeDialog = Util.CreateGameObject("TrophyExchangeDialog", mRoot).AddComponent<TrophyExchangeDialog>();
			mTrophyExchangeDialog.Init(mSelectItem);
			mTrophyExchangeDialog.SetClosedCallback(OnTrophyExchangeDialogClosed);
		}
		else if (mSelectItem.data.Category == 2)
		{
			AccessoryData accessoryData = null;
			accessoryData = mGame.mAccessoryData[mSelectItem.data.ItemId];
			int gotIDByNum = accessoryData.GetGotIDByNum();
			if (mGame.mPlayer.IsCompanionUnlock(gotIDByNum))
			{
				mGame.mPlayer.AddGrowup(1);
				int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
				int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
				int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
				int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
				int advLastStage = mGame.mPlayer.AdvLastStage;
				int advMaxStamina = mGame.mPlayer.AdvMaxStamina;
				ServerCram.ArcadeGetItem(1, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 10, 1, 13, advLastStage, advMaxStamina);
				mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
				mGrowupDialog.Init(null, false, AccessoryData.ACCESSORY_TYPE.GROWUP);
				mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			}
			else
			{
				mGetPartnerDialog = Util.CreateGameObject("GetPartnerDialog", mRoot).AddComponent<GetPartnerDialog>();
				mGetPartnerDialog.Init(accessoryData, GetPartnerDialog.PLACE.TROPHY);
				mGetPartnerDialog.SetClosedCallback(OnGetPartnerDialogClosed);
			}
		}
		else
		{
			mTrophyExchangeDialog = Util.CreateGameObject("TrophyExchangeDialog", mRoot).AddComponent<TrophyExchangeDialog>();
			mTrophyExchangeDialog.Init(mSelectItem);
			mTrophyExchangeDialog.SetClosedCallback(OnTrophyExchangeDialogClosed);
		}
		for (int i = 0; i < mTrophyItemListData.Count; i++)
		{
			bool flag = false;
			TrophyItemListItem data = mTrophyItemListData[i].data;
			if (mTrophyItemListData[i].data.data.Index == mSelectItem.data.Index)
			{
				data.limit--;
				flag = true;
			}
			if (mTrophyItemListData[i].data.limit == 0 && flag)
			{
				if (mTrophyItemListData[i].button != null)
				{
					UnityEngine.Object.Destroy(mTrophyItemListData[i].button.gameObject);
				}
				UIFont uIFont = GameMain.LoadFont();
				BIJImage image2 = ResourceManager.LoadImage("TROPHY").Image;
				UISprite sprite2 = Util.CreateSprite("little" + i, mTrophyItemListData[i].itemGo.gameObject, image2);
				Util.SetSpriteInfo(sprite2, "LC_soldout", mBaseDepth + 8, new Vector3(buttonX, buttonY, 0f), Vector3.one, false);
				if (mTrophyItemListData[i].data.data.Category != 2)
				{
					UILabel limitLabel = mTrophyItemListData[i].limitLabel;
					string format = string.Format(Localization.Get("TrophyPage_ItemStock"), data.limit);
					Util.SetLabelText(limitLabel, string.Format(format));
				}
				continue;
			}
			if (mTrophyItemListData[i].data.data.Category != 2)
			{
				UILabel limitLabel2 = mTrophyItemListData[i].limitLabel;
				string format2 = string.Format(Localization.Get("TrophyPage_ItemStock"), data.limit);
				Util.SetLabelText(limitLabel2, string.Format(format2));
			}
			if (mTrophyItemListData[i].data.data.UseTrophy > mGame.mPlayer.GetTrophy() && mTrophyItemListData[i].trophyNumLabel != null && mTrophyItemListData[i].button != null)
			{
				UnityEngine.Object.Destroy(mTrophyItemListData[i].button.gameObject);
				mTrophyItemListData[i].button = null;
				BIJImage image3 = ResourceManager.LoadImage("TROPHY").Image;
				UISprite uISprite = Util.CreateSprite("little", mTrophyItemListData[i].itemGo.gameObject, image3);
				Util.SetSpriteInfo(uISprite, "LC_button_exchange", mBaseDepth + 8, new Vector3(buttonX, buttonY, 0f), Vector3.one, false);
				uISprite.color = Color.gray;
				UIFont atlasFont = GameMain.LoadFont();
				UILabel uILabel = Util.CreateLabel("Price", uISprite.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, string.Empty + mTrophyItemListData[i].data.data.UseTrophy, mBaseDepth + 9, new Vector3(46f, -3f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel);
				uILabel.fontSize = 24;
				uILabel.color = new Color(16f / 51f, 0f, 0f, 1f);
			}
		}
		mGame.Save();
		int index = mSelectItem.data.Index;
		ServerCram.ExchangeTrophy(index, mSelectItem.data.UseTrophy, mGame.mPlayer.GetTrophy(), mGame.mPlayer.ItemCountCheck(index), mGame.mPlayer.ItemTotalCountCheck(index), mGame.mOptions.PurchaseCount);
		if (mSelectItem.data.Category == 1)
		{
			int num = 1;
			foreach (KeyValuePair<TrophyItemDetail.ItemKind, int> bundleItem in mSelectItem.data.ItemDetail.BundleItems)
			{
				num = bundleItem.Value;
				Def.BOOSTER_KIND boosterID = TrophyItemDetail.GetBoosterID(bundleItem.Key);
				if (boosterID != 0)
				{
					mGame.SendGetBoost(num, 0, 13, boosterID);
				}
			}
		}
		if (index == 11)
		{
			int lastPlaySeries3 = (int)mGame.mPlayer.Data.LastPlaySeries;
			int lastPlayLevel3 = mGame.mPlayer.Data.LastPlayLevel;
			int lastPlaySeries4 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
			int lastPlayLevel4 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
			int advLastStage2 = mGame.mPlayer.AdvLastStage;
			int advMaxStamina2 = mGame.mPlayer.AdvMaxStamina;
			ServerCram.ArcadeGetItem(1, lastPlaySeries3, lastPlayLevel3, lastPlaySeries4, lastPlayLevel4, 1, 7, 1, 13, advLastStage2, advMaxStamina2);
		}
	}

	private void OnTrophyDetailPartnerDialogClosed(TrophyDetailPartnerDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		if (item == TrophyDetailPartnerDialog.SELECT_ITEM.CANCEL)
		{
			mItemList.ScrollCustomMove();
			mState.Change(STATE.MAIN);
		}
		if (item == TrophyDetailPartnerDialog.SELECT_ITEM.BUY)
		{
			mGame.PlaySe("SE_ITEM_BUY_SHOP", -1);
			mUISsAnime = Util.CreateGameObject("GetEffect", mComponent.gameObject).AddComponent<UISsSprite>();
			mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
			mUISsAnime.depth = mBaseDepth + 15;
			if (mDetailPartnerFlg)
			{
				mUISsAnime.transform.localPosition = new Vector3(154f, 32f, 0f);
			}
			else
			{
				mUISsAnime.transform.localPosition = new Vector3(-109f, 32f, 0f);
			}
			mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
			mUISsAnime.PlayCount = 1;
			mUISsAnime.Play();
			UISsSprite uISsSprite = mUISsAnime;
			uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnGetAnimeFinished));
		}
	}

	private void OnTrophyDetailDialogClosed(TrophyItemListItem ItemData, TrophyDetailDialog.SELECT_ITEM item)
	{
		StartCoroutine(GrayIn());
		if (item == TrophyDetailDialog.SELECT_ITEM.CANCEL)
		{
			mItemList.ScrollCustomMove();
			mState.Change(STATE.MAIN);
		}
		if (item == TrophyDetailDialog.SELECT_ITEM.BUY)
		{
			mGame.PlaySe("SE_ITEM_BUY_SHOP", -1);
			mUISsAnime = Util.CreateGameObject("GetEffect", mComponent.gameObject).AddComponent<UISsSprite>();
			mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
			mUISsAnime.depth = mBaseDepth + 15;
			mUISsAnime.transform.localPosition = new Vector3(154f, 32f, 0f);
			mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
			mUISsAnime.PlayCount = 1;
			mUISsAnime.Play();
			UISsSprite uISsSprite = mUISsAnime;
			uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnGetAnimeFinished));
		}
		StartCoroutine(GrayIn());
	}

	private void ListSort()
	{
		int i;
		for (i = 0; i < mTrophyItemListData.Count && mTrophyItemListData[i].data.data.Index != mSelectItem.data.Index; i++)
		{
		}
		UnityEngine.Object.Destroy(mTrophyItemListData[i].itemGo);
		for (int j = 0; j < mTrophyItemListData.Count - 1; j++)
		{
			Vector3 localPosition = mTrophyItemListData[j].itemGo.transform.localPosition;
			mTrophyItemListData[j].itemGo.transform.localPosition = mTrophyItemListData[j + 1].itemGo.transform.localPosition;
			mTrophyItemListData[j + 1].itemGo.transform.localPosition = localPosition;
			TrophyItemListData value = mTrophyItemListData[j + 1];
			mTrophyItemListData[j + 1] = mTrophyItemListData[j];
			mTrophyItemListData[j] = value;
		}
	}

	public void OnGrowupSelectDialogClosed(GrowupPartnerSelectDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupDialog.SelectedPartnerNo;
		bool isCloseButton = mGrowupDialog.IsCloseButton;
		mGrowupDialog.Init(false);
		AccessoryData data = mGrowupDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupDialog.gameObject);
		mGrowupDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerSelectDialog.SELECT_ITEM.PLAY:
			mGrowupPartnerConfirmtDialog = Util.CreateGameObject("GrowupPartnerConfirmDialog", mRoot).AddComponent<GrowupPartnerConfirmDialog>();
			mGrowupPartnerConfirmtDialog.Init(data, selectedPartnerNo, isCloseButton, accessoryType);
			mGrowupPartnerConfirmtDialog.SetClosedCallback(OnGrowupPartnerConfirmDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerSelectDialog.SELECT_ITEM.CANCEL:
			StartCoroutine(GrayIn());
			if (isCloseButton && mGame.mPlayer.GrowUpPiece > 0)
			{
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			}
			else
			{
				mState.Change(STATE.MAIN);
				mItemList.ScrollCustomMove();
			}
			break;
		}
	}

	public void OnGrowupPartnerConfirmDialogClosed(GrowupPartnerConfirmDialog.SELECT_ITEM a_selectItem)
	{
		int selectedPartnerNo = mGrowupPartnerConfirmtDialog.SelectedPartnerNo;
		bool isReplayStamp = mGrowupPartnerConfirmtDialog.IsReplayStamp;
		mGrowupPartnerConfirmtDialog.Init(false);
		AccessoryData data = mGrowupPartnerConfirmtDialog.Data;
		AccessoryData.ACCESSORY_TYPE accessoryType = mGrowupPartnerConfirmtDialog.AccessoryType;
		UnityEngine.Object.Destroy(mGrowupPartnerConfirmtDialog.gameObject);
		mGrowupPartnerConfirmtDialog = null;
		switch (a_selectItem)
		{
		case GrowupPartnerConfirmDialog.SELECT_ITEM.PLAY:
			mGrowupResultDialog = Util.CreateGameObject("GrowupResultDialog", mRoot).AddComponent<GrowupResultDialog>();
			mGrowupResultDialog.Init(data, selectedPartnerNo, true, accessoryType, isReplayStamp);
			mGrowupResultDialog.SetClosedCallback(OnGrowupResultDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		case GrowupPartnerConfirmDialog.SELECT_ITEM.BACK:
			StartCoroutine(GrayIn());
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(data, isReplayStamp, accessoryType);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mState.Reset(STATE.WAIT, true);
			break;
		}
	}

	public void OnGrowupResultDialogClosed(GrowupResultDialog.SELECT_ITEM a_selectItem)
	{
		if (mGrowupResultDialog != null)
		{
			if (mGrowupResultDialog.IsReplayStamp && mGame.mPlayer.GrowUpPiece > 0)
			{
				StartCoroutine(GrayOut());
				mGrowupResultDialog.Init(false);
				mGrowupResultDialog.MapUpdateInit(mCurrentPage.mAvater);
				mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
				mGetStampDialog.Init(null, 0, false, true);
				mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			}
			else
			{
				StartCoroutine(GrayIn());
				mState.Change(STATE.MAIN);
				mItemList.ScrollCustomMove();
			}
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.MAIN);
			mItemList.ScrollCustomMove();
		}
		if (mGrowupResultDialog != null)
		{
			UnityEngine.Object.Destroy(mGrowupResultDialog.gameObject);
			mGrowupResultDialog = null;
		}
	}

	public void OnGetStampDialogClosed(GetStampDialog.SELECT_ITEM a_selectItem)
	{
		if (mGetStampDialog != null)
		{
			UnityEngine.Object.Destroy(mGetStampDialog.gameObject);
		}
		if (a_selectItem == GetStampDialog.SELECT_ITEM.GROWUP)
		{
			mGrowupDialog = Util.CreateGameObject("GrowupPartnerDialog", mRoot).AddComponent<GrowupPartnerSelectDialog>();
			mGrowupDialog.Init(null, true, AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE);
			mGrowupDialog.SetClosedCallback(OnGrowupSelectDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else if (mGame.mPlayer.HeartUpPiece >= 15)
		{
			mGame.mPlayer.SubHeartPiece(15);
			GiftItem giftItem = new GiftItem();
			giftItem.Data.Message = Localization.Get("Gift_RatingReward");
			giftItem.Data.ItemCategory = 1;
			giftItem.Data.ItemKind = 1;
			giftItem.Data.ItemID = 1;
			giftItem.Data.Quantity = 1;
			mGame.mPlayer.AddGift(giftItem);
			mGame.Save();
			mGetAccessoryDialog = Util.CreateGameObject("GetAccessoryDialog", mRoot).AddComponent<GetAccessoryDialog>();
			mGetAccessoryDialog.Init("icon_heart", Localization.Get("GetAccessory_Item_Title"), string.Format(Localization.Get("GetAccessory_HEART_Desc"), 1));
			mGetAccessoryDialog.SetClosedCallback(OnGetHeartPieaceDialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			StartCoroutine(GrayIn());
			mState.Change(STATE.MAIN);
			mItemList.ScrollCustomMove();
		}
	}

	public void OnGetHeartPieaceDialogClosed(GetAccessoryDialog.SELECT_ITEM a_selectItem)
	{
		if (mGame.mPlayer.HeartUpPiece > 0)
		{
			mGetStampDialog = Util.CreateGameObject("GetStampDialog", mRoot).AddComponent<GetStampDialog>();
			mGetStampDialog.Init(null, 0, false, true, false);
			mGetStampDialog.SetClosedCallback(OnGetStampDialogClosed);
			mState.Change(STATE.WAIT);
		}
		else
		{
			StartCoroutine(GrayIn());
			ListSort();
			mState.Change(STATE.MAIN);
			mItemList.ScrollCustomMove();
		}
	}

	public void OnGetPartnerDialogClosed(GetPartnerDialog.SELECT_ITEM a_selectItem)
	{
		StartCoroutine(GrayIn());
		AccessoryData data = mGetPartnerDialog.Data;
		UnityEngine.Object.Destroy(mGetPartnerDialog.gameObject);
		mGetPartnerDialog = null;
		ListSort();
		mState.Change(STATE.MAIN);
		mItemList.ScrollCustomMove();
	}

	public void OnTrophyExchangeDialogClosed()
	{
		StartCoroutine(GrayIn());
		UnityEngine.Object.Destroy(mTrophyExchangeDialog.gameObject);
		mTrophyExchangeDialog = null;
		mState.Change(STATE.MAIN);
		mItemList.ScrollCustomMove();
	}

	public void MissionTrophyGet(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mComponent = go.GetComponent<SMVerticalComponent>();
			mMissionClearSelectItem = mComponent.ListItem as TrophyItemMissionListItem;
			int mReward = mMissionClearSelectItem.data.mData.mSteps[mMissionClearSelectItem.data.mProgress.mStep - 1].mReward;
			mGame.mPlayer.AddTrophy(mReward);
			int i;
			for (i = 0; i < mTrophyItemListMissionData.Count && mTrophyItemListMissionData[i].data.id != mMissionClearSelectItem.id; i++)
			{
			}
			MissionManager.ClearMission(mTrophyItemListMissionData[i].data.id);
			mGame.Save();
			int sub_type = mTrophyItemListMissionData[i].data.data.mProgress.mID * 1000 + mTrophyItemListMissionData[i].data.data.mProgress.mStep;
			ServerCram.GetTrophy(mReward, mGame.mPlayer.GetTrophy(), 1, sub_type, mGame.mOptions.PurchaseCount);
			if (MissionManager.EnableGetReward())
			{
				mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
			}
			else
			{
				mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
			mGame.PlaySe("SE_DIALY_COMPLETE_STAMP", -1);
			mUISsAnime = Util.CreateGameObject("GrowHalo", go.gameObject).AddComponent<UISsSprite>();
			mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
			mUISsAnime.depth = mBaseDepth + 19;
			mUISsAnime.transform.localPosition = new Vector3(0f, 0f, 0f);
			mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
			mUISsAnime.PlayCount = 1;
			mUISsAnime.Play();
			UISsSprite uISsSprite = mUISsAnime;
			uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnTrophyMoved));
			if (mNumberString != null)
			{
				UnityEngine.Object.Destroy(mNumberString.gameObject);
				mNumberString = null;
				BIJImage image = ResourceManager.LoadImage("HUD").Image;
				mNumberString = Util.CreateGameObject("Number", mNumFrame.gameObject).AddComponent<NumberImageString>();
				mNumberString.Init(6, mGame.mPlayer.GetTrophy(), mBaseDepth + 5, 0.7f, UIWidget.Pivot.Right, false, image);
				mNumberString.transform.localPosition = new Vector3(85f, 0f, 0f);
			}
			mItemList.ScrollCustomStop();
			mRotationMissionFlg = true;
			mState.Reset(STATE.WAIT, true);
		}
	}

	public void OnTrophyMoved(UISsSprite sprite)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		if (mNumberString != null)
		{
			mNumberString.transform.localPosition = new Vector3(85f, 0f, 0f);
		}
		int i;
		for (i = 0; i < mTrophyItemListMissionData.Count && mTrophyItemListMissionData[i].data.id != mMissionClearSelectItem.id; i++)
		{
		}
		mTrophyItemListMissionData[i].data.data.mProgress.mStep++;
		UnityEngine.Object.Destroy(mTrophyItemListMissionData[i].boardImage.gameObject);
		GameObject itemGo = mTrophyItemListMissionData[i].itemGo;
		SMVerticalListItem item = mTrophyItemListMissionData[i].item;
		SMVerticalListItem lastItem = mTrophyItemListMissionData[i].lastItem;
		mMissionAddFlg = false;
		TrophyItemMissionListItem trophyItemMissionListItem = item as TrophyItemMissionListItem;
		if (trophyItemMissionListItem.data.mData.mOpenStep <= trophyItemMissionListItem.data.mProgress.mStep - 1)
		{
			OnCreateItem_Mission_Clear(itemGo, i, item, lastItem);
			mItemList.ScrollCustomMove();
		}
		else
		{
			OnCreateItem_Mission(itemGo, i, item, lastItem);
		}
		mMissionAddFlg = true;
	}

	public void OnMissionTrophyClosed()
	{
		int i;
		for (i = 0; i < mTrophyItemListMissionData.Count && mTrophyItemListMissionData[i].data.id != mMissionClearSelectItem.id; i++)
		{
		}
		mTrophyItemListMissionData[i].data.data.mProgress.mStep++;
		UnityEngine.Object.Destroy(mTrophyItemListMissionData[i].boardImage.gameObject);
		GameObject itemGo = mTrophyItemListMissionData[i].itemGo;
		SMVerticalListItem item = mTrophyItemListMissionData[i].item;
		SMVerticalListItem lastItem = mTrophyItemListMissionData[i].lastItem;
		mMissionAddFlg = false;
		TrophyItemMissionListItem trophyItemMissionListItem = item as TrophyItemMissionListItem;
		if (trophyItemMissionListItem.data.mData.mOpenStep <= trophyItemMissionListItem.data.mProgress.mStep - 1)
		{
			OnCreateItem_Mission_Clear(itemGo, i, item, lastItem);
		}
		else
		{
			OnCreateItem_Mission(itemGo, i, item, lastItem);
		}
		mMissionAddFlg = true;
		StartCoroutine(GrayIn());
	}

	private IEnumerator OnGetCollctFinished()
	{
		while (mCollectBatchList.Count == 0 || mEffectWaitFlg)
		{
			yield return null;
		}
		if (MissionManager.EnableGetReward())
		{
			mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
		}
		else
		{
			mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		mMissionCollctFlg = false;
		if (mNumberString != null)
		{
			UnityEngine.Object.Destroy(mNumberString.gameObject);
			mNumberString = null;
			BIJImage atlas = ResourceManager.LoadImage("HUD").Image;
			mNumberString = Util.CreateGameObject("Number", mNumFrame.gameObject).AddComponent<NumberImageString>();
			mNumberString.Init(6, mGame.mPlayer.GetTrophy(), mBaseDepth + 5, 0.7f, UIWidget.Pivot.Right, false, atlas);
			mNumberString.transform.localPosition = new Vector3(85f, 0f, 0f);
		}
		List<TrophyItemMissionListItem> listTrophyMission = new List<TrophyItemMissionListItem>();
		for (int a = 0; a < mNotClearMissionID.Count; a++)
		{
			MissionInfo data = MissionManager.GetMissionInfo(mNotClearMissionID[a]);
			listTrophyMission.Add(new TrophyItemMissionListItem
			{
				data = data,
				id = mNotClearMissionID[a]
			});
		}
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		for (int x = 0; x < listTrophyMission.Count; x++)
		{
			list.Add(listTrophyMission[x]);
		}
		for (int i = 0; i < mCollectBatchList.Count; i++)
		{
			int index;
			for (index = 0; index < mTrophyItemListMissionData.Count && mTrophyItemListMissionData[index].data.id != mCollectBatchList[i].mID; index++)
			{
			}
			UnityEngine.Object.Destroy(mTrophyItemListMissionData[index].boardImage.gameObject);
			GameObject itemGo = mTrophyItemListMissionData[index].itemGo;
			SMVerticalListItem lastItem = mTrophyItemListMissionData[index].lastItem;
			if (MissionManager.IsOpenStepCleared(mTrophyItemListMissionData[index].data.id))
			{
				OnCreateItem_Mission_Clear(itemGo, index, list[index], lastItem);
			}
			else
			{
				OnCreateItem_Mission(itemGo, index, list[index], lastItem);
			}
		}
		mCollectBatchList.Clear();
		mMissionCollctFlg = true;
		StartCoroutine(GrayIn());
		mState.Change(STATE.MAIN);
	}

	private void OnGetFinished(UISsSprite sprite)
	{
		if (mUISsAnime != null)
		{
			UnityEngine.Object.Destroy(mUISsAnime.gameObject);
			mUISsAnime = null;
		}
		mEffectWaitFlg = false;
	}

	private void OnBatchReceivePushed()
	{
		if (mState.GetStatus() == STATE.MAIN && MissionManager.EnableGetReward())
		{
			mState.Reset(STATE.WAIT, true);
			StartCoroutine(GrayOut());
			mUISsAnimePanel.sortingOrder = 50;
			mGame.PlaySe("SE_ITEM_BUY_SHOP", -1);
			mUISsAnime = Util.CreateGameObject("GetEffect", mUISsAnimePanel.gameObject).AddComponent<UISsSprite>();
			mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
			mUISsAnime.depth = mBaseDepth + 105;
			mUISsAnime.transform.localPosition = new Vector3(0f, 26f, 0f);
			mUISsAnime.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
			mUISsAnime.PlayCount = 1;
			mUISsAnime.Play();
			UISsSprite uISsSprite = mUISsAnime;
			uISsSprite.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsSprite.AnimationFinished, new UISsSprite.AnimationCallback(OnGetFinished));
			mEffectWaitFlg = true;
			StartCoroutine(OnGetCollctFinished());
			mCollectBatchList = MissionManager.BatchClearMission(mNotClearMissionID);
		}
	}

	public void Init(SMMapPage CurrentPage)
	{
		mCurrentPage = CurrentPage;
	}

	public void OnExitPushed()
	{
		if (mItemList != null)
		{
			GameMain.SafeDestroy(mItemList.gameObject);
			mItemList = null;
		}
		if (mGameState.PrevState == GameStateManager.GAME_STATE.ADVMENU)
		{
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.ADVMENU);
		}
		else
		{
			mGameState.SetNextGameState(GameStateManager.GAME_STATE.MAP);
		}
	}

	private void OnBackPushed()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mGame.PlaySe("SE_NEGATIVE", -1);
			if (mTrophyChangeNumFlg)
			{
				mGame.Save(true, true);
			}
			OnExitPushed();
		}
	}

	private void ActivateTab(TrophyMode mode)
	{
		BIJImage image = ResourceManager.LoadImage("TROPHY").Image;
		switch (mode)
		{
		case TrophyMode.MISSION:
			Util.SetSpriteImageName(mSelectedTitleFrame, "panel_mission", Vector3.one, false);
			mSelectedTitleFrame.SetDimensions(mSpritePanelSize, 58);
			mSelectedTitleFrame.type = UIBasicSprite.Type.Sliced;
			Util.SetSpriteImageName(mSelectedTitle, "LC_text_mission", Vector3.one, false);
			NGUITools.SetActive(mTabMissionCheck.gameObject, true);
			NGUITools.SetActive(mTabClearedCheck.gameObject, false);
			NGUITools.SetActive(mTabExchangeCheck.gameObject, false);
			mTabMission.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 5;
			mTabCleared.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 4;
			mTabExchange.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 4;
			NGUITools.SetActive(mBatchReceiveButton.gameObject, true);
			NGUITools.SetActive(mExchangeDesc.gameObject, false);
			if (MissionManager.EnableGetReward())
			{
				mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
			}
			else
			{
				mBatchReceiveButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
			break;
		case TrophyMode.CLEARED_MISSION:
			Util.SetSpriteImageName(mSelectedTitleFrame, "panel_limit", Vector3.one, false);
			mSelectedTitleFrame.SetDimensions(mSpritePanelSize, 58);
			mSelectedTitleFrame.type = UIBasicSprite.Type.Sliced;
			Util.SetSpriteImageName(mSelectedTitle, "LC_text_mission_settled", Vector3.one, false);
			NGUITools.SetActive(mTabMissionCheck.gameObject, false);
			NGUITools.SetActive(mTabClearedCheck.gameObject, true);
			NGUITools.SetActive(mTabExchangeCheck.gameObject, false);
			mTabMission.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 4;
			mTabCleared.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 5;
			mTabExchange.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 4;
			NGUITools.SetActive(mExchangeDesc.gameObject, false);
			NGUITools.SetActive(mBatchReceiveButton.gameObject, false);
			break;
		case TrophyMode.EXCHANGE:
			Util.SetSpriteImageName(mSelectedTitleFrame, "panel_prize", Vector3.one, false);
			mSelectedTitleFrame.SetDimensions(mSpritePanelSize, 58);
			mSelectedTitleFrame.type = UIBasicSprite.Type.Sliced;
			Util.SetSpriteImageName(mSelectedTitle, "LC_text_prize", Vector3.one, false);
			NGUITools.SetActive(mTabMissionCheck.gameObject, false);
			NGUITools.SetActive(mTabClearedCheck.gameObject, false);
			NGUITools.SetActive(mTabExchangeCheck.gameObject, true);
			mTabMission.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 4;
			mTabCleared.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 4;
			mTabExchange.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 5;
			NGUITools.SetActive(mExchangeDesc.gameObject, true);
			NGUITools.SetActive(mBatchReceiveButton.gameObject, false);
			break;
		}
	}

	public void OnTabMissionPushed()
	{
		if (mState.GetStatus() == STATE.MAIN && Mode != 0)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			_mode = TrophyMode.MISSION;
			ActivateTab(_mode);
			ChangeMode(_mode);
		}
	}

	public void OnTabClearedPushed()
	{
		if (mState.GetStatus() == STATE.MAIN && Mode != TrophyMode.CLEARED_MISSION)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			_mode = TrophyMode.CLEARED_MISSION;
			ActivateTab(_mode);
			ChangeMode(_mode);
		}
	}

	public void OnTabExchangePushed()
	{
		if (mState.GetStatus() == STATE.MAIN && Mode != TrophyMode.EXCHANGE)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			_mode = TrophyMode.EXCHANGE;
			ActivateTab(_mode);
			ChangeMode(_mode);
		}
	}

	private void OnSendErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void EnableList(bool enabled)
	{
		if (!(mItemList == null))
		{
			NGUITools.SetActive(mItemList.gameObject, enabled);
		}
	}

	private IEnumerator UpdateLabelJump(UILabel Jump)
	{
		float counter2 = 0f;
		bool is_draw = true;
		Vector3 BaseScale = Jump.transform.localScale;
		float delay_timer = 0.1f;
		while (true)
		{
			if (Jump == null)
			{
				yield break;
			}
			if (is_draw)
			{
				if (delay_timer <= 0f)
				{
					counter2 += Time.deltaTime * 540f;
					if (counter2 > 360f)
					{
						break;
					}
					if (Jump != null)
					{
						Vector3 pos = Jump.transform.localScale;
						pos.x = BaseScale.x + Mathf.Abs(Mathf.Sin(counter2 * ((float)Math.PI / 180f))) * 1.3f;
						pos.y = BaseScale.y + Mathf.Abs(Mathf.Sin(counter2 * ((float)Math.PI / 180f))) * 1.3f;
						Jump.transform.localScale = pos;
					}
				}
				else
				{
					delay_timer -= Time.deltaTime;
				}
			}
			else
			{
				delay_timer = 5f;
				counter2 = 0f;
				Jump.transform.localPosition = BaseScale;
			}
			yield return null;
		}
		counter2 = 0f;
		Jump.transform.localScale = BaseScale;
	}

	private IEnumerator MoveNum(float value, float changeValue, float maxNum, NumberImageString num)
	{
		if (changeValue >= maxNum)
		{
			changeValue = maxNum;
		}
		num.SetNum((int)maxNum);
		float kekka = changeValue / maxNum;
		yield return new WaitForSeconds(0.2f);
		while (kekka > 0f)
		{
			kekka -= 0.05f;
			value += maxNum * 0.05f;
			if (maxNum > value)
			{
				if (num != null)
				{
					num.SetNum((int)(maxNum - value));
				}
			}
			else if (num != null)
			{
				num.SetNum(0);
			}
			yield return null;
		}
		if (maxNum >= changeValue)
		{
			if (num != null)
			{
				num.SetNum((int)(maxNum - changeValue));
			}
		}
		else if (num != null)
		{
			num.SetNum(0);
		}
	}

	private IEnumerator MoveGauge(float value, float changeValue, float maxNum, UISprite gauge, GameObject itemGo)
	{
		float angle = 0f;
		if (changeValue > maxNum)
		{
			changeValue = maxNum;
		}
		gauge.fillAmount = value / maxNum;
		int changeNum = 0;
		bool effect = true;
		UISsSpriteP uiSsAnime2 = null;
		uiSsAnime2 = Util.CreateGameObject("Charge", itemGo).AddComponent<UISsSpriteP>();
		uiSsAnime2.Animation = ResourceManager.LoadSsAnimation("EFFECT_CHARGE_TROPHY").SsAnime;
		uiSsAnime2.depth = mBaseDepth + 10;
		uiSsAnime2.transform.localScale = new Vector3(1f, 1f, 1f);
		uiSsAnime2.DestroyAtEnd = true;
		uiSsAnime2.PlayCount = 1;
		uiSsAnime2.Play();
		float posX2 = 0f;
		uiSsAnime2.transform.localPosition = new Vector3(-166f + posX2, -28f, 1f);
		float kekka = changeValue / maxNum;
		yield return new WaitForSeconds(0.2f);
		if (uiSsAnime2 != null)
		{
			NGUITools.SetActive(uiSsAnime2.gameObject, true);
		}
		mGame.PlaySe("SE_CLEAR_BONUS_JUMP", -1);
		while (kekka > 0f)
		{
			kekka -= 0.05f;
			value += 0.05f;
			gauge.fillAmount = value;
			if (effect)
			{
				effect = false;
			}
			posX2 = 232f * value;
			if (posX2 >= 232f)
			{
				posX2 = 232f;
			}
			if (uiSsAnime2 != null)
			{
				uiSsAnime2.transform.localPosition = new Vector3(-166f + posX2, -28f, 1f);
			}
			yield return null;
		}
		if (changeValue == maxNum)
		{
			UISsSpriteP uiSsAnimeLevel = Util.CreateGameObject("TrophyLevel", gauge.gameObject).AddComponent<UISsSpriteP>();
			uiSsAnimeLevel.Animation = ResourceManager.LoadSsAnimation("EFFECT_TROPHY_GAUGE_MAX").SsAnime;
			uiSsAnimeLevel.depth = mBaseDepth + 10;
			uiSsAnimeLevel.transform.localScale = new Vector3(1f, 1f, 1f);
			uiSsAnimeLevel.Play();
		}
		if (uiSsAnime2 != null)
		{
			NGUITools.SetActive(uiSsAnime2.gameObject, false);
			mGame.PlaySe("SE_GAUGE_MAX", -1);
		}
		mItemList.ScrollCustomMove();
		mRotationMissionFlg = false;
		mState.Change(STATE.MAIN);
	}

	private IEnumerator StartButtonJumpCount()
	{
		while (true)
		{
			mCounter += Time.deltaTime * 340f;
			if (mCounter > 360f)
			{
				mCounter = 0f;
				yield return new WaitForSeconds(1.3f);
			}
			yield return null;
		}
	}

	private IEnumerator UpdateButtonJump(UIButton Jump)
	{
		bool is_draw = true;
		Vector3 BasePos = Jump.transform.localPosition;
		float delay_timer = 1f;
		while (!(Jump == null))
		{
			if (is_draw)
			{
				if (delay_timer <= 0f)
				{
					if (Jump != null)
					{
						Vector3 pos = Jump.transform.localPosition;
						pos.y = BasePos.y + Mathf.Abs(Mathf.Sin(mCounter * ((float)Math.PI / 180f))) * 10f;
						Jump.transform.localPosition = pos;
					}
				}
				else
				{
					delay_timer -= Time.deltaTime;
				}
			}
			yield return null;
		}
	}

	protected virtual IEnumerator DownCurtain(UISprite Sprite)
	{
		float posY = (0f - mCurtainHeight) / 2f + 21f;
		Vector3 BasePos = new Vector3(0f, posY, 0f);
		int APPEAR_BOUND = 5;
		float VELOCITY = 33f;
		float GRAVITY = -33f;
		float TIME_RATE = 13f;
		float REF_COEF = -0.8f;
		int bound_counter2 = 0;
		float velocity2 = 0f;
		float y_offset2 = 0f;
		bool first_set = false;
		float prevResetTime = Time.realtimeSinceStartup;
		bound_counter2 = APPEAR_BOUND;
		velocity2 = VELOCITY;
		y_offset2 = 0f;
		float stop_time = 0f;
		float curtain_timer = 0f;
		ScreenOrientation screen = mScreenOrientation;
		if (Sprite != null)
		{
			Sprite.transform.localPosition = BasePos + new Vector3(0f, 100f, 0f);
		}
		while (screen == mScreenOrientation)
		{
			stop_time += Time.deltaTime;
			if (stop_time < 0.1f)
			{
				yield return null;
			}
			else
			{
				if (bound_counter2 < 0)
				{
					if (curtain_timer > 0.4f)
					{
						break;
					}
					curtain_timer += Time.deltaTime;
				}
				else
				{
					velocity2 += GRAVITY * Time.deltaTime * TIME_RATE;
					y_offset2 += velocity2 * Time.deltaTime * TIME_RATE;
					if (!first_set)
					{
						y_offset2 = 30f;
						first_set = true;
					}
					if (y_offset2 < 0f)
					{
						y_offset2 = 0f;
						bound_counter2--;
						velocity2 = REF_COEF * velocity2;
					}
					else if (y_offset2 > 30f)
					{
						y_offset2 = 30f;
					}
					if (mTrophyTutorial)
					{
						TrophyTutorialStart();
					}
				}
				if (Sprite != null)
				{
					Sprite.transform.localPosition = BasePos + new Vector3(0f, y_offset2, 0f);
				}
			}
			yield return null;
		}
	}

	public int TrophyItemSort(TrophyItemListItem a, TrophyItemListItem b)
	{
		if (b.data.ItemDetail.ACCESSORY > a.data.ItemDetail.ACCESSORY)
		{
			return 1;
		}
		if (a.data.ItemDetail.ACCESSORY > b.data.ItemDetail.ACCESSORY)
		{
			return -1;
		}
		return 0;
	}

	public int TrophyoOtherItemSort(TrophyItemListItem a, TrophyItemListItem b)
	{
		if (b.data.ItemDetail.ROADBLOCK_TICKET > a.data.ItemDetail.GROWUP)
		{
			return 1;
		}
		if (a.data.ItemDetail.ROADBLOCK_TICKET > b.data.ItemDetail.GROWUP)
		{
			return -1;
		}
		return 0;
	}
}
