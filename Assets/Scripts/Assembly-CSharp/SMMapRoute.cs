using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class SMMapRoute<T> where T : IComparable<T>
{
	public class RouteNode
	{
		internal T mValue;

		internal RouteNode mMain;

		internal RouteNode mSub;

		internal RouteNode mParent;

		internal Vector3 mPosition;

		internal Vector3 mRoutePosition;

		internal bool mEnableMove;

		public bool mIsStage;

		public string Name { get; private set; }

		public T Value
		{
			get
			{
				return mValue;
			}
			set
			{
				mValue = value;
			}
		}

		public RouteNode NextMain
		{
			get
			{
				return mMain;
			}
		}

		public RouteNode NextSub
		{
			get
			{
				return mSub;
			}
		}

		public RouteNode Parent
		{
			get
			{
				return mParent;
			}
		}

		public Vector3 Position
		{
			get
			{
				return mPosition;
			}
		}

		public Vector3 RoutePosition
		{
			get
			{
				return mRoutePosition;
			}
		}

		public bool IsSub
		{
			get
			{
				if (mParent != null && mParent.NextSub == this)
				{
					return true;
				}
				return false;
			}
		}

		public bool EnableMove
		{
			get
			{
				return mEnableMove;
			}
		}

		internal RouteNode()
			: this("none", default(T), false, (RouteNode)null, true)
		{
		}

		internal RouteNode(string a_name, T a_value, bool _is_stage, RouteNode a_parent, bool a_move)
		{
			Name = a_name;
			mValue = a_value;
			mParent = a_parent;
			mMain = null;
			mSub = null;
			mPosition = new Vector3(-100f, -100f, 0f);
			mRoutePosition = new Vector3(0f, 0f, 0f);
			mEnableMove = a_move;
			mIsStage = _is_stage;
		}

		public void SetMain(RouteNode a_next)
		{
			mMain = a_next;
		}

		public void SetSub(RouteNode a_next)
		{
			mSub = a_next;
		}

		public void SetParent(RouteNode a_parent)
		{
			mParent = a_parent;
		}

		public bool TryNextMain(out RouteNode a_next)
		{
			a_next = mMain;
			return a_next != null;
		}

		public bool TryNextSub(out RouteNode a_next)
		{
			a_next = mSub;
			return a_next != null;
		}

		public void SetPosition(Vector3 a_pos)
		{
			mPosition = a_pos;
		}

		public void SetRoutePosition(Vector3 a_pos)
		{
			mRoutePosition = a_pos;
		}
	}

	private RouteNode mRoot;

	public RouteNode Root
	{
		get
		{
			return mRoot;
		}
	}

	public SMMapRoute()
	{
		mRoot = null;
	}

	public RouteNode InsertMain(string a_name, T a_element, bool _isstage, RouteNode a_parent = null, float a_x = 0f, float a_y = 0f, float a_z = 0f, float a_offset = 0f, bool a_move = true)
	{
		if (mRoot == null)
		{
			MakeRoot(a_name, a_element, _isstage);
			mRoot.SetPosition(new Vector3(a_x, a_y, a_z));
			mRoot.SetRoutePosition(new Vector3(a_x, a_y + a_offset, a_z));
			return mRoot;
		}
		RouteNode routeNode = a_parent;
		if (routeNode == null)
		{
			routeNode = mRoot;
		}
		RouteNode routeNode2 = routeNode;
		RouteNode routeNode3 = null;
		bool flag = false;
		while (routeNode2 != null)
		{
			routeNode3 = routeNode2;
			if (routeNode2.Value.CompareTo(a_element) < 0)
			{
				routeNode2 = routeNode2.NextMain;
				continue;
			}
			flag = true;
			break;
		}
		if (flag)
		{
			RouteNode parent = routeNode3.Parent;
			routeNode2 = new RouteNode(a_name, a_element, _isstage, parent, a_move);
			routeNode2.SetPosition(new Vector3(a_x, a_y, a_z));
			routeNode2.SetRoutePosition(new Vector3(a_x, a_y + a_offset, a_z));
			parent.SetMain(routeNode2);
			routeNode2.SetMain(routeNode3);
			routeNode3.SetParent(routeNode2);
		}
		else
		{
			routeNode2 = new RouteNode(a_name, a_element, _isstage, routeNode3, a_move);
			routeNode2.SetPosition(new Vector3(a_x, a_y, a_z));
			routeNode2.SetRoutePosition(new Vector3(a_x, a_y + a_offset, a_z));
			routeNode3.SetMain(routeNode2);
		}
		return routeNode2;
	}

	public RouteNode InsertSub(string a_name, T a_element, bool _is_stage, RouteNode a_parent = null, float a_x = 0f, float a_y = 0f, float a_z = 0f, float a_offset = 0f, bool a_move = true)
	{
		if (mRoot == null)
		{
			MakeRoot(a_name, a_element, _is_stage);
			mRoot.SetPosition(new Vector3(a_x, a_y, a_z));
			mRoot.SetRoutePosition(new Vector3(a_x, a_y + a_offset, a_z));
			return mRoot;
		}
		RouteNode routeNode = a_parent;
		if (routeNode == null)
		{
			routeNode = mRoot;
		}
		RouteNode routeNode2 = routeNode;
		RouteNode routeNode3 = null;
		bool flag = false;
		while (routeNode2 != null)
		{
			routeNode3 = routeNode2;
			if (routeNode2.Value.CompareTo(a_element) < 0)
			{
				routeNode2 = routeNode2.NextSub;
				continue;
			}
			flag = true;
			break;
		}
		if (flag)
		{
			RouteNode parent = routeNode3.Parent;
			routeNode2 = new RouteNode(a_name, a_element, _is_stage, parent, a_move);
			routeNode2.SetPosition(new Vector3(a_x, a_y, a_z));
			routeNode2.SetRoutePosition(new Vector3(a_x, a_y + a_offset, a_z));
			parent.SetSub(routeNode2);
			routeNode2.SetSub(routeNode3);
			routeNode3.SetParent(routeNode2);
		}
		else
		{
			routeNode2 = new RouteNode(a_name, a_element, _is_stage, routeNode3, a_move);
			routeNode2.SetPosition(new Vector3(a_x, a_y, a_z));
			routeNode2.SetRoutePosition(new Vector3(a_x, a_y + a_offset, a_z));
			routeNode3.SetSub(routeNode2);
		}
		return routeNode2;
	}

	public void Delete(string a_name)
	{
		Delete(Find(a_name));
	}

	public void Delete(RouteNode a_node)
	{
		if (a_node == null)
		{
			return;
		}
		RouteNode parent = a_node.Parent;
		RouteNode nextMain = a_node.NextMain;
		if (parent != null)
		{
			if (nextMain != null)
			{
				nextMain.SetParent(parent);
				parent.SetMain(nextMain);
			}
			else
			{
				parent.SetMain(null);
			}
		}
		else if (nextMain != null)
		{
			nextMain.SetParent(null);
		}
	}

	public RouteNode Find(string a_name, RouteNode a_node = null)
	{
		RouteNode result = null;
		RouteNode routeNode = null;
		RouteNode routeNode2 = a_node;
		if (routeNode2 == null)
		{
			routeNode2 = mRoot;
		}
		routeNode = FindSub(a_name, routeNode2);
		if (routeNode != null)
		{
			result = routeNode;
		}
		return result;
	}

	public RouteNode FindOrder(T a_order, RouteNode a_node = null, bool isMain = true)
	{
		RouteNode result = null;
		RouteNode routeNode = null;
		RouteNode routeNode2 = a_node;
		if (routeNode2 == null)
		{
			routeNode2 = mRoot;
		}
		bool a_isMain = isMain;
		bool a_isSub = !isMain;
		routeNode = FindOrderSub(a_order, routeNode2, a_isMain, a_isSub);
		if (routeNode != null)
		{
			result = routeNode;
		}
		return result;
	}

	public RouteNode FindContain(string a_name, RouteNode a_node, bool a_isMain, bool a_isSub)
	{
		RouteNode result = null;
		RouteNode routeNode = null;
		RouteNode routeNode2 = a_node;
		if (routeNode2 == null)
		{
			routeNode2 = mRoot;
		}
		routeNode = FindSub(a_name, routeNode2, true, a_isMain, a_isSub);
		if (routeNode != null)
		{
			result = routeNode;
		}
		return result;
	}

	private RouteNode FindSub(string a_name, RouteNode a_node, bool a_isContain = false, bool a_isMain = true, bool a_isSub = true)
	{
		if (a_node.Name == a_name)
		{
			return a_node;
		}
		if (a_isContain && a_node.Name.Contains(a_name))
		{
			return a_node;
		}
		RouteNode result = null;
		RouteNode routeNode = null;
		RouteNode a_next;
		if (a_isMain && a_node.TryNextMain(out a_next))
		{
			routeNode = FindSub(a_name, a_next, a_isContain, a_isMain, a_isSub);
		}
		RouteNode routeNode2 = null;
		RouteNode a_next2;
		if (a_isSub && a_node.TryNextSub(out a_next2))
		{
			routeNode2 = FindSub(a_name, a_next2, a_isContain, a_isMain, a_isSub);
		}
		if (routeNode != null)
		{
			result = routeNode;
		}
		else if (routeNode2 != null)
		{
			result = routeNode2;
		}
		return result;
	}

	private RouteNode FindOrderSub(T a_order, RouteNode a_node, bool a_isMain = true, bool a_isSub = true)
	{
		if (a_node.Value.CompareTo(a_order) == 0)
		{
			return a_node;
		}
		RouteNode result = null;
		RouteNode routeNode = null;
		RouteNode a_next;
		if (a_isMain && a_node.TryNextMain(out a_next))
		{
			routeNode = FindOrderSub(a_order, a_next, a_isMain, a_isSub);
		}
		RouteNode routeNode2 = null;
		RouteNode a_next2;
		if (a_isSub && a_node.TryNextSub(out a_next2))
		{
			routeNode2 = FindOrderSub(a_order, a_next2, a_isMain, a_isSub);
		}
		if (routeNode != null)
		{
			result = routeNode;
		}
		else if (routeNode2 != null)
		{
			result = routeNode2;
		}
		return result;
	}

	public List<RouteNode> SearchPath(RouteNode a_target, RouteNode a_node)
	{
		List<RouteNode> list = new List<RouteNode>();
		RouteNode routeNode = null;
		RouteNode routeNode2 = null;
		if (a_target == a_node)
		{
			return list;
		}
		RouteNode a_root = null;
		RouteNode a_root2 = null;
		List<RouteNode> subRoot = GetSubRoot(a_target, out a_root);
		List<RouteNode> subRoot2 = GetSubRoot(a_node, out a_root2);
		if (a_root != null && a_root2 != null && a_root == a_root2)
		{
			routeNode = a_node;
			routeNode2 = null;
			while (routeNode != null)
			{
				routeNode2 = routeNode;
				list.Add(routeNode2);
				routeNode = ((a_target.Value.CompareTo(a_node.Value) >= 0) ? routeNode.NextSub : routeNode.Parent);
				if (routeNode == a_target)
				{
					break;
				}
			}
			list.Add(routeNode);
			return list;
		}
		if (a_root2 != null)
		{
			for (int i = 0; i < subRoot2.Count - 1; i++)
			{
				list.Add(subRoot2[i]);
			}
		}
		else
		{
			a_root2 = a_node;
		}
		bool flag = true;
		if (a_root == null)
		{
			a_root = a_target;
			flag = false;
		}
		routeNode = a_root2;
		routeNode2 = null;
		while (routeNode != null)
		{
			routeNode2 = routeNode;
			list.Add(routeNode2);
			routeNode = ((a_root.Value.CompareTo(a_root2.Value) >= 0) ? routeNode.NextMain : routeNode.Parent);
			if (routeNode == a_root)
			{
				break;
			}
		}
		list.Add(routeNode);
		if (flag)
		{
			for (int num = subRoot.Count - 2; num >= 0; num--)
			{
				list.Add(subRoot[num]);
			}
		}
		return list;
	}

	public List<RouteNode> SearchPath(string a_target, RouteNode a_node)
	{
		List<RouteNode> result = new List<RouteNode>();
		RouteNode routeNode = Find(a_target);
		if (routeNode == null)
		{
			return result;
		}
		return SearchPath(routeNode, a_node);
	}

	public List<RouteNode> GetSubRoot(RouteNode a_node, out RouteNode a_root)
	{
		a_root = null;
		List<RouteNode> list = new List<RouteNode>();
		if (!a_node.IsSub)
		{
			list.Add(a_node);
			return list;
		}
		RouteNode routeNode = null;
		RouteNode routeNode2 = null;
		routeNode = a_node;
		do
		{
			routeNode2 = routeNode;
			list.Add(routeNode2);
			routeNode = routeNode.Parent;
		}
		while (routeNode.IsSub);
		a_root = routeNode;
		list.Add(routeNode);
		return list;
	}

	public void MakeRoot(string a_name, T a_element, bool _is_stage)
	{
		mRoot = new RouteNode(a_name, a_element, _is_stage, null, true);
	}

	public void Print(string prefix, RouteNode a_root = null)
	{
		RouteNode routeNode = a_root;
		if (routeNode == null)
		{
			routeNode = mRoot;
		}
		RouteNode routeNode2 = routeNode;
		string text = string.Empty;
		while (routeNode2 != null)
		{
			text = text + routeNode2.Name.ToString() + "->";
			routeNode2 = routeNode2.NextMain;
		}
	}

	public void PrintPath(List<RouteNode> a_path)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("RoutePath = ");
		for (int i = 0; i < a_path.Count; i++)
		{
			stringBuilder.Append(a_path[i].Name);
			stringBuilder.Append("(");
			stringBuilder.Append(a_path[i].Value.ToString());
			stringBuilder.Append(")->");
		}
		stringBuilder.Append("End");
	}
}
