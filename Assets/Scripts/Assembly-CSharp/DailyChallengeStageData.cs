public class DailyChallengeStageData
{
	public int ID { get; set; }

	public int No { get; set; }

	public string Series { get; set; }

	public int MainNo { get; set; }

	public int RewardID { get; set; }

	public void Deserialize(BIJBinaryReader reader)
	{
	}

	public void Serialize(ref DailyChallengeStage data, ref DailyChallengeRewardListSetting init_list_setting, int idx)
	{
	}
}
