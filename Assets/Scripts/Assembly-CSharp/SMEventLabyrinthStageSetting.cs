public class SMEventLabyrinthStageSetting : SMMapPageSettingBase
{
	public short Difficulty;

	public short BoxKind;

	public short ItemAmount;

	public short ChanceFreq;

	public short ChanceItemLimit_1;

	public short ChanceItemLimit_3;

	public short ChanceItemLimit_5;

	public short ChanceItemLimit_9;

	public short ItemDropRate;

	public short ItemDropLimit_1;

	public short ItemDropLimit_3;

	public short ItemDropLimit_5;

	public short ItemDropLimit_9;

	public override void Serialize(BIJBinaryWriter a_writer)
	{
		a_writer.WriteShort((short)mCourseNo);
		a_writer.WriteShort((short)mStageNo);
		a_writer.WriteShort(Difficulty);
		a_writer.WriteShort(BoxKind);
		a_writer.WriteShort(ItemAmount);
		a_writer.WriteShort(ChanceFreq);
		a_writer.WriteShort(ChanceItemLimit_1);
		a_writer.WriteShort(ChanceItemLimit_3);
		a_writer.WriteShort(ChanceItemLimit_5);
		a_writer.WriteShort(ChanceItemLimit_9);
		a_writer.WriteShort(ItemDropRate);
		a_writer.WriteShort(ItemDropLimit_1);
		a_writer.WriteShort(ItemDropLimit_3);
		a_writer.WriteShort(ItemDropLimit_5);
		a_writer.WriteShort(ItemDropLimit_9);
	}

	public override void Deserialize(BIJBinaryReader a_reader)
	{
		mCourseNo = a_reader.ReadShort();
		mStageNo = a_reader.ReadShort();
		Difficulty = a_reader.ReadShort();
		BoxKind = a_reader.ReadShort();
		ItemAmount = a_reader.ReadShort();
		ChanceFreq = a_reader.ReadShort();
		ChanceItemLimit_1 = a_reader.ReadShort();
		ChanceItemLimit_3 = a_reader.ReadShort();
		ChanceItemLimit_5 = a_reader.ReadShort();
		ChanceItemLimit_9 = a_reader.ReadShort();
		ItemDropRate = a_reader.ReadShort();
		ItemDropLimit_1 = a_reader.ReadShort();
		ItemDropLimit_3 = a_reader.ReadShort();
		ItemDropLimit_5 = a_reader.ReadShort();
		ItemDropLimit_9 = a_reader.ReadShort();
	}
}
