using System;

public class UserAuthResponse : ICloneable
{
	[MiniJSONAlias("uuid")]
	public int UUID { get; set; }

	[MiniJSONAlias("bhiveid")]
	public int HiveID { get; set; }

	[MiniJSONAlias("status")]
	public int Status { get; set; }

	[MiniJSONAlias("transfer_id")]
	public string TransferId { get; set; }

	[MiniJSONAlias("transfer_pass")]
	public string TransferPass { get; set; }

	public UserAuthResponse()
	{
		UUID = 0;
		HiveID = 0;
		Status = 0;
		TransferId = string.Empty;
		TransferPass = string.Empty;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public UserAuthResponse Clone()
	{
		return MemberwiseClone() as UserAuthResponse;
	}
}
