using System;
using System.Text;

public class DeepLink : ICloneable
{
	public const int PLACE_STORY = 0;

	public const int PLACE_ARCADE = 1;

	public LinkBannerInfo mLinkBannerInfoCache;

	[MiniJSONAlias("transition")]
	public string Action { get; set; }

	[MiniJSONAlias("trans_subid")]
	public int Param { get; set; }

	[MiniJSONAlias("page_no")]
	public int PageNo { get; set; }

	[MiniJSONAlias("info_place")]
	public int InfoPlace { get; set; }

	[MiniJSONAlias("banner_id")]
	public string BannerID { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public DeepLink Clone()
	{
		return MemberwiseClone() as DeepLink;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("\nAction=");
		stringBuilder.Append(Action);
		stringBuilder.Append("\nParam=");
		stringBuilder.Append(Param.ToString());
		stringBuilder.Append("\nPageNo=");
		stringBuilder.Append(PageNo.ToString());
		stringBuilder.Append("\nInfoPlace=");
		stringBuilder.Append(InfoPlace.ToString());
		stringBuilder.Append("\nBannerID=");
		stringBuilder.Append(BannerID.ToString());
		return string.Format("[DeepLink: {0}]", stringBuilder.ToString());
	}

	public LinkBannerInfo GetLinkBannerInfo()
	{
		if (mLinkBannerInfoCache == null)
		{
			mLinkBannerInfoCache = new LinkBannerInfo();
			mLinkBannerInfoCache.SetAction(Action);
			mLinkBannerInfoCache.param = Param;
		}
		return mLinkBannerInfoCache;
	}
}
