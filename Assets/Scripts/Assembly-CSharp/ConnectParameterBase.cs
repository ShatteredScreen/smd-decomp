using System;

public class ConnectParameterBase : ICloneable
{
	public delegate bool ConnectSuccessHandler(ConnectBase a_target);

	private ConnectSuccessHandler mSuccessCallback;

	public bool IsIgnoreError { get; protected set; }

	public ConnectCommonErrorDialog.STYLE OverrideErrorDialogStyle { get; protected set; }

	public ConnectCommonErrorDialog.STYLE OverrideMaintenanceDialogStyle { get; protected set; }

	public ConnectParameterBase()
	{
		IsIgnoreError = false;
		OverrideErrorDialogStyle = ConnectCommonErrorDialog.STYLE.NONE;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void SetIgnoreError(bool a_isshow)
	{
		IsIgnoreError = a_isshow;
	}

	public void SetErrorDialogStyle(ConnectCommonErrorDialog.STYLE a_style)
	{
		OverrideErrorDialogStyle = a_style;
	}

	public void SetMaintenanceDialogStyle(ConnectCommonErrorDialog.STYLE a_style)
	{
		OverrideMaintenanceDialogStyle = a_style;
	}

	public void SetSuccessCallback(ConnectSuccessHandler a_callback)
	{
		mSuccessCallback = a_callback;
	}

	public bool RunSuccessCallback(ConnectBase a_target)
	{
		bool result = false;
		if (mSuccessCallback != null)
		{
			result = mSuccessCallback(a_target);
		}
		return result;
	}

	public ConnectParameterBase Clone()
	{
		return MemberwiseClone() as ConnectParameterBase;
	}
}
