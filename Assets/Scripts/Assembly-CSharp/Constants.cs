using System;
using System.Collections.Generic;

public sealed class Constants
{
	public static string OPTIONS_FILE { get; private set; }

	public static string PLAYER_FILE { get; private set; }

	public static string PLAYER_FILE_OLD { get; private set; }

	public static string PLAYER_FILE_HASH { get; private set; }

	public static string BACKUP_FILE { get; private set; }

	public static string BACKUP_FILE_OLD { get; private set; }

	public static string METADATA_FILE { get; private set; }

	public static string STAGE_METADATA_FILE { get; private set; }

	public static string PURCHASE_FILE { get; private set; }

	public static string CAMPAIGN_FILE { get; private set; }

	public static string TRANSFER_FILE { get; private set; }

	public static string TRANSFER_FILE_OLD { get; private set; }

	public static string OTHER_PLAYER_FILE { get; private set; }

	public static string OTHER_PLAYER_FILE_OLD { get; private set; }

	public static float FREEMIUM_NOTICE_TIME { get; private set; }

	public static int HAVE_LIMIT_SD { get; private set; }

	public static int PURCHASE_LIMIT_SD { get; private set; }

	public static int PURCHASE_MAX_SEQUENCE_TIME { get; private set; }

	public static int PURCHASE_MAX_SEQUENCE_COUNT { get; private set; }

	public static float DEF_BGM_VOL { get; private set; }

	public static float DEF_SE_VOL { get; private set; }

	public static int MAX_CHARA_LEVEL { get; private set; }

	public static int FIRST_LEVEL { get; private set; }

	public static int FIRST_OPENLEVEL { get; private set; }

	public static int FIRST_MAX_LIFE { get; private set; }

	public static int FIRST_LIFE { get; private set; }

	public static int FIRST_MAX_EXTENDLIFE { get; private set; }

	public static int FIRST_EXTENDLIFE { get; private set; }

	public static int FIRST_SD { get; private set; }

	public static int LIFE_RECOVER_SEC { get; private set; }

	public static int REMOTE_TIMECHEAT_CHECK { get; private set; }

	public static double LOCAL_TIMECHEAT_MARGIN { get; private set; }

	public static double LOCAL_TIMECHEAT_PENALTY { get; private set; }

	public static int SNS_FRIEND_MAX { get; private set; }

	public static int GAME_FRIEND_MAX { get; private set; }

	public static int DISPLAY_RANDOM_FRIEND_MAX { get; private set; }

	public static int RANDOM_FRIEND_MAX { get; private set; }

	public static int APPLY_RANDOM_FRIEND_MAX { get; private set; }

	public static long GET_GAME_INFO_FREQ { get; private set; }

	public static long GET_SEGMENT_INFO_FREQ { get; private set; }

	public static long GET_LOGINBONUS_FREQ { get; private set; }

	public static long GET_SUPPORT_FREQ { get; private set; }

	public static long GET_SUPPORTPURCHASE_FREQ { get; private set; }

	public static long GET_GIFT_FREQ { get; private set; }

	public static long GET_APPLY_FREQ { get; private set; }

	public static long GET_FRIEND_FREQ { get; private set; }

	public static long GET_SOCIAL_FREQ { get; private set; }

	public static long GET_FRIEND_ICON_FREQ { get; private set; }

	public static long GET_ONE_ICON_FREQ { get; private set; }

	public static long GET_RANDOM_USER_FREQ { get; private set; }

	public static long GET_GEMCOUNT_FREQ { get; private set; }

	public static long GET_AUTHUUID_FREQ { get; private set; }

	public static long GET_MAINTENANCE_INFO_FREQ { get; private set; }

	public static int GIFT_SUPPORT_AMOUNT { get; private set; }

	public static int GIFT_AMOUNT { get; private set; }

	public static string INVITE_CODE { get; private set; }

	public static string INVITE_URL { get; private set; }

	public static int INVITE_COOLDOWN_INTERVAL { get; private set; }

	public static string POST_CODE { get; private set; }

	public static string POST_URL { get; private set; }

	public static string STORE_URL_AD { get; private set; }

	public static string STORE_URL_IOS { get; private set; }

	public static int GIFT_EXPIRED { get; private set; }

	public static int FRIENDHEART_GIFT_EXPIRED { get; private set; }

	public static int FRIENDHELP_EXPIRED { get; private set; }

	public static int GIFT_EXPIRED_CHECK_FREQ { get; private set; }

	public static int SEND_FRIENDHELP_FREQ { get; private set; }

	public static int SEND_HEART_FREQ { get; private set; }

	public static int SEND_ROADBLOCK_FREQ { get; private set; }

	public static int GIFT_EXPIRED_FRIEND_REQ_FREQ { get; private set; }

	public static int RATING_FREQ { get; private set; }

	public static int RATING_MAX { get; private set; }

	public static int RATING_LEVEL { get; private set; }

	public static int EVENT_LASTDAY_NUM { get; private set; }

	public static string CHARLIE00_CODE { get; private set; }

	public static string CHARLIE00_URL { get; private set; }

	public static string RESOURCEDL_LOCALFILE_OLD { get; private set; }

	public static string RESOURCEDL_LOCALFILE { get; private set; }

	public static string RESOURCEDL_SERVERFILE { get; private set; }

	public static string RESOURCEDL_TEMPFILE { get; private set; }

	public static int RESOURCEDL_CHECKSUM_BUFFSIZ { get; private set; }

	public static string CRYPT_AES_IV { get; private set; }

	public static string CRYPT_AES_KEY { get; private set; }

	public static int[] CHAR_EXPERIENCE_LEVELS { get; private set; }

	public static int[] MAX_CHARACTER_LEVEL { get; private set; }

	public static int SAFE_MAX_CHARACTER_LEVEL { get; private set; }

	public static int SAFE_MAX_CHARACTER_SKILLLEVEL { get; private set; }

	public static float[] CHARACTER_LEVELUP_GAIN_COST { get; private set; }

	public static float[] CHARACTER_LEVELUP_GAIN_HP { get; private set; }

	public static float[] CHARACTER_LEVELUP_GAIN_ATK { get; private set; }

	public static float[] CHARACTER_LEVELUP_GAIN_RECOVER { get; private set; }

	public static int HAVEPARTY_NUM { get; private set; }

	public static int BATTLE_CHAR_NUM { get; private set; }

	public static int UNLIMITLEVEL_MAX_NUM { get; private set; }

	public static int UNLIMITLEVEL_UNIT_GAIN_MAXLEVEL { get; private set; }

	public static int TOKEN_EXCHANGERATE { get; private set; }

	public static float SELL_LEVEL_GAIN { get; private set; }

	public static float PWXP_MUL { get; private set; }

	public static float PWCOST_LEVEL_GAIN { get; private set; }

	public static int MAX_ENEMY_NUM { get; private set; }

	public static float[] CHARGEPER_UP_TABLE { get; private set; }

	public static float[] COMBOPER_UP_TABLE { get; private set; }

	public static float CHARGEPER_UP_RECOVERMUL { get; private set; }

	public static int ADV_GACHATICKET_NORMAL_MAX { get; private set; }

	public static int ADV_GACHATICKET_PREMIUM_MAX { get; private set; }

	public static int ADV_CHARGEEXP_MAX { get; private set; }

	public static int ADV_SKILLCHARGEEXP_MAX { get; private set; }

	public static float ADV_CONTINUE_RECOVER_PER { get; private set; }

	public static float ADV_GIVEUP_EXP_PER { get; private set; }

	public static int ADV_FIRST_STAMINA { get; private set; }

	public static int ADV_STAMINA_RECOVER_SEC { get; private set; }

	public static int ADV_CONVERT_RATE { get; private set; }

	public static int ADV_CLEARSTAGE_BORDER { get; private set; }

	public static int ADV_LOTTERYRANDOMARRAY_SIZE { get; private set; }

	public static string ADV_FILE { get; private set; }

	public static string ADV_BACKUP { get; private set; }

	public static string ADV_MASTERTABLE_FILE { get; private set; }

	public static string ADV_TRANSFER_FILE { get; private set; }

	public static string LOGINBONUS_BACKUP_FILE { get; private set; }

	public static int GUIDE_TO_SEC { get; private set; }

	public static int ADV_MEDAL_BUY_NUM { get; private set; }

	public static string OTHER_PLAYER_FILE_CX { get; private set; }

	public static string AUTHKEY_FILE { get; private set; }

	public static string UDINFO_FILE { get; private set; }

	public static string USERBEHAVIOR_FILE { get; private set; }

	private Constants()
	{
	}

	public static void Load()
	{
		using (BIJResourceBinaryReader reader = new BIJResourceBinaryReader("Data/constants.bin"))
		{
			Load(reader);
		}
	}

	public static void Load(BIJBinaryReader reader)
	{
		OPTIONS_FILE = reader.ReadUTF();
		PLAYER_FILE = reader.ReadUTF();
		PLAYER_FILE_OLD = reader.ReadUTF();
		PLAYER_FILE_HASH = reader.ReadUTF();
		BACKUP_FILE = reader.ReadUTF();
		BACKUP_FILE_OLD = reader.ReadUTF();
		METADATA_FILE = reader.ReadUTF();
		STAGE_METADATA_FILE = reader.ReadUTF();
		PURCHASE_FILE = reader.ReadUTF();
		CAMPAIGN_FILE = reader.ReadUTF();
		TRANSFER_FILE = reader.ReadUTF();
		TRANSFER_FILE_OLD = reader.ReadUTF();
		OTHER_PLAYER_FILE = reader.ReadUTF();
		OTHER_PLAYER_FILE_OLD = reader.ReadUTF();
		FREEMIUM_NOTICE_TIME = reader.ReadFloat();
		HAVE_LIMIT_SD = reader.ReadInt();
		PURCHASE_LIMIT_SD = reader.ReadInt();
		PURCHASE_MAX_SEQUENCE_TIME = reader.ReadInt();
		PURCHASE_MAX_SEQUENCE_COUNT = reader.ReadInt();
		DEF_BGM_VOL = reader.ReadFloat();
		DEF_SE_VOL = reader.ReadFloat();
		MAX_CHARA_LEVEL = reader.ReadInt();
		FIRST_LEVEL = reader.ReadInt();
		FIRST_OPENLEVEL = reader.ReadInt();
		FIRST_MAX_LIFE = reader.ReadInt();
		FIRST_LIFE = reader.ReadInt();
		FIRST_MAX_EXTENDLIFE = reader.ReadInt();
		FIRST_EXTENDLIFE = reader.ReadInt();
		FIRST_SD = reader.ReadInt();
		LIFE_RECOVER_SEC = reader.ReadInt();
		REMOTE_TIMECHEAT_CHECK = reader.ReadInt();
		LOCAL_TIMECHEAT_MARGIN = reader.ReadDouble();
		LOCAL_TIMECHEAT_PENALTY = reader.ReadDouble();
		SNS_FRIEND_MAX = reader.ReadInt();
		GAME_FRIEND_MAX = reader.ReadInt();
		DISPLAY_RANDOM_FRIEND_MAX = reader.ReadInt();
		RANDOM_FRIEND_MAX = reader.ReadInt();
		APPLY_RANDOM_FRIEND_MAX = reader.ReadInt();
		GET_GAME_INFO_FREQ = reader.ReadLong();
		GET_SEGMENT_INFO_FREQ = reader.ReadLong();
		GET_LOGINBONUS_FREQ = reader.ReadLong();
		GET_SUPPORT_FREQ = reader.ReadLong();
		GET_SUPPORTPURCHASE_FREQ = reader.ReadLong();
		GET_GIFT_FREQ = reader.ReadLong();
		GET_APPLY_FREQ = reader.ReadLong();
		GET_FRIEND_FREQ = reader.ReadLong();
		GET_SOCIAL_FREQ = reader.ReadLong();
		GET_FRIEND_ICON_FREQ = reader.ReadLong();
		GET_ONE_ICON_FREQ = reader.ReadLong();
		GET_RANDOM_USER_FREQ = reader.ReadLong();
		GET_GEMCOUNT_FREQ = reader.ReadLong();
		GET_AUTHUUID_FREQ = reader.ReadLong();
		GET_MAINTENANCE_INFO_FREQ = reader.ReadLong();
		GIFT_SUPPORT_AMOUNT = reader.ReadInt();
		GIFT_AMOUNT = reader.ReadInt();
		INVITE_CODE = reader.ReadUTF();
		INVITE_URL = reader.ReadUTF();
		INVITE_COOLDOWN_INTERVAL = reader.ReadInt();
		POST_CODE = reader.ReadUTF();
		POST_URL = reader.ReadUTF();
		STORE_URL_AD = reader.ReadUTF();
		STORE_URL_IOS = reader.ReadUTF();
		GIFT_EXPIRED = reader.ReadInt();
		FRIENDHEART_GIFT_EXPIRED = reader.ReadInt();
		FRIENDHELP_EXPIRED = reader.ReadInt();
		GIFT_EXPIRED_CHECK_FREQ = reader.ReadInt();
		SEND_FRIENDHELP_FREQ = reader.ReadInt();
		SEND_HEART_FREQ = reader.ReadInt();
		SEND_ROADBLOCK_FREQ = reader.ReadInt();
		GIFT_EXPIRED_FRIEND_REQ_FREQ = reader.ReadInt();
		RATING_FREQ = reader.ReadInt();
		RATING_MAX = reader.ReadInt();
		RATING_LEVEL = reader.ReadInt();
		EVENT_LASTDAY_NUM = reader.ReadInt();
		CHARLIE00_CODE = reader.ReadUTF();
		CHARLIE00_URL = reader.ReadUTF();
		RESOURCEDL_LOCALFILE_OLD = reader.ReadUTF();
		RESOURCEDL_LOCALFILE = reader.ReadUTF();
		RESOURCEDL_SERVERFILE = reader.ReadUTF();
		RESOURCEDL_TEMPFILE = reader.ReadUTF();
		RESOURCEDL_CHECKSUM_BUFFSIZ = reader.ReadInt();
		CRYPT_AES_IV = reader.ReadUTF();
		CRYPT_AES_KEY = reader.ReadUTF();
		CHAR_EXPERIENCE_LEVELS = reader.ReadIntArray();
		MAX_CHARACTER_LEVEL = reader.ReadIntArray();
		SAFE_MAX_CHARACTER_LEVEL = reader.ReadInt();
		SAFE_MAX_CHARACTER_SKILLLEVEL = reader.ReadInt();
		CHARACTER_LEVELUP_GAIN_COST = reader.ReadFloatArray();
		CHARACTER_LEVELUP_GAIN_HP = reader.ReadFloatArray();
		CHARACTER_LEVELUP_GAIN_ATK = reader.ReadFloatArray();
		CHARACTER_LEVELUP_GAIN_RECOVER = reader.ReadFloatArray();
		HAVEPARTY_NUM = reader.ReadInt();
		BATTLE_CHAR_NUM = reader.ReadInt();
		UNLIMITLEVEL_MAX_NUM = reader.ReadInt();
		UNLIMITLEVEL_UNIT_GAIN_MAXLEVEL = reader.ReadInt();
		TOKEN_EXCHANGERATE = reader.ReadInt();
		SELL_LEVEL_GAIN = reader.ReadFloat();
		PWXP_MUL = reader.ReadFloat();
		PWCOST_LEVEL_GAIN = reader.ReadFloat();
		MAX_ENEMY_NUM = reader.ReadInt();
		CHARGEPER_UP_TABLE = reader.ReadFloatArray();
		COMBOPER_UP_TABLE = reader.ReadFloatArray();
		CHARGEPER_UP_RECOVERMUL = reader.ReadFloat();
		ADV_GACHATICKET_NORMAL_MAX = reader.ReadInt();
		ADV_GACHATICKET_PREMIUM_MAX = reader.ReadInt();
		ADV_CHARGEEXP_MAX = reader.ReadInt();
		ADV_SKILLCHARGEEXP_MAX = reader.ReadInt();
		ADV_CONTINUE_RECOVER_PER = reader.ReadFloat();
		ADV_GIVEUP_EXP_PER = reader.ReadFloat();
		ADV_FIRST_STAMINA = reader.ReadInt();
		ADV_STAMINA_RECOVER_SEC = reader.ReadInt();
		ADV_CONVERT_RATE = reader.ReadInt();
		ADV_CLEARSTAGE_BORDER = reader.ReadInt();
		ADV_LOTTERYRANDOMARRAY_SIZE = reader.ReadInt();
		ADV_FILE = reader.ReadUTF();
		ADV_BACKUP = reader.ReadUTF();
		ADV_MASTERTABLE_FILE = reader.ReadUTF();
		ADV_TRANSFER_FILE = reader.ReadUTF();
		LOGINBONUS_BACKUP_FILE = reader.ReadUTF();
		GUIDE_TO_SEC = reader.ReadInt();
		ADV_MEDAL_BUY_NUM = reader.ReadInt();
		OTHER_PLAYER_FILE_CX = reader.ReadUTF();
		AUTHKEY_FILE = reader.ReadUTF();
		UDINFO_FILE = reader.ReadUTF();
		USERBEHAVIOR_FILE = reader.ReadUTF();
	}

	public static void Unload()
	{
		CHAR_EXPERIENCE_LEVELS = null;
		MAX_CHARACTER_LEVEL = null;
		CHARACTER_LEVELUP_GAIN_COST = null;
		CHARACTER_LEVELUP_GAIN_HP = null;
		CHARACTER_LEVELUP_GAIN_ATK = null;
		CHARACTER_LEVELUP_GAIN_RECOVER = null;
		CHARGEPER_UP_TABLE = null;
		COMBOPER_UP_TABLE = null;
	}

	public new static string ToString()
	{
		Type typeFromHandle = typeof(Constants);
		List<string> list = new List<string>();
		list.Add(string.Format("{0}={1}", "OPTIONS_FILE", OPTIONS_FILE));
		list.Add(string.Format("{0}={1}", "PLAYER_FILE", PLAYER_FILE));
		list.Add(string.Format("{0}={1}", "PLAYER_FILE_OLD", PLAYER_FILE_OLD));
		list.Add(string.Format("{0}={1}", "PLAYER_FILE_HASH", PLAYER_FILE_HASH));
		list.Add(string.Format("{0}={1}", "BACKUP_FILE", BACKUP_FILE));
		list.Add(string.Format("{0}={1}", "BACKUP_FILE_OLD", BACKUP_FILE_OLD));
		list.Add(string.Format("{0}={1}", "METADATA_FILE", METADATA_FILE));
		list.Add(string.Format("{0}={1}", "STAGE_METADATA_FILE", STAGE_METADATA_FILE));
		list.Add(string.Format("{0}={1}", "PURCHASE_FILE", PURCHASE_FILE));
		list.Add(string.Format("{0}={1}", "CAMPAIGN_FILE", CAMPAIGN_FILE));
		list.Add(string.Format("{0}={1}", "TRANSFER_FILE", TRANSFER_FILE));
		list.Add(string.Format("{0}={1}", "TRANSFER_FILE_OLD", TRANSFER_FILE_OLD));
		list.Add(string.Format("{0}={1}", "OTHER_PLAYER_FILE", OTHER_PLAYER_FILE));
		list.Add(string.Format("{0}={1}", "OTHER_PLAYER_FILE_OLD", OTHER_PLAYER_FILE_OLD));
		list.Add(string.Format("{0}={1}", "FREEMIUM_NOTICE_TIME", FREEMIUM_NOTICE_TIME));
		list.Add(string.Format("{0}={1}", "HAVE_LIMIT_SD", HAVE_LIMIT_SD));
		list.Add(string.Format("{0}={1}", "PURCHASE_LIMIT_SD", PURCHASE_LIMIT_SD));
		list.Add(string.Format("{0}={1}", "PURCHASE_MAX_SEQUENCE_TIME", PURCHASE_MAX_SEQUENCE_TIME));
		list.Add(string.Format("{0}={1}", "PURCHASE_MAX_SEQUENCE_COUNT", PURCHASE_MAX_SEQUENCE_COUNT));
		list.Add(string.Format("{0}={1}", "DEF_BGM_VOL", DEF_BGM_VOL));
		list.Add(string.Format("{0}={1}", "DEF_SE_VOL", DEF_SE_VOL));
		list.Add(string.Format("{0}={1}", "MAX_CHARA_LEVEL", MAX_CHARA_LEVEL));
		list.Add(string.Format("{0}={1}", "FIRST_LEVEL", FIRST_LEVEL));
		list.Add(string.Format("{0}={1}", "FIRST_OPENLEVEL", FIRST_OPENLEVEL));
		list.Add(string.Format("{0}={1}", "FIRST_MAX_LIFE", FIRST_MAX_LIFE));
		list.Add(string.Format("{0}={1}", "FIRST_LIFE", FIRST_LIFE));
		list.Add(string.Format("{0}={1}", "FIRST_MAX_EXTENDLIFE", FIRST_MAX_EXTENDLIFE));
		list.Add(string.Format("{0}={1}", "FIRST_EXTENDLIFE", FIRST_EXTENDLIFE));
		list.Add(string.Format("{0}={1}", "FIRST_SD", FIRST_SD));
		list.Add(string.Format("{0}={1}", "LIFE_RECOVER_SEC", LIFE_RECOVER_SEC));
		list.Add(string.Format("{0}={1}", "REMOTE_TIMECHEAT_CHECK", REMOTE_TIMECHEAT_CHECK));
		list.Add(string.Format("{0}={1}", "LOCAL_TIMECHEAT_MARGIN", LOCAL_TIMECHEAT_MARGIN));
		list.Add(string.Format("{0}={1}", "LOCAL_TIMECHEAT_PENALTY", LOCAL_TIMECHEAT_PENALTY));
		list.Add(string.Format("{0}={1}", "SNS_FRIEND_MAX", SNS_FRIEND_MAX));
		list.Add(string.Format("{0}={1}", "GAME_FRIEND_MAX", GAME_FRIEND_MAX));
		list.Add(string.Format("{0}={1}", "DISPLAY_RANDOM_FRIEND_MAX", DISPLAY_RANDOM_FRIEND_MAX));
		list.Add(string.Format("{0}={1}", "RANDOM_FRIEND_MAX", RANDOM_FRIEND_MAX));
		list.Add(string.Format("{0}={1}", "APPLY_RANDOM_FRIEND_MAX", APPLY_RANDOM_FRIEND_MAX));
		list.Add(string.Format("{0}={1}", "GET_GAME_INFO_FREQ", GET_GAME_INFO_FREQ));
		list.Add(string.Format("{0}={1}", "GET_SEGMENT_INFO_FREQ", GET_SEGMENT_INFO_FREQ));
		list.Add(string.Format("{0}={1}", "GET_LOGINBONUS_FREQ", GET_LOGINBONUS_FREQ));
		list.Add(string.Format("{0}={1}", "GET_SUPPORT_FREQ", GET_SUPPORT_FREQ));
		list.Add(string.Format("{0}={1}", "GET_SUPPORTPURCHASE_FREQ", GET_SUPPORTPURCHASE_FREQ));
		list.Add(string.Format("{0}={1}", "GET_GIFT_FREQ", GET_GIFT_FREQ));
		list.Add(string.Format("{0}={1}", "GET_APPLY_FREQ", GET_APPLY_FREQ));
		list.Add(string.Format("{0}={1}", "GET_FRIEND_FREQ", GET_FRIEND_FREQ));
		list.Add(string.Format("{0}={1}", "GET_SOCIAL_FREQ", GET_SOCIAL_FREQ));
		list.Add(string.Format("{0}={1}", "GET_FRIEND_ICON_FREQ", GET_FRIEND_ICON_FREQ));
		list.Add(string.Format("{0}={1}", "GET_ONE_ICON_FREQ", GET_ONE_ICON_FREQ));
		list.Add(string.Format("{0}={1}", "GET_RANDOM_USER_FREQ", GET_RANDOM_USER_FREQ));
		list.Add(string.Format("{0}={1}", "GET_GEMCOUNT_FREQ", GET_GEMCOUNT_FREQ));
		list.Add(string.Format("{0}={1}", "GET_AUTHUUID_FREQ", GET_AUTHUUID_FREQ));
		list.Add(string.Format("{0}={1}", "GET_MAINTENANCE_INFO_FREQ", GET_MAINTENANCE_INFO_FREQ));
		list.Add(string.Format("{0}={1}", "GIFT_SUPPORT_AMOUNT", GIFT_SUPPORT_AMOUNT));
		list.Add(string.Format("{0}={1}", "GIFT_AMOUNT", GIFT_AMOUNT));
		list.Add(string.Format("{0}={1}", "INVITE_CODE", INVITE_CODE));
		list.Add(string.Format("{0}={1}", "INVITE_URL", INVITE_URL));
		list.Add(string.Format("{0}={1}", "INVITE_COOLDOWN_INTERVAL", INVITE_COOLDOWN_INTERVAL));
		list.Add(string.Format("{0}={1}", "POST_CODE", POST_CODE));
		list.Add(string.Format("{0}={1}", "POST_URL", POST_URL));
		list.Add(string.Format("{0}={1}", "STORE_URL_AD", STORE_URL_AD));
		list.Add(string.Format("{0}={1}", "STORE_URL_IOS", STORE_URL_IOS));
		list.Add(string.Format("{0}={1}", "GIFT_EXPIRED", GIFT_EXPIRED));
		list.Add(string.Format("{0}={1}", "FRIENDHEART_GIFT_EXPIRED", FRIENDHEART_GIFT_EXPIRED));
		list.Add(string.Format("{0}={1}", "FRIENDHELP_EXPIRED", FRIENDHELP_EXPIRED));
		list.Add(string.Format("{0}={1}", "GIFT_EXPIRED_CHECK_FREQ", GIFT_EXPIRED_CHECK_FREQ));
		list.Add(string.Format("{0}={1}", "SEND_FRIENDHELP_FREQ", SEND_FRIENDHELP_FREQ));
		list.Add(string.Format("{0}={1}", "SEND_HEART_FREQ", SEND_HEART_FREQ));
		list.Add(string.Format("{0}={1}", "SEND_ROADBLOCK_FREQ", SEND_ROADBLOCK_FREQ));
		list.Add(string.Format("{0}={1}", "GIFT_EXPIRED_FRIEND_REQ_FREQ", GIFT_EXPIRED_FRIEND_REQ_FREQ));
		list.Add(string.Format("{0}={1}", "RATING_FREQ", RATING_FREQ));
		list.Add(string.Format("{0}={1}", "RATING_MAX", RATING_MAX));
		list.Add(string.Format("{0}={1}", "RATING_LEVEL", RATING_LEVEL));
		list.Add(string.Format("{0}={1}", "EVENT_LASTDAY_NUM", EVENT_LASTDAY_NUM));
		list.Add(string.Format("{0}={1}", "CHARLIE00_CODE", CHARLIE00_CODE));
		list.Add(string.Format("{0}={1}", "CHARLIE00_URL", CHARLIE00_URL));
		list.Add(string.Format("{0}={1}", "RESOURCEDL_LOCALFILE_OLD", RESOURCEDL_LOCALFILE_OLD));
		list.Add(string.Format("{0}={1}", "RESOURCEDL_LOCALFILE", RESOURCEDL_LOCALFILE));
		list.Add(string.Format("{0}={1}", "RESOURCEDL_SERVERFILE", RESOURCEDL_SERVERFILE));
		list.Add(string.Format("{0}={1}", "RESOURCEDL_TEMPFILE", RESOURCEDL_TEMPFILE));
		list.Add(string.Format("{0}={1}", "RESOURCEDL_CHECKSUM_BUFFSIZ", RESOURCEDL_CHECKSUM_BUFFSIZ));
		list.Add(string.Format("{0}={1}", "CRYPT_AES_IV", CRYPT_AES_IV));
		list.Add(string.Format("{0}={1}", "CRYPT_AES_KEY", CRYPT_AES_KEY));
		list.Add(string.Format("{0}={1}", "CHAR_EXPERIENCE_LEVELS", CHAR_EXPERIENCE_LEVELS));
		list.Add(string.Format("{0}={1}", "MAX_CHARACTER_LEVEL", MAX_CHARACTER_LEVEL));
		list.Add(string.Format("{0}={1}", "SAFE_MAX_CHARACTER_LEVEL", SAFE_MAX_CHARACTER_LEVEL));
		list.Add(string.Format("{0}={1}", "SAFE_MAX_CHARACTER_SKILLLEVEL", SAFE_MAX_CHARACTER_SKILLLEVEL));
		list.Add(string.Format("{0}={1}", "CHARACTER_LEVELUP_GAIN_COST", CHARACTER_LEVELUP_GAIN_COST));
		list.Add(string.Format("{0}={1}", "CHARACTER_LEVELUP_GAIN_HP", CHARACTER_LEVELUP_GAIN_HP));
		list.Add(string.Format("{0}={1}", "CHARACTER_LEVELUP_GAIN_ATK", CHARACTER_LEVELUP_GAIN_ATK));
		list.Add(string.Format("{0}={1}", "CHARACTER_LEVELUP_GAIN_RECOVER", CHARACTER_LEVELUP_GAIN_RECOVER));
		list.Add(string.Format("{0}={1}", "HAVEPARTY_NUM", HAVEPARTY_NUM));
		list.Add(string.Format("{0}={1}", "BATTLE_CHAR_NUM", BATTLE_CHAR_NUM));
		list.Add(string.Format("{0}={1}", "UNLIMITLEVEL_MAX_NUM", UNLIMITLEVEL_MAX_NUM));
		list.Add(string.Format("{0}={1}", "UNLIMITLEVEL_UNIT_GAIN_MAXLEVEL", UNLIMITLEVEL_UNIT_GAIN_MAXLEVEL));
		list.Add(string.Format("{0}={1}", "TOKEN_EXCHANGERATE", TOKEN_EXCHANGERATE));
		list.Add(string.Format("{0}={1}", "SELL_LEVEL_GAIN", SELL_LEVEL_GAIN));
		list.Add(string.Format("{0}={1}", "PWXP_MUL", PWXP_MUL));
		list.Add(string.Format("{0}={1}", "PWCOST_LEVEL_GAIN", PWCOST_LEVEL_GAIN));
		list.Add(string.Format("{0}={1}", "MAX_ENEMY_NUM", MAX_ENEMY_NUM));
		list.Add(string.Format("{0}={1}", "CHARGEPER_UP_TABLE", CHARGEPER_UP_TABLE));
		list.Add(string.Format("{0}={1}", "COMBOPER_UP_TABLE", COMBOPER_UP_TABLE));
		list.Add(string.Format("{0}={1}", "CHARGEPER_UP_RECOVERMUL", CHARGEPER_UP_RECOVERMUL));
		list.Add(string.Format("{0}={1}", "ADV_GACHATICKET_NORMAL_MAX", ADV_GACHATICKET_NORMAL_MAX));
		list.Add(string.Format("{0}={1}", "ADV_GACHATICKET_PREMIUM_MAX", ADV_GACHATICKET_PREMIUM_MAX));
		list.Add(string.Format("{0}={1}", "ADV_CHARGEEXP_MAX", ADV_CHARGEEXP_MAX));
		list.Add(string.Format("{0}={1}", "ADV_SKILLCHARGEEXP_MAX", ADV_SKILLCHARGEEXP_MAX));
		list.Add(string.Format("{0}={1}", "ADV_CONTINUE_RECOVER_PER", ADV_CONTINUE_RECOVER_PER));
		list.Add(string.Format("{0}={1}", "ADV_GIVEUP_EXP_PER", ADV_GIVEUP_EXP_PER));
		list.Add(string.Format("{0}={1}", "ADV_FIRST_STAMINA", ADV_FIRST_STAMINA));
		list.Add(string.Format("{0}={1}", "ADV_STAMINA_RECOVER_SEC", ADV_STAMINA_RECOVER_SEC));
		list.Add(string.Format("{0}={1}", "ADV_CONVERT_RATE", ADV_CONVERT_RATE));
		list.Add(string.Format("{0}={1}", "ADV_CLEARSTAGE_BORDER", ADV_CLEARSTAGE_BORDER));
		list.Add(string.Format("{0}={1}", "ADV_LOTTERYRANDOMARRAY_SIZE", ADV_LOTTERYRANDOMARRAY_SIZE));
		list.Add(string.Format("{0}={1}", "ADV_FILE", ADV_FILE));
		list.Add(string.Format("{0}={1}", "ADV_BACKUP", ADV_BACKUP));
		list.Add(string.Format("{0}={1}", "ADV_MASTERTABLE_FILE", ADV_MASTERTABLE_FILE));
		list.Add(string.Format("{0}={1}", "ADV_TRANSFER_FILE", ADV_TRANSFER_FILE));
		list.Add(string.Format("{0}={1}", "LOGINBONUS_BACKUP_FILE", LOGINBONUS_BACKUP_FILE));
		list.Add(string.Format("{0}={1}", "GUIDE_TO_SEC", GUIDE_TO_SEC));
		list.Add(string.Format("{0}={1}", "ADV_MEDAL_BUY_NUM", ADV_MEDAL_BUY_NUM));
		list.Add(string.Format("{0}={1}", "OTHER_PLAYER_FILE_CX", OTHER_PLAYER_FILE_CX));
		list.Add(string.Format("{0}={1}", "AUTHKEY_FILE", AUTHKEY_FILE));
		list.Add(string.Format("{0}={1}", "UDINFO_FILE", UDINFO_FILE));
		list.Add(string.Format("{0}={1}", "USERBEHAVIOR_FILE", USERBEHAVIOR_FILE));
		return string.Format("[{0}: {1}]", typeFromHandle.Name, string.Join(", ", list.ToArray()));
	}
}
