public class AdvGetMailProfile_Request : RequestBase
{
	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }

	[MiniJSONAlias("is_retry")]
	public int IsRetry { get; set; }
}
