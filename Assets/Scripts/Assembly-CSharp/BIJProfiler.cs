using System.Collections.Generic;
using UnityEngine;

public class BIJProfiler
{
	public string Name;

	private Dictionary<string, float> mSpanDict;

	private List<string> mSpanList;

	private float mStartTime;

	public BIJProfiler(string a_name)
	{
		Name = a_name;
		mSpanDict = null;
	}

	public void Start()
	{
		mStartTime = Time.realtimeSinceStartup;
	}

	public void RecordSpan(string a_name, bool a_islog = true)
	{
		if (mSpanDict == null)
		{
			mSpanDict = new Dictionary<string, float>();
		}
		if (mSpanList == null)
		{
			mSpanList = new List<string>();
		}
		float value = Time.realtimeSinceStartup - mStartTime;
		mSpanDict.Add(a_name, value);
		mSpanList.Add(a_name);
		if (!a_islog)
		{
		}
	}

	public void AddLog(string a_log, bool a_islog = true)
	{
		if (mSpanDict == null)
		{
			mSpanDict = new Dictionary<string, float>();
		}
		if (mSpanList == null)
		{
			mSpanList = new List<string>();
		}
		mSpanDict.Add(a_log, 0f);
		mSpanList.Add(a_log);
		if (!a_islog)
		{
		}
	}

	public void Result()
	{
		if (mSpanList != null && mSpanDict != null)
		{
			string text = string.Empty;
			for (int i = 0; i < mSpanList.Count; i++)
			{
				string text2 = mSpanList[i];
				float num = mSpanDict[text2];
				string text3 = text;
				text = text3 + "@@@ RESULT Step #" + text2 + "  is  " + num.ToString("0.000") + " sec.\n";
			}
			if (!string.IsNullOrEmpty(text))
			{
			}
		}
	}

	public void Clear()
	{
		if (mSpanList != null && mSpanDict != null)
		{
			mSpanDict.Clear();
			mSpanList.Clear();
		}
	}
}
