using System.Collections;
using System.Collections.Generic;

public class ThreadSafeList<T> : IEnumerable, IEnumerable<T>, ICollection<T>, IList<T>
{
	protected readonly object _lock = new object();

	protected List<T> _interalList;

	public T this[int index]
	{
		get
		{
			lock (_lock)
			{
				return _interalList[index];
			}
		}
		set
		{
			lock (_lock)
			{
				_interalList[index] = value;
			}
		}
	}

	public int Count
	{
		get
		{
			lock (_lock)
			{
				return _interalList.Count;
			}
		}
	}

	public bool IsReadOnly
	{
		get
		{
			lock (_lock)
			{
				return false;
			}
		}
	}

	public ThreadSafeList()
		: this(0)
	{
	}

	public ThreadSafeList(int initialCapacity)
	{
		_interalList = new List<T>(initialCapacity);
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return Clone().GetEnumerator();
	}

	public void AddRange(IEnumerable<T> collection)
	{
		lock (_lock)
		{
			_interalList.AddRange(collection);
		}
	}

	public int IndexOf(T item)
	{
		lock (_lock)
		{
			return _interalList.IndexOf(item);
		}
	}

	public void Insert(int index, T item)
	{
		lock (_lock)
		{
			_interalList.Insert(index, item);
		}
	}

	public void RemoveAt(int index)
	{
		lock (_lock)
		{
			_interalList.RemoveAt(index);
		}
	}

	public void Add(T item)
	{
		lock (_lock)
		{
			_interalList.Add(item);
		}
	}

	public void Clear()
	{
		lock (_lock)
		{
			_interalList.Clear();
		}
	}

	public bool Contains(T item)
	{
		lock (_lock)
		{
			return _interalList.Contains(item);
		}
	}

	public void CopyTo(T[] array, int arrayIndex)
	{
		lock (_lock)
		{
			_interalList.CopyTo(array, arrayIndex);
		}
	}

	public bool Remove(T item)
	{
		lock (_lock)
		{
			return _interalList.Remove(item);
		}
	}

	public IEnumerator<T> GetEnumerator()
	{
		return Clone().GetEnumerator();
	}

	public IList<T> Clone()
	{
		IList<T> list = new List<T>();
		lock (_lock)
		{
			if (_interalList.Count > 0)
			{
				foreach (T interal in _interalList)
				{
					list.Add(interal);
				}
			}
		}
		return list;
	}
}
