using System;
using System.IO;
using System.Text;

public class BIJDataBinaryWriter : BIJBinaryWriter
{
	private FileStream mStream;

	public BIJDataBinaryWriter(string name)
		: this(name, Encoding.UTF8, false)
	{
	}

	public BIJDataBinaryWriter(string name, Encoding encoding, bool append)
	{
		mStream = null;
		Writer = null;
		try
		{
			FileMode mode = ((!append) ? FileMode.Create : FileMode.Append);
			mPath = System.IO.Path.Combine(GetBasePath(), name);
			mStream = new FileStream(mPath, mode);
			Writer = new BinaryWriter(mStream, encoding);
		}
		catch (Exception ex)
		{
			Close();
			throw ex;
		}
	}

	internal static string GetBasePath()
	{
		return BIJUnity.getDataPath();
	}

	public override void Close()
	{
		base.Close();
		try
		{
			if (mStream != null)
			{
				mStream.Close();
			}
		}
		catch (Exception)
		{
		}
		mStream = null;
	}
}
