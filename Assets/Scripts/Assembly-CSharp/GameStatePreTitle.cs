using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatePreTitle : GameState
{
	public enum STATE
	{
		LOAD_WAIT = 0,
		UNLOAD_WAIT = 1,
		MOVE_NEXT = 2,
		LOAD_DLDATA = 3,
		INIT = 4,
		GET_DLLIST = 5,
		WAIT_DLLIST = 6,
		GOT_DLLIST = 7,
		RE_GET_DLLIST = 8,
		RE_WAIT_DLLIST = 9,
		DL_START = 10,
		DL_CHECK_LIST = 11,
		DL = 12,
		DL_WAIT = 13,
		ASSET_UNZIP = 14,
		ASSET_UNZIP_WAIT = 15,
		ERROR = 16,
		ERROR_WAIT = 17,
		ERROR_CONFIRM_WAIT = 18,
		RESUME = 19,
		RESUME_WAIT = 20,
		WAIT = 21,
		NONE = 22
	}

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	private GameStateManager mGameStateManager;

	private ResourceDownloadManager.KIND mCurrentKind;

	public List<ResourceDownloadManager.KIND> mKindDlList;

	private int mTotalFileSize;

	private int mTotalFileNum;

	public int mDLFileNum;

	private STATE mNextState = STATE.WAIT;

	private STATE mPrevState = STATE.WAIT;

	private STATE mSuspendPrevState = STATE.NONE;

	private bool mIsDLFinished;

	private bool mIsUnzipFinished;

	private bool mIsShowErrorDialog = true;

	private ConfirmDialog mSizeConfirmDialog;

	private ConfirmDialog mRetryConfirmDialog;

	private bool mIsRetryDialogPushed;

	private ConfirmDialog mConfirmDialog;

	private bool mIsConfirmDialogPushed;

	private ConfirmDialog mResumeConfirmDialog;

	private bool mIsResumeConfirmDialogPushed;

	private UIPanel mIconPanel;

	private GameObject mLoadicon;

	private bool mPlayVoice;

	private List<IEnumerator> mCoroutineList = new List<IEnumerator>();

	private int mRetryCount;

	private float mPerPer;

	private int mPeriodCount;

	private int mListNum;

	private string mMessageKey = string.Empty;

	private UILabel mMessageLabel;

	private UILabel mGuageValueLabel;

	private UILabel mAnnouncementLabel;

	private UILabel mNumLabel;

	private UISprite mNumGauge;

	private UILabel mPerLabel;

	private UISprite mPerGauge;

	private UISprite mPerWS;

	private UISprite mTopBar;

	private UISprite mButtomBar;

	private UISprite mGauge;

	private Vector3 mPerWSBasePos;

	private static Def.TUTORIAL_INDEX[] mTutorialIndex = new Def.TUTORIAL_INDEX[5]
	{
		Def.TUTORIAL_INDEX.STARTUP_8010,
		Def.TUTORIAL_INDEX.STARTUP_8020,
		Def.TUTORIAL_INDEX.STARTUP_8030,
		Def.TUTORIAL_INDEX.STARTUP_8040,
		Def.TUTORIAL_INDEX.STARTUP_8050
	};

	private bool mProcessDLListCheckDone;

	private float mPosZ;

	public override void Start()
	{
		base.Start();
		mGameStateManager = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mIsDLFinished = false;
		mIsUnzipFinished = false;
		mIsRetryDialogPushed = false;
		mIsConfirmDialogPushed = false;
		mIsResumeConfirmDialogPushed = false;
		mState.SetLogInChange(true, "LOADING");
		mGame.mDLManager.mIsSuspend = false;
	}

	private void Awake()
	{
	}

	public override void Unload()
	{
		base.Unload();
	}

	public void OnDestroy()
	{
	}

	public void OnApplicationPause(bool a_pauseStatus)
	{
		if (a_pauseStatus)
		{
			ResourceDLUtils.dbg1("Pause", true);
			if (mSuspendPrevState == STATE.NONE)
			{
				mSuspendPrevState = mState.GetStatus();
			}
			mGame.mDLManager.mIsSuspend = true;
		}
		else
		{
			ResourceDLUtils.dbg1("Resume", true);
			mState.Reset(STATE.RESUME, true);
		}
	}

	public override void Update()
	{
		bool flag = true;
		switch (mState.GetStatus())
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
				{
					mGame.mTutorialManager.TutorialComplete(Def.TUTORIAL_INDEX.STARTUP_8000);
					mGame.Save();
				}
				mState.Change(STATE.INIT);
			}
			break;
		case STATE.INIT:
			mKindDlList = new List<ResourceDownloadManager.KIND>(mGame.mDLManager.mKindList);
			mTotalFileSize = 0;
			mTotalFileNum = 0;
			mDLFileNum = 0;
			mGame.mDLManager.mDLFileNum = 0;
			mGame.mDLManager.InitMultiThreadDisable();
			mPerPer = 0f;
			mPerGauge.fillAmount = mPerPer / 100f;
			mPlayVoice = true;
			mState.Change(STATE.GET_DLLIST);
			break;
		case STATE.GET_DLLIST:
			mPrevState = STATE.GET_DLLIST;
			mCurrentKind = mKindDlList[0];
			mGuageValueLabel.text = "0%";
			ResourceDLUtils.DeleteDLFile(ResourceDLUtils.DEL_TYPE.RecordDLFilelist, mCurrentKind);
			mGame.mDLManager.ClearResourceDLInfo(mCurrentKind);
			mGame.Server_DLFilelistCheck(mCurrentKind);
			mState.Change(STATE.WAIT_DLLIST);
			break;
		case STATE.WAIT_DLLIST:
			if (GameMain.mDLFilelistCheckDone)
			{
				mState.Change(STATE.GOT_DLLIST);
			}
			break;
		case STATE.GOT_DLLIST:
			if (mState.IsChanged())
			{
				StartCoroutine(ProcessDLListCheck(true));
			}
			else
			{
				if (!mProcessDLListCheckDone)
				{
					break;
				}
				if (mGame.DLErrorInfo != null)
				{
					ResourceDLUtils.err("ERROR in DL!!!!", true);
					mState.Change(STATE.ERROR);
					break;
				}
				if (mKindDlList.Count > 0)
				{
					mKindDlList.RemoveAt(0);
				}
				if (mKindDlList.Count > 0)
				{
					mState.Change(STATE.GET_DLLIST);
				}
				else if (mGame.mDLManager.mKindList.Count > 0)
				{
					float num2 = mTotalFileSize;
					num2 /= 1048576f;
					if (num2 < 0.01f)
					{
						num2 = 0.01f;
					}
					string desc2 = string.Format(Localization.Get("ResourceDL_DLStart_Desc"), num2);
					mSizeConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
					mSizeConfirmDialog.Init(Localization.Get("ResourceDL_DLStart_Title"), desc2, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, DialogBase.DIALOG_SIZE.MEDIUM, string.Empty, string.Empty);
					mSizeConfirmDialog.SetClosedCallback(OnSizeDialogClosed);
					mSizeConfirmDialog.SetSortOrder(510);
					mState.Change(STATE.WAIT);
				}
				else
				{
					mMessageKey = "ResourceDL_LoadMessage";
					mState.Change(STATE.LOAD_DLDATA);
				}
			}
			break;
		case STATE.RE_GET_DLLIST:
			mPrevState = STATE.DL_CHECK_LIST;
			ResourceDLUtils.DeleteDLFile(ResourceDLUtils.DEL_TYPE.RecordDLFilelist, mCurrentKind);
			mGame.mDLManager.ClearResourceDLInfo(mCurrentKind);
			mGame.Server_DLFilelistCheck(mCurrentKind);
			mState.Change(STATE.RE_WAIT_DLLIST);
			break;
		case STATE.RE_WAIT_DLLIST:
			if (GameMain.mDLFilelistCheckDone)
			{
				mState.Change(STATE.DL_CHECK_LIST);
			}
			break;
		case STATE.DL_START:
			mPrevState = STATE.DL_CHECK_LIST;
			mGame.NeverSleepTimeout();
			mCurrentKind = mGame.mDLManager.mKindList[0];
			mGame.mDLManager.ClearResourceDLInfo(mCurrentKind);
			mState.Reset(STATE.DL_CHECK_LIST, true);
			goto case STATE.DL_CHECK_LIST;
		case STATE.DL_CHECK_LIST:
			if (mState.IsChanged())
			{
				mPrevState = STATE.DL_CHECK_LIST;
				StartCoroutine(ProcessDLListCheck());
			}
			else if (mProcessDLListCheckDone)
			{
				ResourceDLUtils.dbg1("GO TO " + mNextState);
				mState.Change(mNextState);
				mNextState = STATE.WAIT;
			}
			break;
		case STATE.DL:
		{
			NetworkReachability status2 = InternetReachability.Status;
			if (status2 != 0 && mGame.mOptions.NetworkState != (int)status2)
			{
				mGame.mOptions.NetworkState = (int)status2;
				mGame.mOptions.DLThreadNum = mGame.GameProfile.DlParallelNum;
				mGame.SaveOptions();
			}
			if (mGame.mOptions.DLThreadNum <= 0)
			{
				mGame.mOptions.DLThreadNum = mGame.GameProfile.DlParallelNum;
				mGame.SaveOptions();
			}
			else if (mGame.mOptions.DLThreadNum > mGame.GameProfile.DlParallelNum)
			{
				mGame.mOptions.DLThreadNum = mGame.GameProfile.DlParallelNum;
				mGame.SaveOptions();
			}
			ProcessDL();
			mState.Change(STATE.DL_WAIT);
			break;
		}
		case STATE.DL_WAIT:
		{
			if (mPlayVoice)
			{
				mPlayVoice = false;
				mGame.PlaySe("VOICE_028", -1);
			}
			bool flag2 = false;
			float num = 0f;
			if (mTotalFileNum > 0)
			{
				num = (float)mGame.mDLManager.mDLFileNum / (float)mTotalFileNum;
			}
			if (mPerPer <= num)
			{
				mPerPer += (num - mPerPer) / 8f;
				float f = mPerPer - num;
				if (Mathf.Abs(f) < 0.5f)
				{
					mPerPer = num;
					flag2 = true;
				}
				mPerGauge.fillAmount = mPerPer;
			}
			mGuageValueLabel.text = string.Empty + (int)(mPerPer * 100f) + "%";
			if (mGame.DLErrorInfo != null)
			{
				if (mGame.mDLManager.mRunningThreadNum > 1)
				{
					NetworkReachability status = InternetReachability.Status;
					if (status != 0)
					{
						mGame.mOptions.NetworkState = (int)status;
						mGame.mOptions.DLThreadNum = mGame.mDLManager.mRunningThreadNum - 1;
						mGame.SaveOptions();
						mIsShowErrorDialog = false;
					}
				}
				ResourceDLUtils.err("ERROR in DL!!!!", true);
				mState.Change(STATE.ERROR);
				mPrevState = STATE.DL_WAIT;
			}
			else
			{
				mIsDLFinished = mGame.mDLManager.IsDownloadFinished;
				if (mIsDLFinished && flag2)
				{
					mState.Change(STATE.ASSET_UNZIP);
				}
				else if (mGame.mDLManager.IsDLCountShort)
				{
					mState.Change(STATE.DL_CHECK_LIST);
				}
			}
			break;
		}
		case STATE.ASSET_UNZIP:
			ProcessUnzip();
			mState.Change(STATE.ASSET_UNZIP_WAIT);
			break;
		case STATE.ASSET_UNZIP_WAIT:
			if (mGame.DLErrorInfo != null)
			{
				ResourceDLUtils.err("ERROR in unzip!!!!", true);
				mState.Change(STATE.ERROR);
				mPrevState = STATE.ASSET_UNZIP_WAIT;
				break;
			}
			mIsUnzipFinished = mGame.mDLManager.IsUnzipFinished;
			if (mIsUnzipFinished)
			{
				SendTutorialComplete(mCurrentKind);
				if (mGame.mDLManager.mKindList.Count > 0)
				{
					mGame.mDLManager.mKindList.RemoveAt(0);
				}
				if (mGame.mDLManager.mKindList.Count > 0)
				{
					mMessageKey = "ResourceDL_DLMessage";
					mState.Change(STATE.DL_START);
					break;
				}
				mPerPer = 1f;
				mPerGauge.fillAmount = mPerPer;
				mGuageValueLabel.text = string.Empty + (int)(mPerPer * 100f) + "%";
				mGame.mOptions.DLThreadNum = mGame.GameProfile.DlParallelNum;
				mGame.SaveOptions();
				mMessageKey = "ResourceDL_LoadMessage";
				mState.Change(STATE.LOAD_DLDATA);
			}
			break;
		case STATE.LOAD_DLDATA:
			mPrevState = STATE.WAIT;
			mGame.mDLManager.EndDLThread();
			mGame.RestoreSleepTimeout();
			mGame.mDLManager.ClearBuffer();
			ResourceManager.LoadAssetBundleList();
			mGame.mDLManager.LoadUpdateFiles();
			mGameStateManager.SetNextGameState(GameStateManager.GAME_STATE.TITLE);
			mState.Change(STATE.UNLOAD_WAIT);
			break;
		case STATE.MOVE_NEXT:
			flag = false;
			mState.Change(STATE.WAIT);
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.MOVE_NEXT);
			}
			break;
		case STATE.ERROR:
		{
			ResourceDLUtils.dbg1("ERROR SEQ");
			if (mGame.mDLManager.isThreadProcessing())
			{
				break;
			}
			flag = false;
			mGame.FinishLoading();
			mGame.FinishConnecting();
			mIsRetryDialogPushed = false;
			mIsConfirmDialogPushed = false;
			mIsResumeConfirmDialogPushed = false;
			while (mCoroutineList.Count > 0)
			{
				IEnumerator routine = mCoroutineList[0];
				StopCoroutine(routine);
				mCoroutineList.RemoveAt(0);
			}
			ResourceDLErrorInfo resourceDLErrorInfo = null;
			string empty = string.Empty;
			if (mGame.DLErrorInfo != null)
			{
				ResourceDLUtils.dbg1("GET ErrorInfo from GameMain");
				resourceDLErrorInfo = mGame.DLErrorInfo;
			}
			if (resourceDLErrorInfo == null)
			{
				ResourceDLUtils.dbg1("NO ErrorInfo");
				resourceDLErrorInfo = new ResourceDLErrorInfo(null, ResourceDLErrorInfo.ERROR_CODE.OTHER, ResourceDLErrorInfo.PLACE.GAMESTATEPRETITLE, string.Empty, string.Empty);
			}
			empty = resourceDLErrorInfo.GetMessage();
			if (resourceDLErrorInfo.GetAfterAction() != 0 || mRetryCount >= 5)
			{
				break;
			}
			if (mRetryConfirmDialog == null)
			{
				if (!mIsShowErrorDialog)
				{
					OnRetryDialogClosed(ConfirmDialog.SELECT_ITEM.POSITIVE);
					mIsShowErrorDialog = true;
				}
				else
				{
					ResourceDLUtils.dbg1("RETRY Dialog !!!");
					mPlayVoice = true;
					empty += string.Format(Localization.Get("ResourceDL_Retry"));
					mRetryConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
					ConfirmDialog.CONFIRM_DIALOG_STYLE style = ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY;
					mRetryConfirmDialog.Init(resourceDLErrorInfo.ErrorTitle, empty, style, DialogBase.DIALOG_SIZE.LARGE, string.Empty, string.Empty);
					mRetryConfirmDialog.SetClosedCallback(OnRetryDialogClosed);
					mRetryConfirmDialog.SetSortOrder(510);
				}
				mGame.mDLManager.EndDLThread();
			}
			mState.Change(STATE.ERROR_WAIT);
			break;
		}
		case STATE.ERROR_WAIT:
			flag = false;
			if (mIsRetryDialogPushed)
			{
				ResourceDLUtils.dbg1("PUSH ERROR Dialog to " + mNextState, true);
				mState.Change(mNextState);
				mNextState = STATE.WAIT;
				mGame.mDLManager.ClearRequests();
				mGame.ResetDLErrorInfo();
			}
			break;
		case STATE.ERROR_CONFIRM_WAIT:
			flag = false;
			if (mIsConfirmDialogPushed)
			{
				ResourceDLUtils.dbg1("PUSH ERROR CONFIRM Dialog to " + mNextState, true);
				mState.Change(mNextState);
				mNextState = STATE.WAIT;
			}
			break;
		case STATE.RESUME:
			ResourceDLUtils.dbg1("RESUME SEQ");
			flag = false;
			ProcessResume();
			if (mRetryConfirmDialog != null)
			{
				mRetryConfirmDialog.enabled = false;
			}
			if (mResumeConfirmDialog == null)
			{
				ResourceDLUtils.dbg1("RESUME Dialog !!!");
				string desc = string.Format(Localization.Get("ResourceDL_Resume"));
				mResumeConfirmDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
				mResumeConfirmDialog.Init(Localization.Get("ResourceDL_Resume_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY, DialogBase.DIALOG_SIZE.MEDIUM, string.Empty, string.Empty);
				mResumeConfirmDialog.SetClosedCallback(OnResumeConfirmDialogClosed);
				mResumeConfirmDialog.SetSortOrder(514);
				mIsResumeConfirmDialogPushed = false;
			}
			mState.Change(STATE.RESUME_WAIT);
			break;
		case STATE.RESUME_WAIT:
			flag = false;
			if (mIsResumeConfirmDialogPushed)
			{
				ResourceDLUtils.dbg1("PUSH DEFAULT Dialog to " + mNextState, true);
				mState.Reset(mNextState, true);
				if (mRetryConfirmDialog != null)
				{
					mRetryConfirmDialog.enabled = true;
				}
				mGame.mDLManager.mIsSuspend = false;
				mPlayVoice = true;
			}
			break;
		case STATE.WAIT:
			flag = false;
			break;
		}
		mState.Update();
		if (flag)
		{
			UpdateDLMessage();
		}
	}

	private void UpdateDLMessage()
	{
		if (mMessageLabel != null && !string.IsNullOrEmpty(mMessageKey))
		{
			mPeriodCount = (mPeriodCount + 1) % 4;
			string text = Localization.Get(mMessageKey);
			for (int i = 0; i < mPeriodCount; i++)
			{
				text += ".";
			}
			mMessageLabel.text = text;
		}
	}

	public IEnumerator ProcessDLListCheck(bool getFileNum = false)
	{
		ResourceDLUtils.dbg1("ProcessDLListCheck", true);
		mProcessDLListCheckDone = false;
		mGame.CreateDLManager();
		if (ResourceDLUtils.IsDebugDLFileListToNull())
		{
			mGame.mDLManager.ClearResourceDLInfo(mCurrentKind);
		}
		if (!mGame.mDLManager.mServerListDict.ContainsKey(mCurrentKind))
		{
			ResourceDLUtils.dbg1("DLFilelist empty or parse miss!!!!", true);
			string tempPath = ResourceDownloadManager.getResourceDLPath(mCurrentKind);
			tempPath += Constants.RESOURCEDL_SERVERFILE;
			mGame.mDLManager.LoadLocalDLFilelist(mCurrentKind, tempPath);
			if (ResourceDLUtils.IsDebugLocalDLFileListToNull())
			{
				mGame.mDLManager.ClearResourceDLInfo(mCurrentKind);
			}
			if (!mGame.mDLManager.mServerListDict.ContainsKey(mCurrentKind))
			{
				ResourceDLUtils.dbg1("Local's DLFilelist empty!!!!", true);
				mGame.CreateDLError(ResourceDLErrorInfo.ERROR_CODE.NO_DLLIST, ResourceDLErrorInfo.PLACE.GAMESTATEPRETITLE, string.Empty);
				mNextState = STATE.ERROR;
				mProcessDLListCheckDone = true;
				yield break;
			}
			ResourceDLUtils.dbg1("Local's DLFilelist LOAD", true);
		}
		mGame.mDLManager.Init(mCurrentKind);
		mGame.mDLManager.LoadLocalDataList(mCurrentKind);
		yield return StartCoroutine(mGame.mDLManager.RegisterFileDataToDownload(mCurrentKind));
		if (!getFileNum)
		{
			if (mGame.mDLManager.isNeedDownload())
			{
				mNextState = STATE.DL;
			}
			else
			{
				mNextState = STATE.ASSET_UNZIP;
			}
		}
		else
		{
			int needDLSize = 0;
			int needDLNum = mGame.mDLManager.getNeedDownloadNum();
			if (needDLNum == 0)
			{
				mGame.mDLManager.mKindList.Remove(mCurrentKind);
				SendTutorialComplete(mCurrentKind);
			}
			else
			{
				needDLSize = mGame.mDLManager.getNeedDownloadSize();
			}
			mTotalFileSize += needDLSize;
			mTotalFileNum += needDLNum;
		}
		mProcessDLListCheckDone = true;
	}

	public void ProcessDL()
	{
		ResourceDLUtils.dbg1("Run DL");
		mGame.mDLManager.DoDownload(mCurrentKind);
	}

	public void ProcessUnzip()
	{
		IEnumerator enumerator = mGame.mDLManager.UnzipAssets();
		mCoroutineList.Add(enumerator);
		StartCoroutine(enumerator);
	}

	private void ProcessResume()
	{
		ResourceDLUtils.dbg1("resume process", true);
		switch (mSuspendPrevState)
		{
		case STATE.DL_CHECK_LIST:
		case STATE.DL:
		case STATE.DL_WAIT:
			ResourceDLUtils.dbg1("download again : " + mSuspendPrevState, true);
			break;
		default:
			ResourceDLUtils.dbg1("other : " + mSuspendPrevState, true);
			break;
		}
	}

	private void SendTutorialComplete(ResourceDownloadManager.KIND aKind)
	{
		if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL1))
		{
			Def.TUTORIAL_INDEX index = mTutorialIndex[(int)aKind];
			mGame.mTutorialManager.TutorialComplete(index);
			mGame.Save();
		}
	}

	private void OnSizeDialogClosed(ConfirmDialog.SELECT_ITEM a_item)
	{
		mSizeConfirmDialog = null;
		mMessageKey = "ResourceDL_DLMessage";
		mState.Change(STATE.DL_START);
	}

	private void OnRetryDialogClosed(ConfirmDialog.SELECT_ITEM a_item)
	{
		ResourceDLUtils.dbg1(string.Format("PUSH Retry Dialog({0}) from {1}", a_item, mPrevState), true);
		mRetryConfirmDialog = null;
		if (mPrevState == STATE.GET_DLLIST)
		{
			mNextState = STATE.GET_DLLIST;
		}
		else if (mPrevState == STATE.DL_CHECK_LIST)
		{
			mNextState = STATE.RE_GET_DLLIST;
		}
		else if (mPrevState == STATE.DL_WAIT)
		{
			mNextState = STATE.DL_CHECK_LIST;
		}
		else if (mPrevState == STATE.ASSET_UNZIP_WAIT)
		{
			mNextState = STATE.ASSET_UNZIP;
		}
		ResourceDLUtils.dbg1(string.Format("Retry {0} next to {1} ", mRetryCount, mNextState), true);
		mPrevState = STATE.WAIT;
		mIsRetryDialogPushed = true;
	}

	private void OnResumeConfirmDialogClosed(ConfirmDialog.SELECT_ITEM a_item)
	{
		ResourceDLUtils.dbg1(string.Format("PUSH Resume Confirm Dialog({0})", a_item), true);
		mNextState = mSuspendPrevState;
		STATE sTATE = mSuspendPrevState;
		if (sTATE == STATE.ASSET_UNZIP || sTATE == STATE.ASSET_UNZIP_WAIT)
		{
			mNextState = STATE.ASSET_UNZIP;
		}
		ResourceDLUtils.dbg1(string.Format("RESUME next:" + mNextState), true);
		mSuspendPrevState = STATE.NONE;
		mIsResumeConfirmDialogPushed = true;
	}

	public override IEnumerator LoadGameState()
	{
		int depth = 30;
		float _start = Time.realtimeSinceStartup;
		yield return Resources.UnloadUnusedAssets();
		BIJImage atlas = ResourceManager.LoadImage("DL_BACKGROUND").Image;
		yield return 0;
		UIFont font = GameMain.LoadFont();
		yield return 0;
		Camera camera = GameObject.Find("Camera").GetComponent<Camera>();
		yield return 0;
		yield return StartCoroutine(mGameStateManager.CoTipsScreenOn(0, true));
		GameObject obj = GameObject.Find("TipsAnchorBottom");
		mGauge = Util.CreateSprite("GaugeBase", obj, atlas);
		Util.SetSpriteInfo(mGauge, "dl_frame", depth + 3, new Vector3(0f, 56f, mPosZ), Vector3.one, false);
		mPerGauge = Util.CreateSprite("Fill", mGauge.gameObject, atlas);
		Util.SetSpriteInfo(mPerGauge, "dl_bar", depth + 4, new Vector3(0f, 0f, mPosZ), Vector3.one, false);
		mPerGauge.type = UIBasicSprite.Type.Filled;
		mPerGauge.fillDirection = UIBasicSprite.FillDirection.Horizontal;
		mPerGauge.fillAmount = 0f;
		UIPanel panel = Util.CreateGameObject("GaugeMessagePanel", mGauge.gameObject).AddComponent<UIPanel>();
		panel.renderQueue = UIPanel.RenderQueue.Explicit;
		panel.depth = depth + 5;
		panel.startingRenderQueue = 4010;
		panel.sortingOrder = 510;
		mMessageLabel = Util.CreateLabel("MessageLabel", panel.gameObject, font);
		Util.SetLabelInfo(mMessageLabel, string.Empty, depth + 7, new Vector3(-135f, 40f, mPosZ), 26, 0, 0, UIWidget.Pivot.Left);
		mMessageKey = "ResourceDL_CheckMessage";
		mMessageLabel.text = Localization.Get(mMessageKey);
		mMessageLabel.color = Color.black;
		mPeriodCount = 0;
		mGuageValueLabel = Util.CreateLabel("GuageValueLabel", panel.gameObject, font);
		Util.SetLabelInfo(mGuageValueLabel, string.Empty, depth + 6, new Vector3(10f, -2f, mPosZ), 26, 0, 0, UIWidget.Pivot.Center);
		mGuageValueLabel.text = string.Empty;
		mGuageValueLabel.color = Color.black;
		mAnnouncementLabel = Util.CreateLabel("AnnouncementLabel", panel.gameObject, font);
		Util.SetLabelInfo(mAnnouncementLabel, string.Empty, depth + 8, new Vector3(0f, -45f, mPosZ), 22, 0, 0, UIWidget.Pivot.Center);
		mAnnouncementLabel.text = Localization.Get("ResourceDL_Announcement");
		mAnnouncementLabel.color = Color.black;
		mListNum = mGame.mDLManager.mKindList.Count;
		if (mListNum == 0)
		{
			mGame.mDLManager.RegisterKindListAll();
			mListNum = mGame.mDLManager.mKindList.Count;
		}
		yield return 0;
		yield return 0;
		SetLayout(Util.ScreenOrientation);
		LoadGameStateFinished();
		yield return 0;
	}

	public override IEnumerator UnloadGameState()
	{
		mGameStateManager.TipsScreenOff();
		if (mIconPanel != null)
		{
			Object.Destroy(mIconPanel.gameObject);
			mIconPanel = null;
		}
		if (mLoadicon != null)
		{
			Object.Destroy(mLoadicon.gameObject);
			mLoadicon = null;
		}
		if (mGauge != null)
		{
			Object.Destroy(mGauge.gameObject);
			mGauge = null;
		}
		if (mPerGauge != null)
		{
			Object.Destroy(mPerGauge.gameObject);
			mPerGauge = null;
		}
		if (mMessageLabel != null)
		{
			Object.Destroy(mMessageLabel.gameObject);
			mMessageLabel = null;
		}
		if (mGuageValueLabel != null)
		{
			Object.Destroy(mGuageValueLabel.gameObject);
			mGuageValueLabel = null;
		}
		if (mAnnouncementLabel != null)
		{
			Object.Destroy(mAnnouncementLabel.gameObject);
			mAnnouncementLabel = null;
		}
		yield return 0;
		ResourceManager.UnloadImage("DL_BACKGROUND");
		yield return 0;
		ResourceManager.UnloadImage("HUD");
		yield return 0;
		UnloadGameStateFinished();
		yield return 0;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		if (o == ScreenOrientation.Portrait || o == ScreenOrientation.PortraitUpsideDown)
		{
			mGauge.transform.localPosition = new Vector3(0f, 140f, mPosZ);
		}
		else
		{
			mGauge.transform.localPosition = new Vector3(0f, 100f, mPosZ);
		}
	}
}
