using System;
using UnityEngine;

public class IconSettingDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public enum STATE
	{
		INIT = 0,
		MAIN = 1,
		WAIT = 2,
		NETWORK_LOGIN00 = 3,
		NETWORK_LOGIN01 = 4,
		NETWORK_LOGOUT = 5
	}

	public delegate void OnDialogClosed(bool mHereLogin);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	public bool mHereLogin;

	private ConfirmDialog mErrorDialog;

	public bool Modified { get; private set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.NETWORK_LOGIN00:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns2 = mSNS;
			_sns2.Login(delegate(BIJSNS.Response res, object userdata)
			{
				if (res != null && res.result == 0)
				{
					mState.Change(STATE.NETWORK_LOGIN01);
				}
				else
				{
					if (GameMain.UpdateOptions(_sns2, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns2);
				}
			}, null);
			break;
		}
		case STATE.NETWORK_LOGIN01:
		{
			mState.Change(STATE.WAIT);
			BIJSNS _sns = mSNS;
			_sns.GetUserInfo(null, delegate(BIJSNS.Response res, object userdata)
			{
				try
				{
					if (res != null && res.result == 0)
					{
						IBIJSNSUser iBIJSNSUser = res.detail as IBIJSNSUser;
						if (GameMain.UpdateOptions(_sns, iBIJSNSUser.ID, iBIJSNSUser.Name))
						{
							Modified = false;
							mGame.SaveOptions();
						}
						mGame.mPlayer.Data.LastGetSocialTime = DateTimeUtil.UnitLocalTimeEpoch;
						mGame.mPlayer.Data.LastGetFriendTime = DateTimeUtil.UnitLocalTimeEpoch;
						mHereLogin = true;
						mGame.mPlayer.Data.PlayerIcon = -1;
					}
				}
				catch (Exception)
				{
				}
				if (!GameMain.IsSNSLogined(_sns))
				{
					if (GameMain.UpdateOptions(_sns, string.Empty, string.Empty))
					{
						Modified = false;
						mGame.SaveOptions();
					}
					SNSError(_sns);
				}
				else
				{
					mState.Change(STATE.MAIN);
				}
			}, null);
			break;
		}
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("SetPickIconTitle");
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(titleDescKey);
		titleDescKey = string.Format(Localization.Get("SetPickIcon"));
		UILabel uILabel = Util.CreateLabel("Top_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(0f, 74f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.spacingY = 10;
		UIButton button = Util.CreateJellyImageButton("FaceBookButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "icon_facebook", "icon_facebook", "icon_facebook", mBaseDepth + 3, new Vector3(-110f, -22f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnFaceBookIconPushed", UIButtonMessage.Trigger.OnClick);
		titleDescKey = string.Format(Localization.Get("SelectedIconFB"));
		uILabel = Util.CreateLabel("Top_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(-110f, -122f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		button = Util.CreateJellyImageButton("NameChangeButton", base.gameObject, image);
		Util.SetImageButtonInfo(button, "icon_chara_unknown", "icon_chara_unknown", "icon_chara_unknown", mBaseDepth + 3, new Vector3(110f, -22f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnCharaIconPushed", UIButtonMessage.Trigger.OnClick);
		titleDescKey = string.Format(Localization.Get("SelectedIconInGame"));
		uILabel = Util.CreateLabel("Top_Desk", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 4, new Vector3(110f, -122f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		CreateCancelButton("OnCancelPushed");
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnCharaIconPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.mPlayer.Data.PlayerIcon = mGame.mPlayer.Data.LastUsedIcon;
			Close();
		}
	}

	private void OnFaceBookIconPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mSNS = SocialManager.Instance[SNS.Facebook];
			if (IsFacebookLogined())
			{
				mGame.mPlayer.Data.PlayerIcon = -1;
				Close();
			}
			else
			{
				mState.Reset(STATE.NETWORK_LOGIN00, true);
			}
		}
	}

	private bool SNSError(BIJSNS _sns)
	{
		mState.Change(STATE.WAIT);
		GameObject parent = base.gameObject;
		mConfirmDialog = Util.CreateGameObject("ConfirmDialog", parent).AddComponent<ConfirmDialog>();
		string desc = string.Format(Localization.Get("SNS_Login_Error_Desc"), Localization.Get("SNS_" + _sns.Kind));
		mConfirmDialog.Init(Localization.Get("SNS_Login_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.CLOSE_ONLY);
		mConfirmDialog.SetClosedCallback(OnSNSErrorDialogClosed);
		mConfirmDialog.SetBaseDepth(mBaseDepth + 70);
		return true;
	}

	private void OnSNSErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mHereLogin);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
