using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGimmick : MonoBehaviour
{
	public enum STATE
	{
		LOAD = 0,
		INIT = 1,
		STAY = 2,
		ATTACK = 3,
		DAMAGE = 4,
		DEAD = 5,
		DEAD_FINISHED = 6
	}

	private struct BOSS_SETTING
	{
		public string attack;

		public string die;

		public string damage;

		public string idle;

		public BOSS_SETTING(string _attack, string _die, string _damage, string _idle)
		{
			attack = _attack;
			die = _die;
			damage = _damage;
			idle = _idle;
		}
	}

	public delegate void ShuffleTargetsDelegate(Def.TILE_KIND[] newTargets, bool immediate);

	public delegate void PlaceBrokenWallDelegate(int num1, int num2, int num3);

	public delegate void PlaceFamiliarDelegate(int maxNum, int num);

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD);

	private GameMain mGame;

	private SsSprite mSprite;

	private PartsChangedSprite mCountDownSprite;

	private FixedSprite mLifeGaugeFrame;

	private FixedSprite mLifeGauge;

	private int mIndex;

	private int mLifeMax;

	private int mAttackTargetNum;

	private List<Def.TILE_KIND> mAttackTargetList = new List<Def.TILE_KIND>();

	private Def.BOSS_ATTACK mAttackPattern;

	public int mAttackParam1;

	public int mAttackParam2;

	public int mAttackParam3;

	private int mFirstAttackInterval;

	private int mAttackInterval;

	private Vector2 mLifeGaugeBasePos;

	private int mLife;

	private int mAttackCount;

	private List<Def.TILE_KIND> mTargets = new List<Def.TILE_KIND>();

	private float mShakeTimer;

	private int mTileWidth = 2;

	private int mTileHeight = 2;

	public ShuffleTargetsDelegate OnShuffleTargets = delegate
	{
	};

	public PlaceBrokenWallDelegate OnPlaceBrokenWall = delegate
	{
	};

	public PlaceFamiliarDelegate OnPlaceFamiliar = delegate
	{
	};

	private static BOSS_SETTING[] AnimationSetting = new BOSS_SETTING[4]
	{
		new BOSS_SETTING("GIMMICK_BOSS00_ATTACK", "GIMMICK_BOSS00_DEAD", "GIMMICK_BOSS00_DAMAGE", "GIMMICK_BOSS00_IDLE"),
		new BOSS_SETTING("GIMMICK_BOSS01_ATTACK", "GIMMICK_BOSS01_DEAD", "GIMMICK_BOSS01_DAMAGE", "GIMMICK_BOSS01_IDLE"),
		new BOSS_SETTING("GIMMICK_BOSS02_ATTACK", "GIMMICK_BOSS02_DEAD", "GIMMICK_BOSS02_DAMAGE", "GIMMICK_BOSS02_IDLE"),
		new BOSS_SETTING("GIMMICK_BOSS03_ATTACK", "GIMMICK_BOSS03_DEAD", "GIMMICK_BOSS03_DAMAGE", "GIMMICK_BOSS03_IDLE")
	};

	private static string[] NumSpriteName = new string[10] { "combo0", "combo1", "combo2", "combo3", "combo4", "combo5", "combo6", "combo7", "combo8", "combo9" };

	private void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			AttackCountDown(0);
			mState.Change(STATE.STAY);
			break;
		case STATE.ATTACK:
			if (!mSprite.IsPlaying())
			{
				if (mAttackPattern == Def.BOSS_ATTACK.CHANGE_COLORS)
				{
					ShuffleAttackTarget(false, true);
				}
				if (mAttackPattern == Def.BOSS_ATTACK.SPAWN_PLATE)
				{
					PlaceBrokenWall();
				}
				if (mAttackPattern == Def.BOSS_ATTACK.SPAWN_FAMILIAR)
				{
					PlaceFamiliar();
				}
				mAttackCount = mAttackInterval;
				ChangeCountDown();
				Stay();
			}
			break;
		case STATE.DAMAGE:
			if (!mSprite.IsPlaying())
			{
				Stay();
			}
			break;
		case STATE.DEAD:
			if (!mSprite.IsPlaying())
			{
				mState.Change(STATE.DEAD_FINISHED);
			}
			break;
		}
		UpdateLifeGauge();
		mState.Update();
	}

	public void Init(ref ISMStageData stageData)
	{
		mSprite = Util.CreateGameObject("Body", base.gameObject).AddComponent<SsSprite>();
		mIndex = stageData.BossID;
		mLifeMax = stageData.BossHP;
		mAttackTargetNum = stageData.AttackCandyNum;
		mAttackTargetList.Clear();
		if (stageData.AttackUseCandy0)
		{
			mAttackTargetList.Add(Def.TILE_KIND.COLOR0);
		}
		if (stageData.AttackUseCandy1)
		{
			mAttackTargetList.Add(Def.TILE_KIND.COLOR1);
		}
		if (stageData.AttackUseCandy2)
		{
			mAttackTargetList.Add(Def.TILE_KIND.COLOR2);
		}
		if (stageData.AttackUseCandy3)
		{
			mAttackTargetList.Add(Def.TILE_KIND.COLOR3);
		}
		if (stageData.AttackUseCandy4)
		{
			mAttackTargetList.Add(Def.TILE_KIND.COLOR4);
		}
		if (stageData.AttackUseCandy5)
		{
			mAttackTargetList.Add(Def.TILE_KIND.COLOR5);
		}
		mAttackPattern = stageData.BossAttackPattern;
		mAttackParam1 = stageData.BossAttackParam;
		mAttackParam2 = stageData.BossAttackParam2;
		mAttackParam3 = stageData.BossAttackParam3;
		mFirstAttackInterval = stageData.BossFirstAttack;
		mAttackInterval = stageData.BossAttackInterval;
		mAttackCount = mFirstAttackInterval;
		mLife = mLifeMax;
		float x = ((float)mTileWidth / 2f - 0.5f) * Def.TILE_PITCH_H;
		float y = ((float)mTileHeight / 2f - 0.5f) * Def.TILE_PITCH_V;
		mSprite.transform.localPosition = new Vector3(x, y, 0f);
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_TILE");
		mCountDownSprite = Util.CreateGameObject("Count", base.gameObject).AddComponent<PartsChangedSprite>();
		float x2 = Def.TILE_PITCH_H * (float)mTileWidth - Def.TILE_PITCH_H * 0.8f;
		float y2 = Def.TILE_PITCH_V * (float)mTileHeight - Def.TILE_PITCH_V * 0.8f;
		mCountDownSprite.transform.localPosition = new Vector3(x2, y2, -1.1f);
		mCountDownSprite.Atlas = resImage.Image;
		ChangeCountDown();
		x2 = Def.TILE_PITCH_H * (float)mTileWidth / 2f - Def.TILE_PITCH_H / 2f;
		y2 = (0f - Def.TILE_PITCH_V) / 2f + 8f;
		mLifeGaugeBasePos = new Vector2(x2, y2);
		mLifeGaugeFrame = Util.CreateGameObject("LifeFrame", base.gameObject).AddComponent<FixedSprite>();
		mLifeGaugeFrame.Init("PUZZLETILE_FIXED_SPRITE", "gimmick_boss05", new Vector3(x2, y2, -1f), resImage.Image);
		mLifeGauge = Util.CreateGameObject("Life", base.gameObject).AddComponent<FixedSprite>();
		mLifeGauge.Init("PUZZLETILE_FIXED_SPRITE", "gimmick_boss04", new Vector3(x2, y2, -1.1f), resImage.Image);
		StartCoroutine(LoadAnimation());
	}

	private IEnumerator LoadAnimation()
	{
		List<ResourceInstance> resList = new List<ResourceInstance>
		{
			ResourceManager.LoadSsAnimationAsync(AnimationSetting[mIndex].idle),
			ResourceManager.LoadSsAnimationAsync(AnimationSetting[mIndex].attack),
			ResourceManager.LoadSsAnimationAsync(AnimationSetting[mIndex].damage),
			ResourceManager.LoadSsAnimationAsync(AnimationSetting[mIndex].die)
		};
		while (true)
		{
			bool wait = false;
			foreach (ResourceInstance res in resList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
		Stay();
		mState.Change(STATE.INIT);
	}

	private void UpdateLifeGauge()
	{
		Vector2 vector = Vector2.zero;
		Color white = Color.white;
		if (mShakeTimer > 0f)
		{
			mShakeTimer -= Time.deltaTime;
			vector = new Vector2(Random.Range(-5f, 5f), Random.Range(-5f, 5f));
		}
		float num = 128f;
		float num2 = (float)mLife / (float)mLifeMax;
		float num3 = num * num2;
		mLifeGauge.transform.localScale = new Vector3(num2, 1f, 1f);
		if (num2 <= 0.2f)
		{
			mLifeGauge.ChangeSprite("gimmick_boss07");
		}
		else if (num2 <= 0.5f)
		{
			mLifeGauge.ChangeSprite("gimmick_boss06");
		}
		else
		{
			mLifeGauge.ChangeSprite("gimmick_boss04");
		}
		float num4 = (0f - (num - num3)) / 2f;
		mLifeGauge.transform.localPosition = new Vector3(mLifeGaugeBasePos.x + vector.x + num4, mLifeGaugeBasePos.y + vector.y, -1.1f);
		mLifeGaugeFrame.transform.localPosition = new Vector3(mLifeGaugeBasePos.x + vector.x, mLifeGaugeBasePos.y + vector.y, -1f);
	}

	private void ChangeCountDown()
	{
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[3];
		array[0].partName = "change_000";
		array[0].spriteName = "null";
		array[1].partName = "change_001";
		array[1].spriteName = "null";
		array[2].partName = "change_002";
		array[2].spriteName = "null";
		if (mAttackCount < 10)
		{
			array[0].spriteName = NumSpriteName[mAttackCount];
		}
		else
		{
			array[1].spriteName = NumSpriteName[mAttackCount % 10];
			array[2].spriteName = NumSpriteName[mAttackCount / 10];
		}
		mCountDownSprite.ChangeAnime("GIMMICK_BOSS_NUM_IDLE", array, true, 0, null);
	}

	private void Stay()
	{
		ChangeAnime(AnimationSetting[mIndex].idle, 0);
		mState.Change(STATE.STAY);
	}

	private void Attack()
	{
		mGame.PlaySe("SE_BOSS_ATTACK", 3);
		ChangeAnime(AnimationSetting[mIndex].attack, 1);
		mState.Change(STATE.ATTACK);
	}

	private void Damage()
	{
		mShakeTimer = 0.5f;
		mGame.PlaySe("SE_BOSS_DAMAGE", -1);
		ChangeAnime(AnimationSetting[mIndex].damage, 1);
		mState.Change(STATE.DAMAGE);
	}

	private void Dead()
	{
		mLifeGauge.enabled = false;
		mLifeGaugeFrame.enabled = false;
		mCountDownSprite.enabled = false;
		mGame.PlaySe("SE_BOSS_DEAD", -1);
		ChangeAnime(AnimationSetting[mIndex].die, 1);
		float x = ((float)mTileWidth / 2f - 0.5f) * Def.TILE_PITCH_H;
		float y = ((float)mTileHeight / 2f - 0.5f) * Def.TILE_PITCH_V;
		Util.CreateOneShotParticle("Dead", "Particles/PuzzleGimmick/Boss_Dead", base.gameObject, new Vector3(x, y, -30f), new Vector3(1f, 1f, 0.1f), 5f, 0f);
		mState.Reset(STATE.DEAD, true);
	}

	private void ChangeAnime(string key, int playCount)
	{
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(key);
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		mSprite.Animation = ssAnime;
		mSprite.ResetAnimationStatus();
		mSprite.Play();
		mSprite.PlayCount = playCount;
	}

	private void ShuffleAttackTarget(bool immediate, bool subTargetNum)
	{
		mTargets = new List<Def.TILE_KIND>(mAttackTargetList);
		if (subTargetNum)
		{
			mAttackTargetNum--;
			if (mAttackTargetNum < 1)
			{
				mAttackTargetNum = 1;
			}
		}
		for (int num = mAttackTargetList.Count - mAttackTargetNum; num > 0; num--)
		{
			int index = Random.Range(0, mTargets.Count);
			mTargets.RemoveAt(index);
		}
		OnShuffleTargets(mTargets.ToArray(), immediate);
	}

	private void PlaceBrokenWall()
	{
		OnPlaceBrokenWall(mAttackParam1, mAttackParam2, mAttackParam3);
	}

	private void PlaceFamiliar()
	{
		OnPlaceFamiliar(mAttackParam1, mAttackParam2);
	}

	public bool AddDamage(int amount)
	{
		if (IsDead())
		{
			return false;
		}
		Damage();
		mLife -= amount;
		if (mLife <= 0)
		{
			mLife = 0;
			Dead();
			return true;
		}
		return false;
	}

	public bool AttackCountDown(int amount)
	{
		if (mLife == 0)
		{
			return false;
		}
		if (mTargets.Count <= 0)
		{
			ShuffleAttackTarget(true, false);
			return false;
		}
		if (mAttackCount == 0)
		{
			return false;
		}
		mAttackCount--;
		ChangeCountDown();
		if (mAttackCount == 0)
		{
			Attack();
			return true;
		}
		return false;
	}

	public bool IsWeak()
	{
		if ((float)mLife / (float)mLifeMax <= 0.2f)
		{
			return true;
		}
		return false;
	}

	public bool IsDeadBusy()
	{
		if (mState.GetStatus() == STATE.DEAD)
		{
			return true;
		}
		return false;
	}

	public bool IsDead()
	{
		if (mState.GetStatus() == STATE.DEAD)
		{
			return true;
		}
		if (mState.GetStatus() == STATE.DEAD_FINISHED)
		{
			return true;
		}
		return false;
	}

	public Vector3 GetCenterPos()
	{
		return mSprite.transform.position;
	}

	public Def.BOSS_ATTACK GetAttackType()
	{
		return mAttackPattern;
	}
}
