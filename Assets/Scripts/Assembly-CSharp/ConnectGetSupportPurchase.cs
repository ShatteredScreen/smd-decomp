using System;

public class ConnectGetSupportPurchase : ConnectBase
{
	protected Server.MailType mMailType;

	protected string mJson;

	protected bool mNetworkDataModified;

	private SCCheckResponse mResponse;

	private bool mIsSetResponseData;

	public void SetResponseData(Server.MailType a_kind, string a_json)
	{
		mMailType = a_kind;
		mJson = a_json;
	}

	public void SetResponseData(SCCheckResponse a_response)
	{
		mResponse = a_response;
		mIsSetResponseData = true;
	}

	protected override void InitServerFlg()
	{
		GameMain.mMapSupportPurchaseCheckDone = false;
		GameMain.mMapSupportPurchaseSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mMapSupportPurchaseCheckDone = true;
		GameMain.mMapSupportPurchaseSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mMapSupportPurchaseCheckDone = true;
		GameMain.mMapSupportPurchaseSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			return false;
		}
		long num = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastGetSupportPurchaseTime, DateTime.Now);
		long gET_SUPPORTPURCHASE_FREQ = GameMain.GET_SUPPORTPURCHASE_FREQ;
		if (gET_SUPPORTPURCHASE_FREQ > 0 && num < gET_SUPPORTPURCHASE_FREQ)
		{
			ServerFlgSucceeded();
			return false;
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		if (base.IsSkip)
		{
			ServerFlgSucceeded();
			base.StateStart();
			return;
		}
		if (mParam == null)
		{
			mParam = new ConnectParameterBase();
			mParam.SetIgnoreError(true);
		}
		InitServerFlg();
		if (!ServerIAP.SubcurrencyCheck())
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
	}

	protected override void StateConnectFinish()
	{
		try
		{
			if (mResponse != null && mResponse.results != null)
			{
				foreach (SCStatus result in mResponse.results)
				{
					GiftItemData giftItemData = new GiftItemData();
					giftItemData.UseIcon = true;
					giftItemData.UsePlayerInfo = false;
					giftItemData.FromID = 0;
					giftItemData.RemoteID = result.id;
					giftItemData.GiftSource = (GiftMailType)result.mail_type;
					giftItemData.ItemCategory = result.category;
					giftItemData.ItemKind = result.sub_category;
					giftItemData.ItemID = result.itemid;
					giftItemData.Quantity = result.quantity;
					giftItemData.MailType = result.mail_type;
					giftItemData.Message = result.message;
					GiftItem giftItem = new GiftItem();
					giftItem.SetData(giftItemData);
					mPlayer.AddGift(giftItem);
					mNetworkDataModified = true;
				}
			}
			mPlayer.Data.LastGetSupportPurchaseTime = DateTime.Now;
			if (mNetworkDataModified)
			{
				mGame.Save();
			}
		}
		catch (Exception ex)
		{
			BIJLog.E("GetSupportPurchase Error : " + ex);
		}
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
			mConnectResultKind = 3;
			base.StateConnectFinish();
			return false;
		}
		return false;
	}
}
