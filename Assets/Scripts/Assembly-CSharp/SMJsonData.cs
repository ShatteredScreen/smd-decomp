public abstract class SMJsonData
{
	public string GetName()
	{
		return GetType().Name;
	}

	public abstract string SerializeToJson();

	public abstract void DeserializeFromJson(string a_jsonStr);

	public abstract void SerializeToBinary(BIJBinaryWriter data);

	public abstract void DeserializeFromBinary(BIJBinaryReader data, int reqVersion);
}
