public enum SsColorBlendTarget
{
	None = 0,
	Overall = 1,
	Vertex = 2,
	Num = 3
}
