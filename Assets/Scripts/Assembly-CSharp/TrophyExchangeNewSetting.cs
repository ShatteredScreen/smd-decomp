using System.Collections.Generic;

public class TrophyExchangeNewSetting
{
	[MiniJSONAlias("months")]
	public List<int> mMonths { get; set; }

	[MiniJSONAlias("day")]
	public int mDay { get; set; }

	[MiniJSONAlias("hour")]
	public int mHour { get; set; }

	[MiniJSONAlias("ids")]
	public List<int> mIDs { get; set; }

	[MiniJSONAlias("limit")]
	public int mLimit { get; set; }

	public TrophyExchangeNewSetting()
	{
		mMonths = new List<int>();
		mIDs = new List<int>();
	}
}
