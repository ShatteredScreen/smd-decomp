using System.Collections;
using UnityEngine;

public class TutorialSkipConfirmDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	private OnDialogClosed mCallback;

	private BIJImage mSkipAtlas;

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public override IEnumerator BuildResource()
	{
		ResImage image = ResourceManager.LoadImageAsync("SKIPTUTORIAL");
		while (image == null || image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		mSkipAtlas = image.Image;
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(Localization.Get("TutSkip_Title"));
		UISprite sprite = Util.CreateSprite("Luna", base.gameObject, mSkipAtlas);
		Util.SetSpriteInfo(sprite, "chara_skip", mBaseDepth + 4, new Vector3(-130f, 35f, 0f), Vector3.one, false);
		sprite = Util.CreateSprite("Hukidashi", base.gameObject, mSkipAtlas);
		Util.SetSpriteInfo(sprite, "skip_hukidashi", mBaseDepth + 4, new Vector3(110f, 45f, 0f), Vector3.one, false);
		UILabel uILabel = Util.CreateLabel("Desc", sprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("TutSkip_New_Desc"), mBaseDepth + 5, new Vector3(0f, -4f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(250, 120);
		float y = -100f;
		UIButton button = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_yes", "LC_button_yes", "LC_button_yes", mBaseDepth + 4, new Vector3(-130f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		button = Util.CreateJellyImageButton("ButtonNegative", base.gameObject, image);
		Util.SetImageButtonInfo(button, "LC_button_no", "LC_button_no", "LC_button_no", mBaseDepth + 4, new Vector3(130f, y, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnNegativePushed", UIButtonMessage.Trigger.OnClick);
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnNegativePushed(null);
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			Close();
		}
	}

	public void OnNegativePushed(GameObject go)
	{
		if (!GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			mSelectItem = SELECT_ITEM.NEGATIVE;
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}
}
