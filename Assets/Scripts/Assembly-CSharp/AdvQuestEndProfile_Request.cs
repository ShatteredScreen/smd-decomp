public class AdvQuestEndProfile_Request : RequestBase
{
	[MiniJSONAlias("is_win")]
	public int IsWin { get; set; }

	[MiniJSONAlias("first_clear_reward_ids")]
	public int[] FirstRewardIDs { get; set; }

	[MiniJSONAlias("enemy_drop_reward_ids")]
	public int[] EnemyRewardIDs { get; set; }

	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }

	[MiniJSONAlias("is_retry")]
	public int IsRetry { get; set; }
}
