using System.Collections.Generic;

public sealed class ChanceDataInfo
{
	[MiniJSONAlias("type")]
	public int Type { get; set; }

	[MiniJSONAlias("enable")]
	public int Enable { get; set; }

	[MiniJSONAlias("try_num")]
	public int TryNum { get; set; }

	[MiniJSONAlias("per")]
	public List<int> Probability { get; set; }

	[MiniJSONAlias("cond")]
	public List<ChanceConditionInfo> Condition { get; set; }

	[MiniJSONAlias("ab_place")]
	public List<int> ABtestingPlace { get; set; }

	public bool IsEnable
	{
		get
		{
			return Enable != 0;
		}
	}

	public ChanceDataInfo()
	{
		Type = 0;
		Enable = 0;
		TryNum = 0;
		Probability = new List<int>();
		Condition = new List<ChanceConditionInfo>();
		ABtestingPlace = new List<int>();
	}
}
