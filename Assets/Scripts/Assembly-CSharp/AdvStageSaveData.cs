using System;

public class AdvStageSaveData
{
	public bool mClear { get; set; }

	public bool mPlayed { get; set; }

	public int mWinCount { get; set; }

	public int mLoseCount { get; set; }

	public int mStage { get; set; }

	public float WinRate
	{
		get
		{
			if (mWinCount + mLoseCount < 1)
			{
				return 0f;
			}
			double num = (double)mWinCount / (double)(mWinCount + mLoseCount);
			return Convert.ToSingle(num * 100.0);
		}
	}

	public AdvStageSaveData()
	{
		Init();
	}

	public void Init()
	{
		mClear = false;
		mPlayed = false;
		mWinCount = 0;
		mLoseCount = 0;
		mStage = 0;
	}

	public void SerializeToBinary(BIJBinaryWriter aWriter)
	{
		aWriter.WriteBool(mClear);
		aWriter.WriteBool(mPlayed);
		aWriter.WriteInt(mWinCount);
		aWriter.WriteInt(mLoseCount);
		aWriter.WriteInt(mStage);
	}

	public void DeserializeFromBinary(BIJBinaryReader aReader)
	{
		mClear = aReader.ReadBool();
		mPlayed = aReader.ReadBool();
		mWinCount = aReader.ReadInt();
		mLoseCount = aReader.ReadInt();
		mStage = aReader.ReadInt();
	}
}
