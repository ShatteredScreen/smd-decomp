public class MissionInfo
{
	public MissionData mData;

	public MissionProgress mProgress;

	public bool IsAdvMission()
	{
		if (mData.mID >= 1000)
		{
			return true;
		}
		return false;
	}
}
