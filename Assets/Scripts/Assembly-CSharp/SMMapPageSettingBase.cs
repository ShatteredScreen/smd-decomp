public abstract class SMMapPageSettingBase
{
	public bool HasError;

	public int mStageNo = -1;

	public int mSubNo;

	public int mCourseNo;

	public string OpenDemo = string.Empty;

	public string ClearDemo = string.Empty;

	public int StageNo
	{
		get
		{
			if (Series != Def.SERIES.SM_EV)
			{
				return Def.GetStageNo(mStageNo, mSubNo);
			}
			return Def.GetStageNo(mCourseNo, mStageNo, mSubNo);
		}
	}

	public Def.SERIES Series { get; protected set; }

	public string Key
	{
		get
		{
			return MakeKey(mStageNo, mSubNo, mCourseNo);
		}
	}

	public SMMapPageSettingBase()
	{
		Series = Def.SERIES.SM_FIRST;
	}

	public void SetSeries(Def.SERIES a_series)
	{
		Series = a_series;
	}

	public static string MakeKey(int a_main, int a_sub, int a_course = 0)
	{
		if (a_sub == 0)
		{
			return string.Format("{0:D4}", a_main + 1000 * a_course);
		}
		return string.Format("{0:D4}-{1}", a_main + 1000 * a_course, a_sub);
	}

	public int GetStoryDemoClear()
	{
		int result = -1;
		if (!int.TryParse(ClearDemo, out result))
		{
			result = -1;
		}
		return result;
	}

	public abstract void Serialize(BIJBinaryWriter a_writer);

	public abstract void Deserialize(BIJBinaryReader a_reader);
}
