using System;
using System.Collections.Generic;
using UnityEngine;

public class MapMovableObject : MapEntity
{
	public delegate void OnDisappeared(MapMovableObject a_object);

	public STATE mStateStatus;

	private OnDisappeared mCallback;

	private int mSpawnPointIndex = -1;

	private new float mStateTime;

	private Vector3 mDirection;

	private Vector3 mBasePos;

	private Vector3 mTargetPos;

	private Vector3 mControlPos;

	private float mSpeed;

	private bool mFirstInitialized;

	private string mName;

	private int mCounter;

	private bool mIsUnlock;

	private bool mHasParticle;

	private ParticleOnSSAnime mParticle;

	private bool mNoAngle;

	private float mAlpha = 1f;

	public float mTime;

	public void SetDisappearedCallback(OnDisappeared callback)
	{
		mCallback = callback;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		if (mParticle != null)
		{
			UnityEngine.Object.Destroy(mParticle.gameObject);
			mParticle = null;
		}
	}

	public override void Update()
	{
		mStateStatus = mState.GetStatus();
		switch (mStateStatus)
		{
		case STATE.STAY:
		{
			mStateTime += Time.deltaTime * mSpeed;
			float num = mStateTime;
			if (num >= 90f)
			{
				num = 90f;
			}
			mTime = Mathf.Sin(num * ((float)Math.PI / 180f));
			float num2 = 0.999f;
			if (mNoAngle)
			{
				num2 = 0.65f;
			}
			if (mTime >= num2)
			{
				mTime = 1f;
				mState.Change(STATE.WAIT);
			}
			float num3 = mTime;
			float x = num3 * num3 * mTargetPos.x + 2f * num3 * (1f - num3) * mControlPos.x + (1f - num3) * (1f - num3) * mBasePos.x;
			float y = num3 * num3 * mTargetPos.y + 2f * num3 * (1f - num3) * mControlPos.y + (1f - num3) * (1f - num3) * mBasePos.y;
			base._pos = new Vector3(x, y, base._pos.z);
			break;
		}
		case STATE.WAIT:
			if (mCallback != null)
			{
				mCallback(this);
			}
			break;
		}
		mState.Update();
	}

	public void Init(int _counter, string _name, BIJImage atlas, float x, float y, Vector3 scale, float a_deg, int a_spawn, int a_mapIndex, Def.SERIES a_series, bool a_noAngle, string a_animeKey, float a_offset = 0f)
	{
		AnimeKey = a_animeKey;
		DefaultAnimeKey = a_animeKey;
		mCounter = _counter;
		mName = _name;
		mAtlas = atlas;
		mScale = scale;
		float f = a_deg * ((float)Math.PI / 180f);
		mNoAngle = a_noAngle;
		mSpawnPointIndex = a_spawn;
		float z = Def.MAP_MOVABLE_Z;
		float num = 0f;
		if (mNoAngle)
		{
			z = Def.MAP_ENTITY_Z;
			num = 10f;
			mAlpha = UnityEngine.Random.Range(0.5f, 1f);
			y += (float)(UnityEngine.Random.Range(-3, 10) * 20);
		}
		else
		{
			y += UnityEngine.Random.Range(-50f, 50f);
		}
		mBasePos = new Vector3(x, y, z);
		x *= 1.2f;
		float num2 = 0f;
		if (x > 0f)
		{
			mDirection = new Vector3(-1f * Mathf.Cos(f), -1f * Mathf.Sin(f), z) * x * 2f;
		}
		else if (x < 0f)
		{
			mDirection = new Vector3(Mathf.Cos(f), Mathf.Sin(f), z) * x * 2f;
		}
		else
		{
			mDirection = new Vector3(Mathf.Cos(f), y, z) * x * 2f;
		}
		if (!mNoAngle)
		{
			num2 = ((!(x > 0f)) ? (0f - UnityEngine.Random.Range(0f, 15f)) : UnityEngine.Random.Range(0f, 15f));
			mSpeed = 20f;
		}
		else
		{
			mSpeed = UnityEngine.Random.Range(0.5f, 1.6f);
		}
		mTargetPos = mBasePos + mDirection;
		Vector3 vector = new Vector3(mDirection.x * Mathf.Cos(num2 * ((float)Math.PI / 180f)) - mDirection.y * Mathf.Sin(num2 * ((float)Math.PI / 180f)), mDirection.x * Mathf.Sin(num2 * ((float)Math.PI / 180f)) + mDirection.y * Mathf.Cos(num2 * ((float)Math.PI / 180f)), z);
		mControlPos = vector + mBasePos;
		mStateTime = 0f;
		base._pos = mBasePos;
		base._zrot = 0f;
		PlayAnime(AnimeKey, true);
		if (_sprite != null && _sprite.Animation != null)
		{
			SsAnimation animation = _sprite.Animation;
			SsPartRes[] partList = animation.PartList;
			for (int i = 0; i < partList.Length; i++)
			{
				if (partList[i].IsRoot || !partList[i].Name.StartsWith("pe_"))
				{
					continue;
				}
				string[] array = partList[i].Name.Split('_');
				if (array.Length > 1)
				{
					string text = string.Empty;
					for (int j = 1; j < array.Length; j++)
					{
						text = text + array[j] + "_";
					}
					text = text.TrimEnd('_');
					mParticle = Util.CreateGameObject(_name + "_pe", null).AddComponent<ParticleOnSSAnime>();
					mParticle.Init(_sprite, "Particles/" + text, 5f, partList[i].Name, base.gameObject, a_offset);
					mParticle.SetEmitterDestroyCallback(OnParticleFinished);
					mParticle.Play();
				}
			}
		}
		if (mNoAngle)
		{
			SetAlpha(mAlpha);
		}
		mFirstInitialized = true;
	}

	public void ResetAnime()
	{
		mChangeParts = new Dictionary<string, string>();
		PlayAnime(AnimeKey, true);
	}

	public void OnParticleFinished()
	{
		if (mParticle != null)
		{
			GameMain.SafeDestroy(mParticle.gameObject);
			mParticle = null;
		}
	}

	public void SetEnable(bool a_flg)
	{
	}
}
