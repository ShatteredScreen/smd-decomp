using UnityEngine;

public interface IPuzzleManager
{
	GameObject Root { get; }

	float ExpandedScoreRatio { get; }

	float SkillExpandedScoreRatio { get; }

	Vector2 GetDefaultCoords(IntVector2 a_pos);

	bool IsInField(IntVector2 pos);

	bool IsInField(int x, int y);

	PuzzleTile GetAttribute(IntVector2 pos, int layer = -1);

	void MoveTempCandy(PuzzleTile candy, IntVector2 toPos);

	PuzzleTile RemoveTile(PuzzleTile candy, bool removeToTemp);

	PuzzleTile RemoveTile(IntVector2 pos, bool removeToTemp);

	void PlaceTile(IntVector2 tilePos, PuzzleTile tile);

	bool IsCollectable(PuzzleTile candy);

	void BeginCollect(PuzzleTile tile);

	Vector2 GetFallTargetCoords(IntVector2 pos);

	bool IsThreeOrMoreGather(IntVector2 pos);

	void MakeHitNum(Vector3 _pos);

	void CrushTile(PuzzleTile tile, Def.DIR moveDir);

	void AddCombo();

	IntVector2 SearchFallTarget(IntVector2 pos, bool exitable);

	bool IsItemUsable();

	bool IsItemUsableTapItem();

	void BeginTapCrush(PuzzleTile tile);

	PuzzleTile GetTile(IntVector2 pos, bool checkForTemp);

	void OneShotAnime_OnFinished(OneShotAnime a_anime);

	void AddOneShotAnime(OneShotAnime a_anime);

	void AddScore(int deltaScore);

	void AddTime(float deltaTime);

	void AddMoves(int deltaMove);

	void AddHp(float deltaHP);

	Def.TILE_KIND RandomColorTile(int spawnPointIndex);

	bool BeginCrush(PuzzleTile tile, float timer, Def.EFFECT_OVERRIDE effectOverride, Def.TILE_KIND nextKind, Def.TILE_FORM nextForm, int score, Def.TILE_KIND rainbowTargetKind, int bombSize, bool tapTrigger = false, bool forceCrush = false);

	void BeginAttributeCrush(IntVector2 pos, float timer, bool effectCrush);

	void SpecialWaveH(IntVector2 pos);

	void SpecialWaveV(IntVector2 pos);

	void SpecialBomb(IntVector2 pos, int size);

	void SpecialBossDamage(PuzzleTile tile);

	void SpecialRainbow(IntVector2 pos, Def.TILE_KIND targetKind);

	void SpecialPaint(IntVector2 pos, Def.TILE_KIND targetKind, Def.TILE_KIND changeKind);

	void SkillPushed(PuzzleTile targetTile);

	void SpecialCombo_WaveWave(IntVector2 pos);

	void SpecialCombo_WaveBomb(IntVector2 pos);

	void SpecialCombo_WaveRainbow(IntVector2 pos, Def.TILE_KIND targetKind);

	void SpecialCombo_BombBomb(IntVector2 pos);

	void SpecialCombo_BombRainbow(IntVector2 pos, Def.TILE_KIND targetKind);

	void SpecialCombo_RainbowRainbow(IntVector2 pos);

	void SpecialCombo_BombPaint(IntVector2 pos, Def.TILE_KIND paintKind, Def.TILE_KIND targetKind);

	void SpecialCombo_WavePaint(IntVector2 pos, Def.TILE_KIND paintKind, Def.TILE_KIND targetKind);

	void SpecialCombo_PaintRainbow(IntVector2 pos, Def.TILE_KIND changeKind);

	void SpecialCombo_PaintPaint(IntVector2 pos, Def.TILE_KIND changeKind);

	void AddSpecialCreateCount(Def.TILE_FORM form);

	void AddSpecialCrushCount(Def.TILE_FORM form);
}
