using System.Collections.Generic;
using UnityEngine;

public class EventStarRoadBlock : MapRoadBlock
{
	protected static readonly string[] numberRBImageName = new string[10] { "quota_num0", "quota_num1", "quota_num2", "quota_num3", "quota_num4", "quota_num5", "quota_num6", "quota_num7", "quota_num8", "quota_num9" };

	protected short mCourseID;

	public int Debug_Accessories;

	public void SetCourseID(short a_courseID)
	{
		mCourseID = a_courseID;
	}

	public override void Init(int _counter, string _name, SMRoadBlockSetting data, BIJImage atlas, float x, float y, Vector3 scale, bool a_unlock, float a_rad, string a_animeKey = "MAP_ROADBLOCK")
	{
		AnimeKey = a_animeKey;
		mBaseAnimeKey = a_animeKey;
		mCounter = _counter;
		mName = _name;
		mAtlas = atlas;
		mScale = scale;
		mData = data;
		mRad = 0f;
		mIsUnlock = a_unlock;
		SMEventRoadBlockSetting sMEventRoadBlockSetting = mData as SMEventRoadBlockSetting;
		base._pos = new Vector3(x, y, Def.MAP_BUTTON_Z - 0.1f);
		base._zrot = mRad;
		if (!mIsUnlock)
		{
			HasTapAnime = true;
			mChangeParts = new Dictionary<string, string>();
			Player mPlayer = mGame.mPlayer;
			int roadBlockLevel = data.RoadBlockLevel;
			bool flag = true;
			PlayerClearData _psd;
			if (mPlayer.GetStageClearData(mPlayer.Data.CurrentSeries, mPlayer.Data.CurrentEventID, mCourseID, roadBlockLevel, out _psd))
			{
				flag = false;
			}
			SetChangeParts();
			if (flag)
			{
				IsLockTapAnime = true;
				mChangeParts["mess"] = "null";
			}
			else
			{
				mChangeParts["mess"] = "ac_mess_tap";
			}
			if (!mFirstInitialized)
			{
				mBoxCollider = base.gameObject.AddComponent<BoxCollider>();
				mEventTrigger = base.gameObject.AddComponent<UIEventTrigger>();
				mBoxCollider.size = new Vector3(100f, 100f, 0f);
				EventDelegate item = new EventDelegate(this, "OnButtonPushed");
				mEventTrigger.onPress.Add(item);
				EventDelegate item2 = new EventDelegate(this, "OnButtonReleased");
				mEventTrigger.onRelease.Add(item2);
				EventDelegate item3 = new EventDelegate(this, "OnButtonClicked");
				mEventTrigger.onClick.Add(item3);
			}
		}
		else
		{
			AnimeKey = mBaseAnimeKey + "_UNLOCKED";
			mChangeParts = null;
			HasTapAnime = false;
			if (mFirstInitialized)
			{
				mBoxCollider.enabled = false;
				mEventTrigger.enabled = false;
			}
		}
		PlayAnime(AnimeKey, true);
		mFirstInitialized = true;
	}

	protected void SetChangeParts()
	{
		mChangeParts = new Dictionary<string, string>();
		SMEventRoadBlockSetting sMEventRoadBlockSetting = mData as SMEventRoadBlockSetting;
		mChangeParts["item"] = "ss_" + sMEventRoadBlockSetting.ImageName;
		mChangeParts["item"] = "ss_" + sMEventRoadBlockSetting.ImageName;
		mChangeParts["one_01"] = "null";
		mChangeParts["ten_01"] = "null";
		mChangeParts["ten_02"] = "null";
		mChangeParts["hundred_01"] = "null";
		mChangeParts["hundred_02"] = "null";
		mChangeParts["hundred_03"] = "null";
		int unlockStars = sMEventRoadBlockSetting.UnlockStars;
		if (unlockStars < 10)
		{
			mChangeParts["one_01"] = numberRBImageName[unlockStars];
		}
		else if (unlockStars < 100)
		{
			mChangeParts["ten_01"] = numberRBImageName[unlockStars % 10];
			mChangeParts["ten_02"] = numberRBImageName[unlockStars / 10];
		}
		else if (unlockStars < 1000)
		{
			int num = unlockStars;
			mChangeParts["hundred_01"] = numberRBImageName[num % 10];
			num /= 10;
			mChangeParts["hundred_02"] = numberRBImageName[num % 10];
			mChangeParts["hundred_03"] = numberRBImageName[num / 10];
		}
	}

	public override void OpenAnime()
	{
		AnimeKey = mBaseAnimeKey;
		SetChangeParts();
		mChangeParts["mess"] = "ac_mess_tap";
		PlayAnime(AnimeKey, true, true);
		HasTapAnime = true;
		mParticle = Util.CreateGameObject("OpenPartilce", base.gameObject).AddComponent<ParticleOnSSAnime>();
		mParticle.Init(_sprite, "Particles/StageOpenParticle", -10f, "mess");
		mParticle.SetEmitterDestroyCallback(base.OnParticleFinished);
		mState.Change(STATE.STAY);
	}

	public override void UnlockAnime()
	{
		SetChangeParts();
		if (AnimeKey.CompareTo(mBaseAnimeKey) == 0)
		{
			PlayAnime(AnimeKey + "_UNLOCK", false);
		}
		AnimeKey = mBaseAnimeKey + "_UNLOCKED";
		HasTapAnime = false;
		mState.Change(STATE.UNLOCK_WAIT);
	}

	public virtual void SetDebugInfo(SMRoadBlockSetting a_setting)
	{
		SMEventRoadBlockSetting sMEventRoadBlockSetting = a_setting as SMEventRoadBlockSetting;
		Debug_Accessories = sMEventRoadBlockSetting.AccessoryIDOnOpened;
	}
}
