using System.Collections.Generic;
using UnityEngine;

public class TutorialManager
{
	private GameMain mGame;

	private Def.TutorialStartData mCurrentTutorialData;

	private Def.TUTORIAL_INDEX mCurrentTutorial = Def.TUTORIAL_INDEX.NONE;

	private Def.TUTORIAL_INDEX mNextTutorial = Def.TUTORIAL_INDEX.NONE;

	private bool mTutorialPlaying;

	private TutorialMessageDialog mMessageDlg;

	public List<PuzzleTile> mHighlightTiles = new List<PuzzleTile>();

	public PuzzleTile mWantMoveTile;

	public Def.DIR mWantMoveDir;

	public PuzzleTile mWantMoveTileR;

	public Def.DIR mWantMoveDirR;

	private float mWantMoveIntervalTimer;

	private static float WANT_MOVE_INTERVAL_TIME = 0.5f;

	public List<GameObject> mAllowedButtons = new List<GameObject>();

	private TutorialArrow mArrow;

	private TutorialArrow mArrowPoint1;

	private TutorialArrow mArrowPoint2;

	private List<TutorialArrow> mArrowList = new List<TutorialArrow>();

	public TutorialManager()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mWantMoveIntervalTimer = 0f;
	}

	public void InitialAdvTutorialComplete()
	{
		TutorialComplete(Def.TUTORIAL_INDEX.ADV_INITIAL_COMPLETE);
	}

	public bool IsInitialAdvTutorialCompleted()
	{
		return mGame.mPlayer.IsTutorialCompleted(Def.TUTORIAL_INDEX.ADV_INITIAL_COMPLETE.ToString());
	}

	public void GashaAdvTutorialComplete()
	{
		TutorialComplete(Def.TUTORIAL_INDEX.ADV_GASHA_COMPLETE);
	}

	public bool IsGashaAdvTutorialCompleted()
	{
		return mGame.mPlayer.IsTutorialCompleted(Def.TUTORIAL_INDEX.ADV_GASHA_COMPLETE.ToString());
	}

	public void TeamAdvTutorialComplete()
	{
		TutorialComplete(Def.TUTORIAL_INDEX.ADV_TEAM_DETAIL_COMPLETE);
	}

	public bool IsTeamAdvTutorialCompleted()
	{
		return mGame.mPlayer.IsTutorialCompleted(Def.TUTORIAL_INDEX.ADV_TEAM_DETAIL_COMPLETE.ToString());
	}

	public void BonusEnemyTutorialComplete()
	{
		TutorialComplete(Def.TUTORIAL_INDEX.ADV_BONUS_ENEMY_COMPLETE);
	}

	public bool IsBonusEnemyTutorialComplete()
	{
		return mGame.mPlayer.IsTutorialCompleted(Def.TUTORIAL_INDEX.ADV_BONUS_ENEMY_COMPLETE.ToString());
	}

	public void AdvTutorialAllReset()
	{
		for (Def.TUTORIAL_INDEX tUTORIAL_INDEX = Def.TUTORIAL_INDEX.ADV_INITIAL_HOME_1_0; tUTORIAL_INDEX <= Def.TUTORIAL_INDEX.ADV_TEAM_DETAIL_COMPLETE; tUTORIAL_INDEX++)
		{
			mGame.mPlayer.ResetTutorial(tUTORIAL_INDEX.ToString());
		}
		for (Def.TUTORIAL_INDEX tUTORIAL_INDEX2 = Def.TUTORIAL_INDEX.ADV_PUZZLE_HEAL_1_0; tUTORIAL_INDEX2 <= Def.TUTORIAL_INDEX.ADV_PUZZLE_HEAL_1_2; tUTORIAL_INDEX2++)
		{
			mGame.mPlayer.ResetTutorial(tUTORIAL_INDEX2.ToString());
		}
	}

	~TutorialManager()
	{
	}

	public void Update(float deltaTime)
	{
		if (mWantMoveIntervalTimer > 0f)
		{
			mWantMoveIntervalTimer -= deltaTime;
			if (mWantMoveIntervalTimer < 0f)
			{
				mWantMoveIntervalTimer = 0f;
			}
		}
	}

	public bool IsTutorialCompleted(Def.TUTORIAL_INDEX index, bool skip = false)
	{
		string index2 = Def.TUTORIAL_START_DATA[(int)index].lastIndex.ToString();
		bool flag = mGame.mPlayer.IsTutorialCompleted(index2);
		if (skip && !flag && Def.TUTORIAL_START_DATA[(int)index].skipRule)
		{
			TutorialSkip(index);
			mGame.SavePlayerData();
			flag = true;
		}
		return flag;
	}

	public void ResetTutorialCompleted(Def.TUTORIAL_INDEX index)
	{
		string index2 = Def.TUTORIAL_START_DATA[(int)index].lastIndex.ToString();
		mGame.mPlayer.ResetTutorial(index2);
	}

	public void InitialTutorialComplete()
	{
		TutorialComplete(Def.TUTORIAL_INDEX.EPILOGUE);
	}

	public bool IsInitialTutorialCompleted()
	{
		return mGame.mPlayer.IsTutorialCompleted(Def.TUTORIAL_INDEX.EPILOGUE.ToString());
	}

	public Def.TUTORIAL_INDEX TutorialComplete(Def.TUTORIAL_INDEX index)
	{
		mCurrentTutorial = Def.TUTORIAL_INDEX.NONE;
		mTutorialPlaying = false;
		if (mGame.mPlayer.TutorialComplete(index.ToString()))
		{
			mGame.SendTutorialStep(index);
		}
		DeleteTutorialArrow();
		ClearAllowedButtons();
		mNextTutorial = Def.TUTORIAL_START_DATA[(int)index].nextIndex;
		if (mNextTutorial == Def.TUTORIAL_INDEX.NONE || Def.TUTORIAL_START_DATA[(int)index].lastIndex == index)
		{
			MessageClose();
		}
		return mNextTutorial;
	}

	public Def.TUTORIAL_INDEX TutorialSkip(Def.TUTORIAL_INDEX index)
	{
		int num = (int)index;
		TutorialComplete(index);
		while (Def.TUTORIAL_START_DATA[num].nextIndex != Def.TUTORIAL_INDEX.NONE)
		{
			TutorialComplete(Def.TUTORIAL_START_DATA[num].nextIndex);
			num = (int)Def.TUTORIAL_START_DATA[num].nextIndex;
		}
		return (Def.TUTORIAL_INDEX)num;
	}

	public void TutorialStart(Def.TUTORIAL_INDEX index, GameObject parent, TutorialMessageDialog.OnDialogClosed onClosed, TutorialMessageDialog.OnDialogClosed onMessageFinished, bool Tutorialsort = false)
	{
		Def.TutorialStartData data = Def.TUTORIAL_START_DATA[(int)index];
		if (mMessageDlg != null)
		{
			mMessageDlg.Change(index, data);
			if (Tutorialsort)
			{
				NGUITools.FindInParents<UIPanel>(mMessageDlg.gameObject).sortingOrder += 21;
			}
		}
		else if (data.messageDisp)
		{
			mMessageDlg = Util.CreateGameObject("TutorialMessage", parent).AddComponent<TutorialMessageDialog>();
			mMessageDlg.Init(index, data);
			mMessageDlg.SetClosedCallback(onClosed, onMessageFinished);
		}
		mCurrentTutorial = index;
		mTutorialPlaying = true;
	}

	public void MessageClose()
	{
		if (mMessageDlg != null)
		{
			mMessageDlg.Close();
			mMessageDlg = null;
		}
	}

	public void MessageFakeClose(TutorialMessageDialog.OnFakeClosed onFakeClosed)
	{
		if (mMessageDlg != null)
		{
			mMessageDlg.FakeClose(onFakeClosed);
		}
	}

	public void MessageFakeOpen()
	{
		if (mMessageDlg != null)
		{
			mMessageDlg.FakeOpen();
		}
	}

	public bool IsMessageFakeClosed()
	{
		if (mMessageDlg != null)
		{
			return mMessageDlg.IsFakeClosed();
		}
		return false;
	}

	public bool IsTutorialPlaying()
	{
		return mTutorialPlaying;
	}

	public Def.TUTORIAL_INDEX GetCurrentTutorial()
	{
		return mCurrentTutorial;
	}

	public Def.TutorialStartData GetCurrentTutorialData()
	{
		return mCurrentTutorialData;
	}

	public Def.TUTORIAL_INDEX GetNextTutorial()
	{
		return mNextTutorial;
	}

	public bool IsWantMove(PuzzleTile tile, Def.DIR dir)
	{
		if (!mTutorialPlaying)
		{
			return true;
		}
		if (mWantMoveIntervalTimer <= 0f)
		{
			if (tile == mWantMoveTile && dir == mWantMoveDir)
			{
				return true;
			}
			if (tile == mWantMoveTileR && dir == mWantMoveDirR)
			{
				return true;
			}
		}
		return false;
	}

	public void SetWantMove(PuzzleTile tile, Def.DIR dir, PuzzleTile tileR)
	{
		mWantMoveTile = tile;
		mWantMoveTileR = tileR;
		mWantMoveDir = dir;
		if (dir == Def.DIR.NONE)
		{
			mWantMoveDirR = Def.DIR.NONE;
		}
		else
		{
			mWantMoveDirR = Def.RETURN_DIR[(int)dir];
		}
		mWantMoveIntervalTimer = WANT_MOVE_INTERVAL_TIME;
	}

	public void ClearWantMove()
	{
		mWantMoveTile = null;
		mWantMoveTileR = null;
		mWantMoveDir = Def.DIR.NONE;
		mWantMoveDirR = Def.DIR.NONE;
	}

	public bool IsHighLightTile(PuzzleTile tile)
	{
		return mHighlightTiles.Contains(tile);
	}

	public void AddHighLightTile(PuzzleTile tile)
	{
		mHighlightTiles.Add(tile);
	}

	public void RemoveHighLightTile(PuzzleTile tile)
	{
		mHighlightTiles.Remove(tile);
	}

	public void ClearHighLightTiles()
	{
		mHighlightTiles.Clear();
	}

	public bool IsAllowed(GameObject go)
	{
		return mAllowedButtons.Contains(go);
	}

	public void AddAllowedButton(GameObject go)
	{
		mAllowedButtons.Add(go);
	}

	public void RemoveAllowedButton(GameObject go)
	{
		mAllowedButtons.Remove(go);
	}

	public void ClearAllowedButtons()
	{
		mAllowedButtons.Clear();
	}

	public void SetTutorialArrow(GameObject parent, GameObject target, bool vFlip = false, int depth = 80, Def.DIR dir = Def.DIR.NONE)
	{
		mArrow = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		mArrow.Init(TutorialArrow.STYLE.ARROW, target, vFlip, Def.DIR.DOWN, depth, dir, 0f);
	}

	public void SetTutorialArrowOffset(float x, float y)
	{
		if (mArrow != null)
		{
			mArrow.SetTargetPosOfs(x, y);
		}
	}

	public void SetTutorialPoint1Arrow(GameObject parent, GameObject target, bool vFlip = false, int depth = 80, Def.DIR dir = Def.DIR.NONE, float Ypos = 0f)
	{
		mArrowPoint1 = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		mArrowPoint1.Init(TutorialArrow.STYLE.ARROW, target, vFlip, Def.DIR.DOWN, depth, dir, Ypos);
	}

	public void SetTutorialmArrowPoint1Offset(float x, float y)
	{
		if (mArrowPoint1 != null)
		{
			mArrowPoint1.SetTargetPosOfs(x, y);
		}
	}

	public void SetTutorialPoint2Arrow(GameObject parent, GameObject target, bool vFlip = false, int depth = 80, Def.DIR dir = Def.DIR.NONE, float Ypos = 0f)
	{
		mArrowPoint2 = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		mArrowPoint2.Init(TutorialArrow.STYLE.ARROW, target, vFlip, Def.DIR.DOWN, depth, dir, Ypos);
	}

	public void SetTutorialFinger(GameObject parent, GameObject target, Def.DIR dir, int depth = 80)
	{
		mArrow = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		mArrow.Init(TutorialArrow.STYLE.FINGER, target, false, dir, depth, Def.DIR.NONE, 0f);
	}

	public void SetTutorialFinger(GameObject parent, GameObject target, Def.DIR dir, bool vFlip, int depth = 80)
	{
		mArrow = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		mArrow.Init(TutorialArrow.STYLE.FINGER, target, vFlip, dir, depth, Def.DIR.NONE, 0f);
	}

	public void SetTutorialFingerMulti(GameObject parent, GameObject target, Def.DIR dir, int depth = 80)
	{
		TutorialArrow tutorialArrow = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		tutorialArrow.Init(TutorialArrow.STYLE.FINGER, target, false, dir, depth, Def.DIR.NONE, 0f);
		mArrowList.Add(tutorialArrow);
	}

	public void SetTutorialFingerMulti(GameObject parent, GameObject target, Def.DIR dir, bool vFlip, int depth = 80)
	{
		TutorialArrow tutorialArrow = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		tutorialArrow.Init(TutorialArrow.STYLE.FINGER, target, vFlip, dir, depth, Def.DIR.NONE, 0f);
		mArrowList.Add(tutorialArrow);
	}

	public void SetTutorialFingerTween(GameObject parent, GameObject target, GameObject dest, Def.DIR dir, int depth = 80)
	{
		TutorialArrow tutorialArrow = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		tutorialArrow.Init(TutorialArrow.STYLE.FINGER_TWEEN, target, dest, false, dir, depth, Def.DIR.NONE, 0f);
		mArrowList.Add(tutorialArrow);
	}

	public void SetTutorialFingerTween(GameObject parent, GameObject target, GameObject dest, Def.DIR dir, bool vFlip, int depth = 80)
	{
		TutorialArrow tutorialArrow = Util.CreateGameObject("TutorialArrow", parent).AddComponent<TutorialArrow>();
		tutorialArrow.Init(TutorialArrow.STYLE.FINGER_TWEEN, target, dest, vFlip, dir, depth, Def.DIR.NONE, 0f);
		mArrowList.Add(tutorialArrow);
	}

	public TutorialArrow GetTutorialArrow(int a_index = -1)
	{
		if (a_index < 0)
		{
			return mArrow;
		}
		if (mArrowList.Count > 0 && a_index < mArrowList.Count)
		{
			return mArrowList[a_index];
		}
		return null;
	}

	public void DestroyTutorialArrow(int a_index = -1)
	{
		if (a_index < 0)
		{
			if (mArrow != null)
			{
				Object.Destroy(mArrow.gameObject);
				mArrow = null;
			}
		}
		else if (mArrowList.Count > 0 && a_index < mArrowList.Count && mArrowList[a_index] != null)
		{
			Object.Destroy(mArrowList[a_index].gameObject);
			mArrowList[a_index] = null;
		}
	}

	public bool HasTutorialArrow()
	{
		if (mArrow != null)
		{
			return true;
		}
		if (mArrowList.Count > 0)
		{
			for (int i = 0; i < mArrowList.Count; i++)
			{
				if (mArrowList[i] != null)
				{
					return true;
				}
			}
		}
		return false;
	}

	public void DeleteTutorialArrow()
	{
		if (mArrow != null)
		{
			Object.Destroy(mArrow.gameObject);
			mArrow = null;
		}
		if (mArrowList.Count > 0)
		{
			for (int i = 0; i < mArrowList.Count; i++)
			{
				Object.Destroy(mArrowList[i].gameObject);
			}
			mArrowList.Clear();
		}
	}

	public void DeleteTutorialPointArrow()
	{
		if (mArrowPoint1 != null)
		{
			Object.Destroy(mArrowPoint1.gameObject);
			mArrowPoint1 = null;
		}
		if (mArrowPoint2 != null)
		{
			Object.Destroy(mArrowPoint2.gameObject);
			mArrowPoint2 = null;
		}
	}

	public void DeleteTutorialFinger()
	{
		DeleteTutorialArrow();
	}
}
