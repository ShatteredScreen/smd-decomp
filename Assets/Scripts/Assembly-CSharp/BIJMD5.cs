using System;
using System.Security.Cryptography;
using System.Text;

public class BIJMD5
{
	private byte[] mHashBytes = new byte[0];

	public byte[] Hash
	{
		get
		{
			return mHashBytes;
		}
	}

	public BIJMD5(string strToEncrypt)
	{
		UTF8Encoding uTF8Encoding = new UTF8Encoding();
		byte[] bytes = uTF8Encoding.GetBytes(strToEncrypt);
		MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
		mHashBytes = mD5CryptoServiceProvider.ComputeHash(bytes);
		mD5CryptoServiceProvider.Clear();
	}

	public BIJMD5(int numToEncrypt)
	{
		byte[] bytes = BitConverter.GetBytes(numToEncrypt);
		MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
		mHashBytes = mD5CryptoServiceProvider.ComputeHash(bytes);
		mD5CryptoServiceProvider.Clear();
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < mHashBytes.Length; i++)
		{
			byte b = mHashBytes[i];
			stringBuilder.Append(b.ToString("x02"));
		}
		return stringBuilder.ToString().PadLeft(32, '0');
	}
}
