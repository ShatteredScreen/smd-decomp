using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartsChangedSprite : ScSpriteObject
{
	public BIJImage Atlas;

	protected MeshRenderer mMeshRenderer;

	public string mCurrentAnimation;

	public int mPlayCount;

	public SsSprite.AnimationCallback mAnimationFinished;

	protected Color mBaseColor = Color.white;

	private BoxCollider mBoxCollder;

	public override void Awake()
	{
		base.Awake();
		mMeshRenderer = GetComponent<MeshRenderer>();
		_sprite.UpdateCollider = false;
		_sprite.PlayAtStart = false;
		mPlayCount = 0;
		mAnimationFinished = null;
	}

	public override void OnDestroy()
	{
		mMeshRenderer = null;
		mCurrentAnimation = null;
		base.OnDestroy();
	}

	public new IEnumerator Start()
	{
		yield return new WaitForEndOfFrame();
	}

	public void OnDisable()
	{
		mMeshRenderer.enabled = false;
	}

	public void OnEnable()
	{
		mMeshRenderer.enabled = true;
	}

	public virtual bool ChangeAnime(string fileName, CHANGE_PART_INFO[] changePartsArray, bool force, int playCount, SsSprite.AnimationCallback callBackFunc)
	{
		if (string.IsNullOrEmpty(fileName))
		{
			fileName = mCurrentAnimation;
		}
		if (!force && fileName == mCurrentAnimation)
		{
			return false;
		}
		if (Atlas == null)
		{
			return false;
		}
		mCurrentAnimation = fileName;
		mPlayCount = playCount;
		mAnimationFinished = callBackFunc;
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(fileName);
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		if (ssAnime == null)
		{
			_sprite.Animation = null;
			return false;
		}
		if (changePartsArray != null)
		{
			Dictionary<int, CHANGE_PART_INFO> dictionary = new Dictionary<int, CHANGE_PART_INFO>();
			for (int i = 0; i < changePartsArray.Length; i++)
			{
				dictionary.Add(changePartsArray[i].partName.GetHashCode(), changePartsArray[i]);
			}
			SsPartRes[] partList = ssAnime.PartList;
			for (int j = 0; j < partList.Length; j++)
			{
				CHANGE_PART_INFO value;
				if (partList[j].IsRoot || !dictionary.TryGetValue(partList[j].Name.GetHashCode(), out value))
				{
					continue;
				}
				Vector2 offset = Atlas.GetOffset(value.spriteName);
				Vector2 size = Atlas.GetSize(value.spriteName);
				float x = offset.x;
				float y = offset.y;
				float x2 = offset.x + size.x;
				float y2 = offset.y + size.y;
				partList[j].UVs = new Vector2[4]
				{
					new Vector2(x, y2),
					new Vector2(x2, y2),
					new Vector2(x2, y),
					new Vector2(x, y)
				};
				Vector4 pixelPadding = Atlas.GetPixelPadding(value.spriteName);
				size = Atlas.GetPixelSize(value.spriteName);
				if (value.enableOffset)
				{
					offset = value.centerOffset;
				}
				else
				{
					offset = new Vector2(partList[j].OriginX, partList[j].OriginY);
					int num = partList[j].PicArea.Right - partList[j].PicArea.Left;
					int num2 = partList[j].PicArea.Bottom - partList[j].PicArea.Top;
					if (num == 1 && num2 == 1)
					{
						offset.x = size.x / 2f;
						offset.y = size.y / 2f;
					}
				}
				offset.x += pixelPadding.x;
				offset.y += pixelPadding.y;
				size.x -= pixelPadding.x + pixelPadding.z;
				size.y -= pixelPadding.y + pixelPadding.w;
				x = 0f - offset.x;
				y = 0f - (size.y - offset.y);
				x2 = size.x - offset.x;
				y2 = offset.y;
				float z = 0f;
				partList[j].OrgVertices = new Vector3[4]
				{
					new Vector3(x, y2, z),
					new Vector3(x2, y2, z),
					new Vector3(x2, y, z),
					new Vector3(x, y, z)
				};
			}
		}
		CleanupAnimation();
		_sprite.Animation = ssAnime;
		if (mBaseColor != Color.white)
		{
			SetColor(mBaseColor);
		}
		_sprite.Play();
		_sprite.PlayCount = mPlayCount;
		_sprite.AnimationFinished = mAnimationFinished;
		return true;
	}

	protected virtual void DoUpdate()
	{
	}

	public void SetPosition(Vector3 a_pos)
	{
		base.transform.localPosition = a_pos;
	}

	public void SetBaseColor(Color col)
	{
		mBaseColor = col;
	}

	public void SetColor(Color col)
	{
		if (!(_sprite == null) && _sprite._colors != null)
		{
			mBaseColor = col;
			for (int i = 0; i < _sprite._colors.Length; i++)
			{
				_sprite._colors[i] = col;
			}
			_sprite._colorChanged = true;
		}
	}

	public Color GetColor()
	{
		return mBaseColor;
	}
}
