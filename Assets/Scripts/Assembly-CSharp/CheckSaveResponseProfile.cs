using System;

public class CheckSaveResponseProfile : ICloneable
{
	[MiniJSONAlias("is_save")]
	public int is_save { get; set; }

	[MiniJSONAlias("server_time")]
	public long server_time { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public CheckSaveResponseProfile Clone()
	{
		return MemberwiseClone() as CheckSaveResponseProfile;
	}
}
