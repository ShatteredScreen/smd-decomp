using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SsPartRes
{
	[HideInInspector]
	public SsAnimation AnimeRes;

	public string Name;

	public SsPartType Type;

	public SsRect PicArea;

	public int OriginX;

	public int OriginY;

	public int MyId;

	public int ParentId;

	public int ChildNum;

	public int SrcObjId;

	public SsSourceObjectType SrcObjType;

	public SsAlphaBlendOperation AlphaBlendType;

	public SsInheritanceState InheritState;

	public List<SsFloatKeyFrame> PosXKeys;

	public List<SsFloatKeyFrame> PosYKeys;

	public List<SsFloatKeyFrame> AngleKeys;

	public List<SsFloatKeyFrame> ScaleXKeys;

	public List<SsFloatKeyFrame> ScaleYKeys;

	public List<SsFloatKeyFrame> TransKeys;

	public List<SsIntKeyFrame> PrioKeys;

	public List<SsBoolKeyFrame> FlipHKeys;

	public List<SsBoolKeyFrame> FlipVKeys;

	public List<SsBoolKeyFrame> HideKeys;

	public List<SsColorBlendKeyFrame> PartsColKeys;

	public List<SsPaletteKeyFrame> PartsPalKeys;

	public List<SsVertexKeyFrame> VertexKeys;

	public List<SsUserDataKeyFrame> UserKeys;

	public List<SsSoundKeyFrame> SoundKeys;

	public List<SsIntKeyFrame> ImageOffsetXKeys;

	public List<SsIntKeyFrame> ImageOffsetYKeys;

	public List<SsIntKeyFrame> ImageOffsetWKeys;

	public List<SsIntKeyFrame> ImageOffsetHKeys;

	public List<SsIntKeyFrame> OriginOffsetXKeys;

	public List<SsIntKeyFrame> OriginOffsetYKeys;

	public SsFloatAttrValue[] PosXValues;

	public SsFloatAttrValue[] PosYValues;

	public SsFloatAttrValue[] AngleValues;

	public SsFloatAttrValue[] ScaleXValues;

	public SsFloatAttrValue[] ScaleYValues;

	public SsFloatAttrValue[] TransValues;

	public SsIntAttrValue[] PrioValues;

	public SsBoolAttrValue[] FlipHValues;

	public SsBoolAttrValue[] FlipVValues;

	public SsBoolAttrValue[] HideValues;

	public SsColorBlendAttrValue[] PartsColValues;

	public SsPaletteAttrValue[] PartsPalValues;

	public SsVertexAttrValue[] VertexValues;

	public SsUserDataAttrValue[] UserValues;

	public SsSoundAttrValue[] SoundValues;

	public SsIntAttrValue[] ImageOffsetXValues;

	public SsIntAttrValue[] ImageOffsetYValues;

	public SsIntAttrValue[] ImageOffsetWValues;

	public SsIntAttrValue[] ImageOffsetHValues;

	public SsIntAttrValue[] OriginOffsetXValues;

	public SsIntAttrValue[] OriginOffsetYValues;

	[SerializeField]
	private SsKeyAttrFlags _hasAttrFlags;

	public bool HasTrancparency;

	public int FrameNum;

	public SsImageFile imageFile;

	public Vector2[] UVs;

	public Vector3[] OrgVertices;

	public bool IsRoot
	{
		get
		{
			return MyId == 0;
		}
	}

	public bool HasParent
	{
		get
		{
			return ParentId >= 0;
		}
	}

	public SsPartRes()
	{
		Name = null;
		PicArea = new SsRect();
		InheritState = new SsInheritanceState();
		PosXKeys = new List<SsFloatKeyFrame>();
		PosYKeys = new List<SsFloatKeyFrame>();
		AngleKeys = new List<SsFloatKeyFrame>();
		ScaleXKeys = new List<SsFloatKeyFrame>();
		ScaleYKeys = new List<SsFloatKeyFrame>();
		TransKeys = new List<SsFloatKeyFrame>();
		PrioKeys = new List<SsIntKeyFrame>();
		FlipHKeys = new List<SsBoolKeyFrame>();
		FlipVKeys = new List<SsBoolKeyFrame>();
		HideKeys = new List<SsBoolKeyFrame>();
		PartsColKeys = new List<SsColorBlendKeyFrame>();
		PartsPalKeys = new List<SsPaletteKeyFrame>();
		VertexKeys = new List<SsVertexKeyFrame>();
		UserKeys = new List<SsUserDataKeyFrame>();
		SoundKeys = new List<SsSoundKeyFrame>();
		ImageOffsetXKeys = new List<SsIntKeyFrame>();
		ImageOffsetYKeys = new List<SsIntKeyFrame>();
		ImageOffsetWKeys = new List<SsIntKeyFrame>();
		ImageOffsetHKeys = new List<SsIntKeyFrame>();
		OriginOffsetXKeys = new List<SsIntKeyFrame>();
		OriginOffsetYKeys = new List<SsIntKeyFrame>();
	}

	public SsInheritanceParam InheritParam(SsKeyAttr attr)
	{
		return InheritState.Values[(int)attr];
	}

	public bool Inherits(SsKeyAttr attr)
	{
		return InheritParam(attr).Use;
	}

	public float InheritRate(SsKeyAttr attr)
	{
		SsInheritanceParam ssInheritanceParam = InheritParam(attr);
		if (!ssInheritanceParam.Use)
		{
			return 0f;
		}
		return ssInheritanceParam.Rate;
	}

	public bool HasAttrFlags(SsKeyAttrFlags masks)
	{
		return (_hasAttrFlags & masks) != 0;
	}

	public Vector2[] CalcUVs(int ofsX, int ofsY, int ofsW, int ofsH)
	{
		if (Type != 0)
		{
			return null;
		}
		float num = imageFile.width;
		float num2 = imageFile.height;
		if (UVs == null)
		{
			UVs = new Vector2[4]
			{
				default(Vector2),
				default(Vector2),
				default(Vector2),
				default(Vector2)
			};
		}
		Rect rect = (Rect)PicArea;
		rect.xMin += ofsX;
		rect.yMin += ofsY;
		rect.xMax += ofsX + ofsW;
		rect.yMax += ofsY + ofsH;
		UVs[0].x = (UVs[3].x = rect.xMin / num);
		UVs[0].y = (UVs[1].y = 1f - rect.yMin / num2);
		UVs[1].x = (UVs[2].x = rect.xMax / num);
		UVs[2].y = (UVs[3].y = 1f - rect.yMax / num2);
		return UVs;
	}

	public Vector3[] GetVertices(Vector2 size)
	{
		Vector2 vector = new Vector2(OriginX, OriginY);
		Vector3[] array = new Vector3[4]
		{
			new Vector3(0f - vector.x, vector.y, 0f),
			new Vector3(size.x - vector.x, vector.y, 0f),
			new Vector3(size.x - vector.x, 0f - (size.y - vector.y), 0f),
			new Vector3(0f - vector.x, 0f - (size.y - vector.y), 0f)
		};
		for (int i = 0; i < 4; i++)
		{
			array[i] *= AnimeRes.ScaleFactor;
		}
		return array;
	}

	public void CreateAttrValues(int frameNum)
	{
		if (frameNum <= 0)
		{
			UnityEngine.Debug.LogWarning("No keys to precalculate.");
			return;
		}
		FrameNum = frameNum;
		for (int i = 0; i < 21; i++)
		{
			SsKeyAttr ssKeyAttr = (SsKeyAttr)i;
			int keyNumOf = GetKeyNumOf(ssKeyAttr);
			if (keyNumOf <= 0)
			{
				continue;
			}
			int refKeyIndex = 0;
			SsKeyFrameInterface ssKeyFrameInterface = GetKey(ssKeyAttr, 0);
			int num = 0;
			SsKeyFrameInterface ssKeyFrameInterface2 = null;
			int num2 = 1;
			if (keyNumOf > 1)
			{
				ssKeyFrameInterface2 = GetKey(ssKeyAttr, 1);
				num = 1;
				for (int j = 1; j < keyNumOf; j++)
				{
					SsKeyFrameInterface key = GetKey(ssKeyAttr, j);
					if (!key.EqualsValue(ssKeyFrameInterface))
					{
						num2 = frameNum;
						break;
					}
				}
			}
			else if (keyNumOf == 1 && ssKeyAttr == SsKeyAttr.Hide)
			{
				SsBoolKeyFrame ssBoolKeyFrame = (SsBoolKeyFrame)ssKeyFrameInterface;
				if (!ssBoolKeyFrame.Value)
				{
					num2 = frameNum;
				}
			}
			SsKeyAttrDesc byId = SsKeyAttrDescManager.GetById(ssKeyAttr);
			switch (ssKeyAttr)
			{
			case SsKeyAttr.PosX:
				PosXValues = new SsFloatAttrValue[num2];
				break;
			case SsKeyAttr.PosY:
				PosYValues = new SsFloatAttrValue[num2];
				break;
			case SsKeyAttr.Angle:
				AngleValues = new SsFloatAttrValue[num2];
				break;
			case SsKeyAttr.ScaleX:
				ScaleXValues = new SsFloatAttrValue[num2];
				break;
			case SsKeyAttr.ScaleY:
				ScaleYValues = new SsFloatAttrValue[num2];
				break;
			case SsKeyAttr.Trans:
				TransValues = new SsFloatAttrValue[num2];
				break;
			case SsKeyAttr.Prio:
				PrioValues = new SsIntAttrValue[num2];
				break;
			case SsKeyAttr.FlipH:
				FlipHValues = new SsBoolAttrValue[num2];
				break;
			case SsKeyAttr.FlipV:
				FlipVValues = new SsBoolAttrValue[num2];
				break;
			case SsKeyAttr.Hide:
				HideValues = new SsBoolAttrValue[num2];
				break;
			case SsKeyAttr.PartsCol:
				PartsColValues = new SsColorBlendAttrValue[num2];
				break;
			case SsKeyAttr.PartsPal:
				PartsPalValues = new SsPaletteAttrValue[num2];
				break;
			case SsKeyAttr.Vertex:
				VertexValues = new SsVertexAttrValue[num2];
				break;
			case SsKeyAttr.User:
				UserValues = new SsUserDataAttrValue[num2];
				break;
			case SsKeyAttr.Sound:
				SoundValues = new SsSoundAttrValue[num2];
				break;
			case SsKeyAttr.ImageOffsetX:
				ImageOffsetXValues = new SsIntAttrValue[num2];
				break;
			case SsKeyAttr.ImageOffsetY:
				ImageOffsetYValues = new SsIntAttrValue[num2];
				break;
			case SsKeyAttr.ImageOffsetW:
				ImageOffsetWValues = new SsIntAttrValue[num2];
				break;
			case SsKeyAttr.ImageOffsetH:
				ImageOffsetHValues = new SsIntAttrValue[num2];
				break;
			case SsKeyAttr.OriginOffsetX:
				OriginOffsetXValues = new SsIntAttrValue[num2];
				break;
			case SsKeyAttr.OriginOffsetY:
				OriginOffsetYValues = new SsIntAttrValue[num2];
				break;
			}
			_hasAttrFlags |= (SsKeyAttrFlags)(1 << i);
			for (int k = 0; k < num2; k++)
			{
				if (ssKeyFrameInterface2 != null && k >= ssKeyFrameInterface2.Time)
				{
					refKeyIndex = num;
					ssKeyFrameInterface = ssKeyFrameInterface2;
					num++;
					object obj;
					if (num < keyNumOf)
					{
						SsKeyFrameInterface key2 = GetKey(ssKeyAttr, num);
						obj = key2;
					}
					else
					{
						obj = null;
					}
					ssKeyFrameInterface2 = (SsKeyFrameInterface)obj;
				}
				SsAttrValue ssAttrValue = null;
				SsBoolAttrValue ssBoolAttrValue = null;
				SsIntAttrValue ssIntAttrValue = null;
				SsFloatAttrValue ssFloatAttrValue = null;
				SsPaletteAttrValue ssPaletteAttrValue = null;
				SsColorBlendAttrValue ssColorBlendAttrValue = null;
				SsVertexAttrValue ssVertexAttrValue = null;
				SsUserDataAttrValue ssUserDataAttrValue = null;
				SsSoundAttrValue ssSoundAttrValue = null;
				switch (byId.ValueType)
				{
				case SsKeyValueType.Data:
					switch (byId.CastType)
					{
					default:
						ssAttrValue = (ssIntAttrValue = new SsIntAttrValue());
						break;
					case SsKeyCastType.Float:
					case SsKeyCastType.Degree:
						ssAttrValue = (ssFloatAttrValue = new SsFloatAttrValue());
						break;
					}
					break;
				case SsKeyValueType.Param:
					ssAttrValue = (ssBoolAttrValue = new SsBoolAttrValue());
					break;
				case SsKeyValueType.Palette:
					ssAttrValue = (ssPaletteAttrValue = new SsPaletteAttrValue());
					break;
				case SsKeyValueType.Color:
					ssAttrValue = (ssColorBlendAttrValue = new SsColorBlendAttrValue());
					break;
				case SsKeyValueType.Vertex:
					ssAttrValue = (ssVertexAttrValue = new SsVertexAttrValue());
					break;
				case SsKeyValueType.User:
					ssAttrValue = (ssUserDataAttrValue = new SsUserDataAttrValue());
					break;
				case SsKeyValueType.Sound:
					ssAttrValue = (ssSoundAttrValue = new SsSoundAttrValue());
					break;
				}
				if (byId.NeedsInterpolatable && ssKeyFrameInterface2 != null && k >= ssKeyFrameInterface.Time)
				{
					bool flag = true;
					if (byId.Attr == SsKeyAttr.PartsCol)
					{
						SsColorBlendKeyValue ssColorBlendKeyValue = (SsColorBlendKeyValue)ssKeyFrameInterface.ObjectValue;
						SsColorBlendKeyValue ssColorBlendKeyValue2 = (SsColorBlendKeyValue)ssKeyFrameInterface2.ObjectValue;
						if (ssColorBlendKeyValue.Target == SsColorBlendTarget.None && ssColorBlendKeyValue2.Target == SsColorBlendTarget.None)
						{
							ssAttrValue.RefKeyIndex = refKeyIndex;
							flag = false;
						}
					}
					if (flag)
					{
						object obj2 = ((k != ssKeyFrameInterface.Time) ? SsInterpolation.InterpolateKeyValue(ssKeyFrameInterface, ssKeyFrameInterface2, k) : ssKeyFrameInterface.ObjectValue);
						try
						{
							if (ssBoolAttrValue)
							{
								ssBoolAttrValue.Value = Convert.ToBoolean(obj2);
							}
							else if (ssIntAttrValue)
							{
								ssIntAttrValue.Value = Convert.ToInt32(obj2);
							}
							else if (ssFloatAttrValue)
							{
								ssFloatAttrValue.Value = Convert.ToSingle(obj2);
							}
							else
							{
								ssAttrValue.SetValue(obj2);
							}
						}
						catch
						{
							UnityEngine.Debug.LogError("[INTERNAL] failed to unbox: " + obj2);
						}
						if (byId.Attr == SsKeyAttr.PartsCol)
						{
							SsColorBlendKeyValue ssColorBlendKeyValue3 = (SsColorBlendKeyValue)ssKeyFrameInterface.ObjectValue;
							SsColorBlendKeyValue ssColorBlendKeyValue4 = (SsColorBlendKeyValue)ssKeyFrameInterface2.ObjectValue;
							if (ssColorBlendKeyValue3.Target == SsColorBlendTarget.Vertex || ssColorBlendKeyValue4.Target == SsColorBlendTarget.Vertex)
							{
								SsColorBlendKeyValue value = ssColorBlendAttrValue.Value;
								value.Target = SsColorBlendTarget.Vertex;
								value.Operation = ssColorBlendKeyValue4.Operation;
							}
						}
					}
				}
				else
				{
					ssAttrValue.RefKeyIndex = refKeyIndex;
				}
				switch (ssKeyAttr)
				{
				case SsKeyAttr.PosX:
					PosXValues[k] = ssFloatAttrValue;
					break;
				case SsKeyAttr.PosY:
					PosYValues[k] = ssFloatAttrValue;
					break;
				case SsKeyAttr.Angle:
					AngleValues[k] = ssFloatAttrValue;
					break;
				case SsKeyAttr.ScaleX:
					ScaleXValues[k] = ssFloatAttrValue;
					break;
				case SsKeyAttr.ScaleY:
					ScaleYValues[k] = ssFloatAttrValue;
					break;
				case SsKeyAttr.Trans:
					TransValues[k] = ssFloatAttrValue;
					break;
				case SsKeyAttr.Prio:
					PrioValues[k] = ssIntAttrValue;
					break;
				case SsKeyAttr.FlipH:
					FlipHValues[k] = ssBoolAttrValue;
					break;
				case SsKeyAttr.FlipV:
					FlipVValues[k] = ssBoolAttrValue;
					break;
				case SsKeyAttr.Hide:
					HideValues[k] = ssBoolAttrValue;
					break;
				case SsKeyAttr.PartsCol:
					PartsColValues[k] = ssColorBlendAttrValue;
					break;
				case SsKeyAttr.PartsPal:
					PartsPalValues[k] = ssPaletteAttrValue;
					break;
				case SsKeyAttr.Vertex:
					VertexValues[k] = ssVertexAttrValue;
					break;
				case SsKeyAttr.User:
					UserValues[k] = ssUserDataAttrValue;
					break;
				case SsKeyAttr.Sound:
					SoundValues[k] = ssSoundAttrValue;
					break;
				case SsKeyAttr.ImageOffsetX:
					ImageOffsetXValues[k] = ssIntAttrValue;
					break;
				case SsKeyAttr.ImageOffsetY:
					ImageOffsetYValues[k] = ssIntAttrValue;
					break;
				case SsKeyAttr.ImageOffsetW:
					ImageOffsetWValues[k] = ssIntAttrValue;
					break;
				case SsKeyAttr.ImageOffsetH:
					ImageOffsetHValues[k] = ssIntAttrValue;
					break;
				case SsKeyAttr.OriginOffsetX:
					OriginOffsetXValues[k] = ssIntAttrValue;
					break;
				case SsKeyAttr.OriginOffsetY:
					OriginOffsetYValues[k] = ssIntAttrValue;
					break;
				}
			}
		}
		HasTrancparency = false;
		if (!HasAttrFlags(SsKeyAttrFlags.Trans))
		{
			return;
		}
		foreach (SsFloatKeyFrame transKey in TransKeys)
		{
			if (transKey.Value < 1f)
			{
				HasTrancparency = true;
				break;
			}
		}
	}

	public bool HasColorBlendKey()
	{
		return HasAttrFlags(SsKeyAttrFlags.PartsCol);
	}

	public void AddKeyFrame(SsKeyAttr attr, SsKeyFrameInterface key)
	{
		switch (attr)
		{
		case SsKeyAttr.PosX:
			PosXKeys.Add((SsFloatKeyFrame)key);
			break;
		case SsKeyAttr.PosY:
			PosYKeys.Add((SsFloatKeyFrame)key);
			break;
		case SsKeyAttr.Angle:
			AngleKeys.Add((SsFloatKeyFrame)key);
			break;
		case SsKeyAttr.ScaleX:
			ScaleXKeys.Add((SsFloatKeyFrame)key);
			break;
		case SsKeyAttr.ScaleY:
			ScaleYKeys.Add((SsFloatKeyFrame)key);
			break;
		case SsKeyAttr.Trans:
			TransKeys.Add((SsFloatKeyFrame)key);
			break;
		case SsKeyAttr.Prio:
			PrioKeys.Add((SsIntKeyFrame)key);
			break;
		case SsKeyAttr.FlipH:
			FlipHKeys.Add((SsBoolKeyFrame)key);
			break;
		case SsKeyAttr.FlipV:
			FlipVKeys.Add((SsBoolKeyFrame)key);
			break;
		case SsKeyAttr.Hide:
			HideKeys.Add((SsBoolKeyFrame)key);
			break;
		case SsKeyAttr.PartsCol:
			PartsColKeys.Add((SsColorBlendKeyFrame)key);
			break;
		case SsKeyAttr.PartsPal:
			PartsPalKeys.Add((SsPaletteKeyFrame)key);
			break;
		case SsKeyAttr.Vertex:
			VertexKeys.Add((SsVertexKeyFrame)key);
			break;
		case SsKeyAttr.User:
			UserKeys.Add((SsUserDataKeyFrame)key);
			break;
		case SsKeyAttr.Sound:
			SoundKeys.Add((SsSoundKeyFrame)key);
			break;
		case SsKeyAttr.ImageOffsetX:
			ImageOffsetXKeys.Add((SsIntKeyFrame)key);
			break;
		case SsKeyAttr.ImageOffsetY:
			ImageOffsetYKeys.Add((SsIntKeyFrame)key);
			break;
		case SsKeyAttr.ImageOffsetW:
			ImageOffsetWKeys.Add((SsIntKeyFrame)key);
			break;
		case SsKeyAttr.ImageOffsetH:
			ImageOffsetHKeys.Add((SsIntKeyFrame)key);
			break;
		case SsKeyAttr.OriginOffsetX:
			OriginOffsetXKeys.Add((SsIntKeyFrame)key);
			break;
		case SsKeyAttr.OriginOffsetY:
			OriginOffsetYKeys.Add((SsIntKeyFrame)key);
			break;
		default:
			UnityEngine.Debug.LogError("Unknown attribute: " + attr);
			break;
		}
	}

	public SsKeyFrameInterface GetKey(SsKeyAttr attr, int index)
	{
		switch (attr)
		{
		case SsKeyAttr.PosX:
			return PosXKeys[index];
		case SsKeyAttr.PosY:
			return PosYKeys[index];
		case SsKeyAttr.Angle:
			return AngleKeys[index];
		case SsKeyAttr.ScaleX:
			return ScaleXKeys[index];
		case SsKeyAttr.ScaleY:
			return ScaleYKeys[index];
		case SsKeyAttr.Trans:
			return TransKeys[index];
		case SsKeyAttr.Prio:
			return PrioKeys[index];
		case SsKeyAttr.FlipH:
			return FlipHKeys[index];
		case SsKeyAttr.FlipV:
			return FlipVKeys[index];
		case SsKeyAttr.Hide:
			return HideKeys[index];
		case SsKeyAttr.PartsCol:
			return PartsColKeys[index];
		case SsKeyAttr.PartsPal:
			return PartsPalKeys[index];
		case SsKeyAttr.Vertex:
			return VertexKeys[index];
		case SsKeyAttr.User:
			return UserKeys[index];
		case SsKeyAttr.Sound:
			return SoundKeys[index];
		case SsKeyAttr.ImageOffsetX:
			return ImageOffsetXKeys[index];
		case SsKeyAttr.ImageOffsetY:
			return ImageOffsetYKeys[index];
		case SsKeyAttr.ImageOffsetW:
			return ImageOffsetWKeys[index];
		case SsKeyAttr.ImageOffsetH:
			return ImageOffsetHKeys[index];
		case SsKeyAttr.OriginOffsetX:
			return OriginOffsetXKeys[index];
		case SsKeyAttr.OriginOffsetY:
			return OriginOffsetYKeys[index];
		default:
			UnityEngine.Debug.LogError("Unknown attribute: " + attr);
			return null;
		}
	}

	public int GetKeyNumOf(SsKeyAttr attr)
	{
		switch (attr)
		{
		case SsKeyAttr.PosX:
			return PosXKeys.Count;
		case SsKeyAttr.PosY:
			return PosYKeys.Count;
		case SsKeyAttr.Angle:
			return AngleKeys.Count;
		case SsKeyAttr.ScaleX:
			return ScaleXKeys.Count;
		case SsKeyAttr.ScaleY:
			return ScaleYKeys.Count;
		case SsKeyAttr.Trans:
			return TransKeys.Count;
		case SsKeyAttr.Prio:
			return PrioKeys.Count;
		case SsKeyAttr.FlipH:
			return FlipHKeys.Count;
		case SsKeyAttr.FlipV:
			return FlipVKeys.Count;
		case SsKeyAttr.Hide:
			return HideKeys.Count;
		case SsKeyAttr.PartsCol:
			return PartsColKeys.Count;
		case SsKeyAttr.PartsPal:
			return PartsPalKeys.Count;
		case SsKeyAttr.Vertex:
			return VertexKeys.Count;
		case SsKeyAttr.User:
			return UserKeys.Count;
		case SsKeyAttr.Sound:
			return SoundKeys.Count;
		case SsKeyAttr.ImageOffsetX:
			return ImageOffsetXKeys.Count;
		case SsKeyAttr.ImageOffsetY:
			return ImageOffsetYKeys.Count;
		case SsKeyAttr.ImageOffsetW:
			return ImageOffsetWKeys.Count;
		case SsKeyAttr.ImageOffsetH:
			return ImageOffsetHKeys.Count;
		case SsKeyAttr.OriginOffsetX:
			return OriginOffsetXKeys.Count;
		case SsKeyAttr.OriginOffsetY:
			return OriginOffsetYKeys.Count;
		default:
			UnityEngine.Debug.LogError("Unknown attribute: " + attr);
			return 0;
		}
	}

	public SsAttrValue[] GetAttrValues(SsKeyAttr attr)
	{
		switch (attr)
		{
		case SsKeyAttr.PosX:
			return PosXValues;
		case SsKeyAttr.PosY:
			return PosYValues;
		case SsKeyAttr.Angle:
			return AngleValues;
		case SsKeyAttr.ScaleX:
			return ScaleXValues;
		case SsKeyAttr.ScaleY:
			return ScaleYValues;
		case SsKeyAttr.Trans:
			return TransValues;
		case SsKeyAttr.Prio:
			return PrioValues;
		case SsKeyAttr.FlipH:
			return FlipHValues;
		case SsKeyAttr.FlipV:
			return FlipVValues;
		case SsKeyAttr.Hide:
			return HideValues;
		case SsKeyAttr.PartsCol:
			return PartsColValues;
		case SsKeyAttr.PartsPal:
			return PartsPalValues;
		case SsKeyAttr.Vertex:
			return VertexValues;
		case SsKeyAttr.User:
			return UserValues;
		case SsKeyAttr.Sound:
			return SoundValues;
		case SsKeyAttr.ImageOffsetX:
			return ImageOffsetXValues;
		case SsKeyAttr.ImageOffsetY:
			return ImageOffsetYValues;
		case SsKeyAttr.ImageOffsetW:
			return ImageOffsetWValues;
		case SsKeyAttr.ImageOffsetH:
			return ImageOffsetHValues;
		case SsKeyAttr.OriginOffsetX:
			return OriginOffsetXValues;
		case SsKeyAttr.OriginOffsetY:
			return OriginOffsetYValues;
		default:
			UnityEngine.Debug.LogError("Unknown attribute: " + attr);
			return null;
		}
	}

	public T AttrValue<T, AttrType, KeyType>(SsKeyAttr attr, int frame, List<KeyType> keyList) where AttrType : SsAttrValueBase<T, KeyType> where KeyType : SsKeyFrameBase<T>
	{
		SsAttrValue[] attrValues = GetAttrValues(attr);
		if (attrValues.Length == 1)
		{
			frame = 0;
		}
		SsAttrValue ssAttrValue = attrValues[frame];
		if (ssAttrValue.HasValue)
		{
			AttrType val = (AttrType)ssAttrValue;
			return val.Value;
		}
		KeyType val2 = keyList[ssAttrValue.RefKeyIndex];
		return val2.Value;
	}

	public float PosX(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.PosX))
		{
			return 0f;
		}
		return AttrValue<float, SsFloatAttrValue, SsFloatKeyFrame>(SsKeyAttr.PosX, frame, PosXKeys);
	}

	public float PosY(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.PosY))
		{
			return 0f;
		}
		return AttrValue<float, SsFloatAttrValue, SsFloatKeyFrame>(SsKeyAttr.PosY, frame, PosYKeys);
	}

	public float Angle(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.Angle))
		{
			return 0f;
		}
		return AttrValue<float, SsFloatAttrValue, SsFloatKeyFrame>(SsKeyAttr.Angle, frame, AngleKeys);
	}

	public float ScaleX(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.ScaleX))
		{
			return 1f;
		}
		return AttrValue<float, SsFloatAttrValue, SsFloatKeyFrame>(SsKeyAttr.ScaleX, frame, ScaleXKeys);
	}

	public float ScaleY(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.ScaleY))
		{
			return 1f;
		}
		return AttrValue<float, SsFloatAttrValue, SsFloatKeyFrame>(SsKeyAttr.ScaleY, frame, ScaleYKeys);
	}

	public float Trans(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.Trans))
		{
			return 1f;
		}
		return AttrValue<float, SsFloatAttrValue, SsFloatKeyFrame>(SsKeyAttr.Trans, frame, TransKeys);
	}

	public float Prio(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.Prio))
		{
			return 0f;
		}
		return AttrValue<int, SsIntAttrValue, SsIntKeyFrame>(SsKeyAttr.Prio, frame, PrioKeys);
	}

	public bool FlipH(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.FlipH))
		{
			return false;
		}
		return AttrValue<bool, SsBoolAttrValue, SsBoolKeyFrame>(SsKeyAttr.FlipH, frame, FlipHKeys);
	}

	public bool FlipV(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.FlipV))
		{
			return false;
		}
		return AttrValue<bool, SsBoolAttrValue, SsBoolKeyFrame>(SsKeyAttr.FlipV, frame, FlipVKeys);
	}

	public bool Hide(int frame)
	{
		if (IsBeforeFirstKey(frame))
		{
			return true;
		}
		return AttrValue<bool, SsBoolAttrValue, SsBoolKeyFrame>(SsKeyAttr.Hide, frame, HideKeys);
	}

	public bool IsBeforeFirstKey(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.Hide))
		{
			return true;
		}
		return frame < HideKeys[0].Time;
	}

	public SsColorBlendKeyValue PartsCol(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.PartsCol))
		{
			return null;
		}
		if (frame < GetKey(SsKeyAttr.PartsCol, 0).Time)
		{
			return null;
		}
		return AttrValue<SsColorBlendKeyValue, SsColorBlendAttrValue, SsColorBlendKeyFrame>(SsKeyAttr.PartsCol, frame, PartsColKeys);
	}

	public SsPaletteKeyValue PartsPal(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.PartsPal))
		{
			return null;
		}
		return AttrValue<SsPaletteKeyValue, SsPaletteAttrValue, SsPaletteKeyFrame>(SsKeyAttr.PartsPal, frame, PartsPalKeys);
	}

	public SsVertexKeyValue Vertex(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.Vertex))
		{
			return null;
		}
		return AttrValue<SsVertexKeyValue, SsVertexAttrValue, SsVertexKeyFrame>(SsKeyAttr.Vertex, frame, VertexKeys);
	}

	public SsUserDataKeyValue User(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.User))
		{
			return null;
		}
		return AttrValue<SsUserDataKeyValue, SsUserDataAttrValue, SsUserDataKeyFrame>(SsKeyAttr.User, frame, UserKeys);
	}

	public SsSoundKeyValue Sound(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.Sound))
		{
			return null;
		}
		return AttrValue<SsSoundKeyValue, SsSoundAttrValue, SsSoundKeyFrame>(SsKeyAttr.Sound, frame, SoundKeys);
	}

	public int ImageOffsetX(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.ImageOffsetX))
		{
			return 0;
		}
		return AttrValue<int, SsIntAttrValue, SsIntKeyFrame>(SsKeyAttr.ImageOffsetX, frame, ImageOffsetXKeys);
	}

	public int ImageOffsetY(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.ImageOffsetY))
		{
			return 0;
		}
		return AttrValue<int, SsIntAttrValue, SsIntKeyFrame>(SsKeyAttr.ImageOffsetY, frame, ImageOffsetYKeys);
	}

	public int ImageOffsetW(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.ImageOffsetW))
		{
			return 0;
		}
		return AttrValue<int, SsIntAttrValue, SsIntKeyFrame>(SsKeyAttr.ImageOffsetW, frame, ImageOffsetWKeys);
	}

	public int ImageOffsetH(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.ImageOffsetH))
		{
			return 0;
		}
		return AttrValue<int, SsIntAttrValue, SsIntKeyFrame>(SsKeyAttr.ImageOffsetH, frame, ImageOffsetHKeys);
	}

	public int OriginOffsetX(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.OriginOffsetX))
		{
			return 0;
		}
		return AttrValue<int, SsIntAttrValue, SsIntKeyFrame>(SsKeyAttr.OriginOffsetX, frame, OriginOffsetXKeys);
	}

	public int OriginOffsetY(int frame)
	{
		if (!HasAttrFlags(SsKeyAttrFlags.OriginOffsetY))
		{
			return 0;
		}
		return AttrValue<int, SsIntAttrValue, SsIntKeyFrame>(SsKeyAttr.OriginOffsetY, frame, OriginOffsetYKeys);
	}

	public override string ToString()
	{
		string text = string.Format("\nName:\t\t{0}\nType:\t\t{1}\nPicArea:\t{2}\nOriginX:\t{3}\nOriginY:\t{4}\nMyID:\t\t{5}\nParentID:\t{6}\nChildNum:\t{7}\nSrcObjId:\t{8}\nSrcObjType:\t{9}\nBlendType:\t{10}\n", Name, Type, PicArea, OriginX, OriginY, MyId, ParentId, ChildNum, SrcObjId, SrcObjType, AlphaBlendType);
		text = text + "InheritState:\n" + InheritState;
		return text + "\n";
	}
}
