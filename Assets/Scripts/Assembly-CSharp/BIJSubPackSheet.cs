using System;
using System.Collections.Specialized;

public class BIJSubPackSheet
{
	private StringCollection mSubPackNames;

	public int CountPerPack;

	public int Count
	{
		get
		{
			return (mSubPackNames != null) ? mSubPackNames.Count : 0;
		}
	}

	public int this[string name]
	{
		get
		{
			int num = -1;
			if (mSubPackNames != null)
			{
				num = mSubPackNames.IndexOf(name);
			}
			if (num < 0 || num >= mSubPackNames.Count)
			{
				throw new IndexOutOfRangeException();
			}
			return num;
		}
	}

	public BIJSubPackSheet(string name)
	{
		CountPerPack = 0;
		mSubPackNames = null;
		using (BIJResourceBinaryReader bIJResourceBinaryReader = new BIJResourceBinaryReader(name))
		{
			CountPerPack = bIJResourceBinaryReader.ReadByte();
			mSubPackNames = new StringCollection();
			string text = null;
			while ((text = bIJResourceBinaryReader.ReadUTF()) != null)
			{
				mSubPackNames.Add(text);
			}
		}
	}

	public int IndexOf(string name)
	{
		try
		{
			return this[name];
		}
		catch (IndexOutOfRangeException)
		{
		}
		return -1;
	}
}
