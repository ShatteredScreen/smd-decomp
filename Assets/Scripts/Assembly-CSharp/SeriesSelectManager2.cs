using UnityEngine;

public class SeriesSelectManager2 : SeriesSelectManager
{
	protected int mSelectedButtonIndex = -1;

	public override void OnSeriesSelected(GameObject go)
	{
		if (mState.GetStatus() != STATE.MAIN)
		{
			return;
		}
		mState.Reset(STATE.WAIT, true);
		int selected;
		SeriesSelectItem selectedButton = GetSelectedButton(go, out selected);
		if (selectedButton == null)
		{
			return;
		}
		bool flag = true;
		if (!mGame.mGameProfile.IsSeriesAvailable(selectedButton.Series))
		{
			flag = false;
		}
		if (flag)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mIsStartCurrentSeries[mCurrentSeries] = false;
			mSelectedButtonIndex = selected;
			mCurrentSeries = selected;
			mSelectedSeries = mSelectedButtonIndex;
			int a_depth = mBaseDepth + 6;
			GameObject value;
			if (mButtonItems.TryGetValue(mSelectedButtonIndex, out value))
			{
				mIsStartCurrentSeries[mCurrentSeries] = true;
				StartCoroutine(DisplayCurrentSeries(go, a_depth, mSelectedButtonIndex));
			}
			OnClose();
		}
		else
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
		}
	}

	protected override void FirstSelectProcess()
	{
		mIsStartCurrentSeries[mCurrentSeries] = true;
	}
}
