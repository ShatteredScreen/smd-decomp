using System;
using System.Collections.Generic;

public class AdvQuestStartProfile_Response : ICloneable
{
	[MiniJSONAlias("first_clear_rewards")]
	public List<AdvRewardData> FitstRewardDataList { get; set; }

	[MiniJSONAlias("enemy_drop_rewards")]
	public List<AdvRewardData> EnemyRewardDataList { get; set; }

	[MiniJSONAlias("received_rewards")]
	public List<AdvRewardData> Received_List { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvQuestStartProfile_Response Clone()
	{
		return MemberwiseClone() as AdvQuestStartProfile_Response;
	}
}
