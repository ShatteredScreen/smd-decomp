using System;

public class ConnectLoginBonus : ConnectBase
{
	protected override void InitServerFlg()
	{
		GameMain.mLoginBonusCheckDone = false;
		GameMain.mLoginBonusSucceed = false;
	}

	protected override void ServerFlgSucceeded()
	{
		GameMain.mLoginBonusCheckDone = true;
		GameMain.mLoginBonusSucceed = true;
	}

	protected override void ServerFlgFailed()
	{
		GameMain.mLoginBonusCheckDone = true;
		GameMain.mLoginBonusSucceed = false;
	}

	public override bool IsValidConnectInstance()
	{
		if (GameMain.NO_NETWORK)
		{
			ServerFlgSucceeded();
			mGame.mLoginBonusData = new SMDLoginBonusData();
			return false;
		}
		DateTime tdt = DateTime.Now;
		if (GameMain.ServerTime.HasValue)
		{
			tdt = GameMain.ServerTime.Value;
		}
		if (GameMain.USE_DEBUG_DIALOG && mGame.mDebugUseFakeServerTime && mGame.DebugFakeServerTime.HasValue)
		{
			tdt = mGame.DebugFakeServerTime.Value;
		}
		long num = DateTimeUtil.BetweenDays0(mPlayer.Data.LastLoginBonusTime, tdt);
		if (num <= 0)
		{
			long num2 = DateTimeUtil.BetweenMinutes(mPlayer.Data.LastLoginBonusTime, DateTime.Now);
			long gET_LOGINBONUS_FREQ = GameMain.GET_LOGINBONUS_FREQ;
			if (gET_LOGINBONUS_FREQ > 0 && num2 < gET_LOGINBONUS_FREQ)
			{
				ServerFlgSucceeded();
				mGame.LoadLoginBonusLocalFile();
				return false;
			}
		}
		return true;
	}

	protected override void StateStart()
	{
		mState.Change(STATE.WAIT);
		InitServerFlg();
		string a_request = mConnectGUID;
		string a_guid;
		if (!Server.LoginBonus(a_request, out a_guid))
		{
			ServerFlgFailed();
			SetServerResponse(1, -1);
		}
		else
		{
			mConnectGUID = a_guid;
		}
	}

	protected override void StateConnectFinish()
	{
		mPlayer.Data.LastLoginBonusTime = DateTime.Now;
		if (mGame.mLoginBonusData != null && mGame.mLoginBonusData.HasLoginData)
		{
			mGame.mLoginBonusData.FinishedConnect();
			for (int i = 0; i < mGame.mLoginBonusData.login_bonus.Count; i++)
			{
				GetLoginBonusResponse getLoginBonusResponse = mGame.mLoginBonusData.login_bonus[i];
				int loginDayCount = getLoginBonusResponse.LoginDayCount;
				LoginBonusRewardData loginBonusRewardData = getLoginBonusResponse.RewardList[loginDayCount - 1];
				int cur_event = mPlayer.Data.CurrentEventID;
				if (!Def.IsEventSeries(mPlayer.Data.CurrentSeries))
				{
					cur_event = 0;
				}
				ServerCram.LoginBonusReceive((int)mPlayer.Data.CurrentSeries, cur_event, getLoginBonusResponse.LoginID, getLoginBonusResponse.Category, getLoginBonusResponse.Sheet, loginDayCount, loginBonusRewardData.RewardID, loginBonusRewardData.BonusID, loginBonusRewardData.Category, loginBonusRewardData.SubCategory, loginBonusRewardData.Quantity);
			}
			mGame.mPlayer.Data.LastGetSupportTime = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.LastAdvGetMail = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.mPlayer.Data.LastGetSupportPurchaseTime = DateTimeUtil.UnitLocalTimeEpoch;
		}
		base.StateConnectFinish();
	}

	public override bool SetServerResponse(int a_result, int a_code)
	{
		bool flag = base.SetServerResponse(a_result, a_code);
		if (a_result != 0)
		{
			mPlayer.Data.LastLoginBonusTime = DateTimeUtil.UnitLocalTimeEpoch;
			if (!flag)
			{
				ShowErrorDialog(mErrorDialogStyle);
			}
			return false;
		}
		return false;
	}
}
