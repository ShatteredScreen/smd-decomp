using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using live2d;
using live2d.framework;

public class Live2DInstance : MonoBehaviour
{
	public enum PIVOT
	{
		CENTER = 0,
		TOP = 1,
		TOPRIGHT = 2,
		RIGHT = 3,
		BOTTOMRIGHT = 4,
		BOTTOM = 5,
		BOTTOMLEFT = 6,
		LEFT = 7,
		TOPLEFT = 8
	}

	protected PIVOT mPivot;

	private Vector2 mPivotOffset = Vector2.zero;

	private Transform mTransform;

	private bool mLoadFinished;

	private bool mAutoRelease;

	public float mCanvasHeight;

	public float mCanvasWidth;

	public bool ZOverWrite;

	public float Z;

	public string mL2DKey;

	public ResLive2DAnimation mL2DAnime;

	protected Live2DModelUnity mL2DModel;

	protected Dictionary<string, Live2DMotion> mMotionDict;

	protected Dictionary<string, int> mMotionFadeInDict;

	protected Dictionary<string, L2DExpressionMotion> mExpressionDict;

	protected Dictionary<string, int> mExpressionFadeInDict;

	protected L2DPhysics mL2DPhysics;

	protected MotionQueueManager mMotionManager;

	protected MotionQueueManager mExpressionManager;

	protected EyeBlinkMotion mEyeMotion;

	protected L2DTargetPoint mDragManager;

	protected bool mLipSyncEnable;

	protected float mLipSyncAngle;

	protected float mLipSyncSpeed;

	protected float mLipSyncInterval;

	protected float mLipSyncIntervalScale;

	public bool Loop;

	private string mPlayingMotionKey = string.Empty;

	public int FadeInMSec;

	public int FadeOutMSec;

	public float MotionPlayTimer;

	private Color mInitialColor;

	public PIVOT Pivot
	{
		get
		{
			return mPivot;
		}
		set
		{
			mPivot = value;
			mPivotOffset = GetPivotOffet(value);
		}
	}

	public float Scale
	{
		get
		{
			return mTransform.localScale.x;
		}
		set
		{
			mTransform.localScale = new Vector3(value, value, 1f);
			mPivotOffset = GetPivotOffet(mPivot);
		}
	}

	public Vector3 Position { get; set; }

	public virtual void Awake()
	{
		mTransform = base.gameObject.transform;
	}

	public virtual void Start()
	{
	}

	public virtual void OnDestroy()
	{
		if (mAutoRelease)
		{
			ResourceManager.UnloadLive2DAnimation(mL2DKey);
		}
		if (mMotionDict != null)
		{
			mMotionDict.Clear();
		}
		if (mMotionFadeInDict != null)
		{
			mMotionFadeInDict.Clear();
		}
		if (mExpressionDict != null)
		{
			mExpressionDict.Clear();
		}
		if (mExpressionFadeInDict != null)
		{
			mExpressionFadeInDict.Clear();
		}
		mL2DPhysics = null;
		mEyeMotion = null;
		mDragManager = null;
		if (mMotionManager != null)
		{
			mMotionManager.stopAllMotions();
			mMotionManager = null;
		}
		if (mExpressionManager != null)
		{
			mExpressionManager.stopAllMotions();
			mExpressionManager = null;
		}
		if (mL2DModel != null)
		{
			if (mL2DAnime != null)
			{
				for (int i = 0; i < mL2DAnime.Textures.Length; i++)
				{
					mL2DModel.releaseModelTextureNo(i);
				}
			}
			mL2DModel.releaseModel();
			mL2DModel = null;
		}
		if (mL2DAnime != null)
		{
			mL2DAnime = null;
		}
	}

	public virtual void Update()
	{
		if (Time.deltaTime == 0f)
		{
			return;
		}
		UpdateMatrix();
		if (mMotionManager != null)
		{
			mMotionManager.updateParam(mL2DModel);
		}
		if (mExpressionManager != null)
		{
			mExpressionManager.updateParam(mL2DModel);
		}
		if (mEyeMotion != null)
		{
			mEyeMotion.setParam(mL2DModel);
		}
		if (mLipSyncEnable)
		{
			UpdateLipSync();
		}
		if (mL2DPhysics != null)
		{
			mL2DPhysics.updateParam(mL2DModel);
		}
		if (mDragManager != null)
		{
			mDragManager.update();
			if (mL2DModel != null)
			{
				mL2DModel.setParamFloat("PARAM_ANGLE_X", mDragManager.getX() * 30f);
				mL2DModel.setParamFloat("PARAM_BODY_ANGLE_X", mDragManager.getX() * 10f);
			}
		}
		if (mL2DModel != null)
		{
			mL2DModel.update();
		}
		if (Loop && mMotionManager != null && mMotionManager.isFinished())
		{
			RestartMotion();
		}
		MotionPlayTimer += Time.deltaTime;
	}

	public void ModelDraw()
	{
		if (mL2DModel != null)
		{
			mL2DModel.draw();
		}
	}

	public void Init(string key, Vector3 pos, float scale, PIVOT pivot)
	{
		Init(key, pos, scale, pivot, new Color(1f, 1f, 1f, 1f), false);
	}

	public void Init(string key, Vector3 pos, float scale, PIVOT pivot, Color initialColor, bool _autorelease)
	{
		mLoadFinished = false;
		mL2DKey = key;
		Scale = scale;
		Position = pos;
		Pivot = pivot;
		mInitialColor = initialColor;
		mAutoRelease = _autorelease;
		mL2DAnime = ResourceManager.LoadLive2DAnimationAsync(key);
		if (mL2DAnime.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			StartCoroutine(LoadAnimationWait());
			return;
		}
		BuildModel();
		if (mInitialColor != Color.white)
		{
			SetBaseColor(mInitialColor);
		}
		mLoadFinished = true;
	}

	private void BuildModel()
	{
		mL2DModel = mL2DAnime.CreateModelInstance();
		for (int i = 0; i < mL2DAnime.Textures.Length; i++)
		{
			mL2DModel.setTexture(i, mL2DAnime.Textures[i]);
		}
		mMotionDict = new Dictionary<string, Live2DMotion>();
		mMotionFadeInDict = new Dictionary<string, int>();
		foreach (string key in mL2DAnime.MotionDataDict.Keys)
		{
			Live2DMotion live2DMotion = mL2DAnime.CreateMotionInstance(key);
			mMotionDict.Add(key, live2DMotion);
			mMotionFadeInDict.Add(key, live2DMotion.getFadeIn());
			live2DMotion.setFadeOut(0);
		}
		mExpressionDict = new Dictionary<string, L2DExpressionMotion>();
		mExpressionFadeInDict = new Dictionary<string, int>();
		if (mL2DAnime.ExpressionDataDict != null)
		{
			foreach (string key2 in mL2DAnime.ExpressionDataDict.Keys)
			{
				L2DExpressionMotion l2DExpressionMotion = mL2DAnime.CreateExpressionInstance(key2);
				mExpressionDict.Add(key2, l2DExpressionMotion);
				mExpressionFadeInDict.Add(key2, l2DExpressionMotion.getFadeIn());
			}
		}
		mL2DPhysics = mL2DAnime.CreatePhysicsInstance();
		if (mL2DPhysics != null)
		{
			mL2DPhysics.updateParam(mL2DModel);
		}
		if (mL2DModel != null)
		{
			mL2DModel.update();
		}
		mCanvasWidth = mL2DModel.getCanvasWidth();
		mCanvasHeight = mL2DModel.getCanvasHeight();
		mMotionManager = null;
		mEyeMotion = null;
		mExpressionManager = null;
		mDragManager = null;
		mLipSyncEnable = false;
		UpdateMatrix();
	}

	private IEnumerator LoadAnimationWait()
	{
		while (mL2DAnime.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		BuildModel();
		mLoadFinished = true;
	}

	public bool IsLoadFinished()
	{
		return mLoadFinished;
	}

	private float LocalToScreenRatio(float n)
	{
		return n / 568f;
	}

	private Vector2 GetPivotOffet(PIVOT pivot)
	{
		Vector2 zero = Vector2.zero;
		switch (pivot)
		{
		case PIVOT.CENTER:
			zero.x = (0f - mCanvasWidth) / 2f;
			zero.y = mCanvasHeight / 2f;
			break;
		case PIVOT.TOP:
			zero.x = (0f - mCanvasWidth) / 2f;
			zero.y = 0f;
			break;
		case PIVOT.TOPRIGHT:
			zero.x = 0f - mCanvasWidth;
			zero.y = 0f;
			break;
		case PIVOT.RIGHT:
			zero.x = 0f - mCanvasWidth;
			zero.y = mCanvasHeight / 2f;
			break;
		case PIVOT.BOTTOMRIGHT:
			zero.x = 0f - mCanvasWidth;
			zero.y = mCanvasHeight;
			break;
		case PIVOT.BOTTOM:
			zero.x = (0f - mCanvasWidth) / 2f;
			zero.y = mCanvasHeight;
			break;
		case PIVOT.BOTTOMLEFT:
			zero.x = 0f;
			zero.y = mCanvasHeight;
			break;
		case PIVOT.LEFT:
			zero.x = 0f;
			zero.y = mCanvasHeight / 2f;
			break;
		case PIVOT.TOPLEFT:
			zero.x = 0f;
			zero.y = 0f;
			break;
		}
		zero.x *= base.transform.localScale.x;
		zero.y *= base.transform.localScale.y;
		return zero;
	}

	private void UpdateMatrix()
	{
		if (mL2DModel != null)
		{
			float z = Position.z;
			if (ZOverWrite)
			{
				z = Z;
			}
			base.transform.localPosition = new Vector3(Position.x + mPivotOffset.x, Position.y + mPivotOffset.y, z);
			mPivotOffset = GetPivotOffet(mPivot);
			Matrix4x4 matrix4x = Matrix4x4.Ortho(-1f, 1f, 1f, -1f, 0f, 0.1f);
			Matrix4x4 localToWorldMatrix = base.transform.localToWorldMatrix;
			Matrix4x4 matrix = localToWorldMatrix * matrix4x;
			mL2DModel.setMatrix(matrix);
		}
	}

	public void StartMotion(string key, bool loop, int fadeInMsec = -1)
	{
		if (!mMotionDict.ContainsKey(key))
		{
			mMotionManager = null;
			return;
		}
		if (mMotionManager == null)
		{
			mMotionManager = new MotionQueueManager();
		}
		if (fadeInMsec >= 0)
		{
			mMotionDict[key].setFadeIn(fadeInMsec);
		}
		else if (mPlayingMotionKey == key)
		{
			mMotionDict[key].setFadeIn(0);
		}
		else
		{
			mMotionDict[key].setFadeIn(mMotionFadeInDict[key]);
		}
		mMotionManager.startMotion(mMotionDict[key]);
		int motionIndex = mL2DAnime.GetMotionIndex(key);
		if (mL2DAnime.ModelSetting.IsMotionExpandParam(string.Empty, motionIndex, "eyeblink"))
		{
			if (mEyeMotion == null)
			{
				mEyeMotion = new EyeBlinkMotion();
			}
		}
		else if (mEyeMotion != null)
		{
			mEyeMotion = null;
		}
		MotionPlayTimer = 0f;
		mPlayingMotionKey = key;
		Loop = loop;
	}

	public void RestartMotion()
	{
		if (!mMotionDict.ContainsKey(mPlayingMotionKey))
		{
			mMotionManager = null;
			return;
		}
		if (mMotionManager == null)
		{
			mMotionManager = new MotionQueueManager();
		}
		mMotionDict[mPlayingMotionKey].setFadeIn(0);
		mMotionManager.startMotion(mMotionDict[mPlayingMotionKey]);
	}

	public void SetExpression(string key, int fadeInMsec = -1)
	{
		if (!mExpressionDict.ContainsKey(key))
		{
			mExpressionManager = null;
			return;
		}
		if (mExpressionManager == null)
		{
			mExpressionManager = new MotionQueueManager();
		}
		if (fadeInMsec >= 0)
		{
			mExpressionDict[key].setFadeIn(fadeInMsec);
		}
		else
		{
			mExpressionDict[key].setFadeIn(mExpressionFadeInDict[key]);
		}
		mExpressionManager.startMotion(mExpressionDict[key]);
		mExpressionManager.updateParam(mL2DModel);
		int expressionIndex = mL2DAnime.GetExpressionIndex(key);
		if (mL2DAnime.ModelSetting.IsExpressionExpandParam(expressionIndex, "eyeblink"))
		{
			if (mEyeMotion == null)
			{
				mEyeMotion = new EyeBlinkMotion();
			}
		}
		else if (mEyeMotion != null)
		{
			mEyeMotion = null;
		}
	}

	public void StartLipSync(float speedScale = 1f, float intervalScale = 1f)
	{
		mLipSyncEnable = true;
		mLipSyncAngle = 0f;
		mLipSyncSpeed = 1440f * speedScale;
		mLipSyncIntervalScale = intervalScale;
	}

	public void EndLipSync()
	{
		mLipSyncEnable = false;
		if (mL2DModel != null)
		{
			mL2DModel.setParamFloat("PARAM_MOUTH_OPEN_Y", 0f);
		}
	}

	private void UpdateLipSync()
	{
		if (mLipSyncInterval > 0f)
		{
			mLipSyncInterval -= Time.deltaTime;
			return;
		}
		mLipSyncAngle += Time.deltaTime * mLipSyncSpeed;
		if (mLipSyncAngle > 180f)
		{
			mLipSyncAngle = -180f;
			mLipSyncInterval = UnityEngine.Random.RandomRange(-0.4f, 0.5f);
			if (mLipSyncInterval > 0f)
			{
				mLipSyncInterval *= mLipSyncIntervalScale;
			}
		}
		float value = Mathf.Sin(mLipSyncAngle * ((float)Math.PI / 180f));
		if (mL2DModel != null)
		{
			mL2DModel.setParamFloat("PARAM_MOUTH_OPEN_Y", value);
		}
	}

	public void SetTargetPoint(Vector2 targetPoint, bool immediate = false)
	{
		if (mDragManager == null)
		{
			mDragManager = new L2DTargetPoint();
		}
		mDragManager.Set(targetPoint.x, targetPoint.y);
		if (immediate)
		{
			if (targetPoint.x > 0f)
			{
				mDragManager.setX(1f);
			}
			else if (targetPoint.x < 0f)
			{
				mDragManager.setX(-1f);
			}
		}
	}

	public void SetParamFloat(string paramName, float val)
	{
		mL2DModel.setParamFloat(paramName, val);
	}

	public void SetScale(Vector3 a_scale)
	{
		mTransform.localScale = a_scale;
		mPivotOffset = GetPivotOffet(mPivot);
	}

	public void SetBaseColor(Color col)
	{
		if (mL2DModel != null)
		{
			DrawParam drawParam = mL2DModel.getDrawParam();
			drawParam.setBaseColor(col.a, col.r, col.g, col.b);
		}
	}

	public bool IsAnimationFinished()
	{
		if (mMotionManager == null)
		{
			return false;
		}
		return mMotionManager.isFinished();
	}
}
