using System;

public class UserGetResponse : ICloneable
{
	[MiniJSONAlias("nickname")]
	public string NickName { get; set; }

	[MiniJSONAlias("iconid")]
	public int IconID { get; set; }

	[MiniJSONAlias("emoid")]
	public int EmoticonID { get; set; }

	[MiniJSONAlias("iconlvl")]
	public int IconLvl { get; set; }

	[MiniJSONAlias("searchid")]
	public string SearchID { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public UserGetResponse Clone()
	{
		return MemberwiseClone() as UserGetResponse;
	}
}
