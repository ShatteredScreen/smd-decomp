using System.Collections;
using UnityEngine;

public class TrophyDetailPartnerDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		CANCEL = 0,
		BUY = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private Live2DRender mL2DRender;

	private UITexture mTexture;

	private CompanionData mCompanionData;

	private UISprite mShadow;

	private BIJImage atlas;

	private int mChara_index;

	private AccessoryData mData;

	private TrophyItemListItem mItemData;

	public AccessoryData Data
	{
		get
		{
			return mData;
		}
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(AccessoryData a_data, TrophyItemListItem a_item)
	{
		mItemData = a_item;
		mData = a_data;
		mCompanionData = null;
		if (mData != null && mData.AccessoryType == AccessoryData.ACCESSORY_TYPE.PARTNER)
		{
			mChara_index = mData.GetGotIDByNum();
			if (mChara_index != -1 && mChara_index < mGame.mCompanionData.Count)
			{
				mCompanionData = mGame.mCompanionData[mChara_index];
			}
		}
		if (mCompanionData == null)
		{
			BIJLog.E("Companion Data is NULL !!!");
		}
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			if (mShadow != null)
			{
				mShadow.color = Color.clear;
			}
			mState.Update();
			return;
		}
		if (mBaseState.GetStatus() == BASE_STATE.MAIN && mBaseState.IsChanged() && mCompanionData.index != 10)
		{
			mShadow = Util.CreateSprite("Shadow", base.gameObject, atlas);
			Util.SetSpriteInfo(mShadow, "character_foot2", mBaseDepth + 3, new Vector3(0f, -34f, 0f), Vector3.one, false);
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override IEnumerator BuildResource()
	{
		ResLive2DAnimation anim = ResourceManager.LoadLive2DAnimationAsync(mCompanionData.GetModelKey(0));
		while (anim.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		BIJImage image = ResourceManager.LoadImage("TROPHY").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("TrophyPrizeInfo_CharaInfo");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		if (mCompanionData != null)
		{
			mL2DRender = mGame.CreateLive2DRender(568, 568);
			Live2DInstance live2DInstance = Util.CreateGameObject("DummyPartner", mL2DRender.gameObject).AddComponent<Live2DInstance>();
			live2DInstance.Init(mCompanionData.GetModelKey(0), Vector3.zero, 1f, Live2DInstance.PIVOT.CENTER);
			live2DInstance.StartMotion("motions/wait00.mtn", true);
			live2DInstance.Position = Vector3.zero;
			mL2DRender.ResisterInstance(live2DInstance);
		}
		UILabel uILabel = Util.CreateLabel("CharacterName", base.gameObject, atlasFont);
		titleDescKey = string.Format(Localization.Get(mItemData.data.Name));
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 6, new Vector3(0f, 212f, 0f), 28, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		UISprite uISprite = Util.CreateSprite("MessageFrame", base.gameObject, atlas);
		Util.SetSpriteInfo(uISprite, "instruction_panel15", mBaseDepth + 2, new Vector3(0f, -125f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(515, 101);
		GameObject gameObject = uISprite.gameObject;
		uISprite = Util.CreateSprite("MessageFrameTitle", gameObject, atlas);
		Util.SetSpriteInfo(uISprite, "instruction_accessory10", mBaseDepth + 3, new Vector3(0f, 52f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(499, 42);
		uILabel = Util.CreateLabel("SkillName", uISprite.gameObject, atlasFont);
		titleDescKey = mGame.mCompanionData[mChara_index].GetSkillName(1);
		titleDescKey = titleDescKey.Replace("\n", string.Empty);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 6, new Vector3(0f, 0f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.effectStyle = UILabel.Effect.Shadow;
		uILabel = Util.CreateLabel("SkillDesc", gameObject.gameObject, atlasFont);
		titleDescKey = mGame.mCompanionData[mChara_index].GetSkillDesc(1) + mGame.mCompanionData[mChara_index].GetSkillSubDesc(1);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 6, new Vector3(0f, -8f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = new Color(0.2509804f, 0.20784314f, 6f / 85f);
		uILabel.spacingY = 7;
		if (mItemData.data.UseTrophy <= mGame.mPlayer.GetTrophy())
		{
			UIButton uIButton = Util.CreateJellyImageButton("ButtonClose", base.gameObject, image);
			Util.SetImageButtonInfo(uIButton, "LC_button_exchange", "LC_button_exchange", "LC_button_exchange", mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
			Util.AddImageButtonMessage(uIButton, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
			uILabel = Util.CreateLabel("Price", uIButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty + mItemData.data.UseTrophy, mBaseDepth + 4, new Vector3(46f, -2f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(uILabel);
		}
		else
		{
			uISprite = Util.CreateSprite("ButtonClose", base.gameObject, image);
			Util.SetSpriteInfo(uISprite, "LC_button_exchange", mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one, false);
			uILabel = Util.CreateLabel("Price", uISprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty + mItemData.data.UseTrophy, mBaseDepth + 4, new Vector3(46f, -2f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
			DialogBase.SetDialogLabelEffect2(uILabel);
			uISprite.color = Color.gray;
			uILabel.color = new Color(32f / 85f, 28f / 85f, 11f / 85f, 1f);
		}
		uILabel.fontSize = 24;
		CreateCancelButton("OnCancelPushed");
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mSelectItem = SELECT_ITEM.BUY;
			Close();
		}
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mState.Reset(STATE.WAIT, true);
			mSelectItem = SELECT_ITEM.CANCEL;
			Close();
		}
	}

	public override void Close()
	{
		if (mTexture != null)
		{
			Object.Destroy(mTexture.gameObject);
		}
		if (mL2DRender != null)
		{
			Object.Destroy(mL2DRender.gameObject);
		}
		mGame.PlaySe("SE_POSITIVE", -1);
		base.Close();
	}

	public override void OnOpening()
	{
		base.OnOpening();
		mTexture = Util.CreateGameObject("Texture", base.gameObject).AddComponent<UITexture>();
		mTexture.mainTexture = mL2DRender.RenderTexture;
		mTexture.SetDimensions(450, 450);
		mTexture.transform.localPosition = new Vector3(0f, 105f, -50f);
		mTexture.depth = mBaseDepth + 10;
		mTexture.transform.localScale = new Vector3(0.66f, 0.66f, 1f);
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		StartCoroutine(CloseProcess());
	}

	public IEnumerator CloseProcess()
	{
		yield return StartCoroutine(UnloadUnusedAssets());
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
