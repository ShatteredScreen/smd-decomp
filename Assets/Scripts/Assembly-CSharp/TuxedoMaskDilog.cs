using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuxedoMaskDilog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private OnDialogClosed mCallback;

	private int Characterdata;

	private BIJImage atlas;

	private BIJImage atlasShop;

	private float mCounter;

	private bool mOKEnable = true;

	private bool mAnimeEnable = true;

	private UISsSprite UISsAnime;

	private ShopItemData mItemData;

	private float mStateTime;

	private bool mFaceChanged;

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.MAIN:
			if (!mFaceChanged)
			{
				mStateTime += Time.deltaTime;
				if (mStateTime > 3f)
				{
					mStateTime = 0f;
					mFaceChanged = true;
				}
			}
			break;
		}
		mCounter += Time.deltaTime;
		if (mOKEnable)
		{
			if (mCounter > 0.3f && mAnimeEnable)
			{
				mGame.PlaySe("SE_ITEM_BUY_SHOP", -1);
				UISsAnime = Util.CreateGameObject("GetAnime", base.gameObject).AddComponent<UISsSprite>();
				UISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_ITEMGET_ANIMATION").SsAnime;
				UISsAnime.depth = mBaseDepth + 10;
				UISsAnime.transform.localPosition = new Vector3(0f, 0f, 0f);
				UISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
				UISsAnime.PlayCount = 1;
				UISsAnime.Play();
				UISsSprite uISsAnime = UISsAnime;
				uISsAnime.AnimationFinished = (UISsSprite.AnimationCallback)Delegate.Combine(uISsAnime.AnimationFinished, new UISsSprite.AnimationCallback(OnGetAnimeFinished));
				mAnimeEnable = false;
			}
			if (mCounter > 2f)
			{
				if (UISsAnime != null)
				{
					UnityEngine.Object.Destroy(UISsAnime.gameObject);
					UISsAnime = null;
				}
				UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
				Util.SetImageButtonInfo(button, "LC_button_ok2", "LC_button_ok2", "LC_button_ok2", mBaseDepth + 3, new Vector3(0f, -221f, 0f), Vector3.one);
				Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
				mOKEnable = false;
			}
		}
		mState.Update();
	}

	private void OnGetAnimeFinished(UISsSprite sprite)
	{
		if (UISsAnime != null)
		{
			UnityEngine.Object.Destroy(UISsAnime.gameObject);
			UISsAnime = null;
		}
	}

	public override IEnumerator BuildResource()
	{
		ResLive2DAnimation anim = ResourceManager.LoadLive2DAnimationAsync("TUXEDO");
		ResImage shop = ResourceManager.LoadImageAsync("MAIL_BOX");
		while (anim.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		while (shop.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return 0;
		}
		atlasShop = shop.Image;
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("Store_END_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UISprite uISprite = Util.CreateSprite("balloon", base.gameObject, atlasShop);
		Util.SetSpriteInfo(uISprite, "store_balloon", mBaseDepth + 2, new Vector3(83f, 150f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(390, 124);
		int num = 0;
		num = UnityEngine.Random.Range(0, mGame.mGameProfile.TuxedWordList.Count);
		int num2 = mGame.mGameProfile.TuxedWordList[num];
		if (num2 <= 0)
		{
			num2 = 1;
		}
		titleDescKey = Localization.Get(string.Format("Store_Desc{0:00}", num2));
		UILabel uILabel = Util.CreateLabel("Desc", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, titleDescKey, mBaseDepth + 3, new Vector3(0f, 12f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uISprite = Util.CreateSprite("TUXEDO", base.gameObject, atlasShop);
		Util.SetSpriteInfo(uISprite, "chara_shop", mBaseDepth + 8, new Vector3(-169f, -7f), Vector3.one, false);
		UISprite uISprite2 = Util.CreateSprite("lace", base.gameObject, atlasShop);
		Util.SetSpriteInfo(uISprite2, "store_lace", mBaseDepth + 2, new Vector3(90f, -36f, 0f), Vector3.one, false);
		uISprite2.SetDimensions(364, 269);
		int num3 = 0;
		foreach (KeyValuePair<ShopItemDetail.ItemKind, int> bundleItem in mItemData.ItemDetail.BundleItems)
		{
			if (mItemData.ItemDetail.BundleItems.Count >= 5)
			{
				continue;
			}
			Vector3 vector = new Vector3(0f, 0f, 0f);
			Vector3 vector2 = new Vector3(0f, 0f, 0f);
			float num4 = 0f;
			if (!mItemData.IsSetBundle)
			{
				vector = new Vector3(37f, -9f, 0f);
				vector2 = new Vector3(-45f, -4f, 0f);
			}
			else
			{
				int num5 = num3 / 2;
				int num6 = num3 % 2;
				vector = new Vector3(-35f + 140f * (float)num6, 10f - 62f * (float)num5, 0f);
				vector2 = new Vector3(-100f + 140f * (float)num6, 17f - 62f * (float)num5, 0f);
			}
			string empty = string.Empty;
			if (ShopItemDetail.ItemKind.HEART > bundleItem.Key)
			{
				BoosterData boosterData = mGame.mBoosterData[(int)bundleItem.Key];
				empty = boosterData.iconBuy;
			}
			else
			{
				ShopItemDetail.ItemKind key = bundleItem.Key;
				empty = ((key != ShopItemDetail.ItemKind.HEART && key == ShopItemDetail.ItemKind.GROWUP) ? "icon_level" : "icon_heart");
			}
			if (mItemData.ItemDetail.BundleItems.Count == 1)
			{
				if (Def.MugenHeartIdToKindData.ContainsKey(mItemData.Index))
				{
					empty = "icon_heartinfinite";
					vector2 = new Vector3(0f, -4f, 0f);
				}
				else
				{
					string empty2 = string.Empty;
					empty2 = Localization.Get("Purchase_mul") + " " + bundleItem.Value;
					uILabel = Util.CreateLabel("ItemNum", uISprite2.gameObject, atlasFont);
					Util.SetLabelInfo(uILabel, empty2, mBaseDepth + 3, vector, 24, 0, 0, UIWidget.Pivot.Center);
					DialogBase.SetDialogLabelEffect2(uILabel);
					uILabel.fontSize = 32;
				}
				UISprite sprite = Util.CreateSprite("ItemIcon", uISprite2.gameObject, atlas);
				Util.SetSpriteInfo(sprite, empty, mBaseDepth + 3, vector2, Vector3.one, false);
			}
			else
			{
				UISprite uISprite3 = Util.CreateSprite("ItemIcon", uISprite2.gameObject, atlas);
				Util.SetSpriteInfo(uISprite3, empty, mBaseDepth + 3, vector2, Vector3.one, false);
				uISprite3.SetDimensions(60, 60);
				string text = Localization.Get("Purchase_mul") + " " + bundleItem.Value;
				uILabel = Util.CreateLabel("ItemNum", uISprite2.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel, text, mBaseDepth + 3, vector, 20, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel);
				num4 = 11f;
			}
			if (num3 == 0)
			{
				string text2 = string.Format(Localization.Get(mItemData.Name));
				UILabel uILabel2 = Util.CreateLabel("ItemName", uISprite2.gameObject, atlasFont);
				Util.SetLabelInfo(uILabel2, text2, mBaseDepth + 5, new Vector3(0f, 59f + num4, 0f), 24, 0, 0, UIWidget.Pivot.Center);
				DialogBase.SetDialogLabelEffect2(uILabel2);
				uILabel2.fontSize = 30;
				uILabel2.color = new Color(0.8980392f, 0.18431373f, 0.34509805f, 1f);
			}
			num3++;
		}
		if (mItemData.ItemDetail.BundleItems.Count >= 5)
		{
			Vector3 vector3 = new Vector3(0f, 0f, 0f);
			Vector3 vector4 = new Vector3(0f, 0f, 0f);
			float num7 = 0f;
			vector4 = new Vector3(0f, -4f, 0f);
			string text3 = mItemData.IconName;
			if (string.IsNullOrEmpty(text3))
			{
				text3 = "icon_150itemset";
			}
			UISprite sprite2 = Util.CreateSprite("ItemIcon", uISprite2.gameObject, atlasShop);
			Util.SetSpriteInfo(sprite2, text3, mBaseDepth + 3, vector4, Vector3.one, false);
			string empty3 = string.Empty;
		}
		CreateGemWindow();
	}

	public void Init(ShopItemData a_item)
	{
		mItemData = a_item;
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		if (!mOKEnable)
		{
			OnClosePushed();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
