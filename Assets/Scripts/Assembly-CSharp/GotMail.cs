using System.Collections.Generic;

public class GotMail : ParameterObject<GotMail>
{
	public class Target
	{
		[MiniJSONAlias("id")]
		public int id { get; set; }

		public Target(int a_id)
		{
			id = a_id;
		}
	}

	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("openudid")]
	public string OpenUDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("targets")]
	public List<Target> Targets { get; set; }

	public GotMail()
	{
		Targets = new List<Target>();
	}

	public void AddTargets(int a_id)
	{
		Targets.Add(new Target(a_id));
	}
}
