using System;

public class ChargePriceCheckResponseProfile : ICloneable
{
	[MiniJSONAlias("price")]
	public long price { get; set; }

	[MiniJSONAlias("server_time")]
	public long server_time { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public ChargePriceCheckResponseProfile Clone()
	{
		return MemberwiseClone() as ChargePriceCheckResponseProfile;
	}
}
