public class SCDebugSetRequest : ParameterObject<SCDebugSetRequest>
{
	public int bhiveid { get; set; }

	public string udid { get; set; }

	public string uniq_id { get; set; }

	public int uuid { get; set; }

	public int category { get; set; }

	public int sub_category { get; set; }

	public int itemid { get; set; }

	public int quantity { get; set; }
}
