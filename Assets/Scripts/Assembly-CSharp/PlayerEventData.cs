using System;
using System.Collections.Generic;

public class PlayerEventData : SMJsonData, ICloneable
{
	public const int ClearCheckFlag_None = 0;

	public const int ClearCheckFlag_StageAllClear = 1;

	public const int ClearCheckFlag_StarComplete = 2;

	public int Version { get; set; }

	public int EventID { get; set; }

	public short CourseID { get; set; }

	public short PlayableMaxCourseID { get; set; }

	public Dictionary<short, PlayerMapData> CourseData { get; set; }

	public Dictionary<short, bool> CourseClearFlag { get; set; }

	public Dictionary<short, bool> CourseCompleteFlag { get; set; }

	public bool ShowCourseDescFlg { get; set; }

	public byte FirstHowToPlayFlg { get; set; }

	public List<short> ShowDemoIDList { get; set; }

	public Dictionary<short, bool> EventNoticeFlag { get; set; }

	public PlayerEventLabyrinthData LabyrinthData { get; set; }

	public PlayerEventData()
	{
		Version = 1290;
		CourseData = new Dictionary<short, PlayerMapData>();
		CourseClearFlag = new Dictionary<short, bool>();
		CourseCompleteFlag = new Dictionary<short, bool>();
		EventNoticeFlag = new Dictionary<short, bool>();
		ShowDemoIDList = new List<short>();
		LabyrinthData = null;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void NewCreate(int a_eventID, short a_maxCourse, Def.EVENT_TYPE a_eventType)
	{
		EventID = a_eventID;
		CourseID = 0;
		PlayableMaxCourseID = 0;
		for (int i = 0; i < a_maxCourse; i++)
		{
			PlayerMapData playerMapData = new PlayerMapData();
			playerMapData.PlayableMaxLevel = 100;
			CourseData.Add((short)i, playerMapData);
		}
		if (a_eventType == Def.EVENT_TYPE.SM_LABYRINTH)
		{
			LabyrinthData = new PlayerEventLabyrinthData();
		}
	}

	public void UpdateCourseClear(short a_courseID, bool a_flg)
	{
		if (!CourseClearFlag.ContainsKey(a_courseID))
		{
			CourseClearFlag.Add(a_courseID, a_flg);
		}
		else
		{
			CourseClearFlag[a_courseID] = a_flg;
		}
	}

	public void UpdateCourseComplete(short a_courseID, bool a_flg)
	{
		if (!CourseCompleteFlag.ContainsKey(a_courseID))
		{
			CourseCompleteFlag.Add(a_courseID, a_flg);
		}
		else
		{
			CourseCompleteFlag[a_courseID] = a_flg;
		}
	}

	public void ShowNotice(short a_key)
	{
		EventNoticeFlag[a_key] = true;
	}

	public bool HasShownNotice(short a_key)
	{
		if (EventNoticeFlag.ContainsKey(a_key))
		{
			return EventNoticeFlag[a_key];
		}
		return false;
	}

	public bool HasShownDemo(short a_demoID)
	{
		return ShowDemoIDList.Contains(a_demoID);
	}

	public void ShowDemo(short a_demoID)
	{
		if (!ShowDemoIDList.Contains(a_demoID))
		{
			ShowDemoIDList.Add(a_demoID);
		}
	}

	public void CheckEventStageClearReward(Def.EVENT_TYPE a_eventType, short a_course, List<int> a_stages, out bool a_isClear, out bool a_isComplete)
	{
		a_isClear = false;
		a_isComplete = false;
		if (!CourseData.ContainsKey(a_course))
		{
			return;
		}
		byte b = 0;
		if (CourseClearFlag.ContainsKey(a_course) && CourseClearFlag[a_course])
		{
			b = (byte)(b | 1u);
		}
		if (CourseCompleteFlag.ContainsKey(a_course) && CourseCompleteFlag[a_course])
		{
			b = (byte)(b | 2u);
		}
		if ((b & 3) == 3)
		{
			return;
		}
		bool flag = true;
		bool flag2 = true;
		for (int i = 0; i < a_stages.Count; i++)
		{
			int a_stageNo = a_stages[i];
			PlayerClearData clearData = CourseData[a_course].GetClearData(a_stageNo);
			if (clearData != null)
			{
				if (clearData.GotStars != 3)
				{
					flag2 = false;
				}
				continue;
			}
			flag = false;
			flag2 = false;
			break;
		}
		if (flag && (b & 1) == 0)
		{
			a_isClear = true;
		}
		if (flag2 && (b & 2) == 0)
		{
			a_isComplete = true;
		}
	}

	public void GetEventClearStarCount(short a_courseID, out short a_stage, out short a_star)
	{
		a_stage = 0;
		a_star = 0;
		if (!CourseData.ContainsKey(a_courseID))
		{
			return;
		}
		PlayerMapData playerMapData = CourseData[a_courseID];
		if (playerMapData == null)
		{
			return;
		}
		a_stage = (short)playerMapData.StageClearData.Count;
		for (int i = 0; i < a_stage; i++)
		{
			PlayerClearData playerClearData = playerMapData.StageClearData[i];
			if (playerClearData != null)
			{
				a_star += (short)playerClearData.GotStars;
			}
		}
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		data.WriteInt(EventID);
		data.WriteShort(CourseID);
		data.WriteShort(PlayableMaxCourseID);
		data.WriteBool(ShowCourseDescFlg);
		data.WriteByte(FirstHowToPlayFlg);
		data.WriteInt(CourseData.Count);
		foreach (KeyValuePair<short, PlayerMapData> courseDatum in CourseData)
		{
			data.WriteShort(courseDatum.Key);
			courseDatum.Value.SerializeToBinary(data);
		}
		data.WriteInt(CourseClearFlag.Count);
		foreach (KeyValuePair<short, bool> item in CourseClearFlag)
		{
			data.WriteShort(item.Key);
			data.WriteBool(item.Value);
		}
		data.WriteInt(CourseCompleteFlag.Count);
		foreach (KeyValuePair<short, bool> item2 in CourseCompleteFlag)
		{
			data.WriteShort(item2.Key);
			data.WriteBool(item2.Value);
		}
		data.WriteInt(EventNoticeFlag.Count);
		foreach (KeyValuePair<short, bool> item3 in EventNoticeFlag)
		{
			data.WriteShort(item3.Key);
			data.WriteBool(item3.Value);
		}
		data.WriteInt(ShowDemoIDList.Count);
		foreach (short showDemoID in ShowDemoIDList)
		{
			data.WriteShort(showDemoID);
		}
		if (LabyrinthData != null)
		{
			data.WriteBool(true);
			LabyrinthData.SerializeToBinary(data);
		}
		else
		{
			data.WriteBool(false);
		}
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		short num = 1020;
		if (reqVersion >= 1030)
		{
			num = data.ReadShort();
		}
		EventID = data.ReadInt();
		CourseID = data.ReadShort();
		PlayableMaxCourseID = data.ReadShort();
		ShowCourseDescFlg = data.ReadBool();
		FirstHowToPlayFlg = data.ReadByte();
		CourseData.Clear();
		int num2 = data.ReadInt();
		for (int i = 0; i < num2; i++)
		{
			short key = data.ReadShort();
			PlayerMapData playerMapData = new PlayerMapData();
			playerMapData.DeserializeFromBinary<PlayerEventClearData, PlayerEventStageData>(data, reqVersion);
			CourseData.Add(key, playerMapData);
		}
		num2 = data.ReadInt();
		for (int j = 0; j < num2; j++)
		{
			short key2 = data.ReadShort();
			bool value = data.ReadBool();
			CourseClearFlag.Add(key2, value);
		}
		num2 = data.ReadInt();
		for (int k = 0; k < num2; k++)
		{
			short key3 = data.ReadShort();
			bool value2 = data.ReadBool();
			CourseCompleteFlag.Add(key3, value2);
		}
		if (num >= 1030)
		{
			num2 = data.ReadInt();
			for (int l = 0; l < num2; l++)
			{
				short key4 = data.ReadShort();
				bool value3 = data.ReadBool();
				EventNoticeFlag.Add(key4, value3);
			}
		}
		if (num >= 1180)
		{
			num2 = data.ReadInt();
			for (int m = 0; m < num2; m++)
			{
				short item = data.ReadShort();
				ShowDemoIDList.Add(item);
			}
		}
		if (num >= 1200 && data.ReadBool())
		{
			LabyrinthData = new PlayerEventLabyrinthData();
			LabyrinthData.DeserializeFromBinary(data, num);
		}
	}

	public override string SerializeToJson()
	{
		return string.Empty;
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerMapData obj = new PlayerMapData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch
		{
		}
	}

	public PlayerEventData Clone()
	{
		return MemberwiseClone() as PlayerEventData;
	}
}
