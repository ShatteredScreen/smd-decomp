using System.Collections;
using System.Text;
using UnityEngine;

public class GameStateServiceFinish : GameState
{
	public enum STATE
	{
		LOAD_WAIT = 0,
		UNLOAD_WAIT = 1,
		MAIN = 2,
		WAIT = 3,
		AUTH = 4,
		NICKNAME = 5,
		GEM = 6,
		WEBVIEW_SERVICE_FINISH_START = 7,
		WEBVIEW_SERVICE_FINISH_OPEN = 8,
		WEBVIEW_SERVICE_FINISH_WAIT = 9
	}

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD_WAIT);

	protected ConfirmDialog mErrorDialog;

	private WebViewDialog mWebViewDialog;

	private string mWebViewURL;

	private bool mShowWebView;

	private bool mLoadFailed;

	private bool mAuthFailed;

	public override void Start()
	{
		base.Start();
		mWebViewDialog = null;
		mShowWebView = false;
	}

	public override void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.LOAD_WAIT:
			if (isLoadFinished())
			{
				mState.Change(STATE.WEBVIEW_SERVICE_FINISH_START);
			}
			break;
		case STATE.UNLOAD_WAIT:
			if (isUnloadFinished())
			{
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.AUTH:
			if (mState.IsChanged())
			{
				mGame.mPlayer.Data.LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
				mGame.Network_UserAuth();
			}
			else
			{
				if (!GameMain.mUserAuthCheckDone)
				{
					break;
				}
				if (GameMain.mUserAuthSucceed)
				{
					if (GameMain.mUserAuthTransfered)
					{
						GameMain.mUserAuthTransfered = false;
						int uUID = mGame.mPlayer.UUID;
						int hiveId = mGame.mPlayer.HiveId;
						mGame.ResetPlayerData(true);
						mGame.mPlayer.SetUUID(uUID);
						mGame.mPlayer.HiveId = hiveId;
						mGame.Save();
					}
					mState.Change(STATE.NICKNAME);
				}
				else if (GameMain.mUserAuthErrorCode == Server.ErrorCode.AUTHKEY_REGISTERED)
				{
					UserDataInfo.Instance.ReCreateAuthKey();
					mState.Reset(STATE.AUTH, true);
				}
				else if (GameMain.mUserAuthErrorCode == Server.ErrorCode.AUTHKEY_DIFFERENT)
				{
					mAuthFailed = true;
					mState.Change(STATE.WEBVIEW_SERVICE_FINISH_START);
				}
				else
				{
					mGame.FinishLoading();
					mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 4, OnConnectErrorRetry, null);
					mState.Change(STATE.WAIT);
				}
			}
			break;
		case STATE.NICKNAME:
			if (mState.IsChanged())
			{
				mGame.Network_NicknameCheck();
			}
			else if (GameMain.mNickNameCheckDone)
			{
				if (GameMain.mNickNameSucceed)
				{
					mState.Change(STATE.GEM);
					break;
				}
				mGame.FinishLoading();
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 5, OnConnectErrorRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.GEM:
			if (mState.IsChanged())
			{
				mGame.GetGemCountGetTime = DateTimeUtil.UnitLocalTimeEpoch;
				mGame.Network_GetGemCount();
			}
			else if (GameMain.mGetGemCountCheckDone)
			{
				if (GameMain.mGetGemCountSucceed)
				{
					mState.Change(STATE.WEBVIEW_SERVICE_FINISH_START);
					break;
				}
				mGame.FinishLoading();
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.RETRY, 6, OnConnectErrorRetry, null);
				mState.Change(STATE.WAIT);
			}
			break;
		case STATE.WEBVIEW_SERVICE_FINISH_START:
			mGame.FinishLoading();
			ShowServiceFinished();
			mState.Change(STATE.WEBVIEW_SERVICE_FINISH_OPEN);
			break;
		case STATE.WEBVIEW_SERVICE_FINISH_OPEN:
			if (OpenWebViewDialog())
			{
				mState.Change(STATE.WEBVIEW_SERVICE_FINISH_WAIT);
			}
			break;
		}
		mState.Update();
	}

	public void OnDestroy()
	{
		if (mWebViewDialog != null)
		{
			Object.Destroy(mWebViewDialog.gameObject);
			mWebViewDialog = null;
		}
	}

	public override void Unload()
	{
		base.Unload();
		mState.Change(STATE.UNLOAD_WAIT);
	}

	public override IEnumerator LoadGameState()
	{
		mGame.StartLoading();
		LoadGameStateFinished();
		SetLayout(Util.ScreenOrientation);
		yield return 0;
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (isLoadFinished())
		{
			SetLayout(o);
		}
	}

	protected virtual void SetLayout(ScreenOrientation o)
	{
	}

	public override IEnumerator UnloadGameState()
	{
		yield return 0;
		mErrorDialog = null;
		if (mWebViewDialog != null)
		{
			Object.Destroy(mWebViewDialog.gameObject);
			mWebViewDialog = null;
		}
		UnloadGameStateFinished();
		yield return 0;
	}

	private void ShowWebView(string url, Color bgColor)
	{
		GameObject gameObject = Util.CreateGameObject("WebViewDialog", mRoot);
		mWebViewDialog = gameObject.AddComponent<WebViewDialog>();
		mWebViewDialog.SetWebViewBackGroundColor(bgColor);
		mWebViewDialog.SetClosedCallback(OnWebViewDialogClosed);
		mWebViewDialog.SetPageMovedCallback(OnWebViewDialogPageMoved);
		mWebViewDialog.SetInvisibleCancelButton(true);
		mWebViewURL = url;
		mShowWebView = true;
	}

	private void ShowServiceFinished()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(mGame.mMaintenanceProfile.ServiceFinishUrl);
		ShowWebView(stringBuilder.ToString(), Color.white);
	}

	private bool OpenWebViewDialog()
	{
		if (mWebViewDialog == null)
		{
			mState.Change(STATE.MAIN);
			return false;
		}
		if (!mWebViewDialog.IsOpend)
		{
			return false;
		}
		mWebViewDialog.SetExternalSpawnMode(WebViewDialog.EXTERNAL_SPAWN_MODE.SPAWN_AT_OUT_OF_DOMAIN);
		mWebViewDialog.LoadURL(mWebViewURL);
		return true;
	}

	private void OnWebViewDialogClosed()
	{
		if (mErrorDialog != null)
		{
			mErrorDialog.Close();
		}
		Object.Destroy(mWebViewDialog.gameObject);
		mWebViewDialog = null;
		mShowWebView = false;
	}

	private void OnWebViewDialogPageMoved(bool first, bool error)
	{
		if (!error)
		{
			if (mErrorDialog != null)
			{
				mErrorDialog.Close();
			}
			mWebViewDialog.ShowWebview(true);
		}
		else
		{
			mWebViewDialog.ShowWebview(false);
			mErrorDialog = Util.CreateGameObject("ConfirmDialog", mRoot).AddComponent<ConfirmDialog>();
			string desc = string.Format(Localization.Get("Network_Error_Desc"));
			mErrorDialog.Init(Localization.Get("Network_Error_Title"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.RETRY_ONLY);
			mErrorDialog.SetClosedCallback(OnWebViewErrorDialogClosed);
			mErrorDialog.SetBaseDepth(70);
		}
	}

	private void OnWebViewErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		if (mWebViewDialog != null)
		{
			mWebViewDialog.Reload();
		}
		mErrorDialog = null;
	}

	private void OnConnectErrorRetry(int a_state)
	{
		mGame.StartLoading();
		mState.Change((STATE)a_state);
	}
}
