using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FBIconManager : SingletonMonoBehaviour<FBIconManager>
{
	private enum STATE
	{
		IDLE = 0,
		GET_ICON = 1,
		WAIT_ICON = 2,
		COMP_ICON = 3,
		DELETE_CASH = 4
	}

	public delegate void OnPlayerIconGetCallback(bool ret, string fbid, Texture2D icon);

	private OnPlayerIconGetCallback mPlayerCallback;

	private STATE mState;

	private bool mCashDelete;

	private MyFriend mReqFriend;

	private MyFriend mComFriend;

	private FBIconRequest mRequest;

	private List<FBIconRequest> mPrimaryList = new List<FBIconRequest>();

	private List<FBIconRequest> mSecondaryList = new List<FBIconRequest>();

	private GameMain mGame;

	private List<FBUserManager> mUserManagerList = new List<FBUserManager>();

	protected override void Awake()
	{
		base.Awake();
		mState = STATE.IDLE;
		mCashDelete = false;
		mReqFriend = new MyFriend();
		mComFriend = new MyFriend();
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Start()
	{
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		mReqFriend = null;
		mComFriend = null;
		mPrimaryList.Clear();
		mPrimaryList = null;
		mSecondaryList.Clear();
		mSecondaryList = null;
		mUserManagerList.Clear();
		mUserManagerList = null;
	}

	public void Update()
	{
		switch (mState)
		{
		case STATE.IDLE:
			if (mGame.mGameStateManager.EnableBackgroundProcess())
			{
				mRequest = null;
				if (mPrimaryList.Count > 0)
				{
					mRequest = mPrimaryList[0];
					mPrimaryList.RemoveAt(0);
				}
				else if (mSecondaryList.Count > 0)
				{
					mRequest = mSecondaryList[0];
					mSecondaryList.RemoveAt(0);
				}
				if (mRequest != null)
				{
					mState = STATE.GET_ICON;
				}
				else if (mCashDelete)
				{
					mState = STATE.DELETE_CASH;
				}
			}
			break;
		case STATE.GET_ICON:
		{
			mState = STATE.WAIT_ICON;
			BIJSNS bIJSNS = SocialManager.Instance[SNS.Facebook];
			mState = STATE.WAIT_ICON;
			bIJSNS.FetchUserImage(mRequest.fbid, delegate(BIJSNS.Response res, object userdata)
			{
				FBIconRequest fBIconRequest = userdata as FBIconRequest;
				if (fBIconRequest != null && res != null && res.detail != null)
				{
					fBIconRequest.icon = res.detail as Texture2D;
				}
				mState = STATE.COMP_ICON;
			}, mRequest);
			break;
		}
		case STATE.COMP_ICON:
			if (mRequest.isPlayer)
			{
				bool ret = false;
				if (mRequest.icon != null)
				{
					ret = true;
				}
				if (mPlayerCallback != null)
				{
					mPlayerCallback(ret, mRequest.fbid, mRequest.icon);
				}
			}
			else
			{
				mReqFriend.Data.UUID = mRequest.uuid;
				mReqFriend.Icon = mRequest.icon;
				mReqFriend.SaveIcon();
			}
			mRequest.isDone = true;
			mRequest = null;
			mState = STATE.IDLE;
			break;
		case STATE.DELETE_CASH:
			DeleteIconCash();
			mState = STATE.IDLE;
			break;
		}
		for (int i = 0; i < mUserManagerList.Count; i++)
		{
			mUserManagerList[i].Update();
		}
	}

	public void AddFBUserManager(FBUserManager aManager)
	{
		if (!mUserManagerList.Contains(aManager))
		{
			mUserManagerList.Add(aManager);
		}
	}

	public FileInfo GetIconFileInfo(int uuid)
	{
		mComFriend.Data.UUID = uuid;
		return mComFriend.GetIconFileInfo();
	}

	public Texture2D GetIcon(int uuid)
	{
		mComFriend.Data.UUID = uuid;
		mComFriend.LoadIcon();
		return mComFriend.Icon;
	}

	public FBIconRequest GetIconRequest(int uuid, string fbid, bool isPrimary)
	{
		FBIconRequest fBIconRequest = new FBIconRequest();
		fBIconRequest.uuid = uuid;
		fBIconRequest.fbid = fbid;
		if (isPrimary)
		{
			mPrimaryList.Add(fBIconRequest);
		}
		else
		{
			mSecondaryList.Add(fBIconRequest);
		}
		return fBIconRequest;
	}

	public FBIconRequest GetPlayerIconRequest(string fbid, OnPlayerIconGetCallback aCallback)
	{
		mPlayerCallback = aCallback;
		FBIconRequest fBIconRequest = new FBIconRequest();
		fBIconRequest.uuid = 0;
		fBIconRequest.fbid = fbid;
		fBIconRequest.isPlayer = true;
		mPrimaryList.Add(fBIconRequest);
		return fBIconRequest;
	}

	public void DeleteIconCashRequest()
	{
		mCashDelete = true;
	}

	private void DeleteIconCash()
	{
		mCashDelete = false;
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		List<string> list = new List<string>();
		foreach (MyFriend value in instance.mPlayer.Friends.Values)
		{
			if (string.IsNullOrEmpty(value.Data.Fbid))
			{
				list.Add(value.IconFilePath);
			}
		}
		if (!Directory.Exists(MyFriend.IconBasePath))
		{
			return;
		}
		string[] files = Directory.GetFiles(MyFriend.IconBasePath);
		if (files == null)
		{
			return;
		}
		foreach (string text in files)
		{
			if (list.Contains(text))
			{
				continue;
			}
			FileInfo fileInfo = new FileInfo(text);
			if (fileInfo != null)
			{
				long num = DateTimeUtil.BetweenMinutes(fileInfo.LastWriteTime, DateTime.Now);
				if (num < 10080)
				{
					continue;
				}
			}
			if (File.Exists(text))
			{
				File.Delete(text);
			}
		}
	}
}
