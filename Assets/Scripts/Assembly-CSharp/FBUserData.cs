using UnityEngine;

public class FBUserData
{
	public enum STATE
	{
		DEFAULT = 0,
		LOADING = 1,
		SUCCESS = 2,
		FAILURE = 3
	}

	public int uuid { get; set; }

	public string fbid { get; set; }

	public Texture2D icon { get; set; }

	public STATE status { get; set; }

	public bool changed { get; set; }

	public FBUserData()
	{
		uuid = 0;
		fbid = string.Empty;
		icon = null;
		status = STATE.DEFAULT;
		changed = false;
	}

	public FBUserData(int aUuid, string aFbid, Texture2D aIcon)
	{
		uuid = aUuid;
		fbid = aFbid;
		icon = aIcon;
		status = STATE.DEFAULT;
		changed = false;
	}
}
