using System;

[Serializable]
public class SsPoint : SsInterpolatable
{
	public int X;

	public int Y;

	public SsPoint()
	{
	}

	public SsPoint(SsPoint r)
	{
		X = r.X;
		Y = r.Y;
	}

	public override string ToString()
	{
		return "X: " + X + ", Y: " + Y;
	}

	public static SsPoint[] CreateArray(int num)
	{
		SsPoint[] array = new SsPoint[num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = new SsPoint();
		}
		return array;
	}

	public SsPoint Clone()
	{
		return new SsPoint(this);
	}

	public SsInterpolatable GetInterpolated(SsCurveParams curve, float time, SsInterpolatable start, SsInterpolatable end, int startTime, int endTime)
	{
		SsPoint ssPoint = new SsPoint();
		return ssPoint.Interpolate(curve, time, start, end, startTime, endTime);
	}

	public SsInterpolatable Interpolate(SsCurveParams curve, float time, SsInterpolatable start_, SsInterpolatable end_, int startTime, int endTime)
	{
		SsPoint ssPoint = (SsPoint)start_;
		SsPoint ssPoint2 = (SsPoint)end_;
		X = SsInterpolation.Interpolate(curve, time, ssPoint.X, ssPoint2.X, startTime, endTime);
		Y = SsInterpolation.Interpolate(curve, time, ssPoint.Y, ssPoint2.Y, startTime, endTime);
		return this;
	}

	public void Scale(float s)
	{
		X = (int)((float)X * s);
		Y = (int)((float)Y * s);
	}
}
