using UnityEngine;

public class TileSprite : PartsChangedSprite
{
	public bool IsGrayOut;

	public void OnBeginRecycle()
	{
		base.enabled = true;
		mBaseColor = Color.white;
	}

	public void OnRecycled()
	{
	}

	public void ToRecycle()
	{
		CleanupAnimation();
		base.enabled = false;
		IsGrayOut = false;
	}

	public void Grayout(bool gray)
	{
		IsGrayOut = gray;
		if (gray)
		{
			SetColor(new Color(1f, 1f, 1f, 0.5f));
		}
		else
		{
			SetColor(Color.white);
		}
	}
}
