using System;

[Flags]
public enum SsKeyAttrFlags
{
	PosX = 1,
	PosY = 2,
	Pos = 3,
	Angle = 4,
	ScaleX = 8,
	ScaleY = 0x10,
	Scale = 0x18,
	Trans = 0x20,
	Prio = 0x40,
	FlipH = 0x80,
	FlipV = 0x100,
	Flip = 0x180,
	Hide = 0x200,
	PartsCol = 0x400,
	PartsPal = 0x800,
	Vertex = 0x1000,
	User = 0x2000,
	Sound = 0x4000,
	ImageOffsetX = 0x8000,
	ImageOffsetY = 0x10000,
	ImageOffsetXY = 0x18000,
	ImageOffsetW = 0x20000,
	ImageOffsetH = 0x40000,
	ImageOffsetWH = 0x60000,
	ImageOffset = 0x78000,
	OriginOffsetX = 0x80000,
	OriginOffsetY = 0x100000,
	OriginOffset = 0x180000,
	AllMask = 0x1FFFFF
}
