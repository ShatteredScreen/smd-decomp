using UnityEngine;

public class InstagramPlugin : MonoBehaviour
{
	private static InstagramPlugin mInstance;

	private InstagramIF mPluginIF;

	public static InstagramPlugin Instance
	{
		get
		{
			if (mInstance == null)
			{
				GameObject gameObject = new GameObject("Instagram");
				mInstance = gameObject.GetComponent<InstagramPlugin>();
				if (mInstance == null)
				{
					mInstance = gameObject.AddComponent<InstagramPlugin>();
				}
			}
			return mInstance;
		}
	}

	public static InstagramIF PluginIF
	{
		get
		{
			return Instance.mPluginIF;
		}
	}

	private void Awake()
	{
		if (mInstance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		mInstance = this;
		Object.DontDestroyOnLoad(base.gameObject);
		mPluginIF = base.gameObject.AddComponent<InstagramAndroid>();
	}

	private void OnDestroy()
	{
	}

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnApplicationPause(bool pause)
	{
	}
}
