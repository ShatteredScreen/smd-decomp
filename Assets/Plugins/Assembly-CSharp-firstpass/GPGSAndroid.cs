using UnityEngine;

public class GPGSAndroid : GooglePlayGameServiceIF
{
	private static AndroidJavaObject mJavaInstance;

	private static AndroidJavaObject javaInstance
	{
		get
		{
			if (mJavaInstance == null)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.bij.bijunityplugin.MainActivity"))
				{
					mJavaInstance = androidJavaClass.CallStatic<AndroidJavaObject>("getGooglePlayGameServicesPlugin", new object[0]);
				}
			}
			return mJavaInstance;
		}
	}

	public override void enableLogging(bool enable, bool verbose)
	{
		javaInstance.Call("enableLogging", enable, verbose);
	}

	public override void setup(string appId, bool autoConnect, string clientId, string clientSecret)
	{
		javaInstance.Call("setup", base.gameObject.name, appId, autoConnect, clientId, clientSecret);
	}

	public override void onPause()
	{
	}

	public override void onResume()
	{
	}

	public override bool isCanceled()
	{
		return false;
	}

	public override bool isSignedIn()
	{
		return javaInstance.Call<bool>("isSignedIn", new object[0]);
	}

	public override void authenticate()
	{
		javaInstance.Call("authenticate");
	}

	public override void signOut()
	{
		javaInstance.Call("signOut");
	}

	public override bool loadPlayer(string id)
	{
		return javaInstance.Call<bool>("loadPlayer", new object[1] { GooglePlayGameServiceIF.ToSS(id) });
	}

	public override bool unlockAchievement(string id)
	{
		return javaInstance.Call<bool>("unlockAchievement", new object[1] { GooglePlayGameServiceIF.ToSS(id) });
	}

	public override bool incrementAchievement(string id, int step)
	{
		return javaInstance.Call<bool>("incrementAchievement", new object[2]
		{
			GooglePlayGameServiceIF.ToSS(id),
			step
		});
	}

	public override bool showAchievements()
	{
		return javaInstance.Call<bool>("showAchievements", new object[0]);
	}

	public override bool resetAchievement(string id)
	{
		return javaInstance.Call<bool>("resetAchievement", new object[1] { GooglePlayGameServiceIF.ToSS(id) });
	}

	public override bool resetAchievementAll()
	{
		return javaInstance.Call<bool>("resetAchievementAll", new object[0]);
	}
}
