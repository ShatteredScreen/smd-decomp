using System;

public class TrophyExchangeInfo : SMJsonData, ICloneable
{
	public int PrizeID { get; set; }

	public int ExchangeCount { get; set; }

	public int SettingID { get; set; }

	public int ExchangeTotalCount { get; set; }

	public TrophyExchangeInfo()
	{
		PrizeID = 0;
		ExchangeCount = 0;
		SettingID = 0;
		ExchangeTotalCount = 0;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(PrizeID);
		data.WriteInt(ExchangeCount);
		data.WriteInt(SettingID);
		data.WriteInt(ExchangeTotalCount);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		PrizeID = data.ReadInt();
		ExchangeCount = data.ReadInt();
		SettingID = data.ReadInt();
		ExchangeTotalCount = data.ReadInt();
	}

	public override string SerializeToJson()
	{
		return new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Serialize(this);
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			TrophyExchangeInfo obj = new TrophyExchangeInfo();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch (Exception)
		{
		}
	}

	public TrophyExchangeInfo Clone()
	{
		return MemberwiseClone() as TrophyExchangeInfo;
	}
}
