public class SpecialChanceData
{
	public int ID { get; set; }

	public string Description { get; set; }

	public string ChanceType { get; set; }

	public string EffectPointFirst { get; set; }

	public string EffectPointR { get; set; }

	public string EffectPointS { get; set; }

	public string EffectPointSS { get; set; }

	public string EffectPointStarS { get; set; }

	public string EffectPointSideStory { get; set; }

	public string EffectPointReserve1 { get; set; }

	public string EffectPointReserve2 { get; set; }

	public string EffectPointReserve3 { get; set; }

	public string EffectPointReserve4 { get; set; }

	public string EffectPointRally { get; set; }

	public string EffectPointStar { get; set; }

	public string EffectPointRally2 { get; set; }

	public string EffectPointBingo { get; set; }

	public string EffectPointDailyChallenge { get; set; }

	public string EffectPointLabyrinth { get; set; }

	public int? AcquisitionStarMin { get; set; }

	public int? AcquisitionStarMax { get; set; }

	public string PossessWaveBomb { get; set; }

	public string PossessTapBomb2 { get; set; }

	public string PossessRainbow { get; set; }

	public string PossessAddScore { get; set; }

	public string PossessSkillCharge { get; set; }

	public string PossessSelectOne { get; set; }

	public string PossessSelectCollect { get; set; }

	public string PossessColorCrush { get; set; }

	public string PossessBlockCrush { get; set; }

	public string PossessPairMaker { get; set; }

	public string PossessAllCrush { get; set; }

	public int? BeforeUseMin { get; set; }

	public int? BeforeUseMax { get; set; }

	public string UseBeforeWaveBomb { get; set; }

	public string UseBeforeTapBomb2 { get; set; }

	public string UseBeforeRainbow { get; set; }

	public string UseBeforeAddScore { get; set; }

	public string UseBeforeSkillCharge { get; set; }

	public int? ContinueMin { get; set; }

	public int? ContinueMax { get; set; }

	public string PlayResult { get; set; }

	public string PlayLittleBitMore { get; set; }

	public int? PlayGetStarMin { get; set; }

	public int? PlayGetStarMax { get; set; }

	public void Deserialize(BIJBinaryReader reader)
	{
	}

	public SpecialChanceConditionItem Serialize(string path)
	{
		return null;
	}
}
