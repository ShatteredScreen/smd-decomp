public interface ISMTile
{
	Def.TILE_KIND Kind { get; }

	Def.TILE_FORM Form { get; }

	bool IsKind(Def.TILE_KIND a_kind);
}
