using UnityEngine;

public class ResScriptableObject : ResourceInstance
{
	private ScriptableObjectContainer mContainer;

	public GameObject mPrefab;

	public ScriptableObjectContainer Container
	{
		get
		{
			return mContainer;
		}
		set
		{
			if (value == null)
			{
				if (ScriptableObject != null)
				{
					ScriptableObject = null;
				}
				mContainer = null;
			}
			else
			{
				ScriptableObject = value.Data;
				mContainer = value;
			}
		}
	}

	public ScriptableObject ScriptableObject { get; protected set; }

	public ResScriptableObject(string name)
		: base(name)
	{
		ResourceType = TYPE.SCRIPTABLE_OBJECT;
	}
}
