using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class PuzzleBackGrounds : MonoBehaviour
{
	private class UpdateInfo
	{
		public bool hflipped;

		public bool vflipped;
	}

	private enum UV_ROTATE
	{
		ROTATE_0 = 0,
		ROTATE_90 = 1,
		ROTATE_180 = 2,
		ROTATE_270 = 3
	}

	private const float BORDER = 1f;

	public GameMain mGame;

	public IPuzzleManager mManager;

	private PuzzleAttributeInfo[,] mBackgroundInfos;

	private List<PuzzleAttributeInfo> mPAs = new List<PuzzleAttributeInfo>();

	private short[] mVertices;

	public BIJImage mAtlas;

	private MeshFilter mMeshFilter;

	private MeshRenderer mMeshRenderer;

	private Mesh mMesh;

	private Material mMaterial;

	private bool mModified;

	private bool mInitialized;

	private Dictionary<int, UpdateInfo> mUpdateCache = new Dictionary<int, UpdateInfo>();

	private static string[] EDGE_NAME = new string[16]
	{
		"null", "edge_N", "edge_E", "edge_NE", "edge_S", "edge_NS", "edge_SE", "edge_NES", "edge_W", "edge_NW",
		"edge_WE", "edge_NWE", "edge_SW", "edge_NWS", "edge_SWE", "edge_NWSE"
	};

	private static string EDGE_NAME_UP_RIGHT = "edge_corner_NE";

	private static string EDGE_NAME_DOWN_RIGHT = "edge_corner_SE";

	private static string EDGE_NAME_DOWN_LEFT = "edge_corner_SW";

	private static string EDGE_NAME_UP_LEFT = "edge_corner_NW";

	private static string[] DIANA_ROUTE_NAME = new string[16]
	{
		"footstamp_grid02", "footstamp_grid02", "footstamp_grid02", "footstamp_grid01", "footstamp_grid02", "footstamp_grid00", "footstamp_grid01", "footstamp_grid03", "footstamp_grid02", "footstamp_grid01",
		"footstamp_grid00", "footstamp_grid00", "footstamp_grid01", "footstamp_grid03", "footstamp_grid03", "footstamp_grid02"
	};

	private static UV_ROTATE[] DIANA_ROUTE_ROTATE = new UV_ROTATE[16]
	{
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_270,
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_0,
		UV_ROTATE.ROTATE_90,
		UV_ROTATE.ROTATE_90,
		UV_ROTATE.ROTATE_90,
		UV_ROTATE.ROTATE_180,
		UV_ROTATE.ROTATE_180,
		UV_ROTATE.ROTATE_270,
		UV_ROTATE.ROTATE_0
	};

	public bool Modified
	{
		get
		{
			return mModified;
		}
		set
		{
			mModified = value;
		}
	}

	public static PuzzleBackGrounds Make(GameObject _parent, GameMain _gameMain, IPuzzleManager _manager, BIJImage _atlas, PuzzleAttributeInfo[,] _backgroundObject, Vector3 _position)
	{
		PuzzleBackGrounds puzzleBackGrounds = Util.CreateGameObject("GoPuzzleBackGrounds", _parent).AddComponent<PuzzleBackGrounds>();
		puzzleBackGrounds.mGame = _gameMain;
		puzzleBackGrounds.mManager = _manager;
		puzzleBackGrounds.mAtlas = _atlas;
		puzzleBackGrounds.mBackgroundInfos = _backgroundObject;
		puzzleBackGrounds.Init();
		puzzleBackGrounds.MakeArray();
		puzzleBackGrounds.gameObject.transform.localPosition = _position;
		puzzleBackGrounds.gameObject.transform.localScale = Vector3.one;
		return puzzleBackGrounds;
	}

	private void Awake()
	{
		base.gameObject.isStatic = true;
		mMeshFilter = GetComponent<MeshFilter>();
		mMeshRenderer = GetComponent<MeshRenderer>();
		mMesh = new Mesh();
		mMaterial = new Material(GameMain.DefaultShader);
	}

	private void OnDestroy()
	{
		GameMain.SafeDestroy(mMeshFilter.sharedMesh);
		GameMain.SafeDestroy(mMeshRenderer.sharedMaterial);
	}

	private void InitForArray()
	{
		BIJNGUIImage bIJNGUIImage = (BIJNGUIImage)mAtlas;
		mPAs.Clear();
		int i = 0;
		for (int length = mBackgroundInfos.GetLength(0); i < length; i++)
		{
			int j = 0;
			for (int length2 = mBackgroundInfos.GetLength(1); j < length2; j++)
			{
				PuzzleAttributeInfo puzzleAttributeInfo = mBackgroundInfos[i, j];
				if (puzzleAttributeInfo != null)
				{
					Def.TILE_KIND kind = puzzleAttributeInfo.kind;
					if (kind == Def.TILE_KIND.NORMAL || kind == Def.TILE_KIND.DIANA_ROUTE)
					{
						puzzleAttributeInfo.pos = new IntVector2(j, i);
						mPAs.Add(puzzleAttributeInfo);
					}
				}
			}
		}
		int k = 0;
		for (int length3 = mBackgroundInfos.GetLength(0); k < length3; k++)
		{
			int l = 0;
			for (int length4 = mBackgroundInfos.GetLength(1); l < length4; l++)
			{
				PuzzleAttributeInfo puzzleAttributeInfo2 = mBackgroundInfos[k, l];
				if (puzzleAttributeInfo2 != null)
				{
					Def.TILE_KIND kind = puzzleAttributeInfo2.kind;
					if (kind == Def.TILE_KIND.EDGE)
					{
						puzzleAttributeInfo2.pos = new IntVector2(l, k);
						mPAs.Add(puzzleAttributeInfo2);
					}
				}
			}
		}
		int count = mPAs.Count;
		float z = base.gameObject.transform.localPosition.z;
		Vector3[] array = new Vector3[count * 4];
		Vector2[] array2 = new Vector2[count * 4];
		int[] array3 = new int[count * 6];
		Color32[] array4 = new Color32[count * 4];
		for (int m = 0; m < count; m++)
		{
			PuzzleAttributeInfo puzzleAttributeInfo3 = mPAs[m];
			string spriteName;
			UV_ROTATE rotate;
			GetSpriteIndex(puzzleAttributeInfo3, out spriteName, out rotate);
			Vector2 defaultCoords = mManager.GetDefaultCoords(puzzleAttributeInfo3.pos);
			Vector2 vector = new Vector2(Def.TILE_PITCH_H, Def.TILE_PITCH_V) + new Vector2(0.5f, 0.5f);
			Vector4 zero = Vector4.zero;
			float x = defaultCoords.x - vector.x / 2f + zero.x;
			float y = defaultCoords.y + vector.y / 2f + zero.y;
			float x2 = defaultCoords.x + vector.x / 2f - zero.z;
			float y2 = defaultCoords.y - vector.y / 2f - zero.w;
			array[m * 4] = new Vector3(x, y2, z);
			array[m * 4 + 1] = new Vector3(x2, y2, z);
			array[m * 4 + 2] = new Vector3(x, y, z);
			array[m * 4 + 3] = new Vector3(x2, y, z);
			array2[m * 4] = new Vector2(0f, 0f);
			array2[m * 4 + 1] = new Vector2(1f, 0f);
			array2[m * 4 + 2] = new Vector2(0f, 1f);
			array2[m * 4 + 3] = new Vector2(1f, 1f);
			array3[m * 6] = m * 4 + 2;
			array3[m * 6 + 1] = m * 4 + 1;
			array3[m * 6 + 2] = m * 4;
			array3[m * 6 + 3] = m * 4 + 2;
			array3[m * 6 + 4] = m * 4 + 3;
			array3[m * 6 + 5] = m * 4 + 1;
			array4[m * 4] = Color.clear;
			array4[m * 4 + 1] = Color.clear;
			array4[m * 4 + 2] = Color.clear;
			array4[m * 4 + 3] = Color.clear;
		}
		mMesh.vertices = array;
		mMesh.uv = array2;
		mMesh.triangles = array3;
		mMesh.colors32 = array4;
		mMesh.RecalculateNormals();
		mMesh.RecalculateBounds();
		mInitialized = true;
		mMeshFilter.sharedMesh = mMesh;
		mMeshRenderer.sharedMaterial = mMaterial;
	}

	private void GetSpriteIndex(PuzzleAttributeInfo _pa, out string spriteName, out UV_ROTATE rotate)
	{
		int[] array = new int[8];
		rotate = UV_ROTATE.ROTATE_0;
		switch (_pa.kind)
		{
		case Def.TILE_KIND.EDGE:
		{
			spriteName = "null";
			for (int j = 0; j < 8; j++)
			{
				if (mManager.IsInField(new IntVector2(_pa.pos.x + Def.DIR_OFS[j].x, _pa.pos.y + Def.DIR_OFS[j].y)))
				{
					PuzzleAttributeInfo puzzleAttributeInfo2 = mBackgroundInfos[_pa.pos.y + Def.DIR_OFS[j].y, _pa.pos.x + Def.DIR_OFS[j].x];
					if (puzzleAttributeInfo2 != null && puzzleAttributeInfo2.kind == Def.TILE_KIND.NORMAL)
					{
						array[j] = 1;
					}
				}
			}
			int num2 = array[0] * 1 + array[2] * 2 + array[4] * 4 + array[6] * 8;
			if (num2 > 0)
			{
				spriteName = EDGE_NAME[num2];
			}
			else if (array[1] != 0)
			{
				spriteName = EDGE_NAME_UP_RIGHT;
			}
			else if (array[3] != 0)
			{
				spriteName = EDGE_NAME_DOWN_RIGHT;
			}
			else if (array[5] != 0)
			{
				spriteName = EDGE_NAME_DOWN_LEFT;
			}
			else if (array[7] != 0)
			{
				spriteName = EDGE_NAME_UP_LEFT;
			}
			break;
		}
		case Def.TILE_KIND.DIANA_ROUTE:
		{
			spriteName = "null";
			for (int i = 0; i < 8; i++)
			{
				if (mManager.IsInField(new IntVector2(_pa.pos.x + Def.DIR_OFS[i].x, _pa.pos.y + Def.DIR_OFS[i].y)))
				{
					PuzzleAttributeInfo puzzleAttributeInfo = mBackgroundInfos[_pa.pos.y + Def.DIR_OFS[i].y, _pa.pos.x + Def.DIR_OFS[i].x];
					if (puzzleAttributeInfo != null && puzzleAttributeInfo.kind != Def.TILE_KIND.DIANA_ROUTE)
					{
						array[i] = 1;
					}
				}
			}
			int num = array[0] * 1 + array[2] * 2 + array[4] * 4 + array[6] * 8;
			spriteName = DIANA_ROUTE_NAME[num];
			rotate = DIANA_ROUTE_ROTATE[num];
			break;
		}
		default:
			if ((_pa.pos.x + _pa.pos.y) % 2 == 0)
			{
				spriteName = "puzzle_grid1";
			}
			else
			{
				spriteName = "puzzle_grid2";
			}
			break;
		}
	}

	public void Init()
	{
		InitForArray();
	}

	public void Uninit()
	{
	}

	public void SetPart(int tileIndex, bool hflipped, bool vflipped)
	{
		UpdateInfo updateInfo = new UpdateInfo();
		updateInfo.hflipped = hflipped;
		updateInfo.vflipped = vflipped;
		if (!mUpdateCache.ContainsKey(tileIndex))
		{
			mUpdateCache.Add(tileIndex, updateInfo);
		}
		else
		{
			mUpdateCache[tileIndex] = updateInfo;
		}
		Modified = true;
	}

	private void Start()
	{
		mInitialized = false;
	}

	private void Update()
	{
		if (Modified)
		{
			UpdateMesh();
			mUpdateCache.Clear();
			Modified = false;
		}
	}

	private void DebugOut()
	{
		if (!UnityEngine.Debug.isDebugBuild)
		{
		}
	}

	private void UpdateMesh()
	{
		float z = base.gameObject.transform.localPosition.z;
		BIJImage bIJImage = mAtlas;
		mMeshRenderer.sharedMaterial.mainTexture = bIJImage.Texture;
		mMeshRenderer.sharedMaterial.mainTextureOffset = new Vector2(0f, 0f);
		mMeshRenderer.sharedMaterial.mainTextureScale = new Vector2(1f, 1f);
		Vector2[] uv = mMesh.uv;
		Color32[] colors = mMesh.colors32;
		foreach (KeyValuePair<int, UpdateInfo> item in mUpdateCache)
		{
			int key = item.Key;
			PuzzleAttributeInfo pa = mPAs[key];
			UpdateInfo value = item.Value;
			bool hflipped = value.hflipped;
			bool vflipped = value.vflipped;
			string spriteName;
			UV_ROTATE rotate;
			GetSpriteIndex(pa, out spriteName, out rotate);
			Vector2 offset = bIJImage.GetOffset(spriteName);
			Vector2 size = bIJImage.GetSize(spriteName);
			Vector4 border = bIJImage.GetBorder(spriteName);
			float x;
			float y;
			float x2;
			float y2;
			if (hflipped)
			{
				if (vflipped)
				{
					x = offset.x + size.x - border.z;
					y = offset.y + size.y - border.w;
					x2 = offset.x + border.x;
					y2 = offset.y + border.y;
				}
				else
				{
					x = offset.x + size.x - border.z;
					y = offset.y + border.y;
					x2 = offset.x + border.x;
					y2 = offset.y + size.y - border.w;
				}
			}
			else if (vflipped)
			{
				x = offset.x + border.x;
				y = offset.y + size.y - border.w;
				x2 = offset.x + size.x - border.z;
				y2 = offset.y + border.y;
			}
			else
			{
				x = offset.x + border.x;
				y = offset.y + border.y;
				x2 = offset.x + size.x - border.z;
				y2 = offset.y + size.y - border.w;
			}
			switch (rotate)
			{
			case UV_ROTATE.ROTATE_90:
				uv[key * 4] = new Vector2(x, y2);
				uv[key * 4 + 1] = new Vector2(x, y);
				uv[key * 4 + 2] = new Vector2(x2, y2);
				uv[key * 4 + 3] = new Vector2(x2, y);
				break;
			case UV_ROTATE.ROTATE_180:
				uv[key * 4] = new Vector2(x2, y2);
				uv[key * 4 + 1] = new Vector2(x, y2);
				uv[key * 4 + 2] = new Vector2(x2, y);
				uv[key * 4 + 3] = new Vector2(x, y);
				break;
			case UV_ROTATE.ROTATE_270:
				uv[key * 4] = new Vector2(x2, y);
				uv[key * 4 + 1] = new Vector2(x2, y2);
				uv[key * 4 + 2] = new Vector2(x, y);
				uv[key * 4 + 3] = new Vector2(x, y2);
				break;
			default:
				uv[key * 4] = new Vector2(x, y);
				uv[key * 4 + 1] = new Vector2(x2, y);
				uv[key * 4 + 2] = new Vector2(x, y2);
				uv[key * 4 + 3] = new Vector2(x2, y2);
				break;
			}
			colors[key * 4] = Color.white;
			colors[key * 4 + 1] = Color.white;
			colors[key * 4 + 2] = Color.white;
			colors[key * 4 + 3] = Color.white;
		}
		mMesh.uv = uv;
		mMesh.colors32 = colors;
		if (!mInitialized)
		{
			mMesh.RecalculateNormals();
			mMesh.RecalculateBounds();
			mInitialized = true;
		}
		mMeshFilter.sharedMesh = mMesh;
	}

	public void MakeArray()
	{
		InitForArray();
		for (int i = 0; i < mPAs.Count; i++)
		{
			SetPart(i, false, false);
		}
	}
}
