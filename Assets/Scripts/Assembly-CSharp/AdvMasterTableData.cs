using System.Collections.Generic;

public class AdvMasterTableData
{
	[MiniJSONAlias("name")]
	public string name { get; set; }

	[MiniJSONAlias("latest_revision")]
	public int latest_revision { get; set; }

	[MiniJSONAlias("data")]
	public List<AdvTableData> data { get; set; }
}
