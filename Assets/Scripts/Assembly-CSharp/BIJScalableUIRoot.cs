using UnityEngine;

public class BIJScalableUIRoot : MonoBehaviour
{
	public int ManualWidth = -1;

	public int ManualHeight = -1;

	private UIRoot mRoot;

	private void Awake()
	{
		mRoot = GetComponent<UIRoot>();
		mRoot.maximumHeight = 2048;
		if (ManualWidth < 0)
		{
			ManualWidth = Screen.width;
		}
		if (ManualHeight < 0)
		{
			ManualHeight = Screen.height;
		}
		Update();
	}

	private void Start()
	{
	}

	private void Update()
	{
		if ((bool)mRoot && ManualWidth > 0 && ManualHeight > 0)
		{
			int num = ManualHeight;
			float num2 = (float)(Screen.height * ManualWidth) / (float)(Screen.width * ManualHeight);
			if (num2 > 1f)
			{
				num = (int)((float)num * num2);
			}
			if (mRoot.manualHeight != num)
			{
				mRoot.manualHeight = num;
			}
		}
	}
}
