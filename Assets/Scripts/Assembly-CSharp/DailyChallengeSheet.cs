using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/DailyChallenge/Sheet")]
public sealed class DailyChallengeSheet : ScriptableObject
{
	[SerializeField]
	private string mSheetName;

	[SerializeField]
	private DailyChallengeStage[] mItems;

	private Dictionary<int, DailyChallengeStage> mCache;

	public string SheetName
	{
		get
		{
			return mSheetName;
		}
		set
		{
			mSheetName = value;
		}
	}

	public int StageCount
	{
		get
		{
			return (Cache != null) ? Cache.Count : 0;
		}
	}

	private Dictionary<int, DailyChallengeStage> Cache
	{
		get
		{
			if (mCache == null)
			{
				mCache = mItems.Where((DailyChallengeStage n) => n != null).Select((DailyChallengeStage v, int i) => new
				{
					Value = v,
					Index = i
				}).ToDictionary(k => k.Index, v => v.Value);
			}
			return mCache;
		}
	}

	public DailyChallengeStage this[int idx]
	{
		get
		{
			return (Cache == null || !Cache.ContainsKey(idx)) ? null : Cache[idx];
		}
	}

	public void SetData(int id, DailyChallengeStage item)
	{
		if (id >= 0)
		{
			if (mItems == null)
			{
				mItems = new DailyChallengeStage[0];
				mCache = null;
			}
			if (id >= mItems.Length)
			{
				Array.Resize(ref mItems, id + 1);
				mCache = null;
			}
			mItems[id] = item;
		}
	}
}
