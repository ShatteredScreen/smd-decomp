public class ChargePriceCheckProfile : ParameterObject<ChargePriceCheckProfile>
{
	[MiniJSONAlias("bhiveid")]
	public int HiveID { get; set; }

	[MiniJSONAlias("uuid")]
	public int UUID { get; set; }

	[MiniJSONAlias("udid")]
	public string UDID { get; set; }

	[MiniJSONAlias("uniq_id")]
	public string UniqueID { get; set; }

	[MiniJSONAlias("past_seconds")]
	public long PastSeconds { get; set; }
}
