using UnityEngine;

public class BIJSwipeCollider : MonoBehaviour
{
	public delegate void OnPressHander(InputInfo a_info);

	public delegate void OnClickHandler(InputInfo a_info);

	public delegate void OnDragHandler(InputInfo a_info);

	public delegate void OnFlickHandler(InputInfo a_info);

	private GameObject mParentGO;

	private GameMain mGame;

	private UICamera mUICamera;

	protected Vector3 OriginPosition;

	protected Vector3 RangePosition;

	protected InputInfo mTouchInfo = new InputInfo();

	protected int mTouchID = -1;

	protected bool mIsEnable;

	private OnPressHander mPressHandler;

	private OnClickHandler mClickHandler;

	private OnDragHandler mDragHandler;

	private OnFlickHandler mFlickHandler;

	public int Width { get; protected set; }

	public int Height { get; protected set; }

	public static BIJSwipeCollider Make(GameObject a_parent, string a_name, int a_width, int a_height)
	{
		BIJSwipeCollider bIJSwipeCollider = Util.CreateGameObject(a_name, a_parent).AddComponent<BIJSwipeCollider>();
		bIJSwipeCollider.mParentGO = a_parent;
		bIJSwipeCollider.Width = a_width;
		bIJSwipeCollider.Height = a_height;
		bIJSwipeCollider.UpdatePosition();
		return bIJSwipeCollider;
	}

	public void SetEnable(bool a_flg)
	{
		mIsEnable = a_flg;
		mTouchInfo.Reset();
		mTouchID = -1;
	}

	public void UpdatePosition()
	{
		GameObject gameObject = Util.CreateGameObject("tmp", mParentGO);
		float x = base.transform.localPosition.x - (float)(Width / 2);
		float y = base.transform.localPosition.y - (float)(Height / 2);
		float x2 = base.transform.localPosition.x + (float)(Width / 2);
		float y2 = base.transform.localPosition.y + (float)(Height / 2);
		gameObject.transform.localPosition = new Vector3(x, y, base.transform.localPosition.z);
		OriginPosition = gameObject.transform.position;
		gameObject.transform.localPosition = new Vector3(x2, y2, base.transform.localPosition.z);
		RangePosition = gameObject.transform.position;
		Object.Destroy(gameObject);
	}

	public void SetCallbacks(OnPressHander a_press, OnClickHandler a_click, OnDragHandler a_drag, OnFlickHandler a_flick)
	{
		mPressHandler = a_press;
		mClickHandler = a_click;
		mDragHandler = a_drag;
		mFlickHandler = a_flick;
	}

	private void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mUICamera = mGame.mUICamera;
	}

	private void Update()
	{
		if (!mIsEnable)
		{
			return;
		}
		mTouchInfo.IsUpTrigger = false;
		mTouchInfo.IsDownTrigger = false;
		mTouchInfo.IsFlickTrigger = false;
		bool flag = false;
		if ((!(mUICamera != null) || !mUICamera.useMouse) ? InputForTouch(ref mTouchInfo) : InputForMouse(ref mTouchInfo))
		{
			if (mTouchInfo.IsDrag && mDragHandler != null)
			{
				mDragHandler(mTouchInfo);
			}
			else if (mTouchInfo.IsDown && mPressHandler != null)
			{
				mPressHandler(mTouchInfo);
			}
			if (mTouchInfo.IsUp && mClickHandler != null)
			{
				mClickHandler(mTouchInfo);
			}
			if (mTouchInfo.IsFlickTrigger && mFlickHandler != null)
			{
				mFlickHandler(mTouchInfo);
			}
		}
	}

	private Touch? GetTouch()
	{
		if (Input.touchCount <= 0)
		{
			return null;
		}
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (mTouchID == Input.touches[i].fingerId)
			{
				return Input.touches[i];
			}
		}
		if (Input.touches[0].phase == TouchPhase.Began)
		{
			mTouchID = Input.touches[0].fingerId;
			return Input.touches[0];
		}
		return null;
	}

	private bool IsRange(Vector3 a_pos)
	{
		bool result = false;
		float x = OriginPosition.x;
		float y = OriginPosition.y;
		float num = RangePosition.x - OriginPosition.x;
		float num2 = RangePosition.y - OriginPosition.y;
		if (a_pos.x >= x && a_pos.x <= x + num && a_pos.y <= y && a_pos.y >= y - num2)
		{
			result = true;
		}
		return result;
	}

	private bool InputForTouch(ref InputInfo a_info)
	{
		Touch? touch = GetTouch();
		if (!touch.HasValue)
		{
			return false;
		}
		a_info.SetTouchInfo(touch.GetValueOrDefault());
		switch (touch.GetValueOrDefault().phase)
		{
		case TouchPhase.Began:
			if (IsRange(a_info.ScreenPosition))
			{
				a_info.IsDown = true;
				a_info.IsDownTrigger = true;
				a_info.IsUp = false;
				a_info.FirstScreenPosition = touch.GetValueOrDefault().position;
				a_info.TouchFrameCount = 0;
				a_info.TouchTime = 0f;
			}
			break;
		case TouchPhase.Ended:
		case TouchPhase.Canceled:
		{
			a_info.IsUp = true;
			a_info.IsUpTrigger = true;
			a_info.IsDown = false;
			a_info.IsDrag = false;
			a_info.IsStay = false;
			a_info.TouchFrameCount = 0;
			a_info.TouchTime = 0f;
			float num4 = Vector3.Dot(a_info.FirstScreenPosition.normalized, a_info.ScreenPosition.normalized);
			if ((double)num4 > 0.9)
			{
				a_info.IsFlickTrigger = true;
				a_info.SetFlickDir();
			}
			mTouchID = -1;
			break;
		}
		case TouchPhase.Moved:
		{
			if (!IsRange(a_info.ScreenPosition))
			{
				goto case TouchPhase.Ended;
			}
			float num = a_info.OldScreenPosition.x - a_info.ScreenPosition.x;
			float num2 = a_info.OldScreenPosition.y - a_info.ScreenPosition.y;
			float num3 = Mathf.Sqrt(num * num + num2 * num2);
			if (num3 > 10f)
			{
				a_info.IsDrag = true;
				a_info.IsStay = false;
				a_info.TouchFrameCount++;
				a_info.TouchTime += Time.deltaTime;
				break;
			}
			goto case TouchPhase.Stationary;
		}
		case TouchPhase.Stationary:
			a_info.IsStay = true;
			a_info.TouchFrameCount++;
			a_info.TouchTime += Time.deltaTime;
			break;
		}
		return true;
	}

	private bool InputForMouse(ref InputInfo a_info)
	{
		if (Input.GetMouseButton(0))
		{
			a_info.SetMouseInfo();
			a_info.ScreenPosition = Input.mousePosition;
			return true;
		}
		return false;
	}
}
