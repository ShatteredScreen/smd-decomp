using UnityEngine;

[SerializeField]
public class ConnectBase : MonoBehaviour
{
	protected enum STATE
	{
		INIT = 0,
		START = 1,
		FINISH = 2,
		WAIT = 3,
		RESULT_WAIT = 4
	}

	public delegate void OnStateWaitHandler(ConnectBase a_target);

	public const short SUCCESS = 0;

	public const short ERROR = 1;

	public const short IGNORE_ERROR = 3;

	public const short ABANDON = 5;

	public const short TRANSFERED = 13;

	protected GameMain mGame;

	protected Player mPlayer;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	[SerializeField]
	protected STATE mCurrentState;

	[SerializeField]
	protected short mConnectResultKind = 1;

	protected ConnectParameterBase mParam;

	[SerializeField]
	protected ConnectCommonErrorDialog.STYLE mErrorDialogStyle = ConnectCommonErrorDialog.STYLE.RETRY;

	[SerializeField]
	protected ConnectCommonErrorDialog.STYLE mMaintenanceDialogStyle = ConnectCommonErrorDialog.STYLE.MAINTENANCE_RETRY;

	protected OnStateWaitHandler mResultWaitCallback;

	protected OnStateWaitHandler mDialogWaitCallback;

	protected string mConnectGUID = string.Empty;

	[SerializeField]
	public int ConnectAPI { get; private set; }

	public bool IsConnecting
	{
		get
		{
			return mState.GetStatus() != STATE.FINISH;
		}
	}

	public bool IsConnectAbort
	{
		get
		{
			return mState.GetStatus() == STATE.FINISH && IsSameBitMask(5);
		}
	}

	public bool ShowConnectErrorDialog { get; protected set; }

	public bool IsSkip { get; protected set; }

	public string ErrorTitle { get; protected set; }

	public string ErrorDesc { get; protected set; }

	public ConnectCommonErrorDialog.STYLE ErrorDialogStyle { get; protected set; }

	public ConnectCommonErrorDialog.OnDialogClosed ErrorDialogClosed { get; protected set; }

	public ConnectCommonErrorDialog.OnDialogClosed ErrorDialogClosedTemp { get; protected set; }

	public short RetryCount { get; protected set; }

	public void SetSkipFlag(bool a_isskip)
	{
		IsSkip = a_isskip;
	}

	public virtual ConnectResult GetResult()
	{
		ConnectResult.ERROR_TYPE eRROR_TYPE = ConnectResult.ERROR_TYPE.None;
		if (mConnectResultKind != 0)
		{
			if (IsSameBitMask(3))
			{
				eRROR_TYPE = ConnectResult.ERROR_TYPE.None;
			}
			else
			{
				if (IsSameBitMask(13))
				{
					eRROR_TYPE = ConnectResult.ERROR_TYPE.Transfered;
				}
				else if (IsSameBitMask(5))
				{
					eRROR_TYPE = ConnectResult.ERROR_TYPE.Abandon;
				}
				if (eRROR_TYPE == ConnectResult.ERROR_TYPE.None)
				{
					eRROR_TYPE = ConnectResult.ERROR_TYPE.Error;
				}
			}
		}
		return new ConnectResult(ConnectAPI, eRROR_TYPE);
	}

	public ConnectResult GetNoConnectResult()
	{
		return new ConnectResult(ConnectAPI);
	}

	private bool IsSameBitMask(short a_bit)
	{
		return (mConnectResultKind & a_bit) == a_bit;
	}

	protected void SetOverrideDialogStyle()
	{
		if (mParam != null)
		{
			if (mParam.OverrideErrorDialogStyle != 0)
			{
				mErrorDialogStyle = mParam.OverrideErrorDialogStyle;
			}
			if (mParam.OverrideMaintenanceDialogStyle != 0)
			{
				mMaintenanceDialogStyle = mParam.OverrideMaintenanceDialogStyle;
			}
		}
	}

	public void SetResultWaitCallback(OnStateWaitHandler a_callback)
	{
		mResultWaitCallback = a_callback;
	}

	public virtual void Init(int a_api)
	{
		ConnectAPI = a_api;
		RetryCount = 0;
		IsSkip = false;
		InitServerFlg();
	}

	public virtual bool IsValidConnectInstance()
	{
		return true;
	}

	protected virtual void InitServerFlg()
	{
	}

	protected virtual void ServerFlgSucceeded()
	{
	}

	protected virtual void ServerFlgFailed()
	{
	}

	public void SetConnectParam(ConnectParameterBase a_param)
	{
		mParam = a_param.Clone();
		SetOverrideDialogStyle();
	}

	public void StartConnect()
	{
		mState.Change(STATE.START);
	}

	public void ChangeWaitStateToAbort()
	{
		STATE status = mState.GetStatus();
		if (status == STATE.RESULT_WAIT)
		{
			mConnectResultKind = 5;
			mState.Change(STATE.FINISH);
		}
	}

	public void ChangeWaitStateToFinish()
	{
		STATE status = mState.GetStatus();
		if (status == STATE.RESULT_WAIT)
		{
			mConnectResultKind = 0;
			StateConnectFinish();
		}
	}

	protected virtual void StateStart()
	{
		mConnectResultKind = 0;
		mState.Change(STATE.FINISH);
	}

	protected virtual void StateConnectFinish()
	{
		bool flag = true;
		if (mParam != null)
		{
			flag = !mParam.RunSuccessCallback(this);
		}
		if (flag)
		{
			ConnectResult result = GetResult();
			if (result.ErrorType == ConnectResult.ERROR_TYPE.None)
			{
				ServerFlgSucceeded();
			}
			else
			{
				ServerFlgFailed();
			}
			mState.Change(STATE.FINISH);
		}
		else
		{
			mState.Change(STATE.RESULT_WAIT);
		}
	}

	public virtual bool SetServerResponse(int a_result, int a_code)
	{
		bool result = false;
		if (a_result == 0)
		{
			mConnectResultKind = 0;
			StateConnectFinish();
		}
		else
		{
			mConnectResultKind = 1;
			bool flag = false;
			if (mParam != null && mParam.IsIgnoreError)
			{
				flag = true;
			}
			if (!flag)
			{
				if (a_code == 98)
				{
					ShowErrorDialog(mMaintenanceDialogStyle);
					result = true;
				}
			}
			else
			{
				mConnectResultKind = 3;
				mState.Change(STATE.FINISH);
			}
		}
		return result;
	}

	protected virtual void Awake()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mPlayer = mGame.mPlayer;
	}

	protected virtual void Update()
	{
		mCurrentState = mState.GetStatus();
		switch (mCurrentState)
		{
		case STATE.INIT:
			RetryCount = 0;
			mState.Change(STATE.WAIT);
			break;
		case STATE.START:
			InitServerFlg();
			StateStart();
			break;
		case STATE.RESULT_WAIT:
			if (mResultWaitCallback != null)
			{
				mResultWaitCallback(this);
			}
			break;
		}
		mState.Update();
	}

	protected void MakeConnectGUID()
	{
		if (string.IsNullOrEmpty(mConnectGUID))
		{
			mConnectGUID = Network.RequestID;
		}
	}

	protected virtual void ShowErrorDialog(ConnectCommonErrorDialog.STYLE a_errorType)
	{
		ShowErrorDialog(string.Empty, string.Empty, a_errorType, OnConnectErrorDialogClosed);
	}

	protected virtual void ShowErrorDialog(string a_title, string a_desc, ConnectCommonErrorDialog.STYLE a_style, ConnectCommonErrorDialog.OnDialogClosed a_delegate)
	{
		ShowConnectErrorDialog = true;
		ErrorTitle = a_title;
		ErrorDesc = a_desc;
		ErrorDialogStyle = a_style;
		ErrorDialogClosedTemp = a_delegate;
		ErrorDialogClosed = OnErrorDialogClosedFirstCallback;
		mState.Change(STATE.WAIT);
	}

	protected void OnErrorDialogClosedFirstCallback(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		ShowConnectErrorDialog = false;
		if (ErrorDialogClosedTemp != null)
		{
			ErrorDialogClosedTemp(i, a_state);
		}
	}

	protected virtual void OnConnectErrorDialogClosed(ConnectCommonErrorDialog.SELECT_ITEM i, int a_state)
	{
		if (i == ConnectCommonErrorDialog.SELECT_ITEM.RETRY)
		{
			RetryCount++;
			mState.Change(STATE.START);
		}
		else
		{
			mConnectResultKind = 5;
			mState.Change(STATE.FINISH);
		}
	}
}
