using System;
using System.Collections.Generic;

public class EventProfile : ICloneable
{
	[MiniJSONAlias("TrophyExchange")]
	public List<TrophyLimited> TrophyExchangeList { get; set; }

	[MiniJSONAlias("TrophyExchangeNew")]
	public TrophyExchangeNewSetting TrophyExchangeSetting { get; set; }

	[MiniJSONAlias("daily")]
	public List<DailyEventSettings> DailyEventList { get; set; }

	[MiniJSONAlias("season")]
	public List<SeasonEventSettings> SeasonEventList { get; set; }

	[MiniJSONAlias("revival")]
	public List<RevivalEventSettings> RevivalEventList { get; set; }

	[MiniJSONAlias("sdaily")]
	public SpecialDailyEventSettings SDailyEvent { get; set; }

	[MiniJSONAlias("dailyc")]
	public List<DailyChallengeEventSettings> DailyChallengeEventList { get; set; }

	[MiniJSONAlias("gc_guerrilla")]
	public List<GLAEventSettings> GLAEvent { get; set; }

	[MiniJSONAlias("gc_guerrilla_new")]
	public GLAEventNewSettings GLAEventSetting { get; set; }

	[MiniJSONAlias("gc_tower")]
	public List<TWEventSettings> TWEvent { get; set; }

	[MiniJSONAlias("gc_pointget")]
	public List<PointGetEventSettings> PGEvent { get; set; }

	[MiniJSONAlias("gc_etype_as")]
	public List<int> EventTypeASList { get; set; }

	[MiniJSONAlias("event_appeal")]
	public List<EventAppealSettings> EventAppealList { get; set; }

	[MiniJSONAlias("series_campaign")]
	public List<SeriesCampaignSetting> SeriesCampaignList { get; set; }

	[MiniJSONAlias("datekey")]
	public long DateKey { get; set; }

	public EventProfile()
	{
		DailyEventList = new List<DailyEventSettings>();
		SeasonEventList = new List<SeasonEventSettings>();
		RevivalEventList = new List<RevivalEventSettings>();
		DailyChallengeEventList = new List<DailyChallengeEventSettings>();
		TrophyExchangeList = new List<TrophyLimited>();
		TrophyExchangeSetting = new TrophyExchangeNewSetting();
		GLAEvent = new List<GLAEventSettings>();
		GLAEventSetting = new GLAEventNewSettings();
		PGEvent = new List<PointGetEventSettings>();
		EventTypeASList = new List<int>();
		EventAppealList = new List<EventAppealSettings>();
		SeriesCampaignList = new List<SeriesCampaignSetting>();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public EventProfile Clone()
	{
		return MemberwiseClone() as EventProfile;
	}

	public SeasonEventSettings GetSeasonEvent(int a_eventID)
	{
		SeasonEventSettings result = null;
		for (int i = 0; i < SeasonEventList.Count; i++)
		{
			if (SeasonEventList[i].EventID == a_eventID)
			{
				result = SeasonEventList[i];
				break;
			}
		}
		return result;
	}

	public RevivalEventSettings GetRevivalEvent(int a_eventID)
	{
		RevivalEventSettings result = null;
		for (int i = 0; i < RevivalEventList.Count; i++)
		{
			if (RevivalEventList[i].EventID == a_eventID)
			{
				result = RevivalEventList[i];
				break;
			}
		}
		return result;
	}

	public EventAppealSettings GetEventAppeal()
	{
		EventAppealSettings eventAppealSettings = null;
		DateTime dateTime = DateTimeUtil.UnitLocalTimeEpoch;
		bool flag = false;
		Player mPlayer = SingletonMonoBehaviour<GameMain>.Instance.mPlayer;
		if (mPlayer.AdvSaveData.mEventEffectDispInfoList.Count > 0)
		{
			for (int num = mPlayer.AdvSaveData.mEventEffectDispInfoList.Count - 1; num >= 0; num--)
			{
				mPlayer.Data.EventEffectDispList.Insert(0, mPlayer.AdvSaveData.mEventEffectDispInfoList[num]);
			}
			mPlayer.AdvSaveData.mEventEffectDispInfoList.Clear();
			flag = true;
		}
		for (int i = 0; i < EventAppealList.Count; i++)
		{
			if (EventAppealList[i].IsNotShow == 1)
			{
				continue;
			}
			if (mPlayer.Data.EventEffectDispList != null && mPlayer.Data.EventEffectDispList.Count > 0)
			{
				bool flag2 = false;
				for (int j = 0; j < mPlayer.Data.EventEffectDispList.Count; j++)
				{
					if (mPlayer.Data.EventEffectDispList[j].mEventId == EventAppealList[i].EventID && mPlayer.Data.EventEffectDispList[j].mIsAdvEvent == EventAppealList[i].IsAdvEvent)
					{
						flag2 = true;
						break;
					}
				}
				if (flag2)
				{
					continue;
				}
			}
			bool a_ishold = false;
			DateTime a_start = DateTime.Now;
			DateTime a_end = DateTime.Now;
			if (EventAppealList[i].IsAdvEvent == 1)
			{
				if (!SingletonMonoBehaviour<GameMain>.Instance.IsPlayEventDungeon())
				{
					continue;
				}
				SingletonMonoBehaviour<GameMain>.Instance.GetAdvEventSchedule(EventAppealList[i].EventID, out a_start, out a_end, out a_ishold);
			}
			else
			{
				if (!SingletonMonoBehaviour<GameMain>.Instance.mTutorialManager.IsInitialTutorialCompleted())
				{
					continue;
				}
				SingletonMonoBehaviour<GameMain>.Instance.GetEventSchedule(EventAppealList[i].EventID, out a_start, out a_end, out a_ishold);
			}
			if (!a_ishold)
			{
				continue;
			}
			if (mPlayer.Data.EventEffectDispList != null)
			{
				EventEffectDispInfo eventEffectDispInfo = new EventEffectDispInfo();
				eventEffectDispInfo.mEventId = EventAppealList[i].EventID;
				eventEffectDispInfo.mIsAdvEvent = EventAppealList[i].IsAdvEvent;
				mPlayer.Data.EventEffectDispList.Add(eventEffectDispInfo);
			}
			if (eventAppealSettings == null)
			{
				eventAppealSettings = EventAppealList[i];
				dateTime = a_start;
			}
			else if (dateTime == a_start)
			{
				if (eventAppealSettings.IsAdvEvent == 1)
				{
					if (EventAppealList[i].IsAdvEvent == 1)
					{
						if (eventAppealSettings.EventID < EventAppealList[i].EventID)
						{
							eventAppealSettings = null;
							eventAppealSettings = EventAppealList[i];
							dateTime = a_start;
						}
					}
					else
					{
						eventAppealSettings = null;
						eventAppealSettings = EventAppealList[i];
						dateTime = a_start;
					}
				}
				else if (EventAppealList[i].IsAdvEvent == 0 && eventAppealSettings.EventID < EventAppealList[i].EventID)
				{
					eventAppealSettings = null;
					eventAppealSettings = EventAppealList[i];
					dateTime = a_start;
				}
			}
			else if (dateTime < a_start)
			{
				eventAppealSettings = null;
				eventAppealSettings = EventAppealList[i];
				dateTime = a_start;
			}
		}
		if (eventAppealSettings != null || flag)
		{
			SingletonMonoBehaviour<GameMain>.Instance.Save();
			if (flag)
			{
				SingletonMonoBehaviour<GameMain>.Instance.SaveAdvData();
			}
		}
		return eventAppealSettings;
	}

	public SeriesCampaignSetting GetSeriesCampaign(Def.SERIES a_series, long a_servertime, int a_eventID = -1)
	{
		if (SeriesCampaignList.Count == 0)
		{
			return null;
		}
		for (int i = 0; i < SeriesCampaignList.Count; i++)
		{
			if (SeriesCampaignList[i].Series == a_series && (a_series != Def.SERIES.SM_EV || a_eventID == SeriesCampaignList[i].EventID) && SeriesCampaignList[i].StartTime <= a_servertime && a_servertime <= SeriesCampaignList[i].EndTime)
			{
				return SeriesCampaignList[i];
			}
		}
		return null;
	}

	public List<SeriesCampaignSetting> GetSeriesCampaignList(long a_servertime)
	{
		if (SeriesCampaignList.Count == 0)
		{
			return null;
		}
		List<SeriesCampaignSetting> list = new List<SeriesCampaignSetting>();
		for (int i = 0; i < SeriesCampaignList.Count; i++)
		{
			if (SeriesCampaignList[i].StartTime <= a_servertime && a_servertime <= SeriesCampaignList[i].EndTime)
			{
				list.Add(SeriesCampaignList[i]);
			}
		}
		return list;
	}

	public bool AddTestSeriesCampaign(Def.SERIES a_series, long a_starttime, long a_endtime, int a_moves, int a_eventid = -1, bool a_override = false)
	{
		if (a_override)
		{
			for (int i = 0; i < SeriesCampaignList.Count; i++)
			{
				if (SeriesCampaignList[i].Series == a_series && (!Def.IsEventSeries(a_series) || SeriesCampaignList[i].EventID == a_eventid))
				{
					SeriesCampaignList[i].StartTime = a_starttime;
					SeriesCampaignList[i].EndTime = a_endtime;
					SeriesCampaignList[i].Moves = a_moves;
					return false;
				}
			}
		}
		SeriesCampaignSetting seriesCampaignSetting = new SeriesCampaignSetting();
		seriesCampaignSetting.SeriesOriginal = (short)a_series;
		seriesCampaignSetting.StartTime = a_starttime;
		seriesCampaignSetting.EndTime = a_endtime;
		seriesCampaignSetting.Moves = a_moves;
		seriesCampaignSetting.EventID = ((!Def.IsEventSeries(a_series)) ? (-1) : a_eventid);
		SeriesCampaignList.Add(seriesCampaignSetting);
		return true;
	}

	public TWEventSettings GetTWEventSetting()
	{
		TWEventSettings result = null;
		DateTime dateTime = DateTimeUtil.Now();
		if (TWEvent != null)
		{
			foreach (TWEventSettings item in TWEvent)
			{
				if (item != null)
				{
					DateTime dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(item.StartTime, true);
					DateTime dateTime3 = DateTimeUtil.UnixTimeStampToDateTime(item.EndTime, true);
					if (!(dateTime < dateTime2) && !(dateTime3 < dateTime))
					{
						result = item;
						break;
					}
				}
			}
		}
		return result;
	}

	public void ConvertTrophyExchangeSetting()
	{
		TrophyExchangeList.Clear();
		int aYear;
		int aMonth;
		int aDay;
		int aHour;
		_GetTrophyItemReplenishTime(out aYear, out aMonth, out aDay, out aHour);
		int trophyCount = aYear * 10000 + aMonth * 100 + aDay;
		for (int i = 0; i < TrophyExchangeSetting.mIDs.Count; i++)
		{
			TrophyLimited trophyLimited = new TrophyLimited();
			trophyLimited.TrophyItemID = TrophyExchangeSetting.mIDs[i];
			trophyLimited.TrophyItemLimit = TrophyExchangeSetting.mLimit;
			trophyLimited.TrophyCount = trophyCount;
			TrophyExchangeList.Add(trophyLimited);
		}
	}

	public DateTime GetTrophyItemReplenishTime()
	{
		int aYear;
		int aMonth;
		int aDay;
		int aHour;
		_GetTrophyItemReplenishTime(out aYear, out aMonth, out aDay, out aHour);
		return new DateTime(aYear, aMonth, aDay, aHour, 0, 0);
	}

	private void _GetTrophyItemReplenishTime(out int aYear, out int aMonth, out int aDay, out int aHour)
	{
		DateTime dateTime = DateTime.Now.AddSeconds(GameMain.mServerTimeDiff * -1.0).ToUniversalTime();
		if (dateTime.Hour < TrophyExchangeSetting.mHour)
		{
			dateTime = dateTime.AddDays(-1.0);
		}
		aYear = dateTime.Year;
		aMonth = 0;
		aDay = TrophyExchangeSetting.mDay;
		aHour = TrophyExchangeSetting.mHour;
		int num = dateTime.Month * 100 + dateTime.Day;
		for (int i = 0; i < TrophyExchangeSetting.mMonths.Count; i++)
		{
			int num2 = TrophyExchangeSetting.mMonths[i];
			int num3 = num2 * 100 + TrophyExchangeSetting.mDay;
			if (num < num3)
			{
				break;
			}
			aMonth = num2;
		}
		if (aMonth == 0)
		{
			if (TrophyExchangeSetting.mMonths.Count > 0)
			{
				aMonth = TrophyExchangeSetting.mMonths[TrophyExchangeSetting.mMonths.Count - 1];
				aYear--;
				return;
			}
			DateTime unitLocalTimeEpoch = DateTimeUtil.UnitLocalTimeEpoch;
			aYear = unitLocalTimeEpoch.Year;
			aMonth = unitLocalTimeEpoch.Month;
			aDay = unitLocalTimeEpoch.Day;
			aHour = unitLocalTimeEpoch.Hour;
		}
	}

	public void ConvertAdvGuerrillaSetting()
	{
		GLAEvent.Clear();
		if (GLAEventSetting.mTimeList.Count == 0)
		{
			return;
		}
		for (int num = 7 - GLAEventSetting.mWeek.Count; num > 0; num--)
		{
			GLAEventSetting.mWeek.Add(0);
		}
		List<DateTime> list = new List<DateTime>();
		List<DateTime> list2 = new List<DateTime>();
		DateTime dateTime = DateTimeUtil.Now().ToUniversalTime().AddDays(-1.0);
		int num2 = (int)dateTime.DayOfWeek;
		DateTime dateTime2 = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0);
		short num3 = (short)(dateTime.Hour * 100 + dateTime.Minute);
		for (int i = 1; i <= 7; i++)
		{
			if (GLAEventSetting.mWeek.Count > num2 && GLAEventSetting.mWeek[num2] != 0)
			{
				for (int j = 0; j < GLAEventSetting.mTimeList.Count; j++)
				{
					short mStart = GLAEventSetting.mTimeList[j].mStart;
					short mEnd = GLAEventSetting.mTimeList[j].mEnd;
					DateTime item = dateTime2.AddHours(mStart / 100).AddMinutes(mStart % 100);
					DateTime item2 = dateTime2.AddHours(mEnd / 100).AddMinutes(mEnd % 100);
					if (mStart > mEnd)
					{
						item2 = item2.AddDays(1.0);
					}
					list.Add(item);
					list2.Add(item2);
				}
			}
			dateTime2 = dateTime2.AddDays(1.0);
			num2++;
			if (num2 >= 7)
			{
				num2 = 0;
			}
		}
		bool flag = true;
		flag = false;
		for (int k = 0; k < list.Count; k++)
		{
			GLAEventSettings gLAEventSettings = new GLAEventSettings();
			gLAEventSettings.StartTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(list[k], flag);
			gLAEventSettings.EndTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(list2[k], flag);
			gLAEventSettings.EventID = GLAEventSetting.mEventID;
			gLAEventSettings.HelpURL = GLAEventSetting.mHelpURL;
			GLAEvent.Add(gLAEventSettings);
		}
	}

	public void AddTestEventAppeal(short eventID, short partnerID, bool a_showBG = false, long a_start = 0, long a_end = 0)
	{
		EventAppealSettings eventAppealSettings = new EventAppealSettings();
		eventAppealSettings.IsAdvEvent = 0;
		eventAppealSettings.EventID = eventID;
		eventAppealSettings.PartnerID = partnerID;
		eventAppealSettings.IsShowBG = (short)(a_showBG ? 1 : 0);
		EventAppealList.Clear();
		EventAppealList.Add(eventAppealSettings);
		SeasonEventSettings seasonEventSettings = null;
		foreach (SeasonEventSettings seasonEvent in SeasonEventList)
		{
			if (seasonEvent.EventID == eventID)
			{
				seasonEventSettings = seasonEvent;
				break;
			}
		}
		DateTime dateTime = DateTimeUtil.Now();
		DateTime dateTime2 = dateTime.AddDays(10.0);
		if (a_start > 0)
		{
			dateTime = DateTimeUtil.UnixTimeStampToDateTime(a_start, true);
		}
		if (a_end > 0)
		{
			dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(a_end, true);
		}
		if (seasonEventSettings == null)
		{
			seasonEventSettings = new SeasonEventSettings();
			seasonEventSettings.StartTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime, true);
			seasonEventSettings.EndTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime2, true);
			seasonEventSettings.EventID = eventID;
			seasonEventSettings.HelpURL = SingletonMonoBehaviour<GameMain>.Instance.mGameProfile.PromoBaseURL + string.Format("howto/{1}/event{0:00}/event{0:00}.html", eventID, SingletonMonoBehaviour<Network>.Instance.AppName.Substring(1));
			seasonEventSettings.HelpID = 1;
			SeasonEventList.Add(seasonEventSettings);
		}
		else
		{
			seasonEventSettings.StartTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime, true);
			seasonEventSettings.EndTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime2, true);
		}
	}

	public void AddTestAdvEventAppeal(short eventID, long a_start = 0, long a_end = 0)
	{
		EventAppealSettings eventAppealSettings = new EventAppealSettings();
		eventAppealSettings.IsAdvEvent = 1;
		eventAppealSettings.EventID = eventID;
		EventAppealList.Clear();
		EventAppealList.Add(eventAppealSettings);
		PointGetEventSettings pointGetEventSettings = null;
		foreach (PointGetEventSettings item in PGEvent)
		{
			if (item.EventID == eventID)
			{
				pointGetEventSettings = item;
				break;
			}
		}
		DateTime dateTime = DateTimeUtil.Now();
		DateTime dateTime2 = dateTime.AddDays(10.0);
		if (a_start > 0)
		{
			dateTime = DateTimeUtil.UnixTimeStampToDateTime(a_start, true);
		}
		if (a_end > 0)
		{
			dateTime2 = DateTimeUtil.UnixTimeStampToDateTime(a_end, true);
		}
		if (pointGetEventSettings == null)
		{
			string promoBaseURL = SingletonMonoBehaviour<GameMain>.Instance.mGameProfile.PromoBaseURL;
			pointGetEventSettings = new PointGetEventSettings();
			pointGetEventSettings.StartTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime, true);
			pointGetEventSettings.EndTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime2, true);
			pointGetEventSettings.EventID = eventID;
			pointGetEventSettings.HelpURL = promoBaseURL + string.Format("howto/smdjp/adv_event{0:00}/adv_event{0:00}.html", eventID);
			pointGetEventSettings.SpBonusListURL = promoBaseURL + string.Format("howto/smdjp/adv_event{0:00}/adv_event{0:00}_01.html", eventID);
			pointGetEventSettings.LinkBanner = new LinkBannerSetting();
			pointGetEventSettings.LinkBanner.BaseURL = promoBaseURL + "linkbanner/images/";
			LinkBannerDetail linkBannerDetail = new LinkBannerDetail();
			linkBannerDetail.FileName = "004_christmas_night_gasya.png";
			linkBannerDetail.Action = "NOP";
			linkBannerDetail.Param = eventID;
			pointGetEventSettings.LinkBanner.BannerList.Add(linkBannerDetail);
			PGEvent.Add(pointGetEventSettings);
		}
		else
		{
			pointGetEventSettings.StartTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime, true);
			pointGetEventSettings.EndTime = (long)DateTimeUtil.DateTimeToUnixTimeStamp(dateTime2, true);
		}
	}
}
