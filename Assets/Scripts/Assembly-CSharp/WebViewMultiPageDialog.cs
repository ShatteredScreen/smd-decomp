using UnityEngine;
using UnityEngine.Events;

public sealed class WebViewMultiPageDialog : WebViewDialog
{
	public enum SELECT_ITEM
	{
		POSITIVE = 0,
		NEGATIVE = 1
	}

	private enum CHANGE_STATE
	{
		NONE = 0,
		CHANGE = 1,
		FINISHED = 2
	}

	private enum CHECK_STATE : byte
	{
		ON = 0,
		OFF = 1
	}

	private sealed class CallbackInfo : UnityEvent<SELECT_ITEM, int>
	{
	}

	private const float WEB_VIEW_SEPARATE_HEIGHT = 35f;

	public const float BOTTOM_UI_OFFSET = 13f;

	private readonly string[] CHECK_IMAGE_NAME = new string[2] { "notice_check", "null" };

	private readonly Vector4 CHECK_POSITION = new Vector4(-143f, 158f, 330f, 62f);

	private readonly Vector4 BUTTON_POSITION = new Vector4(0f, 60f, 180f, 84f);

	private CallbackInfo mMultiPageCallback = new CallbackInfo();

	private int mPageRemaining;

	private SELECT_ITEM mSelectItem = SELECT_ITEM.NEGATIVE;

	private BoxCollider mCollider;

	private string mURL;

	private StatusManager<CHANGE_STATE> mChangeStatus = new StatusManager<CHANGE_STATE>(CHANGE_STATE.NONE);

	private UIButton mButton;

	private UIButton mCheckButton;

	private UISprite mCheckSprite;

	private UISprite mBottomBarL;

	private UISprite mBottomBarR;

	private float mMarginOffset;

	private bool mHaveOtherPage;

	public bool IsCheckState { get; private set; }

	public bool CanShowAutoNotice { get; private set; }

	public void Init(string title, int page_count, string url, bool is_visible_cancel_button, bool have_other_page)
	{
		base.Title = title;
		mPageRemaining = page_count;
		mURL = url;
		base.IsInvisibleCancel = !is_visible_cancel_button;
		mHaveOtherPage = have_other_page;
		if (mChangeStatus != null)
		{
			mChangeStatus.Change(CHANGE_STATE.NONE);
		}
	}

	public void Init(string title, int page_count, string url, bool is_visible_cancel_button, Color back_ground_color, bool have_other_page)
	{
		Init(back_ground_color);
		base.Title = title;
		mPageRemaining = page_count;
		mURL = url;
		base.IsInvisibleCancel = !is_visible_cancel_button;
		mHaveOtherPage = have_other_page;
		if (mChangeStatus != null)
		{
			mChangeStatus.Change(CHANGE_STATE.NONE);
		}
	}

	public override void Update()
	{
		base.Update();
		if (mChangeStatus != null)
		{
			if (mChangeStatus.GetStatus() == CHANGE_STATE.NONE && !IsEqualStatus(STATE.OPEN))
			{
				mChangeStatus.Change(CHANGE_STATE.CHANGE);
			}
			mChangeStatus.Update();
		}
	}

	private void LateUpdate()
	{
		if (mChangeStatus != null && mChangeStatus.GetStatus() == CHANGE_STATE.CHANGE && !string.IsNullOrEmpty(mURL) && !base.IsBusy)
		{
			mChangeStatus.Change(CHANGE_STATE.FINISHED);
			LoadURL(mURL);
		}
	}

	public override void BuildDialog()
	{
		CanShowAutoNotice = true;
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		Vector2 vector = Util.LogScreenSize();
		float num = (0f - vector.y) * 0.5f;
		Camera component = GameObject.Find("Camera").GetComponent<Camera>();
		CalculateAnchorPos();
		if (--mPageRemaining <= 0 && !mHaveOtherPage)
		{
			Vector4 cHECK_POSITION = CHECK_POSITION;
			float num2 = cHECK_POSITION.y + num + vector.y * 0.5f;
			Vector4 cHECK_POSITION2 = CHECK_POSITION;
			mMarginOffset = num2 + cHECK_POSITION2.w * 0.5f;
			IsCheckState = true;
			string text = CHECK_IMAGE_NAME[0];
			Vector4 cHECK_POSITION3 = CHECK_POSITION;
			float x = cHECK_POSITION3.x;
			Vector4 cHECK_POSITION4 = CHECK_POSITION;
			Vector3 position = new Vector3(x, num + cHECK_POSITION4.y + mAnchorBottomPos.y);
			mCheckButton = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(mCheckButton, text, text, text, mBaseDepth + 6, position, Vector3.one);
			Util.AddImageButtonMessage(mCheckButton, this, "OnTogglePushed", UIButtonMessage.Trigger.OnClick);
			mCollider = mCheckButton.GetComponent<BoxCollider>();
			if ((bool)mCollider)
			{
				mCollider.center = new Vector3(115f, 0f, 0f);
				BoxCollider boxCollider = mCollider;
				Vector4 cHECK_POSITION5 = CHECK_POSITION;
				float z = cHECK_POSITION5.z;
				Vector4 cHECK_POSITION6 = CHECK_POSITION;
				boxCollider.size = new Vector3(z, cHECK_POSITION6.w, 1f);
			}
			else
			{
				mCheckButton.gameObject.AddComponent<BoxCollider>();
			}
			string text2 = string.Format(Localization.Get("Webview_Checkbox_Desc"));
			UIFont atlasFont = GameMain.LoadFont();
			UILabel uILabel = Util.CreateLabel("Checkbox_Desc", mCheckButton.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, text2, mBaseDepth + 5, new Vector3(40f, 0f, 0f), 26, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			mCheckSprite = Util.CreateSprite("Check", base.gameObject, image);
			Util.SetSpriteInfo(mCheckSprite, "notice_check_panel", mBaseDepth + 5, position, Vector3.one, false);
			Vector4 bUTTON_POSITION = BUTTON_POSITION;
			float x2 = bUTTON_POSITION.x;
			Vector4 bUTTON_POSITION2 = BUTTON_POSITION;
			position = new Vector3(x2, num + bUTTON_POSITION2.y + mAnchorBottomPos.y);
			text = "LC_button_close2";
			mButton = Util.CreateJellyImageButton("CloseButton", base.gameObject, image);
			Util.SetImageButtonInfo(mButton, text, text, text, mBaseDepth + 5, position, Vector3.one);
			Util.AddImageButtonMessage(mButton, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		}
		else
		{
			Vector4 bUTTON_POSITION3 = BUTTON_POSITION;
			float num3 = bUTTON_POSITION3.y + num + vector.y * 0.5f;
			Vector4 bUTTON_POSITION4 = BUTTON_POSITION;
			mMarginOffset = num3 + bUTTON_POSITION4.w * 0.5f + 35f;
			Vector4 bUTTON_POSITION5 = BUTTON_POSITION;
			float x3 = bUTTON_POSITION5.x;
			Vector4 bUTTON_POSITION6 = BUTTON_POSITION;
			Vector3 position2 = new Vector3(x3, num + bUTTON_POSITION6.y + mAnchorBottomPos.y);
			string text3 = "LC_button_next";
			mButton = Util.CreateJellyImageButton("ButtonPositive", base.gameObject, image);
			Util.SetImageButtonInfo(mButton, text3, text3, text3, mBaseDepth + 5, position2, Vector3.one);
			Util.AddImageButtonMessage(mButton, this, "OnPositivePushed", UIButtonMessage.Trigger.OnClick);
		}
		float y = (0f - vector.y) * 0.5f - 13f;
		mBottomBarR = Util.CreateSprite("BottomBarR", base.gameObject, image);
		Util.SetSpriteInfo(mBottomBarR, "menubar_frame", mBaseDepth + 4, new Vector3(-2f, y, 0f), new Vector3(1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		mBottomBarL = Util.CreateSprite("BottomBarL", base.gameObject, image);
		Util.SetSpriteInfo(mBottomBarL, "menubar_frame", mBaseDepth + 4, new Vector3(2f, y, 0f), new Vector3(-1f, 1f, 1f), false, UIWidget.Pivot.BottomLeft);
		SetWebViewType(WEBVIEW_TYPE.ADJUST_MIN_WIDTH_VERTICAL_RATE, (vector.y - mMarginOffset - mAnchorBottomPos.y) / vector.y);
		base.BuildDialog();
	}

	public void ResumeStateWithReload()
	{
		base.ResumeState();
		if (IsEqualWebViewStatus(WEBVIEW_STATE.ERROR))
		{
			Reload();
		}
	}

	public void ClosePage(bool show_auto_notice = true)
	{
		mPageRemaining = 0;
		CanShowAutoNotice = show_auto_notice;
		Close();
	}

	protected override void SetLayout(ScreenOrientation o)
	{
		base.RemarginInLayout = false;
		base.SetLayout(o);
		Vector2 log_screen = Util.LogScreenSize();
		float num = (0f - log_screen.y) * 0.5f;
		if (mButton != null)
		{
			Transform obj = mButton.transform;
			Vector4 bUTTON_POSITION = BUTTON_POSITION;
			float x = bUTTON_POSITION.x;
			float num2 = num - 13f;
			Vector4 bUTTON_POSITION2 = BUTTON_POSITION;
			obj.localPosition = new Vector3(x, num2 + bUTTON_POSITION2.y + mAnchorBottomPos.y);
		}
		if (mCheckButton != null)
		{
			Transform obj2 = mCheckButton.transform;
			Vector4 cHECK_POSITION = CHECK_POSITION;
			float x2 = cHECK_POSITION.x;
			float num3 = num - 13f;
			Vector4 cHECK_POSITION2 = CHECK_POSITION;
			obj2.localPosition = new Vector3(x2, num3 + cHECK_POSITION2.y + mAnchorBottomPos.y);
		}
		if (mCheckSprite != null)
		{
			Transform obj3 = mCheckSprite.transform;
			Vector4 cHECK_POSITION3 = CHECK_POSITION;
			float x3 = cHECK_POSITION3.x;
			float num4 = num - 13f;
			Vector4 cHECK_POSITION4 = CHECK_POSITION;
			obj3.localPosition = new Vector3(x3, num4 + cHECK_POSITION4.y + mAnchorBottomPos.y);
		}
		if (mBottomBarL != null)
		{
			mBottomBarL.transform.localPosition = new Vector3(2f, num - 13f + mAnchorBottomPos.y - 123f, 0f);
		}
		if (mBottomBarR != null)
		{
			mBottomBarR.transform.localPosition = new Vector3(-2f, num - 13f + mAnchorBottomPos.y - 123f, 0f);
		}
		float header_height = (float)(-mTitleBarR.height) - 50f - 123f - mAnchorTopPos.y;
		SetWebViewType(WEBVIEW_TYPE.ADJUST_MIN_WIDTH_VERTICAL_RATE, (log_screen.y - mMarginOffset - mAnchorBottomPos.y) / log_screen.y);
		SetWebViewMargin(header_height, log_screen);
	}

	public override void OnCloseFinished()
	{
		if (mIsClickDeepLink)
		{
			mIsClickDeepLink = false;
			mPageRemaining = 0;
		}
		InvokeMultiPageCallback(mSelectItem, mPageRemaining);
		Object.Destroy(base.gameObject);
	}

	public void OnTogglePushed(GameObject go)
	{
		IsCheckState = !IsCheckState;
		string text = ((!IsCheckState) ? CHECK_IMAGE_NAME[1] : CHECK_IMAGE_NAME[0]);
		Util.SetImageButtonGraphic(go.GetComponent<UIButton>(), text, text, text);
		if (mCollider != null)
		{
			mCollider.center = new Vector3(115f, 0f, 0f);
			mCollider.size = new Vector3(330f, 62f, 1f);
		}
	}

	public void OnPositivePushed(GameObject go)
	{
		if (!base.IsBusy)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			mSelectItem = SELECT_ITEM.POSITIVE;
			ShowWebview(false);
			Close();
		}
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		base.OnScreenChanged(o);
	}

	public override void OnCancelPushed(GameObject go)
	{
		mPageRemaining = 0;
		base.OnCancelPushed(go);
	}

	private void ExecMultiPageCallbackMethod(UnityAction<UnityAction<SELECT_ITEM, int>> method, params UnityAction<SELECT_ITEM, int>[] callback)
	{
		if (method == null || callback == null)
		{
			return;
		}
		for (int i = 0; i < callback.Length; i++)
		{
			if (callback[i] != null)
			{
				method(callback[i]);
			}
		}
	}

	private void InvokeMultiPageCallback(SELECT_ITEM item, int page)
	{
		if (mMultiPageCallback != null)
		{
			mMultiPageCallback.Invoke(item, page);
		}
	}

	public void SetMultiPageCallback(params UnityAction<SELECT_ITEM, int>[] callback)
	{
		ClearMultiPageCallback();
		AddMultiPageCallback(callback);
	}

	public void AddMultiPageCallback(params UnityAction<SELECT_ITEM, int>[] callback)
	{
		if (mMultiPageCallback != null)
		{
			ExecMultiPageCallbackMethod(mMultiPageCallback.AddListener, callback);
		}
	}

	public void ClearMultiPageCallback()
	{
		if (mMultiPageCallback != null)
		{
			mMultiPageCallback.RemoveAllListeners();
		}
	}

	public void RemoveMultiPageCallback(params UnityAction<SELECT_ITEM, int>[] callback)
	{
		if (mMultiPageCallback != null)
		{
			ExecMultiPageCallbackMethod(mMultiPageCallback.RemoveListener, callback);
		}
	}
}
