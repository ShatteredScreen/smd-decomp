using UnityEngine;

public class GetStampDialog : DialogBase
{
	public enum SELECT_ITEM
	{
		OK = 0,
		GROWUP = 1
	}

	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	private enum ITEMTYPE
	{
		DIALOG = 0,
		GROW_PIECE = 1,
		HEART_PIECE = 2
	}

	public delegate void OnDialogClosed(SELECT_ITEM i);

	private SELECT_ITEM mSelectItem;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private ITEMTYPE mItemtype;

	private OnDialogClosed mCallback;

	private AccessoryData mData;

	private string mIconName = "null";

	private string mTitle = string.Empty;

	private string mText = string.Empty;

	private string mLastStage = string.Empty;

	private string mImagename = string.Empty;

	private int KeyCount;

	private bool mIsGotGem;

	private int mCount;

	private int mNumCount;

	private float mXleft;

	private float mYdown;

	private bool mStampComplete;

	private bool mOKEnable = true;

	private bool mStampEnable = true;

	private bool mStampLastEnable = true;

	private UISsSprite mUISsAnime;

	private BIJImage atlas;

	private UILabel mLabelText;

	private int mPlaySeFlg;

	private float mCounter;

	private float mStampCounter;

	public AccessoryData Data
	{
		get
		{
			return mData;
		}
	}

	public bool CanRepeat { get; set; }

	public override void Start()
	{
		base.Start();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}

	public void Init(AccessoryData a_data, int count, bool a_addAccessory = true, bool GrowResultExtra = false, bool Growup_piece = true, int multicount = 0, int get_type = -1)
	{
		mSelectItem = SELECT_ITEM.OK;
		mData = a_data;
		mCount = count;
		Def.CONSUME_ID cONSUME_ID = Def.CONSUME_ID.NONE;
		if (mCount == 0)
		{
			if (!GrowResultExtra)
			{
				mGame.mPlayer.AddAccessory(mData, a_addAccessory);
				mCount = a_data.GetGotIDByNum();
			}
			else if (Growup_piece)
			{
				mCount = mGame.mPlayer.GrowUpPiece;
			}
			else
			{
				mCount = mGame.mPlayer.HeartUpPiece;
			}
			if (a_data != null && a_data.AccessoryType == AccessoryData.ACCESSORY_TYPE.HEART_PIECE)
			{
				mTitle = "GetAccessory_Heart_Piece_Desc";
				mText = string.Format(Localization.Get("Stamp_Get_Desc_Heart"));
				mImagename = "icon_heart";
				mItemtype = ITEMTYPE.HEART_PIECE;
				cONSUME_ID = Def.CONSUME_ID.HEART_PIECE;
			}
			else if (Growup_piece)
			{
				mTitle = "GetAccessory_Growup_Piece_Desc";
				mText = Localization.Get("Stamp_Get_Desc_Level");
				mImagename = "icon_level";
				mItemtype = ITEMTYPE.GROW_PIECE;
				cONSUME_ID = Def.CONSUME_ID.GROWUP_PIECE;
			}
			else
			{
				mTitle = "GetAccessory_Heart_Piece_Desc";
				mText = Localization.Get("Stamp_Get_Desc_Heart");
				mImagename = "icon_heart";
				mItemtype = ITEMTYPE.HEART_PIECE;
				cONSUME_ID = Def.CONSUME_ID.HEART_PIECE;
			}
		}
		else
		{
			if (!GrowResultExtra)
			{
				mGame.mPlayer.AddAccessory(mData, a_addAccessory);
			}
			if (!Growup_piece)
			{
				mTitle = "GetAccessory_Heart_Piece_Desc";
				mText = string.Format(Localization.Get("Stamp_Get_Desc_Heart"));
				mImagename = "icon_heart";
				mItemtype = ITEMTYPE.HEART_PIECE;
				cONSUME_ID = Def.CONSUME_ID.HEART_PIECE;
			}
			else
			{
				mTitle = "GetAccessory_Growup_Piece_Desc";
				mText = Localization.Get("Stamp_Get_Desc_Level");
				mImagename = "icon_level";
				mItemtype = ITEMTYPE.GROW_PIECE;
				cONSUME_ID = Def.CONSUME_ID.GROWUP_PIECE;
			}
			mCount = multicount;
		}
		if (get_type != -1 && cONSUME_ID != 0)
		{
			ServerCram.ArcadeGetItem(mCount, (int)mGame.mPlayer.Data.LastPlaySeries, mGame.mPlayer.Data.LastPlayLevel, (int)mGame.mPlayer.AdvSaveData.LastPlaySeries, mGame.mPlayer.AdvSaveData.LastPlayLevel, 1, (int)cONSUME_ID, 1, get_type, mGame.mPlayer.AdvLastStage, mGame.mPlayer.AdvMaxStamina);
		}
		mGame.Save();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		if (mCount != 0)
		{
			mStampCounter += Time.deltaTime;
			if (mItemtype == ITEMTYPE.GROW_PIECE)
			{
				if (mStampCounter > 0.2f)
				{
					mNumCount++;
					if (mPlaySeFlg == 0)
					{
						mGame.PlaySe("SE_DAILY_PRESS_STAMP", -1, 1f, 0.5f);
						mPlaySeFlg = 1;
					}
					else if (mPlaySeFlg == 1)
					{
						mGame.PlaySe("SE_DAILY_PRESS_STAMP", 1, 1f, 0.5f);
						mPlaySeFlg = 2;
					}
					else if (mPlaySeFlg == 2)
					{
						mGame.PlaySe("SE_DAILY_PRESS_STAMP", 3, 1f, 0.5f);
						mPlaySeFlg = 3;
					}
					else
					{
						mGame.PlaySe("SE_DAILY_PRESS_STAMP", 2, 1f, 0.5f);
						mPlaySeFlg = 0;
					}
					mUISsAnime = Util.CreateGameObject("GrowHalo", base.gameObject).AddComponent<UISsSprite>();
					mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_STAMP_GROWUP").SsAnime;
					mUISsAnime.depth = mBaseDepth + 4;
					mUISsAnime.transform.localPosition = new Vector3(mXleft, mYdown, 0f);
					mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
					mUISsAnime.PlayCount = 1;
					mUISsAnime.Play();
					mXleft += 70f;
					if (mNumCount > 5)
					{
						mYdown -= 70f;
						mXleft = -175f;
						mNumCount = 0;
					}
					mStampCounter = 0f;
					mCount--;
					if (-175f >= mYdown)
					{
						mStampComplete = true;
						mCount = 0;
					}
				}
			}
			else if (mItemtype == ITEMTYPE.HEART_PIECE && mStampCounter > 0.2f)
			{
				mNumCount++;
				if (mPlaySeFlg == 0)
				{
					mGame.PlaySe("SE_DAILY_PRESS_STAMP", -1, 1f, 0.5f);
					mPlaySeFlg = 1;
				}
				else if (mPlaySeFlg == 1)
				{
					mGame.PlaySe("SE_DAILY_PRESS_STAMP", 1, 1f, 0.5f);
					mPlaySeFlg = 2;
				}
				else if (mPlaySeFlg == 2)
				{
					mGame.PlaySe("SE_DAILY_PRESS_STAMP", 3, 1f, 0.5f);
					mPlaySeFlg = 3;
				}
				else
				{
					mGame.PlaySe("SE_DAILY_PRESS_STAMP", 2, 1f, 0.5f);
					mPlaySeFlg = 0;
				}
				mUISsAnime = Util.CreateGameObject("HeartHalo", base.gameObject).AddComponent<UISsSprite>();
				mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_STAMP_HEART").SsAnime;
				mUISsAnime.depth = mBaseDepth + 4;
				mUISsAnime.transform.localPosition = new Vector3(mXleft, mYdown, 0f);
				mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
				mUISsAnime.PlayCount = 1;
				mUISsAnime.Play();
				mXleft += 88f;
				if (mNumCount > 4)
				{
					mYdown -= 88f;
					mXleft = -176f;
					mNumCount = 0;
				}
				mStampCounter = 0f;
				mCount--;
				if (-176f >= mYdown)
				{
					mStampComplete = true;
					mCount = 0;
				}
			}
		}
		if (mCount == 0)
		{
			mCounter += Time.deltaTime;
			if (mOKEnable && mCounter > 1.1f)
			{
				if (mStampComplete)
				{
					mGame.PlaySe("SE_DIALY_COMPLETE_STAMP", -1);
					mGame.PlaySe("VOICE_029", 2);
					mUISsAnime = Util.CreateGameObject("GrowHalo", base.gameObject).AddComponent<UISsSprite>();
					mUISsAnime.Animation = ResourceManager.LoadSsAnimation("EFFECT_STAMP_COMP").SsAnime;
					mUISsAnime.depth = mBaseDepth + 4;
					mUISsAnime.transform.localPosition = new Vector3(0f, 8f, 0f);
					mUISsAnime.transform.localScale = new Vector3(1f, 1f, 1f);
					mUISsAnime.PlayCount = 1;
					mUISsAnime.Play();
					mStampComplete = false;
					mCounter = -2f;
				}
				else
				{
					string text = "LC_button_ok2";
					UIButton button = Util.CreateJellyImageButton("ButtonOK", base.gameObject, atlas);
					Util.SetImageButtonInfo(button, text, text, text, mBaseDepth + 3, new Vector3(0f, -227f, 0f), Vector3.one);
					Util.AddImageButtonMessage(button, this, "OnClosePushed", UIButtonMessage.Trigger.OnClick);
					mOKEnable = false;
				}
			}
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		base.gameObject.transform.localPosition = new Vector3(0f, 0f, mBaseZ);
		atlas = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(string.Empty);
		UILabel uILabel = Util.CreateLabel("Title", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, string.Empty, mBaseDepth + 135, new Vector3(41f, 286f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_DIALOG_TITLE_COLOR;
		string text = Util.MakeLText(mTitle);
		Util.SetLabelText(uILabel, text);
		if (mItemtype == ITEMTYPE.GROW_PIECE)
		{
			UISprite sprite = Util.CreateSprite("TitleIcon", base.gameObject, atlas);
			Util.SetSpriteInfo(sprite, mImagename, mBaseDepth + 135, new Vector3(-150f, 292f, 0f), Vector3.one, false);
			UISprite uISprite = Util.CreateSprite("BoosterBoard", base.gameObject, atlas);
			Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, -4f, 0f), Vector3.one, false);
			uISprite.SetDimensions(510, 325);
			uISprite.type = UIBasicSprite.Type.Sliced;
			UISprite uISprite2 = Util.CreateSprite("Frame2", base.gameObject, atlas);
			mXleft = -175f;
			mYdown = 105f;
			int num = mCount;
			int growUpPiece = mGame.mPlayer.GrowUpPiece;
			growUpPiece -= num;
			if (mGame.mPlayer.GrowUpPiece >= 24)
			{
				mGame.mPlayer.SubGrowupPiece(24);
				mGame.mPlayer.AddGrowup(1);
				mSelectItem = SELECT_ITEM.GROWUP;
				mGame.Save();
				int lastPlaySeries = (int)mGame.mPlayer.Data.LastPlaySeries;
				int lastPlayLevel = mGame.mPlayer.Data.LastPlayLevel;
				int lastPlaySeries2 = (int)mGame.mPlayer.AdvSaveData.LastPlaySeries;
				int lastPlayLevel2 = mGame.mPlayer.AdvSaveData.LastPlayLevel;
				int advLastStage = mGame.mPlayer.AdvLastStage;
				int advMaxStamina = mGame.mPlayer.AdvMaxStamina;
				ServerCram.ArcadeGetItem(1, lastPlaySeries, lastPlayLevel, lastPlaySeries2, lastPlayLevel2, 1, 10, 1, 30, advLastStage, advMaxStamina);
			}
			for (int i = 0; 4 > i; i++)
			{
				for (int j = 0; 6 > j; j++)
				{
					uISprite = Util.CreateSprite("StampBack", uISprite2.gameObject, atlas);
					Util.SetSpriteInfo(uISprite, "dailyEvent_stamppanel", mBaseDepth + 2, new Vector3(mXleft, mYdown, 0f), Vector3.one, false);
					uISprite.color = new Color(0.95686275f, 81f / 85f, 0.8784314f);
					uISprite.SetDimensions(66, 66);
					mXleft += 70f;
				}
				mYdown -= 70f;
				mXleft = -175f;
			}
			mXleft = -175f;
			mYdown = 105f;
			mNumCount = 0;
			for (int k = 0; growUpPiece > k; k++)
			{
				mNumCount++;
				uISprite = Util.CreateSprite("IconImage", uISprite2.gameObject, atlas);
				Util.SetSpriteInfo(uISprite, "icon_dailyEvent_stamp00", mBaseDepth + 3, new Vector3(mXleft, mYdown, 0f), Vector3.one, false);
				mXleft += 70f;
				if (mNumCount > 5)
				{
					mYdown -= 70f;
					mXleft = -175f;
					mNumCount = 0;
				}
			}
			UILabel uILabel2 = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel2, mText, mBaseDepth + 2, new Vector3(0f, 196f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel2.color = Def.DEFAULT_MESSAGE_COLOR;
		}
		else if (mItemtype == ITEMTYPE.HEART_PIECE)
		{
			UISprite uISprite3 = Util.CreateSprite("TitleIcon", base.gameObject, atlas);
			Util.SetSpriteInfo(uISprite3, mImagename, mBaseDepth + 135, new Vector3(-153f, 291f, 0f), Vector3.one, false);
			uISprite3.SetDimensions(74, 74);
			UISprite uISprite4 = Util.CreateSprite("BoosterBoard", base.gameObject, atlas);
			Util.SetSpriteInfo(uISprite4, "instruction_panel2", mBaseDepth + 2, new Vector3(0f, -4f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(510, 325);
			uISprite4.type = UIBasicSprite.Type.Sliced;
			UISprite uISprite5 = Util.CreateSprite("Frame2", base.gameObject, atlas);
			mXleft = -176f;
			mYdown = 88f;
			int num2 = mCount;
			int heartUpPiece = mGame.mPlayer.HeartUpPiece;
			heartUpPiece -= num2;
			for (int l = 0; 3 > l; l++)
			{
				for (int m = 0; 5 > m; m++)
				{
					uISprite4 = Util.CreateSprite("StampBack", uISprite5.gameObject, atlas);
					Util.SetSpriteInfo(uISprite4, "dailyEvent_stamppanel", mBaseDepth + 2, new Vector3(mXleft, mYdown, 0f), Vector3.one, false);
					mXleft += 88f;
					uISprite4.color = new Color(49f / 51f, 0.9137255f, 0.9372549f);
					uISprite4.SetDimensions(86, 86);
				}
				mYdown -= 88f;
				mXleft = -176f;
			}
			mXleft = -176f;
			mYdown = 88f;
			mNumCount = 0;
			for (int n = 0; heartUpPiece > n; n++)
			{
				mNumCount++;
				uISprite4 = Util.CreateSprite("IconImage", uISprite5.gameObject, atlas);
				Util.SetSpriteInfo(uISprite4, "icon_dailyEvent_stamp01", mBaseDepth + 3, new Vector3(mXleft, mYdown, 0f), Vector3.one, false);
				mXleft += 88f;
				if (mNumCount > 4)
				{
					mYdown -= 88f;
					mXleft = -176f;
					mNumCount = 0;
				}
			}
			UILabel uILabel3 = Util.CreateLabel("Text", base.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel3, mText, mBaseDepth + 2, new Vector3(0f, 196f, 0f), 26, 0, 0, UIWidget.Pivot.Center);
			uILabel3.color = new Color(0.3529412f, 37f / 85f, 3f / 85f);
		}
		mGame.PlaySe("SE_ACCESSORY_UNLOCK", -1);
	}

	public void OnClosePushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			Close();
		}
	}

	public override void Close()
	{
		base.Close();
	}

	public override void OnOpenFinished()
	{
		base.OnOpenFinished();
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback(mSelectItem);
		}
	}

	public override void OnBackKeyPress()
	{
		if (!mOKEnable)
		{
			OnClosePushed();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetInputEnabled(bool _enable)
	{
		if (_enable)
		{
			mState.Reset(STATE.MAIN, true);
		}
		else
		{
			mState.Reset(STATE.WAIT, true);
		}
	}
}
