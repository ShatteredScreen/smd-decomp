using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;
using UnityEngine;

public class PurchaseDialog : DialogBase
{
	public enum STATE
	{
		INIT = 0,
		AUTH_CHECK = 1,
		AUTH_CHECK_WAIT = 2,
		IS_PURCHASE_POSSIBLE = 3,
		GET_TIER_INFO = 4,
		GET_BIRTHDAY = 5,
		INPUT_BIRTHDAY = 6,
		SET_BIRTHDAY = 7,
		RESTORE_TRANSACTIONS_CHECK = 8,
		RESTORE_TRANSACTIONS = 9,
		TIER_SELECT = 10,
		PURCHASE_CHECK = 11,
		PURCHASE = 12,
		CONSUME_PRODUCT = 13,
		ADD_GEM = 14,
		PURCHASE_END = 15,
		UPDATE_GEM = 16,
		CLOSE = 17,
		ERROR = 18,
		FAIL = 19,
		WAIT = 20,
		EDITOR_WAIT = 21,
		AUTHERROR_WAIT = 22
	}

	public enum FAILKIND
	{
		NO = 0,
		FAIL = 1,
		CANCEL = 2,
		MAXOVER = 3,
		TIME = 4,
		PARETNAL_CTRL = 5
	}

	public enum ITEMKIND
	{
		DOLLAR = 0,
		REWARD_NOAH = 1,
		REWARD_ADWAYS = 2
	}

	public delegate void OnDialogClosed();

	public const int IAP_CHEAT_NO = 255;

	public const string IAP_CHEAT_ORDER_NAME = "Cheat_itemID";

	public const string IAP_CHEAT_ORDER_ID = "Cheat_orderID";

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.INIT);

	protected List<GooglePurchase> mNotConsumedGPList = new List<GooglePurchase>();

	protected Dictionary<string, GoogleSkuInfo> mGoogleSkuInfoDict = new Dictionary<string, GoogleSkuInfo>();

	private List<SMVerticalListItem> mTierList = new List<SMVerticalListItem>();

	protected SMVerticalList mTierSelectList;

	private static SMVerticalListInfo TierListInfo = new SMVerticalListInfo(1, 510f, 420f, 1, 510f, 420f, 1, 126);

	private static SMVerticalListInfo TierListInfoEn = new SMVerticalListInfo(1, 510f, 400f, 1, 510f, 400f, 1, 126);

	protected bool mPurchasePossible;

	protected string mBirthday;

	protected GameObject mCenterGameobject;

	protected int mCenterIndex;

	protected int mCenterScrollCount;

	protected BIJImage mAtlasMailBox;

	protected OnDialogClosed mCallback;

	protected ConnectAuthParam.OnUserTransferedHandler mAuthErrorCallback;

	protected EULADialog mEULADialog;

	public PItem selectPItem;

	protected GameMain.PurchaseInfo purchaseInfo;

	public bool purchasing;

	public bool purchased;

	public int resultflag;

	public bool timeoutflag;

	public bool isConnnection;

	public bool isSucess;

	public bool isFailProducts;

	protected float PurchaseStartTime;

	protected string[] successItemID;

	protected ConfirmDialog mConfirmDialog;

	protected string mMessage;

	protected float waittime;

	protected float duration;

	protected float lastTime;

	protected bool purchaseFailed;

	public FAILKIND failKind;

	public readonly string[] failTextTbl = new string[6] { "Purchase fail", "Purchase fail", "Purchase cancel", "Cant Purchase Max SD", "Cant Purchase in continuity", "ParentalControl" };

	public int itemKind;

	private static List<string> IconList = new List<string> { "icon_currency", "icon_currency2", "icon_currency3", "icon_currency4", "icon_currency5", "icon_currency6", "icon_currency7" };

	public override void Start()
	{
		base.Start();
		CleanupManager();
		InitManager();
	}

	public void Init(string message)
	{
		mMessage = message;
	}

	public override IEnumerator BuildResource()
	{
		ResImage image = ResourceManager.LoadImageAsync("MAIL_BOX");
		while (image == null || image.LoadState != ResourceInstance.LOADSTATE.DONE)
		{
			yield return null;
		}
		mAtlasMailBox = image.Image;
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("Purchase_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		UILabel uILabel = Util.CreateLabel("Caution", base.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Purchase_Caution_Decs_En"), mBaseDepth + 5, new Vector3(-240f, -230f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = new Color32(242, 55, 117, byte.MaxValue);
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnOpenFinished()
	{
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.TIER_SELECT && !GetBusy())
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public void SetAuthErrorClosedCallback(ConnectAuthParam.OnUserTransferedHandler callback)
	{
		mAuthErrorCallback = callback;
	}

	public void OnBuyPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.TIER_SELECT)
		{
			string[] array = go.name.Split('@');
			PItem item = PurchaseManager.instance.GetItem(array[1]);
			mGame.PlaySe("SE_POSITIVE", -1);
			selectPItem = item;
			purchasing = true;
			mState.Change(STATE.PURCHASE_CHECK);
		}
	}

	protected void Purchase()
	{
		PurchaseStartTime = -1f;
		GoogleSkuInfo googleSkuInfo = mGoogleSkuInfoDict[selectPItem.itemID];
		if (googleSkuInfo != null)
		{
			PurchaseStartTime = Time.realtimeSinceStartup;
			string developerPayload = GameMain.MakeDeveloperPayload();
			GoogleIAB.purchaseProduct(selectPItem.itemID, developerPayload);
			isConnnection = true;
			isSucess = false;
			Server.ManualConnecting = true;
		}
		else
		{
			failKind = FAILKIND.FAIL;
		}
	}

	protected void DialogBaseUpdate()
	{
		base.Update();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		case STATE.INIT:
			selectPItem = null;
			purchasing = false;
			mState.Change(STATE.AUTH_CHECK);
			break;
		case STATE.AUTH_CHECK:
			mGame.mPlayer.Data.LastGetAuthTime = DateTimeUtil.UnitLocalTimeEpoch;
			mGame.Network_UserAuth();
			mState.Change(STATE.AUTH_CHECK_WAIT);
			break;
		case STATE.AUTH_CHECK_WAIT:
		{
			if (!GameMain.mUserAuthCheckDone)
			{
				break;
			}
			if (GameMain.mUserAuthSucceed)
			{
				if (!GameMain.mUserAuthTransfered)
				{
					mState.Change(STATE.IS_PURCHASE_POSSIBLE);
				}
				else
				{
					mState.Change(STATE.AUTHERROR_WAIT);
				}
				break;
			}
			Server.ErrorCode mUserAuthErrorCode = GameMain.mUserAuthErrorCode;
			if (mUserAuthErrorCode == Server.ErrorCode.MAINTENANCE_MODE)
			{
				mGame.CreateConnectErrorDialog(ConnectCommonErrorDialog.STYLE.MAINTENANCE_CLOSE, 1, OnConnectErrorAuthRetry, OnConnectErrorAuthAbandon);
				mState.Change(STATE.WAIT);
			}
			else
			{
				CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), STATE.CLOSE);
			}
			break;
		}
		case STATE.IS_PURCHASE_POSSIBLE:
			mPurchasePossible = PurchaseManager.instance.CanMakePayments();
			if (!mPurchasePossible)
			{
				CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("ParentalControl"), STATE.CLOSE);
			}
			else
			{
				mState.Change(STATE.GET_TIER_INFO);
			}
			break;
		case STATE.GET_TIER_INFO:
			if (mState.IsChanged())
			{
				PurchaseManager.instance.RequestProductData();
				mGame.StartConnecting();
				isSucess = false;
				isConnnection = true;
			}
			else
			{
				if (isConnnection)
				{
					break;
				}
				mGame.FinishConnecting();
				if (isSucess)
				{
					if (purchaseFailed)
					{
						mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
						purchaseFailed = false;
					}
					else
					{
						CreateList();
						mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase fail products"), STATE.CLOSE);
				}
			}
			break;
		case STATE.GET_BIRTHDAY:
			if (mState.IsChanged())
			{
				mGame.Network_GetBirthday();
				mGame.StartConnecting();
			}
			else
			{
				if (!GameMain.mGetBirthdayCheckDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mGetBirthdaySucceed)
				{
					if (string.IsNullOrEmpty(GameMain.mGetBirthdayValue))
					{
						mState.Change(STATE.INPUT_BIRTHDAY);
					}
					else
					{
						mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), STATE.CLOSE);
				}
			}
			break;
		case STATE.INPUT_BIRTHDAY:
			if (mState.IsChanged())
			{
				AgeConfirmationDialog ageConfirmationDialog = Util.CreateGameObject("InputBirthdayDialog", base.transform.parent.gameObject).AddComponent<AgeConfirmationDialog>();
				ageConfirmationDialog.SetClosedCallback(delegate(string birthday)
				{
					mBirthday = birthday;
					mState.Change(STATE.SET_BIRTHDAY);
				});
			}
			break;
		case STATE.SET_BIRTHDAY:
			if (mState.IsChanged())
			{
				mGame.Network_SetBirthday(mBirthday);
				mGame.StartConnecting();
			}
			else if (GameMain.mSetBirthdayCheckDone)
			{
				mGame.FinishConnecting();
				if (GameMain.mSetBirthdaySucceed)
				{
					mState.Change(STATE.RESTORE_TRANSACTIONS_CHECK);
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_SetBirthdayFailedDesc"), STATE.CLOSE);
				}
			}
			break;
		case STATE.RESTORE_TRANSACTIONS_CHECK:
			if (PurchaseManager.LogData.HasPLog())
			{
				PurchaseLog.PLog pLog = PurchaseManager.LogData.GetLastOnePLog();
				if (pLog.purchaseState == 100)
				{
					for (int j = 0; j < mNotConsumedGPList.Count; j++)
					{
						if (mNotConsumedGPList[j].productId == pLog.itemID)
						{
							PurchaseManager.LogData.PurchaseComplete();
							PurchaseManager.SavePurchaseData();
							pLog = null;
						}
					}
					if (pLog != null)
					{
						PurchaseManager.LogData.SetLastOnePLogStatus(PurchaseLog.PLog.State.COMPLETED);
						CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverDesc"), STATE.RESTORE_TRANSACTIONS);
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverDesc"), STATE.RESTORE_TRANSACTIONS);
				}
			}
			if (PurchaseManager.LogData.HasPLog())
			{
				break;
			}
			if (mNotConsumedGPList.Count > 0)
			{
				successItemID = new string[1];
				successItemID[0] = mNotConsumedGPList[0].productId;
				PItem item = PurchaseManager.instance.GetItem(mNotConsumedGPList[0].productId);
				if (item != null)
				{
					item.signature = mNotConsumedGPList[0].signature;
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverDesc"), STATE.CONSUME_PRODUCT);
				}
				else
				{
					mState.Change(STATE.TIER_SELECT);
				}
			}
			else
			{
				mState.Change(STATE.TIER_SELECT);
			}
			break;
		case STATE.RESTORE_TRANSACTIONS:
			if (mState.IsChanged())
			{
				PurchaseLog.PLog lastOnePLog = PurchaseManager.LogData.GetLastOnePLog();
				GameMain.mBuyGemPlace = ServerIAP.BuyGemPlace.TRANSACTION;
				mGame.Network_BuyGem(PurchaseManager.instance.GetItemIdx(lastOnePLog.itemID), lastOnePLog.receipt, lastOnePLog.signature);
				mGame.StartConnecting();
			}
			else if (GameMain.mBuyGemCheckDone)
			{
				mGame.FinishConnecting();
				if (GameMain.mBuyGemSucceed)
				{
					PurchaseManager.LogData.PurchaseComplete();
					PurchaseManager.SavePurchaseData();
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverSuccess"), STATE.RESTORE_TRANSACTIONS_CHECK);
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_RecoverTitle"), Localization.Get("Purchase_RecoverFail"), STATE.RESTORE_TRANSACTIONS);
				}
			}
			break;
		case STATE.TIER_SELECT:
			if (!mState.IsChanged())
			{
			}
			break;
		case STATE.PURCHASE_CHECK:
			if (mState.IsChanged())
			{
				CampaignSaleEntry iAPSale = mGame.SegmentProfile.GetIAPSale(selectPItem.ItemID);
				if (iAPSale == null)
				{
					CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail"), STATE.TIER_SELECT);
					break;
				}
				if (!iAPSale.InTime())
				{
					CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail time"), STATE.TIER_SELECT);
					break;
				}
				mGame.Network_ChargeCheck(selectPItem.ItemNo);
				mGame.StartConnecting();
			}
			else
			{
				if (!GameMain.mChargeCheckCheckDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mChargeCheckSucceed)
				{
					if (GameMain.mChargeCheckResult)
					{
						if (GameMain.mChargeCheckAgeType == 1 || GameMain.mChargeCheckAgeType == 2)
						{
							string desc = Localization.Get("Purchase_ParentConfirmDesc") + "\n[FF0000]" + Localization.Get("AgeConfimation_Desc03") + "[-]";
							ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
							confirmDialog.Init(Localization.Get("Purchase_ParentConfirmTitle"), desc, ConfirmDialog.CONFIRM_DIALOG_STYLE.IMAGE_CHOICE, DIALOG_SIZE.MEDIUM, "LC_button_purchase", "LC_button_disagree");
							confirmDialog.SetClosedCallback(delegate(ConfirmDialog.SELECT_ITEM i)
							{
								if (i == ConfirmDialog.SELECT_ITEM.POSITIVE)
								{
									mState.Change(STATE.PURCHASE);
								}
								else
								{
									mState.Change(STATE.TIER_SELECT);
								}
							});
							mState.Change(STATE.ERROR);
						}
						else
						{
							mState.Change(STATE.PURCHASE);
						}
					}
					else if (!GameMain.mChargeCheckLimitResult)
					{
						CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Cant Purchase Max SD"), STATE.TIER_SELECT);
					}
					else if (!GameMain.mChargeCheckAgeResult)
					{
						if (GameMain.mChargeCheckAgeType == 1)
						{
							CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Purchase_ChargeCheckFailed_AGE1"), STATE.TIER_SELECT);
						}
						else if (GameMain.mChargeCheckAgeType == 2)
						{
							CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Purchase_ChargeCheckFailed_AGE2"), STATE.TIER_SELECT);
						}
						else
						{
							CreateErrorMessage(Localization.Get("Purchase_ChargeCheckFailed"), Localization.Get("Purchase_ChargeCheckFailed_AGE3"), STATE.TIER_SELECT);
						}
					}
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), STATE.TIER_SELECT);
				}
			}
			break;
		case STATE.PURCHASE:
			if (mState.IsChanged())
			{
				Purchase();
			}
			else if (!isConnnection)
			{
				if (isSucess)
				{
					mState.Change(STATE.CONSUME_PRODUCT);
					break;
				}
				if (failKind == FAILKIND.CANCEL)
				{
					CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase cancel"), STATE.TIER_SELECT);
					break;
				}
				purchaseFailed = true;
				CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail"), STATE.GET_TIER_INFO);
			}
			break;
		case STATE.CONSUME_PRODUCT:
			if (mState.IsChanged())
			{
				GoogleIAB.consumeProducts(successItemID);
				isConnnection = true;
				isSucess = false;
			}
			else
			{
				if (isConnnection)
				{
					break;
				}
				if (isSucess)
				{
					if (mNotConsumedGPList.Count > 0)
					{
						mNotConsumedGPList.RemoveAt(0);
					}
					mState.Change(STATE.ADD_GEM);
				}
				else
				{
					CreateErrorMessage(Localization.Get("Purchase fail title"), Localization.Get("Purchase fail"), STATE.TIER_SELECT);
				}
			}
			break;
		case STATE.ADD_GEM:
			if (mState.IsChanged())
			{
				mGame.Network_BuyGem(purchaseInfo.IAPNo, purchaseInfo.ReceiptDataString, purchaseInfo.ReceiptSignature);
				mGame.StartConnecting();
				if (purchaseInfo != null)
				{
					mGame.mOptions.AddPurchaseHistory(DateTime.Now, (float)purchaseInfo.TierData.Cost, purchaseInfo.TierData.CurrencyCode);
					mGame.SaveOptions();
					mGame.mServerCramVariableData.mPurchased = true;
					mGame.SaveServerCramVariableData();
					UserBehavior.Instance.AddPurchase((short)purchaseInfo.IAPNo);
					UserBehavior.Save();
				}
			}
			else
			{
				if (!GameMain.mBuyGemCheckDone)
				{
					break;
				}
				mGame.FinishConnecting();
				if (GameMain.mBuyGemSucceed)
				{
					mState.Change(STATE.PURCHASE_END);
					break;
				}
				CreateErrorMessage(Localization.Get("Purchase_NetworkErrorTitle"), Localization.Get("Purchase_NetworkErrorDesc"), delegate
				{
					mState.Change(STATE.ADD_GEM);
				});
			}
			break;
		case STATE.PURCHASE_END:
			mState.Change(STATE.UPDATE_GEM);
			break;
		case STATE.UPDATE_GEM:
			if (mState.IsChanged())
			{
				CreateErrorMessage(Localization.Get("Purchase success title"), Localization.Get("Purchase success"), STATE.RESTORE_TRANSACTIONS_CHECK);
				PurchaseManager.LogData.PurchaseComplete();
				PurchaseManager.SavePurchaseData();
				ITierData tierData = null;
				if (purchaseInfo != null)
				{
					tierData = purchaseInfo.TierData;
				}
				if (tierData == null)
				{
					return;
				}
				string currencyCode = tierData.CurrencyCode;
				int itemNo = tierData.ItemNo;
				string itemID = tierData.ItemID;
				double cost = tierData.Cost;
				int item_num = 1;
				double num = cost;
				SingletonMonoBehaviour<Marketing>.Instance.PartytrackSendPayment(itemID, item_num, currencyCode, num);
				SingletonMonoBehaviour<Marketing>.Instance.AdjustSendPayment(itemNo, currencyCode, num);
			}
			break;
		case STATE.CLOSE:
			Close();
			break;
		case STATE.AUTHERROR_WAIT:
			SetClosedCallback(null);
			if (mAuthErrorCallback != null)
			{
				mAuthErrorCallback();
			}
			Close();
			mState.Change(STATE.WAIT);
			break;
		}
		if (mTierSelectList != null && mTierSelectList.IsContentsCreated && mCenterScrollCount < 2)
		{
			float num2 = 25f;
			mCenterGameobject.transform.localPosition -= new Vector3(0f, 0f - num2);
			mTierSelectList.CenterOn(mCenterGameobject);
			mCenterScrollCount++;
			mCenterGameobject.transform.localPosition -= new Vector3(0f, num2);
		}
		else if (mCenterScrollCount == 2)
		{
			mTierSelectList.CenterOffEnabled();
			mCenterScrollCount++;
		}
		mState.Update();
	}

	private void CreateErrorMessage(string title, string text, STATE nextState)
	{
		ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(title, text, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(delegate
		{
			mState.Change(nextState);
		});
		mState.Change(STATE.ERROR);
	}

	private void CreateErrorMessage(string title, string text, ConfirmDialog.OnDialogClosed onCloseFunc)
	{
		ConfirmDialog confirmDialog = Util.CreateGameObject("ConfirmDialog", base.transform.parent.gameObject).AddComponent<ConfirmDialog>();
		confirmDialog.Init(title, text, ConfirmDialog.CONFIRM_DIALOG_STYLE.OK_ONLY);
		confirmDialog.SetClosedCallback(onCloseFunc);
		mState.Change(STATE.ERROR);
	}

	public void OnCommonDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
	}

	public void OnErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		mConfirmDialog = null;
	}

	protected virtual void CreateList()
	{
		CreateList(TierListInfoEn, 0f, 20f, 2);
	}

	protected void CreateList(SMVerticalListInfo aListInfo, float aPosX, float aPosY, int aCenterIndex)
	{
		mTierList.Clear();
		List<TierSelectListItem> list = new List<TierSelectListItem>();
		for (int i = 0; i < PurchaseManager.instance.mPItems.Count; i++)
		{
			if (mGoogleSkuInfoDict.ContainsKey(PurchaseManager.instance.mPItems[i].itemID))
			{
				CampaignSaleEntry iAPSale = mGame.SegmentProfile.GetIAPSale(PurchaseManager.instance.mPItems[i].ItemID);
				if (iAPSale != null && iAPSale.IsEnable() && iAPSale.InTime())
				{
					TierSelectListItem tierSelectListItem = new TierSelectListItem();
					tierSelectListItem.enable = true;
					tierSelectListItem.googleSkuInfo = mGoogleSkuInfoDict[PurchaseManager.instance.mPItems[i].itemID];
					tierSelectListItem.pItem = PurchaseManager.instance.mPItems[i];
					tierSelectListItem.price = tierSelectListItem.googleSkuInfo.price;
					tierSelectListItem.name = tierSelectListItem.googleSkuInfo.title;
					tierSelectListItem.amount = PurchaseManager.instance.mPItems[i].amount.ToString();
					tierSelectListItem.mCampaignSaleEntry = iAPSale;
					list.Add(tierSelectListItem);
				}
			}
		}
		list.Sort(TierSelectListItem.CompareByShowId);
		for (int j = 0; j < list.Count; j++)
		{
			mTierList.Add(list[j]);
		}
		mTierSelectList = SMVerticalList.Make(base.gameObject, this, aListInfo, mTierList, aPosX, aPosY, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		mTierSelectList.OnCreateItem = OnCreateItem_Tier;
		mTierSelectList.OnBeforeCreate = OnBeforeCreate_Tier;
		mTierSelectList.OnCreateFinish = OnCreateFinish_Tier;
		mCenterIndex = aCenterIndex;
	}

	private void OnCreateItem_Tier(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		ResImage resImage = ResourceManager.LoadImage("HUD");
		UIFont atlasFont = GameMain.LoadFont();
		float cellWidth = mTierSelectList.mList.cellWidth;
		float cellHeight = mTierSelectList.mList.cellHeight;
		TierSelectListItem tierSelectListItem = (TierSelectListItem)item;
		if (index == mCenterIndex)
		{
			mCenterGameobject = itemGO;
		}
		UISprite uISprite = Util.CreateSprite("Back", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "instruction_panel2", mBaseDepth + 1, Vector3.zero, Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = (int)cellWidth;
		uISprite.height = (int)cellHeight;
		string text = null;
		if (tierSelectListItem.pItem.iconID < IconList.Count)
		{
			text = IconList[tierSelectListItem.pItem.iconID];
		}
		if (string.IsNullOrEmpty(text))
		{
			text = "icon_currency";
		}
		uISprite = Util.CreateSprite("Gem", itemGO, mAtlasMailBox);
		Util.SetSpriteInfo(uISprite, text, mBaseDepth + 2, new Vector3(-192f, 0f, 0f), new Vector3(1.1f, 1.1f, 1f), false);
		string text2 = string.Empty;
		switch (tierSelectListItem.mCampaignSaleEntry.ImageNo)
		{
		case 1:
			text2 = "LC_instruction_accessory00";
			break;
		case 2:
			text2 = "LC_sale_mini00";
			break;
		case 3:
			text2 = "LC_sale_mini01";
			break;
		}
		if (!string.IsNullOrEmpty(text2))
		{
			uISprite = Util.CreateSprite("GoodItem", itemGO, resImage.Image);
			Util.SetSpriteInfo(uISprite, text2, mBaseDepth + 2, new Vector3(-89f, -3f, 0f), new Vector3(1.1f, 1.1f, 1f), false);
		}
		uISprite = Util.CreateSprite("CostBack", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "buy_panel", mBaseDepth + 2, new Vector3(24f, -31f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 120;
		uISprite.height = 40;
		UILabel uILabel = Util.CreateLabel("Price", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, tierSelectListItem.price, mBaseDepth + 4, new Vector3(50f, -1f, 0f), 22, 0, 0, UIWidget.Pivot.Right);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		string text3 = tierSelectListItem.mCampaignSaleEntry.Text;
		uILabel = Util.CreateLabel("Gem_Gain", itemGO.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, text3, mBaseDepth + 4, new Vector3(-32f, -2f, 0f), 20, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = new Color32(242, 55, 117, byte.MaxValue);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(160, 20);
		uISprite = Util.CreateSprite("Line", itemGO, resImage.Image);
		Util.SetSpriteInfo(uISprite, "line00", mBaseDepth + 2, new Vector3(50f, 12f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.width = 360;
		uISprite.height = 6;
		string[] array = tierSelectListItem.name.Split('(');
		uILabel = Util.CreateLabel("Name", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, array[0], mBaseDepth + 4, new Vector3(-120f, 30f, 0f), 24, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(300, 24);
		uILabel = Util.CreateLabel("Amount", itemGO, atlasFont);
		Util.SetLabelInfo(uILabel, Localization.Get("Xtext") + tierSelectListItem.amount, mBaseDepth + 4, new Vector3(-129f, -32f, 0f), 24, 0, 0, UIWidget.Pivot.Left);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		UIButton button = Util.CreateJellyImageButton("BuyButton@" + tierSelectListItem.pItem.ItemID, itemGO, resImage.Image);
		Util.SetImageButtonInfo(button, "LC_button_buy3", "LC_button_buy3", "LC_button_buy3", mBaseDepth + 3, new Vector3(180f, -26f, 0f), Vector3.one);
		Util.AddImageButtonMessage(button, this, "OnBuyPushed", UIButtonMessage.Trigger.OnClick);
	}

	public virtual void OnBeforeCreate_Tier()
	{
	}

	public virtual void OnCreateFinish_Tier()
	{
	}

	protected void SetTickWait(float _time)
	{
		duration = 0f;
		waittime = _time;
	}

	protected bool CheckTickWait()
	{
		duration += Time.deltaTime;
		return duration > waittime;
	}

	public void StartCheckTimeOut(float _time)
	{
		StopCoroutine("OnTimeOut");
		timeoutflag = false;
		StartCoroutine("OnTimeOut", _time);
	}

	public IEnumerator OnTimeOut(float _time)
	{
		yield return new WaitForSeconds(_time);
		timeoutflag = true;
	}

	public virtual void OnPushedDisplay()
	{
		if (mState.GetStatus() == STATE.TIER_SELECT)
		{
			mEULADialog = Util.CreateGameObject("EULADialog", base.gameObject).AddComponent<EULADialog>();
			mEULADialog.Init(EULADialog.MODE.AS);
			mEULADialog.SetCallback(OnEULADialogClosed);
			mState.Reset(STATE.WAIT, true);
		}
	}

	public new virtual void OnEULADialogClosed(EULADialog.SELECT_ITEM item)
	{
		mEULADialog = null;
		mState.Reset(STATE.TIER_SELECT, true);
	}

	public void InitManager()
	{
		PurchaseManager.instance.restoreMode = false;
		PurchaseManager.instance.onBillingSupportedEventFunc = OBSupportedEvent;
		PurchaseManager.instance.onBillingNotSupportedEventFunc = OBNotSupportedEvent;
		PurchaseManager.instance.onQueryInventorySucceededEventFunc = OQInventorySucceededEvent;
		PurchaseManager.instance.onQueryInventoryFailedEventFunc = OQInventoryFailedEvent;
		PurchaseManager.instance.onPurchaseCompleteAwaitingVerificationEventFunc = OPCompleteAwaitingVerificationEvent;
		PurchaseManager.instance.onPurchaseSucceededEventFunc = OPSucceededEvent;
		PurchaseManager.instance.onPurchaseFailedEventFunc = OPFailedEvent;
		PurchaseManager.instance.onConsumePurchaseSucceededEventFunc = OCPSucceededEvent;
		PurchaseManager.instance.onConsumePurchaseFailedEventFunc = OCPFailedEvent;
	}

	public void CleanupManager()
	{
		PurchaseManager.instance.restoreMode = false;
		PurchaseManager.instance.onBillingSupportedEventFunc = null;
		PurchaseManager.instance.onBillingNotSupportedEventFunc = null;
		PurchaseManager.instance.onQueryInventorySucceededEventFunc = null;
		PurchaseManager.instance.onQueryInventoryFailedEventFunc = null;
		PurchaseManager.instance.onPurchaseCompleteAwaitingVerificationEventFunc = null;
		PurchaseManager.instance.onPurchaseSucceededEventFunc = null;
		PurchaseManager.instance.onPurchaseFailedEventFunc = null;
		PurchaseManager.instance.onConsumePurchaseSucceededEventFunc = null;
		PurchaseManager.instance.onConsumePurchaseFailedEventFunc = null;
	}

	private void OnConnectErrorAuthRetry(int a_state)
	{
		mState.Change((STATE)a_state);
	}

	private void OnConnectErrorAuthAbandon()
	{
		mGame.PlaySe("SE_NEGATIVE", -1);
		Close();
	}

	private void OBSupportedEvent()
	{
	}

	private void OBNotSupportedEvent(string _s0)
	{
	}

	private void OQInventorySucceededEvent(List<GooglePurchase> _l0, List<GoogleSkuInfo> _l1)
	{
		mNotConsumedGPList.Clear();
		for (int i = 0; i < _l0.Count; i++)
		{
			mNotConsumedGPList.Add(_l0[i]);
		}
		mGoogleSkuInfoDict.Clear();
		for (int j = 0; j < _l1.Count; j++)
		{
			mGoogleSkuInfoDict.Add(_l1[j].productId, _l1[j]);
		}
		isConnnection = false;
		isSucess = true;
	}

	private void OQInventoryFailedEvent(string _s0)
	{
		mNotConsumedGPList.Clear();
		mGoogleSkuInfoDict.Clear();
		isConnnection = false;
		isSucess = false;
	}

	private void OPCompleteAwaitingVerificationEvent(string _s0, string _s1)
	{
	}

	private void OPSucceededEvent(List<PItem> _plist)
	{
		isConnnection = false;
		isSucess = true;
		successItemID = new string[_plist.Count];
		int num = 0;
		foreach (PItem item in _plist)
		{
			successItemID[num] = item.itemID;
			num++;
		}
	}

	private void OPFailedEvent(string _s0)
	{
		isConnnection = false;
		isSucess = false;
		Server.ManualConnecting = false;
		failKind = FAILKIND.FAIL;
	}

	private void OCPSucceededEvent(GooglePurchase _gp, int _state, PItem item)
	{
		if (_state == 0)
		{
			long num = 0L;
			if (PurchaseStartTime >= 0f)
			{
				num = Mathf.FloorToInt((Time.realtimeSinceStartup - PurchaseStartTime) * 1000f);
			}
			isConnnection = false;
			isSucess = true;
			Server.ManualConnecting = false;
			purchaseInfo = GameMain.MakePurchaseInfoAndroid(_gp, item, itemKind);
		}
		else
		{
			isConnnection = false;
			isSucess = false;
			Server.ManualConnecting = false;
			failKind = FAILKIND.CANCEL;
		}
	}

	private void OCPFailedEvent(string _s0)
	{
		isConnnection = false;
		isSucess = false;
		Server.ManualConnecting = false;
		failKind = FAILKIND.FAIL;
	}

	public static PurchaseDialog CreatePurchaseDialog(GameObject aParent)
	{
		PurchaseDialog purchaseDialog = null;
		if (SingletonMonoBehaviour<GameMain>.Instance.GameProfile.mOldPurchaseDialog)
		{
			return Util.CreateGameObject("PurchasDialog", aParent).AddComponent<PurchaseDialog>();
		}
		return Util.CreateGameObject("PurchaseFullDialog", aParent).AddComponent<PurchaseFullDialog>();
	}
}
