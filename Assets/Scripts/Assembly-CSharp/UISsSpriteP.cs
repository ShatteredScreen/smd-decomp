using System;
using System.Collections.Generic;
using UnityEngine;

public class UISsSpriteP : MonoBehaviour
{
	public delegate void AnimationCallback(UISsSpriteP sprite);

	private bool depthchange;

	private int mDepth;

	public List<SsSubAnimeController> subAnimations = new List<SsSubAnimeController>();

	[SerializeField]
	[HideInInspector]
	internal SsAnimation _animation;

	private SsAnimation _prevAnimation;

	internal Transform _transform;

	internal bool _flipChanged = true;

	private bool _recalcNormals = true;

	private bool _hFlip;

	private bool _vFlip;

	private bool _isPlaying = true;

	public AnimationCallback AnimationFinished;

	private bool _isFinished;

	public float _animeFrame;

	internal float _prevFrame;

	internal int _prevStepSign;

	public int _startAnimeFrame;

	public int _endAnimeFrame;

	public SsAnimePlayDirection _playDirection;

	[HideInInspector]
	public SsAnimePlayDirection _prevPlayDirection;

	internal int _currentStepSign = 1;

	private bool _returnTrip;

	public int PlayCount;

	private int _currentPlayCount;

	public float Speed = 1f;

	public bool PlayAtStart = true;

	public bool DestroyAtEnd;

	public float _lifeTime;

	private float _lifeTimeCount;

	public bool UpdateCollider;

	public bool DrawBoundingBox;

	public bool DrawBoundingParts;

	private SsPartRes[] _partResList;

	private SsImageFile[] _imageList;

	private int _partsNum;

	private UISsPartP[] _parts;

	internal Bounds _bounds = new Bounds(Vector3.zero, Vector3.zero);

	private List<UISsPartP> _boundPartList;

	internal int _playFrameLength;

	private MeshRenderer _meshRenderer;

	private BoxCollider _boxCollider;

	private SphereCollider _sphereCollider;

	private CapsuleCollider _capsuleCollider;

	internal UISsPartP _rootPart;

	internal Material[] _materials;

	internal Mesh _mesh;

	internal Vector3[] _vertices;

	internal Vector2[] _uvs;

	internal Color[] _colors;

	internal Vector2[] _extras;

	private UISsPartP[] _sortedParts;

	internal bool _vertChanged;

	internal bool _uvChanged;

	internal bool _colorChanged;

	internal bool _prioChanged;

	internal bool _matChanged;

	internal bool _extraChanged;

	[HideInInspector]
	[SerializeField]
	private int ImportedTime;

	[HideInInspector]
	[SerializeField]
	private int ImportedTimeHigh;

	private bool update_visiblity = true;

	public int SeqNo;

	private bool _isFirstUpdate = true;

	private float update_last_delta_time;

	public int depth
	{
		get
		{
			return mDepth;
		}
		set
		{
			if (mDepth != value)
			{
				mDepth = value;
				depthchange = true;
				UpdateDrawerDepth();
			}
		}
	}

	public SsAnimation Animation
	{
		get
		{
			return _animation;
		}
		set
		{
			_animation = value;
			if (!(value == _prevAnimation))
			{
				Init();
				_startAnimeFrame = 0;
				if ((bool)_animation)
				{
					_endAnimeFrame = _animation.EndFrame;
				}
				else
				{
					_endAnimeFrame = 0;
				}
				_prevAnimation = _animation;
				depthchange = true;
				UpdateDrawerDepth();
				if (PlayAtStart && Application.isPlaying)
				{
					Play();
				}
			}
		}
	}

	public Vector3 Position
	{
		get
		{
			return _transform.localPosition;
		}
		set
		{
			_transform.localPosition = value;
		}
	}

	public Vector3 Rotation
	{
		get
		{
			return _transform.localRotation.eulerAngles;
		}
		set
		{
			Quaternion identity = Quaternion.identity;
			identity.eulerAngles = value;
			_transform.localRotation = identity;
		}
	}

	public Vector3 Scale
	{
		get
		{
			return _transform.localScale;
		}
		set
		{
			_transform.localScale = value;
		}
	}

	public bool hFlip
	{
		get
		{
			return _hFlip;
		}
		set
		{
			if (_hFlip != value)
			{
				_hFlip = value;
				_flipChanged = true;
			}
		}
	}

	public bool vFlip
	{
		get
		{
			return _vFlip;
		}
		set
		{
			if (_vFlip != value)
			{
				_vFlip = value;
				_flipChanged = true;
			}
		}
	}

	public float AnimFrame
	{
		get
		{
			return _animeFrame;
		}
		set
		{
			if (!(value < 0f) && (bool)_animation && !(value > (float)_animation.EndFrame))
			{
				_animeFrame = value;
			}
		}
	}

	public float StartFrame
	{
		get
		{
			return _startAnimeFrame;
		}
		set
		{
			if (!(value < 0f) && (bool)_animation && !(value > (float)_animation.EndFrame) && !(value > (float)_endAnimeFrame))
			{
				_startAnimeFrame = (int)value;
			}
		}
	}

	public float EndFrame
	{
		get
		{
			return _endAnimeFrame;
		}
		set
		{
			if (!(value < 0f) && (bool)_animation && !(value > (float)_animation.EndFrame) && !(value < (float)_startAnimeFrame))
			{
				_endAnimeFrame = (int)value;
			}
		}
	}

	public SsAnimePlayDirection PlayDirection
	{
		get
		{
			return _playDirection;
		}
		set
		{
			_playDirection = value;
			ResetAnimationStatus();
		}
	}

	private bool IsRoundTrip
	{
		get
		{
			return PlayDirection == SsAnimePlayDirection.RoundTrip || PlayDirection == SsAnimePlayDirection.ReverseRoundTrip;
		}
	}

	public float LifeTime
	{
		get
		{
			return _lifeTime;
		}
		set
		{
			_lifeTime = value;
			_lifeTimeCount = 0f;
		}
	}

	public SsPartRes[] PartResList
	{
		get
		{
			return _partResList;
		}
	}

	public Bounds bounds
	{
		get
		{
			return _bounds;
		}
	}

	public Mesh mesh
	{
		get
		{
			return _mesh;
		}
	}

	public MeshRenderer renderer
	{
		get
		{
			return _meshRenderer;
		}
	}

	public static GameObject CreateGameObject(string name)
	{
		GameObject gameObject = new GameObject(name);
		gameObject.AddComponent<UISsSpriteP>();
		return gameObject;
	}

	public static GameObject CreateGameObjectWithAnime(string name, string animName)
	{
		GameObject gameObject = new GameObject(name);
		UISsSpriteP uISsSpriteP = gameObject.AddComponent<UISsSpriteP>();
		SsAnimation animation = (SsAnimation)Resources.Load(animName, typeof(SsAnimation));
		uISsSpriteP.Animation = animation;
		return gameObject;
	}

	public void ReplaceAnimation(SsAnimation anm)
	{
		if (!_animation)
		{
			Animation = anm;
			return;
		}
		if (anm.PartList.Length != _animation.PartList.Length)
		{
			UnityEngine.Debug.LogError("Can't replace animation because of the difference of the parts count.\n" + anm.name + ": " + anm.PartList.Length + " " + _animation.name + ": " + _animation.PartList.Length);
			return;
		}
		_animation = anm;
		if (anm == _prevAnimation)
		{
			return;
		}
		if (!_animation)
		{
			Init();
			return;
		}
		ResetAnimationStatus();
		Pause();
		_partResList = _animation.PartList;
		_imageList = _animation.ImageList;
		_partsNum = _partResList.Length;
		for (int i = 0; i < _parts.Length; i++)
		{
			_parts[i]._res = _partResList[i];
		}
		_vertChanged = true;
		_uvChanged = true;
		_extraChanged = true;
		UpdateAlways();
		_startAnimeFrame = 0;
		if ((bool)_animation)
		{
			_endAnimeFrame = _animation.EndFrame;
		}
		else
		{
			_endAnimeFrame = 0;
		}
		_prevAnimation = _animation;
		if (PlayAtStart && Application.isPlaying)
		{
			Play();
		}
	}

	public UISsPartP GetPart(int index)
	{
		if (_parts == null)
		{
			return null;
		}
		if (index >= _parts.Length)
		{
			return null;
		}
		return _parts[index];
	}

	public UISsPartP GetPart(string name)
	{
		if (name == null)
		{
			return _parts[0];
		}
		UISsPartP[] parts = _parts;
		foreach (UISsPartP uISsPartP in parts)
		{
			if (uISsPartP._res.Name == name)
			{
				return uISsPartP;
			}
		}
		return null;
	}

	public UISsPartP[] GetParts()
	{
		return _parts;
	}

	public Transform TransformAt(string name)
	{
		if (name == null)
		{
			return _transform;
		}
		UISsPartP part = GetPart(name);
		if (part == null)
		{
			return null;
		}
		return part._transform;
	}

	public Vector3 PositionAt(string name)
	{
		Transform transform = TransformAt(name);
		if (!transform)
		{
			UnityEngine.Debug.LogError("Can't find child part: " + name);
			return Vector3.zero;
		}
		return transform.position;
	}

	public void Play()
	{
		_isPlaying = true;
		_isFinished = false;
	}

	public void Pause()
	{
		_isPlaying = false;
	}

	public bool IsPlaying()
	{
		return _isPlaying;
	}

	public bool IsAnimationFinished()
	{
		return _isFinished;
	}

	public void SetStartEndFrame(int start, int end)
	{
		_startAnimeFrame = ((start >= 0) ? ((start <= _animation.EndFrame) ? start : _animation.EndFrame) : 0);
		_endAnimeFrame = ((end >= 0) ? ((end <= _animation.EndFrame) ? end : _animation.EndFrame) : 0);
	}

	public bool IsLastFrame()
	{
		return _animeFrame >= (float)_endAnimeFrame;
	}

	public void SetPlayDirection(SsAnimePlayDirection dir, bool keepFrame)
	{
		_playDirection = dir;
		float animeFrame = _animeFrame;
		ResetAnimationStatus();
		if (keepFrame)
		{
			_animeFrame = animeFrame;
		}
	}

	public bool IntersectsByBounds(UISsSpriteP other, bool ignoreZ)
	{
		if (!_mesh || !other._mesh)
		{
			return false;
		}
		if (ignoreZ)
		{
			Bounds bounds = other._bounds;
			Vector3 center = bounds.center;
			center.z = _bounds.center.z;
			bounds.center = center;
			return _bounds.Intersects(bounds);
		}
		return _bounds.Intersects(other._bounds);
	}

	public bool IntersectsByBoundingParts(UISsSpriteP other, bool ignoreZ, bool useAABB)
	{
		foreach (UISsPartP boundPart in _boundPartList)
		{
			foreach (UISsPartP boundPart2 in other._boundPartList)
			{
				if (useAABB)
				{
					if (boundPart.IntersectsByAABB(boundPart2, ignoreZ))
					{
						return true;
					}
				}
				else if (boundPart.Intersects(boundPart2, ignoreZ))
				{
					return true;
				}
			}
		}
		return false;
	}

	public bool ContainsPoint(Vector3 point, bool ignoreZ)
	{
		if (!_mesh)
		{
			return false;
		}
		if (ignoreZ)
		{
			Vector3 point2 = point;
			point2.z = _bounds.center.z;
			return _bounds.Contains(point2);
		}
		return _bounds.Contains(point);
	}

	protected void Awake()
	{
		_transform = base.transform;
		EnsureMeshComponent();
		_boxCollider = GetComponent<BoxCollider>();
		_sphereCollider = GetComponent<SphereCollider>();
		_capsuleCollider = GetComponent<CapsuleCollider>();
		Init();
	}

	private void EnsureMeshComponent()
	{
		if (!_meshRenderer)
		{
			_meshRenderer = base.gameObject.GetComponent<MeshRenderer>();
			if (_meshRenderer == null)
			{
				_meshRenderer = base.gameObject.AddComponent<MeshRenderer>();
			}
		}
		_meshRenderer.castShadows = false;
		_meshRenderer.receiveShadows = false;
		_meshRenderer.enabled = false;
	}

	protected void Start()
	{
		_isFirstUpdate = true;
		if (PlayAtStart && Application.isPlaying)
		{
			Play();
		}
	}

	private void Init()
	{
		DeleteTransformChildren();
		if (!_animation)
		{
			_partResList = null;
			_imageList = null;
			_partsNum = 0;
			_parts = null;
			_boundPartList = null;
			_materials = null;
			_vertices = null;
			_uvs = null;
			_colors = null;
			_extras = null;
			_mesh = null;
			_meshRenderer.sharedMaterials = new Material[1];
			_isPlaying = (_isFinished = false);
			PlayCount = 0;
			Speed = 1f;
			_prevPlayDirection = (_playDirection = SsAnimePlayDirection.Forward);
			ResetAnimationStatus();
			_startAnimeFrame = (_endAnimeFrame = (_playFrameLength = 0));
			return;
		}
		EnsureMeshComponent();
		ResetAnimationStatus();
		Pause();
		_isFinished = false;
		_partResList = _animation.PartList;
		_imageList = _animation.ImageList;
		_partsNum = _partResList.Length;
		_materials = new Material[_partsNum - 1];
		int num = (_partsNum - 1) * 4;
		_vertices = new Vector3[num];
		_uvs = new Vector2[num];
		_colors = new Color[num];
		_extras = new Vector2[num];
		_mesh = new Mesh();
		_mesh.MarkDynamic();
		_mesh.vertices = _vertices;
		_mesh.subMeshCount = _partsNum - 1;
		_mesh.uv2 = _extras;
		int layer = _transform.gameObject.layer;
		_parts = new UISsPartP[_partsNum];
		for (int i = 0; i < _parts.Length; i++)
		{
			SsPartRes ssPartRes = _partResList[i];
			SsImageFile imageFile = ((!ssPartRes.IsRoot) ? _imageList[ssPartRes.SrcObjId] : null);
			GameObject gameObject = new GameObject(base.name);
			UISsPartPDraw uISsPartPDraw = gameObject.AddComponent<UISsPartPDraw>();
			gameObject.name = base.name + "_" + i;
			gameObject.transform.parent = _transform;
			gameObject.layer = layer;
			Transform transform = uISsPartPDraw.transform;
			transform.localPosition = Vector3.zero;
			transform.localScale = Vector3.one;
			transform.localEulerAngles = Vector3.zero;
			_parts[i] = new UISsPartP(this, i, ssPartRes, imageFile, uISsPartPDraw);
			if (i == 0)
			{
				_rootPart = _parts[i];
			}
			else
			{
				_materials[i - 1] = _parts[i]._material;
			}
			if (ssPartRes.Type == SsPartType.Bound)
			{
				if (_boundPartList == null)
				{
					_boundPartList = new List<UISsPartP>();
				}
				_boundPartList.Add(_parts[i]);
			}
		}
		_sortedParts = new UISsPartP[_partsNum];
		for (int j = 0; j < _parts.Length; j++)
		{
			_sortedParts[j] = _parts[j];
		}
		_meshRenderer.sharedMaterials = _materials;
		_mesh.vertices = _vertices;
		_mesh.uv = _uvs;
		_vertChanged = true;
		_extraChanged = true;
		UpdateAlways();
	}

	private void OnEnable()
	{
	}

	private void EnsureLatestAnime()
	{
		if ((bool)_animation && !_animation.EqualsImportedTime(ImportedTime, ImportedTimeHigh))
		{
			EnsureMeshComponent();
			Init();
			ImportedTime = _animation.ImportedTime;
			ImportedTimeHigh = _animation.ImportedTimeHigh;
		}
	}

	private void OnDestroy()
	{
		DeleteTransformChildren();
	}

	private void DeleteTransformChildren()
	{
		if (_mesh != null)
		{
			UnityEngine.Object.DestroyImmediate(_mesh);
			_mesh = null;
		}
		if ((bool)_transform)
		{
			int childCount = _transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				UnityEngine.Object.DestroyImmediate(_transform.GetChild(0).gameObject);
			}
		}
		if (_parts != null)
		{
			for (int j = 0; j < _parts.Length; j++)
			{
				_parts[j].DeleteResources();
			}
		}
	}

	public void ResetAnimationStatus()
	{
		if (PlayDirection == SsAnimePlayDirection.Reverse || PlayDirection == SsAnimePlayDirection.ReverseRoundTrip)
		{
			_currentStepSign = -1;
			_animeFrame = _endAnimeFrame;
		}
		else
		{
			_currentStepSign = 1;
			_animeFrame = _startAnimeFrame;
		}
		_playFrameLength = _endAnimeFrame + 1 - _startAnimeFrame;
		_currentPlayCount = 0;
		_returnTrip = false;
		_prevFrame = -1f;
		_prevStepSign = _currentStepSign;
	}

	public void UpdateVertex()
	{
		_vertChanged = true;
	}

	private void Update()
	{
		UpdateAlways();
	}

	public void DoBecameVisible()
	{
		update_visiblity = true;
	}

	public void DoBecameInvisible()
	{
		update_visiblity = false;
	}

	public void UpdateAlways()
	{
		if (_animation == null || _parts == null || _parts[_partsNum - 1] == null)
		{
			return;
		}
		float deltaTime = Time.deltaTime;
		_isFirstUpdate = false;
		if (_lifeTime > 0f)
		{
			_lifeTimeCount += deltaTime;
			if (_lifeTimeCount >= _lifeTime)
			{
				UnityEngine.Object.Destroy(base.gameObject);
				return;
			}
		}
		if (subAnimations != null)
		{
			foreach (SsSubAnimeController subAnimation in subAnimations)
			{
				if (subAnimation.Animation == null)
				{
					continue;
				}
				if (subAnimation.BindsToAllParts)
				{
					for (int i = 1; i < _parts.Length; i++)
					{
						for (int j = 0; j < subAnimation.Animation.PartList.Length; j++)
						{
							_parts[i].AddSubAnime(subAnimation, subAnimation.Animation.PartList[j]);
						}
					}
					continue;
				}
				for (int k = 0; k < subAnimation.Animation.PartList.Length; k++)
				{
					UISsPartP uISsPartP = null;
					uISsPartP = ((!subAnimation.BindsByPartName) ? GetPart(k) : GetPart(subAnimation.Animation.PartList[k].Name));
					if (uISsPartP != null)
					{
						uISsPartP.AddSubAnime(subAnimation, subAnimation.Animation.PartList[k]);
					}
				}
			}
		}
		bool flag = false;
		if (update_visiblity)
		{
			int l = 0;
			for (int num = _parts.Length; l < num; l++)
			{
				UISsPartP uISsPartP2 = _parts[l];
				uISsPartP2.Update();
			}
			if (_prioChanged)
			{
				SortByPriority();
				_prioChanged = false;
			}
			if (_vertChanged)
			{
				_mesh.vertices = _vertices;
				updateBoundingBox();
				_vertChanged = false;
				flag = true;
			}
			if (_uvChanged)
			{
				_mesh.uv = _uvs;
				_uvChanged = false;
				flag = true;
			}
			if (_colorChanged)
			{
				_mesh.colors = _colors;
				_colorChanged = false;
				flag = true;
			}
			if (_extraChanged)
			{
				_mesh.uv2 = _extras;
				_extraChanged = false;
			}
			if (_matChanged)
			{
				_meshRenderer.sharedMaterials = _materials;
				_matChanged = false;
			}
		}
		else
		{
			updateBoundingBox();
		}
		if (!_isPlaying)
		{
			return;
		}
		float num2 = deltaTime * (float)_animation.FPS * Speed * (float)_currentStepSign;
		_prevFrame = _animeFrame;
		_animeFrame += num2;
		if (_animeFrame < (float)_startAnimeFrame || (int)_animeFrame > _endAnimeFrame)
		{
			bool flag2 = true;
			if (IsRoundTrip)
			{
				_prevStepSign = _currentStepSign;
				_currentStepSign *= -1;
				if (!_returnTrip)
				{
					_returnTrip = true;
				}
				else
				{
					_returnTrip = false;
				}
			}
			else
			{
				flag2 = false;
			}
			if (!_returnTrip && PlayCount != 0 && ++_currentPlayCount >= PlayCount)
			{
				flag2 = true;
				_isPlaying = false;
				_isFinished = true;
				if (AnimationFinished != null)
				{
					AnimationFinished(this);
				}
				if (DestroyAtEnd)
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
			}
			float num3 = 0f;
			if (_animeFrame < (float)_startAnimeFrame)
			{
				num3 = (_animeFrame - (float)_startAnimeFrame) % (float)_playFrameLength;
				num3 *= -1f;
			}
			else if ((int)_animeFrame > _endAnimeFrame)
			{
				num3 = (_animeFrame - 1f - (float)_endAnimeFrame) % (float)_playFrameLength;
			}
			if (num3 < 0f)
			{
				num3 = 0f;
			}
			if (flag2)
			{
				if (IsRoundTrip)
				{
					if (_animeFrame < (float)_startAnimeFrame)
					{
						_animeFrame = (float)_startAnimeFrame + num3;
					}
					else if (_animeFrame > (float)_endAnimeFrame)
					{
						_animeFrame = (float)_endAnimeFrame - num3;
					}
				}
				else if (_animeFrame < (float)_startAnimeFrame)
				{
					_animeFrame = _startAnimeFrame;
				}
				else if (_animeFrame > (float)_endAnimeFrame)
				{
					_animeFrame = _endAnimeFrame;
				}
			}
			else if (_animeFrame < (float)_startAnimeFrame)
			{
				_animeFrame = (float)(_endAnimeFrame + 1) - num3;
			}
			else if (_animeFrame > (float)_endAnimeFrame)
			{
				_animeFrame = (float)_startAnimeFrame + num3;
			}
		}
		foreach (SsSubAnimeController subAnimation2 in subAnimations)
		{
			subAnimation2.StepFrame(Time.deltaTime);
		}
		if (depthchange)
		{
			depthchange = false;
			flag = true;
		}
		if (flag)
		{
			UpdateDrawer();
		}
	}

	internal int _StepFrameFromPrev(int frame)
	{
		frame += ((!(Speed > 0f)) ? (-_prevStepSign) : _prevStepSign);
		if (frame > _endAnimeFrame)
		{
			if (IsRoundTrip)
			{
				_prevStepSign *= -1;
				frame = _endAnimeFrame - 1;
				if (frame < 0)
				{
					frame = _endAnimeFrame;
				}
			}
			else
			{
				frame = _startAnimeFrame;
			}
		}
		else if (frame < _startAnimeFrame)
		{
			if (IsRoundTrip)
			{
				_prevStepSign *= -1;
				frame = _startAnimeFrame + 1;
			}
			else
			{
				frame = _endAnimeFrame;
			}
		}
		return frame;
	}

	public void ForcePartsUpdate()
	{
		UISsPartP[] parts = _parts;
		foreach (UISsPartP uISsPartP in parts)
		{
			uISsPartP.Update();
		}
		bool flag = false;
		if (_prioChanged)
		{
			SortByPriority();
			_prioChanged = false;
			flag = true;
		}
		if (_vertChanged)
		{
			_mesh.vertices = _vertices;
			updateBoundingBox();
			_vertChanged = false;
			flag = true;
		}
		if (_uvChanged)
		{
			_mesh.uv = _uvs;
			_uvChanged = false;
			flag = true;
		}
		if (_colorChanged)
		{
			_mesh.colors = _colors;
			_colorChanged = false;
			flag = true;
		}
		if (_extraChanged)
		{
			_mesh.uv2 = _extras;
			_extraChanged = false;
		}
		if (_matChanged)
		{
			_meshRenderer.sharedMaterials = _materials;
			_matChanged = false;
		}
		if (depthchange)
		{
			depthchange = false;
			flag = true;
		}
		if (flag)
		{
			UpdateDrawer();
		}
	}

	public void DebugPartsName()
	{
		int num = 0;
		UISsPartP[] parts = _parts;
		foreach (UISsPartP uISsPartP in parts)
		{
			num++;
		}
	}

	internal void updateBoundingBox()
	{
		_mesh.RecalculateBounds();
		if (_recalcNormals)
		{
			_mesh.RecalculateNormals();
			_recalcNormals = false;
		}
		_bounds = renderer.bounds;
		if (UpdateCollider)
		{
			if ((bool)_boxCollider)
			{
				UnityEngine.Object.DestroyImmediate(_boxCollider);
				_boxCollider = base.gameObject.AddComponent<BoxCollider>();
				Vector3 size = _boxCollider.size;
				size.z = 1f;
				_boxCollider.size = size;
			}
			else if ((bool)_sphereCollider)
			{
				UnityEngine.Object.DestroyImmediate(_sphereCollider);
				_sphereCollider = base.gameObject.AddComponent<SphereCollider>();
			}
			else if ((bool)_capsuleCollider)
			{
				UnityEngine.Object.DestroyImmediate(_capsuleCollider);
				_capsuleCollider = base.gameObject.AddComponent<CapsuleCollider>();
			}
		}
	}

	private void SortByPriority()
	{
		_sortedParts = new UISsPartP[_parts.Length];
		for (int i = 0; i < _parts.Length; i++)
		{
			_sortedParts[i] = _parts[i];
		}
		Array.Sort(_sortedParts);
		int num = 0;
		for (int j = 0; j < _sortedParts.Length; j++)
		{
			if (!_sortedParts[j]._res.IsRoot)
			{
				_sortedParts[j].SetToSubmeshArray(num);
				_sortedParts[j].mUISsPartDraw.depthf = (float)depth + (float)num * 0.0001f;
				_materials[num] = _sortedParts[j]._material;
				num++;
			}
		}
		_matChanged = true;
	}

	internal UISsPartP Sprite(int index)
	{
		if (index < 0 || index >= _partsNum)
		{
			return null;
		}
		return _parts[index];
	}

	public void UpdateDrawerDepth()
	{
		if (_sortedParts == null)
		{
			BIJLog.E("You must set animation before set depth!!!!!!!!!");
			return;
		}
		int num = 0;
		for (int i = 0; i < _sortedParts.Length; i++)
		{
			if (_sortedParts[i] != null && !_sortedParts[i]._res.IsRoot)
			{
				_sortedParts[i].mUISsPartDraw.depthf = (float)depth + (float)num * 0.0001f;
				num++;
			}
		}
	}

	public void UpdateDrawer()
	{
		int i = 0;
		for (int num = _sortedParts.Length; i < num; i++)
		{
			if (!_sortedParts[i]._res.IsRoot && _sortedParts[i]._visible)
			{
				int vIndex = _sortedParts[i]._vIndex;
				UISsPartPDraw mUISsPartDraw = _sortedParts[i].mUISsPartDraw;
				if (!mUISsPartDraw.gameObject.activeSelf)
				{
					NGUITools.SetActive(mUISsPartDraw.gameObject, true);
				}
				mUISsPartDraw.material = _sortedParts[i]._material;
				mUISsPartDraw._verts[0] = _vertices[vIndex];
				mUISsPartDraw._uvs[0] = _uvs[vIndex];
				mUISsPartDraw._uv2s[0] = _extras[vIndex];
				mUISsPartDraw._colors[0] = _colors[vIndex];
				vIndex++;
				mUISsPartDraw._verts[1] = _vertices[vIndex];
				mUISsPartDraw._uvs[1] = _uvs[vIndex];
				mUISsPartDraw._uv2s[1] = _extras[vIndex];
				mUISsPartDraw._colors[1] = _colors[vIndex];
				vIndex++;
				mUISsPartDraw._verts[2] = _vertices[vIndex];
				mUISsPartDraw._uvs[2] = _uvs[vIndex];
				mUISsPartDraw._uv2s[2] = _extras[vIndex];
				mUISsPartDraw._colors[2] = _colors[vIndex];
				vIndex++;
				mUISsPartDraw._verts[3] = _vertices[vIndex];
				mUISsPartDraw._uvs[3] = _uvs[vIndex];
				mUISsPartDraw._uv2s[3] = _extras[vIndex];
				mUISsPartDraw._colors[3] = _colors[vIndex];
				vIndex++;
				mUISsPartDraw.MarkAsChanged();
			}
			else
			{
				UISsPartPDraw mUISsPartDraw2 = _sortedParts[i].mUISsPartDraw;
				if (mUISsPartDraw2.gameObject.activeSelf)
				{
					NGUITools.SetActive(mUISsPartDraw2.gameObject, false);
				}
			}
		}
	}
}
