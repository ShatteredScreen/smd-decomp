using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DianaGimmick : MonoBehaviour
{
	public enum STATE
	{
		LOAD = 0,
		CREATE = 1,
		STAY = 2,
		STOP = 3,
		MOVE = 4,
		GOAL = 5,
		AFTER_GOAL = 6
	}

	private const int STEP_MAX = 99;

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.LOAD);

	private SsSprite mGoalSprite;

	private GameMain mGame;

	private PuzzleManager mPuzzle;

	private SsSprite mSprite;

	private PartsChangedSprite mBalloonSprite;

	private IntVector2 mTilePos;

	private Vector3 mLocalPos;

	private Vector3 mOffsetPos = new Vector3(0f, (0f - Def.TILE_PITCH_V) / 2f, 0f);

	private Def.DIR mHDir;

	private int mWayPosIndex;

	private PuzzleTile mPuzzleTile;

	private int mStepCount;

	private bool mBalloonOpen;

	private float mBalloonBaseScale = 0.55f * Def.TILE_SCALE;

	private float mBalloonOfsYMax = 96f * Def.TILE_SCALE;

	private float mBalloonOfsYMin = 46f * Def.TILE_SCALE;

	private float mBalloonOfsY = 96f * Def.TILE_SCALE;

	private static string[] NumSpriteName = new string[10] { "footstamp_num0", "footstamp_num1", "footstamp_num2", "footstamp_num3", "footstamp_num4", "footstamp_num5", "footstamp_num6", "footstamp_num7", "footstamp_num8", "footstamp_num9" };

	private void Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
	}

	private void Update()
	{
		switch (mState.GetStatus())
		{
		case STATE.CREATE:
			CreateSprites();
			break;
		case STATE.GOAL:
			if (!mSprite.IsPlaying())
			{
				AfterGoal();
			}
			break;
		}
		if (mSprite != null)
		{
			mSprite.transform.localPosition = mLocalPos + mOffsetPos;
			if (mBalloonSprite != null)
			{
				IntVector2 intVector = mGame.mCurrentStage.DianaWayPosList[mWayPosIndex];
				if (intVector.y >= 9)
				{
					mBalloonOfsY -= 10f * Def.TILE_SCALE;
					if (mBalloonOfsY < mBalloonOfsYMin)
					{
						mBalloonOfsY = mBalloonOfsYMin;
					}
				}
				else
				{
					mBalloonOfsY += 10f * Def.TILE_SCALE;
					if (mBalloonOfsY > mBalloonOfsYMax)
					{
						mBalloonOfsY = mBalloonOfsYMax;
					}
				}
				if (mHDir == Def.DIR.RIGHT)
				{
					mBalloonSprite.transform.localPosition = mLocalPos + mOffsetPos + new Vector3(-4f * Def.TILE_SCALE, mBalloonOfsY * Def.TILE_SCALE, -0.5f);
				}
				else
				{
					mBalloonSprite.transform.localPosition = mLocalPos + mOffsetPos + new Vector3(36f * Def.TILE_SCALE, mBalloonOfsY * Def.TILE_SCALE, -0.5f);
				}
			}
		}
		mState.Update();
	}

	public void Init(IntVector2 tilePos, PuzzleManager puzzle, PuzzleTile puzzleTile)
	{
		mPuzzle = puzzle;
		mPuzzleTile = puzzleTile;
		mTilePos = tilePos;
		mStepCount = 0;
		mWayPosIndex = 0;
		StartCoroutine(LoadAnimation());
	}

	private IEnumerator LoadAnimation()
	{
		List<ResourceInstance> resList = new List<ResourceInstance> { ResourceManager.LoadSsAnimationAsync("GIMMICK_DIANA_IDLE") };
		while (true)
		{
			bool wait = false;
			foreach (ResourceInstance res in resList)
			{
				if (res.LoadState != ResourceInstance.LOADSTATE.DONE)
				{
					wait = true;
					break;
				}
			}
			if (!wait)
			{
				break;
			}
			yield return 0;
		}
		mState.Reset(STATE.CREATE, true);
	}

	private void CreateSprites()
	{
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_TILE");
		mSprite = Util.CreateGameObject("Body", base.gameObject).AddComponent<SsSprite>();
		Vector2 defaultCoords = mPuzzle.GetDefaultCoords(mTilePos);
		mLocalPos = new Vector3(defaultCoords.x, defaultCoords.y, 5f);
		mSprite.transform.localPosition = mLocalPos + mOffsetPos;
		mBalloonSprite = Util.CreateGameObject("Balloon", base.gameObject).AddComponent<PartsChangedSprite>();
		mBalloonSprite.Atlas = resImage.Image;
		mBalloonSprite.transform.localScale = new Vector3(mBalloonBaseScale, mBalloonBaseScale, 1f);
		mBalloonOpen = true;
		mHDir = Def.DIR.NONE;
		if (mGame.mCurrentStage.DianaWayPosList[0].x > mGame.mCurrentStage.DianaWayPosList[1].x)
		{
			SetHDir(Def.DIR.LEFT);
		}
		else
		{
			SetHDir(Def.DIR.RIGHT);
		}
		int index = mGame.mCurrentStage.DianaWayPosList.Count - 1;
		IntVector2 pos = mGame.mCurrentStage.DianaWayPosList[index];
		PuzzleTile attribute = mPuzzle.GetAttribute(pos);
		if (attribute != null)
		{
			Vector2 vector = Vector2.zero;
			if (attribute.Kind == Def.TILE_KIND.DIANA_GOAL_L)
			{
				vector = new Vector2(Def.TILE_PITCH_H / 2f, 0f);
			}
			else if (attribute.Kind == Def.TILE_KIND.DIANA_GOAL_R)
			{
				vector = new Vector2((0f - Def.TILE_PITCH_H) / 2f, 0f);
			}
			defaultCoords = mPuzzle.GetDefaultCoords(pos);
			mGoalSprite = Util.CreateGameObject("Goal", base.gameObject).AddComponent<SsSprite>();
			mGoalSprite.transform.localPosition = new Vector3(defaultCoords.x + vector.x, defaultCoords.y + vector.y, 10.5f);
			mGoalSprite.transform.localScale = new Vector3(Def.TILE_SCALE, Def.TILE_SCALE, 1f);
			ChangeAnime(mGoalSprite, "GIMMICK_DIANA_HOUSE", 0);
			mGoalSprite.PlayAtStart = false;
			mGoalSprite.Pause();
		}
		Stay();
		mPuzzle.OnDianaStepRemainChanged(mGame.mCurrentStage.DianaWayPosList.Count - 2);
	}

	private void Stay()
	{
		ChangeAnime(mSprite, "GIMMICK_DIANA_IDLE", 0);
		mState.Reset(STATE.STAY, true);
	}

	private void Stop()
	{
		if (mState.GetStatus() != STATE.STOP)
		{
			ChangeAnime(mSprite, "GIMMICK_DIANA_STOP", 0);
			mGame.PlaySe("SE_DIANA_STOP", 2);
			mState.Reset(STATE.STOP, true);
		}
	}

	private void Goal()
	{
		ChangeAnime(mSprite, "GIMMICK_DIANA_GOAL", 1);
		mGame.PlaySe("SE_DIANA_GOAL", 2);
		mState.Reset(STATE.GOAL, true);
		mGoalSprite.Play();
	}

	private void AfterGoal()
	{
		ChangeAnime(mSprite, "GIMMICK_DIANA_GOAL_LOOP", 0);
		mState.Reset(STATE.AFTER_GOAL, true);
	}

	private void ChangeAnime(SsSprite sprite, string key, int playCount)
	{
		ResSsAnimation resSsAnimation = ResourceManager.LoadSsAnimation(key);
		SsAnimation ssAnime = resSsAnimation.SsAnime;
		sprite.Animation = ssAnime;
		sprite.ResetAnimationStatus();
		sprite.Play();
		sprite.PlayCount = playCount;
	}

	private void UpdateBalloon()
	{
		CHANGE_PART_INFO[] array = new CHANGE_PART_INFO[5];
		array[0].partName = "change_000";
		array[0].spriteName = "null";
		array[1].partName = "change_001";
		array[1].spriteName = "null";
		array[2].partName = "change_002";
		array[2].spriteName = "null";
		array[3].partName = "change_003";
		array[3].spriteName = "null";
		array[4].partName = "change_004";
		array[4].spriteName = "null";
		int num = mStepCount;
		if (num > 99)
		{
			num = 99;
		}
		if (num < 10)
		{
			array[0].spriteName = NumSpriteName[num];
		}
		else
		{
			array[2].spriteName = NumSpriteName[num / 10];
			array[3].spriteName = NumSpriteName[num % 10];
		}
		if (mHDir == Def.DIR.RIGHT)
		{
			array[4].spriteName = "efc_footstamp_dsbL";
		}
		else
		{
			array[4].spriteName = "efc_footstamp_dsbR";
		}
		mBalloonSprite.ChangeAnime("GIMMICK_DIANA_BALLOON", array, true, 0, null);
		if (!mBalloonOpen && mStepCount > 0)
		{
			mBalloonOpen = true;
			StartCoroutine(CoBalloonOpen());
		}
		if (mBalloonOpen && mStepCount == 0)
		{
			mBalloonOpen = false;
			StartCoroutine(CoBalloonClose());
		}
	}

	private void SetHDir(Def.DIR dir)
	{
		if (dir != mHDir)
		{
			mHDir = dir;
			if (mHDir == Def.DIR.RIGHT)
			{
				mSprite.Scale = new Vector3(0.55f * Def.TILE_SCALE, 0.55f * Def.TILE_SCALE, 1f);
			}
			else
			{
				mSprite.Scale = new Vector3(-0.55f * Def.TILE_SCALE, 0.55f * Def.TILE_SCALE, 1f);
			}
			UpdateBalloon();
		}
	}

	public bool MoveCheck(out Def.DIR moveDir)
	{
		bool result = false;
		moveDir = Def.DIR.NONE;
		if (mStepCount <= 0)
		{
			return false;
		}
		if (IsGoal())
		{
			return false;
		}
		if (mWayPosIndex + 1 >= mGame.mCurrentStage.DianaWayPosList.Count)
		{
			return false;
		}
		IntVector2 intVector = mGame.mCurrentStage.DianaWayPosList[mWayPosIndex + 1] - mTilePos;
		if (intVector.x > 0)
		{
			moveDir = Def.DIR.RIGHT;
		}
		else if (intVector.x < 0)
		{
			moveDir = Def.DIR.LEFT;
		}
		else if (intVector.y < 0)
		{
			moveDir = Def.DIR.DOWN;
		}
		else if (intVector.y > 0)
		{
			moveDir = Def.DIR.UP;
		}
		IntVector2 pos = mGame.mCurrentStage.DianaWayPosList[mWayPosIndex + 1];
		PuzzleTile attribute = mPuzzle.GetAttribute(pos);
		PuzzleTile tile = mPuzzle.GetTile(pos, true);
		if (attribute != null && (!attribute.isMovable() || !attribute.isThrough()))
		{
			moveDir = Def.DIR.NONE;
			result = true;
		}
		else if (tile != null && !tile.isMovable())
		{
			moveDir = Def.DIR.NONE;
			result = true;
		}
		return result;
	}

	public bool Move()
	{
		Def.DIR moveDir;
		bool flag = MoveCheck(out moveDir);
		if (moveDir == Def.DIR.NONE)
		{
			if (flag)
			{
				Stop();
			}
			return false;
		}
		mState.Reset(STATE.MOVE, true);
		StartCoroutine(CoMove());
		return true;
	}

	private IEnumerator CoMove()
	{
		bool stop = false;
		while (mStepCount > 0)
		{
			Def.DIR dir;
			stop = MoveCheck(out dir);
			if (dir == Def.DIR.NONE)
			{
				break;
			}
			while (true)
			{
				PuzzleTile tile = mPuzzle.GetTile(mGame.mCurrentStage.DianaWayPosList[mWayPosIndex], false);
				if (tile == null || tile.Kind != Def.TILE_KIND.DIANA)
				{
					yield return 0;
					continue;
				}
				break;
			}
			mPuzzle.BeginMoveDiana(mPuzzleTile, dir);
			IntVector2 toTilePos = mGame.mCurrentStage.DianaWayPosList[mWayPosIndex + 1];
			Vector2 fromPos = mPuzzle.GetDefaultCoords(mTilePos);
			Vector2 toPos = mPuzzle.GetDefaultCoords(toTilePos);
			Vector2 distance = toPos - fromPos;
			Def.DIR hdir = mHDir;
			if (distance.x < 0f)
			{
				hdir = Def.DIR.LEFT;
			}
			if (distance.x > 0f)
			{
				hdir = Def.DIR.RIGHT;
			}
			if (mHDir != hdir)
			{
				SetHDir(hdir);
			}
			ChangeAnime(mSprite, "GIMMICK_DIANA_JUMP", 1);
			yield return new WaitForSeconds(1f / (float)Def.TARGET_FRAMERATE * 4f);
			float angle = 0f;
			while (angle < 90f)
			{
				angle += Time.deltaTime * 270f;
				if (angle > 90f)
				{
					angle = 90f;
				}
				float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
				mLocalPos = fromPos + distance * ratio;
				yield return 0;
			}
			yield return new WaitForSeconds(1f / (float)Def.TARGET_FRAMERATE * 1f);
			mTilePos = toTilePos;
			mWayPosIndex++;
			AddStepCount(-1);
			mPuzzle.OnDianaStepRemainChanged(mGame.mCurrentStage.DianaWayPosList.Count - 2 - mWayPosIndex);
			if (mWayPosIndex >= mGame.mCurrentStage.DianaWayPosList.Count - 2)
			{
				yield return new WaitForSeconds(1f / (float)Def.TARGET_FRAMERATE * 10f);
				AddStepCount(-99);
				Goal();
				yield break;
			}
		}
		if (stop)
		{
			Stop();
		}
		else
		{
			Stay();
		}
	}

	private IEnumerator CoBalloonOpen()
	{
		if (!(mBalloonSprite != null))
		{
			yield break;
		}
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 270f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = Mathf.Sin(angle * ((float)Math.PI / 180f));
			mBalloonSprite.transform.localScale = new Vector3(mBalloonBaseScale * ratio, mBalloonBaseScale * ratio, 1f);
			yield return 0;
		}
	}

	private IEnumerator CoBalloonClose()
	{
		if (!(mBalloonSprite != null))
		{
			yield break;
		}
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 270f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float ratio = 1f - Mathf.Sin(angle * ((float)Math.PI / 180f));
			mBalloonSprite.transform.localScale = new Vector3(mBalloonBaseScale * ratio, mBalloonBaseScale * ratio, 1f);
			yield return 0;
		}
	}

	public bool IsMoving()
	{
		if (mState.GetStatus() == STATE.MOVE)
		{
			return true;
		}
		return false;
	}

	public bool IsGoal()
	{
		if (mState.GetStatus() == STATE.AFTER_GOAL || mState.GetStatus() == STATE.GOAL)
		{
			return true;
		}
		return false;
	}

	public bool IsChance()
	{
		if (mWayPosIndex < 1)
		{
			return false;
		}
		if (mGame.mCurrentStage.DianaWayPosList.Count - mWayPosIndex <= 5)
		{
			return true;
		}
		return false;
	}

	public void AddStepCount(int amount)
	{
		if (!IsGoal())
		{
			mStepCount += amount;
			if (mStepCount < 0)
			{
				mStepCount = 0;
			}
			if (mStepCount > 99)
			{
				mStepCount = 99;
			}
			UpdateBalloon();
		}
	}

	public Vector3 GetCenterPos()
	{
		return mSprite.transform.position;
	}

	public STATE GetStatus()
	{
		return mState.GetStatus();
	}
}
