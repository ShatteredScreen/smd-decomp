using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleHud : MonoBehaviour
{
	public enum STATE
	{
		NONE = 0,
		WAIT = 1
	}

	private enum STAR_LEVEL
	{
		NONE = 0,
		BRONZE = 1,
		SILVER = 2,
		GOLD = 3
	}

	public delegate void TriggerDelegate();

	public delegate void OnBoosterDelegate(Def.BOOSTER_KIND kind);

	public delegate void OnCollectDelegate(PuzzleTile tile, Vector3 worldPosition, string overWriteImage);

	private StatusManager<STATE> mState = new StatusManager<STATE>(STATE.NONE);

	private GameMain mGame;

	private GameStatePuzzle mGameState;

	private PuzzleManager mPuzzleManager;

	private Partner mPartner;

	public int mBronzeScore;

	public int mSilverScore;

	public int mGoldScore;

	public int mMaxScore;

	private int mScoreDisplayNum;

	private int mLimitDisplayNum;

	private float mSkillDisplayPercent;

	private GameObject mAnchorBottom;

	private GameObject mAnchorBottomLeft;

	private GameObject mAnchorBottomRight;

	private GameObject mAnchorTop;

	private GameObject mAnchorTopLeft;

	private GameObject mAnchorTopRight;

	private int mBaseDepth = 40;

	private NumberImageString mScoreNum;

	private NumberImageString mTimeNum;

	private UISprite mHurryCircle;

	private bool mHurry;

	private float mHurryAngle;

	private bool mStageClear;

	private ComboEffect mCombo;

	private GameObject mNormaBack;

	private GameObject mLimitBack;

	private GameObject mScoreBack;

	private GameObject mPauseBack;

	private GameObject mBoosterBack;

	private GameObject mBoosterBlind;

	private GameObject mSkillBack;

	private List<GameObject> mBoxBackList = new List<GameObject>();

	private UISprite mGaugeFillBar;

	private UILabel mScorePopupLabel;

	private UISprite mBronzeStar;

	private UISprite mSilverStar;

	private UISprite mGoldStar;

	private bool mSkillButtonEnable;

	private UISprite mSkillButton;

	private UISprite mSkillGaugeBack;

	private UISprite mSkillGauge;

	private UISprite mSkillCursor;

	private bool mSkillMaxEnable;

	private bool mSkillMaxEndRequest;

	private float mSkillCursorBlinkAngle;

	private SMParticle mSkillUpParticle;

	private float mSkillUpRatio;

	private SMParticle mScoreUpParticle;

	private bool mDailyChallengeMode;

	private STAR_LEVEL mStarLevel;

	private bool mBronzeEffectEnd;

	private bool mSilverEffectEnd;

	private bool mGoldEffectEnd;

	private List<UISprite> mGaugePresentBoxSpriteList = new List<UISprite>();

	private List<SpecialSpawnData> mGaugePresentBoxSpawnList = new List<SpecialSpawnData>();

	private int mBoxCollectCount;

	private List<SMVerticalListItem> mBoosterList = new List<SMVerticalListItem>();

	private bool mBoosterOpenCloseBusy;

	private bool mBoosterOpen;

	private UIButton mBoosterOpenCloseButton;

	private float mBoosterBackPosX;

	private float buttonPitch = 68f;

	private UIButton mPauseButton;

	public TriggerDelegate OnPausePushed = delegate
	{
	};

	public OnBoosterDelegate OnBoosterPushed = delegate
	{
	};

	public TriggerDelegate OnSkillCharged = delegate
	{
	};

	public TriggerDelegate OnContinueEffectFinished = delegate
	{
	};

	public OnCollectDelegate OnCollect = delegate
	{
	};

	private Def.TILE_KIND[] mNormaKindArray;

	private Dictionary<Def.TILE_KIND, CollectImageString2> mTargets = new Dictionary<Def.TILE_KIND, CollectImageString2>();

	private Dictionary<Def.TILE_KIND, UISprite> mTargetImages = new Dictionary<Def.TILE_KIND, UISprite>();

	private NumberImageString mDianaStepRamain;

	private static readonly string[] HurryNumImageNameTbl = new string[10] { "pinch_num0", "pinch_num1", "pinch_num2", "pinch_num3", "pinch_num4", "pinch_num5", "pinch_num6", "pinch_num7", "pinch_num8", "pinch_num9" };

	private Def.EVENT_TYPE mEventType = Def.EVENT_TYPE.SM_NONE;

	private UISprite mEventItemFrameSprite;

	private NumberImageString mJewelScoreNum;

	private int mEventItemDispNum;

	private string[] mNumTable_Jewel = new string[10] { "num_red0", "num_red1", "num_red2", "num_red3", "num_red4", "num_red5", "num_red6", "num_red7", "num_red8", "num_red9" };

	public int ScoreTargetNum { get; set; }

	public int LimitTargetNum { get; set; }

	public float SkillTargetPercent { get; set; }

	private string GetNormaImageName(Def.TILE_KIND kind)
	{
		string result = string.Empty;
		switch (kind)
		{
		case Def.TILE_KIND.COLOR0:
			result = "norma_color4";
			break;
		case Def.TILE_KIND.COLOR1:
			result = "norma_color0";
			break;
		case Def.TILE_KIND.COLOR2:
			result = "norma_color1";
			break;
		case Def.TILE_KIND.COLOR3:
			result = "norma_color5";
			break;
		case Def.TILE_KIND.COLOR4:
			result = "norma_color2";
			break;
		case Def.TILE_KIND.COLOR5:
			result = "norma_color3";
			break;
		case Def.TILE_KIND.COLOR6:
			result = string.Empty;
			break;
		case Def.TILE_KIND.INGREDIENT0:
			result = "norma_ingredient0";
			break;
		case Def.TILE_KIND.INGREDIENT1:
			result = "norma_ingredient1";
			break;
		case Def.TILE_KIND.INGREDIENT2:
			result = "norma_ingredient2";
			break;
		case Def.TILE_KIND.THROUGH_WALL0:
		case Def.TILE_KIND.THROUGH_WALL1:
		case Def.TILE_KIND.THROUGH_WALL2:
			result = "norma_through_wall";
			break;
		case Def.TILE_KIND.BROKEN_WALL0:
		case Def.TILE_KIND.BROKEN_WALL1:
		case Def.TILE_KIND.BROKEN_WALL2:
			result = string.Empty;
			break;
		case Def.TILE_KIND.MOVABLE_WALL0:
		case Def.TILE_KIND.MOVABLE_WALL1:
		case Def.TILE_KIND.MOVABLE_WALL2:
			result = string.Empty;
			break;
		case Def.TILE_KIND.CAPTURE0:
		case Def.TILE_KIND.CAPTURE1:
		case Def.TILE_KIND.CAPTURE2:
			result = string.Empty;
			break;
		case Def.TILE_KIND.INCREASE:
			result = "norma_enemy0";
			break;
		case Def.TILE_KIND.GROWN_UP0:
		case Def.TILE_KIND.GROWN_UP1:
		case Def.TILE_KIND.GROWN_UP2:
			result = "norma_grown_up";
			break;
		case Def.TILE_KIND.PAIR0_PARTS:
		case Def.TILE_KIND.PAIR1_PARTS:
			result = "norma_cake";
			break;
		case Def.TILE_KIND.PAIR0_COMPLETE:
		case Def.TILE_KIND.PAIR1_COMPLETE:
			result = "norma_cake";
			break;
		case Def.TILE_KIND.FIND0:
		case Def.TILE_KIND.FIND1:
		case Def.TILE_KIND.FIND2:
		case Def.TILE_KIND.FIND3:
			result = "norma_stencil_runa";
			break;
		case Def.TILE_KIND.TALISMAN0_0:
			result = "talisman_00";
			break;
		case Def.TILE_KIND.TALISMAN1_0:
			result = "talisman_01";
			break;
		case Def.TILE_KIND.TALISMAN2_0:
			result = "talisman_02";
			break;
		case Def.TILE_KIND.STAR:
			result = "norma_starget";
			break;
		case Def.TILE_KIND.WAVE:
		case Def.TILE_KIND.BOMB:
		case Def.TILE_KIND.RAINBOW:
		case Def.TILE_KIND.WAVE_WAVE:
		case Def.TILE_KIND.BOMB_BOMB:
		case Def.TILE_KIND.RAINBOW_RAINBOW:
		case Def.TILE_KIND.WAVE_BOMB:
		case Def.TILE_KIND.BOMB_RAINBOW:
		case Def.TILE_KIND.WAVE_RAINBOW:
		case Def.TILE_KIND.PAINT:
		case Def.TILE_KIND.BOMB_PAINT:
		case Def.TILE_KIND.WAVE_PAINT:
		case Def.TILE_KIND.PAINT_RAINBOW:
		case Def.TILE_KIND.PAINT_PAINT:
			result = string.Empty;
			break;
		}
		return result;
	}

	private IEnumerator Start()
	{
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		mGameState = GameObject.Find("GameStatePuzzle").GetComponent<GameStatePuzzle>();
		mAnchorBottom = null;
		mAnchorBottomLeft = null;
		mAnchorBottomRight = null;
		mAnchorTop = null;
		mAnchorTopLeft = null;
		mAnchorTopRight = null;
		while (!mGameState.StageDataLoadFinished)
		{
			yield return 0;
		}
		if (mGame.mCurrentStage == null)
		{
			mState.Change(STATE.NONE);
			yield break;
		}
		mGoldScore = mGame.mCurrentStage.GoldScore;
		mSilverScore = mGame.mCurrentStage.SilverScore;
		mBronzeScore = mGame.mCurrentStage.BronzeScore;
		if (mDailyChallengeMode)
		{
			mSilverScore = mBronzeScore;
			mGoldScore = mBronzeScore;
		}
		mStarLevel = STAR_LEVEL.NONE;
		mBronzeEffectEnd = false;
		mSilverEffectEnd = false;
		mGoldEffectEnd = false;
		mBoxCollectCount = 0;
		mSkillUpRatio = 0f;
		Camera camera = mGame.mCamera;
		mAnchorBottom = Util.CreateAnchorWithChild("HudAnchorBottom", camera.gameObject, UIAnchor.Side.Bottom, camera).gameObject;
		mAnchorBottomLeft = Util.CreateAnchorWithChild("HudAnchorBottomLeft", camera.gameObject, UIAnchor.Side.BottomLeft, camera).gameObject;
		mAnchorTop = Util.CreateAnchorWithChild("HudAnchorTop", camera.gameObject, UIAnchor.Side.Top, camera).gameObject;
		mAnchorTopLeft = Util.CreateAnchorWithChild("HudAnchorTopLeft", camera.gameObject, UIAnchor.Side.TopLeft, camera).gameObject;
		mAnchorTopRight = Util.CreateAnchorWithChild("HudAnchorTopRight", camera.gameObject, UIAnchor.Side.TopRight, camera).gameObject;
		UIFont font = GameMain.LoadFont();
		ResImage resImagePuzzleHud = ResourceManager.LoadImage("PUZZLE_HUD");
		ResImage resImagePuzzleTile = ResourceManager.LoadImage("PUZZLE_TILE");
		UISprite sprite10 = Util.CreateSprite("LimitBack", mAnchorTopLeft, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(sprite10, "limit_back", mBaseDepth + 2, new Vector3(80f, -80f, Def.HUD_Z), Vector3.one, false);
		GameObject parent9 = sprite10.gameObject;
		mLimitBack = sprite10.gameObject;
		sprite10 = Util.CreateSprite("Remain", parent9, resImagePuzzleHud.Image);
		if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES)
		{
			Util.SetSpriteInfo(sprite10, "LC_text_move", mBaseDepth + 3, new Vector3(-14f, 46f, 0f), Vector3.one, false);
		}
		else
		{
			Util.SetSpriteInfo(sprite10, "LC_text_time", mBaseDepth + 3, new Vector3(-14f, 46f, 0f), Vector3.one, false);
		}
		mTimeNum = Util.CreateGameObject("LimitNum", parent9).AddComponent<NumberImageString>();
		mTimeNum.Init(3, 0, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, true, resImagePuzzleHud.Image);
		mTimeNum.SetColor(Color.white);
		mTimeNum.transform.localPosition = new Vector3(-16f, 2f, 0f);
		mTimeNum.SetPitch(36f);
		mHurryCircle = Util.CreateSprite("HurryCircle", parent9, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(mHurryCircle, "efc_circle", mBaseDepth + 4, new Vector3(-16f, 2f, 0f), Vector3.one, false);
		mHurryCircle.color = Color.red;
		mHurryCircle.enabled = false;
		sprite10 = Util.CreateSprite("ScoreBack", mAnchorTopRight, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(sprite10, "LC_score_back", mBaseDepth + 2, new Vector3(-144f, -320f, Def.HUD_Z), Vector3.one, false);
		parent9 = sprite10.gameObject;
		mScoreBack = sprite10.gameObject;
		mScoreNum = Util.CreateGameObject("ScoreNum", parent9).AddComponent<NumberImageString>();
		mScoreNum.Init(7, 0, mBaseDepth + 3, 0.6f, UIWidget.Pivot.Right, true, resImagePuzzleHud.Image);
		mScoreNum.transform.localPosition = new Vector3(128f, 14f, 0f);
		mScoreNum.SetPitch(36f);
		mGaugeFillBar = Util.CreateSprite("Fill", parent9, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(mGaugeFillBar, "scorebar", mBaseDepth + 3, new Vector3(16f, -15f, Def.HUD_Z), Vector3.one, false);
		mGaugeFillBar.type = UIBasicSprite.Type.Filled;
		mGaugeFillBar.fillDirection = UIBasicSprite.FillDirection.Horizontal;
		mGaugeFillBar.fillAmount = 0f;
		parent9 = mGaugeFillBar.gameObject;
		int gaugeWidth = 224;
		SMMapStageSetting stageInfo = mGame.mCurrentStageInfo;
		mMaxScore = Mathf.Max(1, mGoldScore);
		if (!mDailyChallengeMode)
		{
			if (stageInfo != null && mGame.PlayingFriendHelp == null)
			{
				for (int m = 0; m < stageInfo.BoxSpawnCondition.Count; m++)
				{
					if (stageInfo.BoxSpawnCondition[m] == Def.UNLOCK_TYPE.SCORE && stageInfo.BoxSpawnConditionID[m] > mMaxScore)
					{
						mMaxScore = stageInfo.BoxSpawnConditionID[m];
					}
				}
			}
			float ratio5 = (float)mGoldScore / (float)mMaxScore;
			mGoldStar = Util.CreateSprite("GoldStar", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(mGoldStar, "scorestar_back", mBaseDepth + 4, new Vector3((float)(-gaugeWidth) / 2f + (float)gaugeWidth * ratio5, -11f, 0f), Vector3.one, false);
			ratio5 = (float)mSilverScore / (float)mMaxScore;
			mSilverStar = Util.CreateSprite("SilverStar", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(mSilverStar, "scorestar_back", mBaseDepth + 4, new Vector3((float)(-gaugeWidth) / 2f + (float)gaugeWidth * ratio5, -11f, 0f), Vector3.one, false);
			ratio5 = (float)mBronzeScore / (float)mMaxScore;
			mBronzeStar = Util.CreateSprite("BronzeStar", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(mBronzeStar, "scorestar_back", mBaseDepth + 4, new Vector3((float)(-gaugeWidth) / 2f + (float)gaugeWidth * ratio5, -11f, 0f), Vector3.one, false);
		}
		else if (mGame.CanAcquireAccessoryIdListForWin != null && mGame.CanAcquireAccessoryIdListForWin.Count > 0)
		{
			int accessoryId = mGame.CanAcquireAccessoryIdListForWin[0];
			AccessoryData accessory = mGame.GetAccessoryData(accessoryId);
			if (accessory != null)
			{
				string str2 = string.Empty;
				switch (accessory.AccessoryType)
				{
				case AccessoryData.ACCESSORY_TYPE.TROPHY:
					str2 = "daily_button_icon00";
					break;
				case AccessoryData.ACCESSORY_TYPE.GROWUP_PIECE:
					str2 = "daily_button_icon01";
					break;
				default:
					str2 = "null";
					break;
				}
				float ratio5 = (float)mGoldScore / (float)mMaxScore;
				mGoldStar = Util.CreateSprite("RewardItem", parent9, resImagePuzzleHud.Image);
				Util.SetSpriteInfo(mGoldStar, str2, mBaseDepth + 4, new Vector3((float)(-gaugeWidth) / 2f + (float)gaugeWidth * ratio5, -2f, 0f), Vector3.one, false);
				mGoldStar.SetDimensions(34, 34);
			}
		}
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.STARGET)
		{
			mScoreBack.SetActive(false);
		}
		sprite10 = Util.CreateSprite("NormaBack", mAnchorTopRight, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(sprite10, "norma_back", mBaseDepth + 2, new Vector3(-121f, -228f, Def.HUD_Z), Vector3.one, false);
		parent9 = sprite10.gameObject;
		mNormaBack = sprite10.gameObject;
		mTargets.Clear();
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.SCORE)
		{
			sprite10 = Util.CreateSprite("Target", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(sprite10, "LC_text_norma", mBaseDepth + 3, new Vector3(-54f, 24f, 0f), Vector3.one, false);
			NumberImageString numberString = Util.CreateGameObject("TargetScore", parent9).AddComponent<NumberImageString>();
			numberString.Init(7, mGame.mCurrentStage.BronzeScore, mBaseDepth + 3, 0.7f, UIWidget.Pivot.Left, false, resImagePuzzleHud.Image);
			numberString.SetColor(Color.white);
			numberString.SetPitch(38f);
			float width2 = numberString.GetWidth();
			float x = (0f - width2) / 2f;
			numberString.transform.localPosition = new Vector3(x + 20f + 26.6f, -12f, 0f);
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.DEFEAT)
		{
			sprite10 = Util.CreateSprite("Target", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(sprite10, "LC_attackPeace", mBaseDepth + 3, new Vector3(-54f, 24f, 0f), Vector3.one, false);
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.GOAL)
		{
			sprite10 = Util.CreateSprite("Target", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(sprite10, "LC_text_footstamp_norma", mBaseDepth + 3, new Vector3(-36f, 12f, 0f), Vector3.one, false);
			mDianaStepRamain = Util.CreateGameObject("RemainNum", parent9).AddComponent<NumberImageString>();
			mDianaStepRamain.Init(2, 0, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, true, resImagePuzzleHud.Image, false, HurryNumImageNameTbl);
			mDianaStepRamain.SetColor(Color.white);
			mDianaStepRamain.transform.localPosition = new Vector3(66f, 0f, 0f);
			mDianaStepRamain.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
			mDianaStepRamain.SetPitch(36f);
		}
		else
		{
			mNormaKindArray = mGame.mCurrentStage.GetNormaKindArray();
			if (mNormaKindArray != null)
			{
				int _max = mNormaKindArray.Length;
				int count = 1;
				int targetNum = 0;
				for (int l = 0; l < _max; l++)
				{
					if (mGame.mCurrentStage.GetTargetNum(mNormaKindArray[l]) > 0)
					{
						targetNum++;
					}
				}
				float width = 250f;
				float pitch = width / (float)(targetNum + 1);
				for (int k = 0; k < _max; k++)
				{
					Def.TILE_KIND _kind = mNormaKindArray[k];
					string _icon = GetNormaImageName(_kind);
					int _num = mGame.mCurrentStage.GetTargetNum(_kind);
					if (_num > 0)
					{
						mTargetImages[_kind] = Util.CreateSprite("TargetIcon" + k, parent9, resImagePuzzleHud.Image);
						Util.SetSpriteInfo(mTargetImages[_kind], _icon, mBaseDepth + 4, new Vector3((0f - width) / 2f + pitch * (float)count, 12f, 0f), Vector3.one, false);
						mTargets[_kind] = Util.CreateGameObject("Target" + k, parent9).AddComponent<CollectImageString2>();
						mTargets[_kind].Init(_num, 0, mBaseDepth + 4, 0.45f, resImagePuzzleHud.Image);
						mTargets[_kind].transform.localPosition = new Vector3((0f - width) / 2f + pitch * (float)count, -24f, 0f);
						mTargets[_kind].SetPitch(36f);
						count++;
					}
				}
			}
		}
		parent9 = Util.CreateGameObject("Pause", mAnchorTopRight);
		parent9.transform.localPosition = new Vector3(-40f, -40f, Def.HUD_Z);
		mPauseBack = parent9;
		mPauseButton = Util.CreateJellyImageButton("PauseButton", parent9, resImagePuzzleHud.Image);
		Util.SetImageButtonInfo(mPauseButton, "pausebutton00", "pausebutton00", "pausebutton00", mBaseDepth + 3, new Vector3(0f, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mPauseButton, this, "OnPauseButtonPushed", UIButtonMessage.Trigger.OnClick);
		parent9 = Util.CreateGameObject("BoosterBack", parent9);
		parent9.transform.localPosition = new Vector3(160f, -330f, Def.HUD_Z);
		mBoosterBack = parent9;
		bool bCrushAllEnable = false;
		if (mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.ALL_CRUSH) > 0)
		{
			bCrushAllEnable = true;
		}
		else if (mGame.IsBoosterInSale(Def.BOOSTER_KIND.ALL_CRUSH))
		{
			bCrushAllEnable = true;
		}
		List<Def.BOOSTER_KIND> boosterList = new List<Def.BOOSTER_KIND>();
		if (!bCrushAllEnable)
		{
			boosterList.Add(Def.BOOSTER_KIND.ALL_CRUSH);
		}
		switch (mGame.mCurrentStage.WinType)
		{
		case Def.STAGE_WIN_CONDITION.PAIR:
			boosterList.Add(Def.BOOSTER_KIND.SELECT_COLLECT);
			boosterList.Add(Def.BOOSTER_KIND.BLOCK_CRUSH);
			boosterList.Add(Def.BOOSTER_KIND.SELECT_ONE);
			boosterList.Add(Def.BOOSTER_KIND.PAIR_MAKER);
			boosterList.Add(Def.BOOSTER_KIND.COLOR_CRUSH);
			break;
		case Def.STAGE_WIN_CONDITION.COLLECT:
		case Def.STAGE_WIN_CONDITION.TALISMAN:
			boosterList.Add(Def.BOOSTER_KIND.PAIR_MAKER);
			boosterList.Add(Def.BOOSTER_KIND.BLOCK_CRUSH);
			boosterList.Add(Def.BOOSTER_KIND.SELECT_ONE);
			boosterList.Add(Def.BOOSTER_KIND.SELECT_COLLECT);
			boosterList.Add(Def.BOOSTER_KIND.COLOR_CRUSH);
			break;
		default:
			boosterList.Add(Def.BOOSTER_KIND.PAIR_MAKER);
			boosterList.Add(Def.BOOSTER_KIND.SELECT_COLLECT);
			boosterList.Add(Def.BOOSTER_KIND.SELECT_ONE);
			boosterList.Add(Def.BOOSTER_KIND.BLOCK_CRUSH);
			boosterList.Add(Def.BOOSTER_KIND.COLOR_CRUSH);
			break;
		}
		if (bCrushAllEnable)
		{
			boosterList.Add(Def.BOOSTER_KIND.ALL_CRUSH);
		}
		float buttonPos2 = buttonPitch / 2f;
		for (int j = 0; j < boosterList.Count; j++)
		{
			BoosterSelectListItem item = new BoosterSelectListItem
			{
				data = mGame.mBoosterData[(int)boosterList[j]],
				num = -1,
				button = Util.CreateJellyImageButton("BoosterButton_" + j, parent9, resImagePuzzleHud.Image)
			};
			Util.SetImageButtonInfo(item.button, item.data.iconPuzzleHud, item.data.iconPuzzleHud, item.data.iconPuzzleHud, mBaseDepth + 4, new Vector3(buttonPos2, 0f, 0f), Vector3.one);
			Util.AddImageButtonMessage(item.button, this, "OnBoosterButtonPushed", UIButtonMessage.Trigger.OnClick);
			if (!mGame.mTutorialManager.IsTutorialCompleted(Def.TUTORIAL_INDEX.MAP_LEVEL8_3))
			{
				item.button.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
			else if (item.data.index == 14 && !bCrushAllEnable)
			{
				item.button.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
			sprite10 = Util.CreateSprite("BoosterNumBack" + j, item.button.gameObject, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(sprite10, "button_booster_num", mBaseDepth + 5, new Vector3(22f, -22f, -1f), Vector3.one, false);
			item.numLabel = Util.CreateLabel("BoosterNum" + j, sprite10.gameObject, font);
			Util.SetLabelInfo(item.numLabel, "99", mBaseDepth + 6, new Vector3(-1f, 0f, 0f), 20, 0, 0, UIWidget.Pivot.Center);
			item.button.gameObject.GetComponent<JellyImageButton>().SetBaseScale(0.9f);
			item.button.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
			buttonPos2 += buttonPitch;
			mBoosterList.Add(item);
		}
		mBoosterOpenCloseButton = Util.CreateJellyImageButton("BoosterOpenCloseButton", parent9, resImagePuzzleHud.Image);
		buttonPos2 -= 4f;
		Util.SetImageButtonInfo(mBoosterOpenCloseButton, "boosterpanel01", "boosterpanel01", "boosterpanel01", mBaseDepth + 4, new Vector3(buttonPos2, 0f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mBoosterOpenCloseButton, this, "OnBoosterOpenCloseButtonPushed", UIButtonMessage.Trigger.OnClick);
		sprite10 = Util.CreateSprite("BoosterBackFrame", parent9, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(sprite10, "boosterpanel00", mBaseDepth + 3, new Vector3(-40f, 0f, -2f), Vector3.one, false, UIWidget.Pivot.Left);
		sprite10.type = UIBasicSprite.Type.Sliced;
		sprite10.SetDimensions((int)((float)(mBoosterList.Count + 1) * buttonPitch) + 40, 84);
		sprite10 = Util.CreateSprite("BoosterBlindFrame", mAnchorTopLeft, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(sprite10, "boosterpanel03", mBaseDepth + 7, new Vector3(0f, 0f, -2f), Vector3.one, false, UIWidget.Pivot.Right);
		mBoosterBlind = sprite10.gameObject;
		mBoosterBackPosX = (0f - (float)Mathf.Max(mBoosterList.Count - 3, 0)) * buttonPitch;
		mBoosterOpenCloseBusy = false;
		mBoosterOpen = false;
		if (mSkillButtonEnable)
		{
			parent9 = Util.CreateGameObject("SkillBack", mAnchorTopLeft);
			parent9.transform.localPosition = new Vector3(60f, -200f, Def.HUD_Z);
			mSkillBack = parent9;
			mSkillGaugeBack = Util.CreateSprite("SkillBack", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(mSkillGaugeBack, "skillgauge_back", mBaseDepth + 2, Vector3.zero, Vector3.one, false);
			parent9 = mSkillGaugeBack.gameObject;
			mSkillGauge = Util.CreateSprite("SkillGauge", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(mSkillGauge, "skillgauge", mBaseDepth + 3, Vector3.zero, Vector3.one, false);
			mSkillGauge.type = UIBasicSprite.Type.Filled;
			mSkillGauge.fillDirection = UIBasicSprite.FillDirection.Radial360;
			mSkillGauge.invert = true;
			mSkillGauge.fillAmount = 0f;
			mSkillCursor = Util.CreateSprite("SkillCursor", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(mSkillCursor, "skillgauge_light", mBaseDepth + 4, Vector3.zero, Vector3.one, false);
			mSkillCursorBlinkAngle = 0f;
			mSkillButton = Util.CreateSprite("SkillButton", parent9, resImagePuzzleHud.Image);
			Util.SetSpriteInfo(mSkillButton, "skillbutton", mBaseDepth + 4, Vector3.zero, Vector3.one, false);
			mSkillUpParticle = Util.CreateGameObject("SkillGaugeParticle", parent9).AddComponent<SMParticle>();
			mSkillUpParticle.LoadFromPrefab("Particles/SkillGaugeParticle");
			mSkillUpParticle.gameObject.transform.localScale = new Vector3(1f, 1f, 0.01f);
			mSkillUpParticle.gameObject.transform.localPosition = new Vector3(0f, 0f, 50f);
		}
		mCombo = Util.CreateGameObject("Combo", mAnchorTopLeft).AddComponent<ComboEffect>();
		mCombo.transform.localPosition = new Vector3(120f, -150f, Def.HUD_Z);
		if (stageInfo != null && mGame.PlayingFriendHelp == null && !mDailyChallengeMode)
		{
			parent9 = mGaugeFillBar.gameObject;
			for (int i = 0; i < stageInfo.BoxSpawnCondition.Count; i++)
			{
				if (mGame.mPlayer.IsAccessoryUnlock(stageInfo.BoxID[i]) || (stageInfo.BoxSpawnCondition[i] != Def.UNLOCK_TYPE.STAR && stageInfo.BoxSpawnCondition[i] != Def.UNLOCK_TYPE.SCORE))
				{
					continue;
				}
				SpecialSpawnData data = new SpecialSpawnData(stageInfo.BoxSpawnTileKind[i], stageInfo.BoxKind[i], stageInfo.BoxID[i], stageInfo.BoxSpawnCondition[i], stageInfo.BoxSpawnConditionID[i]);
				mGaugePresentBoxSpawnList.Add(data);
				float ratio5 = 0f;
				if (stageInfo.BoxSpawnCondition[i] == Def.UNLOCK_TYPE.SCORE)
				{
					ratio5 = (float)stageInfo.BoxSpawnConditionID[i] / (float)mMaxScore;
				}
				else
				{
					switch (stageInfo.BoxSpawnConditionID[i])
					{
					case 1:
						ratio5 = (float)mBronzeScore / (float)mMaxScore;
						break;
					case 2:
						ratio5 = (float)mSilverScore / (float)mMaxScore;
						break;
					case 3:
						ratio5 = (float)mGoldScore / (float)mMaxScore;
						break;
					}
				}
				StartCoroutine(PresentBoxStay(mGaugePresentBoxSpawnList.IndexOf(data), parent9, new Vector3((float)(-gaugeWidth) / 2f + (float)gaugeWidth * ratio5, -11f, 0f)));
			}
		}
		mHurryAngle = 0f;
	}

	public void OnDestroy()
	{
		if (GameObject.Find("Anchor") != null)
		{
			if (mAnchorBottom != null)
			{
				UnityEngine.Object.Destroy(mAnchorBottom);
			}
			if (mAnchorBottomLeft != null)
			{
				UnityEngine.Object.Destroy(mAnchorBottomLeft);
			}
			if (mAnchorBottomRight != null)
			{
				UnityEngine.Object.Destroy(mAnchorBottomRight);
			}
			if (mAnchorTop != null)
			{
				UnityEngine.Object.Destroy(mAnchorTop);
			}
			if (mAnchorTopLeft != null)
			{
				UnityEngine.Object.Destroy(mAnchorTopLeft);
			}
			if (mAnchorTopRight != null)
			{
				UnityEngine.Object.Destroy(mAnchorTopRight);
			}
		}
	}

	private void Update()
	{
		UpdateScoreDisplay(false);
		UpdateLimitDisplay(false);
		UpdateSkillDisplay(false);
		UpdateEventDisplay(false);
		for (int i = 0; i < mBoosterList.Count; i++)
		{
			BoosterSelectListItem boosterSelectListItem = (BoosterSelectListItem)mBoosterList[i];
			int num = Mathf.Min(99, mGame.mPlayer.GetBoosterNum((Def.BOOSTER_KIND)boosterSelectListItem.data.index));
			if (boosterSelectListItem.num == num)
			{
				continue;
			}
			boosterSelectListItem.num = num;
			boosterSelectListItem.numLabel.text = string.Empty + num;
			if (boosterSelectListItem.data.index == 14 && !mGame.IsBoosterInSale(Def.BOOSTER_KIND.ALL_CRUSH))
			{
				if (num > 0)
				{
					boosterSelectListItem.button.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
				}
				else
				{
					boosterSelectListItem.button.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
				}
			}
		}
		mState.Update();
	}

	public void Init(PuzzleManager puzzleManager, Partner partner, bool skillButtonEnable, bool dailyChallengeMode)
	{
		mPuzzleManager = puzzleManager;
		mPartner = partner;
		mSkillButtonEnable = skillButtonEnable;
		mDailyChallengeMode = dailyChallengeMode;
	}

	public void OnPauseButtonPushed(GameObject go)
	{
		if (go.GetComponent<JellyImageButton>().IsEnable())
		{
			OnPausePushed();
		}
	}

	public void OnBoosterOpenCloseButtonPushed(GameObject go)
	{
		StartCoroutine(CoBoosterOpenClose(!mBoosterOpen));
	}

	private IEnumerator CoBoosterOpenClose(bool open)
	{
		mBoosterOpenCloseBusy = true;
		Vector3 posBuf = mBoosterBack.transform.localPosition;
		float beginX;
		float endX;
		if (open)
		{
			beginX = (0f - (float)Mathf.Max(mBoosterList.Count - 3, 0)) * buttonPitch;
			endX = 0f;
		}
		else
		{
			beginX = 0f;
			endX = (0f - (float)Mathf.Max(mBoosterList.Count - 3, 0)) * buttonPitch;
		}
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 360f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			mBoosterBackPosX = beginX + (endX - beginX) * Mathf.Sin(angle * ((float)Math.PI / 180f));
			mBoosterBack.transform.localPosition = new Vector3(mBoosterBackPosX, posBuf.y, posBuf.z);
			yield return 0;
		}
		mBoosterOpen = open;
		if (open)
		{
			Util.SetImageButtonGraphic(mBoosterOpenCloseButton, "boosterpanel02", "boosterpanel02", "boosterpanel02");
		}
		else
		{
			Util.SetImageButtonGraphic(mBoosterOpenCloseButton, "boosterpanel01", "boosterpanel01", "boosterpanel01");
		}
		if (open && (Util.ScreenOrientation == ScreenOrientation.LandscapeLeft || Util.ScreenOrientation == ScreenOrientation.LandscapeRight))
		{
			mPauseButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
		}
		else
		{
			mPauseButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
		}
		mBoosterOpenCloseBusy = false;
	}

	public void ForceOpenBoosterMenu()
	{
		StartCoroutine(CoOpenBoosterMenu());
	}

	private IEnumerator CoOpenBoosterMenu()
	{
		while (mBoosterOpenCloseBusy)
		{
			yield return 0;
		}
		if (!mBoosterOpen)
		{
			OnBoosterOpenCloseButtonPushed(null);
		}
	}

	public void OnBoosterButtonPushed(GameObject go)
	{
		string[] array = go.name.Split('_');
		BoosterSelectListItem boosterSelectListItem = (BoosterSelectListItem)mBoosterList[int.Parse(array[1])];
		if (boosterSelectListItem.button.gameObject.GetComponent<JellyImageButton>().IsEnable() && (boosterSelectListItem.data.index != 14 || mGame.mPlayer.GetBoosterNum(Def.BOOSTER_KIND.ALL_CRUSH) > 0 || mGame.IsBoosterInSale(Def.BOOSTER_KIND.ALL_CRUSH)))
		{
			OnBoosterPushed((Def.BOOSTER_KIND)boosterSelectListItem.data.index);
		}
	}

	public void CrushTile(Def.TILE_KIND kind)
	{
		if (mNormaKindArray == null)
		{
			return;
		}
		CollectImageString2 value = null;
		if (mTargets.TryGetValue(kind, out value) && value != null)
		{
			int collectNum = 0;
			int targetNum = 0;
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY)
			{
				targetNum = value.GetTargetNum() - 1;
			}
			else if (mGame.mCurrentStage.WinType != 0)
			{
				collectNum = value.GetCollectNum() + 1;
				targetNum = mGame.mCurrentStage.GetTargetNum(kind);
			}
			value.SetNum(targetNum, collectNum);
		}
	}

	public void AdjustNormaCount()
	{
		if (mNormaKindArray == null)
		{
			return;
		}
		foreach (KeyValuePair<Def.TILE_KIND, CollectImageString2> mTarget in mTargets)
		{
			int num = 0;
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY)
			{
				num = mPuzzleManager.GetTileCount(mTarget.Key);
				mTarget.Value.SetNum(num, 0);
			}
			else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.PAIR)
			{
				int collectNum = mTarget.Value.GetCollectNum();
				num = mGame.mCurrentStage.GetTargetNum(mTarget.Key);
				mTarget.Value.SetNum(num, collectNum);
			}
			else if (mGame.mCurrentStage.WinType != 0)
			{
				int exitCount = mPuzzleManager.GetExitCount(mTarget.Key);
				if (mTarget.Value.GetCollectNum() != exitCount)
				{
					mTarget.Value.SetNum(exitCount);
				}
			}
		}
	}

	public bool IsChance()
	{
		bool result = false;
		int num = 3;
		if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
		{
			num = 5;
		}
		if (mLimitDisplayNum < num)
		{
			return false;
		}
		int num2 = 1;
		int num3 = 0;
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.SCORE)
		{
			num3 = ScoreTargetNum;
			num2 = mBronzeScore;
		}
		else
		{
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.DEFEAT)
			{
				if (mPuzzleManager.IsBossGimmickDead() || mPuzzleManager.IsBossGimmickDeadBusy())
				{
					if (ScoreTargetNum < mBronzeScore && num3 == 0 && mScorePopupLabel == null)
					{
						StartCoroutine(ScorePopup());
					}
					return false;
				}
				return mPuzzleManager.IsBossGimmickWeak();
			}
			if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY)
			{
				if (mNormaKindArray != null)
				{
					num2 = 0;
					num3 = 0;
					foreach (KeyValuePair<Def.TILE_KIND, CollectImageString2> mTarget in mTargets)
					{
						num3 += mPuzzleManager.GetTileCount(mTarget.Key);
						num2 += mGame.mCurrentStage.GetTargetNum(mTarget.Key);
					}
				}
			}
			else
			{
				if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.GOAL)
				{
					if (mPuzzleManager.IsDianaGimmickGoal())
					{
						if (ScoreTargetNum < mBronzeScore && num3 == 0 && mScorePopupLabel == null)
						{
							StartCoroutine(ScorePopup());
						}
						return false;
					}
					return mPuzzleManager.IsDianaGimmickChance();
				}
				if (mNormaKindArray != null)
				{
					num2 = 0;
					num3 = 0;
					foreach (KeyValuePair<Def.TILE_KIND, CollectImageString2> mTarget2 in mTargets)
					{
						num3 += Mathf.Min(mTarget2.Value.GetCollectNum(), mTarget2.Value.GetTargetNum());
						num2 += mTarget2.Value.GetTargetNum();
					}
				}
			}
		}
		int num4 = (int)((float)num2 * 0.8f);
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.SCORE)
		{
			if (num3 >= num4 && num3 < num2)
			{
				result = true;
			}
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY)
		{
			num4 = num2 - num4;
			if (num4 > 5)
			{
				num4 = 5;
			}
			if (num4 < 1)
			{
				num4 = 1;
			}
			if (num3 <= num4 && num3 > 0)
			{
				result = true;
			}
			if (ScoreTargetNum < mBronzeScore && num3 == 0 && mScorePopupLabel == null)
			{
				StartCoroutine(ScorePopup());
			}
		}
		else
		{
			if (num4 < 1)
			{
				num4 = 1;
			}
			if (num3 >= num4 && num3 < num2)
			{
				result = true;
			}
			if (ScoreTargetNum < mBronzeScore && num3 >= num2 && mScorePopupLabel == null)
			{
				StartCoroutine(ScorePopup());
			}
		}
		return result;
	}

	public bool IsNormaCompleted()
	{
		int num = 1;
		int num2 = 0;
		if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.SCORE)
		{
			num2 = ScoreTargetNum;
			num = mBronzeScore;
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.DEFEAT)
		{
			if (mPuzzleManager.IsBossGimmickDead() || mPuzzleManager.IsBossGimmickDeadBusy())
			{
				num = 1;
				num2 = 1;
			}
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.ENEMY)
		{
			if (mNormaKindArray != null)
			{
				num = 0;
				num2 = 0;
				foreach (KeyValuePair<Def.TILE_KIND, CollectImageString2> mTarget in mTargets)
				{
					num2 += mPuzzleManager.GetTileCount(mTarget.Key);
					num += mGame.mCurrentStage.GetTargetNum(mTarget.Key);
				}
				num2 = num - num2;
			}
		}
		else if (mGame.mCurrentStage.WinType == Def.STAGE_WIN_CONDITION.GOAL)
		{
			if (mPuzzleManager.IsDianaGimmickGoal())
			{
				num = 1;
				num2 = 1;
			}
		}
		else if (mNormaKindArray != null)
		{
			num = 0;
			num2 = 0;
			foreach (KeyValuePair<Def.TILE_KIND, CollectImageString2> mTarget2 in mTargets)
			{
				num2 += Mathf.Min(mTarget2.Value.GetCollectNum(), mTarget2.Value.GetTargetNum());
				num += mTarget2.Value.GetTargetNum();
			}
		}
		if (num2 >= num)
		{
			return true;
		}
		return false;
	}

	public bool IsBronzeScoreCompleted()
	{
		return ScoreTargetNum >= mBronzeScore;
	}

	private IEnumerator ScorePopup()
	{
		ResImage atlasPuzzleHud = ResourceManager.LoadImage("PUZZLE_HUD");
		UIFont font = GameMain.LoadFont();
		GameObject parent = Util.CreateGameObject("ScorePopupParent", mScoreBack);
		parent.transform.localPosition = new Vector3(24f, -8f, 0f);
		UISprite sprite = Util.CreateSprite("ScorePopup", parent, atlasPuzzleHud.Image);
		Util.SetSpriteInfo(sprite, "LC_label_score_conditions", mBaseDepth + 4, Vector3.zero, Vector3.one, false, UIWidget.Pivot.Bottom);
		sprite.type = UIBasicSprite.Type.Sliced;
		sprite.SetDimensions(240, 42);
		string text = string.Empty + Mathf.Max(mBronzeScore - mScoreDisplayNum, 0);
		mScorePopupLabel = Util.CreateLabel("label", sprite.gameObject, font);
		Util.SetLabelInfo(mScorePopupLabel, text, mBaseDepth + 5, new Vector3(46f, 20f, 0f), 22, 0, 0, UIWidget.Pivot.Center);
		mScorePopupLabel.color = Color.white;
		mScorePopupLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		mScorePopupLabel.SetDimensions(120, 40);
		do
		{
			float angle3 = 0f;
			float baseScale2 = Mathf.Sin((float)Math.PI / 2f) * 0.1f;
			while (angle3 < 90f)
			{
				angle3 += Time.deltaTime * 720f;
				if (angle3 > 90f)
				{
					angle3 = 90f;
				}
				float scale = Mathf.Sin(angle3 * ((float)Math.PI / 180f));
				parent.transform.localScale = new Vector3(scale * (1f - baseScale2), scale * (1f + baseScale2), 1f);
				yield return 0;
			}
			angle3 = 90f;
			while (angle3 < 720f)
			{
				angle3 += Time.deltaTime * 1400f;
				if (angle3 > 720f)
				{
					angle3 = 720f;
				}
				baseScale2 = Mathf.Cos(angle3 / 8f * ((float)Math.PI / 180f));
				float scale2 = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * (0.1f * baseScale2);
				parent.transform.localScale = new Vector3(1f - scale2, 1f + scale2, 1f);
				yield return 0;
			}
			yield return new WaitForSeconds(3f);
			angle3 = 90f;
			while (angle3 < 180f)
			{
				angle3 += Time.deltaTime * 910f;
				if (angle3 > 180f)
				{
					angle3 = 180f;
				}
				float scale3 = Mathf.Sin(angle3 * ((float)Math.PI / 180f));
				parent.transform.localScale = new Vector3(scale3, scale3, 1f);
				yield return 0;
			}
			parent.SetActive(false);
			yield return new WaitForSeconds(1.5f);
			parent.SetActive(true);
		}
		while (mBronzeScore > ScoreTargetNum);
		UnityEngine.Object.Destroy(parent.gameObject);
		mScorePopupLabel = null;
	}

	public void Combo(int comboNum)
	{
		mCombo.OnCombo(comboNum);
	}

	public void ComboFinished(int comboNum)
	{
		mCombo.OnComboFinished();
	}

	public void StageClear()
	{
		mStageClear = true;
	}

	public void StartContinueEffect(int continueCount, int additionalMoves)
	{
		StartCoroutine(ContinueEffect(Def.CONTINUE_STEP[continueCount].addMove, (int)Def.CONTINUE_STEP[continueCount].addTime, additionalMoves));
	}

	private IEnumerator ContinueEffect(int addMove, int addTime, int additionalMoves)
	{
		yield return StartCoroutine(AddMovesEffect(addMove + additionalMoves, addTime + additionalMoves * 3));
		OnContinueEffectFinished();
	}

	public void StartRecoveryEffect(int addMove, int addTime)
	{
		StartCoroutine(RecoveryEffect(addMove, addTime));
	}

	private IEnumerator RecoveryEffect(int addMove, int addTime)
	{
		yield return StartCoroutine(AddMovesEffect(addMove, addTime));
	}

	public void StartScoreUpEffect(float rate)
	{
		if (mScoreUpParticle != null)
		{
			UnityEngine.Object.Destroy(mScoreUpParticle.gameObject);
			mScoreUpParticle = null;
		}
		if (mScoreUpParticle == null)
		{
			GameObject parent = mScoreBack.gameObject;
			mScoreUpParticle = Util.CreateGameObject("ScoreUpParticle", parent).AddComponent<SMParticle>();
			mScoreUpParticle.LoadFromPrefab("Particles/ScoreUpParticle");
			mScoreUpParticle.gameObject.transform.localPosition = new Vector3(20f, -20f, 20f);
			mScoreUpParticle.mParticle.Play();
			if (rate > 0.1f)
			{
				mScoreUpParticle.mParticle.startSize = 0.16f;
				mScoreUpParticle.mParticle.startColor = new Color(1f, 32f / 51f, 32f / 51f, 46f / 51f);
			}
		}
	}

	private void UpdateScoreDisplay(bool force)
	{
		if (!mScoreBack.activeSelf || (mScoreDisplayNum == ScoreTargetNum && !force))
		{
			return;
		}
		if (ScoreTargetNum - mScoreDisplayNum > 1000)
		{
			mScoreDisplayNum += 100;
		}
		else
		{
			mScoreDisplayNum += 10;
		}
		if (mScoreDisplayNum > ScoreTargetNum)
		{
			mScoreDisplayNum = ScoreTargetNum;
		}
		mScoreNum.SetNum(mScoreDisplayNum);
		float num = (float)mScoreDisplayNum / (float)mMaxScore;
		if (num > 1f)
		{
			num = 1f;
		}
		mGaugeFillBar.fillAmount = num;
		if (mScorePopupLabel != null)
		{
			mScorePopupLabel.text = string.Empty + Mathf.Max(mBronzeScore - mScoreDisplayNum, 0);
		}
		if (mDailyChallengeMode)
		{
			return;
		}
		if (mStarLevel == STAR_LEVEL.NONE && mScoreDisplayNum >= mBronzeScore)
		{
			mStarLevel = STAR_LEVEL.BRONZE;
			mGame.PlaySe("SE_RESULT_STAR_BRONZE", 0);
			StartCoroutine(StarCollectEffect(STAR_LEVEL.BRONZE, mBronzeStar.gameObject));
		}
		else if (mStarLevel == STAR_LEVEL.BRONZE && mScoreDisplayNum >= mSilverScore)
		{
			mStarLevel = STAR_LEVEL.SILVER;
			mGame.PlaySe("SE_RESULT_STAR_SILVER", 0);
			StartCoroutine(StarCollectEffect(STAR_LEVEL.SILVER, mSilverStar.gameObject));
		}
		else if (mStarLevel == STAR_LEVEL.SILVER && mScoreDisplayNum >= mGoldScore)
		{
			mStarLevel = STAR_LEVEL.GOLD;
			mGame.PlaySe("SE_RESULT_STAR_GOLD", 0);
			StartCoroutine(StarCollectEffect(STAR_LEVEL.GOLD, mGoldStar.gameObject));
		}
		for (int i = 0; i < mGaugePresentBoxSpawnList.Count; i++)
		{
			if (mGaugePresentBoxSpawnList[i].SpawnFinised)
			{
				continue;
			}
			int countDown = mGaugePresentBoxSpawnList[i].CountDown;
			if (mGaugePresentBoxSpawnList[i].SpawnCondition == Def.UNLOCK_TYPE.STAR)
			{
				switch (mGaugePresentBoxSpawnList[i].CountDown)
				{
				case 1:
					countDown = mBronzeScore;
					break;
				case 2:
					countDown = mSilverScore;
					break;
				case 3:
					countDown = mGoldScore;
					break;
				}
			}
			if (ScoreTargetNum >= countDown)
			{
				PuzzleTile tile = new PuzzleTile(mGaugePresentBoxSpawnList[i].Kind, Def.TILE_FORM.NORMAL, new IntVector2(0, 0), Vector3.zero, null, null, mGaugePresentBoxSpawnList[i].Category, mGaugePresentBoxSpawnList[i].ItemIndex);
				mGaugePresentBoxSpawnList[i].SpawnFinised = true;
				OnCollect(tile, mGaugePresentBoxSpriteList[i].transform.position, string.Empty);
				UnityEngine.Object.Destroy(mGaugePresentBoxSpriteList[i].gameObject);
				mGaugePresentBoxSpriteList[i] = null;
			}
		}
	}

	private void UpdateLimitDisplay(bool force)
	{
		if (!mLimitBack.activeSelf)
		{
			return;
		}
		int num = mLimitDisplayNum;
		if (mLimitDisplayNum != LimitTargetNum || force)
		{
			if (mLimitDisplayNum < LimitTargetNum)
			{
				mLimitDisplayNum++;
			}
			else
			{
				mLimitDisplayNum--;
			}
			mTimeNum.SetNum(mLimitDisplayNum);
		}
		if (num > 5 && mLimitDisplayNum <= 5 && !mStageClear)
		{
			mHurry = true;
			mTimeNum.SetImageNameTbl(HurryNumImageNameTbl);
		}
		if (mHurry && (mLimitDisplayNum > 5 || mStageClear))
		{
			mHurry = false;
			mTimeNum.ResetImageNameTbl();
		}
		if (mHurry)
		{
			mHurryAngle += Time.deltaTime * 90f;
			if (mHurryAngle > 90f)
			{
				mHurryAngle -= 90f;
			}
			float num2 = 1f + 0.5f * Mathf.Sin(mHurryAngle * ((float)Math.PI / 180f));
			mTimeNum.transform.localScale = new Vector3(num2, num2, 1f);
		}
		else
		{
			mTimeNum.transform.localScale = Vector3.one;
		}
	}

	private void UpdateSkillDisplay(bool force)
	{
		if (!mSkillButtonEnable)
		{
			return;
		}
		if (mSkillDisplayPercent != SkillTargetPercent || force)
		{
			if (mSkillDisplayPercent < SkillTargetPercent)
			{
				mSkillDisplayPercent += 5f;
				if (mSkillDisplayPercent > SkillTargetPercent)
				{
					mSkillDisplayPercent = SkillTargetPercent;
				}
			}
			else
			{
				mSkillDisplayPercent -= 5f;
				if (mSkillDisplayPercent < SkillTargetPercent)
				{
					mSkillDisplayPercent = SkillTargetPercent;
				}
			}
			float fillAmount = mSkillDisplayPercent / 100f;
			mSkillGauge.fillAmount = fillAmount;
		}
		mSkillCursorBlinkAngle += Time.deltaTime * 360f;
		if (mSkillDisplayPercent == 0f)
		{
			if (mSkillCursor.enabled)
			{
				mSkillCursor.enabled = false;
			}
			if (mSkillMaxEnable)
			{
				mSkillMaxEndRequest = true;
			}
			return;
		}
		if (mSkillDisplayPercent == 100f)
		{
			if (mSkillCursor.enabled)
			{
				mSkillCursor.enabled = false;
			}
			if (!mSkillMaxEnable)
			{
				StartCoroutine(SkillMaxBlink());
			}
			return;
		}
		if (!mSkillCursor.enabled)
		{
			mSkillCursor.enabled = true;
		}
		if (mSkillMaxEnable)
		{
			mSkillMaxEndRequest = true;
		}
		float num = mSkillDisplayPercent / 100f * 360f + 90f;
		float x = (0f - Mathf.Cos(num * ((float)Math.PI / 180f))) * 44f;
		float y = Mathf.Sin(num * ((float)Math.PI / 180f)) * 44f;
		mSkillCursor.transform.localPosition = new Vector3(x, y, 0f);
		float a = Mathf.Sin(mSkillCursorBlinkAngle * ((float)Math.PI / 180f));
		mSkillCursor.color = new Color(1f, 1f, 1f, a);
		mSkillCursor.transform.localRotation = Quaternion.Euler(0f, 0f, mSkillCursorBlinkAngle);
	}

	private IEnumerator SkillMaxBlink()
	{
		mSkillMaxEndRequest = false;
		mSkillMaxEnable = true;
		int baseDepth = 40;
		UISprite sprite = Util.CreateSprite(atlas: ResourceManager.LoadImage("PUZZLE_HUD").Image, name: "SkillMax", parent: mSkillBack);
		Util.SetSpriteInfo(sprite, "skillgauge_max", baseDepth + 4, new Vector3(0f, 0f, Def.HUD_Z - 2f), Vector3.one, false);
		sprite.color = new Color(1f, 1f, 1f, 0.4f);
		while (!mSkillMaxEndRequest)
		{
			yield return new WaitForSeconds(1f);
			sprite.color = new Color(1f, 1f, 1f, 0.6f);
			yield return new WaitForSeconds(1f / 30f);
			sprite.color = new Color(1f, 1f, 1f, 0.4f);
			yield return new WaitForSeconds(1f / 30f);
			sprite.color = new Color(1f, 1f, 1f, 0.6f);
			yield return new WaitForSeconds(1f / 30f);
			sprite.color = new Color(1f, 1f, 1f, 0.4f);
			yield return new WaitForSeconds(1f / 30f);
			sprite.color = new Color(1f, 1f, 1f, 0.6f);
			yield return new WaitForSeconds(1f / 30f);
			sprite.color = new Color(1f, 1f, 1f, 0.4f);
		}
		UnityEngine.Object.Destroy(sprite.gameObject);
		mSkillMaxEnable = false;
	}

	public void TransformSkillBall(GameObject target)
	{
		StartCoroutine(SkillGaugeMoveToTarget(target));
	}

	private IEnumerator SkillGaugeMoveToTarget(GameObject target)
	{
		ResImage resImagePuzzleHud = ResourceManager.LoadImage("PUZZLE_HUD");
		UISprite sprite2 = Util.CreateSprite("Light", mSkillBack.gameObject, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(sprite2, "get_star_back", mBaseDepth + 5, mSkillGaugeBack.transform.localPosition, Vector3.one, false);
		float rotateAngle3 = 0f;
		float sizeAngle3 = 0f;
		while (sizeAngle3 < 180f)
		{
			sizeAngle3 += Time.deltaTime * 240f;
			if (sizeAngle3 > 180f)
			{
				sizeAngle3 = 180f;
			}
			float ratio = Mathf.Sin(sizeAngle3 * ((float)Math.PI / 180f)) * 4f;
			sprite2.transform.localScale = new Vector3(ratio, ratio, 1f);
			rotateAngle3 += Time.deltaTime * 180f;
			sprite2.transform.localRotation = Quaternion.Euler(0f, 0f, rotateAngle3);
			yield return 0;
		}
		UnityEngine.Object.Destroy(sprite2.gameObject);
		Vector2 logScreenSize = Util.LogScreenSize();
		Vector3 targetPos = target.transform.position * logScreenSize.y / 2f;
		Vector3 gaugePos = mSkillGaugeBack.transform.position * logScreenSize.y / 2f;
		Vector2 distance = new Vector2(gaugePos.x - targetPos.x, gaugePos.y - targetPos.y);
		rotateAngle3 = Mathf.Atan2(distance.y, distance.x) * 57.29578f;
		float length = Mathf.Sqrt(distance.x * distance.x + distance.y * distance.y);
		sizeAngle3 = 90f;
		while (sizeAngle3 > 0f)
		{
			sizeAngle3 -= Time.deltaTime * 180f;
			if (sizeAngle3 < 0f)
			{
				sizeAngle3 = 0f;
			}
			float lengthRatio = Mathf.Sin(sizeAngle3 * ((float)Math.PI / 180f));
			rotateAngle3 -= Time.deltaTime * 360f;
			float x = Mathf.Cos(rotateAngle3 * ((float)Math.PI / 180f)) * (length * lengthRatio);
			float y = Mathf.Sin(rotateAngle3 * ((float)Math.PI / 180f)) * (length * lengthRatio);
			mSkillGaugeBack.transform.position = (targetPos + new Vector3(x, y, 0f)) / (logScreenSize.y / 2f);
			yield return 0;
		}
		sprite2 = Util.CreateSprite("Light", mSkillBack.gameObject, resImagePuzzleHud.Image);
		Util.SetSpriteInfo(sprite2, "get_star_back", mBaseDepth + 5, mSkillGaugeBack.transform.localPosition, Vector3.one, false);
		rotateAngle3 = 0f;
		sizeAngle3 = 0f;
		while (sizeAngle3 < 90f)
		{
			sizeAngle3 += Time.deltaTime * 240f;
			if (sizeAngle3 > 90f)
			{
				sizeAngle3 = 90f;
			}
			float ratio2 = Mathf.Sin(sizeAngle3 * ((float)Math.PI / 180f)) * 4f;
			sprite2.transform.localScale = new Vector3(ratio2, ratio2, 1f);
			rotateAngle3 += Time.deltaTime * 180f;
			sprite2.transform.localRotation = Quaternion.Euler(0f, 0f, rotateAngle3);
			yield return 0;
		}
		StartCoroutine(SkillGaugeRecover());
		while (sizeAngle3 < 180f)
		{
			sizeAngle3 += Time.deltaTime * 240f;
			if (sizeAngle3 > 180f)
			{
				sizeAngle3 = 180f;
			}
			float ratio3 = Mathf.Sin(sizeAngle3 * ((float)Math.PI / 180f)) * 4f;
			sprite2.transform.localScale = new Vector3(ratio3, ratio3, 1f);
			rotateAngle3 += Time.deltaTime * 180f;
			sprite2.transform.localRotation = Quaternion.Euler(0f, 0f, rotateAngle3);
			yield return 0;
		}
		UnityEngine.Object.Destroy(sprite2.gameObject);
	}

	private IEnumerator SkillGaugeRecover()
	{
		mSkillGaugeBack.transform.localPosition = Vector3.zero;
		ClearSkillPower();
		float angle = 0f;
		while (angle < 90f)
		{
			angle += Time.deltaTime * 180f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float scale = Mathf.Sin(angle * ((float)Math.PI / 180f)) * 1.2f;
			mSkillGaugeBack.transform.localScale = new Vector3(scale, scale, 1f);
			yield return 0;
		}
		mSkillGaugeBack.transform.localScale = Vector3.one;
	}

	public void SkillGaugeChargeUpStart()
	{
		if (!mSkillUpParticle.mParticle.isPlaying)
		{
			mSkillUpParticle.mParticle.Play();
			mSkillUpRatio = Def.BOOSTER_SKILL_CHARGE_RATIO;
		}
	}

	private IEnumerator PresentBoxStay(int index, GameObject parent, Vector3 pos)
	{
		ResImage resImagePuzzleTile = ResourceManager.LoadImage("PUZZLE_TILE");
		UISprite sprite = Util.CreateSprite("Box", parent, resImagePuzzleTile.Image);
		Util.SetSpriteInfo(sprite, mGame.mTileData[(int)mGaugePresentBoxSpawnList[index].Kind].DropImageNames[0][0], mBaseDepth + 5, pos, new Vector3(0.7f, 0.7f, 1f), false);
		mGaugePresentBoxSpriteList.Add(sprite);
		float appealTimer = 0f;
		while (!mGaugePresentBoxSpawnList[index].SpawnFinised)
		{
			appealTimer += Time.deltaTime;
			if (appealTimer >= 2f)
			{
				appealTimer -= 2f;
			}
			if (appealTimer >= 1.6f)
			{
				float time = appealTimer - 1.6f;
				if (time < 0.1f)
				{
					sprite.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, -10f));
				}
				else if (time < 0.2f)
				{
					sprite.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 10f));
				}
				else if (time < 0.3f)
				{
					sprite.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, -10f));
				}
				else
				{
					sprite.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 10f));
				}
			}
			else
			{
				sprite.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
			}
			yield return 0;
		}
	}

	public GameObject GetTutorialArrowTarget(Def.TUTORIAL_INDEX index)
	{
		GameObject result = null;
		switch (index)
		{
		case Def.TUTORIAL_INDEX.LEVEL1_5:
		case Def.TUTORIAL_INDEX.LEVEL11_2:
			result = mScoreBack.gameObject;
			break;
		case Def.TUTORIAL_INDEX.LEVEL1_6:
		case Def.TUTORIAL_INDEX.LEVEL2_5:
		case Def.TUTORIAL_INDEX.LEVEL10_4:
		case Def.TUTORIAL_INDEX.LEVEL15_1:
		case Def.TUTORIAL_INDEX.LEVEL16_0:
		case Def.TUTORIAL_INDEX.LEVEL18_0:
		case Def.TUTORIAL_INDEX.LEVEL21_1:
		case Def.TUTORIAL_INDEX.LEVEL31_2:
		case Def.TUTORIAL_INDEX.LEVEL62R_4:
		case Def.TUTORIAL_INDEX.LEVEL107SS_3:
		case Def.TUTORIAL_INDEX.TALISMAN_5:
			result = mBronzeStar.gameObject;
			break;
		case Def.TUTORIAL_INDEX.LEVEL1_7:
		case Def.TUTORIAL_INDEX.LEVEL7_0:
		case Def.TUTORIAL_INDEX.LEVEL21_0:
			result = mLimitBack.gameObject;
			break;
		case Def.TUTORIAL_INDEX.LEVEL8_0:
		case Def.TUTORIAL_INDEX.LEVEL10_1:
		case Def.TUTORIAL_INDEX.LEVEL11_0:
		case Def.TUTORIAL_INDEX.LEVEL15_0:
		case Def.TUTORIAL_INDEX.LEVEL31_0:
			result = mNormaBack.gameObject;
			break;
		case Def.TUTORIAL_INDEX.SELECT_ONE_2:
		case Def.TUTORIAL_INDEX.SELECT_ONE_3:
			result = GetBoosterButton(Def.BOOSTER_KIND.SELECT_ONE);
			if (CheckBoosterButtonOpen(Def.BOOSTER_KIND.SELECT_ONE))
			{
				ForceOpenBoosterMenu();
			}
			break;
		case Def.TUTORIAL_INDEX.SELECT_COLLECT_2:
		case Def.TUTORIAL_INDEX.SELECT_COLLECT_3:
			result = GetBoosterButton(Def.BOOSTER_KIND.SELECT_COLLECT);
			if (CheckBoosterButtonOpen(Def.BOOSTER_KIND.SELECT_COLLECT))
			{
				ForceOpenBoosterMenu();
			}
			break;
		case Def.TUTORIAL_INDEX.COLOR_CRUSH_2:
		case Def.TUTORIAL_INDEX.COLOR_CRUSH_3:
			result = GetBoosterButton(Def.BOOSTER_KIND.COLOR_CRUSH);
			if (CheckBoosterButtonOpen(Def.BOOSTER_KIND.COLOR_CRUSH))
			{
				ForceOpenBoosterMenu();
			}
			break;
		}
		return result;
	}

	private GameObject GetBoosterButton(Def.BOOSTER_KIND findKind)
	{
		for (int i = 0; i < mBoosterList.Count; i++)
		{
			BoosterSelectListItem boosterSelectListItem = (BoosterSelectListItem)mBoosterList[i];
			if (findKind == (Def.BOOSTER_KIND)boosterSelectListItem.data.index)
			{
				return boosterSelectListItem.button.gameObject;
			}
		}
		return null;
	}

	private bool CheckBoosterButtonOpen(Def.BOOSTER_KIND findKind)
	{
		int num = 0;
		for (int i = 0; i < mBoosterList.Count; i++)
		{
			BoosterSelectListItem boosterSelectListItem = (BoosterSelectListItem)mBoosterList[i];
			if (findKind == (Def.BOOSTER_KIND)boosterSelectListItem.data.index)
			{
				num = i;
				break;
			}
		}
		if (num < mBoosterList.Count - 3)
		{
			return true;
		}
		return false;
	}

	public bool IsNormaTarget(Def.TILE_KIND kind)
	{
		CollectImageString2 value;
		if (mTargets.TryGetValue(kind, out value) && value.GetCurrentNum() > 0)
		{
			return true;
		}
		return false;
	}

	public Vector3 GetCollectTargetPos(Def.TILE_KIND kind)
	{
		CollectImageString2 collectImageString = mTargets[kind];
		if (collectImageString != null)
		{
			return new Vector3(collectImageString.transform.position.x, collectImageString.transform.position.y, -30f);
		}
		return Vector3.zero;
	}

	public Vector3 GetScoreBoardPos()
	{
		return new Vector3(mScoreBack.transform.position.x, mScoreBack.transform.position.y, -30f);
	}

	public Vector3 GetNormaBoardPos()
	{
		return new Vector3(mNormaBack.transform.position.x, mNormaBack.transform.position.y, -30f);
	}

	public GameObject GetNormaBoard()
	{
		return mNormaBack.gameObject;
	}

	public Vector3 GetLimitBoardPos()
	{
		return new Vector3(mLimitBack.transform.position.x, mLimitBack.transform.position.y, -30f);
	}

	public bool GetSkillButtonEnable()
	{
		return mSkillButtonEnable;
	}

	public Vector3 GetSkillButtonPos()
	{
		if (mSkillBack != null)
		{
			return mSkillBack.transform.position;
		}
		return Vector3.zero;
	}

	public GameObject GetCollectBoxParent()
	{
		if (mBoxCollectCount <= 0)
		{
			return null;
		}
		return mBoxBackList[mBoxCollectCount - 1];
	}

	public void AddBoxCollectCount()
	{
		GameObject gameObject;
		if (Util.ScreenOrientation == ScreenOrientation.Portrait || Util.ScreenOrientation == ScreenOrientation.PortraitUpsideDown)
		{
			gameObject = Util.CreateGameObject("BoxBack" + mBoxCollectCount, mAnchorTopRight.gameObject);
			gameObject.transform.localPosition = new Vector3((float)mBoxCollectCount * -60f - 40f, -245f, Def.HUD_Z);
		}
		else
		{
			gameObject = Util.CreateGameObject("BoxBack" + mBoxCollectCount, mAnchorBottomLeft.gameObject);
			gameObject.transform.localPosition = new Vector3((float)mBoxCollectCount * 60f + 40f, 120f, Def.HUD_Z);
		}
		mBoxBackList.Add(gameObject);
		mBoxCollectCount++;
	}

	public bool IsAttackTarget(Def.TILE_KIND kind)
	{
		UISprite value;
		if (mTargetImages.TryGetValue(kind, out value))
		{
			return true;
		}
		return false;
	}

	public void SetAttackTarget(Def.TILE_KIND[] newTargets)
	{
		foreach (KeyValuePair<Def.TILE_KIND, UISprite> mTargetImage in mTargetImages)
		{
			UnityEngine.Object.Destroy(mTargetImage.Value.gameObject);
		}
		mTargetImages.Clear();
		ResImage resImage = ResourceManager.LoadImage("PUZZLE_HUD");
		int num = newTargets.Length;
		float num2 = 250f;
		float num3 = num2 / (float)(num + 1);
		for (int i = 0; i < num; i++)
		{
			Def.TILE_KIND tILE_KIND = newTargets[i];
			string normaImageName = GetNormaImageName(tILE_KIND);
			mTargetImages[tILE_KIND] = Util.CreateSprite("TargetIcon" + i, mNormaBack.gameObject, resImage.Image);
			Util.SetSpriteInfo(mTargetImages[tILE_KIND], normaImageName, mBaseDepth + 4, new Vector3((0f - num2) / 2f + num3 * (float)(i + 1), -14f, 0f), Vector3.one, false);
		}
	}

	public void SetDianaStepRemain(int remain)
	{
		if (mDianaStepRamain != null)
		{
			mDianaStepRamain.SetNum(remain);
		}
	}

	public void HudOn()
	{
		mNormaBack.gameObject.SetActive(true);
		mScoreBack.gameObject.SetActive(true);
		mLimitBack.gameObject.SetActive(true);
		mPauseBack.gameObject.SetActive(true);
		mBoosterBack.gameObject.SetActive(true);
		if (mSkillBack != null)
		{
			mSkillBack.gameObject.SetActive(true);
		}
		if (mEventItemFrameSprite != null)
		{
			mEventItemFrameSprite.gameObject.SetActive(true);
		}
		if (mJewelScoreNum != null)
		{
			mJewelScoreNum.gameObject.SetActive(true);
		}
	}

	public void HudOff()
	{
		mNormaBack.gameObject.SetActive(false);
		mScoreBack.gameObject.SetActive(false);
		mLimitBack.gameObject.SetActive(false);
		mPauseBack.gameObject.SetActive(false);
		mBoosterBack.gameObject.SetActive(false);
		if (mSkillBack != null)
		{
			mSkillBack.gameObject.SetActive(false);
		}
		if (mEventItemFrameSprite != null)
		{
			mEventItemFrameSprite.gameObject.SetActive(false);
		}
		if (mJewelScoreNum != null)
		{
			mJewelScoreNum.gameObject.SetActive(false);
		}
	}

	private IEnumerator HurryEffect()
	{
		mHurryCircle.enabled = true;
		float angle = 0f;
		while (angle <= 90f)
		{
			angle += Time.deltaTime * 200f;
			if (angle > 90f)
			{
				angle = 90f;
			}
			float scale = 2f * Mathf.Sin(angle * ((float)Math.PI / 180f));
			mHurryCircle.transform.localScale = new Vector3(scale, scale, 1f);
			yield return 0;
		}
		mHurryCircle.enabled = false;
	}

	private IEnumerator StarCollectEffect(STAR_LEVEL starLevel, GameObject target)
	{
		if (starLevel != STAR_LEVEL.BRONZE)
		{
			if (starLevel == STAR_LEVEL.SILVER)
			{
				while (!mBronzeEffectEnd)
				{
					yield return 0;
				}
			}
			else if (starLevel == STAR_LEVEL.GOLD)
			{
				while (!mSilverEffectEnd)
				{
					yield return 0;
				}
			}
		}
		Vector2 logScreenSize = Util.LogScreenSize();
		float scaleMax = 2f;
		float x = target.transform.position.x;
		float y = target.transform.position.y;
		Vector3 targetPos = new Vector3(x, y, 0f);
		ResImage atlasPuzzleHud = ResourceManager.LoadImage("PUZZLE_HUD");
		UISprite starSprite = Util.CreateSprite("Star", mGameState.mRoot, atlasPuzzleHud.Image);
		Util.SetSpriteInfo(starSprite, "get_star", 50, new Vector3(0f, 200f, -30f), Vector3.one, false);
		starSprite.transform.localScale = new Vector3(0.01f, 0.01f, 1f);
		UISprite lightSprite = Util.CreateSprite("Light", mGameState.mRoot, atlasPuzzleHud.Image);
		Util.SetSpriteInfo(lightSprite, "get_star_back", 49, new Vector3(0f, 200f, -30f), Vector3.one, false);
		lightSprite.transform.localScale = new Vector3(0.01f, 0.01f, 1f);
		Vector3 basePos = starSprite.transform.position;
		for (float angle3 = 0f; angle3 < 90f; angle3 += Time.deltaTime * 180f)
		{
			if (angle3 > 90f)
			{
				angle3 = 90f;
			}
			float scale = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * scaleMax;
			starSprite.transform.localScale = new Vector3(scale, scale, 1f);
			starSprite.transform.localRotation = Quaternion.Euler(0f, 0f, (0f - angle3) * 4f);
			lightSprite.transform.localScale = new Vector3(scale * 2.5f, scale * 2.5f, 1f);
			lightSprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle3);
			yield return 0;
		}
		starSprite.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
		for (float angle3 = 90f; angle3 < 180f; angle3 += Time.deltaTime * 180f)
		{
			if (angle3 > 180f)
			{
				angle3 = 180f;
			}
			float scale2 = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * scaleMax;
			lightSprite.transform.localScale = new Vector3(scale2 * 2.5f, scale2 * 2.5f, 1f);
			lightSprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle3);
			yield return 0;
		}
		UnityEngine.Object.Destroy(lightSprite.gameObject);
		for (float angle3 = 0f; angle3 < 90f; angle3 += Time.deltaTime * 180f)
		{
			if (angle3 > 90f)
			{
				angle3 = 90f;
			}
			float xpos = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * (targetPos.x - basePos.x);
			float ypos = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * (targetPos.y - basePos.y);
			float scale3 = Mathf.Cos(angle3 * ((float)Math.PI / 180f)) * scaleMax * 0.9f + 0.1f;
			starSprite.transform.position = new Vector3(basePos.x + xpos, basePos.y + ypos, basePos.z);
			starSprite.transform.localScale = new Vector3(scale3, scale3, 1f);
			yield return 0;
		}
		switch (starLevel)
		{
		case STAR_LEVEL.BRONZE:
			mBronzeEffectEnd = true;
			break;
		case STAR_LEVEL.SILVER:
			mSilverEffectEnd = true;
			break;
		case STAR_LEVEL.GOLD:
			mGoldEffectEnd = true;
			break;
		}
		Util.SetSpriteImageName(target.GetComponent<UISprite>(), "scorestar", Vector3.one, false);
		UnityEngine.Object.Destroy(starSprite.gameObject);
	}

	public IEnumerator AddMovesEffect(int addMove, int addTime)
	{
		mGame.PlaySe("SE_CONTINUE", 3);
		GameObject target = mLimitBack.gameObject;
		float scaleMax = 1f;
		Vector2 logScreenSize = Util.LogScreenSize();
		float x = target.transform.position.x * logScreenSize.y / 2f;
		float y = target.transform.position.y * logScreenSize.y / 2f;
		Vector3 targetPos = new Vector3(x, y, -30f);
		Vector3 basePos = new Vector3(0f, 0f, -30f);
		BIJImage atlas = ResourceManager.LoadImage("PUZZLE_HUD").Image;
		GameObject rootObj = Util.CreateGameObject("EffectRoot", mGameState.mRoot);
		rootObj.transform.localPosition = basePos;
		UISprite plusSprite = Util.CreateSprite("Plus", rootObj, atlas);
		Util.SetSpriteInfo(plusSprite, "continue_plus", 50, Vector3.zero, Vector3.one, false);
		UISprite unitSprite = Util.CreateSprite("Unit", rootObj, atlas);
		int num = 0;
		if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.MOVES)
		{
			Util.SetSpriteInfo(unitSprite, "continue_moves", 50, Vector3.zero, Vector3.one, false);
			num = addMove;
		}
		else if (mGame.mCurrentStage.LoseType == Def.STAGE_LOSE_CONDITION.TIME)
		{
			Util.SetSpriteInfo(unitSprite, "continue_sec", 50, Vector3.zero, Vector3.one, false);
			num = addTime;
		}
		string[] numImageNameTbl = new string[10] { "continue_num0", "continue_num1", "continue_num2", "continue_num3", "continue_num4", "continue_num5", "continue_num6", "continue_num7", "continue_num8", "continue_num9" };
		if (num >= 10)
		{
			UISprite numSprite3 = Util.CreateSprite("Unit", rootObj, atlas);
			Util.SetSpriteInfo(numSprite3, numImageNameTbl[num / 10], 50, Vector3.zero, Vector3.one, false);
			UISprite numSprite2 = Util.CreateSprite("Unit", rootObj, atlas);
			Util.SetSpriteInfo(numSprite2, numImageNameTbl[num % 10], 50, Vector3.zero, Vector3.one, false);
			int width2 = numSprite3.width + numSprite2.width + plusSprite.width + unitSprite.width;
			int posX14 = -width2 / 2;
			posX14 += plusSprite.width / 2;
			plusSprite.transform.localPosition = new Vector3(posX14, 0f, 0f);
			posX14 += plusSprite.width / 2;
			posX14 += numSprite3.width / 2;
			numSprite3.transform.localPosition = new Vector3(posX14, 0f, 0f);
			posX14 += numSprite3.width / 2;
			posX14 += numSprite2.width / 2;
			numSprite2.transform.localPosition = new Vector3(posX14, 0f, 0f);
			posX14 += numSprite2.width / 2;
			posX14 += unitSprite.width / 2;
			unitSprite.transform.localPosition = new Vector3(posX14, 0f, 0f);
		}
		else
		{
			UISprite numSprite2 = Util.CreateSprite("Unit", rootObj, atlas);
			Util.SetSpriteInfo(numSprite2, numImageNameTbl[num], 50, Vector3.zero, Vector3.one, false);
			int width = numSprite2.width + plusSprite.width + unitSprite.width;
			int posX6 = -width / 2;
			posX6 += plusSprite.width / 2;
			plusSprite.transform.localPosition = new Vector3(posX6, 0f, 0f);
			posX6 += plusSprite.width / 2;
			posX6 += numSprite2.width / 2;
			numSprite2.transform.localPosition = new Vector3(posX6, 0f, 0f);
			posX6 += numSprite2.width / 2;
			posX6 += unitSprite.width / 2;
			unitSprite.transform.localPosition = new Vector3(posX6, 0f, 0f);
		}
		rootObj.transform.localScale = new Vector3(0.01f, 0.01f, 1f);
		UISprite lightSprite = Util.CreateSprite("Light", mGameState.mRoot, atlas);
		Util.SetSpriteInfo(lightSprite, "get_star_back", 49, basePos, Vector3.one, false);
		lightSprite.transform.localScale = new Vector3(0.01f, 0.01f, 1f);
		for (float angle3 = 0f; angle3 < 90f; angle3 += Time.deltaTime * 180f)
		{
			if (angle3 > 90f)
			{
				angle3 = 90f;
			}
			float scale = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * scaleMax;
			rootObj.transform.localScale = new Vector3(scale, scale, 1f);
			rootObj.transform.localRotation = Quaternion.Euler(0f, 0f, (0f - angle3) * 4f);
			lightSprite.transform.localScale = new Vector3(scale * 2.5f, scale * 2.5f, 1f);
			lightSprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle3);
			yield return 0;
		}
		rootObj.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
		for (float angle3 = 90f; angle3 < 180f; angle3 += Time.deltaTime * 180f)
		{
			if (angle3 > 180f)
			{
				angle3 = 180f;
			}
			float scale2 = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * scaleMax;
			lightSprite.transform.localScale = new Vector3(scale2 * 2.5f, scale2 * 2.5f, 1f);
			lightSprite.transform.localRotation = Quaternion.Euler(0f, 0f, angle3);
			yield return 0;
		}
		UnityEngine.Object.Destroy(lightSprite.gameObject);
		for (float angle3 = 0f; angle3 < 90f; angle3 += Time.deltaTime * 180f)
		{
			if (angle3 > 90f)
			{
				angle3 = 90f;
			}
			float xpos = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * (targetPos.x - basePos.x);
			float ypos = Mathf.Sin(angle3 * ((float)Math.PI / 180f)) * (targetPos.y - basePos.y);
			float scale3 = Mathf.Cos(angle3 * ((float)Math.PI / 180f)) * scaleMax * 0.9f + 0.1f;
			rootObj.transform.localPosition = new Vector3(basePos.x + xpos, basePos.y + ypos, -30f);
			rootObj.transform.localScale = new Vector3(scale3, scale3, 1f);
			yield return 0;
		}
		UnityEngine.Object.Destroy(rootObj.gameObject);
	}

	public void AddSkillPower(float num)
	{
		float num2 = 0f;
		switch (mPartner.GetSkillRank())
		{
		case 1:
			num2 = mPartner.mData.skill0Ratio * (1f + mSkillUpRatio);
			break;
		case 2:
			num2 = mPartner.mData.skill1Ratio * (1f + mSkillUpRatio);
			break;
		}
		SkillTargetPercent += num * num2;
		if (SkillTargetPercent > 100f)
		{
			SkillTargetPercent = 100f;
		}
	}

	public void ClearSkillPower()
	{
		SkillTargetPercent = 0f;
	}

	public void SetSkillPower(float power)
	{
		SkillTargetPercent = power;
	}

	public bool IsSkillCharge()
	{
		if (SkillTargetPercent >= 100f)
		{
			return true;
		}
		return false;
	}

	public bool IsScorePopup()
	{
		return mScorePopupLabel != null;
	}

	public void SetLayout(ScreenOrientation o)
	{
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
		{
			mNormaBack.transform.parent = mAnchorTopRight.transform;
			mNormaBack.transform.localPosition = new Vector3(-122f, -320f, Def.HUD_Z);
			mScoreBack.transform.parent = mAnchorTopRight.transform;
			mScoreBack.transform.localPosition = new Vector3(-144f, -395f, Def.HUD_Z);
			mBoosterBack.transform.parent = mAnchorTopLeft.transform;
			mBoosterBack.transform.localPosition = new Vector3(mBoosterBackPosX, -320f, Def.HUD_Z);
			mBoosterBlind.transform.parent = mAnchorTopLeft.transform;
			mBoosterBlind.transform.localPosition = new Vector3(0f, -320f, Def.HUD_Z);
			mPauseBack.transform.parent = mAnchorTopRight.transform;
			mPauseBack.transform.localPosition = new Vector3(-40f, -40f, Def.HUD_Z);
			mPauseButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
			mCombo.transform.parent = mAnchorTopLeft.transform;
			mCombo.transform.localPosition = new Vector3(160f, -400f, Def.HUD_Z);
			for (int j = 0; j < mBoxBackList.Count; j++)
			{
				mBoxBackList[j].transform.parent = mAnchorTopRight.transform;
				mBoxBackList[j].transform.localPosition = new Vector3((float)j * -60f - 40f, -245f, Def.HUD_Z);
			}
			break;
		}
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
		{
			mNormaBack.transform.parent = mAnchorTopLeft.transform;
			mNormaBack.transform.localPosition = new Vector3(268f, -46f, Def.HUD_Z);
			mScoreBack.transform.parent = mAnchorTopLeft.transform;
			mScoreBack.transform.localPosition = new Vector3(250f, -120f, Def.HUD_Z);
			mBoosterBack.transform.parent = mAnchorBottomLeft.transform;
			mBoosterBack.transform.localPosition = new Vector3(mBoosterBackPosX, 40f, Def.HUD_Z);
			mBoosterBlind.transform.parent = mAnchorBottomLeft.transform;
			mBoosterBlind.transform.localPosition = new Vector3(0f, 40f, Def.HUD_Z);
			mPauseBack.transform.parent = mAnchorBottomLeft.transform;
			mPauseBack.transform.localPosition = new Vector3(350f, 40f, Def.HUD_Z);
			if (mBoosterOpen)
			{
				mPauseButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(false);
			}
			else
			{
				mPauseButton.gameObject.GetComponent<JellyImageButton>().SetButtonEnable(true);
			}
			mCombo.transform.parent = mAnchorTopRight.transform;
			mCombo.transform.localPosition = new Vector3(-160f, -100f, Def.HUD_Z);
			for (int i = 0; i < mBoxBackList.Count; i++)
			{
				mBoxBackList[i].transform.parent = mAnchorBottomLeft.transform;
				mBoxBackList[i].transform.localPosition = new Vector3((float)i * 60f + 40f, 120f, Def.HUD_Z);
			}
			break;
		}
		}
		SetLayoutEvent(o);
	}

	public void InitEvent(Def.EVENT_TYPE a_type)
	{
		if (a_type != Def.EVENT_TYPE.SM_NONE)
		{
			mEventType = a_type;
			ResImage resImage = ResourceManager.LoadImage("PUZZLE_HUD");
			ResImage resImage2 = ResourceManager.LoadImage("HUD");
			Def.EVENT_TYPE eVENT_TYPE = mEventType;
			if (eVENT_TYPE == Def.EVENT_TYPE.SM_LABYRINTH)
			{
				mEventItemFrameSprite = Util.CreateSprite("LabyrinthJewelFrame", mAnchorTopRight, resImage.Image);
				Util.SetSpriteInfo(mEventItemFrameSprite, "event_instruction_panel00", mBaseDepth + 1, new Vector3(-96f, -259f, Def.HUD_Z), Vector3.one, false);
				mEventItemFrameSprite.SetDimensions(185, 42);
				mEventItemFrameSprite.type = UIBasicSprite.Type.Sliced;
				mJewelScoreNum = Util.CreateGameObject("JewelNum", mEventItemFrameSprite.gameObject).AddComponent<NumberImageString>();
				mJewelScoreNum.Init(7, 0, mBaseDepth + 3, 0.8f, UIWidget.Pivot.Center, true, resImage2.Image, false, mNumTable_Jewel);
				mJewelScoreNum.transform.localPosition = new Vector3(31f, 1f, 0f);
				mJewelScoreNum.SetPitch(34f);
				mEventItemFrameSprite.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
			}
		}
	}

	public void UpdateEventDisplay(bool a_force)
	{
		if (mEventType == Def.EVENT_TYPE.SM_NONE)
		{
			return;
		}
		Def.EVENT_TYPE eVENT_TYPE = mEventType;
		if (eVENT_TYPE != Def.EVENT_TYPE.SM_LABYRINTH || !(mJewelScoreNum != null))
		{
			return;
		}
		short dispJewelCount = mGameState.DispJewelCount;
		if (mEventItemDispNum != dispJewelCount || a_force)
		{
			if (mEventItemDispNum < dispJewelCount)
			{
				mEventItemDispNum++;
			}
			mJewelScoreNum.SetNum(mEventItemDispNum);
		}
	}

	public void SetLayoutEvent(ScreenOrientation o)
	{
		if (mEventType == Def.EVENT_TYPE.SM_NONE)
		{
			return;
		}
		Def.EVENT_TYPE eVENT_TYPE = mEventType;
		if (eVENT_TYPE == Def.EVENT_TYPE.SM_LABYRINTH)
		{
			switch (o)
			{
			case ScreenOrientation.Portrait:
			case ScreenOrientation.PortraitUpsideDown:
				mEventItemFrameSprite.transform.parent = mAnchorTopRight.transform;
				mEventItemFrameSprite.transform.localPosition = new Vector3(-78f, -259f, Def.HUD_Z);
				break;
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				mEventItemFrameSprite.transform.parent = mAnchorBottomLeft.transform;
				mEventItemFrameSprite.transform.localPosition = new Vector3(78f, 106f, Def.HUD_Z);
				break;
			}
		}
	}
}
