using System;
using UnityEngine;

[Serializable]
public class SsAttrValueBase<T, KeyType> : SsAttrValue where KeyType : SsKeyFrameInterface
{
	[SerializeField]
	private int _RefKeyIndex;

	[SerializeField]
	private T _Value;

	public bool HasValue
	{
		get
		{
			return _RefKeyIndex == -1;
		}
	}

	public int RefKeyIndex
	{
		get
		{
			return _RefKeyIndex;
		}
		set
		{
			_RefKeyIndex = value;
		}
	}

	public T Value
	{
		get
		{
			return _Value;
		}
		set
		{
			_Value = value;
			_RefKeyIndex = -1;
		}
	}

	public void SetValue(object v)
	{
		Value = (T)v;
	}

	public static bool operator true(SsAttrValueBase<T, KeyType> p)
	{
		return p != null;
	}

	public static bool operator false(SsAttrValueBase<T, KeyType> p)
	{
		return p == null;
	}
}
