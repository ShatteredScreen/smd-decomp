using System;
using System.Reflection;
using System.Text;

public class StageRankingData : SMVerticalListItem, ICloneable, IComparable
{
	private string mNickName;

	public int uuid { get; set; }

	public int clr_score { get; set; }

	public string nickname
	{
		get
		{
			return mNickName;
		}
		set
		{
			mNickName = StringUtils.ReplaceEmojiToAsterisk(value);
		}
	}

	public int iconid { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	int IComparable.CompareTo(object _obj)
	{
		return CompareTo(_obj as StageRankingData);
	}

	public StageRankingData Clone()
	{
		return MemberwiseClone() as StageRankingData;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[StageRankingData: {0}]", stringBuilder.ToString());
	}

	public void CopyTo(StageRankingData _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(StageRankingData);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}

	public int CompareTo(StageRankingData _obj)
	{
		if (_obj == null)
		{
			return -1;
		}
		return _obj.clr_score - clr_score;
	}
}
