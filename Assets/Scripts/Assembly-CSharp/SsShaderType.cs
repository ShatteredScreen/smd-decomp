using System;

[Flags]
public enum SsShaderType
{
	NonColor = 0,
	MixColor = 1,
	MulColor = 2,
	AddColor = 3,
	SubColor = 4,
	ColorMask = 0xF,
	AlphaShift = 4,
	NonAlpha = 0,
	MixAlpha = 0x10,
	MulAlpha = 0x20,
	AddAlpha = 0x30,
	SubAlpha = 0x40,
	AlphaMask = 0xF0,
	MatColShift = 8,
	NonMatColor = 0,
	MulMatColor = 0x100,
	Mask = 0x1FF,
	Num = 0x32
}
