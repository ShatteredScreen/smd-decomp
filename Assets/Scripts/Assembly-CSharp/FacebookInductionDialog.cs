using UnityEngine;

public class FacebookInductionDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1
	}

	public enum TYPE
	{
		SIMPLE = 0,
		LUXURY = 1
	}

	public delegate void OnDialogClosed();

	public delegate void OnLoginPushed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	protected TYPE mType;

	private UIFont font = GameMain.LoadFont();

	private OnDialogClosed mCloseDialogCallback;

	private OnLoginPushed mLoginCallback;

	public void Init(OnLoginPushed login_callback, OnDialogClosed close_callback, TYPE type = TYPE.SIMPLE)
	{
		mType = type;
		mLoginCallback = login_callback;
		mCloseDialogCallback = close_callback;
	}

	public override void Start()
	{
		base.Start();
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void Update()
	{
		base.Update();
	}

	public override void BuildDialog()
	{
		switch (mType)
		{
		case TYPE.SIMPLE:
			BuildDialog_Simple();
			break;
		case TYPE.LUXURY:
			BuildDialog_Luxury();
			break;
		}
		CreateCancelButton("OnCancelPushed");
	}

	private void BuildDialog_Simple()
	{
		InitDialogFrame(DIALOG_SIZE.MEDIUM);
		InitDialogTitle(Localization.Get("FacebookLogin_Title"));
		CreateFacebookDesc(new Vector3(0f, -140f, 0f));
		CreateFacebookButton(new Vector3(0f, -285f, 0f));
	}

	private void BuildDialog_Luxury()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(Localization.Get("FacebookLogin_Title"));
		CreateFacebookDesc(new Vector3(0f, -95f, 0f));
		CreateFacebookButton(new Vector3(0f, -510f, 0f));
		UISprite sprite = Util.CreateSprite("FriendsImage", base.gameObject, image);
		Util.SetSpriteInfo(sprite, "313", mBaseDepth + 1, new Vector3(0f, -340f, 0f), Vector3.one, false);
	}

	private void CreateFacebookDesc(Vector3 pos, float pitch = 45f)
	{
		Vector3 position = pos;
		UILabel uILabel = Util.CreateLabel("Desc", base.gameObject, font);
		Util.SetLabelInfo(uILabel, Localization.Get("FacebookLogin_Desc1"), mBaseDepth + 3, position, 22, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(480, 35);
		position.y -= pitch;
		UILabel uILabel2 = Util.CreateLabel("Desc", base.gameObject, font);
		Util.SetLabelInfo(uILabel2, Localization.Get("FacebookLogin_Desc2"), mBaseDepth + 3, position, 22, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel2);
		uILabel2.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel2.SetDimensions(480, 35);
		position.y -= pitch;
		UILabel uILabel3 = Util.CreateLabel("Desc", base.gameObject, font);
		Util.SetLabelInfo(uILabel3, Localization.Get("FacebookLogin_Desc3"), mBaseDepth + 3, position, 22, 0, 0, UIWidget.Pivot.Center);
		DialogBase.SetDialogLabelEffect(uILabel3);
		uILabel3.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel3.SetDimensions(480, 35);
	}

	private void CreateFacebookButton(Vector3 pos)
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIButton uIButton = Util.CreateJellyImageButton("FacebookButton", base.gameObject, image);
		Util.SetImageButtonInfo(uIButton, "146", "146", "146", mBaseDepth + 3, pos, Vector3.one);
		JellyImageButton component = uIButton.gameObject.GetComponent<JellyImageButton>();
		Util.AddImageButtonMessage(uIButton, this, "OnFacebookButtonPushed", UIButtonMessage.Trigger.OnClick);
		UILabel uILabel = Util.CreateLabel("FacebookName", uIButton.gameObject, font);
		string text = Localization.Get("Scrounge_Desc");
		Util.SetLabelInfo(uILabel, text, mBaseDepth + 4, new Vector3(20f, 0f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(155, 50);
	}

	public override void OnCloseFinished()
	{
		if (mCloseDialogCallback != null)
		{
			mCloseDialogCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			PlaySE("SE_NEGATIVE");
			Close();
		}
	}

	private void PlaySE(string key, Res.SE_CHANNEL cannel = Res.SE_CHANNEL.AUTO, float vol = 1f)
	{
		mGame.PlaySe(key, (int)cannel, vol);
	}

	private void OnFacebookButtonPushed(GameObject go)
	{
		if (mState.GetStatus() == STATE.MAIN && mLoginCallback != null)
		{
			mLoginCallback();
		}
	}

	public void SetInputRejection(bool flag)
	{
		if (flag)
		{
			mState.Reset(STATE.WAIT, true);
		}
		else
		{
			mState.Reset(STATE.MAIN, true);
		}
	}
}
