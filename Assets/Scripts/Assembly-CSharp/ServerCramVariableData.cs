using System;
using System.Collections.Generic;

public class ServerCramVariableData : ICloneable
{
	public struct PurchaseCampaignInfo
	{
		public long StartOfferTime;

		public int ShowDialogCount;

		public int BuyCount;

		public int Place;
	}

	public struct LabyrinthInfo
	{
		public int DoubleUpChanceCount;

		public Dictionary<int, int> CourseClearCount;

		public int TotalStageClearCount;
	}

	private short mVersion;

	private short mPlayStartVersion;

	public int mPrevUUID;

	public int mPrevHiveId;

	public string mPrevSearchID = string.Empty;

	public static int mLocalUUID;

	public static int mLocalHiveId;

	public static string mLocalSearchID = string.Empty;

	public static long mBackupFileSize;

	public static long mBackupAdvFileSize;

	public static long mTransferFileSize;

	public static long mTransferAdvFileSize;

	public static int mOrgStageScore;

	public bool mPurchased { get; set; }

	public int EventAppealCount { get; set; }

	public Dictionary<int, PurchaseCampaignInfo> PurchaseCampaign { get; set; }

	public Dictionary<int, LabyrinthInfo> Labyrinth { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public ServerCramVariableData Clone()
	{
		return MemberwiseClone() as ServerCramVariableData;
	}

	public static ServerCramVariableData CreateForNewPlayer()
	{
		ServerCramVariableData serverCramVariableData = new ServerCramVariableData();
		GameMain instance = SingletonMonoBehaviour<GameMain>.Instance;
		serverCramVariableData.mVersion = 1290;
		serverCramVariableData.mPlayStartVersion = instance.mPlayer.Data.Version;
		serverCramVariableData.mPurchased = false;
		if (instance.mOptions.FirstPurchasedTime != DateTimeUtil.UnitLocalTimeEpoch)
		{
			serverCramVariableData.mPurchased = true;
		}
		serverCramVariableData.EventAppealCount = 0;
		serverCramVariableData.PurchaseCampaign = new Dictionary<int, PurchaseCampaignInfo>();
		serverCramVariableData.mPrevUUID = 0;
		serverCramVariableData.mPrevHiveId = 0;
		serverCramVariableData.mPrevSearchID = string.Empty;
		serverCramVariableData.Labyrinth = new Dictionary<int, LabyrinthInfo>();
		return serverCramVariableData;
	}

	public void Serialize(BIJBinaryWriter data)
	{
		data.WriteShort(1290);
		data.WriteShort(mPlayStartVersion);
		data.WriteBool(mPurchased);
		data.WriteInt(EventAppealCount);
		if (PurchaseCampaign != null)
		{
			data.WriteInt(PurchaseCampaign.Count);
			foreach (KeyValuePair<int, PurchaseCampaignInfo> item in PurchaseCampaign)
			{
				data.WriteInt(item.Key);
				data.WriteLong(item.Value.StartOfferTime);
				data.WriteInt(item.Value.ShowDialogCount);
				data.WriteInt(item.Value.BuyCount);
				data.WriteInt(item.Value.Place);
			}
		}
		data.WriteInt(mPrevUUID);
		data.WriteInt(mPrevHiveId);
		data.WriteUTF(mPrevSearchID);
		if (Labyrinth == null)
		{
			return;
		}
		data.WriteInt(Labyrinth.Keys.Count);
		foreach (KeyValuePair<int, LabyrinthInfo> item2 in Labyrinth)
		{
			data.WriteInt(item2.Key);
			data.WriteInt(item2.Value.DoubleUpChanceCount);
			data.WriteInt(item2.Value.TotalStageClearCount);
			data.WriteInt((item2.Value.CourseClearCount != null) ? item2.Value.CourseClearCount.Count : 0);
			if (item2.Value.CourseClearCount == null)
			{
				continue;
			}
			foreach (KeyValuePair<int, int> item3 in item2.Value.CourseClearCount)
			{
				data.WriteInt(item3.Key);
				data.WriteInt(item3.Value);
			}
		}
	}

	public static ServerCramVariableData Deserialize(BIJBinaryReader data)
	{
		ServerCramVariableData serverCramVariableData = new ServerCramVariableData();
		serverCramVariableData.mVersion = data.ReadShort();
		serverCramVariableData.mPlayStartVersion = data.ReadShort();
		serverCramVariableData.mPurchased = data.ReadBool();
		serverCramVariableData.EventAppealCount = data.ReadInt();
		if (serverCramVariableData.mVersion >= 1190)
		{
			serverCramVariableData.PurchaseCampaign = new Dictionary<int, PurchaseCampaignInfo>();
			int num = data.ReadInt();
			for (int i = 0; i < num; i++)
			{
				serverCramVariableData.PurchaseCampaign.Add(data.ReadInt(), new PurchaseCampaignInfo
				{
					StartOfferTime = data.ReadLong(),
					ShowDialogCount = data.ReadInt(),
					BuyCount = data.ReadInt(),
					Place = data.ReadInt()
				});
			}
		}
		int num2 = 1200;
		num2 = 1180;
		if (serverCramVariableData.mVersion >= num2)
		{
			serverCramVariableData.mPrevUUID = data.ReadInt();
			serverCramVariableData.mPrevHiveId = data.ReadInt();
			serverCramVariableData.mPrevSearchID = data.ReadUTF();
		}
		serverCramVariableData.Labyrinth = new Dictionary<int, LabyrinthInfo>();
		num2 = 1210;
		if (serverCramVariableData.mVersion >= num2)
		{
			int num3 = data.ReadInt();
			for (int j = 0; j < num3; j++)
			{
				LabyrinthInfo value = default(LabyrinthInfo);
				value.CourseClearCount = new Dictionary<int, int>();
				int key = data.ReadInt();
				value.DoubleUpChanceCount = data.ReadInt();
				value.TotalStageClearCount = data.ReadInt();
				int num4 = data.ReadInt();
				for (int k = 0; k < num4; k++)
				{
					int key2 = data.ReadInt();
					int value2 = data.ReadInt();
					value.CourseClearCount.Add(key2, value2);
				}
				serverCramVariableData.Labyrinth.Add(key, value);
			}
		}
		return serverCramVariableData;
	}

	public bool IsNewUser()
	{
		if (mPlayStartVersion / 10 == 129)
		{
			return true;
		}
		return false;
	}
}
