using UnityEngine;

public class NumberImageString : MonoBehaviour
{
	private UISprite[] mNumSprite;

	private EffectNumSprite[] mEffect;

	private int mKeta;

	private int mDispKeta;

	private bool mZeroPadding;

	private float mScale;

	private bool mUseEffect;

	private UIWidget.Pivot mPivot;

	private int mNum;

	private int mMaxNum;

	private float mPitch;

	private float mKerning;

	private static string[] numTbl = new string[10] { "num0", "num1", "num2", "num3", "num4", "num5", "num6", "num7", "num8", "num9" };

	private string[] mNumTbl;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void Init(int keta, int num, int depth, float scale, UIWidget.Pivot pivot, bool useEffect, BIJImage atlas, bool zeroPadding = false, string[] numTblOverride = null)
	{
		mZeroPadding = zeroPadding;
		mKeta = keta;
		mNum = num;
		mScale = scale;
		mUseEffect = useEffect;
		mPivot = pivot;
		if (numTblOverride != null)
		{
			mNumTbl = numTblOverride;
		}
		else
		{
			mNumTbl = numTbl;
		}
		mPitch = atlas.GetPixelSize(mNumTbl[0]).x;
		mNumSprite = new UISprite[keta];
		mEffect = new EffectNumSprite[keta];
		mMaxNum = 1;
		for (int i = 0; i < keta; i++)
		{
			mNumSprite[i] = Util.CreateSprite("Num", base.gameObject, atlas);
			Util.SetSpriteInfo(mNumSprite[i], string.Empty, depth, Vector3.zero, new Vector3(scale, scale, 1f), false);
			if (useEffect)
			{
				mEffect[i] = mNumSprite[i].gameObject.AddComponent<EffectNumSprite>();
				mEffect[i].SetImageNameTable(mNumTbl);
				mEffect[i].SetNum(0);
				mEffect[i].SetBaseScale(scale);
			}
			mMaxNum *= 10;
		}
		mMaxNum--;
		mKerning = 0f;
		SetNum(num);
	}

	public void SetNum(int num)
	{
		mNum = num;
		int num2 = num;
		if (num2 > mMaxNum)
		{
			num2 = mMaxNum;
		}
		mDispKeta = 0;
		for (int i = 0; i < mKeta; i++)
		{
			if (i != 0 && num2 == 0 && !mZeroPadding)
			{
				mNumSprite[i].enabled = false;
			}
			else
			{
				mNumSprite[i].enabled = true;
				mDispKeta++;
			}
			if (mUseEffect)
			{
				mEffect[i].ChangeNum(num2 % 10);
			}
			else
			{
				Util.SetSpriteImageName(mNumSprite[i], mNumTbl[num2 % 10], new Vector3(mScale, mScale, 1f), false);
			}
			num2 /= 10;
		}
		UpdatePosition();
	}

	public void UpdatePosition()
	{
		float num = mPitch * mScale;
		float num2;
		switch (mPivot)
		{
		case UIWidget.Pivot.Center:
			num2 = (float)mDispKeta * num / 2f - num / 2f + mKerning * (float)(mDispKeta - 1) / 2f;
			break;
		case UIWidget.Pivot.Left:
			num2 = (float)(mDispKeta - 1) * num + num / 2f + mKerning * (float)(mDispKeta - 1);
			break;
		case UIWidget.Pivot.Right:
			num2 = (0f - num) / 2f;
			break;
		default:
			num2 = (float)mDispKeta * num / 2f - num / 2f + mKerning * (float)(mDispKeta - 1) / 2f;
			break;
		}
		for (int i = 0; i < mKeta; i++)
		{
			mNumSprite[i].transform.localPosition = new Vector3(num2, 0f, 0f);
			num2 -= num + mKerning * (float)(i + 1);
		}
	}

	public void SetPitch(float pitch)
	{
		mPitch = pitch;
		UpdatePosition();
	}

	public void SetColor(Color col)
	{
		for (int i = 0; i < mKeta; i++)
		{
			mNumSprite[i].color = col;
		}
	}

	public void SetKerning(float kerning)
	{
		mKerning = kerning;
		UpdatePosition();
	}

	public void SetImageNameTbl(string[] replaceTbl)
	{
		mNumTbl = replaceTbl;
		if (mUseEffect)
		{
			for (int i = 0; i < mKeta; i++)
			{
				mEffect[i].SetImageNameTable(replaceTbl);
			}
		}
		SetNum(mNum);
	}

	public void ResetImageNameTbl()
	{
		mNumTbl = numTbl;
		if (mUseEffect)
		{
			for (int i = 0; i < mKeta; i++)
			{
				mEffect[i].SetImageNameTable(mNumTbl);
			}
		}
		SetNum(mNum);
	}

	public float GetWidth()
	{
		float num = 0f;
		for (int i = 0; i < mKeta; i++)
		{
			if (mNumSprite[i].enabled)
			{
				num += mPitch * mScale;
			}
		}
		return num;
	}
}
