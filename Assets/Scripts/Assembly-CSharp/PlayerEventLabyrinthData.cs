using System;
using System.Collections.Generic;
using System.Linq;

public class PlayerEventLabyrinthData : SMJsonData, ICloneable
{
	public int JewelAmount { get; set; }

	public short JewelGetChanceAddCount { get; set; }

	public bool DoneForceJewelGetChance { get; set; }

	public bool NoNextLottery { get; set; }

	public short NOUSE_PlayableCourseNo { get; set; }

	public short NOUSE_OpenNoticeCourseNo { get; set; }

	public Dictionary<short, Player.STAGE_STATUS> CourseStageStatus { get; set; }

	public Dictionary<short, Player.STAGE_STATUS> CourseLineStatus { get; set; }

	public short LastPlayCourseNo { get; set; }

	public Dictionary<short, List<short>> CurrentClearedStages { get; set; }

	public Dictionary<short, List<short>> ClearedStages { get; set; }

	public PlayerEventLabyrinthData()
	{
		Init();
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public void RestartCourse(short a_courseID)
	{
		if (CurrentClearedStages.ContainsKey(a_courseID))
		{
			CurrentClearedStages.Remove(a_courseID);
		}
	}

	public void Init()
	{
		JewelAmount = 0;
		JewelGetChanceAddCount = 0;
		DoneForceJewelGetChance = false;
		NoNextLottery = false;
		NOUSE_PlayableCourseNo = 100;
		NOUSE_OpenNoticeCourseNo = -1;
		CourseStageStatus = new Dictionary<short, Player.STAGE_STATUS>();
		CourseLineStatus = new Dictionary<short, Player.STAGE_STATUS>();
		LastPlayCourseNo = 100;
		CurrentClearedStages = new Dictionary<short, List<short>>();
		ClearedStages = new Dictionary<short, List<short>>();
	}

	public void AddJewelGetChanceCount()
	{
		JewelGetChanceAddCount++;
	}

	public void ResetJewelGetChanceCount()
	{
		JewelGetChanceAddCount = 0;
	}

	public int GetClearedStageCount()
	{
		int num = 0;
		foreach (KeyValuePair<short, List<short>> clearedStage in ClearedStages)
		{
			num += clearedStage.Value.Count;
		}
		return num;
	}

	public bool ClearStageProcess(short a_courseID, int a_stageNo, bool a_useCurrentStages = true, bool a_addClear = true)
	{
		bool result = false;
		if (!ClearedStages.ContainsKey(a_courseID))
		{
			ClearedStages.Add(a_courseID, new List<short>());
		}
		if (!ClearedStages[a_courseID].Contains((short)a_stageNo))
		{
			if (a_addClear)
			{
				ClearedStages[a_courseID].Add((short)a_stageNo);
			}
			result = true;
		}
		if (a_useCurrentStages)
		{
			if (!CurrentClearedStages.ContainsKey(a_courseID))
			{
				CurrentClearedStages.Add(a_courseID, new List<short>());
			}
			if (!CurrentClearedStages[a_courseID].Contains((short)a_stageNo) && a_addClear)
			{
				CurrentClearedStages[a_courseID].Add((short)a_stageNo);
			}
		}
		return result;
	}

	public override void SerializeToBinary(BIJBinaryWriter data)
	{
		data.WriteInt(JewelAmount);
		data.WriteShort(NOUSE_PlayableCourseNo);
		data.WriteShort(NOUSE_OpenNoticeCourseNo);
		data.WriteShort((short)CourseStageStatus.Count);
		List<short> list = CourseStageStatus.Keys.ToList();
		list.Sort();
		for (int i = 0; i < list.Count; i++)
		{
			short num = list[i];
			Player.STAGE_STATUS sTAGE_STATUS = CourseStageStatus[num];
			data.WriteShort(num);
			data.WriteShort((short)sTAGE_STATUS);
		}
		data.WriteShort((short)CourseLineStatus.Count);
		list = CourseLineStatus.Keys.ToList();
		list.Sort();
		for (int j = 0; j < list.Count; j++)
		{
			short num2 = list[j];
			Player.STAGE_STATUS sTAGE_STATUS2 = CourseLineStatus[num2];
			data.WriteShort(num2);
			data.WriteShort((short)sTAGE_STATUS2);
		}
		data.WriteShort(LastPlayCourseNo);
		data.WriteShort((short)CurrentClearedStages.Count);
		list = CurrentClearedStages.Keys.ToList();
		list.Sort();
		for (int k = 0; k < list.Count; k++)
		{
			short num3 = list[k];
			data.WriteShort(num3);
			List<short> list2 = CurrentClearedStages[num3];
			data.WriteShort((short)list2.Count);
			for (int l = 0; l < list2.Count; l++)
			{
				data.WriteShort(list2[l]);
			}
		}
		data.WriteShort((short)ClearedStages.Count);
		list = ClearedStages.Keys.ToList();
		list.Sort();
		for (int m = 0; m < list.Count; m++)
		{
			short num4 = list[m];
			data.WriteShort(num4);
			List<short> list3 = ClearedStages[num4];
			data.WriteShort((short)list3.Count);
			for (int n = 0; n < list3.Count; n++)
			{
				data.WriteShort(list3[n]);
			}
		}
		data.WriteShort(JewelGetChanceAddCount);
		data.WriteBool(DoneForceJewelGetChance);
		data.WriteBool(NoNextLottery);
	}

	public override void DeserializeFromBinary(BIJBinaryReader data, int reqVersion)
	{
		JewelAmount = data.ReadInt();
		NOUSE_PlayableCourseNo = data.ReadShort();
		NOUSE_OpenNoticeCourseNo = data.ReadShort();
		CourseStageStatus.Clear();
		int num = data.ReadShort();
		for (int i = 0; i < num; i++)
		{
			short key = data.ReadShort();
			Player.STAGE_STATUS value = (Player.STAGE_STATUS)data.ReadShort();
			CourseStageStatus.Add(key, value);
		}
		CourseLineStatus.Clear();
		num = data.ReadShort();
		for (int j = 0; j < num; j++)
		{
			short key2 = data.ReadShort();
			Player.STAGE_STATUS value2 = (Player.STAGE_STATUS)data.ReadShort();
			CourseLineStatus.Add(key2, value2);
		}
		LastPlayCourseNo = data.ReadShort();
		CurrentClearedStages.Clear();
		num = data.ReadShort();
		for (int k = 0; k < num; k++)
		{
			short key3 = data.ReadShort();
			short num2 = data.ReadShort();
			List<short> list = new List<short>();
			for (int l = 0; l < num2; l++)
			{
				short item = data.ReadShort();
				list.Add(item);
			}
			CurrentClearedStages.Add(key3, list);
		}
		ClearedStages.Clear();
		num = data.ReadShort();
		for (int m = 0; m < num; m++)
		{
			short key4 = data.ReadShort();
			short num3 = data.ReadShort();
			List<short> list2 = new List<short>();
			for (int n = 0; n < num3; n++)
			{
				short item2 = data.ReadShort();
				list2.Add(item2);
			}
			ClearedStages.Add(key4, list2);
		}
		if (reqVersion >= 1210)
		{
			JewelGetChanceAddCount = data.ReadShort();
			DoneForceJewelGetChance = data.ReadBool();
			NoNextLottery = data.ReadBool();
		}
	}

	public override string SerializeToJson()
	{
		return string.Empty;
	}

	public override void DeserializeFromJson(string a_jsonStr)
	{
		try
		{
			PlayerMapData obj = new PlayerMapData();
			new MiniJSONSerializer(JsonExtension.DEFAULT_MAPPER).Populate(ref obj, a_jsonStr);
		}
		catch
		{
		}
	}

	public PlayerEventLabyrinthData Clone()
	{
		return MemberwiseClone() as PlayerEventLabyrinthData;
	}
}
