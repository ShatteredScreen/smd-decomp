namespace live2d.framework
{
	public class L2DMatrix44
	{
		protected float[] tr = new float[16];

		public L2DMatrix44()
		{
			identity();
		}

		public void identity()
		{
			for (int i = 0; i < 16; i++)
			{
				tr[i] = ((i % 5 == 0) ? 1 : 0);
			}
		}

		public float[] getArray()
		{
			return tr;
		}

		public float[] getCopyMatrix()
		{
			return (float[])tr.Clone();
		}

		public void setMatrix(float[] tr)
		{
			if (tr != null && this.tr.Length == tr.Length)
			{
				for (int i = 0; i < 16; i++)
				{
					this.tr[i] = tr[i];
				}
			}
		}

		public float getScaleX()
		{
			return tr[0];
		}

		public float getScaleY()
		{
			return tr[5];
		}

		public float transformX(float src)
		{
			return tr[0] * src + tr[12];
		}

		public float transformY(float src)
		{
			return tr[5] * src + tr[13];
		}

		public float invertTransformX(float src)
		{
			return (src - tr[12]) / tr[0];
		}

		public float invertTransformY(float src)
		{
			return (src - tr[13]) / tr[5];
		}

		protected static void mul(float[] a, float[] b, float[] dst)
		{
			float[] array = new float[16];
			int num = 4;
			for (int i = 0; i < num; i++)
			{
				for (int j = 0; j < num; j++)
				{
					for (int k = 0; k < num; k++)
					{
						array[i + j * 4] += a[i + k * 4] * b[k + j * 4];
					}
				}
			}
			for (int i = 0; i < 16; i++)
			{
				dst[i] = array[i];
			}
		}

		public void multTranslate(float shiftX, float shiftY)
		{
			float[] a = new float[16]
			{
				1f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f,
				1f, 0f, shiftX, shiftY, 0f, 1f
			};
			mul(a, tr, tr);
		}

		public void translate(float x, float y)
		{
			tr[12] = x;
			tr[13] = y;
		}

		public void multTranslate(float shiftX, float shiftY, float shiftZ)
		{
			float[] a = new float[16]
			{
				1f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f,
				1f, 0f, shiftX, shiftY, shiftZ, 1f
			};
			mul(a, tr, tr);
		}

		public void translate(float x, float y, float z)
		{
			tr[12] = x;
			tr[13] = y;
			tr[14] = z;
		}

		public void translateX(float x)
		{
			tr[12] = x;
		}

		public void translateY(float y)
		{
			tr[13] = y;
		}

		public void multRotateX(float sin, float cos)
		{
			float[] a = new float[16]
			{
				1f,
				0f,
				0f,
				0f,
				0f,
				cos,
				sin,
				0f,
				0f,
				0f - sin,
				cos,
				0f,
				0f,
				0f,
				0f,
				1f
			};
			mul(a, tr, tr);
		}

		public void multRotateY(float sin, float cos)
		{
			float[] a = new float[16]
			{
				cos,
				0f,
				0f - sin,
				0f,
				0f,
				1f,
				0f,
				0f,
				sin,
				0f,
				cos,
				0f,
				0f,
				0f,
				0f,
				1f
			};
			mul(a, tr, tr);
		}

		public void multRotateZ(float sin, float cos)
		{
			float[] a = new float[16]
			{
				cos,
				sin,
				0f,
				0f,
				0f - sin,
				cos,
				0f,
				0f,
				0f,
				0f,
				1f,
				0f,
				0f,
				0f,
				0f,
				1f
			};
			mul(a, tr, tr);
		}

		public void multScale(float scaleX, float scaleY)
		{
			float[] a = new float[16]
			{
				scaleX, 0f, 0f, 0f, 0f, scaleY, 0f, 0f, 0f, 0f,
				1f, 0f, 0f, 0f, 0f, 1f
			};
			mul(a, tr, tr);
		}

		public void scale(float scaleX, float scaleY)
		{
			tr[0] = scaleX;
			tr[5] = scaleY;
		}

		public void multScale(float scaleX, float scaleY, float scaleZ)
		{
			float[] a = new float[16]
			{
				scaleX, 0f, 0f, 0f, 0f, scaleY, 0f, 0f, 0f, 0f,
				scaleZ, 0f, 0f, 0f, 0f, 1f
			};
			mul(a, tr, tr);
		}

		public void scale(float scaleX, float scaleY, float scaleZ)
		{
			tr[0] = scaleX;
			tr[5] = scaleY;
			tr[10] = scaleZ;
		}
	}
}
