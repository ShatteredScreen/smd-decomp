using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

public class PlayerMetaData : ICloneable
{
	public int Version { get; private set; }

	public int Series { get; private set; }

	public int StageNo { get; private set; }

	public int PlayCount { get; set; }

	public int WinCount { get; private set; }

	public int LoseCount { get; private set; }

	public int TotalMoves { get; private set; }

	public float TotalTime { get; private set; }

	public int TotalContinue { get; private set; }

	public int TotalUseBoost0 { get; private set; }

	public int TotalUseBoost1 { get; private set; }

	public int TotalUseBoost2 { get; private set; }

	public int TotalUseBoost3 { get; private set; }

	public int TotalUseBoost4 { get; private set; }

	public int TotalUseBoost5 { get; private set; }

	public int TotalUseBoost6 { get; private set; }

	public int TotalDropPresentBox0 { get; private set; }

	public int TotalDropPresentBox1 { get; private set; }

	public int TotalDropPresentBox2 { get; private set; }

	public bool HasProfile { get; private set; }

	public short ProfilePartnerID { get; private set; }

	public int ProfilePlayableMaxLevel { get; private set; }

	public long ProfileMaxCombo { get; private set; }

	public long ProfileMaxScore { get; private set; }

	public long ProfileTotalScore { get; private set; }

	public long ProfileTotalStar { get; private set; }

	public long ProfileTotalTrophy { get; private set; }

	public PlayerMetaData()
	{
		Version = 1290;
		Series = 0;
		PlayCount = 1;
		WinCount = 0;
		LoseCount = 0;
		HasProfile = false;
		ProfilePartnerID = 0;
		ProfilePlayableMaxLevel = Constants.FIRST_LEVEL;
		ProfileMaxCombo = 0L;
		ProfileMaxScore = 0L;
		ProfileTotalScore = 0L;
		ProfileTotalStar = 0L;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}

	public PlayerMetaData Clone()
	{
		return MemberwiseClone() as PlayerMetaData;
	}

	public override string ToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append(',');
			}
			stringBuilder.Append(propertyInfo.Name);
			stringBuilder.Append('=');
			stringBuilder.Append(propertyInfo.GetValue(this, null));
		}
		return string.Format("[PlayerMetadata: {0}]", stringBuilder.ToString());
	}

	public void CopyTo(PlayerMetaData _obj)
	{
		if (_obj == null)
		{
			return;
		}
		Type typeFromHandle = typeof(PlayerMetaData);
		Type type = GetType();
		PropertyInfo[] properties = type.GetProperties();
		foreach (PropertyInfo propertyInfo in properties)
		{
			PropertyInfo property = typeFromHandle.GetProperty(propertyInfo.Name);
			if (property != null)
			{
				property.SetValue(_obj, propertyInfo.GetValue(this, null), null);
			}
		}
	}

	public void Init(Def.SERIES a_series, int stage, Player pPlayer)
	{
		if (a_series == (Def.SERIES)Series && stage == StageNo)
		{
			PlayCount++;
			return;
		}
		Series = (int)a_series;
		StageNo = stage;
		PlayCount = 1;
		WinCount = 0;
		LoseCount = 0;
		TotalMoves = 0;
		TotalTime = 0f;
		TotalContinue = 0;
		TotalUseBoost0 = 0;
		TotalUseBoost1 = 0;
		TotalUseBoost2 = 0;
		TotalUseBoost3 = 0;
		TotalUseBoost4 = 0;
		TotalUseBoost5 = 0;
		TotalUseBoost6 = 0;
		TotalDropPresentBox0 = 0;
		TotalDropPresentBox1 = 0;
		TotalDropPresentBox2 = 0;
		HasProfile = true;
		ProfilePartnerID = pPlayer.Data.SelectedCompanion;
		ProfilePlayableMaxLevel = pPlayer.Level;
		ProfileMaxCombo = pPlayer.Data.MaxCombo;
		ProfileMaxScore = pPlayer.Data.MaxScore;
		ProfileTotalScore = pPlayer.GetRankingTotalScore();
		ProfileTotalStar = pPlayer.GetRankingTotalStar();
		ProfileTotalTrophy = pPlayer.GetTotalTrophy();
	}

	public void Update(Def.SERIES a_series, int stage, int stageMoves, float stageTime, int continueCount, Dictionary<Def.BOOSTER_KIND, int> useBooster, Dictionary<Def.TILE_KIND, int> dropPresentBox, Player pPlayer, bool isWon)
	{
		if (a_series == (Def.SERIES)Series && stage == StageNo)
		{
			TotalMoves += stageMoves;
			TotalTime += stageTime;
			TotalContinue += continueCount;
			int value = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.WAVE_BOMB, out value);
			int value2 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.TAP_BOMB2, out value2);
			int value3 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.RAINBOW, out value3);
			int value4 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.ADD_SCORE, out value4);
			int value5 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.SKILL_CHARGE, out value5);
			int value6 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.SELECT_ONE, out value6);
			int value7 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.SELECT_COLLECT, out value7);
			int value8 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.COLOR_CRUSH, out value8);
			int value9 = 0;
			useBooster.TryGetValue(Def.BOOSTER_KIND.ALL_CRUSH, out value9);
			TotalUseBoost0 += value;
			TotalUseBoost1 += value2;
			TotalUseBoost2 += value3;
			TotalUseBoost3 += value4;
			TotalUseBoost4 += value5;
			TotalUseBoost5 += value6;
			TotalUseBoost6 += value7;
			int value10 = 0;
			dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX0, out value10);
			int value11 = 0;
			dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX1, out value11);
			int value12 = 0;
			dropPresentBox.TryGetValue(Def.TILE_KIND.PRESENTBOX2, out value12);
			TotalDropPresentBox0 += value10;
			TotalDropPresentBox1 += value11;
			TotalDropPresentBox2 += value12;
			if (isWon)
			{
				WinCount++;
			}
			else
			{
				LoseCount++;
			}
			HasProfile = true;
			ProfilePartnerID = pPlayer.Data.SelectedCompanion;
			ProfilePlayableMaxLevel = pPlayer.Level;
			ProfileMaxCombo = pPlayer.Data.MaxCombo;
			ProfileMaxScore = pPlayer.Data.MaxScore;
			ProfileTotalScore = pPlayer.GetRankingTotalScore();
			ProfileTotalStar = pPlayer.GetRankingTotalStar();
			ProfileTotalTrophy = pPlayer.GetTotalTrophy();
		}
	}

	public void Serialize(BIJBinaryWriter data, int reqVersion)
	{
		data.WriteInt(reqVersion);
		data.WriteInt(Series);
		data.WriteInt(StageNo);
		data.WriteInt(PlayCount);
		data.WriteInt(TotalMoves);
		data.WriteFloat(TotalTime);
		data.WriteInt(TotalContinue);
		data.WriteInt(TotalUseBoost0);
		data.WriteInt(TotalUseBoost1);
		data.WriteInt(TotalUseBoost2);
		data.WriteInt(TotalUseBoost3);
		data.WriteInt(TotalUseBoost4);
		data.WriteInt(TotalUseBoost5);
		data.WriteInt(TotalUseBoost6);
		data.WriteInt(TotalDropPresentBox0);
		data.WriteInt(TotalDropPresentBox1);
		data.WriteInt(TotalDropPresentBox2);
		data.WriteBool(true);
		data.WriteShort(ProfilePartnerID);
		data.WriteInt(ProfilePlayableMaxLevel);
		data.WriteLong(ProfileMaxCombo);
		data.WriteLong(ProfileMaxScore);
		data.WriteLong(ProfileTotalScore);
		data.WriteLong(ProfileTotalStar);
		data.WriteLong(ProfileTotalTrophy);
	}

	public static PlayerMetaData Deserialize(BIJBinaryReader data, int reqVersion)
	{
		PlayerMetaData playerMetaData = new PlayerMetaData();
		int num = data.ReadInt();
		playerMetaData.Version = reqVersion;
		playerMetaData.Series = data.ReadInt();
		playerMetaData.StageNo = data.ReadInt();
		playerMetaData.PlayCount = data.ReadInt();
		playerMetaData.TotalMoves = data.ReadInt();
		playerMetaData.TotalTime = data.ReadFloat();
		playerMetaData.TotalContinue = data.ReadInt();
		playerMetaData.TotalUseBoost0 = data.ReadInt();
		playerMetaData.TotalUseBoost1 = data.ReadInt();
		playerMetaData.TotalUseBoost2 = data.ReadInt();
		playerMetaData.TotalUseBoost3 = data.ReadInt();
		playerMetaData.TotalUseBoost4 = data.ReadInt();
		playerMetaData.TotalUseBoost5 = data.ReadInt();
		playerMetaData.TotalUseBoost6 = data.ReadInt();
		playerMetaData.TotalDropPresentBox0 = data.ReadInt();
		playerMetaData.TotalDropPresentBox1 = data.ReadInt();
		playerMetaData.TotalDropPresentBox2 = data.ReadInt();
		playerMetaData.HasProfile = data.ReadBool();
		playerMetaData.ProfilePartnerID = data.ReadShort();
		playerMetaData.ProfilePlayableMaxLevel = data.ReadInt();
		playerMetaData.ProfileMaxCombo = data.ReadLong();
		playerMetaData.ProfileMaxScore = data.ReadLong();
		playerMetaData.ProfileTotalScore = data.ReadLong();
		playerMetaData.ProfileTotalStar = data.ReadLong();
		playerMetaData.ProfileTotalTrophy = data.ReadLong();
		return playerMetaData;
	}
}
