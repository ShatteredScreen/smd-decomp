using System;

public class AdvGetTransferParamProfile_Response : ICloneable
{
	[MiniJSONAlias("max_quest_id")]
	public int max_quest_id { get; set; }

	[MiniJSONAlias("xp")]
	public int xp { get; set; }

	[MiniJSONAlias("server_time")]
	public long ServerTime { get; set; }

	object ICloneable.Clone()
	{
		return Clone();
	}

	public AdvGetTransferParamProfile_Response Clone()
	{
		return MemberwiseClone() as AdvGetTransferParamProfile_Response;
	}
}
