using System.Collections.Generic;
using UnityEngine;

public class FriendRankingDialog : DialogBase
{
	public enum STATE
	{
		MAIN = 0,
		WAIT = 1,
		NETWORK_LOGIN00 = 2,
		NETWORK_LOGIN01 = 3,
		NETWORK_00_00 = 4,
		NETWORK_00_01 = 5,
		NETWORK_01_00 = 6,
		NETWORK_01_01 = 7,
		NETWORK_02_00 = 8,
		NETWORK_02_01 = 9
	}

	public delegate void OnDialogClosed();

	protected StatusManager<STATE> mState = new StatusManager<STATE>(STATE.MAIN);

	private long mBeforeData;

	private int mBeforeRanking;

	private OnDialogClosed mCallback;

	private GiftListItem mSelectedGiftItem;

	private List<GiftListItem> mAllGiftItems = new List<GiftListItem>();

	private SMVerticalListInfo mFriendItemListInfo;

	private SMVerticalList mFriendItemList;

	private FriendItem mFriendItem;

	private List<FriendItem> mAllFriendItems = new List<FriendItem>();

	private List<MyFriend> sendableFriends;

	private GameObject mDialogCenter;

	private UIButton mTabButtonStar;

	private UIButton mTabButtonStage;

	private UIButton mTabButtonTrophy;

	private UISprite mAttentionReceive;

	private UISprite mAttentionSend;

	private UISprite mAttentionRescue;

	private float mAttentionReceiveCount;

	private float mAttentionSendCount;

	private float mAttentionRescueCount;

	private UISprite mCommingSoonIcon;

	private UISprite mCommingSoonWindow;

	private UILabel mCommingSoonLabel;

	private BIJSNS mSNS;

	private ConfirmDialog mConfirmDialog;

	private int mSNSCallCount;

	private float mBasePosY;

	private bool mAllData;

	private string mImageTab0;

	private string mImageTab1;

	private string mImageTab2;

	private UILabel mCautionLabel;

	public GameMain.RANKINGMode Mode { get; set; }

	public override void Start()
	{
		Mode = GameMain.RANKINGMode.STAR;
		base.Start();
	}

	private new void OnDestroy()
	{
		base.OnDestroy();
	}

	public override void Update()
	{
		base.Update();
		if (GetBusy())
		{
			mState.Update();
			return;
		}
		switch (mState.GetStatus())
		{
		}
		mState.Update();
	}

	public override void BuildDialog()
	{
		BIJImage image = ResourceManager.LoadImage("HUD").Image;
		UIFont uIFont = GameMain.LoadFont();
		string titleDescKey = Localization.Get("Ranking_Title");
		InitDialogFrame(DIALOG_SIZE.LARGE);
		InitDialogTitle(titleDescKey);
		mDialogCenter = Util.CreateGameObject("GridRoot", base.gameObject);
		GameMain.RANKINGMode mode = Mode;
		mTabButtonStar = Util.CreateImageButton("TabA", base.gameObject, image);
		Util.SetImageButtonInfo(mTabButtonStar, mImageTab0, mImageTab0, mImageTab0, mBaseDepth + 1, new Vector3(-138f, -327f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonStar, this, "OnStarTabPushed", UIButtonMessage.Trigger.OnClick);
		mTabButtonStage = Util.CreateImageButton("TabB", base.gameObject, image);
		Util.SetImageButtonInfo(mTabButtonStage, mImageTab1, mImageTab1, mImageTab1, mBaseDepth + 1, new Vector3(0f, -327f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonStage, this, "OnStageTabPushed", UIButtonMessage.Trigger.OnClick);
		mTabButtonTrophy = Util.CreateImageButton("TabC", base.gameObject, image);
		Util.SetImageButtonInfo(mTabButtonTrophy, mImageTab2, mImageTab2, mImageTab2, mBaseDepth + 1, new Vector3(138f, -327f, 0f), Vector3.one);
		Util.AddImageButtonMessage(mTabButtonTrophy, this, "OnTrophyTabPushed", UIButtonMessage.Trigger.OnClick);
		ActivateTab(mode);
		SetLayout(Util.ScreenOrientation);
		CreateCancelButton("OnCancelPushed");
	}

	public override void OnScreenChanged(ScreenOrientation o)
	{
		if (!GetBusy())
		{
			SetLayout(o);
		}
	}

	private void SetLayout(ScreenOrientation o)
	{
		Vector2 vector = Util.LogScreenSize();
		switch (o)
		{
		case ScreenOrientation.Portrait:
		case ScreenOrientation.PortraitUpsideDown:
			mTabButtonStar.transform.localPosition = new Vector3(-138f, -327f, 0f);
			mTabButtonStage.transform.localPosition = new Vector3(0f, -327f, 0f);
			mTabButtonTrophy.transform.localPosition = new Vector3(138f, -327f, 0f);
			mImageTab0 = "freranking_tab00";
			mImageTab1 = "LC_freranking_tab01";
			mImageTab2 = "LC_freranking_tab02";
			Util.SetImageButtonGraphic(mTabButtonStar, mImageTab0, mImageTab0, mImageTab0);
			Util.SetImageButtonGraphic(mTabButtonStage, mImageTab1, mImageTab1, mImageTab1);
			Util.SetImageButtonGraphic(mTabButtonTrophy, mImageTab2, mImageTab2, mImageTab2);
			mBasePosY = -29f;
			break;
		case ScreenOrientation.LandscapeLeft:
		case ScreenOrientation.LandscapeRight:
			mTabButtonStar.transform.localPosition = new Vector3(322f, 143f, 0f);
			mTabButtonStage.transform.localPosition = new Vector3(322f, 0f, 0f);
			mTabButtonTrophy.transform.localPosition = new Vector3(322f, -143f, 0f);
			mImageTab0 = "freranking_tabBeside00";
			mImageTab1 = "LC_freranking_tabBeside01";
			mImageTab2 = "LC_freranking_tabBeside02";
			Util.SetImageButtonGraphic(mTabButtonStar, mImageTab0, mImageTab0, mImageTab0);
			Util.SetImageButtonGraphic(mTabButtonStage, mImageTab1, mImageTab1, mImageTab1);
			Util.SetImageButtonGraphic(mTabButtonTrophy, mImageTab2, mImageTab2, mImageTab2);
			mBasePosY = 50f;
			break;
		}
	}

	private void ChangeMode(GameMain.RANKINGMode mode)
	{
		if (!mAllData)
		{
			sendableFriends = mGame.AllFriends();
			mAllData = true;
		}
		if (Mode != mode)
		{
		}
		Mode = mode;
		if (mFriendItemList != null)
		{
			Object.Destroy(mFriendItemList.gameObject);
			mFriendItemList = null;
		}
		if ((bool)mCommingSoonIcon)
		{
			Object.Destroy(mCommingSoonIcon.gameObject);
			mCommingSoonIcon = null;
		}
		if (mCommingSoonWindow != null)
		{
			Object.Destroy(mCommingSoonWindow.gameObject);
			mCommingSoonWindow = null;
		}
		if (mCommingSoonLabel != null)
		{
			Object.Destroy(mCommingSoonLabel.gameObject);
			mCommingSoonLabel = null;
		}
		List<SMVerticalListItem> list = new List<SMVerticalListItem>();
		List<FriendItem> list2 = new List<FriendItem>();
		FriendRankingItem friendRankingItem = new FriendRankingItem();
		switch (mode)
		{
		case GameMain.RANKINGMode.STAR:
			mFriendItemListInfo = new SMVerticalListInfo(1, 512f, 472f, 1, 512f, 472f, 1, 121);
			if (sendableFriends != null && sendableFriends.Count > 0)
			{
				foreach (MyFriend sendableFriend in sendableFriends)
				{
					list2.Add(FriendItem.Make(sendableFriend, GameMain.RANKINGMode.STAR));
					friendRankingItem = new FriendRankingItem();
				}
				list2.Sort(StarItemSort);
				for (int j = 0; j < list2.Count; j++)
				{
					list.Add(list2[j]);
				}
			}
			if (mCautionLabel != null)
			{
				NGUITools.SetActive(mCautionLabel.gameObject, false);
			}
			break;
		case GameMain.RANKINGMode.STAGE:
			mFriendItemListInfo = new SMVerticalListInfo(1, 512f, 472f, 1, 512f, 472f, 1, 121);
			if (sendableFriends != null && sendableFriends.Count > 0)
			{
				foreach (MyFriend sendableFriend2 in sendableFriends)
				{
					list2.Add(FriendItem.Make(sendableFriend2, GameMain.RANKINGMode.STAGE));
					friendRankingItem = new FriendRankingItem();
				}
				list2.Sort(LevelItemSort);
				for (int k = 0; k < list2.Count; k++)
				{
					list.Add(list2[k]);
				}
			}
			if (mCautionLabel != null)
			{
				NGUITools.SetActive(mCautionLabel.gameObject, false);
			}
			break;
		case GameMain.RANKINGMode.TROPHY:
		{
			mFriendItemListInfo = new SMVerticalListInfo(1, 512f, 422f, 1, 512f, 422f, 1, 121);
			if (sendableFriends != null && sendableFriends.Count > 0)
			{
				foreach (MyFriend sendableFriend3 in sendableFriends)
				{
					list2.Add(FriendItem.Make(sendableFriend3, GameMain.RANKINGMode.TROPHY));
					friendRankingItem = new FriendRankingItem();
				}
			}
			list2.Sort(TrophyItemSort);
			for (int i = 0; i < list2.Count; i++)
			{
				list.Add(list2[i]);
			}
			if (mCautionLabel == null)
			{
				UIFont atlasFont = GameMain.LoadFont();
				string text = string.Format(Localization.Get("TrophyMailInfo"));
				mCautionLabel = Util.CreateLabel("CautionLabel", base.gameObject, atlasFont);
				Util.SetLabelInfo(mCautionLabel, text, mBaseDepth + 4, new Vector3(0f, 198f, 0f), 24, 0, 0, UIWidget.Pivot.Center);
				mCautionLabel.color = Def.DEFAULT_MESSAGE_COLOR;
			}
			NGUITools.SetActive(mCautionLabel.gameObject, true);
			break;
		}
		}
		mDialogCenter.transform.localPosition = new Vector3(0f, -45f);
		if (mode != GameMain.RANKINGMode.TROPHY)
		{
			mFriendItemList = SMVerticalList.Make(mDialogCenter, this, mFriendItemListInfo, list, 0f, 30f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		}
		else
		{
			mFriendItemList = SMVerticalList.Make(mDialogCenter, this, mFriendItemListInfo, list, 0f, 5f, mBaseDepth + 1, true, UIScrollView.Movement.Vertical);
		}
		mFriendItemList.OnCreateItem = OnCreateItem_Friend;
	}

	private bool IsFacebookLogined()
	{
		BIJSNS sns = SocialManager.Instance[SNS.Facebook];
		return GameMain.IsSNSLogined(sns);
	}

	private void OnCreateItem_Friend(GameObject itemGO, int index, SMVerticalListItem item, SMVerticalListItem lastItem)
	{
		BIJImage image = ResourceManager.LoadImage("ICON").Image;
		BIJImage image2 = ResourceManager.LoadImage("HUD").Image;
		UIFont atlasFont = GameMain.LoadFont();
		int num = mBaseDepth + 5;
		UIButton uIButton = null;
		UISprite uISprite = null;
		UISprite uISprite2 = null;
		UISprite uISprite3 = null;
		UISprite uISprite4 = null;
		UISprite uISprite5 = null;
		string empty = string.Empty;
		string empty2 = string.Empty;
		mGame = SingletonMonoBehaviour<GameMain>.Instance;
		float num2 = 0f;
		uISprite = Util.CreateSprite("FriendBoard", itemGO, image2);
		Util.SetSpriteInfo(uISprite, "instruction_panel", mBaseDepth + 1, new Vector3(-10f, 0f, 0f), Vector3.one, false);
		uISprite.type = UIBasicSprite.Type.Sliced;
		uISprite.SetDimensions(490, 115);
		FriendItem friendItem = item as FriendItem;
		MyFriend info = friendItem.Info;
		if (info == null)
		{
			BIJLog.E("friend is null!!");
			return;
		}
		int num3 = 0;
		UITexture uITexture = null;
		UISprite sprite;
		if (GameMain.IsFacebookIconEnable() && info.Data.IconID >= 10000)
		{
			if (info.Icon != null)
			{
				uITexture = Util.CreateGameObject("Icon", uISprite.gameObject).AddComponent<UITexture>();
				uITexture.depth = num + 2;
				uITexture.transform.localPosition = new Vector3(-42f, -7f, 0f);
				uITexture.mainTexture = info.Icon;
				uITexture.SetDimensions(65, 65);
			}
			else
			{
				uISprite5 = Util.CreateSprite("FriendIcon", uISprite.gameObject, image);
				Util.SetSpriteInfo(uISprite5, "icon_chara_facebook", num + 1, new Vector3(-42f, -7f, 0f), Vector3.one, false);
				uISprite5.SetDimensions(65, 65);
			}
			num3 = info.Data.IconID - 10000;
			if (num3 >= mGame.mCompanionData.Count)
			{
				num3 = 0;
			}
			empty = mGame.mCompanionData[num3].iconName;
			uISprite4 = Util.CreateSprite("Icon", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite4, empty, num + 3, new Vector3(-8f, 26f, 0f), Vector3.one, false);
			uISprite4.SetDimensions(50, 50);
		}
		else
		{
			num3 = info.Data.IconID;
			if (num3 >= 10000)
			{
				num3 -= 10000;
			}
			if (num3 >= mGame.mCompanionData.Count)
			{
				num3 = 0;
			}
			empty = mGame.mCompanionData[num3].iconName;
			uISprite2 = Util.CreateSprite("Icon", uISprite.gameObject, image);
			Util.SetSpriteInfo(uISprite2, empty, num + 2, new Vector3(-34f, 3f, 0f), Vector3.one, false);
			uISprite2.SetDimensions(84, 84);
			empty = ((info.Data.IconLevel <= 0) ? "lv_1" : ("lv_" + info.Data.IconLevel));
			if (info.Data.UUID == mGame.mPlayer.UUID)
			{
				CompanionData companionData = mGame.mCompanionData[num3];
				empty = ((info.Data.IconLevel >= companionData.maxLevel) ? Def.CharacterIconLevelMaxImageTbl[info.Data.IconLevel] : Def.CharacterIconLevelImageTbl[info.Data.IconLevel]);
			}
			uISprite3 = Util.CreateSprite("Lv", uISprite.gameObject, image2);
			Util.SetSpriteInfo(uISprite3, empty, num + 3, new Vector3(-34f, -36f, 0f), Vector3.one, false);
			if (GameMain.IsFacebookMiniIconEnable())
			{
				if (!string.IsNullOrEmpty(info.Data.Fbid))
				{
					string imageName = "icon_sns00_mini";
					sprite = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
					Util.SetSpriteInfo(sprite, imageName, num + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
				}
				else if (info.Data.UUID == mGame.mPlayer.UUID && IsFacebookLogined())
				{
					string imageName2 = "icon_sns00_mini";
					sprite = Util.CreateSprite("FacebookIcon", uISprite2.gameObject, image);
					Util.SetSpriteInfo(sprite, imageName2, num + 4, new Vector3(25f, 25f, 0f), Vector3.one, false);
				}
			}
		}
		UILabel uILabel = Util.CreateLabel("FriendName", uISprite.gameObject, atlasFont);
		Util.SetLabelInfo(uILabel, info.Data.Name, num + 3, new Vector3(121f, 0f, 0f), 30, 0, 0, UIWidget.Pivot.Center);
		uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
		uILabel.overflowMethod = UILabel.Overflow.ShrinkContent;
		uILabel.SetDimensions(230, 30);
		uILabel.transform.localPosition = new Vector3(86f, 26f);
		if (uITexture != null)
		{
			uITexture.transform.localPosition = new Vector3(-116f, -7f);
		}
		if (uISprite5 != null)
		{
			uISprite5.transform.localPosition = new Vector3(-116f, -7f);
		}
		if (uISprite4 != null)
		{
			uISprite4.transform.localPosition = new Vector3(-86f, 23f);
		}
		if (uISprite2 != null)
		{
			uISprite2.transform.localPosition = new Vector3(-108f, 3f);
		}
		if (uISprite3 != null)
		{
			uISprite3.transform.localPosition = new Vector3(-108f, -36f);
		}
		sprite = Util.CreateSprite("Linetop", uISprite.gameObject, image2);
		Util.SetSpriteInfo(sprite, "line00", num + 1, new Vector3(86f, 7f, 0f), Vector3.one, false);
		sprite.type = UIBasicSprite.Type.Sliced;
		sprite.SetDimensions(292, 6);
		if (friendItem.Mode == GameMain.RANKINGMode.STAR)
		{
			sprite = Util.CreateSprite("MapIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(sprite, "icon_ranking_star", num + 3, new Vector3(-7f, -21f, 0f), Vector3.one, false);
			sprite.SetDimensions(54, 54);
			uILabel = Util.CreateLabel("StageNum", sprite.gameObject, atlasFont);
			int a_main;
			int a_sub;
			Def.SplitStageNo(info.Data.Level, out a_main, out a_sub);
			Util.SetLabelInfo(uILabel, string.Empty + info.Data.TotalStars, num + 4, new Vector3(35f, -5f, 0f), 30, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			if (index == 0)
			{
				mBeforeRanking = index;
			}
			else if (mBeforeData == info.Data.TotalStars)
			{
				index = mBeforeRanking;
			}
			else
			{
				mBeforeRanking = index;
			}
			mBeforeData = info.Data.TotalStars;
		}
		else if (friendItem.Mode == GameMain.RANKINGMode.STAGE)
		{
			int num4 = 1;
			List<PlayStageParam> playStage = info.Data.PlayStage;
			num4 = ((playStage.Count != 0) ? mGame.HavePlayStageLevel(playStage) : mGame.NoHavePlayStageLevel(info.Data.Level));
			SMDTools sMDTools = new SMDTools();
			sMDTools.SMDSeries(uISprite.gameObject, num, (int)mGame.mPlayer.Data.CurrentSeries, num4, 31f, -8f);
			if (index == 0)
			{
				mBeforeRanking = index;
			}
			else if (mBeforeData == num4)
			{
				index = mBeforeRanking;
			}
			else
			{
				mBeforeRanking = index;
			}
			mBeforeData = num4;
		}
		else
		{
			sprite = Util.CreateSprite("StarIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(sprite, "icon_ranking_trophy", num + 3, new Vector3(-7f, -21f, 0f), Vector3.one, false);
			sprite.color = new Color(1f, 1f, 1f, 0.85f);
			sprite.SetDimensions(50, 50);
			uILabel = Util.CreateLabel("StageNum", sprite.gameObject, atlasFont);
			Util.SetLabelInfo(uILabel, string.Empty + info.Data.TotalTrophy, num + 4, new Vector3(35f, -5f, 0f), 30, 0, 0, UIWidget.Pivot.Left);
			uILabel.color = Def.DEFAULT_MESSAGE_COLOR;
			if (index == 0)
			{
				mBeforeRanking = index;
			}
			else if (mBeforeData == info.Data.TotalTrophy)
			{
				index = mBeforeRanking;
			}
			else
			{
				mBeforeRanking = index;
			}
			mBeforeData = info.Data.TotalTrophy;
		}
		NumberImageString numberImageString = Util.CreateGameObject("Number", uISprite.gameObject).AddComponent<NumberImageString>();
		if (3 > index)
		{
			sprite = Util.CreateSprite("RankingIcon", uISprite.gameObject, image2);
			Util.SetSpriteInfo(sprite, "ranking" + (index + 1) + "_02", num + 3, new Vector3(-198f, 0f, 0f), Vector3.one, false);
		}
		else if (index > 2 && 9 > index)
		{
			numberImageString.Init(1, index + 1, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, false, image2);
			numberImageString.transform.localPosition = new Vector3(-198f, 0f, 0f);
		}
		else
		{
			numberImageString.Init(2, index + 1, mBaseDepth + 3, 1f, UIWidget.Pivot.Center, false, image2);
			numberImageString.SetPitch(32f);
			numberImageString.transform.localPosition = new Vector3(-198f, 0f, 0f);
		}
	}

	public override void OnOpenFinished()
	{
		ChangeMode(Mode);
	}

	public override void OnCloseFinished()
	{
		if (mCallback != null)
		{
			mCallback();
		}
		Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		base.Close();
		if (mFriendItemList != null)
		{
			GameMain.SafeDestroy(mFriendItemList.gameObject);
			mFriendItemList = null;
		}
		if (Mode == GameMain.RANKINGMode.STAR)
		{
			mGame.ClearModifiedGifts(GameMain.GiftUIMode.RECEIVE);
		}
		else if (Mode == GameMain.RANKINGMode.STAGE)
		{
			mGame.ClearModifiedGifts(GameMain.GiftUIMode.RESCUE);
		}
	}

	public override void OnBackKeyPress()
	{
		OnCancelPushed(null);
	}

	public void OnCancelPushed(GameObject go)
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN)
		{
			mGame.PlaySe("SE_NEGATIVE", -1);
			Close();
		}
	}

	private void ActivateTab(GameMain.RANKINGMode mode)
	{
		switch (mode)
		{
		case GameMain.RANKINGMode.STAR:
			Util.SetImageButtonColor(mTabButtonStar, Color.white);
			Util.SetImageButtonColor(mTabButtonStage, Color.gray);
			Util.SetImageButtonColor(mTabButtonTrophy, Color.gray);
			mTabButtonStar.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 1;
			mTabButtonStage.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			mTabButtonTrophy.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			break;
		case GameMain.RANKINGMode.STAGE:
			Util.SetImageButtonColor(mTabButtonStar, Color.gray);
			Util.SetImageButtonColor(mTabButtonStage, Color.white);
			Util.SetImageButtonColor(mTabButtonTrophy, Color.gray);
			mTabButtonStar.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			mTabButtonStage.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 1;
			mTabButtonTrophy.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			break;
		case GameMain.RANKINGMode.TROPHY:
			Util.SetImageButtonColor(mTabButtonStar, Color.gray);
			Util.SetImageButtonColor(mTabButtonStage, Color.gray);
			Util.SetImageButtonColor(mTabButtonTrophy, Color.white);
			mTabButtonStar.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			mTabButtonStage.gameObject.GetComponent<UISprite>().depth = mBaseDepth - 1;
			mTabButtonTrophy.gameObject.GetComponent<UISprite>().depth = mBaseDepth + 1;
			break;
		}
	}

	public void OnStarTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && Mode != 0)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.RANKINGMode mode = GameMain.RANKINGMode.STAR;
			ActivateTab(mode);
			ChangeMode(mode);
		}
	}

	public void OnStageTabPushed()
	{
		SetLayout(Util.ScreenOrientation);
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && Mode != GameMain.RANKINGMode.STAGE)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.RANKINGMode mode = GameMain.RANKINGMode.STAGE;
			ActivateTab(mode);
			ChangeMode(mode);
		}
	}

	public void OnTrophyTabPushed()
	{
		if (!GetBusy() && mState.GetStatus() == STATE.MAIN && mJelly.IsMoveFinished() && Mode != GameMain.RANKINGMode.TROPHY)
		{
			mGame.PlaySe("SE_POSITIVE", -1);
			GameMain.RANKINGMode mode = GameMain.RANKINGMode.TROPHY;
			ActivateTab(mode);
			ChangeMode(mode);
		}
	}

	private void OnSendErrorDialogClosed(ConfirmDialog.SELECT_ITEM item)
	{
		EnableList(true);
		mConfirmDialog = null;
		mState.Change(STATE.MAIN);
	}

	private void EnableList(bool enabled)
	{
		if (!(mFriendItemList == null))
		{
			NGUITools.SetActive(mFriendItemList.gameObject, enabled);
		}
	}

	public void SetClosedCallback(OnDialogClosed callback)
	{
		mCallback = callback;
	}

	public int StarItemSort(FriendItem a, FriendItem b)
	{
		if (b.Info.Data.TotalStars > a.Info.Data.TotalStars)
		{
			return 1;
		}
		if (a.Info.Data.TotalStars > b.Info.Data.TotalStars)
		{
			return -1;
		}
		if (b.Info.Data.UUID == mGame.mPlayer.UUID)
		{
			return 1;
		}
		if (b.Info.Data.UUID > a.Info.Data.UUID)
		{
			return 1;
		}
		if (a.Info.Data.UUID > b.Info.Data.UUID)
		{
			return -1;
		}
		return 0;
	}

	public int LevelItemSort(FriendItem a, FriendItem b)
	{
		int num = 0;
		int num2 = 0;
		List<PlayStageParam> playStage = a.Info.Data.PlayStage;
		List<PlayStageParam> playStage2 = b.Info.Data.PlayStage;
		num = ((playStage.Count != 0) ? mGame.HavePlayStageLevel(playStage) : mGame.NoHavePlayStageLevel(a.Info.Data.Level));
		num2 = ((playStage2.Count != 0) ? mGame.HavePlayStageLevel(playStage2) : mGame.NoHavePlayStageLevel(b.Info.Data.Level));
		if (num2 > num)
		{
			return 1;
		}
		if (num > num2)
		{
			return -1;
		}
		if (b.Info.Data.UUID == mGame.mPlayer.UUID)
		{
			return 1;
		}
		if (b.Info.Data.UUID > a.Info.Data.UUID)
		{
			return 1;
		}
		if (a.Info.Data.UUID > b.Info.Data.UUID)
		{
			return -1;
		}
		return 0;
	}

	public int TrophyItemSort(FriendItem a, FriendItem b)
	{
		if (b.Info.Data.TotalTrophy > a.Info.Data.TotalTrophy)
		{
			return 1;
		}
		if (a.Info.Data.TotalTrophy > b.Info.Data.TotalTrophy)
		{
			return -1;
		}
		if (b.Info.Data.UUID == mGame.mPlayer.UUID)
		{
			return 1;
		}
		if (b.Info.Data.UUID > a.Info.Data.UUID)
		{
			return 1;
		}
		if (a.Info.Data.UUID > b.Info.Data.UUID)
		{
			return -1;
		}
		return 0;
	}
}
