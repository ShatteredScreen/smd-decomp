public class EventEffectDispInfo
{
	public short mIsAdvEvent { get; set; }

	public short mEventId { get; set; }
}
