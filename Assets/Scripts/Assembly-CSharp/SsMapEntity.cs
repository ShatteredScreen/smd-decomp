using UnityEngine;

public struct SsMapEntity
{
	public string Name;

	public string AnimeKey;

	public Vector3 Position;

	public Vector3 Scale;

	public int MapAtlasIndex;

	public int MapOffsetCounter;

	public float Angle;

	public bool EnableDisplay;

	public string Start;

	public string End;
}
