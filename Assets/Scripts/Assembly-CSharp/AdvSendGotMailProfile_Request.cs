using System.Collections.Generic;

public class AdvSendGotMailProfile_Request : RequestBase
{
	[MiniJSONAlias("targets")]
	public List<AdvMailTarget> MailTargets { get; set; }

	[MiniJSONAlias("request_id")]
	public string RequestID { get; set; }

	[MiniJSONAlias("is_retry")]
	public int IsRetry { get; set; }
}
